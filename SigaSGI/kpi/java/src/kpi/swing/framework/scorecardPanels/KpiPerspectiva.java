package kpi.swing.framework.scorecardPanels;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JToolBar;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import kpi.beans.JBITextArea;
import kpi.core.BIRequest;
import kpi.core.KpiDataController;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import pv.jfcx.JPVEdit;

public class KpiPerspectiva extends JInternalFrame implements KpiDefaultFrameFunctions {

    private String recordType           = "PERSPECTIVA"; //NOI18N
    private String formType             = recordType;//Tipo do Formulario
    private String parentType           = "FORMMATRIX";//Tipo formulario pai, caso haja. //NOI18N
    private String matrixParent         = ""; //NOI18N
    public final static int INSERTING   = 1;
    public final static int UPDATING    = 2;
    public final static int NORMAL      = 3;
    
    public KpiPerspectiva(int operation, String idAux, String contextId) {
	initComponents();
	putClientProperty("MAXI", true); //NOI18N

	event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
	event.setEnableFields(pnlPespectiva, true);
	btnSave.setEnabled(true);
    }

    @Override
    public void disableFields() {
	event.setEnableFields(pnlPespectiva, false);
    }

    @Override
    public void refreshFields() {
	txtNome.setText(record.getString("NOME"));
	txtDescricao.setText(record.getString("DESCRICAO"));
	txtOrdem.setValue(record.getInt("ORDEM"));
        
        // Caso esteja em modo de inser��o, n�o aparece o spinner para sele��o de ordem.
        if (this.getStatus() == INSERTING) {
            lblOrdem.setVisible(false);
            txtOrdem.setVisible(false);
            txtOrdem.setValue(0);
        } else {
            lblOrdem.setVisible(true);
            txtOrdem.setVisible(true);
        }
    }

    @Override
    public BIXMLRecord getFieldsContents() {
	BIXMLRecord recordAux = new BIXMLRecord(recordType);

	if (matrixParent.equals("0")) { //NOI18N
	    recordAux.set("PARENTID", ""); //NOI18N
	} else {
	    recordAux.set("PARENTID", matrixParent); //NOI18N
	}

	recordAux.set("ID", id); //NOI18N
	recordAux.set("NOME", txtNome.getText()); //NOI18N
	recordAux.set("DESCRICAO", txtDescricao.getText()); //NOI18N
	recordAux.set("ORDEM", txtOrdem.getValue().toString()); //NOI18N

	return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
	boolean retorno = true;

	if (txtNome.getText().trim().equals("")) { //NOI18N
	    errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUnidade_00005"));
	    retorno = false;
	}

	if (!retorno) {
	    btnSave.setEnabled(true);
	}
	return retorno;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        btnReload = new JButton();
        btnAjuda = new JButton();
        pnlParentName = new JPanel();
        lblOrganizacao = new JLabel();
        lblEstrategia = new JLabel();
        pnlRecord = new JPanel();
        pnlLeftForm = new JPanel();
        pnlRightForm = new JPanel();
        pnlBottomForm = new JPanel();
        jTabbedPane1 = new JTabbedPane();
        scpRecord = new JScrollPane();
        pnlPespectiva = new JPanel();
        lblNome = new JLabel();
        txtNome = new JPVEdit();
        lblDescricao = new JLabel();
        scrDescricao = new JScrollPane();
        txtDescricao = new JBITextArea();
        txtOrdem = new JSpinner();
        lblOrdem = new JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiPerspectiva_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_perspectiva.gif")));         setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(480, 312));

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 83));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif")));         btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif")));         btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(340, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(250, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif")));         btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif")));         btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif")));         btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif")));         ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        pnlParentName.setBackground(new Color(223, 229, 243));
        pnlParentName.setBorder(BorderFactory.createLineBorder(new Color(221, 221, 221)));
        pnlParentName.setPreferredSize(new Dimension(10, 55));

        lblOrganizacao.setText("jLabel1");

        lblEstrategia.setText("jLabel1");

        GroupLayout pnlParentNameLayout = new GroupLayout(pnlParentName);
        pnlParentName.setLayout(pnlParentNameLayout);
        pnlParentNameLayout.setHorizontalGroup(
            pnlParentNameLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(pnlParentNameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlParentNameLayout.createParallelGroup(Alignment.LEADING)
                    .addComponent(lblOrganizacao, GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)
                    .addComponent(lblEstrategia, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlParentNameLayout.setVerticalGroup(
            pnlParentNameLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(pnlParentNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrganizacao)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(lblEstrategia)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlTools.add(pnlParentName, BorderLayout.NORTH);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        pnlRecord.setLayout(new BorderLayout());
        pnlRecord.add(pnlLeftForm, BorderLayout.WEST);
        pnlRecord.add(pnlRightForm, BorderLayout.EAST);
        pnlRecord.add(pnlBottomForm, BorderLayout.SOUTH);

        scpRecord.setBorder(null);

        pnlPespectiva.setLayout(null);

        lblNome.setForeground(new Color(51, 51, 255));
        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiPerspectiva_00002")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new Dimension(100, 16));
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlPespectiva.add(lblNome);
        lblNome.setBounds(10, 10, 100, 20);

        txtNome.setMaxLength(120);
        pnlPespectiva.add(txtNome);
        txtNome.setBounds(10, 30, 400, 22);

        lblDescricao.setText(bundle.getString("KpiPerspectiva_00003")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlPespectiva.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 60, 20);

        txtDescricao.setFont(new Font("Tahoma", 0, 11));         scrDescricao.setViewportView(txtDescricao);

        pnlPespectiva.add(scrDescricao);
        scrDescricao.setBounds(10, 70, 400, 60);
        pnlPespectiva.add(txtOrdem);
        txtOrdem.setBounds(10, 150, 400, 22);

        lblOrdem.setText(bundle.getString("KpiPerspectiva_00004")); // NOI18N
        lblOrdem.setEnabled(false);
        pnlPespectiva.add(lblOrdem);
        lblOrdem.setBounds(10, 130, 60, 20);

        scpRecord.setViewportView(pnlPespectiva);

        jTabbedPane1.addTab(bundle1.getString("KpiPerspectiva_00001"), scpRecord); // NOI18N

        pnlRecord.add(jTabbedPane1, BorderLayout.CENTER);

        getContentPane().add(pnlRecord, BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
	    event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
	    event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); //NOI18N

            StringBuilder msg = new StringBuilder();

            msg.append(bundle.getString("MatrixFrameBehavior_00009")); //NOI18N
            msg.append(bundle.getString("MatrixFrameBehavior_00006")); //NOI18N

            if (event.cascateDelete(msg.toString(), this)) {
		setID(""); //NOI18N
		event.loadRecord();
	    }
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
	    event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
	    btnSave.setEnabled(false);
	    event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
	    event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAjuda;
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnReload;
    private JButton btnSave;
    private JTabbedPane jTabbedPane1;
    private JLabel lblDescricao;
    private JLabel lblEstrategia;
    private JLabel lblNome;
    private JLabel lblOrdem;
    private JLabel lblOrganizacao;
    private JPanel pnlBottomForm;
    private JPanel pnlConfirmation;
    private JPanel pnlLeftForm;
    private JPanel pnlOperation;
    private JPanel pnlParentName;
    private JPanel pnlPespectiva;
    private JPanel pnlRecord;
    private JPanel pnlRightForm;
    private JPanel pnlTools;
    private JScrollPane scpRecord;
    private JScrollPane scrDescricao;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JBITextArea txtDescricao;
    private JPVEdit txtNome;
    private JSpinner txtOrdem;
    // End of variables declaration//GEN-END:variables
    private MatrixFrameBehavior event = new MatrixFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
	status = value;
    }

    @Override
    public int getStatus() {
	return status;
    }

    @Override
    public void setType(String value) {
	recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
	return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
	id = String.valueOf(value);
    }

    @Override
    public String getID() {
	return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
	if ("COMMANDS".equals(recordAux.getTagName())) { //NOI18N
	    BIXMLAttributes att = recordAux.getAttributes();
	    matrixParent = att.getString("PARENTID");
            
            BIRequest request = new BIRequest();
            KpiDataController dataController = KpiStaticReferences.getKpiDataController();

            request.setParameter("TIPO", "PERSPECTIVA"); //NOI18N
            request.setParameter("PARENTID", matrixParent); //NOI18N

            BIXMLRecord rec = dataController.listRecords(request);
            
            StringBuilder orgName = new StringBuilder();

            orgName.append("<html>"); //NOI18N
            orgName.append("<b>");
            orgName.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMapaDrillObjetivo_00001"));
            orgName.append("</b> ");

            orgName.append(rec.getBIXMLVector("PARENTS_NAMES").getAttributes().getString("ORGANIZACAO_NOME")); //NOI18N
            orgName.append("</html>"); //NOI18N
            
            lblOrganizacao.setText(orgName.toString());
            
            StringBuilder estName = new StringBuilder();

            estName.append("<html>"); //NOI18N
            estName.append("<b>");
            estName.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMapaDrillObjetivo_00002"));
            estName.append("</b> ");
            estName.append(rec.getBIXMLVector("PARENTS_NAMES").getAttributes().getString("ESTRATEGIA_NOME")); //NOI18N
            estName.append("</html>"); //NOI18N
            
            lblEstrategia.setText(estName.toString());

	} else {
	    record = recordAux;
	}
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
	return record;
    }

    @Override
    public void showDataButtons() {
	pnlConfirmation.setVisible(true);
	pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
	pnlConfirmation.setVisible(false);
	pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
	return this;
    }

    @Override
    public String getParentID() {
	return "0";//Sempre zero para fazer refresh no parent. //NOI18N
    }

    @Override
    public void loadRecord() {
	event.loadRecord();
    }

    @Override
    public String getFormType() {
	return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
	return null;
    }

    @Override
    public String getParentType() {
	return parentType;
    }
}
