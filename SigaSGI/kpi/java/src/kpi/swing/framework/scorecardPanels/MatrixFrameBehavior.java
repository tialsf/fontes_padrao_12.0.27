/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kpi.swing.framework.scorecardPanels;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.swing.KpiTreeNode;

/**
 *
 * @author asilva
 */
public class MatrixFrameBehavior extends KpiDefaultFrameBehavior {
    private KpiTreeNode treeNodeSelected = null;
    
    public MatrixFrameBehavior(KpiDefaultFrameFunctions frameAux) {
	super(frameAux);
    }
    /**
     * M�todo para execu��o de exclus�o em cascata.
     * @param msg Mensagem a ser Exibida.
     * @param parentFrame Frame a que se refere a exclus�o.
     * @return indica se atualiza ou n�o os registros.
     */
    public boolean cascateDelete(String msg, JInternalFrame parentFrame) {
	String segura = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("MatrixFrameBehavior_00001");
	String recursiva = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("MatrixFrameBehavior_00002");
	String cancelar = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("MatrixFrameBehavior_00003");
	final Object[] opcoes = {segura, recursiva, cancelar};

	final int escolha = JOptionPane.showOptionDialog(parentFrame,
		msg,
		java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("MatrixFrameBehavior_00008"),
		JOptionPane.DEFAULT_OPTION,
		JOptionPane.QUESTION_MESSAGE,
		null,
		opcoes,
		opcoes[0]);

	if (escolha == JOptionPane.YES_OPTION) {
	    super.deleteRecord("NORMAL"); //NOI18N
	} else if (escolha == JOptionPane.NO_OPTION) {
	    super.deleteRecord("CASCATA"); //NOI18N
	}

	return false;
    }

    /**
     * @return the treeNodeSelected
     */
    public KpiTreeNode getTreeNodeSelected() {
	return treeNodeSelected;
    }

    /**
     * @param treeNodeSelected the treeNodeSelected to set
     */
    public void setTreeNodeSelected(KpiTreeNode treeNodeSelected) {
	this.treeNodeSelected = treeNodeSelected;
    }
}