package kpi.swing.graph;

import ChartDirector.Chart;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ItemEvent;
import java.awt.image.BufferedImage;
import javax.swing.JTable;
import kpi.beans.JBIXMLTable;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;
import java.util.*;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.event.TableModelEvent;
import kpi.beans.JBIChartDirectorController;
import kpi.beans.JBIChartDirectorSerie;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiFileChooser;
import kpi.swing.analisys.KpiScoreCarding;
import kpi.util.KpiToolKit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class KpiGraphIndicador extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    //ATEN��O. Este valores dever ser iguais ao do KPI_DEFS, constantes:
    //STATUS_GREEN,STATUS_YELLOW,STATUS_RED,STATUS_BLUE
    public final static int COL_STATUS      = 3;
    public final static int LABEL           = 0;
    public final static int VALOR           = 1;
    public final static int META            = 2;
    public final static int DIFERENCA       = 3;
    public final static int VALOR_ACUM      = 4;
    public final static int META_ACUM       = 5;
    public final static int PREVIA          = 6;
    public final static int PREVIA_ACUM     = 7;
    private String recordType = "GRAPH_IND";
    private String formType = recordType;
    private String parentType = recordType;
    private JTable oTable = null;	
    private String graphIndID;
    private MyPlaDadosCellRenderer cellRenderer;
    private kpi.beans.JBIChartDirectorController graphController = null;
    private BIXMLVector planilhaDados = null;
    private BIXMLRecord recDadosInd = null;
    private Vector vctValores = new Vector();
    private Vector vctColors = new Vector();
    private String[] legendas = {
        java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00054"),
        java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00032"),
        java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00031"),
        java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00030")
    };
    private boolean previaEnabled = true;
    private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
        if (btnConfigGraph.isVisible()) {
            btnFechar.setVisible(false);
        }
    }

    @Override
    public void refreshFields() {
        loadGraphRecord("true");
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        recordAux.set("ID", id);
        recordAux.set("INDICADOR", record.getString("INDICADOR"));

        //Op��es do gr�fico
        recordAux.set("TIPO", cmbTipoGrafico.getSelectedIndex());//Tipo do grafico.
        recordAux.set("POS_LEGENDA", cmbLegenda.getSelectedIndex());//Posicao da legenda
        recordAux.set("COR_FUNDO", jPVCorFundo.getColor().getRGB());//Cor de fundo.
        recordAux.set("LINHA_COLUNA", chkLinhaColuna.isSelected() ? "T" : "F");//Mostra linha X coluna invertido?
        recordAux.set("LEGENDA_X", chkLegendaX.isSelected() ? "T" : "F");//Mostra a legenda do eixo X.
        recordAux.set("LEGENDA_Y", chkLegendaY.isSelected() ? "T" : "F");//Mostra a legenda do eixo Y.
        recordAux.set("LEGENDA_VAL", chkLegendaValores.isSelected() ? "T" : "F");//Mostra a legenda dos valores.
        recordAux.set("MES_EXTENSO", chkMesExtenso.isSelected() ? "T" : "F");//Mostra o nome do mes por extenso?
        recordAux.set("MOSTRA_VALOR", chkValor.isSelected() ? "T" : "F");//Mostra a valor
        recordAux.set("MOSTRA_META", chkMeta.isSelected() ? "T" : "F");//Mostra a meta
        recordAux.set("MOSTRA_PREVIA", chkPrevia.isSelected() ? "T" : "F");//Mostra a previa
        recordAux.set("MOSTRA_VALOR_ACUM", chkValorAcum.isSelected() ? "T" : "F");//Mostra a valor acumulado
        recordAux.set("MOSTRA_META_ACUM", chkMetaAcum.isSelected() ? "T" : "F");//Mostra a meta acumulada
        recordAux.set("MOSTRA_PREVIA_ACUM", chkPreviaAcum.isSelected() ? "T" : "F");//Mostra a previa acumulada
        recordAux.set("MOSTRA_DIFERENCA", chkDiferenca.isSelected() ? "T" : "F");//Mostra a diferenca
        recordAux.set("COR_VALOR", jPVCorValor.getColor().getRGB());//Cor do valor
        recordAux.set("COR_META", jPVCorMeta.getColor().getRGB());//Cor da meta
        recordAux.set("COR_PREVIA", jPVCorPrevia.getColor().getRGB());//Cor da previa
        recordAux.set("COR_VALOR_ACUM", jPVCorValorAcum.getColor().getRGB());//Cor do valor acumulado
        recordAux.set("COR_META_ACUM", jPVCorMetaAcum.getColor().getRGB());//Cor da meta acumulada
        recordAux.set("COR_PREVIA_ACUM", jPVCorPreviaAcum.getColor().getRGB());//Cor da previa acumulada
        recordAux.set("COR_DIFERENCA", jPVCorDiferenca.getColor().getRGB());//Cor da diferenca
        recordAux.set("TIPO_VALOR", cmbValorTipo.getSelectedIndex());//Tipo da serie do valor
        recordAux.set("TIPO_META", cmbMetaTipo.getSelectedIndex());//Tipo da serie da meta
        recordAux.set("TIPO_PREVIA", cmbPreviaTipo.getSelectedIndex());//Tipo da serie previa
        recordAux.set("TIPO_VALOR_ACUM", cmbValorTipoAcum.getSelectedIndex());//Tipo da serie do valor acumulado
        recordAux.set("TIPO_META_ACUM", cmbMetaTipoAcum.getSelectedIndex());//Tipo da serie da meta acumulada
        recordAux.set("TIPO_PREVIA_ACUM", cmbPreviaTipoAcum.getSelectedIndex());//Tipo da serie previa acumulada
        recordAux.set("TIPO_DIFERENCA", cmbDiferencaTipo.getSelectedIndex());//Tipo da serie diferenca
        recordAux.set("POS_SPLIT_VERT", 0);//Tipo da serie diferenca
        recordAux.set("GRAPH_DATADE", datePane.getDataDe());//Tipo da serie diferenca
        recordAux.set("GRAPH_DATAATE", datePane.getDataAte());//Tipo da serie diferenca
        recordAux.set("ZOOM", (String) cbbZoom.getSelectedItem());//Valor do Zoom;

        //Mostra a coluna de acumulados
        int item = planilhaDados.size() - 1;
        javax.swing.JCheckBox oTmpCheck = (javax.swing.JCheckBox) pnlSelectLine.getComponent(item);
        recordAux.set("MOSTRA_ACUMULADO", oTmpCheck.isSelected() ? "T" : "F");

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        toolBar = new javax.swing.JToolBar();
        btnConfigGraph = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        sprConfiguracao = new javax.swing.JSeparator();
        datePane = new kpi.beans.JBIDatePane(this);
        sprOperacao1 = new javax.swing.JToolBar.Separator();
        btnReload = new javax.swing.JButton();
        sprOperacao3 = new javax.swing.JToolBar.Separator();
        btnSave = new javax.swing.JButton();
        sprPeriodo = new javax.swing.JToolBar.Separator();
        btnAnalitico = new javax.swing.JButton();
        sprOperacao2 = new javax.swing.JToolBar.Separator();
        btnExportacao = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        sprPeriodo1 = new javax.swing.JToolBar.Separator();
        btnAjuda = new javax.swing.JButton();
        splitGrafico = new javax.swing.JSplitPane();
        pnlOpcao = new javax.swing.JPanel();
        tskGrafico = new com.l2fprod.common.swing.JTaskPane();
        tspGrafico = new com.l2fprod.common.swing.JTaskPaneGroup();
        pnlGraficao = new javax.swing.JPanel();
        javax.swing.JLabel lblTipo = new javax.swing.JLabel();
        cmbTipoGrafico = new javax.swing.JComboBox();
        javax.swing.JLabel lblLegenda = new javax.swing.JLabel();
        cmbLegenda = cmbLegenda = new javax.swing.JComboBox(legendas);
        jPVCorFundo = new pv.jfcx.JPVColor(Color.white);
        lblCorSerie1 = new javax.swing.JLabel();
        chkLegendaY = new javax.swing.JCheckBox();
        chkMesExtenso = new javax.swing.JCheckBox();
        chkLegendaX = new javax.swing.JCheckBox();
        chkLinhaColuna = new javax.swing.JCheckBox();
        chkLegendaValores = new javax.swing.JCheckBox();
        lblSerieValor1 = new javax.swing.JLabel();
        cbbZoom = new javax.swing.JComboBox();
        lblZoom = new javax.swing.JLabel();
        tspSerie = new com.l2fprod.common.swing.JTaskPaneGroup();
        pnlSerie = new javax.swing.JPanel();
        lblSerieTipo = new javax.swing.JLabel();

        cmbValorTipo = new javax.swing.JComboBox();

        cmbMetaTipo = new javax.swing.JComboBox();

        cmbDiferencaTipo = new javax.swing.JComboBox();

        cmbPreviaTipo = new javax.swing.JComboBox();
        jPVCorPrevia = new pv.jfcx.JPVColor(new java.awt.Color(139,191,150));
        jPVCorDiferenca = new pv.jfcx.JPVColor(new java.awt.Color(27,95,232));
        jPVCorMeta = new pv.jfcx.JPVColor(new java.awt.Color(255,235,155));
        jPVCorValor = new pv.jfcx.JPVColor(new java.awt.Color(202,138,27));
        lblCorSerie = new javax.swing.JLabel();
        chkPrevia = new javax.swing.JCheckBox();
        chkDiferenca = new javax.swing.JCheckBox();
        chkMeta = new javax.swing.JCheckBox();
        chkValor = new javax.swing.JCheckBox();
        lblSerieValor = new javax.swing.JLabel();
        chkValorAcum = new javax.swing.JCheckBox();
        jPVCorValorAcum = new pv.jfcx.JPVColor(new java.awt.Color(202,138,27));

        cmbValorTipoAcum = new javax.swing.JComboBox();
        chkMetaAcum = new javax.swing.JCheckBox();
        jPVCorMetaAcum = new pv.jfcx.JPVColor(new java.awt.Color(255,235,155));

        cmbMetaTipoAcum = new javax.swing.JComboBox();
        chkPreviaAcum = new javax.swing.JCheckBox();
        jPVCorPreviaAcum = new pv.jfcx.JPVColor(new java.awt.Color(139,191,150));

        cmbPreviaTipoAcum = new javax.swing.JComboBox();
        tspColuna = new com.l2fprod.common.swing.JTaskPaneGroup();
        crlSelectLine = new javax.swing.JScrollPane();
        pnlSelectLine = new javax.swing.JPanel();
        tpAnalise = new javax.swing.JTabbedPane();
        scrGraph = new javax.swing.JScrollPane();
        pnlGraph = new javax.swing.JPanel();
        oChartViewer = new ChartDirector.ChartViewer();
        tbPlanilha = new kpi.beans.JBIXMLTable();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiGraphIndicador_00007")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grafico.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(600, 358));

        toolBar.setFloatable(false);
        toolBar.setRollover(true);
        toolBar.setPreferredSize(new java.awt.Dimension(250, 25));

        btnConfigGraph.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ferramentas.gif"))); // NOI18N
        btnConfigGraph.setText(bundle.getString("KpiGraphIndicador_00009")); // NOI18N
        btnConfigGraph.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfigGraphActionPerformed(evt);
            }
        });
        toolBar.add(btnConfigGraph);

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/close-normal.gif"))); // NOI18N
        btnFechar.setText("Fechar Configura��o ");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });
        toolBar.add(btnFechar);

        sprConfiguracao.setOrientation(javax.swing.SwingConstants.VERTICAL);
        sprConfiguracao.setMaximumSize(new java.awt.Dimension(5, 20));
        sprConfiguracao.setPreferredSize(new java.awt.Dimension(5, 0));
        toolBar.add(sprConfiguracao);

        datePane.setDataAlvoVsisible(false);
        datePane.setMaximumSize(new java.awt.Dimension(290, 30));
        datePane.setMinimumSize(new java.awt.Dimension(270, 20));
        datePane.setPreferredSize(new java.awt.Dimension(290, 20));
        toolBar.add(datePane);
        toolBar.add(sprOperacao1);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        toolBar.add(btnReload);
        toolBar.add(sprOperacao3);

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        toolBar.add(btnSave);
        toolBar.add(sprPeriodo);

        btnAnalitico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        btnAnalitico.setText("Relat�rio Anal�tico");
        btnAnalitico.setEnabled(false);
        btnAnalitico.setFocusable(false);
        btnAnalitico.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnAnalitico.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAnalitico.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAnalitico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnaliticoActionPerformed(evt);
            }
        });
        toolBar.add(btnAnalitico);
        toolBar.add(sprOperacao2);

        btnExportacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_exportacao.gif"))); // NOI18N
        btnExportacao.setText(bundle.getString("KpiGraphIndicador_00034")); // NOI18N
        btnExportacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportacaoActionPerformed(evt);
            }
        });
        toolBar.add(btnExportacao);

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        btnAbrir.setText(bundle.getString("KpiGraphIndicador_00033")); // NOI18N
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        toolBar.add(btnAbrir);
        toolBar.add(sprPeriodo1);

        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("KpiGraphIndicador_00011")); // NOI18N
        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        btnAjuda.setToolTipText(bundle2.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda.setPreferredSize(new java.awt.Dimension(30, 23));
        toolBar.add(btnAjuda);

        getContentPane().add(toolBar, java.awt.BorderLayout.NORTH);

        splitGrafico.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        splitGrafico.setDividerLocation(320);
        splitGrafico.setDividerSize(0);
        splitGrafico.setMaximumSize(new java.awt.Dimension(400, 320));
        splitGrafico.setMinimumSize(new java.awt.Dimension(400, 0));
        splitGrafico.setOneTouchExpandable(true);
        splitGrafico.setOpaque(false);
        splitGrafico.setPreferredSize(new java.awt.Dimension(400, 0));
        splitGrafico.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                splitGraficoComponentResized(evt);
            }
        });

        pnlOpcao.setPreferredSize(new java.awt.Dimension(400, 352));
        pnlOpcao.setLayout(new java.awt.BorderLayout());

        tspGrafico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grafico.gif"))); // NOI18N
        tspGrafico.setScrollOnExpand(true);
        tspGrafico.setSpecial(true);
        tspGrafico.setTitle("Gr�fico");

        pnlGraficao.setOpaque(false);
        pnlGraficao.setPreferredSize(new java.awt.Dimension(390, 170));
        pnlGraficao.setLayout(null);

        lblTipo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTipo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTipo.setText(bundle.getString("KpiGraphIndicador_00062")); // NOI18N
        lblTipo.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlGraficao.add(lblTipo);
        lblTipo.setBounds(130, 20, 40, 20);

        cmbTipoGrafico.setMaximumSize(new java.awt.Dimension(95, 22));
        cmbTipoGrafico.setMinimumSize(new java.awt.Dimension(95, 22));
        cmbTipoGrafico.setPreferredSize(new java.awt.Dimension(95, 22));
        cmbTipoGrafico.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbTipoGraficoItemStateChanged(evt);
            }
        });
        pnlGraficao.add(cmbTipoGrafico);
        cmbTipoGrafico.setBounds(180, 20, 90, 22);

        lblLegenda.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblLegenda.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblLegenda.setText(bundle2.getString("KpiGraphIndicador_00041")); // NOI18N
        lblLegenda.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlGraficao.add(lblLegenda);
        lblLegenda.setBounds(110, 50, 60, 20);

        cmbLegenda.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbLegendaItemStateChanged(evt);
            }
        });
        pnlGraficao.add(cmbLegenda);
        cmbLegenda.setBounds(180, 50, 90, 22);

        jPVCorFundo.setBackground(new java.awt.Color(255, 255, 255));
        jPVCorFundo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPVCorFundoActionPerformed(evt);
            }
        });
        pnlGraficao.add(jPVCorFundo);
        jPVCorFundo.setBounds(180, 110, 90, 22);

        lblCorSerie1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblCorSerie1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCorSerie1.setText("Cor:");
        lblCorSerie1.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlGraficao.add(lblCorSerie1);
        lblCorSerie1.setBounds(120, 110, 50, 20);

        chkLegendaY.setSelected(true);
        chkLegendaY.setText(bundle.getString("KpiGraphIndicador_00016")); // NOI18N
        chkLegendaY.setOpaque(false);
        chkLegendaY.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkLegendaYItemStateChanged(evt);
            }
        });
        pnlGraficao.add(chkLegendaY);
        chkLegendaY.setBounds(0, 50, 120, 22);

        chkMesExtenso.setSelected(true);
        chkMesExtenso.setLabel("M�s por extenso");
        chkMesExtenso.setOpaque(false);
        chkMesExtenso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkMesExtensoActionPerformed(evt);
            }
        });
        pnlGraficao.add(chkMesExtenso);
        chkMesExtenso.setBounds(0, 140, 105, 22);

        chkLegendaX.setSelected(true);
        chkLegendaX.setText(bundle.getString("KpiGraphIndicador_00015")); // NOI18N
        chkLegendaX.setIconTextGap(6);
        chkLegendaX.setOpaque(false);
        chkLegendaX.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkLegendaXItemStateChanged(evt);
            }
        });
        pnlGraficao.add(chkLegendaX);
        chkLegendaX.setBounds(0, 20, 120, 22);

        chkLinhaColuna.setText("Inverter Eixos");
        chkLinhaColuna.setOpaque(false);
        chkLinhaColuna.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkLinhaColunaItemStateChanged(evt);
            }
        });
        pnlGraficao.add(chkLinhaColuna);
        chkLinhaColuna.setBounds(0, 80, 120, 22);

        chkLegendaValores.setSelected(true);
        chkLegendaValores.setText(bundle.getString("KpiGraphIndicador_00063")); // NOI18N
        chkLegendaValores.setOpaque(false);
        chkLegendaValores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkLegendaValoresActionPerformed(evt);
            }
        });
        pnlGraficao.add(chkLegendaValores);
        chkLegendaValores.setBounds(0, 110, 110, 22);

        lblSerieValor1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblSerieValor1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSerieValor1.setText(bundle2.getString("KpiGraphIndicador_00045")); // NOI18N
        lblSerieValor1.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlGraficao.add(lblSerieValor1);
        lblSerieValor1.setBounds(0, 0, 110, 20);

        cbbZoom.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "50", "100", "150", "200" }));
        cbbZoom.setMaximumSize(new java.awt.Dimension(50, 22));
        cbbZoom.setMinimumSize(new java.awt.Dimension(50, 22));
        cbbZoom.setPreferredSize(new java.awt.Dimension(45, 22));
        cbbZoom.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbZoomItemStateChanged(evt);
            }
        });
        pnlGraficao.add(cbbZoom);
        cbbZoom.setBounds(180, 80, 90, 22);

        lblZoom.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblZoom.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblZoom.setText("Zoom:");
        lblZoom.setMaximumSize(new java.awt.Dimension(40, 14));
        lblZoom.setMinimumSize(new java.awt.Dimension(40, 14));
        lblZoom.setPreferredSize(new java.awt.Dimension(40, 14));
        pnlGraficao.add(lblZoom);
        lblZoom.setBounds(130, 80, 40, 20);

        tspGrafico.getContentPane().add(pnlGraficao);

        tskGrafico.add(tspGrafico);

        tspSerie.setExpanded(false);
        tspSerie.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grafico.gif"))); // NOI18N
        tspSerie.setScrollOnExpand(true);
        tspSerie.setTitle("Series");

        pnlSerie.setMinimumSize(new java.awt.Dimension(100, 230));
        pnlSerie.setOpaque(false);
        pnlSerie.setPreferredSize(new java.awt.Dimension(300, 225));
        pnlSerie.setLayout(null);

        lblSerieTipo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblSerieTipo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSerieTipo.setText(bundle2.getString("KpiGraphIndicador_00042")); // NOI18N
        lblSerieTipo.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlSerie.add(lblSerieTipo);
        lblSerieTipo.setBounds(178, 0, 55, 18);

        cmbValorTipo.setMaximumSize(new java.awt.Dimension(80, 22));
        cmbValorTipo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbValorTipoItemStateChanged(evt);
            }
        });
        pnlSerie.add(cmbValorTipo);
        cmbValorTipo.setBounds(180, 20, 90, 20);

        cmbMetaTipo.setMaximumSize(new java.awt.Dimension(80, 22));
        cmbMetaTipo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbMetaTipoItemStateChanged(evt);
            }
        });
        pnlSerie.add(cmbMetaTipo);
        cmbMetaTipo.setBounds(180, 80, 90, 20);

        cmbDiferencaTipo.setMaximumSize(new java.awt.Dimension(80, 22));
        cmbDiferencaTipo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbDiferencaTipoItemStateChanged(evt);
            }
        });
        pnlSerie.add(cmbDiferencaTipo);
        cmbDiferencaTipo.setBounds(180, 140, 90, 20);

        cmbPreviaTipo.setMaximumSize(new java.awt.Dimension(80, 22));
        cmbPreviaTipo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbPreviaTipoItemStateChanged(evt);
            }
        });
        pnlSerie.add(cmbPreviaTipo);
        cmbPreviaTipo.setBounds(180, 170, 90, 20);

        jPVCorPrevia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPVCorPreviaActionPerformed(evt);
            }
        });
        pnlSerie.add(jPVCorPrevia);
        jPVCorPrevia.setBounds(150, 170, 20, 20);

        jPVCorDiferenca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPVCorDiferencaActionPerformed(evt);
            }
        });
        pnlSerie.add(jPVCorDiferenca);
        jPVCorDiferenca.setBounds(150, 140, 20, 20);

        jPVCorMeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPVCorMetaActionPerformed(evt);
            }
        });
        pnlSerie.add(jPVCorMeta);
        jPVCorMeta.setBounds(150, 80, 20, 20);

        jPVCorValor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPVCorValorActionPerformed(evt);
            }
        });
        pnlSerie.add(jPVCorValor);
        jPVCorValor.setBounds(150, 20, 20, 20);

        lblCorSerie.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblCorSerie.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCorSerie.setText(bundle2.getString("KpiGraphIndicador_00020")); // NOI18N
        lblCorSerie.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlSerie.add(lblCorSerie);
        lblCorSerie.setBounds(139, 0, 39, 18);

        chkPrevia.setSelected(true);
        chkPrevia.setText(KpiStaticReferences.getKpiCustomLabels().getPrevia());
        chkPrevia.setOpaque(false);
        chkPrevia.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkPreviaItemStateChanged(evt);
            }
        });
        pnlSerie.add(chkPrevia);
        chkPrevia.setBounds(0, 170, 136, 23);

        chkDiferenca.setSelected(true);
        chkDiferenca.setText("Diferen�a");
        chkDiferenca.setOpaque(false);
        chkDiferenca.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkDiferencaItemStateChanged(evt);
            }
        });
        pnlSerie.add(chkDiferenca);
        chkDiferenca.setBounds(0, 140, 136, 23);

        chkMeta.setSelected(true);
        chkMeta.setText(KpiStaticReferences.getKpiCustomLabels().getMeta());
        chkMeta.setOpaque(false);
        chkMeta.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkMetaItemStateChanged(evt);
            }
        });
        pnlSerie.add(chkMeta);
        chkMeta.setBounds(0, 80, 136, 23);

        chkValor.setSelected(true);
        chkValor.setText(KpiStaticReferences.getKpiCustomLabels().getReal());
        chkValor.setOpaque(false);
        chkValor.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkValorItemStateChanged(evt);
            }
        });
        pnlSerie.add(chkValor);
        chkValor.setBounds(0, 20, 136, 23);

        lblSerieValor.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblSerieValor.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSerieValor.setText("Exibir");
        lblSerieValor.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlSerie.add(lblSerieValor);
        lblSerieValor.setBounds(3, 0, 136, 18);

        chkValorAcum.setSelected(true);
        chkValorAcum.setText(KpiStaticReferences.getKpiCustomLabels().getReal().concat(" Acumulado"));
        chkValorAcum.setOpaque(false);
        chkValorAcum.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkValorAcumItemStateChanged(evt);
            }
        });
        pnlSerie.add(chkValorAcum);
        chkValorAcum.setBounds(0, 50, 136, 20);

        jPVCorValorAcum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPVCorValorAcumActionPerformed(evt);
            }
        });
        pnlSerie.add(jPVCorValorAcum);
        jPVCorValorAcum.setBounds(150, 50, 20, 20);

        cmbValorTipoAcum.setMaximumSize(new java.awt.Dimension(80, 22));
        cmbValorTipoAcum.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbValorTipoAcumItemStateChanged(evt);
            }
        });
        pnlSerie.add(cmbValorTipoAcum);
        cmbValorTipoAcum.setBounds(180, 50, 90, 20);

        chkMetaAcum.setSelected(true);
        chkMetaAcum.setText(KpiStaticReferences.getKpiCustomLabels().getMeta().concat(" Acumulada"));
        chkMetaAcum.setActionCommand(KpiStaticReferences.getKpiCustomLabels().getMeta().concat(" Acumulada"));
        chkMetaAcum.setOpaque(false);
        chkMetaAcum.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkMetaAcumItemStateChanged(evt);
            }
        });
        pnlSerie.add(chkMetaAcum);
        chkMetaAcum.setBounds(0, 110, 136, 20);

        jPVCorMetaAcum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPVCorMetaAcumActionPerformed(evt);
            }
        });
        pnlSerie.add(jPVCorMetaAcum);
        jPVCorMetaAcum.setBounds(150, 110, 20, 20);

        cmbMetaTipoAcum.setMaximumSize(new java.awt.Dimension(80, 22));
        cmbMetaTipoAcum.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbMetaTipoAcumItemStateChanged(evt);
            }
        });
        pnlSerie.add(cmbMetaTipoAcum);
        cmbMetaTipoAcum.setBounds(180, 110, 90, 20);

        chkPreviaAcum.setSelected(true);
        chkPreviaAcum.setText(KpiStaticReferences.getKpiCustomLabels().getPrevia().concat(" Acumulada"));
        chkPreviaAcum.setOpaque(false);
        chkPreviaAcum.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkPreviaAcumItemStateChanged(evt);
            }
        });
        pnlSerie.add(chkPreviaAcum);
        chkPreviaAcum.setBounds(0, 200, 136, 20);

        jPVCorPreviaAcum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPVCorPreviaAcumActionPerformed(evt);
            }
        });
        pnlSerie.add(jPVCorPreviaAcum);
        jPVCorPreviaAcum.setBounds(150, 200, 20, 20);

        cmbPreviaTipoAcum.setMaximumSize(new java.awt.Dimension(80, 22));
        cmbPreviaTipoAcum.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbPreviaTipoAcumItemStateChanged(evt);
            }
        });
        pnlSerie.add(cmbPreviaTipoAcum);
        cmbPreviaTipoAcum.setBounds(180, 200, 90, 20);

        tspSerie.getContentPane().add(pnlSerie);

        tskGrafico.add(tspSerie);

        tspColuna.setExpanded(false);
        tspColuna.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grafico.gif"))); // NOI18N
        tspColuna.setScrollOnExpand(true);
        tspColuna.setTitle("Colunas");

        crlSelectLine.setBackground(new java.awt.Color(255, 255, 255));
        crlSelectLine.setBorder(null);
        crlSelectLine.setOpaque(false);
        crlSelectLine.setPreferredSize(new java.awt.Dimension(200, 200));

        pnlSelectLine.setBackground(new java.awt.Color(255, 255, 255));
        pnlSelectLine.setLayout(new javax.swing.BoxLayout(pnlSelectLine, javax.swing.BoxLayout.Y_AXIS));
        crlSelectLine.setViewportView(pnlSelectLine);

        tspColuna.getContentPane().add(crlSelectLine);

        tskGrafico.add(tspColuna);

        pnlOpcao.add(tskGrafico, java.awt.BorderLayout.CENTER);

        splitGrafico.setLeftComponent(pnlOpcao);

        tpAnalise.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        tpAnalise.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);
        tpAnalise.setPreferredSize(new java.awt.Dimension(400, 230));
        tpAnalise.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tpAnaliseStateChanged(evt);
            }
        });

        scrGraph.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        pnlGraph.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlGraph.setMaximumSize(new java.awt.Dimension(0, 0));
        pnlGraph.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlGraph.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlGraph.add(oChartViewer);

        scrGraph.setViewportView(pnlGraph);

        tpAnalise.addTab(bundle.getString("KpiIndicador_00074"), new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grafico.gif")), scrGraph); // NOI18N
        tpAnalise.addTab(bundle.getString("KpiIndicador_00033"), new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planilha.gif")), tbPlanilha); // NOI18N

        splitGrafico.setRightComponent(tpAnalise);

        getContentPane().add(splitGrafico, java.awt.BorderLayout.CENTER);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void chkLegendaValoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkLegendaValoresActionPerformed
            graphController.setLegValVisible(chkLegendaValores.isSelected());
            graphController.updateGraph();
	}//GEN-LAST:event_chkLegendaValoresActionPerformed

	private void cbbZoomItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbZoomItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                setZoom();
            }
	}//GEN-LAST:event_cbbZoomItemStateChanged

	private void chkLegendaYItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkLegendaYItemStateChanged
            graphController.setLegYvisible(chkLegendaY.isSelected());
            graphController.updateGraph();
	}//GEN-LAST:event_chkLegendaYItemStateChanged

	private void chkLegendaXItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkLegendaXItemStateChanged

            graphController.setLegXvisible(chkLegendaX.isSelected());
            graphController.updateGraph();
	}//GEN-LAST:event_chkLegendaXItemStateChanged

	private void chkPreviaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkPreviaItemStateChanged
            if (previaEnabled) {
                selectGraphColumn(PREVIA, chkPrevia.isSelected(), false);
                updateGraph();
            }
	}//GEN-LAST:event_chkPreviaItemStateChanged

	private void chkDiferencaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkDiferencaItemStateChanged
            selectGraphColumn(DIFERENCA, chkDiferenca.isSelected(), false);
            updateGraph();
	}//GEN-LAST:event_chkDiferencaItemStateChanged

	private void chkMetaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkMetaItemStateChanged
            selectGraphColumn(META, chkMeta.isSelected(), false);
            updateGraph();
	}//GEN-LAST:event_chkMetaItemStateChanged

	private void chkValorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkValorItemStateChanged
            selectGraphColumn(VALOR, chkValor.isSelected(), false);
            updateGraph();
	}//GEN-LAST:event_chkValorItemStateChanged

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            btnSave.setEnabled(false);
            event.editRecord();
            event.saveRecord();
            btnSave.setEnabled(true);
	}//GEN-LAST:event_btnSaveActionPerformed

	private void cmbTipoGraficoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbTipoGraficoItemStateChanged
            if (evt.getStateChange() == ItemEvent.DESELECTED) {
                boolean isTransaction = graphController.isTrasanction();

                graphController.setInTrasanction(true);
                addTypeSerieToComboBox(cmbTipoGrafico.getSelectedIndex());

                //Se estiver carregando a 1 vez nao retirar o controle da transacao. evita erro de heap.
                if (!isTransaction) {
                    graphController.setInTrasanction(false);
                    updateGraph();
                }
            }
	}//GEN-LAST:event_cmbTipoGraficoItemStateChanged

	private void cmbValorTipoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbValorTipoItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                changeSerieType(cmbValorTipo, VALOR);
            }
	}//GEN-LAST:event_cmbValorTipoItemStateChanged

	private void cmbMetaTipoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbMetaTipoItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                changeSerieType(cmbMetaTipo, META);
            }
	}//GEN-LAST:event_cmbMetaTipoItemStateChanged

	private void cmbPreviaTipoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbPreviaTipoItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                if (previaEnabled) {
                    changeSerieType(cmbPreviaTipo, PREVIA);
                }
            }
	}//GEN-LAST:event_cmbPreviaTipoItemStateChanged

	private void cmbDiferencaTipoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbDiferencaTipoItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                changeSerieType(cmbDiferencaTipo, DIFERENCA);
            }
	}//GEN-LAST:event_cmbDiferencaTipoItemStateChanged

	private void chkMesExtensoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkMesExtensoActionPerformed
            loadGraphRecord("false");
            graphController.setLegendas(getLabels());
            graphController.updateGraph();
            pnlSelectLine.setOpaque(false);
            pnlSelectLine.repaint();
            pnlSelectLine.validate();
	}//GEN-LAST:event_chkMesExtensoActionPerformed

	private void jPVCorPreviaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPVCorPreviaActionPerformed
            setGraphColor(PREVIA, jPVCorPrevia.getColor(), true);
	}//GEN-LAST:event_jPVCorPreviaActionPerformed

	private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);

            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("graphs\\*.jpg");

            frmChooser.setVisible(true);

            frmChooser.setUrlPath("/graphs/");
            frmChooser.openFile();
	}//GEN-LAST:event_btnAbrirActionPerformed

	private void btnExportacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportacaoActionPerformed
            try {
                KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
                frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
                frmChooser.setFileType("*.jpg");
                frmChooser.loadServerFiles("graphs\\*.jpg");
                frmChooser.setFileName("grafico.jpg");
                frmChooser.setVisible(true);

                if (frmChooser.isValidFile()) {
                    kpi.util.KpiToolKit tool = new kpi.util.KpiToolKit();
                    java.awt.image.BufferedImage myImage;
                    int yPos = 0;
                    int pictSize = pnlGraph.getHeight() + tbPlanilha.getTable().getHeight() + tbPlanilha.getTable().getTableHeader().getHeight() + 4;

                    Dimension size = new Dimension(pnlGraph.getWidth(), pictSize);
                    myImage = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);

                    Graphics2D g2 = myImage.createGraphics();
                    Color color = new Color(255, 255, 255);

                    g2.setBackground(color);
                    g2.fillRect(0, 0, size.width, size.height);

                    //Desenhando o grafico
                    pnlGraph.doLayout();
                    pnlGraph.paint(g2);

                    //Desenhando a tabela
                    tbPlanilha.doLayout();
                    yPos = pnlGraph.getHeight();
                    g2.translate(0, yPos + 1);

                    tbPlanilha.getTable().getTableHeader().paint(g2);
                    yPos = tbPlanilha.getTable().getTableHeader().getSize().height;
                    g2.translate(0, yPos + 2);
                    tbPlanilha.getJTable().paint(g2);

                    //Gravando a imagem.
                    tool.writeJPEGFile(myImage, "graphs//" + frmChooser.getFileName());
                }
            } catch (Exception e) {
                System.out.println(e);
            }
	}//GEN-LAST:event_btnExportacaoActionPerformed

	private void jPVCorDiferencaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPVCorDiferencaActionPerformed
            setGraphColor(DIFERENCA, jPVCorDiferenca.getColor(), true);
	}//GEN-LAST:event_jPVCorDiferencaActionPerformed

	private void jPVCorFundoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPVCorFundoActionPerformed
            pnlGraph.setBackground(jPVCorFundo.getColor());
            graphController.setBackGroundColor(jPVCorFundo.getColor(), false);
            updateGraph();
	}//GEN-LAST:event_jPVCorFundoActionPerformed

	private void splitGraficoComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_splitGraficoComponentResized
            splitGrafico.setDividerLocation(0);
            splitGrafico.setDividerSize(0);
	}//GEN-LAST:event_splitGraficoComponentResized

	private void jPVCorMetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPVCorMetaActionPerformed
            setGraphColor(META, jPVCorMeta.getColor(), true);
	}//GEN-LAST:event_jPVCorMetaActionPerformed

	private void cmbLegendaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbLegendaItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                graphController.setLegendPos(cmbLegenda.getSelectedIndex(), true);
            }
	}//GEN-LAST:event_cmbLegendaItemStateChanged

	private void jPVCorValorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPVCorValorActionPerformed
            setGraphColor(VALOR, jPVCorValor.getColor(), true);
	}//GEN-LAST:event_jPVCorValorActionPerformed

	private void btnConfigGraphActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfigGraphActionPerformed
            splitGrafico.setDividerLocation(320);
            splitGrafico.setDividerSize(0);
            btnFechar.setVisible(true);
            btnConfigGraph.setVisible(false);
	}//GEN-LAST:event_btnConfigGraphActionPerformed

	private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
            splitGrafico.setDividerLocation(0);
            splitGrafico.setDividerSize(0);
            btnFechar.setVisible(false);
            btnConfigGraph.setVisible(true);
	}//GEN-LAST:event_btnFecharActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            refreshFields();
	}//GEN-LAST:event_btnReloadActionPerformed

    private void chkLinhaColunaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkLinhaColunaItemStateChanged
        graphController.setSwapXY(chkLinhaColuna.isSelected(), true);
    }//GEN-LAST:event_chkLinhaColunaItemStateChanged

    private void chkValorAcumItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkValorAcumItemStateChanged
        selectGraphColumn(VALOR_ACUM, chkValorAcum.isSelected(), false);
        updateGraph();
}//GEN-LAST:event_chkValorAcumItemStateChanged

    private void jPVCorValorAcumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPVCorValorAcumActionPerformed
        setGraphColor(VALOR_ACUM, jPVCorValorAcum.getColor(), true);
}//GEN-LAST:event_jPVCorValorAcumActionPerformed

    private void cmbValorTipoAcumItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbValorTipoAcumItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            changeSerieType(cmbValorTipoAcum, VALOR_ACUM);
        }
}//GEN-LAST:event_cmbValorTipoAcumItemStateChanged

    private void chkMetaAcumItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkMetaAcumItemStateChanged
        selectGraphColumn(META_ACUM, chkMetaAcum.isSelected(), false);
        updateGraph();
}//GEN-LAST:event_chkMetaAcumItemStateChanged

    private void jPVCorMetaAcumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPVCorMetaAcumActionPerformed
        setGraphColor(META_ACUM, jPVCorMetaAcum.getColor(), true);
}//GEN-LAST:event_jPVCorMetaAcumActionPerformed

    private void cmbMetaTipoAcumItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbMetaTipoAcumItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            changeSerieType(cmbMetaTipoAcum, META_ACUM);
        }
}//GEN-LAST:event_cmbMetaTipoAcumItemStateChanged

    private void chkPreviaAcumItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkPreviaAcumItemStateChanged
        if (previaEnabled) {
            selectGraphColumn(PREVIA_ACUM, chkPreviaAcum.isSelected(), false);
            updateGraph();
        }
}//GEN-LAST:event_chkPreviaAcumItemStateChanged

    private void jPVCorPreviaAcumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPVCorPreviaAcumActionPerformed
        setGraphColor(PREVIA_ACUM, jPVCorPreviaAcum.getColor(), true);
}//GEN-LAST:event_jPVCorPreviaAcumActionPerformed

    private void cmbPreviaTipoAcumItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbPreviaTipoAcumItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            if (previaEnabled) {
                changeSerieType(cmbPreviaTipoAcum, PREVIA_ACUM);
            }
        }
}//GEN-LAST:event_cmbPreviaTipoAcumItemStateChanged

    private void btnAnaliticoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnaliticoActionPerformed
        StringBuilder parametros = new StringBuilder();
        int linha = tbPlanilha.getSelectedRow();

        if (linha > -1) {
            BIXMLRecord recLinha = tbPlanilha.getXMLData().get(linha);

            if (recLinha.contains("INDICADOR")) {
                parametros.append("analitico.apw");
                parametros.append("?indicador=");
                parametros.append(recLinha.getString("INDICADOR"));
                parametros.append("&alvo=");
                parametros.append(recLinha.getString("ALVO"));
                parametros.append("&pagina=");
                parametros.append("1");

                KpiToolKit toolkit = new KpiToolKit();
                toolkit.browser(parametros.toString());
            }
        }
    }//GEN-LAST:event_btnAnaliticoActionPerformed

    private void tpAnaliseStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tpAnaliseStateChanged
        btnAnalitico.setEnabled(tpAnalise.getSelectedIndex() == 1);
    }//GEN-LAST:event_tpAnaliseStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnAnalitico;
    private javax.swing.JButton btnConfigGraph;
    private javax.swing.JButton btnExportacao;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbbZoom;
    private javax.swing.JCheckBox chkDiferenca;
    private javax.swing.JCheckBox chkLegendaValores;
    private javax.swing.JCheckBox chkLegendaX;
    private javax.swing.JCheckBox chkLegendaY;
    private javax.swing.JCheckBox chkLinhaColuna;
    private javax.swing.JCheckBox chkMesExtenso;
    private javax.swing.JCheckBox chkMeta;
    private javax.swing.JCheckBox chkMetaAcum;
    private javax.swing.JCheckBox chkPrevia;
    private javax.swing.JCheckBox chkPreviaAcum;
    private javax.swing.JCheckBox chkValor;
    private javax.swing.JCheckBox chkValorAcum;
    private javax.swing.JComboBox cmbDiferencaTipo;
    private javax.swing.JComboBox cmbLegenda;
    private javax.swing.JComboBox cmbMetaTipo;
    private javax.swing.JComboBox cmbMetaTipoAcum;
    private javax.swing.JComboBox cmbPreviaTipo;
    private javax.swing.JComboBox cmbPreviaTipoAcum;
    private javax.swing.JComboBox cmbTipoGrafico;
    private javax.swing.JComboBox cmbValorTipo;
    private javax.swing.JComboBox cmbValorTipoAcum;
    private javax.swing.JScrollPane crlSelectLine;
    private kpi.beans.JBIDatePane datePane;
    private pv.jfcx.JPVColor jPVCorDiferenca;
    private pv.jfcx.JPVColor jPVCorFundo;
    private pv.jfcx.JPVColor jPVCorMeta;
    private pv.jfcx.JPVColor jPVCorMetaAcum;
    private pv.jfcx.JPVColor jPVCorPrevia;
    private pv.jfcx.JPVColor jPVCorPreviaAcum;
    private pv.jfcx.JPVColor jPVCorValor;
    private pv.jfcx.JPVColor jPVCorValorAcum;
    private javax.swing.JLabel lblCorSerie;
    private javax.swing.JLabel lblCorSerie1;
    private javax.swing.JLabel lblSerieTipo;
    private javax.swing.JLabel lblSerieValor;
    private javax.swing.JLabel lblSerieValor1;
    private javax.swing.JLabel lblZoom;
    private ChartDirector.ChartViewer oChartViewer;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlGraficao;
    private javax.swing.JPanel pnlGraph;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlOpcao;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlSelectLine;
    private javax.swing.JPanel pnlSerie;
    private javax.swing.JScrollPane scrGraph;
    private javax.swing.JSplitPane splitGrafico;
    private javax.swing.JSeparator sprConfiguracao;
    private javax.swing.JToolBar.Separator sprOperacao1;
    private javax.swing.JToolBar.Separator sprOperacao2;
    private javax.swing.JToolBar.Separator sprOperacao3;
    private javax.swing.JToolBar.Separator sprPeriodo;
    private javax.swing.JToolBar.Separator sprPeriodo1;
    private kpi.beans.JBIXMLTable tbPlanilha;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JTabbedPane tpAnalise;
    private com.l2fprod.common.swing.JTaskPane tskGrafico;
    private com.l2fprod.common.swing.JTaskPaneGroup tspColuna;
    private com.l2fprod.common.swing.JTaskPaneGroup tspGrafico;
    private com.l2fprod.common.swing.JTaskPaneGroup tspSerie;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiGraphIndicador(int operation, String idAux, String contextId) {
        ChartDirector.Chart.setLicenseCode("RDST-245J-39ZJ-A6R8-ED7B-1DCB");
        putClientProperty("MAXI", true);
        initComponents();

        event.defaultConstructor(operation, idAux, contextId);
        createChart();
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        record = new kpi.xml.BIXMLRecord(parentType);
    }

    /*
     *Evento load record personalizado para carregar os dados do indicador.
     */
    public void loadGraphRecord(String indID, String dataDe, String dataAte) {
        graphIndID = indID;
        requestGraphRecord(indID, "true");
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    private void createChart() {
        graphController = new kpi.beans.JBIChartDirectorController(oChartViewer);

        Object[] headers = new Object[]{""};
        Object[][] cells = new Object[][]{{new Double(0)}};

        //
        graphController.setTitle(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00025"), false);

        //Cria��o da tabela que conter� os valores do gr�fico.
        javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(cells, headers);
        oTable = new JTable(model);
    }

    private void loadGraphRecord(String loadDefault) {
        requestGraphRecord(graphIndID, loadDefault);
    }

    /*
     *Requisitando os dados ao Protheus.
     * @param indID			= Id do Indicador
     * @param loadDefault	= Define se a op��o que est�o gravadas ser�o carregadas na requisi��o.
     */
    private void requestGraphRecord(String indID, String loadDefault) {
        graphController.setInTrasanction(true); //N�o atualizar os objetos do grafico at� terminar de carregar as propriedades salvas.
        
        javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel) oTable.getModel();
        Vector vctCabecalho = new Vector();
        vctValores.clear();

        //Preparando o comando de requisicao
        StringBuilder cRequest = new StringBuilder(loadDefault);
        cRequest.append("|");
        cRequest.append(datePane.getDataDe());
        cRequest.append("|");
        cRequest.append(datePane.getDataAte());
        cRequest.append("|");
        cRequest.append(chkMesExtenso.isSelected());
        BIXMLRecord recDadosGraph = event.loadRecord(indID, recordType, cRequest.toString());
        recDadosInd = event.loadRecord(indID, "INDICADOR", "");

        //Salva o id do indicador para envio ao ADVPL.
        record.set("INDICADOR", recDadosGraph.getString("INDICADOR"));

        //Vetor com os dados da planilha.
        planilhaDados = recDadosGraph.getBIXMLVector("COLUNAS");

        //Preparando as linhas dos dados.
        pnlSelectLine.removeAll();
        for (int coluna = 0; coluna < planilhaDados.size(); coluna++) {
            BIXMLRecord recColuna = planilhaDados.get(coluna);
            vctValores.addElement(criaColuna(recColuna));
        }

        //Vetor com os titulos do cabe�alho.
        vctCabecalho.add("Labels");
        vctCabecalho.add(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00026"));
        vctCabecalho.add(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00027"));
        vctCabecalho.add(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00028"));
        vctCabecalho.add(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00068")); //"Valor Acumulado"
        vctCabecalho.add(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00069")); //"Meta Acumulada");
        vctCabecalho.add(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00070")); //Pr�via Acumulada");

        //Verifica a necessidade de mostrar a previa.
        setPrevia(vctCabecalho, (recDadosGraph.contains("VALOR_PREVIO") && recDadosGraph.getBoolean("VALOR_PREVIO")));

        //Atualiza os dados recebidos
        model.setDataVector(vctValores, vctCabecalho);
        model.fireTableChanged(new TableModelEvent(model));

        vctColors.clear();
        for (int item = 0; item < planilhaDados.size(); item++) {
            //Adiciona os listeners do checkbox
            JCheckBox oTmpCheck = (JCheckBox) pnlSelectLine.getComponent(item);
            oTmpCheck.putClientProperty("LINHA", new Integer(item));
            oTmpCheck.setOpaque(false);
            oTmpCheck.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    selectGraphRow(evt);
                }
            });

            //Capturando cores da coluna
            BIXMLRecord recColuna = planilhaDados.get(item);
            int nStatus = recColuna.getInt("STATUS");
            switch (nStatus) {
                case KpiScoreCarding.STATUS_RED:
                    vctColors.add(new java.awt.Color(234, 140, 136));
                    //vctColors[item] = new java.awt.Color(234,140,136);
                    break;
                case KpiScoreCarding.STATUS_YELLOW:
                    vctColors.add(new java.awt.Color(255, 235, 155));
                    //vctColors[item] = new java.awt.Color(255,235,155);
                    break;
                case KpiScoreCarding.STATUS_GREEN:
                    vctColors.add(new java.awt.Color(139, 191, 150));
                    //vctColors[item] = new java.awt.Color(139,191,150);
                    break;
                case KpiScoreCarding.STATUS_BLUE:
                    vctColors.add(new java.awt.Color(109,178,247));
                    break;
            }
        }

        if (loadDefault.equals("true")) {
            putChartSeries(recDadosGraph);
            setUserInterface(recDadosGraph);
        }

        setChartController();
        
        graphController.setTitle(recDadosGraph.getString("IND_NOME"), false);
        graphController.setOrientation(recDadosGraph.getBoolean("ASCEND"), false, recDadosGraph.getBoolean("MELHORF"));
        graphController.setUnitMeasure(recDadosGraph.getString("UNMEDIDA"), false);
        graphController.setInTrasanction(false);
        
        // Mensagem gerada para que o foco na tela seja restaurado para que o grafico possa ser exibido. 
        // A mensagem fecha sozinha e fica em exibicao durante 1 milesimo, sendo imperceptivel ao usuario.
        final JLabel messageLabel = new JLabel("Processando...");
        Timer timer = new Timer(1, 
            new ActionListener(){   
                @Override
                public void actionPerformed(ActionEvent event){
                    SwingUtilities.getWindowAncestor(messageLabel).dispose();
                }
            });
        timer.setRepeats(false);
        timer.start();
        JOptionPane.showMessageDialog(null, messageLabel, "SGI", JOptionPane.INFORMATION_MESSAGE);        
        
        updateGraph();
        
        //Atualiza da planilha de dados.
        updatePlaDados();
                
    }

    private void setChartController() {
        graphController.setLegendas(getLabels());
        graphController.setXLegenda(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00055"), false);
        graphController.setYLegenda(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00056"), false);

        graphController.getSerieItem(DIFERENCA).setColor(jPVCorDiferenca.getColor());
        graphController.getSerieItem(DIFERENCA).setVisible(chkDiferenca.isSelected());
        graphController.getSerieItem(VALOR).setColor(jPVCorValor.getColor());
        graphController.getSerieItem(VALOR).setVisible(chkValor.isSelected());
        graphController.getSerieItem(META).setColor(jPVCorMeta.getColor());
        graphController.getSerieItem(META).setVisible(chkMeta.isSelected());
        graphController.getSerieItem(VALOR_ACUM).setColor(jPVCorValorAcum.getColor());
        graphController.getSerieItem(VALOR_ACUM).setVisible(chkValorAcum.isSelected());
        graphController.getSerieItem(META_ACUM).setColor(jPVCorMetaAcum.getColor());
        graphController.getSerieItem(META_ACUM).setVisible(chkMetaAcum.isSelected());

        if (previaEnabled) {
            graphController.getSerieItem(PREVIA).setColor(jPVCorPrevia.getColor());
            graphController.getSerieItem(PREVIA).setVisible(chkPrevia.isSelected());

            graphController.getSerieItem(PREVIA_ACUM).setColor(jPVCorPreviaAcum.getColor());
            graphController.getSerieItem(PREVIA_ACUM).setVisible(chkPreviaAcum.isSelected());
        }

        graphController.setLegXvisible(chkLegendaX.isSelected());
        graphController.setLegYvisible(chkLegendaY.isSelected());

        graphController.setSwapXY(chkLinhaColuna.isSelected(), true);
        graphController.setLegValVisible(chkLegendaValores.isSelected());

        graphController.updateGraph();
    }

    private void putChartSeries(kpi.xml.BIXMLRecord recGraph) {
        graphController.removeAllSeries();
        graphController.addSerie(VALOR, getSeries(VALOR));
        graphController.getSerieItem(VALOR).setSerieName(KpiStaticReferences.getKpiCustomLabels().getReal());
        graphController.getSerieItem(VALOR).setSymbol(Chart.SquareSymbol);

        graphController.addSerie(VALOR_ACUM, getSeries(VALOR_ACUM));
        graphController.getSerieItem(VALOR_ACUM).setSerieName(KpiStaticReferences.getKpiCustomLabels().getReal().concat(" Acumulado"));
        graphController.getSerieItem(VALOR_ACUM).setSymbol(Chart.SquareSymbol);

        graphController.addSerie(META, getSeries(META));
        graphController.getSerieItem(META).setSerieName(KpiStaticReferences.getKpiCustomLabels().getMeta());
        graphController.getSerieItem(META).setSymbol(Chart.DiamondShape);

        graphController.addSerie(META_ACUM, getSeries(META_ACUM));
        graphController.getSerieItem(META_ACUM).setSerieName(KpiStaticReferences.getKpiCustomLabels().getMeta().concat(" Acumulada"));
        graphController.getSerieItem(META_ACUM).setSymbol(Chart.DiamondShape);

        graphController.addSerie(DIFERENCA, getSeries(DIFERENCA));
        graphController.getSerieItem(DIFERENCA).setSymbol(Chart.TriangleShape);
        graphController.getSerieItem(DIFERENCA).setSerieName(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00047"));

        if (previaEnabled) {
            graphController.addSerie(PREVIA, getSeries(PREVIA));
            graphController.getSerieItem(PREVIA).setSerieName(KpiStaticReferences.getKpiCustomLabels().getPrevia());
            graphController.getSerieItem(PREVIA).setSymbol(Chart.CircleShape);

            graphController.addSerie(PREVIA_ACUM, getSeries(PREVIA_ACUM));
            graphController.getSerieItem(PREVIA_ACUM).setSerieName(KpiStaticReferences.getKpiCustomLabels().getPrevia().concat(" Acumulada"));
            graphController.getSerieItem(PREVIA_ACUM).setSymbol(Chart.CircleShape);
        }
    }

    private void setPrevia(Vector cabec, boolean enable) {
        previaEnabled = enable;
        chkPrevia.setVisible(enable);
        chkPrevia.setVisible(enable);
        jPVCorPrevia.setVisible(enable);
        cmbPreviaTipo.setVisible(enable);

        chkPreviaAcum.setVisible(enable);
        chkPreviaAcum.setVisible(enable);
        jPVCorPreviaAcum.setVisible(enable);
        cmbPreviaTipoAcum.setVisible(enable);

        if (enable) {
            cabec.add(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00035"));
        }
    }

    /*
     *Retorna a s�rie solicitada.
     */
    private double[] getSeries(int serie) {
        double[] data = new double[vctValores.size()];

        for (int iItem = 0; iItem < vctValores.size(); iItem++) {
            Vector vctItens = (Vector) vctValores.get(iItem);
            data[iItem] = Double.valueOf((Double) vctItens.get(serie));
        }

        return data;
    }

    /*
     *Retorna as labels para o gr�fico
     */
    private String[] getLabels() {
        String[] labels = new String[vctValores.size()];

        for (int iItem = 0; iItem < vctValores.size(); iItem++) {
            Vector vctItens = (Vector) vctValores.get(iItem);
            labels[iItem] = (String) vctItens.get(LABEL);
        }

        return labels;
    }

    private Vector criaColuna(BIXMLRecord recColuna) {
        Vector valorColuna = new Vector();
        JCheckBox chkBox = new JCheckBox(recColuna.getString("LABELX"));
        chkBox.setOpaque(false);

        pnlSelectLine.add(chkBox, true);

        valorColuna.add(recColuna.getString("LABELX"));     //PERIODO
        valorColuna.add(recColuna.getDouble("VALOR"));      //VALOR = 1;
        valorColuna.add(recColuna.getDouble("META"));       //META = 2;
        valorColuna.add(recColuna.getDouble("DIFERENCA"));  //DIFERENCA = 3;
        valorColuna.add(recColuna.getDouble("VALOR_ACUM")); //VALOR_ACUM = 4;
        valorColuna.add(recColuna.getDouble("META_ACUM"));  //META_ACUM = 5;
        valorColuna.add(recColuna.getDouble("PREVIA"));     //PREVIA = 6;
        valorColuna.add(recColuna.getDouble("PREVIA_ACUM"));//PREVIA_ACUM = 7;

        return valorColuna;
    }

    private void selectGraphColumn(int serie, boolean visible, boolean refresh) {
        if (graphController != null) {
            graphController.getSerieItem(serie).setVisible(visible);
            if (refresh) {
                updateGraph();
            }
        }
    }

    private void selectGraphRow(java.awt.event.ActionEvent evt) {
        javax.swing.JCheckBox oTmpCheck = (javax.swing.JCheckBox) evt.getSource();
        removeRow(oTmpCheck);
    }

    private void removeRow(javax.swing.JCheckBox oTmpCheck) {
        Integer linha = (Integer) oTmpCheck.getClientProperty("LINHA");
        if (linha != null) {
            int line = linha.intValue();
            if (oTmpCheck.isSelected()) {
                graphController.removeRow(line, false);
            } else {
                graphController.removeRow(line, true);
            }
        }

        if (cmbTipoGrafico.getSelectedIndex() == kpi.beans.JBIChartDirectorController.PIE_CHART) {
            updateGraph();
        } else if (cmbTipoGrafico.getSelectedIndex() == kpi.beans.JBIChartDirectorController.XY_CHART) {
            setGraphSize();
            graphController.updateGraph();
        }
    }

    private void setGraphColor(int serie, java.awt.Color color, boolean refresh) {
        JBIChartDirectorSerie chartSerie = graphController.getSerieItem(serie);
        if (graphController != null && chartSerie != null) {
            chartSerie.setColor(color);
            if (refresh) {
                updateGraph();
            }
        }
    }

    private void updatePlaDados() {
        tbPlanilha.setDataSource(planilhaDados);
        setRenderPlaDados();
    }

    /*
     *Seta o Render para a tabela.
     */
    private void setRenderPlaDados() {
        String tipoColuna = null;
        cellRenderer = new MyPlaDadosCellRenderer(tbPlanilha);

        /*
         * Configurando as propriedades das colunas, e renderer.
         */
        for (int col = 0; col < tbPlanilha.getTable().getColumnCount(); col++) {
            tipoColuna = tbPlanilha.getColumnType(col);
            int colTipo = new Integer(tipoColuna).intValue();

            if (colTipo == JBIXMLTable.KPI_FLOAT || colTipo == JBIXMLTable.KPI_STRING) {
                tbPlanilha.getColumn(col).setCellRenderer(cellRenderer);
            }
        }
    }

    private void addTypeSerieToComboBox(int option) {
        javax.swing.JComboBox[] serieCombos = {cmbMetaTipo, cmbValorTipo, cmbPreviaTipo, cmbDiferencaTipo, cmbMetaTipoAcum, cmbValorTipoAcum, cmbPreviaTipoAcum};

        switch (option) {
            case kpi.beans.JBIChartDirectorController.XY_CHART:
                for (int n = 0; n < serieCombos.length; n++) {
                    javax.swing.JComboBox tmpCombo = (javax.swing.JComboBox) serieCombos[n];
                    tmpCombo.removeAllItems();
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00003"));
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00004"));
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00001"));
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00002"));
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00053"));
                }
                cmbLegenda.setEnabled(true);
                chkLinhaColuna.setEnabled(true);
                chkLegendaX.setEnabled(true);
                chkLegendaY.setEnabled(true);
                chkLegendaValores.setEnabled(true);
                chkMesExtenso.setEnabled(true);
                //lblOpcSerie.setEnabled(true);
                jPVCorDiferenca.setEnabled(true);
                jPVCorMeta.setEnabled(true);
                jPVCorValor.setEnabled(true);
                jPVCorMetaAcum.setEnabled(true);
                jPVCorValorAcum.setEnabled(true);
                lblCorSerie.setEnabled(true);

                //lblTituloConfiguracao2.setEnabled(true);
                lblCorSerie.setEnabled(true);
                lblSerieTipo.setEnabled(true);
                lblSerieValor.setEnabled(true);

                chkValor.setEnabled(true);
                chkMeta.setEnabled(true);
                chkValorAcum.setEnabled(true);
                chkMetaAcum.setEnabled(true);
                chkDiferenca.setEnabled(true);

                cmbValorTipo.setEnabled(true);
                cmbMetaTipo.setEnabled(true);
                cmbValorTipoAcum.setEnabled(true);
                cmbMetaTipoAcum.setEnabled(true);
                cmbDiferencaTipo.setEnabled(true);
                setEnableFields(pnlSelectLine, true);

                if (previaEnabled) {
                    jPVCorPrevia.setEnabled(true);
                    chkPrevia.setEnabled(true);
                    cmbPreviaTipo.setEnabled(true);
                    jPVCorPreviaAcum.setEnabled(true);
                    chkPreviaAcum.setEnabled(true);
                    cmbPreviaTipoAcum.setEnabled(true);
                } else {
                    jPVCorPrevia.setEnabled(false);
                    jPVCorPreviaAcum.setEnabled(false);
                }

                break;
            case kpi.beans.JBIChartDirectorController.PIE_CHART:
                for (int n = 0; n < serieCombos.length; n++) {
                    javax.swing.JComboBox tmpCombo = (javax.swing.JComboBox) serieCombos[n];
                    tmpCombo.removeAllItems();
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00005"));
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00006"));
                }
                cmbLegenda.setEnabled(false);
                chkLinhaColuna.setEnabled(false);
                chkLegendaX.setEnabled(false);
                chkLegendaY.setEnabled(false);
                chkLegendaValores.setEnabled(false);
                chkMesExtenso.setEnabled(true);
                //lblOpcSerie.setEnabled(true);
                jPVCorDiferenca.setEnabled(false);
                jPVCorMeta.setEnabled(false);
                jPVCorPrevia.setEnabled(false);
                jPVCorValor.setEnabled(false);
                jPVCorMetaAcum.setEnabled(false);
                jPVCorPreviaAcum.setEnabled(false);
                jPVCorValorAcum.setEnabled(false);
                lblCorSerie.setEnabled(false);

                //lblTituloConfiguracao2.setEnabled(true);
                lblCorSerie.setEnabled(false);
                lblSerieTipo.setEnabled(true);
                lblSerieValor.setEnabled(true);

                chkValor.setEnabled(true);
                chkMeta.setEnabled(true);
                chkValorAcum.setEnabled(true);
                chkMetaAcum.setEnabled(true);
                chkDiferenca.setEnabled(true);

                cmbValorTipo.setEnabled(true);
                cmbMetaTipo.setEnabled(true);
                cmbValorTipoAcum.setEnabled(true);
                cmbMetaTipoAcum.setEnabled(true);
                cmbDiferencaTipo.setEnabled(true);
                setEnableFields(pnlSelectLine, true);

                if (previaEnabled) {
                    jPVCorPrevia.setEnabled(false);
                    chkPrevia.setEnabled(false);
                    cmbPreviaTipo.setEnabled(false);
                    jPVCorPreviaAcum.setEnabled(false);
                    chkPreviaAcum.setEnabled(false);
                    cmbPreviaTipoAcum.setEnabled(false);
                } else {
                    jPVCorPrevia.setEnabled(false);
                    jPVCorPreviaAcum.setEnabled(false);
                }
                break;
            case kpi.beans.JBIChartDirectorController.STATUS_CHART:
                for (int n = 0; n < serieCombos.length; n++) {
                    javax.swing.JComboBox tmpCombo = (javax.swing.JComboBox) serieCombos[n];
                    tmpCombo.removeAllItems();
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00003"));
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00004"));
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00001"));
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00002"));
                    tmpCombo.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00053"));
                    tmpCombo.addItem("Barra Default");
                    tmpCombo.addItem("Linha Default");
                }
                cmbLegenda.setEnabled(true);
                chkLinhaColuna.setEnabled(true);
                chkLegendaX.setEnabled(true);
                chkLegendaY.setEnabled(true);
                chkLegendaValores.setEnabled(true);
                chkMesExtenso.setEnabled(false);
                //lblOpcSerie.setEnabled(false);
                jPVCorDiferenca.setEnabled(false);
                jPVCorMeta.setEnabled(false);
                jPVCorPrevia.setEnabled(false);
                jPVCorValor.setEnabled(false);
                jPVCorMetaAcum.setEnabled(false);
                jPVCorPreviaAcum.setEnabled(false);
                jPVCorValorAcum.setEnabled(false);
                lblCorSerie.setEnabled(false);

                //lblTituloConfiguracao2.setEnabled(false);
                lblCorSerie.setEnabled(false);
                lblSerieTipo.setEnabled(false);
                lblSerieValor.setEnabled(false);

                chkValor.setEnabled(false);
                chkMeta.setEnabled(false);
                chkValorAcum.setEnabled(false);
                chkMetaAcum.setEnabled(false);
                chkDiferenca.setEnabled(false);

                cmbValorTipo.setEnabled(false);
                cmbMetaTipo.setEnabled(false);
                cmbValorTipoAcum.setEnabled(false);
                cmbMetaTipoAcum.setEnabled(false);
                cmbDiferencaTipo.setEnabled(false);
                setEnableFields(pnlSelectLine, false);

                if (previaEnabled) {
                    jPVCorPrevia.setEnabled(false);
                    chkPrevia.setEnabled(false);
                    cmbPreviaTipo.setEnabled(false);
                    jPVCorPreviaAcum.setEnabled(false);
                    chkPreviaAcum.setEnabled(false);
                    cmbPreviaTipoAcum.setEnabled(false);
                } else {
                    jPVCorPrevia.setEnabled(false);
                    jPVCorPreviaAcum.setEnabled(false);
                }
                break;
        }

        //Setando o tipo do gr�fico.
        graphController.setGraphType(option);
    }

    private void setEnableFields(javax.swing.JComponent compToEnable, boolean enable) {
        for (int i = 0; i < compToEnable.getComponentCount(); i++) {
            compToEnable.getComponent(i).setEnabled(enable);
        }
    }

    private void changeSerieType(javax.swing.JComboBox cmbOpcao, int serie) {
        switch (cmbTipoGrafico.getSelectedIndex()) {
            case kpi.beans.JBIChartDirectorController.XY_CHART:
                graphController.setSerieType(serie, cmbOpcao.getSelectedIndex());
                break;
            case kpi.beans.JBIChartDirectorController.STATUS_CHART:
                graphController.setSerieType(serie, cmbOpcao.getSelectedIndex());
                break;
            case kpi.beans.JBIChartDirectorController.PIE_CHART:
                if (cmbOpcao.getSelectedIndex() == 0) {
                    graphController.setSerieType(serie, JBIChartDirectorController.PIE);
                } else if (cmbOpcao.getSelectedIndex() == 1) {
                    graphController.setSerieType(serie, JBIChartDirectorController.PIE_3D);
                }
        }

        updateGraph();
    }

    private void updateGraph() {
        if (!graphController.isTrasanction()) {
            setGraphSize();//Ajusta a tela para suportar o n�mero de meses retornado pela fun��o.
            setZoom();
        }
    }

    private void doLayoutGraph() {
        ChartDirector.ChartViewer viewer = new ChartDirector.ChartViewer();
        pnlGraph.removeAll();

        if (cmbTipoGrafico.getSelectedIndex() == kpi.beans.JBIChartDirectorController.PIE_CHART) {// Se o grafico for do tipo pizza faz um pizza para cada s�rie.
            int serie = 1;
            for (int i = 0; i < graphController.getSerieSize(); ++i) {
                if (graphController.getSerieItem(serie).isVisible()) {
                    viewer = new ChartDirector.ChartViewer();
                    graphController.setChartViewer(viewer);
                    graphController.resetChart();
                    graphController.updateGraph(serie);
                    graphController.drawGraph();
                    pnlGraph.add(viewer);
                }
                serie++;
            }

        } else if (cmbTipoGrafico.getSelectedIndex() == kpi.beans.JBIChartDirectorController.XY_CHART) {
            graphController.setGraphType(kpi.beans.JBIChartDirectorController.XY_CHART);
            pnlGraph.add(viewer);
            graphController.setChartViewer(viewer);
            graphController.updateGraph();

        } else if (cmbTipoGrafico.getSelectedIndex() == kpi.beans.JBIChartDirectorController.STATUS_CHART) {
            graphController.setGraphType(kpi.beans.JBIChartDirectorController.STATUS_CHART);
            graphController.getSerieItem(DIFERENCA).setVisible(false);

            if (previaEnabled) {
                graphController.getSerieItem(PREVIA).setVisible(false);
            }

            graphController.getSerieItem(VALOR).setSerieType(5);
            graphController.getSerieItem(VALOR).setPersonalizateColors(vctColors);
            graphController.getSerieItem(META).setSerieType(0);

            chkDiferenca.setSelected(false);
            chkPrevia.setSelected(false);
            chkPreviaAcum.setSelected(false);

            setGraphColor(META, new Color(-16777069), false);

            JCheckBox oTmpCheck = (JCheckBox) pnlSelectLine.getComponent(planilhaDados.size() - 1);
            oTmpCheck.setSelected(false);
            oTmpCheck.setOpaque(false);
            removeRow(oTmpCheck);

            if (chkMesExtenso.isSelected()) {
                chkMesExtenso.setSelected(false);
                loadGraphRecord("false");
            }

            pnlGraph.add(viewer);
            graphController.setChartViewer(viewer);
            graphController.updateGraph();

        }

        pnlGraph.repaint();
        pnlGraph.validate();
    }

    /*
     *Ajusta a tela para suportar o n�mero de meses retornado pela fun��o.
     */
    private void setGraphSize() {
        int height = 490;

        if (cmbTipoGrafico.getSelectedIndex() == kpi.beans.JBIChartDirectorController.PIE_CHART) {
            int numSerieDis = graphController.getVisibleSerieSize();
            pnlGraph.setPreferredSize(new java.awt.Dimension(splitGrafico.getWidth(), numSerieDis * height));
            graphController.setSize(splitGrafico.getWidth(), height);//100 Margem
        } else if (cmbTipoGrafico.getSelectedIndex() == kpi.beans.JBIChartDirectorController.XY_CHART) {
            //Conta quantas barras est�o visiveis.
            int qtdVisivel = 0;
            for (int item = 0; item < planilhaDados.size(); item++) {
                JCheckBox oTmpCheck = (JCheckBox) pnlSelectLine.getComponent(item);
                oTmpCheck.setOpaque(false);
                if (oTmpCheck.isSelected()) {
                    qtdVisivel++;
                }
            }

            int width = 140 * qtdVisivel;

            if (width < splitGrafico.getWidth()) {
                width = splitGrafico.getWidth();
            }

            pnlGraph.setPreferredSize(new java.awt.Dimension(width, height));
            graphController.setSize(width, height);
        }
    }

    /**
     * Atualiza a tela do gr�fico com as op��es gravadas do usu�rio.
     **/
    private void setUserInterface(kpi.xml.BIXMLRecord recGraph) {
        cmbTipoGrafico.removeAllItems();
        cmbTipoGrafico.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00049"));
        cmbTipoGrafico.addItem(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00050"));
        cmbTipoGrafico.addItem("Status");

        cmbTipoGrafico.setSelectedIndex(recGraph.getInt("TIPO"));

        addTypeSerieToComboBox(cmbTipoGrafico.getSelectedIndex());

        cmbLegenda.setSelectedIndex(recGraph.getInt("POS_LEGENDA"));
        jPVCorFundo.setColor(new java.awt.Color(recGraph.getInt("COR_FUNDO")));
        pnlGraph.setBackground(jPVCorFundo.getColor());
        chkLinhaColuna.setSelected(recGraph.getBoolean("LINHA_COLUNA"));
        chkLegendaX.setSelected(recGraph.getBoolean("LEGENDA_X"));
        chkLegendaY.setSelected(recGraph.getBoolean("LEGENDA_Y"));
        chkLegendaValores.setSelected(recGraph.getBoolean("LEGENDA_VAL"));
        chkMesExtenso.setSelected(recGraph.getBoolean("MES_EXTENSO"));
        chkValor.setSelected(recGraph.getBoolean("MOSTRA_VALOR"));
        chkValorAcum.setSelected(recGraph.getBoolean("MOSTRA_VALOR_ACUM"));
        chkMeta.setSelected(recGraph.getBoolean("MOSTRA_META"));
        chkMetaAcum.setSelected(recGraph.getBoolean("MOSTRA_META_ACUM"));

        chkDiferenca.setSelected(recGraph.getBoolean("MOSTRA_DIFERENCA"));

        jPVCorValor.setColor(new java.awt.Color(recGraph.getInt("COR_VALOR")));
        jPVCorValorAcum.setColor(new java.awt.Color(recGraph.getInt("COR_VALOR_ACUM")));
        jPVCorMeta.setColor(new java.awt.Color(recGraph.getInt("COR_META")));
        jPVCorMetaAcum.setColor(new java.awt.Color(recGraph.getInt("COR_META_ACUM")));
        jPVCorDiferenca.setColor(new java.awt.Color(recGraph.getInt("COR_DIFERENCA")));

        cmbValorTipo.setSelectedIndex(recGraph.getInt("TIPO_VALOR"));
        cmbValorTipoAcum.setSelectedIndex(recGraph.getInt("TIPO_VALOR_ACUM"));
        cmbMetaTipo.setSelectedIndex(recGraph.getInt("TIPO_META"));
        cmbMetaTipoAcum.setSelectedIndex(recGraph.getInt("TIPO_META_ACUM"));
        cmbDiferencaTipo.setSelectedIndex(recGraph.getInt("TIPO_DIFERENCA"));

        if (previaEnabled) {
            jPVCorPrevia.setColor(new java.awt.Color(recGraph.getInt("COR_PREVIA")));
            chkPrevia.setSelected(recGraph.getBoolean("MOSTRA_PREVIA"));
            chkPreviaAcum.setSelected(recGraph.getBoolean("MOSTRA_PREVIA"));
            cmbPreviaTipo.setSelectedIndex(recGraph.getInt("TIPO_PREVIA"));
            cmbPreviaTipoAcum.setSelectedIndex(recGraph.getInt("TIPO_PREVIA_ACUM"));
        }

        int item = planilhaDados.size() - 1;
        javax.swing.JCheckBox oTmpCheck = (javax.swing.JCheckBox) pnlSelectLine.getComponent(item);
        oTmpCheck.setSelected(recGraph.getBoolean("MOSTRA_ACUMULADO"));
        removeRow(oTmpCheck);
        cbbZoom.setSelectedItem(recGraph.getString("ZOOM")); //Valor do Zoom;
    }

    private void setZoom() {
        String zoom = (String) cbbZoom.getSelectedItem();
        int valZoom = 1;

        try {
            valZoom = Integer.parseInt(zoom);
        } catch (NumberFormatException ex) {
            dialog.errorMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00051"));
            cbbZoom.setSelectedItem("100");
            valZoom = 100;
        }

        if (valZoom > 200) {
            dialog.errorMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00052"));
            cbbZoom.setSelectedItem("200");
            valZoom = 200;
        }

        int iLargura = ((splitGrafico.getWidth() * valZoom) / 100) - 50;
        int iAltura = ((splitGrafico.getHeight() * valZoom) / 100) - 50;

        if (cmbTipoGrafico.getSelectedIndex() == kpi.beans.JBIChartDirectorController.PIE_CHART) {
            graphController.setSize(iLargura, iAltura);
            int numSerieDis = graphController.getVisibleSerieSize();
            iAltura *= numSerieDis;
        } else {
            graphController.setSize(iLargura, iAltura);
        }

        scrGraph.setViewportView(null);

        pnlGraph.setPreferredSize(new java.awt.Dimension(iLargura, iAltura));
        scrGraph.setViewportView(pnlGraph);

        doLayoutGraph();
    }
}

class MyPlaDadosCellRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private kpi.beans.JBIXMLTable biTable = null;

    public MyPlaDadosCellRenderer() {
    }

    public MyPlaDadosCellRenderer(kpi.beans.JBIXMLTable table) {
        biTable = table;
    }

    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
        BIXMLRecord recLinha = null;
        int linha = biTable.getOriginalRow(row);

        oRender.setLocale(kpi.core.KpiStaticReferences.getKpiDefaultLocale());
        oRender.setLAF(false);
        oRender.setBorderStyle(0);
        oRender.setFont(new java.awt.Font("Tahoma", 0, 11));

        if (biTable != null) {
            column = biTable.getColumn(column).getModelIndex();
            recLinha = biTable.getXMLData().get(linha);
            String tipoColuna = biTable.getColumnType(column);
            int colTipo = new Integer(tipoColuna).intValue();

            if (colTipo == JBIXMLTable.KPI_FLOAT) {
                renderNumber(oRender, new Double(value.toString()), column, row, recLinha);
            } else if (colTipo == JBIXMLTable.KPI_STRING) {
                renderString(oRender, value.toString(), column, row, recLinha);
            }

        }

        int rowType = recLinha.getInt("ROWTYPE");
        if (rowType == 1) {
            if (column != KpiGraphIndicador.COL_STATUS) {
                if (hasFocus) {
                    oRender.setBackground(new java.awt.Color(212, 208, 200));
                } else if (table.getSelectedRow() == row) {
                    oRender.setBackground(table.getSelectionBackground());
                    oRender.setForeground(table.getSelectionForeground());
                }
            }
        } else if (rowType == 2) {
            oRender.setBackground(java.awt.Color.lightGray);
            oRender.setForeground(java.awt.Color.blue);
        }

        return oRender;
    }

    private pv.jfcx.JPVEdit renderNumber(pv.jfcx.JPVEdit oRender, Double dbValor, int column, int row, BIXMLRecord recLinha) {
        java.text.NumberFormat oValor = java.text.NumberFormat.getInstance();

        if (column == KpiGraphIndicador.COL_STATUS) {
            int status = recLinha.getInt("STATUS");
            switch (status) {
                case KpiScoreCarding.STATUS_RED:
                    oRender.setBackground(new java.awt.Color(234, 140, 136));
                    break;
                case KpiScoreCarding.STATUS_YELLOW:
                    oRender.setBackground(new java.awt.Color(255, 235, 155));
                    break;
                case KpiScoreCarding.STATUS_GREEN:
                    oRender.setBackground(new java.awt.Color(139, 191, 150));
                    break;
                case KpiScoreCarding.STATUS_BLUE:
                    oRender.setBackground(new java.awt.Color(109, 178, 247));
                    break;
            }
        }

        if (recLinha.contains("DECIMAIS")) {
            oValor.setMinimumFractionDigits(recLinha.getInt("DECIMAIS"));
        } else {
            oValor.setMinimumFractionDigits(2);
        }

        oRender.setAlignment(2);
        oRender.setValue(oValor.format(dbValor));

        return oRender;
    }

    private pv.jfcx.JPVEdit renderString(pv.jfcx.JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha) {
        oRender.setValue(valor);
        return oRender;
    }
}
