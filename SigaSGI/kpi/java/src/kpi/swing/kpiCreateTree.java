package kpi.swing;

import kpi.xml.*;
import javax.swing.tree.*;
import java.util.*;
import javax.swing.*;

/**
 * @author Alexandre Alves da Silva
 */
public class kpiCreateTree {

    private String treeNodeID = new String();
    private DefaultMutableTreeNode root;
    private TreePath treePath;
    private BIXMLAttributes attributeFilter;

    public DefaultMutableTreeNode createTree(BIXMLRecord treeXML) {
	String key = (String) treeXML.getKeyNames().next();
	treeNodeID = "0";
	setRoot(insertGroup(treeXML.getBIXMLVector(key), null));

	return getRoot();
    }

    /**
     * M�todo construtor.
     * @param treeXML Vector com os dados para constru��o da �rvore.
     * @return (DefaultMutableTreeNode) �rvore.
     * @author Valdiney V GOMES
     * @since 30-03-2009 
     */
    public DefaultMutableTreeNode createTree(BIXMLVector treeXML) {
	treeNodeID = "0";

	setRoot(insertGroup(treeXML, null));

	return getRoot();
    }


    public BIXMLAttributes getAttributeFilter(){
	return attributeFilter;
    }

    public void setAttributeFilter(BIXMLAttributes filter){
	attributeFilter = filter;
    }

    private void setNodeProperties(BIXMLAttributes attributes, kpi.swing.KpiTreeNode oNode) {
	if (attributes.contains("IMAGE")) {
	    oNode.setIDimage(attributes.getInt("IMAGE"));
	}

	if (attributes.contains("ENABLE")) {
	    oNode.setEnable(attributes.getBoolean("ENABLE"));
	}

	//Verifica presen�a de filtros de campos
	if (oNode.isEnable() && attributeFilter != null) {
	    String attrName;
	    boolean enabled = true;
	    Iterator<String> itAttr = attributeFilter.getKeyNames();

	    while (enabled && itAttr.hasNext()) {
		attrName = itAttr.next();

		if (attributes.contains(attrName)){
		    enabled = (attributes.getString(attrName).equals(attributeFilter.getString(attrName)));
		}else{
		    enabled = false;
		}
	    }

	    oNode.setEnable(enabled);
	}
    }

    public DefaultMutableTreeNode insertRecord(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
	kpi.swing.KpiTreeNode oNode = new kpi.swing.KpiTreeNode(vector.getAttributes().getString("NOME"),
		vector.getAttributes().getString("ID"),
		vector.getAttributes().getString("NOME"),
		treeNodeID);

	setNodeProperties(vector.getAttributes(), oNode);
	DefaultMutableTreeNode child = new DefaultMutableTreeNode(oNode);

	for (int i = 0; i < vector.size(); i++) {
	    treeNodeID = vector.getMainTag().concat(vector.get(i).getAttributes().getString("ID"));
	    child = insertRecord(vector.get(i), child);
	}

	if (tree != null) {
	    tree.add(child);
	    return tree;
	} else {
	    return child;
	}
    }

    public DefaultMutableTreeNode insertRecord(BIXMLRecord record, DefaultMutableTreeNode tree) throws BIXMLException {
	String nodeType = record.getAttributes().contains("TIPO") ? record.getAttributes().getString("TIPO") : record.getTagName();
	kpi.swing.KpiTreeNode oNode = new kpi.swing.KpiTreeNode(nodeType,
		record.getAttributes().getString("ID"),
		record.getAttributes().getString("NOME"),
		treeNodeID, "", record);

	setNodeProperties(record.getAttributes(), oNode);
	DefaultMutableTreeNode child = new DefaultMutableTreeNode(oNode);

	for (Iterator e = record.getKeyNames(); e.hasNext();) {
	    String strChave = (String) e.next();

	    treeNodeID = record.getTagName().concat(record.getAttributes().getString("ID"));
	    if (record.getBIXMLVector(strChave).getAttributes().contains("TIPO")) {
		child = insertGroup(record.getBIXMLVector(strChave), child);
	    } else {
		child = insertRecord(record.getBIXMLVector(strChave), child);
	    }
	}

	if (tree != null) {
	    tree.add(child);
	    return tree;
	} else {
	    return child;
	}
    }

    public DefaultMutableTreeNode insertGroup(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
	BIXMLRecord record = new BIXMLRecord(vector.getRecordTag());
	record.setAttributes(vector.getAttributes());

	kpi.swing.KpiTreeNode oNode = new kpi.swing.KpiTreeNode(vector.getAttributes().getString("TIPO"),
		vector.getAttributes().getString("ID"),
		vector.getAttributes().getString("NOME"),
		treeNodeID, "", record);

	DefaultMutableTreeNode child = new DefaultMutableTreeNode(oNode);
	setNodeProperties(vector.getAttributes(), oNode);

	for (int i = 0; i < vector.size(); i++) {
	    treeNodeID = vector.getMainTag().concat(vector.get(i).getAttributes().getString("ID"));
	    child = insertRecord(vector.get(i), child);
	}

	if (tree != null) {
	    tree.add(child);
	    return tree;
	} else {
	    return child;
	}
    }

    /**
     * Insere um conjunto de filhos a um n� de uma �rvore.
     * @param record Conjunto de itens que ser�o inclu�dos na �rvore.
     * @param tree N� da �rvore no qual ser�o inclu�dos os itens.
     * @return (DefaultMutableTreeNode) �rvore montada.
     * @throws BIXMLException
     * @author Valdiney V GOMES
     * @since 30-03-2009
     */
    public DefaultMutableTreeNode insertChild(BIXMLRecord record, DefaultMutableTreeNode tree) throws BIXMLException {

	tree.removeAllChildren();

	for (Iterator e = record.getKeyNames(); e.hasNext();) {
	    String strChave = (String) e.next();

	    insertRecord(record.getBIXMLVector(strChave), tree);
	}

	return tree;
    }

    /**
     * Percore a �rvore e expande o caminho at� o �ltimo n� selecionado.
     * @param tree JTree que ser� percorrida.
     * @param path Path do �ltimo n� selecionado.
     * @return (Boolean) Identifica se a �rvore foi percorrida.
     * @author Valdiney V GOMES
     * @since 30-03-2009 
     */
    public boolean expandPath(JTree tree, TreePath path) {
	int jRows = 0;
	int i = 0;

	if (path != null) {

	    while (jRows < tree.getRowCount()) {

		DefaultMutableTreeNode oTreeNode = (DefaultMutableTreeNode) tree.getPathForRow(jRows).getLastPathComponent();
		DefaultMutableTreeNode oPathNode = (DefaultMutableTreeNode) path.getPathComponent(i);

		KpiTreeNode oKpiTreeNode = (KpiTreeNode) oTreeNode.getUserObject();
		KpiTreeNode oKpiPathNode = (KpiTreeNode) oPathNode.getUserObject();

		//Utiliza o ID para comparar o objeto do Path com o objeto da Tree.
		if (oKpiTreeNode.getID().equals(oKpiPathNode.getID())) {
		    //Impede que o �ltimo n� selecionado seja expandido.
		    if (i < path.getPathCount() - 1) {
			tree.expandRow(jRows);
			i++;
		    }

		    tree.setSelectionRow(jRows);
		}
		jRows++;
	    }

	    return true;

	} else {
	    return false;
	}
    }

    /**
     * Percore e expande a �rvoreo at� o caminho desejado.
     * @param tree JTree que ser� percorrida.
     * @param path String contendo os scorecards que fazem parte o caminho.
     * @return (Boolean) Identifica se a �rvore foi percorrida.
     * @author Valdiney V GOMES
     * @since 30-03-2009
     */
    public boolean expandPath(JTree tree, String path) {
	ArrayList<String> scorecards = new ArrayList<String>();
	StringTokenizer st = new StringTokenizer(path);
	int jRows = 0;
	int i = 0;

	while (st.hasMoreTokens()) {
	    scorecards.add(st.nextToken("|"));
	}

	while (jRows < tree.getRowCount()) {

	    DefaultMutableTreeNode oTreeNode = (DefaultMutableTreeNode) tree.getPathForRow(jRows).getLastPathComponent();

	    KpiTreeNode oKpiTreeNode = (KpiTreeNode) oTreeNode.getUserObject();

	    //Utiliza o ID para comparar o objeto do array com o objeto da Tree.
	    if (oKpiTreeNode.getID().equals(scorecards.get(i))) {

		if (i < scorecards.size() - 1) {
		    tree.expandRow(jRows);
		    i++;
		}
		tree.setSelectionRow(jRows);
	    }
	    jRows++;
	}
	return true;
    }

    /**
     * Guarda o caminho percorrido na �rvore.
     * @param treePath caminho percorrido na �rvore.
     * @author Valdiney V GOMES
     * @since 30-03-2009
     */
    public void setSelectedTreePath(TreePath treePath) {
	this.treePath = treePath;
    }

    /**
     * Retorna o caminho percorrido na �rvore.
     * @return TreePath caminho percorrido na �rvore.
     * @author Valdiney V GOMES
     * @since 30-03-2009
     */
    public TreePath getSelectedTreePath() {
	return treePath;
    }

    public DefaultMutableTreeNode getRoot() {
	return root;
    }

    public void setRoot(DefaultMutableTreeNode root) {
	this.root = root;
    }
}
