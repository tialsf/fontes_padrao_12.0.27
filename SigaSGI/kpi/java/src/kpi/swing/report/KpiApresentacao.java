package kpi.swing.report;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.swing.JCheckBox;
import javax.swing.JTabbedPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import kpi.beans.JBIXMLTable;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiFileChooser;
import kpi.swing.KpiSelectedTree;
import kpi.swing.KpiSelectedTreeCellRender;
import kpi.swing.KpiSelectedTreeNode;
import kpi.swing.analisys.KpiScoreCarding;
import kpi.swing.desktop.KpiDesktopFunctions;
import kpi.util.KpiDateUtil;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLException;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;
import pv.jfcx.JPVEdit;
import pv.jfcx.JPVTable;

public class KpiApresentacao extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private final int ABA_INDICADOR = 0;
    private final int ABA_ACAO = 1;
    private final int ABA_PROJETO = 2;  
    private String recordType = "RELAPR";
    private String parentType = "LSTRELAPR";
    private String formType = recordType;
    private KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private int status = KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private BIXMLRecord record = null;
    private KpiDateUtil date_Util = new KpiDateUtil();
    private KpiApresentacaoTableRenderer indicadorCellRenderer;
    private KpiApresentacaoTableRenderer planoCellRenderer;
    private BIXMLVector scorecards = new BIXMLVector();//Todos os scorecards.
    private BIXMLVector projetos = new BIXMLVector();//Projetos gravados no banco.
    private BIXMLVector acoes = new BIXMLVector();//Planos de a��es gravados no banco.
    private BIXMLVector indicadores = new BIXMLVector();//Indicadores gravados no banco.
    private HashMap projetoMarcado = new HashMap();//Projetos selecionados para grava��o
    private HashMap acaoMarcado = new HashMap();//Planos de A��es selecionados para grava��o.
    private HashMap indicadorMarcado = new HashMap();//Indicadores selecionados para grava��o.
    private HashMap cacheIndicador = new HashMap();//Dados j� requisitados ao servidor.
    private HashMap cacheProjeto = new HashMap();//Dados j� requisitados ao servidor.
    private String cachePeriodo = "";//Controle de altera��o na data;
    private List scorecardModificado = new ArrayList();
    private List projetoModificado = new ArrayList();
    private List indicadorModificado = new ArrayList();

    public KpiApresentacao(int operation, String idAux, String contextId) {
        putClientProperty("MAXI", true);
        initComponents();
        event.defaultConstructor(operation, idAux, contextId);

        configuraLista();
    }

    /**
     * M�todo para incluir uma coluna com check box para selecionar a linha
     * @param vector Vetor com os dados originais sem a coluna de checkBox.
     * @return <B>BIXMLVecotr</B> Com a a coluna de check box adicionada.
     */
    public BIXMLVector putColSelecionar(BIXMLVector vector, String tagSelecionar) {
        int numberOfColumns = 0;
        String attrAux;
        BIXMLAttributes newAttr = vector.getAttributes();
        BIXMLRecord recAux;

        newAttr.getKeyNames();

        for (java.util.Iterator e = vector.getAttributes().getKeyNames(); e.hasNext();) {
            attrAux = (String) e.next();
            if (attrAux.startsWith("TAG")) {
                numberOfColumns++;
            }
        }

        String zeros = "";
        if (numberOfColumns <= 9) {
            zeros = "00".concat(Integer.toString(numberOfColumns));
        } else if (numberOfColumns <= 99) {
            zeros = "0".concat(Integer.toString(numberOfColumns));
        }

        newAttr.set("TAG".concat(zeros), tagSelecionar);
        newAttr.set("CAB".concat(zeros), java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDefaultFrameBehavior_00006"));
        newAttr.set("CLA".concat(zeros), kpi.beans.JBIXMLTable.KPI_BOOLEAN);
        newAttr.set("EDT".concat(zeros), true);
        newAttr.set("CUM".concat(zeros), false);

        for (int reg = 0; reg < vector.size(); reg++) {
            recAux = vector.get(reg);
            recAux.set(tagSelecionar, false);
        }

        return vector;
    }

    /**
     * Marca as linhas que est�o selecionadas.
     * @param vctAllData Vetor com todos os dados.
     * @param vctSelected Vetor com os dados selecionados.
     * @return void
     * @Obs: A compara��o � feita atrav�s da tag <B><I>ID</I></B> que deve estar presente nos dois vetores.
     */
    public void markSelectedRow(BIXMLVector vctAllData, BIXMLVector vctSelected, String tagSelecionar, boolean selected) {
        BIXMLRecord regAux;
        BIXMLRecord regSelAux;

        for (int nReg = 0; nReg < vctAllData.size(); nReg++) {
            regAux = vctAllData.get(nReg);
            for (int nRegSel = 0; nRegSel < vctSelected.size(); nRegSel++) {
                regSelAux = vctSelected.get(nRegSel);
                if (regAux.getString("ID").equals(regSelAux.getString("ID"))) {
                    regAux.set(tagSelecionar, selected);
                    break;
                }
            }
        }
    }

    /**
     * Retorna todas as linhas que foram selecionadas no vetor.
     * @param vector Vetor com os todos os dados
     * @param String Nome da tag que identifica a coluna de sele��o.
     * @return <B>BIXMLVecotr</B> Com todos os itens que foram selecionados.
     */
    public BIXMLVector getVectorRowsSelected(BIXMLVector vctAllData, String tagSelecionar, String tagVector, String tagRecord) {

        DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) treeScorecard.getModel().getRoot();
        BIXMLVector vctSco = new BIXMLVector(tagVector, tagRecord);
        KpiSelectedTreeNode oNode;

        while (oMutTreeNode != null) {
            if (!oMutTreeNode.isRoot()) {
                oNode = (KpiSelectedTreeNode) oMutTreeNode.getUserObject();
                if (oNode.isSelected()) {
                    kpi.xml.BIXMLRecord oRec = new kpi.xml.BIXMLRecord(tagRecord);
                    oRec.set("ID", oNode.getID());
                    oRec.set("SELECTED", oNode.isSelected());
                    vctSco.add(oRec);
                }
            }
            oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();
        }
        return vctSco;
    }

    /**
     * Retorna um vetor com os conte�dos de um hashmap.
     * @param htbDados origem dos dados.
     * @param tagGrupo grupo de registros do XML.
     * @param tagRegistro registro do XML.
     * @return BIXMLVector
     */
    public BIXMLVector hashtoBIXMLVector(HashMap htbDados, String tagGrupo, String tagRegistro) {
        BIXMLVector vctSelected = new BIXMLVector(tagGrupo, tagRegistro);
        Collection<BIXMLRecord> colRecords = htbDados.values();
        BIXMLRecord recAux;

        for (Iterator e = colRecords.iterator(); e.hasNext();) {
            recAux = (BIXMLRecord) e.next();
            BIXMLRecord newRecord = recAux.clone2();
            newRecord.setTagName(tagRegistro);
            vctSelected.add(newRecord);
        }

        return vctSelected;
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlDados, true);
        event.setEnableFields(pnlTabela, true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlDados, false);
        event.setEnableFields(pnlTabela, false);

        setAba(false, false, false);
    }

    @Override
    public void refreshFields() {
        indicadorCellRenderer = new KpiApresentacaoTableRenderer(tbIndicador, record);
        planoCellRenderer = new KpiApresentacaoTableRenderer(tbAcao, record);

        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        txtTopicos.setText(record.getString("TOPICOS"));
        fldDataDe.setCalendar(record.getDate("DATADE"));
        fldDataAte.setCalendar(record.getDate("DATAATE"));
        fldDataAlvo.setCalendar(record.getDate("DATAALVO"));

        setListaScorecard();
        getProjetos();
        getAcoes();

        if (status == KpiDefaultFrameBehavior.INSERTING) {
            tskDados.setExpanded(true);
            pnlTabela.setPreferredSize(new Dimension(50, this.getHeight() + 70));
        } else {
            tskDados.setExpanded(false);
            pnlTabela.setPreferredSize(new Dimension(0, getContentPane().getHeight() - 165));
        }

    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        BIXMLRecord conteudo = new BIXMLRecord(recordType);

        //n�o remover - for�a atualiza��o do XML interno da table
        tbIndicador.getXMLData();
        tbAcao.getXMLData();
        tbProjeto.getXMLData();

        conteudo.set("ID", id);
        conteudo.set("NOME", fldNome.getText());
        conteudo.set("DESCRICAO", txtDescricao.getText());
        conteudo.set("DATADE", fldDataDe.getCalendar());
        conteudo.set("DATAATE", fldDataAte.getCalendar());
        conteudo.set("DATAALVO", fldDataAlvo.getCalendar());
        conteudo.set("TOPICOS", txtTopicos.getText());
        conteudo.set(getVectorRowsSelected(scorecards, "SELECTED", "SCORECARDS", "SCORECARD"));


        //INDICADORES MARCADOS
        indicadorMarcado.clear();
        acaoMarcado.clear();
        projetoMarcado.clear();

        for (Object key : cacheIndicador.keySet()) {

            BIXMLVector vecInd = (BIXMLVector) cacheIndicador.get(key);

            for (Iterator it = vecInd.iterator(); it.hasNext();) {
                BIXMLRecord recAux = (BIXMLRecord) it.next();

                if (recAux.getBoolean("SELECAO")) {
                    indicadorMarcado.put(recAux.getString("ID"), recAux);
                }
                //PLANO DE ACOES MARCADOS
                for (Iterator ita = recAux.getBIXMLVector("PLANOSACAO").iterator(); ita.hasNext();) {
                    BIXMLRecord recAuxAcoes = (BIXMLRecord) ita.next();

                    if (recAuxAcoes.contains("SELECTED") && recAuxAcoes.getBoolean("SELECTED")) {

                        BIXMLRecord valor = new BIXMLRecord("INDXPLAN");
                        valor.set("ID", recAuxAcoes.getString("ID"));
                        valor.set("ID_APRES", getID());
                        valor.set("ID_SCOREC", (String) key);
                        valor.set("ID_INDIC", recAux.getString("ID"));
                        valor.set("ID_PLANO", recAuxAcoes.getString("ID"));

                        acaoMarcado.put(recAuxAcoes.getString("ID"), valor);
                    }
                }
            }
        }

        //PROJETOS MARCADOS
        for (Object key : cacheProjeto.keySet()) {

            BIXMLVector vProj = (BIXMLVector) cacheProjeto.get(key);

            for (Iterator itp = vProj.iterator(); itp.hasNext();) {

                BIXMLRecord recProj = (BIXMLRecord) itp.next();
                if (recProj.getBoolean("SELECTED")) {

                    BIXMLRecord valorProj = new BIXMLRecord("SCORXPRJ");
                    valorProj.set("ID", recProj.getString("ID"));
                    valorProj.set("ID_APRES", getID());
                    valorProj.set("ID_SCOREC", (String) key);
                    valorProj.set("ID_PROJ", recProj.getString("ID"));

                    projetoMarcado.put(recProj.getString("ID"), valorProj);
                }

            }

        }

        conteudo.set(hashtoBIXMLVector(indicadorMarcado, "SCORXINDS", "SCORXIND"));
        conteudo.set(hashtoBIXMLVector(acaoMarcado, "INDXPLANS", "INDXPLAN"));
        conteudo.set(hashtoBIXMLVector(projetoMarcado, "SCORXPRJS", "SCORXPRJ"));

        conteudo.set("LST_SCOR_ALT", scorecardModificado.toString());//Lista com os scorecards que devem ser alterados;
        conteudo.set("LST_PROJ_ALT", projetoModificado.toString());//Lista com os projetos que devem ser alterados;
        conteudo.set("LST_IND_ALT", indicadorModificado.toString());//Lista com os indicadores que devem ser alterados;

        return conteudo;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00023"));
            retorno = false;
        }

        if (!retorno) {
            btnSave.setEnabled(true);
        }
        return retorno;
    }

    public boolean hasCombos() {
        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnVisualizar = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        btnGerar = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        btnAjuda = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        tbApresentacao = new javax.swing.JTabbedPane();
        tspApresentacao = new com.l2fprod.common.swing.JTaskPane();
        tskDados = new com.l2fprod.common.swing.JTaskPaneGroup();
        pnlDados = new javax.swing.JPanel();
        scrDescricao = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        fldNome = new pv.jfcx.JPVEdit();
        lblDescricao = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        lblDataAlvo = new javax.swing.JLabel();
        lblDataDe = new javax.swing.JLabel();
        lblDataAte = new javax.swing.JLabel();
        scrTopicos = new javax.swing.JScrollPane();
        txtTopicos = new kpi.beans.JBITextArea();
        lblTopicos = new javax.swing.JLabel();
        scrDataDe = new javax.swing.JScrollPane();
        fldDataDe = new pv.jfcx.JPVDatePlus();
        scrDataAte = new javax.swing.JScrollPane();
        fldDataAte = new pv.jfcx.JPVDatePlus();
        scrDataAlvo = new javax.swing.JScrollPane();
        fldDataAlvo = new pv.jfcx.JPVDatePlus();
        tskTabela = new com.l2fprod.common.swing.JTaskPaneGroup();
        pnlTabela = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        tabSelecao = new javax.swing.JTabbedPane();
        tbIndicador = new kpi.beans.JBIXMLTable();
        tbAcao = new kpi.beans.JBIXMLTable();
        tbProjeto = new kpi.beans.JBIXMLTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        treeScorecard = new javax.swing.JTree();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiApresentacao_00003")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 693, 558));
        setPreferredSize(new java.awt.Dimension(785, 450));

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlRecord.setPreferredSize(new java.awt.Dimension(760, 420));
        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(380, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(530, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(550, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new java.awt.Dimension(410, 25));
        tbInsertion.setMinimumSize(new java.awt.Dimension(410, 25));
        tbInsertion.setPreferredSize(new java.awt.Dimension(300, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle1.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnVisualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        btnVisualizar.setText(bundle1.getString("JBIListPanel_00002")); // NOI18N
        btnVisualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVisualizarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVisualizar);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.setPreferredSize(new java.awt.Dimension(82, 32));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        jSeparator1.setMaximumSize(new java.awt.Dimension(20, 20));
        jSeparator1.setMinimumSize(new java.awt.Dimension(20, 20));
        jSeparator1.setPreferredSize(new java.awt.Dimension(20, 20));
        tbInsertion.add(jSeparator1);

        btnGerar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_imprimir.gif"))); // NOI18N
        btnGerar.setText(bundle1.getString("KpiRelatorioPlanoAcao_00016")); // NOI18N
        btnGerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGerarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGerar);

        btnGravar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar_rel.gif"))); // NOI18N
        btnGravar.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGravar);

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        btnAbrir.setText(bundle1.getString("KpiApresentacao_00016")); // NOI18N
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAbrir);

        jSeparator2.setMaximumSize(new java.awt.Dimension(20, 20));
        jSeparator2.setMinimumSize(new java.awt.Dimension(20, 20));
        jSeparator2.setPreferredSize(new java.awt.Dimension(20, 20));
        tbInsertion.add(jSeparator2);

        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setPreferredSize(new java.awt.Dimension(750, 350));
        pnlFields.setLayout(new java.awt.BorderLayout());

        tskDados.setTitle(bundle1.getString("KpiApresentacao_00017")); // NOI18N
        tskDados.setToolTipText(bundle1.getString("KpiApresentacao_00017")); // NOI18N

        pnlDados.setOpaque(false);
        pnlDados.setPreferredSize(new java.awt.Dimension(0, 125));
        pnlDados.setLayout(null);

        txtDescricao.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        scrDescricao.setViewportView(txtDescricao);

        pnlDados.add(scrDescricao);
        scrDescricao.setBounds(20, 60, 400, 60);

        fldNome.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        fldNome.setMaxLength(60);
        pnlDados.add(fldNome);
        fldNome.setBounds(20, 20, 400, 22);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00005")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDados.add(lblDescricao);
        lblDescricao.setBounds(10, 40, 72, 20);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00004")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDados.add(lblNome);
        lblNome.setBounds(10, 0, 72, 20);

        lblDataAlvo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataAlvo.setText(bundle.getString("KpiApresentacao_00008")); // NOI18N
        lblDataAlvo.setEnabled(false);
        lblDataAlvo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataAlvo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDados.add(lblDataAlvo);
        lblDataAlvo.setBounds(460, 80, 80, 20);

        lblDataDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataDe.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00006")); // NOI18N
        lblDataDe.setEnabled(false);
        lblDataDe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataDe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataDe.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDados.add(lblDataDe);
        lblDataDe.setBounds(450, 0, 80, 20);

        lblDataAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataAte.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00007")); // NOI18N
        lblDataAte.setEnabled(false);
        lblDataAte.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataAte.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataAte.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDados.add(lblDataAte);
        lblDataAte.setBounds(450, 40, 80, 20);

        txtTopicos.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        scrTopicos.setViewportView(txtTopicos);

        pnlDados.add(scrTopicos);
        scrTopicos.setBounds(660, 20, 190, 100);

        lblTopicos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTopicos.setText(bundle.getString("KpiApresentacao_00019")); // NOI18N
        lblTopicos.setEnabled(false);
        lblTopicos.setMaximumSize(new java.awt.Dimension(100, 13));
        lblTopicos.setMinimumSize(new java.awt.Dimension(100, 13));
        lblTopicos.setPreferredSize(new java.awt.Dimension(100, 13));
        pnlDados.add(lblTopicos);
        lblTopicos.setBounds(660, 0, 68, 20);

        scrDataDe.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataDe.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataDe.setBorderStyle(0);
        fldDataDe.setButtonBorder(0);
        fldDataDe.setDialog(true);
        fldDataDe.setUseLocale(true);
        fldDataDe.setWaitForCalendarDate(true);
        fldDataDe.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                fldDataDeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                fldDataDeFocusLost(evt);
            }
        });
        scrDataDe.setViewportView(fldDataDe);

        pnlDados.add(scrDataDe);
        scrDataDe.setBounds(450, 20, 180, 22);

        scrDataAte.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataAte.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataAte.setBorderStyle(0);
        fldDataAte.setButtonBorder(0);
        fldDataAte.setDialog(true);
        fldDataAte.setUseLocale(true);
        fldDataAte.setWaitForCalendarDate(true);
        fldDataAte.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                fldDataAteFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                fldDataAteFocusLost(evt);
            }
        });
        scrDataAte.setViewportView(fldDataAte);

        pnlDados.add(scrDataAte);
        scrDataAte.setBounds(450, 60, 180, 22);

        scrDataAlvo.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataAlvo.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataAlvo.setBorderStyle(0);
        fldDataAlvo.setButtonBorder(0);
        fldDataAlvo.setCurrentDate(true);
        fldDataAlvo.setDialog(true);
        fldDataAlvo.setUseLocale(true);
        fldDataAlvo.setWaitForCalendarDate(true);
        fldDataAlvo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                fldDataAlvoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                fldDataAlvoFocusLost(evt);
            }
        });
        scrDataAlvo.setViewportView(fldDataAlvo);

        pnlDados.add(scrDataAlvo);
        scrDataAlvo.setBounds(450, 100, 180, 22);

        tskDados.getContentPane().add(pnlDados);

        tspApresentacao.add(tskDados);

        tskTabela.setTitle(bundle1.getString("KpiApresentacao_00003")); // NOI18N
        tskTabela.setToolTipText(bundle1.getString("KpiApresentacao_00003")); // NOI18N
        tskTabela.setAutoscrolls(true);

        pnlTabela.setAutoscrolls(true);
        pnlTabela.setLayout(new java.awt.BorderLayout());

        jSplitPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jSplitPane1.setDividerLocation(250);
        jSplitPane1.setOneTouchExpandable(true);

        tabSelecao.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);
        tabSelecao.addTab(bundle.getString("KpiApresentacao_00012"), new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_indicador.gif")), tbIndicador); // NOI18N
        tabSelecao.addTab(bundle.getString("KpiApresentacao_00013"), new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif")), tbAcao); // NOI18N
        tabSelecao.addTab(bundle.getString("KpiApresentacao_00014"), new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_projeto.gif")), tbProjeto); // NOI18N

        jSplitPane1.setRightComponent(tabSelecao);

        jScrollPane1.setAutoscrolls(true);

        treeScorecard.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        treeScorecard.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        treeScorecard.setAutoscrolls(true);
        treeScorecard.setRootVisible(false);
        treeScorecard.setRowHeight(18);
        treeScorecard.setShowsRootHandles(true);
        treeScorecard.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                treeScorecardMouseClicked(evt);
            }
        });
        treeScorecard.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                treeScorecardValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(treeScorecard);
        treeScorecard.getAccessibleContext().setAccessibleParent(jScrollPane1);

        jSplitPane1.setLeftComponent(jScrollPane1);

        pnlTabela.add(jSplitPane1, java.awt.BorderLayout.CENTER);

        tskTabela.getContentPane().add(pnlTabela);

        tspApresentacao.add(tskTabela);

        tbApresentacao.addTab(bundle1.getString("KpiApresentacao_00003"), tspApresentacao); // NOI18N

        pnlFields.add(tbApresentacao, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlRecord, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
            KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder path = new StringBuilder();
            StringBuilder filter = new StringBuilder();

            filter.append("report");
            filter.append("\\");
            filter.append(systemProperties.recDesktop.getString("IDUSER"));
            filter.append("\\");
            filter.append("Spool");
            filter.append("\\");
            filter.append("*.");
            filter.append("html");

            path.append("/");
            path.append("report");
            path.append("/");
            path.append(systemProperties.recDesktop.getString("IDUSER"));
            path.append("/");
            path.append("spool");
            path.append("/");

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            frmChooser.setFileType("*.html");
            frmChooser.loadServerFiles(filter.toString());
            frmChooser.setVisible(true);
            frmChooser.setUrlPath(path.toString());
            frmChooser.openFile();
	}//GEN-LAST:event_btnAbrirActionPerformed

	private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
            KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder file = new StringBuilder();
            StringBuilder filter = new StringBuilder();

            filter.append("report");
            filter.append("\\");
            filter.append(systemProperties.recDesktop.getString("IDUSER"));
            filter.append("\\");
            filter.append("Spool");
            filter.append("\\");
            filter.append("*.");
            filter.append("html");

            file.append("rel052");
            file.append("_");
            file.append(record.getString("ID"));
            file.append(".");
            file.append("html");

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
            frmChooser.setFileType("*.html");
            frmChooser.loadServerFiles(filter.toString());
            frmChooser.setFileName(file.toString());
            frmChooser.setVisible(true);

            if (frmChooser.isValidFile()) {
                event.executeRecord("SALVAR;" + (String) frmChooser.getFileName());
            }
	}//GEN-LAST:event_btnGravarActionPerformed

	private void fldDataAteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fldDataAteFocusLost
            String dateActual = date_Util.getCalendarString(fldDataAte.getCalendar());
            if (!dateActual.equals(cachePeriodo)) {
                limpaCache();
            }
	}//GEN-LAST:event_fldDataAteFocusLost

	private void fldDataAteFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fldDataAteFocusGained
            cachePeriodo = date_Util.getCalendarString(fldDataAte.getCalendar());
	}//GEN-LAST:event_fldDataAteFocusGained

	private void fldDataAlvoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fldDataAlvoFocusLost
            String dateActual = date_Util.getCalendarString(fldDataAlvo.getCalendar());
            if (!dateActual.equals(cachePeriodo)) {
                limpaCache();
            }
	}//GEN-LAST:event_fldDataAlvoFocusLost

	private void fldDataAlvoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fldDataAlvoFocusGained
            cachePeriodo = date_Util.getCalendarString(fldDataAlvo.getCalendar());
	}//GEN-LAST:event_fldDataAlvoFocusGained

	private void fldDataDeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fldDataDeFocusLost
            String dateActual = date_Util.getCalendarString(fldDataDe.getCalendar());
            if (!dateActual.equals(cachePeriodo)) {
                limpaCache();
            }
	}//GEN-LAST:event_fldDataDeFocusLost

	private void fldDataDeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fldDataDeFocusGained
            cachePeriodo = date_Util.getCalendarString(fldDataDe.getCalendar());
	}//GEN-LAST:event_fldDataDeFocusGained

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

	private void btnVisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVisualizarActionPerformed
            KpiToolKit toolkit = new KpiToolKit();
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder file = new StringBuilder();

            file.append("report");
            file.append("/");
            file.append(systemProperties.recDesktop.getString("IDUSER"));
            file.append("/");
            file.append("rel052");
            file.append("_");
            file.append(this.id);
            file.append(".");
            file.append("html");

            toolkit.browser(file.toString());            
	}//GEN-LAST:event_btnVisualizarActionPerformed

	private void btnGerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGerarActionPerformed
            StringBuilder parametro = new StringBuilder("");

            if (status == KpiDefaultFrameBehavior.INSERTING || status == KpiDefaultFrameBehavior.UPDATING) {
                parametro.append("SALVAR;");
            }

            parametro.append("rel052_");
            parametro.append(record.getString("ID"));
            parametro.append(".html");

            event.executeRecord(parametro.toString());
	}//GEN-LAST:event_btnGerarActionPerformed

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
            //limpaCache();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

private void treeScorecardMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_treeScorecardMouseClicked

    if (status == KpiDefaultFrameBehavior.UPDATING || status == KpiDefaultFrameBehavior.INSERTING) {

        int x = evt.getX();
        int y = evt.getY();
        int somaX = new JCheckBox().getPreferredSize().width;

        int row = treeScorecard.getRowForLocation(x, y);
        TreePath path = treeScorecard.getPathForRow(row);

        if (tabSelecao.getSelectedIndex() == ABA_ACAO) {
            tabSelecao.setSelectedIndex(ABA_INDICADOR);
        }

        setAba(tabSelecao.isEnabledAt(ABA_INDICADOR), false, tabSelecao.isEnabledAt(ABA_PROJETO));
        if (path != null) {
            if (x <= treeScorecard.getPathBounds(path).x + somaX) {
                DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) path.getLastPathComponent();
                KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
                boolean bSelected = !(node.isSelected());
                node.setSelected(bSelected);

                if (evt.getButton() == MouseEvent.BUTTON1) {
                    selectedFromRoot(nodeAux, bSelected);
                }

                ((DefaultTreeModel) treeScorecard.getModel()).nodeChanged(node);

                treeScorecard.revalidate();
                treeScorecard.repaint();
            }
        }
    }

}//GEN-LAST:event_treeScorecardMouseClicked

private void treeScorecardValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_treeScorecardValueChanged

    BIXMLVector projeto = new BIXMLVector();
    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) evt.getPath().getLastPathComponent();
    KpiSelectedTreeNode node = (KpiSelectedTreeNode) treeNode.getUserObject();
    String chave = node.getID();

    setListaIndicador(chave);
    projeto = setListaProjeto(chave);
    setAba(indicadores.size() > 0, false, projeto.size() > 0);

}//GEN-LAST:event_treeScorecardValueChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnGerar;
    private javax.swing.JButton btnGravar;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnVisualizar;
    private pv.jfcx.JPVDatePlus fldDataAlvo;
    private pv.jfcx.JPVDatePlus fldDataAte;
    private pv.jfcx.JPVDatePlus fldDataDe;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblDataAte;
    private javax.swing.JLabel lblDataDe;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblTopicos;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlDados;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTabela;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scrDataAlvo;
    private javax.swing.JScrollPane scrDataAte;
    private javax.swing.JScrollPane scrDataDe;
    private javax.swing.JScrollPane scrDescricao;
    private javax.swing.JScrollPane scrTopicos;
    private javax.swing.JTabbedPane tabSelecao;
    private kpi.beans.JBIXMLTable tbAcao;
    private javax.swing.JTabbedPane tbApresentacao;
    private javax.swing.JToolBar tbConfirmation;
    private kpi.beans.JBIXMLTable tbIndicador;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBIXMLTable tbProjeto;
    private javax.swing.JTree treeScorecard;
    private com.l2fprod.common.swing.JTaskPaneGroup tskDados;
    private com.l2fprod.common.swing.JTaskPaneGroup tskTabela;
    private com.l2fprod.common.swing.JTaskPane tspApresentacao;
    private kpi.beans.JBITextArea txtDescricao;
    private kpi.beans.JBITextArea txtTopicos;
    // End of variables declaration//GEN-END:variables

    /**
     * Define os listeners das tabelas.
     */
    private void configuraLista() {
        setListaDefault();
        setIndicadorListener();
    }

    /**
     * Define o listener para lista de indicadores.
     */
    private void setIndicadorListener() {
        JPVTable indicador = tbIndicador.getTable();
        indicador.addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {

                if (status != KpiDefaultFrameBehavior.NORMAL) {
                    //int linha = setSelecao(evt, indicadorMarcado, tbIndicador, "SCORXIND", "SELECAO", ABA_INDICADOR);

                    JPVTable tabela = (JPVTable) evt.getSource();
                    int linha = tbIndicador.getOriginalRow(tabela.getFocusRow());

                    if (linha > -1) {
                        BIXMLVector acoes = setListaAcao(linha);
                        setAba(tabSelecao.isEnabledAt(ABA_INDICADOR), acoes.size() > 0, tabSelecao.isEnabledAt(ABA_PROJETO));
                    }
                }
            }
        });
    }

    /**
     * Recupera as a��es gravadas e prepara o objeto para sele��o.
     * @throws BIXMLException
     */
    private void getAcoes() throws BIXMLException {
        acoes = record.getBIXMLVector("INDXPLANS");
        for (int i = 0; i < acoes.size(); i++) {
            BIXMLRecord auxiliar = acoes.get(i);
            acaoMarcado.put(auxiliar.getString("ID"), auxiliar);
        }
    }

    /**
     * Recupera os projetos gravados e prepara o objeto para sele��o.
     * @throws BIXMLException
     */
    private void getProjetos() throws BIXMLException {
        projetos = record.getBIXMLVector("SCORXPRJS");
        for (int i = 0; i < projetos.size(); i++) {
            BIXMLRecord auxiliar = projetos.get(i);
            projetoMarcado.put(auxiliar.getString("ID"), auxiliar);
        }
    }

    /**
     * Recupera os scorecards gravados e prepara o objeto para sele��o.
     * @throws BIXMLException
     */
    private void setListaScorecard() throws BIXMLException {

        scorecards = record.getBIXMLVector("SCORECARDS");

        treeScorecard.setModel(new javax.swing.tree.DefaultTreeModel(KpiSelectedTree.createTree(scorecards)));
        treeScorecard.setCellRenderer(new KpiSelectedTreeCellRender(kpi.core.KpiStaticReferences.getKpiImageResources()));
        treeScorecard.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    /**
     * Popula a lista de a��es.
     * @param linha posi��o do indicador selecionado.
     * @return lista.
     * @throws BIXMLException
     */
    private BIXMLVector setListaAcao(int linha) throws BIXMLException {

        tbAcao.getXMLData();

        BIXMLVector lista = indicadores.get(linha).getBIXMLVector("PLANOSACAO");

        if (!lista.getAttributes().toString().contains("SELECTED")) {
            putColSelecionar(lista, "SELECTED");
            markSelectedRow(lista, record.getBIXMLVector("INDXPLANS"), "SELECTED", true);
            indicadorModificado.add(indicadores.get(linha).getString("ID"));
        }


        tbAcao.setDataSource(lista);

        applyRenderPlano(tbAcao);
        tbAcao.getTable().moveColumn(8, 0);

        return lista;
    }

    /**
     *
     * @param tabela
     * @param linha
     * @return
     * @throws BIXMLException
     */
    private String setListaIndicador(String chave) throws BIXMLException {

        tbIndicador.getXMLData();

        if (cacheIndicador.containsKey(chave)) {
            indicadores = (BIXMLVector) cacheIndicador.get(chave);
        } else {
            StringBuilder parametro = new StringBuilder("ID_SCOREC = ");
            parametro.append("'".concat(chave).concat("'"));
            parametro.append(",");
            parametro.append("SELECIONAR");
            parametro.append(",");
            parametro.append(date_Util.getCalendarString(fldDataDe.getCalendar()));
            parametro.append(",");
            parametro.append(date_Util.getCalendarString(fldDataAte.getCalendar()));
            parametro.append(",");
            parametro.append(date_Util.getCalendarString(fldDataAlvo.getCalendar()));
            parametro.append(",");
            parametro.append(getID());
            BIXMLRecord lista = event.listRecords("INDICADOR", parametro.toString());
            indicadores = lista.getBIXMLVector("INDICADORES");
            cacheIndicador.put(chave, indicadores);
            scorecardModificado.add(chave);
        }

        tbIndicador.setDataSource(indicadores);
        applyRenderIndicador(tbIndicador);
        tbIndicador.getColumn(1).setPreferredWidth(200);

        return chave;

    }

    /**
     *
     * @param chave
     * @param projeto
     * @return
     * @throws BIXMLException
     */
    private BIXMLVector setListaProjeto(String chave) throws BIXMLException {

        tbProjeto.getXMLData();

        if (cacheProjeto.containsKey(chave)) {
            projetos = (BIXMLVector) cacheProjeto.get(chave);

        } else {
            BIXMLRecord recProjetos = event.listRecords("PROJETO", chave);
            projetos = recProjetos.getBIXMLVector("PROJETOS");

            if (!projetos.getAttributes().toString().contains("SELECTED")) {
                projetos = putColSelecionar(projetos, "SELECTED");
                markSelectedRow(projetos, record.getBIXMLVector("SCORXPRJS"), "SELECTED", true);
            }

            cacheProjeto.put(chave, projetos);
            projetoModificado.add(chave);
        }

        tbProjeto.setDataSource(projetos);
        tbProjeto.getTable().moveColumn(1, 0);
        tbProjeto.getColumn(0).setMaxWidth(70);

        return projetos;
    }

    /**
     *
     * @param indicador
     * @param acao
     * @param projeto
     */
    private void setAba(boolean indicador, boolean acao, boolean projeto) {
        tabSelecao.setEnabledAt(ABA_INDICADOR, indicador);
        tabSelecao.setEnabledAt(ABA_ACAO, acao);
        tabSelecao.setEnabledAt(ABA_PROJETO, projeto);
    }

    /**
     * Define o valor inicial da lista de Indicadores x Scorecards.
     */
    private void setListaDefault() {
        BIXMLAttributes atributos = new BIXMLAttributes();

        atributos.set("TIPO", "INDICADOR");
        atributos.set("TAG000", "NOME");
        atributos.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00011"));
        atributos.set("CLA000", "4");
        atributos.set("RETORNA", "F");

        BIXMLVector lista = new BIXMLVector("INDICADORES", "INDICADOR", atributos);

        tbIndicador.setDataSource(lista);
    }

    /**
     *Limpa as cole��es de controle da apresenta��o.
     */
    private void limpaCache() {
        cacheIndicador.clear();
        cacheProjeto.clear();
        scorecardModificado.clear();
        projetoModificado.clear();
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getFormType() {
        return formType;
    }

    /**
     *
     * @param oTable
     */
    private void applyRenderIndicador(kpi.beans.JBIXMLTable oTable) {
        KpiApresentacaoCellEditor editor = new KpiApresentacaoCellEditor(oTable, new JPVEdit(), JPVTable.BOOLEAN);
        indicadorCellRenderer.resetRendererBuffer();

        for (int col = 0; col < tbIndicador.getModel().getColumnCount(); col++) {
            if (col == 0) {
                oTable.getColumn(col).setMaxWidth(70);
            }
            String tipoColuna = oTable.getColumnType(col);
            int colTipo = new Integer(tipoColuna).intValue();

            if (colTipo == JBIXMLTable.KPI_FLOAT || colTipo == JBIXMLTable.KPI_STRING) {
                oTable.getColumn(col).setCellRenderer(indicadorCellRenderer);
            } else if (colTipo == JBIXMLTable.KPI_BOOLEAN) {
                oTable.getColumn(col).setCellEditor(editor);
            }
        }
    }

    /**
     *
     * @param oTable
     */
    private void applyRenderPlano(kpi.beans.JBIXMLTable oTable) {
        int colInd = -1;

        planoCellRenderer.resetRendererBuffer();

        for (int col = 0; col < tbAcao.getModel().getColumnCount(); col++) {
            if (col == 0) {
                oTable.getColumn(col).setMaxWidth(70);
            }

            int colTipo = Integer.parseInt(oTable.getColumnType(col));
            String colName = tbAcao.getColumnTag(col);

            if ((colTipo == JBIXMLTable.KPI_FLOAT || colTipo == JBIXMLTable.KPI_STRING) && !colName.equals("IMG_OWNER")) {
                oTable.getColumn(col).setCellRenderer(planoCellRenderer);
            }

            if (colName.equals("ID_IND")) {
                colInd = col;
            }
        }

        if (colInd != -1) {
            oTable.getTable().getColumnModel().removeColumn(oTable.getColumn(colInd));
        }
    }

    private void selectedFromRoot(DefaultMutableTreeNode oNode, boolean bSelected) {
        Enumeration e = oNode.children();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) e.nextElement();
            KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
            node.setSelected(bSelected);
            selectedFromRoot(nodeAux, bSelected);


        }
    }
}

class KpiApresentacaoTableRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private Color colorRed = new java.awt.Color(234, 140, 136);
    private Color colorYellow = new java.awt.Color(255, 235, 155);
    private Color colorGreen = new java.awt.Color(139, 191, 150);
    private Color colorGray = new java.awt.Color(228, 228, 228);
    private Color colorWhite = new java.awt.Color(255, 255, 255);
    private Color colorBlue = new java.awt.Color(109, 178, 247);
    private JBIXMLTable biTable = null;
    private JPVEdit oRender = new JPVEdit();
    private HashMap buffLinha = new HashMap();
    private Font fonteType = new Font("Tahoma", 0, 11);
    private Locale locale = KpiStaticReferences.getKpiDefaultLocale();
    private boolean lShowVar = false;
    private int nVarDecimal = 2;

    /**
     *
     */
    public KpiApresentacaoTableRenderer() {
    }

    /**
     *
     * @param table
     * @param record
     */
    public KpiApresentacaoTableRenderer(kpi.beans.JBIXMLTable table, kpi.xml.BIXMLRecord record) {
        biTable = table;
        lShowVar = record.getBoolean("SHOWVARCOL");
        nVarDecimal = record.getInt("DECIMALVAR");
    }

    /**
     *
     */
    public void resetRendererBuffer() {
        buffLinha.clear();
    }

    /**
     *
     * @param table
     * @param value
     * @param isSelected
     * @param hasFocus
     * @param row
     * @param coluna
     * @return
     */
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int coluna) {
        BIXMLRecord dados = null;
        int linha = biTable.getOriginalRow(row);

        oRender.setBackground(colorWhite);
        oRender.setForeground(java.awt.Color.BLACK);
        oRender.setAlignment(0);
        oRender.setFontStyle(pv.jfcx.JPVEdit.NORMAL);
        oRender.setLocale(locale);
        oRender.setLAF(false);
        oRender.setBorderStyle(0);
        oRender.setFont(fonteType);
        oRender.setText("");
        oRender.setToolTipText("");

        if (biTable != null) {
            coluna = biTable.getColumn(coluna).getModelIndex();
            String tipo = biTable.getColumnType(coluna);
            String nome = biTable.getColumnTag(coluna);
            int dado = new Integer(tipo).intValue();

            if (buffLinha.containsKey(linha)) {
                dados = (BIXMLRecord) buffLinha.get(linha);
            } else {
                dados = biTable.getXMLData().get(linha);
                buffLinha.put(linha, dados);
            }

            if (dado == JBIXMLTable.KPI_FLOAT) {
                if (!dados.contains("IND_ESTRAT") || !dados.getBoolean("IND_ESTRAT")) {
                    renderNumber(oRender, new Double(value.toString()), coluna, row, dados, nome);
                }
            } else if (dado == JBIXMLTable.KPI_STRING) {
                renderString(oRender, value.toString(), coluna, row, dados, nome);
            }

            if (!nome.equals("VARIACAO") && !nome.equals("VARIACAOACUM")) {
                if (table.getSelectedRow() == row) {
                    oRender.setBackground(table.getSelectionBackground());
                    oRender.setForeground(table.getSelectionForeground());
                }
            }
        }

        if (dados.contains("IND_ESTRAT") && dados.getBoolean("IND_ESTRAT")) {
            oRender.setBackground(colorGray);
        }

        return oRender;
    }

    /**
     *
     * @param oRender
     * @param dbValor
     * @param column
     * @param row
     * @param recLinha
     * @param colName
     * @return
     */
    private pv.jfcx.JPVEdit renderNumber(pv.jfcx.JPVEdit oRender, Double dbValor, int column, int row, BIXMLRecord recLinha, String colName) {
        NumberFormat valor = NumberFormat.getInstance();

        if (colName.equals("VARIACAO") || colName.equals("VARIACAOACUM")) {
            int status = 0;
            if (colName.equals("VARIACAO")) {
                status = recLinha.getInt("STATUS");
            } else {
                status = recLinha.getInt("STATUS_ACU");
            }

            switch (status) {
                case KpiScoreCarding.STATUS_RED:
                    oRender.setBackground(colorRed);
                    break;
                case KpiScoreCarding.STATUS_YELLOW:
                    oRender.setBackground(colorYellow);
                    break;
                case KpiScoreCarding.STATUS_GREEN:
                    oRender.setBackground(colorGreen);
                    break;
                case KpiScoreCarding.ESTAVEL_GRAY:
                    oRender.setBackground(colorGray);
                    break;
                case KpiScoreCarding.STATUS_BLUE:
                    oRender.setBackground(colorBlue);
                    break;
            }

            if (lShowVar) {
                valor.setMinimumFractionDigits(nVarDecimal);
                valor.setMaximumFractionDigits(nVarDecimal);
                oRender.setAlignment(2);
                oRender.setValue(valor.format(dbValor).toString().concat(" %"));
            } else {
                if (recLinha.contains("DECIMAIS")) {
                    valor.setMinimumFractionDigits(recLinha.getInt("DECIMAIS"));
                    valor.setMaximumFractionDigits(recLinha.getInt("DECIMAIS"));
                } else {
                    valor.setMinimumFractionDigits(2);
                    valor.setMaximumFractionDigits(2);
                }

                oRender.setAlignment(2);
                oRender.setValue(valor.format(dbValor));
            }
        } else {
            if (recLinha.contains("DECIMAIS")) {
                valor.setMinimumFractionDigits(recLinha.getInt("DECIMAIS"));
                valor.setMaximumFractionDigits(recLinha.getInt("DECIMAIS"));
            } else {
                valor.setMinimumFractionDigits(2);
                valor.setMaximumFractionDigits(2);
            }

            oRender.setAlignment(2);
            oRender.setValue(valor.format(dbValor));
        }

        return oRender;
    }

    /**
     *
     * @param oRender
     * @param valor
     * @param column
     * @param row
     * @param recLinha
     * @param colName
     * @return
     */
    private pv.jfcx.JPVEdit renderString(pv.jfcx.JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha, String colName) {
        oRender.setValue(valor);

        if (colName.equals("STATUS")) {
            int status = recLinha.getInt("STATUS_ID");
            switch (status) {
                case 1:
                    oRender.setBackground(colorRed);
                    break;
                case 2:
                    oRender.setBackground(colorYellow);
                    break;
                case 3:
                    oRender.setBackground(colorGreen);
                    break;
                case 4:
                    oRender.setBackground(java.awt.Color.white);
                    break;
                case 5:
                    oRender.setBackground(colorGray);
                    break;
                case 6:
                    oRender.setBackground(colorGray);
                    break;
            }
        }

        return oRender;
    }
}

/**
 *
 * @author Chaves
 */
class KpiApresentacaoCellEditor extends pv.jfcx.PVTableEditor {

    private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();
    private kpi.beans.JBIXMLTable biTable = null;
    private BIXMLRecord recLinha = null;

    /**
     *
     * @param c
     * @param i
     */
    public KpiApresentacaoCellEditor(Component c, int i) {
        super(c, i);
    }

    /**
     *
     * @param table
     * @param c
     * @param i
     */
    public KpiApresentacaoCellEditor(kpi.beans.JBIXMLTable table, Component c, int i) {
        super(c, i);
        this.biTable = table;
    }

    /**
     * 
     * @param e
     * @return
     */
    @Override
    public boolean isCellEditable(java.util.EventObject e) {
        JPVTable tabela = (JPVTable) e.getSource();
        int linha = biTable.getOriginalRow(tabela.getEditingRow());

        if (linha != -1) {
            recLinha = biTable.getXMLData().get(linha);
            if (recLinha.getBoolean("ENABLE")) {
                return super.isCellEditable(e);
            } else {
                if (((java.awt.event.MouseEvent) e).getClickCount() > 1) {
                    dialog.informationMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00020") + "\n"
                            + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00021") + "\n"
                            + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00022"));
                }
                return false;
            }
        }

        return true;
    }
}