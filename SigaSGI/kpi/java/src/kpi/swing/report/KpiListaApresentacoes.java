package kpi.swing.report;

public class KpiListaApresentacoes extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "RELAPR";
    private String formType = "LSTRELAPR";
    private String parentType = recordType;

    public KpiListaApresentacoes(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        kpi.xml.BIXMLRecord recGrupos = event.listRecords("RELAPR", "");
        jBIListGrupoIndicador.setDataSource(recGrupos.getBIXMLVector("RELAPRS"), "0", "0", this.event);
        jBIListGrupoIndicador.xmlTable.setSortColumn(0);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new javax.swing.JTabbedPane();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        jBIListGrupoIndicador = new kpi.beans.JBISeekListPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiApresentacao_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(600, 358));

        tapCadastro.setEnabled(false);
        tapCadastro.setFocusable(false);
        tapCadastro.setPreferredSize(new java.awt.Dimension(0, 0));
        getContentPane().add(tapCadastro, java.awt.BorderLayout.WEST);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);
        getContentPane().add(jBIListGrupoIndicador, java.awt.BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private kpi.beans.JBISeekListPanel jBIListGrupoIndicador;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JTabbedPane tapCadastro;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }
    

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void loadRecord() {
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
