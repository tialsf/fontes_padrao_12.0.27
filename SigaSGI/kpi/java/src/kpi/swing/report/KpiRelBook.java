package kpi.swing.report;

import java.awt.event.MouseEvent;
import java.util.Enumeration;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JTabbedPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiFileChooser;
import kpi.swing.KpiSelectedTree;
import kpi.swing.KpiSelectedTreeCellRender;
import kpi.swing.KpiSelectedTreeNode;
import kpi.swing.desktop.KpiDesktopFunctions;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 * @author felipe.lopes
 */
public class KpiRelBook extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    /** Creates new form KpiRelBook */
    private BIXMLRecord record = null;
    private String recordType = "RELBOOK"; //NOI18N
    private String parentType = recordType;
    private String formType = recordType;
    private BIXMLVector scorecards = new BIXMLVector();//Todos os scorecards.
    private KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private String id = new String();
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;

    public KpiRelBook(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
        event.editRecord(); // Relatorio por default edit
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        tbIndicadores = new javax.swing.JTabbedPane();
        jTaskPane1 = new com.l2fprod.common.swing.JTaskPane();
        tskDados = new com.l2fprod.common.swing.JTaskPaneGroup();
        pnlDados = new javax.swing.JPanel();
        scrDescricao = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        fldNome = new pv.jfcx.JPVEdit();
        lblDescricao = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        pnlOpcoes = new javax.swing.JPanel();
        chkIncluirDescricao = new javax.swing.JCheckBox();
        chkIncluirIndicadores = new javax.swing.JCheckBox();
        pnlVisual = new javax.swing.JPanel();
        chkExpandido = new javax.swing.JCheckBox();
        tskEntidades = new com.l2fprod.common.swing.JTaskPaneGroup();
        pnlEntidades = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        treeScorecard1 = new javax.swing.JTree();
        pnlTools = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnGeraRel = new javax.swing.JButton();
        btnVizualizaRel = new javax.swing.JButton();
        btnGravaRel = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiRelBook_00001")); // NOI18N

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setLayout(new java.awt.BorderLayout());

        tskDados.setTitle("Dados Gerais");

        pnlDados.setOpaque(false);
        pnlDados.setPreferredSize(new java.awt.Dimension(0, 125));
        pnlDados.setLayout(null);

        txtDescricao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        scrDescricao.setViewportView(txtDescricao);

        pnlDados.add(scrDescricao);
        scrDescricao.setBounds(20, 60, 400, 60);

        fldNome.setMaxLength(60);
        pnlDados.add(fldNome);
        fldNome.setBounds(20, 20, 400, 22);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00005")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDados.add(lblDescricao);
        lblDescricao.setBounds(10, 40, 72, 20);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00004")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDados.add(lblNome);
        lblNome.setBounds(10, 0, 72, 20);

        pnlOpcoes.setBackground(new java.awt.Color(214, 223, 247));
        pnlOpcoes.setLayout(null);

        chkIncluirDescricao.setBackground(new java.awt.Color(214, 223, 247));
        chkIncluirDescricao.setEnabled(false);
        chkIncluirDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        chkIncluirDescricao.setLabel(bundle.getString("KpiRelBook_00005")); // NOI18N
        pnlOpcoes.add(chkIncluirDescricao);
        chkIncluirDescricao.setBounds(10, 10, 111, 20);

        chkIncluirIndicadores.setBackground(new java.awt.Color(214, 223, 247));
        chkIncluirIndicadores.setText(bundle.getString("KpiRelBook_00006")); // NOI18N
        chkIncluirIndicadores.setEnabled(false);
        chkIncluirIndicadores.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        pnlOpcoes.add(chkIncluirIndicadores);
        chkIncluirIndicadores.setBounds(10, 40, 120, 20);

        pnlDados.add(pnlOpcoes);
        pnlOpcoes.setBounds(450, 10, 150, 70);

        pnlVisual.setBackground(new java.awt.Color(214, 223, 247));
        pnlVisual.setLayout(null);

        chkExpandido.setBackground(new java.awt.Color(214, 223, 247));
        chkExpandido.setText("Visualiza��o expandida");
        chkExpandido.setEnabled(false);
        pnlVisual.add(chkExpandido);
        chkExpandido.setBounds(10, 10, 140, 23);

        pnlDados.add(pnlVisual);
        pnlVisual.setBounds(450, 80, 150, 40);

        tskDados.getContentPane().add(pnlDados);

        jTaskPane1.add(tskDados);

        tskEntidades.setTitle("Entidades");

        pnlEntidades.setLayout(new java.awt.BorderLayout());

        treeScorecard1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        treeScorecard1.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        treeScorecard1.setRootVisible(false);
        treeScorecard1.setRowHeight(18);
        treeScorecard1.setShowsRootHandles(true);
        treeScorecard1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                treeScorecard1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(treeScorecard1);

        pnlEntidades.add(jScrollPane2, java.awt.BorderLayout.PAGE_START);

        tskEntidades.getContentPane().add(pnlEntidades);

        jTaskPane1.add(tskEntidades);

        tbIndicadores.addTab("Book de Planejamento Estrat�gico", jTaskPane1);

        pnlFields.add(tbIndicadores, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        pnlTools.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnGeraRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_imprimir.gif"))); // NOI18N
        btnGeraRel.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00008")); // NOI18N
        btnGeraRel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnGeraRel.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnGeraRel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnGeraRel.setMaximumSize(new java.awt.Dimension(60, 22));
        btnGeraRel.setMinimumSize(new java.awt.Dimension(60, 22));
        btnGeraRel.setPreferredSize(new java.awt.Dimension(60, 22));
        btnGeraRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGeraRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGeraRel);

        btnVizualizaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        btnVizualizaRel.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00009")); // NOI18N
        btnVizualizaRel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnVizualizaRel.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnVizualizaRel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnVizualizaRel.setMaximumSize(new java.awt.Dimension(80, 22));
        btnVizualizaRel.setMinimumSize(new java.awt.Dimension(80, 22));
        btnVizualizaRel.setPreferredSize(new java.awt.Dimension(80, 22));
        btnVizualizaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVizualizaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVizualizaRel);

        btnGravaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar_rel.gif"))); // NOI18N
        btnGravaRel.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00010")); // NOI18N
        btnGravaRel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnGravaRel.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnGravaRel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnGravaRel.setMaximumSize(new java.awt.Dimension(70, 22));
        btnGravaRel.setMinimumSize(new java.awt.Dimension(70, 22));
        btnGravaRel.setPreferredSize(new java.awt.Dimension(70, 22));
        btnGravaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGravaRel);

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",  kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAbrir.setText(bundle1.getString("KpiRelatorioScoreInd_00012")); // NOI18N
        btnAbrir.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnAbrir.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAbrir.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnAbrir.setMaximumSize(new java.awt.Dimension(60, 22));
        btnAbrir.setMinimumSize(new java.awt.Dimension(60, 22));
        btnAbrir.setPreferredSize(new java.awt.Dimension(60, 22));
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAbrir);

        pnlTools.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(pnlRecord, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.LINE_END);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.LINE_START);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGeraRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeraRelActionPerformed
        event.executeRecord("SALVAR;");
   }//GEN-LAST:event_btnGeraRelActionPerformed

    private void btnVizualizaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVizualizaRelActionPerformed
        KpiToolKit toolkit = new KpiToolKit();
        KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
        StringBuilder file = new StringBuilder();

        file.append("report");
        file.append("/");
        file.append(systemProperties.recDesktop.getString("IDUSER"));
        file.append("/");
        file.append("rel055");
        file.append("_");
        file.append(this.id);
        file.append(".");
        file.append("html");

        toolkit.browser(file.toString());
    }//GEN-LAST:event_btnVizualizaRelActionPerformed

    private void btnGravaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravaRelActionPerformed
        KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
        KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
        StringBuilder file = new StringBuilder();
        StringBuilder filter = new StringBuilder();

        filter.append("report");
        filter.append("\\");
        filter.append(systemProperties.recDesktop.getString("IDUSER"));
        filter.append("\\");
        filter.append("Spool");
        filter.append("\\");
        filter.append("*.");
        filter.append("html");

        file.append("rel055");
        file.append("_");
        file.append(record.getString("ID"));
        file.append(".");
        file.append("html");

        frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
        frmChooser.setFileType("*.html");
        frmChooser.loadServerFiles(filter.toString());
        frmChooser.setFileName(file.toString());
        frmChooser.setVisible(true);

        if (frmChooser.isValidFile()) {
            event.executeRecord("SALVAR;" + (String) frmChooser.getFileName());
        }
    }//GEN-LAST:event_btnGravaRelActionPerformed

    private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
        KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
        KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
        StringBuilder path = new StringBuilder();
        StringBuilder filter = new StringBuilder();

        filter.append("report");
        filter.append("\\");
        filter.append(systemProperties.recDesktop.getString("IDUSER"));
        filter.append("\\");
        filter.append("Spool");
        filter.append("\\");
        filter.append("*.");
        filter.append("html");

        path.append("/");
        path.append("report");
        path.append("/");
        path.append(systemProperties.recDesktop.getString("IDUSER"));
        path.append("/");
        path.append("spool");
        path.append("/");

        frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
        frmChooser.setFileType("*.html");
        frmChooser.loadServerFiles(filter.toString());
        frmChooser.setVisible(true);
        frmChooser.setUrlPath(path.toString());
        frmChooser.openFile();

    }//GEN-LAST:event_btnAbrirActionPerformed

    private void treeScorecard1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_treeScorecard1MouseClicked
        if (status == KpiDefaultFrameBehavior.UPDATING) {
            int x = evt.getX();
            int y = evt.getY();
            int somaX = new JCheckBox().getPreferredSize().width;
            int row = treeScorecard1.getRowForLocation(x, y);
            TreePath path = treeScorecard1.getPathForRow(row);

            if (path != null) {
                if (x <= treeScorecard1.getPathBounds(path).x + somaX) {
                    DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) path.getLastPathComponent();
                    KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
                    boolean bSelected = !(node.isSelected());
                    node.setSelected(bSelected);

                    if (evt.getButton() == MouseEvent.BUTTON1) {
                        selectedFromRoot(nodeAux, bSelected);
                    }

                    ((DefaultTreeModel) treeScorecard1.getModel()).nodeChanged(node);

                    treeScorecard1.revalidate();
                    treeScorecard1.repaint();
                }
            }
        }
    }//GEN-LAST:event_treeScorecard1MouseClicked
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnGeraRel;
    private javax.swing.JButton btnGravaRel;
    private javax.swing.JButton btnVizualizaRel;
    private javax.swing.JCheckBox chkExpandido;
    private javax.swing.JCheckBox chkIncluirDescricao;
    private javax.swing.JCheckBox chkIncluirIndicadores;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JScrollPane jScrollPane2;
    private com.l2fprod.common.swing.JTaskPane jTaskPane1;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlDados;
    private javax.swing.JPanel pnlEntidades;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlOpcoes;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlVisual;
    private javax.swing.JScrollPane scrDescricao;
    private javax.swing.JTabbedPane tbIndicadores;
    private javax.swing.JToolBar tbInsertion;
    private javax.swing.JTree treeScorecard1;
    private com.l2fprod.common.swing.JTaskPaneGroup tskDados;
    private com.l2fprod.common.swing.JTaskPaneGroup tskEntidades;
    private kpi.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables

    @Override
    public void enableFields() {
        event.setEnableFields(pnlDados, true);
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {

        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        chkIncluirDescricao.setSelected(record.getBoolean("IMPDESC")); //NOI18N
        chkIncluirIndicadores.setSelected(record.getBoolean("IMPIND")); //NOI18N
        chkExpandido.setSelected(record.getBoolean("IMPEXP")); //NOI18N
        setListaScorecard();

    }

    @Override
    public BIXMLRecord getFieldsContents() {

        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        recordAux.set("NOME", fldNome.getText()); //NOI18N
        recordAux.set("DESCRICAO", txtDescricao.getText()); //NOI18N
        recordAux.set("IMPDESC", chkIncluirDescricao.isSelected()); //NOI18N
        recordAux.set("IMPIND", chkIncluirIndicadores.isSelected()); //NOI18N
        recordAux.set("IMPEXP", chkExpandido.isSelected()); //NOI18N
        recordAux.set(getVectorRowsSelected(scorecards, "SELECTED", "SCORECARDS", "SCORECARD")); //NOI18N

        return recordAux;

    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) { //NOI18N
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return ""; //NOI18N
        }
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void setRecord(BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    @Override
    public JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public JTabbedPane getTabbedPane() {
        return null;
    }

    private void selectedFromRoot(DefaultMutableTreeNode oNode, boolean bSelected) {

        Enumeration e = oNode.children();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) e.nextElement();
            KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
            node.setSelected(bSelected);
            selectedFromRoot(nodeAux, bSelected);
        }
    }

    public BIXMLVector getVectorRowsSelected(BIXMLVector vctAllData, String tagSelecionar, String tagVector, String tagRecord) {

        DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) treeScorecard1.getModel().getRoot();
        BIXMLVector vctSco = new BIXMLVector(tagVector, tagRecord);
        KpiSelectedTreeNode oNode;

        while (oMutTreeNode != null) {
            if (!oMutTreeNode.isRoot()) {
                oNode = (KpiSelectedTreeNode) oMutTreeNode.getUserObject();
                if (oNode.isSelected()) {
                    kpi.xml.BIXMLRecord oRec = new kpi.xml.BIXMLRecord(tagRecord);
                    oRec.set("ID", oNode.getID()); //NOI18N
                    oRec.set("SELECTED", oNode.isSelected()); //NOI18N
                    vctSco.add(oRec);
                }
            }
            oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();
        }
        return vctSco;
    }

    private void setListaScorecard() {
        scorecards = record.getBIXMLVector("SCORECARDS"); //NOI18N
        treeScorecard1.setModel(new javax.swing.tree.DefaultTreeModel(KpiSelectedTree.createTree(scorecards)));
        treeScorecard1.setCellRenderer(new KpiSelectedTreeCellRender(kpi.core.KpiStaticReferences.getKpiImageResources()));
        treeScorecard1.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    }
}
