package kpi.swing.report;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.util.ResourceBundle;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import kpi.beans.JBISelectionPanel;
import kpi.beans.JBITextArea;
import kpi.beans.JBITreeSelection;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiFileChooser;
import kpi.swing.desktop.KpiDesktopFunctions;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLVector;
import pv.jfcx.JPVEdit;

public class KpiRelEstatPlan extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "RELESTATPLAN";
    private String parentType = recordType;
    private String formType = recordType;
    private Hashtable htbScorecard = new Hashtable();

    public KpiRelEstatPlan(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
        event.editRecord();
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlEstatistica, true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlEstatistica, false);
    }

    @Override
    public void refreshFields() {
        setRecord(event.loadRecord(getID(), "RELESTATPLAN", ""));

        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        BIXMLVector vctScorecard = event.listRecords("SCORECARD", "LISTA_SCO_OWNER").getBIXMLVector("SCORECARDS");
        event.populateCombo(vctScorecard, "SCORECARD", htbScorecard, cbScorecard);
        event.selectComboItem(cbScorecard, event.getComboItem(vctScorecard, record.getString("ID_SCORE"), true));
        tsArvore.setVetor(vctScorecard);
        chkIncluirDescricao.setSelected(record.getBoolean("IMPDESC"));

        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);

        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("IMPDESC", chkIncluirDescricao.isSelected());
        recordAux.set("ID_SCORE", event.getComboValue(htbScorecard, cbScorecard));
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    public boolean hasCombos() {
        return false;
    }

    /**
     *Cria a requisi��o para ser enviada ao ADPVL.
     *return 1 - Gerar / 2 - Gravar
     */
    private String createRequest(int tipo) {
        StringBuilder parametros = new StringBuilder("");
        KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
        StringBuilder file = new StringBuilder();
        StringBuilder filter = new StringBuilder();

        filter.append("report");
        filter.append("\\");
        filter.append(systemProperties.recDesktop.getString("IDUSER"));
        filter.append("\\");
        filter.append("Spool");
        filter.append("\\");
        filter.append("*.");
        filter.append("html");

        file.append("rel054");
        file.append("_");
        file.append(record.getString("ID"));
        file.append(".");
        file.append("html");


        parametros.append("SALVAR;");

        if (tipo == 1) {
            parametros.append(file.toString());
        } else {
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
            frmChooser.setFileType("*.html");
            frmChooser.loadServerFiles(filter.toString());
            frmChooser.setFileName(file.toString());
            frmChooser.setVisible(true);
            if (frmChooser.isValidFile()) {
                parametros.append((String) frmChooser.getFileName());
            }
        }

        parametros.append("|");

        for (int i = 0; i < splIndicador.getRowCount(); i++) {

            parametros.append(splIndicador.getXMLData().get(i).getString("ID"));

            if (i != (splIndicador.getRowCount() - 1)) {
                parametros.append(",");
            }
        }

        return parametros.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btngrpEntidades = new ButtonGroup();
        tapCadastro = new JTabbedPane();
        pnlBottomForm = new JPanel();
        pnlRightForm = new JPanel();
        pnlLeftForm = new JPanel();
        pnlRecord = new JPanel();
        pnlTools = new JPanel();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnGeraRel = new JButton();
        btnVizualizaRel = new JButton();
        btnGravaRel = new JButton();
        btnAbrir = new JButton();
        pnlFields = new JPanel();
        pnlBottomRecord = new JPanel();
        pnlRightRecord = new JPanel();
        pnlLeftRecord = new JPanel();
        tblEstatistica = new JTabbedPane();
        scpRecord = new JScrollPane();
        pnlEstatistica = new JPanel();
        lblNome = new JLabel();
        fldNome = new JPVEdit();
        jScrollPane1 = new JScrollPane();
        txtDescricao = new JBITextArea();
        lblDescricao = new JLabel();
        splIndicador = new JBISelectionPanel();
        cbScorecard = new JComboBox();
        jLabel2 = new JLabel();
        tsArvore = new JBITreeSelection();
        pnlOpcoes = new JPanel();
        chkIncluirDescricao = new JCheckBox();
        lblScoreCard = new JLabel();
        pnlTopRecord = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiRelEstatPlan_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 450));
        setPreferredSize(new Dimension(500, 528));

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightForm, BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlRecord.setPreferredSize(new Dimension(432, 470));
        pnlRecord.setLayout(new BorderLayout());

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 29));
        pnlTools.setLayout(new BorderLayout());

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(310, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(225, 32));

        btnGeraRel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_imprimir.gif"))); // NOI18N
        btnGeraRel.setText(bundle.getString("KpiRelEstatPlan_00006")); // NOI18N
        btnGeraRel.setHorizontalAlignment(SwingConstants.TRAILING);
        btnGeraRel.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnGeraRel.setMaximumSize(new Dimension(60, 22));
        btnGeraRel.setMinimumSize(new Dimension(60, 22));
        btnGeraRel.setPreferredSize(new Dimension(60, 22));
        btnGeraRel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnGeraRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGeraRel);

        btnVizualizaRel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        btnVizualizaRel.setText(bundle.getString("KpiRelEstatPlan_00007")); // NOI18N
        btnVizualizaRel.setHorizontalAlignment(SwingConstants.TRAILING);
        btnVizualizaRel.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnVizualizaRel.setMaximumSize(new Dimension(70, 22));
        btnVizualizaRel.setMinimumSize(new Dimension(70, 22));
        btnVizualizaRel.setPreferredSize(new Dimension(70, 22));
        btnVizualizaRel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnVizualizaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVizualizaRel);

        btnGravaRel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar_rel.gif"))); // NOI18N
        btnGravaRel.setText(bundle.getString("KpiRelEstatPlan_00008")); // NOI18N
        btnGravaRel.setHorizontalAlignment(SwingConstants.TRAILING);
        btnGravaRel.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnGravaRel.setMaximumSize(new Dimension(70, 22));
        btnGravaRel.setMinimumSize(new Dimension(70, 22));
        btnGravaRel.setPreferredSize(new Dimension(70, 22));
        btnGravaRel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnGravaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGravaRel);

        btnAbrir.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        ResourceBundle bundle1 = ResourceBundle.getBundle("international",  KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAbrir.setText(bundle1.getString("KpiRelEstatPlan_00009")); // NOI18N
        btnAbrir.setHorizontalAlignment(SwingConstants.TRAILING);
        btnAbrir.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnAbrir.setMaximumSize(new Dimension(60, 22));
        btnAbrir.setMinimumSize(new Dimension(60, 22));
        btnAbrir.setPreferredSize(new Dimension(60, 22));
        btnAbrir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAbrir);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        pnlRecord.add(pnlTools, BorderLayout.NORTH);

        pnlFields.setPreferredSize(new Dimension(400, 350));
        pnlFields.setLayout(new BorderLayout());

        pnlBottomRecord.setPreferredSize(new Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new Dimension(1, 1));
        pnlFields.add(pnlRightRecord, BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new Dimension(1, 1));
        pnlFields.add(pnlLeftRecord, BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new Dimension(400, 400));

        pnlEstatistica.setPreferredSize(new Dimension(100, 360));
        pnlEstatistica.setLayout(null);

        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiRelEstatPlan_00002")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new Dimension(100, 16));
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlEstatistica.add(lblNome);
        lblNome.setBounds(20, 0, 98, 18);

        fldNome.setMaxLength(60);
        pnlEstatistica.add(fldNome);
        fldNome.setBounds(20, 20, 400, 22);

        txtDescricao.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        jScrollPane1.setViewportView(txtDescricao);

        pnlEstatistica.add(jScrollPane1);
        jScrollPane1.setBounds(20, 60, 400, 60);

        lblDescricao.setHorizontalAlignment(SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("KpiRelEstatPlan_00003")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new Dimension(100, 16));
        lblDescricao.setMinimumSize(new Dimension(100, 16));
        lblDescricao.setPreferredSize(new Dimension(100, 16));
        pnlEstatistica.add(lblDescricao);
        lblDescricao.setBounds(20, 40, 60, 18);

        splIndicador.setMaximumSize(new Dimension(2147483647, 150));
        splIndicador.setPreferredSize(new Dimension(257, 150));
        pnlEstatistica.add(splIndicador);
        splIndicador.setBounds(15, 180, 455, 170);

        cbScorecard.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                cbScorecardActionPerformed(evt);
            }
        });
        pnlEstatistica.add(cbScorecard);
        cbScorecard.setBounds(20, 140, 400, 22);

        ResourceBundle bundle2 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        jLabel2.setText(bundle2.getString("KpiMainPanel_00019")); // NOI18N
        jLabel2.setEnabled(false);
        pnlEstatistica.add(jLabel2);
        jLabel2.setBounds(20, 166, 120, 14);

        tsArvore.setCombo(cbScorecard);
        tsArvore.setRoot(false);
        pnlEstatistica.add(tsArvore);
        tsArvore.setBounds(420, 140, 30, 22);

        pnlOpcoes.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(204, 204, 204))));
        pnlOpcoes.setLayout(null);

        chkIncluirDescricao.setEnabled(false);
        chkIncluirDescricao.setHorizontalAlignment(SwingConstants.LEFT);
        chkIncluirDescricao.setLabel("Imprime descri��o");
        pnlOpcoes.add(chkIncluirDescricao);
        chkIncluirDescricao.setBounds(10, 10, 130, 16);

        pnlEstatistica.add(pnlOpcoes);
        pnlOpcoes.setBounds(20, 360, 400, 40);

        lblScoreCard.setHorizontalAlignment(SwingConstants.LEFT);
        lblScoreCard.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        lblScoreCard.setEnabled(false);
        lblScoreCard.setMaximumSize(new Dimension(100, 16));
        lblScoreCard.setMinimumSize(new Dimension(100, 16));
        lblScoreCard.setPreferredSize(new Dimension(100, 16));
        pnlEstatistica.add(lblScoreCard);
        lblScoreCard.setBounds(20, 120, 260, 20);

        scpRecord.setViewportView(pnlEstatistica);

        tblEstatistica.addTab(bundle2.getString("KpiRelEstatPlan_00001"), scpRecord); // NOI18N

        pnlFields.add(tblEstatistica, BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new Dimension(10, 2));
        pnlFields.add(pnlTopRecord, BorderLayout.NORTH);

        pnlRecord.add(pnlFields, BorderLayout.CENTER);

        getContentPane().add(pnlRecord, BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

	private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
            KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder path = new StringBuilder();
            StringBuilder filter = new StringBuilder();

            filter.append("report");
            filter.append("\\");
            filter.append(systemProperties.recDesktop.getString("IDUSER"));
            filter.append("\\");
            filter.append("Spool");
            filter.append("\\");
            filter.append("*.");
            filter.append("html");

            path.append("/");
            path.append("report");
            path.append("/");
            path.append(systemProperties.recDesktop.getString("IDUSER"));
            path.append("/");
            path.append("spool");
            path.append("/");

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            frmChooser.setFileType("*.html");
            frmChooser.loadServerFiles(filter.toString());
            frmChooser.setVisible(true);
            frmChooser.setUrlPath(path.toString());
            frmChooser.openFile();
	}//GEN-LAST:event_btnAbrirActionPerformed

	private void btnGravaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravaRelActionPerformed
            event.executeRecord(createRequest(2));
	}//GEN-LAST:event_btnGravaRelActionPerformed

	private void btnVizualizaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVizualizaRelActionPerformed
            KpiToolKit toolkit = new KpiToolKit();
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder file = new StringBuilder();

            file.append("report");
            file.append("/");
            file.append(systemProperties.recDesktop.getString("IDUSER"));
            file.append("/");
            file.append("rel054");
            file.append("_");
            file.append(this.id);
            file.append(".");
            file.append("html");

            toolkit.browser(file.toString());
	}//GEN-LAST:event_btnVizualizaRelActionPerformed

	private void btnGeraRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeraRelActionPerformed
            event.executeRecord(createRequest(1));
	}//GEN-LAST:event_btnGeraRelActionPerformed

        private void cbScorecardActionPerformed(ActionEvent evt) {//GEN-FIRST:event_cbScorecardActionPerformed
            StringBuilder request = new StringBuilder("ID_SCOREC = '");
            request.append(event.getComboValue(htbScorecard, cbScorecard));
            request.append("',LSTINDICADOR");
            kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", request.toString());
            vctIndicadores = recIndFiltrados.getBIXMLVector("INDICADORES");
            splIndicador.setDataSource(vctIndicadores, vctIndicadores, "INDICADORES", "INDICADOR");

            splIndicador.setTileSelection("Indicadores");
        }//GEN-LAST:event_cbScorecardActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAbrir;
    private JButton btnGeraRel;
    private JButton btnGravaRel;
    private JButton btnVizualizaRel;
    private ButtonGroup btngrpEntidades;
    private JComboBox cbScorecard;
    private JCheckBox chkIncluirDescricao;
    private JPVEdit fldNome;
    private JLabel jLabel2;
    private JScrollPane jScrollPane1;
    private JLabel lblDescricao;
    private JLabel lblNome;
    private JLabel lblScoreCard;
    private JPanel pnlBottomForm;
    private JPanel pnlBottomRecord;
    private JPanel pnlEstatistica;
    private JPanel pnlFields;
    private JPanel pnlLeftForm;
    private JPanel pnlLeftRecord;
    private JPanel pnlOpcoes;
    private JPanel pnlOperation;
    private JPanel pnlRecord;
    private JPanel pnlRightForm;
    private JPanel pnlRightRecord;
    private JPanel pnlTools;
    private JPanel pnlTopRecord;
    private JScrollPane scpRecord;
    private JBISelectionPanel splIndicador;
    private JTabbedPane tapCadastro;
    private JToolBar tbInsertion;
    private JTabbedPane tblEstatistica;
    private JBITreeSelection tsArvore;
    private JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;
    private kpi.xml.BIXMLVector vctIndicadores = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getFormType() {
        return formType;
    }
}
