/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * KpiRelInd.java
 *
 * Created on 13/10/2011, 10:21:15
 */
package kpi.swing.report;

import java.awt.event.MouseEvent;
import java.util.Enumeration;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JTabbedPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import kpi.core.KpiDateBase;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiFileChooser;
import kpi.swing.KpiSelectedTree;
import kpi.swing.KpiSelectedTreeCellRender;
import kpi.swing.KpiSelectedTreeNode;
import kpi.swing.desktop.KpiDesktopFunctions;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 *
 * @author tiago.tudisco
 */
public class KpiRelInd extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private BIXMLRecord record = null;
    private String recordType = "RELIND"; //NOI18N
    private String parentType = recordType;
    private String formType = recordType;
    private BIXMLVector scorecards = new BIXMLVector();//Todos os scorecards.
    private BIXMLVector grupo_indicadores = new BIXMLVector();//Todos os scorecards.
    private KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private String id = new String();
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private KpiDateBase dateBase = KpiStaticReferences.getKpiDateBase();

    public KpiRelInd(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true); //NOI18N
        event.defaultConstructor(operation, idAux, contextId);
        event.editRecord(); // Relatorio por default edit        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tbInsertion = new javax.swing.JToolBar();
        btnGeraRel = new javax.swing.JButton();
        btnVizualizaRel = new javax.swing.JButton();
        btnGravaRel = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        tbIndicadores = new javax.swing.JTabbedPane();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        lblDescricao1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        treeScorecard = new javax.swing.JTree();
        lblScoreDe2 = new javax.swing.JLabel();
        cbObjetivo = new javax.swing.JComboBox();
        pnlGrpIndicador = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        treeScorecard1 = new javax.swing.JTree();
        chkEntidades = new javax.swing.JCheckBox();
        chkGrpIndicador = new javax.swing.JCheckBox();
        chkIncluirDescricao = new javax.swing.JCheckBox();
        jBIDatePane1 = new kpi.beans.JBIDatePane();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiRelInd_00001")); // NOI18N

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnGeraRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_imprimir.gif"))); // NOI18N
        btnGeraRel.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00008")); // NOI18N
        btnGeraRel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnGeraRel.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnGeraRel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnGeraRel.setMaximumSize(new java.awt.Dimension(60, 22));
        btnGeraRel.setMinimumSize(new java.awt.Dimension(60, 22));
        btnGeraRel.setPreferredSize(new java.awt.Dimension(60, 22));
        btnGeraRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGeraRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGeraRel);

        btnVizualizaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        btnVizualizaRel.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00009")); // NOI18N
        btnVizualizaRel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnVizualizaRel.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnVizualizaRel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnVizualizaRel.setMaximumSize(new java.awt.Dimension(80, 22));
        btnVizualizaRel.setMinimumSize(new java.awt.Dimension(80, 22));
        btnVizualizaRel.setPreferredSize(new java.awt.Dimension(80, 22));
        btnVizualizaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVizualizaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVizualizaRel);

        btnGravaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar_rel.gif"))); // NOI18N
        btnGravaRel.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00010")); // NOI18N
        btnGravaRel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnGravaRel.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnGravaRel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnGravaRel.setMaximumSize(new java.awt.Dimension(70, 22));
        btnGravaRel.setMinimumSize(new java.awt.Dimension(70, 22));
        btnGravaRel.setPreferredSize(new java.awt.Dimension(70, 22));
        btnGravaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGravaRel);

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",  kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAbrir.setText(bundle1.getString("KpiRelatorioScoreInd_00012")); // NOI18N
        btnAbrir.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnAbrir.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAbrir.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnAbrir.setMaximumSize(new java.awt.Dimension(60, 22));
        btnAbrir.setMinimumSize(new java.awt.Dimension(60, 22));
        btnAbrir.setPreferredSize(new java.awt.Dimension(60, 22));
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAbrir);

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(420, 360));

        pnlData.setAutoscrolls(true);
        pnlData.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        pnlData.setPreferredSize(new java.awt.Dimension(460, 720));
        pnlData.setLayout(null);

        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiRelInd_00002")); // NOI18N
        lblNome.setAlignmentX(0.5F);
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(20, 0, 40, 20);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 20, 400, 22);

        txtDescricao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jScrollPane1.setViewportView(txtDescricao);

        pnlData.add(jScrollPane1);
        jScrollPane1.setBounds(20, 60, 400, 50);

        lblDescricao1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao1.setText(bundle.getString("KpiRelInd_00003")); // NOI18N
        lblDescricao1.setAlignmentX(0.5F);
        lblDescricao1.setEnabled(false);
        lblDescricao1.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao1.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao1.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDescricao1);
        lblDescricao1.setBounds(20, 40, 60, 18);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel1.setForeground(new java.awt.Color(204, 204, 204));

        treeScorecard.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        treeScorecard.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        treeScorecard.setRootVisible(false);
        treeScorecard.setRowHeight(18);
        treeScorecard.setShowsRootHandles(true);
        treeScorecard.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                treeScorecardMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(treeScorecard);

        lblScoreDe2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblScoreDe2.setText(bundle.getString("KpiRelInd_00006")); // NOI18N
        lblScoreDe2.setEnabled(false);
        lblScoreDe2.setMaximumSize(new java.awt.Dimension(100, 16));
        lblScoreDe2.setMinimumSize(new java.awt.Dimension(100, 16));
        lblScoreDe2.setPreferredSize(new java.awt.Dimension(100, 16));

        cbObjetivo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Atingido", "N�o Atingido", "Dentro da Toler�ncia", "Superou", "Todos" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbObjetivo, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblScoreDe2, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblScoreDe2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(cbObjetivo, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 205, Short.MAX_VALUE))))
        );

        pnlData.add(jPanel1);
        jPanel1.setBounds(20, 190, 400, 260);

        pnlGrpIndicador.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));

        treeScorecard1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        treeScorecard1.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        treeScorecard1.setRootVisible(false);
        treeScorecard1.setRowHeight(18);
        treeScorecard1.setShowsRootHandles(true);
        treeScorecard1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                treeScorecard1MouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(treeScorecard1);

        javax.swing.GroupLayout pnlGrpIndicadorLayout = new javax.swing.GroupLayout(pnlGrpIndicador);
        pnlGrpIndicador.setLayout(pnlGrpIndicadorLayout);
        pnlGrpIndicadorLayout.setHorizontalGroup(
            pnlGrpIndicadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGrpIndicadorLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(158, Short.MAX_VALUE))
        );
        pnlGrpIndicadorLayout.setVerticalGroup(
            pnlGrpIndicadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlGrpIndicadorLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65))
        );

        pnlData.add(pnlGrpIndicador);
        pnlGrpIndicador.setBounds(20, 490, 400, 260);

        chkEntidades.setSelected(true);
        chkEntidades.setText(bundle.getString ("KpiRelInd_00004")); // NOI18N
        chkEntidades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkEntidadesActionPerformed(evt);
            }
        });
        pnlData.add(chkEntidades);
        chkEntidades.setBounds(20, 160, 77, 23);

        chkGrpIndicador.setText("Grupo de Indicadores:");
        chkGrpIndicador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkGrpIndicadorActionPerformed(evt);
            }
        });
        pnlData.add(chkGrpIndicador);
        chkGrpIndicador.setBounds(20, 460, 170, 23);

        chkIncluirDescricao.setText(bundle.getString("KpiRelInd_00007")); // NOI18N
        chkIncluirDescricao.setEnabled(false);
        chkIncluirDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        chkIncluirDescricao.setLabel(bundle.getString("KpiRelInd_00007")); // NOI18N
        pnlData.add(chkIncluirDescricao);
        chkIncluirDescricao.setBounds(300, 160, 120, 20);
        pnlData.add(jBIDatePane1);
        jBIDatePane1.setBounds(20, 110, 400, 40);

        scpRecord.setViewportView(pnlData);

        tbIndicadores.addTab(bundle.getString("KpiRelInd_00001"), scpRecord); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tbInsertion, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(174, 174, 174))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tbIndicadores)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tbInsertion, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(tbIndicadores, javax.swing.GroupLayout.DEFAULT_SIZE, 814, Short.MAX_VALUE)
                .addContainerGap())
        );

        tbIndicadores.getAccessibleContext().setAccessibleName(bundle.getString("KpiRelInd_00001")); // NOI18N

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void treeScorecardMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_treeScorecardMouseClicked

        if (status == KpiDefaultFrameBehavior.UPDATING) {

            int x = evt.getX();
            int y = evt.getY();
            int somaX = new JCheckBox().getPreferredSize().width;

            int row = treeScorecard.getRowForLocation(x, y);
            TreePath path = treeScorecard.getPathForRow(row);

            if (path != null) {
                if (x <= treeScorecard.getPathBounds(path).x + somaX) {
                    DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) path.getLastPathComponent();
                    KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
                    boolean bSelected = !(node.isSelected());
                    node.setSelected(bSelected);

                    if (evt.getButton() == MouseEvent.BUTTON1) {
                        selectedFromRoot(nodeAux, bSelected);
                    }

                    ((DefaultTreeModel) treeScorecard.getModel()).nodeChanged(node);

                    treeScorecard.revalidate();
                    treeScorecard.repaint();
                }
            }
        }
    }//GEN-LAST:event_treeScorecardMouseClicked

    private void btnGravaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravaRelActionPerformed
        KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
        KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
        StringBuilder file = new StringBuilder();
        StringBuilder filter = new StringBuilder();

        filter.append("report");
        filter.append("\\");
        filter.append(systemProperties.recDesktop.getString("IDUSER"));
        filter.append("\\");
        filter.append("Spool");
        filter.append("\\");
        filter.append("*.");
        filter.append("html");

        file.append("rel056");
        file.append("_");
        file.append(record.getString("ID"));
        file.append(".");
        file.append("html");

        frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
        frmChooser.setFileType("*.html");
        frmChooser.loadServerFiles(filter.toString());
        frmChooser.setFileName(file.toString());
        frmChooser.setVisible(true);

        if (frmChooser.isValidFile()) {
            event.executeRecord("SALVAR;" + (String) frmChooser.getFileName());
        }
    }//GEN-LAST:event_btnGravaRelActionPerformed

    private void btnVizualizaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVizualizaRelActionPerformed
        KpiToolKit toolkit = new KpiToolKit();
        KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
        StringBuilder file = new StringBuilder();

        file.append("report");
        file.append("/");
        file.append(systemProperties.recDesktop.getString("IDUSER"));
        file.append("/");
        file.append("rel056");
        file.append("_");
        file.append(this.id);
        file.append(".");
        file.append("html");

        toolkit.browser(file.toString());
    }//GEN-LAST:event_btnVizualizaRelActionPerformed

    private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
        KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
        KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
        StringBuilder path = new StringBuilder();
        StringBuilder filter = new StringBuilder();

        filter.append("report");
        filter.append("\\");
        filter.append(systemProperties.recDesktop.getString("IDUSER"));
        filter.append("\\");
        filter.append("Spool");
        filter.append("\\");
        filter.append("*.");
        filter.append("html");

        path.append("/");
        path.append("report");
        path.append("/");
        path.append(systemProperties.recDesktop.getString("IDUSER"));
        path.append("/");
        path.append("spool");
        path.append("/");

        frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
        frmChooser.setFileType("*.html");
        frmChooser.loadServerFiles(filter.toString());
        frmChooser.setVisible(true);
        frmChooser.setUrlPath(path.toString());
        frmChooser.openFile();

    }//GEN-LAST:event_btnAbrirActionPerformed

    private void btnGeraRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeraRelActionPerformed
        event.executeRecord("SALVAR;");
    }//GEN-LAST:event_btnGeraRelActionPerformed

    private void treeScorecard1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_treeScorecard1MouseClicked
        
        if (status == KpiDefaultFrameBehavior.UPDATING) {

            int x = evt.getX();
            int y = evt.getY();
            int somaX = new JCheckBox().getPreferredSize().width;

            int row = treeScorecard1.getRowForLocation(x, y);
            TreePath path = treeScorecard1.getPathForRow(row);

            if (path != null) {
                if (x <= treeScorecard1.getPathBounds(path).x + somaX) {
                    DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) path.getLastPathComponent();
                    KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
                    boolean bSelected = !(node.isSelected());
                    node.setSelected(bSelected);

                    if (evt.getButton() == MouseEvent.BUTTON1) {
                        selectedFromRoot(nodeAux, bSelected);
                    }

                    ((DefaultTreeModel) treeScorecard1.getModel()).nodeChanged(node);

                    treeScorecard1.revalidate();
                    treeScorecard1.repaint();
                }
            }
        }        
    }//GEN-LAST:event_treeScorecard1MouseClicked

    private void chkEntidadesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkEntidadesActionPerformed
        ControlPivot(chkEntidades.getText());
    }//GEN-LAST:event_chkEntidadesActionPerformed

    private void chkGrpIndicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkGrpIndicadorActionPerformed
        ControlPivot(chkGrpIndicador.getText());
    }//GEN-LAST:event_chkGrpIndicadorActionPerformed

    /**
     * Este m�todo � respons�vel por controlar os checkbox do relat�rio de indicadores
     * quando um estiver selecionado o outro n�o poder� estar e suas op��es ou ficam habilitadas
     * ou desabilitadas
     * @param nomePivot String que recebe o nome do pivot clicado.
     * @Since 22/04/2014
     * @Author Helio Leal
     */
    private void ControlPivot(String nomePivot) {
        if (nomePivot.equalsIgnoreCase("Entidades:")) {
            // Ao menos um checkBox Tem que estar selecionado.
            if (!chkEntidades.isSelected()) {
                chkEntidades.setSelected(true);
            }
            
            setEnableEntidades(true);
            setEnableGrpIndicadores(false);
        } else {
            // Ao menos um checkBox Tem que estar selecionado.
            if (!chkGrpIndicador.isSelected()) {
                chkGrpIndicador.setSelected(true);
            }

            setEnableEntidades(false);
            setEnableGrpIndicadores(true);
        }
    }

    /**
     * M�todo que habilita ou desabilita as entidades.
     * @param Enable Booleano que habilita ou n�o as op��es das Entidades.
     * @Since 22/04/2014
     * @Author Helio Leal
     */
    private void setEnableEntidades(boolean Enable) {
        treeScorecard.setEnabled(Enable);
        lblScoreDe2.setEnabled(Enable);
        cbObjetivo.setEnabled(Enable);
        chkEntidades.setSelected(Enable);
    }
            
    /**
     * M�todo que habilita ou desabilita os grupos de indicadores.
     * @param Enable Booleano que habilita ou n�o as op��es dos grupos de indicadores.
     * @Since 22/04/2014
     * @Author Helio Leal
     */
    private void setEnableGrpIndicadores(boolean Enable) {
        treeScorecard1.setEnabled(Enable);
        chkGrpIndicador.setSelected(Enable);
    }
    
    private void selectedFromRoot(DefaultMutableTreeNode oNode, boolean bSelected) {
        Enumeration e = oNode.children();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) e.nextElement();
            KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
            node.setSelected(bSelected);
            selectedFromRoot(nodeAux, bSelected);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnGeraRel;
    private javax.swing.JButton btnGravaRel;
    private javax.swing.JButton btnVizualizaRel;
    private javax.swing.JComboBox cbObjetivo;
    private javax.swing.JCheckBox chkEntidades;
    private javax.swing.JCheckBox chkGrpIndicador;
    private javax.swing.JCheckBox chkIncluirDescricao;
    private pv.jfcx.JPVEdit fldNome;
    private kpi.beans.JBIDatePane jBIDatePane1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblDescricao1;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblScoreDe2;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlGrpIndicador;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tbIndicadores;
    private javax.swing.JToolBar tbInsertion;
    private javax.swing.JTree treeScorecard;
    private javax.swing.JTree treeScorecard1;
    private kpi.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables

    @Override
    public void enableFields() {
        event.setEnableFields(pnlData, true);
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        chkIncluirDescricao.setSelected(record.getBoolean("IMPDESC"));
        jBIDatePane1.fldDataAlvo.setCalendar(record.getDate("DTALVO"));
        cbObjetivo.setSelectedIndex(record.getInt("SITUACAO"));
        
        // Verifica se a Data de est� preenchida.
        if (record.getDate("DTDE") == null)
            jBIDatePane1.fldDataDe.setCalendar(dateBase.getDataAcumuladoDe());
        else
            jBIDatePane1.fldDataDe.setCalendar(record.getDate("DTDE"));
        
        // Verifica se a Data at� est� preenchida.
        if (record.getDate("DTATE") == null)
            jBIDatePane1.fldDataAte.setCalendar(dateBase.getDataAcumuladoAte());
        else
            jBIDatePane1.fldDataAte.setCalendar(record.getDate("DTATE"));        

        // Seleciona o checkbox.
        if (record.getInt("TIPOREL") == 0) {
            setEnableEntidades(true);
            setEnableGrpIndicadores(false);
        }
        else {
            setEnableEntidades(false);
            setEnableGrpIndicadores(true);
        }

        setListaScorecard();
    }

    @Override
    public BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("IMPDESC", chkIncluirDescricao.isSelected());
        recordAux.set("DTALVO", jBIDatePane1.fldDataAlvo.getCalendar());
        recordAux.set("DTDE", jBIDatePane1.fldDataDe.getCalendar());
        recordAux.set("DTATE", jBIDatePane1.fldDataAte.getCalendar());
        recordAux.set("SITUACAO", cbObjetivo.getSelectedIndex());

        if (chkEntidades.isSelected()) {
            // Por entidades.
            recordAux.set(getVectorRowsSelected(scorecards, "SELECTED", "SCORECARDS", "SCORECARD", treeScorecard));
            recordAux.set("TIPOREL", 0); // 0 - Entidades.
        } else {
            // Por Grupo de indicadores.
            recordAux.set(getVectorRowsSelected(grupo_indicadores, "SELECTED", "GRUPO_INDS", "GRUPO_IND", treeScorecard1));
            recordAux.set("TIPOREL", 1); // 1 - Grupo de indicadores.
        }

        return recordAux;
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) { //NOI18N
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return ""; //NOI18N
        }
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void setRecord(BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    @Override
    public JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public JTabbedPane getTabbedPane() {
        return null;
    }

    /**
     * M�todo que monta a �rvore de scorecards e de grupo de indicadores.
     */
    private void setListaScorecard() {
        // Recebe os scorecards e monta a �rvore.
        scorecards = record.getBIXMLVector("SCORECARDS");
        treeScorecard.setModel(new javax.swing.tree.DefaultTreeModel(KpiSelectedTree.createTree(scorecards)));
        treeScorecard.setCellRenderer(new KpiSelectedTreeCellRender(kpi.core.KpiStaticReferences.getKpiImageResources()));
        treeScorecard.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        // Recebe os grupos de indicadores e monta a �rvore.
        grupo_indicadores = record.getBIXMLVector("GRUPO_INDS");  
        treeScorecard1.setModel(new javax.swing.tree.DefaultTreeModel(KpiSelectedTree.createTree(grupo_indicadores)));
        treeScorecard1.setCellRenderer(new KpiSelectedTreeCellRender(kpi.core.KpiStaticReferences.getKpiImageResources()));
        treeScorecard1.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    /**
     * Retorna todas as linhas que foram selecionadas no vetor.
     * @param vector Vetor com os todos os dados
     * @param String Nome da tag que identifica a coluna de sele��o.
     * @return <B>BIXMLVecotr</B> Com todos os itens que foram selecionados.
     */
    public BIXMLVector getVectorRowsSelected(BIXMLVector vctAllData, String tagSelecionar, String tagVector, String tagRecord, javax.swing.JTree tree) {
        
        DefaultMutableTreeNode oMutTreeNode = null;
        
        if (tree == null) {
            oMutTreeNode = (DefaultMutableTreeNode) treeScorecard.getModel().getRoot();
        } else {
            oMutTreeNode = (DefaultMutableTreeNode) tree.getModel().getRoot();
        }
        
        BIXMLVector vctSco = new BIXMLVector(tagVector, tagRecord);
        KpiSelectedTreeNode oNode;

        while (oMutTreeNode != null) {
            if (!oMutTreeNode.isRoot()) {
                oNode = (KpiSelectedTreeNode) oMutTreeNode.getUserObject();
                if (oNode.isSelected()) {
                    kpi.xml.BIXMLRecord oRec = new kpi.xml.BIXMLRecord(tagRecord);
                    oRec.set("ID", oNode.getID()); //NOI18N
                    oRec.set("SELECTED", oNode.isSelected()); //NOI18N
                    vctSco.add(oRec);
                }
            }
            oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();
        }
        return vctSco;
    }
}
