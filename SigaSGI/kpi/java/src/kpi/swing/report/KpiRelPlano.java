package kpi.swing.report;

import java.util.Hashtable;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiFileChooser;
import kpi.swing.desktop.KpiDesktopFunctions;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiRelPlano extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "RELPLAN";
    private String parentType = recordType;
    private String formType = recordType;
    private boolean isAdmin = false;
    private Hashtable htbUserDe = new Hashtable();
    private Hashtable htbUserAte = new Hashtable();
    private Hashtable htbProjetoDe = new Hashtable();
    private Hashtable htbProjetoAte = new Hashtable();
    private Hashtable htbIndDe = new Hashtable();
    private Hashtable htbIndAte = new Hashtable();
    private Hashtable htbScorecard = new Hashtable();
    private Hashtable htbStatus = new Hashtable();
    private Hashtable htbTipoImp = new Hashtable();
    private Hashtable htbSituacao = new Hashtable();

    @Override
    public void enableFields() {
        event.setEnableFields(pnlData, true);
        verificaPlanoAcao();
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        isAdmin = record.getBoolean("ISADMIN");
        //Nome.
        fldNome.setText(record.getString("NOME"));
        //Imprimir.
        event.populateCombo(record.getBIXMLVector("TIPOIMPS"), "TIPOIMP", htbTipoImp, cbTipo);
        cbTipo.removeItemAt(0);
        //Descri��o.
        txtDescricao.setText(record.getString("DESCRICAO"));
        //Scorecard.
        event.populateCombo(record.getBIXMLVector("SCORECARDS"), "SCORECARD", htbScorecard, cbScorecard);
        event.selectComboItem(cbScorecard, event.getComboItem(record.getBIXMLVector("SCORECARDS"), record.getString("SCORECARD"), true));
        tsArvore.setVetor(record.getBIXMLVector("SCORECARDS"));
        //Projeto De:
        event.populateCombo(record.getBIXMLVector("PROJETOS"), "PROJ_DE", htbProjetoDe, cbProjetoDe);
        //Projeto At�:
        event.populateCombo(record.getBIXMLVector("PROJETOS"), "PROJ_ATE", htbProjetoAte, cbProjetoAte);
        //Indicadores De e At�:
        if (cbTipo.getSelectedIndex() == 0 || cbTipo.getSelectedIndex() == 2) {
            BIXMLRecord recIndicadores = event.listRecords("INDICADOR", "ID_SCOREC = '".concat(record.getString("SCORECARD")).concat("'"));
            BIXMLVector vctIndicadores = recIndicadores.getBIXMLVector("INDICADORES");
            event.selectComboItem(cbIndicadorDe, event.getComboItem(vctIndicadores, record.getString("INDIC_DE"), true));
            event.selectComboItem(cbIndicadorAte, event.getComboItem(vctIndicadores, record.getString("INDIC_ATE"), true));
        }

        //Seleciona apenas objetivo.
        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
        }

        //Situa��o.
        event.populateCombo(record.getBIXMLVector("LSTSITUACAO"), "SITUACAO", htbSituacao, cbSituacao);
        //Status.
        event.populateCombo(record.getBIXMLVector("LSTSTATUS"), "STATUS", htbStatus, cbStatus);
        //Categoria.
        cmbCategoria.setSelectedIndex(record.getInt("CATEGORIA"));

        //Recupera a lista de usu�rios.
        BIXMLVector vctUsuarios = record.getBIXMLVector("RESPONSAVEIS");
        BIXMLVector vctTreeUser = record.getBIXMLVector("TREEUSERS");

        //Responsavel pela acao
        event.populateCombo(vctUsuarios, "TP_RESP_ID_RESP", htbUserDe, cbUsuarioDe);
        event.selectComboItem(cbUsuarioDe, event.getComboItem(vctUsuarios, record.getString("TP_RESP_ID_RESP"), true));
        tsTreeResp.setVetor(vctUsuarios);
        tsTreeResp.setTree(vctTreeUser);

        //T�rmino De:
        fldTerminoDe.setCalendar(record.getDate("TERMINODE"));
        //T�rmino At�:
        fldTerminoAte.setCalendar(record.getDate("TERMINOATE"));
        //Acumulado De:
        fldInicioDe.setCalendar(record.getDate("INICIODE"));
        //Acumulado At�:
        fldInicioAte.setCalendar(record.getDate("INICIOATE"));
        //Imprimir Descri��o. 
        chkIncluirDescricao.setSelected(record.getBoolean("IMPDESC"));

        verificaPlanoAcao();

    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("IMPDESC", chkIncluirDescricao.isSelected());
        recordAux.set("INICIODE", fldInicioDe.getCalendar());
        recordAux.set("INICIOATE", fldInicioAte.getCalendar());
        recordAux.set("TERMINODE", fldTerminoDe.getCalendar());
        recordAux.set("TERMINOATE", fldTerminoAte.getCalendar());
        if (event.getComboValue(htbUserDe, cbUsuarioDe).substring(0, 1).equals("0")) {
            recordAux.set("TP_RESP", "");
            recordAux.set("ID_RESP", "");
        } else {
            recordAux.set("TP_RESP", event.getComboValue(htbUserDe, cbUsuarioDe).substring(0, 1));
            recordAux.set("ID_RESP", event.getComboValue(htbUserDe, cbUsuarioDe).substring(1));
        }

        recordAux.set("PROJ_DE", event.getComboValue(htbProjetoDe, cbProjetoDe));
        recordAux.set("PROJ_ATE", event.getComboValue(htbProjetoAte, cbProjetoAte));
        recordAux.set("SCORECARD", event.getComboValue(htbScorecard, cbScorecard, false));
        recordAux.set("INDIC_DE", event.getComboValue(htbIndDe, cbIndicadorDe));
        recordAux.set("INDIC_ATE", event.getComboValue(htbIndAte, cbIndicadorAte));
        recordAux.set("STATUS", event.getComboValue(htbStatus, cbStatus));
        recordAux.set("TIPOIMP", event.getComboValue(htbTipoImp, cbTipo, true));
        recordAux.set("SITUACAO", event.getComboValue(htbSituacao, cbSituacao));
        recordAux.set("CATEGORIA", cmbCategoria.getSelectedIndex());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (cbScorecard.getSelectedIndex() <= 0 && !isAdmin) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00004"));
            retorno = false;
        }

        return retorno;
    }

    public boolean hasCombos() {
        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btngrpEntidades = new javax.swing.ButtonGroup();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnGeraRel = new javax.swing.JButton();
        btnVizualizaRel = new javax.swing.JButton();
        btnGravaRel = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        tblAcoes = new javax.swing.JTabbedPane();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        lblTipo = new javax.swing.JLabel();
        cbTipo = new javax.swing.JComboBox();
        lblDescricao = new javax.swing.JLabel();
        fldInicioDe = new pv.jfcx.JPVDatePlus();
        fldInicioAte = new pv.jfcx.JPVDatePlus();
        lblInicioDe = new javax.swing.JLabel();
        lblInicioAte = new javax.swing.JLabel();
        lblTerminoDe = new javax.swing.JLabel();
        fldTerminoDe = new pv.jfcx.JPVDatePlus();
        lblTerminoAte = new javax.swing.JLabel();
        fldTerminoAte = new pv.jfcx.JPVDatePlus();
        cbScorecard = new javax.swing.JComboBox();
        lblProjetoDe = new javax.swing.JLabel();
        cbProjetoDe = new javax.swing.JComboBox();
        lblProjetoAte = new javax.swing.JLabel();
        cbProjetoAte = new javax.swing.JComboBox();
        lblIndicadorDe = new javax.swing.JLabel();
        cbIndicadorDe = new javax.swing.JComboBox();
        cbIndicadorAte = new javax.swing.JComboBox();
        lblIndicadorAte = new javax.swing.JLabel();
        lblUsuarioDe = new javax.swing.JLabel();
        cbUsuarioDe = new javax.swing.JComboBox();
        lblSituacao = new javax.swing.JLabel();
        cbSituacao = new javax.swing.JComboBox();
        cbStatus = new javax.swing.JComboBox();
        lblStatus = new javax.swing.JLabel();
        cmbCategoria = new javax.swing.JComboBox();
        lblCategoria = new javax.swing.JLabel();
        tsArvore = new kpi.beans.JBITreeSelection();
        pnlOpcoes = new javax.swing.JPanel();
        chkIncluirDescricao = new javax.swing.JCheckBox();
        lblScoreCard = new javax.swing.JLabel();
        tsTreeResp = new kpi.beans.JBITreeSelection(kpi.beans.JBITreeSelection.TYPE_DEFAULT);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiRelatorioPlanoAcao_00022")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 450));
        setPreferredSize(new java.awt.Dimension(497, 732));

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnGeraRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_imprimir.gif"))); // NOI18N
        btnGeraRel.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00016")); // NOI18N
        btnGeraRel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnGeraRel.setMaximumSize(new java.awt.Dimension(60, 22));
        btnGeraRel.setMinimumSize(new java.awt.Dimension(60, 22));
        btnGeraRel.setPreferredSize(new java.awt.Dimension(60, 22));
        btnGeraRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGeraRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGeraRel);

        btnVizualizaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        btnVizualizaRel.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00017")); // NOI18N
        btnVizualizaRel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnVizualizaRel.setMaximumSize(new java.awt.Dimension(80, 22));
        btnVizualizaRel.setMinimumSize(new java.awt.Dimension(80, 22));
        btnVizualizaRel.setPreferredSize(new java.awt.Dimension(80, 22));
        btnVizualizaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVizualizaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVizualizaRel);

        btnGravaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar_rel.gif"))); // NOI18N
        btnGravaRel.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00018")); // NOI18N
        btnGravaRel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnGravaRel.setMaximumSize(new java.awt.Dimension(70, 22));
        btnGravaRel.setMinimumSize(new java.awt.Dimension(70, 22));
        btnGravaRel.setPreferredSize(new java.awt.Dimension(70, 22));
        btnGravaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGravaRel);

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",  kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAbrir.setText(bundle1.getString("KpiRelatorioPlanoAcao_00021")); // NOI18N
        btnAbrir.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnAbrir.setMaximumSize(new java.awt.Dimension(60, 22));
        btnAbrir.setMinimumSize(new java.awt.Dimension(60, 22));
        btnAbrir.setPreferredSize(new java.awt.Dimension(60, 22));
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAbrir);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setPreferredSize(new java.awt.Dimension(484, 470));
        pnlFields.setLayout(new java.awt.BorderLayout());

        scpRecord.setBorder(null);
        scpRecord.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scpRecord.setPreferredSize(new java.awt.Dimension(464, 366));

        pnlData.setAutoscrolls(true);
        pnlData.setPreferredSize(new java.awt.Dimension(460, 620));
        pnlData.setLayout(null);

        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00001")); // NOI18N
        lblNome.setAlignmentX(0.5F);
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 0, 95, 20);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 20, 400, 22);

        txtDescricao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jScrollPane1.setViewportView(txtDescricao);

        pnlData.add(jScrollPane1);
        jScrollPane1.setBounds(20, 100, 400, 60);

        lblTipo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTipo.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00003")); // NOI18N
        lblTipo.setAlignmentX(0.5F);
        lblTipo.setEnabled(false);
        lblTipo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblTipo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblTipo.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblTipo);
        lblTipo.setBounds(10, 40, 95, 20);

        cbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbTipoActionPerformed(evt);
            }
        });
        pnlData.add(cbTipo);
        cbTipo.setBounds(20, 60, 400, 22);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00002")); // NOI18N
        lblDescricao.setAlignmentX(0.5F);
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 80, 88, 20);

        fldInicioDe.setDialog(true);
        fldInicioDe.setUseLocale(true);
        fldInicioDe.setWaitForCalendarDate(true);
        pnlData.add(fldInicioDe);
        fldInicioDe.setBounds(20, 500, 180, 22);

        fldInicioAte.setDialog(true);
        fldInicioAte.setUseLocale(true);
        fldInicioAte.setWaitForCalendarDate(true);
        pnlData.add(fldInicioAte);
        fldInicioAte.setBounds(240, 500, 180, 22);

        lblInicioDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblInicioDe.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00012")); // NOI18N
        lblInicioDe.setAlignmentX(0.5F);
        lblInicioDe.setEnabled(false);
        lblInicioDe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblInicioDe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblInicioDe.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblInicioDe);
        lblInicioDe.setBounds(10, 480, 95, 20);

        lblInicioAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblInicioAte.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00013")); // NOI18N
        lblInicioAte.setAlignmentX(0.5F);
        lblInicioAte.setEnabled(false);
        lblInicioAte.setMaximumSize(new java.awt.Dimension(100, 16));
        lblInicioAte.setMinimumSize(new java.awt.Dimension(100, 16));
        lblInicioAte.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblInicioAte);
        lblInicioAte.setBounds(230, 480, 88, 20);

        lblTerminoDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTerminoDe.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00014")); // NOI18N
        lblTerminoDe.setAlignmentX(0.5F);
        lblTerminoDe.setEnabled(false);
        lblTerminoDe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblTerminoDe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblTerminoDe.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblTerminoDe);
        lblTerminoDe.setBounds(10, 440, 95, 20);

        fldTerminoDe.setDialog(true);
        fldTerminoDe.setUseLocale(true);
        fldTerminoDe.setWaitForCalendarDate(true);
        pnlData.add(fldTerminoDe);
        fldTerminoDe.setBounds(20, 460, 180, 22);

        lblTerminoAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTerminoAte.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00013")); // NOI18N
        lblTerminoAte.setAlignmentX(0.5F);
        lblTerminoAte.setEnabled(false);
        lblTerminoAte.setMaximumSize(new java.awt.Dimension(100, 16));
        lblTerminoAte.setMinimumSize(new java.awt.Dimension(100, 16));
        lblTerminoAte.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblTerminoAte);
        lblTerminoAte.setBounds(230, 440, 88, 20);

        fldTerminoAte.setDialog(true);
        fldTerminoAte.setUseLocale(true);
        fldTerminoAte.setWaitForCalendarDate(true);
        pnlData.add(fldTerminoAte);
        fldTerminoAte.setBounds(240, 460, 180, 22);

        cbScorecard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbScorecardActionPerformed(evt);
            }
        });
        pnlData.add(cbScorecard);
        cbScorecard.setBounds(20, 180, 400, 23);

        lblProjetoDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblProjetoDe.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00005")); // NOI18N
        lblProjetoDe.setAlignmentX(0.5F);
        lblProjetoDe.setEnabled(false);
        lblProjetoDe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblProjetoDe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblProjetoDe.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblProjetoDe);
        lblProjetoDe.setBounds(10, 200, 95, 20);
        pnlData.add(cbProjetoDe);
        cbProjetoDe.setBounds(20, 220, 180, 23);

        lblProjetoAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblProjetoAte.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00006")); // NOI18N
        lblProjetoAte.setAlignmentX(0.5F);
        lblProjetoAte.setEnabled(false);
        lblProjetoAte.setMaximumSize(new java.awt.Dimension(100, 16));
        lblProjetoAte.setMinimumSize(new java.awt.Dimension(100, 16));
        lblProjetoAte.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblProjetoAte);
        lblProjetoAte.setBounds(230, 200, 95, 20);
        pnlData.add(cbProjetoAte);
        cbProjetoAte.setBounds(240, 220, 180, 23);

        lblIndicadorDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblIndicadorDe.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00008")); // NOI18N
        lblIndicadorDe.setAlignmentX(0.5F);
        lblIndicadorDe.setEnabled(false);
        lblIndicadorDe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblIndicadorDe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblIndicadorDe.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblIndicadorDe);
        lblIndicadorDe.setBounds(10, 240, 88, 20);
        pnlData.add(cbIndicadorDe);
        cbIndicadorDe.setBounds(20, 260, 180, 23);
        pnlData.add(cbIndicadorAte);
        cbIndicadorAte.setBounds(240, 260, 180, 23);

        lblIndicadorAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblIndicadorAte.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00009")); // NOI18N
        lblIndicadorAte.setAlignmentX(0.5F);
        lblIndicadorAte.setEnabled(false);
        lblIndicadorAte.setMaximumSize(new java.awt.Dimension(100, 16));
        lblIndicadorAte.setMinimumSize(new java.awt.Dimension(100, 16));
        lblIndicadorAte.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblIndicadorAte);
        lblIndicadorAte.setBounds(230, 240, 88, 20);

        lblUsuarioDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUsuarioDe.setText("Usu�rio:");
        lblUsuarioDe.setAlignmentX(0.5F);
        lblUsuarioDe.setEnabled(false);
        lblUsuarioDe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblUsuarioDe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblUsuarioDe.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblUsuarioDe);
        lblUsuarioDe.setBounds(20, 380, 95, 20);
        pnlData.add(cbUsuarioDe);
        cbUsuarioDe.setBounds(20, 400, 400, 23);

        lblSituacao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSituacao.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00015")); // NOI18N
        lblSituacao.setAlignmentX(0.5F);
        lblSituacao.setEnabled(false);
        lblSituacao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblSituacao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblSituacao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblSituacao);
        lblSituacao.setBounds(10, 290, 88, 20);

        pnlData.add(cbSituacao);
        cbSituacao.setBounds(20, 310, 180, 23);
        pnlData.add(cbStatus);
        cbStatus.setBounds(240, 310, 180, 23);

        lblStatus.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblStatus.setText("   "+java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioPlanoAcao_00020")); // NOI18N
        lblStatus.setAlignmentX(0.5F);
        lblStatus.setEnabled(false);
        lblStatus.setMaximumSize(new java.awt.Dimension(100, 16));
        lblStatus.setMinimumSize(new java.awt.Dimension(100, 16));
        lblStatus.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblStatus);
        lblStatus.setBounds(230, 290, 88, 20);

        cmbCategoria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "[Todos]", "Corretiva", "Preventiva" }));
        pnlData.add(cmbCategoria);
        cmbCategoria.setBounds(20, 350, 180, 22);

        lblCategoria.setText("Tipo:");
        lblCategoria.setEnabled(false);
        pnlData.add(lblCategoria);
        lblCategoria.setBounds(20, 330, 24, 20);

        tsArvore.setCombo(cbScorecard);
        tsArvore.setRoot(false);
        pnlData.add(tsArvore);
        tsArvore.setBounds(420, 180, 30, 22);

        pnlOpcoes.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204))));
        pnlOpcoes.setLayout(null);

        chkIncluirDescricao.setEnabled(false);
        chkIncluirDescricao.setHideActionText(true);
        chkIncluirDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        chkIncluirDescricao.setLabel("Imprimir descri��o");
        pnlOpcoes.add(chkIncluirDescricao);
        chkIncluirDescricao.setBounds(10, 10, 130, 15);

        pnlData.add(pnlOpcoes);
        pnlOpcoes.setBounds(20, 540, 400, 40);

        lblScoreCard.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblScoreCard.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        lblScoreCard.setEnabled(false);
        lblScoreCard.setMaximumSize(new java.awt.Dimension(100, 16));
        lblScoreCard.setMinimumSize(new java.awt.Dimension(100, 16));
        lblScoreCard.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblScoreCard);
        lblScoreCard.setBounds(20, 160, 260, 20);

        tsTreeResp.setCombo(cbUsuarioDe);
        tsTreeResp.setRoot(false);
        pnlData.add(tsTreeResp);
        tsTreeResp.setBounds(420, 400, 25, 22);

        scpRecord.setViewportView(pnlData);

        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        tblAcoes.addTab(bundle2.getString("KpiRelatorioPlanoAcao_00022"), scpRecord); // NOI18N

        pnlFields.add(tblAcoes, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlRecord, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

	private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
            KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder path = new StringBuilder();
            StringBuilder filter = new StringBuilder();

            filter.append("report");
            filter.append("\\");
            filter.append(systemProperties.recDesktop.getString("IDUSER"));
            filter.append("\\");
            filter.append("Spool");
            filter.append("\\");
            filter.append("*.");
            filter.append("html");

            path.append("/");
            path.append("report");
            path.append("/");
            path.append(systemProperties.recDesktop.getString("IDUSER"));
            path.append("/");
            path.append("spool");
            path.append("/");

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            frmChooser.setFileType("*.html");
            frmChooser.loadServerFiles(filter.toString());
            frmChooser.setVisible(true);
            frmChooser.setUrlPath(path.toString());
            frmChooser.openFile();
	}//GEN-LAST:event_btnAbrirActionPerformed

    private void cbTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbTipoActionPerformed
        verificaPlanoAcao();
    }//GEN-LAST:event_cbTipoActionPerformed

	private void btnGravaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravaRelActionPerformed
            KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder file = new StringBuilder();
            StringBuilder filter = new StringBuilder();

            filter.append("report");
            filter.append("\\");
            filter.append(systemProperties.recDesktop.getString("IDUSER"));
            filter.append("\\");
            filter.append("Spool");
            filter.append("\\");
            filter.append("*.");
            filter.append("html");

            file.append("rel051");
            file.append("_");
            file.append(record.getString("ID"));
            file.append(".");
            file.append("html");

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
            frmChooser.setFileType("*.html");
            frmChooser.loadServerFiles(filter.toString());
            frmChooser.setFileName(file.toString());
            frmChooser.setVisible(true);

            if (frmChooser.isValidFile()) {
                String reportName = (String) frmChooser.getFileName();
                event.executeRecord(createRequest(reportName));
            }
	}//GEN-LAST:event_btnGravaRelActionPerformed

	private void btnVizualizaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVizualizaRelActionPerformed
            KpiToolKit toolkit = new KpiToolKit();
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder file = new StringBuilder();

            file.append("report");
            file.append("/");
            file.append(systemProperties.recDesktop.getString("IDUSER"));
            file.append("/");
            file.append("rel051");
            file.append("_");
            file.append(this.id);
            file.append(".");
            file.append("html");

            toolkit.browser(file.toString());
	}//GEN-LAST:event_btnVizualizaRelActionPerformed

	private void btnGeraRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeraRelActionPerformed
            StringBuilder file = new StringBuilder();

            file.append("rel051");
            file.append("_");
            file.append(record.getString("ID"));
            file.append(".");
            file.append("html");

            event.executeRecord(createRequest(file.toString()));
	}//GEN-LAST:event_btnGeraRelActionPerformed

        private void cbScorecardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbScorecardActionPerformed
            kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", "ID_SCOREC = '".concat(event.getComboValue(htbScorecard, cbScorecard, false)) + "'");
            event.populateCombo(recIndFiltrados.getBIXMLVector("INDICADORES"), "INDIC_DE", htbIndDe, cbIndicadorDe);
            event.populateCombo(recIndFiltrados.getBIXMLVector("INDICADORES"), "INDIC_ATE", htbIndAte, cbIndicadorAte);

        }//GEN-LAST:event_cbScorecardActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnGeraRel;
    private javax.swing.JButton btnGravaRel;
    private javax.swing.JButton btnVizualizaRel;
    private javax.swing.ButtonGroup btngrpEntidades;
    private javax.swing.JComboBox cbIndicadorAte;
    private javax.swing.JComboBox cbIndicadorDe;
    private javax.swing.JComboBox cbProjetoAte;
    private javax.swing.JComboBox cbProjetoDe;
    private javax.swing.JComboBox cbScorecard;
    private javax.swing.JComboBox cbSituacao;
    private javax.swing.JComboBox cbStatus;
    private javax.swing.JComboBox cbTipo;
    private javax.swing.JComboBox cbUsuarioDe;
    private javax.swing.JCheckBox chkIncluirDescricao;
    private javax.swing.JComboBox cmbCategoria;
    private pv.jfcx.JPVDatePlus fldInicioAte;
    private pv.jfcx.JPVDatePlus fldInicioDe;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVDatePlus fldTerminoAte;
    private pv.jfcx.JPVDatePlus fldTerminoDe;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCategoria;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblIndicadorAte;
    private javax.swing.JLabel lblIndicadorDe;
    private javax.swing.JLabel lblInicioAte;
    private javax.swing.JLabel lblInicioDe;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblProjetoAte;
    private javax.swing.JLabel lblProjetoDe;
    private javax.swing.JLabel lblScoreCard;
    private javax.swing.JLabel lblSituacao;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblTerminoAte;
    private javax.swing.JLabel lblTerminoDe;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JLabel lblUsuarioDe;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlOpcoes;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbInsertion;
    private javax.swing.JTabbedPane tblAcoes;
    private kpi.beans.JBITreeSelection tsArvore;
    private kpi.beans.JBITreeSelection tsTreeResp;
    private kpi.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiRelPlano(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
        event.editRecord(); // Relatorio por default edit
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();

    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getFormType() {
        return formType;
    }

    private void verificaPlanoAcao() {
        if (cbTipo.getSelectedIndex() == 0) {
            lblScoreCard.setEnabled(true);
            lblIndicadorDe.setEnabled(true);
            cbIndicadorDe.setEnabled(true);
            lblIndicadorAte.setEnabled(true);
            cbIndicadorAte.setEnabled(true);

            lblProjetoDe.setEnabled(false);
            cbProjetoDe.setEnabled(false);
            lblProjetoAte.setEnabled(false);
            cbProjetoAte.setEnabled(false);

        } else if (cbTipo.getSelectedIndex() == 1) {
            lblProjetoDe.setEnabled(true);
            cbProjetoDe.setEnabled(true);
            lblProjetoAte.setEnabled(true);
            cbProjetoAte.setEnabled(true);
            lblScoreCard.setEnabled(false);
            lblIndicadorDe.setEnabled(false);
            cbIndicadorDe.setEnabled(false);
            lblIndicadorAte.setEnabled(false);
            cbIndicadorAte.setEnabled(false);

        } else {
            lblScoreCard.setEnabled(true);
            lblIndicadorDe.setEnabled(true);
            cbIndicadorDe.setEnabled(true);
            lblIndicadorAte.setEnabled(true);
            cbIndicadorAte.setEnabled(true);
            lblProjetoDe.setEnabled(true);
            cbProjetoDe.setEnabled(true);
            lblProjetoAte.setEnabled(true);
            cbProjetoAte.setEnabled(true);
        }
    }

    private String createRequest(String reportName) {
        StringBuilder parametro = new StringBuilder("");

        kpi.util.KpiComboString projetoDe = ((kpi.util.KpiComboString) cbProjetoDe.getSelectedItem());
        kpi.util.KpiComboString projetoAte = ((kpi.util.KpiComboString) cbProjetoAte.getSelectedItem());
        kpi.util.KpiComboString indicadorDe = ((kpi.util.KpiComboString) cbIndicadorDe.getSelectedItem());
        kpi.util.KpiComboString indicadorAte = ((kpi.util.KpiComboString) cbIndicadorAte.getSelectedItem());

        parametro.append("SALVAR;");
        parametro.append(reportName);

        if (projetoDe != null) {
            parametro.append(",").append(projetoDe);
        } else {
            parametro.append(", ");
        }
        if (projetoAte != null) {
            StringBuilder append = parametro.append(",").append(projetoAte);
        } else {
            parametro.append(", ");
        }
        if (indicadorDe != null) {
            parametro.append(",").append(indicadorDe);
        } else {
            parametro.append(", ");
        }
        if (indicadorAte != null) {
            parametro.append(",").append(indicadorAte);
        } else {
            parametro.append(", ");
        }

        return parametro.toString();
    }
}
