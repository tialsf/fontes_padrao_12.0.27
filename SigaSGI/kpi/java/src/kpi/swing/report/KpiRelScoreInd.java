package kpi.swing.report;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import kpi.beans.JBITextArea;
import kpi.beans.JBITreeSelection;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiFileChooser;
import kpi.swing.desktop.KpiDesktopFunctions;
import kpi.util.KpiToolKit;
import pv.jfcx.JPVEdit;

public class KpiRelScoreInd extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "RELSCOREIND";
    private String parentType = recordType;
    private String formType = recordType;
    private boolean isAdmin = false;
    java.util.Hashtable htbScoreDe = new java.util.Hashtable();
    java.util.Hashtable htbScoreAte = new java.util.Hashtable();
    java.util.Hashtable htbTipoImp = new java.util.Hashtable();

    @Override
    public void enableFields() {
        event.setEnableFields(pnlData, true);
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        this.isAdmin = record.getBoolean("ISADMIN");
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        chkIncluirDescricao.setSelected(record.getBoolean("IMPDESC"));
        chkIncluirIndicador.setSelected(record.getBoolean("LISTAIND"));

        //Scorecard De:
        event.populateCombo(record.getBIXMLVector("SCORECARDS"), "SCORE_DE", htbScoreDe, cbScoreDe);
        event.selectComboItem(cbScoreDe, event.getComboItem(record.getBIXMLVector("SCORECARDS"), record.getString("SCORE_DE"), true));
        tsArvoreDe.setVetor(record.getBIXMLVector("SCORECARDS"));

        //Scorecard At�:
        event.populateCombo(record.getBIXMLVector("SCORECARDS"), "SCORE_ATE", htbScoreAte, cbScoreAte);
        event.selectComboItem(cbScoreAte, event.getComboItem(record.getBIXMLVector("SCORECARDS"), record.getString("SCORE_ATE"), true));
        tsArvoreAte.setVetor(record.getBIXMLVector("SCORECARDS"));

        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            tsArvoreDe.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
        }

        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            tsArvoreAte.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
        }

    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        recordAux.set("ID", id);

        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }

        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("IMPDESC", chkIncluirDescricao.isSelected());
        recordAux.set("LISTAIND", chkIncluirIndicador.isSelected());
        recordAux.set("SCORE_DE", event.getComboValue(htbScoreDe, cbScoreDe, false));
        recordAux.set("SCORE_ATE", event.getComboValue(htbScoreAte, cbScoreAte, false));
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (cbScoreDe.getSelectedIndex() <= 0 && !isAdmin) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00004"));
            errorMessage.append("\n");
            retorno = false;
        }

        if (cbScoreAte.getSelectedIndex() <= 0 && !isAdmin) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00004"));
            errorMessage.append("\n");
            retorno = false;
        }

        return retorno;
    }

    public boolean hasCombos() {
        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btngrpEntidades = new ButtonGroup();
        tapCadastro = new JTabbedPane();
        pnlBottomForm = new JPanel();
        pnlRightForm = new JPanel();
        pnlLeftForm = new JPanel();
        pnlRecord = new JPanel();
        pnlTools = new JPanel();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnGeraRel = new JButton();
        btnVizualizaRel = new JButton();
        btnGravaRel = new JButton();
        btnAbrir = new JButton();
        pnlFields = new JPanel();
        tblScorecard = new JTabbedPane();
        scpRecord = new JScrollPane();
        pnlData = new JPanel();
        lblNome = new JLabel();
        fldNome = new JPVEdit();
        jScrollPane1 = new JScrollPane();
        txtDescricao = new JBITextArea();
        lblDescricao = new JLabel();
        pnlEntidade = new JPanel();
        cbScoreDe = new JComboBox();
        lblScoreAte = new JLabel();
        cbScoreAte = new JComboBox();
        tsArvoreDe = new JBITreeSelection();
        tsArvoreAte = new JBITreeSelection();
        lblScoreDe = new JLabel();
        pnlOpcoes = new JPanel();
        chkIncluirDescricao = new JCheckBox();
        chkIncluirIndicador = new JCheckBox();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiRelatorioScoreInd_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        setPreferredSize(new Dimension(500, 400));

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightForm, BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlRecord.setPreferredSize(new Dimension(432, 385));
        pnlRecord.setLayout(new BorderLayout());

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 29));
        pnlTools.setLayout(new BorderLayout());

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(310, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(225, 32));

        btnGeraRel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_imprimir.gif"))); // NOI18N
        btnGeraRel.setText("   "+ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00008")); // NOI18N
        btnGeraRel.setHorizontalAlignment(SwingConstants.TRAILING);
        btnGeraRel.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnGeraRel.setMargin(new Insets(0, 0, 0, 0));
        btnGeraRel.setMaximumSize(new Dimension(60, 22));
        btnGeraRel.setMinimumSize(new Dimension(60, 22));
        btnGeraRel.setPreferredSize(new Dimension(60, 22));
        btnGeraRel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnGeraRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGeraRel);

        btnVizualizaRel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        btnVizualizaRel.setText("   "+ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00009")); // NOI18N
        btnVizualizaRel.setHorizontalAlignment(SwingConstants.TRAILING);
        btnVizualizaRel.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnVizualizaRel.setMargin(new Insets(0, 0, 0, 0));
        btnVizualizaRel.setMaximumSize(new Dimension(80, 22));
        btnVizualizaRel.setMinimumSize(new Dimension(80, 22));
        btnVizualizaRel.setPreferredSize(new Dimension(80, 22));
        btnVizualizaRel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnVizualizaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVizualizaRel);

        btnGravaRel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar_rel.gif"))); // NOI18N
        btnGravaRel.setText("   "+ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00010")); // NOI18N
        btnGravaRel.setHorizontalAlignment(SwingConstants.TRAILING);
        btnGravaRel.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnGravaRel.setMargin(new Insets(0, 0, 0, 0));
        btnGravaRel.setMaximumSize(new Dimension(70, 22));
        btnGravaRel.setMinimumSize(new Dimension(70, 22));
        btnGravaRel.setPreferredSize(new Dimension(70, 22));
        btnGravaRel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnGravaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGravaRel);

        btnAbrir.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        ResourceBundle bundle1 = ResourceBundle.getBundle("international",  KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAbrir.setText(bundle1.getString("KpiRelatorioScoreInd_00012")); // NOI18N
        btnAbrir.setHorizontalAlignment(SwingConstants.TRAILING);
        btnAbrir.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnAbrir.setMargin(new Insets(0, 0, 0, 0));
        btnAbrir.setMaximumSize(new Dimension(60, 22));
        btnAbrir.setMinimumSize(new Dimension(60, 22));
        btnAbrir.setPreferredSize(new Dimension(60, 22));
        btnAbrir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAbrir);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        pnlRecord.add(pnlTools, BorderLayout.NORTH);

        pnlFields.setPreferredSize(new Dimension(432, 360));
        pnlFields.setLayout(new BorderLayout());

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new Dimension(420, 360));

        pnlData.setPreferredSize(new Dimension(100, 260));
        pnlData.setLayout(null);

        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        lblNome.setText("   "+ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00002")); // NOI18N
        lblNome.setAlignmentX(0.5F);
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new Dimension(100, 16));
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 0, 98, 18);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 20, 400, 22);

        txtDescricao.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        jScrollPane1.setViewportView(txtDescricao);

        pnlData.add(jScrollPane1);
        jScrollPane1.setBounds(20, 60, 400, 60);

        lblDescricao.setHorizontalAlignment(SwingConstants.LEFT);
        lblDescricao.setText("   "+ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRelatorioScoreInd_00003")); // NOI18N
        lblDescricao.setAlignmentX(0.5F);
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new Dimension(100, 16));
        lblDescricao.setMinimumSize(new Dimension(100, 16));
        lblDescricao.setPreferredSize(new Dimension(100, 16));
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 40, 60, 18);

        pnlEntidade.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlEntidade.setEnabled(false);
        pnlEntidade.setLayout(null);
        pnlEntidade.add(cbScoreDe);
        cbScoreDe.setBounds(10, 0, 400, 22);

        lblScoreAte.setHorizontalAlignment(SwingConstants.LEFT);
        lblScoreAte.setText(bundle.getString("KpiRelatorioScoreInd_00006")); // NOI18N
        lblScoreAte.setEnabled(false);
        lblScoreAte.setMaximumSize(new Dimension(100, 16));
        lblScoreAte.setMinimumSize(new Dimension(100, 16));
        lblScoreAte.setPreferredSize(new Dimension(100, 16));
        pnlEntidade.add(lblScoreAte);
        lblScoreAte.setBounds(10, 20, 110, 18);
        pnlEntidade.add(cbScoreAte);
        cbScoreAte.setBounds(10, 40, 400, 22);

        tsArvoreDe.setCombo(cbScoreDe);
        tsArvoreDe.setRoot(false);
        pnlEntidade.add(tsArvoreDe);
        tsArvoreDe.setBounds(410, 0, 30, 22);

        tsArvoreAte.setCombo(cbScoreAte);
        tsArvoreAte.setRoot(false);
        pnlEntidade.add(tsArvoreAte);
        tsArvoreAte.setBounds(410, 40, 30, 22);

        pnlData.add(pnlEntidade);
        pnlEntidade.setBounds(10, 140, 450, 70);

        lblScoreDe.setHorizontalAlignment(SwingConstants.LEFT);
        lblScoreDe.setText(bundle.getString("KpiRelatorioScoreInd_00005")); // NOI18N
        lblScoreDe.setEnabled(false);
        lblScoreDe.setMaximumSize(new Dimension(100, 16));
        lblScoreDe.setMinimumSize(new Dimension(100, 16));
        lblScoreDe.setPreferredSize(new Dimension(100, 16));
        pnlData.add(lblScoreDe);
        lblScoreDe.setBounds(20, 120, 250, 20);

        pnlOpcoes.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(204, 204, 204))));
        pnlOpcoes.setLayout(null);

        chkIncluirDescricao.setEnabled(false);
        chkIncluirDescricao.setHorizontalAlignment(SwingConstants.LEFT);
        chkIncluirDescricao.setLabel("Imprimir descri��o");
        pnlOpcoes.add(chkIncluirDescricao);
        chkIncluirDescricao.setBounds(10, 10, 136, 20);

        chkIncluirIndicador.setText("Imprimir Indicadores");
        chkIncluirIndicador.setEnabled(false);
        chkIncluirIndicador.setHorizontalAlignment(SwingConstants.LEFT);
        pnlOpcoes.add(chkIncluirIndicador);
        chkIncluirIndicador.setBounds(10, 30, 160, 20);

        pnlData.add(pnlOpcoes);
        pnlOpcoes.setBounds(20, 220, 400, 60);

        scpRecord.setViewportView(pnlData);

        ResourceBundle bundle2 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        tblScorecard.addTab(bundle2.getString("KpiRelatorioScoreInd_00001"), scpRecord); // NOI18N

        pnlFields.add(tblScorecard, BorderLayout.CENTER);

        pnlRecord.add(pnlFields, BorderLayout.CENTER);

        getContentPane().add(pnlRecord, BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

	private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
            KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder path = new StringBuilder();
            StringBuilder filter = new StringBuilder();

            filter.append("report");
            filter.append("\\");
            filter.append(systemProperties.recDesktop.getString("IDUSER"));
            filter.append("\\");
            filter.append("Spool");
            filter.append("\\");
            filter.append("*.");
            filter.append("html");

            path.append("/");
            path.append("report");
            path.append("/");
            path.append(systemProperties.recDesktop.getString("IDUSER"));
            path.append("/");
            path.append("spool");
            path.append("/");

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            frmChooser.setFileType("*.html");
            frmChooser.loadServerFiles(filter.toString());
            frmChooser.setVisible(true);
            frmChooser.setUrlPath(path.toString());
            frmChooser.openFile();
	}//GEN-LAST:event_btnAbrirActionPerformed

	private void btnGravaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravaRelActionPerformed
            KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder file = new StringBuilder();
            StringBuilder filter = new StringBuilder();

            filter.append("report");
            filter.append("\\");
            filter.append(systemProperties.recDesktop.getString("IDUSER"));
            filter.append("\\");
            filter.append("Spool");
            filter.append("\\");
            filter.append("*.");
            filter.append("html");

            file.append("rel053");
            file.append("_");
            file.append(record.getString("ID"));
            file.append(".");
            file.append("html");

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
            frmChooser.setFileType("*.html");
            frmChooser.loadServerFiles(filter.toString());
            frmChooser.setFileName(file.toString());
            frmChooser.setVisible(true);

            if (frmChooser.isValidFile()) {
                event.executeRecord("SALVAR;" + (String) frmChooser.getFileName());
            }
	}//GEN-LAST:event_btnGravaRelActionPerformed

	private void btnVizualizaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVizualizaRelActionPerformed
            KpiToolKit toolkit = new KpiToolKit();
            KpiDesktopFunctions systemProperties = new KpiDesktopFunctions();
            StringBuilder file = new StringBuilder();

            file.append("report");
            file.append("/");
            file.append(systemProperties.recDesktop.getString("IDUSER"));
            file.append("/");
            file.append("rel053");
            file.append("_");
            file.append(this.id);
            file.append(".");
            file.append("html");

            toolkit.browser(file.toString());
	}//GEN-LAST:event_btnVizualizaRelActionPerformed

	private void btnGeraRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeraRelActionPerformed
            event.executeRecord("SALVAR;");
	}//GEN-LAST:event_btnGeraRelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAbrir;
    private JButton btnGeraRel;
    private JButton btnGravaRel;
    private JButton btnVizualizaRel;
    private ButtonGroup btngrpEntidades;
    private JComboBox cbScoreAte;
    private JComboBox cbScoreDe;
    private JCheckBox chkIncluirDescricao;
    private JCheckBox chkIncluirIndicador;
    private JPVEdit fldNome;
    private JScrollPane jScrollPane1;
    private JLabel lblDescricao;
    private JLabel lblNome;
    private JLabel lblScoreAte;
    private JLabel lblScoreDe;
    private JPanel pnlBottomForm;
    private JPanel pnlData;
    private JPanel pnlEntidade;
    private JPanel pnlFields;
    private JPanel pnlLeftForm;
    private JPanel pnlOpcoes;
    private JPanel pnlOperation;
    private JPanel pnlRecord;
    private JPanel pnlRightForm;
    private JPanel pnlTools;
    private JScrollPane scpRecord;
    private JTabbedPane tapCadastro;
    private JToolBar tbInsertion;
    private JTabbedPane tblScorecard;
    private JBITreeSelection tsArvoreAte;
    private JBITreeSelection tsArvoreDe;
    private JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiRelScoreInd(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
        event.editRecord(); // Relatorio por default edit
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();

    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getFormType() {
        return formType;
    }
}
