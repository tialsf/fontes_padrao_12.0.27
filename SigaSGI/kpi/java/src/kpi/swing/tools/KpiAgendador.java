package kpi.swing.tools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import kpi.beans.JBIListPanel;
import kpi.core.KpiStaticReferences;

public class KpiAgendador extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {
    private String recordType = "AGENDADOR";
    private String formType = recordType;//Tipo do Formulario
    private String parentType = recordType;//Tipo formulario pai, caso haja.
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiAgendador(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        listaAgendamento.setDataSource(record.getBIXMLVector("AGENDAMENTOS"), "0", "0", this.event);
        listaAgendamento.xmlTable.getColumn(0).setPreferredWidth(200);

        if (record.getBoolean("SITUACAO")) {
            fldSituacao.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiAgendador_00021"));
            lblSemaforo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_semaforo_verde.gif")));
        } else {
            fldSituacao.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiAgendador_00022"));
            lblSemaforo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_semaforo_vermelho.gif")));
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new JTabbedPane();
        pnlControl = new JPanel();
        pnlFieldsAgendador = new JPanel();
        lblSemaforo = new JLabel();
        fldSituacao = new JLabel();
        pnlToolsAgendador = new JPanel();
        tbAction = new JToolBar();
        btnIniciar = new JButton();
        btnParar = new JButton();
        btnAtualizar = new JButton();
        btnAjuda = new JButton();
        listaAgendamento = new JBIListPanel();
        pnlTopForm = new JPanel();
        pnlRightForm = new JPanel();
        pnlLeftForm = new JPanel();
        pnlBottomForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiAgendador_00013")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_agendador.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(545, 310));
        setRequestFocusEnabled(false);

        tapCadastro.setPreferredSize(new Dimension(530, 300));

        pnlControl.setPreferredSize(new Dimension(20, 22));
        pnlControl.setLayout(new BorderLayout());

        pnlFieldsAgendador.setLayout(null);

        lblSemaforo.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_semaforo_vermelho.gif"))); // NOI18N
        pnlFieldsAgendador.add(lblSemaforo);
        lblSemaforo.setBounds(70, 50, 60, 80);

        fldSituacao.setFont(new Font("MS Sans Serif", 1, 11)); // NOI18N
        fldSituacao.setHorizontalAlignment(SwingConstants.LEFT);
        fldSituacao.setHorizontalTextPosition(SwingConstants.LEFT);
        fldSituacao.setMinimumSize(new Dimension(180, 22));
        fldSituacao.setPreferredSize(new Dimension(180, 22));
        pnlFieldsAgendador.add(fldSituacao);
        fldSituacao.setBounds(140, 80, 220, 20);

        pnlControl.add(pnlFieldsAgendador, BorderLayout.CENTER);

        pnlToolsAgendador.setLayout(new BorderLayout());

        tbAction.setFloatable(false);
        tbAction.setRollover(true);
        tbAction.setMaximumSize(new Dimension(260, 33));
        tbAction.setMinimumSize(new Dimension(260, 33));
        tbAction.setPreferredSize(new Dimension(260, 30));

        btnIniciar.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_play.gif"))); // NOI18N
        btnIniciar.setText(bundle.getString("KpiAgendador_00016")); // NOI18N
        btnIniciar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnIniciarActionPerformed(evt);
            }
        });
        tbAction.add(btnIniciar);

        btnParar.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_parar.gif"))); // NOI18N
        btnParar.setText(bundle.getString("KpiAgendador_00017")); // NOI18N
        btnParar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnPararActionPerformed(evt);
            }
        });
        tbAction.add(btnParar);

        btnAtualizar.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnAtualizar.setText(bundle.getString("JBIListPanel_00003")); // NOI18N
        btnAtualizar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAtualizarActionPerformed(evt);
            }
        });
        tbAction.add(btnAtualizar);

        btnAjuda.setFont(new Font("Dialog", 0, 12));
        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbAction.add(btnAjuda);

        pnlToolsAgendador.add(tbAction, BorderLayout.NORTH);

        pnlControl.add(pnlToolsAgendador, BorderLayout.NORTH);

        tapCadastro.addTab(bundle.getString("KpiAgendador_00015"), pnlControl); // NOI18N
        tapCadastro.addTab(bundle1.getString("KpiAgendador_00023"), listaAgendamento); // NOI18N

        getContentPane().add(tapCadastro, BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlTopForm, BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightForm, BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pack();
    }//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

    private void btnAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarActionPerformed
        event.loadRecord();
    }//GEN-LAST:event_btnAtualizarActionPerformed

    private void btnPararActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPararActionPerformed
        // Parando ...
        fldSituacao.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiAgendador_00020"));
        event.executeRecord("STOP");
        lblSemaforo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_semaforo_amarelo.gif")));
    }//GEN-LAST:event_btnPararActionPerformed

    private void btnIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarActionPerformed
        // Iniciando ...
        fldSituacao.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiAgendador_00019"));
        event.executeRecord("START");
        lblSemaforo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_semaforo_verde.gif")));
    }//GEN-LAST:event_btnIniciarActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAjuda;
    private JButton btnAtualizar;
    private JButton btnIniciar;
    private JButton btnParar;
    private JLabel fldSituacao;
    private JLabel lblSemaforo;
    private JBIListPanel listaAgendamento;
    private JPanel pnlBottomForm;
    private JPanel pnlControl;
    private JPanel pnlFieldsAgendador;
    private JPanel pnlLeftForm;
    private JPanel pnlRightForm;
    private JPanel pnlToolsAgendador;
    private JPanel pnlTopForm;
    private JTabbedPane tapCadastro;
    private JToolBar tbAction;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }
}
