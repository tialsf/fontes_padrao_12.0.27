package kpi.swing.tools;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ResourceBundle;
import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import kpi.beans.JBITextArea;
import kpi.beans.JBITreeSelection;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiFileChooser;
import pv.jfcx.JPVDatePlus;
import pv.jfcx.JPVEdit;
import pv.jfcx.JPVTime;

public class KpiAgendamento extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private final int FREQ_DIARIO = 1;
    private final int FREQ_SEMANAL = 2;
    private final int FREQ_MENSAL = 3;
    private final int FUNCAO_ADVPL = 1;
    private final int IMPORTA_INDICADOR = 2;
    private final int NOTIFICA_PLANO_ATRASADO = 3;
    private final int IMPORTA_FONTE_DADOS = 4;
    private final int CALCULA_INDICADOR = 5;
    private final int ALERTA_PREENCHIMENTO = 6;
    //
    private String recordType = "AGENDAMENTO";
    private String formType = recordType;
    private String parentType = "AGENDADOR";
    private java.util.Hashtable htbElemento = new java.util.Hashtable();
    private kpi.util.KpiDateUtil date_Util = new kpi.util.KpiDateUtil();
    private java.util.Hashtable htbAcao = new java.util.Hashtable();
    private java.util.Hashtable htbScoreCard = new java.util.Hashtable();
    private CardLayout cardLayout = new CardLayout();
    private kpi.xml.BIXMLVector vctDataSrc = null;
    private kpi.xml.BIXMLVector vctComandos = null;
    private kpi.xml.BIXMLVector vctScorecard = null;
    private final String diaSemana[][] = {{"2", "Segunda-Feira"}, {"3", "Ter�a-Feira"}, {"4", "Quarta-Feira"}, {"5", "Quinta-Feira"}, {"6", "Sexta-Feira"}, {"7", "S�bado"}, {"1", "Domingo"}};
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;
    private Boolean BlqDiaLimite;

    public KpiAgendamento(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
        this.pnlAction_FuncaoAdvpl.setVisible(false);
        this.pnlAction_ImportacaoDeDados.setVisible(false);
        this.pnlAction_ImportacaoFonteDados.setVisible(false);
        this.pnlAction_Calculo.setVisible(false);
        this.pnlAction_AlertaPlanilhaValores.setVisible(false);
        this.fldDataCalcDe.setCalendar(new java.util.GregorianCalendar());
        this.fldDataCalcAte.setCalendar(new java.util.GregorianCalendar());
    }

    
    @Override
    public void enableFields() {
        event.setEnableFields(pnlTarefa, true);
        event.setEnableFields(pnlAction_AlertaPlanilhaValores, true);
        event.setEnableFields(pnlAction_Calculo, true);
        event.setEnableFields(pnlAction_FuncaoAdvpl, true);
        event.setEnableFields(pnlAction_ImportacaoDeDados, true);
        event.setEnableFields(pnlAction_ImportacaoFonteDados, true);
        event.setEnableFields(pnlAgendar, true);
        event.setEnableFields(pnlAgendar_Frequencia, true);
        event.setEnableFields(pnlAgendar_Periodo, true);
        event.setEnableFields(pnlCardAction, true);
        event.setEnableFields(pnlFixo, true);
        event.setEnableFields(pnlOpcaoPeriodo, true);
        event.setEnableFields(pnlDinamico, true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlTarefa, false);
        event.setEnableFields(pnlAction_AlertaPlanilhaValores, false);
        event.setEnableFields(pnlAction_Calculo, false);
        event.setEnableFields(pnlAction_FuncaoAdvpl, false);
        event.setEnableFields(pnlAction_ImportacaoDeDados, false);
        event.setEnableFields(pnlAction_ImportacaoFonteDados, false);
        event.setEnableFields(pnlAgendar, false);
        event.setEnableFields(pnlAgendar_Frequencia, false);
        event.setEnableFields(pnlAgendar_Periodo, false);
        event.setEnableFields(pnlCardAction, false);
        event.setEnableFields(pnlFixo, false);
        event.setEnableFields(pnlOpcaoPeriodo, false);
        event.setEnableFields(pnlDinamico, false);
    }

    @Override
    public void refreshFields() {
        int frequencia = record.getInt("FREQ");

        fldNome.setText(record.getString("NOME"));
        fldDataIni.setCalendar(record.getDate("DATAINI"));
        fldHorarioIni.setText(record.getString("HORAINI"));
        fldDataFim.setCalendar(record.getDate("DATAFIM"));
        fldHorarioFim.setText(record.getString("HORAFIM"));
        fldFreq_HorarioFire.setText(record.getString("HORAFIRE"));
        fldDataExe.setCalendar(record.getDate("DATAEXE"));
        fldHorarioExe.setText(record.getString("HORAEXE"));
        fldDataNext.setCalendar(record.getDate("DATANEXT"));
        fldHorarioNext.setText(record.getString("HORANEXT"));
        fldAmbiente.setText(record.getString("ENV"));
        fldDiretorio.setText(record.getString("DIRETORIO"));
        cmdComando.setText(record.getString("ACAO"));
        fldDataCalcDe.setCalendar(record.getDate("DTCALCDE"));
        fldDataCalcAte.setCalendar(record.getDate("DTCALCATE"));
        spnDiaLimite.setValue(record.getInt("DIA_LIMITE"));
        vctComandos = record.getBIXMLVector("COMANDOS");
        vctDataSrc = record.getBIXMLVector("DATASRCS");
        vctScorecard = record.getBIXMLVector("SCORECARDS");
        jrbDinamico.setSelected(record.getString("DINAMICO").equalsIgnoreCase("T"));
        spnMenos.setValue(record.getInt("DATAMENOS"));
        spnMais.setValue(record.getInt("DATAMAIS"));
        event.populateCombo(vctComandos, "IDACAO", htbAcao, cbAcao);
        populaDiaSemana();
        
        BlqDiaLimite = record.getBoolean("BLOQ_POR_DIA_LIMITE");

        if (status == KpiDefaultFrameBehavior.INSERTING) {
            cbAcao.setSelectedIndex(0);
            rbDiario.setSelected(true);
            regraFrequencia(FREQ_DIARIO);
            fldDataIni.setCalendar(new java.util.GregorianCalendar());
            fldDataFim.setCalendar(new java.util.GregorianCalendar());
            fldHorarioIni.setText("00:00");
            fldHorarioFim.setText("23:59");
            fldFreq_HorarioFire.setText("23:59");
            spnDia.setValue(1);
        } else {

            switch (frequencia) {
                case FREQ_DIARIO:	//Diario
                    rbDiario.setSelected(true);
                    spnDia.setValue(1);
                    break;

                case FREQ_SEMANAL:	//Semanal
                    rbSemanal.setSelected(true);
                    selecionaDiaSemana(record.getString("DIAFIRE"));
                    spnDia.setValue(1);
                    break;

                case FREQ_MENSAL:	//Mensal
                    rbMensal.setSelected(true);
                    spnDia.setValue(record.getInt("DIAFIRE"));
                    break;
            }

            regraFrequencia(frequencia);
        }

        configuraAcao();
        tipoCalculo();
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        recordAux.set("ID", id);
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DATAINI", fldDataIni.getCalendar());
        recordAux.set("DATAFIM", fldDataFim.getCalendar());
        recordAux.set("HORAINI", fldHorarioIni.getText());
        recordAux.set("HORAFIM", fldHorarioFim.getText());
        recordAux.set("ENV", fldAmbiente.getText());
        recordAux.set("HORAFIRE", fldFreq_HorarioFire.getText());
        recordAux.set("IDACAO", event.getComboValue(htbAcao, cbAcao));
        recordAux.set("DTCALCDE", fldDataCalcDe.getCalendar());
        recordAux.set("DTCALCATE", fldDataCalcAte.getCalendar());

        if (rbMensal.isSelected()) {
            recordAux.set("FREQ", FREQ_MENSAL);
            recordAux.set("DIAFIRE", (Integer) spnDia.getValue());
        } else if (rbSemanal.isSelected()) {
            recordAux.set("FREQ", FREQ_SEMANAL);
            recordAux.set("DIAFIRE", Integer.parseInt(getDiaSemana()));
        } else {
            recordAux.set("FREQ", FREQ_DIARIO);
            recordAux.set("DIAFIRE", 0);
        }

        recordAux.set("DIRETORIO", fldDiretorio.getText());
        recordAux.set("DIA_LIMITE", ((Integer) spnDiaLimite.getValue()).intValue());

        //A��o de c�lulo dos indicadores.
        if (cbAcao.getSelectedIndex() == CALCULA_INDICADOR) {
            recordAux.set("ACAO", configuraCalculo());
        } else {
            recordAux.set("ACAO", cmdComando.getText());
        }

        //Identifica se o per�odo de c�lculo � fixo ou din�mico.
        if (jrbDinamico.isSelected()) {
            recordAux.set("DINAMICO", "T");
        } else {
            recordAux.set("DINAMICO", "F");
        }

        //Identifica o per�odo de-at� para c�lculo din�mico.
        recordAux.set("DATAMENOS", ((Integer) spnMenos.getValue()).intValue());
        recordAux.set("DATAMAIS", ((Integer) spnMais.getValue()).intValue());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean isValid = true;
        if (cbAcao.getSelectedIndex() == IMPORTA_FONTE_DADOS && cbElemento.getSelectedIndex() <= 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiAgendamento_00012"));//Selecione o item para importa��o da fonte de dados.
            isValid = false;

        } else if (cbAcao.getSelectedIndex() == CALCULA_INDICADOR && cbbScoreCard.getSelectedIndex() <= 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiProjeto_00008")); //Scorecard n�o informado!
            isValid = false;

        } else if (cbAcao.getSelectedIndex() == ALERTA_PREENCHIMENTO && ((Integer) spnDiaLimite.getValue()).intValue() < 1 || ((Integer) spnDiaLimite.getValue()).intValue() > 31) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiAgendamento_00015")); //"Dia limite para preenchimento inv�lido."
            isValid = false;
        }

        if (!isValid) {
            btnSave.setEnabled(true);
        }

        return isValid;
    }

    /**
     *Exibe e configura a tela para cada tipo de a��o dispon�vel no agendador.
     */
    private void configuraAcao() {
        int acao = cbAcao.getSelectedIndex();
        String comando = null;

        switch (acao) {
            //Fun��o ADVPL.
            case FUNCAO_ADVPL:
                cardLayout.show(pnlCardAction, "cardFuncaoADVPL");
                cmdComando.setText("");
                break;

            //Importa��o de Indicadores.
            case IMPORTA_INDICADOR:
                cardLayout.show(pnlCardAction, "cardImportacaoDeDados");
                break;

            //Notifica��o de Planos Atrasados.
            case NOTIFICA_PLANO_ATRASADO:
                cardLayout.show(pnlCardAction, "cardNotificacaoPlanosAtrasados");
                break;

            //Importa��o da Fonte de dados.
            case IMPORTA_FONTE_DADOS:
                cardLayout.show(pnlCardAction, "cardImportacaoFonteDados");
                event.populateCombo(vctDataSrc, "", htbElemento, cbElemento);

                comando = record.getString("ACAO");

                if (comando.length() > 0 && comando.startsWith("KpiDataSrcJob")) {
                    String itemID = comando.substring(comando.indexOf("(") + 1, comando.indexOf(","));
                    cbElemento.setSelectedIndex(event.getComboItem(vctDataSrc, itemID, true));
                }
                break;

            //C�lculo dos Indicadores.
            case CALCULA_INDICADOR:
                cardLayout.show(pnlCardAction, "cardCalculo");
                event.populateCombo(vctScorecard, "SCOR_ID", htbScoreCard, cbbScoreCard);
                tsArvore.setVetor(vctScorecard);

                comando = record.getString("ACAO");

                if (comando.length() > 0) {
                    String[] aJobCmd = comando.split(",");
                    if (aJobCmd.length > 2) {
                        String jobCmd = aJobCmd[3];
                        String[] detJobCmd = jobCmd.split("\\|");
                        event.selectComboItem(cbbScoreCard, event.getComboItem(vctScorecard, detJobCmd[0].replace("\'", ""), true));
                    }
                }

                cmdComando.setText("KpiCalc_Indicador");
                break;

            //Alerta para preenchimento da planilha de valores.
            case ALERTA_PREENCHIMENTO:
                if (BlqDiaLimite){
                    cardLayout.show(pnlCardAction, "cardAlertaPlanValoresDiaLimite");
                }else{
                    cardLayout.show(pnlCardAction, "cardAlertaPlanilhaValores");
                }
                break;
        }
    }

    /**
     *Exibe o formul�rio para cada tipo de c�lculo.
     */
    private void tipoCalculo() {
        if (jrbDinamico.isSelected()) {
            cardLayout.show(pnlPeriodo, "cardDinamico");
        } else {
            cardLayout.show(pnlPeriodo, "cardFixo");
        }
    }

    /**
     *Cria a requisi��o para ser enviada ao ADPVL.
     */
    private String configuraCalculo() {
        String scorID = event.getComboValue(htbScoreCard, cbbScoreCard);
        StringBuilder parametros = new StringBuilder(cmdComando.getText());
        parametros.append("(");
        parametros.append(scorID); //ID do Scorecard.

        //Per�odo Din�mico.
        if (jrbDinamico.isSelected()) {
            parametros.append("|");
            parametros.append("DINAMICO"); //Identificador do tipo de per�odo de c�lculo.
            parametros.append("|");
            parametros.append(((Integer) spnMenos.getValue()).intValue()); //Data da execu��o menos o valor informado(Per�odo de).
            parametros.append("-");
            parametros.append(((Integer) spnMais.getValue()).intValue()); //Data da execu��o mais o valor informado(Per�odo at�).

            //Per�odo Fixo.
        } else {
            parametros.append("|");
            parametros.append(parseToDate(date_Util.getCalendarString(fldDataCalcDe.getCalendar()))); //Per�odo de:
            parametros.append("|");
            parametros.append(parseToDate(date_Util.getCalendarString(fldDataCalcAte.getCalendar()))); //Per�odo at�:
        }

        parametros.append(")");
        return parametros.toString();
    }

    /**
     *
     * @param date
     * @return
     */
    private String parseToDate(String date) {
        String data = "";
        if (!date.endsWith("  /  /  ")) {
            int nPos = date.trim().indexOf(" ");
            if (nPos > 0) {
                data = date.substring(0, nPos);
            } else {
                data = date;
            }
        } else {
            data = "  /  /  ";
        }
        return data;
    }

    /**
     * Popula a combo cboDia com o dia da semana.
     */
    private void populaDiaSemana() {
        cboDia.removeAllItems();
        for (int i = 0; i < diaSemana.length; i++) {
            cboDia.addItem(diaSemana[i][1]);
        }
    }

    /**
     * Recupera o equivalente num�rico a um dia da semana.
     * @return
     */
    private String getDiaSemana() {
        return diaSemana[cboDia.getSelectedIndex()][0];
    }

    /**
     * Seleciona um dia da semana no combo cboDia.
     * @param dia
     */
    private void selecionaDiaSemana(String dia) {
        if (!(dia.equals("") || dia.equals("0"))) {
            for (int i = 0; i < diaSemana.length; i++) {
                if (diaSemana[i][0].equals(dia)) {
                    cboDia.setSelectedIndex(i);
                    break;
                }
            }
        }
    }

    /**
     * Determina a regra de exibi��o de campos de acordo com a frequ�ncia.
     * @param frequencia
     */
    private void regraFrequencia(int frequencia) {
        switch (frequencia) {
            case FREQ_DIARIO: //Di�rio
                cboDia.setVisible(false);
                spnDia.setVisible(false);
                lblfreq_Dia.setVisible(false);
                break;

            case FREQ_SEMANAL: //Semanal
                cboDia.setVisible(true);
                spnDia.setVisible(false);
                lblfreq_Dia.setVisible(true);
                break;

            case FREQ_MENSAL: //Mensal
                cboDia.setVisible(false);
                spnDia.setVisible(true);
                lblfreq_Dia.setVisible(true);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpCalculo = new ButtonGroup();
        grpFrequencia = new ButtonGroup();
        tapCadastro = new JTabbedPane();
        pnlRecord = new JPanel();
        pnlFields = new JPanel();
        scpRecord = new JScrollPane();
        pnlTarefa = new JPanel();
        lblNome = new JLabel();
        fldNome = new JPVEdit();
        cbAcao = new JComboBox();
        lblAcao = new JLabel();
        pnlCardAction = new JPanel(cardLayout);
        pnlAction_NotificacaoPlanosAtrasados = new JPanel();
        pnlAction_FuncaoAdvpl = new JPanel();
        lblAmbiente = new JLabel();
        fldAmbiente = new JPVEdit();
        lblComando = new JLabel();
        scrComando = new JScrollPane();
        cmdComando = new JBITextArea();
        pnlAction_ImportacaoDeDados = new JPanel();
        lblDiretorio = new JLabel();
        fldDiretorio = new JPVEdit();
        btnLog = new JButton();
        pnlAction_ImportacaoFonteDados = new JPanel();
        lblElemento = new JLabel();
        cbElemento = new JComboBox();
        pnlAction_AlertaPlanilhaValores = new JPanel();
        lblDiaLimite = new JLabel();
        spnDiaLimite = new JSpinner();
        pnlAction_Calculo = new JPanel();
        lblScoreCard = new JLabel();
        cbbScoreCard = new JComboBox();
        pnlPeriodo = new JPanel(cardLayout);
        pnlFixo = new JPanel();
        fldDataCalcAte = new JPVDatePlus();
        lblDataAte = new JLabel();
        fldDataCalcDe = new JPVDatePlus();
        lblDataDe = new JLabel();
        pnlDinamico = new JPanel();
        spnMenos = new JSpinner();
        spnMais = new JSpinner();
        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jLabel5 = new JLabel();
        jLabel8 = new JLabel();
        pnlOpcaoPeriodo = new JPanel();
        jrbFixo = new JRadioButton();
        jrbDinamico = new JRadioButton();
        tsArvore = new JBITreeSelection();
        pnlAction_AlertaPlanValoresDiaLimite = new JPanel();
        jSeparator1 = new JSeparator();
        jSeparator2 = new JSeparator();
        lblMsgDiaLimite = new JLabel();
        pnlAgendar = new JPanel();
        pnlAgendar_Periodo = new JPanel();
        lblDataIni = new JLabel();
        fldDataIni = new JPVDatePlus();
        fldHorarioIni = new JPVTime();
        lblDataFim = new JLabel();
        fldDataFim = new JPVDatePlus();
        fldHorarioFim = new JPVTime();
        jLabel3 = new JLabel();
        jLabel4 = new JLabel();
        pnlAgendar_Frequencia = new JPanel();
        rbDiario = new JRadioButton();
        rbSemanal = new JRadioButton();
        rbMensal = new JRadioButton();
        fldFreq_HorarioFire = new JPVTime();
        cboDia = new JComboBox();
        spnDia = new JSpinner();
        lblFreq_Horario = new JLabel();
        lblfreq_Dia = new JLabel();
        pnlDetalhes = new JPanel();
        pnlDetalhe_UltExec = new JPanel();
        lblDataExe1 = new JLabel();
        jLabel6 = new JLabel();
        fldDataExe = new JPVDatePlus();
        fldHorarioExe = new JPVTime();
        pnlDetalhe_ProxExec = new JPanel();
        lblDataExe2 = new JLabel();
        fldDataNext = new JPVDatePlus();
        fldHorarioNext = new JPVTime();
        jLabel7 = new JLabel();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        btnReload = new JButton();
        pnlRightForm = new JPanel();
        pnlBottomForm = new JPanel();
        pnlLeftForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiAgendamento_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_agendador.gif")));         setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(480, 400));

        pnlRecord.setLayout(new BorderLayout());

        pnlFields.setLayout(new BorderLayout());

        scpRecord.setBorder(null);

        pnlTarefa.setLayout(null);

        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiBaseFrame_00006")); // NOI18N
        lblNome.setMaximumSize(new Dimension(100, 16));
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlTarefa.add(lblNome);
        lblNome.setBounds(20, 10, 58, 20);
        pnlTarefa.add(fldNome);
        fldNome.setBounds(20, 30, 400, 22);

        cbAcao.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                cbAcaoItemStateChanged(evt);
            }
        });
        pnlTarefa.add(cbAcao);
        cbAcao.setBounds(20, 70, 400, 22);

        lblAcao.setHorizontalAlignment(SwingConstants.LEFT);
        lblAcao.setText(bundle.getString("KpiAgendamento_00009")); // NOI18N
        lblAcao.setMaximumSize(new Dimension(100, 16));
        lblAcao.setMinimumSize(new Dimension(100, 16));
        lblAcao.setPreferredSize(new Dimension(100, 16));
        pnlTarefa.add(lblAcao);
        lblAcao.setBounds(20, 50, 58, 20);

        /*
        pnlCardAction.setLayout(new java.awt.CardLayout());
        */
        pnlCardAction.add(pnlAction_NotificacaoPlanosAtrasados, "cardNotificacaoPlanosAtrasados");

        pnlAction_FuncaoAdvpl.setLayout(null);

        lblAmbiente.setHorizontalAlignment(SwingConstants.LEFT);
        lblAmbiente.setText(bundle.getString("KpiAgendamento_00003")); // NOI18N
        lblAmbiente.setMaximumSize(new Dimension(100, 16));
        lblAmbiente.setMinimumSize(new Dimension(100, 16));
        lblAmbiente.setPreferredSize(new Dimension(100, 16));
        pnlAction_FuncaoAdvpl.add(lblAmbiente);
        lblAmbiente.setBounds(0, 4, 58, 16);

        fldAmbiente.setMaxLength(160);
        pnlAction_FuncaoAdvpl.add(fldAmbiente);
        fldAmbiente.setBounds(0, 20, 400, 22);

        lblComando.setHorizontalAlignment(SwingConstants.LEFT);
        lblComando.setText(bundle.getString("KpiAgendamento_00002")); // NOI18N
        lblComando.setMaximumSize(new Dimension(100, 16));
        lblComando.setMinimumSize(new Dimension(100, 16));
        lblComando.setPreferredSize(new Dimension(100, 16));
        pnlAction_FuncaoAdvpl.add(lblComando);
        lblComando.setBounds(0, 40, 140, 20);

        cmdComando.setFont(new Font("Tahoma", 0, 11));         scrComando.setViewportView(cmdComando);

        pnlAction_FuncaoAdvpl.add(scrComando);
        scrComando.setBounds(0, 60, 400, 60);

        pnlCardAction.add(pnlAction_FuncaoAdvpl, "cardFuncaoADVPL");

        pnlAction_ImportacaoDeDados.setLayout(null);

        lblDiretorio.setHorizontalAlignment(SwingConstants.LEFT);
        lblDiretorio.setText(bundle.getString("KpiAgendamento_00010")); // NOI18N
        lblDiretorio.setMaximumSize(new Dimension(100, 16));
        lblDiretorio.setMinimumSize(new Dimension(100, 16));
        lblDiretorio.setPreferredSize(new Dimension(100, 16));
        pnlAction_ImportacaoDeDados.add(lblDiretorio);
        lblDiretorio.setBounds(0, 0, 85, 20);

        fldDiretorio.setMaxLength(160);
        pnlAction_ImportacaoDeDados.add(fldDiretorio);
        fldDiretorio.setBounds(0, 20, 400, 22);

        btnLog.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_log.gif")));         ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnLog.setToolTipText(bundle1.getString("KpiAgendamento_00011")); // NOI18N
        btnLog.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnLogActionPerformed(evt);
            }
        });
        pnlAction_ImportacaoDeDados.add(btnLog);
        btnLog.setBounds(410, 20, 25, 22);

        pnlCardAction.add(pnlAction_ImportacaoDeDados, "cardImportacaoDeDados");

        pnlAction_ImportacaoFonteDados.setLayout(null);

        lblElemento.setHorizontalAlignment(SwingConstants.LEFT);
        lblElemento.setText(bundle.getString("KpiAgendamento_00008")); // NOI18N
        lblElemento.setMaximumSize(new Dimension(100, 16));
        lblElemento.setMinimumSize(new Dimension(100, 16));
        lblElemento.setPreferredSize(new Dimension(100, 16));
        pnlAction_ImportacaoFonteDados.add(lblElemento);
        lblElemento.setBounds(0, 0, 58, 20);

        cbElemento.setPreferredSize(new Dimension(26, 23));
        cbElemento.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                cbElementoItemStateChanged(evt);
            }
        });
        pnlAction_ImportacaoFonteDados.add(cbElemento);
        cbElemento.setBounds(0, 20, 400, 22);

        pnlCardAction.add(pnlAction_ImportacaoFonteDados, "cardImportacaoFonteDados");

        pnlAction_AlertaPlanilhaValores.setLayout(null);

        lblDiaLimite.setHorizontalAlignment(SwingConstants.LEFT);
        lblDiaLimite.setText(bundle1.getString("KpiAgendamento_00014")); // NOI18N
        lblDiaLimite.setMaximumSize(new Dimension(100, 16));
        lblDiaLimite.setMinimumSize(new Dimension(100, 16));
        lblDiaLimite.setPreferredSize(new Dimension(100, 16));
        pnlAction_AlertaPlanilhaValores.add(lblDiaLimite);
        lblDiaLimite.setBounds(0, 0, 90, 20);

        spnDiaLimite.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                spnDiaLimiteStateChanged(evt);
            }
        });
        pnlAction_AlertaPlanilhaValores.add(spnDiaLimite);
        spnDiaLimite.setBounds(0, 20, 180, 22);

        pnlCardAction.add(pnlAction_AlertaPlanilhaValores, "cardAlertaPlanilhaValores");

        pnlAction_Calculo.setLayout(null);

        lblScoreCard.setText(bundle1.getString("KpiDataSourceFrame_00021")); // NOI18N
        lblScoreCard.setPreferredSize(new Dimension(80, 15));
        pnlAction_Calculo.add(lblScoreCard);
        lblScoreCard.setBounds(0, 5, 80, 15);
        pnlAction_Calculo.add(cbbScoreCard);
        cbbScoreCard.setBounds(0, 20, 400, 22);

        /*
        pnlPeriodo.setLayout(new java.awt.CardLayout());
        */

        pnlFixo.setLayout(null);

        fldDataCalcAte.setAllowNull(false);
        fldDataCalcAte.setDialog(true);
        fldDataCalcAte.setUseLocale(true);
        fldDataCalcAte.setWaitForCalendarDate(true);
        pnlFixo.add(fldDataCalcAte);
        fldDataCalcAte.setBounds(220, 20, 180, 22);

        lblDataAte.setText(bundle1.getString("KpiDataSourceFrame_00023")); // NOI18N
        lblDataAte.setPreferredSize(new Dimension(30, 15));
        pnlFixo.add(lblDataAte);
        lblDataAte.setBounds(220, 5, 30, 15);

        fldDataCalcDe.setAllowNull(false);
        fldDataCalcDe.setCalendarMonths(2);
        fldDataCalcDe.setDialog(true);
        fldDataCalcDe.setUseLocale(true);
        fldDataCalcDe.setWaitForCalendarDate(true);
        pnlFixo.add(fldDataCalcDe);
        fldDataCalcDe.setBounds(0, 20, 180, 22);

        lblDataDe.setText(bundle1.getString("KpiDataSourceFrame_00022")); // NOI18N
        lblDataDe.setPreferredSize(new Dimension(80, 15));
        pnlFixo.add(lblDataDe);
        lblDataDe.setBounds(0, 5, 80, 15);

        pnlPeriodo.add(pnlFixo, "cardFixo");

        pnlDinamico.setLayout(null);

        spnMenos.setModel(new SpinnerNumberModel(0, 0, 365, 1));
        pnlDinamico.add(spnMenos);
        spnMenos.setBounds(220, 20, 180, 20);

        spnMais.setModel(new SpinnerNumberModel(0, 0, 365, 1));
        pnlDinamico.add(spnMais);
        spnMais.setBounds(220, 60, 180, 20);

        jLabel1.setText(bundle1.getString("KpiAgendamento_00031")); // NOI18N
        pnlDinamico.add(jLabel1);
        jLabel1.setBounds(0, 20, 200, 20);

        jLabel2.setText(bundle1.getString("KpiAgendamento_00032")); // NOI18N
        pnlDinamico.add(jLabel2);
        jLabel2.setBounds(0, 60, 200, 20);

        jLabel5.setText(bundle1.getString("KpiAgendamento_00033")); // NOI18N
        pnlDinamico.add(jLabel5);
        jLabel5.setBounds(220, 0, 60, 20);

        jLabel8.setText(bundle1.getString("KpiAgendamento_00033")); // NOI18N
        pnlDinamico.add(jLabel8);
        jLabel8.setBounds(220, 40, 60, 20);

        pnlPeriodo.add(pnlDinamico, "cardDinamico");

        pnlAction_Calculo.add(pnlPeriodo);
        pnlPeriodo.setBounds(0, 110, 400, 90);

        pnlOpcaoPeriodo.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(204, 204, 204))));
        pnlOpcaoPeriodo.setLayout(null);

        grpCalculo.add(jrbFixo);
        jrbFixo.setSelected(true);
        jrbFixo.setText(bundle1.getString("KpiAgendamento_00029")); // NOI18N
        jrbFixo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jrbFixoActionPerformed(evt);
            }
        });
        pnlOpcaoPeriodo.add(jrbFixo);
        jrbFixo.setBounds(10, 10, 100, 20);

        grpCalculo.add(jrbDinamico);
        jrbDinamico.setText(bundle1.getString("KpiAgendamento_00030")); // NOI18N
        jrbDinamico.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jrbDinamicoActionPerformed(evt);
            }
        });
        pnlOpcaoPeriodo.add(jrbDinamico);
        jrbDinamico.setBounds(10, 30, 110, 23);

        pnlAction_Calculo.add(pnlOpcaoPeriodo);
        pnlOpcaoPeriodo.setBounds(0, 50, 400, 60);

        tsArvore.setCombo(cbbScoreCard);
        pnlAction_Calculo.add(tsArvore);
        tsArvore.setBounds(400, 20, 25, 22);

        pnlCardAction.add(pnlAction_Calculo, "cardCalculo");

        pnlAction_AlertaPlanValoresDiaLimite.setLayout(null);
        pnlAction_AlertaPlanValoresDiaLimite.add(jSeparator1);
        jSeparator1.setBounds(0, 36, 397, 2);
        pnlAction_AlertaPlanValoresDiaLimite.add(jSeparator2);
        jSeparator2.setBounds(0, 83, 397, 11);

        lblMsgDiaLimite.setFont(new Font("Arial", 0, 14));         lblMsgDiaLimite.setText("<html>A data limite para preenchimento ser� calculada a partir do dia<br> limite informado no cadastro do indicador<html>");
        lblMsgDiaLimite.setVerticalAlignment(SwingConstants.TOP);
        pnlAction_AlertaPlanValoresDiaLimite.add(lblMsgDiaLimite);
        lblMsgDiaLimite.setBounds(0, 40, 400, 40);

        pnlCardAction.add(pnlAction_AlertaPlanValoresDiaLimite, "cardAlertaPlanValoresDiaLimite");

        pnlTarefa.add(pnlCardAction);
        pnlCardAction.setBounds(20, 90, 450, 200);

        scpRecord.setViewportView(pnlTarefa);

        pnlFields.add(scpRecord, BorderLayout.CENTER);

        pnlRecord.add(pnlFields, BorderLayout.CENTER);

        tapCadastro.addTab(bundle1.getString("KpiAgendamento_00025"), pnlRecord); // NOI18N

        pnlAgendar.setLayout(null);

        pnlAgendar_Periodo.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(204, 204, 204)), bundle1.getString("KpiAgendamento_00017")));         pnlAgendar_Periodo.setLayout(null);

        lblDataIni.setHorizontalAlignment(SwingConstants.LEFT);
        lblDataIni.setText(bundle.getString("KpiAgendamento_00004")); // NOI18N
        lblDataIni.setMaximumSize(new Dimension(100, 16));
        lblDataIni.setMinimumSize(new Dimension(100, 16));
        lblDataIni.setPreferredSize(new Dimension(100, 16));
        pnlAgendar_Periodo.add(lblDataIni);
        lblDataIni.setBounds(20, 20, 85, 20);

        fldDataIni.setDialog(true);
        fldDataIni.setUseLocale(true);
        fldDataIni.setWaitForCalendarDate(true);
        pnlAgendar_Periodo.add(fldDataIni);
        fldDataIni.setBounds(20, 40, 180, 22);

        fldHorarioIni.setLeadingZero(true);
        fldHorarioIni.setShowSeconds(false);
        fldHorarioIni.setTwelveHours(false);
        pnlAgendar_Periodo.add(fldHorarioIni);
        fldHorarioIni.setBounds(220, 40, 180, 22);

        lblDataFim.setHorizontalAlignment(SwingConstants.LEFT);
        lblDataFim.setText(bundle.getString("KpiAgendamento_00005")); // NOI18N
        lblDataFim.setMaximumSize(new Dimension(100, 16));
        lblDataFim.setMinimumSize(new Dimension(100, 16));
        lblDataFim.setPreferredSize(new Dimension(100, 16));
        pnlAgendar_Periodo.add(lblDataFim);
        lblDataFim.setBounds(20, 62, 85, 20);

        fldDataFim.setDialog(true);
        fldDataFim.setUseLocale(true);
        fldDataFim.setWaitForCalendarDate(true);
        pnlAgendar_Periodo.add(fldDataFim);
        fldDataFim.setBounds(20, 80, 180, 22);

        fldHorarioFim.setLeadingZero(true);
        fldHorarioFim.setShowSeconds(false);
        fldHorarioFim.setTwelveHours(false);
        pnlAgendar_Periodo.add(fldHorarioFim);
        fldHorarioFim.setBounds(220, 80, 180, 22);

        jLabel3.setText(bundle1.getString("KpiAgendamento_00018")); // NOI18N
        pnlAgendar_Periodo.add(jLabel3);
        jLabel3.setBounds(220, 20, 50, 20);

        jLabel4.setText(bundle1.getString("KpiAgendamento_00018")); // NOI18N
        pnlAgendar_Periodo.add(jLabel4);
        jLabel4.setBounds(220, 60, 50, 20);

        pnlAgendar.add(pnlAgendar_Periodo);
        pnlAgendar_Periodo.setBounds(10, 10, 420, 120);

        pnlAgendar_Frequencia.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(204, 204, 204)), bundle1.getString("KpiAgendamento_00019")));         pnlAgendar_Frequencia.setLayout(null);

        grpFrequencia.add(rbDiario);
        rbDiario.setSelected(true);
        rbDiario.setText(bundle1.getString("KpiAgendamento_00020")); // NOI18N
        rbDiario.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        rbDiario.setMargin(new Insets(0, 0, 0, 0));
        rbDiario.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                rbDiarioItemStateChanged(evt);
            }
        });
        pnlAgendar_Frequencia.add(rbDiario);
        rbDiario.setBounds(20, 40, 90, 15);

        grpFrequencia.add(rbSemanal);
        rbSemanal.setText(bundle1.getString("KpiAgendamento_00021")); // NOI18N
        rbSemanal.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        rbSemanal.setMargin(new Insets(0, 0, 0, 0));
        rbSemanal.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                rbSemanalItemStateChanged(evt);
            }
        });
        pnlAgendar_Frequencia.add(rbSemanal);
        rbSemanal.setBounds(20, 60, 90, 15);

        grpFrequencia.add(rbMensal);
        rbMensal.setText(bundle1.getString("KpiAgendamento_00022")); // NOI18N
        rbMensal.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        rbMensal.setMargin(new Insets(0, 0, 0, 0));
        rbMensal.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                rbMensalItemStateChanged(evt);
            }
        });
        pnlAgendar_Frequencia.add(rbMensal);
        rbMensal.setBounds(20, 80, 90, 15);

        fldFreq_HorarioFire.setLeadingZero(true);
        fldFreq_HorarioFire.setShowSeconds(false);
        fldFreq_HorarioFire.setTwelveHours(false);
        pnlAgendar_Frequencia.add(fldFreq_HorarioFire);
        fldFreq_HorarioFire.setBounds(220, 40, 180, 22);
        pnlAgendar_Frequencia.add(cboDia);
        cboDia.setBounds(220, 80, 180, 22);

        spnDia.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                spnDiaStateChanged(evt);
            }
        });
        pnlAgendar_Frequencia.add(spnDia);
        spnDia.setBounds(220, 80, 180, 20);

        lblFreq_Horario.setText(bundle1.getString("KpiAgendamento_00018")); // NOI18N
        pnlAgendar_Frequencia.add(lblFreq_Horario);
        lblFreq_Horario.setBounds(220, 20, 50, 20);

        lblfreq_Dia.setText(bundle1.getString("KpiAgendamento_00026")); // NOI18N
        pnlAgendar_Frequencia.add(lblfreq_Dia);
        lblfreq_Dia.setBounds(220, 60, 50, 20);

        pnlAgendar.add(pnlAgendar_Frequencia);
        pnlAgendar_Frequencia.setBounds(10, 140, 420, 120);

        tapCadastro.addTab(bundle1.getString("KpiAgendamento_00016"), pnlAgendar); // NOI18N

        pnlDetalhes.setLayout(null);

        pnlDetalhe_UltExec.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(204, 204, 204)), bundle1.getString("KpiAgendamento_00023")));         pnlDetalhe_UltExec.setLayout(null);

        lblDataExe1.setHorizontalAlignment(SwingConstants.LEFT);
        lblDataExe1.setText(bundle1.getString("KpiAgendamento_00027")); // NOI18N
        lblDataExe1.setEnabled(false);
        lblDataExe1.setMaximumSize(new Dimension(100, 16));
        lblDataExe1.setMinimumSize(new Dimension(100, 16));
        lblDataExe1.setPreferredSize(new Dimension(100, 16));
        pnlDetalhe_UltExec.add(lblDataExe1);
        lblDataExe1.setBounds(20, 20, 50, 20);

        jLabel6.setText(bundle1.getString("KpiAgendamento_00018")); // NOI18N
        jLabel6.setEnabled(false);
        pnlDetalhe_UltExec.add(jLabel6);
        jLabel6.setBounds(220, 20, 50, 20);

        fldDataExe.setDialog(true);
        fldDataExe.setEnabled(false);
        fldDataExe.setUseLocale(true);
        fldDataExe.setWaitForCalendarDate(true);
        pnlDetalhe_UltExec.add(fldDataExe);
        fldDataExe.setBounds(20, 40, 180, 22);

        fldHorarioExe.setEnabled(false);
        fldHorarioExe.setShowSeconds(false);
        fldHorarioExe.setTwelveHours(false);
        pnlDetalhe_UltExec.add(fldHorarioExe);
        fldHorarioExe.setBounds(220, 40, 180, 22);

        pnlDetalhes.add(pnlDetalhe_UltExec);
        pnlDetalhe_UltExec.setBounds(10, 10, 420, 80);

        pnlDetalhe_ProxExec.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(204, 204, 204)), bundle1.getString("KpiAgendamento_00024")));         pnlDetalhe_ProxExec.setLayout(null);

        lblDataExe2.setHorizontalAlignment(SwingConstants.LEFT);
        lblDataExe2.setText(bundle1.getString("KpiAgendamento_00027")); // NOI18N
        lblDataExe2.setEnabled(false);
        lblDataExe2.setMaximumSize(new Dimension(100, 16));
        lblDataExe2.setMinimumSize(new Dimension(100, 16));
        lblDataExe2.setPreferredSize(new Dimension(100, 16));
        pnlDetalhe_ProxExec.add(lblDataExe2);
        lblDataExe2.setBounds(20, 20, 50, 20);

        fldDataNext.setDialog(true);
        fldDataNext.setEnabled(false);
        fldDataNext.setUseLocale(true);
        fldDataNext.setWaitForCalendarDate(true);
        pnlDetalhe_ProxExec.add(fldDataNext);
        fldDataNext.setBounds(20, 40, 180, 22);

        fldHorarioNext.setEnabled(false);
        fldHorarioNext.setShowSeconds(false);
        fldHorarioNext.setTwelveHours(false);
        pnlDetalhe_ProxExec.add(fldHorarioNext);
        fldHorarioNext.setBounds(220, 40, 180, 22);

        jLabel7.setText(bundle1.getString("KpiAgendamento_00018")); // NOI18N
        jLabel7.setEnabled(false);
        pnlDetalhe_ProxExec.add(jLabel7);
        jLabel7.setBounds(220, 20, 50, 20);

        pnlDetalhes.add(pnlDetalhe_ProxExec);
        pnlDetalhe_ProxExec.setBounds(10, 100, 420, 80);

        tapCadastro.addTab(bundle1.getString("KpiAgendamento_00028"), pnlDetalhes); // NOI18N

        getContentPane().add(tapCadastro, BorderLayout.CENTER);

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 29));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif")));         btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif")));         btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(235, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(225, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif")));         btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif")));         btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif")));         btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightForm, BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void spnDiaLimiteStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spnDiaLimiteStateChanged
            int dia = (Integer) spnDiaLimite.getValue();
            if (dia > 31) {
                spnDiaLimite.setValue(31);
            } else if (dia < 1) {
                spnDiaLimite.setValue(1);
            }
	}//GEN-LAST:event_spnDiaLimiteStateChanged

	private void rbMensalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbMensalItemStateChanged
            regraFrequencia(3);
	}//GEN-LAST:event_rbMensalItemStateChanged

	private void rbSemanalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbSemanalItemStateChanged
            regraFrequencia(2);
	}//GEN-LAST:event_rbSemanalItemStateChanged

	private void rbDiarioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbDiarioItemStateChanged
            regraFrequencia(1);
	}//GEN-LAST:event_rbDiarioItemStateChanged

	private void spnDiaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spnDiaStateChanged
            int dia = (Integer) spnDia.getValue();
            if (dia > 31) {
                spnDia.setValue(31);
            } else if (dia < 1) {
                spnDia.setValue(1);
            }
	}//GEN-LAST:event_spnDiaStateChanged

	private void cbElementoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbElementoItemStateChanged
            if (cbElemento.getSelectedIndex() > -1) {
                if (cbElemento.getSelectedItem().toString().trim().length() > 0) {
                    String comando = "";
                    if (cbAcao.getSelectedIndex() >= 0) {
                        comando = vctComandos.get(cbAcao.getSelectedIndex() - 1).getString("ACAO");
                    }

                    cmdComando.setText(comando + "(" + event.getComboValue(htbElemento, cbElemento).trim() + ")");
                }
            }
	}//GEN-LAST:event_cbElementoItemStateChanged

	private void cbAcaoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbAcaoItemStateChanged
            configuraAcao();
	}//GEN-LAST:event_cbAcaoItemStateChanged

	private void btnLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogActionPerformed
            String serverFolder = "\\logs\\imports\\";
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);

            frmChooser.setFileType(".html");
            frmChooser.loadServerFiles(serverFolder.concat("*.html"));

            frmChooser.setVisible(true);

            frmChooser.setUrlPath("/logs/imports");
            frmChooser.openFile();
	}//GEN-LAST:event_btnLogActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
            event.loadRecord();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

        private void jrbFixoActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jrbFixoActionPerformed
            tipoCalculo();
        }//GEN-LAST:event_jrbFixoActionPerformed

        private void jrbDinamicoActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jrbDinamicoActionPerformed
            tipoCalculo();
        }//GEN-LAST:event_jrbDinamicoActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnLog;
    private JButton btnReload;
    private JButton btnSave;
    private JComboBox cbAcao;
    private JComboBox cbElemento;
    private JComboBox cbbScoreCard;
    private JComboBox cboDia;
    private JBITextArea cmdComando;
    private JPVEdit fldAmbiente;
    private JPVDatePlus fldDataCalcAte;
    private JPVDatePlus fldDataCalcDe;
    private JPVDatePlus fldDataExe;
    private JPVDatePlus fldDataFim;
    private JPVDatePlus fldDataIni;
    private JPVDatePlus fldDataNext;
    private JPVEdit fldDiretorio;
    private JPVTime fldFreq_HorarioFire;
    private JPVTime fldHorarioExe;
    private JPVTime fldHorarioFim;
    private JPVTime fldHorarioIni;
    private JPVTime fldHorarioNext;
    private JPVEdit fldNome;
    private ButtonGroup grpCalculo;
    private ButtonGroup grpFrequencia;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JSeparator jSeparator1;
    private JSeparator jSeparator2;
    private JRadioButton jrbDinamico;
    private JRadioButton jrbFixo;
    private JLabel lblAcao;
    private JLabel lblAmbiente;
    private JLabel lblComando;
    private JLabel lblDataAte;
    private JLabel lblDataDe;
    private JLabel lblDataExe1;
    private JLabel lblDataExe2;
    private JLabel lblDataFim;
    private JLabel lblDataIni;
    private JLabel lblDiaLimite;
    private JLabel lblDiretorio;
    private JLabel lblElemento;
    private JLabel lblFreq_Horario;
    private JLabel lblMsgDiaLimite;
    private JLabel lblNome;
    private JLabel lblScoreCard;
    private JLabel lblfreq_Dia;
    private JPanel pnlAction_AlertaPlanValoresDiaLimite;
    private JPanel pnlAction_AlertaPlanilhaValores;
    private JPanel pnlAction_Calculo;
    private JPanel pnlAction_FuncaoAdvpl;
    private JPanel pnlAction_ImportacaoDeDados;
    private JPanel pnlAction_ImportacaoFonteDados;
    private JPanel pnlAction_NotificacaoPlanosAtrasados;
    private JPanel pnlAgendar;
    private JPanel pnlAgendar_Frequencia;
    private JPanel pnlAgendar_Periodo;
    private JPanel pnlBottomForm;
    private JPanel pnlCardAction;
    private JPanel pnlConfirmation;
    private JPanel pnlDetalhe_ProxExec;
    private JPanel pnlDetalhe_UltExec;
    private JPanel pnlDetalhes;
    private JPanel pnlDinamico;
    private JPanel pnlFields;
    private JPanel pnlFixo;
    private JPanel pnlLeftForm;
    private JPanel pnlOpcaoPeriodo;
    private JPanel pnlOperation;
    private JPanel pnlPeriodo;
    private JPanel pnlRecord;
    private JPanel pnlRightForm;
    private JPanel pnlTarefa;
    private JPanel pnlTools;
    private JRadioButton rbDiario;
    private JRadioButton rbMensal;
    private JRadioButton rbSemanal;
    private JScrollPane scpRecord;
    private JScrollPane scrComando;
    private JSpinner spnDia;
    private JSpinner spnDiaLimite;
    private JSpinner spnMais;
    private JSpinner spnMenos;
    private JTabbedPane tapCadastro;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JBITreeSelection tsArvore;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        btnSave.setEnabled(true);
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        return "";
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void afterExecute() {
    }
}
