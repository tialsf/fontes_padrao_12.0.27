package kpi.swing.tools;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.beans.PropertyVetoException;
import java.util.Hashtable;
import java.util.ResourceBundle;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import kpi.applet.KpiMainPanel;
import kpi.beans.JBIProgressBar;
import kpi.beans.JBITextArea;
import kpi.beans.JBITreeSelection;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiFileChooser;
import kpi.util.KpiDateUtil;
import kpi.xml.BIXMLRecord;
import pv.jfcx.JPVDatePlus;

public class KpiCalcIndicador extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions, kpi.beans.JBIProgressBarListener {

    private static final int EXECUTANDO = 1;
    private static final int FINALIZADO = 2;
    //
    private KpiDateUtil calendario = new KpiDateUtil();
    private String recordType = "CALC_INDICADOR";
    private String formType = recordType;//Tipo do Formulario
    private String parentType = recordType;//Tipo formulario pai, caso haja.
    private Hashtable htbScoreCard = new Hashtable();
    private KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private int status = KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiCalcIndicador(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        biProgressbar.addJBIProgressBarListener(this);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(barCalculo, true);
        event.setEnableFields(pnlData, true);
        btnCancelar.setEnabled(false);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(barCalculo, false);
        event.setEnableFields(pnlData, false);
        btnCancelar.setEnabled(true);
    }

    @Override
    public void refreshFields() {
        event.populateCombo(record.getBIXMLVector("SCORECARDS"), "SCOR_ID", htbScoreCard, cbbScoreCard);
        tsArvore.setVetor(record.getBIXMLVector("SCORECARDS"));
        fldDataCalcDe.setCalendar(record.getDate("DATADE"));
        fldDataCalcAte.setCalendar(record.getDate("DATAATE"));
        setMessageStatus(record.getInt("STATUS"));
    }

    @Override
    public BIXMLRecord getFieldsContents() {
        setMessageStatus(1);
        return new BIXMLRecord(recordType);
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        int dataDe = fldDataCalcAte.getCalendar().get(java.util.Calendar.YEAR);
        int dataAte = fldDataCalcDe.getCalendar().get(java.util.Calendar.YEAR);

        if (dataDe != dataAte) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiCalcIndicador_00001"));
            retorno = false;
        } else if (cbbScoreCard.getSelectedIndex() <= 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00004"));
            retorno = false;
        }

        return retorno;
    }

    /**
     * Par�metros da requisi��o de c�lculo.
     * @return String de par�metros
     */
    private String createRequest() {
        StringBuilder parametros = new StringBuilder();
        parametros.append(event.getComboValue(htbScoreCard, cbbScoreCard, false));
        parametros.append("|");
        parametros.append(calendario.getCalendarString(fldDataCalcDe.getCalendar()));
        parametros.append("|");
        parametros.append(calendario.getCalendarString(fldDataCalcAte.getCalendar()));
        parametros.append("|");
        parametros.append("T");

        return parametros.toString();
    }

    /**
     * Gerencia o status da execu��o do c�lculo.
     * @param mensagem status da execu��o. 
     */
    private void setMessageStatus(int mensagem) {
        if (mensagem == EXECUTANDO) {
            disableFields();
            startProgressBar();
            txtMesangemCalculo.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiCalcIndicador_00002"));
            lblSemaforo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_semaforo_verde.gif")));

        } else if (mensagem == FINALIZADO) {
            txtMesangemCalculo.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiCalcIndicador_00002"));
            lblSemaforo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_semaforo_amarelo.gif")));

        } else {
            enableFields();
            setCloseEnabled(true);
            txtMesangemCalculo.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiCalcIndicador_00006"));
            lblSemaforo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_semaforo_vermelho.gif")));
        }
    }
    private void initComponents() {//GEN-BEGIN:initComponents

        lblSemaforo = new JLabel();
        tapCadastro = new JTabbedPane();
        pnlCalculo = new JPanel();
        pnlData = new JPanel();
        lblDataDe = new JLabel();
        scrDataCalcDe = new JScrollPane();
        fldDataCalcDe = new JPVDatePlus();
        lblDataAte = new JLabel();
        scrDataCalcAte = new JScrollPane();
        fldDataCalcAte = new JPVDatePlus();
        lblScoreCard = new JLabel();
        cbbScoreCard = new JComboBox();
        txtMesangemCalculo = new JBITextArea();
        tsArvore = new JBITreeSelection();
        biProgressbar = new JBIProgressBar();
        barCalculo = new JToolBar();
        btnCalcular = new JButton();
        btnCancelar = new JButton();
        btnStatus = new JButton();
        btnLog = new JButton();
        btnAjuda = new JButton();
        pnlRightForm = new JPanel();
        pnlBottomForm = new JPanel();
        pnlLeftForm = new JPanel();

        lblSemaforo.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_semaforo_vermelho.gif"))); // NOI18N

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiCalcIndicador_00007")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_calculo.gif"))); // NOI18N
        setMinimumSize(new Dimension(300, 300));
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(454, 325));
        setRequestFocusEnabled(false);
        try {
            setSelected(true);
        } catch (PropertyVetoException e1) {
            e1.printStackTrace();
        }

        tapCadastro.setPreferredSize(new Dimension(530, 300));

        pnlCalculo.setLayout(new BorderLayout());

        pnlData.setLayout(null);

        lblDataDe.setText(bundle.getString("KpiCalcIndicador_00010")); // NOI18N
        lblDataDe.setPreferredSize(new Dimension(80, 15));
        pnlData.add(lblDataDe);
        lblDataDe.setBounds(20, 55, 80, 15);

        scrDataCalcDe.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataCalcDe.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataCalcDe.setAllowNull(false);
        fldDataCalcDe.setBorderStyle(0);
        fldDataCalcDe.setButtonBorder(0);
        fldDataCalcDe.setDialog(true);
        fldDataCalcDe.setUseLocale(true);
        fldDataCalcDe.setWaitForCalendarDate(true);
        scrDataCalcDe.setViewportView(fldDataCalcDe);

        pnlData.add(scrDataCalcDe);
        scrDataCalcDe.setBounds(20, 70, 180, 22);

        lblDataAte.setText(bundle.getString("KpiCalcIndicador_00011")); // NOI18N
        lblDataAte.setPreferredSize(new Dimension(30, 15));
        pnlData.add(lblDataAte);
        lblDataAte.setBounds(240, 55, 30, 15);

        scrDataCalcAte.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataCalcAte.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataCalcAte.setAllowNull(false);
        fldDataCalcAte.setBorderStyle(0);
        fldDataCalcAte.setButtonBorder(0);
        fldDataCalcAte.setDialog(true);
        fldDataCalcAte.setUseLocale(true);
        fldDataCalcAte.setWaitForCalendarDate(true);
        scrDataCalcAte.setViewportView(fldDataCalcAte);

        pnlData.add(scrDataCalcAte);
        scrDataCalcAte.setBounds(240, 70, 180, 22);

        lblScoreCard.setText(KpiStaticReferences.getKpiCustomLabels().getSco().concat(":"));
        lblScoreCard.setPreferredSize(new Dimension(80, 15));
        pnlData.add(lblScoreCard);
        lblScoreCard.setBounds(20, 10, 310, 20);
        pnlData.add(cbbScoreCard);
        cbbScoreCard.setBounds(20, 30, 400, 22);

        txtMesangemCalculo.setEditable(false);
        txtMesangemCalculo.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(204, 204, 204)), bundle.getString("KpiCalcIndicador_00017"), TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION)); // NOI18N
        txtMesangemCalculo.setFont(new Font("Tahoma", 0, 11));
        txtMesangemCalculo.setOpaque(false);
        pnlData.add(txtMesangemCalculo);
        txtMesangemCalculo.setBounds(20, 110, 400, 80);

        tsArvore.setCombo(cbbScoreCard);
        pnlData.add(tsArvore);
        tsArvore.setBounds(420, 30, 25, 22);

        pnlCalculo.add(pnlData, BorderLayout.CENTER);
        pnlCalculo.add(biProgressbar, BorderLayout.SOUTH);

        tapCadastro.addTab(bundle.getString("KpiCalcIndicador_00008"), pnlCalculo); // NOI18N

        getContentPane().add(tapCadastro, BorderLayout.CENTER);

        barCalculo.setFloatable(false);
        barCalculo.setRollover(true);

        btnCalcular.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_play.gif"))); // NOI18N
        btnCalcular.setText(bundle.getString("KpiCalcIndicador_00013")); // NOI18N
        btnCalcular.setPreferredSize(new Dimension(80, 23));
        btnCalcular.setVerticalAlignment(SwingConstants.BOTTOM);
        btnCalcular.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        barCalculo.add(btnCalcular);

        btnCancelar.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_parar.gif"))); // NOI18N
        btnCancelar.setText(bundle.getString("KpiCalcIndicador_00015")); // NOI18N
        btnCancelar.setPreferredSize(new Dimension(80, 23));
        btnCancelar.setVerticalAlignment(SwingConstants.BOTTOM);
        btnCancelar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        barCalculo.add(btnCancelar);

        btnStatus.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnStatus.setText(bundle.getString("KpiCalcIndicador_00014")); // NOI18N
        btnStatus.setPreferredSize(new Dimension(110, 23));
        btnStatus.setVerticalAlignment(SwingConstants.BOTTOM);
        btnStatus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnStatusActionPerformed(evt);
            }
        });
        barCalculo.add(btnStatus);

        btnLog.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_log.gif"))); // NOI18N
        btnLog.setText(bundle.getString("KpiCalcIndicador_00016")); // NOI18N
        btnLog.setPreferredSize(new Dimension(80, 23));
        btnLog.setVerticalAlignment(SwingConstants.BOTTOM);
        btnLog.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnLogActionPerformed(evt);
            }
        });
        barCalculo.add(btnLog);

        btnAjuda.setFont(new Font("Dialog", 0, 12));
        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        barCalculo.add(btnAjuda);

        getContentPane().add(barCalculo, BorderLayout.NORTH);
        getContentPane().add(pnlRightForm, BorderLayout.EAST);
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pack();
    }//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
            setMessageStatus(3);
            event.executeRecord("JOBSTOP");
	}//GEN-LAST:event_btnCancelarActionPerformed

	private void btnLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogActionPerformed
            String serverFolder = "\\logs\\calcs\\";
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            frmChooser.setFileType(".html");
            frmChooser.loadServerFiles(serverFolder.concat("*.html"));
            frmChooser.setVisible(true);
            frmChooser.setUrlPath("/logs/calcs/");
            frmChooser.openFile();
	}//GEN-LAST:event_btnLogActionPerformed

	private void btnStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStatusActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnStatusActionPerformed

	private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
            event.executeRecord(createRequest());
	}//GEN-LAST:event_btnCalcularActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JToolBar barCalculo;
    private JBIProgressBar biProgressbar;
    private JButton btnAjuda;
    private JButton btnCalcular;
    private JButton btnCancelar;
    private JButton btnLog;
    private JButton btnStatus;
    private JComboBox cbbScoreCard;
    private JPVDatePlus fldDataCalcAte;
    private JPVDatePlus fldDataCalcDe;
    private JLabel lblDataAte;
    private JLabel lblDataDe;
    private JLabel lblScoreCard;
    private JLabel lblSemaforo;
    private JPanel pnlBottomForm;
    private JPanel pnlCalculo;
    private JPanel pnlData;
    private JPanel pnlLeftForm;
    private JPanel pnlRightForm;
    private JScrollPane scrDataCalcAte;
    private JScrollPane scrDataCalcDe;
    private JTabbedPane tapCadastro;
    private JBITreeSelection tsArvore;
    private JBITextArea txtMesangemCalculo;
    // End of variables declaration//GEN-END:variables

    private void startProgressBar() {
        biProgressbar.setJobName("indcalc_1");
        biProgressbar.startProgressBar();
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void afterExecute() {
    }

    @Override
    public void onStartBar(ComponentEvent componentEvent) {
        KpiStaticReferences.getKpiMainPanel().setWorkStatus(KpiMainPanel.WORKST_RED);
    }

    @Override
    public void onFinishBar(ComponentEvent componentEvent) {
        setMessageStatus(0);
        KpiStaticReferences.getKpiMainPanel().refreshWorkStatus();
    }

    @Override
    public void onErrorBar(ComponentEvent componentEvent) {
        setMessageStatus(0);
        KpiStaticReferences.getKpiMainPanel().refreshWorkStatus();
    }

    @Override
    public void onCancelBar(ComponentEvent componentEvent) {
        setMessageStatus(0);
        KpiStaticReferences.getKpiMainPanel().refreshWorkStatus();
    }
}
