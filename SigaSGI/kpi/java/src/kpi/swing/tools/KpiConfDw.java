package kpi.swing.tools;

public class KpiConfDw extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String recordType = new String("DW_CONF");
    private String formType = recordType;
    private String parentType = recordType;

    @Override
    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblConta.setEnabled(true);
        fldWsdl.setEnabled(true);
        lblSMTP.setEnabled(true);
        fldUrl.setEnabled(true);
        lblUsuario.setEnabled(true);
        fldUsuario.setEnabled(true);
        lblSenha.setEnabled(true);
        fldSenha.setEnabled(true);
    }

    @Override
    public void disableFields() {
        // Desabilita os campos do formul�rio.
        lblConta.setEnabled(false);
        fldWsdl.setEnabled(false);
        lblSMTP.setEnabled(false);
        fldUrl.setEnabled(false);
        lblUsuario.setEnabled(false);
        fldUsuario.setEnabled(false);
        lblSenha.setEnabled(false);
        fldSenha.setEnabled(false);
    }
    java.util.Hashtable htbResponsavel = new java.util.Hashtable();

    @Override
    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        fldWsdl.setText(record.getString("WS_DW_INTEGRATION"));
        fldUrl.setText(record.getString("DW_URL"));
        fldUsuario.setText(record.getString("DW_USER"));
        fldSenha.setText(record.getString("DW_PWD"));

    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.
        recordAux.set("WS_DW_INTEGRATION", fldWsdl.getText());
        recordAux.set("DW_URL", fldUrl.getText());
        recordAux.set("DW_USER", fldUsuario.getText());
        recordAux.set("DW_PWD", fldSenha.getText());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        return retorno;
    }

    public boolean hasCombos() {
        return false;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnTestConection = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblConta = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        lblSMTP = new javax.swing.JLabel();
        fldWsdl = new pv.jfcx.JPVEdit();
        lblSenha = new javax.swing.JLabel();
        fldUrl = new pv.jfcx.JPVEdit();
        fldUsuario = new pv.jfcx.JPVEdit();
        fldSenha = new pv.jfcx.JPVPassword();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiConfDw_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_dw.gif"))); // NOI18N
        setName(""); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 105, 132));
        setPreferredSize(new java.awt.Dimension(459, 251));

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 30));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(340, 30));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new java.awt.Dimension(240, 30));
        tbInsertion.setPreferredSize(new java.awt.Dimension(300, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnTestConection.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_play.gif"))); // NOI18N
        btnTestConection.setText(bundle.getString("KpiConfDw_00002")); // NOI18N
        btnTestConection.setFocusable(false);
        btnTestConection.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnTestConection.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnTestConection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestConectionActionPerformed(evt);
            }
        });
        tbInsertion.add(btnTestConection);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.setFocusable(false);
        btnAjuda.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        tapCadastro.setMinimumSize(new java.awt.Dimension(100, 100));
        tapCadastro.setPreferredSize(new java.awt.Dimension(530, 300));

        pnlFields.setPreferredSize(new java.awt.Dimension(400, 350));
        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(500, 400));

        pnlData.setPreferredSize(new java.awt.Dimension(100, 100));
        pnlData.setLayout(null);

        lblConta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblConta.setText(bundle.getString("KpiConfDw_00003")); // NOI18N
        lblConta.setEnabled(false);
        pnlData.add(lblConta);
        lblConta.setBounds(10, 10, 90, 20);

        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUsuario.setText(bundle.getString("KpiConfDw_00005")); // NOI18N
        lblUsuario.setEnabled(false);
        pnlData.add(lblUsuario);
        lblUsuario.setBounds(10, 90, 90, 20);

        lblSMTP.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSMTP.setText(bundle.getString("KpiConfDw_00004")); // NOI18N
        lblSMTP.setEnabled(false);
        pnlData.add(lblSMTP);
        lblSMTP.setBounds(10, 50, 90, 20);

        fldWsdl.setMaxLength(60);
        pnlData.add(fldWsdl);
        fldWsdl.setBounds(10, 30, 400, 22);

        lblSenha.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSenha.setText(bundle.getString("KpiConfDw_00006")); // NOI18N
        lblSenha.setEnabled(false);
        pnlData.add(lblSenha);
        lblSenha.setBounds(230, 90, 90, 20);

        fldUrl.setMaxLength(60);
        pnlData.add(fldUrl);
        fldUrl.setBounds(10, 70, 400, 22);

        fldUsuario.setMaxLength(60);
        pnlData.add(fldUsuario);
        fldUsuario.setBounds(10, 110, 180, 22);

        fldSenha.setCase(0);
        pnlData.add(fldSenha);
        fldSenha.setBounds(230, 110, 180, 22);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiConfDw_00001"), pnlFields); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        tapCadastro.getAccessibleContext().setAccessibleName("Mensagens");
        tapCadastro.getAccessibleContext().setAccessibleDescription("Mensagens");

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        event.saveRecord();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        event.cancelOperation();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        event.editRecord();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
        event.loadRecord();
    }//GEN-LAST:event_btnReloadActionPerformed

	private void btnTestConectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestConectionActionPerformed
            event.executeRecord("");
}//GEN-LAST:event_btnTestConectionActionPerformed

    private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
        event.loadHelp(recordType);
}//GEN-LAST:event_btnAjudaActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnTestConection;
    private pv.jfcx.JPVPassword fldSenha;
    private pv.jfcx.JPVEdit fldUrl;
    private pv.jfcx.JPVEdit fldUsuario;
    private pv.jfcx.JPVEdit fldWsdl;
    private javax.swing.JLabel lblConta;
    private javax.swing.JLabel lblSMTP;
    private javax.swing.JLabel lblSenha;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event =
            new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiConfDw(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public void afterExecute() {
    }
}
