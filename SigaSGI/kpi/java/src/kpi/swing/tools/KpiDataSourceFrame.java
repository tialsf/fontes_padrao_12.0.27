package kpi.swing.tools;

import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.util.Hashtable;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiFileChooser;
import kpi.xml.BIXMLVector;

public class KpiDataSourceFrame extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions, kpi.beans.JBIProgressBarListener {

    public final static int KPI_SRC_ENVIRONMENT = 1;
    public final static int KPI_SRC_CUSTOM = 2;
    public final static int KPI_SRC_TOP = 0;
    public final static int KPI_SRC_ADVPL = 1;
    public final static int KPI_SRC_DW = 2;
    public final static int KPI_SRC_FORMULA = 3;
    private String recordType = "DATASRC";
    private String formType = recordType;
    private String parentType = "LSTDATASRC";
    private String idConsultaDW = "";
    private Hashtable htbDataWareHouse = new Hashtable();
    private Hashtable htbIndIDs = new Hashtable();
    private Hashtable htbIndCodIDs = new Hashtable();
    private Hashtable htbCampos = new Hashtable();
    private Hashtable htbReal = new Hashtable();
    private Hashtable htbMeta = new Hashtable();
    private Hashtable htbPrevia = new Hashtable();
    private Hashtable htbConsultas = new Hashtable();
    private Hashtable htbScorecard = new Hashtable();
    private Hashtable htbIndDW = new Hashtable();
    private Hashtable htbClasse = new Hashtable();
    private BIXMLVector vctConsultaSel = null;
    private boolean isEnable = false;
    private boolean connectedDW = false;
    private kpi.xml.BIXMLRecord recDataWareHouse = null;
    private final String conexSrv[][] = {{"", ""},
	{"TCPIP", "TCP/IP"},
	{"LOCAL", "Local"},
	{"APPC", "APPC"},
	{"BRIDGE", "Bridge"},
	{"NPIPE", "NPipe"}};
    private final String conexBco[][] = {{"", ""},
	{"MSSQL", "Sql Server"},
	{"ORACLE", "Oracle"},
	{"SYBASE", "Sybase"},
	{"INFORMIX", "Informix"},
	{"POSTGRES", "Postgres"},
	{"MYSQL", "MySQL"},
	{"DB2", "DB2"},
	{"MSADO", "MS ADO"}};

    public KpiDataSourceFrame(int operation, String idAux, String contextId) {
	initComponents();
	putClientProperty("MAXI", true);
	biProgressbar.addJBIProgressBarListener(this);
	event.defaultConstructor(operation, idAux, contextId);
	radAmbiente.setSelected(true);
    }

    @Override
    public void enableFields() {
	isEnable = true;
	event.setEnableFields(pnlPrincipal, true);
	event.setEnableFields(pnlConfiguracao, true);
	event.setEnableFields(pnlDeclaracao, true);
	event.setEnableFields(pnlIndicadores, true);
	if (cbbClasse.getSelectedIndex() == KPI_SRC_DW) {
	    radAmbiente.setSelected(true);
	    radTopConnect.setEnabled(false);
	}
	enableTopFields();
    }

    @Override
    public void disableFields() {
	isEnable = false;
	event.setEnableFields(pnlPrincipal, false);
	event.setEnableFields(pnlConfiguracao, false);
	event.setEnableFields(pnlDeclaracao, false);
	event.setEnableFields(pnlIndicadores, false);
	enableDWFields(false);
    }

    /**
     * Regra de exibi��o para os campos do DW.
     * @param enable estado dos campos.
     */
    private void enableDWFields(boolean enable) {
	cbbConsultas.setEnabled(enable);
	cbbDataWareHouse.setEnabled(enable);
	cbbRealDW.setEnabled(enable);
	cbbConsultas.setEnabled(enable);
	cbbDataDW.setEnabled(enable);
	cbbRealDW.setEnabled(enable);
	cbbMetaDW.setEnabled(enable);
	cbbPreviaDW.setEnabled(enable);
    }

    /**
     * Define a regra de exibi��o de campos do TOP.
     */
    private void enableTopFields() {
	pnlDbAccess.setVisible(false);
	pnlProtheus.setVisible(false);
	pnlSigaDw.setVisible(false);

	//ADVPL
	if (cbbClasse.getSelectedIndex() == KPI_SRC_ADVPL) {
	    pnlProtheus.setVisible(true);
	    radAmbiente.setSelected(true);
	    chkPrepareEnv.setVisible(true);
	    if (isEnable) {
		radAmbiente.setEnabled(true);
		radTopConnect.setEnabled(false);
	    }
	    pnlProtheus.setVisible(true);
	    for (int i = 0; i < pnlProtheus.getComponentCount(); i++) {
		pnlProtheus.getComponent(i).setVisible(true);
	    }
	    //DW
	} else if (cbbClasse.getSelectedIndex() == KPI_SRC_DW) {
	    pnlSigaDw.setVisible(true);
	    chkPrepareEnv.setVisible(false);
	    if (isEnable) {

		radAmbiente.setEnabled(false);
		radTopConnect.setEnabled(false);
		radTopConnect.setSelected(true);
	    }
	    //TOP
	} else {
	    chkPrepareEnv.setVisible(false);
	    if (isEnable) {
		radAmbiente.setEnabled(true);
		radTopConnect.setEnabled(true);
	    }

	    if (radAmbiente.isSelected()) {
		pnlProtheus.setVisible(true);
		for (int i = 0; i < pnlProtheus.getComponentCount(); i++) {
		    pnlProtheus.getComponent(i).setVisible(false);
		}
		fldAmbiente.setVisible(true);
		lblAmbiente.setVisible(true);
	    } else {
		pnlDbAccess.setVisible(true);
	    }
	}

	if (cbbClasse.getSelectedIndex() == KPI_SRC_DW && tbbDataSource.indexOfComponent(pnlDeclaracao) >= 0) {
	    tbbDataSource.remove(pnlDeclaracao);
	    tbbDataSource.addTab("Indicadores", pnlIndicadores);
	    tbbDataSource.doLayout();
	} else if (cbbClasse.getSelectedIndex() != KPI_SRC_DW && tbbDataSource.indexOfComponent(pnlDeclaracao) == -1) {
	    tbbDataSource.addTab(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDataSourceFrame_00017"), pnlDeclaracao);
	    tbbDataSource.remove(pnlIndicadores);
	    tbbDataSource.doLayout();
	} else if (cbbClasse.getSelectedIndex() != KPI_SRC_DW && tbbDataSource.indexOfComponent(pnlIndicadores) >= 0) {
	    tbbDataSource.addTab(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDataSourceFrame_00017"), pnlDeclaracao);
	    tbbDataSource.remove(pnlIndicadores);
	    tbbDataSource.doLayout();
	}
    }

    /**
     * Monta a declara��o de exemplo para as diferentes tipos de fontes de dados.
     * @param type tipo da fonte de dados.
     */
    private void buildDefaultText(int type) {
	StringBuilder sText = new StringBuilder();
	if (type == KPI_SRC_ADVPL) {
	    sText.append("u_aFuncion()\n\n");
	} else {
	    sText.append("SELECT\n");
	    sText.append("'0000000000'      AS ID,         -- Id do indicador");
	    sText.append("\n");
	    sText.append("'COD. IMPORTA��O' AS USERID,     -- C�digo do cliente");
	    sText.append("\n");
	    sText.append("'YYYYMMDD'        AS DATA,       -- Data");
	    sText.append("\n");
	    sText.append("'00000.00'        AS VALOR,      -- Realizado");
	    sText.append("\n");
	    sText.append("'00000.00'        AS META,       -- Meta");
	    sText.append("\n");
	    sText.append("'00000.00'        AS PREVIA      -- Pr�via");
	    sText.append("\n");
	    sText.append("FROM\n<Tabela>\nGROUP BY DATA\n\n");
	    sText.append("-- ******************* ");
	    sText.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDataSourceFrame_00048").toUpperCase());//importante
	    sText.append(" *********************\n");
	    sText.append("-- *");
	    sText.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDataSourceFrame_00049"));//"Os valores n�o poder�o conter separador de milhares"
	    sText.append("\n--\n-- *");
	    sText.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDataSourceFrame_00050"));//"Formato da data dever� ser yyyymmdd"
	    sText.append("\n--\n-- *");
	    sText.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDataSourceFrame_00051"));//"Campo PREVIA � opcional"
	    sText.append("\n--\n-- *");
	    sText.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDataSourceFrame_00052"));//"Caso o campo ID e USERID sejam informados prevalecer� o campo USERID"
	    sText.append("\n-- ****************************************************");
	}

	txtDeclaracoes.setText(sText.toString());
    }

    @Override
    public void refreshFields() {
	if (!record.getBoolean("VALOR_PREVIO")) {
	    lblPreviaDW.setVisible(false);
	    cbbPreviaDW.setVisible(false);
	    fldPreviaDWSel.setVisible(false);
	}

	populateCboBanco();
	populateCboServer();

	fldNome.setText(record.getString("NOME"));
	txtDescricao.setText(record.getString("DESCRICAO"));
	event.populateCombo(record.getBIXMLVector("CLASSES"), "CLASSE", htbClasse, cbbClasse);
	cbbClasse.removeItemAt(0);
	cbbClasse.setSelectedIndex(record.getInt("CLASSE"));
	event.populateCombo(record.getBIXMLVector("SCORECARDS"), "ID_SCOREC", htbScorecard, cbScorecard);
	tsArvore.setVetor(record.getBIXMLVector("SCORECARDS"));
	event.selectComboItem(cbScorecard, event.getComboItem(record.getBIXMLVector("SCORECARDS"), record.getString("ID_SCOREC"), true));

	//Page (Declara��es)
	txtDeclaracoes.setText(record.getString("TEXTO"));
	chkLimpaPlanilha.setSelected(record.getBoolean("LIMPEZA"));
	rdPeriodoEspecifico.setSelected(record.getBoolean("ESPECIFI"));
	txtData.setText(record.getString("PERIODO"));

	//Page (Configura��es)
	if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
	    radAmbiente.setSelected(true);
	    buildDefaultText(KPI_SRC_TOP);
	} else {
	    boolean ambSelected = record.getInt("TIPOENV") == KPI_SRC_ADVPL;
	    if (ambSelected) {
		radAmbiente.setSelected(true);
		radTopConnect.setSelected(false);
	    } else {
		radAmbiente.setSelected(false);
		radTopConnect.setSelected(true);
	    }
	}

	setCboBancoValue(record.getString("TOPDB"));
	fldAlias.setText(record.getString("TOPALIAS"));
	fldServer.setText(record.getString("TOPSERVER"));
	setCboServerValue(record.getString("TOPCONTYPE"));
	fldAmbiente.setText(record.getString("ENVIRON"));
	fldServidor.setText(record.getString("SERVIDOR"));
	fldPorta.setText(record.getString("PORTA"));
	fldEmpresa.setText(record.getString("EMPRESA"));
	fldFilial.setText(record.getString("FILIAL"));
	chkPrepareEnv.setSelected(record.getBoolean("PREPENV"));

	//Integra��o com o DW
	fldUrlDW.setText(record.getString("DW_URL"));//Url de conex�o com DW
	fldDWSel.setText(record.getString("DW_NOME"));//DW Selecionada.
	fldDwConsultaSel.setText(record.getString("DW_CON"));//Consulta Selecionada.
	fldDWDataSel.setText(record.getString("DW_DATA"));//Coluna com a Data.
	fldRealDWSel.setText(record.getString("DW_INDREAL"));//Coluna com o Indicador Real.
	fldMetaDWSel.setText(record.getString("DW_INDMETA"));//Coluna com o Indicador Meta.
	fldPreviaDWSel.setText(record.getString("DW_INDPREV"));//Coluna com o Indicador Previa.
	
	fldDWIndSel.setText(record.getString("DW_INDID"));
	fldCodImpInd.setText(record.getString("DW_INDIDIP"));

	event.populateCombo(record.getBIXMLVector("SCORECARDS"), "ID_SCOREC", htbScorecard, cbbScorecard);
	tsArvoreScorecard.setVetor(record.getBIXMLVector("SCORECARDS"));

	idConsultaDW = record.getString("DW_IDCON");

	enableTopFields();

	if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
	    tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
	    tsArvoreScorecard.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);

	}
    }

    /**
     *
     */
    private void populateCboBanco() {
	cboDataBase.removeAllItems();
	for (int i = 0; i < conexBco.length; i++) {
	    cboDataBase.addItem(conexBco[i][1]);
	}
    }

    /**
     *
     * @return
     */
    private String getCboBancoValue() {
	return conexBco[cboDataBase.getSelectedIndex()][0];
    }

    /**
     *
     * @param value (String)
     */
    private void setCboBancoValue(String value) {
	for (int i = 0; i < conexBco.length; i++) {
	    if (conexBco[i][0].equals(value)) {
		cboDataBase.setSelectedIndex(i);
		break;
	    }
	}
    }

    /**
     *
     */
    private void populateCboServer() {
	cboConType.removeAllItems();
	for (int i = 0; i < conexSrv.length; i++) {
	    cboConType.addItem(conexSrv[i][1]);
	}
    }

    /**
     *
     * @return
     */
    private String getCboServerValue() {
	return conexSrv[cboConType.getSelectedIndex()][0];
    }

    /**
     *
     * @param value
     */
    private void setCboServerValue(String value) {
	for (int i = 0; i < conexSrv.length; i++) {
	    if (conexSrv[i][0].equals(value)) {
		cboConType.setSelectedIndex(i);
		break;
	    }
	}
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
	kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
	//Page (Principal)
	recordAux.set("ID", id);
	recordAux.set("NOME", fldNome.getText());
	recordAux.set("DESCRICAO", txtDescricao.getText());
	recordAux.set("CLASSE", cbbClasse.getSelectedIndex());
	recordAux.set("PREPENV", chkPrepareEnv.isSelected());

	//Page (Configura��es)
	recordAux.set("TIPOENV", (radAmbiente.isSelected() ? KPI_SRC_ENVIRONMENT : KPI_SRC_CUSTOM));
	recordAux.set("TOPDB", getCboBancoValue());
	recordAux.set("TOPALIAS", fldAlias.getText());
	recordAux.set("TOPSERVER", fldServer.getText());
	recordAux.set("TOPCONTYPE", getCboServerValue());

	recordAux.set("ENVIRON", fldAmbiente.getText());
	recordAux.set("SERVIDOR", fldServidor.getText());
	recordAux.set("PORTA", fldPorta.getText());
	recordAux.set("EMPRESA", fldEmpresa.getText());
	recordAux.set("FILIAL", fldFilial.getText());

	//Integracao com DW.
	recordAux.set("DW_URL", fldUrlDW.getText());//Url de conex�o com DW
	recordAux.set("DW_NOME", fldDWSel.getText());//DW Selecionada.
	recordAux.set("DW_CON", fldDwConsultaSel.getText());//Consulta Selecionada.
	recordAux.set("DW_DATA", fldDWDataSel.getText());//Coluna com a Data.
	recordAux.set("DW_INDREAL", fldRealDWSel.getText());//Coluna com o Indicador.
	recordAux.set("DW_INDMETA", fldMetaDWSel.getText());//Coluna com o Indicador.
	recordAux.set("DW_INDPREV", fldPreviaDWSel.getText());//Coluna com o Indicador.
	//
	recordAux.set("DW_INDID", fldDWIndSel.getText());//Coluna com o Indicador.
	recordAux.set("DW_INDIDIP", fldCodImpInd.getText());//Coluna com o Indicador.
	recordAux.set("SGI_IND", getID_IndSGI());

	String tmpIDCon = event.getComboValue(htbConsultas, cbbConsultas);
	if (tmpIDCon.trim().length() == 0 || tmpIDCon.equals("0")) {
	    tmpIDCon = idConsultaDW;
	}

	//Page (Declara��es)
	recordAux.set("DW_IDCON", tmpIDCon);
	recordAux.set("TEXTO", txtDeclaracoes.getText());
	recordAux.set("LIMPEZA", chkLimpaPlanilha.isSelected());
	recordAux.set("ID_SCOREC", event.getComboValue(htbScorecard, cbScorecard));
	recordAux.set("ESPECIFI", rdPeriodoEspecifico.isSelected());
	recordAux.set("PERIODO", txtData.getCalendar());

	return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
	String indID = getID_IndSGI();
	btnSave.setEnabled(true);
	if (cbbClasse.getSelectedIndex() == KPI_SRC_DW) {
	    if ((fldDWSel.getText().trim().length() == 0 || fldDwConsultaSel.getText().trim().length() == 0
		    || fldDWDataSel.getText().trim().length() == 0 || fldRealDWSel.getText().trim().length() == 0)) {

		errorMessage.append("Informe todos os dados para cria��o da fonte de dados.");
		return false;
	    } else if ((indID.equals("") || indID.equals("0")) && fldDWIndSel.getText().isEmpty() && fldCodImpInd.getText().isEmpty()) {
		errorMessage.append("Informe um indicador como destino da fonte de dados.");
		return false;
	    }
	}
	return true;
    }

    public boolean hasCombos() {
	return false;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupTopConnect = new javax.swing.ButtonGroup();
        grpReimportacao = new javax.swing.ButtonGroup();
        scrlPrincipal = new javax.swing.JScrollPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnImportar = new javax.swing.JButton();
        btnLog = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        tbbDataSource = new javax.swing.JTabbedPane();
        sclMain = new javax.swing.JScrollPane();
        pnlMain = new javax.swing.JPanel();
        pnlPrincipal = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        scrlDescricao = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        fldNome = new pv.jfcx.JPVEdit();
        sclConfiguracao = new javax.swing.JScrollPane();
        pnlConfiguracao = new javax.swing.JPanel();
        lblClasse = new javax.swing.JLabel();
        cbbClasse = new javax.swing.JComboBox();
        radAmbiente = new javax.swing.JRadioButton();
        radTopConnect = new javax.swing.JRadioButton();
        pnlSigaDw = new javax.swing.JPanel();
        lblEnderecoDW = new javax.swing.JLabel();
        fldUrlDW = new pv.jfcx.JPVEdit();
        lblDatawarehouse = new javax.swing.JLabel();
        cbbDataWareHouse = new javax.swing.JComboBox();
        fldDWSel = new javax.swing.JTextField();
        lblConsulta = new javax.swing.JLabel();
        cbbConsultas = new javax.swing.JComboBox();
        fldDwConsultaSel = new javax.swing.JTextField();
        lblIDCodIndicador = new javax.swing.JLabel();
        cbbDataDW = new javax.swing.JComboBox();
        fldDWDataSel = new javax.swing.JTextField();
        lblRealDW = new javax.swing.JLabel();
        cbbRealDW = new javax.swing.JComboBox();
        fldRealDWSel = new javax.swing.JTextField();
        lblMetaDW = new javax.swing.JLabel();
        cbbMetaDW = new javax.swing.JComboBox();
        fldMetaDWSel = new javax.swing.JTextField();
        lblPreviaDW = new javax.swing.JLabel();
        fldPreviaDWSel = new javax.swing.JTextField();
        cbbPreviaDW = new javax.swing.JComboBox();
        jToolBar1 = new javax.swing.JToolBar();
        btnPostEndereco = new javax.swing.JButton();
        cbbCodImpInd = new javax.swing.JComboBox();
        fldCodImpInd = new javax.swing.JTextField();
        lblDataDW = new javax.swing.JLabel();
        lblIDIndicador = new javax.swing.JLabel();
        cbbIndicadorID = new javax.swing.JComboBox();
        fldDWIndSel = new javax.swing.JTextField();
        pnlDbAccess = new javax.swing.JPanel();
        lblConType = new javax.swing.JLabel();
        fldServer = new pv.jfcx.JPVEdit();
        cboConType = new javax.swing.JComboBox();
        cboDataBase = new javax.swing.JComboBox();
        fldAlias = new pv.jfcx.JPVEdit();
        lblConType1 = new javax.swing.JLabel();
        lblConType2 = new javax.swing.JLabel();
        lblConType3 = new javax.swing.JLabel();
        pnlProtheus = new javax.swing.JPanel();
        fldFilial = new pv.jfcx.JPVEdit();
        fldEmpresa = new pv.jfcx.JPVEdit();
        fldPorta = new pv.jfcx.JPVEdit();
        fldServidor = new pv.jfcx.JPVEdit();
        fldAmbiente = new pv.jfcx.JPVEdit();
        lblAmbiente = new javax.swing.JLabel();
        lblServidor = new javax.swing.JLabel();
        lblPorta = new javax.swing.JLabel();
        lblEmpresa = new javax.swing.JLabel();
        lblFilial = new javax.swing.JLabel();
        chkPrepareEnv = new javax.swing.JCheckBox();
        pnlDeclaracao = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        scrlDeclaracoes = new javax.swing.JScrollPane();
        txtDeclaracoes = new javax.swing.JTextArea();
        pnlRightRecord1 = new javax.swing.JPanel();
        pnlLeftRecord1 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        pnlRecarga = new javax.swing.JPanel();
        txtData = new pv.jfcx.JPVDatePlus();
        rdPeriodoAtual = new javax.swing.JRadioButton();
        rdPeriodoEspecifico = new javax.swing.JRadioButton();
        chkLimpaPlanilha = new javax.swing.JCheckBox();
        cbScorecard = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        tsArvore = new kpi.beans.JBITreeSelection();
        pnlCima = new javax.swing.JPanel();
        pnlDireita = new javax.swing.JPanel();
        pnlEsquerda = new javax.swing.JPanel();
        pnlBaixo = new javax.swing.JPanel();
        pnlIndicadores = new javax.swing.JPanel();
        cbbIndicador = new javax.swing.JComboBox();
        lblIndicador = new javax.swing.JLabel();
        cbbScorecard = new javax.swing.JComboBox();
        tsArvoreScorecard = new kpi.beans.JBITreeSelection();
        lblScoreCard = new javax.swing.JLabel();
        pnlBottomForm = new javax.swing.JPanel();
        biProgressbar = new kpi.beans.JBIProgressBar();
        pnlBottomForm1 = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiDataSourceFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_fontedados.gif"))); // NOI18N
        setMaximumSize(null);
        setMinimumSize(null);
        setPreferredSize(new java.awt.Dimension(534, 540));
        try {
            setSelected(true);
        } catch (java.beans.PropertyVetoException e1) {
            e1.printStackTrace();
        }

        scrlPrincipal.setBorder(null);
        scrlPrincipal.setPreferredSize(new java.awt.Dimension(300, 200));

        pnlRecord.setPreferredSize(new java.awt.Dimension(512, 318));
        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 31));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",  kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnEdit.setText(bundle2.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle2.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnImportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_exportacao.gif"))); // NOI18N
        btnImportar.setText("Importar");
        btnImportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImportarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnImportar);

        btnLog.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_log.gif"))); // NOI18N
        btnLog.setText(bundle.getString("KpiCalcIndicador_00016")); // NOI18N
        btnLog.setPreferredSize(new java.awt.Dimension(80, 23));
        btnLog.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogActionPerformed(evt);
            }
        });
        tbInsertion.add(btnLog);

        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle1.getString("KpiAjuda_00001")); // NOI18N
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setPreferredSize(new java.awt.Dimension(500, 400));
        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        tbbDataSource.setPreferredSize(new java.awt.Dimension(300, 500));

        sclMain.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        pnlMain.setPreferredSize(new java.awt.Dimension(450, 300));
        pnlMain.setLayout(new java.awt.BorderLayout());

        pnlPrincipal.setPreferredSize(new java.awt.Dimension(300, 300));
        pnlPrincipal.setLayout(null);

        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiDataSourceFrame_00002")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlPrincipal.add(lblNome);
        lblNome.setBounds(20, 10, 98, 18);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("KpiDataSourceFrame_00003")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlPrincipal.add(lblDescricao);
        lblDescricao.setBounds(20, 50, 100, 20);

        txtDescricao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        scrlDescricao.setViewportView(txtDescricao);

        pnlPrincipal.add(scrlDescricao);
        scrlDescricao.setBounds(20, 70, 400, 60);
        pnlPrincipal.add(fldNome);
        fldNome.setBounds(20, 30, 400, 22);

        pnlMain.add(pnlPrincipal, java.awt.BorderLayout.CENTER);

        sclMain.setViewportView(pnlMain);

        tbbDataSource.addTab(bundle.getString("KpiDataSourceFrame_00007"), sclMain); // NOI18N

        sclConfiguracao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        pnlConfiguracao.setPreferredSize(new java.awt.Dimension(450, 400));
        pnlConfiguracao.setLayout(null);

        lblClasse.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblClasse.setText(bundle.getString("KpiDataSourceFrame_00030")); // NOI18N
        lblClasse.setEnabled(false);
        lblClasse.setMaximumSize(new java.awt.Dimension(100, 16));
        lblClasse.setMinimumSize(new java.awt.Dimension(100, 16));
        lblClasse.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlConfiguracao.add(lblClasse);
        lblClasse.setBounds(20, 10, 50, 18);

        cbbClasse.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbClasseItemStateChanged(evt);
            }
        });
        pnlConfiguracao.add(cbbClasse);
        cbbClasse.setBounds(20, 30, 400, 22);

        buttonGroupTopConnect.add(radAmbiente);
        radAmbiente.setText(bundle.getString("KpiDataSourceFrame_00013")); // NOI18N
        radAmbiente.setEnabled(false);
        radAmbiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radAmbienteActionPerformed(evt);
            }
        });
        pnlConfiguracao.add(radAmbiente);
        radAmbiente.setBounds(20, 60, 210, 23);

        buttonGroupTopConnect.add(radTopConnect);
        radTopConnect.setText(bundle.getString("KpiDataSourceFrame_00012")); // NOI18N
        radTopConnect.setEnabled(false);
        radTopConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radTopConnectActionPerformed(evt);
            }
        });
        pnlConfiguracao.add(radTopConnect);
        radTopConnect.setBounds(20, 80, 210, 20);

        pnlSigaDw.setPreferredSize(new java.awt.Dimension(600, 450));
        pnlSigaDw.setLayout(null);

        lblEnderecoDW.setText("Endere�o do \tDW:");
        pnlSigaDw.add(lblEnderecoDW);
        lblEnderecoDW.setBounds(20, 10, 100, 20);

        fldUrlDW.setMaxLength(60);
        pnlSigaDw.add(fldUrlDW);
        fldUrlDW.setBounds(20, 30, 400, 22);

        lblDatawarehouse.setText("DataWareHouse:");
        pnlSigaDw.add(lblDatawarehouse);
        lblDatawarehouse.setBounds(20, 50, 93, 20);

        cbbDataWareHouse.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbDataWareHouseItemStateChanged(evt);
            }
        });
        pnlSigaDw.add(cbbDataWareHouse);
        cbbDataWareHouse.setBounds(20, 70, 180, 22);

        fldDWSel.setEditable(false);
        pnlSigaDw.add(fldDWSel);
        fldDWSel.setBounds(240, 70, 180, 22);

        lblConsulta.setText(bundle2.getString("KpiDataSourceFrame_00038")); // NOI18N
        pnlSigaDw.add(lblConsulta);
        lblConsulta.setBounds(20, 90, 94, 20);

        cbbConsultas.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbConsultasItemStateChanged(evt);
            }
        });
        pnlSigaDw.add(cbbConsultas);
        cbbConsultas.setBounds(20, 110, 180, 22);

        fldDwConsultaSel.setEditable(false);
        pnlSigaDw.add(fldDwConsultaSel);
        fldDwConsultaSel.setBounds(240, 110, 180, 22);

        lblIDCodIndicador.setText("ID C�d. Imp. Indicador:");
        pnlSigaDw.add(lblIDCodIndicador);
        lblIDCodIndicador.setBounds(20, 180, 160, 20);

        cbbDataDW.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbDataDWItemStateChanged(evt);
            }
        });
        pnlSigaDw.add(cbbDataDW);
        cbbDataDW.setBounds(20, 240, 180, 22);

        fldDWDataSel.setEditable(false);
        pnlSigaDw.add(fldDWDataSel);
        fldDWDataSel.setBounds(240, 240, 180, 22);

        lblRealDW.setText("Real:");
        pnlSigaDw.add(lblRealDW);
        lblRealDW.setBounds(20, 260, 89, 20);

        cbbRealDW.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbRealDWItemStateChanged(evt);
            }
        });
        pnlSigaDw.add(cbbRealDW);
        cbbRealDW.setBounds(20, 280, 180, 22);

        fldRealDWSel.setEditable(false);
        pnlSigaDw.add(fldRealDWSel);
        fldRealDWSel.setBounds(240, 280, 180, 22);

        lblMetaDW.setText("Meta:");
        pnlSigaDw.add(lblMetaDW);
        lblMetaDW.setBounds(20, 300, 89, 20);

        cbbMetaDW.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbMetaDWItemStateChanged(evt);
            }
        });
        pnlSigaDw.add(cbbMetaDW);
        cbbMetaDW.setBounds(20, 320, 180, 22);

        fldMetaDWSel.setEditable(false);
        pnlSigaDw.add(fldMetaDWSel);
        fldMetaDWSel.setBounds(240, 320, 180, 22);

        lblPreviaDW.setText("Pr�via:");
        pnlSigaDw.add(lblPreviaDW);
        lblPreviaDW.setBounds(20, 340, 89, 20);

        fldPreviaDWSel.setEditable(false);
        pnlSigaDw.add(fldPreviaDWSel);
        fldPreviaDWSel.setBounds(240, 360, 180, 22);

        cbbPreviaDW.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbPreviaDWItemStateChanged(evt);
            }
        });
        pnlSigaDw.add(cbbPreviaDW);
        cbbPreviaDW.setBounds(20, 360, 180, 22);

        jToolBar1.setFloatable(false);

        btnPostEndereco.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnPostEndereco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_verificar.gif"))); // NOI18N
        btnPostEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPostEnderecoActionPerformed(evt);
            }
        });
        jToolBar1.add(btnPostEndereco);

        pnlSigaDw.add(jToolBar1);
        jToolBar1.setBounds(420, 30, 25, 25);

        cbbCodImpInd.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbCodImpIndItemStateChanged(evt);
            }
        });
        pnlSigaDw.add(cbbCodImpInd);
        cbbCodImpInd.setBounds(20, 200, 180, 22);

        fldCodImpInd.setEditable(false);
        pnlSigaDw.add(fldCodImpInd);
        fldCodImpInd.setBounds(240, 200, 180, 22);

        lblDataDW.setText("Data:");
        pnlSigaDw.add(lblDataDW);
        lblDataDW.setBounds(20, 220, 90, 20);

        lblIDIndicador.setText("ID Indicador:");
        pnlSigaDw.add(lblIDIndicador);
        lblIDIndicador.setBounds(20, 135, 90, 20);

        cbbIndicadorID.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbIndicadorIDItemStateChanged(evt);
            }
        });
        pnlSigaDw.add(cbbIndicadorID);
        cbbIndicadorID.setBounds(20, 155, 180, 22);

        fldDWIndSel.setEditable(false);
        pnlSigaDw.add(fldDWIndSel);
        fldDWIndSel.setBounds(240, 155, 180, 22);

        pnlConfiguracao.add(pnlSigaDw);
        pnlSigaDw.setBounds(0, 130, 490, 410);

        pnlDbAccess.setLayout(null);

        lblConType.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblConType.setText("dbDataBase:");
        lblConType.setMaximumSize(new java.awt.Dimension(100, 16));
        lblConType.setMinimumSize(new java.awt.Dimension(100, 16));
        lblConType.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDbAccess.add(lblConType);
        lblConType.setBounds(20, 0, 98, 20);
        pnlDbAccess.add(fldServer);
        fldServer.setBounds(20, 100, 400, 22);

        pnlDbAccess.add(cboConType);
        cboConType.setBounds(20, 140, 400, 22);

        pnlDbAccess.add(cboDataBase);
        cboDataBase.setBounds(20, 20, 400, 22);
        pnlDbAccess.add(fldAlias);
        fldAlias.setBounds(20, 60, 400, 22);

        lblConType1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblConType1.setText("dbAlias:");
        lblConType1.setMaximumSize(new java.awt.Dimension(100, 16));
        lblConType1.setMinimumSize(new java.awt.Dimension(100, 16));
        lblConType1.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDbAccess.add(lblConType1);
        lblConType1.setBounds(20, 40, 98, 20);

        lblConType2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblConType2.setText("dbServer:");
        lblConType2.setMaximumSize(new java.awt.Dimension(100, 16));
        lblConType2.setMinimumSize(new java.awt.Dimension(100, 16));
        lblConType2.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDbAccess.add(lblConType2);
        lblConType2.setBounds(20, 80, 98, 20);

        lblConType3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblConType3.setText("dbContype:");
        lblConType3.setMaximumSize(new java.awt.Dimension(100, 16));
        lblConType3.setMinimumSize(new java.awt.Dimension(100, 16));
        lblConType3.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDbAccess.add(lblConType3);
        lblConType3.setBounds(20, 120, 98, 20);

        pnlConfiguracao.add(pnlDbAccess);
        pnlDbAccess.setBounds(0, 130, 490, 240);

        pnlProtheus.setLayout(null);
        pnlProtheus.add(fldFilial);
        fldFilial.setBounds(20, 180, 400, 22);
        pnlProtheus.add(fldEmpresa);
        fldEmpresa.setBounds(20, 140, 400, 22);
        pnlProtheus.add(fldPorta);
        fldPorta.setBounds(20, 100, 400, 22);
        pnlProtheus.add(fldServidor);
        fldServidor.setBounds(20, 60, 400, 22);
        pnlProtheus.add(fldAmbiente);
        fldAmbiente.setBounds(20, 20, 400, 22);

        lblAmbiente.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAmbiente.setText(bundle.getString("KpiDataSourceFrame_00024")); // NOI18N
        lblAmbiente.setMaximumSize(new java.awt.Dimension(100, 16));
        lblAmbiente.setMinimumSize(new java.awt.Dimension(100, 16));
        lblAmbiente.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlProtheus.add(lblAmbiente);
        lblAmbiente.setBounds(20, 0, 110, 18);

        lblServidor.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblServidor.setText(bundle.getString("KpiDataSourceFrame_00025")); // NOI18N
        lblServidor.setMaximumSize(new java.awt.Dimension(100, 16));
        lblServidor.setMinimumSize(new java.awt.Dimension(100, 16));
        lblServidor.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlProtheus.add(lblServidor);
        lblServidor.setBounds(20, 40, 110, 18);

        lblPorta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPorta.setText(bundle.getString("KpiDataSourceFrame_00026")); // NOI18N
        lblPorta.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPorta.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPorta.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlProtheus.add(lblPorta);
        lblPorta.setBounds(20, 80, 110, 18);

        lblEmpresa.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEmpresa.setText(bundle.getString("KpiDataSourceFrame_00027")); // NOI18N
        lblEmpresa.setMaximumSize(new java.awt.Dimension(100, 16));
        lblEmpresa.setMinimumSize(new java.awt.Dimension(100, 16));
        lblEmpresa.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlProtheus.add(lblEmpresa);
        lblEmpresa.setBounds(20, 120, 110, 18);

        lblFilial.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblFilial.setText(bundle.getString("KpiDataSourceFrame_00028")); // NOI18N
        lblFilial.setMaximumSize(new java.awt.Dimension(100, 16));
        lblFilial.setMinimumSize(new java.awt.Dimension(100, 16));
        lblFilial.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlProtheus.add(lblFilial);
        lblFilial.setBounds(20, 160, 110, 18);

        pnlConfiguracao.add(pnlProtheus);
        pnlProtheus.setBounds(0, 130, 488, 300);

        chkPrepareEnv.setText(bundle.getString("KpiDataSourceFrame_00047")); // NOI18N
        pnlConfiguracao.add(chkPrepareEnv);
        chkPrepareEnv.setBounds(20, 100, 320, 20);

        sclConfiguracao.setViewportView(pnlConfiguracao);

        tbbDataSource.addTab(bundle.getString("KpiDataSourceFrame_00016"), sclConfiguracao); // NOI18N

        pnlDeclaracao.setPreferredSize(new java.awt.Dimension(530, 260));
        pnlDeclaracao.setLayout(new java.awt.BorderLayout());

        pnlTopForm.setPreferredSize(new java.awt.Dimension(11, 11));
        pnlTopForm.setLayout(null);
        pnlDeclaracao.add(pnlTopForm, java.awt.BorderLayout.PAGE_START);

        scrlDeclaracoes.setPreferredSize(new java.awt.Dimension(500, 21));

        txtDeclaracoes.setColumns(80);
        txtDeclaracoes.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtDeclaracoes.setLineWrap(true);
        txtDeclaracoes.setWrapStyleWord(true);
        scrlDeclaracoes.setViewportView(txtDeclaracoes);

        pnlDeclaracao.add(scrlDeclaracoes, java.awt.BorderLayout.CENTER);
        pnlDeclaracao.add(pnlRightRecord1, java.awt.BorderLayout.EAST);
        pnlDeclaracao.add(pnlLeftRecord1, java.awt.BorderLayout.WEST);

        jPanel1.setLayout(new java.awt.BorderLayout());

        pnlRecarga.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), "Recarga de Dados"));
        pnlRecarga.setPreferredSize(new java.awt.Dimension(100, 150));
        pnlRecarga.setLayout(null);

        txtData.setButtonBorder(0);
        txtData.setDialog(true);
        txtData.setUseLocale(true);
        pnlRecarga.add(txtData);
        txtData.setBounds(130, 110, 100, 20);

        grpReimportacao.add(rdPeriodoAtual);
        rdPeriodoAtual.setSelected(true);
        rdPeriodoAtual.setText("Per�odo Atual");
        rdPeriodoAtual.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdPeriodoAtualItemStateChanged(evt);
            }
        });
        pnlRecarga.add(rdPeriodoAtual);
        rdPeriodoAtual.setBounds(10, 90, 100, 20);

        grpReimportacao.add(rdPeriodoEspecifico);
        rdPeriodoEspecifico.setText("Per�odo Espec�fico:");
        pnlRecarga.add(rdPeriodoEspecifico);
        rdPeriodoEspecifico.setBounds(10, 110, 120, 20);

        chkLimpaPlanilha.setText("Limpar planilha de valores");
        pnlRecarga.add(chkLimpaPlanilha);
        chkLimpaPlanilha.setBounds(10, 20, 160, 23);
        pnlRecarga.add(cbScorecard);
        cbScorecard.setBounds(10, 60, 300, 22);

        jLabel1.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        jLabel1.setEnabled(false);
        pnlRecarga.add(jLabel1);
        jLabel1.setBounds(10, 40, 300, 20);

        tsArvore.setCombo(cbScorecard);
        tsArvore.setRoot(false);
        pnlRecarga.add(tsArvore);
        tsArvore.setBounds(310, 50, 25, 22);

        jPanel1.add(pnlRecarga, java.awt.BorderLayout.CENTER);

        pnlCima.setLayout(null);
        jPanel1.add(pnlCima, java.awt.BorderLayout.PAGE_START);
        jPanel1.add(pnlDireita, java.awt.BorderLayout.EAST);
        jPanel1.add(pnlEsquerda, java.awt.BorderLayout.WEST);

        pnlBaixo.setLayout(null);
        jPanel1.add(pnlBaixo, java.awt.BorderLayout.PAGE_END);

        pnlDeclaracao.add(jPanel1, java.awt.BorderLayout.PAGE_END);

        tbbDataSource.addTab(bundle.getString("KpiDataSourceFrame_00017"), pnlDeclaracao); // NOI18N

        pnlIndicadores.setLayout(null);

        cbbIndicador.setEnabled(false);
        cbbIndicador.setPreferredSize(new java.awt.Dimension(120, 20));
        pnlIndicadores.add(cbbIndicador);
        cbbIndicador.setBounds(20, 70, 400, 22);

        lblIndicador.setText("Indicador:");
        lblIndicador.setEnabled(false);
        lblIndicador.setFocusable(false);
        lblIndicador.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlIndicadores.add(lblIndicador);
        lblIndicador.setBounds(20, 50, 72, 20);

        cbbScorecard.setEnabled(false);
        cbbScorecard.setPreferredSize(new java.awt.Dimension(10, 10));
        cbbScorecard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbScorecardActionPerformed(evt);
            }
        });
        pnlIndicadores.add(cbbScorecard);
        cbbScorecard.setBounds(20, 30, 400, 22);

        tsArvoreScorecard.setCombo(cbbScorecard);
        tsArvoreScorecard.setRoot(false);
        pnlIndicadores.add(tsArvoreScorecard);
        tsArvoreScorecard.setBounds(430, 30, 30, 22);

        lblScoreCard.setForeground(new java.awt.Color(51, 51, 255));
        lblScoreCard.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblScoreCard.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        lblScoreCard.setEnabled(false);
        pnlIndicadores.add(lblScoreCard);
        lblScoreCard.setBounds(20, 10, 280, 20);

        tbbDataSource.addTab(bundle2.getString("KpiDataSourceFrame_00036"), pnlIndicadores); // NOI18N

        pnlFields.add(tbbDataSource, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(200, 40));
        pnlBottomForm.setLayout(new java.awt.BorderLayout());

        biProgressbar.setPreferredSize(new java.awt.Dimension(200, 35));
        pnlBottomForm.add(biProgressbar, java.awt.BorderLayout.SOUTH);

        pnlRecord.add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        scrlPrincipal.setViewportView(pnlRecord);

        getContentPane().add(scrlPrincipal, java.awt.BorderLayout.CENTER);

        pnlBottomForm1.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm1, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void cbbScorecardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbScorecardActionPerformed
	    if (cbbScorecard.getSelectedIndex() == 0) {
		cbbIndicador.removeAllItems();
		cbbIndicador.setEnabled(false);
		cbbIndicador.addItem(record.getString("SGI_IND_NOME"));
	    } else {
		cbbIndicador.setEnabled(true);
		kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", "ID_SCOREC = '".concat(event.getComboValue(htbScorecard, cbbScorecard)) + "'");
		event.populateCombo(recIndFiltrados.getBIXMLVector("INDICADORES"), "INDICADOR", htbIndDW, cbbIndicador);
	    }

	}//GEN-LAST:event_cbbScorecardActionPerformed

	private void cbbRealDWItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbRealDWItemStateChanged
	    if (evt.getStateChange() == ItemEvent.DESELECTED) {
		int itemSelecionado = cbbRealDW.getSelectedIndex();
		if (itemSelecionado > 0) {
		    fldRealDWSel.setText(((kpi.util.KpiComboString) cbbRealDW.getSelectedItem()).toString());
		} else {
		    fldRealDWSel.setText("");
		}
	    }
}//GEN-LAST:event_cbbRealDWItemStateChanged

	private void cbbDataDWItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbDataDWItemStateChanged
	    if (evt.getStateChange() == ItemEvent.DESELECTED) {
		int itemSelecionado = cbbDataDW.getSelectedIndex();
		if (itemSelecionado > 0) {
		    fldDWDataSel.setText(((kpi.util.KpiComboString) cbbDataDW.getSelectedItem()).toString());
		} else {
		    fldDWDataSel.setText("");
		}
	    }
	}//GEN-LAST:event_cbbDataDWItemStateChanged

	private void cbbConsultasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbConsultasItemStateChanged
	    if (evt.getStateChange() == ItemEvent.DESELECTED) {
		int itemSelecionado = cbbConsultas.getSelectedIndex();
		if (itemSelecionado > 0) {
		    kpi.xml.BIXMLRecord recConsulta = vctConsultaSel.get(itemSelecionado - 1);
		    setCboBancoValue("");
		    fldRealDWSel.setText("");

		    //Se for maior que dois existem itens de consulta.
		    if (recConsulta.size() > 2) {
			fldDwConsultaSel.setText(((kpi.util.KpiComboString) cbbConsultas.getSelectedItem()).toString());
			kpi.xml.BIXMLVector vctDatas = recConsulta.getBIXMLVector("DATAS");
			kpi.xml.BIXMLVector vctIndic = recConsulta.getBIXMLVector("INDICADORES");
			kpi.xml.BIXMLVector vctIdsInd = recConsulta.getBIXMLVector("INDICADORES_IDS");

			//Adicionando os campos de data.
			if (vctDatas.size() > 0) {
			    event.populateCombo(vctDatas, "CPO_DATA", htbCampos, cbbDataDW);
			} else {
			    cbbDataDW.removeAllItems();
			}

			//Adicionando os campos de indicadores.
			if (vctIndic.size() > 0) {
			    event.populateCombo(vctIndic, "INDICADOR", htbReal, cbbRealDW);
			    event.populateCombo(vctIndic, "INDICADOR", htbMeta, cbbMetaDW);
			    event.populateCombo(vctIndic, "INDICADOR", htbPrevia, cbbPreviaDW);
			} else {
			    cbbRealDW.removeAllItems();
			    cbbMetaDW.removeAllItems();
			    cbbPreviaDW.removeAllItems();
			}

			//Adicionado vetores com os ids dos Indicadores.
			if (vctDatas.size() > 0) {
			    event.populateCombo(vctIdsInd, "CPO_DATA", htbIndIDs, cbbIndicadorID);
			    event.populateCombo(vctIdsInd, "CPO_DATA", htbIndCodIDs, cbbCodImpInd);
			} else {
			    cbbIndicadorID.removeAllItems();
			    cbbCodImpInd.removeAllItems();
			}

		    } else {
			fldDwConsultaSel.setText("");
			cbbDataDW.removeAllItems();
			cbbRealDW.removeAllItems();
			cbbMetaDW.removeAllItems();
			cbbPreviaDW.removeAllItems();
		    }
		} else {
		    setCboBancoValue("");
		    fldRealDWSel.setText("");
		    fldDwConsultaSel.setText("");
		    cbbDataDW.removeAllItems();
		    cbbRealDW.removeAllItems();
		    cbbMetaDW.removeAllItems();
		    cbbPreviaDW.removeAllItems();
		}
	    }
	}//GEN-LAST:event_cbbConsultasItemStateChanged

	private void cbbDataWareHouseItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbDataWareHouseItemStateChanged
	    if (evt.getStateChange() == ItemEvent.DESELECTED) {
		int itemSelecionado = cbbDataWareHouse.getSelectedIndex();
		if (itemSelecionado > 0) {
		    kpi.xml.BIXMLVector dwConsultas = recDataWareHouse.getBIXMLVector("DWCONSULTAS");
		    kpi.xml.BIXMLRecord recConsultas = dwConsultas.get(itemSelecionado - 1);

		    if (recConsultas.size() > 2) {
			kpi.util.KpiComboString itemSele = (kpi.util.KpiComboString) cbbDataWareHouse.getSelectedItem();
			fldDWSel.setText(itemSele.toString());
			vctConsultaSel = recConsultas.getBIXMLVector("QUERY_LISTS");
			event.populateCombo(vctConsultaSel, "QUERY_LIST", htbConsultas, cbbConsultas);
		    } else {
			fldDWSel.setText("");
			cbbConsultas.removeAllItems();
		    }
		} else {
		    fldDWSel.setText("");
		    cbbConsultas.removeAllItems();
		}
	    }
	}//GEN-LAST:event_cbbDataWareHouseItemStateChanged

	private void btnPostEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPostEnderecoActionPerformed
	    kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(1);
	    kpi.core.KpiStaticReferences.getKpiMainPanel().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));

	    StringBuilder request = new StringBuilder("REQ_DATAWAREHOUSE");
	    request.append("|");
	    request.append(fldUrlDW.getText());
	    request.append("|");
	    request.append("true"); //Carrega os detalhes da consulta.

	    recDataWareHouse = event.loadRecord("-1", "DWCONSULTA", request.toString());
	    if (recDataWareHouse != null) {
		event.populateCombo(recDataWareHouse.getBIXMLVector("DWCONSULTAS"), "DWCONSULTA", htbDataWareHouse, cbbDataWareHouse);
		setConnectedDW(true);
		enableDWFields(true);
	    } else {
		setConnectedDW(false);
		enableDWFields(false);
	    }
	    kpi.core.KpiStaticReferences.getKpiMainPanel().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
	    kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(0);

	}//GEN-LAST:event_btnPostEnderecoActionPerformed

    private void radTopConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radTopConnectActionPerformed
	if (getStatus() == KpiDefaultFrameBehavior.INSERTING || getStatus() == KpiDefaultFrameBehavior.UPDATING) {
	    enableTopFields();
	}
    }//GEN-LAST:event_radTopConnectActionPerformed

    private void radAmbienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radAmbienteActionPerformed
	if (getStatus() == KpiDefaultFrameBehavior.INSERTING || getStatus() == KpiDefaultFrameBehavior.UPDATING) {
	    enableTopFields();
	}
    }//GEN-LAST:event_radAmbienteActionPerformed

	private void btnLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogActionPerformed
	    String serverFolder = "\\logs\\imports\\";
	    kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);

	    frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
	    frmChooser.setFileType(".html");
	    frmChooser.loadServerFiles(serverFolder.concat("*.html"));
	    frmChooser.setVisible(true);

	    if (frmChooser.isValidFile()) {

		frmChooser.setUrlPath("/logs/imports/");
		frmChooser.openFile();
	    }
	}//GEN-LAST:event_btnLogActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
	    event.loadHelp("DATASRC");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void cbbClasseItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbClasseItemStateChanged
	    if (evt.getStateChange() == ItemEvent.SELECTED && cbbClasse.getItemCount() > 0) {
		enableTopFields();
		buildDefaultText(cbbClasse.getSelectedIndex());
	    }
	}//GEN-LAST:event_cbbClasseItemStateChanged

	private void btnImportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportarActionPerformed
	    setCloseEnabled(false);
	    biProgressbar.setJobName("datasrc_".concat(getID()));
	    biProgressbar.startProgressBar();
	    event.executeRecord("IMPORTCON");
	}//GEN-LAST:event_btnImportarActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
	    event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
	    btnSave.setEnabled(false);
	    this.setTitle(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDataSourceFrame_00001") + " - " + fldNome.getText());
	    event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
	    event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
	    event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
	    event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

	private void cbbMetaDWItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbMetaDWItemStateChanged
	    if (evt.getStateChange() == ItemEvent.DESELECTED) {
		int itemSelecionado = cbbMetaDW.getSelectedIndex();
		if (itemSelecionado > 0) {
		    fldMetaDWSel.setText(((kpi.util.KpiComboString) cbbMetaDW.getSelectedItem()).toString());
		} else {
		    fldMetaDWSel.setText("");
		}
	    }
}//GEN-LAST:event_cbbMetaDWItemStateChanged

	private void cbbPreviaDWItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbPreviaDWItemStateChanged
	    if (evt.getStateChange() == ItemEvent.DESELECTED) {
		int itemSelecionado = cbbPreviaDW.getSelectedIndex();
		if (itemSelecionado > 0) {
		    fldPreviaDWSel.setText(((kpi.util.KpiComboString) cbbPreviaDW.getSelectedItem()).toString());
		} else {
		    fldPreviaDWSel.setText("");
		}
	    }
}//GEN-LAST:event_cbbPreviaDWItemStateChanged

        private void rdPeriodoAtualItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdPeriodoAtualItemStateChanged
	    if (evt.getStateChange() == ItemEvent.SELECTED) {
		txtData.setText("");
	    }
        }//GEN-LAST:event_rdPeriodoAtualItemStateChanged

    private void cbbCodImpIndItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbCodImpIndItemStateChanged
	if (evt.getStateChange() == ItemEvent.DESELECTED) {
	    int itemSelecionado = cbbCodImpInd.getSelectedIndex();
	    if (itemSelecionado > 0) {
		fldCodImpInd.setText(((kpi.util.KpiComboString) cbbCodImpInd.getSelectedItem()).toString());
	    } else {
		fldCodImpInd.setText("");
	    }
	}
    }//GEN-LAST:event_cbbCodImpIndItemStateChanged

    private void cbbIndicadorIDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbIndicadorIDItemStateChanged
	if (evt.getStateChange() == ItemEvent.DESELECTED) {
	    
	    int itemSelecionado = cbbIndicadorID.getSelectedIndex();
	    if (itemSelecionado > 0) {
		fldDWIndSel.setText(((kpi.util.KpiComboString) cbbIndicadorID.getSelectedItem()).toString());
	    } else {
		fldDWIndSel.setText("");
	    }
	}
    }//GEN-LAST:event_cbbIndicadorIDItemStateChanged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private kpi.beans.JBIProgressBar biProgressbar;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnImportar;
    private javax.swing.JButton btnLog;
    private javax.swing.JButton btnPostEndereco;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private javax.swing.ButtonGroup buttonGroupTopConnect;
    private javax.swing.JComboBox cbScorecard;
    private javax.swing.JComboBox cbbClasse;
    private javax.swing.JComboBox cbbCodImpInd;
    private javax.swing.JComboBox cbbConsultas;
    private javax.swing.JComboBox cbbDataDW;
    private javax.swing.JComboBox cbbDataWareHouse;
    private javax.swing.JComboBox cbbIndicador;
    private javax.swing.JComboBox cbbIndicadorID;
    private javax.swing.JComboBox cbbMetaDW;
    private javax.swing.JComboBox cbbPreviaDW;
    private javax.swing.JComboBox cbbRealDW;
    private javax.swing.JComboBox cbbScorecard;
    private javax.swing.JComboBox cboConType;
    private javax.swing.JComboBox cboDataBase;
    private javax.swing.JCheckBox chkLimpaPlanilha;
    private javax.swing.JCheckBox chkPrepareEnv;
    private pv.jfcx.JPVEdit fldAlias;
    private pv.jfcx.JPVEdit fldAmbiente;
    private javax.swing.JTextField fldCodImpInd;
    private javax.swing.JTextField fldDWDataSel;
    private javax.swing.JTextField fldDWIndSel;
    private javax.swing.JTextField fldDWSel;
    private javax.swing.JTextField fldDwConsultaSel;
    private pv.jfcx.JPVEdit fldEmpresa;
    private pv.jfcx.JPVEdit fldFilial;
    private javax.swing.JTextField fldMetaDWSel;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldPorta;
    private javax.swing.JTextField fldPreviaDWSel;
    private javax.swing.JTextField fldRealDWSel;
    private pv.jfcx.JPVEdit fldServer;
    private pv.jfcx.JPVEdit fldServidor;
    private pv.jfcx.JPVEdit fldUrlDW;
    private javax.swing.ButtonGroup grpReimportacao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lblAmbiente;
    private javax.swing.JLabel lblClasse;
    private javax.swing.JLabel lblConType;
    private javax.swing.JLabel lblConType1;
    private javax.swing.JLabel lblConType2;
    private javax.swing.JLabel lblConType3;
    private javax.swing.JLabel lblConsulta;
    private javax.swing.JLabel lblDataDW;
    private javax.swing.JLabel lblDatawarehouse;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblEmpresa;
    private javax.swing.JLabel lblEnderecoDW;
    private javax.swing.JLabel lblFilial;
    private javax.swing.JLabel lblIDCodIndicador;
    private javax.swing.JLabel lblIDIndicador;
    private javax.swing.JLabel lblIndicador;
    private javax.swing.JLabel lblMetaDW;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPorta;
    private javax.swing.JLabel lblPreviaDW;
    private javax.swing.JLabel lblRealDW;
    private javax.swing.JLabel lblScoreCard;
    private javax.swing.JLabel lblServidor;
    private javax.swing.JPanel pnlBaixo;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomForm1;
    private javax.swing.JPanel pnlCima;
    private javax.swing.JPanel pnlConfiguracao;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlDbAccess;
    private javax.swing.JPanel pnlDeclaracao;
    private javax.swing.JPanel pnlDireita;
    private javax.swing.JPanel pnlEsquerda;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlIndicadores;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlLeftRecord1;
    private javax.swing.JPanel pnlMain;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlPrincipal;
    private javax.swing.JPanel pnlProtheus;
    private javax.swing.JPanel pnlRecarga;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlRightRecord1;
    private javax.swing.JPanel pnlSigaDw;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JRadioButton radAmbiente;
    private javax.swing.JRadioButton radTopConnect;
    private javax.swing.JRadioButton rdPeriodoAtual;
    private javax.swing.JRadioButton rdPeriodoEspecifico;
    private javax.swing.JScrollPane sclConfiguracao;
    private javax.swing.JScrollPane sclMain;
    private javax.swing.JScrollPane scrlDeclaracoes;
    private javax.swing.JScrollPane scrlDescricao;
    private javax.swing.JScrollPane scrlPrincipal;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private javax.swing.JTabbedPane tbbDataSource;
    private kpi.beans.JBITreeSelection tsArvore;
    private kpi.beans.JBITreeSelection tsArvoreScorecard;
    private pv.jfcx.JPVDatePlus txtData;
    private javax.swing.JTextArea txtDeclaracoes;
    private kpi.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
	status = value;
    }

    @Override
    public int getStatus() {
	return status;
    }

    @Override
    public void setType(String value) {
	recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
	return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
	id = String.valueOf(value);
    }

    @Override
    public String getID() {
	return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
	record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
	return record;
    }

    @Override
    public void showDataButtons() {
	pnlConfirmation.setVisible(true);
	pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
	pnlConfirmation.setVisible(false);
	pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
	return this;
    }

    @Override
    public String getParentID() {
	if (record.contains("PARENTID")) {
	    return String.valueOf(record.getString("PARENTID"));
	} else {
	    return "";
	}
    }

    @Override
    public void loadRecord() {
	event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
	return new javax.swing.JTabbedPane();
    }

    @Override
    public String getFormType() {
	return formType;
    }

    @Override
    public String getParentType() {
	return parentType;
    }

    @Override
    public void afterExecute() {
    }

    @Override
    public void onStartBar(ComponentEvent componentEvent) {
	onProgressBar(false);
    }

    @Override
    public void onFinishBar(ComponentEvent componentEvent) {
	onProgressBar(true);
    }

    @Override
    public void onErrorBar(ComponentEvent componentEvent) {
	onProgressBar(true);
    }

    @Override
    public void onCancelBar(ComponentEvent componentEvent) {
	onProgressBar(true);
    }

    private void onProgressBar(boolean enable) {
	setCloseEnabled(enable);
	btnImportar.setEnabled(enable);
	btnDelete.setEnabled(enable);
	btnEdit.setEnabled(enable);
	btnImportar.setEnabled(enable);
	btnSave.setEnabled(enable);
	btnReload1.setEnabled(enable);
	btnReload.setEnabled(enable);
    }

    private void setConnectedDW(boolean connectedDW) {
	this.connectedDW = connectedDW;
    }

    private String getID_IndSGI() {
	String indID = record.getString("SGI_IND");

	if (cbbScorecard.getSelectedIndex() != 0) {//Se for 0 o usuario nao mudou o indicador.
	    indID = event.getComboValue(htbIndDW, cbbIndicador);
	}

	return indID;
    }
}

