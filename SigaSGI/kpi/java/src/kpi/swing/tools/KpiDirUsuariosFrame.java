package kpi.swing.tools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import kpi.beans.JBIListPanel;
import kpi.core.KpiStaticReferences;

public class KpiDirUsuariosFrame extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "DIRUSUARIOS";
    private String formType = recordType;
    private String parentType = recordType;
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiDirUsuariosFrame(int operation, String idAux, String contextAux) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextAux);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlData, true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlData, false);
    }

    @Override
    public void refreshFields() {
        fldTotalUsuarios.setText(record.getString("TOTALUSUARIOS"));
        fldTotalGrupos.setText(record.getString("TOTALGRUPOS"));

        usersList.setDataSource(record.getBIXMLVector("USUARIOS"), "0", "0", this.event);
        groupList.setDataSource(record.getBIXMLVector("GRUPOS"), "0", "0", this.event);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        recordAux.set("TOTALUSUARIOS", fldTotalUsuarios.getText());
        recordAux.set("TOTALGRUPOS", fldTotalGrupos.getText());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    public void populateCombos(kpi.xml.BIXMLRecord serverData) {
        return;
    }

    public boolean hasCombos() {
        return false;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new JTabbedPane();
        pnlRecord = new JPanel();
        pnlFields = new JPanel();
        scpRecord = new JScrollPane();
        pnlData = new JPanel();
        lblTotalUsuarios = new JLabel();
        fldTotalUsuarios = new JTextField();
        lblTotalGrupos = new JLabel();
        fldTotalGrupos = new JTextField();
        groupList = new JBIListPanel();
        usersList = new JBIListPanel();
        pnlRightForm = new JPanel();
        pnlBottomForm = new JPanel();
        pnlLeftForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiUsuariosFrame_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_dirusuario.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 105, 132));
        setPreferredSize(new Dimension(543, 358));

        pnlRecord.setLayout(new BorderLayout());

        pnlFields.setLayout(new BorderLayout());

        scpRecord.setBorder(null);

        pnlData.setLayout(null);

        lblTotalUsuarios.setHorizontalAlignment(SwingConstants.LEFT);
        lblTotalUsuarios.setText(bundle.getString("KpiUsuariosFrame_00003")); // NOI18N
        pnlData.add(lblTotalUsuarios);
        lblTotalUsuarios.setBounds(20, 10, 120, 22);
        pnlData.add(fldTotalUsuarios);
        fldTotalUsuarios.setBounds(20, 30, 180, 22);

        lblTotalGrupos.setHorizontalAlignment(SwingConstants.LEFT);
        lblTotalGrupos.setText(bundle.getString("KpiUsuariosFrame_00002")); // NOI18N
        pnlData.add(lblTotalGrupos);
        lblTotalGrupos.setBounds(20, 50, 150, 22);
        pnlData.add(fldTotalGrupos);
        fldTotalGrupos.setBounds(20, 70, 180, 22);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, BorderLayout.CENTER);

        pnlRecord.add(pnlFields, BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiUsuariosFrame_00005"), pnlRecord); // NOI18N

        tapCadastro.addTab(bundle.getString("KpiUsuariosFrame_00001"), new ImageIcon(getClass().getResource("/kpi/imagens/ic_grupo.gif")), groupList); // NOI18N
        tapCadastro.addTab(bundle.getString("KpiUsuariosFrame_00004"), new ImageIcon(getClass().getResource("/kpi/imagens/ic_usuario.gif")), usersList); // NOI18N
        getContentPane().add(tapCadastro, BorderLayout.CENTER);

        pnlRightForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightForm, BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pack();
    }//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JTextField fldTotalGrupos;
    private JTextField fldTotalUsuarios;
    private JBIListPanel groupList;
    private JLabel lblTotalGrupos;
    private JLabel lblTotalUsuarios;
    private JPanel pnlBottomForm;
    private JPanel pnlData;
    private JPanel pnlFields;
    private JPanel pnlLeftForm;
    private JPanel pnlRecord;
    private JPanel pnlRightForm;
    private JScrollPane scpRecord;
    private JTabbedPane tapCadastro;
    private JBIListPanel usersList;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getFormType() {
        return formType;
    }
}
