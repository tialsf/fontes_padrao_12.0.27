package kpi.swing.tools;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.util.ResourceBundle;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import kpi.beans.JBIProgressBar;
import kpi.beans.JBITextArea;
import kpi.beans.JBITreeSelection;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiFileChooser;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiDuplicador extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions, kpi.beans.JBIProgressBarListener {

    private String recordType = "SCO_DUPLICADOR";
    private String formType = recordType;
    private String parentType = recordType;
    private java.util.Hashtable htbScoreCard = new java.util.Hashtable();
    private String[] posicaoDiferenciador = {java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDuplicador_00009"),
	java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDuplicador_00010")};
    //
    private static final int PREPARADO = 0;
    private static final int EXECUTANDO = 1;
    private static final int FINALIZADO = 2;
    private String scorParentID = "";

    public KpiDuplicador(int operation, String idAux, String contextId) {
	initComponents();
	putClientProperty("MAXI", true);
	pgbDuplicador.addJBIProgressBarListener(this);
	event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
	event.setEnableFields(pnlDuplicador, true);
	event.setEnableFields(barDuplicador, true);
    }

    @Override
    public void disableFields() {
	event.setEnableFields(pnlDuplicador, false);
	event.setEnableFields(barDuplicador, false);
    }

    @Override
    public void refreshFields() {
	BIXMLVector vctScorecard = record.getBIXMLVector("SCORECARDS");

	//Popula os combos com a lista de scorecards.
	event.populateCombo(vctScorecard, "SCOR_ID", htbScoreCard, cmbScoOrigem);
	tsOrigem.setVetor(vctScorecard);
	event.populateCombo(vctScorecard, "SCOR_ID", htbScoreCard, cmbScoDestino);
	tsDestino.setVetor(vctScorecard);

	//Posiciona o combo origem no scorecard selecionado no cadastro de scorecards.
	if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
	    event.selectComboItem(cmbScoOrigem, event.getComboItem(vctScorecard, scorParentID, true));
	} else {
	    event.selectComboItem(cmbScoOrigem, event.getComboItem(vctScorecard, this.getID(), true));
	}

	//Define o estado do formul�rio.
	setProgressStatus(PREPARADO);

	//Define se a op��o 'Aplicar Caracter Diferenciador aos Indicadores' estar� ativa.
	chkDifIndicador.setEnabled(chkDupIndicador.isSelected());
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
	return new BIXMLRecord(recordType);
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
	boolean retorno = true;

	//O diferenciador n�o pode conter ponto.
	if (txtDiferenciadorNome.getText().indexOf(".") != -1) {
	    errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDuplicador_00001"));
	    retorno = false;
	}

	//O diferenciador n�o pode conter ponto.
	if (txtDiferenciadorCodigo.getText().indexOf(".") != -1) {
	    errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDuplicador_00001"));
	    retorno = false;
	}

	//O scorecard de origem deve ser informado.
	if (cmbScoOrigem.getSelectedIndex() == 0) {
	    errorMessage.append("O scorecard de origem deve ser informado!");
	    retorno = false;
	}

	//O scorecard principal n�o pode ser duplicado.
	if (cmbScoOrigem.getSelectedIndex() == 1) {
	    errorMessage.append("O scorecard principal n�o pode ser duplicado!");
	    retorno = false;
	}

	//O scorecard de destino deve ser informado.
	if (cmbScoDestino.getSelectedIndex() == 0) {
	    errorMessage.append("O scorecard de destino deve ser informado!");
	    retorno = false;
	}

	//Validacao do tipo de origem e destino.
	if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
	    retorno = isDuplicatePermited();
	    if (!retorno) {
		errorMessage.append("Duplica��o do scorecard n�o permitida. Origem destino incompat�veis.");
	    }
	}

	//Define o status da execu��o da duplica��o.
	if (retorno) {
	    setProgressStatus(EXECUTANDO);
	}

	return retorno;
    }

    /**
     * Define a mensagem o comportamento do formul�rio de acordo com o status de execu��o. 
     * @param iMessage status (PREPARADO, EXECUTANDO, FINALIZADO).
     */
    private void setProgressStatus(int iMessage) {
	if (iMessage == PREPARADO) {
	    //Habilita os campos do formul�rio.
	    enableFields();

	} else if (iMessage == EXECUTANDO) {
	    //Inicia o acompanhamento da barra de progresso.
	    pgbDuplicador.setJobName("dup_sco_1");
	    pgbDuplicador.startProgressBar();

	    //Define o conte�do do campo mensagem.
	    txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDuplicador_00002"));

	    //Habilita os campos do formul�rio.
	    disableFields();

	} else if (iMessage == FINALIZADO) {
	    //Define o conte�do do campo mensagem.
	    txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDuplicador_00003"));

	    //Habilita os campos do formul�rio.
	    enableFields();
	}
    }

    /**
     * Monta os par�metros para a requisi��o ao ADVPL.
     * @return
     */
    private String createRequest() {
	StringBuilder strComando = new StringBuilder();

	strComando.append(event.getComboValue(htbScoreCard, cmbScoOrigem, false));
	strComando.append("|");
	strComando.append(event.getComboValue(htbScoreCard, cmbScoDestino, false));
	strComando.append("|");
	strComando.append(txtDiferenciadorNome.getText());
	strComando.append("|");
	strComando.append(cmbPosDiferenciadorNome.getSelectedIndex());
	strComando.append("|");
	strComando.append(chkDifIndicador.isSelected());
	strComando.append("|");
	strComando.append(chkDupIndicador.isSelected());
	strComando.append("|");
	strComando.append(txtDiferenciadorCodigo.getText());
	strComando.append("|");
	strComando.append(cmbPosDiferenciadorCodigo.getSelectedIndex());

	return strComando.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new JTabbedPane();
        jPanel1 = new JPanel();
        pgbDuplicador = new JBIProgressBar();
        pnlMainPanel = new JPanel();
        pnlCenterForm = new JPanel();
        pnlDuplicador = new JPanel();
        lblOrigem = new JLabel();
        cmbScoOrigem = new JComboBox();
        lblScoreCardPai = new JLabel();
        cmbScoDestino = new JComboBox();
        lblDestino = new JLabel();
        txtDiferenciadorNome = new JTextField();
        cmbPosDiferenciadorNome = new JComboBox(posicaoDiferenciador);
        lblPosDiferenciador = new JLabel();
        tsOrigem = new JBITreeSelection();
        tsDestino = new JBITreeSelection();
        chkDupIndicador = new JCheckBox();
        chkDifIndicador = new JCheckBox();
        txtMesangem = new JBITextArea();
        jLabel1 = new JLabel();
        cmbPosDiferenciadorCodigo = new JComboBox(posicaoDiferenciador);
        lblPosDiferenciador1 = new JLabel();
        txtDiferenciadorCodigo = new JTextField();
        jLabel2 = new JLabel();
        jSeparator1 = new JSeparator();
        barDuplicador = new JToolBar();
        btnCalcular = new JButton();
        btnStatus = new JButton();
        btnLog = new JButton();
        btnAjuda = new JButton();
        pnlRightForm = new JPanel();
        pnlBottomForm = new JPanel();
        pnlLeftForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiDuplicador_00004")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_duplicador.gif")));         setMinimumSize(new Dimension(300, 300));
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(480, 450));
        setRequestFocusEnabled(false);

        tapCadastro.setPreferredSize(new Dimension(530, 300));

        jPanel1.setLayout(new BorderLayout());
        jPanel1.add(pgbDuplicador, BorderLayout.PAGE_END);

        pnlMainPanel.setPreferredSize(new Dimension(187, 20));
        pnlMainPanel.setLayout(new BorderLayout());

        pnlCenterForm.setLayout(new BorderLayout());

        pnlDuplicador.setLayout(null);

        lblOrigem.setForeground(new Color(0, 70, 213));
        lblOrigem.setText(bundle.getString("KpiDuplicador_00005")); // NOI18N
        lblOrigem.setPreferredSize(new Dimension(80, 15));
        pnlDuplicador.add(lblOrigem);
        lblOrigem.setBounds(20, 10, 400, 20);
        pnlDuplicador.add(cmbScoOrigem);
        cmbScoOrigem.setBounds(20, 30, 400, 22);

        lblScoreCardPai.setForeground(new Color(0, 70, 213));
        lblScoreCardPai.setText(bundle.getString("KpiDuplicador_00013")); // NOI18N
        lblScoreCardPai.setPreferredSize(new Dimension(80, 15));
        pnlDuplicador.add(lblScoreCardPai);
        lblScoreCardPai.setBounds(20, 50, 400, 20);
        pnlDuplicador.add(cmbScoDestino);
        cmbScoDestino.setBounds(20, 70, 400, 22);

        lblDestino.setForeground(new Color(0, 70, 213));
        lblDestino.setText(bundle.getString("KpiDuplicador_00007")); // NOI18N
        lblDestino.setPreferredSize(new Dimension(80, 15));
        pnlDuplicador.add(lblDestino);
        lblDestino.setBounds(20, 150, 80, 20);
        pnlDuplicador.add(txtDiferenciadorNome);
        txtDiferenciadorNome.setBounds(20, 200, 180, 22);
        pnlDuplicador.add(cmbPosDiferenciadorNome);
        cmbPosDiferenciadorNome.setBounds(240, 200, 180, 22);

        lblPosDiferenciador.setText(bundle.getString("KpiDuplicador_00011")); // NOI18N
        lblPosDiferenciador.setPreferredSize(new Dimension(80, 15));
        pnlDuplicador.add(lblPosDiferenciador);
        lblPosDiferenciador.setBounds(240, 220, 80, 20);

        tsOrigem.setCombo(cmbScoOrigem);
        pnlDuplicador.add(tsOrigem);
        tsOrigem.setBounds(420, 30, 25, 22);

        tsDestino.setCombo(cmbScoDestino);
        pnlDuplicador.add(tsDestino);
        tsDestino.setBounds(420, 70, 25, 22);

        chkDupIndicador.setText("Duplicar Indicadores");
        chkDupIndicador.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                chkDupIndicadorActionPerformed(evt);
            }
        });
        pnlDuplicador.add(chkDupIndicador);
        chkDupIndicador.setBounds(20, 100, 150, 23);

        chkDifIndicador.setText("Aplicar Diferenciador nos Indicadores");
        pnlDuplicador.add(chkDifIndicador);
        chkDifIndicador.setBounds(20, 120, 260, 23);

        txtMesangem.setEditable(false);
        txtMesangem.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(204, 204, 204)), bundle.getString("KpiEstruturaExport_00006"), TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION));         txtMesangem.setFont(new Font("Tahoma", 0, 11));         txtMesangem.setOpaque(false);
        pnlDuplicador.add(txtMesangem);
        txtMesangem.setBounds(20, 280, 400, 80);

        jLabel1.setText("Nome:");
        pnlDuplicador.add(jLabel1);
        jLabel1.setBounds(20, 180, 150, 20);
        pnlDuplicador.add(cmbPosDiferenciadorCodigo);
        cmbPosDiferenciadorCodigo.setBounds(240, 240, 180, 22);

        lblPosDiferenciador1.setText(bundle.getString("KpiDuplicador_00011")); // NOI18N
        lblPosDiferenciador1.setPreferredSize(new Dimension(80, 15));
        pnlDuplicador.add(lblPosDiferenciador1);
        lblPosDiferenciador1.setBounds(240, 180, 80, 20);
        pnlDuplicador.add(txtDiferenciadorCodigo);
        txtDiferenciadorCodigo.setBounds(20, 240, 180, 22);

        jLabel2.setText("C�digo de Importa��o:");
        pnlDuplicador.add(jLabel2);
        jLabel2.setBounds(20, 220, 150, 20);
        pnlDuplicador.add(jSeparator1);
        jSeparator1.setBounds(20, 170, 400, 10);

        pnlCenterForm.add(pnlDuplicador, BorderLayout.CENTER);

        pnlMainPanel.add(pnlCenterForm, BorderLayout.CENTER);

        jPanel1.add(pnlMainPanel, BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiDuplicador_00004"), jPanel1); // NOI18N

        getContentPane().add(tapCadastro, BorderLayout.CENTER);

        barDuplicador.setFloatable(false);
        barDuplicador.setRollover(true);

        btnCalcular.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_play.gif")));         btnCalcular.setText(bundle.getString("KpiDuplicador_00008")); // NOI18N
        btnCalcular.setPreferredSize(new Dimension(80, 23));
        btnCalcular.setVerticalAlignment(SwingConstants.BOTTOM);
        btnCalcular.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        barDuplicador.add(btnCalcular);

        btnStatus.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif")));         btnStatus.setText(bundle.getString("JBIListPanel_00003")); // NOI18N
        btnStatus.setPreferredSize(new Dimension(110, 23));
        btnStatus.setVerticalAlignment(SwingConstants.BOTTOM);
        btnStatus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnStatusActionPerformed(evt);
            }
        });
        barDuplicador.add(btnStatus);

        btnLog.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_log.gif")));         btnLog.setText(bundle.getString("KpiCalcIndicador_00016")); // NOI18N
        btnLog.setPreferredSize(new Dimension(80, 23));
        btnLog.setVerticalAlignment(SwingConstants.BOTTOM);
        btnLog.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnLogActionPerformed(evt);
            }
        });
        barDuplicador.add(btnLog);

        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif")));         btnAjuda.setText(bundle.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        barDuplicador.add(btnAjuda);

        getContentPane().add(barDuplicador, BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightForm, BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
	    event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStatusActionPerformed
	    event.loadRecord();
	}//GEN-LAST:event_btnStatusActionPerformed

	private void btnLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogActionPerformed
	    String serverFolder = "\\logs\\ScoCopy\\";
	    kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
	    frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
	    frmChooser.setFileType(".html");
	    frmChooser.loadServerFiles(serverFolder.concat("*.html"));
	    frmChooser.setVisible(true);
	    frmChooser.setUrlPath("/logs/ScoCopy/");
	    frmChooser.openFile();
	}//GEN-LAST:event_btnLogActionPerformed

	private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
	    event.executeRecord(createRequest());
	}//GEN-LAST:event_btnCalcularActionPerformed

        private void chkDupIndicadorActionPerformed(ActionEvent evt) {//GEN-FIRST:event_chkDupIndicadorActionPerformed
	    chkDifIndicador.setEnabled(chkDupIndicador.isSelected());
	    chkDifIndicador.setSelected(chkDupIndicador.isSelected());
        }//GEN-LAST:event_chkDupIndicadorActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JToolBar barDuplicador;
    private JButton btnAjuda;
    private JButton btnCalcular;
    private JButton btnLog;
    private JButton btnStatus;
    private JCheckBox chkDifIndicador;
    private JCheckBox chkDupIndicador;
    private JComboBox cmbPosDiferenciadorCodigo;
    private JComboBox cmbPosDiferenciadorNome;
    private JComboBox cmbScoDestino;
    private JComboBox cmbScoOrigem;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel jPanel1;
    private JSeparator jSeparator1;
    private JLabel lblDestino;
    private JLabel lblOrigem;
    private JLabel lblPosDiferenciador;
    private JLabel lblPosDiferenciador1;
    private JLabel lblScoreCardPai;
    private JBIProgressBar pgbDuplicador;
    private JPanel pnlBottomForm;
    private JPanel pnlCenterForm;
    private JPanel pnlDuplicador;
    private JPanel pnlLeftForm;
    private JPanel pnlMainPanel;
    private JPanel pnlRightForm;
    private JTabbedPane tapCadastro;
    private JBITreeSelection tsDestino;
    private JBITreeSelection tsOrigem;
    private JTextField txtDiferenciadorCodigo;
    private JTextField txtDiferenciadorNome;
    private JBITextArea txtMesangem;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
	status = value;
    }

    @Override
    public int getStatus() {
	return status;
    }

    @Override
    public void setType(String value) {
	recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
	return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
	id = String.valueOf(value);
    }

    @Override
    public String getID() {
	return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
	if ("COMMANDS".equals(recordAux.getTagName())) {
	    BIXMLAttributes att = recordAux.getAttributes();
	    scorParentID = att.getString("SCORECARD_SELECTED");
	} else {
	    record = recordAux;
	}

	record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
	return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
	return this;
    }

    @Override
    public String getParentID() {
	if (record.contains("PARENTID")) {
	    return String.valueOf(record.getString("PARENTID"));
	} else {
	    return "";
	}
    }

    @Override
    public void loadRecord() {
	event.loadRecord();
    }

    @Override
    public String getFormType() {
	return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
	return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
	return parentType;
    }

    @Override
    public void afterExecute() {
    }

    @Override
    public void onStartBar(ComponentEvent componentEvent) {
    }

    @Override
    public void onFinishBar(ComponentEvent componentEvent) {
	setProgressStatus(FINALIZADO);
    }

    @Override
    public void onErrorBar(ComponentEvent componentEvent) {
	setProgressStatus(FINALIZADO);
    }

    @Override
    public void onCancelBar(ComponentEvent componentEvent) {
	setProgressStatus(FINALIZADO);
    }

    private boolean isDuplicatePermited() {
	String idScoOri = event.getComboValue(htbScoreCard, cmbScoOrigem, false);
	String idScorDes = event.getComboValue(htbScoreCard, cmbScoDestino, false);
	String scoTipoOri = "XX";
	String scoTipoDes = "XX";
	BIXMLVector vctScore = record.getBIXMLVector("SCORECARDS");
	BIXMLRecord recScore = null;
	boolean granted = false;

	
	for (int iItem = 0; iItem < vctScore.size(); iItem++) {
	    recScore = vctScore.get(iItem);
	    if (recScore.getString("ID").equals(idScoOri)) {
		scoTipoOri = recScore.getString("TIPOSCORE");
	    }
	    if (recScore.getString("ID").equals(idScorDes)) {
		scoTipoDes = recScore.getString("TIPOSCORE");
	    }
	    //Verifica��o de permiss�o de c�pia.
	    if (!scoTipoOri.equals("XX") && !scoTipoDes.equals("XX")) {
		if (scoTipoOri.trim().equals("1") && scoTipoDes.trim().equals("")) {
		    granted = true;
		} else if (scoTipoOri.trim().equals("2") && scoTipoDes.trim().equals("1")) {
		    granted = true;
		} else if (scoTipoOri.trim().equals("3") && scoTipoDes.trim().equals("2")) {
		    granted = true;
		} else if (scoTipoOri.trim().equals("4") && scoTipoDes.trim().equals("3")) {
		    granted = true;
		}
		break;
	    }
	}
	return granted;
    }
}