package kpi.swing.tools;

import java.awt.event.ComponentEvent;
import java.util.Hashtable;
import javax.swing.JInternalFrame;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultDialogSystem;
import kpi.swing.KpiFileChooser;
import kpi.xml.BIXMLVector;

public class KpiEstruturaExport extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions, kpi.beans.JBIProgressBarListener {

    private String recordType = "ESTEXPORT";
    private String formType = recordType;
    private String parentType = recordType;
    private Hashtable htbScoreCard = new Hashtable();

    public KpiEstruturaExport(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        btnCancelar.setVisible(false);
        biProgressbar.addJBIProgressBarListener(this);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        btnCalcular.setEnabled(true);
        btnCancelar.setEnabled(false);
        btnLog.setEnabled(true);
        event.setEnableFields(pnlDados, true);
        privateRule();
    }

    @Override
    public void disableFields() {
        btnCalcular.setEnabled(false);
        btnCancelar.setEnabled(true);
        btnLog.setEnabled(false);
        event.setEnableFields(pnlDados, false);
    }

    @Override
    public void refreshFields() {
        BIXMLVector scorecard = record.getBIXMLVector("SCORECARDS");
        event.populateCombo(scorecard, "SCOR_ID", htbScoreCard, cboScoreCard);
        tsArvore.setVetor(scorecard);
        setMessageStatus(record.getInt("STATUS"));
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        return new kpi.xml.BIXMLRecord(recordType);
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (cboScoreCard.getSelectedIndex() == 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaExport_00007"));
            errorMessage.append("\n");
            retorno = false;
        }

        if (fldNome.getText().trim().length() == 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaExport_00008"));
            errorMessage.append("\n");
            retorno = false;
        }

        if (chkPrivate.isSelected() && (txtLicense.getText().trim().length() == 0)) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaExport_00015"));
            retorno = false;
        }

        return retorno;
    }

    /**
     * Define a regra de exibi��o dos campos para exporta��o protegida.
     */
    private void privateRule() {
        if (chkPrivate.isSelected()) {
            txtLicense.setEnabled(true);
            lblLicense.setEnabled(true);
        } else {
            txtLicense.setEnabled(false);
            txtLicense.setText("");
            lblLicense.setEnabled(false);
        }
    }

    /**
     * Controla o comportamento da tela de acordo com cada status da exporta��o. 
     * @param status c�digo do status.
     */
    private void setMessageStatus(int status) {
        if (cboScoreCard.getItemCount() > 1) {
            if (status == 1) {
                //"Aguarde... Exportando"
                txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaExport_00009"));
                disableFields();
                startProgressBar();

            } else if (status == 2) {
                //"Aguarde... Parando a exporta��o"
                txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaExport_00010"));

            } else if (status == 3) {
                //"Opera��o finalizada. Verifique o arquivo de log para mais detalhes."
                txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaExport_00011"));
                enableFields();

            } else {
                //"Pressione o bot�o iniciar, para exportar a estrutura."
                txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaExport_00012"));
                enableFields();
                setCloseEnabled(true);
            }
        } else {
            //"N�o existem estruturas pr�-cadastradas para ser exportada."
            txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaExport_00013"));
            disableFields();
        }
    }
    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        pnlMainPanel = new javax.swing.JPanel();
        pnlCenterForm = new javax.swing.JPanel();
        pnlDados = new javax.swing.JPanel();
        txtMesangem = new kpi.beans.JBITextArea();
        lblScoreCard = new javax.swing.JLabel();
        cboScoreCard = new javax.swing.JComboBox();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        chkPrivate = new javax.swing.JCheckBox();
        txtLicense = new javax.swing.JTextField();
        lblLicense = new javax.swing.JLabel();
        chkFonteDeDados = new javax.swing.JCheckBox();
        chkAgendamento = new javax.swing.JCheckBox();
        tsArvore = new kpi.beans.JBITreeSelection();
        biProgressbar = new kpi.beans.JBIProgressBar();
        barCalculo = new javax.swing.JToolBar();
        btnCalcular = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnLog = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiEstruturaExport_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_exportar.gif"))); // NOI18N
        setMaximumSize(new java.awt.Dimension(0, 0));
        setMinimumSize(new java.awt.Dimension(0, 0));
        setPreferredSize(new java.awt.Dimension(456, 427));
        setRequestFocusEnabled(false);

        tapCadastro.setPreferredSize(new java.awt.Dimension(530, 300));

        jPanel1.setLayout(new java.awt.BorderLayout());

        pnlMainPanel.setPreferredSize(new java.awt.Dimension(187, 20));
        pnlMainPanel.setLayout(new java.awt.BorderLayout());

        pnlCenterForm.setLayout(new java.awt.BorderLayout());

        pnlDados.setLayout(null);

        txtMesangem.setEditable(false);
        txtMesangem.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), bundle.getString("KpiEstruturaExport_00006"), javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION)); // NOI18N
        txtMesangem.setFont(new java.awt.Font("Tahoma", 0, 11));
        txtMesangem.setOpaque(false);
        pnlDados.add(txtMesangem);
        txtMesangem.setBounds(20, 240, 400, 80);

        lblScoreCard.setForeground(new java.awt.Color(51, 51, 255));
        lblScoreCard.setText(KpiStaticReferences.getKpiCustomLabels().getSco().concat(":"));
        lblScoreCard.setPreferredSize(new java.awt.Dimension(80, 15));
        pnlDados.add(lblScoreCard);
        lblScoreCard.setBounds(20, 10, 120, 20);
        pnlDados.add(cboScoreCard);
        cboScoreCard.setBounds(20, 30, 400, 22);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setText(bundle.getString("KpiEstruturaExport_00005")); // NOI18N
        pnlDados.add(lblNome);
        lblNome.setBounds(20, 50, 90, 20);

        fldNome.setMaxLength(25);
        pnlDados.add(fldNome);
        fldNome.setBounds(20, 70, 400, 22);

        chkPrivate.setAlignmentY(0.0F);
        chkPrivate.setAutoscrolls(true);
        chkPrivate.setBorderPaintedFlat(true);
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        chkPrivate.setLabel(bundle1.getString("KpiEstruturaExport_00014")); // NOI18N
        chkPrivate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkPrivateActionPerformed(evt);
            }
        });
        pnlDados.add(chkPrivate);
        chkPrivate.setBounds(20, 150, 180, 22);
        chkPrivate.getAccessibleContext().setAccessibleName("");

        pnlDados.add(txtLicense);
        txtLicense.setBounds(20, 200, 400, 22);

        lblLicense.setForeground(new java.awt.Color(51, 51, 255));
        lblLicense.setText("License Key:");
        pnlDados.add(lblLicense);
        lblLicense.setBounds(20, 180, 80, 14);

        chkFonteDeDados.setText("Exportar Fonte de Dados");
        pnlDados.add(chkFonteDeDados);
        chkFonteDeDados.setBounds(20, 100, 180, 22);

        chkAgendamento.setText("Exportar Agendamentos");
        pnlDados.add(chkAgendamento);
        chkAgendamento.setBounds(20, 120, 180, 22);

        tsArvore.setCombo(cboScoreCard);
        pnlDados.add(tsArvore);
        tsArvore.setBounds(420, 30, 25, 22);

        pnlCenterForm.add(pnlDados, java.awt.BorderLayout.CENTER);
        pnlCenterForm.add(biProgressbar, java.awt.BorderLayout.SOUTH);

        pnlMainPanel.add(pnlCenterForm, java.awt.BorderLayout.CENTER);

        jPanel1.add(pnlMainPanel, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiEstruturaExport_00003"), jPanel1); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        barCalculo.setFloatable(false);
        barCalculo.setRollover(true);

        btnCalcular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_play.gif"))); // NOI18N
        btnCalcular.setText(bundle.getString("KpiEstruturaExport_00002")); // NOI18N
        btnCalcular.setPreferredSize(new java.awt.Dimension(80, 23));
        btnCalcular.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        barCalculo.add(btnCalcular);

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_parar.gif"))); // NOI18N
        btnCancelar.setText(bundle.getString("KpiCalcIndicador_00015")); // NOI18N
        btnCancelar.setPreferredSize(new java.awt.Dimension(80, 23));
        btnCancelar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        barCalculo.add(btnCancelar);

        btnLog.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_log.gif"))); // NOI18N
        btnLog.setText(bundle.getString("KpiCalcIndicador_00016")); // NOI18N
        btnLog.setPreferredSize(new java.awt.Dimension(80, 23));
        btnLog.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogActionPerformed(evt);
            }
        });
        barCalculo.add(btnLog);

        btnAjuda.setFont(new java.awt.Font("Dialog", 0, 12));
        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        barCalculo.add(btnAjuda);

        getContentPane().add(barCalculo, java.awt.BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
            event.executeRecord("JOBSTOP");
	}//GEN-LAST:event_btnCancelarActionPerformed

	private void btnLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogActionPerformed
            String diretorio = "\\logs\\metadados\\export\\";
            KpiFileChooser arquivo = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);

            arquivo.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            arquivo.setFileType(".html");
            arquivo.loadServerFiles(diretorio.concat("*.html"));
            arquivo.setVisible(true);

            if (arquivo.isValidFile()) {
                arquivo.setUrlPath("/logs/metadados/export/");
                arquivo.openFile();
            }
	}//GEN-LAST:event_btnLogActionPerformed

	private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
            StringBuffer errorMessage = new StringBuffer();
            if (validateFields(errorMessage)) {
                StringBuilder parametro = new StringBuilder();

                parametro.append(fldNome.getText());
                parametro.append("|");
                parametro.append(event.getComboValue(htbScoreCard, cboScoreCard));
                parametro.append("|");
                parametro.append(chkPrivate.isSelected());
                parametro.append("|");
                parametro.append(txtLicense.getText());
                parametro.append("|");
                parametro.append(chkFonteDeDados.isSelected());
                parametro.append("|");
                parametro.append(chkAgendamento.isSelected());

                event.executeRecord(parametro.toString());

                setMessageStatus(1);
            } else {
                KpiDefaultDialogSystem alerta = new KpiDefaultDialogSystem((JInternalFrame) this);
                alerta.warningMessage(errorMessage.toString());
            }

	}//GEN-LAST:event_btnCalcularActionPerformed

    private void chkPrivateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkPrivateActionPerformed
        privateRule();
}//GEN-LAST:event_chkPrivateActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToolBar barCalculo;
    private kpi.beans.JBIProgressBar biProgressbar;
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnLog;
    private javax.swing.JComboBox cboScoreCard;
    private javax.swing.JCheckBox chkAgendamento;
    private javax.swing.JCheckBox chkFonteDeDados;
    private javax.swing.JCheckBox chkPrivate;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblLicense;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblScoreCard;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlCenterForm;
    private javax.swing.JPanel pnlDados;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlMainPanel;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JTabbedPane tapCadastro;
    private kpi.beans.JBITreeSelection tsArvore;
    private javax.swing.JTextField txtLicense;
    private kpi.beans.JBITextArea txtMesangem;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    private void startProgressBar() {
        biProgressbar.setJobName("kpiestexp_1");
        biProgressbar.startProgressBar();
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void afterExecute() {
    }

    @Override
    public void onStartBar(ComponentEvent componentEvent) {
    }

    @Override
    public void onFinishBar(ComponentEvent componentEvent) {
        setMessageStatus(3);
    }

    @Override
    public void onErrorBar(ComponentEvent componentEvent) {
        setMessageStatus(3);
    }

    @Override
    public void onCancelBar(ComponentEvent componentEvent) {
        setMessageStatus(3);
    }
}
