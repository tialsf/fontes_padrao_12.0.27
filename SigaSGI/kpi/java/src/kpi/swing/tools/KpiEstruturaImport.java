package kpi.swing.tools;

import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import javax.swing.JInternalFrame;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultDialogSystem;
import kpi.swing.KpiFileChooser;
import kpi.xml.BIXMLVector;

public class KpiEstruturaImport extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions, kpi.beans.JBIProgressBarListener {

    private String recordType = "ESTIMPORT";
    private String formType = recordType;
    private String parentType = recordType;
    private java.util.Hashtable htbArqs = new java.util.Hashtable();
    private java.util.Hashtable htbDepto = new java.util.Hashtable();

    public KpiEstruturaImport(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        btnCancelar.setVisible(false);
        biProgressbar.addJBIProgressBarListener(this);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        btnCalcular.setEnabled(true);
        btnCancelar.setEnabled(false);
        btnLog.setEnabled(true);
        event.setEnableFields(pnlDados, true);
    }

    @Override
    public void disableFields() {
        btnCalcular.setEnabled(false);
        btnCancelar.setEnabled(true);
        btnLog.setEnabled(false);
        event.setEnableFields(pnlDados, false);
    }

    @Override
    public void refreshFields() {
        BIXMLVector scorecard = record.getBIXMLVector("SCORECARDS");
        event.populateCombo(record.getBIXMLVector("ARQUIVOS"), "NOME", htbArqs, cboArquivos);
        event.populateCombo(scorecard, "DESCRICAO", htbDepto, cboScorecard);
        tsArvore.setVetor(scorecard);
        lblDataModificacao.setText("");
        lblTamanhoArquivo.setText("");
        setMessageStatus(record.getInt("STATUS"));
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        return new kpi.xml.BIXMLRecord(recordType);
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (cboScorecard.getSelectedIndex() == 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaExport_00007"));
            errorMessage.append("\n");
            retorno = false;
        }

        if (cboArquivos.getSelectedIndex() == 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaImport_00014"));
            errorMessage.append("\n");
            retorno = false;
        }

        return retorno;
    }

    /**
     * Controla o comportamento da tela de acordo com cada status da exporta��o.
     * @param status c�digo do status.
     */
    private void setMessageStatus(int status) {
        if (cboArquivos.getItemCount() > 1) {
            if (status == 1) {
                //"Enquanto a estrutura estiver sendo importada, os dados apresentados no scorecarding podem ficar inconsistentes."
                txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaImport_00009"));
                disableFields();
                startProgressBar();

            } else if (status == 2) {
                //"Aguarde... Parando a importa��o"
                txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaImport_00010"));

            } else {
                //"Selecione o arquivo com estrutura e pressione o bot�o iniciar, para importar os indicadores e departamentos."
                txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaImport_00011"));
                enableFields();
                setCloseEnabled(true);
            }
        } else {
            //"N�o existem estruturas pr�-cadastradas para ser importada."
            txtMesangem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstruturaImport_00012"));
            disableFields();
        }
    }
    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        pnlMainPanel = new javax.swing.JPanel();
        pnlCenterForm = new javax.swing.JPanel();
        pnlDados = new javax.swing.JPanel();
        lblArquivos = new javax.swing.JLabel();
        pnlDetalhes = new javax.swing.JPanel();
        lblItuloTamanhoArquivo = new javax.swing.JLabel();
        lblTituloDataModif = new javax.swing.JLabel();
        lblTamanhoArquivo = new javax.swing.JLabel();
        lblDataModificacao = new javax.swing.JLabel();
        txtMesangem = new kpi.beans.JBITextArea();
        cboScorecard = new javax.swing.JComboBox();
        lblDeptos = new javax.swing.JLabel();
        tsArvore = new kpi.beans.JBITreeSelection();
        cboArquivos = new javax.swing.JComboBox();
        biProgressbar = new kpi.beans.JBIProgressBar();
        barCalculo = new javax.swing.JToolBar();
        btnCalcular = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnLog = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiEstruturaImport_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_importar.gif"))); // NOI18N
        setMinimumSize(new java.awt.Dimension(300, 300));
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(456, 475));

        tapCadastro.setPreferredSize(new java.awt.Dimension(530, 300));

        jPanel1.setLayout(new java.awt.BorderLayout());

        pnlMainPanel.setPreferredSize(new java.awt.Dimension(187, 20));
        pnlMainPanel.setLayout(new java.awt.BorderLayout());

        pnlCenterForm.setLayout(new java.awt.BorderLayout());

        pnlDados.setLayout(null);

        lblArquivos.setForeground(new java.awt.Color(51, 51, 251));
        lblArquivos.setText(bundle.getString("KpiEstruturaImport_00004")); // NOI18N
        lblArquivos.setPreferredSize(new java.awt.Dimension(80, 15));
        pnlDados.add(lblArquivos);
        lblArquivos.setBounds(20, 5, 90, 30);

        pnlDetalhes.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), bundle.getString("KpiEstruturaImport_00005"))); // NOI18N
        pnlDetalhes.setLayout(null);

        lblItuloTamanhoArquivo.setText(bundle.getString("KpiEstruturaImport_00006")); // NOI18N
        pnlDetalhes.add(lblItuloTamanhoArquivo);
        lblItuloTamanhoArquivo.setBounds(10, 25, 120, 14);

        lblTituloDataModif.setText(bundle.getString("KpiEstruturaImport_00007")); // NOI18N
        pnlDetalhes.add(lblTituloDataModif);
        lblTituloDataModif.setBounds(10, 50, 120, 14);

        lblTamanhoArquivo.setText("    ");
        pnlDetalhes.add(lblTamanhoArquivo);
        lblTamanhoArquivo.setBounds(135, 25, 250, 14);

        lblDataModificacao.setText("    ");
        pnlDetalhes.add(lblDataModificacao);
        lblDataModificacao.setBounds(135, 50, 250, 14);

        pnlDados.add(pnlDetalhes);
        pnlDetalhes.setBounds(20, 110, 400, 80);

        txtMesangem.setEditable(false);
        txtMesangem.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), bundle.getString("KpiEstruturaImport_00008"), javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION)); // NOI18N
        txtMesangem.setFont(new java.awt.Font("Tahoma", 0, 11));
        txtMesangem.setOpaque(false);
        pnlDados.add(txtMesangem);
        txtMesangem.setBounds(20, 200, 400, 140);
        pnlDados.add(cboScorecard);
        cboScorecard.setBounds(20, 70, 400, 22);

        lblDeptos.setText(KpiStaticReferences.getKpiCustomLabels().getSco().concat(":"));
        lblDeptos.setPreferredSize(new java.awt.Dimension(80, 15));
        pnlDados.add(lblDeptos);
        lblDeptos.setBounds(20, 50, 130, 20);

        tsArvore.setCombo(cboScorecard);
        pnlDados.add(tsArvore);
        tsArvore.setBounds(420, 70, 25, 22);

        pnlDados.add(cboArquivos);
        cboArquivos.setBounds(20, 30, 400, 22);

        pnlCenterForm.add(pnlDados, java.awt.BorderLayout.CENTER);
        pnlCenterForm.add(biProgressbar, java.awt.BorderLayout.SOUTH);

        pnlMainPanel.add(pnlCenterForm, java.awt.BorderLayout.CENTER);

        jPanel1.add(pnlMainPanel, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiEstruturaImport_00003"), jPanel1); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        barCalculo.setFloatable(false);
        barCalculo.setRollover(true);

        btnCalcular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_play.gif"))); // NOI18N
        btnCalcular.setText(bundle.getString("KpiEstruturaImport_00002")); // NOI18N
        btnCalcular.setPreferredSize(new java.awt.Dimension(80, 23));
        btnCalcular.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        barCalculo.add(btnCalcular);

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_parar.gif"))); // NOI18N
        btnCancelar.setText(bundle.getString("KpiCalcIndicador_00015")); // NOI18N
        btnCancelar.setPreferredSize(new java.awt.Dimension(80, 23));
        btnCancelar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        barCalculo.add(btnCancelar);

        btnLog.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_log.gif"))); // NOI18N
        btnLog.setText(bundle.getString("KpiCalcIndicador_00016")); // NOI18N
        btnLog.setPreferredSize(new java.awt.Dimension(80, 23));
        btnLog.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogActionPerformed(evt);
            }
        });
        barCalculo.add(btnLog);

        btnAjuda.setFont(new java.awt.Font("Dialog", 0, 12));
        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        barCalculo.add(btnAjuda);

        getContentPane().add(barCalculo, java.awt.BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }//GEN-END:initComponents

	private void cboArquivosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboArquivosItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED && cboArquivos.getItemCount() > 0) {
                if (cboArquivos.getSelectedIndex() > 0) {
                    lblDataModificacao.setText(record.getBIXMLVector("ARQUIVOS").get(cboArquivos.getSelectedIndex() - 1).getString("DATE").toString());
                    lblTamanhoArquivo.setText(record.getBIXMLVector("ARQUIVOS").get(cboArquivos.getSelectedIndex() - 1).getString("SIZE").toString());
                } else {
                    lblDataModificacao.setText("");
                    lblTamanhoArquivo.setText("");
                }
            }
	}//GEN-LAST:event_cboArquivosItemStateChanged

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
            event.executeRecord("JOBSTOP");
	}//GEN-LAST:event_btnCancelarActionPerformed

	private void btnLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogActionPerformed
            String diretorio = "\\logs\\metadados\\import\\";
            kpi.swing.KpiFileChooser escolha = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);

            escolha.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            escolha.setFileType(".html");
            escolha.loadServerFiles(diretorio.concat("*.html"));
            escolha.setVisible(true);

            if (escolha.isValidFile()) {
                escolha.setUrlPath("/logs/metadados/import/");
                escolha.openFile();
            }
	}//GEN-LAST:event_btnLogActionPerformed

	private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
            StringBuffer errorMessage = new StringBuffer();
            if (validateFields(errorMessage)) {

                StringBuilder parametro = new StringBuilder();

                parametro.append(event.getComboValue(htbArqs, cboArquivos));
                parametro.append("|");
                parametro.append(event.getComboValue(htbDepto, cboScorecard));

                event.executeRecord(parametro.toString());
                setMessageStatus(1);

            } else {
                KpiDefaultDialogSystem alerta = new KpiDefaultDialogSystem((JInternalFrame) this);
                alerta.warningMessage(errorMessage.toString());
            }
	}//GEN-LAST:event_btnCalcularActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToolBar barCalculo;
    private kpi.beans.JBIProgressBar biProgressbar;
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnLog;
    private javax.swing.JComboBox cboArquivos;
    private javax.swing.JComboBox cboScorecard;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblArquivos;
    private javax.swing.JLabel lblDataModificacao;
    private javax.swing.JLabel lblDeptos;
    private javax.swing.JLabel lblItuloTamanhoArquivo;
    private javax.swing.JLabel lblTamanhoArquivo;
    private javax.swing.JLabel lblTituloDataModif;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlCenterForm;
    private javax.swing.JPanel pnlDados;
    private javax.swing.JPanel pnlDetalhes;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlMainPanel;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JTabbedPane tapCadastro;
    private kpi.beans.JBITreeSelection tsArvore;
    private kpi.beans.JBITextArea txtMesangem;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    private void startProgressBar() {
        biProgressbar.setJobName("kpiestimp_1");
        biProgressbar.startProgressBar();
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void afterExecute() {
    }

    @Override
    public void onStartBar(ComponentEvent componentEvent) {
    }

    @Override
    public void onFinishBar(ComponentEvent componentEvent) {
        setMessageStatus(0);
    }

    @Override
    public void onErrorBar(ComponentEvent componentEvent) {
        setMessageStatus(0);
    }

    @Override
    public void onCancelBar(ComponentEvent componentEvent) {
        setMessageStatus(0);
    }
}
