package kpi.swing.tools;

import kpi.core.KpiDataController;
import kpi.util.KpiToolKit;

public class KpiExportPlan extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String recordType = "EXPORTPLAN";
    private String formType = recordType;//Tipo do Formulario
    private String parentType = recordType;//Tipo formulario pai, caso haja.
    private kpi.core.KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();
    private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();
    java.util.Hashtable htbStatus = new java.util.Hashtable();
    java.util.Hashtable htbSituacao = new java.util.Hashtable();
    java.util.Hashtable htbTipo = new java.util.Hashtable();

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        kpi.xml.BIXMLVector xmlResps = record.getBIXMLVector("USUARIOS");
        kpi.xml.BIXMLVector xmlRespSelected = record.getBIXMLVector("USUARIOS");
        splResp.setDataSource(xmlResps, xmlRespSelected, "RESPONSAVEIS", "RESPONSAVEL");
        event.populateCombo(record.getBIXMLVector("LSTSTATUS"), "STATUS", htbStatus, cbxStatus);
        event.populateCombo(record.getBIXMLVector("LSTSITUACAO"), "SITUACAO", htbSituacao, cbxSituacao);
        event.populateCombo(record.getBIXMLVector("LSTTIPOACAO"), "TIPOACAO", htbTipo, cbxTipo);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        return new kpi.xml.BIXMLRecord(recordType);
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        return retorno;
    }

    /**
     *Cria a requisi��o para ser enviada ao ADPVL.
     */
    private String createRequest() {
        StringBuilder parms = new StringBuilder("");
        parms.append("");
        parms.append(event.getComboValue(htbSituacao, cbxSituacao));
        parms.append("|");
        parms.append("");
        parms.append(event.getComboValue(htbStatus, cbxStatus));
        parms.append("|");
        parms.append("");
        parms.append(event.getComboValue(htbTipo, cbxTipo));
        parms.append("|");

        if (splResp.getRowCount() == 0) {
            parms.append("'-1'");
        } else {
            for (int i = 0; i < splResp.getRowCount(); i++) {
                parms.append("'");
                parms.append(splResp.getXMLData().get(i).getString("ID"));
                parms.append("'");
                if (i != splResp.getRowCount() - 1) {
                    parms.append(",");
                }
            }
        }

        return parms.toString();
    }
    private void initComponents() {//GEN-BEGIN:initComponents

        groupOrdem = new javax.swing.ButtonGroup();
        tapCadastro = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        pnlMainPanel = new javax.swing.JPanel();
        pnlCenterForm = new javax.swing.JPanel();
        lblResp = new javax.swing.JLabel();
        splResp = new kpi.beans.JBISelectionPanel();
        lblStatus = new javax.swing.JLabel();
        cbxStatus = new javax.swing.JComboBox();
        lblSituacao = new javax.swing.JLabel();
        cbxSituacao = new javax.swing.JComboBox();
        lblTipo = new javax.swing.JLabel();
        cbxTipo = new javax.swing.JComboBox();
        barCalculo = new javax.swing.JToolBar();
        btnGerar = new javax.swing.JButton();
        btnAtualizar = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiExportPlan_00008")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_gera_planilha.gif"))); // NOI18N
        setMinimumSize(new java.awt.Dimension(300, 300));
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(497, 422));
        setRequestFocusEnabled(false);

        tapCadastro.setPreferredSize(new java.awt.Dimension(500, 340));

        jPanel1.setLayout(new java.awt.BorderLayout());

        pnlMainPanel.setPreferredSize(new java.awt.Dimension(187, 20));
        pnlMainPanel.setLayout(new java.awt.BorderLayout());

        pnlCenterForm.setLayout(null);

        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblResp.setText(bundle1.getString("KpiExportPlan_00006")); // NOI18N
        lblResp.setPreferredSize(new java.awt.Dimension(80, 15));
        pnlCenterForm.add(lblResp);
        lblResp.setBounds(20, 135, 80, 20);

        splResp.setAlignmentX(0.0F);
        splResp.setAlignmentY(0.0F);
        pnlCenterForm.add(splResp);
        splResp.setBounds(16, 150, 455, 175);

        lblStatus.setText(bundle1.getString("KpiExportPlan_00005")); // NOI18N
        pnlCenterForm.add(lblStatus);
        lblStatus.setBounds(20, 50, 80, 20);

        pnlCenterForm.add(cbxStatus);
        cbxStatus.setBounds(20, 70, 400, 22);

        lblSituacao.setText(bundle1.getString("KpiExportPlan_00004")); // NOI18N
        pnlCenterForm.add(lblSituacao);
        lblSituacao.setBounds(20, 10, 80, 20);

        pnlCenterForm.add(cbxSituacao);
        cbxSituacao.setBounds(20, 30, 400, 22);

        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblTipo.setText(bundle2.getString("KpiExportPlan_00007")); // NOI18N
        pnlCenterForm.add(lblTipo);
        lblTipo.setBounds(20, 90, 80, 20);

        pnlCenterForm.add(cbxTipo);
        cbxTipo.setBounds(20, 110, 400, 22);

        pnlMainPanel.add(pnlCenterForm, java.awt.BorderLayout.CENTER);

        jPanel1.add(pnlMainPanel, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle1.getString("KpiExportPlan_00003"), jPanel1); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        barCalculo.setFloatable(false);
        barCalculo.setRollover(true);

        btnGerar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_play.gif"))); // NOI18N
        btnGerar.setText(bundle1.getString("KpiExportPlan_00001")); // NOI18N
        btnGerar.setPreferredSize(new java.awt.Dimension(80, 23));
        btnGerar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnGerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGerarActionPerformed(evt);
            }
        });
        barCalculo.add(btnGerar);

        btnAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnAtualizar.setText(bundle1.getString("KpiExportPlan_00002")); // NOI18N
        btnAtualizar.setPreferredSize(new java.awt.Dimension(110, 23));
        btnAtualizar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarActionPerformed(evt);
            }
        });
        barCalculo.add(btnAtualizar);

        btnAjuda.setFont(new java.awt.Font("Dialog", 0, 12));
        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle2.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        barCalculo.add(btnAjuda);

        getContentPane().add(barCalculo, java.awt.BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnAtualizarActionPerformed

	private void btnGerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGerarActionPerformed
            event.executeRecord(createRequest());
	}//GEN-LAST:event_btnGerarActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToolBar barCalculo;
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnAtualizar;
    private javax.swing.JButton btnGerar;
    private javax.swing.JComboBox cbxSituacao;
    private javax.swing.JComboBox cbxStatus;
    private javax.swing.JComboBox cbxTipo;
    private javax.swing.ButtonGroup groupOrdem;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblResp;
    private javax.swing.JLabel lblSituacao;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlCenterForm;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlMainPanel;
    private javax.swing.JPanel pnlRightForm;
    private kpi.beans.JBISelectionPanel splResp;
    private javax.swing.JTabbedPane tapCadastro;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiExportPlan(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void afterExecute() {
        if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
            int iOption = this.dialog.confirmMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00013"));

            if (iOption == javax.swing.JOptionPane.YES_OPTION) {
                /*Requisita a URL para download da planilha de planos de acao.*/
                KpiToolKit oToolKit = new KpiToolKit();
                oToolKit.browser("sgidownload.apw?file=sgiExport\\SGIEXPORT001.xls"); //FOLHA DE CALCULO DE PLANOS DE A��O

            }
        }
    }
}
