package kpi.swing.tools;

import java.awt.event.MouseEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Locale;
import javax.swing.JFileChooser;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import kpi.applet.KpiMainPanel;
import kpi.core.KpiDataController;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiFileChooser;
import kpi.swing.KpiSelectedTree;
import kpi.swing.KpiSelectedTreeCellRender;
import kpi.swing.KpiSelectedTreeNode;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLException;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiGeraPlanilha extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = new String("GERAPLANILHA");
    private String recordType2 = new String("IMPPLANILHA");
    private String formType = recordType;//Tipo do Formulario
    private String parentType = recordType;//Tipo formulario pai, caso haja.
    private java.util.Hashtable htbScoreCard = new java.util.Hashtable(), htbTipos = new java.util.Hashtable();
    private kpi.core.KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();
    private int idMessage = 0;
    private kpi.core.KpiDateBase dateBase = kpi.core.KpiStaticReferences.getKpiDateBase();
    private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();
    private String[] opcExportacao = {java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00015"),
        java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00016"),
        java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00017")
    };
    private static final String pathImport = "sgiimport";
    //Tipos de Execu��o
    private final static int EXEC_GERAPLANILHA = 1, EXEC_DELFILE = 2, EXEC_IMPORT = 3;
    private int executeType = 0;

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        dtpPeriodoDe.setCalendar(dateBase.getDataAnaliseDe());
        dtpPeriodoAte.setCalendar(dateBase.getDataAnaliseDe());
        
        jTree1.setModel(new javax.swing.tree.DefaultTreeModel(createTree(record)));
        jTree1.setCellRenderer(new KpiSelectedTreeCellRender(kpi.core.KpiStaticReferences.getKpiImageResources()));
        jTree1.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        KpiSelectedTree.expandAll(jTree1, true);
        
        event.populateCombo(record.getBIXMLVector("TIPOS"), "TIPO_ATU", htbTipos, cbbTipoAtualizacao);
        loadImportFiles();
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        return new kpi.xml.BIXMLRecord(recordType);
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;
        
        // Recebe a �rvore de ScoreCards.
        BIXMLVector vctSco = getXMLScoTree();

        // Parte do princ�pio de que n�o h� ScoreCards Selecionados.
        retorno = false;

        for (int nCont = 0; nCont < vctSco.size(); nCont++){
            if (vctSco.get(nCont).getBoolean("SELECTED")){
                retorno = true; // Caso haja no minimo um SC selecionado � colocado a valida��o como True.
            }
        }

        // Caso n�o haja nenhum scoredCard selecionado.
        if (retorno == false) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00031"));
            errorMessage.append("\n");
        }
        
        return retorno;
    }

    /**
     *Cria a requisi��o para ser enviada ao ADPVL.
     */
    private String createRequest() {
        /*Criado novo modelo de procuras de scoreboards*/
        BIXMLVector vctSco = getXMLScoTree();
        /*Recupera as datas DE e ATE.*/
        Date periodoDe = dtpPeriodoDe.getDate();
        Date periodoAte = dtpPeriodoAte.getDate();
        /*Define o formato fixo em que as datas ser�o enviada para o Protheus.*/
        SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt_BR"));

        StringBuffer parms = new StringBuffer("");

        parms.append(formatador.format(periodoDe).toString());
        parms.append("|");
        parms.append(formatador.format(periodoAte).toString());
        parms.append("|");
        
        if (vctSco.size() == 0){
            parms.append("'-1'");
        } else {
            for (int nCont = 0; nCont < vctSco.size(); nCont++){
                if (vctSco.get(nCont).getBoolean("SELECTED")){
                    parms.append("'");
                    parms.append(vctSco.get(nCont).getString("ID"));
                    parms.append("'"); 
                    if (nCont != vctSco.size()) {
                        parms.append(",");
                    }                    
                }
            }
        }
        //Colocado deste jeito para n�o perder performance
        parms.replace(0, parms.length(), parms.substring(0,parms.length()-1));
        parms.append("|");

        if (rdbScorecard.isSelected()) {
            parms.append("1");
        } else if (rdbIndicador.isSelected()) {
            parms.append("2");
        } else {
            parms.append("3");
        }
        parms.append("|");
        parms.append("\\sgiExport\\");
        parms.append("|");
        if (chkExportarValores.isSelected()) {
            parms.append(".t.");
        } else {
            parms.append(".f.");
        }

        parms.append("|");
        parms.append(cbbTipoExportacao.getSelectedIndex());

        parms.append("|");
        parms.append(cbbTipoAtualizacao.getSelectedIndex());

        return parms.toString();
    }

    public void loadImportFiles() {
        String url = "//sgiimport//*.*";
        KpiDataController dataController;
        dataController = kpi.core.KpiStaticReferences.getKpiDataController();
        kpi.xml.BIXMLRecord recordAux = dataController.listServerFiles(url);

        if (dataController.getStatus() != KpiDataController.KPI_ST_OK) {
            if (dataController.getStatus() == KpiDataController.KPI_ST_GENERALERROR) {
                dialog.errorMessage(dataController.getErrorMessage());
            } else {
                dialog.errorMessage(dataController.getStatus());
            }

            if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                KpiStaticReferences.getKpiApplet().resetApplet();
            }
        }

        kpi.xml.BIXMLRecord recFiles = new kpi.xml.BIXMLRecord("FILE");
        kpi.xml.BIXMLAttributes attIndicador = new kpi.xml.BIXMLAttributes();
        attIndicador.set("TIPO", "ARQUIVO");
        attIndicador.set("RETORNA", "F");

        attIndicador.set("TAG000", "NAME");
        attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00026")); //"Nome"
        attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_STRING);
        attIndicador.set("EDT000", "F");
        attIndicador.set("CUM000", "F");

        attIndicador.set("TAG001", "DATE");
        attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00027")); //"Data da cria��o"
        attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_STRING);
        attIndicador.set("EDT001", "F");
        attIndicador.set("CUM001", "F");

        attIndicador.set("TAG002", "SIZE");
        attIndicador.set("CAB002", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00028")); //"Tamanho"
        attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_STRING);
        attIndicador.set("EDT002", "F");
        attIndicador.set("CUM002", "F");

        recFiles = recordAux;
        kpi.xml.BIXMLVector vctFiles = null;
        vctFiles = recFiles.getBIXMLVector("ARQUIVOS");
        vctFiles.setAttributes(attIndicador);

        this.jBIXMLFiles.setDataSource(vctFiles);
        this.jBIXMLFiles.setHeaderHeight(20);
        this.jBIXMLFiles.setRowHeight(22);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groupOrdem = new javax.swing.ButtonGroup();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlMainPanel = new javax.swing.JPanel();
        pnlExportCenter = new javax.swing.JPanel();
        pnlCenter = new javax.swing.JPanel();
        pnlFiltro = new javax.swing.JPanel();
        lblExportar = new javax.swing.JLabel();
        lblDataAte = new javax.swing.JLabel();
        lblDataDe = new javax.swing.JLabel();
        scrPeriodoDe = new javax.swing.JScrollPane();
        dtpPeriodoDe = new pv.jfcx.JPVDatePlus();
        scrPeriodoAte = new javax.swing.JScrollPane();
        dtpPeriodoAte = new pv.jfcx.JPVDatePlus();
        pnlOrdem = new javax.swing.JPanel();
        rdbScorecard = new javax.swing.JRadioButton();
        rdbIndicador = new javax.swing.JRadioButton();
        rdbCodCli = new javax.swing.JRadioButton();
        chkExportarValores = new javax.swing.JCheckBox();
        lblFiltrar = new javax.swing.JLabel();
        cbbTipoAtualizacao = new javax.swing.JComboBox(opcExportacao);
        cbbTipoExportacao = new javax.swing.JComboBox(opcExportacao);
        jPanel1 = new javax.swing.JPanel();
        scrollTeste = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jBarExport = new javax.swing.JToolBar();
        btnCalcular = new javax.swing.JButton();
        btnStatus = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        pnlMainUpload = new javax.swing.JPanel();
        pnlUploadCenter = new javax.swing.JPanel();
        jBarDoc = new javax.swing.JToolBar();
        btnDocUpload = new javax.swing.JButton();
        btnDocExcluir = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        btnDocPlay = new javax.swing.JButton();
        btnDocAtualizar = new javax.swing.JButton();
        btnLog = new javax.swing.JButton();
        btnAjuda2 = new javax.swing.JButton();
        jBIXMLFiles = new kpi.beans.JBIXMLTable();
        barCalculo = new javax.swing.JToolBar();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiGeraPlanilha_00020")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_gera_planilha.gif"))); // NOI18N
        setMinimumSize(new java.awt.Dimension(300, 300));
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 380));
        setPreferredSize(new java.awt.Dimension(707, 427));
        setRequestFocusEnabled(false);

        tapCadastro.setPreferredSize(new java.awt.Dimension(500, 420));

        pnlMainPanel.setLayout(new java.awt.BorderLayout());

        pnlExportCenter.setLayout(new java.awt.BorderLayout());

        pnlCenter.setLayout(new java.awt.BorderLayout());

        pnlFiltro.setMinimumSize(new java.awt.Dimension(200, 120));
        pnlFiltro.setPreferredSize(new java.awt.Dimension(600, 200));
        pnlFiltro.setLayout(null);

        lblExportar.setText(bundle.getString("KpiGeraPlanilha_00018")); // NOI18N
        lblExportar.setPreferredSize(new java.awt.Dimension(30, 15));
        pnlFiltro.add(lblExportar);
        lblExportar.setBounds(20, 50, 100, 20);

        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblDataAte.setText(bundle1.getString("KpiGeraPlanilha_00005")); // NOI18N
        lblDataAte.setPreferredSize(new java.awt.Dimension(30, 15));
        pnlFiltro.add(lblDataAte);
        lblDataAte.setBounds(240, 10, 100, 20);

        lblDataDe.setText(bundle1.getString("KpiGeraPlanilha_00004")); // NOI18N
        lblDataDe.setPreferredSize(new java.awt.Dimension(30, 15));
        pnlFiltro.add(lblDataDe);
        lblDataDe.setBounds(20, 10, 100, 20);

        scrPeriodoDe.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrPeriodoDe.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        dtpPeriodoDe.setBorderStyle(0);
        dtpPeriodoDe.setButtonBorder(0);
        dtpPeriodoDe.setCaretInverted(true);
        dtpPeriodoDe.setFreeEntry(true);
        dtpPeriodoDe.setUseLocale(true);
        dtpPeriodoDe.setWaitForCalendarDate(true);
        scrPeriodoDe.setViewportView(dtpPeriodoDe);

        pnlFiltro.add(scrPeriodoDe);
        scrPeriodoDe.setBounds(20, 30, 180, 22);

        scrPeriodoAte.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrPeriodoAte.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        dtpPeriodoAte.setBorderStyle(0);
        dtpPeriodoAte.setButtonBorder(0);
        dtpPeriodoAte.setUseLocale(true);
        dtpPeriodoAte.setWaitForCalendarDate(true);
        scrPeriodoAte.setViewportView(dtpPeriodoAte);

        pnlFiltro.add(scrPeriodoAte);
        scrPeriodoAte.setBounds(240, 30, 180, 22);

        pnlOrdem.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), bundle1.getString("KpiGeraPlanilha_00007"))); // NOI18N
        pnlOrdem.setLayout(null);

        groupOrdem.add(rdbScorecard);
        rdbScorecard.setSelected(true);
        rdbScorecard.setText(bundle1.getString("KpiGeraPlanilha_00008")); // NOI18N
        rdbScorecard.setName(""); // NOI18N
        pnlOrdem.add(rdbScorecard);
        rdbScorecard.setBounds(10, 20, 120, 15);

        groupOrdem.add(rdbIndicador);
        rdbIndicador.setText(bundle1.getString("KpiGeraPlanilha_00009")); // NOI18N
        rdbIndicador.setName(""); // NOI18N
        pnlOrdem.add(rdbIndicador);
        rdbIndicador.setBounds(10, 40, 130, 15);

        groupOrdem.add(rdbCodCli);
        rdbCodCli.setText(bundle1.getString("KpiGeraPlanilha_00010")); // NOI18N
        rdbCodCli.setName(""); // NOI18N
        pnlOrdem.add(rdbCodCli);
        rdbCodCli.setBounds(10, 60, 130, 15);

        pnlFiltro.add(pnlOrdem);
        pnlOrdem.setBounds(20, 100, 180, 90);

        chkExportarValores.setText(bundle1.getString("KpiGeraPlanilha_00014")); // NOI18N
        chkExportarValores.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkExportarValores.setMargin(new java.awt.Insets(0, 0, 0, 0));
        pnlFiltro.add(chkExportarValores);
        chkExportarValores.setBounds(240, 100, 180, 20);
        chkExportarValores.getAccessibleContext().setAccessibleName("Exportar valores\n");

        lblFiltrar.setText(bundle.getString("KpiGeraPlanilha_00019")); // NOI18N
        lblFiltrar.setPreferredSize(new java.awt.Dimension(30, 15));
        pnlFiltro.add(lblFiltrar);
        lblFiltrar.setBounds(240, 50, 100, 20);

        pnlFiltro.add(cbbTipoAtualizacao);
        cbbTipoAtualizacao.setBounds(240, 70, 180, 22);

        pnlFiltro.add(cbbTipoExportacao);
        cbbTipoExportacao.setBounds(20, 70, 180, 22);

        pnlCenter.add(pnlFiltro, java.awt.BorderLayout.NORTH);

        jPanel1.setPreferredSize(new java.awt.Dimension(900, 600));
        jPanel1.setLayout(new java.awt.BorderLayout());

        scrollTeste.setPreferredSize(new java.awt.Dimension(600, 350));

        jTree1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        jTree1.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        jTree1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTree1MouseClicked(evt);
            }
        });
        scrollTeste.setViewportView(jTree1);

        jPanel1.add(scrollTeste, java.awt.BorderLayout.CENTER);

        pnlCenter.add(jPanel1, java.awt.BorderLayout.CENTER);

        pnlExportCenter.add(pnlCenter, java.awt.BorderLayout.CENTER);

        jBarExport.setFloatable(false);
        jBarExport.setRollover(true);

        btnCalcular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_play.gif"))); // NOI18N
        btnCalcular.setText(bundle1.getString("KpiGeraPlanilha_00001")); // NOI18N
        btnCalcular.setPreferredSize(new java.awt.Dimension(80, 23));
        btnCalcular.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGerarActionPerformed(evt);
            }
        });
        jBarExport.add(btnCalcular);

        btnStatus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnStatus.setText(bundle1.getString("KpiGeraPlanilha_00002")); // NOI18N
        btnStatus.setPreferredSize(new java.awt.Dimension(110, 23));
        btnStatus.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarActionPerformed(evt);
            }
        });
        jBarExport.add(btnStatus);

        btnAjuda.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        jBarExport.add(btnAjuda);

        pnlExportCenter.add(jBarExport, java.awt.BorderLayout.NORTH);

        pnlMainPanel.add(pnlExportCenter, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiGeraPlanilha_00003"), pnlMainPanel); // NOI18N

        pnlMainUpload.setLayout(new java.awt.BorderLayout());

        pnlUploadCenter.setLayout(new java.awt.BorderLayout());

        jBarDoc.setFloatable(false);
        jBarDoc.setRollover(true);

        btnDocUpload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_upload.gif"))); // NOI18N
        btnDocUpload.setText(bundle.getString("KpiGeraPlanilha_00022")); // NOI18N
        btnDocUpload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDocUploadActionPerformed(evt);
            }
        });
        jBarDoc.add(btnDocUpload);

        btnDocExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDocExcluir.setText(bundle.getString("KpiGeraPlanilha_00023")); // NOI18N
        btnDocExcluir.setMaximumSize(new java.awt.Dimension(65, 27));
        btnDocExcluir.setMinimumSize(new java.awt.Dimension(65, 27));
        btnDocExcluir.setPreferredSize(new java.awt.Dimension(65, 9));
        btnDocExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDocExcluirActionPerformed(evt);
            }
        });
        jBarDoc.add(btnDocExcluir);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setMaximumSize(new java.awt.Dimension(5, 15));
        jSeparator1.setPreferredSize(new java.awt.Dimension(10, 10));
        jBarDoc.add(jSeparator1);

        btnDocPlay.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_play.gif"))); // NOI18N
        btnDocPlay.setText(bundle.getString("KpiGeraPlanilha_00024")); // NOI18N
        btnDocPlay.setPreferredSize(new java.awt.Dimension(80, 23));
        btnDocPlay.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnDocPlay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDocPlaybtnGerarActionPerformed(evt);
            }
        });
        jBarDoc.add(btnDocPlay);

        btnDocAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnDocAtualizar.setText(bundle1.getString("KpiGeraPlanilha_00002")); // NOI18N
        btnDocAtualizar.setPreferredSize(new java.awt.Dimension(110, 23));
        btnDocAtualizar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnDocAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDocAtualizarActionPerformed(evt);
            }
        });
        jBarDoc.add(btnDocAtualizar);

        btnLog.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_log.gif"))); // NOI18N
        btnLog.setText(bundle.getString("KpiGeraPlanilha_00025")); // NOI18N
        btnLog.setPreferredSize(new java.awt.Dimension(80, 23));
        btnLog.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogActionPerformed(evt);
            }
        });
        jBarDoc.add(btnLog);

        btnAjuda2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        btnAjuda2.setText(bundle2.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda2.setFocusable(false);
        btnAjuda2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAjuda2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjuda2ActionPerformed(evt);
            }
        });
        jBarDoc.add(btnAjuda2);

        pnlUploadCenter.add(jBarDoc, java.awt.BorderLayout.NORTH);
        pnlUploadCenter.add(jBIXMLFiles, java.awt.BorderLayout.CENTER);

        pnlMainUpload.add(pnlUploadCenter, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiGeraPlanilha_00021"), pnlMainUpload); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        tapCadastro.getAccessibleContext().setAccessibleName("");

        getContentPane().add(barCalculo, java.awt.BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogActionPerformed
            String serverFolder = new String("\\logs\\imports\\");
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            frmChooser.setFileType(".html");
            frmChooser.loadServerFiles(serverFolder.concat("*.html"));
            frmChooser.setVisible(true);

            frmChooser.setUrlPath("/logs/imports/");
            frmChooser.openFile();
	}//GEN-LAST:event_btnLogActionPerformed

	private void btnDocAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDocAtualizarActionPerformed
            loadImportFiles();
	}//GEN-LAST:event_btnDocAtualizarActionPerformed

	private void btnDocPlaybtnGerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDocPlaybtnGerarActionPerformed
            executeType = EXEC_IMPORT;
            KpiStaticReferences.getKpiMainPanel().setWorkStatus(KpiMainPanel.WORKST_RED);
            event.executeRecord("IMPORTFILE|".concat(pathImport));
	}//GEN-LAST:event_btnDocPlaybtnGerarActionPerformed

	private void btnDocExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDocExcluirActionPerformed
            int linhaSelecionada = jBIXMLFiles.getSelectedRow();
            if (linhaSelecionada > -1) {
                StringBuilder request = new StringBuilder();
                kpi.xml.BIXMLRecord recLinha = jBIXMLFiles.getXMLData().get(linhaSelecionada);
                request.append("DELFILE|");
                request.append(recLinha.getString("NAME"));
                executeType = EXEC_DELFILE;
                event.executeRecord(request.toString());
            }

	}//GEN-LAST:event_btnDocExcluirActionPerformed

	private void btnDocUploadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDocUploadActionPerformed
            try {
                JFileChooser chooser = new JFileChooser();
                chooser.setDialogType(JFileChooser.CUSTOM_DIALOG);
                chooser.setApproveButtonText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00030")); //"Selecionar"
                chooser.setApproveButtonToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00031")); //"Selecionar Planilha"
                chooser.setAcceptAllFileFilterUsed(false);
                chooser.addChoosableFileFilter(new kpi.util.kpiFileFilter("txt", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00029").concat(" TXT")));
                chooser.addChoosableFileFilter(new kpi.util.kpiFileFilter("csv", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00029").concat(" CSV")));
                int retval = chooser.showDialog(this, null);

                if (retval == JFileChooser.APPROVE_OPTION) {

                    kpi.swing.KpiDefaultFrameFunctions frame =
                            kpi.core.KpiStaticReferences.getKpiFormController().getForm("KPIUPLOAD", "0", "");
                    kpi.swing.tools.KpiUpload frmUpload = (kpi.swing.tools.KpiUpload) frame;
                    File oFile = chooser.getSelectedFile();
                    frmUpload.setPathDest("\\sgiimport\\");
                    frmUpload.setParentType("GERAPLANILHA");
                    frmUpload.startUpload(oFile);
                }
            } catch (java.security.AccessControlException e) {
                kpi.swing.KpiDefaultDialogSystem oDlg = new kpi.swing.KpiDefaultDialogSystem(this);
                if (oDlg.confirmMessage("Arquivo de permiss�o n�o encontrado. \nDeseja salvar o arquivo?", javax.swing.JOptionPane.WARNING_MESSAGE) == javax.swing.JOptionPane.YES_OPTION) {
                    kpi.applet.KpiMainPanel.openUrlPolitica();
                }
            } catch (Exception e) {
                System.out.println(e);
            }

	}//GEN-LAST:event_btnDocUploadActionPerformed

	private void btnAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnAtualizarActionPerformed

	private void btnGerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGerarActionPerformed
            executeType = EXEC_GERAPLANILHA;
            event.executeRecord(createRequest());            
	}//GEN-LAST:event_btnGerarActionPerformed

    private void btnAjuda2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjuda2ActionPerformed
        event.loadHelp(recordType2);
    }//GEN-LAST:event_btnAjuda2ActionPerformed

    private void jTree1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTree1MouseClicked
                int x = evt.getX();
        int y = evt.getY();
        int row = jTree1.getRowForLocation(x, y);
        TreePath path = jTree1.getPathForRow(row);

        if (path != null) {
            DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) path.getLastPathComponent();
            KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
            boolean bSelected = !(node.isSelected());
            node.setSelected(bSelected);

            if (evt.getButton() == MouseEvent.BUTTON1) {
                selectedFromRoot(nodeAux, bSelected);
            }

            ((DefaultTreeModel) jTree1.getModel()).nodeChanged(node);

            jTree1.revalidate();
            jTree1.repaint();
        }
    }//GEN-LAST:event_jTree1MouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToolBar barCalculo;
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnAjuda2;
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnDocAtualizar;
    private javax.swing.JButton btnDocExcluir;
    private javax.swing.JButton btnDocPlay;
    private javax.swing.JButton btnDocUpload;
    private javax.swing.JButton btnLog;
    private javax.swing.JButton btnStatus;
    private javax.swing.JComboBox cbbTipoAtualizacao;
    private javax.swing.JComboBox cbbTipoExportacao;
    private javax.swing.JCheckBox chkExportarValores;
    private pv.jfcx.JPVDatePlus dtpPeriodoAte;
    private pv.jfcx.JPVDatePlus dtpPeriodoDe;
    private javax.swing.ButtonGroup groupOrdem;
    private kpi.beans.JBIXMLTable jBIXMLFiles;
    private javax.swing.JToolBar jBarDoc;
    private javax.swing.JToolBar jBarExport;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTree jTree1;
    private javax.swing.JLabel lblDataAte;
    private javax.swing.JLabel lblDataDe;
    private javax.swing.JLabel lblExportar;
    private javax.swing.JLabel lblFiltrar;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlCenter;
    private javax.swing.JPanel pnlExportCenter;
    private javax.swing.JPanel pnlFiltro;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlMainPanel;
    private javax.swing.JPanel pnlMainUpload;
    private javax.swing.JPanel pnlOrdem;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlUploadCenter;
    private javax.swing.JRadioButton rdbCodCli;
    private javax.swing.JRadioButton rdbIndicador;
    private javax.swing.JRadioButton rdbScorecard;
    private javax.swing.JScrollPane scrPeriodoAte;
    private javax.swing.JScrollPane scrPeriodoDe;
    private javax.swing.JScrollPane scrollTeste;
    private javax.swing.JTabbedPane tapCadastro;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiGeraPlanilha(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
        loadImportFiles();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void afterExecute() {
        switch (executeType) {
            case EXEC_GERAPLANILHA:

                if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
                    int iOption = this.dialog.confirmMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00013"));

                    if (iOption == javax.swing.JOptionPane.YES_OPTION) {
                        /*Requisita a URL para download da planilha para importa��o.*/
                        KpiToolKit oToolKit = new KpiToolKit();
                        oToolKit.browser("sgidownload.apw?file=sgiExport\\SGIEXPORT002.xls"); //FOLHA DE CALCULO DE DADOS
                    }
                }
                break;
            case EXEC_DELFILE:
                loadImportFiles();
                break;
            case EXEC_IMPORT:
                loadImportFiles();
                KpiStaticReferences.getKpiMainPanel().refreshWorkStatus();
                break;
        }
    }
    
    public DefaultMutableTreeNode createTree(BIXMLRecord treeXML) {
        DefaultMutableTreeNode root = insertGroup(treeXML.getBIXMLVector("DPTS"), null);
        return root;
    }

    public DefaultMutableTreeNode insertRecord(BIXMLRecord record, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiSelectedTreeNode oScoNode = new KpiSelectedTreeNode(
                record.getAttributes().getString("ID"),
                record.getAttributes().getString("NOME"),
                record.getAttributes().getBoolean("ENABLED"),
                record.getAttributes().getBoolean("SELECTED"),
                record.getAttributes().getInt("IMAGE"));

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oScoNode);

        for (Iterator e = record.getKeyNames(); e.hasNext();) {
            String strChave = (String) e.next();

            if (record.getBIXMLVector(strChave).getAttributes().contains("TIPO")) {
                child = insertGroup(record.getBIXMLVector(strChave), child);
            } else {
                child = insertRecord(record.getBIXMLVector(strChave), child);
            }
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    //Novo este
    public DefaultMutableTreeNode insertRecord(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiSelectedTreeNode oScoNode = new KpiSelectedTreeNode(
                vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"),
                vector.getAttributes().getBoolean("ENABLED"),
                vector.getAttributes().getBoolean("SELECTED"),
                vector.getAttributes().getInt("IMAGE"));

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oScoNode);

        for (int i = 0; i < vector.size(); i++) {
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    public DefaultMutableTreeNode insertGroup(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        DefaultMutableTreeNode child = new DefaultMutableTreeNode(
                new KpiSelectedTreeNode(vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"),
                vector.getAttributes().getBoolean("ENABLED"),
                vector.getAttributes().getBoolean("SELECTED"),
                vector.getAttributes().getInt("IMAGE")));

        for (int i = 0; i < vector.size(); i++) {
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }
    
    private void selectedFromRoot(DefaultMutableTreeNode oNode, boolean bSelected) {
        Enumeration e = oNode.children();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) e.nextElement();
            KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
            node.setSelected(bSelected);
            selectedFromRoot(nodeAux, bSelected);
        }
    }
    
    protected BIXMLVector getXMLScoTree() {
        DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) jTree1.getModel().getRoot();
        BIXMLVector vctSco = new BIXMLVector("SCOS", "SCO");
        KpiSelectedTreeNode oNode;

        while (oMutTreeNode != null) {
            if (!oMutTreeNode.isRoot()) {
                oNode = (KpiSelectedTreeNode) oMutTreeNode.getUserObject();
                kpi.xml.BIXMLRecord oRec = new kpi.xml.BIXMLRecord("SCO");
                oRec.set("ID", oNode.getID());
                oRec.set("SELECTED", oNode.isSelected() && oNode.isEnabled());
                vctSco.add(oRec);
            }
            oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();
        }

        return vctSco;
    }
}
