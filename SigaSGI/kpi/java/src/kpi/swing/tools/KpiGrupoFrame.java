package kpi.swing.tools;

import java.awt.Color;
import java.awt.event.MouseEvent;
import javax.swing.tree.*;
import java.util.*;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiSelectedTreeCellRender;
import kpi.swing.KpiSelectedTreeNode;
import kpi.xml.BIXMLException;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiGrupoFrame extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions, javax.swing.event.TreeSelectionListener, java.awt.event.ActionListener {

    private final int USUARIO = 1;
    private final int SEGURANCA = 2;
    private final int SCORECARD = 3;
    //
    private String recordType = "GRUPO";
    private String formType = recordType;//Tipo do Formulario
    private String parentType = "DIRUSUARIOS";//Tipo formulario pai, caso haja.
    private kpi.swing.kpiCreateTree buildTree = new kpi.swing.kpiCreateTree();
    private kpi.xml.BIXMLVector arvoreRetorno, vctSeguranca;
    private boolean isEditMode = false;
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiGrupoFrame(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
        prepareSearchField();
    }

    @Override
    public void enableFields() {
        isEditMode = true;

        event.setEnableFields(pnlGrupo, true);
        event.setEnableFields(pnlUsuarios, true);
        event.setEnableFields(pnlTopSeguranca, true);
        event.setEnableFields(pnlScorecard, true);
    }

    @Override
    public void disableFields() {
        isEditMode = false;

        event.setEnableFields(pnlGrupo, false);
        event.setEnableFields(pnlUsuarios, false);
        event.setEnableFields(pnlTopSeguranca, false);
        event.setEnableFields(pnlScorecard, false);
    }

    @Override
    public void refreshFields() {
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        arvoreRetorno = record.getBIXMLVector("ARVORES_SEG");
        vctSeguranca = record.getBIXMLVector("REGRAS");

        if (getStatus() == kpi.swing.KpiDefaultFrameBehavior.INSERTING) {
            tapGrupo.setEnabledAt(USUARIO, false);
            tapGrupo.setEnabledAt(SEGURANCA, false);
            tapGrupo.setEnabledAt(SCORECARD, false);
        } else {
            kpiTreeSeguranca.removeAll();
            kpiTreeSeguranca.setModel(new javax.swing.tree.DefaultTreeModel(buildTree.createTree(arvoreRetorno.get(0))));
            kpiTreeSeguranca.setCellRenderer(new kpi.swing.KpiTreeCellRenderer(kpi.core.KpiStaticReferences.getKpiImageResources()));
            kpiTreeSeguranca.addTreeSelectionListener(this);
        }

        BIXMLVector usuGrupo = record.getBIXMLVector("GRPUSUARIOS");
        BIXMLVector vctUsuarios = record.getBIXMLVector("USUARIOS");
        pnlUser.setDataSource(vctUsuarios, usuGrupo, "GRPUSUARIOS", "GRPUSUARIO");
        kpiTreeSco.setModel(new javax.swing.tree.DefaultTreeModel(createTree(record)));
        kpiTreeSco.setCellRenderer(new KpiSelectedTreeCellRender(kpi.core.KpiStaticReferences.getKpiImageResources()));
        kpiTreeSco.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("ATIVO", true);
        recordAux.set(pnlUser.getXMLData());
        recordAux.set(getXMLScoTree());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGrupoPessoaFrame_00015"));
            retorno = false;
        } else if (txtDescricao.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGrupoPessoaFrame_00016"));
            retorno = false;
        }

        return retorno;
    }

    protected BIXMLVector getXMLScoTree() {
        DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) kpiTreeSco.getModel().getRoot();
        BIXMLVector vctSco = new BIXMLVector("SCOS", "SCO");
        KpiSelectedTreeNode oNode;

        while (oMutTreeNode != null) {
            if (!oMutTreeNode.isRoot()) {
                oNode = (KpiSelectedTreeNode) oMutTreeNode.getUserObject();
                kpi.xml.BIXMLRecord oRec = new kpi.xml.BIXMLRecord("SCO");
                oRec.set("ID", oNode.getID());
                oRec.set("SELECTED", oNode.isSelected());
                vctSco.add(oRec);
            }
            oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();
        }

        return vctSco;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda2 = new javax.swing.JButton();
        tapGrupo = new javax.swing.JTabbedPane();
        pnlData = new javax.swing.JPanel();
        pnlDataCenter = new javax.swing.JPanel();
        pnlGrupo = new javax.swing.JPanel();
        lblDescricao = new javax.swing.JLabel();
        scrDescricao = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        fldNome = new pv.jfcx.JPVEdit();
        lblNome = new javax.swing.JLabel();
        pnlUsuarios = new javax.swing.JPanel();
        pnlUser = new kpi.beans.JBISelectionPanel();
        pnlTopSeguranca = new javax.swing.JPanel();
        splSeguranca = new javax.swing.JSplitPane();
        scrSeguranca = new javax.swing.JScrollPane();
        kpiTreeSeguranca = new javax.swing.JTree();
        pnlSeguranca = new javax.swing.JPanel();
        pnlScorecard = new javax.swing.JPanel();
        scrollTreeSco = new javax.swing.JScrollPane();
        kpiTreeSco = new javax.swing.JTree();
        pnlTreeScoFind = new javax.swing.JPanel();
        pnlTreeScoLabel = new javax.swing.JPanel();
        lblTreeSco = new javax.swing.JLabel();
        jLblProcurar = new javax.swing.JLabel();
        pnlEspace = new javax.swing.JPanel();
        jTxtProcurar = new javax.swing.JTextField();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiGrupoPessoaFrame_00006")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grupo.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(457, 342));

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new java.awt.Dimension(310, 19));
        tbInsertion.setPreferredSize(new java.awt.Dimension(300, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle1.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda2.setText(bundle.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjuda2ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda2);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlData.setLayout(new java.awt.BorderLayout());

        pnlDataCenter.setLayout(new java.awt.BorderLayout());

        pnlGrupo.setMinimumSize(new java.awt.Dimension(100, 24));
        pnlGrupo.setLayout(null);

        lblDescricao.setForeground(new java.awt.Color(51, 51, 255));
        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("KpiGrupoPessoaFrame_00005")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setPreferredSize(new java.awt.Dimension(80, 16));
        pnlGrupo.add(lblDescricao);
        lblDescricao.setBounds(20, 50, 80, 20);

        txtDescricao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtDescricao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        scrDescricao.setViewportView(txtDescricao);

        pnlGrupo.add(scrDescricao);
        scrDescricao.setBounds(20, 70, 400, 60);

        fldNome.setMaxLength(30);
        pnlGrupo.add(fldNome);
        fldNome.setBounds(20, 30, 400, 22);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiGrupoPessoaFrame_00004")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(80, 16));
        pnlGrupo.add(lblNome);
        lblNome.setBounds(20, 10, 80, 20);

        pnlDataCenter.add(pnlGrupo, java.awt.BorderLayout.CENTER);

        pnlData.add(pnlDataCenter, java.awt.BorderLayout.CENTER);

        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        tapGrupo.addTab(bundle2.getString("KpiGrupoPessoaFrame_00002"), pnlData); // NOI18N

        pnlUsuarios.setLayout(new java.awt.BorderLayout());

        pnlUser.setEnabled(false);
        pnlUser.setPreferredSize(new java.awt.Dimension(100, 100));
        pnlUsuarios.add(pnlUser, java.awt.BorderLayout.CENTER);

        tapGrupo.addTab(bundle2.getString("KpiGrupoPessoaFrame_00012"), pnlUsuarios); // NOI18N

        pnlTopSeguranca.setLayout(new java.awt.GridLayout(1, 0));

        splSeguranca.setDividerLocation(200);

        scrSeguranca.setPreferredSize(new java.awt.Dimension(100, 64));

        kpiTreeSeguranca.setRowHeight(20);
        scrSeguranca.setViewportView(kpiTreeSeguranca);

        splSeguranca.setLeftComponent(scrSeguranca);

        pnlSeguranca.setLayout(new javax.swing.BoxLayout(pnlSeguranca, javax.swing.BoxLayout.Y_AXIS));
        splSeguranca.setRightComponent(pnlSeguranca);

        pnlTopSeguranca.add(splSeguranca);

        tapGrupo.addTab(bundle2.getString("KpiGrupoPessoaFrame_00008"), pnlTopSeguranca); // NOI18N

        pnlScorecard.setLayout(new java.awt.BorderLayout());

        scrollTreeSco.setPreferredSize(new java.awt.Dimension(100, 64));

        kpiTreeSco.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        kpiTreeSco.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        kpiTreeSco.setRowHeight(18);
        kpiTreeSco.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                kpiTreeScoMouseClicked(evt);
            }
        });
        scrollTreeSco.setViewportView(kpiTreeSco);

        pnlScorecard.add(scrollTreeSco, java.awt.BorderLayout.CENTER);

        pnlTreeScoFind.setPreferredSize(new java.awt.Dimension(254, 25));
        pnlTreeScoFind.setLayout(new javax.swing.BoxLayout(pnlTreeScoFind, javax.swing.BoxLayout.LINE_AXIS));

        pnlTreeScoLabel.setLayout(new java.awt.BorderLayout());

        lblTreeSco.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTreeSco.setText(bundle2.getString("KpiUsuarioFrame_00040")); // NOI18N
        pnlTreeScoLabel.add(lblTreeSco, java.awt.BorderLayout.CENTER);

        pnlTreeScoFind.add(pnlTreeScoLabel);

        jLblProcurar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLblProcurar.setText(bundle.getString("JBISeekListPanel_00001")); // NOI18N
        jLblProcurar.setIconTextGap(0);
        jLblProcurar.setMaximumSize(new java.awt.Dimension(70, 16));
        jLblProcurar.setMinimumSize(new java.awt.Dimension(50, 16));
        jLblProcurar.setPreferredSize(new java.awt.Dimension(70, 20));
        pnlTreeScoFind.add(jLblProcurar);

        pnlEspace.setPreferredSize(new java.awt.Dimension(3, 10));
        pnlTreeScoFind.add(pnlEspace);

        jTxtProcurar.setMaximumSize(new java.awt.Dimension(10, 21));
        jTxtProcurar.setPreferredSize(new java.awt.Dimension(200, 22));
        jTxtProcurar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtProcurarFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtProcurarFocusLost(evt);
            }
        });
        jTxtProcurar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtProcurarKeyReleased(evt);
            }
        });
        pnlTreeScoFind.add(jTxtProcurar);

        pnlScorecard.add(pnlTreeScoFind, java.awt.BorderLayout.PAGE_START);

        tapGrupo.addTab(KpiStaticReferences.getKpiCustomLabels().getSco(), pnlScorecard);

        getContentPane().add(tapGrupo, java.awt.BorderLayout.CENTER);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjuda2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjuda2ActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjuda2ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
            prepareSearchField();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
            event.loadRecord();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void kpiTreeScoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_kpiTreeScoMouseClicked
        if (isEditMode) {
            int x = evt.getX();
            int y = evt.getY();
            int row = kpiTreeSco.getRowForLocation(x, y);
            TreePath path = kpiTreeSco.getPathForRow(row);
            if (path != null) {
                DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) path.getLastPathComponent();
                KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
                boolean bSelected = !(node.isSelected());
                node.setSelected(bSelected);

                if (evt.getButton() == MouseEvent.BUTTON1) {
                    selectedFromRoot(nodeAux, bSelected);
                }

                ((DefaultTreeModel) kpiTreeSco.getModel()).nodeChanged(node);

                kpiTreeSco.revalidate();
                kpiTreeSco.repaint();
            }
        }
        prepareSearchField();
}//GEN-LAST:event_kpiTreeScoMouseClicked

    private void selectedFromRoot(DefaultMutableTreeNode oNode, boolean bSelected) {
        Enumeration e = oNode.children();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) e.nextElement();
            KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
            node.setSelected(bSelected);
            selectedFromRoot(nodeAux, bSelected);
        }
    }

    private void jTxtProcurarFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtProcurarFocusGained
        initSearchField();
}//GEN-LAST:event_jTxtProcurarFocusGained

    /**
     * Inicia campos de pesquisa com valor padr�o.
     * @return void
     */
    private void initSearchField() {
        jTxtProcurar.setText("");
        jTxtProcurar.setForeground(Color.BLACK);
    }

    /**
     * Define as caracter�sticas do campo de pesquisa de acordo com o resultado da pesquisa.
     * @param isFound
     * @return void
     */
    private void setSearchField(boolean isFound) {

        if (isFound) {
            jTxtProcurar.setBackground(Color.WHITE);
        } else {
            jTxtProcurar.setBackground(new Color(253, 117, 127)/*Red*/);
        }

    }

    /**
     * Prepar  a os campos de pesquisa.
     * @param isVisibled
     * @return void
     */
    public void prepareSearchField() {
        jTxtProcurar.setText(ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("JBIListPanel_00004" /*Digite sua pesquisa*/));
        jTxtProcurar.setForeground(Color.LIGHT_GRAY);
        jTxtProcurar.setBackground(Color.WHITE);
    }

    private void jTxtProcurarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtProcurarKeyReleased
        boolean found = false;

        if (jTxtProcurar.getText().length() == 0) {
            found = true;
        } else {
            DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) kpiTreeSco.getModel().getRoot();
            while (oMutTreeNode != null) {
                if (oMutTreeNode.getUserObject().toString().toLowerCase().contains(jTxtProcurar.getText().toLowerCase())) {
                    DefaultTreeModel treeModel = (DefaultTreeModel) kpiTreeSco.getModel();
                    TreePath tp = new TreePath(treeModel.getPathToRoot(oMutTreeNode));
                    kpiTreeSco.setSelectionPath(tp);
                    found = true;
                    break;
                }
                oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();
            }
        }

        setSearchField(found);
}//GEN-LAST:event_jTxtProcurarKeyReleased

    private void jTxtProcurarFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtProcurarFocusLost
        if (jTxtProcurar.getText().length() == 0) {
            prepareSearchField();
        }
    }//GEN-LAST:event_jTxtProcurarFocusLost
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjuda2;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JLabel jLblProcurar;
    private javax.swing.JTextField jTxtProcurar;
    private javax.swing.JTree kpiTreeSco;
    private javax.swing.JTree kpiTreeSeguranca;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblTreeSco;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDataCenter;
    private javax.swing.JPanel pnlEspace;
    private javax.swing.JPanel pnlGrupo;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlScorecard;
    private javax.swing.JPanel pnlSeguranca;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopSeguranca;
    private javax.swing.JPanel pnlTreeScoFind;
    private javax.swing.JPanel pnlTreeScoLabel;
    private kpi.beans.JBISelectionPanel pnlUser;
    private javax.swing.JPanel pnlUsuarios;
    private javax.swing.JScrollPane scrDescricao;
    private javax.swing.JScrollPane scrSeguranca;
    private javax.swing.JScrollPane scrollTreeSco;
    private javax.swing.JSplitPane splSeguranca;
    private javax.swing.JTabbedPane tapGrupo;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables

    public DefaultMutableTreeNode createTree(BIXMLRecord treeXML) {
        DefaultMutableTreeNode root = insertGroup(treeXML.getBIXMLVector("DPTOS"), null);
        return root;
    }

    public DefaultMutableTreeNode insertRecord(BIXMLRecord record, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiSelectedTreeNode oScoNode = new KpiSelectedTreeNode(
                record.getAttributes().getString("ID"),
                record.getAttributes().getString("NOME"),
                record.getAttributes().getBoolean("ENABLED"),
                record.getAttributes().getBoolean("SELECTED"),
                record.getAttributes().getInt("IMAGE"));        

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oScoNode);

        for (Iterator e = record.getKeyNames(); e.hasNext();) {
            String strChave = (String) e.next();

            if (record.getBIXMLVector(strChave).getAttributes().contains("TIPO")) {
                child = insertGroup(record.getBIXMLVector(strChave), child);
            } else {
                child = insertRecord(record.getBIXMLVector(strChave), child);
            }
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    public DefaultMutableTreeNode insertRecord(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiSelectedTreeNode oScoNode = new KpiSelectedTreeNode(
                vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"),
                vector.getAttributes().getBoolean("ENABLED"),
                vector.getAttributes().getBoolean("SELECTED"),
                vector.getAttributes().getInt("IMAGE"));

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oScoNode);

        for (int i = 0; i < vector.size(); i++) {
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    public DefaultMutableTreeNode insertGroup(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        DefaultMutableTreeNode child = new DefaultMutableTreeNode(
                new KpiSelectedTreeNode(vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"),
                vector.getAttributes().getBoolean("ENABLED"),
                vector.getAttributes().getBoolean("SELECTED"),
                vector.getAttributes().getInt("IMAGE")));

        for (int i = 0; i < vector.size(); i++) {
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }

    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        return "1";
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapGrupo;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void valueChanged(javax.swing.event.TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) kpiTreeSeguranca.getLastSelectedPathComponent();
        javax.swing.JCheckBox chkOption;

        if (node != null) {
            pnlSeguranca.removeAll();

            Object userObject = node.getUserObject();
            if (userObject instanceof kpi.swing.KpiTreeNode) {
                //Item clicado na arvore
                kpi.xml.BIXMLRecord nodeClicked = (((kpi.swing.KpiTreeNode) userObject).getRecord());
                //Item correspondente no vetor de segura�a.
                if (nodeClicked != null) {
                    kpi.xml.BIXMLRecord nodeTemp = vctSeguranca.get(nodeClicked.getAttributes().getInt("ID") - 1);
                    //vetor com os itens de seguran�a.
                    kpi.xml.BIXMLVector vctItens = nodeTemp.getBIXMLVector("ITEMREGRA");

                    javax.swing.JLabel titulo = new javax.swing.JLabel(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGrupoPessoaFrame_00011"));
                    titulo.setFont(new java.awt.Font("Tahoma", 1, 11));
                    pnlSeguranca.add(titulo);
                    for (int iTem = 0; iTem < vctItens.size(); iTem++) {
                        try {
                            String nome = getRegraByID(vctItens.get(iTem).getAttributes().getInt("REGRAID"));
                            boolean valor = getRegraValor(vctItens.get(iTem).getAttributes().getInt("VALOR"));

                            chkOption = new javax.swing.JCheckBox(nome, valor);
                            chkOption.setOpaque(true);
                            chkOption.addActionListener(this);
                            chkOption.putClientProperty("REGRA_ID", new Integer(iTem));
                            pnlSeguranca.add(chkOption);
                        } catch (Exception ex) {
                            break;
                        }
                    }
                }
            }

            pnlSeguranca.validate();
            pnlSeguranca.repaint();
        }
    }

    @Override
    public void actionPerformed(java.awt.event.ActionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) kpiTreeSeguranca.getLastSelectedPathComponent();
        javax.swing.JCheckBox source = (javax.swing.JCheckBox) e.getSource();
        Object userObject = node.getUserObject();

        if (userObject instanceof kpi.swing.KpiTreeNode) {
            //Item clicado na arvore
            kpi.xml.BIXMLRecord nodeClicked = (((kpi.swing.KpiTreeNode) userObject).getRecord());
            //Item correspondente no vetor de segura�a.
            kpi.xml.BIXMLRecord nodeTemp = vctSeguranca.get(nodeClicked.getAttributes().getInt("ID") - 1);
            //vetor com itens de seguran�a.
            kpi.xml.BIXMLVector vctItens = nodeTemp.getBIXMLVector("ITEMREGRA");
            //Retornando o atributo, para setar o valor do item.
            kpi.xml.BIXMLAttributes attributes = vctItens.get((Integer) source.getClientProperty("REGRA_ID")).getAttributes();
            attributes.set("VALOR", source.isSelected() ? "1" : "0");
        }
    }

    private String getRegraByID(int id) {
        String regra = new String();
        switch (id) {
            case 1:
                regra = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGrupoPessoaFrame_00013");
                break;
            case 2:
                regra = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGrupoPessoaFrame_00014");
                break;
            case 3:
                regra = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGrupoPessoaFrame_00017");
                break;
            case 4:
                regra = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGrupoPessoaFrame_00018");
                break;
            case 5:
                regra = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGrupoPessoaFrame_00019");
                break;
        }
        return regra;
    }

    private boolean getRegraValor(int valor) {
        return valor == 1 ? true : false;
    }
}
