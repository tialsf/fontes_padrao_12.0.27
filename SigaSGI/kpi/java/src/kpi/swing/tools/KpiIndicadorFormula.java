/**
 * KpiIndicadorFormula.java
 * Formul�rio para exibir a f�rmula de um indicador
 *
 * Created on 07/01/2009, 14:13:25
 * @author paulo.vieira
 */
package kpi.swing.tools;

import java.text.NumberFormat;
import javax.swing.JInternalFrame;
import javax.swing.JTabbedPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import kpi.core.KpiImageResources;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.swing.KpiInternalFrame;
import kpi.swing.KpiTree;
import kpi.swing.KpiTreeCellRenderer;
import kpi.swing.KpiTreeNode;
import kpi.swing.kpiCreateTree;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;

public class KpiIndicadorFormula extends KpiInternalFrame implements KpiDefaultFrameFunctions {

    private String formType = "INDICADOR_FORMULA";
    private KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private int status;
    private String id;
    private String parentID;
    private BIXMLRecord record;
    private KpiTree kpiTree = new KpiTree("IND_FORMULA");
    private kpiCreateTree kpiCreateTree = new kpiCreateTree();
    private KpiImageResources imagesResources = new KpiImageResources();
    private NumberFormat formatValues = NumberFormat.getInstance();

    public KpiIndicadorFormula() {
        initComponents();
        putClientProperty("MAXI", true);
    }

    public KpiIndicadorFormula(int operation, String idAux, String contextId) {
        this();
        event.defaultConstructor(operation, idAux, contextId);
        kpiTree.addTreeSelectionListener(new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent e) {
                nodeClicked();
            }
        });
        kpiTree.setCellRenderer(new KpiTreeCellRenderer(KpiStaticReferences.getKpiImageResources()));
        jSplitPane1.setLeftComponent(kpiTree);
    }

    protected void cleanFields() {
        this.pvEdtNome.setValue("");
        this.pvEdtDepartamento.setValue("");
        this.edtFormula.setText("");
        this.pvEdtValorReal.setValue("");
        this.pvEdtValorMeta.setValue("");
        this.pvEdtValorRealAcum.setValue("");
        this.pvEdtValorMetaAcum.setValue("");
        this.lblReal.setIcon(imagesResources.getImage(KpiImageResources.KPI_IMG_VAZIO));
        this.lblAcumulado.setIcon(imagesResources.getImage(KpiImageResources.KPI_IMG_VAZIO));
        this.pvEdtValorPrevia.setValue("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        pnlValores = new javax.swing.JPanel();
        pvEdtValorPrevia = new pv.jfcx.JPVEdit();
        pvEdtValorReal = new pv.jfcx.JPVEdit();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblReal = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        pvEdtValorMeta = new pv.jfcx.JPVEdit();
        pvEdtValorRealAcum = new pv.jfcx.JPVEdit();
        lblAcumulado = new javax.swing.JLabel();
        pvEdtValorMetaAcum = new pv.jfcx.JPVEdit();
        pnlFormula = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        scrFormula = new javax.swing.JScrollPane();
        edtFormula = new javax.swing.JEditorPane();
        pnlDetalhes = new javax.swing.JPanel();
        pvEdtDepartamento = new pv.jfcx.JPVEdit();
        pvEdtNome = new pv.jfcx.JPVEdit();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblScoreCard1 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiIndicadorFormula_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_indicadorformula.gif"))); // NOI18N
        setPreferredSize(new java.awt.Dimension(775, 377));
        setRequestFocusEnabled(false);
        try {
            setSelected(true);
        } catch (java.beans.PropertyVetoException e1) {
            e1.printStackTrace();
        }
        setVerifyInputWhenFocusTarget(false);

        jSplitPane1.setDividerLocation(300);
        jSplitPane1.setOneTouchExpandable(true);

        jPanel1.setLayout(new java.awt.BorderLayout());

        pnlValores.setMaximumSize(new java.awt.Dimension(140, 140));
        pnlValores.setMinimumSize(new java.awt.Dimension(140, 140));
        pnlValores.setPreferredSize(new java.awt.Dimension(130, 130));
        pnlValores.setLayout(null);
        pnlValores.add(pvEdtValorPrevia);
        pvEdtValorPrevia.setBounds(10, 100, 180, 22);
        pnlValores.add(pvEdtValorReal);
        pvEdtValorReal.setBounds(10, 20, 180, 22);

        jLabel7.setText(bundle.getString("KpiIndicadorFormula_00007")); // NOI18N
        pnlValores.add(jLabel7);
        jLabel7.setBounds(230, 0, 140, 20);

        jLabel8.setText(bundle.getString("KpiIndicadorFormula_00008")); // NOI18N
        pnlValores.add(jLabel8);
        jLabel8.setBounds(230, 40, 140, 20);

        lblReal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_voltar.gif"))); // NOI18N
        pnlValores.add(lblReal);
        lblReal.setBounds(200, 20, 20, 20);

        jLabel5.setText(bundle.getString("KpiIndicadorFormula_00006")); // NOI18N
        pnlValores.add(jLabel5);
        jLabel5.setBounds(10, 40, 110, 20);

        jLabel9.setText(bundle.getString("KpiIndicadorFormula_00011")); // NOI18N
        pnlValores.add(jLabel9);
        jLabel9.setBounds(10, 80, 140, 20);

        jLabel3.setText(bundle.getString("KpiIndicadorFormula_00005")); // NOI18N
        pnlValores.add(jLabel3);
        jLabel3.setBounds(10, 0, 110, 20);
        pnlValores.add(pvEdtValorMeta);
        pvEdtValorMeta.setBounds(10, 60, 180, 22);
        pnlValores.add(pvEdtValorRealAcum);
        pvEdtValorRealAcum.setBounds(230, 20, 180, 22);

        lblAcumulado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_voltar.gif"))); // NOI18N
        pnlValores.add(lblAcumulado);
        lblAcumulado.setBounds(420, 20, 20, 20);
        pnlValores.add(pvEdtValorMetaAcum);
        pvEdtValorMetaAcum.setBounds(230, 60, 180, 22);

        jPanel1.add(pnlValores, java.awt.BorderLayout.PAGE_END);

        pnlFormula.setLayout(new java.awt.BorderLayout());
        pnlFormula.add(jPanel2, java.awt.BorderLayout.LINE_START);
        pnlFormula.add(jPanel3, java.awt.BorderLayout.LINE_END);

        edtFormula.setContentType("text/html");
        edtFormula.setEditable(false);
        scrFormula.setViewportView(edtFormula);

        pnlFormula.add(scrFormula, java.awt.BorderLayout.CENTER);

        jPanel1.add(pnlFormula, java.awt.BorderLayout.CENTER);

        pnlDetalhes.setMaximumSize(new java.awt.Dimension(110, 110));
        pnlDetalhes.setMinimumSize(new java.awt.Dimension(110, 110));
        pnlDetalhes.setPreferredSize(new java.awt.Dimension(110, 110));
        pnlDetalhes.setLayout(null);
        pnlDetalhes.add(pvEdtDepartamento);
        pvEdtDepartamento.setBounds(10, 70, 400, 22);
        pnlDetalhes.add(pvEdtNome);
        pvEdtNome.setBounds(10, 30, 400, 22);

        jLabel1.setText(bundle.getString("KpiIndicadorFormula_00002")); // NOI18N
        pnlDetalhes.add(jLabel1);
        jLabel1.setBounds(10, 10, 110, 20);

        jLabel6.setText(bundle.getString("KpiIndicadorFormula_00004")); // NOI18N
        pnlDetalhes.add(jLabel6);
        jLabel6.setBounds(10, 90, 380, 20);

        lblScoreCard1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblScoreCard1.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        lblScoreCard1.setEnabled(false);
        lblScoreCard1.setMaximumSize(new java.awt.Dimension(100, 16));
        lblScoreCard1.setMinimumSize(new java.awt.Dimension(100, 16));
        lblScoreCard1.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDetalhes.add(lblScoreCard1);
        lblScoreCard1.setBounds(10, 50, 260, 20);

        jPanel1.add(pnlDetalhes, java.awt.BorderLayout.PAGE_START);

        jSplitPane1.setRightComponent(jPanel1);

        getContentPane().add(jSplitPane1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JEditorPane edtFormula;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JLabel lblAcumulado;
    private javax.swing.JLabel lblReal;
    private javax.swing.JLabel lblScoreCard1;
    private javax.swing.JPanel pnlDetalhes;
    private javax.swing.JPanel pnlFormula;
    private javax.swing.JPanel pnlValores;
    private pv.jfcx.JPVEdit pvEdtDepartamento;
    private pv.jfcx.JPVEdit pvEdtNome;
    private pv.jfcx.JPVEdit pvEdtValorMeta;
    private pv.jfcx.JPVEdit pvEdtValorMetaAcum;
    private pv.jfcx.JPVEdit pvEdtValorPrevia;
    private pv.jfcx.JPVEdit pvEdtValorReal;
    private pv.jfcx.JPVEdit pvEdtValorRealAcum;
    private javax.swing.JScrollPane scrFormula;
    // End of variables declaration//GEN-END:variables

    @Override
    public void afterExecute() {
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
        this.pvEdtNome.setEditable(false);
        this.pvEdtDepartamento.setEditable(false);
        this.edtFormula.setEditable(false);
        this.pvEdtValorReal.setEditable(false);
        this.pvEdtValorMeta.setEditable(false);
        this.pvEdtValorRealAcum.setEditable(false);
        this.pvEdtValorMetaAcum.setEditable(false);
        this.pvEdtValorPrevia.setEditable(false);
    }

    @Override
    public void refreshFields() {
        kpiTree.setModel(new DefaultTreeModel(kpiCreateTree.createTree(record)));
        kpiTree.setSelectionRow(0);

        this.nodeClicked();
    }

    private void nodeClicked() {
        DefaultMutableTreeNode dmt = (DefaultMutableTreeNode) kpiTree.getLastSelectedPathComponent();
        KpiTreeNode node = (KpiTreeNode) dmt.getUserObject();
        BIXMLRecord nodeRecord = node.getRecord();

        BIXMLAttributes nodeAttributes = nodeRecord.getAttributes();

        this.cleanFields();
        this.pvEdtNome.setValue(nodeAttributes.getString("NOME"));
        this.pvEdtDepartamento.setValue(nodeAttributes.getString("DEPARTAMENTO"));
        this.edtFormula.setText(nodeAttributes.getString("FORMULA"));

        if ("METAFORMULAS".equals(nodeAttributes.getString("TIPO"))) {
            this.pvEdtValorReal.setValue(nodeAttributes.getString("VALOR_REAL"));
            this.pvEdtValorMeta.setValue(nodeAttributes.getString("VALOR_META"));
            this.pvEdtValorRealAcum.setValue(nodeAttributes.getString("VALOR_REAL_ACUM"));
            this.pvEdtValorMetaAcum.setValue(nodeAttributes.getString("VALOR_META_ACUM"));
            this.pvEdtValorPrevia.setValue(nodeAttributes.getString("VALOR_PREVIA"));
        } else {
            this.pvEdtValorReal.setValue(formatValues.format(nodeAttributes.getFloat("VALOR_REAL")));
            this.pvEdtValorMeta.setValue(formatValues.format(nodeAttributes.getFloat("VALOR_META")));
            this.pvEdtValorRealAcum.setValue(formatValues.format(nodeAttributes.getFloat("VALOR_REAL_ACUM")));
            this.pvEdtValorMetaAcum.setValue(formatValues.format(nodeAttributes.getFloat("VALOR_META_ACUM")));
            this.pvEdtValorPrevia.setValue(formatValues.format(nodeAttributes.getFloat("VALOR_PREVIA")));

        }

        this.lblReal.setIcon(imagesResources.getImage(nodeAttributes.getInt("VALOR_REAL_STATUS")));
        this.lblAcumulado.setIcon(imagesResources.getImage(nodeAttributes.getInt("VALOR_ACUM_STATUS")));
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true; /* not used */ }

    @Override
    public JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public JTabbedPane getTabbedPane() {
        return null;
    }

    @Override
    public BIXMLRecord getFieldsContents() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void loadRecord() {
        kpi.core.KpiDateBase dateBase = kpi.core.KpiStaticReferences.getKpiDateBase();
        kpi.util.KpiDateUtil date_Util = new kpi.util.KpiDateUtil();

        StringBuilder strComando = new StringBuilder("");
        strComando.append(date_Util.getCalendarString(dateBase.getDataAnaliseDe()));
        strComando.append("|");
        strComando.append(date_Util.getCalendarString(dateBase.getDataAcumuladoDe()));
        strComando.append("|");
        strComando.append(date_Util.getCalendarString(dateBase.getDataAcumuladoAte()));

        this.setRecord(event.loadRecord(this.getID(), this.getType(), strComando.toString()));

        int intDecimals = this.getRecord().getAttributes().getInt("PERCENTDEC");
        formatValues.setMinimumFractionDigits(intDecimals);
        formatValues.setMaximumFractionDigits(intDecimals);

        this.refreshFields();
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void setStatus(int value) {
        this.status = value;
    }

    @Override
    public int getStatus() {
        return this.status;
    }

    @Override
    public void setID(String value) {
        this.id = value;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public String getParentID() {
        return this.parentID;
    }

    @Override
    public void setType(String value) {
        this.formType = value;
    }

    @Override
    public String getType() {
        return this.getFormType();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return this.getFormType();
    }

    @Override
    public void setRecord(BIXMLRecord recordAux) {
        this.record = recordAux;
    }

    @Override
    public BIXMLRecord getRecord() {
        return this.record;
    }
}
