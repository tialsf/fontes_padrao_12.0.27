package kpi.swing.tools;

import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiLembrete extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = new String("PLANOACAO");
    private String formType = new String("LSTLEMBRETE");
    private String parentType = recordType;
    private kpi.xml.BIXMLRecord recPlanos;
    //Constante com a largura das colunas
    private static final int[] TAMANHO_COLUNAS = {24, 326, 60};
    //vari�vel que determina o n�mero de dias para os planos de a��es vencerem
    private int prazoDia = 7;

    @Override
    public void enableFields() {
        // Habilita os campos do formul�rio.
        listaIndicadores.setEnabled(true);
        listaProjetos.setEnabled(true);
    }

    @Override
    public void disableFields() {
        // Desabilita os campos do formul�rio.
        listaIndicadores.setEnabled(false);
        listaProjetos.setEnabled(false);
    }

    @Override
    public void refreshFields() {
        //kpi.xml.BIXMLRecord recPlanos = event.listRecords("PLANOACAO","LEMBRETE");


        prazoDia = recPlanos.getInt("PRAZO_PARA_VENC");


        BIXMLVector vctPlanos = recPlanos.getBIXMLVector("PLANOSACAO");
        BIXMLVector projetos = new BIXMLVector(vctPlanos.getMainTag(), vctPlanos.getRecordTag(), vctPlanos.getAttributes());
        BIXMLVector projetosFuturos = new BIXMLVector(vctPlanos.getMainTag(), vctPlanos.getRecordTag(), vctPlanos.getAttributes());
        BIXMLVector indicadores = new BIXMLVector(vctPlanos.getMainTag(), vctPlanos.getRecordTag(), vctPlanos.getAttributes());
        BIXMLVector indicadoresFuturos = new BIXMLVector(vctPlanos.getMainTag(), vctPlanos.getRecordTag(), vctPlanos.getAttributes());
        BIXMLVector vctMsg = recPlanos.getBIXMLVector("MENSAGENS");

        for (int i = 0; i < vctPlanos.size(); i++) {
            if (vctPlanos.get(i).getString("TIPOACAO").equals("2")) {
                if (vctPlanos.get(i).getTagName().equals("PLANOACAO")) {
                    projetos.add(vctPlanos.get(i));
                } else {
                    projetosFuturos.add(vctPlanos.get(i));
                }
            } else {
                if (vctPlanos.get(i).getTagName().equals("PLANOACAO")) {
                    indicadores.add(vctPlanos.get(i));
                } else {
                    indicadoresFuturos.add(vctPlanos.get(i));
                }
            }
        }
        listaProjetos.setDataSource(projetos);
        listaProjAVencer.setDataSource(projetosFuturos);
        listaIndicadores.setDataSource(indicadores);
        listaIndAVencer.setDataSource(indicadoresFuturos);
        listaEmail.setDataSource(vctMsg);

        doPlanRender();

        quantidadePlanos();
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        pnlLembrete = new javax.swing.JPanel();
        lblEmail = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblIndicador = new javax.swing.JLabel();
        lblAcoesIndVenc = new javax.swing.JLabel();
        lblProjeto = new javax.swing.JLabel();
        lblAcoesProjVenc = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lblIndAVencer = new javax.swing.JLabel();
        lblAcoesIndAVencer = new javax.swing.JLabel();
        lblAcoesProjAVencer = new javax.swing.JLabel();
        lblProjAVencer = new javax.swing.JLabel();
        jTaskPaneGroup1 = new com.l2fprod.common.swing.JTaskPaneGroup();
        jTaskPaneGroup2 = new com.l2fprod.common.swing.JTaskPaneGroup();
        jTaskPaneGroup3 = new com.l2fprod.common.swing.JTaskPaneGroup();
        jTaskPane1 = new com.l2fprod.common.swing.JTaskPane();
        jPGroupAcao = new com.l2fprod.common.swing.JTaskPaneGroup();
        jTPlanoAcao = new com.l2fprod.common.swing.JTaskPaneGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        listaIndicadores = new kpi.beans.JBIXMLTable();
        listaIndAVencer = new kpi.beans.JBIXMLTable();
        jTProjeto = new com.l2fprod.common.swing.JTaskPaneGroup();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        listaProjetos = new kpi.beans.JBIXMLTable();
        listaProjAVencer = new kpi.beans.JBIXMLTable();
        jTPMsg = new com.l2fprod.common.swing.JTaskPaneGroup();
        listaEmail = new kpi.beans.JBIXMLTable();

        pnlLembrete.setBackground(new java.awt.Color(255, 255, 220));
        pnlLembrete.setPreferredSize(new java.awt.Dimension(550, 490));
        pnlLembrete.setLayout(null);

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblEmail.setText(bundle.getString("KpiLembrete_00007")); // NOI18N
        pnlLembrete.add(lblEmail);
        lblEmail.setBounds(25, 365, 490, 18);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("KpiLembrete_00003"))); // NOI18N
        jPanel1.setOpaque(false);
        jPanel1.setLayout(null);

        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblIndicador.setText(bundle1.getString("KpiLembrete_00002")); // NOI18N
        jPanel1.add(lblIndicador);
        lblIndicador.setBounds(21, 21, 260, 18);

        lblAcoesIndVenc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(lblAcoesIndVenc);
        lblAcoesIndVenc.setBounds(360, 20, 110, 18);

        lblProjeto.setText(bundle1.getString("KpiLembrete_00004")); // NOI18N
        jPanel1.add(lblProjeto);
        lblProjeto.setBounds(21, 96, 260, 18);

        lblAcoesProjVenc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(lblAcoesProjVenc);
        lblAcoesProjVenc.setBounds(360, 95, 110, 18);

        pnlLembrete.add(jPanel1);
        jPanel1.setBounds(20, 5, 500, 175);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel2.setOpaque(false);
        jPanel2.setLayout(null);

        lblIndAVencer.setText(bundle1.getString("KpiLembrete_00002")); // NOI18N
        jPanel2.add(lblIndAVencer);
        lblIndAVencer.setBounds(21, 21, 260, 18);

        lblAcoesIndAVencer.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel2.add(lblAcoesIndAVencer);
        lblAcoesIndAVencer.setBounds(360, 20, 110, 18);

        lblAcoesProjAVencer.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel2.add(lblAcoesProjAVencer);
        lblAcoesProjAVencer.setBounds(360, 95, 110, 18);

        lblProjAVencer.setText(bundle1.getString("KpiLembrete_00004")); // NOI18N
        jPanel2.add(lblProjAVencer);
        lblProjAVencer.setBounds(21, 96, 260, 18);

        pnlLembrete.add(jPanel2);
        jPanel2.setBounds(20, 185, 500, 175);

        jTaskPaneGroup1.setBackground(new java.awt.Color(255, 255, 220));
        jTaskPaneGroup1.setTitle("A��es de indicadores");
        jTaskPaneGroup1.setMaximumSize(new java.awt.Dimension(800, 800));
        jTaskPaneGroup1.getContentPane().setLayout(null);

        jTaskPaneGroup2.setTitle("A��es de projetos");
        jTaskPaneGroup2.getContentPane().setLayout(null);

        jTaskPaneGroup3.setTitle("Mensagens (Caixa de entrada)");
        jTaskPaneGroup3.setMaximumSize(new java.awt.Dimension(800, 800));
        jTaskPaneGroup3.getContentPane().setLayout(null);

        setBackground(new java.awt.Color(255, 255, 255));
        setClosable(true);
        setForeground(java.awt.Color.white);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle(bundle1.getString("KpiLembrete_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_lembrete.gif"))); // NOI18N
        setPreferredSize(new java.awt.Dimension(550, 490));

        jTaskPane1.setAutoscrolls(true);

        jPGroupAcao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif"))); // NOI18N
        jPGroupAcao.setSpecial(true);
        jPGroupAcao.setTitle("A��es");
        jPGroupAcao.setFocusable(false);

        jTPlanoAcao.setAnimated(false);
        jTPlanoAcao.setExpanded(false);
        jTPlanoAcao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_indicador.gif"))); // NOI18N
        jTPlanoAcao.setTitle("Indicadores");
        jTPlanoAcao.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTPlanoAcaoPropertyChange(evt);
            }
        });

        jTabbedPane1.addTab("Vencidas", listaIndicadores);

        listaIndAVencer.getTable().setTableHeader(null);
        jTabbedPane1.addTab("A vencer", listaIndAVencer);

        jTPlanoAcao.getContentPane().add(jTabbedPane1);

        jPGroupAcao.getContentPane().add(jTPlanoAcao);

        jTProjeto.setAnimated(false);
        jTProjeto.setExpanded(false);
        jTProjeto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_projeto.gif"))); // NOI18N
        jTProjeto.setTitle("Projetos");
        jTProjeto.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTProjetoPropertyChange(evt);
            }
        });

        jTabbedPane2.addTab("Vencidas", listaProjetos);

        listaProjAVencer.getTable().setTableHeader(null);
        jTabbedPane2.addTab("A vencer", listaProjAVencer);

        jTProjeto.getContentPane().add(jTabbedPane2);

        jPGroupAcao.getContentPane().add(jTProjeto);

        jTaskPane1.add(jPGroupAcao);

        jTPMsg.setAnimated(false);
        jTPMsg.setExpanded(false);
        jTPMsg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_emailrecebido.gif"))); // NOI18N
        jTPMsg.setTitle("Mensagens (Caixa de entrada)");
        jTPMsg.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTPMsgPropertyChange(evt);
            }
        });

        listaProjAVencer.getTable().setTableHeader(null);
        jTPMsg.getContentPane().add(listaEmail);

        jTaskPane1.add(jTPMsg);

        getContentPane().add(jTaskPane1, java.awt.BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents

    private void jTPlanoAcaoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTPlanoAcaoPropertyChange
        if (evt.getPropertyName().equals("expanded") || evt.getPropertyName().equals("collapsed")) {
            if ((Boolean) evt.getNewValue()) {
                jTPMsg.setExpanded(false);
                jTProjeto.setExpanded(false);
            }
        }
    }//GEN-LAST:event_jTPlanoAcaoPropertyChange

    private void jTProjetoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTProjetoPropertyChange
        if (evt.getPropertyName().equals("expanded") || evt.getPropertyName().equals("collapsed")) {
            if ((Boolean) evt.getNewValue()) {
                jTPMsg.setExpanded(false);
                jTPlanoAcao.setExpanded(false);
            }
        }
    }//GEN-LAST:event_jTProjetoPropertyChange

    private void jTPMsgPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTPMsgPropertyChange
        if (evt.getPropertyName().equals("expanded") || evt.getPropertyName().equals("collapsed")) {
            if ((Boolean) evt.getNewValue()) {
                jTProjeto.setExpanded(false);
                jTPlanoAcao.setExpanded(false);
            }
        }
    }//GEN-LAST:event_jTPMsgPropertyChange
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.l2fprod.common.swing.JTaskPaneGroup jPGroupAcao;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private com.l2fprod.common.swing.JTaskPaneGroup jTPMsg;
    private com.l2fprod.common.swing.JTaskPaneGroup jTPlanoAcao;
    private com.l2fprod.common.swing.JTaskPaneGroup jTProjeto;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private com.l2fprod.common.swing.JTaskPane jTaskPane1;
    private com.l2fprod.common.swing.JTaskPaneGroup jTaskPaneGroup1;
    private com.l2fprod.common.swing.JTaskPaneGroup jTaskPaneGroup2;
    private com.l2fprod.common.swing.JTaskPaneGroup jTaskPaneGroup3;
    private javax.swing.JLabel lblAcoesIndAVencer;
    private javax.swing.JLabel lblAcoesIndVenc;
    private javax.swing.JLabel lblAcoesProjAVencer;
    private javax.swing.JLabel lblAcoesProjVenc;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblIndAVencer;
    private javax.swing.JLabel lblIndicador;
    private javax.swing.JLabel lblProjAVencer;
    private javax.swing.JLabel lblProjeto;
    private kpi.beans.JBIXMLTable listaEmail;
    private kpi.beans.JBIXMLTable listaIndAVencer;
    private kpi.beans.JBIXMLTable listaIndicadores;
    private kpi.beans.JBIXMLTable listaProjAVencer;
    private kpi.beans.JBIXMLTable listaProjetos;
    private javax.swing.JPanel pnlLembrete;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiLembrete(int operation, String idAux, String contextId) {
        initComponents();
        event.defaultConstructor(operation, idAux, contextId);



        listaProjetos.setEnabled(true);
        listaProjetos.getTable().setOpaque(true);
        listaIndicadores.setEnabled(true);
        listaIndicadores.getTable().setBackground(pnlLembrete.getBackground());

        //Adicionando eventos.
        listaProjetos.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                projetoMouseClicked(evt);
            }
        });

        listaIndicadores.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                indicadorMouseClicked(evt);
            }

            public void mouseEnter(java.awt.event.MouseEvent evt) {
                indicadorMouseEnter(evt);
            }
        });

        listaProjAVencer.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                projAVencerMouseClicked(evt);
            }
        });

        listaIndAVencer.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                indAVencerMouseClicked(evt);
            }

            public void mouseEnter(java.awt.event.MouseEvent evt) {
                indAVencerMouseEnter(evt);
            }
        });

        listaEmail.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaEmailMouseClicked(evt);
            }
        });

        doPlanRender();

    }

    private void doPlanRender() {
        pv.jfcx.JPVTable tbProjetos = listaProjetos.getTable();
        pv.jfcx.JPVTable tbIndicadores = listaIndicadores.getTable();

        tbProjetos.setShowGrid(false);
        tbProjetos.setOpaque(false);
        tbProjetos.setTableHeader(null);

        tbIndicadores.setTableHeader(null);
        tbIndicadores.setShowGrid(false);


        setColumnSize(listaProjetos.getTable().getColumnModel());
        setColumnSize(listaIndicadores.getTable().getColumnModel());
        setColumnSize(listaIndAVencer.getTable().getColumnModel());
        setColumnSize(listaProjAVencer.getTable().getColumnModel());
        listaEmail.getTable().getColumnModel().getColumn(0).setPreferredWidth(250);
        listaEmail.getTable().setBackground(new java.awt.Color(217, 235, 255));


        listaIndAVencer.setHeaderHeight(20);
        listaIndAVencer.setRowHeight(25);

        listaIndicadores.setHeaderHeight(20);
        listaIndicadores.setRowHeight(25);

        listaProjAVencer.setHeaderHeight(20);
        listaProjAVencer.setRowHeight(25);

        listaProjetos.setHeaderHeight(20);
        listaProjetos.setRowHeight(25);

        /*
        listaIndAVencer.getTable().setBackground(corAmarelo);
        listaIndicadores.getTable().setBackground(corVermelho);
        listaProjAVencer.getTable().setBackground(corAmarelo);
        listaProjetos.getTable().setBackground(corVermelho);
         *
         */
    }

    private void setColumnSize(TableColumnModel model) {
        TableColumn coluna = null;
        for (int i = 0; i < model.getColumnCount(); i++) {
            coluna = model.getColumn(i);
            coluna.setPreferredWidth(KpiLembrete.TAMANHO_COLUNAS[i]);
        }

    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public void loadRecord() {
    }

    /**
     * Carregamento personalizado para os lembretes.
     * @param record
     */
    public void lembreteLoadRecord(kpi.xml.BIXMLRecord record) {
        recPlanos = record;
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    private void projetoMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            int pos = listaProjetos.getSelectedRow();
            BIXMLRecord projeto = listaProjetos.getXMLData().get(pos);
            javax.swing.JInternalFrame frame =
                    kpi.core.KpiStaticReferences.getKpiFormController().getForm(
                    recordType, projeto.getString("ID"), projeto.getString("NOME")).asJInternalFrame();
            frame.show();
        }
    }

    private void projAVencerMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            int pos = listaProjAVencer.getSelectedRow();
            BIXMLRecord projAVencer = listaProjAVencer.getXMLData().get(pos);
            javax.swing.JInternalFrame frame =
                    kpi.core.KpiStaticReferences.getKpiFormController().getForm(
                    recordType, projAVencer.getString("ID"), projAVencer.getString("NOME")).asJInternalFrame();
            frame.show();
        }
    }

    private void indicadorMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            int pos = listaIndicadores.getSelectedRow();
            BIXMLRecord indicador = listaIndicadores.getXMLData().get(pos);
            javax.swing.JInternalFrame frame =
                    kpi.core.KpiStaticReferences.getKpiFormController().getForm(
                    recordType, indicador.getString("ID"), indicador.getString("NOME")).asJInternalFrame();
            frame.show();
        }
    }

    private void indAVencerMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            int pos = listaIndAVencer.getSelectedRow();
            BIXMLRecord indAVencer = listaIndAVencer.getXMLData().get(pos);
            javax.swing.JInternalFrame frame =
                    kpi.core.KpiStaticReferences.getKpiFormController().getForm(
                    recordType, indAVencer.getString("ID"), indAVencer.getString("NOME")).asJInternalFrame();
            frame.show();
        }
    }

    private void listaEmailMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            int pos = listaEmail.getSelectedRow();
            BIXMLRecord eMail = listaEmail.getXMLData().get(pos);
            kpi.swing.tools.KpiMensagemFrame frame = (kpi.swing.tools.KpiMensagemFrame) kpi.core.KpiStaticReferences.getKpiFormController().getForm(
                    "MENSAGEM", eMail.getString("ID"), eMail.getString("NOME")).asJInternalFrame();
            frame.setLembrete(this);
            frame.show();
        }
    }

    private void indicadorMouseEnter(java.awt.event.MouseEvent evt) {
        /*		int pos = listaIndicadores.getSelectedRow();
        BIXMLRecord indicador = listaIndicadores.getXMLData().get(pos);
        kpi.beans.JBIHyperlinkLabel hiperLink = new kpi.beans.JBIHyperlinkLabel("PLANOACAO", indicador.getString("ID"), indicador.getString("NOME"));
        listaIndicadores.setValueAt(hiperLink, pos, 1);*/
    }

    private void indAVencerMouseEnter(java.awt.event.MouseEvent evt) {
        /*		int pos = listaIndicadores.getSelectedRow();
        BIXMLRecord indicador = listaIndicadores.getXMLData().get(pos);
        kpi.beans.JBIHyperlinkLabel hiperLink = new kpi.beans.JBIHyperlinkLabel("PLANOACAO", indicador.getString("ID"), indicador.getString("NOME"));
        listaIndicadores.setValueAt(hiperLink, pos, 1);*/
    }

    public void addJBIListPanelListener(kpi.beans.JBIListPanelListener listener) {
    }

    public void removeJBIListPanelListener(kpi.beans.JBIListPanelListener listener) {
    }

    public void selectLine(int lineIni, int lineEnd) {
    }

    private void quantidadePlanos() {
        int indVencidos = listaIndicadores.getModel().getRowCount();
        int indAVencer = listaIndAVencer.getModel().getRowCount();
        int projVencidos = listaProjetos.getModel().getRowCount();
        int projAVencer = listaProjAVencer.getModel().getRowCount();
        int emailTot = listaEmail.getModel().getRowCount();
        int totAcao = indVencidos + indAVencer + projVencidos + projAVencer;
        String strVenc = "A vencer em ".concat(String.valueOf(prazoDia)).concat(" dias");


        String sAcao = "A��es (".concat(String.valueOf(totAcao)).concat(")");
        jPGroupAcao.setTitle(sAcao);

        String sInd = "Indicadores (".concat(String.valueOf(indVencidos + indAVencer)).concat(")");
        jTPlanoAcao.setTitle(sInd);

        String sProj = "Projetos (".concat(String.valueOf(projVencidos + projAVencer)).concat(")");
        jTProjeto.setTitle(sProj);

        String sEmail = "Mensagens recebidas (".concat(String.valueOf(emailTot)).concat(")");
        jTPMsg.setTitle(sEmail);


        jTabbedPane1.setTitleAt(1, strVenc);
        jTabbedPane2.setTitleAt(1, strVenc);

        if ((indVencidos + indAVencer) > 0) {
            jTPlanoAcao.setExpanded(true);
        } else if ((projVencidos + projAVencer) > 0) {
            jTProjeto.setExpanded(true);
        } else if (emailTot > 0) {
            jTPMsg.setExpanded(true);
        }

    }
}
