package kpi.swing.tools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import kpi.beans.JBIListPanel;
import kpi.core.KpiStaticReferences;

public class KpiLstDataSource extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "DATASRC";
    private String formType = "LSTDATASRC";
    private String parentType = recordType;
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiLstDataSource(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        kpi.xml.BIXMLRecord recDataSrc = event.listRecords("DATASRC", "");
        listaAgendamento.setDataSource(recDataSrc.getBIXMLVector("DATASRCS"), "0", "0", this.event);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new JTabbedPane();
        listaAgendamento = new JBIListPanel();
        pnlRightForm = new JPanel();
        pnlBottomForm = new JPanel();
        pnlLeftForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiDataSourceFrame_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_fontedados.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(545, 310));
        setRequestFocusEnabled(false);

        tapCadastro.setPreferredSize(new Dimension(530, 300));
        ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        tapCadastro.addTab(bundle1.getString("KpiDataSourceFrame_00001"), listaAgendamento); // NOI18N

        getContentPane().add(tapCadastro, BorderLayout.CENTER);

        pnlRightForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightForm, BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pack();
    }//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JBIListPanel listaAgendamento;
    private JPanel pnlBottomForm;
    private JPanel pnlLeftForm;
    private JPanel pnlRightForm;
    private JTabbedPane tapCadastro;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }
}
