package kpi.swing.tools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import kpi.beans.JBISelectionPanel;
import kpi.beans.JBITextArea;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiMensagemFrame extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "MENSAGEM";
    private String formType = recordType;
    private String parentType = recordType;
    private BIXMLVector vectorScoreResp = null;
    private boolean valid = true;
    final static int KPI_MSG_ENVIADA = 1;
    final static int KPI_MSG_RECEBIDA = 2;
    final static int KPI_MSG_EXCLUIDA = 3;
    final static String KPI_MSG_BAIXA = "1";
    final static String KPI_MSG_MEDIA = "2";
    final static String KPI_MSG_ALTA = "3";
    final static int KPI_MSG_REPLY = 1;
    final static int KPI_MSG_REPLYALL = 2;
    final static int KPI_MSG_FORWARD = 3;
    private kpi.swing.tools.KpiLembrete lembrete;
    java.util.Hashtable htbRemetente = new java.util.Hashtable();
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiMensagemFrame(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlMensagem, true);
        event.setEnableFields(listaPara, true);
        event.setEnableFields(listaCC, true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlMensagem, false);
        event.setEnableFields(listaPara, false);
        event.setEnableFields(listaCC, false);
    }

    @Override
    public void refreshFields() {
        fldAssunto.setText(record.getString("NOME"));
        txtMensagem.setText(record.getString("TEXTO"));
        rdbBaixa.setSelected(record.getString("PRIORIDADE").equals(KPI_MSG_BAIXA));
        rdbMedia.setSelected(record.getString("PRIORIDADE").equals(KPI_MSG_MEDIA));
        rdbAlta.setSelected(record.getString("PRIORIDADE").equals(KPI_MSG_ALTA));

        event.populateCombo(record.getBIXMLVector("REMETENTES"), "PARENTID", htbRemetente, cbbDe);
        cbbDe.removeItemAt(0);

        listaCC.setDataSource(record.getBIXMLVector("PESSOAS"), record.getBIXMLVector("CCS"), "CCS", "PESSOA");
        listaPara.setDataSource(record.getBIXMLVector("PESSOAS"), record.getBIXMLVector("PARAS"), "PARAS", "PESSOA");

        if (record.getInt("PASTA") == KPI_MSG_ENVIADA || record.getInt("PASTA") == KPI_MSG_EXCLUIDA) {
            btnReply.setVisible(false);
            btnReplyAll.setVisible(false);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);

        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        recordAux.set("PARENTID", event.getComboValue(htbRemetente, cbbDe, true));
        recordAux.set("NOME", fldAssunto.getText());
        recordAux.set("TEXTO", txtMensagem.getText());

        if (rdbBaixa.isSelected()) {
            recordAux.set("PRIORIDADE", KPI_MSG_BAIXA);
        } else if (rdbMedia.isSelected()) {
            recordAux.set("PRIORIDADE", KPI_MSG_MEDIA);
        } else {
            recordAux.set("PRIORIDADE", KPI_MSG_ALTA);
        }

        recordAux.set(listaPara.getXMLData());
        recordAux.set(listaCC.getXMLData());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;
        if (listaPara.getRowCount() <= 0 && listaCC.getRowCount() <= 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KPIMenssagem_00023"));
            retorno = false;
        }
        return retorno;
    }

    /**
     * Configura os dados de acordo com o tipo da mensagem.
     * @param operacao
     */
    public void configuraMensagem(int operacao) {
        int pasta = record.getInt("PASTA");
        if (vectorScoreResp != null) {
            pasta = 2;
        }
        int itens = 0;

        kpi.xml.BIXMLVector vectorRemetente = record.getBIXMLVector("REMETENTES").clone2();
        kpi.xml.BIXMLVector vectorPessoas = record.getBIXMLVector("PESSOAS").clone2();
        kpi.xml.BIXMLVector vectorRem = record.getBIXMLVector("REMS").clone2();
        kpi.xml.BIXMLVector vectorDest = record.getBIXMLVector("DESTS").clone2();

        if (pasta == KPI_MSG_ENVIADA && operacao == KPI_MSG_FORWARD) {

            record.set("NOME", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KPIMenssagem_00020") + record.getString("NOME"));

            //Extrai todos os record do vector "PARAS"
            itens = record.getBIXMLVector("PARAS").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("PARAS").removeRecord(0);
            }

            //Extrai todos os record do vector "CCS"
            itens = record.getBIXMLVector("CCS").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("CCS").removeRecord(0);
            }

            //Extrai todos os record do vector "PESSOAS"
            itens = record.getBIXMLVector("PESSOAS").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("PESSOAS").removeRecord(0);
            }

            //Adiciona destinatario do vector "DEST"
            for (int i = 0; i < vectorDest.size(); i++) {
                record.getBIXMLVector("PESSOAS").add(vectorDest.get(i));
            }

        } else if (pasta == KPI_MSG_RECEBIDA) {

            //Extrai todos os record do vector "REMETENTES"
            itens = record.getBIXMLVector("REMETENTES").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("REMETENTES").removeRecord(0);
            }

            //Adiciona destinatario do vector "REMS"
            for (int i = 0; i < vectorRem.size(); i++) {
                record.getBIXMLVector("REMETENTES").add(vectorRem.get(i));
            }

            //Extrai todos os record do vector "PARAS"
            itens = record.getBIXMLVector("PARAS").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("PARAS").removeRecord(0);
            }

            //Extrai todos os record do vector "CCS"
            itens = record.getBIXMLVector("CCS").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("CCS").removeRecord(0);
            }

            //Extrai todos os record do vector "PESSOAS"
            itens = record.getBIXMLVector("PESSOAS").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("PESSOAS").removeRecord(0);
            }

            //Adiciona lista de destinatarios na lista de "PESSOAS"
            for (int i = 0; i < vectorDest.size(); i++) {
                record.getBIXMLVector("PESSOAS").add(vectorDest.get(i));
            }

            for (int i = 0; i < vectorRemetente.size(); i++) {
                record.getBIXMLVector("PESSOAS").add(vectorRemetente.get(i));
            }

            if (operacao == KPI_MSG_REPLY) {
                if (this.vectorScoreResp != null) {
                    record.set(vectorScoreResp);
                } else {
                    record.set("NOME", "Resp: " + record.getString("NOME"));
                    //Adiciona o remetente na lista "PARAS"
                    for (int i = 0; i < vectorRemetente.size(); i++) {
                        record.getBIXMLVector("PARAS").add(vectorRemetente.get(i));
                    }
                }


            } else if (operacao == KPI_MSG_REPLYALL) {

                record.set("NOME", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KPIMenssagem_00019") + record.getString("NOME"));

                //Adiciona o remetente na lista "PARAS"
                for (int i = 0; i < vectorRemetente.size(); i++) {
                    record.getBIXMLVector("PARAS").add(vectorRemetente.get(i));
                }

                //Adiciona as pessoas da lista "PESSOAS" lista "PARAS"
                for (int i = 0; i < vectorPessoas.size(); i++) {
                    record.getBIXMLVector("PARAS").add(vectorPessoas.get(i));
                }

            } else if (operacao == KPI_MSG_FORWARD) {

                record.set("NOME", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KPIMenssagem_00020") + record.getString("NOME"));

            }
        } else if (pasta == KPI_MSG_EXCLUIDA) {

            record.set("NOME", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KPIMenssagem_00020") + record.getString("NOME"));

            //Extrai todos os record do vector "REMETENTES"
            itens = record.getBIXMLVector("REMETENTES").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("REMETENTES").removeRecord(0);
            }

            //Adiciona destinatario do vector "REMS"
            for (int i = 0; i < vectorRem.size(); i++) {
                record.getBIXMLVector("REMETENTES").add(vectorRem.get(i));
            }

            //Extrai todos os record do vector "PESSOAS"
            itens = record.getBIXMLVector("PESSOAS").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("PESSOAS").removeRecord(0);
            }

            //Adiciona lista de destinatarios na lista de "PESSOAS"
            for (int i = 0; i < vectorDest.size(); i++) {
                record.getBIXMLVector("PESSOAS").add(vectorDest.get(i));
            }

            //Extrai todos os record do vector "PARAS"
            itens = record.getBIXMLVector("PARAS").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("PARAS").removeRecord(0);
            }

            //Extrai todos os record do vector "CCS"
            itens = record.getBIXMLVector("CCS").size();
            for (int i = 0; i < itens; i++) {
                record.getBIXMLVector("CCS").removeRecord(0);
            }
        }

        enableFields();
        refreshFields();

        btnReply.setVisible(false);
    }

    /**
     * Realiza a atualização das mensagens da tela de lembrete.
     * @return
     */
    private kpi.xml.BIXMLRecord atualizaLembrete() {
        kpi.xml.BIXMLRecord recPlanos = kpi.core.KpiStaticReferences.getKpiDataController().listRecords("PLANOACAO", "LEMBRETE");
        kpi.xml.BIXMLRecord recMsg = kpi.core.KpiStaticReferences.getKpiDataController().listRecords("MENSAGEM", "");
        kpi.xml.BIXMLVector vctMsg = recMsg.getBIXMLVector("MENSAGENS");
        recPlanos.set(vctMsg);
        return recPlanos;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        grpPrioridade = new ButtonGroup();
        pnlRecord = new JPanel();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnReply = new JButton();
        btnReplyAll = new JButton();
        btnForward = new JButton();
        btnDelete = new JButton();
        pnlFields = new JPanel();
        pnlBottomRecord = new JPanel();
        pnlRightRecord = new JPanel();
        pnlLeftRecord = new JPanel();
        pnlTopRecord = new JPanel();
        tbMensagem = new JTabbedPane();
        pnlMensagem = new JPanel();
        pnlConfiguraca = new JPanel();
        lblDe = new JLabel();
        cbbDe = new JComboBox();
        lblAssunto = new JLabel();
        rdbAlta = new JRadioButton();
        rdbMedia = new JRadioButton();
        rdbBaixa = new JRadioButton();
        pnlCorpo = new JPanel();
        Mensagem = new JScrollPane();
        txtMensagem = new JBITextArea();
        pnlRightRecord1 = new JPanel();
        pnlLeftRecord1 = new JPanel();
        pnlBottomRecord1 = new JPanel();
        pnlAssunto = new JPanel();
        fldAssunto = new JTextField();
        pnlBottomRecord2 = new JPanel();
        pnlRightRecord2 = new JPanel();
        pnlLeftRecord2 = new JPanel();
        listaPara = new JBISelectionPanel(1);
        listaCC = new JBISelectionPanel(1);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KPIMenssagem_00002")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_email.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(467, 395));

        pnlRecord.setLayout(new BorderLayout());

        pnlTools.setMinimumSize(new Dimension(480, 22));
        pnlTools.setPreferredSize(new Dimension(10, 22));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 22));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 22));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KPIMenssagem_00003")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(510, 22));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(225, 32));

        btnReply.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReply.setText(bundle.getString("KPIMenssagem_00004")); // NOI18N
        btnReply.setMaximumSize(new Dimension(100, 22));
        btnReply.setMinimumSize(new Dimension(100, 22));
        btnReply.setPreferredSize(new Dimension(100, 22));
        btnReply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReplyActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReply);

        btnReplyAll.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReplyAll.setText(bundle.getString("KPIMenssagem_00005")); // NOI18N
        btnReplyAll.setMaximumSize(new Dimension(120, 22));
        btnReplyAll.setMinimumSize(new Dimension(120, 22));
        btnReplyAll.setPreferredSize(new Dimension(120, 22));
        btnReplyAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReplyAllActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReplyAll);

        btnForward.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnForward.setText(bundle.getString("KPIMenssagem_00006")); // NOI18N
        btnForward.setMaximumSize(new Dimension(100, 22));
        btnForward.setMinimumSize(new Dimension(100, 22));
        btnForward.setPreferredSize(new Dimension(100, 22));
        btnForward.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnForwardActionPerformed(evt);
            }
        });
        tbInsertion.add(btnForward);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.setMaximumSize(new Dimension(100, 22));
        btnDelete.setMinimumSize(new Dimension(100, 22));
        btnDelete.setPreferredSize(new Dimension(80, 22));
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        pnlRecord.add(pnlTools, BorderLayout.NORTH);

        pnlFields.setLayout(new BorderLayout());
        pnlFields.add(pnlBottomRecord, BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, BorderLayout.WEST);
        pnlFields.add(pnlTopRecord, BorderLayout.NORTH);

        pnlMensagem.setLayout(new BorderLayout());

        pnlConfiguraca.setMaximumSize(new Dimension(105, 105));
        pnlConfiguraca.setMinimumSize(new Dimension(105, 105));
        pnlConfiguraca.setPreferredSize(new Dimension(70, 70));
        pnlConfiguraca.setLayout(null);

        lblDe.setHorizontalAlignment(SwingConstants.LEFT);
        lblDe.setText(bundle.getString("KPIMenssagem_00007")); // NOI18N
        lblDe.setEnabled(false);
        pnlConfiguraca.add(lblDe);
        lblDe.setBounds(10, 10, 71, 20);

        cbbDe.setPreferredSize(new Dimension(24, 22));
        pnlConfiguraca.add(cbbDe);
        cbbDe.setBounds(10, 30, 400, 22);

        lblAssunto.setHorizontalAlignment(SwingConstants.LEFT);
        lblAssunto.setText("Assunto:");
        lblAssunto.setEnabled(false);
        pnlConfiguraca.add(lblAssunto);
        lblAssunto.setBounds(10, 50, 130, 20);

        grpPrioridade.add(rdbAlta);
        rdbAlta.setText(bundle.getString("KPIMenssagem_00021")); // NOI18N
        rdbAlta.setEnabled(false);
        pnlConfiguraca.add(rdbAlta);
        rdbAlta.setBounds(570, 30, 70, 22);

        grpPrioridade.add(rdbMedia);
        rdbMedia.setSelected(true);
        rdbMedia.setText(bundle.getString("KPIMenssagem_00011")); // NOI18N
        rdbMedia.setEnabled(false);
        pnlConfiguraca.add(rdbMedia);
        rdbMedia.setBounds(500, 30, 70, 22);

        grpPrioridade.add(rdbBaixa);
        rdbBaixa.setText(bundle.getString("KPIMenssagem_00012")); // NOI18N
        rdbBaixa.setEnabled(false);
        pnlConfiguraca.add(rdbBaixa);
        rdbBaixa.setBounds(440, 30, 70, 22);

        pnlMensagem.add(pnlConfiguraca, BorderLayout.PAGE_START);

        pnlCorpo.setLayout(new BorderLayout());

        Mensagem.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtMensagem.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        Mensagem.setViewportView(txtMensagem);

        pnlCorpo.add(Mensagem, BorderLayout.CENTER);
        pnlCorpo.add(pnlRightRecord1, BorderLayout.EAST);
        pnlCorpo.add(pnlLeftRecord1, BorderLayout.WEST);
        pnlCorpo.add(pnlBottomRecord1, BorderLayout.SOUTH);

        pnlAssunto.setLayout(new BorderLayout());
        pnlAssunto.add(fldAssunto, BorderLayout.CENTER);
        pnlAssunto.add(pnlBottomRecord2, BorderLayout.SOUTH);
        pnlAssunto.add(pnlRightRecord2, BorderLayout.EAST);
        pnlAssunto.add(pnlLeftRecord2, BorderLayout.WEST);

        pnlCorpo.add(pnlAssunto, BorderLayout.PAGE_START);

        pnlMensagem.add(pnlCorpo, BorderLayout.CENTER);

        tbMensagem.addTab(bundle.getString("KPIMenssagem_00009"), pnlMensagem); // NOI18N
        tbMensagem.addTab(bundle.getString("KPIMenssagem_00015"), listaPara); // NOI18N
        tbMensagem.addTab(bundle.getString("KPIMenssagem_00016"), listaCC); // NOI18N

        pnlFields.add(tbMensagem, BorderLayout.CENTER);

        pnlRecord.add(pnlFields, BorderLayout.CENTER);

        getContentPane().add(pnlRecord, BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            record.set("EMAIL", false);
            if (valid) {
                kpi.swing.KpiDefaultDialogSystem dialogSystem = kpi.core.KpiStaticReferences.getKpiDialogSystem();
                // Deseja notificar por email?
                if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KPIMenssagem_00017")) == javax.swing.JOptionPane.YES_OPTION) {
                    record.set("EMAIL", true);
                }
            }
            event.saveRecord();
            if (valid) {
                event.closeOperation();
            }

            this.vectorScoreResp = null;

	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
            kpi.beans.JBIMessagePanel parentFrame = (kpi.beans.JBIMessagePanel) this.getClientProperty("FORM_PARENT");
            if (parentFrame != null) {
                kpi.swing.KpiDefaultFrameBehavior parentFrameBehavior = parentFrame.getFrame();
                parentFrameBehavior.loadRecord();
            }
            event.closeOperation();

            if (getLembrete() != null) {
                kpi.xml.BIXMLRecord record = atualizaLembrete();
                if (record != null) {
                    getLembrete().lembreteLoadRecord(record);
                }
            }
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnForwardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnForwardActionPerformed
            kpi.swing.tools.KpiMensagemFrame frmDestino = (kpi.swing.tools.KpiMensagemFrame) kpi.core.KpiStaticReferences.getKpiFormController().newDetailForm("MENSAGEM", "0", "0");
            frmDestino.setRecord(cloneRecord());
            frmDestino.configuraMensagem(KPI_MSG_FORWARD);
	}//GEN-LAST:event_btnForwardActionPerformed

	private void btnReplyAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReplyAllActionPerformed
            kpi.swing.tools.KpiMensagemFrame frmDestino = (kpi.swing.tools.KpiMensagemFrame) kpi.core.KpiStaticReferences.getKpiFormController().newDetailForm("MENSAGEM", "0", "0");
            frmDestino.setRecord(cloneRecord());
            frmDestino.configuraMensagem(KPI_MSG_REPLYALL);
	}//GEN-LAST:event_btnReplyAllActionPerformed

	private void btnReplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReplyActionPerformed
            kpi.swing.tools.KpiMensagemFrame frmDestino = (kpi.swing.tools.KpiMensagemFrame) kpi.core.KpiStaticReferences.getKpiFormController().newDetailForm("MENSAGEM", "0", "0");
            frmDestino.setRecord(cloneRecord());
            frmDestino.configuraMensagem(KPI_MSG_REPLY);
	}//GEN-LAST:event_btnReplyActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JScrollPane Mensagem;
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnForward;
    private JButton btnReply;
    private JButton btnReplyAll;
    private JButton btnSave;
    private JComboBox cbbDe;
    private JTextField fldAssunto;
    private ButtonGroup grpPrioridade;
    private JLabel lblAssunto;
    private JLabel lblDe;
    private JBISelectionPanel listaCC;
    private JBISelectionPanel listaPara;
    private JPanel pnlAssunto;
    private JPanel pnlBottomRecord;
    private JPanel pnlBottomRecord1;
    private JPanel pnlBottomRecord2;
    private JPanel pnlConfiguraca;
    private JPanel pnlConfirmation;
    private JPanel pnlCorpo;
    private JPanel pnlFields;
    private JPanel pnlLeftRecord;
    private JPanel pnlLeftRecord1;
    private JPanel pnlLeftRecord2;
    private JPanel pnlMensagem;
    private JPanel pnlOperation;
    private JPanel pnlRecord;
    private JPanel pnlRightRecord;
    private JPanel pnlRightRecord1;
    private JPanel pnlRightRecord2;
    private JPanel pnlTools;
    private JPanel pnlTopRecord;
    private JRadioButton rdbAlta;
    private JRadioButton rdbBaixa;
    private JRadioButton rdbMedia;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JTabbedPane tbMensagem;
    private JBITextArea txtMensagem;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getFormType() {
        return formType;
    }

    private BIXMLRecord cloneRecord() {
        BIXMLRecord recFrame = record.clone2();
        recFrame.set(record.getBIXMLVector("PESSOAS").clone2());
        recFrame.set(record.getBIXMLVector("PARAS").clone2());
        recFrame.set(record.getBIXMLVector("CCS").clone2());
        recFrame.set(record.getBIXMLVector("REMETENTES").clone2());
        recFrame.set(record.getBIXMLVector("REMS").clone2());
        recFrame.set(record.getBIXMLVector("DESTS").clone2());

        return recFrame;
    }

    public kpi.swing.tools.KpiLembrete getLembrete() {
        return lembrete;
    }

    public void setLembrete(kpi.swing.tools.KpiLembrete lembrete) {
        this.lembrete = lembrete;
    }

    public void setAssunto(String assunto) {
        if (assunto != null && !("".equals(assunto))) {
            record.set("NOME", assunto);
        }
    }

    public void setDestinatario(BIXMLVector vectorDestinatario) {
        if (vectorDestinatario != null) {
            this.vectorScoreResp = vectorDestinatario;
        }
    }

    public boolean hasCombos() {
        return true;
    }

    public void populateCombos(kpi.xml.BIXMLRecord serverData) {
        return;
    }
}
