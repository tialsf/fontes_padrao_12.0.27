package kpi.swing.tools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import kpi.beans.JBIMessagePanel;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;

public class KpiMensagensExcluidasFrame extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "MENSAGENS_EXCLUIDAS";
    private String formType = recordType;
    private String parentType = recordType;
    private KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;
    private javax.swing.JButton btnExcluirTudo;

    public KpiMensagensExcluidasFrame(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        listaMensagem.setNewVisible(false);
        event.defaultConstructor(operation, idAux, contextId);
        addBtnExcluirTudo();
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        listaMensagem.setDataSource(record.getBIXMLVector("MENSAGENS"), record.getString("ID"), record.getString("ID"), this.event);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    public boolean hasCombos() {
        return false;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        pnlTopForm = new JPanel();
        tapCadastro = new JTabbedPane();
        listaMensagem = new JBIMessagePanel();
        pnlRightForm = new JPanel();
        pnlBottomForm = new JPanel();
        pnlLeftForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KPIMenssagensExcluidas_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_emailexcluido.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(600, 358));
        getContentPane().add(pnlTopForm, BorderLayout.NORTH);

        tapCadastro.addTab(bundle.getString("KPIMenssagensExcluidas_00001"), listaMensagem); // NOI18N

        getContentPane().add(tapCadastro, BorderLayout.CENTER);

        pnlRightForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightForm, BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pack();
    }//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JBIMessagePanel listaMensagem;
    private JPanel pnlBottomForm;
    private JPanel pnlLeftForm;
    private JPanel pnlRightForm;
    private JPanel pnlTopForm;
    private JTabbedPane tapCadastro;
    // End of variables declaration//GEN-END:variables

    private void addBtnExcluirTudo() {
        btnExcluirTudo = new javax.swing.JButton();
        btnExcluirTudo.setFont(new java.awt.Font("Tahoma", 0, 11));
        btnExcluirTudo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir_tudo.gif")));
        btnExcluirTudo.setVisible(true);
        btnExcluirTudo.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00081")); //"Excluir"

        btnExcluirTudo.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirTudoActionPerformed(evt);
            }
        });

        listaMensagem.tbInsertion.add(btnExcluirTudo);
        listaMensagem.tbInsertion.add(listaMensagem.btnAjuda);
        listaMensagem.pnlOperation.setPreferredSize(new java.awt.Dimension(600, 27));
        listaMensagem.tbInsertion.setMaximumSize(new java.awt.Dimension(600, 25));
        listaMensagem.tbInsertion.setPreferredSize(new java.awt.Dimension(600, 32));
    }

    private void btnExcluirTudoActionPerformed(java.awt.event.ActionEvent evt) {
        String comando = "";
        int iNumMensagens = listaMensagem.xmlTable.getModel().getRowCount();

        kpi.core.KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();
        kpi.swing.KpiDefaultDialogSystem oDialog = new kpi.swing.KpiDefaultDialogSystem(this);

        if (iNumMensagens != 0) {
            //"Deseja excluir todas as mensagens"
            int iRet = oDialog.confirmMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KPIMenssagem_00024"), 2);
            if (iRet == javax.swing.JOptionPane.OK_OPTION) {
                try {
                    comando = "DELETATODAS|";
                    comando = comando + this.getType() + "|";
                    dataController.executeRecord(this.getID(), "MENSAGEM", comando);
                    event.loadRecord();
                } catch (Exception e) {
                }
            }
        }
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getFormType() {
        return formType;
    }
}
