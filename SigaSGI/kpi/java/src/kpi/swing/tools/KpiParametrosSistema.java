package kpi.swing.tools;

import com.l2fprod.common.swing.JTaskPane;
import com.l2fprod.common.swing.JTaskPaneGroup;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import kpi.core.KpiCustomLabels;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiFileChooser;

public class KpiParametrosSistema extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "PARAMETRO"; //NOI18N
    private String formType = recordType;
    private String parentType = recordType;
    java.util.Hashtable htbItensSCD = new java.util.Hashtable();
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiParametrosSistema(int operation, String idAux, String contextId) {
        initComponents();
        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            jLabel4.setVisible(false);
            txtScorecardCaption.setVisible(false);

            chkRestPlan.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00063"));
            chkPermissaoRecursiva.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00062"));
        } else {
            chkRestPlan.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00037"));
            chkPermissaoRecursiva.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00061"));
        }
        
        putClientProperty("MAXI", true); //NOI18N
        event.defaultConstructor(operation, idAux, contextId);
        btnDelete.setVisible(false);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlScorecard, true);
        event.setEnableFields(pnlIndicador, true);
        event.setEnableFields(pnlAcao, true);
        event.setEnableFields(pnlApresentacao, true);
        event.setEnableFields(pnlConfiguracao, true);
        event.setEnableFields(pnlLabelConfig, true);        
        event.setEnableFields(pnlLabelScor, true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlScorecard, false);
        event.setEnableFields(pnlIndicador, false);
        event.setEnableFields(pnlAcao, false);
        event.setEnableFields(pnlApresentacao, false);
        event.setEnableFields(pnlConfiguracao, false);
        event.setEnableFields(pnlLabelConfig, false);
        event.setEnableFields(pnlLabelScor, false);
    }

    @Override
    public void refreshFields() {
        chkUserLog.setSelected(record.getBoolean("LOG_USER_ENABLE")); //NOI18N
        spnShowProjetosDias.setValue(record.getInt("NUM_DIA_PRO_FIN")); //NOI18N
        spnPrazoVencimento.setValue(record.getInt("PRAZO_PARA_VENC")); //NOI18N
        spnUpdScorecard.setValue(record.getInt("SCRDING_INTERVAL_UPD")); //NOI18N
        spnCasasVariacao.setValue(record.getInt("DECIMALVAR")); //NOI18N
        chkVariacaoPercent.setSelected(record.getBoolean("SHOWVARCOL")); //NOI18N
        chkValorPrevio.setSelected(record.getBoolean("VALOR_PREVIO")); //NOI18N
        chkDataPlanoAcao.setSelected(record.getBoolean("ALT_DATAFIM_PLANO")); //NOI18N
        chkCfgDtAcumulado.setSelected(record.getBoolean("CFGDTACUMULADO")); //NOI18N
        chkShowColPeriodo.setSelected(record.getBoolean("SHOWCOLPERIODO")); //NOI18N
        chkShowColMedida.setSelected(record.getBoolean("SHOWCOLMEDIDA")); //NOI18N
        chkIndVermApresentacao.setSelected(record.getBoolean("INDVERM_APRESENT")); //NOI18N
        chkAltVlr.setSelected(record.getBoolean("BLQ_ALT_VLR")); //NOI18N
        chkWfInsertAction.setSelected(record.getBoolean("WFINSERTACTION")); //NOI18N
        chkRestPlan.setSelected(record.getBoolean("OWNER_ALT_PLANILHA")); //NOI18N
        chkAltMeta.setSelected(record.getBoolean("INFO_ALT_META")); //NOI18N
        chkIncMeta.setSelected(record.getBoolean("INFO_INC_META")); //NOI18N
        chkExcMeta.setSelected(record.getBoolean("INFO_EXC_META")); //NOI18N
        chkPermissaoRecursiva.setSelected(record.getBoolean("PERMISSAO_RECURSIVA")); //NOI18N
        chkSegurancaNota.setSelected(record.getBoolean("SEGURANCA_NOTA")); //NOI18N
        txtScorecardCaption.setText(record.getString("STR_SCO"));
        txtRealCaption.setText(record.getString("STR_REAL"));
        txtMetaCaption.setText(record.getString("STR_META"));
        txtPreviaCaption.setText(record.getString("STR_PREVIA"));
        chkFilterAcao.setSelected(record.getBoolean("FILTER_ACAO_PLANACAO")); //NOI18N
        chkControleAprov.setSelected(record.getBoolean("CTR_APROV_PLANACAO")); //NOI18N
        chkAvalMetaScore.setSelected(record.getBoolean("AVAL_META_SCORE"));
        chkNenhumEmailAlerta.setSelected(record.getBoolean("NENHUM_MAIL_ALERTA"));
        txtTendencia.setText(record.getString("STR_TEND"));

        event.populateCombo(record.getBIXMLVector("ITENS_SCD"), "", htbItensSCD, cboOrdemScoreCarding); //NOI18N
        cboOrdemScoreCarding.removeItemAt(0);
        cboOrdemScoreCarding.setSelectedIndex(record.getInt("ORDEM_SCD")); //NOI18N

        if (record.getString("CFGDTMESANTERIOR").equals("0")) { //NOI18N
            chkCfgDtAlvoMesAnterior.setSelected(false);
        } else {
            chkCfgDtAlvoMesAnterior.setSelected(true);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        recordAux.set("ID", getID()); //NOI18N
        recordAux.set("LOG_USER_ENABLE", chkUserLog.isSelected()); //NOI18N
        recordAux.set("NUM_DIA_PRO_FIN", ((Integer) spnShowProjetosDias.getValue()).intValue()); //NOI18N
        recordAux.set("PRAZO_PARA_VENC", ((Integer) spnPrazoVencimento.getValue()).intValue()); //NOI18N
        recordAux.set("SCRDING_INTERVAL_UPD", ((Integer) spnUpdScorecard.getValue()).intValue()); //NOI18N
        recordAux.set("DECIMALVAR", ((Integer) spnCasasVariacao.getValue()).intValue()); //NOI18N
        recordAux.set("SHOWVARCOL", chkVariacaoPercent.isSelected()); //NOI18N
        recordAux.set("VALOR_PREVIO", chkValorPrevio.isSelected()); //NOI18N
        recordAux.set("ALT_DATAFIM_PLANO", chkDataPlanoAcao.isSelected()); //NOI18N
        recordAux.set("CFGDTACUMULADO", chkCfgDtAcumulado.isSelected()); //NOI18N
        recordAux.set("SHOWCOLPERIODO", chkShowColPeriodo.isSelected()); //NOI18N
        recordAux.set("SHOWCOLMEDIDA", chkShowColMedida.isSelected()); //NOI18N
        recordAux.set("INDVERM_APRESENT", chkIndVermApresentacao.isSelected()); //NOI18N
        recordAux.set("BLQ_ALT_VLR", chkAltVlr.isSelected()); //NOI18N
        recordAux.set("WFINSERTACTION", chkWfInsertAction.isSelected()); //NOI18N
        recordAux.set("OWNER_ALT_PLANILHA", chkRestPlan.isSelected()); //NOI18N
        recordAux.set("INFO_ALT_META", chkAltMeta.isSelected()); //NOI18N
        recordAux.set("INFO_INC_META", chkIncMeta.isSelected()); //NOI18N
        recordAux.set("INFO_EXC_META", chkExcMeta.isSelected()); //NOI18N
        recordAux.set("ORDEM_SCD", cboOrdemScoreCarding.getSelectedIndex()); //NOI18N
        recordAux.set("PERMISSAO_RECURSIVA", chkPermissaoRecursiva.isSelected()); //NOI18N
        recordAux.set("SEGURANCA_NOTA", chkSegurancaNota.isSelected()); //NOI18N
        recordAux.set("STR_SCO", txtScorecardCaption.getText()); //NOI18N
        recordAux.set("STR_REAL", txtRealCaption.getText()); //NOI18N
        recordAux.set("STR_META", txtMetaCaption.getText()); //NOI18N
        recordAux.set("STR_PREVIA", txtPreviaCaption.getText()); //NOI18N
        recordAux.set("FILTER_ACAO_PLANACAO", chkFilterAcao.isSelected()); //NOI18N
        recordAux.set("CTR_APROV_PLANACAO", chkControleAprov.isSelected()); //NOI18N
        recordAux.set("AVAL_META_SCORE", chkAvalMetaScore.isSelected()); //NOI18N
        recordAux.set("NENHUM_MAIL_ALERTA", chkNenhumEmailAlerta.isSelected()); //NOI18N
        recordAux.set("STR_TEND", txtTendencia.getText()); //NOI18N

        if (chkCfgDtAlvoMesAnterior.isSelected()) {
            recordAux.set("CFGDTMESANTERIOR", "-1"); //NOI18N
        } else {
            recordAux.set("CFGDTMESANTERIOR", "0"); //NOI18N
        }

        KpiCustomLabels customLabels = KpiStaticReferences.getKpiCustomLabels();
        customLabels.setSco(txtScorecardCaption.getText());
        customLabels.setReal(txtRealCaption.getText());
        customLabels.setMeta(txtMetaCaption.getText());
        customLabels.setPrevia(txtPreviaCaption.getText());
        customLabels.setTend(txtTendencia.getText());
        KpiStaticReferences.getKpiMainPanel().refreshCustomLabels();
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean bRet = true;

        if (txtScorecardCaption.getText().trim().length() == 0) {
            errorMessage.append("Campo T�tulo do ScoreCard � obrigat�rio.\n");
            bRet = false;
        }

        if (txtRealCaption.getText().trim().length() == 0) {
            errorMessage.append("Campo t�tulo do campo Real � obrigat�rio.\n");
            bRet = false;
        }

        if (txtMetaCaption.getText().trim().length() == 0) {
            errorMessage.append("Campo t�tulo do campo Meta � obrigat�rio.\n");
            bRet = false;
        }

        if (txtPreviaCaption.getText().trim().length() == 0) {
            errorMessage.append("Campo t�tulo do campo Previa � obrigat�rio.\n");
            bRet = false;
        }

        //Valida��o do titulo da Tend�ncia no scorecarding
        if (txtTendencia.getText().trim().length() == 0) {
            errorMessage.append("Campo t�tulo do campo Tend�ncia � obrigat�rio.\n");
            bRet = false;
        }
        
        return bRet;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new JFrame();
        jFrame2 = new JFrame();
        pnlRecord = new JPanel();
        scrParametro = new JScrollPane();
        pnlParametro = new JPanel();
        tskParametro = new JTaskPane();
        tskScorecard = new JTaskPaneGroup();
        pnlScorecard = new JPanel();
        chkCfgDtAlvoMesAnterior = new JCheckBox();
        lblOrdemScoreCarding = new JLabel();
        cboOrdemScoreCarding = new JComboBox();
        lblInterUpdScorecard = new JLabel();
        spnCasasVariacao = new JSpinner();
        lblCasasVariacao = new JLabel();
        spnUpdScorecard = new JSpinner();
        lblSexUpdScorecard = new JLabel();
        lblSexCasasDecimais = new JLabel();
        chkVariacaoPercent = new JCheckBox();
        chkCfgDtAcumulado = new JCheckBox();
        chkShowColPeriodo = new JCheckBox();
        chkShowColMedida = new JCheckBox();
        chkPermissaoRecursiva = new JCheckBox();
        chkAvalMetaScore = new JCheckBox();
        pnlLabelScor = new JPanel();
        lblTendencia = new JLabel();
        txtTendencia = new JTextField();
        tskIndicador = new JTaskPaneGroup();
        pnlIndicador = new JPanel();
        chkAltVlr = new JCheckBox();
        chkValorPrevio = new JCheckBox();
        chkRestPlan = new JCheckBox();
        pnlAuditoriaMeta = new JPanel();
        chkIncMeta = new JCheckBox();
        chkAltMeta = new JCheckBox();
        chkExcMeta = new JCheckBox();
        chkSegurancaNota = new JCheckBox();
        tskAcao = new JTaskPaneGroup();
        pnlAcao = new JPanel();
        chkDataPlanoAcao = new JCheckBox();
        chkWfInsertAction = new JCheckBox();
        lblShowProjetos = new JLabel();
        lblShowPrazoVencimento = new JLabel();
        spnShowProjetosDias = new JSpinner();
        spnPrazoVencimento = new JSpinner();
        lblShowProjetosDias = new JLabel();
        lblShowPrazoVencimentoDias = new JLabel();
        chkFilterAcao = new JCheckBox();
        chkControleAprov = new JCheckBox();
        chkNenhumEmailAlerta = new JCheckBox();
        tskApresentacao = new JTaskPaneGroup();
        pnlApresentacao = new JPanel();
        chkIndVermApresentacao = new JCheckBox();
        tskConfiguracao = new JTaskPaneGroup();
        pnlConfiguracao = new JPanel();
        chkUserLog = new JCheckBox();
        pnlLabelConfig = new JPanel();
        jLabel1 = new JLabel();
        txtPreviaCaption = new JTextField();
        jLabel2 = new JLabel();
        txtRealCaption = new JTextField();
        jLabel3 = new JLabel();
        txtMetaCaption = new JTextField();
        jLabel4 = new JLabel();
        txtScorecardCaption = new JTextField();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        btnReload = new JButton();
        btnLogUser = new JButton();
        btnAjuda = new JButton();
        pnlBottomForm = new JPanel();
        pnlLeftForm = new JPanel();
        pnlRightRecord = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiParametrosSistema_00050")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_system_setup.gif"))); // NOI18N
        setMinimumSize(new Dimension(125, 335));
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(605, 396));

        pnlRecord.setLayout(new BorderLayout());

        scrParametro.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        scrParametro.setPreferredSize(new Dimension(482, 222));

        pnlParametro.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlParametro.setAutoscrolls(true);
        pnlParametro.setMinimumSize(new Dimension(0, 0));
        pnlParametro.setPreferredSize(new Dimension(480, 380));
        pnlParametro.setLayout(new BorderLayout());

        tskParametro.setAutoscrolls(true);

        tskScorecard.setExpanded(false);
        tskScorecard.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_scorecard.gif"))); // NOI18N
        tskScorecard.setScrollOnExpand(true);
        tskScorecard.setTitle("Scorecarding");

        pnlScorecard.setOpaque(false);
        pnlScorecard.setPreferredSize(new Dimension(600, 400));
        pnlScorecard.setLayout(null);

        chkCfgDtAlvoMesAnterior.setText(bundle.getString("KpiParametrosSistema_00024")); // NOI18N
        chkCfgDtAlvoMesAnterior.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkCfgDtAlvoMesAnterior.setEnabled(false);
        chkCfgDtAlvoMesAnterior.setMaximumSize(new Dimension(189, 23));
        chkCfgDtAlvoMesAnterior.setMinimumSize(new Dimension(189, 23));
        chkCfgDtAlvoMesAnterior.setOpaque(false);
        chkCfgDtAlvoMesAnterior.setPreferredSize(new Dimension(189, 23));
        pnlScorecard.add(chkCfgDtAlvoMesAnterior);
        chkCfgDtAlvoMesAnterior.setBounds(0, 0, 440, 22);

        lblOrdemScoreCarding.setHorizontalAlignment(SwingConstants.LEFT);
        lblOrdemScoreCarding.setText(bundle.getString("KpiParametrosSistema_00015")); // NOI18N
        lblOrdemScoreCarding.setEnabled(false);
        lblOrdemScoreCarding.setMaximumSize(new Dimension(100, 16));
        lblOrdemScoreCarding.setMinimumSize(new Dimension(100, 16));
        lblOrdemScoreCarding.setPreferredSize(new Dimension(100, 16));
        pnlScorecard.add(lblOrdemScoreCarding);
        lblOrdemScoreCarding.setBounds(0, 140, 220, 20);

        pnlScorecard.add(cboOrdemScoreCarding);
        cboOrdemScoreCarding.setBounds(0, 160, 180, 22);

        ResourceBundle bundle1 = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblInterUpdScorecard.setText(bundle1.getString("KpiParametrosSistema_00039")); // NOI18N
        lblInterUpdScorecard.setEnabled(false);
        pnlScorecard.add(lblInterUpdScorecard);
        lblInterUpdScorecard.setBounds(0, 180, 230, 20);
        pnlScorecard.add(spnCasasVariacao);
        spnCasasVariacao.setBounds(0, 240, 180, 22);

        lblCasasVariacao.setText(bundle.getString("KpiParametrosSistema_00047")); // NOI18N
        lblCasasVariacao.setEnabled(false);
        pnlScorecard.add(lblCasasVariacao);
        lblCasasVariacao.setBounds(0, 220, 200, 20);
        pnlScorecard.add(spnUpdScorecard);
        spnUpdScorecard.setBounds(0, 200, 180, 22);

        lblSexUpdScorecard.setHorizontalAlignment(SwingConstants.LEFT);
        lblSexUpdScorecard.setText(bundle1.getString("KpiParametrosSistema_00040")); // NOI18N
        lblSexUpdScorecard.setEnabled(false);
        lblSexUpdScorecard.setMaximumSize(new Dimension(100, 16));
        lblSexUpdScorecard.setMinimumSize(new Dimension(100, 16));
        lblSexUpdScorecard.setPreferredSize(new Dimension(100, 16));
        pnlScorecard.add(lblSexUpdScorecard);
        lblSexUpdScorecard.setBounds(190, 200, 70, 16);

        lblSexCasasDecimais.setHorizontalAlignment(SwingConstants.LEFT);
        lblSexCasasDecimais.setText(bundle.getString("KpiParametrosSistema_00046")); // NOI18N
        lblSexCasasDecimais.setEnabled(false);
        lblSexCasasDecimais.setMaximumSize(new Dimension(100, 16));
        lblSexCasasDecimais.setMinimumSize(new Dimension(100, 16));
        lblSexCasasDecimais.setPreferredSize(new Dimension(100, 16));
        pnlScorecard.add(lblSexCasasDecimais);
        lblSexCasasDecimais.setBounds(190, 240, 120, 16);

        chkVariacaoPercent.setText(bundle.getString("KpiParametrosSistema_00048")); // NOI18N
        chkVariacaoPercent.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkVariacaoPercent.setEnabled(false);
        chkVariacaoPercent.setMaximumSize(new Dimension(189, 23));
        chkVariacaoPercent.setMinimumSize(new Dimension(189, 23));
        chkVariacaoPercent.setOpaque(false);
        chkVariacaoPercent.setPreferredSize(new Dimension(189, 23));
        pnlScorecard.add(chkVariacaoPercent);
        chkVariacaoPercent.setBounds(0, 80, 450, 22);

        chkCfgDtAcumulado.setText(bundle.getString("KpiParametrosSistema_00049")); // NOI18N
        chkCfgDtAcumulado.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkCfgDtAcumulado.setEnabled(false);
        chkCfgDtAcumulado.setMaximumSize(new Dimension(189, 23));
        chkCfgDtAcumulado.setMinimumSize(new Dimension(189, 23));
        chkCfgDtAcumulado.setOpaque(false);
        chkCfgDtAcumulado.setPreferredSize(new Dimension(189, 23));
        pnlScorecard.add(chkCfgDtAcumulado);
        chkCfgDtAcumulado.setBounds(0, 20, 440, 22);

        chkShowColPeriodo.setText(bundle.getString("KpiParametrosSistema_00053")); // NOI18N
        chkShowColPeriodo.setActionCommand(bundle.getString("KpiParametrosSistema_00051")); // NOI18N
        chkShowColPeriodo.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkShowColPeriodo.setEnabled(false);
        chkShowColPeriodo.setMaximumSize(new Dimension(189, 23));
        chkShowColPeriodo.setMinimumSize(new Dimension(189, 23));
        chkShowColPeriodo.setOpaque(false);
        chkShowColPeriodo.setPreferredSize(new Dimension(189, 23));
        pnlScorecard.add(chkShowColPeriodo);
        chkShowColPeriodo.setBounds(0, 60, 440, 22);

        chkShowColMedida.setText(bundle.getString("KpiParametrosSistema_00052")); // NOI18N
        chkShowColMedida.setActionCommand(bundle.getString("KpiParametrosSistema_00051")); // NOI18N
        chkShowColMedida.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkShowColMedida.setEnabled(false);
        chkShowColMedida.setMaximumSize(new Dimension(189, 23));
        chkShowColMedida.setMinimumSize(new Dimension(189, 23));
        chkShowColMedida.setOpaque(false);
        chkShowColMedida.setPreferredSize(new Dimension(189, 23));
        pnlScorecard.add(chkShowColMedida);
        chkShowColMedida.setBounds(0, 40, 440, 22);

        chkPermissaoRecursiva.setText("O respons�vel por um scorecard pode acessar todos os filhos do scorecard. (N�o recomendado)");
        chkPermissaoRecursiva.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        chkPermissaoRecursiva.setEnabled(false);
        chkPermissaoRecursiva.setOpaque(false);
        pnlScorecard.add(chkPermissaoRecursiva);
        chkPermissaoRecursiva.setBounds(0, 100, 600, 20);

        chkAvalMetaScore.setText("Respeita valores da planilha independente da meta do indicador");
        chkAvalMetaScore.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        chkAvalMetaScore.setEnabled(false);
        chkAvalMetaScore.setOpaque(false);
        pnlScorecard.add(chkAvalMetaScore);
        chkAvalMetaScore.setBounds(0, 120, 390, 20);

        pnlLabelScor.setBorder(BorderFactory.createTitledBorder(bundle.getString("KpiParametrosSistema_00064"))); // NOI18N
        pnlLabelScor.setOpaque(false);
        pnlLabelScor.setPreferredSize(new Dimension(300, 80));
        pnlLabelScor.setLayout(null);

        lblTendencia.setText(bundle.getString("KpiIndicadorFormula_00012")); // NOI18N
        pnlLabelScor.add(lblTendencia);
        lblTendencia.setBounds(10, 20, 160, 14);

        txtTendencia.setText(bundle.getString("KpiParametrosSistema_00065")); // NOI18N
        pnlLabelScor.add(txtTendencia);
        txtTendencia.setBounds(10, 40, 160, 20);

        pnlScorecard.add(pnlLabelScor);
        pnlLabelScor.setBounds(10, 280, 300, 80);
        pnlLabelScor.getAccessibleContext().setAccessibleName("Personalisar titulo da coluna:"); // NOI18N

        tskScorecard.getContentPane().add(pnlScorecard);

        tskParametro.add(tskScorecard);

        tskIndicador.setExpanded(false);
        tskIndicador.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_indicador.gif"))); // NOI18N
        tskIndicador.setScrollOnExpand(true);
        tskIndicador.setTitle(bundle.getString("KpiParametrosSistema_00042")); // NOI18N

        pnlIndicador.setMinimumSize(new Dimension(460, 188));
        pnlIndicador.setOpaque(false);
        pnlIndicador.setPreferredSize(new Dimension(460, 188));
        pnlIndicador.setLayout(null);

        chkAltVlr.setText(bundle.getString("KpiParametrosSistema_00014")); // NOI18N
        chkAltVlr.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkAltVlr.setEnabled(false);
        chkAltVlr.setMaximumSize(new Dimension(189, 23));
        chkAltVlr.setMinimumSize(new Dimension(189, 23));
        chkAltVlr.setOpaque(false);
        chkAltVlr.setPreferredSize(new Dimension(189, 23));
        chkAltVlr.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                chkAltVlrActionPerformed(evt);
            }
        });
        pnlIndicador.add(chkAltVlr);
        chkAltVlr.setBounds(0, 0, 440, 22);

        chkValorPrevio.setText(bundle1.getString("KpiParametrosSistema_00011")); // NOI18N
        chkValorPrevio.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkValorPrevio.setEnabled(false);
        chkValorPrevio.setOpaque(false);
        chkValorPrevio.setPreferredSize(new Dimension(189, 23));
        pnlIndicador.add(chkValorPrevio);
        chkValorPrevio.setBounds(0, 40, 440, 22);

        chkRestPlan.setText(bundle.getString("KpiParametrosSistema_00037")); // NOI18N
        chkRestPlan.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkRestPlan.setEnabled(false);
        chkRestPlan.setMaximumSize(new Dimension(189, 23));
        chkRestPlan.setMinimumSize(new Dimension(189, 23));
        chkRestPlan.setOpaque(false);
        chkRestPlan.setPreferredSize(new Dimension(189, 23));
        pnlIndicador.add(chkRestPlan);
        chkRestPlan.setBounds(0, 20, 450, 22);

        pnlAuditoriaMeta.setBorder(BorderFactory.createTitledBorder(bundle.getString("KpiParametrosSistema_00054"))); // NOI18N
        pnlAuditoriaMeta.setOpaque(false);
        pnlAuditoriaMeta.setLayout(null);

        chkIncMeta.setText(bundle.getString("KpiParametrosSistema_00055")); // NOI18N
        chkIncMeta.setActionCommand(bundle.getString("KpiParametrosSistema_00055")); // NOI18N
        chkIncMeta.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        chkIncMeta.setEnabled(false);
        chkIncMeta.setLabel(bundle.getString("KpiParametrosSistema_00055")); // NOI18N
        chkIncMeta.setMaximumSize(new Dimension(189, 31));
        chkIncMeta.setMinimumSize(new Dimension(189, 31));
        chkIncMeta.setOpaque(false);
        chkIncMeta.setPreferredSize(new Dimension(189, 31));
        pnlAuditoriaMeta.add(chkIncMeta);
        chkIncMeta.setBounds(10, 20, 440, 22);
        chkIncMeta.getAccessibleContext().setAccessibleName(bundle.getString("KpiParametrosSistema_00055")); // NOI18N

        chkAltMeta.setText(bundle.getString("KpiParametrosSistema_00056")); // NOI18N
        chkAltMeta.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        chkAltMeta.setEnabled(false);
        chkAltMeta.setLabel(bundle.getString("KpiParametrosSistema_00056")); // NOI18N
        chkAltMeta.setMaximumSize(new Dimension(189, 31));
        chkAltMeta.setMinimumSize(new Dimension(189, 31));
        chkAltMeta.setOpaque(false);
        chkAltMeta.setPreferredSize(new Dimension(189, 31));
        pnlAuditoriaMeta.add(chkAltMeta);
        chkAltMeta.setBounds(10, 40, 440, 22);

        chkExcMeta.setText(bundle.getString("KpiParametrosSistema_00057")); // NOI18N
        chkExcMeta.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        chkExcMeta.setEnabled(false);
        chkExcMeta.setLabel(bundle.getString("KpiParametrosSistema_00057")); // NOI18N
        chkExcMeta.setMaximumSize(new Dimension(189, 31));
        chkExcMeta.setMinimumSize(new Dimension(189, 31));
        chkExcMeta.setOpaque(false);
        chkExcMeta.setPreferredSize(new Dimension(189, 31));
        pnlAuditoriaMeta.add(chkExcMeta);
        chkExcMeta.setBounds(10, 60, 530, 22);

        pnlIndicador.add(pnlAuditoriaMeta);
        pnlAuditoriaMeta.setBounds(0, 90, 390, 90);
        pnlAuditoriaMeta.getAccessibleContext().setAccessibleName(bundle.getString("KpiParametrosSistema_00054")); // NOI18N

        chkSegurancaNota.setText(bundle.getString("KpiParametrosSistema_00058")); // NOI18N
        chkSegurancaNota.setActionCommand(bundle.getString("KpiParametrosSistema_00058")); // NOI18N
        chkSegurancaNota.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        chkSegurancaNota.setEnabled(false);
        chkSegurancaNota.setLabel(bundle.getString("KpiParametrosSistema_00058")); // NOI18N
        chkSegurancaNota.setOpaque(false);
        pnlIndicador.add(chkSegurancaNota);
        chkSegurancaNota.setBounds(0, 60, 670, 20);
        chkSegurancaNota.getAccessibleContext().setAccessibleName(bundle.getString("KpiParametrosSistema_00058")); // NOI18N

        tskIndicador.getContentPane().add(pnlIndicador);

        tskParametro.add(tskIndicador);

        tskAcao.setExpanded(false);
        tskAcao.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif"))); // NOI18N
        tskAcao.setScrollOnExpand(true);
        tskAcao.setTitle(bundle.getString("KpiParametrosSistema_00043")); // NOI18N

        pnlAcao.setMinimumSize(new Dimension(500, 190));
        pnlAcao.setOpaque(false);
        pnlAcao.setPreferredSize(new Dimension(500, 200));
        pnlAcao.setLayout(null);

        chkDataPlanoAcao.setText(bundle.getString("KpiParametrosSistema_00012")); // NOI18N
        chkDataPlanoAcao.setActionCommand(bundle.getString("KpiParametrosSistema_00012")); // NOI18N
        chkDataPlanoAcao.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkDataPlanoAcao.setEnabled(false);
        chkDataPlanoAcao.setLabel(bundle.getString("KpiParametrosSistema_00012")); // NOI18N
        chkDataPlanoAcao.setMaximumSize(new Dimension(189, 23));
        chkDataPlanoAcao.setMinimumSize(new Dimension(189, 23));
        chkDataPlanoAcao.setOpaque(false);
        chkDataPlanoAcao.setPreferredSize(new Dimension(189, 23));
        pnlAcao.add(chkDataPlanoAcao);
        chkDataPlanoAcao.setBounds(0, 0, 440, 22);
        chkDataPlanoAcao.getAccessibleContext().setAccessibleName(bundle.getString("KpiParametrosSistema_00012")); // NOI18N

        chkWfInsertAction.setText(bundle.getString("KpiParametrosSistema_00026")); // NOI18N
        chkWfInsertAction.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkWfInsertAction.setEnabled(false);
        chkWfInsertAction.setMaximumSize(new Dimension(189, 23));
        chkWfInsertAction.setMinimumSize(new Dimension(189, 23));
        chkWfInsertAction.setOpaque(false);
        chkWfInsertAction.setPreferredSize(new Dimension(189, 23));
        pnlAcao.add(chkWfInsertAction);
        chkWfInsertAction.setBounds(0, 20, 610, 22);

        lblShowProjetos.setHorizontalAlignment(SwingConstants.LEFT);
        lblShowProjetos.setText(bundle.getString("KpiParametrosSistema_00004")); // NOI18N
        lblShowProjetos.setEnabled(false);
        lblShowProjetos.setMaximumSize(new Dimension(100, 16));
        lblShowProjetos.setMinimumSize(new Dimension(100, 16));
        lblShowProjetos.setPreferredSize(new Dimension(100, 16));
        pnlAcao.add(lblShowProjetos);
        lblShowProjetos.setBounds(0, 110, 220, 20);

        lblShowPrazoVencimento.setText(bundle1.getString("KpiParametrosSistema_00010")); // NOI18N
        lblShowPrazoVencimento.setEnabled(false);
        pnlAcao.add(lblShowPrazoVencimento);
        lblShowPrazoVencimento.setBounds(0, 150, 230, 20);

        spnShowProjetosDias.setOpaque(false);
        pnlAcao.add(spnShowProjetosDias);
        spnShowProjetosDias.setBounds(0, 130, 180, 22);

        spnPrazoVencimento.setOpaque(false);
        pnlAcao.add(spnPrazoVencimento);
        spnPrazoVencimento.setBounds(0, 170, 180, 22);

        lblShowProjetosDias.setHorizontalAlignment(SwingConstants.LEFT);
        lblShowProjetosDias.setText(bundle.getString("KpiParametrosSistema_00005")); // NOI18N
        lblShowProjetosDias.setEnabled(false);
        lblShowProjetosDias.setMaximumSize(new Dimension(100, 16));
        lblShowProjetosDias.setMinimumSize(new Dimension(100, 16));
        lblShowProjetosDias.setPreferredSize(new Dimension(100, 16));
        pnlAcao.add(lblShowProjetosDias);
        lblShowProjetosDias.setBounds(190, 130, 35, 20);

        lblShowPrazoVencimentoDias.setHorizontalAlignment(SwingConstants.LEFT);
        lblShowPrazoVencimentoDias.setText(bundle.getString("KpiParametrosSistema_00005")); // NOI18N
        lblShowPrazoVencimentoDias.setEnabled(false);
        lblShowPrazoVencimentoDias.setMaximumSize(new Dimension(100, 16));
        lblShowPrazoVencimentoDias.setMinimumSize(new Dimension(100, 16));
        lblShowPrazoVencimentoDias.setPreferredSize(new Dimension(100, 16));
        pnlAcao.add(lblShowPrazoVencimentoDias);
        lblShowPrazoVencimentoDias.setBounds(190, 170, 35, 20);

        chkFilterAcao.setText( ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00059")); // NOI18N
        chkFilterAcao.setActionCommand( ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00059")); // NOI18N
        chkFilterAcao.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkFilterAcao.setEnabled(false);
        chkFilterAcao.setLabel( ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00059")); // NOI18N
        chkFilterAcao.setMaximumSize(new Dimension(189, 23));
        chkFilterAcao.setMinimumSize(new Dimension(189, 23));
        chkFilterAcao.setOpaque(false);
        chkFilterAcao.setPreferredSize(new Dimension(189, 23));
        pnlAcao.add(chkFilterAcao);
        chkFilterAcao.setBounds(0, 60, 460, 22);
        chkFilterAcao.getAccessibleContext().setAccessibleName( ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00059")); // NOI18N

        chkControleAprov.setText(bundle.getString("KpiParametrosSistema_00060")); // NOI18N
        chkControleAprov.setActionCommand( ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00060")); // NOI18N
        chkControleAprov.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkControleAprov.setEnabled(false);
        chkControleAprov.setLabel( ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00060")); // NOI18N
        chkControleAprov.setMaximumSize(new Dimension(189, 23));
        chkControleAprov.setMinimumSize(new Dimension(189, 23));
        chkControleAprov.setOpaque(false);
        chkControleAprov.setPreferredSize(new Dimension(189, 23));
        pnlAcao.add(chkControleAprov);
        chkControleAprov.setBounds(0, 80, 460, 22);
        chkControleAprov.getAccessibleContext().setAccessibleName( ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00060")); // NOI18N

        chkNenhumEmailAlerta.setText("Exibir alerta somente quando nenhum respons�vel possuir email cadastrado. (Aplica-se tamb�m para o envio de mensagens entre usu�rios)"); // NOI18N
        chkNenhumEmailAlerta.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkNenhumEmailAlerta.setEnabled(false);
        chkNenhumEmailAlerta.setMaximumSize(new Dimension(189, 23));
        chkNenhumEmailAlerta.setMinimumSize(new Dimension(189, 23));
        chkNenhumEmailAlerta.setOpaque(false);
        chkNenhumEmailAlerta.setPreferredSize(new Dimension(189, 23));
        pnlAcao.add(chkNenhumEmailAlerta);
        chkNenhumEmailAlerta.setBounds(0, 40, 750, 22);

        tskAcao.getContentPane().add(pnlAcao);

        tskParametro.add(tskAcao);

        tskApresentacao.setExpanded(false);
        tskApresentacao.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        tskApresentacao.setScrollOnExpand(true);
        tskApresentacao.setTitle(bundle.getString("KpiParametrosSistema_00044")); // NOI18N

        pnlApresentacao.setOpaque(false);
        pnlApresentacao.setPreferredSize(new Dimension(460, 20));
        pnlApresentacao.setLayout(null);

        chkIndVermApresentacao.setText(bundle.getString("KpiParametrosSistema_00025")); // NOI18N
        chkIndVermApresentacao.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkIndVermApresentacao.setEnabled(false);
        chkIndVermApresentacao.setMaximumSize(new Dimension(189, 23));
        chkIndVermApresentacao.setMinimumSize(new Dimension(189, 23));
        chkIndVermApresentacao.setOpaque(false);
        chkIndVermApresentacao.setPreferredSize(new Dimension(189, 23));
        pnlApresentacao.add(chkIndVermApresentacao);
        chkIndVermApresentacao.setBounds(0, 0, 420, 22);

        tskApresentacao.getContentPane().add(pnlApresentacao);

        tskParametro.add(tskApresentacao);

        tskConfiguracao.setExpanded(false);
        tskConfiguracao.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_system_setup.gif"))); // NOI18N
        tskConfiguracao.setScrollOnExpand(true);
        tskConfiguracao.setTitle(bundle.getString("KpiParametrosSistema_00045")); // NOI18N

        pnlConfiguracao.setOpaque(false);
        pnlConfiguracao.setPreferredSize(new Dimension(400, 180));
        pnlConfiguracao.setLayout(null);

        chkUserLog.setText(bundle.getString("KpiParametrosSistema_00003")); // NOI18N
        chkUserLog.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkUserLog.setEnabled(false);
        chkUserLog.setOpaque(false);
        pnlConfiguracao.add(chkUserLog);
        chkUserLog.setBounds(0, 0, 250, 22);

        pnlLabelConfig.setBorder(BorderFactory.createTitledBorder(bundle.getString("KpiParametrosSistema_00064"))); // NOI18N
        pnlLabelConfig.setOpaque(false);
        pnlLabelConfig.setLayout(null);

        jLabel1.setText("Previa:");
        pnlLabelConfig.add(jLabel1);
        jLabel1.setBounds(10, 70, 160, 14);

        txtPreviaCaption.setText("Previa");
        pnlLabelConfig.add(txtPreviaCaption);
        txtPreviaCaption.setBounds(10, 90, 160, 20);

        jLabel2.setText("Real:");
        pnlLabelConfig.add(jLabel2);
        jLabel2.setBounds(210, 20, 160, 14);

        txtRealCaption.setText("Real");
        pnlLabelConfig.add(txtRealCaption);
        txtRealCaption.setBounds(210, 40, 160, 20);

        jLabel3.setText("Meta:");
        pnlLabelConfig.add(jLabel3);
        jLabel3.setBounds(210, 70, 160, 14);

        txtMetaCaption.setText("Meta");
        pnlLabelConfig.add(txtMetaCaption);
        txtMetaCaption.setBounds(210, 90, 160, 20);

        jLabel4.setText("ScoreCard:");
        pnlLabelConfig.add(jLabel4);
        jLabel4.setBounds(10, 20, 160, 14);

        txtScorecardCaption.setText("Scorecard");
        pnlLabelConfig.add(txtScorecardCaption);
        txtScorecardCaption.setBounds(10, 40, 160, 20);

        pnlConfiguracao.add(pnlLabelConfig);
        pnlLabelConfig.setBounds(0, 30, 390, 140);

        tskConfiguracao.getContentPane().add(pnlConfiguracao);

        tskParametro.add(tskConfiguracao);

        pnlParametro.add(tskParametro, BorderLayout.CENTER);

        scrParametro.setViewportView(pnlParametro);

        pnlRecord.add(scrParametro, BorderLayout.CENTER);

        getContentPane().add(pnlRecord, BorderLayout.CENTER);

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 25));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setPreferredSize(new Dimension(310, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(225, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle1.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnLogUser.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_log.gif"))); // NOI18N
        btnLogUser.setText(bundle1.getString("KpiParametrosSistema_00006")); // NOI18N
        btnLogUser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnLogUserActionPerformed(evt);
            }
        });
        tbInsertion.add(btnLogUser);

        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlRightRecord.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightRecord, BorderLayout.EAST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnLogUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogUserActionPerformed
            String serverFolder = "logs\\userslog\\";
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            frmChooser.setFileType(".html");
            frmChooser.loadServerFiles(serverFolder.concat("*.html"));
            frmChooser.setVisible(true);

            frmChooser.setUrlPath("/logs/userslog/");
            frmChooser.openFile();
	}//GEN-LAST:event_btnLogUserActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
//		event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void chkAltVlrActionPerformed(ActionEvent evt) {//GEN-FIRST:event_chkAltVlrActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkAltVlrActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAjuda;
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnLogUser;
    private JButton btnReload;
    private JButton btnSave;
    private JComboBox cboOrdemScoreCarding;
    private JCheckBox chkAltMeta;
    private JCheckBox chkAltVlr;
    private JCheckBox chkAvalMetaScore;
    private JCheckBox chkCfgDtAcumulado;
    private JCheckBox chkCfgDtAlvoMesAnterior;
    private JCheckBox chkControleAprov;
    private JCheckBox chkDataPlanoAcao;
    private JCheckBox chkExcMeta;
    private JCheckBox chkFilterAcao;
    private JCheckBox chkIncMeta;
    private JCheckBox chkIndVermApresentacao;
    private JCheckBox chkNenhumEmailAlerta;
    private JCheckBox chkPermissaoRecursiva;
    private JCheckBox chkRestPlan;
    private JCheckBox chkSegurancaNota;
    private JCheckBox chkShowColMedida;
    private JCheckBox chkShowColPeriodo;
    private JCheckBox chkUserLog;
    private JCheckBox chkValorPrevio;
    private JCheckBox chkVariacaoPercent;
    private JCheckBox chkWfInsertAction;
    private JFrame jFrame1;
    private JFrame jFrame2;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel lblCasasVariacao;
    private JLabel lblInterUpdScorecard;
    private JLabel lblOrdemScoreCarding;
    private JLabel lblSexCasasDecimais;
    private JLabel lblSexUpdScorecard;
    private JLabel lblShowPrazoVencimento;
    private JLabel lblShowPrazoVencimentoDias;
    private JLabel lblShowProjetos;
    private JLabel lblShowProjetosDias;
    private JLabel lblTendencia;
    private JPanel pnlAcao;
    private JPanel pnlApresentacao;
    private JPanel pnlAuditoriaMeta;
    private JPanel pnlBottomForm;
    private JPanel pnlConfiguracao;
    private JPanel pnlConfirmation;
    private JPanel pnlIndicador;
    private JPanel pnlLabelConfig;
    private JPanel pnlLabelScor;
    private JPanel pnlLeftForm;
    private JPanel pnlOperation;
    private JPanel pnlParametro;
    private JPanel pnlRecord;
    private JPanel pnlRightRecord;
    private JPanel pnlScorecard;
    private JPanel pnlTools;
    private JScrollPane scrParametro;
    private JSpinner spnCasasVariacao;
    private JSpinner spnPrazoVencimento;
    private JSpinner spnShowProjetosDias;
    private JSpinner spnUpdScorecard;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JTaskPaneGroup tskAcao;
    private JTaskPaneGroup tskApresentacao;
    private JTaskPaneGroup tskConfiguracao;
    private JTaskPaneGroup tskIndicador;
    private JTaskPane tskParametro;
    private JTaskPaneGroup tskScorecard;
    private JTextField txtMetaCaption;
    private JTextField txtPreviaCaption;
    private JTextField txtRealCaption;
    private JTextField txtScorecardCaption;
    private JTextField txtTendencia;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    @Override
    public String getParentType() {
        return parentType;
    }
}
