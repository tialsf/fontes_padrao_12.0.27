package kpi.swing.tools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import kpi.beans.JBIXMLTable;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;
import pv.jfcx.JPVTime;

public class KpiRestAcesso extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = new String("CFGRESTACESSO");
    private String formType = recordType;
    private String parentType = recordType;
    private KpiTabRestricaoIp tabRestricaoIp = new KpiTabRestricaoIp();
    Hashtable htbItensSCD = new Hashtable();

    public KpiRestAcesso(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlHorario, true);
        event.setEnableFields(pnlIP, true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlHorario, false);
        event.setEnableFields(pnlIP, false);
    }

    @Override
    public void refreshFields() {
        tblIp.setDataSource(record.getBIXMLVector("RESTACESSOS"));
        chkIp.setSelected(record.getBoolean("RESTRINGI_IP"));
        chkTime.setSelected(record.getBoolean("RESTRINGI_HORARIO"));

        StringTokenizer horario = new StringTokenizer(record.getString("HORARIO_ACESSO"), "|");
        if (horario.hasMoreElements()) {
            jPVTimeDe.setValue(horario.nextToken());
        }
        if (horario.hasMoreElements()) {
            jPVTimeAte.setValue(horario.nextToken());
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        BIXMLRecord conteudo = new BIXMLRecord(recordType);

        conteudo.set("ID", getID());
        conteudo.set("RESTRINGI_IP", chkIp.isSelected());
        conteudo.set("RESTRINGI_HORARIO", chkTime.isSelected());
        conteudo.set("HORARIO_ACESSO", jPVTimeDe.getText().concat("|").concat(jPVTimeAte.getText()));

        BIXMLVector vctIPRestrito = tblIp.getXMLData().clone2();
        BIXMLRecord recIPRestrito = tabRestricaoIp.getDelDados();

        if (recIPRestrito != null) {
            vctIPRestrito.add(tabRestricaoIp.getDelDados());
        }

        conteudo.set(vctIPRestrito);

        return conteudo;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean isValido = true;

        if (jPVTimeDe.getTime().compareTo(jPVTimeAte.getTime()) > 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00034")); //"Per�odo para restri��o de acesso inv�lido."
            isValido = false;
        }

        return isValido;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new JTabbedPane();
        pnlHorario = new JPanel();
        chkTime = new JCheckBox();
        lblDe = new JLabel();
        lblAte = new JLabel();
        jPVTimeDe = new JPVTime();
        jPVTimeAte = new JPVTime();
        pnlIP = new JPanel();
        tblIp = new JBIXMLTable();
        pnlIpTop = new JPanel();
        chkIp = new JCheckBox();
        pnlIpEast = new JPanel();
        btnAdd = new JButton();
        btnRemove = new JButton();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnReload = new JButton();
        btnAjuda = new JButton();
        pnlRightForm = new JPanel();
        pnlBottomForm = new JPanel();
        pnlLeftForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiParametrosSistema_00050")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_seguranca.gif"))); // NOI18N
        setMinimumSize(new Dimension(125, 335));
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(605, 396));

        pnlHorario.setPreferredSize(new Dimension(0, 120));
        pnlHorario.setLayout(null);

        chkTime.setText(bundle.getString("KpiParametrosSistema_00029")); // NOI18N
        chkTime.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkTime.setMargin(new Insets(0, 0, 0, 0));
        chkTime.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                chkTimeActionPerformed(evt);
            }
        });
        pnlHorario.add(chkTime);
        chkTime.setBounds(10, 0, 440, 30);

        lblDe.setHorizontalAlignment(SwingConstants.LEFT);
        lblDe.setText(bundle.getString("KpiParametrosSistema_00030")); // NOI18N
        lblDe.setHorizontalTextPosition(SwingConstants.LEFT);
        pnlHorario.add(lblDe);
        lblDe.setBounds(10, 40, 25, 20);

        lblAte.setHorizontalAlignment(SwingConstants.LEFT);
        lblAte.setText(bundle.getString("KpiParametrosSistema_00031")); // NOI18N
        lblAte.setHorizontalTextPosition(SwingConstants.LEFT);
        pnlHorario.add(lblAte);
        lblAte.setBounds(10, 70, 25, 20);

        jPVTimeDe.setAlignment(1);
        jPVTimeDe.setLeadingZero(true);
        jPVTimeDe.setShowSeconds(false);
        jPVTimeDe.setTwelveHours(false);
        pnlHorario.add(jPVTimeDe);
        jPVTimeDe.setBounds(40, 40, 180, 22);

        jPVTimeAte.setAlignment(1);
        jPVTimeAte.setLeadingZero(true);
        jPVTimeAte.setShowSeconds(false);
        jPVTimeAte.setTwelveHours(false);
        pnlHorario.add(jPVTimeAte);
        jPVTimeAte.setBounds(40, 70, 180, 22);

        tapCadastro.addTab("Restri��o por hor�rio\t", pnlHorario);

        pnlIP.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlIP.setPreferredSize(new Dimension(266, 300));
        pnlIP.setLayout(new BorderLayout());
        pnlIP.add(tblIp, BorderLayout.CENTER);

        pnlIpTop.setPreferredSize(new Dimension(0, 30));
        pnlIpTop.setLayout(null);

        chkIp.setText(bundle.getString("KpiParametrosSistema_00033")); // NOI18N
        chkIp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkIp.setMargin(new Insets(0, 0, 0, 0));
        chkIp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                chkIpActionPerformed(evt);
            }
        });
        pnlIpTop.add(chkIp);
        chkIp.setBounds(10, 0, 430, 30);

        pnlIP.add(pnlIpTop, BorderLayout.PAGE_START);

        pnlIpEast.setPreferredSize(new Dimension(50, 0));
        pnlIpEast.setLayout(null);

        btnAdd.setMnemonic('+');
        btnAdd.setText("+");
        ResourceBundle bundle1 = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAdd.setToolTipText(bundle1.getString("JBISelectionPanel_00002")); // NOI18N
        btnAdd.setIconTextGap(0);
        btnAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        pnlIpEast.add(btnAdd);
        btnAdd.setBounds(5, 6, 42, 23);

        btnRemove.setMnemonic('-');
        btnRemove.setText("-");
        btnRemove.setToolTipText(bundle1.getString("JBISelectionPanel_00001")); // NOI18N
        btnRemove.setIconTextGap(0);
        btnRemove.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        pnlIpEast.add(btnRemove);
        btnRemove.setBounds(5, 38, 42, 23);

        pnlIP.add(pnlIpEast, BorderLayout.EAST);

        tapCadastro.addTab("Restri��o por IP", pnlIP);

        getContentPane().add(tapCadastro, BorderLayout.CENTER);

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 29));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setPreferredSize(new Dimension(310, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(225, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setFont(new Font("Dialog", 0, 12));
        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightForm, BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pack();
    }//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
            int linhaSelecionada = tblIp.getSelectedRow();
            if (linhaSelecionada > -1) {
                kpi.xml.BIXMLRecord recLinha = tblIp.getXMLData().get(linhaSelecionada);
                tabRestricaoIp.addRecordDeleted(tblIp.getXMLData().get(linhaSelecionada));
                tblIp.removeRecord();
            }
	}//GEN-LAST:event_btnRemoveActionPerformed

	private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
            tblIp.addNewRecord(tabRestricaoIp.createNewRecord());
	}//GEN-LAST:event_btnAddActionPerformed

	private void chkIpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkIpActionPerformed
            enableFields();
	}//GEN-LAST:event_chkIpActionPerformed

	private void chkTimeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkTimeActionPerformed
            enableFields();
	}//GEN-LAST:event_chkTimeActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAdd;
    private JButton btnAjuda;
    private JButton btnCancel;
    private JButton btnEdit;
    private JButton btnReload;
    private JButton btnRemove;
    private JButton btnSave;
    private JCheckBox chkIp;
    private JCheckBox chkTime;
    private JPVTime jPVTimeAte;
    private JPVTime jPVTimeDe;
    private JLabel lblAte;
    private JLabel lblDe;
    private JPanel pnlBottomForm;
    private JPanel pnlConfirmation;
    private JPanel pnlHorario;
    private JPanel pnlIP;
    private JPanel pnlIpEast;
    private JPanel pnlIpTop;
    private JPanel pnlLeftForm;
    private JPanel pnlOperation;
    private JPanel pnlRightForm;
    private JPanel pnlTools;
    private JTabbedPane tapCadastro;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JBIXMLTable tblIp;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }
}

/**
 *
 * @author Lucio Pelinson
 */
class KpiTabRestricaoIp {

    private kpi.xml.BIXMLAttributes attIndicador = new kpi.xml.BIXMLAttributes();
    private kpi.xml.BIXMLVector vctDados = null;
    private kpi.xml.BIXMLRecord recDelDados = null;

    public KpiTabRestricaoIp() {
        setVctDados(new kpi.xml.BIXMLVector("RESTACESSOS", "RESTACESSO"));
    }

    public kpi.xml.BIXMLVector getVctDados() {
        return vctDados;
    }

    public void setVctDados(kpi.xml.BIXMLVector vctDados) {
        this.vctDados = vctDados;
    }

    public kpi.xml.BIXMLRecord createNewRecord() {
        kpi.xml.BIXMLRecord newRecord = new kpi.xml.BIXMLRecord("RESTACESSO");
        newRecord.set("ID", "N_E_W_");
        newRecord.set("IP", "");
        newRecord.set("DESCRICAO", "");

        return newRecord;
    }

    public void addRecordDeleted(kpi.xml.BIXMLRecord record) {
        if (!record.getString("ID").equals("N_E_W_")) {
            if (recDelDados == null) {
                recDelDados = new kpi.xml.BIXMLRecord("REG_EXCLUIDO");
                recDelDados.set(new kpi.xml.BIXMLVector("EXCLUIDOS", "EXCLUIDO"));
            }
            kpi.xml.BIXMLVector vctAddReg = recDelDados.getBIXMLVector("EXCLUIDOS");
            vctAddReg.add(record);
        }
    }

    public kpi.xml.BIXMLRecord getDelDados() {
        return recDelDados;
    }

    public void resetDados() {
        setVctDados(new kpi.xml.BIXMLVector("RESTACESSO", "RESTACESSOS"));
    }

    public void resetExcluidos() {
        recDelDados = null;
    }

    public void setCabecPeriodo() {
        resetDados();
        attIndicador = new kpi.xml.BIXMLAttributes();
        attIndicador.set("TIPO", "RESTACESSO");
        attIndicador.set("RETORNA", "F");

        attIndicador.set("TAG001", "IP");
        attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00035")); //"Endere�o IP"
        attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_STRING);
        attIndicador.set("EDT001", "T");
        attIndicador.set("CUM001", "F");

        attIndicador.set("TAG002", "DESCRICAO");
        attIndicador.set("CAB002", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiParametrosSistema_00036")); //"Descri��o"
        attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_STRING);
        attIndicador.set("EDT002", "T");
        attIndicador.set("CUM002", "F");

        vctDados.setAttributes(attIndicador);
    }
}
