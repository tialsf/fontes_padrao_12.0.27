package kpi.swing.tools;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import kpi.swing.KpiDefaultFrameBehavior;

public class KpiRestricaoPlanValor extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    /**
     * ************************************************************************
     */
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /**
     * ************************************************************************
     */
    private String recordType = new String("CFGPLANVLR"); //NOI18N
    private String formType = recordType;//Tipo do Formulario
    private String parentType = recordType;//Tipo formulario pai, caso haja.
    java.util.Hashtable htbItensSCD = new java.util.Hashtable();

    @Override
    public void enableFields() {
        // Habilita os campos do formul�rio.s

        tblExcecaoUser.setEnabled(true);
        tblExcecaoDpto.setEnabled(true);
        setEnableFields(pnlIndicadores, true);
        setEnableFields(pnlMsgMail, true);

    }

    @Override
    public void disableFields() {
        // Desabilita os campos do formul�rio.
        tblExcecaoUser.setEnabled(false);
        tblExcecaoDpto.setEnabled(false);
        setEnableFields(pnlIndicadores, false);
        setEnableFields(pnlMsgMail, false);
    }

    private void setEnableFields(javax.swing.JComponent compToEnable, boolean enable) {
        for (int i = 0; i < compToEnable.getComponentCount(); i++) {
            compToEnable.getComponent(i).setEnabled(enable);
        }

        txtMsgMail.setEnabled(showMsgDiaLimite && chkBlqDiaLimite.isSelected());
            
    }

    @Override
    public void refreshFields() {
        fldDataRealDe.setCalendar(record.getDate("DATA_ALT_REALDE")); //NOI18N
        fldDataRealAte.setCalendar(record.getDate("DATA_ALT_REALATE")); //NOI18N
        fldDataMetaDe.setCalendar(record.getDate("DATA_ALT_METADE")); //NOI18N
        fldDataMetaAte.setCalendar(record.getDate("DATA_ALT_METAATE")); //NOI18N
        fldDataPreviaDe.setCalendar(record.getDate("DATA_ALT_PREVIADE")); //NOI18N
        fldDataPreviaAte.setCalendar(record.getDate("DATA_ALT_PREVIAATE")); //NOI18N
        tblExcecaoUser.setDataSource(record.getBIXMLVector("PERMISSOES_ALTERACAO")); //NOI18N
        tblExcecaoDpto.setDataSource(record.getBIXMLVector("PERMISSOES_ALTERACAO_DPTO")); //NOI18N
        applyRenderParametro(tblExcecaoUser);
        applyRenderParametro(tblExcecaoDpto);
        chkBlqDiaLimite.setSelected(record.getBoolean("BLOQ_POR_DIA_LIMITE")); //NOI18N
        txtMsgMail.setText(record.getString("MSG_BLOQ_DIA_LIMITE")); //NOI18N
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        recordAux.set("ID", getID()); //NOI18N
        recordAux.set("DATA_ALT_REALDE", fldDataRealDe.getCalendar()); //NOI18N
        recordAux.set("DATA_ALT_REALATE", fldDataRealAte.getCalendar()); //NOI18N
        recordAux.set("DATA_ALT_METADE", fldDataMetaDe.getCalendar()); //NOI18N
        recordAux.set("DATA_ALT_METAATE", fldDataMetaAte.getCalendar()); //NOI18N
        recordAux.set("DATA_ALT_PREVIADE", fldDataPreviaDe.getCalendar()); //NOI18N
        recordAux.set("DATA_ALT_PREVIAATE", fldDataPreviaAte.getCalendar()); //NOI18N
        recordAux.set(tblExcecaoUser.getXMLData());
        recordAux.set(tblExcecaoDpto.getXMLData());
        recordAux.set("BLOQ_POR_DIA_LIMITE", chkBlqDiaLimite.isSelected()); //NOI18N
        recordAux.set("MSG_BLOQ_DIA_LIMITE",txtMsgMail.getText().trim());
        
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlIndicadores = new javax.swing.JPanel();
        scrDataMetaDe = new javax.swing.JScrollPane();
        fldDataMetaDe = new pv.jfcx.JPVDatePlus();
        scrDataRealDe = new javax.swing.JScrollPane();
        fldDataRealDe = new pv.jfcx.JPVDatePlus();
        scrDataPreviaDe = new javax.swing.JScrollPane();
        fldDataPreviaDe = new pv.jfcx.JPVDatePlus();
        scrDataRealAte = new javax.swing.JScrollPane();
        fldDataRealAte = new pv.jfcx.JPVDatePlus();
        scrDataMetaAte = new javax.swing.JScrollPane();
        fldDataMetaAte = new pv.jfcx.JPVDatePlus();
        scrDataPreviaAte = new javax.swing.JScrollPane();
        fldDataPreviaAte = new pv.jfcx.JPVDatePlus();
        lblMeta = new javax.swing.JLabel();
        lblRealDe = new javax.swing.JLabel();
        lblReal = new javax.swing.JLabel();
        lblPreviaDe = new javax.swing.JLabel();
        lblPrevia = new javax.swing.JLabel();
        lblMetaAte = new javax.swing.JLabel();
        lblRealAte = new javax.swing.JLabel();
        lblPreviaAte = new javax.swing.JLabel();
        lblMetaDe = new javax.swing.JLabel();
        chkBlqDiaLimite = new javax.swing.JCheckBox();
        pnlMsgMail = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtMsgMail = new kpi.beans.JBITextArea();
        pnlExcecao = new javax.swing.JPanel();
        tapExecoes = new javax.swing.JTabbedPane();
        pnlUsuario = new javax.swing.JPanel();
        tblExcecaoUser = new kpi.beans.JBIXMLTable();
        pnlDepto = new javax.swing.JPanel();
        tblExcecaoDpto = new kpi.beans.JBIXMLTable();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiParametrosSistema_00050")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_painelsco.gif"))); // NOI18N
        setMinimumSize(new java.awt.Dimension(125, 335));
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(671, 475));

        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        pnlIndicadores.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), bundle1.getString("KpiParametrosSistema_00008"))); // NOI18N
        pnlIndicadores.setLayout(null);

        fldDataMetaDe.setBackground(new java.awt.Color(251, 255, 240));
        fldDataMetaDe.setBorderStyle(0);
        fldDataMetaDe.setButtonBorder(0);
        fldDataMetaDe.setDialog(true);
        fldDataMetaDe.setUseLocale(true);
        fldDataMetaDe.setWaitForCalendarDate(true);
        scrDataMetaDe.setViewportView(fldDataMetaDe);

        pnlIndicadores.add(scrDataMetaDe);
        scrDataMetaDe.setBounds(20, 80, 180, 22);

        fldDataRealDe.setBackground(new java.awt.Color(234, 255, 255));
        fldDataRealDe.setBorderStyle(0);
        fldDataRealDe.setButtonBorder(0);
        fldDataRealDe.setDialog(true);
        fldDataRealDe.setUseLocale(true);
        fldDataRealDe.setWaitForCalendarDate(true);
        scrDataRealDe.setViewportView(fldDataRealDe);

        pnlIndicadores.add(scrDataRealDe);
        scrDataRealDe.setBounds(20, 40, 180, 22);

        fldDataPreviaDe.setBackground(new java.awt.Color(255, 230, 179));
        fldDataPreviaDe.setBorderStyle(0);
        fldDataPreviaDe.setButtonBorder(0);
        fldDataPreviaDe.setDialog(true);
        fldDataPreviaDe.setUseLocale(true);
        fldDataPreviaDe.setWaitForCalendarDate(true);
        scrDataPreviaDe.setViewportView(fldDataPreviaDe);

        pnlIndicadores.add(scrDataPreviaDe);
        scrDataPreviaDe.setBounds(20, 120, 180, 22);

        fldDataRealAte.setBackground(new java.awt.Color(234, 255, 255));
        fldDataRealAte.setBorderStyle(0);
        fldDataRealAte.setButtonBorder(0);
        fldDataRealAte.setDialog(true);
        fldDataRealAte.setUseLocale(true);
        fldDataRealAte.setWaitForCalendarDate(true);
        scrDataRealAte.setViewportView(fldDataRealAte);

        pnlIndicadores.add(scrDataRealAte);
        scrDataRealAte.setBounds(230, 40, 180, 22);

        fldDataMetaAte.setBackground(new java.awt.Color(251, 255, 240));
        fldDataMetaAte.setBorderStyle(0);
        fldDataMetaAte.setButtonBorder(0);
        fldDataMetaAte.setDialog(true);
        fldDataMetaAte.setUseLocale(true);
        fldDataMetaAte.setWaitForCalendarDate(true);
        scrDataMetaAte.setViewportView(fldDataMetaAte);

        pnlIndicadores.add(scrDataMetaAte);
        scrDataMetaAte.setBounds(230, 80, 180, 22);

        fldDataPreviaAte.setBackground(new java.awt.Color(255, 230, 179));
        fldDataPreviaAte.setBorderStyle(0);
        fldDataPreviaAte.setButtonBorder(0);
        fldDataPreviaAte.setDialog(true);
        fldDataPreviaAte.setUseLocale(true);
        fldDataPreviaAte.setWaitForCalendarDate(true);
        scrDataPreviaAte.setViewportView(fldDataPreviaAte);

        pnlIndicadores.add(scrDataPreviaAte);
        scrDataPreviaAte.setBounds(230, 120, 180, 22);

        lblMeta.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblMeta.setText(bundle1.getString("KpiParametrosSistema_00019")); // NOI18N
        lblMeta.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlIndicadores.add(lblMeta);
        lblMeta.setBounds(20, 60, 50, 20);

        lblRealDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRealDe.setText(bundle1.getString("KpiParametrosSistema_00021")); // NOI18N
        lblRealDe.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlIndicadores.add(lblRealDe);
        lblRealDe.setBounds(70, 20, 130, 20);

        lblReal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblReal.setText(bundle1.getString("KpiParametrosSistema_00018")); // NOI18N
        lblReal.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlIndicadores.add(lblReal);
        lblReal.setBounds(20, 20, 50, 20);

        lblPreviaDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPreviaDe.setText(bundle1.getString("KpiParametrosSistema_00021")); // NOI18N
        lblPreviaDe.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlIndicadores.add(lblPreviaDe);
        lblPreviaDe.setBounds(70, 100, 130, 20);

        lblPrevia.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblPrevia.setText(bundle1.getString("KpiParametrosSistema_00020")); // NOI18N
        lblPrevia.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlIndicadores.add(lblPrevia);
        lblPrevia.setBounds(20, 100, 50, 20);

        lblMetaAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMetaAte.setText(bundle1.getString("KpiParametrosSistema_00022")); // NOI18N
        lblMetaAte.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlIndicadores.add(lblMetaAte);
        lblMetaAte.setBounds(230, 60, 40, 20);

        lblRealAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRealAte.setText(bundle1.getString("KpiParametrosSistema_00022")); // NOI18N
        lblRealAte.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlIndicadores.add(lblRealAte);
        lblRealAte.setBounds(230, 20, 140, 20);

        lblPreviaAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPreviaAte.setText(bundle1.getString("KpiParametrosSistema_00022")); // NOI18N
        lblPreviaAte.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlIndicadores.add(lblPreviaAte);
        lblPreviaAte.setBounds(230, 100, 40, 20);

        lblMetaDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMetaDe.setText(bundle1.getString("KpiParametrosSistema_00021")); // NOI18N
        lblMetaDe.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlIndicadores.add(lblMetaDe);
        lblMetaDe.setBounds(70, 60, 130, 20);

        chkBlqDiaLimite.setText(bundle.getString("KpiRestricaoPlanValor_00003")); // NOI18N
        chkBlqDiaLimite.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkBlqDiaLimite.setEnabled(false);
        chkBlqDiaLimite.setMaximumSize(new java.awt.Dimension(189, 23));
        chkBlqDiaLimite.setMinimumSize(new java.awt.Dimension(189, 23));
        chkBlqDiaLimite.setOpaque(false);
        chkBlqDiaLimite.setPreferredSize(new java.awt.Dimension(189, 23));
        chkBlqDiaLimite.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkBlqDiaLimiteItemStateChanged(evt);
            }
        });
        pnlIndicadores.add(chkBlqDiaLimite);
        chkBlqDiaLimite.setBounds(20, 150, 420, 22);

        pnlMsgMail.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("KpiRestricaoPlanValor_00004"))); // NOI18N
        pnlMsgMail.setEnabled(false);
        pnlMsgMail.setLayout(new java.awt.BorderLayout());

        txtMsgMail.setEnabled(false);
        txtMsgMail.setMaxLength(400);
        jScrollPane1.setViewportView(txtMsgMail);

        pnlMsgMail.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pnlIndicadores.add(pnlMsgMail);
        pnlMsgMail.setBounds(20, 190, 600, 150);
        pnlMsgMail.getAccessibleContext().setAccessibleName("");

        tapCadastro.addTab(bundle1.getString("KpiParametrosSistema_00007"), pnlIndicadores); // NOI18N

        pnlExcecao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlExcecao.setLayout(new java.awt.BorderLayout());

        tapExecoes.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);

        pnlUsuario.setLayout(new java.awt.BorderLayout());
        pnlUsuario.add(tblExcecaoUser, java.awt.BorderLayout.CENTER);

        tapExecoes.addTab(bundle1.getString("KpiParametrosSistema_00017"), new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_usuario.gif")), pnlUsuario); // NOI18N

        pnlDepto.setLayout(new java.awt.BorderLayout());
        pnlDepto.add(tblExcecaoDpto, java.awt.BorderLayout.CENTER);

        tapExecoes.addTab(bundle1.getString("KpiParametrosSistema_00016"), new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_scorecard.gif")), pnlDepto); // NOI18N

        pnlExcecao.add(tapExecoes, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle1.getString("KpiParametrosSistema_00009"), pnlExcecao); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnSave.setText(bundle2.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle2.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle2.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle2.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle2.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            showMsgDiaLimite = false;
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            showMsgDiaLimite = false;
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
//		event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void chkBlqDiaLimiteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkBlqDiaLimiteItemStateChanged
        if (showMsgDiaLimite && chkBlqDiaLimite.isSelected()) {
            
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {

                    String text = java.util.ResourceBundle.getBundle("international").getString("KpiRestricaoPlanValor_00001");
                    JOptionPane.showMessageDialog(null,text,java.util.ResourceBundle.getBundle("international").getString("KpiParametrosSistema_00001"),JOptionPane.WARNING_MESSAGE);

                }
            });
        }

        txtMsgMail.setEnabled(chkBlqDiaLimite.isSelected() && showMsgDiaLimite);
        
    }//GEN-LAST:event_chkBlqDiaLimiteItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JCheckBox chkBlqDiaLimite;
    private pv.jfcx.JPVDatePlus fldDataMetaAte;
    private pv.jfcx.JPVDatePlus fldDataMetaDe;
    private pv.jfcx.JPVDatePlus fldDataPreviaAte;
    private pv.jfcx.JPVDatePlus fldDataPreviaDe;
    private pv.jfcx.JPVDatePlus fldDataRealAte;
    private pv.jfcx.JPVDatePlus fldDataRealDe;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblMeta;
    private javax.swing.JLabel lblMetaAte;
    private javax.swing.JLabel lblMetaDe;
    private javax.swing.JLabel lblPrevia;
    private javax.swing.JLabel lblPreviaAte;
    private javax.swing.JLabel lblPreviaDe;
    private javax.swing.JLabel lblReal;
    private javax.swing.JLabel lblRealAte;
    private javax.swing.JLabel lblRealDe;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlDepto;
    private javax.swing.JPanel pnlExcecao;
    private javax.swing.JPanel pnlIndicadores;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlMsgMail;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlUsuario;
    private javax.swing.JScrollPane scrDataMetaAte;
    private javax.swing.JScrollPane scrDataMetaDe;
    private javax.swing.JScrollPane scrDataPreviaAte;
    private javax.swing.JScrollPane scrDataPreviaDe;
    private javax.swing.JScrollPane scrDataRealAte;
    private javax.swing.JScrollPane scrDataRealDe;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JTabbedPane tapExecoes;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBIXMLTable tblExcecaoDpto;
    private kpi.beans.JBIXMLTable tblExcecaoUser;
    private kpi.beans.JBITextArea txtMsgMail;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;
    private boolean showMsgDiaLimite = false;

    public KpiRestricaoPlanValor(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
        btnDelete.setVisible(false);
    }

    @Override
    public void setStatus(int value) {
        status = value;
        
        showMsgDiaLimite = (status == KpiDefaultFrameBehavior.UPDATING);
        
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    private void applyRenderParametro(kpi.beans.JBIXMLTable oTable) {
        String colName = null;
        pv.jfcx.PVTableRenderer oRender = new pv.jfcx.PVTableRenderer(pv.jfcx.JPVTable.DATE_PLUS);
        pv.jfcx.PVTableRenderer oRenderReal = new pv.jfcx.PVTableRenderer(pv.jfcx.JPVTable.DATE_PLUS);
        pv.jfcx.PVTableRenderer oRenderMeta = new pv.jfcx.PVTableRenderer(pv.jfcx.JPVTable.DATE_PLUS);
        pv.jfcx.PVTableRenderer oRenderPrevia = new pv.jfcx.PVTableRenderer(pv.jfcx.JPVTable.DATE_PLUS);

        /**
         * Configurando as propriedades das colunas, e renderer.
         *
         */
        for (int col = 0; col < oTable.getTable().getColumnCount(); col++) {
            colName = oTable.getColumnTag(col);
            if (!(colName.startsWith("USUARIO") || colName.startsWith("SCORECARD"))) {
                pv.jfcx.JPVDatePlus jpvDate = new pv.jfcx.JPVDatePlus();
                jpvDate.setAllowNull(true);
                jpvDate.setDialog(true);
                jpvDate.setUseLocale(true);
                jpvDate.setWaitForCalendarDate(true);
                oTable.getColumn(col).setCellEditor(new pv.jfcx.PVTableEditor(jpvDate, pv.jfcx.JPVTable.DATE_PLUS));
                oTable.setRowHeight(19);

                if (col == 1 || col == 2) {	     //Real
                    oRenderReal.setBackground(new java.awt.Color(234, 255, 255));
                    oTable.getColumn(col).setCellRenderer(oRenderReal);
                } else if (col == 3 || col == 4) { //Meta
                    oRenderMeta.setBackground(new java.awt.Color(251, 255, 240));
                    oTable.getColumn(col).setCellRenderer(oRenderMeta);
                } else if (col == 5 || col == 6) { //Previa
                    oRenderPrevia.setBackground(new java.awt.Color(255, 230, 179));
                    oTable.getColumn(col).setCellRenderer(oRenderPrevia);
                } else {
                    oRender.setBackground(java.awt.Color.WHITE);
                    oTable.getColumn(col).setCellRenderer(oRender);
                }

            } else {
                oTable.getColumn(col).setPreferredWidth(175);

            }

        }
    }
}
