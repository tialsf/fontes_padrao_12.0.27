package kpi.swing.tools;

import javax.swing.JOptionPane;


public class KpiSMTPConfFrame extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String recordType = new String("SMTPCONF");
	private String formType = recordType;
	private String parentType = recordType;
	
	public void enableFields() {
		// Habilita os campos do formul�rio.
		lblConta.setEnabled(true);
		fldConta.setEnabled(true);
		lblSMTP.setEnabled(true);
		fldSMTP.setEnabled(true);
		lblPortaSMTP.setEnabled(true);
		fldPortaSMTP.setEnabled(true);
		lblUsuario.setEnabled(true);
		fldUsuario.setEnabled(true);
		lblProtocol.setEnabled(true);
		fldSenha.setEnabled(true);
                lblSenha1.setEnabled(true);
                lblProtocol.setEnabled(true);
                cboProtocol.setEnabled(true);
	}
	
	public void disableFields() {
		// Desabilita os campos do formul�rio.
		lblConta.setEnabled(false);
		fldConta.setEnabled(false);
		lblSMTP.setEnabled(false);
		fldSMTP.setEnabled(false);
		lblPortaSMTP.setEnabled(false);
		fldPortaSMTP.setEnabled(false);
		lblUsuario.setEnabled(false);
		fldUsuario.setEnabled(false);
		lblProtocol.setEnabled(false);
		fldSenha.setEnabled(false);
                lblSenha1.setEnabled(false);
                lblProtocol.setEnabled(false);
                cboProtocol.setEnabled(false);
	}
	
	java.util.Hashtable htbResponsavel = new java.util.Hashtable();
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		fldConta.setText(record.getString("NOME") );
		fldPortaSMTP.setText(record.getString("PORTA") );
		fldSMTP.setText(record.getString("SERVIDOR"));
		fldUsuario.setText(record.getString("USUARIO"));
		fldSenha.setText(record.getString("SENHA"));
		cboProtocol.setSelectedIndex(record.getInt("PROTOCOLO"));
	}
	
	public kpi.xml.BIXMLRecord getFieldsContents( ) {
		kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
		recordAux.set( "ID", id );
		if ( record.contains("PARENTID") )
			recordAux.set( "PARENTID", record.getString("PARENTID") );
		if ( record.contains("CONTEXTID") )
			recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );
		
		// Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
		// como retorno da fun��o.
		recordAux.set( "NOME", fldConta.getText() );
		recordAux.set( "SERVIDOR", fldSMTP.getText() );
		recordAux.set( "PORTA", fldPortaSMTP.getText() );
		recordAux.set( "USUARIO", fldUsuario.getText() );
		recordAux.set( "SENHA", fldSenha.getText() );
                // 0 - Nenhum | 1 - TLS | 2 - SSL
		recordAux.set( "PROTOCOLO", cboProtocol.getSelectedIndex() );
		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		boolean retorno = true;
		if(	fldConta.getText().trim().length() == 0 ){
			errorMessage.append( java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KPISMTPConfFrame_00006"));
			retorno = false;
		}
		
		if(	fldSMTP.getText().trim().length() == 0 ){
			errorMessage.append(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KPISMTPConfFrame_00007"));
			retorno = false;
		}
		
		return retorno;
	}
	
	public boolean hasCombos() {
		return false;
	}
	
	public void populateCombos(kpi.xml.BIXMLRecord serverSMTP) {
		return;
	}
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblConta = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        lblSMTP = new javax.swing.JLabel();
        lblPortaSMTP = new javax.swing.JLabel();
        fldConta = new pv.jfcx.JPVEdit();
        lblProtocol = new javax.swing.JLabel();
        fldSMTP = new pv.jfcx.JPVEdit();
        fldPortaSMTP = new pv.jfcx.JPVEdit();
        fldUsuario = new pv.jfcx.JPVEdit();
        fldSenha = new pv.jfcx.JPVPassword();
        cboProtocol = new javax.swing.JComboBox();
        lblSenha1 = new javax.swing.JLabel();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiParametrosSistema_00050")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_emailconfig.gif"))); // NOI18N
        setName(""); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 105, 132));
        setPreferredSize(new java.awt.Dimension(458, 285));

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 30));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 30));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new java.awt.Dimension(173, 30));
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        tapCadastro.setMinimumSize(new java.awt.Dimension(100, 100));
        tapCadastro.setPreferredSize(new java.awt.Dimension(530, 300));

        pnlFields.setPreferredSize(new java.awt.Dimension(400, 350));
        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(500, 400));

        pnlData.setPreferredSize(new java.awt.Dimension(100, 100));
        pnlData.setLayout(null);

        lblConta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblConta.setText(bundle.getString("KPISMTPConfFrame_00001")); // NOI18N
        lblConta.setEnabled(false);
        pnlData.add(lblConta);
        lblConta.setBounds(10, 10, 90, 20);

        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUsuario.setText(bundle.getString("KPISMTPConfFrame_00004")); // NOI18N
        lblUsuario.setEnabled(false);
        pnlData.add(lblUsuario);
        lblUsuario.setBounds(10, 130, 90, 20);

        lblSMTP.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSMTP.setText(bundle.getString("KPISMTPConfFrame_00002")); // NOI18N
        lblSMTP.setEnabled(false);
        pnlData.add(lblSMTP);
        lblSMTP.setBounds(10, 50, 90, 20);

        lblPortaSMTP.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPortaSMTP.setText(bundle.getString("KPISMTPConfFrame_00003")); // NOI18N
        lblPortaSMTP.setEnabled(false);
        pnlData.add(lblPortaSMTP);
        lblPortaSMTP.setBounds(10, 90, 90, 20);

        fldConta.setMaxLength(60);
        pnlData.add(fldConta);
        fldConta.setBounds(10, 30, 400, 22);

        lblProtocol.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblProtocol.setText("Protocolo de Seguran�a:");
        lblProtocol.setEnabled(false);
        pnlData.add(lblProtocol);
        lblProtocol.setBounds(10, 170, 120, 20);

        fldSMTP.setMaxLength(60);
        pnlData.add(fldSMTP);
        fldSMTP.setBounds(10, 70, 400, 22);

        fldPortaSMTP.setMaxLength(60);
        pnlData.add(fldPortaSMTP);
        fldPortaSMTP.setBounds(10, 110, 400, 22);

        fldUsuario.setMaxLength(60);
        pnlData.add(fldUsuario);
        fldUsuario.setBounds(10, 150, 180, 22);

        fldSenha.setCase(0);
        pnlData.add(fldSenha);
        fldSenha.setBounds(230, 150, 180, 22);

        cboProtocol.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nenhum", "TLS", "SSL" }));
        cboProtocol.setEnabled(false);
        pnlData.add(cboProtocol);
        cboProtocol.setBounds(10, 190, 180, 20);

        lblSenha1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSenha1.setText(bundle.getString("KPISMTPConfFrame_00005")); // NOI18N
        lblSenha1.setEnabled(false);
        pnlData.add(lblSenha1);
        lblSenha1.setBounds(230, 130, 90, 20);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KPISMTPConfFrame_00008"), pnlFields); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        tapCadastro.getAccessibleContext().setAccessibleName("Mensagens");
        tapCadastro.getAccessibleContext().setAccessibleDescription("Mensagens");

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents
	
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
	event.saveRecord( );
    }//GEN-LAST:event_btnSaveActionPerformed
	
    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
		event.cancelOperation();
    }//GEN-LAST:event_btnCancelActionPerformed
	
    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
		event.editRecord();
    }//GEN-LAST:event_btnEditActionPerformed
	
    private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
		event.loadRecord( );
    }//GEN-LAST:event_btnReloadActionPerformed
	
	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
		event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cboProtocol;
    private pv.jfcx.JPVEdit fldConta;
    private pv.jfcx.JPVEdit fldPortaSMTP;
    private pv.jfcx.JPVEdit fldSMTP;
    private pv.jfcx.JPVPassword fldSenha;
    private pv.jfcx.JPVEdit fldUsuario;
    private javax.swing.JLabel lblConta;
    private javax.swing.JLabel lblPortaSMTP;
    private javax.swing.JLabel lblProtocol;
    private javax.swing.JLabel lblSMTP;
    private javax.swing.JLabel lblSenha1;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
	
	private kpi.swing.KpiDefaultFrameBehavior event =
			new kpi.swing.KpiDefaultFrameBehavior( this );
	private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private kpi.xml.BIXMLRecord record = null;
	
	public KpiSMTPConfFrame(int operation, String idAux, String contextId) {
		initComponents();
                putClientProperty("MAXI", true);
		event.defaultConstructor( operation, idAux, contextId );
                fldSenha.setMaxLength(15);
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		recordType = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( recordType );
	}
	
	public void setID( String value ) {
		id = String.valueOf(value);
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( kpi.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public kpi.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlConfirmation.setVisible(true);
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlConfirmation.setVisible(false);
		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}
	
	public String getParentType() {
		return parentType;
	}
	
	public String getFormType() {
		return formType;
	}
	
}