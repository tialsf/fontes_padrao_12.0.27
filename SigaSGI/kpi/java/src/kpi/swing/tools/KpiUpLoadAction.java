/*
 * KpiUpLoadListener.java
 *
 * Created on September 14, 2007, 11:05 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package kpi.swing.tools;

/**
 *
 * @author lucio.pelinson
 */
public interface KpiUpLoadAction {
	

	/**
	 * Invoked when the component has beeen started.
	 */
	public void onUpLoadStart(java.awt.event.ComponentEvent componentEvent);
	
	/**
	 * Invoked when the component has beeen finished.
	 */
	public void onUpLoadFinish(java.awt.event.ComponentEvent componentEvent);
	
	
	/**
	 * Invoked when ADVPL return.
	 */
	public void onAfterExecute(java.awt.event.ComponentEvent componentEvent);
	
	/**
	 * Invoked when ocurre an error.
	 */
	public void onUpLoadError(java.awt.event.ComponentEvent componentEvent);
	
	/**
	 * Invoked when ocurre cancel.
	 */
	public void onUpLoadCancel(java.awt.event.ComponentEvent componentEvent);
	
	
	
	/**
	 * Invoked when is uploading
	 */
	public void onUpLoading(java.awt.event.ComponentEvent componentEvent);
	
}
