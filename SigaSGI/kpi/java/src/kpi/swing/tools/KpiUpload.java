/*
 * KpiUpload.java
 *
 * Created on 22 de Novembro de 2006, 17:53
 * SIGA 2516 - Lucio Pelinson
 *
 */

package kpi.swing.tools;

import java.io.File;

public class KpiUpload extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions{
	
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String recordType	= new String("KPIUPLOAD");
	private String formType		= recordType;//Tipo do Formulario
	private String parentType	= recordType;//Tipo formulario pai, caso haja.
	private static final String pathUpload	= "\\sgiupload\\";
	public static boolean cancel = false;
        private boolean moverParaDestino = true;
        private String subpastaUpLoad = new String("");
	private String pathDest			= new String("");
	private String nameAfterUpload	= new String("");
	public static File oFile;
        public static File oFiles[];
	private java.util.Vector vctUpLoad;
	
	public void setMoverParaDestino(boolean mover){
            this.moverParaDestino = mover;    
        }
	
	public void setSubPastaUPL(String subpasta){
            if(!subpasta.equals(null)){
                this.subpastaUpLoad = subpasta;
            }
        }
        
        public void moveArqsDestino(){
            String comando = "";
            kpi.core.KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();
            try {
                    comando = "MOVEFILES|";
                    comando = comando.concat(pathUpload);
                    comando = comando.concat(subpastaUpLoad);
                    comando = comando.concat("|");
                    comando = comando.concat(getPathDest());
                    dataController.executeRecord(this.getID(),"KPIUPLOAD",comando);
            } catch (Exception e) {
                    System.out.println(e);
            }
        }
        
        public void removeArquivos(){
            removeArquivos("*.*");
        }
        
        public void removeArquivos(String arquivo){
            String caminho = new String("");
            
            caminho = pathUpload + subpastaUpLoad + arquivo;
            kpi.util.KpiToolKit	tool = new kpi.util.KpiToolKit();
            tool.eraseServerFiles(this.getID(),"KPIUPLOAD",caminho);
        }
        
	public void setParentType(String parentType) {
		this.parentType = parentType;
	}
	
	public String getParentType() {
		return parentType;
	}
	
	public String getPathDest() {
		return pathDest;
	}
	
	public void setPathDest(String pathDest) {
		this.pathDest = pathDest;
	}
	
	public void setCloseBoxAfterUploaded(boolean close){
		chkCloseWindow.setSelected(close);
	}
	
	public void enableFields() {}
	
	public void disableFields() {}
	
	public void refreshFields() {
		cancel = false;
	}
	
	public kpi.xml.BIXMLRecord getFieldsContents( ) {
		//kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
		
		return null;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}
	
	
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        pnlCenter = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        lblArquivo = new javax.swing.JLabel();
        lblTamanho = new javax.swing.JLabel();
        chkCloseWindow = new javax.swing.JCheckBox();
        barStatus = new javax.swing.JProgressBar();
        jTexturePanel1 = new kpi.beans.JTexturePanel();
        jTexturePanel2 = new kpi.beans.JTexturePanel();
        jPanel1 = new javax.swing.JPanel();
        lblStatusUpload = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setTitle(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00001"));
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_upload.gif")));
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(410, 240));
        pnlCenter.setLayout(null);

        pnlCenter.setPreferredSize(new java.awt.Dimension(392, 162));
        btnCancelar.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00005"));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        pnlCenter.add(btnCancelar);
        btnCancelar.setBounds(290, 170, 90, 20);

        lblArquivo.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00003"));
        pnlCenter.add(lblArquivo);
        lblArquivo.setBounds(20, 100, 370, 14);

        lblTamanho.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00004"));
        pnlCenter.add(lblTamanho);
        lblTamanho.setBounds(20, 120, 370, 14);

        chkCloseWindow.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00002"));
        chkCloseWindow.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkCloseWindow.setMargin(new java.awt.Insets(0, 0, 0, 0));
        pnlCenter.add(chkCloseWindow);
        chkCloseWindow.setBounds(20, 140, 360, 15);

        barStatus.setOpaque(true);
        pnlCenter.add(barStatus);
        barStatus.setBounds(103, 50, 200, 12);

        jTexturePanel1.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_gray_desktop.gif")));
        pnlCenter.add(jTexturePanel1);
        jTexturePanel1.setBounds(10, 10, 83, 80);

        jTexturePanel2.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_gray_world.gif")));
        pnlCenter.add(jTexturePanel2);
        jTexturePanel2.setBounds(310, 13, 80, 70);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlCenter.add(jPanel1);
        jPanel1.setBounds(10, 95, 380, 1);

        lblStatusUpload.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblStatusUpload.setText("Aguarde ...");
        lblStatusUpload.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pnlCenter.add(lblStatusUpload);
        lblStatusUpload.setBounds(110, 33, 180, 14);

        getContentPane().add(pnlCenter, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents
	
	private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
		cancel = true;
		if(btnCancelar.getText().equals(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00008"))){ //"Fechar"
			setVisible( false );
			dispose();
		}
		
	}//GEN-LAST:event_btnCancelarActionPerformed
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barStatus;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JCheckBox chkCloseWindow;
    private javax.swing.JPanel jPanel1;
    private kpi.beans.JTexturePanel jTexturePanel1;
    private kpi.beans.JTexturePanel jTexturePanel2;
    private javax.swing.JLabel lblArquivo;
    private javax.swing.JLabel lblStatusUpload;
    private javax.swing.JLabel lblTamanho;
    private javax.swing.JPanel pnlCenter;
    // End of variables declaration//GEN-END:variables
	
	private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior( this );
	private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private kpi.xml.BIXMLRecord record = null;
	
	public KpiUpload(int operation, String idAux, String contextId) {
		vctUpLoad = new java.util.Vector();
		initComponents();
		
		event.defaultConstructor( operation, idAux, contextId );
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		recordType = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( recordType );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( kpi.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public kpi.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {}
	
	public void showOperationsButtons() {}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public String getFormType(){
		return formType;
	}
	
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return new javax.swing.JTabbedPane();
	}
	
	
	public void setTilePercent(int percent){
		this.setTitle( String.valueOf(percent).concat(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00007")) ); //"%  SGI - Upload conclu�do"
	}
	
        public void startUpload(File theFile){
            File arqs[] = {theFile};
            startUpload(arqs);
        }
        
	public void startUpload(File[] theFiles){
		oFiles = theFiles;
                
		final kpi.core.SwingWorker worker = new kpi.core.SwingWorker() {
			public Object construct() {
				//Variaveis
				kpi.util.KpiToolKit	tool = new kpi.util.KpiToolKit();
				long length = 0;
				byte[] bytes;
                                String cNomeArq = new String("");
                                int arqs = 0;
                                
                                for(arqs = 0;arqs<oFiles.length;arqs++){
                                    oFile = oFiles[arqs];
                                    length = oFile.length();
                                    bytes = new byte[(int)length];
                                    cNomeArq = oFile.getPath().substring( (oFile.getPath().lastIndexOf("\\") + 1),oFile.getPath().length() );
                                    String  cTamArqKb = String.valueOf((int)((float)  oFile.length()/1024 )).concat(" Kb");
                                    setCloseEnabled(false);
                                    barStatus.setVisible(true);
                                    lblArquivo.setText( java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00003").concat(cNomeArq) ); //"Fazendo o upload de: "
                                    lblTamanho.setText( java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00004").concat(cTamArqKb) );//"Tamanho do arquivo: "
                                    lblStatusUpload.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00009")); //"Aguarde ..."
				
                                    if(!moverParaDestino){
                                        cNomeArq = subpastaUpLoad.concat(oFile.getPath().substring((oFile.getPath().lastIndexOf("\\") + 1), oFile.getPath().length()));
                                    }else{
                                        cNomeArq = oFile.getPath().substring((oFile.getPath().lastIndexOf("\\") + 1), oFile.getPath().length());
                                    }

                                    //Se o nome no arquivo n�o for informado usa o atual.
                                    String cPathArq;
                                    if(getArqName().length() > 0){
                                            cPathArq	= pathUpload.concat(getArqName());
                                    }else{
                                            cPathArq	= pathUpload.concat(cNomeArq);

                                    }

                                    //Verifica o tamanho do arquivo''
                                    try {
                                            bytes = tool.getBytesFromFile(oFile);
                                    } catch (java.io.IOException ex) {
                                            onUpLoadErrorAction();
                                            System.out.println(ex);
                                            setCloseEnabled(true);
                                    }


                                    //Enviando dados
                                    int dataSend = 0,
                                            tamArray = 0,
                                            transfer = 102400, //100kb
                                            percConc = 0,
                                            i = 0,
                                            j = 0;

                                    onUpLoadStartAction(); 

                                    byte[] bytesSend = new byte[transfer];
                                    barStatus.setIndeterminate(true);
                                    while(dataSend < bytes.length){
                                            onUpLoadingAction();
                                            if( (dataSend + transfer) <= bytes.length ){
                                                    tamArray = transfer;
                                            }else{
                                                    tamArray = bytes.length - dataSend;
                                                    bytesSend = new byte[tamArray];
                                            }


                                            j = 0;
                                            for(i=dataSend;i<(dataSend+tamArray);i++){
                                                    bytesSend[j] = bytes[i];
                                                    j += 1;
                                            }
                                            dataSend += tamArray;
                                            tool.writeServerFile(bytesSend, cPathArq, false, false);

                                            //delay para nao sobrecarregar o servidor
                                            try {
                                                    Thread.sleep(400);
                                            } catch ( InterruptedException x ) {
                                                    onUpLoadErrorAction();
                                                    System.out.println("Erro ao efetuar o upload do arquivo");
                                                    event.executeRecord("DELFILE|".concat(cPathArq));
                                                    Thread.currentThread().interrupt();
                                                    setCloseEnabled(true);
                                            }

                                            if(dataSend > 0){
                                                    percConc =  (int)((float)dataSend / bytes.length *100);
                                                    setTilePercent(percConc);
                                            }

                                            //Se o usuario cancelar a a��o
                                            if(cancel){
                                                    onUpLoadCancelAction();
                                                    setTitle(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00006")); //"Cancelando Upload"
                                                    lblStatusUpload.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00010")); //"Cancelando ..."
                                                    barStatus.setIndeterminate(false);
                                                    event.executeRecord("DELFILE|".concat(cPathArq));
                                                    setVisible( false );
                                                    dispose();
                                                    return null;
                                            }
                                    }

                                    //Movendo arquivo para pasta de destino
                                    if(moverParaDestino){
                                        StringBuilder request = new StringBuilder();
                                        request.append("MOVEFILE");
                                        request.append("|");
                                        request.append(cPathArq);
                                        request.append("|");
                                        request.append(getPathDest());
                                        event.executeRecord(request.toString());
                                    }
                                }
                                barStatus.setIndeterminate(false);
                                event.refreshParent();
                                setCloseEnabled(true);
                                return null;
			}
			
			public void finished() {
				barStatus.setVisible(false);
                                lblArquivo.setText(" ");
                                lblTamanho.setText(" ");
 				lblStatusUpload.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00011")); //"Upload conclu�do"
				btnCancelar.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUpload_00008")); //"Fechar"
				chkCloseWindow.setVisible(false);
                                
				onUpLoadFinishAction();

				if(chkCloseWindow.isSelected()){
					setVisible( false );
					dispose();
				}
			}
		};
		worker.start();
		
	}
	
	//Retorno do ADVPL
	public void afterExecute() {
		onAfterExecuteAction();
	}
	
	
	public void setArqName(String name) {
		this.nameAfterUpload = name;
	}
	
	public String getArqName() {
		return nameAfterUpload;
	}
	
	
	public void addUpLoadAction(kpi.swing.tools.KpiUpLoadAction action){
		vctUpLoad.add(action);
	}
	
	public void removeUpLoadAction(kpi.swing.tools.KpiUpLoadAction action){
		vctUpLoad.remove(action);
	}
	


	public void onUpLoadStartAction() {
		for(int j=0; j<vctUpLoad.size(); j++){
			((kpi.swing.tools.KpiUpLoadAction) vctUpLoad.get(j)).onUpLoadStart(new java.awt.event.ComponentEvent(this, 0));
		}
	}

	public void onUpLoadFinishAction() {
		for(int j=0; j<vctUpLoad.size(); j++){
			((kpi.swing.tools.KpiUpLoadAction) vctUpLoad.get(j)).onUpLoadFinish(new java.awt.event.ComponentEvent(this, 0));
		}
	}
	
	public void onAfterExecuteAction() {
		for(int j=0; j<vctUpLoad.size(); j++){
			((kpi.swing.tools.KpiUpLoadAction) vctUpLoad.get(j)).onAfterExecute(new java.awt.event.ComponentEvent(this, 0));
		}
	}

	public void onUpLoadErrorAction() {
		for(int j=0; j<vctUpLoad.size(); j++){
			((kpi.swing.tools.KpiUpLoadAction) vctUpLoad.get(j)).onUpLoadError(new java.awt.event.ComponentEvent(this, 0));
		}
	}

	public void onUpLoadCancelAction() {
		for(int j=0; j<vctUpLoad.size(); j++){
			((kpi.swing.tools.KpiUpLoadAction) vctUpLoad.get(j)).onUpLoadCancel(new java.awt.event.ComponentEvent(this, 0));
		}
	}

	public void onUpLoadingAction() {
		for(int j=0; j<vctUpLoad.size(); j++){
			((kpi.swing.tools.KpiUpLoadAction) vctUpLoad.get(j)).onUpLoading(new java.awt.event.ComponentEvent(this, 0));
		}
	}
	
}




