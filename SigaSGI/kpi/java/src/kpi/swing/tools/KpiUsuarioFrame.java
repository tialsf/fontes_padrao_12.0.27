package kpi.swing.tools;

import java.awt.Color;
import kpi.core.KpiFormController;
import kpi.swing.KpiDefaultFrameBehavior;
import javax.swing.tree.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.ImageIcon;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiSelectedTreeCellRender;
import kpi.swing.KpiSelectedTreeNode;
import kpi.swing.KpiTreeNode;
import kpi.xml.*;

public class KpiUsuarioFrame extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions, javax.swing.event.TreeSelectionListener, java.awt.event.ActionListener {

    private final int SEGURANCA = 1;
    private final int SCORECARD = 2;
    private final int PREFERENCIA = 3;
    //
    private String recordType = "USUARIO";
    private String formType = recordType;
    private String parentType = "DIRUSUARIOS";
    private String password = "";
    private int[] aViewType = {1, 1};
    private kpi.xml.BIXMLVector arvoreRetorno, vctSeguranca, vctScoDefault, vctEstrategia;
    private java.util.Hashtable htbScoreDefault = new java.util.Hashtable();
    private java.util.Hashtable htbEstrategia = new java.util.Hashtable();
    private java.util.Hashtable cacheSco = new java.util.Hashtable();
    private boolean isEditMode = false;
    private kpi.swing.kpiCreateTree buildTree = new kpi.swing.kpiCreateTree();
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiUsuarioFrame(int operation, String idAux, String contextAux) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextAux);
        prepareSearchField();
    }

    @Override
    public void enableFields() {
        isEditMode = true;

        event.setEnableFields(pnlData, record.getBoolean("USUA_ISADMIN"));
        event.setEnableFields(pnlPreferencia, true);
        event.setEnableFields(pnlScorecard, true);
        event.setEnableFields(pnlPermissoes, true);

        if (chkScoDefault.isSelected()) {
            lblSco.setEnabled(true);
            cboSco.setEnabled(true);
        } else {
            lblSco.setEnabled(false);
            cboSco.setEnabled(false);
        }
    }

    @Override
    public void disableFields() {
        isEditMode = false;

        event.setEnableFields(pnlData, false);
        event.setEnableFields(pnlPreferencia, false);
        event.setEnableFields(pnlScorecard, false);
        event.setEnableFields(pnlPermissoes, false);
    }

    @Override
    public void refreshFields() {
        cacheSco.clear();

        chkAdministrador.setSelected(record.getBoolean("ADMIN"));
        fldNome.setText(record.getString("NOME"));
        fldPrimNome.setText(record.getString("COMPNOME"));
        fldCargo.setText(record.getString("CARGO"));
        fldTelefone.setText(record.getString("FONE"));
        fldRamal.setText(record.getString("RAMAL"));
        fldEmail.setText(record.getString("EMAIL"));
        arvoreRetorno = record.getBIXMLVector("ARVORES_SEG");
        vctSeguranca = record.getBIXMLVector("REGRAS");
        radJanela.setSelected(record.getInt("DESKVIEWTYPE") == KpiFormController.ST_JANELA);
        radPasta.setSelected(record.getInt("DESKVIEWTYPE") != KpiFormController.ST_JANELA);
        aViewType[0] = record.getInt("DESKVIEWTYPE");
        aViewType[1] = record.getInt("CONFIGVIEWTYPE");

        if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
            tapCadastro.setEnabledAt(SEGURANCA, false);
            tapCadastro.setEnabledAt(SCORECARD, false);
            tapCadastro.setEnabledAt(PREFERENCIA, false);
            
            //--------------------------------------------------------------------------
            // Por padr�o para iniciar o sistema scorecarding � a op��o principal
            // Se scorecarding estiver selecionado, check box mapa estrat�gia n�o 
            // poder� esta e vice e versa.
            //--------------------------------------------------------------------------
            chkShowScorecarding.setSelected(true);
            if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
                chkShowMapaEstrategico.setSelected(false);
            } else {
                chkShowMapaEstrategico.setVisible(false);
                cboEstrategia.setVisible(false);
                tsEstrategia.setVisible(false);
            }
            chkAtivo.setSelected(true);
        } else {
            kpiTreeSeguranca.removeAll();
            kpiTreeSeguranca.setModel(new javax.swing.tree.DefaultTreeModel(buildTree.createTree(arvoreRetorno.get(0))));
            kpiTreeSeguranca.setCellRenderer(new kpi.swing.KpiTreeCellRenderer(kpi.core.KpiStaticReferences.getKpiImageResources()));
            kpiTreeSeguranca.addTreeSelectionListener(this);

            chkShowScorecarding.setSelected(record.getBoolean("SHOWSCORECARDING"));
            chkAtivo.setSelected(record.getBoolean("USERKPI"));
            chkAutoRefresh.setSelected(record.getBoolean("SCOAUTOREFRESH"));
            chkHideTree.setSelected(record.getBoolean("SCOHIDETREE"));

            boolean hasScorecardDefault = record.getBoolean("SCODEFAULT");
            chkScoDefault.setSelected(hasScorecardDefault);
            
            if (hasScorecardDefault) {
                populateScoDefault();
            }
            
            if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
                // Verifica se inicializar sistema com a tela de mapa estrat�gico .
                boolean hasChkEstratSelected = record.getBoolean("SHOWMAPAESTRATEGICO");
                chkShowMapaEstrategico.setSelected(hasChkEstratSelected);

                if (hasChkEstratSelected) {
                    populateEstrategia();
                }
            } else {
                chkShowMapaEstrategico.setVisible(false);
                cboEstrategia.setVisible(false);
                tsEstrategia.setVisible(false);
            }
        }

        cboVisualiza��o.setSelectedIndex(0);
        kpiTreeSco.setModel(new javax.swing.tree.DefaultTreeModel(createTree(record)));
        kpiTreeSco.setCellRenderer(new KpiSelectedTreeCellRender(kpi.core.KpiStaticReferences.getKpiImageResources()));
        kpiTreeSco.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        kpiTreeSco.putClientProperty("JTree.lineStyle", "Angled");

        configuraRegraUsuario();
    }

    private void populateScoDefault() {
        if (cboSco.getItemCount() == 0) {
            kpi.xml.BIXMLRecord recScoreCard = event.listRecords("SCORECARD", "LISTA_SCO_OWNER");
            vctScoDefault = recScoreCard.getBIXMLVector("SCORECARDS");
            event.populateCombo(vctScoDefault, "SCORECARD", htbScoreDefault, cboSco);
            tsArvore.setVetor(vctScoDefault);
        }

        int retItem = event.getComboItem(vctScoDefault, record.getString("IDSCODEFAULT"), true);

        if (retItem != -1 && cboSco.getItemCount() > 0) {
            cboSco.setSelectedIndex(retItem);
        }
    }
    
    /**
     * Preenche o check box de estrat�gias (mapa estrat�gico)
     * @Author Helio Leal
     * @since 03/02/2016
     */
    private void populateEstrategia() {
        if (cboEstrategia.getItemCount() == 0) {
            kpi.xml.BIXMLRecord recScoreCard = event.listRecords("SCORECARD", "LISTA_SCO_OWNER");
            vctEstrategia = recScoreCard.getBIXMLVector("SCORECARDS");
            event.populateCombo(vctEstrategia, "SCORECARD", htbEstrategia, cboEstrategia);
            tsEstrategia.setVetor(vctEstrategia);
            //----------------------------------------------------------------
            // Seleciona apenas estrat�gia
            //----------------------------------------------------------------
            tsEstrategia.setEntitySelection(KpiStaticReferences.CAD_ESTRATEGIA);
        }

        int retItem = event.getComboItem(vctEstrategia, record.getString("IDESTRATEGIA"), true);

        if (retItem != -1 && cboEstrategia.getItemCount() > 0) {
            cboEstrategia.setSelectedIndex(retItem);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);

        if (record.contains("AUTENTIC")) {
            recordAux.set("AUTENTIC", record.getString("AUTENTIC"));
        } else {
            recordAux.set("AUTENTIC", "0");
        }

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("ADMIN", chkAdministrador.isSelected());
        recordAux.set("ID", id);
        recordAux.set("SENHA", password);
        recordAux.set("COMPNOME", fldPrimNome.getText());
        recordAux.set("CARGO", fldCargo.getText());
        recordAux.set("FONE", fldTelefone.getText());
        recordAux.set("RAMAL", fldRamal.getText());
        recordAux.set("EMAIL", fldEmail.getText());
        recordAux.set("USERKPI", chkAtivo.isSelected());
        recordAux.set("DESKVIEWTYPE", aViewType[0]);
        recordAux.set("CONFIGVIEWTYPE", aViewType[1]);
        recordAux.set("SHOWSCORECARDING", chkShowScorecarding.isSelected());        
        
        recordAux.set("SHOWMAPAESTRATEGICO", chkShowMapaEstrategico.isSelected());
        recordAux.set("IDESTRATEGIA", event.getComboValue(htbEstrategia, cboEstrategia));
                
        recordAux.set("SCOAUTOREFRESH", chkAutoRefresh.isSelected());
        recordAux.set("SCOHIDETREE", chkHideTree.isSelected());
        recordAux.set("SCODEFAULT", chkScoDefault.isSelected());
        recordAux.set("IDSCODEFAULT", event.getComboValue(htbScoreDefault, cboSco));
        recordAux.set(vctSeguranca);

        StringBuilder cmdSavePermissao = new StringBuilder("");

        for (Enumeration e = cacheSco.elements(); e.hasMoreElements();) {
            cmdSavePermissao.append("SAVE_PERMISSAO|");
            clsCacheSco oCache = (clsCacheSco) e.nextElement();
            kpi.xml.BIXMLVector vctGranted = oCache.vecXml;
            cmdSavePermissao.append(oCache.sSco.concat("|"));//Id do ScoreCard
            cmdSavePermissao.append(id.concat("|"));//Id do usu�rio.

            for (int item = 0; vctGranted.size() > item; item++) {
                cmdSavePermissao.append(vctGranted.get(item).getString("ID").concat("|"));
            }

            if (e.hasMoreElements()) {
                cmdSavePermissao.append(";");
            }
        }
        recordAux.set("PERMISSAO", cmdSavePermissao.toString());
        recordAux.set(getXMLScoTree());

        return recordAux;
    }

    @Override
    public void valueChanged(javax.swing.event.TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) kpiTreeSeguranca.getLastSelectedPathComponent();
        javax.swing.JCheckBox chkOption;

        if (node != null) {
            // Remove chkboxes
            pnlSeguranca.removeAll();

            Object userObject = node.getUserObject();
            if (userObject instanceof kpi.swing.KpiTreeNode) {
                //Item clicado na arvore
                kpi.xml.BIXMLRecord nodeClicked = (((kpi.swing.KpiTreeNode) userObject).getRecord());
                //Item correspondente no vetor de segura�a.
                if (nodeClicked != null) {
                    kpi.xml.BIXMLRecord nodeTemp = vctSeguranca.get(nodeClicked.getAttributes().getInt("ID") - 1);
                    //vetor com os itens de seguran�a.
                    kpi.xml.BIXMLVector vctItens = nodeTemp.getBIXMLVector("ITEMREGRA");

                    javax.swing.JLabel titulo = new javax.swing.JLabel(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00016"));
                    titulo.setFont(new java.awt.Font("Tahoma", 1, 11));
                    pnlSeguranca.add(titulo);
                    for (int iTem = 0; iTem < vctItens.size(); iTem++) {
                        try {
                            String nome = getRegraByID(vctItens.get(iTem).getAttributes().getInt("REGRAID"));
                            boolean valor = getRegraValor(vctItens.get(iTem).getAttributes().getInt("VALOR"));

                            chkOption = new javax.swing.JCheckBox(nome, valor);
                            chkOption.setOpaque(true);
                            chkOption.addActionListener(this);
                            chkOption.putClientProperty("REGRA_ID", new Integer(iTem));
                            pnlSeguranca.add(chkOption);
                        } catch (Exception ex) {
                            break;
                        }
                    }
                }
            }

            pnlSeguranca.validate();
            pnlSeguranca.repaint();
        }
    }

    public void setPassword(String passwd) {
        password = String.valueOf(passwd);
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean lRet = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00033"));
            lRet = false;
        } else if (fldPrimNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00023"));
            lRet = false;
        }
        return lRet;
    }

    protected BIXMLVector getXMLScoTree() {
        DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) kpiTreeSco.getModel().getRoot();
        BIXMLVector vctSco = new BIXMLVector("SCOS", "SCO");
        KpiSelectedTreeNode oNode;

        while (oMutTreeNode != null) {
            if (!oMutTreeNode.isRoot()) {
                oNode = (KpiSelectedTreeNode) oMutTreeNode.getUserObject();
                kpi.xml.BIXMLRecord oRec = new kpi.xml.BIXMLRecord("SCO");
                oRec.set("ID", oNode.getID());
                oRec.set("SELECTED", oNode.isSelected() && oNode.isEnabled());
                vctSco.add(oRec);
            }
            oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();
        }

        return vctSco;
    }

    /**
     * Inicia campos de pesquisa com valor padr�o.
     * @return void
     */
    private void initSearchField() {
        jTxtProcurar.setText("");
        jTxtProcurar.setForeground(Color.BLACK);
    }

    /**
     * Define as caracter�sticas do campo de pesquisa de acordo com o resultado da pesquisa.
     * @param isFound
     * @return void
     */
    private void setSearchField(boolean isFound) {
        if (isFound) {
            jTxtProcurar.setBackground(Color.WHITE);
        } else {
            jTxtProcurar.setBackground(new Color(253, 117, 127)/*Red*/);
        }
    }

    /**
     * Prepar  a os campos de pesquisa.
     * @param isVisibled
     * @return void
     */
    public void prepareSearchField() {
        jTxtProcurar.setText(ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("JBIListPanel_00004" /*Digite sua pesquisa*/));
        jTxtProcurar.setForeground(Color.LIGHT_GRAY);
        jTxtProcurar.setBackground(Color.WHITE);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpVizual = new javax.swing.ButtonGroup();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblPrimNome = new javax.swing.JLabel();
        lblCargo = new javax.swing.JLabel();
        lblTelefone = new javax.swing.JLabel();
        lblRamal = new javax.swing.JLabel();
        lblEmail = new javax.swing.JLabel();
        fldPrimNome = new pv.jfcx.JPVEdit();
        fldCargo = new pv.jfcx.JPVEdit();
        fldTelefone = new pv.jfcx.JPVMask();
        fldRamal = new pv.jfcx.JPVMask();
        fldEmail = new pv.jfcx.JPVEdit();
        fldNome = new pv.jfcx.JPVEdit();
        chkAtivo = new javax.swing.JCheckBox();
        chkAdministrador = new javax.swing.JCheckBox();
        pnlPermissoes = new javax.swing.JPanel();
        splitSeguranca = new javax.swing.JSplitPane();
        scrollTreeSeguranca = new javax.swing.JScrollPane();
        kpiTreeSeguranca = new javax.swing.JTree();
        pnlSeguranca = new javax.swing.JPanel();
        pnlScorecard = new javax.swing.JPanel();
        scrollTreeSco = new javax.swing.JScrollPane();
        kpiTreeSco = new javax.swing.JTree();
        pnlTreeScoFind = new javax.swing.JPanel();
        pnlTreeScoLabel = new javax.swing.JPanel();
        lblTreeSco = new javax.swing.JLabel();
        jLblProcurar = new javax.swing.JLabel();
        pnlEspace = new javax.swing.JPanel();
        jTxtProcurar = new javax.swing.JTextField();
        pnlPersonalizacao = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord1 = new javax.swing.JScrollPane();
        pnlPreferencia = new javax.swing.JPanel();
        lblVisualiza��o = new javax.swing.JLabel();
        radPasta = new javax.swing.JRadioButton();
        radJanela = new javax.swing.JRadioButton();
        String[] aDesktop = {java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00030"),java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00031")};
        cboVisualiza��o = new javax.swing.JComboBox(aDesktop);
        jList1 = new javax.swing.JList();
        chkAutoRefresh = new javax.swing.JCheckBox();
        chkHideTree = new javax.swing.JCheckBox();
        chkScoDefault = new javax.swing.JCheckBox();
        cboSco = new javax.swing.JComboBox();
        lblSco = new javax.swing.JLabel();
        tsArvore = new kpi.beans.JBITreeSelection();
        pnlInicio = new javax.swing.JPanel();
        chkShowScorecarding = new javax.swing.JCheckBox();
        chkShowMapaEstrategico = new javax.swing.JCheckBox();
        cboEstrategia = new javax.swing.JComboBox();
        tsEstrategia = new kpi.beans.JBITreeSelection();
        pnlTopRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnAlterarSenha = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda2 = new javax.swing.JButton();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiUsuarioFrame_00014")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_usuario.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 660, 395));
        setPreferredSize(new java.awt.Dimension(453, 383));

        tapCadastro.setPreferredSize(new java.awt.Dimension(510, 345));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        scpRecord.setBorder(null);
        scpRecord.setMaximumSize(new java.awt.Dimension(374, 276));
        scpRecord.setMinimumSize(new java.awt.Dimension(374, 276));
        scpRecord.setPreferredSize(new java.awt.Dimension(374, 276));

        pnlData.setPreferredSize(new java.awt.Dimension(355, 150));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblNome.setText(bundle1.getString("KpiUsuarioFrame_00010")); // NOI18N
        lblNome.setEnabled(false);
        pnlData.add(lblNome);
        lblNome.setBounds(20, 60, 52, 20);
        lblNome.getAccessibleContext().setAccessibleName("Usuario");

        lblPrimNome.setForeground(new java.awt.Color(51, 51, 255));
        lblPrimNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPrimNome.setText(bundle1.getString("KpiUsuarioFrame_00004")); // NOI18N
        lblPrimNome.setEnabled(false);
        lblPrimNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPrimNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPrimNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblPrimNome);
        lblPrimNome.setBounds(20, 100, 52, 20);

        lblCargo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCargo.setText(bundle1.getString("KpiUsuarioFrame_00005")); // NOI18N
        lblCargo.setEnabled(false);
        lblCargo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblCargo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblCargo.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblCargo);
        lblCargo.setBounds(20, 140, 52, 20);

        lblTelefone.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTelefone.setText(bundle1.getString("KpiUsuarioFrame_00006")); // NOI18N
        lblTelefone.setEnabled(false);
        lblTelefone.setMaximumSize(new java.awt.Dimension(100, 16));
        lblTelefone.setMinimumSize(new java.awt.Dimension(100, 16));
        lblTelefone.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblTelefone);
        lblTelefone.setBounds(20, 220, 52, 20);

        lblRamal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRamal.setText(bundle1.getString("KpiUsuarioFrame_00007")); // NOI18N
        lblRamal.setEnabled(false);
        lblRamal.setMaximumSize(new java.awt.Dimension(100, 16));
        lblRamal.setMinimumSize(new java.awt.Dimension(100, 16));
        lblRamal.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblRamal);
        lblRamal.setBounds(240, 220, 40, 20);

        lblEmail.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEmail.setText(bundle1.getString("KpiUsuarioFrame_00008")); // NOI18N
        lblEmail.setEnabled(false);
        lblEmail.setMaximumSize(new java.awt.Dimension(100, 16));
        lblEmail.setMinimumSize(new java.awt.Dimension(100, 16));
        lblEmail.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblEmail);
        lblEmail.setBounds(20, 180, 52, 20);

        fldPrimNome.setMaxLength(60);
        pnlData.add(fldPrimNome);
        fldPrimNome.setBounds(20, 120, 400, 22);

        fldCargo.setMaxLength(40);
        pnlData.add(fldCargo);
        fldCargo.setBounds(20, 160, 400, 22);

        fldTelefone.setMask("(###) #####-####");
        fldTelefone.setText("(   )       -    ");
        pnlData.add(fldTelefone);
        fldTelefone.setBounds(20, 240, 180, 22);

        fldRamal.setMask("##########");
        pnlData.add(fldRamal);
        fldRamal.setBounds(240, 240, 180, 22);

        fldEmail.setMaxLength(80);
        pnlData.add(fldEmail);
        fldEmail.setBounds(20, 200, 400, 22);

        fldNome.setMaxLength(25);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 80, 400, 22);

        chkAtivo.setSelected(true);
        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        chkAtivo.setText(bundle2.getString("KpiUsuarioFrame_00015")); // NOI18N
        chkAtivo.setEnabled(false);
        pnlData.add(chkAtivo);
        chkAtivo.setBounds(20, 10, 140, 20);

        chkAdministrador.setText(bundle2.getString("KpiUsuarioFrame_00003")); // NOI18N
        chkAdministrador.setEnabled(false);
        pnlData.add(chkAdministrador);
        chkAdministrador.setBounds(20, 30, 130, 20);

        scpRecord.setViewportView(pnlData);

        pnlRecord.add(scpRecord, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle2.getString("KpiLoginPanel_00002"), pnlRecord); // NOI18N

        pnlPermissoes.setLayout(new java.awt.GridLayout(1, 0));

        splitSeguranca.setDividerLocation(200);

        scrollTreeSeguranca.setPreferredSize(new java.awt.Dimension(100, 64));

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        kpiTreeSeguranca.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        kpiTreeSeguranca.setRowHeight(20);
        scrollTreeSeguranca.setViewportView(kpiTreeSeguranca);

        splitSeguranca.setLeftComponent(scrollTreeSeguranca);

        pnlSeguranca.setLayout(new javax.swing.BoxLayout(pnlSeguranca, javax.swing.BoxLayout.Y_AXIS));
        splitSeguranca.setRightComponent(pnlSeguranca);

        pnlPermissoes.add(splitSeguranca);

        tapCadastro.addTab(bundle.getString("KpiUsuarioFrame_00009"), pnlPermissoes); // NOI18N

        pnlScorecard.setLayout(new java.awt.BorderLayout());

        scrollTreeSco.setPreferredSize(new java.awt.Dimension(100, 64));

        kpiTreeSco.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        kpiTreeSco.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        kpiTreeSco.setRowHeight(18);
        kpiTreeSco.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                kpiTreeScoMouseClicked(evt);
            }
        });
        scrollTreeSco.setViewportView(kpiTreeSco);

        pnlScorecard.add(scrollTreeSco, java.awt.BorderLayout.CENTER);

        pnlTreeScoFind.setPreferredSize(new java.awt.Dimension(254, 25));
        pnlTreeScoFind.setLayout(new javax.swing.BoxLayout(pnlTreeScoFind, javax.swing.BoxLayout.LINE_AXIS));

        pnlTreeScoLabel.setLayout(new java.awt.BorderLayout());

        lblTreeSco.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTreeSco.setText(bundle2.getString("KpiUsuarioFrame_00040")); // NOI18N
        pnlTreeScoLabel.add(lblTreeSco, java.awt.BorderLayout.CENTER);

        pnlTreeScoFind.add(pnlTreeScoLabel);

        jLblProcurar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLblProcurar.setText(bundle.getString("JBISeekListPanel_00001")); // NOI18N
        jLblProcurar.setIconTextGap(0);
        jLblProcurar.setMaximumSize(new java.awt.Dimension(70, 16));
        jLblProcurar.setMinimumSize(new java.awt.Dimension(50, 16));
        jLblProcurar.setPreferredSize(new java.awt.Dimension(70, 20));
        pnlTreeScoFind.add(jLblProcurar);

        pnlEspace.setPreferredSize(new java.awt.Dimension(3, 10));
        pnlTreeScoFind.add(pnlEspace);

        jTxtProcurar.setMaximumSize(new java.awt.Dimension(10, 21));
        jTxtProcurar.setPreferredSize(new java.awt.Dimension(200, 22));
        jTxtProcurar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtProcurarFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtProcurarFocusLost(evt);
            }
        });
        jTxtProcurar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtProcurarKeyReleased(evt);
            }
        });
        pnlTreeScoFind.add(jTxtProcurar);

        pnlScorecard.add(pnlTreeScoFind, java.awt.BorderLayout.PAGE_START);

        tapCadastro.addTab(KpiStaticReferences.getKpiCustomLabels().getSco(), pnlScorecard);

        pnlPersonalizacao.setLayout(new java.awt.BorderLayout());

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord1.setBorder(null);

        pnlPreferencia.setLayout(null);

        lblVisualiza��o.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblVisualiza��o.setText(bundle.getString("KpiUsuarioFrame_00027")); // NOI18N
        lblVisualiza��o.setEnabled(false);
        lblVisualiza��o.setMaximumSize(new java.awt.Dimension(100, 16));
        lblVisualiza��o.setMinimumSize(new java.awt.Dimension(100, 16));
        lblVisualiza��o.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlPreferencia.add(lblVisualiza��o);
        lblVisualiza��o.setBounds(10, 10, 110, 30);

        grpVizual.add(radPasta);
        radPasta.setMnemonic('P');
        radPasta.setText(bundle.getString("KpiUsuarioFrame_00029")); // NOI18N
        radPasta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        radPasta.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlPreferencia.add(radPasta);
        radPasta.setBounds(350, 8, 60, 30);

        grpVizual.add(radJanela);
        radJanela.setMnemonic('J');
        radJanela.setSelected(true);
        radJanela.setText(bundle.getString("KpiUsuarioFrame_00028")); // NOI18N
        radJanela.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        radJanela.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        radJanela.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radJanelaItemStateChanged(evt);
            }
        });
        pnlPreferencia.add(radJanela);
        radJanela.setBounds(280, 8, 70, 30);

        cboVisualiza��o.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboVisualiza��oItemStateChanged(evt);
            }
        });
        pnlPreferencia.add(cboVisualiza��o);
        cboVisualiza��o.setBounds(10, 40, 400, 22);
        pnlPreferencia.add(jList1);
        jList1.setBounds(0, 0, 0, 0);

        chkAutoRefresh.setText(bundle.getString("KpiUsuarioFrame_00035")); // NOI18N
        pnlPreferencia.add(chkAutoRefresh);
        chkAutoRefresh.setBounds(10, 70, 440, 23);

        chkHideTree.setText(bundle.getString("KpiUsuarioFrame_00036")); // NOI18N
        pnlPreferencia.add(chkHideTree);
        chkHideTree.setBounds(10, 100, 440, 23);

        chkScoDefault.setText(bundle.getString("KpiUsuarioFrame_00037")); // NOI18N
        chkScoDefault.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkScoDefaultActionPerformed(evt);
            }
        });
        pnlPreferencia.add(chkScoDefault);
        chkScoDefault.setBounds(10, 130, 330, 23);
        pnlPreferencia.add(cboSco);
        cboSco.setBounds(10, 160, 400, 22);

        lblSco.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSco.setText(bundle.getString("KpiUsuarioFrame_00038")); // NOI18N
        lblSco.setEnabled(false);
        lblSco.setMaximumSize(new java.awt.Dimension(100, 16));
        lblSco.setMinimumSize(new java.awt.Dimension(100, 16));
        lblSco.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlPreferencia.add(lblSco);
        lblSco.setBounds(350, 130, 60, 30);

        tsArvore.setCombo(cboSco);
        tsArvore.setRoot(false);
        pnlPreferencia.add(tsArvore);
        tsArvore.setBounds(410, 160, 30, 22);

        pnlInicio.setBorder(javax.swing.BorderFactory.createTitledBorder(null,  java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00041"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11))); // NOI18N
        pnlInicio.setForeground(new java.awt.Color(204, 204, 204));
        pnlInicio.setToolTipText("");
        pnlInicio.setName(""); // NOI18N

        chkShowScorecarding.setText( java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00023")); // NOI18N
        chkShowScorecarding.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkShowScorecardingActionPerformed(evt);
            }
        });

        chkShowMapaEstrategico.setText( java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00065")); // NOI18N
        chkShowMapaEstrategico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkShowMapaEstrategicoActionPerformed(evt);
            }
        });

        tsEstrategia.setCombo(cboEstrategia);
        tsEstrategia.setRoot(false);

        javax.swing.GroupLayout pnlInicioLayout = new javax.swing.GroupLayout(pnlInicio);
        pnlInicio.setLayout(pnlInicioLayout);
        pnlInicioLayout.setHorizontalGroup(
            pnlInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInicioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkShowScorecarding)
                    .addComponent(chkShowMapaEstrategico)
                    .addGroup(pnlInicioLayout.createSequentialGroup()
                        .addComponent(cboEstrategia, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tsEstrategia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(54, Short.MAX_VALUE))
        );
        pnlInicioLayout.setVerticalGroup(
            pnlInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInicioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tsEstrategia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlInicioLayout.createSequentialGroup()
                        .addComponent(chkShowScorecarding)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkShowMapaEstrategico)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboEstrategia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        pnlPreferencia.add(pnlInicio);
        pnlInicio.setBounds(10, 200, 420, 120);
        pnlInicio.getAccessibleContext().setAccessibleName("");

        scpRecord1.setViewportView(pnlPreferencia);

        pnlFields.add(scpRecord1, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlPersonalizacao.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiUsuarioFrame_00039"), pnlPersonalizacao); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(250, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(250, 32));

        btnAlterarSenha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_seguranca.gif"))); // NOI18N
        btnAlterarSenha.setText(bundle1.getString("KpiUsuarioFrame_00002")); // NOI18N
        btnAlterarSenha.setIconTextGap(0);
        btnAlterarSenha.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnAlterarSenha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarSenhaActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnAlterarSenha);

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle2.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle2.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new java.awt.Dimension(300, 19));
        tbInsertion.setPreferredSize(new java.awt.Dimension(300, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle2.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle2.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle2.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda2.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnAjuda2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda2.setText(bundle.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjuda2ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda2);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents
	private void chkScoDefaultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkScoDefaultActionPerformed
            if (chkScoDefault.isSelected()) {
                lblSco.setEnabled(true);
                cboSco.setEnabled(true);
                populateScoDefault();
            } else {
                lblSco.setEnabled(false);
                cboSco.setEnabled(false);
            }
	}//GEN-LAST:event_chkScoDefaultActionPerformed

	private void btnAjuda2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjuda2ActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjuda2ActionPerformed

	private void cboVisualiza��oItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboVisualiza��oItemStateChanged
            if (evt.getStateChange() == ItemEvent.DESELECTED) {
                radJanela.setSelected(aViewType[cboVisualiza��o.getSelectedIndex()] == KpiFormController.ST_JANELA);
                radPasta.setSelected(aViewType[cboVisualiza��o.getSelectedIndex()] != KpiFormController.ST_JANELA);
            }
	}//GEN-LAST:event_cboVisualiza��oItemStateChanged

    @SuppressWarnings("empty-statement")
	private void radJanelaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radJanelaItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            aViewType[cboVisualiza��o.getSelectedIndex()] = KpiFormController.ST_JANELA;
        } else {
            aViewType[cboVisualiza��o.getSelectedIndex()] = KpiFormController.ST_PASTA;
        }
	}//GEN-LAST:event_radJanelaItemStateChanged

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
            prepareSearchField();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
            event.loadRecord();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

	private void btnAlterarSenhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarSenhaActionPerformed
            KpiSenhaDialog dialog = new KpiSenhaDialog(this);
	}//GEN-LAST:event_btnAlterarSenhaActionPerformed

    private void kpiTreeScoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_kpiTreeScoMouseClicked
        if (isEditMode) {
            int x = evt.getX();
            int y = evt.getY();
            int row = kpiTreeSco.getRowForLocation(x, y);
            TreePath path = kpiTreeSco.getPathForRow(row);

            if (path != null) {
                DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) path.getLastPathComponent();
                KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
                boolean bSelected = !(node.isSelected());
                node.setSelected(bSelected);

                if (evt.getButton() == MouseEvent.BUTTON1) {
                    selectedFromRoot(nodeAux, bSelected);
                }

                ((DefaultTreeModel) kpiTreeSco.getModel()).nodeChanged(node);

                kpiTreeSco.revalidate();
                kpiTreeSco.repaint();
            }
        }
        prepareSearchField();
    }//GEN-LAST:event_kpiTreeScoMouseClicked

    private void jTxtProcurarFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtProcurarFocusGained
        initSearchField();
    }//GEN-LAST:event_jTxtProcurarFocusGained

    private void jTxtProcurarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtProcurarKeyReleased
        boolean found = false;

        if (jTxtProcurar.getText().length() == 0) {
            found = true;
        } else {
            DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) kpiTreeSco.getModel().getRoot();
            while (oMutTreeNode != null) {
                if (oMutTreeNode.getUserObject().toString().toLowerCase().contains(jTxtProcurar.getText().toLowerCase())) {
                    DefaultTreeModel treeModel = (DefaultTreeModel) kpiTreeSco.getModel();
                    TreePath tp = new TreePath(treeModel.getPathToRoot(oMutTreeNode));
                    kpiTreeSco.setSelectionPath(tp);
                    found = true;
                    break;
                }
                oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();
            }
        }
        setSearchField(found);
    }//GEN-LAST:event_jTxtProcurarKeyReleased

    private void jTxtProcurarFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtProcurarFocusLost
        if (jTxtProcurar.getText().length() == 0) {
            prepareSearchField();
        }
    }//GEN-LAST:event_jTxtProcurarFocusLost

    private void chkShowScorecardingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkShowScorecardingActionPerformed
        if (chkShowScorecarding.isSelected()){
            chkShowMapaEstrategico.setSelected(false);
            cboEstrategia.setEnabled(false);
        }
    }//GEN-LAST:event_chkShowScorecardingActionPerformed

    private void chkShowMapaEstrategicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkShowMapaEstrategicoActionPerformed
        if (chkShowMapaEstrategico.isSelected()) {
            chkShowScorecarding.setSelected(false);
            cboEstrategia.setEnabled(true);
            populateEstrategia();
        } else {
            cboEstrategia.setEnabled(false);
        }
    }//GEN-LAST:event_chkShowMapaEstrategicoActionPerformed

    private void selectedFromRoot(DefaultMutableTreeNode oNode, boolean bSelected) {
        Enumeration e = oNode.children();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) e.nextElement();
            KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
            node.setSelected(bSelected);
            selectedFromRoot(nodeAux, bSelected);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjuda2;
    private javax.swing.JButton btnAlterarSenha;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cboEstrategia;
    private javax.swing.JComboBox cboSco;
    private javax.swing.JComboBox cboVisualiza��o;
    private javax.swing.JCheckBox chkAdministrador;
    private javax.swing.JCheckBox chkAtivo;
    private javax.swing.JCheckBox chkAutoRefresh;
    private javax.swing.JCheckBox chkHideTree;
    private javax.swing.JCheckBox chkScoDefault;
    private javax.swing.JCheckBox chkShowMapaEstrategico;
    private javax.swing.JCheckBox chkShowScorecarding;
    private pv.jfcx.JPVEdit fldCargo;
    private pv.jfcx.JPVEdit fldEmail;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldPrimNome;
    private pv.jfcx.JPVMask fldRamal;
    private pv.jfcx.JPVMask fldTelefone;
    private javax.swing.ButtonGroup grpVizual;
    private javax.swing.JLabel jLblProcurar;
    private javax.swing.JList jList1;
    private javax.swing.JTextField jTxtProcurar;
    private javax.swing.JTree kpiTreeSco;
    private javax.swing.JTree kpiTreeSeguranca;
    private javax.swing.JLabel lblCargo;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPrimNome;
    private javax.swing.JLabel lblRamal;
    private javax.swing.JLabel lblSco;
    private javax.swing.JLabel lblTelefone;
    private javax.swing.JLabel lblTreeSco;
    private javax.swing.JLabel lblVisualiza��o;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlEspace;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlInicio;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlPermissoes;
    private javax.swing.JPanel pnlPersonalizacao;
    private javax.swing.JPanel pnlPreferencia;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlScorecard;
    private javax.swing.JPanel pnlSeguranca;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JPanel pnlTreeScoFind;
    private javax.swing.JPanel pnlTreeScoLabel;
    private javax.swing.JRadioButton radJanela;
    private javax.swing.JRadioButton radPasta;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane scpRecord1;
    private javax.swing.JScrollPane scrollTreeSco;
    private javax.swing.JScrollPane scrollTreeSeguranca;
    private javax.swing.JSplitPane splitSeguranca;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBITreeSelection tsArvore;
    private kpi.beans.JBITreeSelection tsEstrategia;
    // End of variables declaration//GEN-END:variables

    public DefaultMutableTreeNode createTree(BIXMLRecord treeXML) {
        DefaultMutableTreeNode root = insertGroup(treeXML.getBIXMLVector("DPTOS"), null);
        return root;
    }

    public DefaultMutableTreeNode insertRecord(BIXMLRecord record, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiSelectedTreeNode oScoNode = new KpiSelectedTreeNode(
                record.getAttributes().getString("ID"),
                record.getAttributes().getString("NOME"),
                record.getAttributes().getBoolean("ENABLED"),
                record.getAttributes().getBoolean("SELECTED"),
                record.getAttributes().getInt("IMAGE"));

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oScoNode);

        for (Iterator e = record.getKeyNames(); e.hasNext();) {
            String strChave = (String) e.next();

            if (record.getBIXMLVector(strChave).getAttributes().contains("TIPO")) {
                child = insertGroup(record.getBIXMLVector(strChave), child);
            } else {
                child = insertRecord(record.getBIXMLVector(strChave), child);
            }
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    //Novo este
    public DefaultMutableTreeNode insertRecord(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiSelectedTreeNode oScoNode = new KpiSelectedTreeNode(
                vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"),
                vector.getAttributes().getBoolean("ENABLED"),
                vector.getAttributes().getBoolean("SELECTED"),
                vector.getAttributes().getInt("IMAGE"));

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oScoNode);

        for (int i = 0; i < vector.size(); i++) {
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    public DefaultMutableTreeNode insertGroup(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        DefaultMutableTreeNode child = new DefaultMutableTreeNode(
                new KpiSelectedTreeNode(vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"),
                vector.getAttributes().getBoolean("ENABLED"),
                vector.getAttributes().getBoolean("SELECTED"),
                vector.getAttributes().getInt("IMAGE")));

        for (int i = 0; i < vector.size(); i++) {
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        return "1";
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public void actionPerformed(java.awt.event.ActionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) kpiTreeSeguranca.getLastSelectedPathComponent();
        javax.swing.JCheckBox source = (javax.swing.JCheckBox) e.getSource();
        Object userObject = node.getUserObject();

        if (userObject instanceof kpi.swing.KpiTreeNode) {
            //Item clicado na arvore
            kpi.xml.BIXMLRecord nodeClicked = (((kpi.swing.KpiTreeNode) userObject).getRecord());
            //Item correspondente no vetor de segura�a.
            kpi.xml.BIXMLRecord nodeTemp = vctSeguranca.get(nodeClicked.getAttributes().getInt("ID") - 1);
            //vetor com itens de seguran�a.
            kpi.xml.BIXMLVector vctItens = nodeTemp.getBIXMLVector("ITEMREGRA");
            //Retornando o atributo, para setar o valor do item.
            kpi.xml.BIXMLAttributes attributes = vctItens.get((Integer) source.getClientProperty("REGRA_ID")).getAttributes();
            attributes.set("VALOR", source.isSelected() ? "1" : "0");
        }

    }

    @Override
    public String getParentType() {
        return parentType;
    }

    /**
     * Retorna o nome da regra, passando a constante de identifica��o
     */
    private String getRegraByID(int id) {
        String regra = new String();
        switch (id) {
            case 1:
                regra = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00020");
                break;

            case 2:
                regra = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00021");
                break;
           case 3:
                regra = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00041");
                break;
            case 4:
                regra = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00042");
                break;
            case 5:
                regra = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUsuarioFrame_00043");
                break;            
        }

        return regra;
    }

    /**
     * Verifica se a regra � valida.
     */
    private boolean getRegraValor(int valor) {
        return valor == 1 ? true : false;
    }

    /**
     * Configura as propriedades quando o usu�rio carregado n�o � administrador
     */
    private void configuraRegraUsuario() {
        if (!record.getBoolean("USUA_ISADMIN")) {
            tapCadastro.remove(pnlPermissoes);
            tapCadastro.remove(pnlScorecard);
            btnDelete.setVisible(false);
            configuraPersonalizacao();
        }
    }

    /*
     *Caso o usuario tenha acesso a op��o de personaliza��o habilita.
     */
    private void configuraPersonalizacao() {
        String key = "";
        if (vctSeguranca != null) {
            for (int regra = 0; regra
                    < vctSeguranca.size(); regra++) {
                key = vctSeguranca.get(regra).getAttributes().getString("NOME");
                if (key.equals("USU_CONFIGS")) {
                    kpi.xml.BIXMLVector vctItemRegra = vctSeguranca.get(regra).getBIXMLVector("ITEMREGRA");
                    boolean hasAccess = false;
                    //Verifica se o usuario tem acesso ao op��o do menu.
                    for (int itemRegra = 0; itemRegra < vctItemRegra.size(); itemRegra++) {
                        int regraValor = vctItemRegra.get(itemRegra).getAttributes().getInt("VALOR");
                        if (regraValor == 1) {
                            hasAccess = true;
                            break;
                        }
                    }
                    if (!hasAccess) {
                        tapCadastro.remove(pnlPersonalizacao);
                    }
                }
            }
        }
    }
}

class clsCacheSco {

    public kpi.xml.BIXMLVector vecXml;
    public String sSco;
}