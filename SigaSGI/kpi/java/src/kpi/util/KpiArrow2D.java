/*
 * KpiSeta2D.java
 *
 * Created on 29 de Setembro de 2003, 10:29
 */
package kpi.util;

import java.awt.*;
import java.awt.geom.*;

/**
 *
 * @author  siga1728
 */
public class KpiArrow2D {

    public final static int LINE = 0;
    public final static int QUADCURVE = 1;
    public final static int CONTROLBOXSIZE = 4;
    private int lineType = LINE; //0-LINE 1-QUADCURVE
    private double nTamSeta = 3;   //0=sem seta 1=Pequeno 2=Medio 3=Grande
    private Line2D line;
    private GeneralPath path;
    private Shape shape;
    private QuadCurve2D quadCurve;
    private Point ctrl;

    public KpiArrow2D() {
    }

    public KpiArrow2D(Point2D p1, Point2D p2) {
	setLine(p1, p2);
    }

    public KpiArrow2D(Point2D p1, Point2D p2, Point ctrl) {
	setLine(p1, p2, ctrl);
    }

    public KpiArrow2D(double zoom, Point2D p1, Point2D p2) {
	if (zoom > 1.5) {
	    nTamSeta = 3;
	} else if (zoom > 0.75) {
	    nTamSeta = 2;
	} else if (zoom >= 0){
	    nTamSeta = 1;
	} else {
	    nTamSeta = 0;
	}
	setLine(p1, p2);
    }

    public KpiArrow2D(double zoom, Point2D p1, Point2D p2, Point ctrl) {
	if (zoom > 1.5) {
	    nTamSeta = 3;
	} else if (zoom > 0.75) {
	    nTamSeta = 2;
	} else if (zoom >= 0){
	    nTamSeta = 1;
	} else {
	    nTamSeta = 0;
	}
	setLine(p1, p2, ctrl);
    }

    public void render(Graphics2D g2) {
	if (this.lineType == LINE) {
	    g2.draw(line);
	} else {
	    g2.draw(quadCurve);
	}

	if (nTamSeta > 0){
	    g2.fill(shape);
	}
    }

    public Point getCenterPoint() {
	int pX = (int) ((line.getX2() - line.getX1()) / 2);
	int pY = (int) ((line.getY2() - line.getY1()) / 2);
	pX += line.getX1();
	pY += line.getY1();

	return new Point(pX, pY);
    }

    public void renderControlPoint(Graphics2D g2) {
	int midPoint = KpiArrow2D.CONTROLBOXSIZE / 2;
	Rectangle controlBox = new Rectangle(ctrl.getLocation());

	controlBox.translate(-midPoint, -midPoint);

	controlBox.setSize(KpiArrow2D.CONTROLBOXSIZE, KpiArrow2D.CONTROLBOXSIZE);

	g2.fill(controlBox);
	g2.draw(controlBox);
    }

    /** Getter for property line.
     * @return Value of property line.
     */
    public java.awt.geom.Line2D getLine() {
	return line;
    }

    /**
     * Desenha uma linha reta
     */
    public void setLine(Point2D p1, Point2D p2) {
	setLine(p1, p2, KpiArrow2D.LINE);
	this.ctrl = new Point(this.getCenterPoint());
    }

    /**
     * Desenha uma curva
     */
    public void setLine(Point2D p1, Point2D p2, Point ctrl) {
	this.ctrl = new Point(ctrl);
	setLine(p1, p2, KpiArrow2D.QUADCURVE);
    }

    /** Setter for property line.
     * @param line New value of property line.
     */
    private void setLine(Point2D p1, Point2D p2, int lineType) {
	this.lineType = lineType;

	if (line == null) {
	    line = new Line2D.Double(p1, p2);
	} else {
	    line.setLine(p1, p2);
	}

	int px1 = 0;
	int px2 = 0;
	int py1 = 0;
	int py2 = 0;

	if (nTamSeta == 3) {
	    px1 = 18;
	    px2 = 18;
	    py1 = 7;
	    py2 = 8;
	} else if (nTamSeta == 2) {
	    px1 = 13;
	    px2 = 13;
	    py1 = 6;
	    py2 = 5;
	} else if (nTamSeta == 1){
	    px1 = 8;
	    px2 = 8;
	    py1 = 4;
	    py2 = 4;
	}

	path = new GeneralPath();
	path.moveTo((float) line.getX2(), (float) line.getY2());
	path.lineTo((float) line.getX2() - px1, (float) line.getY2() - py1);
	path.lineTo((float) line.getX2() - px2, (float) line.getY2() + py2);
	path.closePath();

	double catetoX = 0;
	double catetoY = 0;
	double ang = 0;

	if (this.lineType == QUADCURVE) {
	    if (quadCurve == null) {
		quadCurve = new QuadCurve2D.Double(line.getX1(), line.getY1(), ctrl.getX(), ctrl.getY(), line.getX2(), line.getY2());
	    } else {
		quadCurve.setCurve(line.getX1(), line.getY1(), ctrl.getX(), ctrl.getY(), line.getX2(), line.getY2());
	    }

	    catetoX = quadCurve.getCtrlPt().getX() - line.getX2();
	    catetoY = quadCurve.getCtrlPt().getY() - line.getY2();
	} else {
	    catetoX = line.getX2() - line.getX1();
	    catetoY = line.getY2() - line.getY1();
	}

	double raio = Math.sqrt(Math.pow(catetoX, 2) + Math.pow(catetoY, 2));
	double cos = catetoX / raio;

	if (this.lineType == QUADCURVE) {
	    ang = 135.1 + Math.acos(cos) * (catetoY < 0 ? -1 : 1);
	} else {
	    ang = Math.acos(cos) * (catetoY < 0 ? -1 : 1);
	}

	AffineTransform at = AffineTransform.getRotateInstance(ang, line.getX2(), line.getY2());
	shape = at.createTransformedShape(path);
    }

    public double ptSegDist(Point2D p2D) {
	double distance;
	if (path.contains(p2D)) {
	    //ponto dentro da seta
	    distance = 0.0d;
	} else {
	    if (this.lineType == LINE) {
		distance = line.ptSegDist(p2D);
	    } else {
		int midPoint = KpiArrow2D.CONTROLBOXSIZE / 2;
		Rectangle controlBox = new Rectangle(this.ctrl.getLocation(), new Dimension(KpiArrow2D.CONTROLBOXSIZE, KpiArrow2D.CONTROLBOXSIZE));

		controlBox.translate(-midPoint, -midPoint);

		if (controlBox.contains(p2D)) {
		    distance = 0.0d;
		} else {
		    //quando for quad curve testa apenas o ponto de controle
		    Point p1 = new Point(this.ctrl);
		    distance = p1.distance(p2D);

		    if (distance <= midPoint) {
			distance = 0.0d;
		    }
		}
	    }
	}

	return distance;
    }
}
