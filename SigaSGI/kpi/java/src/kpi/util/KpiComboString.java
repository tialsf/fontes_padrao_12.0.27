package kpi.util;
/*
* Como a JComboBox n�o aceita entradas duplicadas. 
* Foi criado esta classe para suprir esta necessidade caso a como esteja sendo 
* preenchida com String
*/
public class KpiComboString  {
	private String text;
	
	public KpiComboString(String txt) {
		text = txt;
	}
	
	public String toString() {
		return text;
	}
}