/*
 * KpiDateUtil.java
 *
 * Created on 8 de Mar�o de 2006, 17:22
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package kpi.util;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Alexandre Alves da Silva
 */
public class KpiDateUtil {
	
	/** Creates a new instance of KpiDateUtil */
	public KpiDateUtil(){}


	/**
	 * Recebe um Calendar e Retorna um String.
	 */
	public static String calendarToString(Calendar c) {
		String value = "  /  /    ";
		if(c != null) {
			value = DateFormat.getInstance().format(c.getTime());
		}
		return value;
	}


	/** Obt�m o valor GregorianCalendar do atributo referenciado pelo par�metro.
	 *@param key Objeto String com o nome do atributo.
	 *@return Objeto do tipo GregorianCalendar com o conte�do do atributo.
	 *@throws kpi.xml.BIXMLException Caso o atributo n�o exista no registro ou caso
	 * seja imposs�vel convert�-lo para um GregorianCalendar.
	 */
	public static Calendar stringToCalendar( String key ){
		GregorianCalendar gc = null;
		try {
			if(key.length() >= 10) {
				gc = new GregorianCalendar(
					Integer.parseInt(key.substring(6, 10).trim()),
					Integer.parseInt(key.substring(3, 5).trim())-1,
					Integer.parseInt(key.substring(0, 2).trim()));
			}else if(key.length() >= 8){
				gc = new GregorianCalendar(
					Integer.parseInt(key.substring(6, 8).trim()),
					Integer.parseInt(key.substring(3, 5).trim())-1,
					Integer.parseInt(key.substring(0, 2).trim()));
			}
		} catch (java.lang.NumberFormatException e) {
			gc = null;
		}
		return gc;
	}

	/**
	 * Recebe um Calendar e Retorna um String.
	 */
	public String getCalendarString(java.util.Calendar c) {
		
		return KpiDateUtil.calendarToString(c);
	}
	
	/** Obt�m o valor GregorianCalendar do atributo referenciado pelo par�metro.
	 *@param key Objeto String com o nome do atributo.
	 *@return Objeto do tipo GregorianCalendar com o conte�do do atributo.
	 *@throws kpi.xml.BIXMLException Caso o atributo n�o exista no registro ou caso
	 * seja imposs�vel convert�-lo para um GregorianCalendar.
	 */
	public Calendar getDate( String key ){
		return KpiDateUtil.stringToCalendar(key);
	}
}