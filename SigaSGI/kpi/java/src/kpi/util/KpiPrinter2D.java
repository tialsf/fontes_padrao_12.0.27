package kpi.util;
/*
 * Copyright 2001 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This software is the proprietary information of
 * Sun Microsystems, Inc.
 * Use is subject to license terms.
 *
 */

import java.io.*;
import java.awt.*;
import java.net.*;
import java.awt.image.*;
import java.awt.print.*;
import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;

public class KpiPrinter2D implements Printable {
    private Component compToBePrint = null;
    
    public void startPrint(){
	PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
	aset.add(OrientationRequested.PORTRAIT);
	aset.add(new Copies(1));
	aset.add(new JobName("job", null));
	/* Create a print job */
	PrinterJob pj = PrinterJob.getPrinterJob();
	pj.setPrintable(this);
	/* locate a print service that can handle the request */
	PrintService[] services = PrinterJob.lookupPrintServices();
	if (services.length > 0) {
	    try {
		pj.setPrintService(services[0]);
//		pj.pageDialog(aset);
		if(pj.printDialog(aset)) {
		    pj.print(aset);
		}
	    } catch (PrinterException pe) {
		System.err.println(pe);
	    }
	}
    }
    
    public int print(Graphics g,PageFormat pf,int pageIndex) {
	if (pageIndex == 0) {
	    Graphics2D g2d= (Graphics2D)g;
	    g2d.translate(pf.getImageableX(), pf.getImageableY());
	    compToBePrint.paint(g2d);
	    return Printable.PAGE_EXISTS;
	} else {
	    return Printable.NO_SUCH_PAGE;
	}
    }
    
    public void setCompToBePrint(Component compToBePrint) {
	this.compToBePrint = compToBePrint;
    }
    
}