/*
 * KpiToolKit.java
 *
 * Created on 25 de Outubro de 2004, 14:26
 */
package kpi.util;

//Imports para impress�o do gr�fico
import java.awt.Component;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.jnlp.BasicService;
import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;
import javax.swing.RepaintManager;
import kpi.core.KpiStaticReferences;
import kpi.util.prefs.Base64;

/**
 * @author  alexandreas
 */
public class KpiToolKit {

    /**
     * Creates a new instance of KpiToolKit
     */
    public KpiToolKit() {
    }
 
    /**
     * Grava o objeto passado com uma figura do tipo JPEG
     */
    public void saveComponentAsJPEG(java.awt.Component myComponent, String fileName) {
        java.awt.Dimension size = myComponent.getSize();
        java.awt.image.BufferedImage myImage = new java.awt.image.BufferedImage(size.width, size.height, java.awt.image.BufferedImage.TYPE_INT_RGB);

        java.awt.Graphics2D g2 = myImage.createGraphics();
        disableDoubleBuffering(myComponent);
        myComponent.doLayout();
        myComponent.paint(g2);
        enableDoubleBuffering(myComponent);

        writeJPEGFile(myImage, fileName);
    }

    /**
     *Grava a figura no servidor.
     */
    public void writeJPEGFile(java.awt.image.BufferedImage myImage, String fileName) {
        kpi.core.KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();

        /*Grava a figura no servidor.*/
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            /*Salva o arquivo de imagem como JPEG*/
            ImageIO.write(myImage, "jpg", out);
            /*Grava o arquivo no servidor.*/
            dataController.saveBase64(fileName, Base64.byteArrayToBase64(out.toByteArray()));
            /*Fecha o output stream*/
            out.close();
            /*Abre o navegador para salvar a figura localmente.*/
            this.browser(fileName);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void writeServerFile(byte[] file, String fileName) {
        writeServerFile(file, fileName, true, true);
    }

    public void writeServerFile(byte[] file, String fileName, boolean lNewFile, boolean refreshStatusBar) {
        kpi.core.KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();

        /*Grava o arquivo no servidor*/
        try {
            dataController.saveBase64(fileName, kpi.util.prefs.Base64.byteArrayToBase64(file), lNewFile, refreshStatusBar);

            /*Abre o navegador para salvar a figura localmente.*/
            if (lNewFile) {
                this.browser(fileName);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void eraseServerFiles(String ID, String tipo, String origem) {
        String comando = new String("");
        kpi.core.KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();
        try {
            comando = "DELFILES|" + origem;
            dataController.executeRecord(ID, tipo, comando);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //Retorna o conteudo de um arquivo em um array de  bytes
    public byte[] getBytesFromFile(java.io.File file) throws java.io.IOException {
        java.io.InputStream is = new java.io.FileInputStream(file);

        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // O arquivo � muito grande o:((
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        // Assegura que todos os dados foram lidos
        if (offset < bytes.length) {
            throw new java.io.IOException("Could not completely read file " + file.getName());
        }


        // Close the input stream and return bytes
        is.close();
        return bytes;
    }

    /**
     * Desabilita o buffer para figuras.
     * @param c
     */
    public void disableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(false);
    }

    /**
     * Habilita o buffer para figuras.
     * @param c
     */
    public void enableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(true);
    }

    /**
     * Realiza a abertura de uma URL no browser padr�o.
     * @param urlPath
     */
    public void browser(String document) {

        StringBuffer buffer = new StringBuffer();

        try {
            //Verifica se a o programa est� sendo executado via Applet ou Desktop.
            if (KpiStaticReferences.getKpiExecutionType() == KpiStaticReferences.KPI_APLICATION) {
                buffer.append(KpiStaticReferences.getKpiApplication().getCodeBase()).append(document);
            } else {
                buffer.append(KpiStaticReferences.getKpiApplet().getCodeBase().toString()).append(document);
            }

            //Transforma a strUrl em uma URL.
            URL url = new URL(buffer.toString());
            //Realiza a abertura do arquivo no Browser.
            this.showDocument(url);

        } catch (IOException ex) {
            System.out.println(ex.getStackTrace());
        }
    }

    /**
     * Realiza a abertura de uma URL no browser padr�o.
     * @param url Url desejada.
     */
    public void browser(URL url) {
        try {
            //Realiza a abertura do arquivo no Browser.
            this.showDocument(url);

        } catch (IOException ex) {
            System.out.println(ex.getStackTrace());
        }
    }

    /**
     * Realiza a abertura do browser default na URL especificada.
     * @param url Url desejada.
     * @throws IOException
     */
    private void showDocument(URL url) throws IOException {
        if (KpiStaticReferences.getKpiExecutionType() == KpiStaticReferences.KPI_APLICATION) {
            boolean isOk = false;
            try {
                //Realiza a abertura do arquivo no SGI Desktop.
                BasicService bs = (BasicService) ServiceManager.lookup("javax.jnlp.BasicService");
                bs.showDocument(url);
                isOk = true;
            } catch (UnavailableServiceException ex) {
                //Realiza a abertura for�ada do arquivo no SGI Desktop.
                Runtime.getRuntime().exec("cmd.exe /C start " + url.toString());
                isOk = true;
            }

            if (!isOk){
                //Realiza a abertura for�ada do arquivo no SGI Desktop.
                Runtime.getRuntime().exec("cmd.exe /C start " + url.toString());
            }
        } else {
            //Realiza a abertura do arquivo no Browser.
            KpiStaticReferences.getKpiApplet().getAppletContext().showDocument(url, "_blank");
        }
    }
}
