package kpi.xml;

import java.util.*;

/**
 * @author  Alexandre Albert Gon�alves
 * Importado para uso no KPI 22/08/2005
 */
public class BIXMLAttributes extends BIXMLGeneralData{
    
    public BIXMLAttributes clone2() {
	BIXMLAttributes attrs = new BIXMLAttributes();
	for (Iterator e = getKeyNames() ; e.hasNext() ;) {
	    String key = (String) e.next();
	    attrs.set( key, getString( key ) );
	}
	return attrs;
    }
    
    public BIXMLAttributes() {
	super();
    }
    
    public BIXMLAttributes(org.xml.sax.Attributes attrs) {
	if (attrs != null) {
	    for (int i = 0; i < attrs.getLength(); i++) {
		String aName = attrs.getLocalName(i);
		if ("".equals(aName))
		    aName = attrs.getQName(i);
		set(aName, attrs.getValue(i));
	    }
	}
    }

    
    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	for (Iterator e = getKeyNames() ; e.hasNext() ;)
	    try {
		String key = (String) e.next();
		sb.append(key);
		sb.append("=\"");
		sb.append(getString(key));
		sb.append("\" ");
	    } catch( BIXMLException exc) {
		kpi.core.KpiDebug.println(exc.getMessage());
	    }
	return sb.toString();
    }
}