package kpi.xml;

public class BIXMLConversionException extends BIXMLException {
    
    /**
     * Creates a new instance of <code>BIXMLConversionException</code> without detail message.
     */
    public BIXMLConversionException() {
    }
    
    
    /**
     * Constructs an instance of <code>BIXMLConversionException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public BIXMLConversionException(String msg) {
	super(msg);
    }
    
    public BIXMLConversionException(String msg, String dtl) {
	super(msg, dtl);
    }
}
