package kpi.xml;

/** Todos os erros ocorridos na classes do pacote kpi.xml lan�ar�o uma exce��o
 * do tipo BIXMLException. Com isso, pretendemos simplificar o tratamento dos
 * problemas ocorridos durante a execu��o atrav�s da uniformiza��o do modo como
 * ele � feito.
 * @author Alexandre Albert Gon�alves
 */
public class BIXMLException extends java.lang.RuntimeException {
    private String detail = new String();
    
    public BIXMLException() {
    }
    
    /** @param msg Mensagem de erro que acompanha a exce��o.
     */
    public BIXMLException(String msg) {
	super(msg);
    }
    
    /** Este construtor permite que, al�m da tradicional mensagem de erro, o
     *programador consiga armazenar tamb�m um campo de detalhe, muito �til para
     *descobrir qual � a natureza do erro. Por exemplo: quando o XML n�o pode ser
     *parseado corretamente, este campo pode ser utilizado para guardar o XML
     *defeituoso, a fim de que seja f�cil determinar qual � o erro nele.
     *@param msg Mensagem de erro que acompanha a exce��o.
     *@param det Detalhe do erro. Seu conte�do depender� do contexto em que
     *ocorreu o erro. A priori, pode armazenar qualquer valor conveniente para o
     *programador.
     */
    public BIXMLException(String msg, String det) {
	super(msg);
	detail = det;
    }
    
    /**M�todo que permite obter o valor do campo de detalhe da exce��o. Tem um
     * funcionamento similar ao m�todo getMessage() de Exception.
     * @return Um objeto String com o conte�do do campo de detalhe da exce��o.
     */
    public String getDetail() {
	return detail;
    }
}
