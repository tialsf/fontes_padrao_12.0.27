package kpi.xml;

import java.util.*;
import java.text.*;

public class BIXMLGeneralData {

    // protected HashMap props = new HashMap();
    protected TreeMap props = new TreeMap();

    protected String getPropertyAsString(String key) {
	if (props.get(key) instanceof kpi.xml.BIXMLVector) {
	    return ((kpi.xml.BIXMLVector) props.get(key)).toString();
	} else {
	    return (String) props.get(key);
	}
    }

    protected void setPropertyAsString(String key, String value) {
	props.put(key, value);
    }

    public int size() {
	return props.size();
    }

    /** Obt�m o valor de tipo boolean do atributo referenciado pelo par�metro.
     *@param key Objeto String com o nome do atributo.
     *@return Uma vari�vel do tipo boolean com o conte�do do atributo.
     *@throws kpi.xml.BIXMLException Caso o atributo n�o exista no registro ou o seu
     * valor seja inv�lido (diferente de TRUE e FALSE), lan�a-se uma exce��o.
     */
    public boolean getBoolean(String key) throws BIXMLException {
	boolean result = false;
	String value = getPropertyAsString(key);

	if (value == null) {
	    throw new BIXMLKeyDoesNotExistException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00003") + key + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00002"));
	}

	if (value.toUpperCase().indexOf('T') != -1) {
	    result = true;
	} else if (value.equals("1")) {
	    result = true;
	}

	return result;
    }

    /** Obt�m o valor String do atributo referenciado pelo par�metro.
     *@param key Objeto String com o nome do atributo.
     *@return Objeto do tipo String com o conte�do do atributo.
     *@throws kpi.xml.BIXMLException Caso o atributo n�o exista.
     */
    public String getString(String key) throws BIXMLException {
	String value = getPropertyAsString(key);
	if (value == null) {
	    throw new BIXMLKeyDoesNotExistException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00003") + key + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00002"));
	}
	return value;
    }

    /** Obt�m o valor int do atributo referenciado pelo par�metro.
     *@param key Objeto String com o nome do atributo.
     *@return Vari�vel do tipo int com o conte�do do atributo.
     *@throws kpi.xml.BIXMLException Caso o atributo n�o exista no registro ou caso
     * seja imposs�vel convert�-lo para um valor no formato num�rico.
     */
    public int getInt(String key) throws BIXMLException {
	int intValue = 0;
	String value = getPropertyAsString(key);
	if (value == null) {
	    throw new BIXMLKeyDoesNotExistException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00003") + key + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00002"));
	}
	try {
	    intValue = Integer.parseInt(getPropertyAsString(key));
	} catch (java.lang.NumberFormatException e) {
	    intValue = 0;
	}
	return intValue;
    }

    /** Obt�m o valor long do atributo referenciado pelo par�metro.
     *@param key Objeto String com o nome do atributo.
     *@return Vari�vel do tipo long com o conte�do do atributo.
     *@throws kpi.xml.BIXMLException Caso o atributo n�o exista no registro ou caso
     * seja imposs�vel convert�-lo para um valor no formato num�rico.
     */
    public long getLong(String key) throws BIXMLException {
	long longValue = 0;
	String value = getPropertyAsString(key);
	if (value == null) {
	    throw new BIXMLKeyDoesNotExistException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00003") + key + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00002"));
	}
	try {
	    longValue = Long.parseLong(getPropertyAsString(key));
	} catch (java.lang.NumberFormatException e) {
	    longValue = 0;
	}
	return longValue;
    }

    /** Obt�m o valor double do atributo referenciado pelo par�metro.
     *@param key Objeto String com o nome do atributo.
     *@return Vari�vel do tipo double com o conte�do do atributo.
     *@throws kpi.xml.BIXMLException Caso o atributo n�o exista no registro ou caso
     * seja imposs�vel convert�-lo para um valor no formato num�rico.
     */
    public double getDouble(String key) throws BIXMLException {
	double doubleValue = 0;
	String value = getPropertyAsString(key);
	if (value == null) {
	    throw new BIXMLKeyDoesNotExistException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00003") + key + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00002"));
	}
	try {
	    doubleValue = Double.parseDouble(getPropertyAsString(key));
	} catch (java.lang.NumberFormatException e) {
	    doubleValue = 0;
	}
	return doubleValue;
    }

    /** Obt�m o valor float do atributo referenciado pelo par�metro.
     *@param key Objeto String com o nome do atributo.
     *@return Vari�vel do tipo float com o conte�do do atributo.
     *@throws kpi.xml.BIXMLException Caso o atributo n�o exista no registro ou caso
     * seja imposs�vel convert�-lo para um valor no formato num�rico.
     */
    public float getFloat(String key) throws BIXMLException {
	float floatValue = 0;
	String value = getPropertyAsString(key);
	if (value == null) {
	    throw new BIXMLKeyDoesNotExistException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00003") + key + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00002"));
	}
	try {
	    floatValue = Float.parseFloat(getPropertyAsString(key));
	} catch (java.lang.NumberFormatException e) {
	    floatValue = 0;
	}
	return floatValue;
    }

    /** Obt�m o valor GregorianCalendar do atributo referenciado pelo par�metro.
     *@param key Objeto String com o nome do atributo.
     *@return Objeto do tipo GregorianCalendar com o conte�do do atributo.
     *@throws kpi.xml.BIXMLException Caso o atributo n�o exista no registro ou caso
     * seja imposs�vel convert�-lo para um GregorianCalendar.
     */
    public GregorianCalendar getDate(String key) throws BIXMLException {
	GregorianCalendar gc = null;
	String value = getPropertyAsString(key);
	if (value == null) {
	    throw new BIXMLKeyDoesNotExistException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00003") + key + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00002"));
	}
	if (value.length() >= 10) {
	    try {
		gc = new GregorianCalendar(
			Integer.parseInt(getPropertyAsString(key).substring(6, 10)),
			Integer.parseInt(getPropertyAsString(key).substring(3, 5)) - 1,
			Integer.parseInt(getPropertyAsString(key).substring(0, 2)));
	    } catch (java.lang.NumberFormatException e) {
		gc = null;
	    }
	}
	return gc;
    }

    /** Insere um atributo do tipo String.
     * @param key Key do novo atributo.
     * @param value Valor do novo atributo.
     */
    public void set(String key, String value) {
	setPropertyAsString(key, value);
    }

    /** Insere um atributo do tipo boolean.
     * @param key Key do novo atributo.
     * @param value Valor do novo atributo.
     */
    public void set(String key, boolean value) {
	if (value) {
	    setPropertyAsString(key, "T");
	} else {
	    setPropertyAsString(key, "F");
	}
    }

    /** Insere um atributo do tipo long.
     * @param key Key do novo atributo.
     * @param value Valor do novo atributo.
     */
    public void set(String key, long value) {
	setPropertyAsString(key, Long.toString(value));
    }

    /** Insere um atributo do tipo int.
     * @param key Key do novo atributo.
     * @param value Valor do novo atributo.
     */
    public void set(String key, int value) {
	setPropertyAsString(key, Integer.toString(value));
    }

    /** Insere um atributo do tipo float.
     * @param key Key do novo atributo.
     * @param value Valor do novo atributo.
     */
    public void set(String key, float value) {
	setPropertyAsString(key, Float.toString(value));
    }

    /** Insere um atributo do tipo double.
     * @param key Key do novo atributo.
     * @param value Valor do novo atributo.
     */
    public void set(String key, double value) {
	setPropertyAsString(key, Double.toString(value));
    }

    /** Insere um atributo do tipo GregorianCalendar.
     * @param key Key do novo atributo.
     * @param value Valor do novo atributo.
     */
    public void set(String key, Calendar c) {
	// GregorianCalendar gc = (GregorianCalendar) c;
	String value = "  /  /    ";
	if (c != null) {
	    SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
	    value = new String(date.format(c.getTime()));
	}
	setPropertyAsString(key, value);
    }

    public void remove(String key) throws BIXMLKeyDoesNotExistException {
	if (props.remove(key) == null) {
	    throw new BIXMLKeyDoesNotExistException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00010") + key + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLGeneralData_00002"));
	}
    }

    public boolean contains(String key) {
	return props.containsKey(key);
    }

    public Iterator getKeyNames() {
	return props.keySet().iterator();
    }
    /** Retorna o conte�do do registro XML armazenando em um objeto String
     *todos os campos e os seus respectivos valores. �til para debug.
     *@return Conte�do do objeto BIXMLReadRecord.
     */
    /*	public String print() {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    props.list(pw);

    return sw.toString();
    }*/
    /*static public void main ( String strArgs[]) {
    BIXMLGeneralData data = new BIXMLGeneralData();

    try {
    data.set("string", "teste");
    data.set("boolean1", true);
    data.set("boolean2", false);
    data.set("int", 23);
    data.set("long", 12345679);
    data.set("float", 12.3);
    data.set("double", 12354654.234);
    data.set("data", new GregorianCalendar(2002, 4, 3));
    System.out.println(data.print());
    data.remove("data");
    System.out.println(data.print());
    data.set("data", new GregorianCalendar(2002, 05, 03));
    System.out.println(data.print());
    //data.remove("dunha");
    data.set("double", 2);
    System.out.println(data.print());
    }
    catch (BIXMLKeyDoesNotExistException e) {
    System.out.println("KeyDoesNotExist: "+e.getMessage());
    }
    catch (BIXMLException e) {
    System.out.println("Normal: "+e.getMessage());
    }
    }*/
}