package kpi.xml;

public class BIXMLKeyAlreadyExistsException extends BIXMLException {
    
    /**
     * Creates a new instance of <code>BIXMLKeyAlreadyExistsException</code> without detail message.
     */
    public BIXMLKeyAlreadyExistsException() {
    }
    
    
    /**
     * Constructs an instance of <code>BIXMLKeyAlreadyExistsException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public BIXMLKeyAlreadyExistsException(String msg) {
	super(msg);
    }
    
    public BIXMLKeyAlreadyExistsException(String msg, String dtl) {
	super(msg, dtl);
    }
}
