package kpi.xml;

public class BIXMLKeyDoesNotExistException extends BIXMLException {
    
    /**
     * Creates a new instance of <code>BIXMLKeyDoesNotExistException</code> without detail message.
     */
    public BIXMLKeyDoesNotExistException() {
    }
    
    
    /**
     * Constructs an instance of <code>BIXMLKeyDoesNotExistException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public BIXMLKeyDoesNotExistException(String msg) {
	super(msg);
    }
    
    public BIXMLKeyDoesNotExistException(String msg, String dtl) {
	super(msg, dtl);
    }
}
