/*
 * BIXMLVector.java
 *
 * Created on May 12, 2003, 3:09 PM
 */

package kpi.xml;

import java.util.*;
import java.io.*;
import org.xml.sax.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

/**
 * @author  siga1996
 */
public class BIXMLVector implements Iterable<BIXMLRecord>, Iterator<BIXMLRecord>{
    
    ArrayList recordsVector = new ArrayList();
    BIXMLAttributes attributes = new BIXMLAttributes();
    String mainTag = new String();
    String recordTag = new String();
    
    public BIXMLVector clone2() {
	BIXMLVector vector = new BIXMLVector( mainTag, recordTag );
	vector.setAttributes( attributes.clone2() );
	BIXMLRecord recordAux = null;
	for ( int i = 0; i < recordsVector.size(); i++ ) {
	    recordAux = (BIXMLRecord) recordsVector.get(i);
	    vector.add( recordAux.clone2() );
	}
	return vector;
    }
    
    public BIXMLVector( ) {}
    
    public BIXMLVector( String mTag, String rTag ) {
	mainTag = String.valueOf(mTag);
	recordTag = String.valueOf(rTag);
    }
    
    public BIXMLVector( String mTag, String rTag, BIXMLAttributes attr ) {
	mainTag = String.valueOf(mTag);
	recordTag = String.valueOf(rTag);
	attributes = attr;
    }
    
    public void setRecordTag( String rTag ) {
	recordTag = rTag;
    }
    
    public String getRecordTag() {
	return recordTag;
    }
    
    public void setMainTag( String mTag ) {
	mainTag = mTag;
    }
    
    public String getMainTag( ) {
	return mainTag;
    }
    
    public BIXMLAttributes getAttributes() {
	return attributes;
    }
    
    public void setAttributes(BIXMLAttributes attr) {
	attributes = attr;
    }
    
    public BIXMLRecord addNewRecord() {
	BIXMLRecord record = new BIXMLRecord(recordTag);
	recordsVector.add(record);
	return record;
    }
    
    public BIXMLRecord addNewRecord(String rTag) {
	BIXMLRecord record = new BIXMLRecord(rTag);
	recordsVector.add(record);
	return record;
    }
    
    public BIXMLRecord removeRecord(int index) {
	return (BIXMLRecord)recordsVector.remove(index);
    }
    
    public void add( BIXMLRecord record ) {
	recordsVector.add(record);
    }

    public void add(BIXMLRecord record, int pos){
	recordsVector.add(pos, record);
    }
    
    public BIXMLRecord get( int index ) {
	return (BIXMLRecord) recordsVector.get(index);
    }
    
    public void remove( int index ) {
	recordsVector.remove(index);
    }


    public void removeAll(java.util.ArrayList aIndex){
	recordsVector.removeAll(aIndex);
    }
    
    public void removeAll(){
        recordsVector.clear();
    }
    
    public int size() {
	return recordsVector.size();
    }
    
    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	
	sb.append("<");
	sb.append(mainTag);
	if (attributes != null) {
	    sb.append(' ');
	    sb.append(attributes.toString());
	}
	sb.append(">\n");
	
	for (int i = 0; i < recordsVector.size(); i++)
	    sb.append(((BIXMLRecord)recordsVector.get(i)).toString());
	
	sb.append("</");
	sb.append(mainTag);
	sb.append(">");
	return sb.toString();
    }
    
    public String writeXML() {
	String utf8Converted = null;
	try {
	    String strAux = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" + toString();
	    byte[] utf8Bytes = strAux.getBytes("ISO-8859-1");
	    utf8Converted = new String(utf8Bytes, "ISO-8859-1");
	} catch (UnsupportedEncodingException e) {
	}
	return utf8Converted;
    }
    
    private boolean isVector( org.w3c.dom.Node node ) {
	boolean isVector = false;
	for ( int i = 0 ; (!isVector) && (i < node.getChildNodes().getLength() ); i++ )
	    isVector = ( node.getChildNodes().item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE );
	return isVector;
    }
    
    public void createTree(kpi.xml.BIXMLRecord record, org.w3c.dom.Node node) {
	// Obt�m os atributos.
	org.w3c.dom.NamedNodeMap attributes = node.getAttributes();
	if ( attributes != null ) {
	    for ( int j = 0; j < attributes.getLength(); j++ ) {
		record.getAttributes().set( attributes.item(j).getNodeName().trim(),
			attributes.item(j).getNodeValue().trim() );
	    }
	}
	
	for ( int i = 0 ; i < node.getChildNodes().getLength(); i++ ) {
	    org.w3c.dom.Node childNode = node.getChildNodes().item(i);
	    if ( childNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE ) {
		if ( isVector( childNode ) ) {
		    kpi.xml.BIXMLVector newVector = new kpi.xml.BIXMLVector();
		    createTree( newVector, childNode );
		    record.set( newVector );
		}
		// � um campo.
		else {
		    String value = new String();
		    for ( int k = 0; k < childNode.getChildNodes().getLength(); k++ ) {
			org.w3c.dom.Node valueNode = childNode.getChildNodes().item(k);
			if ( valueNode.getNodeType() == org.w3c.dom.Node.TEXT_NODE ) {
			    value = value + valueNode.getNodeValue().trim();
			}
		    }
		    record.set( childNode.getNodeName().trim(), value );
		    org.w3c.dom.NamedNodeMap recordAttributes = childNode.getAttributes();
		    if ( recordAttributes != null ) {
			for ( int w = 0; w < recordAttributes.getLength(); w++ ) {
			    record.getInternalAttributes(childNode.getNodeName().trim()).set(
				    recordAttributes.item(w).getNodeName().trim(),
				    recordAttributes.item(w).getNodeValue().trim() );
			}
		    }
		}
	    } else
		if ( childNode.getNodeType() == org.w3c.dom.Node.TEXT_NODE ) {
		String s = childNode.getNodeValue().trim();
		if (!"".equals( s ) )
		    record.set( s );
		}
	}
    }
    
    public void createTree(kpi.xml.BIXMLVector vector, org.w3c.dom.Node node) {
	vector.setMainTag( node.getNodeName().trim() );
	
	// Obt�m os atributos.
	org.w3c.dom.NamedNodeMap attributes = node.getAttributes();
	if ( attributes != null ) {
	    for ( int j = 0; j < attributes.getLength(); j++ ) {
		vector.getAttributes().set( attributes.item(j).getNodeName().trim(),
			attributes.item(j).getNodeValue().trim() );
	    }
	}
	
	// percorre os registros filhos.
	for ( int i = 0 ; i < node.getChildNodes().getLength(); i++ ) {
	    org.w3c.dom.Node childNode = node.getChildNodes().item(i);
	    if ( childNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE ) {
		kpi.xml.BIXMLRecord newRecord = new kpi.xml.BIXMLRecord(childNode.getNodeName().trim());
		createTree( newRecord, childNode );
		vector.add( newRecord );
	    }
	}
    }
    
    public void parseXML( String xml ) throws BIXMLException {
	if ( xml == null )
	    throw new BIXMLException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLVector_00007"));
	else
	    if ( xml.equals("") )
		throw new BIXMLException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLVector_00006"));
	    else {
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    try {
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream( new StringReader(xml) );
		Document document = builder.parse( is );
		createTree( this, document.getDocumentElement() );
	    } catch (ParserConfigurationException e) {
		throw new BIXMLException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLVector_00002")+
			java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLVector_00003"), e.getMessage());
	    } catch (IOException e) {
		throw new BIXMLException(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLVector_00001"), e.getMessage());
	    } catch (SAXException e) {
		throw new BIXMLException( java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLVector_00005")+
			java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BIXMLVector_00004") + e.getMessage(), xml );
	    }
	    }
    }
    
    @Override
    public boolean equals( Object obj ){
	boolean igual = false;
	if(obj instanceof BIXMLVector){
	    BIXMLVector vector = (BIXMLVector) obj;
	    igual = vector.toString().equals(toString());
	}
	return igual;
    }

    public Iterator<BIXMLRecord> iterator() {
	return recordsVector.iterator();
    }

    public boolean hasNext() {
	return recordsVector.iterator().hasNext();
    }

    public BIXMLRecord next() {
	return (BIXMLRecord)recordsVector.iterator().next();
    }

    public void remove() {
	recordsVector.iterator().remove();
    }    
}