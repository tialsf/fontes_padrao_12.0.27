#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "AVERAGE.CH"
#include 'eiccp400.ch'
#Include "TOPCONN.CH"

/*
Programa   : EICCP400
Objetivo   : Rotina - Catalogo de Produtos
Retorno    : Nil
Autor      : Ramon Prado
Data/Hora  : Dez /2019
Obs.       :
*/
function EICCP400(aCapaAuto,aItensAuto,nOpcAuto)
Local aArea       := GetArea()
Local aCores      := {}
Local nX          := 1
Local cModoAcEK9  := FWModeAccess("EK9",3)
Local cModoAcEKA  := FWModeAccess("EKA",3)
Local cModoAcEKB  := FWModeAccess("EKB",3)
Local cModoAcEKD  := FWModeAccess("EKD",3)
Local cModoAcEKE  := FWModeAccess("EKE",3)
Local cModoAcEKF  := FWModeAccess("EKF",3)
Local cModoAcSB1  := FWModeAccess("SB1",3)
Local cModoAcSA2  := FWModeAccess("SA2",3)
Local oBrowse

Private lCP400Auto   := ValType(aCapaAuto) <> "U" .Or. ValType(aItensAuto) <> "U" .Or. ValType(nOpcAuto) <> "U"
Private aRotina      := {}
Private aAtrib       := {}
Private cValor       := ""
Private cNCm         := "          "
Private cNcmAux      := ""
Private lRetAux      :=.T.
Private lMultiFil

   lMultiFil      := VerSenha(115) .And. cModoAcEK9 == "C" .And. cModoAcSB1 == "E" .And. cModoAcSA2 == "E"

   If !(cModoAcEK9 == cModoAcEKD .And. cModoAcEK9==cModoAcEKA .and. cModoAcEK9 == cModoAcEKE .And. cModoAcEK9==cModoAcEKB .And. cModoAcEK9 == cModoAcEKF)
      EasyHelp(STR0025,STR0014) // "Modo de compatilhamento esta diferente entre as tabelas. Verifique o modo das tabelas EK9, EKA, EKB,EKD, EKE e EKF "#Aten��o
   Else

      aCores := {	{"EK9_STATUS == '1' ","ENABLE"      ,STR0004	},; //"Registrado"
                  {"EK9_STATUS == '2' ","BR_CINZA"	   ,STR0005	},; //"Pendente Registro"
                  {"EK9_STATUS == '3' ","BR_AMARELO"	,STR0006	},; //"Pendente Retifica��o"
                  {"EK9_STATUS == '4' ","DISABLE"     ,STR0007	}}	//"Bloqueado"
      
      INCLUI := nOpcAuto == INCLUIR //Vari�vel INCLUI utilizada no dicion�rio de dados da ek9 para nao permitir alterar o ncm
      
      If !lCP400Auto
         oBrowse := FWMBrowse():New() //Instanciando a Classe
         oBrowse:SetAlias("EK9") //Informando o Alias 
         oBrowse:SetMenuDef("EICCP400") //Nome do fonte do MenuDef
         oBrowse:SetDescription(STR0001) // "Catalogo de Produtos" //Descri��o a ser apresentada no Browse   
      
         //Adiciona a legenda
         For nX := 1 To Len( aCores )   	    
            oBrowse:AddLegend( aCores[nX][1], aCores[nX][2], aCores[nX][3] )
         Next nX
         
         //Habilita a exibi��o de vis�es e gr�ficos
         oBrowse:SetAttach( .T. )
         //Configura as vis�es padr�o
         oBrowse:SetViewsDefault(GetVisions())
         
         //For�a a exibi��o do bot�o fechar o browse para fechar a tela
         oBrowse:ForceQuitButton()
         
         //Ativa o Browse                                                            
         oBrowse:Activate()
      Else
         //Defini��es de WHEN dos campos
         ALTERA := nOpcAuto == ALTERAR
         EXCLUI := nOpcAuto == EXCLUIR
      
         FWMVCRotAuto(ModelDef(), "EK9", nOpcAuto, {{"EK9MASTER",aCapaAuto}, {"EKADETAIL",aItensAuto}/*{"EYYDETAIL",aNFRem}*/ })
      EndIf
   EndIf

   RestArea(aArea)

Return Nil

/*
Programa   : Menudef
Objetivo   : Estrutura do MenuDef - Funcionalidades: Pesquisar, Visualizar, Incluir, Alterar e Excluir
Retorno    : aClone(aRotina)
Autor      : Ramon Prado
Data/Hora  : Dez/2019
Obs.       :
*/
Static Function MenuDef()
Local aRotina := {}

   aAdd( aRotina, { STR0008	, "AxPesqui"         , 0, 1, 0, NIL } )	//'Pesquisar'
   aAdd( aRotina, { STR0009	, 'VIEWDEF.EICCP400'	, 0, 2, 0, NIL } )	//'Visualizar'
   aAdd( aRotina, { STR0010   , 'VIEWDEF.EICCP400'	, 0, 3, 0, NIL } )	//'Incluir'
   aAdd( aRotina, { STR0011   , 'VIEWDEF.EICCP400'	, 0, 4, 0, NIL } )	//'Alterar'
   aAdd( aRotina, { STR0012   , 'VIEWDEF.EICCP400'	, 0, 5, 0, NIL } )	//'Excluir'
   aAdd( aRotina, { STR0013   , 'CP400Legen'	      , 0, 6, 0, NIL } )	//'Legenda'
   aAdd( aRotina, { STR0050   , 'CP400CadOE()'     , 0, 6, 0, NIL } )	//'"Cadastra Operador Estrangeiro" '
   aAdd( aRotina, { STR0045   , 'CP400Integrar()'  , 0, 6, 0, NIL } )	//'Integrar'
 
Return aRotina

/*
Programa   : ModelDef
Objetivo   : Cria a estrutura a ser usada no Modelo de Dados - Regra de Negocios
Retorno    : oModel
Autor      : Ramon Prado
Data/Hora  : 26/11/2019
Obs.       :
*/
Static Function ModelDef()
Local oStruEK9       := FWFormStruct( 1, "EK9", , /*lViewUsado*/ )
Local oStruEKA       := FWFormStruct( 1, "EKA", , /*lViewUsado*/ )
Local oStruEKB       := FWFormStruct( 1, "EKB", , /*lViewUsado*/ )
Local oStruEKC       := FWFormStruct( 1, "EKC", , /*lViewUsado*/ )
Local bCommit        := {|oModel| CP400COMMIT(oModel)}
Local bPosValidacao  := {|oModel| CP400POSVL(oModel)}
Local bCancel        := {|oModel| CP400CANC(oModel)}
Local oMdlEvent      := CP400EV():New()
Local bPreVldEKC     := {|oGridEKC, nLine, cAction, cIDField, xValue, xCurrentValue| EKCLineValid(oGridEKC, nLine, cAction, cIDField, xValue, xCurrentValue)}
Local bPosVldEKA     := {|oGridEKA| EKALineValid(oGridEKA)}
Local oModel         // Modelo de dados que ser� constru�do	
   
   // Cria��o do Modelo
   oModel := MPFormModel():New( "EICCP400", /*bPreValidacao*/, bPosValidacao, bCommit, bCancel )
   //step 2
   //oModel:setOperation(4)
   If !lMultiFil
      oStruEK9:RemoveField("EK9_FILORI")
   EndIf

   // Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
   oModel:AddFields("EK9MASTER", /*cOwner*/ ,oStruEK9 )
   oModel:SetPrimaryKey( { "EK9_FILIAL", "EK9_COD_I"} )	

   // Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid - Rela��o de Produtos
   oModel:AddGrid("EKADETAIL","EK9MASTER", oStruEKA, /*bLinePre*/ , bPosVldEKA, /*bPreVal*/ , /*bPosVal*/, /*BLoad*/ )
   oStruEKA:RemoveField("EKA_COD_I")

   // Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid - Fabricantes
   oModel:AddGrid("EKBDETAIL","EK9MASTER", oStruEKB, /*bLinePre*/ ,/*bLinePost*/, /*bPreVal*/ , /* bPosValEKB */, /*BLoad*/ )
   oModel:GetModel("EKBDETAIL"):SetOptional( .T. ) //Pode deixar o grid sem preencher nenhum Fabricante
   oStruEKB:RemoveField("EKB_COD_I")

   // Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid - Atributos
   oModel:AddGrid("EKCDETAIL","EK9MASTER", oStruEKC, bPreVldEKC ,/*bLinePost*/, /*bPreVal*/ , /*bPosVal*/, /*BLoad*/ )
   oModel:GetModel("EKCDETAIL"):SetOptional( .T. ) 
   oModel:GetModel("EKCDETAIL"):SetNoInsertLine(.T.) 
   //oModel:GetModel("EKCDETAIL"):SetDelAllLine(.T.)
   oModel:GetModel("EKCDETAIL"):SetNoDeleteLine(.F.)
   //oModel:GetModel("EKCDETAIL"):SetNoUpdateLine(.T.)

   oStruEKC:RemoveField("EKC_COD_I")
   //oStruEKC:RemoveField("EKC_VERSAO")

   If !lMultiFil
      oStruEKA:RemoveField("EKA_FILORI")
   EndIf

   If !lMultiFil
      oStruEKB:RemoveField("EKB_FILORI")
   EndIf

   //Modelo de rela��o entre Capa - Produto Referencia(EK9) e detalhe Rela��o de Produtos(EKA)
   oModel:SetRelation('EKADETAIL', {{ 'EKA_FILIAL' , 'xFilial("EKA")' },;
                                    { 'EKA_COD_I'  , 'EK9_COD_I'     }},;
                                    EKA->(IndexKey(1)) )
                                 
   oModel:SetRelation('EKBDETAIL', {{ 'EKB_FILIAL' , 'xFilial("EKB")' },;
                                    { 'EKB_COD_I'  , 'EK9_COD_I'     }},;
                                    EKB->(IndexKey(1)) )

   oModel:SetRelation('EKCDETAIL', {{ 'EKC_FILIAL'	, 'xFilial("EKC")' },;
                                    { 'EKC_COD_I'  , 'EK9_COD_I'     }},;
                                    EKC->(IndexKey(1)) )
                                 
   If lMultiFil
      oModel:GetModel("EKADETAIL"):SetUniqueLine({"EKA_PRDREF","EKA_FILORI"} )
   Else
      oModel:GetModel("EKADETAIL"):SetUniqueLine({"EKA_PRDREF"} )
   EndIf	

   If lMultiFil
      oModel:GetModel("EKBDETAIL"):SetUniqueLine({"EKB_CODFAB","EKB_LOJA","EKB_FILORI"} )
   Else
      oModel:GetModel("EKBDETAIL"):SetUniqueLine({"EKB_CODFAB","EKB_LOJA"} )
   EndIf	

   //Adiciona a descri��o do Componente do Modelo de Dados
   oModel:GetModel("EK9MASTER"):SetDescription(STR0001) //"Catalogo de Produtos"
   oModel:SetDescription(STR0001) // "Catalogo de Produtos"
   oModel:GetModel("EKADETAIL"):SetDescription(STR0014) //'Detalhes do Catalogo de Produtos'
   oModel:GetModel("EKBDETAIL"):SetDescription(STR0024) //"Rela��o de Fabricantes"
   oModel:GetModel("EKCDETAIL"):SetDescription(STR0040) //"Rela��o de Atributos"

   oModel:InstallEvent("CP400EV", , oMdlEvent)
   //step 2
   //oModel:setOperation(4)
Return oModel

/*
Programa   : ViewDef
Objetivo   : Cria a estrutura Visual - Interface
Retorno    : oView
Autor      : Ramon Prado
Data/Hora  : 26/11/2019
Obs.       :
*/
Static Function ViewDef()
Local oStruEK9 := FWFormStruct( 2, "EK9" )
Local oStruEKA := FWFormStruct( 2, "EKA" )
Local oStruEKB := FWFormStruct( 2, "EKB" )
Local oStruEKC := FWFormStruct( 2, "EKC" )
Local oModel   := FWLoadModel( "EICCP400" )
Local oView

   //Cria o objeto de View
   oView := FWFormView():New()

   // Adiciona no nosso View um controle do tipo formul�rio 
   //Define qual o Modelo de dados ser� utilizado na View
   oView:SetModel( oModel )

   oView:SetContinuousForm(.T.)
   oView:CreateHorizontalBox( 'TELA', 10)
   // (antiga Enchoice)
   oView:AddField( 'VIEW_EK9', oStruEK9, 'EK9MASTER' )
   // Relaciona o identificador (ID) da View com o "box" para exibi��o
   oView:SetOwnerView( 'VIEW_EK9', 'TELA' )
   //Identifica��o do componente
   oView:EnableTitleView( "VIEW_EK9", STR0001 ) //"Catalogo de Produtos(Capa)"

   //oView:CreateHorizontalBox( 'SUPERIOR'  , 30 )
   oView:CreateHorizontalBox( 'INFERIOR_EKA'  , 30 )

   If !lMultiFil
      oStruEK9:RemoveField("EK9_FILORI")
   EndIf

   //Adiciona no nosso View um controle do tipo FormGrid(antiga getdados)
   oView:AddGrid("VIEW_EKA",oStruEKA , "EKADETAIL")
   oStruEKA:RemoveField("EKA_COD_I")
   //Identifica��o do componente
   oView:EnableTitleView( "VIEW_EKA", STR0015 ) //"Rela��o de Produtos"

   oView:SetOwnerView( "VIEW_EKA", 'INFERIOR_EKA' )

   //Adiciona no nosso View um controle do tipo FormGrid(antiga getdados)
   oView:AddGrid("VIEW_EKB",oStruEKB , "EKBDETAIL")
   oStruEKB:RemoveField("EKB_COD_I")

   //Identifica��o do componente
   oView:EnableTitleView( "VIEW_EKB", STR0026 ) //"Rela��o de Fabricantes"

   oView:CreateHorizontalBox( 'INFERIOR_EKB'  , 30 )

   oView:SetOwnerView( "VIEW_EKB", 'INFERIOR_EKB' )

   //GRID DE ATRIBUTOS
   //Adiciona no nosso View um controle do tipo FormGrid(antiga getdados)
   oView:AddGrid("VIEW_EKC",oStruEKC , "EKCDETAIL")
   oStruEKC:RemoveField("EKC_COD_I")
   oStruEKC:RemoveField("EKC_VERSAO")
   oStruEKC:RemoveField("EKC_VALOR")
   //Step 1
   oView:SetViewProperty("EKCDETAIL", "GRIDDOUBLECLICK", {{|oFormulario,cFieldName,nLineGrid,nLineModel| EKCDblClick(oFormulario,cFieldName,nLineGrid,nLineModel)}})  
   //Step 1
   //Identifica��o do componente
   oView:EnableTitleView( "VIEW_EKC", "Rela��o de Atributos") //"Rela��o de Atributos"

   oView:CreateHorizontalBox( 'INFERIOR_EKC'  , 30 )

   oView:SetOwnerView( "VIEW_EKC", 'INFERIOR_EKC' )

   If !lMultiFil
      oStruEKA:RemoveField("EKA_FILORI")
   EndIf

   If !lMultiFil
      oStruEKB:RemoveField("EKB_FILORI")
   EndIf

   oView:AddIncrementField( 'VIEW_EKA', 'EKA_ITEM' )

Return oView

/*
Programa   : CP400CANC
Objetivo   : A��o ao clicar no botao cancelar
Retorno    : Logico
Autor      : Ramon Prado
Data/Hora  : Dez/2019
Obs.       :
*/
Static Function CP400CANC(oMdl)

   RollbackSx8()

Return .T.

/*
Programa   : CP400POSVL
Objetivo   : Funcao de Pos Validacao
Retorno    : Logico
Autor      : Ramon Prado
Data/Hora  : Dez/2019
Obs.       :
*/
Static Function CP400POSVL(oMdl)
Local aArea			:= GetArea()
Local lRet			:= .T.
Local oModelEK9	:= oMdl:GetModel("EK9MASTER")
Local oModelEKA	:= oMdl:GetModel("EKADETAIL")
Local oModelEKB	:= oMdl:GetModel("EKBDETAIL")
Local oModelEKC	:= oMdl:GetModel("EKCDETAIL")
Local nI			   := 1
Local nJ          := 1
Local cMsg			:= STR0027 + ENTER + ENTER //" Fabricante(s)/Fornecedor(s) n�o relacionado(s) ao(s) produto(s): "
Local cMsgComp		:= ""
Local cIdManu     := ""
Local cVsManu     := ""
Local cErro       := ""
Local lPosicEKD   := .F.
Local lAchouFabr  := .F. //procura amarra�ao Produto x Fabr/Forn 
Local lEasyhelp   := .F. 

   EKD->(DbSetOrder(1)) //Filial + Cod.Item Cat + Vers�o
   If EKD->(AvSeekLAst( xFilial("EKD") + oModelEK9:GetVAlue("EK9_COD_I") ))
      lPosicEKD := .T.
   EndIf

   If oMdl:GetOperation() == 5 //Exclus�o
      if Alltrim(oModelEK9:GetValue("EK9_STATUS")) <> "2"
         lRet := .F.
         lEasyhelp := .T.
         EasyHelp(STR0003,STR0002) //"Apenas � poss�vel excluir Catalogo de produto com Status 'Registro Pendente' "##"Aten��o"
      EndIf
      
      If lPosicEKD .And. EKD->EKD_STATUS == '1' .Or. EKD->EKD_STATUS == '3' //Integrado ou Cancelado
         lRet := .F.
         lEasyhelp := .T.
         EasyHelp(STR0048,STR0002) //"N�o � poss�vel excluir Catalogo de Produtos que possua integra��o com status Integrado ou Cancelado" //"Apenas � poss�vel excluir Catalogo de produto com Status 'Registro Pendente' "##"Aten��o"
      EndiF   
      
      cIdManu  := oModelEK9:GetValue("EK9_IDMANU")
      cVsManu  := oModelEK9:GetValue("EK9_VSMANU")
      If !Empty(cIdManu) .And. Empty(cVsManu)
         lRet := .F.	
         lEasyhelp := .T.
         EasyHelp(STR0037,STR0002) // "Ao preencher o campo ID Manual tamb�m ser� necess�rio preencher o campo Vers�o Manual"##"Aten��o"
      EndIf

      If Empty(cIdManu) .And. !Empty(cVsManu)
         lRet := .F.	
         lEasyhelp := .T.
         EasyHelp(STR0041,STR0002) // "Ao preencher o campo Vers�o Manual tamb�m ser� necess�rio preencher o campo ID Manual"##"Aten��o"
      EndIf
      
      If lRet .And. lPosicEKD
         If !TemIntegEKD(oModelEK9:GetVAlue("EK9_COD_I")) .And. EKD->EKD_STATUS == '2' //nao achou registros Integrados ou Cancelados e a Ultima versao � "Nao Integrado"            
            cErro := ExcIntegr(EKD->EKD_COD_I, EKD->EKD_VERSAO ) //Exclusao de registro N�o Integrado     
         Else
            lRet := .F.	
            lEasyhelp := .T.
            EasyHelp(STR0044,STR0002) // "N�o � poss�vel excluir Catalogo de Produtos que possua integra��o com status Integrado ou Cancelado""##"Aten��o"
         EndIf
      EndIf

   ElseIf	oMdl:GetOperation() == 3 .Or. oMdl:GetOperation() == 4 //Inclus�o ou Altera��o	
      If oModelEKB:Length() > 0
         oModelEKB:GoLine(1)
         For nI := 1 to oModelEKB:Length()
            oModelEKB:GoLine( nI )
            If !oModelEKB:IsDeleted() .And. !Empty(oModelEKB:GetValue("EKB_CODFAB")) .And. !Empty(oModelEKB:GetValue("EKB_LOJA")) 

               For nJ := 1 to oModelEKA:Length()
                  oModelEKA:GoLine( nJ )
                  If !oModelEKA:IsDeleted() .And. !Empty(oModelEKA:GetValue("EKA_PRDREF"))
                     If lMultiFil            
                        lAchouFabr := CP400VlSA5(oModelEKB:GetValue("EKB_CODFAB"), oModelEKB:GetValue("EKB_LOJA"), oModelEKA:GetValue("EKA_PRDREF"), oModelEKA:GetValue("EKA_FILORI"))				
                        If lAchouFabr
                           Exit
                        EndIf
                     Else
                        lAchouFabr := CP400VlSA5(oModelEKB:GetValue("EKB_CODFAB"), oModelEKB:GetValue("EKB_LOJA"), oModelEKA:GetValue("EKA_PRDREF"))				
                        If lAchouFabr
                           Exit
                        EndIf
                     EndIf
                  EndIf
               Next nJ

               If !lAchouFabr
                  cMsgComp +=  STR0028+Alltrim(oModelEKB:GetValue("EKB_CODFAB"))+STR0029+Alltrim(oModelEKB:GetValue("EKB_LOJA"))+STR0030 //"Fabr./Fornec.: "##### "Loja"##" Raz�o Social: ##############"
                  lRet := .F.
                  If lMultiFil
                     cMsgComp += Alltrim(POSICIONE("SA2",1,oModelEKB:GetValue("EKB_FILORI")+oModelEKB:GetValue("EKB_CODFAB")+oModelEKB:GetValue("EKB_LOJA"),"A2_NOME")) + ENTER          
                  Else
                     cMsgComp +=  Alltrim(POSICIONE("SA2",1,xFilial("SA2")+oModelEKB:GetValue("EKB_CODFAB")+oModelEKB:GetValue("EKB_LOJA"),"A2_NOME")) + ENTER
                  EndIf

                  If IsMemVar("lCP400Auto") .And. lCP400Auto  //rotina automatica
                     exit
                  EndIf
               EndIf
            EndIf

         Next nI

         If !Empty(cMsgComp)
            If  IsMemVar("lCP400Auto") .And. !lCP400Auto
               lRet := .F.
               lEasyhelp := .T.
               cMsg += cMsgComp
               oMdl:SetErrorMessage( 'EKBDETAIL', , , , STR0002, cMsg,STR0031 ) // Aten��o ## "Verifique o Cadastro Produto X Fornecedor"				
            Else
               lRet := .F.
               lEasyhelp := .T.
               EasyHelp(STR0023+cMsgComp,STR0002) //"Existe(m) Fabricante(s) n�o relacionado(s) ao(s) produto(s)##"Aten��o"
            EndIf
         EndIf 
      EndIf

      If oModelEKC:Length() > 0
         For nI := 1 to oModelEKC:Length()
            oModelEKC:GoLine( nI )
            if LEFT(oModelEKC:GetValue("EKC_NOME"),1) == "*" ;
               .AND. Empty(oModelEKC:GetValue("EKC_VALOR")) ;
               .AND. oModelEKC:GetValue("EKC_STATUS") == "VIGENTE"
                  lRet := .F.	
                  lEasyhelp := .T.
                  EasyHelp(StrTran(STR0033, "###", oModelEKC:GetValue("EKC_CODATR"))) //"Atributo: ### � de preenchimento obrigat�rio e est� vazio. Informe um valor para este campo e salve novamente."
            EndIf
         Next
         if lRet
         For nI := 1 to oModelEKC:Length()
            oModelEKC:GoLine( nI )
            If Empty(oModelEKC:GetValue("EKC_VALOR"))
                  oModelEKC:DeleteLine()
            EndIf
         Next
         EndIf
      EndIf

      cIdManu  := oModelEK9:GetValue("EK9_IDMANU") 
      cVsManu  := oModelEK9:GetValue("EK9_VSMANU") 
      If !Empty(cIdManu) .And. Empty(cVsManu)
         lRet := .F.	
         lEasyhelp := .T.
         EasyHelp(STR0037,STR0002) // "Ao preencher o campo ID Manual tamb�m ser� necess�rio preencher o campo Vers�o Manual"##"Aten��o"
      EndIf

      If Empty(cIdManu) .And. !Empty(cVsManu)
         lRet := .F.	
         lEasyhelp := .T.
         EasyHelp(STR0041,STR0002) // "Ao preencher o campo ID Manual tamb�m ser� necess�rio preencher o campo Vers�o Manual"##"Aten��o"
      EndIf
      
      If(!empty(cVsManu) .and. lPosicEKD .And. (EKD->EKD_STATUS == '1' .Or. EKD->EKD_STATUS == '3'))
         If cVsManu < EKD->EKD_VERSAO
            lRet := .F.   
            lEasyhelp := .T.
            EasyHelp(StrTran(STR0038, "###", EKD->EKD_VERSAO),STR0002) //"A vers�o informada manualmente � menor que a vers�o ### Integrada ou Cancelada"###"Aten��o"
         EndIf
      EndIf
   EndIf

   If !Empty(cErro)
      lRet := .F.
      lEasyhelp := .T.
      EasyHelp(cErro,STR0002) //apresenta a mensagem de Erro da Rotima Automatica ExecAuto ## Aten��o
   EndIf

   RestArea(aArea)

   if lCP400Auto .and. !lRet .and. lEasyhelp
      lRet := .T.
   endif

Return lRet
/*
Programa   : CP400COMMIT
Objetivo   : Funcao de Commit - utilizado para campos cujo formulario do mvc nao grava  
Retorno    : Logico
Autor      : Ramon Prado
Data/Hora  : Mar/2020
Obs.       :
*/
Static Function CP400COMMIT(oMdl)
Local cData       := DTOC(Date())
Local cHora       := SUBSTR(TIME(), 1, 2)+":"+SUBSTR(TIME(), 4, 2) //Hora(s) + Minuto(s)
Local oModelEK9	:= oMdl:GetModel("EK9MASTER")
Local oGridEKB    := oMdl:GetModel("EKBDETAIL")
Local cIdManu     := ""
Local cVsManu     := ""
Local cErro       := ""
Local cStatusEK9  := "" 
Local lRet        := .T.
Local lPosicEKD   := .F.
Local lRetDif     := .F.
Local aOEs        := {}
Local lPendRetif  := .F.

   EKD->(DbSetOrder(1)) //Filial + Cod.Item Cat + Vers�o
   If EKD->(AvSeekLAst( xFilial("EKD") + oModelEK9:GetVAlue("EK9_COD_I") ))
      lPosicEKD := .T.
   EndIf

   If oMdl:GetOperation() == 3 .Or. oMdl:GetOperation() == 4 //Inclusao ou Alteracao
      // Caso tenha operador estrangeiro relacionado ao produto n�o cadastrado cadastra via rotina autom�tica
      if CP400OEValid(oGridEKB,oModelEK9,@aOEs)
         CP400ExecEKJ(aOEs)
         oMdl:activate()
      endif
   endif

   Begin Transaction
       
      If oMdl:GetOperation() == 3 .Or. oMdl:GetOperation() == 4 //Inclusao ou Alteracao

         cIdManu  := oModelEK9:GetValue("EK9_IDMANU")
         cVsManu  := oModelEK9:GetValue("EK9_VSMANU")
         If !Empty(cIdManu) .And. !Empty(cVsManu) 
            oModelEK9:SetValue("EK9_ULTALT",cUserName+" "+cData + " " +cHora)
            oModelEK9:SetValue("EK9_IDPORT", oModelEK9:GetValue("EK9_IDMANU") )
            oModelEK9:SetValue("EK9_VATUAL", oModelEK9:GetValue("EK9_VSMANU") )
         EndIf

         If Empty(cVsManu) .Or. EK9->EK9_VATUAL >= cVsManu
            lRetDif := VerificDif(oMdl)
            If lRetDif .And. !Empty(cVsManu) .And. cVsManu == EK9->EK9_VATUAL
               If lRetDif .And. !MsgNoYes(STR0043) //"O catalogo alterado foi registrado manualmente no portal �nico, 
                                                //deseja gerar uma nova vers�o para integra��o autom�tica pelo sistema com os dados informados?"
                  lRetDif := .F. 
               EndIf
            EndIf
         EndIf

         If oMdl:GetOperation() == 4 .and. lRetDif
            If lPosicEKD .And. ((!Empty(cVsManu) .And. cVsManu > EKD->EKD_VERSAO) .or. (Empty(cVsManu)) ) .And. EKD->EKD_STATUS == '2' //Pendente Registro
               cErro := ExcIntegr(EKD->EKD_COD_I, EKD->EKD_VERSAO ) //Exclusao de registro N�o Integrado
            ElseIf lPosicEKD .And. ((!Empty(cVsManu) .And. cVsManu > EKD->EKD_VERSAO) .or. (Empty(cVsManu)) ) .And. EKD->EKD_STATUS $ '1' //Registrado
               cErro := CancInteg(EKD->EKD_COD_I, EKD->EKD_VERSAO ) //Cancelamento de registro Integrado
               lPendRetif := .T.
            EndIf
         EndIf

         if lRetDif
            if lPendRetif .or. EK9->EK9_STATUS == "3"
               cStatusEK9 := "3"
            elseif oMdl:GetOperation() == 3
               cStatusEK9 := "2"
            elseif oModelEK9:GetValue("EK9_MSBLQL") == "1" 
               cStatusEK9 := "4"
            else
               cStatusEK9 := "2"
            endif
         EndIf

         If Empty(cErro)
            FWFormCommit(oMdl)
            //havendo diferen�as, versao manual igual a versao atual - resposta Sim para a pergunta de gerar Inclusao de Integracao Cat Prod.
            If lRetDif .and. oModelEK9:GetValue("EK9_MSBLQL") <> "1" 
               cErro := IncluInteg(oModelEK9)
               if empty(cErro) .and. reclock("EK9",.F.)
                  EK9->EK9_VATUAL := EKD->EKD_VERSAO
                  EK9->EK9_STATUS := cStatusEK9
                  EK9->(msunlock())
               endif
            EndIf

         Else
            DisarmTransaction()
         EndIf
      EndIf 

      If oMdl:GetOperation() == 5
         FWFormCommit(oMdl)
         If lPosicEKD
            cErro := ExcIntegr(EKD->EKD_COD_I, EKD->EKD_VERSAO ) //Exclusao de registro N�o Integrado     
         EndIf
      EndIf

   End Transaction

   //primeiro commita o catalogo com a altera��o do campo para depois incluir a integra��o e se houver erro a transa��o de inclusao
   //sera desarmada pela rotina EICCP401
   If !Empty(cErro)
      EECVIEW(cErro,STR0002) //apresenta a mensagem de Erro da Rotima Automatica ExecAuto ## Aten��o 
      lRet := .F.
   EndIf

Return lRet

/*
Programa   : IncluInteg
Objetivo   : Funcao utilizada para Incluir integra��o do cat�logo de produtos 
Retorno    : Caractere - Erro da Execu��o Automatica
Autor      : Ramon Prado
Data/Hora  : Maio/2020
Obs.       :
*/
Static Function IncluInteg( oModelEK9 )
Local aCapaEKD := {}
Local aErros   := {}
Local nJ       := 1
Local cLogErro := ""

Private lMsHelpAuto     := .T. 
Private lAutoErrNoFile  := .T.
Private lMsErroAuto     := .F.

   aAdd(aCapaEKD,{"EKD_FILIAL", xFilial("EKD")                    , Nil})
   aAdd(aCapaEKD,{"EKD_COD_I"	, oModelEK9:GetValue("EK9_COD_I")   , Nil})

   MSExecAuto({|a,b| EICCP401 (a,b)}, aCapaEKD, 3)

   If lMsErroAuto
      aErros := GetAutoGRLog()
      For nJ:= 1 To Len(aErros)
         cLogErro += aErros[nJ]+ENTER
      Next nJ
   EndIf

Return cLogErro

/*
Programa   : VerificDif(oMdl)
Objetivo   : Funcao utilizada para Incluir integra��o do cat�logo de produtos 
Retorno    : Caractere - Erro da Execu��o Automatica
Autor      : Ramon Prado
Data/Hora  : Maio/2020
Obs.       :
*/
Static Function VerificDif(oMdl)
Local oModelEK9   := oMdl:GetModel("EK9MASTER")
Local oModelEKA   := oMdl:GetModel("EKADETAIL")
Local oModelEKB   := oMdl:GetModel("EKBDETAIL")
Local oModelEKC   := oMdl:GetModel("EKCDETAIL")
Local nI          := 0
Local lAchouDif   := .F.
Local cChaveEKA   := ""
Local cChaveEKB   := ""
Local cChaveEKC   := ""

   begin sequence

      If EK9->EK9_IDPORT <> oModelEK9:GetValue("EK9_IDPORT") .Or. EK9->EK9_VATUAL <> oModelEK9:GetValue("EK9_VATUAL") .Or. ;
         EK9->EK9_IMPORT <> oModelEK9:GetValue("EK9_IMPORT") .Or. EK9->EK9_CNPJ   <> oModelEK9:GetValue("EK9_CNPJ")   .Or. ;
         EK9->EK9_MODALI <> oModelEK9:GetValue("EK9_MODALI") .Or. EK9->EK9_NCM    <> oModelEK9:GetValue("EK9_NCM")    .Or. ;
         EK9->EK9_UNIEST <> oModelEK9:GetValue("EK9_UNIEST") .Or. EK9->EK9_NALADI <> oModelEK9:GetValue("EK9_NALADI") .Or. ;
         EK9->EK9_GPCBRK <> oModelEK9:GetValue("EK9_GPCBRK") .Or. EK9->EK9_GPCCOD <> oModelEK9:GetValue("EK9_GPCCOD") .Or. ;
         EK9->EK9_UNSPSC <> oModelEK9:GetValue("EK9_UNSPSC") .Or. EK9->EK9_STATUS <> oModelEK9:GetValue("EK9_STATUS") .Or. ;
         EK9->EK9_DSCCOM <> oModelEK9:GetValue("EK9_DSCCOM") .Or. EK9->EK9_RETINT <> oModelEK9:GetValue("EK9_RETINT") .Or. ;
         EK9->EK9_MSBLQL <> oModelEK9:GetValue("EK9_MSBLQL") .or. ;
         (lMultiFil .AND. EK9->EK9_FILORI <> oModelEK9:GetValue("EK9_FILORI")) 
            lAchouDif := .T.
            break
      EndIf

      For nI := 1 To oModelEKA:Length()
         oModelEKA:GoLine( nI )
         if ! oModelEKA:isdeleted()
            cChaveEKA := xFilial("EKA") + PADR(oModelEK9:GetValue("EK9_COD_I"),AVSX3("EKA_COD_I",3)) + oModelEKA:GetValue("EKA_PRDREF")
            If ! EKA->(DbSetOrder(1),MsSeek(cChaveEKA))
               lAchouDif := .T.
               break
            EndIf
         else
            lAchouDif := .T.
            break
         EndIf
      Next nI
      
      For nI := 1 To oModelEKB:Length()
         oModelEKB:GoLine( nI )
         if ! oModelEKB:isdeleted()
            cChaveEKB := xFilial("EKB") + PADR(oModelEK9:GetValue("EK9_COD_I"),AVSX3("EKB_COD_I",3)) + oModelEKB:GetValue("EKB_CODFAB") + oModelEKB:GetValue("EKB_LOJA")
            If ! EKB->(dbsetorder(1),MsSeek(cChaveEKB))
               lAchouDif := .T.
               break
            EndIf
         else
            lAchouDif := .T.
            break
         endif
      Next nI

      For nI := 1 To oModelEKC:Length()
         oModelEKC:GoLine( nI )
         if ! oModelEKB:isdeleted()
            cChaveEKC := xFilial("EKC") + PADR(oModelEK9:GetValue("EK9_COD_I"),AVSX3("EKC_COD_I",3)) + oModelEKC:GetValue("EKC_CODATR")
            If EKC->(dbsetorder(1),MsSeek( cChaveEKC ))
               If EKC->EKC_VERSAO <> oModelEKC:GetValue("EKC_VERSAO") .Or. ;
                  EKC->EKC_VALOR  <> oModelEKC:GetValue("EKC_VALOR")
                  lAchouDif := .T.
                  break
               EndIf
            elseIf !Empty(oModelEKC:GetValue("EKC_VALOR"))
               lAchouDif := .T.
               break
            EndIf
         else
            lAchouDif := .T.
            break
         endif
      Next nI

   end sequence

Return lAchouDif
/*/{Protheus.doc} CP400Integrar
   Fun��o para realizar a integra��o do operador estrangeiro com o siscomex
   @author Miguel Prado Gontijo
   @since 16/06/2020
   @version 1
   @param aCatalogos - array com o recno do cat�logo de produtos a ser integrado, se vazio registra o posicionado no browse
   @return Nil
   /*/
Function CP400Integrar(aCatalogos)
Local cURLTest    := EasyGParam("MV_EIC0073",.F.,"https://val.portalunico.siscomex.gov.br") // Teste integrador localhost:3001 - val.portalunico.siscomex.gov.br
Local cURLProd    := EasyGParam("MV_EIC0072",.F.,"https://portalunico.siscomex.gov.br") // Produ��o - portalunico.siscomex.gov.br 
Local lIntgProd   := EasyGParam("MV_EIC0074",.F.,"1") == "1"
Local cVers�oEKD  := ""
Local cErros      := ""
Local lRet        := .T.
Local oProcess

Private cURLIACP    := "/catp/api/ext/produto"
Private cURLAuth    := "/portal/api/autenticar"
Private cCPPathAuth := ""
Private cPathIACP   := ""

Default aCatalogos  := {}

   begin sequence

        // Caso n�o receba par�metro faz a inclus�o do registro posicionado 
         if len(aCatalogos) == 0
            cVers�oEKD := CPGetVersion(xFilial("EKD"),EK9->EK9_COD_I)
            if ! empty(cVers�oEKD) .and. EKD->(dbsetorder(1),msseek(xFilial("EKD")+EK9->EK9_COD_I+cVers�oEKD))
               If EKD->EKD_STATUS $ "2|4"
                  aadd(aCatalogos, EKD->(recno()) )
               Else
                  MsgInfo( StrTran(STR0070,"######",EK9->EK9_COD_I),STR0021) //"Cat�logo ###### com status igual a Integrado ou Cancelado n�o pode ser integrado" //Aviso
                  lRet := .F.
                  break
               EndIf
            endif
         endif

         if ! lIntgProd 
            // se n�o for execauto exibe a pergunta se n�o segue como sim
            if ! lCP400Auto .and. ;
               ! msgnoyes( STR0051 + ENTER ; // "O sistema est� configurado para integra��o com a Base de Testes do Portal �nico."
                         + STR0052 + ENTER ; // "Qualquer integra��o para a Base de Testes n�o ter� qualquer efeito legal e n�o deve ser utilizada em um ambiente de produ��o."
                         + STR0053 + ENTER ; // "Para integrar com a Base Oficial (Produ��o) do Portal �nico, altere o par�metro 'MV_EEC0054' para 1."
                         + STR0054 , STR0002 ) // "Deseja Prosseguir?" // "Aten��o"
                  lRet := .F.
                  break
            else
               cCPPathAuth := cURLTest+cURLAuth
               cPathIACP := cURLTest+cURLIACP
            endif
         else
            cCPPathAuth := cURLProd+cURLAuth
            cPathIACP := cURLProd+cURLIACP
         endif


      if ! lCP400Auto
         oProcess := MsNewProcess():New({|lEnd| lRet := CP400Sicomex(aCatalogos,cCPPathAuth,cPathIACP,oProcess,lEnd,@cErros) },;
                    STR0055 , STR0056 ,.T.) // "Integrar Cat�logo de Produtos" , "Processando integra��o"
         oProcess:Activate()
      else
         lRet := CP400Sicomex(aCatalogos,cCPPathAuth,cPathIACP,oProcess,.F.,@cErros)
      endif

      if ! empty(cErros) .and. ! lRet
         EECView(cErros,STR0067) //"Aten��o"
       ElseIf lRet
         MsgInfo(STR0047,STR0021) //"Integrado com sucesso" //"Aviso"
      endif

   end sequence

Return
/*/{Protheus.doc} CP400Sicomex
   Fun��o que realiza a integra��o com o siscomex para cada item do array aCatalogos
   @author Miguel Prado Gontijo
   @since 16/06/2020
   @version 1
   /*/
Function CP400Sicomex(aCatalogos,cCPPathAuth,cPathIACP,oProcess,lEnd,cErros)
Local cRet           := ""
Local cAux           := ""
Local cPerg          := ""
Local cCodigo        := ""
Local cChaveEKJ      := ""
Local cChaveEKI      := ""
Local cChaveEKE      := ""
Local cSucesso       := ""
Local ctxtJson       := ""
Local cJsonAtt       := ""
Local cCodigoOE      := ""
Local cModalidade    := ""
Local cPaisOrigem    := ""
Local cCodinterno    := ""
Local cCpfCnpjFabric := ""
Local cFabrConhecido := "false"

Local aJson          := {}
Local aJsonErros     := {}
Local aOperadores    := {}

Local lRet           := .T.
Local nj
Local nCP
Local nContCP        := 0
Local nQtdInt        := len(aCatalogos)

Local oJson
Local oEasyJS

Local xRetJson

   begin sequence
      // se n�o for execauto alimenta reguas de processamento
      if ! lCP400Auto
         oProcess:SetRegua1(nQtdInt)
      endif

      // percorre o array de cat�logos a serem entregados
      for nCP := 1 to nQtdInt
         
         //houve cancelamento do processo
         If lEnd
            lRet := .F.
            break
         EndIf
         
         // posiciona na capa do integrador
         EKD->(dbgoto(aCatalogos[nCP]))
         EK9->(dbsetorder(1),msseek(xFilial("EK9")+EKD->EKD_COD_I))
         // se for diferente de registrado e bloqueado
         If EKD->EKD_STATUS $ "2|4"

            // se n�o for execauto alimenta reguas de processamento
            if ! lCP400Auto
               oProcess:IncRegua1( "Integrando:" + alltrim(EKD->EKD_COD_I) + "/" + alltrim(EKD->EKD_VERSAO) ) // 
               oProcess:IncRegua1( "Integrando:" + alltrim(EKD->EKD_COD_I) + "/" + alltrim(EKD->EKD_VERSAO) ) // 
               oProcess:SetRegua2(1)
            endif

            // valida��o dos operadores estrangeiros se est�o cadastrados e registrados
            if CP400VldOE(@cErros,@cPerg,@aOperadores)

               // se houver operador estrangeiro no cat�logo de produtos e n�o no integrador aborta a integra��o
               if ! empty(cErros)
                  lRet := .F.
                  break
               endif

               // caso tenha operadores que ainda n�o estejam integrados com portal �nico 
               if ! empty(cPerg) .and. len(aOperadores) > 0
                  // � perguntado se deseja o envio dos mesmos, caso n�o aborta integra��o do cat�logo
                  if ! msgnoyes( STR0057 + ENTER ; //"Os operadores estrangeiros abaixo n�o est�o integrados"  
                     + cPerg ;
                     + STR0058 , STR0002 ) // "Deseja integrar agora?" // "Aten��o"
                        lRet := .F.
                        break
                  else
                     lOE400Auto := .F.
                        if !OE400Integrar(aOperadores,.T.)
                           lRet := .F.
                           break
                        endif
                  endif
               endif
            endif

            if EKD->EKD_MODALI == "1" // =Importacao
               cModalidade := "IMPORTACAO"
            elseif EKD->EKD_MODALI == "2" // =Exportacao
               cModalidade := "EXPORTACAO"
            elseif empty(EKD->EKD_MODALI) .or. EKD->EKD_MODALI == "3" // =Ambos
               cModalidade := "AMBOS"
            endif

            // posiciona no primeiro fabricante relacionado para validar montagem do json
            if EKB->(dbsetorder(1),msseek(xFilial("EKB") + EKD->EKD_COD_I))
               // caso tenha fabricante o fabricante conhecido ser� enviado como true
               cFabrConhecido := "true"

               cChaveEKJ := xFilial("EKJ") + EKD->EKD_CNPJ + EKB->EKB_CODFAB + EKB->EKB_LOJA
               if EKJ->(dbsetorder(1),msseek(cChaveEKJ))
                  // se o pa�s origem do fabricante for brasil manda o cnpj do cadastro do fabricante 
                  cPaisOrigem := EKJ->EKJ_PAIS
                  if cPaisOrigem == "BR"
                     SA2->(dbsetorder(1),msseek(xFilial("SA2") + EKB->EKB_CODFAB + EKB->EKB_LOJA))
                     cCpfCnpjFabric := SA2->A2_CGC
                     cCodigoOE      := ""
                  else // se o pais origem for diferente de brasil manda o c�digo do operador estrangeiro
                     cCodigoOE      := EKJ->EKJ_TIN
                     cCpfCnpjFabric := ""
                  endif
               endif
            else // caso n�o tenha fabricante ser� enviado como false
               cFabrConhecido := "false"
            endif

            // Monta o texto do json para a integra��o
            ctxtJson := '[{ "seq": '                 + "1"                    + ' ,'
            // opcional, caso esteja em branco � inclus�o e n�o deve ser enviado, depois de preenchido � altera��o
            if ! empty(EKD->EKD_IDPORT)
               ctxtJson += ' "codigo": '             + EKD->EKD_IDPORT        + ' ,'
            endif
            
            ctxtJson += ' "descricao": "'                + alltrim(strtran(EK9->EK9_DSCCOM, chr(13)+chr(10), " ")) + '",' + ;
                        ' "cpfCnpjRaiz": "'              + EKD->EKD_CNPJ          + '",' + ;
                        ' "situacao": "'                 + "ATIVADO"              + '",' + ;
                        ' "modalidade": "'               + cModalidade            + '",' + ;
                        ' "ncm": "'                      + EKD->EKD_NCM           + '",' + ;
                        ' "paisOrigem": "'               + cPaisOrigem            + '",' + ;
                        ' "fabricanteConhecido": '       + cFabrConhecido         + ' ,'
            
            // define o fabricante caso exista 
            if cFabrConhecido == "true"
               if cPaisOrigem == "BR"
                  ctxtJson += ' "cpfCnpjFabricante": "'         + cCpfCnpjFabric   + '",' 
               else
                  ctxtJson += ' "codigoOperadorEstrangeiro": "' + cCodigoOE        + '",' 
               endif
            endif
            
            ///campos foram retirados, mas ainda n�o � certeza
            /* ' "codigoNaladi": "'             + EKD->EKD_NALADI        + '",' + ;
            ' "codigoGPC": "'                + EKD->EKD_GPCCOD        + '",' + ;
            ' "codigoGPCBrick": "'           + EKD->EKD_GPCBRK        + '",' + ;
            ' "codigoUNSPSC": "'             + EKD->EKD_UNSPSC        + '",' + ; */

            // percorre os atributos cadastrados para o cat�logo de produto enviando atributo e valor
            cChaveEKI := xFilial("EKI") + EKD->EKD_COD_I + EKD->EKD_VERSAO
            if EKI->(dbsetorder(1),msseek(cChaveEKI))
               cJsonAtt := ' "atributos": [ '
                  while EKI->(! eof()) .and. EKI->(EKI_FILIAL+EKI_COD_I+EKI_VERSAO) == cChaveEKI
                     if ! empty(EKI->EKI_VALOR)
                        
                        nContCP++
                        if nContCP > 1
                           cJsonAtt += ','
                        endif

                        cJsonAtt += '{' + ;
                                       ' "atributo": "' + EKI->EKI_CODATR                                          + '",' + ;
                                       ' "valor": "'    + alltrim(strtran(EKI->EKI_VALOR, chr(13)+chr(10), " "))   + '" ' + ;
                                    '}'
                     endif
                     EKI->(dbskip())
                  enddo
               cJsonAtt += '],'
               ctxtJson += cJsonAtt
               nContCP := 0
            endif

            // percorre a lista de c�digo relacionados que s�o enviados em lista
            cChaveEKE := xFilial("EKE") + EKD->EKD_COD_I + EKD->EKD_VERSAO
            if EKE->(dbsetorder(1),msseek(cChaveEKE))
               cCodinterno := ' "codigosInterno": [ '
                  while EKE->(! eof()) .and. EKE->(EKE_FILIAL+EKE_COD_I+EKE_VERSAO) == cChaveEKE

                     nContCP++
                     if nContCP > 1
                        cCodinterno += ','
                     endif

                     cCodinterno += '"' + EKE->EKE_PRDREF + '"'

                     EKE->(dbskip())
                  enddo
               cCodinterno += ']'
               ctxtJson += cCodinterno
               nContCP := 0
            endif
            ctxtJson +=   '}' + ;
                     ']'

            // consome o servi�o atrav�s do easyjs
            oEasyJS  := EasyJS():New()
            oEasyJS:cUrl := cCPPathAuth
            oEasyJS:Activate(.T.)
            oEasyJS:runJSSync( CP400Auth( cCPPathAuth , cPathIACP , ctxtJson ) ,{|x| cRet := x } , {|x| cErros := x } )

            // Pega o retorno e converte para json para extrair as informa��es
            if ! empty(cRet)
               cRet     := '{"items":'+cRet+'}'
               oJson    := JsonObject():New()
               xRetJson := oJson:FromJson(cRet)
               if valtype(xRetJson) == "U" 
                  if valtype(oJson:GetJsonObject("items")) == "A"
                     aJson    := oJson:GetJsonObject("items")
                     if len(aJson) > 0
                        cSucesso := aJson[1]:GetJsonText("sucesso")
                        cCodigo  := aJson[1]:GetJsonText("codigo")
                        if valtype(aJson[1]:GetJsonObject("erros")) == "A"
                           aJsonErros := aJson[1]:GetJsonObject("erros")
                           for nj := 1 to len(aJsonErros)
                              cErros += aJsonErros[nj] + ENTER
                           next
                           if empty(cErros)
                              cErros += STR0060
                           endif
                        endif
                     endif
                  else
                     cErros += STR0059 + ENTER // "Arquivo de retorno sem itens!"
                  endif
                  FreeObj(oJson)
               else
                  cErros += STR0060 + ENTER // "Arquivo de retorno inv�lido!"
               endif
            elseif empty(cErros)
               cErros += STR0061 + ENTER // "Integra��o sem nenhum retorno!"
            endif

            // caso d� tudo certo grava as informa��es e finaliza o registro
            if ! empty(cRet) .and. ! empty(cSucesso) .and. upper(cSucesso) == "TRUE"
               reclock("EKD",.F.)
                  EKD->EKD_STATUS   := "1"
                  EKD->EKD_IDPORT   := cCodigo
                  EKD->EKD_DATA     := dDatabase
                  EKD->EKD_HORA     := strtran(time(),":","")
                  EKD->EKD_USERIN   := __cUserID
                  EKD->EKD_RETINT   := ""
               EKD->(msunlock())

               // grava o status de registrado do cat�logo de produtos
               reclock("EK9",.F.)
                  EK9->EK9_STATUS   := "1"
                  EK9->EK9_IDPORT   := cCodigo
                  EK9->EK9_VATUAL   := EKD->EKD_VERSAO
                  EK9->EK9_RETINT   := ""
               EK9->(msunlock())

               if ! lCP400Auto
                  oProcess:IncRegua2( STR0062 ) // "Integrado!"
                  oProcess:IncRegua2( STR0062 ) // "Integrado!"
               endif
            else // caso n�o grava o log, se n�o tiver ret tem algum erro.
               lRet := .F.
               cAux += "Produto/Vers�o: " + alltrim(EKD->EKD_COD_I) + "/" + alltrim(EKD->EKD_VERSAO) + ENTER + cErros
               reclock("EKD",.F.)
                  EKD->EKD_STATUS   := "4"
                  EKD->EKD_DATA     := dDatabase
                  EKD->EKD_HORA     := strtran(time(),":","")
                  EKD->EKD_USERIN   := __cUserID
                  EKD->EKD_RETINT   := cErros
               EKD->(msunlock())

               // grava o status de falha do cat�logo de produtos
               reclock("EK9",.F.)
                  EK9->EK9_RETINT   := cErros
               EK9->(msunlock())

               if ! lCP400Auto
                  oProcess:IncRegua2( STR0063 ) // "Falha!"
                  oProcess:IncRegua2( STR0063 ) // "Falha!"
               endif
            endif
         endif
         
         cErros   := ""
         cRet     := ""
         cCodigo  := ""
         cSucesso := ""
      next
   end sequence

   if ! empty(cAux)
      cErros := cAux
   endif

Return lRet

/*/{Protheus.doc} CP400Auth
   Gera o script para autenticar e consumir o servi�o do portaul unico atrav�s do easyjs 
   @author Miguel Prado Gontijo
   @since 16/06/2020
   @version 1
   /*/
Static Function CP400Auth(cUrl,cPathIACP,cOperador)
Local cVar

   begincontent var cVar
      fetch( '%Exp:cUrl%', {
         method: 'POST',
         mode: 'cors',
         headers: { 
            'Content-Type': 'application/json',
            'Role-Type': 'IMPEXP',
         },
      })
      .then( response => {
         if (!(response.ok)) {
            throw new Error( response.statusText );
         }
         var XCSRFToken = response.headers.get('X-CSRF-Token');
         var SetToken = response.headers.get('Set-Token');
         return fetch( '%Exp:cPathIACP%', {
            method: 'POST',
            mode: 'cors',
            headers: { 
               'Content-Type': 'application/json',
               "Authorization": SetToken,
               "X-CSRF-Token":  XCSRFToken,
            },
            body: '%Exp:cOperador%'
         })
      })
      .then( (res) => res.text() )
      .then( (res) => { retAdvpl(res) } )
      .catch((e) => { retAdvplError(e) });
   endcontent

Return cVar

/*
Programa   : CP400VldOE
Objetivo   : validar se no cadastro de operador estrangeiro o mesmo est� integrado ao portal �nico
           : e se o operador est� registrado no integrador do cat�logo de produtos
Retorno    : lRet
Autor      : Miguel Prado Gontijo
Data/Hora  : 21/07/2020
Obs.       :
*/
Function CP400VldOE(cErros,cPerg,aOperadores)
Local aAreaEKB    := EKB->(getarea())
Local aAreaEKJ    := EKJ->(getarea())
Local cChaveEKB   := ""
Local cChaveEKJ   := ""
Local nContOE     := 0
Local lRet        := .F.

   // posiciona nos fabricantes relacionados ao cat�logo de produtos
   cChaveEKB := xFilial("EKB") + EKD->EKD_COD_I
   if EKB->(dbsetorder(1),msseek(cChaveEKB))
      // enquanto houverem fabricantes relacionados a esse cat�logo de produtos 
      while EKB->( ! eof() ) .and. EKB->EKB_FILIAL+EKB->EKB_COD_I == cChaveEKB

         // verifica se o operador estrangeiro est� cadastrado 
         cChaveEKJ := xFilial("EKJ") + EKD->EKD_CNPJ + EKB->EKB_CODFAB + EKB->EKB_LOJA
         if EKJ->(dbsetorder(1),msseek(cChaveEKJ))
            
            // verifica se o mesmo est� integrado ao portal �nico
            if EKJ->EKJ_STATUS <> "1"
               cPerg += strtran( STR0064 , "XXXX" , alltrim(EKJ->EKJ_FORN) + "/" + alltrim(EKJ->EKJ_FOLOJA)) + ENTER // STR0064 - "Fabricante XXXX n�o integrado!"
               // adciona o operador para realizar a integra��o com portal �nico
               aadd( aOperadores, EKJ->(recno()))
            endif

         else 
            nContOE++
            // caso seja o primeiro registro insere informa��o do produto e vers�o a relatar o problema
            if nContOE == 1
               cErros += strtran(  STR0065 , "XXXX" , alltrim(EKD->EKD_COD_I) + "/" + alltrim(EKD->EKD_VERSAO)) + ENTER // "Produto/Vers�o: XXXX "
            endif
            cErros += strtran(  STR0066 , "XXXX" , alltrim(EKB->EKB_CODFAB)  + "/" + alltrim(EKB->EKB_LOJA)) + ENTER // "Fabricante/Loja XXXX n�o registrado como operador estrangeiro!"

         endif
 
         EKB->(dbskip())
      enddo
   endif

   lRet := ! empty(cErros) .or. (! empty(cPerg) .and. len(aOperadores) > 0)

   restarea(aAreaEKB)
   restarea(aAreaEKJ)

Return lRet
/*{Protheus.doc} CPGetVersion
   Busca a �ltima vers�o do integrador do cat�logo de produtos
   @author Miguel Prado Gontijo
   @since 20/06/2020
   @version 1
   @param cFilial e cXCodI - Filial e c�digo do item a ser buscado a �ltima vers�o
   @return Nil
*/
function CPGetVersion( cXFil , cXCodI )
Local cRet := ""

   if select("EKDVERSAO") > 0
      EKDVERSAO->(dbclosearea())
   endif

   BeginSql Alias "EKDVERSAO"
      select MAX(EKD_VERSAO) VERSAO 
      from %table:EKD% EKD 
      where EKD_FILIAL  = %Exp:cXFil%
         and EKD_COD_I  = %Exp:cXCodI%
         and EKD.%NotDel%
   EndSql

   EKDVERSAO->(dbgotop())
   if EKDVERSAO->(! eof())
      cRet := EKDVERSAO->VERSAO
   endif
   EKDVERSAO->(dbclosearea())

Return cRet
/*
Programa   : CP400CadOE
Objetivo   : Inserir registros no cadastro de operador estrangeiro
Retorno    : 
Autor      : Maur�cio Frison
Data/Hora  : 10/06/2020
Obs.       :
*/
Function CP400CadOE()
Local oModel      := FWLoadModel("EICCP400")
Local oModelEKB   := oModel:GetModel("EKBDETAIL")
Local oModelEK9   := oModel:GetModel("EK9MASTER")
Local aOEs        := {}
Local lRet := .t.
   oModel:Activate()
   if CP400OEValid(oModelEKB,oModelEK9,@aOEs)
      lRet := CP400ExecEKJ(aOEs)
      oModel:activate()
      if lRet 
          MSGINFO(STR0047,STR0021) //Integra��o conclu�da //Aviso
      EndIf
   Else 
         MSGINFO( StrTran(STR0069,"######",EK9->EK9_COD_I),STR0021) //Aviso Nenhum registro para processar. Todos os fabricantes do cat�logo ###### j� se encontram no cadastro de operador estrangeiro
   endif

Return
/*
Programa   : CP400OEValid
Objetivo   : Funcao que valida se deve gerar o cadastro de operador estrangeiro a partir do cadastro de fabricante relacionado ao produto do cat�logo
Retorno    : L�gico
Autor      : Miguel Gontijo
Data/Hora  : 06/2020
Obs.       :
*/
static function CP400OEValid(oModelEKB,oModelEK9,aOEs)
Local lRet        := .F.
Local nLinEKB     := 1
Local cChaveEKJ   := ""
Local aAux        := {}

   for nLinEKB := 1 to oModelEKB:length()
      if ! oModelEKB:isdeleted(nLinEKB)
         if ! empty( oModelEKB:getvalue("EKB_CODFAB", nLinEKB) ) .and. ! empty( oModelEKB:getvalue("EKB_LOJA", nLinEKB) )
            cChaveEKJ := xFilial("EKJ") + oModelEK9:getvalue("EK9_CNPJ") + oModelEKB:getvalue("EKB_CODFAB", nLinEKB)  + oModelEKB:getvalue("EKB_LOJA", nLinEKB)
            if ! EKJ->(dbsetorder(1),msseek(cChaveEKJ))
               aAux := {}
               Aadd( aAux, { "EKJ_FILIAL"  , xFilial("EKJ")                            , Nil })
               Aadd( aAux, { "EKJ_IMPORT"  , oModelEK9:GetValue("EK9_IMPORT")          , Nil })
               Aadd( aAux, { "EKJ_CNPJ_R"  , oModelEK9:GetValue("EK9_CNPJ")            , Nil })
               Aadd( aAux, { "EKJ_FORN"    , oModelEKB:getvalue("EKB_CODFAB" , nLinEKB) , Nil })
               Aadd( aAux, { "EKJ_FOLOJA"  , oModelEKB:getvalue("EKB_LOJA"   , nLinEKB) , Nil })
               aadd( aOEs, aclone(aAux))
            endif
         endif
      endif
   next

   lRet := len(aOEs) > 0

return lRet


/*
Programa   : CP400ExecEKJ(aOEs)
Objetivo   : ExecAuto de Operador Estrangeiro - Grava o fabricante na tabela de operador estrangeiro
Par�metro  : aOEs - array que cont�m uma lista de operadores a serem inclu�dos via rotina autom�tica
Retorno    : Nil
Autor      : Miguel Gontijo
Data/Hora  : 06/2020
Obs.       :
*/
function CP400ExecEKJ(aOEs)
Local aArea       := getarea()
Local aLog        := {}
Local i           := 0
Local nx          := 0
Local nPosForn    := ""
Local nPosFoLoja  := ""
Local cMsg        := ""
Local lRet        := .T.

Private lMsErroAuto     := .F.
Private lAutoErrNoFile  := .T.
Private lMsHelpAuto     := .F. 

   For i := 1 to len(aOEs)
      lMsErroAuto := .F.
      MsExecAuto({|x,y| EICOE400(x,y) },aOEs[i], 3)
      if lMsErroAuto
         lRet := .F.
         aLog        := GetAutoGrLog()
         nPosForn    := ascan(aOEs[i], {|x| x[1] == "EKJ_FORN" })
         nPosFoLoja  := ascan(aOEs[i], {|x| x[1] == "EKJ_FOLOJA" })
         cMsg        += "Erro na inclus�o do Fornecedor: " + Alltrim(aOEs[i][nPosForn][2]) + " Loja: " + Alltrim(aOEs[i][nPosFoLoja][2]) + ENTER
         for nx := 1 to len(aLog)
            cMsg += Alltrim(aLog[nx]) + ENTER
         Next
      EndIf
   Next

   if ! empty(cMsg)
      EECVIEW(cMsg, STR0002)
   endif

   restarea(aArea)

return lRet
/*
Programa   : ExcIntegr
Objetivo   : Funcao utilizada para excluir integra��o do cat�logo de produtos N�o Integrada
Retorno    : Logico
Autor      : Ramon Prado
Data/Hora  : Maio/2020
Obs.       :
*/
Static Function ExcIntegr( cCat, cVersao )
Local aCapaEKD := {}
Local aErros   := {}
Local nJ       := 1
Local cLogErro := ""

Private lMsHelpAuto     := .T. 
Private lAutoErrNoFile  := .T.
Private lMsErroAuto     := .F.

   aAdd(aCapaEKD,{"EKD_FILIAL", xFilial("EKD")  , Nil})
   aAdd(aCapaEKD,{"EKD_COD_I"	, cCat            , Nil})
   aAdd(aCapaEKD,{"EKD_VERSAO", cVersao         , Nil})

   MSExecAuto({|a,b| EICCP401 (a,b)}, aCapaEKD, 5)

   If lMsErroAuto
      aErros := GetAutoGRLog()
      For nJ:= 1 To Len(aErros)
         cLogErro += aErros[nJ]+ENTER
      Next nJ
   EndIf

Return cLogErro
/*
Programa   : CancInteg
Objetivo   : Funcao utilizada para Cancelar integra��o do cat�logo de produtos
Retorno    : Logico
Autor      : Ramon Prado
Data/Hora  : Maio/2020
Obs.       :
*/
Static Function CancInteg( cCat, cVersao )
Local aCapaEKD := {}
Local aErros   := {}
Local nJ       := 1
Local cLogErro := ""

Private lMsHelpAuto     := .T. 
Private lAutoErrNoFile  := .T.
Private lMsErroAuto     := .F.

   aAdd(aCapaEKD,{"EKD_FILIAL", xFilial("EKD")  , Nil})
   aAdd(aCapaEKD,{"EKD_COD_I"	, cCat            , Nil})
   aAdd(aCapaEKD,{"EKD_VERSAO", cVersao         , Nil})

   MSExecAuto({|a,b| EICCP401 (a,b)}, aCapaEKD, 4)

   If lMsErroAuto
      aErros := GetAutoGRLog()
      For nJ:= 1 To Len(aErros)
         cLogErro += aErros[nJ]+ENTER
      Next nJ
   EndIf

Return cLogErro
/*
Programa   : CP400Legen
Objetivo   : Demonstra a legenda das cores da mbrowse
Retorno    : .T.
Autor      : Ramon Prado
Data/Hora  : 27/11/2019
Obs.       :
*/
Function CP400Legen()
Local aCores := {}

   aCores := {	{"ENABLE"      , STR0004   },; // "Registrado"
               {"BR_CINZA"    , STR0005   },; // "Pendente Registro"
               {"BR_AMARELO"  , STR0006   },; // "Pendente Retifica��o"
               {"DISABLE"     , STR0007   }}  // "Bloqueado"

   BrwLegenda(STR0001,STR0013,aCores)

Return .T.
/*
Fun��o     : GetVisions()
Objetivo   : Retorna as vis�es definidas para o Browse
*/
Static Function GetVisions()
Local aVisions    := {}
Local aColunas    := AvGetCpBrw("EK9")
Local aContextos  := {"REGISTRADO", "PENDENTE_REGISTRO", "PENDENTE_RETIFICACAO", "BLOQUEADO"}
Local cFiltro     := ""
Local oDSView
Local i

   If aScan(aColunas, "EK9_FILIAL") == 0
      aAdd(aColunas, "EK9_FILIAL")
   EndIf

   For i := 1 To Len(aContextos)
      cFiltro := RetFilter(aContextos[i])            
      oDSView    := FWDSView():New()
      oDSView:SetName(AllTrim(Str(i)) + "-" + RetFilter(aContextos[i], .T.))
      oDSView:SetPublic(.T.)
      oDSView:SetCollumns(aColunas)
      oDSView:SetOrder(1)
      oDSView:AddFilter(AllTrim(Str(i)) + "-" + RetFilter(aContextos[i], .T.), cFiltro)
      oDSView:SetID(AllTrim(Str(i)))
      oDsView:SetLegend(.T.)
      aAdd(aVisions, oDSView)
   Next

Return aVisions
/*
Fun��o     : RetFilter(cTipo,lNome)
Objetivo   : Retorna a chave ou nome do filtro da tabela EK9 de acordo com o contexto desejado
Par�metros : cTipo - C�digo do Contexto
             lNome - Indica que deve ser retornado o nome correspondente ao filtro (default .f.)
*/
Static Function RetFilter(cTipo, lNome)
Local cRet     := ""
Default lNome  := .F.

   Do Case
      Case cTipo == "REGISTRADO" .And. !lNome
         cRet := "EK9->EK9_STATUS = '1' "
      Case cTipo == "REGISTRADO" .And. lNome
         cRet := STR0004 //"Registrado" "

      Case cTipo == "PENDENTE_REGISTRO" .And. !lNome
         cRet := "EK9->EK9_STATUS = '2' "
      Case cTipo == "PENDENTE_REGISTRO" .And. lNome
         cRet := STR0005 //"Pendente Registro" "

      Case cTipo == "PENDENTE_RETIFICACAO" .And. !lNome
         cRet := "EK9->EK9_STATUS = '3' "
      Case cTipo == "PENDENTE_RETIFICACAO" .And. lNome
         cRet := STR0006 //"Pendente Retifica��o" "

      Case cTipo == "BLOQUEADO" .And. !lNome
         cRet := "EK9->EK9_STATUS = '4' "
      Case cTipo == "BLOQUEADO" .And. lNome
         cRet := STR0007 //"Bloqueado"
   EndCase

Return cRet

/*
Fun��o     : CP400SB1F3()
Objetivo   : Monta a consulta padr�o da filial de origem do produto para sele��o 
             
Par�metros : Nenhum
Retorno    : lRet
Autor      : Ramon Prado (adaptada da fun��o NF400SD2F3, fonte: EICCP400)
Data       : Dez/2019
Revis�o    :
*/
Function CP400SB1F3()
Local aSeek    := {}
Local bOk      := {|| lRet:= .T., oDlg:End()}
Local bCancel  := {|| lRet:= .F.,  oDlg:End()}
Local cCpo     := AllTrim(Upper(ReadVar()))
Local aColunas	:= IF(cCpo == "M->EK9_PRDREF" .Or. cCpo == "M->EKA_PRDREF", AvGetCpBrw("SB1",,.T. /*desconsidera virtual*/), AvGetCpBrw("SA2",,.T. /*desconsidera virtual*/))
Local lRet     := .F.
Local nX       := 1
Local nCont
Local oDlg
Local oBrowse

Private cTitulo   := ""
Private aCampos   := {}
Private aFilter   := {} 

Begin Sequence
   
   If cCpo == "M->EK9_PRDREF" .Or. cCpo == "M->EKA_PRDREF"    
      aAdd(aColunas,"B1_IMPORT")
   Elseif cCpo == "M->EKB_CODFAB" .And. aScan(aColunas, "A2_FILIAL") == 0      
      aAdd(aColunas,Nil)
      AIns(aColunas,1)
      aColunas[1] := "A2_FILIAL"    
   EndIf   
        
   For nX := 1 to Len(aColunas)
   	  /* Campos usados na pesquisa */
      AAdd(aSeek, {AvSx3(aColunas[nX]   , AV_TITULO), {{"", AvSx3(aColunas[nX]  , AV_TIPO), AvSx3(aColunas[nX]   , AV_TAMANHO), AvSx3(aColunas[nX]   , AV_DECIMAL), AvSx3(aColunas[nX]    , AV_TITULO)}}})
      /* Campos usados no filtro */
      AAdd(aFilter, {AvSx3(aColunas[nX]    , AV_TITULO)  , AvSx3(aColunas[nX]    , AV_TITULO) , AvSx3(aColunas[nX]    , AV_TIPO) , AvSx3(aColunas[nX]    , AV_TAMANHO) , AvSx3(aColunas[nX]    , AV_DECIMAL), ""})      
   Next nX
     
   Define MsDialog oDlg Title STR0001 From DLG_LIN_INI, DLG_COL_INI To DLG_LIN_FIM * 0.9, DLG_COL_FIM * 0.9 Of oMainWnd Pixel

   oBrowse:= FWBrowse():New(oDlg)
   
   If cCpo == "M->EK9_PRDREF" .Or. cCpo == "M->EKA_PRDREF" 
      oBrowse:SetDataTable("SB1")
      oBrowse:SetAlias("SB1")
      cTitulo := STR0016
   Else
      oBrowse:SetDataTable("SA2")
      oBrowse:SetAlias("SA2")
      cTitulo := STR0035   
   EndIf      
      
   oBrowse:bLDblClick:= {|| lRet:= .T.,  oDlg:End()}
   
   oBrowse:SetDescription(cTitulo) 
   		
   For nCont:= 1 To Len(aColunas)
      If aColunas[nCont] <> "R_E_C_N_O_"          
         ADD COLUMN oColumn DATA &("{ ||" + aColunas[nCont] + " }") TITLE AvSx3(aColunas[nCont], AV_TITULO) SIZE AvSx3(aColunas[nCont], AV_TAMANHO) OF oBrowse           
      EndIf
   Next
       
   /* Pesquisa */
   oBrowse:SetSeek(, aSeek)
	
   /* Filtro */	
   oBrowse:SetUseFilter()
   oBrowse:SetFieldFilter(aFilter)
   
   If cCpo == "M->EK9_PRDREF" .Or. cCpo == "M->EKA_PRDREF" 
      oBrowse:AddFilter('Default',"SB1->B1_IMPORT == 'S' .And. SB1->B1_MSBLQL <> '1' ",.F.,.T.)
   EndIf   
	
   If cCpo == "M->EKA_PRDREF" .And. !Empty(M->EK9_NCM)
      oBrowse:AddFilter('Ncm',"SB1->B1_POSIPI == '"+M->EK9_NCM+"' ",.F.,.T.)
   EndIF
      		
   oBrowse:Activate()
   
   Activate MsDialog oDlg On Init (EnchoiceBar(oDlg, bOk, bCancel,,,,,,,.F.))	
	
End Sequence

Return lRet

/*
Fun��o     : CP400Relac(cCampo)
Objetivo   : Inicializar dados dos campos do grid(Relacao de Produtos - Catalogo de prds)
Par�metros : cCampo - campo a ser inicializado
Retorno    : cRet - Conteudo a ser inicializado
Autor      : Ramon Prado
Data       : dez/2019
Revis�o    :
*/
Function CP400Relac(cCampo)
Local aArea       := getArea()
Local cRet        := "" 
Local oModel      := FWModelActive()
Local oModelEKA   := oModel:GetModel("EKADETAIL")
Local cChaveEKJ   := ""
Local cChaveSA2   := ""

   If oModel:GetOperation() <> 3

      Do Case
         Case cCampo == "EKA_DESC_I" .And. ValType(oModelEKA) == "O"
            If lMultiFil
               cRet := Posicione("SB1",1,EKA->EKA_FILORI+AvKey(EKA->EKA_PRDREF,"B1_COD"),"B1_DESC")
            Else
               cRet := Posicione("SB1",1,XFILIAL("SB1")+AvKey(EKA->EKA_PRDREF,"B1_COD"),"B1_DESC")
            EndIf
         Case cCampo == "EK9_DESC_I"
            If lMultiFil
               cRet := POSICIONE("SB1",1,FWFLDGET("EK9_FILORI")+FWFLDGET("EK9_PRDREF"),"B1_DESC")
            Else		
               cRet := POSICIONE("SB1",1,XFILIAL("SB1")+FWFLDGET("EK9_PRDREF"),"B1_DESC")
            EndIf
         Case cCampo == "EKB_NOME"
            If lMultiFil
               cRet := POSICIONE("SA2",1,EKB->EKB_FILORI+EKB->EKB_CODFAB+EKB->EKB_LOJA,"A2_NOME")
            Else
               cRet := POSICIONE("SA2",1,EKB->EKB_FILIAL+EKB->EKB_CODFAB+EKB->EKB_LOJA,"A2_NOME")
            EndIf
         Case cCampo $ "EKB_OENOME|EKB_OEEND|EKB_OESTAT"
            If lMultiFil
               cChaveSA2 := EKB->EKB_FILORI+EKB->EKB_CODFAB+EKB->EKB_LOJA
            Else
               cChaveSA2 := EKB->EKB_FILIAL+EKB->EKB_CODFAB+EKB->EKB_LOJA
            EndIf
            if SA2->(dbsetorder(1),msseek(cChaveSA2))
               cChaveEKJ := SA2->A2_FILIAL+EK9->EK9_CNPJ+SA2->A2_COD+SA2->A2_LOJA
               if EKJ->(dbsetorder(1),msseek(cChaveEKJ))
                  if cCampo == "EKB_OENOME"
                     cRet := EKJ->EKJ_NOME
                  elseif cCampo == "EKB_OEEND"
                     cRet := alltrim(EKJ->EKJ_LOGR) + "-" + alltrim(EKJ->EKJ_CIDA) + "-" + alltrim(EKJ->EKJ_SUBP) + "-" + alltrim(EKJ->EKJ_PAIS) + "-" + alltrim(EKJ->EKJ_POSTAL)
                  elseif cCampo == "EKB_OESTAT"
                     cRet := EKJ->EKJ_STATUS
                  endif
               else
                  if cCampo == "EKB_OENOME"
                     cRet := SA2->A2_NOME
                  elseif cCampo == "EKB_OEEND"
                     cRet := alltrim(SA2->A2_END) + "-" + alltrim(SA2->A2_MUN) + "-" + alltrim(SA2->A2_PAISSUB) + "-" + alltrim(SA2->A2_PAIS) + "-" + alltrim(SA2->A2_POSEX)
                  elseif cCampo == "EKB_OESTAT"
                     cRet := "2"
                  endif
               endif
            endif
         Case cCampo == "EKC_STATUS"
            EKG->(dbSetOrder(1))
            If EKG->(DBSEEK(xFilial("EKG") + EK9->EK9_NCM + EKC->EKC_CODATR))
               cRet := CP400Status(EKG->EKG_INIVIG,EKG->EKG_FIMVIG)
            EndIf
         Case cCampo == "EKC_NOME"
            EKG->(dbSetOrder(1))
            If EKG->(DBSEEK(xFilial("EKG") + EK9->EK9_NCM + EKC->EKC_CODATR))
               cRet :=if(EKG->EKG_OBRIGA == "1","* ","") + AllTrim(EKG->EKG_NOME)
            EndIf
         Case cCampo == "EKC_VLEXIB"
            //cRet := IIF(!Empty(EKC->EKC_VALOR),SubSTR(EKC->EKC_VALOR,1,100),"")
      EndCase	
   EndIf

   RestArea(aArea)

Return cRet

/* 
Fun��o     : CP400AtLn1()
Objetivo   : Gatilho para preencher produto de refer�ncia na linha 1 do detalhe(Relacao de produtos)
Par�metros : 
Retorno    : cRet - Conteudo a ser gatilhado
Autor      : Ramon Prado
Data       : dez/2019
Revis�o    :
*/
Function CP400AtLn1()	
Local aArea       := getArea()	 
Local oModel      := FWModelActive()
Local oModelEKA   := oModel:GetModel("EKADETAIL")
Local cRet        := M->EK9_PRDREF
Local nI	         := 1
Local nPos        := 0
Local lExistPrd   := .F.
Local mDesc       := ""

   If oModelEKA:Length() > 0 .And. !Empty(M->EK9_PRDREF)	
      For nI := 1 to oModelEKA:Length()
         oModelEKA:GoLine( nI )
         If oModelEKA:GetValue("EKA_PRDREF") == M->EK9_PRDREF			
            lExistPrd := .T.
            exit
         Endif
         If Empty(oModelEKA:GetValue("EKA_PRDREF"))
            nPos := nI
         EndIf
      Next nI
      
      If !lExistPrd     
         If nPos > 0
            oModelEKA:GoLine( nPos )
            oModelEKA:SetValue("EKA_PRDREF", M->EK9_PRDREF)
         Else
            if oModelEKA:Length() < oModelEKA:AddLine()
               oModelEKA:SetValue("EKA_PRDREF", M->EK9_PRDREF)
               CP400Ncm(oModelEKA)
            Else
               cRet := M->EK9_PRDREF
            EndIf   
         EndIf		
      ElseIf oModelEKA:IsDeleted()
         oModelEKA:UnDeleteLine()		
         oModelEKA:Goline(1) //posiciona na linha 1
      EndIf	
   Endif

   If !Empty(M->EK9_PRDREF)
      mDesc := MSMM(SB1->B1_DESC_GI,AvSx3("B1_VM_GI",3))
      If !Empty(mDesc)	
         FwFldPut("EK9_DSCCOM",AvKey(mDesc,"EK9_DSCCOM"))
      EndIf	
   EndIf

   RestArea(aArea)

Return cRet

/*
Fun��o     : CP400Ncm(model)
Objetivo   : Excluir as linhas que tenham o produto com ncm diferente do ncm da capa(EK9)
Par�metros : 
Retorno    : Retorno .t.
Autor      : Maur�cio Frison
Data       : Abr/2020
Revis�o    :
*/
Function CP400Ncm(oModelEKA)
Local oView       := FWViewActive()
Local nI          :=0
Local cPRod       :=""
Local cFil        :=""
Local aArrayProd	:= {}

   if oModelEKA:GetOperation() == 3 //inclusao
      aArrayProd := CP400ArPrd(oModelEKA)
      if len(aArrayProd) > 0
         oModelEKA:ClearData(.T.,.T.) 
         FOR nI:=1 TO LEN(aArrayProd)
            cProd := aArrayProd[ni][1]
            cFil  := if(empty(M->EK9_FILORI),aArrayProd[ni][2],M->EK9_FILORI)
            If lMultiFil 	
               CP400PsFil(cProd,cFil,"SB1")
            Else
               POSICIONE("SB1",1,xFILIAL("SB1")+cProd,"B1_POSIPI")
            EndIf
            If Alltrim(M->EK9_NCM) == AllTrim(SB1->B1_POSIPI)
               oModelEKA:AddLine()
               oModelEKA:SetValue("EKA_PRDREF", cProd)
            EndIf
         Next
         oview:Refresh("EKADETAIL")
         oModelEKA:GoLine(1)
      EndIf  
   EndIf 

return .T.


/*
Fun��o     : CP400Valid()
Objetivo   : Validar dados digitados nos campos EK9 e EKA
Par�metros : cCampo - campo a ser validado
Retorno    : lRet - Retorno se foi validado ou nao
Autor      : Ramon Prado
Data       : dez/2019
Revis�o    :
*/
Function CP400Valid(cCampo)
Local oModel         := FWModelActive()
Local oModelEK9      := oModel:GetModel("EK9MASTER")
Local oModelEKA      := oModel:GetModel("EKADETAIL")
Local oModelEKB      := oModel:GetModel("EKBDETAIL")
Local oModelEKC      := oModel:GetModel("EKCDETAIL")
Local lRet           := .T.
Local lPosicEKD      := .F.

   Do Case
      Case cCampo == "EK9_PRDREF" .OR. cCampo == "EK9_FILORI" .or. cCampo == "EKA_PRDREF" .OR. cCampo == "EKA_FILORI"
         cProd := if(cCAmpo == "EK9_PRDREF", oModelEK9:GetValue("EK9_PRDREF") ,oModelEKA:GetValue("EKA_PRDREF"))
         cFil  := if(cCampo == "EK9_PRDREF" .or. cCampo == "EKA_PRDREF", NIL, if(cCAmpo == "EK9_FILORI",M->EK9_FILORI , oModelEKA:GetValue("EKA_FILORI")))

         If lMultiFil .And. !Empty(cProd)	
            lRet := CP400PsFil(cProd,cFil,"SB1")
         ElseIf !Empty(cProd)
            lRet := ExistCpo("SB1",cProd)
         EndIf
         If lRet
            If SB1->B1_MSBLQL == '1'
               EasyHelp(STR0019,STR0002) //"'Produto n�o encontrado no cadastro para esta filial'"##"Aten��o"
               lRet := .F.
            EndIf
         Else
            Help(" ",1,"REGNOIS")
         EndIf
         If lRet .And. !Empty(M->EK9_NCM) .and. (cCampo=="EKA_PRDREF" .or. cCampo =="EK9_PRDREF")
               If Alltrim(M->EK9_NCM) <> AllTrim(SB1->B1_POSIPI)
                     If oModel:GetOperation() == 4 // altera��o
                        lRet:=.F.
                        easyhelp(STR0042) //Altera��o n�o permitida porque este produto possui ncm diferente do atual.
                     Else
                        If IsMemVar("lCP400Auto") .And. !lCP400Auto .And. MsgNoYes(STR0020,STR0021) //'O Ncm do produto digitado � diferente do Ncm digitado no Cabe�alho. Deseja Prosseguir?'##'Aviso'
                           lRet := .T.
                           If oModelEKC:Length() > 0 .and. !Empty(oModelEKC:GetValue("EKC_CODATR"))
                              lRet := MsgNoYes(STR0032) // "Confirma altera��o do NCM, se confirmar todas as informa��es sobre os atributos ser�o perdidas"
                           EndIf 
                        Else
                           lREt:=.F.
                        EndIf	
                        if lRet
                           M->EK9_NCM :=SB1->B1_POSIPI
                           CP400ATRIB(.F.)
                        else
                           cNcmAux := M->EK9_NCM
                        EndIf
                     EndIf
               EndIf
         EndIf
      Case cCampo == "EKB_LOJA" .Or. cCampo == "EKB_FILORI"
         cCodFab := oModelEKB:GetValue("EKB_CODFAB")+oModelEKB:GetValue("EKB_LOJA")
         
         If cCampo == "EKB_LOJA"
            cFil  := NIL
         ElseIf cCampo == "EKB_FILORI"
            cFil  := oModelEKB:GetValue("EKB_FILORI")
         EndIf

         If lMultiFil
            If !Empty(oModelEKB:GetValue("EKB_CODFAB")) .And. !Empty(oModelEKB:GetValue("EKB_LOJA"))            
               lRet := CP400PsFil(cCodFab,cFil,"SA2")                      
            EndIf   
         ElseIf !Empty(cCodFab)
            lRet := ExistCpo("SA2",cCodFab)
            If lRet
               dbSelectArea("SA2")
            SA2->(DbSetOrder(1)) //Filial + Cod + Loja
            MsSeek(xFilial("SA2")+cCodFab)
            EndIf
         EndIf
         If !lRet
            Help(" ",1,"REGNOIS")
         EndIf  
      Case cCampo == "EK9_UNIEST" 
         lRet := Vazio() .OR. ExistCpo("SAH",M->EK9_UNIEST)
         If !lRet
            EasyHelp(STR0022,STR0002) //"Unidade de Medida n�o existe para o NCM e produto de refer�ncia"##"Aten��o"
         Endif
      Case cCampo == "EK9_NCM"
         lRet := (lRet .or. Vazio()) .and. ExistCpo("SYD",M->EK9_NCM) 
         // mesmo retornando .F.(quando responde Nao) no controle do campo EKA_PRDREF, o sistema por falha do componente quando
         //entra aqui de novo est� com o valor do campo M->EK9_NCM como se tivesse respondido Sim
         if !Empty(cNcmAux) 
            M->EK9_NCM := cNcmAux
            oModelEK9:LoadValue("EK9_NCM",cNcmAux)
            cNcmAux:=""
         EndIf
         If lREt .and. oModelEKC:Length() > 0 .and. M->EK9_NCM <> cNcm
               if !Empty(oModelEKC:GetValue("EKC_CODATR"))
                  lRet := MsgNoYes(STR0032) // "Confirma altera��o do NCM, se confirmar todas as informa��es sobre os atributos ser�o perdidas"
                  if lRet
                     CP400Ncm(oModelEKA)
                  EndIf
               EndIf
         EndIf
      Case cCampo == "EK9_VSMANU"
         If(lPosicEKD .And. (EKD->EKD_STATUS == '1' .Or. EKD->EKD_STATUS == '3'))
            If M->EK9_VSMANU < EKD->EKD_VERSAO
               EasyHelp(StrTran(STR0038, "###", EKD->EKD_VERSAO),STR0002) //"A vers�o informada manualmente � menor que a vers�o ### Integrada ou Cancelada"###"Aten��o"
               lRet := .F.
            EndIf
         EndIf
      Case cCampo == "EK9_DSCCOM"
         If len(oModelEK9:getvalue("EK9_DSCCOM")) > 3700
            EasyHelp("O campo de descri��o complementar n�o pode ter mais de 3700 caracteres!",STR0002) //"###"Aten��o"
            lRet := .F.
         EndIf
   EndCase	
   lRetAux := lRet

Return lRet

/*
Fun��o     : CP400PsFil()
Objetivo   : Pesquisa filial para o produto digitado - quando nao via F3(consulta padr�o/especifica)
Par�metros : cCampo - Produto a ser pesquisado
			 cTable - Tabela EKA - uso escpecifico para posicionar no produto SB1 para a filial encontrada
Retorno    : lRet - retorna se achou filial para o produto digitado
Autor      : Ramon Prado
Data       : dez/2019
Revis�o    :
*/
Function CP400PsFil(cPesq,cFil,cTable)
Local aFil := {}
Local lRet := .F.

   If(cFil == Nil, aFil := FWAllFilial(), aAdd(aFil,cFil))

   If cTable == "SB1"
      dbSelectArea("SB1")
      SB1->(DbSetOrder(1)) //Filial + Produto
      lRet := aScan(aFil,{|X| MsSeek(X+cPesq)}) > 0
   Else
      dbSelectArea("SA2")
      SA2->(DbSetOrder(1)) //Filial + Cod + Loja
      lRet := aScan(aFil,{|X| MsSeek(X+cPesq)}) > 0
   EndIf

Return lRet

/*
Fun��o     : CP400IniBrw()
Objetivo   : Inicializa Browse - conteudo exibido no browse para campo passado por parametro
Par�metros : cCampo - Campo a a ser inicializado no browse
Retorno    : cRet - Conteudo a ser inicializado no browse
Autor      : Ramon Prado
Data       : dez/2019
Revis�o    :
*/
Function CP400IniBrw(cCampo)
Local cRet	:= ""
Local aArea	:= GetArea()

   If  cCampo == "EK9_DESC_I"
      If lMultiFil
         cRet := POSICIONE("SB1",1,EK9->EK9_FILORI+EK9->EK9_PRDREF,"B1_DESC")
      Else
         cRet := POSICIONE("SB1",1,xFILIAL("SB1")+EK9->EK9_PRDREF,"B1_DESC")
      EndIf
   EndIf

   RestArea(aArea)

Return cRet

/*
Fun��o     : CP400Gatil(cCampo)
Objetivo   : Regras de gatilho para diversos campos
Par�metros : cCampo - campo cujo conteudo deve ser gatilhado
Retorno    : .T.
Autor      : Ramon Prado
Data       : dez/2019
Revis�o    :  
*/
Function CP400Gatil(cCampo)
Local aArea		   := GetArea()
Local oModel	   := FWModelActive()
Local oGridEKB    := oModel:GetModel("EKBDETAIL")
Local oModelEK9   := oModel:GetModel("EK9MASTER")
Local cRet        := ""
Local cChaveEKJ   := ""
Local cChaveSA2   := ""

   Do Case
      Case cCampo == "EKB_NOME"
         If !Empty(SA2->A2_NOME)
            cConteu := SA2->A2_NOME             
            FwFldPut("EKB_NOME",AvKey(cConteu,"EKB_NOME"))
         EndIf   
      Case cCampo $ "EKB_OENOME|EKB_OEEND|EKB_OESTAT"
         If lMultiFil
            cChaveSA2 := oGridEKB:getvalue("EKB_FILORI",oGridEKB:getline())+oGridEKB:getvalue("EKB_CODFAB",oGridEKB:getline())+oGridEKB:getvalue("EKB_LOJA",oGridEKB:getline())
         Else
            cChaveSA2 := oGridEKB:getvalue("EKB_FILIAL",oGridEKB:getline())+oGridEKB:getvalue("EKB_CODFAB",oGridEKB:getline())+oGridEKB:getvalue("EKB_LOJA",oGridEKB:getline())
         EndIf
         if SA2->(dbsetorder(1),msseek(cChaveSA2))
            cChaveEKJ := SA2->A2_FILIAL+oModelEK9:getvalue("EK9_CNPJ")+SA2->A2_COD+SA2->A2_LOJA
            if EKJ->(dbsetorder(1),msseek(cChaveEKJ))
               if cCampo == "EKB_OENOME"
                  cRet := EKJ->EKJ_NOME
               elseif cCampo == "EKB_OEEND"
                  cRet := alltrim(EKJ->EKJ_LOGR) + "-" + alltrim(EKJ->EKJ_CIDA) + "-" + alltrim(EKJ->EKJ_SUBP) + "-" + alltrim(EKJ->EKJ_PAIS) + "-" + alltrim(EKJ->EKJ_POSTAL)
               elseif cCampo == "EKB_OESTAT"
                  cRet := EKJ->EKJ_STATUS
               endif
            else
               if cCampo == "EKB_OENOME"
                  cRet := SA2->A2_NOME
               elseif cCampo == "EKB_OEEND"
                  cRet := alltrim(SA2->A2_END) + "-" + alltrim(SA2->A2_MUN) + "-" + alltrim(SA2->A2_PAISSUB) + "-" + alltrim(SA2->A2_PAIS) + "-" + alltrim(SA2->A2_POSEX)
               elseif cCampo == "EKB_OESTAT"
                  cRet := "2"
               endif
            endif
         endif

   EndCase

   RestArea(aArea)

Return cRet

/*
Fun��o     : CP400Condc(cCampo)
Objetivo   : Regras de condicao para gatilho ser executo para diversos campos
Par�metros : cCampo - campo cujo condicao de execu��o do gatilho deve ser verificada
Retorno    : lRet
Autor      : Ramon Prado
Data       : Jan/2020
Revis�o    :
*/
Function CP400Condc(cCampo)
Local lRet := .F.

   Do Case
      Case cCampo == "EKA_PRDREF"   
         lRet := LMULTIFIL .And. !EMPTY(FWFLDGET("EKA_PRDREF"))
      Case cCampo == "EKB_LOJA"
         lRet := lMultiFil .And. !EMPTY(FWFLDGET("EKB_LOJA"))
   EndCase

Return lRet

/*
Fun��o     : CP400ArPrd()
Objetivo   : Carrega Array de Produtos digitados no grid Rela��o de Produtos
Par�metros : Nil
Retorno    : Array de Produtos
Autor      : Ramon Prado
Data       : dez/2019
Revis�o    :
*/
Function CP400ArPrd(oModelPrd)
Local aArea       := GetArea()
Local aArrayProd  := {}
Local nI          := 1

   If oModelPrd:Length() > 0
      For nI := 1 to oModelPrd:Length()		
         oModelPrd:GoLine(nI)
         if !Empty(oModelPrd:GetValue("EKA_PRDREF"))
               aAdd(aArrayProd, {oModelPrd:GetValue("EKA_PRDREF"),if(lMultiFil,oModelPrd:GetValue("EKA_FILORI"),"")})
         EndIf
      Next nI
   EndIf

   RestArea(aArea)

Return aArrayProd

/*
Fun��o     : CP400VlSA5()
Objetivo   : Pesquisa Amarracao Produto XFornecedor Ou Produto X Fabricante
Par�metros : Nil
Retorno    : lRet - Encontrou Amarracao de produto x Fornecedor Ou Produto X Fabricante
Autor      : Ramon Prado
Data       : dez/2019
Revis�o    :
*/
Function CP400VlSA5(cFabricant, cLoja, cProd, cFilOri)
Local aArea		   := GetArea()
Local lRet		   := .F.
Local cFilSA5     := ""

Default cFilOri   := ""

   iif(!Empty(cFilOri), cFilSA5 := cFilOri, cFilSA5 := xFilial("SA5") )

   SA5->(DbSetOrder(2)) //A5_FILIAL+A5_PRODUTO+A5_FORNECE+A5_LOJA
               
   If SA5->(MsSeek(PADR(cFilSA5, AVSX3("A5_FILIAL",3)) + PADR(cProd, AVSX3("A5_PRODUTO",3)) + PADR(cFabricant, AVSX3("A5_FORNECE",3)) + PADR(cLoja, AVSX3("A5_LOJA",3)) ))
      lRet := .T.
   Else
      SA5->(DbSetOrder(4)) //A5_FILIAL+A5_FABR+A5_FALOJA+A5_PRODUTO
      If SA5->(MsSeek(PADR(cFilSA5, AVSX3("A5_FILIAL",3)) + PADR(cFabricant, AVSX3("A5_FORNECE",3)) + PADR(cLoja, AVSX3("A5_LOJA",3)) + PADR(cProd, AVSX3("A5_PRODUTO",3)) ))
         lRet := .T.
      EndIf
   EndIF

   RestArea(aArea)

Return lRet

/*
Fun��o     : CP400ATRIB() Gatilho EK9_NCM
Objetivo   : Monta o grid com os atributos de acordo como ncm(gatilho no ncm da EK9)
             
Par�metros : Nenhum
Retorno    : lRet
Autor      : Maur�cio Frison
Data       : Mar/2020
Revis�o    :
*/
Function CP400ATRIB(lEvento)

Local aDominio    := {}
Local oModel      := FWModelActive()
Local oModelEKC   := oModel:GetModel("EKCDETAIL")
Local lFirst      := .t.
Default lEvento := .F.

   if cNcm <> M->EK9_NCM .And. oModel:GetOperation() != EXCLUIR
      cNcm := M->EK9_NCM  
      lFirst := IIF(lEvento,.F.,lFirst)
      If oModel:GetOperation() == INCLUIR
         oModelEKC:DelAllline()     
         oModelEKC:ClearData(.T.,.T.)     
         lFirst := .T.
      EndIf

      DbSelectArea("EKG")
      //EKG->(DBSETORDER(2))
      If EKG->(DBSEEK(xFilial("EKG")+M->EK9_NCM))
         aAtrib := {}
         While EKG->(!Eof()) .AND. EKG->EKG_NCM == M->EK9_NCM      
            If !oModelEKC:SeekLine({{"EKC_COD_I", EK9_COD_I} ,{"EKC_CODATR", EKG_COD_I  }})
               if CP400Status(EKG->EKG_INIVIG,EKG->EKG_FIMVIG) != "EXPIRADO" .And. (M->EK9_MODALI == EKG->EKG_MODALI .OR. M->EK9_MODALI == "3" .OR. EKG->EKG_MODALI == "3") 
                  if lFirst .OR. (oModelEKC:length()==1 .AND. empty(oModelEKC:getVAlue("EKC_CODATR")) )
                     lFirst := .f.
                  Else
                     ForceAddLine(oModelEKC)
                  EndIf
                  oModelEKC:LoadValue("EKC_CODATR",EKG->EKG_COD_I)
                  oModelEKC:LoadValue("EKC_STATUS",CP400Status(EKG->EKG_INIVIG,EKG->EKG_FIMVIG))
                  oModelEKC:LoadValue("EKC_NOME",(if(EKG->EKG_OBRIGA == "1","* ","")) + AllTrim(EKG->EKG_NOME))
                  oModelEKC:LoadValue("EKC_VALOR","")
               EndIf  
            Else
               EKC->(dbSetOrder(1))
               If EKC->(dbSeek(xFilial("EKC") + M->EK9_COD_I + EKG->EKG_COD_I))
                  cTela := EKC->EKC_VALOR
                  if AllTrim(EKG->EKG_FORMA) == "BOOLEANO"
                     cTela := if(cTela=="1","SIM",if(cTela=="2","NAO",""))
                  ElseIf AllTrim(EKG->EKG_FORMA) == "LISTA_ESTATICA"
                     IF EKH->(DBSEEK(xFilial("EKH")+M->EK9_NCM+EKC->EKC_CODATR+cTela))
                        cTela := ALLTRIM(EKH->EKH_CODDOM)+"-"+EKH->EKH_DESCRE
                     EndIf
                  EndIf
                  oModelEKC:LoadValue("EKC_VLEXIB",SubSTR(cTela,1,100))
               EndIf
            EndIf
            aDominio := {}
            iF EKH->(DBSEEK(xFilial("EKH")+EKG->EKG_NCM+EKG->EKG_COD_I)) // Tudisco EKH->(DBSEEK(xFilial("EKH")+EKG->EKG_COD_I))
               While EKH->(!Eof()) .AND. EKH->EKH_NCM == EKG->EKG_NCM .AND. EKH->EKH_COD_I == EKG_COD_I 
                  AADD(aDominio,{EKH->EKH_COD_I,EKH->EKH_CODDOM,EKH->EKH_DESCRE})
                  EKH->(DbSkip())
               EndDo
            EndIf
            AADD(aAtrib,{EKG->EKG_COD_I,EKG->EKG_NOME,EKG->EKG_FORMA,EKG->EKG_OBRIGA,EKG->EKG_TAMAXI,EKG->EKG_DECATR,EKG->(RECNO()),aDominio})
            EKG->(DbSkip())
         EndDo
         oModelEKC:GoLine(1) 
      EndIf
   EndIf

return .T.

/*
Fun��o     : ForceAddLine()
Objetivo   : For�a adi��o de linha na tela de grid
Par�metros : oModelGrid
Retorno    : .T.
Autor      : Maur�cio Frison
Data       : Mar/2020
*/
Static Function ForceAddLine(oModelGrid)
Local oModel   := FWModelActive()
Local lDel     := .F.

   nOperation := oModel:GetOperation()
   oModelGrid:SetNoInsertLine(.F.)
   if nOperation == 1
      oModel:nOperation := 3
   EndIf

   If oModelGrid:Length() >= oModelGrid:AddLine()
      oModelGrid:GoLine(1)
      If !oModelGrid:IsDeleted()
         oModelGrid:DeleteLine()
         lDel := .T.
      EndIf
      oModelGrid:AddLine()
      oModelGrid:GoLine(1)
      If lDel
         oModelGrid:UnDeleteLine()
      EndIf
      oModelGrid:GoLine(oModelGrid:Length())
   EndIf

   oModelGrid:SetNoInsertLine(.T.)
   //oModelGrid:SetNoDeleteLine(.T.)
   oModel:nOperation := nOperation

Return .T.

/*
Fun��o     : CP400Status()
Objetivo   : Gera o status conforme data da vig�ncia e data base            
Par�metros : dDataVigencia
Retorno    : Status
Autor      : Maur�cio Frison
Data       : Mar/2020
Revis�o    :
*/
Function CP400Status(dIniVig,dFimVig)
Local cReturn := ""

   If dIniVig > dDataBase
      cReturn := "FUTURO"
   ElseIf dFimVig < dDataBase 
      cReturn := "EXPIRADO"
   Else
      cReturn := "VIGENTE"
   Endif

Return cReturn
/*
Fun��o     : CP400TELA() Gatilho campo EKC_CODATR
Objetivo   : Abrir uma tela diferente para cada tipo de dado de acordo com as tabelas EKG e EKH
Par�metros : cCodAtr c�digo do atributo
Retorno    : cTela - Retorna a informa��o selecionada pelo usu�rio
Autor      : Maur�cio Frison
Data       : Mar/2020
Revis�o    :
*/
Function CP400TELA()
Local oModel    := FWModelActive()
Local oModelEKC := oModel:GetModel("EKCDETAIL")
Local oView     := FWViewActive()
Local cForma 
Local lRetorno

Private cTela := ""

   if oModelEkc:getOperation() == 1
      lRetorno := .F.
   Else
      cAtr := oModelEKC:GetValue("EKC_CODATR")
      cForma := CP400Campo(cAtr,3)
      CP400CapaAtr(cForma,cAtr)
      oview:Refresh("EKCDETAIL")
      lRetorno := .T.
   EndIf

Return lREtorno

/*
Fun��o     : CP400Campo()
Objetivo   : Pegar a forma de preenchimento do atributo
Par�metros : cAtributo o c�digo do atributo, nCampo posi��o qeu define qual campo ser� retornado
Retorno    : valor - Retorna o campo de preenchimento do atributo
Autor      : Maur�cio Frison
Data       : Mar/2020
nCampo             1              2             3              4               5               6          7               8
            : EKG->EKG_COD_I,EKG->EKG_NOME,EKG->EKG_FORMA,EKG->EKG_OBRIGA,EKG->EKG_TAMAXI,EKG->EKG_DECATR,EKG->(RECNO()),aDominio}
Revis�o    :
*/
Function CP400Campo(cAtributo,nCampo)
Local valor
Local nPos := ascan(aAtrib, {|x| AllTrim(x[1]) == AllTrim(cAtributo)})

   if nPos > 0
      if valtype(aAtrib[nPos][nCampo]) == "C"
         valor := AllTrim(aAtrib[nPos][nCampo])
      Else
         valor := aAtrib[nPos][nCampo]
      EndIf   
   EndIf

Return valor

/*
Fun��o     : CP400CapaAtr()
Objetivo   : Exibir a tela de capa do atributo
Par�metros : cForma - � a forma de preenchimento que veio da tabela ekg
Retorno    : 
Autor      : Maur�cio Frison
Data       : Mar/2020
Revis�o    :
*/
Function CP400CapaAtr(cForma,cAtr)
Local oModel      := FWModelActive()
Local oModelEKC   := oModel:GetModel("EKCDETAIL")
Local cPict       := ""
Local cObrig      := ""
Local cValor      := ""
Local cTela       := ""
Local nTam        := 0
Local nDec        := 0
Local nValor      := 0
Local aLista      := {}
Local npos        := 1
Local bOk		   := {|| lRet:= .T., CP400GravaAtr(oModelEKC,cTela,cValor) , oDlg:End()}
Local bCancel     := {|| lRet:= .F., CP400ViewAtr()  , oDlg:End()}
Local aItems      := {'1-SIM','2-NAO'}
Local oBrowse
Local oPanelEnch
Local oPanelAtr
Private aRotina  := menudef()

Private cLabelGrid := ""
Private cTelaView  := "" 



   If !Empty(cAtr)
      EKG->(DBGOTO(CP400Campo(cAtr,7)))
      RegToMemory("EKG", .F.)

      DEFINE MSDIALOG oDlg TITLE "Atributos" FROM DLG_LIN_INI+100,DLG_COL_INI+100 TO DLG_LIN_FIM,DLG_COL_FIM OF oMainWnd PIXEL
      
         oPanelEnch        := TPanel():New(0, 0, "", oDlg,, .F., .F.,,, 90, 165)
         oPanelEnch:Align  := CONTROL_ALIGN_TOP
         oPanelAtr         := TPanel():New(0, 4, "", oDlg,, .F., .F.,,, 90, 165)
         oPanelAtr:Align   := CONTROL_ALIGN_ALLCLIENT
         oEnCh             := MsMGet():New("EKG", ,2, , , , ,PosDlg(oPanelEnch),{},,,,,oPanelEnch,.t.) 
         cObrig            := CP400Campo(cAtr,4)
         cTela             := oModelEKC:GetValue("EKC_VALOR")
         cValor            := oModelEKC:GetValue("EKC_VALOR")
         cTelaView         := oModelEKC:GetValue("EKC_VLEXIB")
         DO CASE

            CASE cForma == "LISTA_ESTATICA"
               // Define o Browse
               // [10:21 AM] Santos, Tiago H. (TR Tech, Content & Ops)
               bOk     := {|| lRet:= .T., cValor := aLista[oBrowse:At()][2], cTela := AllTrim(cValor)+"-"+aLista[oBrowse:At()][3], CP400GravaAtr(oModelEKC,cTela,cValor) , oDlg:End()}
               //tela com grid
               aLista := CP400Campo(oModelEKC:GetValue("EKC_CODATR"),8)
               nPos := AScan(aLista, {|x| x[2] == cTela})
               // Define o Browse
               oBrowse := FWBrowse():New(oPanelAtr)
               oBrowse:SetClrAlterRow(15000804)
               oBrowse:setDataArray()
               oBrowse:setArray( aLista )
               oBrowse:disableConfig()
               oBrowse:disableReport()

               // Adiciona as colunas do Browse 
               oColumn := FWBrwColumn():New()
               oColumn:SetData(&('{ || aLista[oBrowse:At()][2] }'))
               oColumn:ReadVar('aLista[oBrowse:At()][2]')
               oColumn:SetTitle(AvSX3('EKH_COD_I')[5])
               oColumn:SetSize(AvSX3('EKH_COD_I')[3])
               oColumn:SetDoubleClick(bOK)

               oBrowse:SetColumns({oColumn})

               oColumn := FWBrwColumn():New()
               oColumn:SetData(&('{ || aLista[oBrowse:At()][3] }'))
               oColumn:ReadVar('aLista[oBrowse:At()][3]')
               oColumn:SetTitle(CP400Campo(oModelEKC:GetValue("EKC_CODATR"),2))
               oColumn:SetSize(100)
               oColumn:SetDoubleClick(bOK)

               oBrowse:SetColumns({oColumn})

               // Ativa��o do Browse
               oBrowse:Activate()

            CASE cForma == "TEXTO"
               //cMacro      := "cTela"
               nTam:=CP400Campo(cAtr,5)
               bValid :={|u| CP400VlTexto(u,nTam)  }
               cLabelGrid := CP400Campo(cAtr,2) 
               cLabelAux := cLabelGrid + " (" + AllTrim(str(nTam)) + ")"
                  
               oMulti := TMultiget():New(7,5,{|u|if(Pcount()>0,(cTela:=u,cTela := u, cValor:=cTela) ,cTela)}, ;
               oPanelAtr, 890,160, , , , , , .T., , , , , , ,bValid , , , .F.,.T. ,cLabelAux,1 )
               
               oMulti:bGetKey := {|self,cText,nkey,oDlg|CP400GetKey(self,cText,nkey,oPanelAtr,nTam)}
               
            CASE cForma == "NUMERO_REAL"
               //tela com n�mero com picture correspondente
               nTam:=CP400Campo(cAtr,5)
               nDec:=CP400Campo(cAtr,6)
               cPict := CP400GeraPic(nTam,nDec)
               cTela := StrTran(cTela,".","")
               cTela := StrTran(cTela,",",".")
               nValor := val(cTela)
               TGet():New(5, 5, { | u | If( PCount() == 0, nValor, (nValor := u, cTela := alltrim(Transform(nValor,cPict)),cValor:=cTela) ) },oPanelAtr, ;
               060, 010, cPict ,, 0, ,,.F.,,.T., ,.F., ,.F.,.F., ,.F.,.F. ,,"nValor",,,,.T.,.F., , CP400Campo(cAtr,2),1 ) 
            CASE cForma == "BOOLEANO"
               // Tela combox sim ou nao
               cCombo1  := if(cValor == "", aItems[2], if(cValor =="1", aItems[1], aItems[2]))
               cTela    := right(cCombo1,3)
               cValor   := left(cCombo1,1)
               oCombo1  := TComboBox():New(5,5,{ |u| if(PCount()>0, cCombo1:=u , cCombo1) },;
               aItems,100,20,oPanelAtr, , { || cTela:=right(cCombo1,3), cValor:=left(cCombo1,1) } ;
               , , , ,.T., , , , , , , , , "cCombo1" , CP400Campo(cAtr,2),1)
               
         EndCase

         If oBrowse != nil
            oBrowse:GoTo(nPos,.T.)
         EndIf
         
         oEnch:oBox:Align:=CONTROL_ALIGN_ALLCLIENT

      Activate MsDialog oDlg On Init (EnchoiceBar(oDlg, bOk, bCancel,,,,,,,.F.))	
   EndIf
      
Return 

/*
Fun��o   : CP400GetKey(cValor,nTam)
Objetivo   : Exibir o n�mero de caracters dispon�veis
Par�metros : cValor: o valor digitado, nTam: o tamanho m�ximo
Retorno    : .T. se o tamanho do campo digitado estiver dentro do tamanho m�ximo e .F. se passar deste tamanho
Autor      : Maur�cio Frison
Data       : abr/2020
Revis�o    :
*/
Function CP400GetKey(objeto,ctext,nKey,oDlg,nTam)
   objeto:ctitle := cLabelGrid + " (" + allTrim(str(nTam-len(cText))) + ")"
Return .T. 

/*
Fun��o   : CP400VlTexto(oObjeto,nTam)
Objetivo   : trata o tamanho do campo e pergunta se quer truncar
Par�metros : objeto a trucar o texto, nTam: o tamanho m�ximo
Retorno    : lRet se truncou ou n�o o tamanho do objeto
Autor      : Maur�cio Frison
Data       : abr/2020
Revis�o    :
*/
Function CP400VlTexto(oObjeto,nTam)
Local lRet := .T.

   if len(cValor) > nTam 
      lRet := MsgNoYes(STR0036) //Tamanho do campo excedido, as informa��es ser�o truncadas. Deseja prosseguir?
      if lRet
         cValor:=substring(cValor,1,nTam)
         cTela:=cvalor
      EndIf
   EndIf

Return lRet 

/*
Fun��o     : CP400GravaAtr()
Objetivo   : Grava a informa��o no banco
Par�metros : cTela campo gerado na tela
Retorno    : 
Autor      : Maur�cio Frison
Data       : Mar/2020
Revis�o    :
*/
Function CP400GravaAtr(oModelEKC,cTela,cValor)

   oModelEKC:SetValue("EKC_VALOR",cValor)
   cTela := subString(cTela,1,100)
   oModelEKC:LoadValue("EKC_VLEXIB",cTela)

return .T.

/*
Fun��o     : CP400ViewAtr()
Objetivo   : Retorna a informa��o pra tela principal quando sai da tela do F3 sem Confirmar 
Par�metros : 
Retorno    : 
Autor      : Maur�cio Frison
Data       : Mar/2020
Revis�o    :
*/
Function CP400ViewAtr()
   cTela := cTelaView
return .T.

/*
Fun��o     : CP400GeraPic()
Objetivo   : Gera a picture de acordo com os par�meros
Par�metros : nTam tamanho total do campo inclusive com ponto decimal
             nDec n�mero de casas decimais
Retorno    : 
Autor      : Maur�cio Frison
Data       : Mar/2020
Revis�o    :
*/
Function CP400GeraPic(nTam,nDec)
Local cPict := ""
Local nI

   //define decimal
   For nI := 1 to nDec
      cPict := cPict+"9"
   Next
   if nDec <> 0
      cPict :=  "."+cPict
      nTam := nTAm - ndec -1 //-1 referente ao ponto decimal
   EndIf
   //defini parte inteira
   For nI := nTam to 1 step -1
      if Mod(nTam-nI,3) == 0 .And. (nTam-nI) > 0
            cPict := "9," + cpict
      Else 
            cPict := "9" + cpict 
      EndIf     
   Next
   cPict :=  "@E " + cPict

Return cPict

/*
Fun��o     : EKCLineValid()
Objetivo   : Funcao de Pre validacao do grid da EKC
Retorno    : T se quiser continuar e F se n�o quiser continuar
Autor      : THTS - Tiago Tudisco
Data       : Mar/2020
Revis�o    :
*/
Static Function EKCLineValid(oGridEKC, nLine, cAction, cIDField, xValue, xCurrentValue)
Local lRet := .T.
 
   Do Case
      Case cAction == "DELETE" .And. !IsInCallStack("ForceAddLine") .And. !IsInCallStack("CP400POSVL") .And. !IsInCallStack("CP400ATRIB")
         If Alltrim(oGridEKC:GetValue("EKC_STATUS")) != "EXPIRADO"
            Help( ,, 'HELP',, STR0034, 1, 0) //"N�o � poss�vel excluir atributos com o status Vigente ou Futuro." 
            lRet := .F.
         EndIf
   
   EndCase

Return lRet

/*
Fun��o     : EKCDblClick(oFormulario,cFieldName,nLineGrid,nLineModel)
Objetivo   : Funcao para quando efetuado um duplo clik no item abrir em tela
Retorno    : Retorno l�gico
Autor      : THTS - Tiago Tudisco
Data       : Mar/2020
Revis�o    :
*/
Static Function EKCDblClick(oFormulario,cFieldName,nLineGrid,nLineModel)
Local lRet := .F.

   lRet := CP400TELA()

Return lRet
/*
Class      : CP400EV
Objetivo   : CLASSE PARA CRIA��O DE EVENTOS E VALIDA��ES NOS FORMUL�RIOS
Retorno    : Nil
Autor      : THTS - Tiago Tudisco
Data       : Mar/2020
Revis�o    :
*/
Class CP400EV FROM FWModelEvent
     
   Method New()
   Method Activate()

End Class
/*
Class      : M�todo New Class CP400EV 
Objetivo   : M�todo para cria��o do objeto
Retorno    : Nil
Autor      : THTS - Tiago Tudisco
Data       : Mar/2020
Revis�o    :
*/
Method New() Class CP400EV
Return
/*
Class      : M�todo New Class CP400EV 
Objetivo   : M�todo para ativar o objeto
Retorno    : Nil
Autor      : THTS - Tiago Tudisco
Data       : Mar/2020
Revis�o    :
*/
Method Activate(oModel,lCopy) Class CP400EV
   cNCm  := "          "
   CP400ATRIB(.T.)
Return

/*
Fun��o     : EKALineValid()
Objetivo   : Funcao de Pre validacao do grid da EKA
Retorno    : T se quiser continuar e F se n�o quiser continuar
Autor      : Maur�cio Frison
Data       : Abr/2020
Revis�o    :
*/
Static Function EKALineValid(oModelEKA)
Local lRet := .T.

   if IsInCallStack("CP400AtLn1") 
      lRet := lRetAux
   EndIf

Return lRet

/*
Programa   : TemIntegEKD
Objetivo   : Verifica se para o Catalogo informado, existe registro Integrado ou Cancelado
Retorno    : .T. quando encontrar registro Integrado ou Cancelado; .F. n�o encontrar registro Integrado ou Cancelado
Autor      : Ramon Prado
Data/Hora  : Maio/2020
*/
Static Function TemIntegEKD(cCod_I)
Local lRet := .F.
Local cQuery

   cQuery := "SELECT EKD_COD_I FROM " + RetSQLName("EKD")
   cQuery += " WHERE EKD_FILIAL = '" + xFilial("EKD") + "' "
   cQuery += "   AND EKD_COD_I  = '" + cCod_I + "' "
   cQuery += "   AND (EKD_STATUS = '1' OR EKD_STATUS = '3') " //Registrado ou Cancelado
   cQuery += "   AND D_E_L_E_T_ = ' ' "

   If EasyQryCount(cQuery) != 0
      lRet := .T.
   EndIf

Return lRet
