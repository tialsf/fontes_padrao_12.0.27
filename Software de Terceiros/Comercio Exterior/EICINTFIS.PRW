#INCLUDE "AVERAGE.CH"
#INCLUDE "TOPCONN.CH"

//Programa...: EICINTFIS()
//Autor......: Alex Wallauer (AWR)
//Data.......: 04/07/2006
//Objetivo...: Gerar dados para integra��o fiscal com tela de selecao de periodo
//Altera��o..: Alessandro Rodrigues - 15/12/2008 - 15:30 - Gerar dados com tela de selecao de periodo
*---------------------------------------------------*
User Function EICINTFIS()
*---------------------------------------------------*

Local bOk, bCancel, nOpcao, oDlg ,nSeq:=1

Private cDir
Private nEnvio := nTipo := 1
Private cArqDBF := ""
Private aTB_Campos := ArrayBrowse("EW1","Work")

cDir    := EasyGParam("MV_AVG0175",,AllTrim(GetSrvProfString("StartPath","")))
bOk     := {|| IF(E_PERIODO_OK(dDtini,dDtFim),(nOpcao := 1 , oDlg:End()),)} 
bCancel := {|| nOpcao := 0 , oDlg:End() }

dDtIni := ctod("")                  
dDtFim := ctod("")

DO WHILE .T.
   
   cArqTXT := Alltrim(cDir) + "\" + "INTFIS_EIC" + Conv(Date(),"D") + "_" + AllTrim(str(nSeq)) + ".TXT" + SPACE(100)
   Do While File(ALLTRIM(cArqTXT))
      nSeq++
      cArqTXT := Alltrim(cDir) + "\" + "INTFIS_EIC_" + Conv(Date(),"D") + "_" + AllTrim(str(nSeq)) + ".TXT" + SPACE(100)
   Enddo   

   nOpcao:=0
   nLinha:=10
   nCol0 :=22
   nCol1 :=10
   nCol2 :=nCol0+35
   nCol3 :=nCol1+68
   
   DEFINE MSDIALOG oDlg TITLE "Integra��o Fiscal" FROM 1,1 TO 25,35
 
      @ nLinha,nCol0-5 TO nLinha+55,nCol0+90 LABEL " Emiss�o da Nota Fiscal " OF oDlg PIXEL 
      nLinha+=15 

      @ nLinha,nCol0 SAY "Data Inicial" SIZE 60,7  PIXEL 
      @ nLinha,nCol2 MSGET dDtIni SIZE 50,8  PIXEL 
      nLinha+=20 

      @ nLinha,nCol0 SAY "Data Final" SIZE 60,7  PIXEL 
      @ nLinha,nCol2 MSGET dDtFim SIZE 50,8  PIXEL 
      nLinha+=25 
      nAuxLin:=nLinha

      @ nLinha,nCol1-5 TO nLinha+40,nCol1+55 LABEL " Tipo da Integra��o " OF oDlg PIXEL 
      nLinha+=10 

      @ nLinha,nCol1 RADIO nTipo ITEMS "Banco de Dados","Arquivo texto" 3D SIZE 52,14 OF oDlg ON CHANGE (INTMSBOX()) 
      nLinha+=35 

      nLinha:=nAuxLin
      @ nLinha,nCol3-5 TO nLinha+40,nCol3+52 LABEL " Tipo de Dados " OF oDlg PIXEL 
      nLinha+=10 

      @ nLinha,nCol3 RADIO oRadio2 var nEnvio ITEMS "N�o Enviados","Enviados" 3D SIZE 45,14 OF oDlg WHEN nTipo = 1
      nLinha+=40 

      @ nLinha,nCol1-5 TO nLinha+30,nCol3+52 LABEL " Nome do arquivo texto " OF oDlg PIXEL 
      nLinha+=11 

      @ nLinha,nCol1 MSGET oGet var cArqTXT SIZE 115,8 WHEN nTipo # 1 OF oDlg PIXEL 
      nLinha+=30 

      DEFINE SBUTTON FROM nLinha,nCol1+10  TYPE 1 ACTION EVAL(bOk)     ENABLE OF oDlg
      DEFINE SBUTTON FROM nLinha,nCol3+10  TYPE 2 ACTION EVAL(bCancel) ENABLE OF oDlg
  
   ACTIVATE DIALOG oDlg CENTERED

   If nOpcao = 0
      Exit
   Endif

   lEnviados:= (nEnvio = 2)

   lOK:=.F.
   Processa({|| LOK:=LeDados() },"Gerando arquivo de integracao fiscal")

   IF !LOK
      Ferase(cArqTxt)
      LOOP
   ELSE
      IF !MSGYESNO("Gera��o de dados conclu�da com sucesso." + ENTER + "Deseja visualizar os dados gerados?")
         LOOP
      ENDIF
   ENDIF

   Work->(dbGoTop())             

   nOpcao:=0  

   IF !EMPTY(dDtIni) .OR. !EMPTY(dDtFim)
      cSubTitulo:=": Periodo de "+dtoc(dDtIni)+" a "+dtoc(dDtFim)
   ELSE
      cSubTitulo:=""
   ENDIF

   aBotao:={}
   AADD(aBotao,{"COLGERA",{|| TR350ARQUIVO("Work") },"Gera Arquivo","Gera Arq."})

   oMainWnd:ReadClientCoors()
   
   Define MsDialog oDlg1 Title "Dados gerados para integra��o fiscal"+cSubTitulo From oMainWnd:nTop+125,oMainWnd:nLeft+5 To;
                                                                   oMainWnd:nBottom-60,oMainWnd:nRight-10 Of oMainWnd Pixel
         
      aPos := {18,1,(oDlg1:nClientHeight-6)/2,(oDlg1:nClientWidth-4)/2}
      
      //by GFP - 18/10/2010 - 14:23
      aTB_Campos := AddCpoUser(aTB_Campos,"EW1","2")

      oMark:= MsSelect():New("Work",,,aTB_Campos,.F.,"XX",aPos)
      oMark:oBrowse:Align:=CONTROL_ALIGN_ALLCLIENT
                                    
   Activate MsDialog oDlg1 On Init (EnchoiceBar(oDlg1,{|| nOpcao:=1,oDlg1:End() },{|| nOpcao:=0,oDlg1:End() },,aBotao),oMark:oBrowse:Refresh())

ENDDO

IF SELECT("Work") # 0
   Work->(E_EraseArq(cArqDBF))
ENDIF

Return .T.


*---------------------------------------*
Static Function LeDados(lOnLineL,cHAWB)
*---------------------------------------*

LOCAL cFilSA2:=xFilial("SA2")
LOCAL cFilSF1:=xFilial("SF1")
LOCAL cFilSW2:=xFilial("SW2")
LOCAL cFilSW6:=xFilial("SW6")
LOCAL cFilSW9:=xFilial("SW9")
LOCAL cFilSWN:=xFilial("SWN")
LOCAL cFilSYA:=xFilial("SYA")

PRIVATE lOnLine   := IF(lOnLineL=Nil,.F.,lOnLineL)

IF nTipo = 1

   DbSelectArea("EW1")
   If Select("EW1") = 0
      MsgStop("N�o foi poss�vel abrir a tabela EW1! A integra��o nao poder� ser realizada!","Aviso")
      Return .F.
   EndIf

ELSEIF nTipo = 2

   cArqTXT:=ALLTRIM(cArqTXT)

   IF (nArq := EasyCreateFile(cArqTXT)) == -1
      MsgInfo("N�o foi poss�vel criar o arquivo: "+cArqTXT, "Aviso")
      Return .F.
   Endif

Endif

IF SELECT("Work") = 0
   aHeader:={}
   aCampos:=Array(EW1->(FCount()))
   cArqDBF:=E_CRIATRAB("EW1",,"Work",,.F.)
   If !Used()
      MsgInfo("N�o foi poss�vel criar o arquivo: "+cArqDBF, "Aviso")
      RETURN .F.
   EndIf
ELSE
   Work->(avzap())
EndIf

ProcRegua(2)

IncProc("Lendo Dados...")

cQuery1:= " SELECT COUNT(*) TOTAL FROM " + RetSqlName("SWN")+ " SWN WHERE "
cQuery2:= " SELECT R_E_C_N_O_ REGISTRO FROM " + RetSqlName("SWN")+ " SWN WHERE "
cQuery := ""
cQuery += " SWN.D_E_L_E_T_ <> '*'  AND " + ENTER
cQuery += " SWN.WN_DOC <> '0' AND  " + ENTER
cQuery += " SWN.WN_TIPO_NF IN('1','2','3','5','7') AND " + ENTER // Notas Primeira,Complementar,Unica,Mae,Complementar da Mae
cQuery += " SWN.WN_FILIAL  = '" + cFilSWN + "'" + ENTER

IF lOnLine

   cQuery += " AND SWN.WN_HAWB = '" + cHAWB + "' " + ENTER

ELSEIF nTipo = 1

   IF lEnviados
      If "ORACLE" $ Upper(TcGetDb())
  	     cQuery += " AND SWN.WN_HAWB||SWN.WN_TIPO_NF     IN (SELECT EW1_CHAV FROM " + RetSqlName("EW1") + ") "
      ElseIf "MSSQL" $ Upper(TcGetDb())
  	     cQuery += " AND SWN.WN_HAWB+SWN.WN_TIPO_NF      IN (SELECT EW1_CHAV FROM " + RetSqlName("EW1") + ") "
      EndIf      
   ELSE
	  If "ORACLE" $ Upper(TcGetDb())
		 cQuery += " AND SWN.WN_HAWB||SWN.WN_TIPO_NF NOT IN (SELECT EW1_CHAV FROM " + RetSqlName("EW1") + ") "
      ElseIf "MSSQL" $ Upper(TcGetDb())
	     cQuery += " AND SWN.WN_HAWB+SWN.WN_TIPO_NF  NOT IN (SELECT EW1_CHAV FROM " + RetSqlName("EW1") + ") "
      EndIf
   ENDIF

ENDIF

If !Empty(dDtIni) .OR. !Empty(dDtFim)  .OR. SF1->(FIELDPOS("F1_SAPSTJ1")) # 0
   If "ORACLE" $ Upper(TcGetDb())
      cQuery += " AND SWN.WN_HAWB||SWN.WN_DOC||SWN.WN_SERIE||SWN.WN_FORNECE||SWN.WN_LOJA||SWN.WN_TIPO_NF IN "+ENTER+;
                "(SELECT SF1.F1_HAWB||SF1.F1_DOC||SF1.F1_SERIE||SF1.F1_FORNECE||SF1.F1_LOJA||SF1.F1_TIPO_NF FROM "+RetSqlName("SF1")+" SF1 WHERE"+ENTER+;
                " SF1.F1_FILIAL = '"+cFilSF1+"' AND "+ENTER
   ElseIf "MSSQL" $ Upper(TcGetDb())
	  cQuery += " AND SWN.WN_HAWB+SWN.WN_DOC+SWN.WN_SERIE+SWN.WN_FORNECE+SWN.WN_LOJA+SWN.WN_TIPO_NF IN "+ENTER+;
                "(SELECT SF1.F1_HAWB+SF1.F1_DOC+SF1.F1_SERIE+SF1.F1_FORNECE+SF1.F1_LOJA+SF1.F1_TIPO_NF FROM "+RetSqlName("SF1")+" SF1 WHERE"+ENTER+;
                " SF1.F1_FILIAL = '"+cFilSF1+"' AND "+ENTER
   EndIf
		
   If SF1->(FIELDPOS("F1_SAPSTJ1")) # 0
      cQuery += " SF1.F1_SAPSTJ1 = 'ACEITO' AND " + ENTER
   EndIf
      
   If !Empty(dDtIni)
      cQuery += " SF1.F1_EMISSAO >= '" + DTOS(dDtIni) + "' AND " + ENTER
   EndIf

   If !Empty(dDtFim)
      cQuery += " SF1.F1_EMISSAO <= '" + DTOS(dDtFim) + "' AND " + ENTER
   EndIf
   
   If SF1->(FIELDPOS("F1_SAPDOC")) # 0
   	  cQuery += " SF1.F1_SAPDOC <> '' AND " + ENTER
   EndIf
   	  cQuery += " SF1.D_E_L_E_T_ <> '*' )" + ENTER

EndIf

IncProc("Criando Arquivo de Trabalho...")

IF !GerTCQuery(cQuery1+cQuery,"Q_SWN")
   RETURN .F.
ENDIF

nTotal:=Q_SWN->TOTAL

IF !GerTCQuery(cQuery2+cQuery,"Q_SWN")
   RETURN .F.
ENDIF

SA2->(DBSETORDER(1))
SF1->(DBSETORDER(1))
SW6->(DBSETORDER(1))
SW9->(DBSETORDER(1))

aDelHAWB:={}
cMandante:=EasyGParam("MV_AVG0176",,"")

Q_SWN->(DBGOTOP())
ProcRegua(nTotal)

DO WHILE ! Q_SWN->(EOF())    

   SWN->(DBGOTO(Q_SWN->REGISTRO))

   IncProc("Lendo Processo: "+SWN->WN_HAWB)

   SW2->(DBSEEK(cFilSW2+SWN->WN_PO_EIC)) 
   SA2->(DBSEEK(cFilSA2+SWN->WN_FORNECE)) 
   SYA->(DBSEEK(cFilSYA+SA2->A2_PAIS)) 
   SF1->(DBSEEK(cFilSF1+SWN->(WN_DOC+WN_SERIE+WN_FORNECE+WN_LOJA))) 
   SW6->(DBSEEK(cFilSW6+SWN->WN_HAWB))
   //TDF 06/12/2010 - ACRESCENTA O HAWB NA CHAVE DE BUSCA
   SW9->(DBSEEK(cFilSW9+SWN->WN_INVOICE+SWN->WN_FORNECE+EICRetLoja("SWN", "WN_LOJA")+SWN->WN_HAWB))//W9_FILIAL+W9_INVOICE+W9_FORN+W9_HAWB
   
   GeraDados()                                                            
   
   Q_SWN->(DBSKIP())

ENDDO

IF nTipo = 2
   FCLOSE(nArq) 
ENDIF

Q_SWN->(DBCLOSEAREA())
DBSELECTAREA("Work")

Return .T.


*----------------------------------*
Static Function GeraDados()
*----------------------------------*

LOCAL D

Work->(DBAPPEND())

cnGrvWork:=NIL
aDadosMS :={}

IntMS_AADD("EW1_FILIAL" , xFilial("EW1"))                                             // Filial da Empresa
IntMS_AADD("EW1_CODEMP" , IF(SW2->(FIELDPOS("W2_SAPEMPR"))#0,SW2->W2_SAPEMPR,"001") ) // Informar company code do SAP vinculado ao processo
IntMS_AADD("EW1_MANDAN" , cMandante)                                                  // Mandante utilizado
IntMS_AADD("EW1_NUMDOC" , IF(SF1->(FIELDPOS("F1_SAPDOC")) # 0,SF1->F1_SAPDOC,""))     // Enviar o DOCNUM da NF
IntMS_AADD("EW1_DT_DI"  , Conv(SW6->W6_DTREG_D,"DA"),SW6->W6_DTREG_D)                 // Data da DI (AAAAMMDD)
IntMS_AADD("EW1_NUMDI"  , SW6->W6_DI_NUM)                                             // N�mero da DI (sem separador)
IntMS_AADD("EW1_INDFIS" , "1")                                                        // 1 (sempre enviar)
IntMS_AADD("EW1_CODFIS" , SWN->WN_FORNECE)                                            // Codigo do fornecedor no SAP
IntMS_AADD("EW1_NUMNF"  , SWN->WN_DOC)                                                // N�mero da NF
IntMS_AADD("EW1_SERNF"  , SWN->WN_SERIE)                                              // S�rie da NF
IntMS_AADD("EW1_CODPRO" , SWN->WN_PRODUTO)                                            // Enviar c�digo do material
IntMS_AADD("EW1_CODNBM" , SWN->WN_TEC)                                                // Enviar NCM do material
IntMS_AADD("EW1_VLRUN"  , Conv(SWN->WN_PRECO  ,"N",15,2),cnGrvWork)                   // Valor unit�rio da mercadoria na moeda do processo (invoce)
IntMS_AADD("EW1_VLRUNR" , Conv(SWN->WN_PRUNI  ,"N",18,8),cnGrvWork)                   // Valor unit�rio da mercadoria em BRL (NF)
IntMS_AADD("EW1_VLPRO"  , Conv(SWN->WN_VALOR  ,"N",14,2),cnGrvWork)                   // Valor total FOB convertido em BRL
IntMS_AADD("EW1_VL_FRE" , Conv(SWN->WN_FRETE  ,"N",15,2),cnGrvWork)                   // Valor total FRETE convertido em BRL
IntMS_AADD("EW1_VL_SEG" , Conv(SWN->WN_SEGURO ,"N",15,2),cnGrvWork)                   // Valor total SEGURO convertido em BRL
IntMS_AADD("EW1_DESADU" , Conv(SWN->WN_DESPESA+IF(SWN->(FIELDPOS("WN_DESPICM"))#0,SWN->WN_DESPICM,0),"N",15,2),cnGrvWork) // Valor total DESPESAS convertido em BRL
IntMS_AADD("EW1_VL_NF"  , Conv(SWN->WN_CIF    ,"N",15,2),cnGrvWork)                   // Valor CIF em BRL
IntMS_AADD("EW1_BS_IPI" , Conv(SWN->WN_IPIBASE,"N",15,2),cnGrvWork)                   // Base de c�lculo do IPI
IntMS_AADD("EW1_ALIPI"  , Conv(SWN->WN_IPITX  ,"N",06,2),cnGrvWork)                   // Al�quota do IPI
IntMS_AADD("EW1_VL_IPI" , Conv(SWN->WN_IPIVAL ,"N",15,2),cnGrvWork)                   // Valor do IPI
IntMS_AADD("EW1_BS_ICM" , Conv(SWN->WN_BASEICM,"N",15,2),cnGrvWork)                   // Base de c�lculo do ICMS
IntMS_AADD("EW1_ALIICM" , Conv(SWN->WN_ICMS_A ,"N",06,2),cnGrvWork)                   // Al�quota do ICMS
IntMS_AADD("EW1_VL_ICM" , Conv(SWN->WN_VALICM ,"N",14,2),cnGrvWork)                   // Valor do ICMS
IntMS_AADD("EW1_BS_II"  , Conv(SWN->WN_CIF    ,"N",15,2),cnGrvWork)                   // Base de c�lculo do II
IntMS_AADD("EW1_ALI_II" , Conv(SWN->WN_IITX   ,"N",06,2),cnGrvWork)                   // Al�quota do II
IntMS_AADD("EW1_VL_II"  , Conv(SWN->WN_IIVAL  ,"N",15,2),cnGrvWork)                   // Valor do II
IntMS_AADD("EW1_VL_PIS" , Conv(SWN->WN_VLRPIS ,"N",15,2),cnGrvWork)                   // Valor do PIS
IntMS_AADD("EW1_BS_PIS" , Conv(SWN->WN_BASPIS ,"N",15,2),cnGrvWork)                   // Base de c�lculo do PIS
IntMS_AADD("EW1_ALIPIS" , Conv(SWN->WN_PERPIS ,"N",06,2),cnGrvWork)                   // Aliquota do PIS
IntMS_AADD("EW1_VL_COF" , Conv(SWN->WN_VLRCOF ,"N",15,2),cnGrvWork)                   // Valor do COFINS
IntMS_AADD("EW1_BS_COF" , Conv(SWN->WN_BASCOF ,"N",15,2),cnGrvWork)                   // Base de c�lculo do COFINS
IntMS_AADD("EW1_AL_COF" , Conv(SWN->WN_PERCOF ,"N",06,2),cnGrvWork)                   // Al�quota do COFINS
IntMS_AADD("EW1_ADUSIC" , Conv(SWN->WN_DESPESA,"N",15,2),cnGrvWork)                   // Enviar as despesas que n�o fizerem parte da base de c�lculo do ICMS
IntMS_AADD("EW1_MOEORI" , SW9->W9_MOE_FOB)                                            // Moeda do processo
IntMS_AADD("EW1_LIQMER" , Conv(SWN->WN_PRUNI * SWN->WN_QUANT,"N",18,8),cnGrvWork)     // Valor da DI em BRL
IntMS_AADD("EW1_MER_US" , Conv(SWN->WN_QUANT *(SWN->WN_PRUNI / SW6->W6_TX_US_D) ,"N",18,8),cnGrvWork) // Valor da DI em USD
IntMS_AADD("EW1_MER_MO" , Conv(SWN->WN_PRECO * SWN->WN_QUANT,"N",15,2),cnGrvWork)     // Valor da DI na moeda do processo
IntMS_AADD("EW1_PRSEDI" , "0")                                                        // 0 (sempre zero)
IntMS_AADD("EW1_MOEOPF" , IF(!EMPTY(SWN->WN_FRETE) ,SW6->W6_FREMOED,"") )             // Moeda do Frete
IntMS_AADD("EW1_FREMOE" , Conv(IF(!EMPTY(SWN->WN_FRETE ),(SWN->WN_FRETE /SW6->W6_TX_FRET),0),"N",15,2),cnGrvWork) // Valor do Frete da DI
IntMS_AADD("EW1_MOE_OP" , IF(!EMPTY(SWN->WN_SEGURO),SW6->W6_SEGMOED,"") )             // Moeda do Seguro
IntMS_AADD("EW1_SEGMOE" , Conv(IF(!EMPTY(SWN->WN_SEGURO),(SWN->WN_SEGURO/SW6->W6_TX_SEG ),0),"N",15,2),cnGrvWork) // Valor do Seguro da DI
IntMS_AADD("EW1_INTEG"  , "N")                                                        // Indicador de integrado Sim ou N�o
IntMS_AADD("EW1_CHAV"   , SWN->WN_HAWB+SWN->WN_TIPO_NF)                               // Chave sequencial da tabela
IntMS_AADD("EW1_DTINT"  , Conv(dDataBase,"DA"),dDataBase)                             // Data da integracao

FOR D := 1 TO LEN(aDadosMS)
   IntMSGrava(aDadosMS[D,1],aDadosMS[D,2],aDadosMS[D,3])
NEXT

If nTipo =1
   If ASCAN(aDelHAWB,SWN->WN_HAWB+SWN->WN_TIPO_NF) = 0
      AADD (aDelHAWB,SWN->WN_HAWB+SWN->WN_TIPO_NF)
      EW1->(DbSetOrder(2))
      If EW1->(Dbseek(SWN->WN_HAWB+SWN->WN_TIPO_NF))
         Do While EW1->(!Eof()) .AND. EW1->CHAV == SWN->WN_HAWB+SWN->WN_TIPO_NF
            EW1->(DBDELETE())
            EW1->(DbSkip())
         EndDo
      EndIf
   EndIf

   EW1->(RecLock("EW1",.T.))
   AvReplace("Work","EW1")
   EW1->(MsUnLock())

ElseIf nTipo = 2

   FWRITE(nArq,CHR(13)+CHR(10))//FINAL DE LINHA

EndIf

Return .T.


*----------------------------------------------------*
Static Function IntMS_AADD(cCampo,cGrvTXT,cnGrvWork)
*----------------------------------------------------*

AADD(aDadosMS,{cCampo,cGrvTXT,cnGrvWork})

Return .T.


*----------------------------------------------------*
Static Function IntMSGrava(cCampo,cGrvTXT,cnGrvWork)
*----------------------------------------------------*

Work->(FIELDPUT( FIELDPOS(cCampo) , IF(cnGrvWork=NIL,cGrvTXT,cnGrvWork) ))

IF nTipo = 2
   FWRITE(nArq,cGrvTXT+CHR(9))
ENDIF

cnGrvWork:=NIL

RETURN .T.


*--------------------------------------------*
Static Function Conv(nInf,cTipo,nInt,nDec)
*--------------------------------------------*

IF cTipo == "N"                    

   cnGrvWork:=nInf//Gravacao do Work
   cGrvTXT  :=STR(nInf,nInt+nDec+1,nDec)
   cGrvTXT  :=STRTRAN(cGrvTXT,".",",")

ELSEIF cTipo == "DA"//AAAAMMAA

   cGrvTXT  :=Str(Year(nInf),4,0)+Substr(dtoc(nInf),04,02)+Substr(dtoc(nInf),01,02)	
   cnGrvWork:=VAL(cGrvTXT) //Gravacao do Work

ELSEIF cTipo == "D"

   cGrvTXT:=Substr(dtoc(nInf),01,02)+Substr(dtoc(nInf),04,02)+Str(Year(nInf),4,0)

ENDIF
	
Return cGrvTXT//Gravacao do TXT


*------------------------------------------------------------*
STATIC FUNCTION GerTCQuery(cQuery,cAlias)
*------------------------------------------------------------*

cQuery := ChangeQuery(cQuery)

nCod := EasyCreateFile("cQuery.TXT")
IF nCod # -1
   FWrite(nCod,cQuery)
   FClose(nCod)
ENDIF

IF SELECT(cAlias) # 0
   (cAlias)->(DBCLOSEAREA())
ENDIF

TcQuery cQuery ALIAS (cAlias) NEW

IF !USED()
   MSGSTOP("Nao foi poss�vel executar a QUERY : "+ALLTRIM(cQuery)+", Verificar Arquivo: cQuery.TXT")
   RETURN .F.
ENDIF

IF (cAlias)->(EOF()) .AND. (cAlias)->(BOF())
   MSGSTOP("N�o foram encontrados registros que correspondam aos crit�rios de sele��o.", "Aviso")
   RETURN .F.
ENDIF

RETURN .T.


*------------------------------*
STATIC FUNCTION INTMSBOX()
*------------------------------*

IF nTipo = 1
   oRadio2:ENABLE()
   oGet:DISABLE()  
ELSEIF nTipo = 2
   oGet:ENABLE()
   oRadio2:DISABLE()  
ENDIF

RETURN .T.


//Programa...: EICINTOLMS()
//Autor......: Alex Wallauer (AWR)
//Data.......: 06/07/2006
//Parametro..: nOLTipo ==> 1 = Banco de Dados , 2 = Arquivo texto
//Objetivo...: Gerar dados para integra��o fiscal apos a gravacao da Nota Fiscal (OnLine), sem tela, s� do processo posicionado
*---------------------------------------------------*
User Function EICINTOLMS(nOLTipo)
*---------------------------------------------------*

Local nSeq:=1

Private nTipo  := nOLTipo//Tipo da integracao => 1 = Banco de Dados , 2 = Arquivo texto
Private cArqDBF:= ""
Private dDtIni := ctod("")                  
Private dDtFim := ctod("")
Private lEnviados:= .F.//Controle de processos enviados ou nao enviados

IF nTipo = 2
   
   cDir:=EasyGParam("MV_AVG0175",,AllTrim(GetSrvProfString("StartPath","")))
   cArqTXT := Alltrim(cDir) + "\" + "INTFIS_EIC_" + Conv(Date(),"D") + "_" + AllTrim(str(nSeq)) + ".TXT" + SPACE(100)
   DO While File(ALLTRIM(cArqTXT))
      nSeq++
      cArqTXT := Alltrim(cDir) + "\" + "INTFIS_EIC_" + Conv(Date(),"D") + "_" + AllTrim(str(nSeq)) + ".TXT" + SPACE(100)
   Enddo   

ENDIF

lOK:=.F.
Processa({|| LOK:=LeDados(.T.,SW6->W6_HAWB) },"Gerando arquivo de integra��o fiscal")

IF !LOK
   Ferase(cArqTxt)
ENDIF

IF SELECT("Work") # 0
   Work->(E_EraseArq(cArqDBF))
ENDIF

RETURN .F.
