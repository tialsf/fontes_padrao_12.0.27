#INCLUDE "Protheus.ch"
#INCLUDE "Average.ch"

*----------------------*
User Function UAV10ARG(o)
*----------------------*
Private cLog := "Esse ambiente n�o est� preparado para executar este update. Favor entrar em contato com o helpdesk Average."+CHR(13)+CHR(10)

If FindFunction("AVUpdate01")
   oUpd           := AVUpdate01():New()
   //oUpd:cPaises := "ARG" // JVR - 09/03/10 - retirado para ser usado em outras localiza��es e n�o somente em ARG.
   oUpd:cPaises   := LOCALIZACOES
   oUpd:aChamados := { {EIC,{|o| UI721703(o)}},; //KKK=(EIC,EEC,EDC,EFF,ECO)/M=Modulo(I=EIC,E=EEC,D=EDC,F=EFF,C=ECO)/xxxxxx=Chamado Average
                       {EIC,{|o| UI721802(o)}},;  
                       {EIC,{|o| UI721748(o)}},;
                       {EIC,{|o| UI721764(o)}},;
                       {EIC,{|o| UI721766(o)}},;
                       {EIC,{|o| UI721768(o)}},;
                       {EIC,{|o| UI721770(o)}},;
                       {EIC,{|o| UI721772(o)}},;
                       {EIC,{|o| UI721777(o)}},;
                       {EIC,{|o| UI721817(o)}},;
                       {EIC,{|o| UI721784(o)}},;
                       {EIC,{|o| UI721846(o)}},;
                       {EIC,{|o| UI713745(o)}},;
                       {EIC,{|o| UI721857(o)}},;
                       {EIC,{|o| UI721894(o)}},;
                       {EIC,{|o| UI722149(o)}},;
                       {EIC,{|o| UI722169(o)}},;
                       {EIC,{|o| UI722171(o)}},;
                       {EIC,{|o| UI722172(o)}},;
                       {EIC,{|o| UI722174(o)}},;
                       {EIC,{|o| UI706485(o)}}}//JVR - 09/03/10 - Atualiza��o do campo WD_DOCTO.
                         
                                        
   oUpd:Init(o)           
Else
   MsgStop(cLog)
EndIf

Return

*-------------------------*
Static Function UI721703(o)
*-------------------------*
Local hFile, hFile2
Local cBuffer    := ""
Local nSize      := 0
Local nInc 
Local cLine      := ""
Local aMenu      := {}
Local nLidos     := 0
Local cMenu      := ""
Local lAtu       := .F.
Local bWhile     := .F.

Begin Sequence
   cRet  := ""
   cModulo := "sigaeic" //Importa��o

   //Seleciona o arquivo de menu a ser alterado.
   cFile := cModulo + ".xnu"
   hFile := fOpen(cFile,FO_READWRITE+FO_EXCLUSIVE)

   IF fError() <> 0
      MsgStop("N�o foi poss�vel abrir o arquivo de menu.","Aviso")
      lRet := .F.
      Break
   Endif

   //Verifica o Tamaqnho total do arquivo
   nSize := fSeek(hFile,0,2)

   //Posiciona no Inicio do Arquivo
   FSeek(hFile,0)      
   ProcRegua(nSize)
   //carrega o arquivo.
   Do While nLidos < nSize
      IncProc("Carregando arquivo: " + Alltrim(cFile))
      nLidos += o:LerLinha(hFile,@cLine,nSize)
      IF Empty(cLine)
         Loop
      Endif
      aAdd(aMenu,Alltrim(cLine)) 
   EndDo
   
   //Fecha arquivo
   FClose(hFile)

   If aScan(aMenu,{|e| Upper('<Title lang="es">Tablas Siscomex</Title>') $ Upper(StrTran(e,CHR(9),""))}) != 0	
      nPos := aScan(aMenu,{|e| Upper('<Title lang="es">Tablas Siscomex</Title>') $ Upper(StrTran(e,CHR(9),""))}) - 2
      aMenu[nPos]:= TAB +''
      lAtu := .T.
      DO WHILE !('</Menu>' $ ALLTRIM(aMenu[nPos]))
       		aMenu[nPos] := ''	
      	nPos++
      ENDDO
      aMenu[nPos]:= TAB +''
   EndIf                                                        
   
   If !lAtu
      cRet += "N�o foi necess�ria nenhuma atualiza��o." + ENTER
      Break
   EndIf
                             
   //Renomeia o menu antigo
   FRename(cFile, cModulo + "_OLD.XNU")

   hFile2 := FCreate(cFile,0)
   ProcRegua(len(aMenu))
   For nInc := 1 to Len(aMenu)
      IncProc("Atualizando Menu" + AllTrim(cModulo) + "... ")      
      cBuffer += aMenu[nInc] + ENTER
   Next    

   Fwrite(hFile2,cBuffer,Len(cBuffer))  
   fClose(hFile2)         
End Sequence

If Empty(cRet)
   cRet := "Menu do m�dulo " + AllTrim(cModulo) + " atualizado." + ENTER
EndIf


nOpc := 1   
PerguntAtu(nOpc)

Return cRet 

*-------------------------*
Static Function UI721802(o)
*-------------------------*

   /////////////////////////
   //Cria��o de Par�metros//
   /////////////////////////
   //             "X6_FILIAL" ,"X6_VAR"     ,"X6_TIPO" ,"X6_DESCRIC"                              ,"X6_DSCSPA"                               ,"X6_DSCENG"                               ,"X6_DESC1" ,"X6_DSCSPA1" ,"X6_DSCENG1" ,"X6_DESC2"  ,"X6_DSCSPA2" ,"X6_DSCENG2" ,"X6_CONTEUD" ,"X6_CONTSPA" ,"X6_CONTENG" ,"X6_PROPRI" ,"X6_PYME"
   Aadd(o:aSX6,{ "  "       ,"MV_EMAILTE"   ,"C"       ,"Email de liberacao de debito automatico" ,"Email de liberacao de debito automatico" ,"Email de liberacao de debito automatico" ,           ,             ,             ,            ,             ,             ,             ,             ,             ,            ,          })
   Aadd(o:aSX6,{ "  "       ,"MV_EMAILCP"   ,"C"       ,"Email de destino do adiantamento"        ,"Email de destino do adiantamento"        ,"Email de destino do adiantamento"        ,           ,             ,             ,            ,             ,             ,             ,             ,             ,            ,          })
   
Return NIL

*-------------------------*
Static Function UI721748(o)
*-------------------------*
 
   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //             "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
     Aadd(o:aSX3,{             ,           ,"Y5_SCAC"    ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,EIC_NUSADO  ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,           ,"Y5_CODIN"   ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,EIC_NUSADO  ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     
Return NIL

*-------------------------*
Static Function UI721764(o)
*-------------------------*

   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //             "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
     Aadd(o:aSX3,{             ,           ,"Y9_UNCODE"  ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,EIC_NUSADO  ,                  ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
  
Return NIL

*-------------------------*
Static Function UI721766(o)
*-------------------------*

   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //             "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
     Aadd(o:aSX3,{             ,   "01"    ,"YW_FILIAL"  ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "02"    ,"YW_COD"     ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "03"    ,"YW_NOME"    ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "04"    ,"YW_END"     ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "05"    ,"YW_BAIRRO"  ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "06"    ,"YW_MUN"     ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "07"    ,"YW_EST"     ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "08"    ,"YW_PAIS"    ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "09"    ,"YW_DESCPAI" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "10"    ,"YW_CEP"     ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "11"    ,"YW_UNIDFED" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            , EIC_USADO      ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "12"    ,"YW_TEL"     ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "13"    ,"YW_FAX"     ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "14"    ,"YW_CONTATO" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "15"    ,"YW_FORN"    ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "16"    ,"YW_LOJA"    ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "17"    ,"YW_MINIMO"  ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "18"    ,"YW_MAXIMO"  ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "19"    ,"YW_VLR_MIN" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "20"    ,"YW_PERCENT" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "21"    ,"YW_VLR_02"  ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "22"    ,"YW_PERC_02" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "23"    ,"YW_VLR_03"  ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "24"    ,"YW_PERC_03" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "25"    ,"YW_VLR_04"  ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "26"    ,"YW_PERC_04" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "27"    ,"YW_VLR_05"  ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,   "28"    ,"YW_PERC_05" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,                ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })

Return NIL

*-------------------------*
Static Function UI721768(o)
*-------------------------*
Local hFile, hFile2
Local cBuffer    := ""
Local nSize      := 0
Local nInc 
Local cLine      := ""
Local aMenu      := {}
Local nLidos     := 0
Local cMenu      := ""
Local lAtu       := .F.
Local bWhile     := .F.

Begin Sequence
   cRet  := ""
   cModulo := "sigaeic" //Importa��o

   //Seleciona o arquivo de menu a ser alterado.
   cFile := cModulo + ".xnu"
   hFile := fOpen(cFile,FO_READWRITE+FO_EXCLUSIVE)

   IF fError() <> 0
      MsgStop("N�o foi poss�vel abrir o arquivo de menu.","Aviso")
      lRet := .F.
      Break
   Endif

   //Verifica o Tamaqnho total do arquivo
   nSize := fSeek(hFile,0,2)

   //Posiciona no Inicio do Arquivo
   FSeek(hFile,0)      
   ProcRegua(nSize)
   //carrega o arquivo.
   Do While nLidos < nSize
      IncProc("Carregando arquivo: " + Alltrim(cFile))
      nLidos += o:LerLinha(hFile,@cLine,nSize)
      IF Empty(cLine)
         Loop
      Endif
      aAdd(aMenu,Alltrim(cLine)) 
   EndDo
   
   //Fecha arquivo
   FClose(hFile)

   If aScan(aMenu,{|e| Upper('<Title lang="es">Ipi de Pauta</Title>') $ Upper(StrTran(e,CHR(9),""))}) != 0	
      nPos := aScan(aMenu,{|e| Upper('<Title lang="es">Ipi de Pauta</Title>') $ Upper(StrTran(e,CHR(9),""))}) - 2
      aMenu[nPos]:= TAB +''
      lAtu := .T.
      DO WHILE !('</MenuItem>' $ ALLTRIM(aMenu[nPos]))
       		aMenu[nPos] := ''	
      	nPos++
      ENDDO
      aMenu[nPos]:= TAB +''
   EndIf                                                        
   
   If !lAtu
      cRet += "N�o foi necess�ria nenhuma atualiza��o." + ENTER
      Break
   EndIf
                             
   //Renomeia o menu antigo
   FRename(cFile, cModulo + "_OLD.XNU")

   hFile2 := FCreate(cFile,0)
   ProcRegua(len(aMenu))
   For nInc := 1 to Len(aMenu)
      IncProc("Atualizando Menu" + AllTrim(cModulo) + "... ")      
      cBuffer += aMenu[nInc] + ENTER
   Next    

   Fwrite(hFile2,cBuffer,Len(cBuffer))  
   fClose(hFile2)         
End Sequence

If Empty(cRet)
   cRet := "Menu do m�dulo " + AllTrim(cModulo) + " atualizado." + ENTER
EndIf


nOpc := 1   
PerguntAtu(nOpc)

Return cRet

*-------------------------*
Static Function UI721770(o)
*-------------------------*
Local hFile, hFile2
Local cBuffer    := ""
Local nSize      := 0
Local nInc 
Local cLine      := ""
Local aMenu      := {}
Local nLidos     := 0
Local cMenu      := ""
Local lAtu       := .F.
Local bWhile     := .F.

Begin Sequence
   cRet  := ""
   cModulo := "sigaeic" //Importa��o

   //Seleciona o arquivo de menu a ser alterado.
   cFile := cModulo + ".xnu"
   hFile := fOpen(cFile,FO_READWRITE+FO_EXCLUSIVE)

   IF fError() <> 0
      MsgStop("N�o foi poss�vel abrir o arquivo de menu.","Aviso")
      lRet := .F.
      Break
   Endif

   //Verifica o Tamaqnho total do arquivo
   nSize := fSeek(hFile,0,2)

   //Posiciona no Inicio do Arquivo
   FSeek(hFile,0)      
   ProcRegua(nSize)
   //carrega o arquivo.
   Do While nLidos < nSize
      IncProc("Carregando arquivo: " + Alltrim(cFile))
      nLidos += o:LerLinha(hFile,@cLine,nSize)
      IF Empty(cLine)
         Loop
      Endif
      aAdd(aMenu,Alltrim(cLine)) 
   EndDo
   
   //Fecha arquivo
   FClose(hFile)

   If aScan(aMenu,{|e| Upper('<Title lang="es">Eventos Contables</Title>') $ Upper(StrTran(e,CHR(9),""))}) != 0	
      nPos := aScan(aMenu,{|e| Upper('<Title lang="es">Eventos Contables</Title>') $ Upper(StrTran(e,CHR(9),""))}) - 2
      aMenu[nPos]:= TAB +''
      lAtu := .T.
      DO WHILE !('</MenuItem>' $ ALLTRIM(aMenu[nPos]))
       		aMenu[nPos] := ''	
      	nPos++
      ENDDO
      aMenu[nPos]:= TAB +''
   EndIf                                                        
   
   If !lAtu
      cRet += "N�o foi necess�ria nenhuma atualiza��o." + ENTER
      Break
   EndIf
                             
   //Renomeia o menu antigo
   FRename(cFile, cModulo + "_OLD.XNU")

   hFile2 := FCreate(cFile,0)
   ProcRegua(len(aMenu))
   For nInc := 1 to Len(aMenu)
      IncProc("Atualizando Menu" + AllTrim(cModulo) + "... ")      
      cBuffer += aMenu[nInc] + ENTER
   Next    

   Fwrite(hFile2,cBuffer,Len(cBuffer))  
   fClose(hFile2)         
End Sequence

If Empty(cRet)
   cRet := "Menu do m�dulo " + AllTrim(cModulo) + " atualizado." + ENTER
EndIf


nOpc := 1   
PerguntAtu(nOpc)

Return cRet

*-------------------------*
Static Function UI721772(o)
*-------------------------*

   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //             "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
     Aadd(o:aSX3,{             ,           ,"W0_NATUREZ" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,                  ,       ,           ,                ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
  
Return NIL

*-------------------------*
Static Function UI721777(o)
*-------------------------*

   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //             "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
     Aadd(o:aSX3,{             ,           ,"W2_MOEDA"   ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,'"U$S"'          ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,           ,"W2_PARID_U" ,          ,             ,             ,               ,"Paridad U$S"  ,                      ,              ,               ,                ,             ,            ,            ,                 ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
   
Return NIL 

*-------------------------*
Static Function UI721817(o)
*-------------------------*
  
   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //             "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
     Aadd(o:aSX3,{             ,"06"       ,"WI_IDVL"    ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,                  ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,"07"       ,"WI_MOEDA"   ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,                  ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,"08"       ,"WI_PERCAPL" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,                  ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,"09"       ,"WI_DESPBAS" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,                  ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,"10"       ,"WI_VALOR"   ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,                  ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,"11"       ,"WI_QTDDIAS" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,                  ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,"12"       ,"WI_VAL_MAX" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,                  ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{             ,"13"       ,"WI_VAL_MIN" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,                  ,       ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
   
Return NIL

*-------------------------*
Static Function UI721784(o)
*-------------------------*
 
   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //             "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
     Aadd(o:aSX3,{             ,           ,"W6_VMINSEG" ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,            ,            ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            , "2"        ,           ,            ,           ,           ,            ,           ,         })
                                                                                                                                                                                                                                                                             
Return NIL

*-------------------------*
Static Function UI721846(o)
*-------------------------*
 
   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //             "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"   ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"                                              ,"X3_USADO"  ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
     Aadd(o:aSX3,{ "SWD"       , "42"      ,"WD_CAI"     ,"C"       ,14           ,0            ,"CAI"          ,"CAI"          ,"CAI"                 ,"CAI"          ,"CAI"          ,"CAI"           ,"@!"         ,                                                        ,EIC_USADO   ,                 ,        ,           ,                ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{ "SWD"       , "43"      ,"WD_VENCAI"  ,"D"       ,8            ,0            ,"Vencto. CAI"  ,"Vencto. CAI"  ,"Vencto. CAI"         ,"Vencto. CAI"  ,"Vencto. CAI"  ,"Vencto. CAI"   ,"@D"         ,"IIF(!EMPTY(M->WD_CAI),M->WD_VENCAI>ddatabase,vazio())" ,EIC_USADO   ,                 ,        ,           ,                ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{ "SWD"       , "44"      ,"WD_PROVENT" ,"C"       ,2            ,0            ,"Prov. Entrega","Prov. Entrega","Prov. Entrega"       ,"Prov. Entrega","Prov. Entrega","Prov. Entrega" ,"@!"         ,'(Vazio().or.ExistCpo("SX5","12"+M->WD_PROVENT))'       ,EIC_USADO   ,                 ,        ,           ,                ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
                                                                                                                                                                                                                                                                             
Return NIL

*-------------------------*
Static Function UI713745(o)   // Update com fonte na produ��o criado no modelo antigo //
*-------------------------*   //   Deve ser executado antes do update 721857  // 
 
   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //             "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
     Aadd(o:aSX3,{ "SW7"       , "33"      ,"W7_TES"     ,"C"       ,3            ,0            ,"TES"          ,"TES"          ,"TES"                 ,"TES"         ,"TES"          ,"TES"           ,"@!"         ,            ,EIC_USADO   ,                 ,        ,1          ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,           ,            ,           ,         })
     Aadd(o:aSX3,{ "SWN"       , "96"      ,"WN_TES"     ,"C"       ,3            ,0            ,"TES"          ,"TES"          ,"TES"                 ,"TES"         ,"TES"          ,"TES"           ,"@!"         ,            ,EIC_USADO   ,                 ,        ,1          ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,           ,            ,           ,         })
                                                                                                                                                                                                                                                                                  
Return NIL

*-------------------------*
Static Function UI721857(o)
*-------------------------*
 
 ///////////////////////////////////
   //Carregamento do Help de Campos //
   ///////////////////////////////////
   //   **Em caso de Help de campo (F1) somente usar o aHelpProb com o Nome do campo**
      Aadd(o:aHelpProb,{"WN_TES",{"WN_TES ","C�digo do tipo de Entrada e/ou Sa�da"}})
      Aadd(o:aHelpProb,{"W7_TES",{"W7_TES ","C�digo do tipo de Entrada e/ou Sa�da"}}) 
                                                                                                                                                                                                                                                                                
Return NIL                                                     

*-------------------------*
Static Function UI721894(o)
*-------------------------*

   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////
   //           "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"  ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"   ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"   ,"X3_PICTURE"           ,"X3_VALID"       ,"X3_USADO"   ,"X3_RELACAO","X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
   Aadd(o:aSX3,{   "EI1"     ,"32"       ,"EI1_VALIM9","N"       ,14           ,2            ,"Valor Imp. 9" ,"Valor Imp. 9" ,"Valor Imp. 9","Valor Imp. 9","Valor Imp. 9" ,"Valor Imp. 9" ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI1"     ,"33"       ,"EI1_VALIMH","N"       ,14           ,2            ,"Valor Imp. H" ,"Valor Imp. H" ,"Valor Imp. H","Valor Imp. H","Valor Imp. H" ,"Valor Imp. H" ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI1"     ,"34"       ,"EI1_VALIMI","N"       ,14           ,2            ,"Valor Imp. I" ,"Valor Imp. I" ,"Valor Imp. I","Valor Imp. I","Valor Imp. I" ,"Valor Imp. I" ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI1"     ,"35"       ,"EI1_BASIM9","N"       ,14           ,2            ,"Base Imp. 9"  ,"Base Imp. 9"  ,"Base Imp. 9" ,"Base Imp. 9" ,"Base Imp. 9"  ,"Base Imp. 9"  ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI1"     ,"36"       ,"EI1_BASIMH","N"       ,14           ,2            ,"Base Imp. H"  ,"Base Imp. H"  ,"Base Imp. H" ,"Base Imp. H" ,"Base Imp. H"  ,"Base Imp. H"  ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI1"     ,"37"       ,"EI1_BASIMI","N"       ,14           ,2            ,"Base Imp. I"  ,"Base Imp. I"  ,"Base Imp. I" ,"Base Imp. I" ,"Base Imp. I"  ,"Base Imp. I"  ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })

   Aadd(o:aSX3,{   "EI2"     ,"81"       ,"EI2_VALIM9","N"       ,14           ,2            ,"Valor Imp. 9" ,"Valor Imp. 9" ,"Valor Imp. 9","Valor Imp. 9","Valor Imp. 9" ,"Valor Imp. 9" ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI2"     ,"82"       ,"EI2_VALIMH","N"       ,14           ,2            ,"Valor Imp. H" ,"Valor Imp. H" ,"Valor Imp. H","Valor Imp. H","Valor Imp. H" ,"Valor Imp. H" ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI2"     ,"83"       ,"EI2_VALIMI","N"       ,14           ,2            ,"Valor Imp. I" ,"Valor Imp. I" ,"Valor Imp. I","Valor Imp. I","Valor Imp. I" ,"Valor Imp. I" ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI2"     ,"84"       ,"EI2_ALQIM9","N"       ,6            ,2            ,"Aliq. Imp. 9" ,"Aliq. Imp. 9" ,"Aliq. Imp. 9","Aliq. Imp. 9","Aliq. Imp. 9" ,"Aliq. Imp. 9" ,"@E  999.99"           ,                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI2"     ,"85"       ,"EI2_ALQIMH","N"       ,6            ,2            ,"Aliq. Imp. H" ,"Aliq. Imp. H" ,"Aliq. Imp. H","Aliq. Imp. H","Aliq. Imp. H" ,"Aliq. Imp. H" ,"@E  999.99"           ,                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI2"     ,"86"       ,"EI2_ALQIMI","N"       ,6            ,2            ,"Aliq. Imp. I" ,"Aliq. Imp. I" ,"Aliq. Imp. I","Aliq. Imp. I","Aliq. Imp. I" ,"Aliq. Imp. I" ,"@E  999.99"           ,                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI2"     ,"87"       ,"EI2_BASIM9","N"       ,14           ,2            ,"Base Imp. 9"  ,"Base Imp. 9"  ,"Base Imp. 9" ,"Base Imp. 9" ,"Base Imp. 9"  ,"Base Imp. 9"  ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI2"     ,"88"       ,"EI2_BASIMH","N"       ,14           ,2            ,"Base Imp. H"  ,"Base Imp. H"  ,"Base Imp. H" ,"Base Imp. H" ,"Base Imp. H"  ,"Base Imp. H"  ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI2"     ,"89"       ,"EI2_BASIMI","N"       ,14           ,2            ,"Base Imp. I"  ,"Base Imp. I"  ,"Base Imp. I" ,"Base Imp. I" ,"Base Imp. I"  ,"Base Imp. I"  ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })

   Aadd(o:aSX3,{   "EI3"     ,"40"       ,"EI3_VALIM9","N"       ,14           ,2            ,"Valor Imp. 9" ,"Valor Imp. 9" ,"Valor Imp. 9","Valor Imp. 9","Valor Imp. 9" ,"Valor Imp. 9" ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI3"     ,"41"       ,"EI3_VALIMH","N"       ,14           ,2            ,"Valor Imp. H" ,"Valor Imp. H" ,"Valor Imp. H","Valor Imp. H","Valor Imp. H" ,"Valor Imp. H" ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI3"     ,"42"       ,"EI3_VALIMI","N"       ,14           ,2            ,"Valor Imp. I" ,"Valor Imp. I" ,"Valor Imp. I","Valor Imp. I","Valor Imp. I" ,"Valor Imp. I" ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI3"     ,"43"       ,"EI3_ALQIM9","N"       ,6            ,2            ,"Aliq. Imp. 9" ,"Aliq. Imp. 9" ,"Aliq. Imp. 9","Aliq. Imp. 9","Aliq. Imp. 9" ,"Aliq. Imp. 9" ,"@E  999.99"           ,                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI3"     ,"44"       ,"EI3_ALQIMH","N"       ,6            ,2            ,"Aliq. Imp. H" ,"Aliq. Imp. H" ,"Aliq. Imp. H","Aliq. Imp. H","Aliq. Imp. H" ,"Aliq. Imp. H" ,"@E  999.99"           ,                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI3"     ,"45"       ,"EI3_ALQIMI","N"       ,6            ,2            ,"Aliq. Imp. I" ,"Aliq. Imp. I" ,"Aliq. Imp. I","Aliq. Imp. I","Aliq. Imp. I" ,"Aliq. Imp. I" ,"@E  999.99"           ,                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI3"     ,"46"       ,"EI3_BASIM9","N"       ,14           ,2            ,"Base Imp. 9"  ,"Base Imp. 9"  ,"Base Imp. 9" ,"Base Imp. 9" ,"Base Imp. 9"  ,"Base Imp. 9"  ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI3"     ,"47"       ,"EI3_BASIMH","N"       ,14           ,2            ,"Base Imp. H"  ,"Base Imp. H"  ,"Base Imp. H" ,"Base Imp. H" ,"Base Imp. H"  ,"Base Imp. H"  ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
   Aadd(o:aSX3,{   "EI3"     ,"48"       ,"EI3_BASIMI","N"       ,14           ,2            ,"Base Imp. I"  ,"Base Imp. I"  ,"Base Imp. I" ,"Base Imp. I" ,"Base Imp. I"  ,"Base Imp. I"  ,"@E  99,999,999,999.99",                 ,TODOS_MODULOS,            ,        ,1          ,TAM+DEC+ORDEM   ,           ,             ,            ,"N"         ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })
Return NIL 

*-------------------------*
Static Function UI722149(o)//M=Modulo(I=EIC,E=EEC,D=EDC,F=EFF,C=ECO)/xxxxxx=Chamado Average
*-------------------------*

   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //              "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"          ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"     ,"X3_TITSPA"     ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID" ,"X3_USADO"   ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
      Aadd(o:aSX3,{             ,           ,"W6_MULTIMO"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_DT_DSE"         ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_NR_DSE"         ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_NR_DDE"         ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_NR_PROC"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_UL_DSE"         ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_FORNECF"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_LOJAF"          ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_FORNECS"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_LOJAS"          ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_URF_ENT"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_URF_DES"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_SETORRA"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_ARMAZEM"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_BCOPGTO"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_AGEPGTO"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_CTAPGTO"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_FUNDAP"         ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_UFDESEM"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
      Aadd(o:aSX3,{             ,           ,"W6_QBRPICO"        ,          ,             ,             ,                ,                ,                      ,              ,               ,                ,             ,           ,EIC_NUSADO   ,                 ,        ,           ,                ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })
   
   
Return NIL

*-------------------------*
Static Function UI722169(o)//M=Modulo(I=EIC,E=EEC,D=EDC,F=EFF,C=ECO)/xxxxxx=Chamado Average
*-------------------------*

   
   //////////////////////
   //Cria��o dos Campos//                                                                                                                    
   //////////////////////  

   //             "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"     ,"X3_USADO"  ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX" ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
     Aadd(o:aSX3,{             ,"C4"       ,"W6_DT"      ,          ,             ,             ,               ,               ,                      ,              ,               ,                ,             ,"DI600Valid()" ,            ,                  ,       ,           ,""              ,           ,             ,            ,            ,            ,            ,             ,            ,          ,               ,               ,             ,          ,            ,            ,            ,           ,            ,           ,           ,            ,           ,         })

   
   
Return NIL   

*-------------------------*
Static Function UI722171(o)
*-------------------------*
  //            "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"     ,"X3_USADO"    ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX"     ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
   Aadd(o:aSX3,{"SYD"        ,"35"       ,"YD_FOBEXEN" ,"C"       ,1            ,0            ,"FOB Exento"   ,"FOB Exento"   ,"FOB Exento"          ,"FOB Exento"  ,"FOB Exento"   ,"FOB Exento"    ,"@!"         ,""             ,TODOS_MODULOS ,""               ,""      ,0          ,USO             ,""         ,""           ,""          ,"N"         ,"A"         ,"R"         ,""           ,""          ,"1=Sim;2=Nao" ,"1=Sim;2=Nao"  ,"1=Sim;2=Nao"  ,""           ,""        ,""          ,""          ,"1"         ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })

Return NIL
          
*-------------------------*
Static Function UI722172(o)
*-------------------------*
//              "X3_ARQUIVO" ,"X3_ORDEM" ,"X3_CAMPO"   ,"X3_TIPO" ,"X3_TAMANHO" ,"X3_DECIMAL" ,"X3_TITULO"    ,"X3_TITSPA"    ,"X3_TITENG"           ,"X3_DESCRIC"  ,"X3_DESCSPA"   ,"X3_DESCENG"    ,"X3_PICTURE" ,"X3_VALID"     ,"X3_USADO"    ,"X3_RELACAO"     ,"X3_F3" ,"X3_NIVEL" ,"X3_RESERV"     ,"X3_CHECK" ,"X3_TRIGGER" ,"X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT" ,"X3_VLDUSER","X3_CBOX"     ,"X3_CBOXSPA"   ,"X3_CBOXENG"   ,"X3_PICTVAR" ,"X3_WHEN" ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" ,"X3_PYME"  ,"X3_CONDSQL","X3_CHKSQL","X3_IDXSRV","X3_ORTOGRA","X3_IDXFLD","X3_TELA"
   Aadd(o:aSX3,{"SWN"        ,"B9"       ,"WN_ITEMNF"  ,"C"       ,4            ,0            ,"Item NF"      ,"Item NF"      ,"Item NF"             ,"Item NF"    ,"Item NF"       ,"Item NF"       ,"@!"         ,""             ,TODOS_MODULOS ,""               ,""      ,0          ,USO             ,""         ,""           ,""          ,"N"         ,"A"         ,"R"         ,""           ,""          ,""            ,""             ,""             ,""           ,""        ,""          ,""          ,"1"         ,"N"        ,            ,           ,"N"        ,"N"         ,"N"        ,         })

Return NIL

*-------------------------*
Static Function UI722174(o)
*-------------------------*

   Aadd(o:aSIX,{"SWD"    ,"6"     ,"WD_FILIAL+WD_DOCTO+WD_FORN+WD_LOJA","Docto.+Fornecedor+Loja","Docto.+Fornecedor+Loja","Docto.+Fornecedor+Loja","S"      ,""   ,""         ,"S"       })

Return NIL   

*-------------------------*
Static Function UI706485(o)
*-------------------------*
Local aOrd:= SaveOrd("SX3")

Begin Sequence
   DBSelectArea("SX3")
   SX3->(DBSETORDER(2))
   If SX3->(DBSeek("E2_NUM"))
      Aadd(o:aSX3,{  ,  ,"WD_DOCTO"  ,   ,AVSX3("E2_NUM",3/*AV_TAMANHO*/)  ,      ,      ,      ,     ,    ,       ,       ,         ,             , ,               ,      ,          ,             ,         ,           ,          ,         ,         ,         ,           ,          ,            ,             ,             ,           ,        ,          ,          ,         ,        ,            ,           ,        ,         ,        ,         })
   EndIf
End Sequence 
              
RestOrd(aOrd)

Return
