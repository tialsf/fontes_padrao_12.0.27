
#include "protheus.ch"

function OFAGVmiPedido()
return .t.

/*/{Protheus.doc} mil_ver()
		Versao do fonte modelo novo

		@author Vinicius Gati
		@since  12/06/2017
/*/
Static Function mil_ver()
	If .F.
		mil_ver()
	EndIf
Return "1"

/*/{Protheus.doc} OFAGVmiPedido
	Interface DMS - 5 da defini��o do VMI

	{<br>
	  "data": {<br>
	    "dealerLegalNumber": "99.999.999/0001-99",<br>
	    "extractionDateTime": "2016-11-23T10:45:00+03:00",<br>
	    "order": {<br>
	      "orderId": "34562",<br>
	      "orderAgco": "AOL-123456",<br>
	      "orderIdOriginal": "4567",<br>
	      "deliveredDealerLegalNumber": "11.000.000/0001-00",<br>
	      "orderDate": "2016-11-23",<br>
	      "orderType": "STOCK_ORDER",<br>
	      "supplierLegalNumber": "00.000.000/0001-00",<br>
	      "supplierName": "NOME FANTASIA DO FORNECEDOR",<br>
	      "filter1": "",<br>
	      "filter2": "",<br>
	      "filter3": "",<br>
	      "items": [<br>
	        {<br>
	          "sourceLocation": "REPVT03",<br>
	          "orderLineNumber": "1",<br>
	          "partNumber": "1444437P",<br>
	          "receivedDate": "2016-11-23",<br>
	          "requestedQuantity": 1.5,<br>
	          "receivedQuantity": 1.5,<br>
	          "openQuantity": 1.5,<br>
	          "canceledQuantity": 1.5,<br>
	          "lineStatus": "OPEN"<br>
	        }<br>
	      ]<br>
	    }<br>
	  }<br>
	}<br>

	@author Vinicius Gati
	@since 12/06/2017
/*/
Class OFAGVmiPedido from OFAGVmiBase
	Data cIntName
	Method New() CONSTRUCTOR
	Method Trigger()
	Method TriggerDer()
	Method GetDadosPedido()
	Method getTipo()
EndClass

Method New() Class OFAGVmiPedido
	_Super:New()
	AADD(::aMapValid, {"data"                                 , "V005"})
	AADD(::aMapValid, {"data:order:orderID"                   , "Obri"})
	AADD(::aMapValid, {"data:order:deliveredDealerLegalNumber", "Obri"})
	AADD(::aMapValid, {"data:order:orderDate"                 , "Obri"})
	AADD(::aMapValid, {"data:order:orderDate"                 , "Date"})
	AADD(::aMapValid, {"data:order:orderType"                 , "V004"})
	AADD(::aMapValid, {"data:order:supplierLegalNumber"       , "Obri"})
	AADD(::aMapValid, {"data:order:items:orderLineNumber"     , "Obri"})
	AADD(::aMapValid, {"data:order:items:partNumber"          , "Obri"})
	AADD(::aMapValid, {"data:order:items:requestedQuantity"   , "Obri"})
	AADD(::aMapValid, {"data:order:items:openQuantity"        , "Obri"})
	AADD(::aMapValid, {"data:order:items:lineStatus"          , "Obri"})
	AADD(::aMapValid, {"data:order:items:sourceLocation"      , "V005"})
	::cIntName := "DMS-3"
return self

/*/{Protheus.doc} Trigger
	Evento de envio de dados do PEDIDO, modelo DMS-3 do VMI

	@author Vinicius Gati
	@since 21/06/2017
/*/
Method Trigger(oParams) Class OFAGVmiPedido
	Local cCoord  := ""
	Local oSqlHlp := DMS_SqlHelper():New()
	Local oJson   := DMS_DataContainer():New()
	local oArHlp  := DMS_ArrayHelper():New()
	Local cGrupos := "'" + oArHlp:Join(self:oVmiParametros:grupos(), "','") + oArHlp:Join(self:oVmiParametros:gruposparalelos(), "','") + "'"
	Local cIdPedido := oParams:GetValue("CODIGO")
	Local cQuery    := ""
	cQuery := "SELECT SC7.R_E_C_N_O_"
	cQuery += "  FROM "+oSqlHlp:NoLock('SC7')
	cQuery += "  JOIN "+oSqlHlp:NoLock('SB1')
	cQuery += "    ON SB1.B1_FILIAL = '"+xFilial('SB1')+"'"
	cQuery += "   AND SB1.B1_COD = SC7.C7_PRODUTO "
	cQuery += "   AND SB1.B1_GRUPO IN ("+cGrupos+")"
	cQuery += "   AND SB1.D_E_L_E_T_ = ' '"
	cQuery += " WHERE SC7.C7_FILIAL = '"+xFilial("SC7")+"'"
	cQuery += "   AND SC7.C7_NUM = '"+cIdPedido+"'"
	cQuery += "   AND SC7.D_E_L_E_T_ = ' '"
	If FM_SQL( cQuery ) > 0
		SA1->(dbGoTo(self:oFilHlp:GetCliente(cFilAnt)))
		oJson:SetValue("dealerLegalNumber", self:fmtDoc(self:oVmiParametros:DocMatriz()))
		oJson:SetValue("extractionDateTime", FWTIMESTAMP(5))
		oJson:SetValue("order", self:GetDadosPedido(oParams:GetValue("CODIGO")))
		oJson:GetValue("order"):SetValue("deliveredDealerLegalNumber", self:fmtDoc(SA1->A1_CGC))
		cCoord := self:oVmiJson:Persist(self:cIntName, oParams, {oJson})
		if oParams:GetBool('INICIALIZACAO', .F.) == .F.
			self:TriggerDer(oJson:GetValue("order"):GetValue('items'), cCoord)
		end
	EndIf
Return cCoord

/*/{Protheus.doc} TriggerDer
	Engatilha os eventos derivados para atualizar AGCO
	� feito de 7 em 7 pe�as para melhorar performance de gera��o e envio de jsons

	@author Vinicius Gati
	@since 21/06/2017
/*/
Method TriggerDer(aItems, cCoord) Class OFAGVmiPedido
		Local nX := 1
		Local oVmi := OFAGVmi():New()
		For nX:= 1 to Len(aItems)
			oItem := aItems[nX]
			// somente 1 por vez infelizmente
			oVMi:Trigger({;
				{'EVENTO', oVmi:oVmiMovimentos:Inventario },;
				{'NUMCONTROLE' , cCoord                   },;
				{'ORIGEM', "DMS3_DMS1"                    },;
				{'PECAS' , {oItem:GetValue('partNumber') }} ;
			})
		Next
Return .T.

/*/{Protheus.doc} GetPedido
	Retorna dados conforme DMS-3 com dados de um pedido
	@author Vinicius Gati
	@since 21/06/2017
	@param cC7_NUM, String, C�digo do pedido
/*/
Method GetDadosPedido(cC7_NUM) Class OFAGVmiPedido
	Local aArea      := GetArea()
	Local aAreaA1    := SA1->(GetArea())
	Local aAreaA2    := SA2->(GetArea())
	Local aAreaC7    := SC7->(GetArea())
	local oArHlp     := DMS_ArrayHelper():New()
	Local oSqlHlp    := DMS_SqlHelper():New()
	Local cGrupos    := "'" + oArHlp:Join(self:oVmiParametros:grupos(), "','") + oArHlp:Join(self:oVmiParametros:gruposparalelos(), "','") + "'"
	Local lC7_TIPPED := SC7->(FieldPos("C7_TIPPED")) > 0
	Local cIdPedido := xFilial('SC7')+cC7_NUM
	Local oPedido := DMS_DataContainer():New({;
		{"items", {}} ;
	})

	dbSelectArea('SC7')
	dbSetOrder(1)
	If dbSeek( cIdPedido )

		dbSelectArea('SA2')
		dbSetOrder(1)
		dbSeek( xFiliAl('SA2') + SC7->C7_FORNECE + SC7->C7_LOJA )

		oPedido:SetValue("orderId" , cIdPedido)
		oPedido:SetValue("orderIdOriginal" , cIdPedido)
		oPedido:SetValue("orderType", Iif(lC7_TIPPED, self:getTipo(SC7->C7_TIPPED), ''))
		oPedido:SetValue("orderDate", SC7->C7_EMISSAO)
		if EMPTY(SA2->A2_CGC)
			oPedido:remAttr("supplierLegalNumber")
		else
			oPedido:SetValue("supplierLegalNumber", self:fmtDoc(SA2->A2_CGC))
		end
		oPedido:SetValue("supplierName", SA2->A2_NOME)

		dbSelectArea('SC7')
		While !SC7->(Eof()) .and. SC7->C7_FILIAL + SC7->C7_NUM == cIdPedido
			cSQL := "SELECT R_E_C_N_O_ "
			cSQL += "  FROM "+oSqlHlp:NoLock('SB1')
			cSQL += " WHERE B1_FILIAL = '"+xFilial('SB1')+"' "
			cSQL += "   AND B1_COD    = '"+SC7->C7_PRODUTO+"' "
			cSQL += "   AND B1_GRUPO IN ("+cGrupos+")"
			cSQL += "   AND D_E_L_E_T_ = ' ' "
			If FM_SQL(cSQL) > 0
				oItem := DMS_DataContainer():New()
				//oItem:SetValue("sourceLocation", SC7->...) // de onde vem? provavel que vira de outra integra��o, n�o � documentado nada
				oItem:SetValue("orderLineNumber", SC7->C7_ITEM)
				oItem:SetValue("partNumber", SC7->C7_PRODUTO)
				oItem:SetValue("receivedDate", SC7->C7_DATPRF)
				oItem:SetValue("requestedQuantity", SC7->C7_QUANT)
				oItem:SetValue("receivedQuantity", SC7->C7_QUJE)
				oItem:SetValue("openQuantity",  SC7->C7_QUANT-SC7->C7_QUJE )
				//oItem:SetValue("canceledQuantity", 0) // n�o obrigat�rio, geralmente no protheus abate-se com QUJEo que n�o se enquadra em cancelamento, acho que o melhor � n�o enviar
				if (SC7->C7_QUANT - SC7->C7_QUJE) > 0
					oItem:SetValue("lineStatus", 'OPEN')
				else
					oItem:SetValue("lineStatus", 'CLOSED')
				end
				AADD( oPedido:GetValue('items'), oItem )
			EndIf
			SC7->(dbSkip())
		End
	
	EndIf

	RestArea( aAreaA1 )
	RestArea( aAreaA2 )
	RestArea( aAreaC7 )
	RestArea( aArea   )
Return oPedido

/*/{Protheus.doc} getTipo
	Converte o tipo do pedido para um tipo documentado no VMI.
	@author Vinicius Gati
	@since 21/06/2017
	@param cC7_TIPPED, String, C�digo usado no pedido
/*/
Method getTipo(cC7_TIPPED) Class OFAGVmiPedido
	Local oVmiPar := OFAGVmiParametros():New()
	Do Case
	Case oVmiPar:TipPedRepo(cC7_TIPPED)
		return 'STOCK_ORDER'
	Case oVmiPar:TipPedMP(cC7_TIPPED)
		return 'VOR'
	Case oVmiPar:TipPedGar(cC7_TIPPED)
		return 'SERVICE'
	EndCase
Return 'STOCK_ORDER'
