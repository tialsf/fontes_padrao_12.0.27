#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} OFIA290
Funcoes DMS - Chamadas pelo MATA103 ( ENTRADA POR COMPRA )

@author Andre Luis Almeida
@since 01/06/2020
@version 1.0
@return NIL
/*/
Function OFIA290()
Return NIL

/*/{Protheus.doc} OA2900011_A103NFiscal_PodeClassificar
	Chamada do MATA103 funcao A103NFiscal ( Verifica se pode Classificar a NF )

	@author Andre Luis Almeida
	@since 02/06/2020
/*/
Function OA2900011_A103NFiscal_PodeClassificar( aParametros )
Local lRetorno := .f.
// Verifica se pode classificar a NF - Conferencia de Itens da NF de Entrada
lRetorno := OM3900161_PermiteClassificarNFEntrada(	aParametros[1] ,; // Variavel cTipo do MATA103 ( N=Normal / D=Devolucao )
													aParametros[2] ,; // Variavel cNFiscal do MATA103 ( Nro.NF )
													aParametros[3]  ) // Variavel INCLUI do MATA103
Return lRetorno

/*/{Protheus.doc} OA2900021_A103NFiscal_AposOK
	Chamada do MATA103 funcao A103NFiscal ( Rotina de Inclusao/Alteracao/Exclusao de NF de Compra )

	@author Andre Luis Almeida
	@since 02/06/2020
/*/
Function OA2900021_A103NFiscal_AposOK( aParametros )
Local lRetorno := .f.
 // Apos Ok da Tela - Verifica Conferencia de Entrada - Se houver Divergencia, faz movimentacao do Item
lRetorno := OM3900151_AposOkMATA103(	aParametros[1] ,; // nOpc ( 3-Inclusao / 4-Alteracao / 5-Exclusao )
										aParametros[2] ,; // Opcao OK ( Confirmou a Tela )
										aParametros[3] ,; // Numero da NF
										aParametros[4] ,; // Serie da NF
										aParametros[5] ,; // Fornecedor
										aParametros[6]  ) // Loja
Return lRetorno