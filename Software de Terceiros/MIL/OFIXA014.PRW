#include "OFIXA014.CH"
#include "PROTHEUS.CH"
/*
�����������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������Ŀ��
���Funcao    � OFIXA014 � Autor � Andre Luis Almeida / Luis Delorme � Data � 26/01/09 ���
�������������������������������������������������������������������������������������Ĵ��
���Descricao � Altera��o do Status do Orcamento de Pecas e Servicos                   ���
�������������������������������������������������������������������������������������Ĵ��
���Uso       � Veiculos                                                               ���
��������������������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������������
*/
Function OFIXA014()
Private cCadastro := STR0001 // Orcamento de Pecas e Servicos
Private aRotina   := MenuDef()

Private aCores    := 	{	{'VS1->VS1_STATUS == "0"','BR_VERDE'},;
{'VS1->VS1_STATUS == "2"','BR_PINK'},;
{'VS1->VS1_STATUS == "3"','BR_BRANCO'},;
{'VS1->VS1_STATUS == "4"','BR_AZUL'},;
{'VS1->VS1_STATUS == "5"','BR_MARROM'},;
{'VS1->VS1_STATUS == "F"','BR_AMARELO'},;
{'VS1->VS1_STATUS == "I"','BR_LARANJA'},;
{'VS1->VS1_STATUS == "C"','BR_VERMELHO'},;
{'VS1->VS1_STATUS == "X"','BR_PRETO'} }

//��������������������������������������������������������������Ŀ
//� Endereca a funcao de BROWSE                                  �
//����������������������������������������������������������������
//
mBrowse( 6, 1,22,75,"VS1",,,,,,aCores)
//
Return
/*
�����������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������Ŀ��
���Funcao    � VXA014   � Autor � Andre Luis Almeida / Luis Delorme � Data � 26/01/09 ���
�������������������������������������������������������������������������������������Ĵ��
���Descricao � Montagem da Janela de Orcamento de Pecas e Servicos                    ���
�������������������������������������������������������������������������������������Ĵ��
���Uso       � Veiculos                                                               ���
��������������������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������������
*/
Function OXA014(cAlias,nReg,nOpc)
Local lRet := .f.
// regras de permissao
if  VS1->VS1_STATUS == "X" .or. VS1->VS1_STATUS == "I" .or. VS1->VS1_STATUS == "0"
	MsgInfo(STR0017,STR0004)
	return .f.
endif
//
lRet = OFIXX001(NIL,NIL,NIL,2)
if lRet
	if MsgYesNo(STR0005,STR0004)
		// verifica se precisa desreservar os itens
		cFaseIni := VS1->VS1_STATUS
		cFaseOrc := OI001GETFASE(VS1->VS1_NUMORC)				// Fases do orcamento
		if VS1->VS1_STATUS != "C"
			nPosI := At(cFaseIni,cFaseOrc)
			nPosR := At("R",cFaseOrc)
			nPosA := At(VS1->VS1_STATUS,cFaseOrc)
			cDocto := "X"
			if nPosI < nPosR .and. nPosR < nPosA
				cDocto := OX001RESITE(VS1->VS1_NUMORC,.f.)
			endif
			if cDocto == ""
				MsgInfo(STR0007,STR0004)
			endif
			// verifica se precisa cancelar pedido de liberacao
			if !Empty(VS1->VS1_NUMLIB)
				If TCCanOpen(RetSqlName("VS7"))
					cString := "DELETE FROM "+RetSqlName("VS7")+ " WHERE VS7_FILIAL = '"+ xFilial("VS7")+"' AND VS7_NUMIDE= '"+VS1->VS1_NUMLIB+"'"
					TCSqlExec(cString)
				else
					return .f.
				endif
				If TCCanOpen(RetSqlName("VS6"))
					cString := "DELETE FROM "+RetSqlName("VS6")+ " WHERE VS6_FILIAL = '"+ xFilial("VS6")+"' AND VS6_NUMIDE= '"+VS1->VS1_NUMLIB+"'"
					TCSqlExec(cString)
				else
					return .f.
				endif
			endif
		endif
		DBSelectArea("VS1")
		reclock("VS1",.f.)
		VS1->VS1_NUMLIB := ""
		cVS1StAnt := VS1->VS1_STATUS
		VS1->VS1_STATUS := "0"
		msunlock()
		
		If FindFunction("FM_GerLog")
			//grava log das alteracoes das fases do orcamento
			FM_GerLog("F",VS1->VS1_NUMORC,,VS1->VS1_FILIAL,cVS1StAnt)
		EndIF
		
	endif
endif
//
Return .t.
/*
�����������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������Ŀ��
���Funcao    � MenuDef  � Autor � Andre Luis Almeida / Luis Delorme � Data � 26/01/09 ���
�������������������������������������������������������������������������������������Ĵ��
���Descricao � Menu (AROTINA) - Orcamento de Pecas e Servicos                         ���
�������������������������������������������������������������������������������������Ĵ��
���Uso       � Veiculos                                                               ���
��������������������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������������
*/
Static Function MenuDef()
Local aRotina := {	{ OemtoAnsi(STR0002) 	,"AxPesqui" 		, 0 , 1},;			// Pesquisar
{ OemtoAnsi(STR0003) 	,"OXA014"    		, 0 , 2},;			// Visualizar
{ OemtoAnsi(STR0006) 	,"OXA014LEG"		, 0 , 7} }			// Legenda
//
Return aRotina
/*
�����������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������Ŀ��
���Funcao    �VXA014LEG � Autor � Andre Luis Almeida / Luis Delorme � Data � 26/01/09 ���
�������������������������������������������������������������������������������������Ĵ��
���Descricao � Legenda - Orcamento de Pecas e Servicos                                ���
�������������������������������������������������������������������������������������Ĵ��
���Uso       � Veiculos                                                               ���
��������������������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������������
*/
Function OXA014LEG()
Local aLegenda := {		{'BR_VERDE',STR0008},;		// "Orcamento Digitado"
{'BR_PINK',STR0009},;		// "Margem Pendente"
{'BR_BRANCO',STR0010},;		// "Avaliacao de Credito"
{'BR_AZUL',STR0011},;		// "Aguardando Separacao"
{'BR_MARROM',STR0012},;		// "Aguardando Lib.Diverg."
{'BR_AMARELO',STR0013},;	// "Liberado p/ Faturamento"
{'BR_LARANJA',STR0016},;	// "Importado para O.S."
{'BR_VERMELHO',STR0014},;	// "Cancelado"
{'BR_PRETO',STR0015}}		// "Faturado"
//
BrwLegenda(cCadastro,STR0006,aLegenda)
//
Return
