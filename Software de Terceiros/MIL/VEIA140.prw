#Include "PROTHEUS.CH"
#Include "TOPCONN.CH"
#include 'tbiconn.ch'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'VEIA140.CH'

Static cCodMar   := FMX_RETMAR(GetNewPar("MV_MIL0006",""))
Static oModelVJQ := FWLoadModel( 'VEIA141' )
Static oModelVQ0 := FWLoadModel( 'VEIA142' )
Static oModelVV1 := FWLoadModel( 'VEIA070' )

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VEIA140()

Local bProcess
Local cPerg := "VEIA140"

Private lSchedule := FWGetRunSchedule()

bProcess := { |oSelf| VA1400051_Processa(oSelf) }
CriaSX1(cPerg)

If lSchedule
	VA1400051_Processa()
Else
	oTProces := tNewProcess():New(;
	/* 01 */				"VEIA140",;
	/* 02 */				"Dealer Data Exchange Complete Goods (CGPoll)",;
	/* 03 */				bProcess,;
	/* 04 */				STR0001,;
	/* 05 */				cPerg ,;
	/* 06 */				/*aInfoCustom*/ ,;
	/* 07 */				.t. /* lPanelAux */ ,;
	/* 08 */				 /* nSizePanelAux */ ,;
	/* 09 */				/* cDescriAux */ ,;
	/* 10 */				.t. /* lViewExecute */ ,;
	/* 11 */				.t. /* lOneMeter */ )
EndIf

Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA1400051_Processa()

Local cLocal   := Alltrim(MV_PAR01)
Local nX
Local aVetNome := {}
Local aVetTam  := {}
Local aVetData := {}
Local aVetHora := {}

Local cArquivo  := ""
Local cMoveArq  := ""
Local aPedPro   := {}

Local cCompVQ0  := FwModeAccess("VQ0",1) + FwModeAccess("VQ0",2) + FwModeAccess("VQ0",3)
Local cCompVJR  := FwModeAccess("VJR",1) + FwModeAccess("VJR",2) + FwModeAccess("VJR",3)

Local nPosFil   := 0
Local oAuxFil   := DMS_FilialHelper():New()
Local aFiliais  := oAuxFil:GetAllFilEmpresa(.t.)

Private aRegrVl:= {}
Private aVetGrv:= {}
Private aVStat := {}
Private cStr   := ""
Private dDtaIni:= MV_PAR02
Private oFilJD := DMS_DPM():New()
Private aVQ0VQJ:= {}

Private aMapFilDealerCode := {}

If !(cCompVQ0 == cCompVJR)
	MsgStop(STR0002,STR0003) // "Compartilhamento entre as tabelas VQ0 e VJR est�o divergentes. � necess�rio ajustar para prosseguir com a importa��o!" / "Aten��o"
	Return
EndIf

If Empty(dDtaIni)
	MsgStop(STR0004,STR0003) //"Informe a data inicial que o CGPoll foi implementado!" / "Aten��o"
	Return
EndIf

For nPosFil := 1 to Len(aFiliais)
	AADD( aMapFilDealerCode , { aFiliais[nPosFil] , GetNewPar("MV_MIL0005","", aFiliais[nPosFil]) })
Next nPosFil

//Implicit Trailing Signs
aAdd(aRegrVl,{'{', "0"})
aAdd(aRegrVl,{'}', "-0"})
aAdd(aRegrVl,{'A', "1"})
aAdd(aRegrVl,{'B', "2"})
aAdd(aRegrVl,{'C', "3"})
aAdd(aRegrVl,{'D', "4"})
aAdd(aRegrVl,{'E', "5"})
aAdd(aRegrVl,{'F', "6"})
aAdd(aRegrVl,{'G', "7"})
aAdd(aRegrVl,{'H', "8"})
aAdd(aRegrVl,{'I', "9"})
aAdd(aRegrVl,{'J', "-1"})
aAdd(aRegrVl,{'K', "-2"})
aAdd(aRegrVl,{'L', "-3"})
aAdd(aRegrVl,{'M', "-4"})
aAdd(aRegrVl,{'N', "-5"})
aAdd(aRegrVl,{'O', "-6"}) 
aAdd(aRegrVl,{'P', "-7"})
aAdd(aRegrVl,{'Q', "-8"})
aAdd(aRegrVl,{'R', "-9"})

aAdd(aVStat,{"NDA","0"}) // Nenhum
aAdd(aVStat,{"CAN","1"}) // Cancelled
aAdd(aVStat,{"CON","2"}) // Confirmed
aAdd(aVStat,{"ERR","3"}) // Order in error
aAdd(aVStat,{"FRZ","4"}) // Frozen
aAdd(aVStat,{"ICS","5"}) // Invoice complete
aAdd(aVStat,{"IPS","6"}) // Partially invoiced
aAdd(aVStat,{"RLS","7"}) // Released (i.e. inventory applied)
aAdd(aVStat,{"SHP","8"}) // Shipped
aAdd(aVStat,{"SSD","9"}) // Scheduled Ship Date set
aAdd(aVStat,{"UNC","A"}) // Unconfirmed
aAdd(aVStat,{"UNS","B"}) // Unsourced
aAdd(aVStat,{"TRF","C"}) // Transfered

/* Matriz aVQ0VQJ
	1 - Campo a ser gravado
	2 - Campo com o conteudo
	3 - Model que ser� gravada
	4 - Indicador de grava��o
		I - Inclus�o
		T - Inclus�o e Altera��o
*/

aAdd(aVQ0VQJ,{"VJR_ORDNUM","VJQ_ORDNUM","VJRMASTER","I"})

aAdd(aVQ0VQJ,{"VQ0_DATPED","VJQ_ORDDAT","VQ0MASTER","I"})

aAdd(aVQ0VQJ,{"VQ0_NUMPED","VJQ_QUOTNR","VQ0MASTER","T"})
aAdd(aVQ0VQJ,{"VQ0_MODVEI","VJQ_MODNUM","VQ0MASTER","T"})
aAdd(aVQ0VQJ,{"VQ0_CHASSI","VJQ_PRODID","VQ0MASTER","T"})
aAdd(aVQ0VQJ,{"VQ0_FILPED","VJQ_SOLDAC","VQ0MASTER","T"})
aAdd(aVQ0VQJ,{"VQ0_FILENT","VJQ_SHIPAC","VQ0MASTER","T"})

aAdd(aVQ0VQJ,{"VJR_EVENTO","VJQ_EVNTID","VJRMASTER","T"})
aAdd(aVQ0VQJ,{"VJR_STAFAB","VJQ_ORDSTA","VJRMASTER","T"})
aAdd(aVQ0VQJ,{"VJR_DATORS","VJQ_SHIPDT","VJRMASTER","T"})
aAdd(aVQ0VQJ,{"VJR_DATFDD","VJQ_FACTDT","VJRMASTER","T"})
aAdd(aVQ0VQJ,{"VJR_ORDCOD","VJQ_ORDCOD","VJRMASTER","T"})

if aDir(cLocal+"RECEIPTS_*.DAT" ,aVetNome,aVetTam,aVetData,aVetHora) == 0 .and.;
	aDir(cLocal+"OUTBOUND_*.DAT" ,aVetNome,aVetTam,aVetData,aVetHora) == 0
	Return
EndIf

aSort(aVetNome,,,{ |x,y| x < y } )

for nX := 1 to Len(aVetNome)

	cArquivo := Alltrim(cLocal+aVetNome[nX])
	cMoveArq := cLocal + ALLTRIM(STR0005 + "\ ") + aVetNome[nX] //importados

	oFile := FWFileReader():New(cArquivo)

	if (oFile:Open())

		While (oFile:hasLine())
			
			cStr := oFile:GetLine()

			cTpRegtr := Subs(cStr, 1,3) //Network Code

			If Left(cTpRegtr,1) == "Z"
				cTpMovto := Subs(cStr, 4,1) //Poll Record Type
				cGpOrder := Subs(cStr, 5,2) //Order Group code
				cOrderNr := Subs(cStr, 7,6) //Order Number code

				Do Case
					Case cTpMovto == "1" // 1-Header Record
						VA1400052_HeaderRecord(cStr,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)
					Case cTpMovto == "2" // 2-Detail
						VA1400053_Detail(cStr,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)
					Case cTpMovto == "3" // 3-Header Extension Record
						VA1400054_HeaderExtensionRecord(cStr,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)
					Case cTpMovto == "4" // 4-Extension Part 1
						VA1400055_ExtensionPart1(cStr,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)
					Case cTpMovto == "5" // 5-Detail Extension Part 2
						VA1400056_DetailExtensionPart2(cStr,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)
					Case cTpMovto == "6" // 6-Detail Extension Part 3
						VA1400057_DetailExtensionPart3(cStr,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)
				EndCase
			End
		End

		oFile:Close()

		aPedPro := {}

		VA140005O_GravacaoImportacao(@aPedPro)
		VA140005N_GravacaoPedidoVQ0(aPedPro)

		Copy File &(cArquivo) to &(cMoveArq)
		Dele File &(cArquivo)

		VA1400059_LimpaVetor(aVetGrv)
		aVetGrv := {}

	End

Next

Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA1400052_HeaderRecord(cLinha,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)

Local cNrOrder := ""
Local cTpLinha := ""
Local cNrLinha := ""
Local cShipAcc := ""
Local cDtCriac := ""
Local cDtInvoi := ""
Local cNumbAcc := ""
Local cCodFato := ""
Local cCodIden := ""
Local cChkLeit := ""
Local cOrigEnt := ""
Local cVlMaqDl := ""
Local cVlMaqLt := ""
Local cCodProd := ""
Local cNrSerie := ""
Local cSerTran := ""
Local cSerCab  := ""
Local cDataFDD := ""
Local cFiller  := ""
Local cQtdaOri := ""
Local cIndShip := ""
Local cStatOrd := ""
Local cIndCons := ""
Local cCdManut := ""
Local cFiller2 := ""

Default cLinha   := ""
Default cTpRegtr := ""
Default cTpMovto := ""
Default cGpOrder := ""
Default cOrderNr := ""

cNrOrder := Subs(cLinha, 13, 2) //Sequence Number
cTpLinha := Subs(cLinha, 15, 1) //Poll Activity Type
cNrLinha := Subs(cLinha, 16, 2) //Poll Record Sequence
cShipAcc := Subs(cLinha, 18, 6) //Ship To Account
cDtCriac := Subs(cLinha, 24, 8) //Order Date
cDtInvoi := Subs(cLinha, 32, 8) //Invoice Date
cNumbAcc := Subs(cLinha, 40, 6) //Sold to Account
cCodFato := Subs(cLinha, 46, 2) //Source of Supply Unit
cCodIden := Subs(cLinha, 48, 2) //Source of Supply Location
cChkLeit := Subs(cLinha, 50, 3) //Check Letter
cOrigEnt := Subs(cLinha, 53, 1) //Method of Order Entry
cVlMaqDl := Subs(cLinha, 54, 9) //Dealer Order Cost
cVlMaqLt := Subs(cLinha, 63, 9) //Dealer Order List

If cTpLinha $ "IT" //Dealer Invoice - Dealer Transfer
	cCodProd := Subs(cLinha, 72,13) //Product ID Number
	cNrSerie := Subs(cLinha, 85,13) //Engine Serial Number
	cSerTran := Subs(cLinha, 98,13) //Transmission Serial Number
	cSerCab  := Subs(cLinha,111,13) //Cab Serial Number
Else
	cDataFDD := Subs(cLinha, 72, 8) //Date FDD
	cFiller  := Subs(cLinha, 80,44) //Filler
End

cQtdaOri := Subs(cLinha,124, 2) //Original Order Quantity
cIndShip := Subs(cLinha,126, 1) //Early Ship Indicator
cStatOrd := Subs(cLinha,127, 3) //Order Status

If cTpLinha $ "IT"
	cIndCons := Subs(cLinha,130, 1) //Consigned Indicator
	cCdManut := Subs(cLinha,131, 3) //WMC
Else
	cFiller2 := Subs(cLinha,130, 3) //Filler
EndIf

aAdd(aVetGrv,{;
				{"VJQ_NTWCOD",cTpRegtr},;
				{"VJQ_RCRDTP",cTpMovto},;
				{"VJQ_ORDGRP",cGpOrder},;
				{"VJQ_ORDNUM",Val(cOrderNr)},;
				{"VJQ_SEQNUM",cNrOrder},;
				{"VJQ_ACTVTP",cTpLinha},;
				{"VJQ_RCRDSQ",cNrLinha},;
				{"VJQ_SHIPAC",cShipAcc},;
				{"VJQ_ORDDAT",StoD(cDtCriac)},;
				{"VJQ_INVDAT",StoD(cDtInvoi)},;
				{"VJQ_SOLDAC",cNumbAcc},;
				{"VJQ_SUPUNT",cCodFato},;
				{"VJQ_SUPLOC",cCodIden},;
				{"VJQ_CHKLET",cChkLeit},;
				{"VJQ_ORDENT",cOrigEnt},;
				{"VJQ_ORDCOS",VA1400058_ImplicitTrailingSigns(cVlMaqDl,"VJQ_ORDCOS")},;
				{"VJQ_ORDLIS",VA1400058_ImplicitTrailingSigns(cVlMaqLt,"VJQ_ORDLIS")},;
				{"VJQ_PRODID",cCodProd},;
				{"VJQ_ENGSER",cNrSerie},;
				{"VJQ_TRMSER",cSerTran},;
				{"VJQ_CABSER",cSerCab},;
				{"VJQ_DATFDD",StoD(cDataFDD)},;
				{"VJQ_ORDQNT",Val(cQtdaOri)},;
				{"VJQ_SHIPIN",cIndShip},;
				{"VJQ_ORDSTA",cStatOrd},;
				{"VJQ_CNSGIN",cIndCons},;
				{"VJQ_WMC"   ,cCdManut};
			})

Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA1400053_Detail(cLinha,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)

Local cNrOrder := ""
Local cTpLinha := ""
Local cNrLinha := ""
Local cShipAcc := ""
Local cCodMaq  := ""
Local cTpGrMaq := ""
Local cCdModel := ""
Local cSufxMod := ""
Local cDtlhMaq := ""
Local cVlMaqDl := ""
Local cVlMaqLt := ""
Local cQtdaOri := ""
Local cLngDtlh := ""
Local cTpCdPCI := ""
Local cDtaEftv := ""
Local cFiller  := ""
Local cCGDeal  := ""
Local cFiller2 := ""

Default cLinha   := ""
Default cTpRegtr := ""
Default cTpMovto := ""
Default cGpOrder := ""
Default cOrderNr := ""

cNrOrder := Subs(cLinha, 13, 2) //Sequence Number
cTpLinha := Subs(cLinha, 15, 1) //Poll Activity Type
cNrLinha := Subs(cLinha, 16, 2) //Poll Record Sequence
cShipAcc := Subs(cLinha, 18, 6) //Ship To Account
cCodMaq  := Subs(cLinha, 24,12) //Order Code
cTpGrMaq := Subs(cLinha, 36, 1) //Order Code Type
cCdModel := Subs(cLinha, 37, 4) //Model Number
cSufxMod := Subs(cLinha, 41, 1) //Model Suffix
cDtlhMaq := Subs(cLinha, 42,12) //Alternate Attachment Code or Detail Machine Code
cVlMaqDl := Subs(cLinha, 54, 9) //Dealer Order Cost
cVlMaqLt := Subs(cLinha, 63, 9) //Dealer Order List
cQtdaOri := Subs(cLinha, 72, 3) //Attachment Quantity
cLngDtlh := Subs(cLinha, 75,29) //Order Code Description
cTpCdPCI := Subs(cLinha,104,10) //PCI Type Code
cDtaEftv := Subs(cLinha,114, 8) //Price Effective date
cFiller  := Subs(cLinha,122, 2) //Filler
cCGDeal  := Subs(cLinha,124, 3) //CG Dealer Code
cFiller2 := Subs(cLinha,127, 7) //Filler

aAdd(aVetGrv,{;
				{"VJQ_NTWCOD",cTpRegtr},;
				{"VJQ_RCRDTP",cTpMovto},;
				{"VJQ_ORDGRP",cGpOrder},;
				{"VJQ_ORDNUM",Val(cOrderNr)},;
				{"VJQ_SEQNUM",cNrOrder},;
				{"VJQ_ACTVTP",cTpLinha},;
				{"VJQ_RCRDSQ",cNrLinha},;
				{"VJQ_SHIPAC",cShipAcc},;
				{"VJQ_ORDCOD",cCodMaq },;
				{"VJQ_ORDTP" ,cTpGrMaq},;
				{"VJQ_MODNUM",cCdModel},;
				{"VJQ_MODSUF",cSufxMod},;
				{"VJQ_PCITPE",cDtlhMaq},;
				{"VJQ_ORDCOS",VA1400058_ImplicitTrailingSigns(cVlMaqDl,"VJQ_ORDCOS")},;
				{"VJQ_ORDLIS",VA1400058_ImplicitTrailingSigns(cVlMaqLt,"VJQ_ORDLIS")},;
				{"VJQ_ATTQNT",VA1400058_ImplicitTrailingSigns(cQtdaOri,"VJQ_ATTQNT")},;
				{"VJQ_ORDDES",cLngDtlh},;
				{"VJQ_PCITPE",cTpCdPCI},;
				{"VJQ_EFFDAT",StoD(cDtaEftv)},;
				{"VJQ_CGCODE",cCGDeal};
			})

Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA1400054_HeaderExtensionRecord(cLinha,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)

Default cLinha   := ""
Default cTpRegtr := ""
Default cTpMovto := ""
Default cGpOrder := ""
Default cOrderNr := ""

cNrOrder := Subs(cLinha, 13, 2) //Sequence Number
cTpLinha := Subs(cLinha, 15, 1) //Poll Activity Type
cNrLinha := Subs(cLinha, 16, 2) //Poll Record Sequence
cShipAcc := Subs(cLinha, 18, 6) //Ship To Account
cCodMaq  := Subs(cLinha, 24, 1) //Type Order
cTpGrMaq := Subs(cLinha, 25,15) //Customer Order Number
cNumbAcc := Subs(cLinha, 40, 6) //Sold to Account
cModAprt := Subs(cLinha, 46,40) //Decal Model
cChDecal := Subs(cLinha, 86,10) //Decal Unique Identifier
cMakeMaq := Subs(cLinha, 96,25) //Make
cChvMake := Subs(cLinha,121,10) //Make Unique Identifier
cFiller  := Subs(cLinha,131, 3) //Filler

aAdd(aVetGrv,{;
				{"VJQ_NTWCOD",cTpRegtr},;
				{"VJQ_RCRDTP",cTpMovto},;
				{"VJQ_ORDGRP",cGpOrder},;
				{"VJQ_ORDNUM",Val(cOrderNr)},;
				{"VJQ_SEQNUM",cNrOrder},;
				{"VJQ_ACTVTP",cTpLinha},;
				{"VJQ_RCRDSQ",cNrLinha},;
				{"VJQ_SHIPAC",cShipAcc},;
				{"VJQ_ORDCOD",cCodMaq},;
				{"VJQ_ORDTP" ,cTpGrMaq},;
				{"VJQ_SOLDAC",cNumbAcc},;
				{"VJQ_DECMOD",cModAprt},;
				{"VJQ_DECUID",Val(cChDecal)},;
				{"VJQ_MAKE"  ,cMakeMaq},;
				{"VJQ_MAKEID",Val(cChvMake)};
			})
Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA1400055_ExtensionPart1(cLinha,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)

Default cLinha   := ""
Default cTpRegtr := ""
Default cTpMovto := ""
Default cGpOrder := ""
Default cOrderNr := ""

cNrOuote := Subs(cLinha, 13,10) //Quote Number
cStOuote := Subs(cLinha, 23,50) //Quoting Status Code
cEventID := Subs(cLinha, 73,20) //Special Event ID
cDataCCG := Subs(cLinha, 93, 7) //CCG Order Warehouse date
cDtFator := Subs(cLinha,100, 7) //Orig Factory Del date
cDataReq := Subs(cLinha,107, 7) //Req Del Date
cDReqShp := Subs(cLinha,114, 8) //ORIG REQ SHIP DATE
cDOrgEnt := Subs(cLinha,122, 8) //ORIGINAL ORDER ENTRY DATE

aAdd(aVetGrv,{;
				{"VJQ_NTWCOD",cTpRegtr},;
				{"VJQ_RCRDTP",cTpMovto},;
				{"VJQ_ORDGRP",cGpOrder},;
				{"VJQ_ORDNUM",Val(cOrderNr)},;
				{"VJQ_QUOTNR",cNrOuote},;
				{"VJQ_QUOTST",cStOuote},;
				{"VJQ_EVNTID",cEventID},;
				{"VJQ_CCGDAT",VA140005A_LevantaData(cDataCCG)},;
				{"VJQ_FACTDT",VA140005A_LevantaData(cDtFator)},;
				{"VJQ_RQDLDT",VA140005A_LevantaData(cDataReq)},;
				{"VJQ_SHIPDT",StoD(cDReqShp)},;
				{"VJQ_ENTDAT",StoD(cDOrgEnt)};
			})
Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA1400056_DetailExtensionPart2(cLinha,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)

Default cLinha   := ""
Default cTpRegtr := ""
Default cTpMovto := ""
Default cGpOrder := ""
Default cOrderNr := ""

cDescPrt := Subs(cLinha,13,121) //Product

aAdd(aVetGrv,{;
				{"VJQ_NTWCOD",cTpRegtr},;
				{"VJQ_RCRDTP",cTpMovto},;
				{"VJQ_ORDGRP",cGpOrder},;
				{"VJQ_ORDNUM",Val(cOrderNr)},;
				{"VJQ_PRODUC",cDescPrt};
			})

Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA1400057_DetailExtensionPart3(cLinha,cTpRegtr,cTpMovto,cGpOrder,cOrderNr)

Default cLinha   := ""
Default cTpRegtr := ""
Default cTpMovto := ""
Default cGpOrder := ""
Default cOrderNr := ""

cNomeCCG := Subs(cLinha, 13,28) //CCG_ship_to_name
cDtDeliv := Subs(cLinha, 41, 8) //Delivery Date
cDtRepar := Subs(cLinha, 49, 8) //Retail sold Date
cOrdType := Subs(cLinha, 57,40) //ORD_TYP
cVlTotOr := Subs(cLinha, 97, 9) //Order_Tot_Prc
cVlFrete := Subs(cLinha,106, 9) //Order_FRT
cVlComis := Subs(cLinha,115, 9) //Order_Comsn
cVlTotTx := Subs(cLinha,124, 9) //Order_tot_tax

aAdd(aVetGrv,{;
				{"VJQ_NTWCOD",cTpRegtr},;
				{"VJQ_RCRDTP",cTpMovto},;
				{"VJQ_ORDGRP",cGpOrder},;
				{"VJQ_ORDNUM",Val(cOrderNr)},;
				{"VJQ_CCGNAM",cNomeCCG},;
				{"VJQ_DLVDAT",VA140005A_LevantaData(cDtDeliv)},;
				{"VJQ_RTLDAT",VA140005A_LevantaData(cDtRepar)},;
				{"VJQ_ORDTYP",cOrdType},;
				{"VJQ_ORDTOT",Val(cVlTotOr)/100},;
				{"VJQ_ORDFRT",Val(cVlFrete)/100},;
				{"VJQ_ORDCOM",Val(cVlComis)/100},;
				{"VJQ_ORDTAX",Val(cVlTotTx)/100};
	})

Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA1400058_ImplicitTrailingSigns(cValor,cCpo)

Local cVlrRet := cValor

nPosRegra := aScan(aRegrVl,{|x| x[1] == Right(cValor,1)})
If nPosRegra > 0
	cVlrRet := Val(StrTran(cValor,Right(cValor,1),aRegrVl[nPosRegra,2]))
	If GeTSX3Cache(cCpo,"X3_DECIMAL") > 0
		cVlrRet := cVlrRet/100
	EndIf
EndIf

Return cVlrRet

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA1400059_LimpaVetor(aArray)
	aArray := aSize(aArray,0)
Return

Static Function VA140005A_LevantaData(uAuxValor)

If !Empty(uAuxValor)
	cMes := UPPER(SubStr(uAuxValor,3,3))
	Do Case
		Case cMes == "JAN" ; cMes := "01"
		Case cMes == "FEB" ; cMes := "02"
		Case cMes == "MAR" ; cMes := "03"
		Case cMes == "APR" ; cMes := "04"
		Case cMes == "MAY" ; cMes := "05"
		Case cMes == "JUN" ; cMes := "06"
		Case cMes == "JUL" ; cMes := "07"
		Case cMes == "AUG" ; cMes := "08"
		Case cMes == "SEP" ; cMes := "09"
		Case cMes == "OCT" ; cMes := "10"
		Case cMes == "NOV" ; cMes := "11"
		Otherwise ; cMes := "12"
	End Case
	uAuxValor := CtoD(SubStr(uAuxValor,1,2) + "/" + cMes + "/" + SubStr(uAuxValor,6,2))
EndIf

Return uAuxValor

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA140005B_GravacaoVQ0(cNumPed,cComCod,cImport)

Local aRetorno := {,,}
Local nRecNo  := ""

Default cNumPed := ""
Default cComCod := ""

cQuery := "SELECT VQ0.R_E_C_N_O_ VQ0RECNO, VJR.R_E_C_N_O_ VJRRECNO "
cQuery += "FROM " + RetSqlName("VQ0") + " VQ0 "
cQuery += "JOIN " + RetSqlName("VJR") + " VJR "
cQuery +=   " ON VJR.VJR_FILIAL = VQ0.VQ0_FILIAL "
cQuery +=  " AND VJR.VJR_CODVQ0 = VQ0.VQ0_CODIGO "
cQuery +=  " AND VJR.D_E_L_E_T_ = ' ' "
cQuery += "WHERE VQ0.VQ0_FILIAL = '" + xFilial("VQ0") + "' "
//cQuery +=  " AND VQ0.VQ0_NUMPED = '" + cNumPed + "' "
cQuery +=  " AND VJR.VJR_ORDNUM = '" + cComCod + "' "
cQuery +=  " AND VQ0.D_E_L_E_T_ = ' ' "

TcQuery cQuery New Alias "TMPPED"

If !Empty(TMPPED->VQ0RECNO)
	
	DbSelectArea("VQ0")
	DbGoTo(TMPPED->VQ0RECNO)
	
	oModelVQ0:SetOperation( MODEL_OPERATION_UPDATE )

	oModelVQ0:GetModel( 'VJNMASTER' ):SetNoDeleteLine( .F. )
	oModelVQ0:GetModel( 'VJNMASTER' ):SetNoUpdateLine( .F. )
	oModelVQ0:GetModel( 'VJNMASTER' ):SetNoInsertLine( .F. )
	
	DbSelectArea("VJR")
	DbGoTo(TMPPED->VJRRECNO)

	aRetorno[1] := VQ0->VQ0_CODIGO
	aRetorno[2] := "1"
	aRetorno[3] := VQ0->VQ0_CHAINT
Else
	
	oModelVQ0:SetOperation( MODEL_OPERATION_INSERT )

	oModelVQ0:GetModel( 'VJNMASTER' ):SetNoDeleteLine( .F. )
	oModelVQ0:GetModel( 'VJNMASTER' ):SetNoUpdateLine( .F. )
	oModelVQ0:GetModel( 'VJNMASTER' ):SetNoInsertLine( .F. )

	aRetorno[1] := ""
	aRetorno[2] := "0"
	aRetorno[3] := ""
EndIf

TMPPED->(DbCloseArea())

Return aRetorno

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA14000C5_StatusPedido(cStatPed)

Local cRetorno := "0"

nPosSt := aScan(aVStat,{|x| x[1] == cStatPed})
If nPosSt > 0
	cRetorno := aVStat[nPosSt,2]
EndIf

Return cRetorno

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA140005D_ComboStatusFabrica()

Local cRetorno := ""

cRetorno := STR0006 //"1=Cancelado;"
cRetorno += STR0007 //"2=Confirmado;"
cRetorno += STR0008 //"3=Pedido com erro;"
cRetorno += STR0009 //"4=Congelado;"
cRetorno += STR0010 //"5=Faturado;"
cRetorno += STR0011 //"6=Parcialmente faturado;"
cRetorno += STR0012 //"7=Liberado;"
cRetorno += STR0013 //"8=Enviado;"
cRetorno += STR0014 //"9=Programada data de envio;"
cRetorno += STR0015 //"A=N�o confirmado;"
cRetorno += STR0016 //"B=Sem valor;"
cRetorno += STR0017 //"C=Transferido;"

Return cRetorno

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA140005E_MontaChassi(cProdId)

Local cRetorno := ""

Default cProdId := ""

If !Empty(cProdId)

	//Exemplo: 1BM6150JTJD001017
	cRetorno := oModelVJQ:GetValue( "VJQMASTER", "VJQ_WMC") //1BM
	cRetorno += Subs(cProdId,3,5) //6150J
	cRetorno += oModelVJQ:GetValue( "VJQMASTER", "VJQ_CHKLET") //TJD
	cRetorno += Subs(cProdId,8,6) //001017

EndIf

Return cRetorno

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA140005F_MontaModelo(cModNum)

Local cRetorno := Space(GeTSX3Cache("VQ0_MODVEI","X3_TAMANHO"))
Local cBaseCd  := Space(GeTSX3Cache("VJQ_ORDCOD","X3_TAMANHO"))

Default cModNum := ""

If !Empty(cModNum)

	//Exemplo: 6190J
	cModFab := cModNum //6190
	cModFab += oModelVJQ:GetValue( "VJQMASTER", "VJQ_MODSUF") //J

	cBaseCd := oModelVJQ:GetValue( "VJQMASTER", "VJQ_ORDCOD") //J

	cRetorno := VA140005K_BuscaModelo( cCodMar, cModFab, cBaseCd )

EndIf

Return cRetorno

Function VA140005G_GravaMaquina(cChaInt,cModVei,cChassi,cFilEnt,oModelVei)

	Local lRet := .t.
	Local lNTemChassi := .f.

	Default cChaInt   := ""
	Default cModVei   := ""
	Default cChassi   := ""
	Default cFilEnt   := ""

	If Empty(cModVei)
		Return cChaInt
	EndIf

	lNTemChassi := Empty(cChaInt)

	If lNTemChassi
		oModelVei:SetOperation( MODEL_OPERATION_INSERT )
	Else
		VV1->(DbSetOrder(1))
		VV1->(DbSeek(xFilial("VV1")+cChaInt))
		oModelVei:SetOperation( MODEL_OPERATION_UPDATE )
	EndIf

	lRet := oModelVei:Activate()

	If lRet
		
		VV2->( DbSetOrder(1) )
		VV2->( DbSeek( xFilial("VV2") + cCodMar + cModVei ) )
		VVC->( DbSeek( xFilial("VVC") + VV2->VV2_CODMAR ) )

		oModelVei:SetValue( "MODEL_VV1", "VV1_CHASSI", cChassi )
		oModelVei:SetValue( "MODEL_VV1", "VV1_CODMAR", VV2->VV2_CODMAR )
		oModelVei:SetValue( "MODEL_VV1", "VV1_MODVEI", cModVei )
		oModelVei:SetValue( "MODEL_VV1", "VV1_FABMOD", Year2Str(dDataBase) + "/" + Year2Str(dDataBase) )
		oModelVei:SetValue( "MODEL_VV1", "VV1_CORVEI", VVC->VVC_CORVEI )
		oModelVei:SetValue( "MODEL_VV1", "VV1_FILENT", cFilEnt )

		If lNTemChassi
			oModelVei:SetValue( "MODEL_VV1", "VV1_SITVEI", "8" )
			oModelVei:SetValue( "MODEL_VV1", "VV1_ESTVEI", "0" )
		EndIf

		If ( lRet := oModelVei:VldData() )

			if ( lRet := oModelVei:CommitData())
			Else
				Help("",1,"COMMITVV1",,STR0018,1,0) //"N�o foi possivel incluir o(s) registro(s)"
			EndIf
		Else
			Help("",1,"VALIDVV1",,STR0019,1,0) //"Problema na valida��o dos campos e n�o foi possivel concluir o relacionamento"
		EndIf

		cChaInt := oModelVei:GetValue("MODEL_VV1","VV1_CHAINT")

		oModelVei:DeActivate()
	Else
		Help("",1,"ACTIVEVV1",,STR0020,1,0) //"N�o foi possivel ativar o modelo de inclus�o da tabela"
	EndIf

Return cChaInt

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA14000H5_NumeroPedido(cNumPed)

Local cRetorno := Space(GeTSX3Cache("VJR_ORDNUM","X3_TAMANHO"))

Default cNumPed := cRetorno

if !Empty(cNumPed)

	//Exemplo: 20617311
	cRetorno := cValtoChar(oModelVJQ:GetValue( "VJQMASTER", "VJQ_ORDGRP")) //20
	cRetorno += cValtoChar(cNumPed) //617311

EndIf

Return cRetorno

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA14000I5_ReferenciaFilial(cCodFil)

	Local cRetorno := Space(GeTSX3Cache("VQ0_FILENT","X3_TAMANHO"))

	Default cCodFil := cRetorno

	if !Empty(cCodFil)
		cRetorno := oFilJD:GetFiliais(cCodFil)
	EndIf

Return cRetorno

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA14000J5_Filial(cCodFil)

	Local cRetorno := Space(GeTSX3Cache("VQ0_FILIAL","X3_TAMANHO"))
	Local nPosFil  := 0

	Default cCodFil := cRetorno

	nPosFil := aScan(aMapFilDealerCode, { |x| x[2] == cCodFil })
	If nPosFil <> 0
		cRetorno := aMapFilDealerCode[nPosFil,1]
	EndIf

Return cRetorno

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA140005K_BuscaModelo( cCodMar, cModFab, cBaseCd )

	Local cModelo := ""
	Local aModVei := Array(5)

	Default cCodMar := ""
	Default cModFab := ""
	Default cBaseCd := ""

	aModVei[1] := Space(GetSX3Cache("VJU_CODMAR","X3_TAMANHO"))
	aModVei[2] := Space(GetSX3Cache("VJU_GRUMOD","X3_TAMANHO"))
	aModVei[3] := Space(GetSX3Cache("VJU_MODVEI","X3_TAMANHO"))
	aModVei[4] := Space(GetSX3Cache("VJU_SEGMOD","X3_TAMANHO"))
	aModVei[5] := .t.

	VJU->(DbSetOrder(3))
	If !(VJU->(DbSeek(xFilial("VJU")+cModFab+cBaseCd)))

		If VJU->(DbSeek(xFilial("VJU")+cModFab))
			aModVei[1] := VJU->VJU_CODMAR
			aModVei[2] := VJU->VJU_GRUMOD
			aModVei[3] := VJU->VJU_MODVEI
			aModVei[4] := VJU->VJU_SEGMOD
			aModVei[5] := .t.
		Else
			VJU->(DbSetOrder(2))
			If VJU->(DbSeek(xFilial("VJU")+cBaseCd))
				aModVei[1] := VJU->VJU_CODMAR
				aModVei[2] := VJU->VJU_GRUMOD
				aModVei[3] := VJU->VJU_MODVEI
				aModVei[4] := VJU->VJU_SEGMOD
				aModVei[5] := .f.
			EndIf
		EndIf

		VA140005L_GravaModeloFabrica(cModFab , cBaseCd, aModVei)
		VJU->(DbSetOrder(3))
		VJU->(DbSeek(xFilial("VJU")+cModFab+cBaseCd))

	EndIf

	cModelo := Alltrim(VJU->VJU_MODVEI)

Return cModelo

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA140005L_GravaModeloFabrica(cModFab , cBaseCd, aModVei)

	Local oModelVJU := FWLoadModel( 'VEIA145' )

	Default cModFab := ""
	Default cBaseCd := ""
	Default aModVei := Array(5)

	If aModVei[5]
		oModelVJU:SetOperation( MODEL_OPERATION_INSERT )
	Else
		oModelVJU:SetOperation( MODEL_OPERATION_UPDATE )
	EndIf

	lRet := oModelVJU:Activate()

	If lRet

		oModelVJU:SetValue( "VJUMASTER", "VJU_MODEID", cModFab )
		oModelVJU:SetValue( "VJUMASTER", "VJU_BASECD", cBaseCd )

		if !Empty(aModVei[3])
			oModelVJU:SetValue( "VJUMASTER", "VJU_CODMAR", aModVei[1] )
			oModelVJU:SetValue( "VJUMASTER", "VJU_GRUMOD", aModVei[2] )
			oModelVJU:SetValue( "VJUMASTER", "VJU_MODVEI", aModVei[3] )
			oModelVJU:SetValue( "VJUMASTER", "VJU_SEGMOD", aModVei[4] )
		EndIf
	
		If ( lRet := oModelVJU:VldData() )

			if ( lRet := oModelVJU:CommitData())
			Else
				Help("",1,"COMMITVJU",,STR0018,1,0)
			EndIf

		Else
			Help("",1,"VALIDVJU",,STR0019,1,0)
		EndIf

		oModelVJU:DeActivate()
	Else
		Help("",1,"ACTIVEVJU",,STR0020,1,0)
	EndIf

	FreeObj(oModelVJU)

Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA140005M_ImportaModelo(cBaseCode)

	Local lRetorno := .f.
	Local cQuery   := ""

	cQuery := "SELECT VJU.R_E_C_N_O_ "
	cQuery += " FROM " + RetSqlName("VJU") + " VJU "
	cQuery += " WHERE VJU.VJU_FILIAL = '" + xFilial("VJU") + "' "
	cQuery += 	" AND VJU.VJU_BASECD = '" + cBaseCode + "' "
	cQuery += 	" AND VJU.VJU_CONIMP <> '0' "
	cQuery += 	" AND VJU.D_E_L_E_T_ = ' '"

	lRetorno := FM_SQL(cQuery) > 0

Return lRetorno

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA140005N_GravacaoPedidoVQ0(aPedidos)

	Local nI, nJ
	Local cBkpFil   := ""

	Default aPedidos:= {}

	For ni := 1 to Len(aPedidos)

		If aPedidos[ni,3]

			cBkpFil := cFilAnt

			nPosFil := aScan(aPedidos[ni,4],{ |x| x[2] == "VQ0_FILPED"})
			If nPosFil > 0
				If !Empty(aPedidos[ni,4,nPosFil,3])
					cFilAnt := aPedidos[ni,4,nPosFil,3]
				EndIf
			EndIf

			nPosBc:= aScan(aPedidos[ni,4],{ |x| x[2] == "VJR_ORDCOD"})
			If nPosBc > 0
				cBaseCode := aPedidos[ni,4,nPosBc,3]
			EndIf

			lImpPed := .t.
			If !Empty(cBaseCode)
				lImpPed := VA140005M_ImportaModelo(cBaseCode)
			EndIf
			
			If lImpPed

				cComCod := aPedidos[ni,1]
				cCodImp := aPedidos[ni,2]

				nPosNrPd:= aScan(aPedidos[ni,4],{ |x| x[2] == "VQ0_NUMPED"})
				cNroPed := aPedidos[ni,4,nPosNrPd,3]

				aVetReg := VA140005B_GravacaoVQ0(cNroPed,cComCod,cCodImp)

				If oModelVQ0:Activate()

					nPosSta := aScan(aPedidos[ni,4],{ |x| x[2] == "VJR_STAIMP"})
					If nPosSta > 0
						aPedidos[ni,4,nPosSta,3] := aVetReg[2]
					EndIf

					For nJ := 1 to Len(aPedidos[ni,4])
						oModelVQ0:SetValue( aPedidos[ni,4,nJ,1], aPedidos[ni,4,nJ,2], aPedidos[ni,4,nJ,3] )

						If aPedidos[ni,4,nJ,2] == "VJR_ORDCOD" .and.;
							Len(aPedidos[ni,4,nJ,4]) > 0

							VA14000R5_RelacionaOpcionalPedido( oModelVQ0, aPedidos[ni,4,nJ,4])

						EndIf

					Next

					cCriaMaq := VA140005G_GravaMaquina(aVetReg[3],;
														oModelVQ0:GetValue( "VQ0MASTER", "VQ0_MODVEI"),;
														oModelVQ0:GetValue( "VQ0MASTER", "VQ0_CHASSI"),;
														oModelVQ0:GetValue( "VQ0MASTER", "VQ0_FILENT"),;
														oModelVV1)

					If !Empty(cCriaMaq)
						oModelVQ0:SetValue( "VQ0MASTER", "VQ0_CHAINT", cCriaMaq )
					EndIf

					If ( lRetVQ0 := oModelVQ0:VldData() )
						ConfirmSX8()
						if ( lRetVQ0 := oModelVQ0:CommitData())
						Else
							Help("",1,"COMMITVQ0",,STR0018,1,0)
						EndIf
					Else
						Help("",1,"VALIDVQ0",,oModelVQ0:GetErrorMessage()[6] + STR0021 + oModelVQ0:GetErrorMessage()[2],1,0) //" Campo: "
					EndIf

					oModelVQ0:DeActivate()
				Else
					Help("",1,"ACTIVEVQ0",,STR0020,1,0)
				EndIf

			EndIf

			cFilAnt := cBkpFil

		EndIf

	Next

Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA140005O_GravacaoImportacao(aPedPro)

	Local nG, nJ, nZ
	Local cUltPed   := ""
	Local cNroPed   := ""
	Local cConteudo := ""
	Local cTpCpoVQ0 := ""
	Local cTpCpoVJQ := ""
	Local cOpcional:= {}

	cCodImp := GetSXENum("VJQ","VJQ_CODIGO",,1)

	oModelVJQ:SetOperation( MODEL_OPERATION_INSERT )
	
	cUltPed := ""
	nPosPed := 0

	For nG := 1 to Len(aVetGrv)

		lRetVQJ := oModelVJQ:Activate()

		if lRetVQJ

			oModelVJQ:SetValue( "VJQMASTER", "VJQ_CODIGO", cCodImp )
			oModelVJQ:SetValue( "VJQMASTER", "VJQ_SEQUEN", StrZero(nG,TamSX3("VJQ_SEQUEN")[1]) )
			oModelVJQ:SetValue( "VJQMASTER", "VJQ_DATIMP", dDataBase )

			For nJ := 1 to Len(aVetGrv[nG])

				If !Empty(aVetGrv[nG,nJ,2])
					oModelVJQ:SetValue( "VJQMASTER", aVetGrv[nG,nJ,1], aVetGrv[nG,nJ,2] )
				EndIf

			Next

			cNroPed := oModelVJQ:GetValue( "VJQMASTER", "VJQ_QUOTNR")
			cComCod := cValtoChar(oModelVJQ:GetValue( "VJQMASTER", "VJQ_ORDGRP")) + cValtoChar(oModelVJQ:GetValue( "VJQMASTER", "VJQ_ORDNUM"))
			dDtaPed := oModelVJQ:GetValue( "VJQMASTER", "VJQ_ORDDAT")

			If cUltPed <> cComCod

				cUltPed := cComCod
//				nPosPed := aScan(aPedPro,{ |x| x[1] == cComCod })

//				If nPosPed == 0
					aAdd(aPedPro,{;
									cComCod,;
									cCodImp,;
									dDtaPed >= dDtaIni,;
									{	{ "VQ0MASTER", "VQ0_CODVJQ", cCodImp },;
										{ "VQ0MASTER", "VQ0_CODMAR", cCodMar },;
										{ "VJRMASTER", "VJR_STAIMP", "0" },;
										{ "VJRMASTER", "VJR_DATATU", dDataBase };
									};
								})
//				EndIf

				nPosPed := aScan(aPedPro,{ |x| x[1] == cComCod })

			EndIf

			If aPedPro[nPosPed,3]

				For nZ := 1 to Len(aVQ0VQJ)

					cOpcional := ""
					cConteudo := oModelVJQ:GetValue( "VJQMASTER", aVQ0VQJ[nZ,2])

					If Empty(cConteudo)
						Loop
					EndIf

					If aVQ0VQJ[nZ,1] == "VJR_ORDNUM"
						cConteudo := VA14000H5_NumeroPedido(cConteudo)
					ElseIf aVQ0VQJ[nZ,1] == "VJR_STAFAB"
						cConteudo := VA14000C5_StatusPedido(cConteudo)
					ElseIf aVQ0VQJ[nZ,1] == "VQ0_MODVEI"
						If oModelVJQ:GetValue( "VJQMASTER", "VJQ_ORDTP") == "B"
							cConteudo := VA140005F_MontaModelo(cConteudo)
						Else
							Loop
						EndIf
					ElseIf aVQ0VQJ[nZ,1] == "VJR_ORDCOD" .and.;
						oModelVJQ:GetValue( "VJQMASTER", "VJQ_ORDTP") <> "B"

						If oModelVJQ:GetValue( "VJQMASTER", "VJQ_ORDTP") == "O"
							cOpcional := VA14000Q5_GravaOpcional(Right(cConteudo,6),oModelVJQ:GetValue( "VJQMASTER", "VJQ_ORDDES"))
						Else
							Loop
						EndIf

					ElseIf aVQ0VQJ[nZ,1] == "VQ0_CHASSI"
						cConteudo := VA140005E_MontaChassi(cConteudo)
					ElseIf aVQ0VQJ[nZ,1] == "VQ0_FILPED" .or.;
							aVQ0VQJ[nZ,1] == "VQ0_FILENT"
						cConteudo := VA14000I5_ReferenciaFilial(cConteudo)
					ElseIf aVQ0VQJ[nZ,1] == "VJR_EVENTO"
						cConteudo := VA14000P5_GravaEvento(cConteudo)
					Else
						cTpCpoVQ0 := GeTSX3Cache(aVQ0VQJ[nZ,1],"X3_TIPO")
						cTpCpoVJQ := GeTSX3Cache(aVQ0VQJ[nZ,2],"X3_TIPO")

						If cTpCpoVQ0 <> cTpCpoVJQ // Tipos de campos diferentes
							If cTpCpoVQ0 == "C"
								cConteudo := cValToChar(cConteudo)
							ElseIf cTpCpoVQ0 == "N"
								cConteudo := Val(cConteudo)
							EndIf
						EndIf

					EndIf

					if !Empty(cConteudo)
						nPosCp := aScan( aPedPro[nPosPed,4], { |x| x[2] == aVQ0VQJ[nZ,1] } )
						If nPosCp == 0
							aAdd(aPedPro[nPosPed,4],{ aVQ0VQJ[nZ,3], aVQ0VQJ[nZ,1], cConteudo, If(!Empty(cOpcional), { cOpcional } , {} ) } )
						Else
							If Empty(cOpcional)
								aPedPro[nPosPed,4,nPosCp,3] := cConteudo
							Else
								aAdd( aPedPro[nPosPed,4,nPosCp,4] , cOpcional )
							EndIf
						EndIf
					EndIf

				Next

			EndIf

			If ( lRetVQJ := oModelVJQ:VldData() )
				ConfirmSX8()
				if ( lRetVQJ := oModelVJQ:CommitData())
				Else
					Help("",1,"COMMITVJQ",,STR0018,1,0)
				EndIf
			Else
				Help("",1,"VALIDVJQ",,STR0019,1,0)
			EndIf
		Else
			Help("",1,"ACTIVEVJQ",,STR0020,1,0)
		EndIf

		oModelVJQ:DeActivate()

	Next

Return


/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Function VA14000P5_GravaEvento(cDescEvent)

	Local cCodEvento := ""
	Local nSeqEvento := 0
	Local cQuery     := ""

	cQuery := "SELECT VX5.VX5_CODIGO "
	cQuery += " FROM " + RetSqlName("VX5") + " VX5 "
	cQuery += " WHERE VX5.VX5_FILIAL = '" + xFilial("VX5") + "' "
	cQuery += 	" AND VX5.VX5_CHAVE  = '051' "
	cQuery += 	" AND VX5.VX5_DESCRI = '" + Alltrim( cDescEvent ) + "' "
	cQuery += 	" AND VX5.D_E_L_E_T_ = ' '"

	cCodEvento := FM_SQL(cQuery)
	
	If Empty(cCodEvento)

		cQuery := "SELECT MAX(VX5.VX5_CODIGO) "
		cQuery += " FROM " + RetSqlName("VX5") + " VX5 "
		cQuery += " WHERE VX5.VX5_FILIAL = '" + xFilial("VX5") + "' "
		cQuery += 	" AND VX5.VX5_CHAVE  = '051' "

		cSeqEvento := Alltrim(FM_SQL(cQuery))

		If Empty(cSeqEvento)
			cSeqEvento := cValToChar(StrZero(0,GeTSX3Cache("VJR_EVENTO","X3_TAMANHO")))
		EndIf

		cSeqEvento := Soma1(cSeqEvento,Len(cSeqEvento))

		OFIOA560ADD("051", cSeqEvento, cDescEvent)
		If OFIOA560VL("051", cSeqEvento,,.f.)
			cCodEvento := cSeqEvento
		EndIf
	EndIf

Return cCodEvento


/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA14000Q5_GravaOpcional(cOpcional,cDescOpcional)

	Local oModelVJV := FWLoadModel( 'VEIA146' )
	Local cCodOpcional := ""
	
	VJV->(DbSetOrder(2))
	If !VJV->( DbSeek( xFilial("VJV") + cOpcional ) )
		oModelVJV:SetOperation( MODEL_OPERATION_INSERT )
	Else
		Return VJV->VJV_CODOPC
	EndIf

	lRet := oModelVJV:Activate()

	If lRet

		oModelVJV:SetValue( "VJVMASTER", "VJV_CODOPC", Left(cOpcional,GetSX3Cache("VJV_CODOPC","X3_TAMANHO")) )
		oModelVJV:SetValue( "VJVMASTER", "VJV_DESOPC", cDescOpcional )
		oModelVJV:SetValue( "VJVMASTER", "VJV_TIPGER", '0' )

		If ( lRet := oModelVJV:VldData() )

			if ( lRet := oModelVJV:CommitData())
			Else
				Help("",1,"COMMITVJV",,STR0018,1,0)
			EndIf

		Else
			Help("",1,"VALIDVJV",,STR0019,1,0)
		EndIf

		cCodOpcional := oModelVJV:GetValue("VJVMASTER","VJV_CODOPC")

		oModelVJV:DeActivate()
	Else
		Help("",1,"ACTIVEVJV",,STR0020,1,0)
	EndIf

	FreeObj(oModelVJV)
	
Return cCodOpcional

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function VA14000R5_RelacionaOpcionalPedido( oModelPed, aOpcionais )

	Local nK		:= 0

	oModelDet := oModelPed:GetModel("VJNMASTER")

	For nK := 1 to Len(aOpcionais)

		lSeek := oModelDet:SeekLine({;
										{ "VJN_FILIAL" , xFilial("VJN") },;
										{ "VJN_CODVQ0" , oModelPed:GetValue( "VQ0MASTER", "VQ0_CODIGO") },;
										{ "VJN_CODVJV" , aOpcionais[nK] };
									})

		If	!(lSeek) //.and. oModelPed:GetOperation() == MODEL_OPERATION_INSERT
			If oModelDet:Length() == 1 .and. Empty(oModelDet:GetValue("VJN_CODVQ0"))
			Else
				oModelDet:AddLine()
			EndIf

			oModelDet:SetValue( "VJN_CODVQ0", oModelPed:GetValue( "VQ0MASTER", "VQ0_CODIGO") )
			oModelDet:SetValue( "VJN_CODVJV", aOpcionais[nK] )
		EndIf

	Next

Return

/*/
{Protheus.doc} VEIA140

@author Renato Vinicius
@since 15/04/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/

Static Function CriaSX1(cPerg)

	Local aRegs := {}
	Local nOpcGetFil := GETF_RETDIRECTORY

	AADD(aRegs,{"Local do Arquivo"  ,"Local do Arquivo"  ,"Local do Arquivo"  ,"MV_CH1","C",99,0,0,"G","!Vazio().or.(MV_PAR01:=cGetFile('Diretorio','',,,,"+AllTrim(Str(nOpcGetFil))+"))","MV_PAR01","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",{"Informe o local do arquivo"},{},{}}) // Diret�rio
	aAdd(aRegs,{"Data Inicio CGPoll","Data Inicio CGPoll","Data Inicio CGPoll","MV_CH2","D", 8,0,0,"G",""                                                                                ,"MV_PAR02","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",{"Informe a data inicial que o CGPoll ser� implementado. Assim, todos os pedidos anteriores a esta data n�o ser�o criados/atualizados ao importar o arquivo."},{},{}})

	FMX_AJSX1(cPerg,aRegs)

Return