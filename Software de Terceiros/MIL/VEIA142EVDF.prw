#include 'TOTVS.ch'
#Include "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'
#include "FWEVENTVIEWCONSTS.CH"
#INCLUDE 'VEIA142.CH'

CLASS VEIA142EVDF FROM FWModelEvent

	Data aCpoAlt

	METHOD New() CONSTRUCTOR
	METHOD FieldPreVld()
	METHOD InTTS()

ENDCLASS


METHOD New() CLASS VEIA142EVDF

	::aCpoAlt   := {}

RETURN .T.


METHOD FieldPreVld(oModel, cModelID, cAction, cId, xValue) CLASS VEIA142EVDF

	Local nX      := 0
	Local nSeq    := 0
	Local nCodReq := 0
	Local nPosCpo := 0
	
	If cAction == "SETVALUE" .and. oModel:GetOperation() == 4
		if oModel:GetValue(cId) <> xValue

			cContAnt := oModel:GetValue(cId)
			cContNov := xValue

			cTpCpo := GeTSX3Cache(cId,"X3_TIPO")

			If cTpCpo == "N"
				cContAnt := cValToChar(cContAnt)
				cContNov := cValToChar(cContNov)
			ElseIf cTpCpo == "D"
				cContAnt := DtoC(cContAnt)
				cContNov := DtoC(cContNov)
			EndIf
			
			nPosCpo := aScan(self:aCpoAlt,{|x| x[1] == cId})
			If nPosCpo == 0
				aAdd(self:aCpoAlt,{cId,cContAnt,cContNov})
			Else
				self:aCpoAlt[nPosCpo,3] := cContNov
			EndIf

		EndIf
	EndIf

RETURN .t.

METHOD InTTS(oModel, cModelId) CLASS VEIA142EVDF

	Local nPos := 0
	Local oVJS := FWLoadModel( 'VEIA143' )

	For nPos := 1 to Len(self:aCpoAlt)

		If self:aCpoAlt[nPos,2] <> self:aCpoAlt[nPos,3]

			oVJS:SetOperation( MODEL_OPERATION_INSERT )
			lRet := oVJS:Activate()

			if lRet

				oVJS:SetValue( "VJSMASTER", "VJS_CODVQ0", oModel:GetValue("VQ0MASTER","VQ0_CODIGO") )
				oVJS:SetValue( "VJSMASTER", "VJS_DATALT", dDataBase )
				oVJS:SetValue( "VJSMASTER", "VJS_CPOALT", self:aCpoAlt[nPos,1] )
				oVJS:SetValue( "VJSMASTER", "VJS_CONANT", self:aCpoAlt[nPos,2] )
				oVJS:SetValue( "VJSMASTER", "VJS_CONNOV", self:aCpoAlt[nPos,3] )

				If ( lRet := oVJS:VldData() )
					if ( lRet := oVJS:CommitData())
					Else
						Help("",1,"COMMITVJS",,oVJS:GetErrorMessage()[6],1,0)
					EndIf
				Else
					Help("",1,"VALIDVJS",,oVJS:GetErrorMessage()[6] + STR0041 + oVJS:GetErrorMessage()[2],1,0) //"Campo: "
				EndIf
				
				oVJS:DeActivate()

			Else
				Help("",1,"ACTIVEVJS",, STR0029 ,1,0) //"N�o foi possivel ativar o modelo de inclus�o da tabela"
			EndIf

		EndIf

	Next

	self:aCpoAlt := aSize(self:aCpoAlt,0)

	FreeObj(oVJS)

RETURN .t.