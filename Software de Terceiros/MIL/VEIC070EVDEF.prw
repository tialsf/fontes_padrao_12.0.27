#include 'TOTVS.ch'
#include 'FWMVCDef.ch'
#include "FWEVENTVIEWCONSTS.CH"

CLASS VEIC070EVDEF FROM FWModelEvent

	METHOD New() CONSTRUCTOR
	METHOD GridLinePreVld()

ENDCLASS

METHOD New() CLASS VEIC070EVDEF
RETURN .T.

METHOD GridLinePreVld(oSubModel, cModelID, nLine, cAction, cId, xValue, xCurrentValue) CLASS VEIC070EVDEF

	Local cSQL

	If cAction <> "SETVALUE"
		Return .t.
	EndIf

	Do Case
	Case cModelID == "LISTA_MARCA"
		If cId == "SELMARCA"
			cSQL := ;
				"UPDATE " + oTResMarca:GetRealName() + ;
				" SET MARCADO = " + IIf( xValue , "1" , "0" ) +;
				" WHERE VV1_CODMAR = '" + oSubModel:GetValue("MARCACODMAR") + "'"
			oTResMarca:ExecSQL(cSQL )
		EndIf

	Case cModelID == "LISTA_MODELO"
		If cId == "SELMODELO"
			cSQL := ;
				"UPDATE " + oTResModelo:GetRealName() + ;
				" SET MARCADO = " + IIf( xValue , "1" , "0" ) +;
				" WHERE VV1_MODVEI = '" + oSubModel:GetValue("MODMODVEI") + "'"
			oTResModelo:ExecSQL(cSQL )
		EndIf

	Case cModelID == "LISTA_OPCIONAL"
		If cId == "SELOPCIONAL"
			cSQL := ;
				"UPDATE " + oTResOpcional:GetRealName() + ;
				" SET MARCADO = " + IIf( xValue , "1" , "0" ) +;
				" WHERE VV2_OPCION = '" + oSubModel:GetValue("MODOPCION") + "'"
			oTResOpcional:ExecSQL(cSQL )
		EndIf

	Case cModelID == "LISTA_COREXTERNA"
		If cId == "SELCOREXT"
			cSQL := ;
				"UPDATE " + oTResCorExt:GetRealName() + ;
				" SET MARCADO = " + IIf( xValue , "1" , "0" ) +;
				" WHERE VV2_COREXT = '" + oSubModel:GetValue("MODCOREXT") + "'"
			oTResCorExt:ExecSQL(cSQL )
		EndIf

	Case cModelID == "LISTA_CORINTERNA"
		If cId == "SELCORINT"
			cSQL := ;
				"UPDATE " + oTResCorInt:GetRealName() + ;
				" SET MARCADO = " + IIf( xValue , "1" , "0" ) +;
				" WHERE VV2_CORINT = '" + oSubModel:GetValue("MODCORINT") + "'"
			oTResCorInt:ExecSQL(cSQL )
		EndIf

	Case cModelID == "LISTA_ANOFABMOD"
		If cId == "SELFABMOD"
			cSQL := ;
				"UPDATE " + oTResAnoFabMod:GetRealName() + ;
				" SET MARCADO = " + IIf( xValue , "1" , "0" ) +;
				" WHERE VV1_FABMOD = '" + oSubModel:GetValue("MODFABMOD") + "'"
			oTResAnoFabMod:ExecSQL(cSQL )
		EndIf

	Case cModelID == "LISTA_SITUACAO"
		If cId == "SELSITUAC"
			cSQL := ;
				"UPDATE " + oTResSituacao:GetRealName() + ;
				" SET MARCADO = " + IIf( xValue , "1" , "0" ) +;
				" WHERE SITUACAO = '" + oSubModel:GetValue("SITUACAO") + "'"
			oTResSituacao:ExecSQL(cSQL )
		EndIf

	Case cModelID == "LISTA_SITVEI"
		If cId == "SELSITVEI"
			cSQL := ;
				"UPDATE " + oTResSitVei:GetRealName() + ;
				" SET MARCADO = " + IIf( xValue , "1" , "0" ) +;
				" WHERE VV1_SITVEI = '" + oSubModel:GetValue("MODSITVEI") + "'"
			oTResSitVei:ExecSQL(cSQL )
		EndIf

	Case cModelID == "LISTA_EVENTO"
		If cId == "SELEVENTO"
			cSQL := ;
				"UPDATE " + oTResEvento:GetRealName() + ;
				" SET MARCADO = " + IIf( xValue , "1" , "0" ) +;
				" WHERE VJR_EVENTO = '" + oSubModel:GetValue("MODEVENTO") + "'"
			oTResEvento:ExecSQL(cSQL )
		EndIf

	EndCase

RETURN .T.
