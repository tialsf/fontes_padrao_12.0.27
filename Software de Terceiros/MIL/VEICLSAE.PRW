////////////////
// Versao 004 //
////////////////


#include "protheus.ch"
#include "ap5mail.ch"

/*/{Protheus.doc} mil_ver
Versao do fonte modelo novo
@author Andre Luis Almeida
@since 05/12/2017
@version undefined

@type function
/*/
Static Function mil_ver()
	If .F.
		mil_ver()
	EndIf
Return "007480_1"

Function VEICLSAE()
Return()

/*/{Protheus.doc} DMS_EstoqueConfig
	Classe criada para encapsular dados de configuracoes do estoque, referentes a parametros ou n�o
	
	@author Vinicius Gati
	@since  30/07/2014
/*/
Class DMS_EstoqueConfig
	Method New() Constructor
	Method getEstoqueReserva()
EndClass

/*/{Protheus.doc} New
	Construtor simples DMS_EstoqueConfig
	
	@author Vinicius Gati
	@since  30/07/2014
/*/
Method New() Class DMS_EstoqueConfig
Return Self

/*/{Protheus.doc} EstoqueReserva
	Retorna o codigo do estoque de reserva

	@author Vinicius Gati
	@since  30/07/2014
/*/
Method getEstoqueReserva() Class DMS_EstoqueConfig
Return GetNewPar("MV_RESITE", "")


/*/{Protheus.doc} DMS_EmailHelper
	Classe criada facilitar envio de emails
	
	@author Vinicius Gati
	@since  30/07/2014
/*/
CLASS DMS_EmailHelper
	DATA nSecMax
	DATA cTimeMax

	Method New() CONSTRUCTOR
	Method Send()
	Method SendTemplate()
EndClass

/*/{Protheus.doc} New
	Inicia dados base do email

	@author Vinicius Gati
	@since  07/08/2014

/*/
Method New() Class DMS_EmailHelper
Return SELF

/*/{Protheus.doc} Send
	Atualiza o grupo do produto e propaga a atualiza��o por todos os modulos de concession�ria

	@author Vinicius Gati
	@since  21/05/2014
/*/
Method Send(aData) Class DMS_EmailHelper
	Local nIdx     := 1
	Local lEnviado := lConectado := .F.
	Local cError   := "nenhum"
	//
	Local cMailConta := GetNewPar("MV_EMCONTA", 'dpm_info@itmil.com.br') // Usuario/e-mail de envio
	Local cMailSenha := GetNewPar("MV_EMSENHA", "DPM_qwe123!@#") // Senha e-mail de envio
	Local cMailServer:= GetNewPar("MV_RELSERV", 'smtp.itmil.com.br:587') // Server de envio
	Local lAutentica := GetMv("MV_RELAUTH",,.t.)          // Determina se o Servidor de E-mail necessita de Autenticacao
	Local cUserAut   := Alltrim(GetNewPar("MV_RELAUSR",'' )) // Usuario para Autenticacao no Servidor de E-mail
	Local cPassAut   := Alltrim(GetNewPar("MV_RELAPSW",'' )) // Senha para Autenticacao no Servidor de E-mail
	
	oData    := DMS_DataContainer():New(aData)
	cSubject := oData:GetValue('assunto'  , '')
	cBody    := oData:GetValue('mensagem' , '')
	cOrigem  := oData:GetValue('origem'   , '')
	cDestino := oData:GetValue('destino'  , '')
	cArquivo := oData:GetValue('arquivo'  , '')
	
	if EMPTY(cMailConta)
		cMailConta := 'dpm_info@itmil.com.br'
		if EMPTY(cUserAut)
			cUserAut := cMailConta
		EndIf
	EndIf
	If EMPTY(cMailSenha)
		cMailSenha := "DPM_qwe123!@#"
		if EMPTY(cPassAut)
			cPassAut := cMailSenha
		EndIf
	EndIf
	if EMPTY(cMailServer)
		cMailServer := 'smtp.itmil.com.br:587'
	EndIf

	If EMPTY(cOrigem)
		cOrigem  := cMailConta
	EndIf
	If EMPTY(cDestino)
		cDestino := cMailConta
	EndIf

	self:cTimeMax := ( Val(Substr(Time(),1,2))*60*60 ) + ( Val(Substr(Time(),4,2))*60 ) + Val(Substr(Time(),7,2)) + 60 // um minuto pra enviar o email

	if ! EMPTY(cMailServer)
		CONNECT SMTP SERVER cMailServer ACCOUNT cMailConta PASSWORD cMailSenha RESULT lConectado
		If lAutentica
			If ! MailAuth(cUserAut,cPassAut)
				DISCONNECT SMTP SERVER
				conout("VEICLSAE: Falha de conex�o com servidor de email. cMailConta:" + cMailConta + ' cMailSenha:' + cMailSenha + ' cUserAut: ' + cUserAut + ' cPassAut:' + cPassAut)
				return .F.
			EndIf
		EndIf
	Else
		conout("VEICLSAE: Falha de conex�o com servidor de email. cMailConta:" + cMailConta + ' cMailSenha:' + cMailSenha + ' cUserAut: ' + cUserAut + ' cPassAut:' + cPassAut)
		conout("VEICLSAE: Par�metros de envio de email n�o configurados, favor configurar: MV_EMCONTA MV_RELSERV MV_EMSENHA MV_RELAUTH MV_RELAUSR MV_RELAPSW.")
		return .F.
	EndIf

	if lConectado
		For nIdx := 1 To 15 // 15 � o numero de tentativas maximo
			If EMPTY(cArquivo)
				SEND MAIL FROM cOrigem TO cDestino SUBJECT cSubject BODY cBody                     FORMAT TEXT RESULT lEnviado
			Else
				SEND MAIL FROM cOrigem TO cDestino SUBJECT cSubject BODY cBody ATTACHMENT cArquivo FORMAT TEXT RESULT lEnviado
			EndIf

			If lEnviado .Or. self:cTimeMax <= ( Val(Substr(Time(),1,2))*60*60 ) + ( Val(Substr(Time(),4,2))*60 ) + Val(Substr(Time(),7,2))
				EXIT
			Else
				GET MAIL ERROR cError
				conout("VEICLSAE: Falha de envio de email: " + cError)
			EndIf
		Next
	Else
		GET MAIL ERROR cError
		conout("VEICLSAE: Falha de envio de email: " + cError)
	EndIf

	DISCONNECT SMTP SERVER
Return lEnviado

/*/{Protheus.doc} SendTemplate
	Metodo que enviar� email de acordo com um template que existir� no nosso servidor itmil.com.br
	V�rios poder�o ser criados e utilizados a vontade por aqui, eles est�o na  pasta /email/ no servidor

	no template, onde existir uma tag nesse exemplo: {{:teste}}, ser� colocado no lugar o conte�do passado
	por parametro no aData com :... conforme exemplo abaixo.

	@param aData, DMS_DataContainer, contendo template, titulo, assunto, corpo array com dados, e detalhes que � um array com dados tamb�m
		sendo o primeiro elemento sempre o cabe�alho seguido dos itens.
	@example {
		{'template', 'order'},
		{'assunto', 'Cria��o de Pedido X'},
		{'origem'  , 'origem@itmil.com.br'   },;
		{'destino' , 'destino@itmil.com.br' },;
		{':titulo', 'Pedido X'},
		{':dados_template1', 'TESTE'},
		{':corpo',  {{'Item', 'quantidade'},{'LEITE', 1}, {'P�o', 9}} },
		{':detalhes',  {{'Item', 'detalhe'},{'Coca', 'Estava caro'}, {'Manteiga', 'N�o dispon�vel'}} }
	}
	@author Vinicius Gati
	@since  21/05/2014
/*/
Method SendTemplate(aData) Class DMS_EmailHelper
	Local nIdx   := 1
	Local oDados := DMS_DataContainer():New(aData)
	Local oUtil  := DMS_Util():New()
	Local cDados

	cHtmlPage := Httpget('http://www.itmil.com.br/email/'+oDados:GetValue('template')+'.html')

	for nIdx := 1 to LEN(aData)
		cSym := aData[nIdx][1]
		if left(cSym,1) == ":"
			if valtype(aData[nIdx][2]) == "A" // tabela em html
				cDados := oUtil:GerHtmlTable( aData[nIdx][2] )
			else
				cDados := aData[nIdx][2]
			end
			cHtmlPage := STRTRAN(cHtmlPage, "{{"+cSym+"}}", cDados)
		end
	next

	lEnv := self:Send({;
		{'assunto' , oDados:GetValue('assunto') },;
		{'mensagem', EncodeUtf8(cHtmlPage)      },;
		{'origem'  , oDados:GetValue('origem')  },;
		{'destino' , oDados:GetValue('destino') } ;
	})
Return lEnv

/*/{Protheus.doc} DMS_EstoqueConfig
	Classe criada para encapsular dados de configuracoes do estoque, referentes a parametros ou n�o
	
	@author Vinicius Gati
	@since  30/07/2014
/*/
Class DMS_Estoque
	Method New() Constructor
	Method Transfere()
	Method TransfereLote()
EndClass

/*/{Protheus.doc} New
	Construtor simples DMS_EstoqueConfig
	
	@author Vinicius Gati
	@since  30/07/2014
/*/
Method New() Class DMS_Estoque
Return Self

/*/{Protheus.doc} Transfere
	Transfere item de um armazem a outro

	Pontos de entrada:

	@author Vinicius Gati
	@since  18/10/2016
/*/
Method Transfere(cB1_COD, cArmFrom, cArmTo, nQtd) Class DMS_Estoque
	Local aItensNew := {}
	Local oPeca     := DMS_Peca():New()
	Local nTamAEstq := 0
	Local l261IntWMS := a261IntWMS()

	///////////////////////////////////////////////////
	// Posicoes no Vetor de Integracao com o MATA261 //
	///////////////////////////////////////////////////
	nTamAEstq := 21
	If l261IntWMS
		nTamAEstq += 1
	EndIf
	nTamAEstq += 1
	If SD3->(FieldPos("D3_IDDCF"))>0 .And. l261IntWMS
		nTamAEstq += 1
	EndIf
	If SD3->(FieldPos("D3_OBSERVA")) <> 0
		nTamAEstq += 1
	EndIf
	///////////////////////////////////////////////////

	BEGIN TRANSACTION

		SB1->(DbSetOrder(1))
		SB1->(DbSeek( xFilial('SB1') + cB1_COD ))
		//
		// Adiciona cabecalho com numero do documento e data da transferencia modelo II
		//
		cDocumento  := Criavar("D3_DOC")
		cDocumento	:= IIf(Empty(cDocumento),NextNumero("SD3",2,"D3_DOC",.T.),cDocumento)
		cDocumento	:= A261RetINV(cDocumento)
		//
		aadd(aItensNew, { cDocumento, ddatabase })
		//
		// sequencia
		// produto, descricao, unidade de medida, local/localizacao origem
		// produto, descricao, unidade de medida, local/localizacao destino
		// numero de serie, lote, sublote, data de validade, qunatidade
		// quantidade na 2 unidade, estorno, numero de sequencia
		//
		//
		AADD( aItensNew , Array(nTamAEstq) )
		nPosAEstq := Len(aItensNew)
		// Produto Origem
		aItensNew[nPosAEstq,01] := SB1->B1_COD
		aItensNew[nPosAEstq,02] := SB1->B1_DESC
		aItensNew[nPosAEstq,03] := SB1->B1_UM
		aItensNew[nPosAEstq,04] := cArmFrom
		aItensNew[nPosAEstq,05] := "" // IIf(Localiza(SB1->B1_COD),Posicione("VOI",1,xFilial("VOI")+aPecasAlt[iP,4],"VOI_LOCALI"),Space(15))
		// Produto Destino
		aItensNew[nPosAEstq,06] := SB1->B1_COD
		aItensNew[nPosAEstq,07] := SB1->B1_DESC
		aItensNew[nPosAEstq,08] := SB1->B1_UM
		aItensNew[nPosAEstq,09] := cArmTo
		aItensNew[nPosAEstq,10] := "" // IIf(Localiza(SB1->B1_COD),Posicione("VOI",1,xFilial("VOI")+VO3->VO3_TIPTEM,"VOI_LOCALI"),Space(15))
		//
		aItensNew[nPosAEstq,11] := criavar('D3_NUMSERI')
		aItensNew[nPosAEstq,12] := criavar('D3_LOTECTL')
		aItensNew[nPosAEstq,13] := criavar('D3_NUMLOTE')
		aItensNew[nPosAEstq,14] := criavar('D3_DTVALID')
		aItensNew[nPosAEstq,15] := criavar('D3_POTENCI')
		aItensNew[nPosAEstq,16] := nQtd
		aItensNew[nPosAEstq,17] := criavar('D3_QTSEGUM')
		aItensNew[nPosAEstq,18] := criavar('D3_ESTORNO')
		aItensNew[nPosAEstq,19] := criavar('D3_NUMSEQ')
		aItensNew[nPosAEstq,20] := criavar('D3_LOTECTL')
		aItensNew[nPosAEstq,21] := criavar('D3_DTVALID')
		aItensNew[nPosAEstq,21] := criavar('D3_DTVALID')

		nUltPos := 21
		If l261IntWMS
			aItensNew[nPosAEstq,++nUltPos] := criavar("D3_SERVIC")
		EndIf
		aItensNew[nPosAEstq,++nUltPos] := criavar("D3_ITEMGRD")
		If SD3->(FieldPos("D3_IDDCF"))>0 .And. l261IntWMS
			aItensNew[nPosAEstq,++nUltPos] := criavar("D3_IDDCF")
		EndIf
		If SD3->(FieldPos("D3_OBSERVA")) <> 0
			aItensNew[nPosAEstq,++nUltPos] := criavar("D3_OBSERVA")
		EndIf
		
		If (ExistBlock("VCLSAE1"))
			aItensNew := ExecBlock("VCLSAE1", .f., .f., {aItensNew})
		EndIf

		lMsErroAuto := .F.

		MSExecAuto({|x| MATA261(x)},aItensNew)
	
		If lMsErroAuto
			// Cancela Gravacao
			lRet := .f.
			DisarmTransaction()
			MostraErro()
			cDocumento := "ERRO"
			Break
		EndIf
	END TRANSACTION
	
Return .T.

/*/{Protheus.doc} Transfere
	Transfere item de um armazem a outro e nesse caso levando em consideracao o lote

	@author Vinicius Gati
	@since  19/10/2016
/*/
Method TransfereLote(cB1_COD, cArmFrom, cArmTo, nQtd, cLote, cSubLote) Class DMS_Estoque
	Local aItensNew := {}
	Local oPeca     := DMS_Peca():New()
	Local nTamAEstq := 0
	Local l261IntWMS := a261IntWMS()

	///////////////////////////////////////////////////
	// Posicoes no Vetor de Integracao com o MATA261 //
	///////////////////////////////////////////////////
	nTamAEstq := 21
	If l261IntWMS
		nTamAEstq += 1
	EndIf
	nTamAEstq += 1
	If SD3->(FieldPos("D3_IDDCF"))>0 .And. l261IntWMS
		nTamAEstq += 1
	EndIf
	If SD3->(FieldPos("D3_OBSERVA")) <> 0
		nTamAEstq += 1
	EndIf
	///////////////////////////////////////////////////

	SB1->(DbSetOrder(1))
	SB1->(DbSeek( xFilial('SB1') + cB1_COD ))
	//
	// Adiciona cabecalho com numero do documento e data da transferencia modelo II
	//
	cDocumento  := Criavar("D3_DOC")
	cDocumento	:= IIf(Empty(cDocumento),NextNumero("SD3",2,"D3_DOC",.T.),cDocumento)
	cDocumento	:= A261RetINV(cDocumento)
	//
	aadd(aItensNew, { cDocumento, ddatabase })
	//
	// sequencia
	// produto, descricao, unidade de medida, local/localizacao origem
	// produto, descricao, unidade de medida, local/localizacao destino
	// numero de serie, lote, sublote, data de validade, qunatidade
	// quantidade na 2 unidade, estorno, numero de sequencia
	//
	//
	AADD( aItensNew , Array( nTamAEstq ) )
	nPosAEstq := Len(aItensNew)
	// Produto Origem
	aItensNew[nPosAEstq,01] := SB1->B1_COD
	aItensNew[nPosAEstq,02] := SB1->B1_DESC
	aItensNew[nPosAEstq,03] := SB1->B1_UM
	aItensNew[nPosAEstq,04] := cArmFrom
	aItensNew[nPosAEstq,05] := ""
	// Produto Destino
	aItensNew[nPosAEstq,06] := SB1->B1_COD
	aItensNew[nPosAEstq,07] := SB1->B1_DESC
	aItensNew[nPosAEstq,08] := SB1->B1_UM
	aItensNew[nPosAEstq,09] := cArmTo
	aItensNew[nPosAEstq,10] := ""
	//
	aItensNew[nPosAEstq,11] := criavar('D3_NUMSERI')
	aItensNew[nPosAEstq,12] := IIF(EMPTY(cLote),     criavar('D3_LOTECTL'),    cLote)
	aItensNew[nPosAEstq,13] := IIF(EMPTY(cSubLote),  criavar('D3_NUMLOTE'), cSubLote)
	aItensNew[nPosAEstq,14] := criavar('D3_DTVALID')
	aItensNew[nPosAEstq,15] := criavar('D3_POTENCI')
	aItensNew[nPosAEstq,16] := nQtd
	aItensNew[nPosAEstq,17] := criavar('D3_QTSEGUM')
	aItensNew[nPosAEstq,18] := criavar('D3_ESTORNO')
	aItensNew[nPosAEstq,19] := criavar('D3_NUMSEQ')
	aItensNew[nPosAEstq,20] := criavar('D3_LOTECTL')
	aItensNew[nPosAEstq,21] := criavar('D3_DTVALID')
	aItensNew[nPosAEstq,21] := criavar('D3_DTVALID')

	nUltPos := 21
	If l261IntWMS
		aItensNew[nPosAEstq,++nUltPos] := criavar("D3_SERVIC")
	EndIf
	aItensNew[nPosAEstq,++nUltPos] := criavar("D3_ITEMGRD")
	If SD3->(FieldPos("D3_IDDCF"))>0 .And. l261IntWMS
		aItensNew[nPosAEstq,++nUltPos] := criavar("D3_IDDCF")
	EndIf
	If SD3->(FieldPos("D3_OBSERVA")) <> 0
		aItensNew[nPosAEstq,++nUltPos] := criavar("D3_OBSERVA")
	EndIf

	If (ExistBlock("VCLSAE2"))
		aItensNew := ExecBlock("VCLSAE2", .f., .f., {aItensNew})
	EndIf

	lMsErroAuto := .F.

	MSExecAuto({|x| MATA261(x)},aItensNew)

	If lMsErroAuto
		// Cancela Gravacao
		lRet := .f.
		DisarmTransaction()
		MostraErro()
		cDocumento := "ERRO"
	EndIf
Return cDocumento

/*----------------------------------------------------
 Suavizar a nova verifica��o de integra��o com o WMS
------------------------------------------------------*/
Static Function a261IntWMS(cProduto)
Default cProduto := ""
	If FindFunction("IntWMS")
		Return IntWMS(cProduto)
	Else
		Return IntDL(cProduto)
	EndIf
Return
