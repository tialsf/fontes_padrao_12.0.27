#INCLUDE "VEIPR810.CH"
#INCLUDE "PROTHEUS.CH"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Veipr810  � Autor � Ricardo Farinelli  � Data �  26/11/01   ���
�������������������������������������������������������������������������͹��
���Descricao � Relatorio de Grupos disponiveis (VIP)                      ���
���          � 																			                      ���
�������������������������������������������������������������������������͹��
���Uso       � Gestao de Concessionarias                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function VEIPR810()
Local aOrd           := {}
Local cDesc1         := STR0006 //"Este programa tem como objetivo imprimir os Grupos disponiveis"
Local cDesc2         := STR0007 //"a serem utilizados pelo modulo VIP"
Local cDesc3         := ""
Local imprime        := .T.
Local wnrel          := "VEIPR810" 
Local cString        := "VP3"

Private titulo       := STR0008 //"Grupos existentes (VIP)"
Private nLin         := 80
Private lEnd         := .F.
Private lAbortPrint  := .F.
Private limite       := 80
Private tamanho      := "P"
Private Cabec1       := STR0009 //" [Grupo] [Descricao-------------------] [Abertura] [Mes] [Desp. Mes] [Encerram]"
Private Cabec2       := ""
Private nTipo        := 15
Private aReturn      := { STR0001, 1, STR0002, 1, 2, 1, "", 1} //"Zebrado"###"Administracao"
Private nLastKey     := 0
Private cPerg        := "VPR810"
Private cbtxt        := Space(10)
Private cbcont       := 00
Private CONTFL       := 01
Private m_pag        := 01

DbSelectArea("VP3")
DbSetOrder(1)

Pergunte(cPerg,.F.)

wnrel := SetPrint(cString,wnrel,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.F.,Tamanho,,.F.)

If nLastKey == 27
	Return
EndIf

SetDefault(aReturn,cString)

If nLastKey == 27
	Return
EndIf

nTipo := If(aReturn[4]==1,15,18)

RptStatus({|lEnd| VPR810IMP(@lEnd,wnrel,cString)},Titulo)

Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Fun��o    �VPR810IMP � Autor � Ricardo Farinelli  � Data �  26/11/01   ���
�������������������������������������������������������������������������͹��
���Descri��o � Funcao auxiliar para a impressao do relatorio de Grupos    ���
���          � consorcio VIP                                              ���
�������������������������������������������������������������������������͹��
���Uso       � Gestao de Concessionarias                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function VPR810IMP(lEnd,wnrel,cString)

DbselectArea("VP3")
//���������������������������������������������������������������������Ŀ
//� SETREGUA -> Indica quantos registros serao processados para a regua �
//�����������������������������������������������������������������������
SetRegua(RecCount())
//���������������������������������������������Ŀ
//�MV_PAR01 = Data Inicial                      �
//�MV_PAR02 = Data Final                        �
//�����������������������������������������������
Dbseek(xFilial("VP3")+MV_PAR01,.T.)

Do While  VP3->(VP3_FILIAL+VP3_CODGRU) <= xFilial("VP3")+MV_PAR02 .and. !VP3->(Eof())

	If lAbortPrint .or. lEnd
		@nLin,00 PSAY STR0010 //"*** CANCELADO PELO OPERADOR ***"
		Exit
	EndIf
   
  IncRegua()
   
	If nLin > 58
		Cabec(Titulo,Cabec1,Cabec2,wnrel,Tamanho,nTipo)
		nLin:=9
	EndIf

	@ nLin,01 PSAY VP3->VP3_CODGRU
	@ nLin,09 PSAY VP3->VP3_DESGRU
	@ nLin,40 PSAY VP3->VP3_DATABE
	@ nLin,51 PSAY Strzero(VP3->VP3_NUMMES,5)
	@ nLin,57 PSAY Transform(VP3->VP3_VALDES,"@E 9999,999.99")
	@ nLin,69 PSAY VP3->VP3_DATENC
	nLin++  
	If nLin > 58
		Cabec(Titulo,Cabec1,Cabec2,wnrel,Tamanho,nTipo)
		nLin:=9
	EndIf
	VP3->(DbSkip())
Enddo

//���������������������������������������������������������������������Ŀ
//� Finaliza a execucao do relatorio...                                 �
//�����������������������������������������������������������������������

SET DEVICE TO SCREEN

//���������������������������������������������������������������������Ŀ
//� Se impressao em disco, chama o gerenciador de impressao...          �
//�����������������������������������������������������������������������

If aReturn[5]==1
	DbCommitAll()
	SET PRINTER TO
	OurSpool(wnrel)
EndIf

MS_FLUSH()

Return