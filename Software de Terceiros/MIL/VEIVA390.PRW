// 浜様様様曜様様様様�  
// � Versao � 02     �
// 藩様様様擁様様様様�

#INCLUDE "TOPCONN.CH"
#INCLUDE "VEIVA390.CH"
/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �VEIVA390  �Autor  �Renato Vinicius     � Data �  27/06/16   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Desc.     � Cadastro de percentual de Bonus default por Marca/Modelo   艮�
臼�          �                                                            艮�
臼麺様様様様謡様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様恒�
臼�Uso       � Veiculo                                                    艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/

Function VEIVA390()

	Private aRotina    := MenuDef()
	Private cCadastro := OemToAnsi(STR0001)
	Private aTELA[0][0] , aGETS[0] // Variaveis utilizadas pela enchoice que deve existir em todo o contexto
	Private aCampos := {}
	
	DbSelectArea("SX3")
	DbSetOrder(1)
	DbSeek("VR3")
	do While !eof() .and. X3_ARQUIVO == "VR3"
	   If X3USO(X3_USADO).and.cNivel>=X3_NIVEL 	   
	      aadd(aCampos,X3_CAMPO)	      
	   EndIf
		SX3->(DbSkip())	   
	Enddo 

	DbSelectArea("VR3")
	mBrowse( 6, 1,22,75,"VR3")
	
Return

Static Function MenuDef()
Return { ;
	{ OemToAnsi(STR0002)  ,"AxPesqui"  , 0 , 1 },;  // Pesquisar
	{ OemToAnsi(STR0003)  ,"VA390V"    , 0 , 2 },;  // Visualizar
	{ OemToAnsi(STR0004)  ,"VA390I"    , 0 , 3 },;  // Incluir
	{ OemToAnsi(STR0005)  ,"VA390A"    , 0 , 4 },;  // Alterar
	{ OemToAnsi(STR0006)  ,"VA390E"    , 0 , 5 } ;  // Excluir
}

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �VEIVA390  �Autor  �Renato Vinicius     � Data �  28/06/16   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Desc.     � Visualiza艫o                                               艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function VA390V(cAlias,nReg,nOpc)
	AxVisual(cAlias,nReg,nOpc,aCampos)
Return


/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �VEIVA390  �Autor  �Renato Vinicius     � Data �  28/06/16   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Desc.     � Inclus�o                                                   艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function VA390I(cAlias,nReg,nOpc)

Begin Transaction
	If AxInclui(cAlias,nReg,nOpc,aCampos,,,"FS_VALMOD()") == 0
		DisarmTransaction()
	EndIf
End Transaction

Return

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �VEIVA390  �Autor  �Renato Vinicius     � Data �  28/06/16   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Desc.     � Altera艫o                                                  艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function VA390A(cAlias,nReg,nOpc)
	AxAltera(cAlias,nReg,nOpc,aCampos)
Return

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �VEIVA390  �Autor  �Renato Vinicius     � Data �  28/06/16   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Desc.     � Exclus�o                                                   艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function VA390E(cAlias,nReg,nOpc)
	AxDeleta(cAlias,nReg,nOpc)
Return

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �VEIVA390  �Autor  �Renato Vinicius     � Data �  28/06/16   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Desc.     � Valida艫o da Inclus�o de Bonus por modelo                  艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function FS_VALMOD()

Local lRet := .t.

If !Empty(M->VR3_CODMAR) .and. !Empty(M->VR3_MODVEI)
	cQuery := "SELECT * FROM "+RetSqlName("VR3")+" VR3 "
	cQuery += "WHERE VR3.VR3_FILIAL = '"+xFilial("VR3")+"' AND VR3.VR3_CODMAR = '"+M->VR3_CODMAR+"' AND "
	cQuery += "VR3.VR3_MODVEI = '"+M->VR3_MODVEI+"' AND VR3.D_E_L_E_T_ = ' '"
	TcQuery cQuery New Alias "TMPVR3"
	If !TMPVR3->(Eof())
		MsgStop(STR0007,STR0008)
		lRet := .f.
	EndIf
	TMPVR3->(DbCloseArea())	
EndIf

Return lRet