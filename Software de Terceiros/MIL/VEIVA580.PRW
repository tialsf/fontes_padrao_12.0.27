#INCLUDE "VEIVA580.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � VEIVA580 � Autor �Ricardo Farinelli      � Data � 03/01/02 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Tabela de Status para os Veiculos (simulacao/proposta)     ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Gestao de Concessionarias                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function VEIVA580
//��������������������������������������������������������������Ŀ
//� Define o cabecalho da tela de atualizacoes                   �
//����������������������������������������������������������������
PRIVATE cCadastro := OemToAnsi(STR0001) //"Tabela de Status de Veiculos"
//��������������������������������������������������������������Ŀ
//� Endereca a funcao axCadastro                                 �
//����������������������������������������������������������������
axCadastro("VAE", cCadastro, "VEIVA580EX()")

Return .T.
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �Veiva580Ex� Autor � Ricardo Farinelli     � Data � 03/01/02 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Valida se pode ser feito a exclusao                        ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Gestao de Concessionarias                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function Veiva580Ex()
Local aArquivos := {}
aadd(aArquivos,{"VV1","VV1_STATUS",VAE->VAE_CODIGO,})
Return FG_DELETA(aArquivos)

Static Function MenuDef()
Return StaticCall(MATXATU,MENUDEF)