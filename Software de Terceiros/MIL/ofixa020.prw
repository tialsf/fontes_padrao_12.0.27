// 浜様様様曜様様様様�
// � Versao � 16     �
// 藩様様様擁様様様様�
#include "OFIXA020.CH"
#include "PROTHEUS.CH"

/*/{Protheus.doc} mil_ver()
    Versao do fonte modelo novo

    @author Andre Luis Almeida
    @since  13/11/2017
/*/
Static Function mil_ver()
	If .F.
		mil_ver()
	EndIf
Return "007480_1"

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Funcao    � OFIXA020 � Autor � Luis Delorme                      � Data � 25/06/12 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descricao � Pedidos de Transfer�ncia de Pe�as                                      咳�
臼青陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function OFIXA020()

Local cFiltro := ""

Private cCadastro := STR0001
Private aRotina   := MenuDef()
Private aCores    := {;
{'VDD->VDD_STATUS == "S"','BR_AMARELO'},;	// Pendente 
{'VDD->VDD_STATUS == "A"','BR_VERDE'},;		// Atendida
{'VDD->VDD_STATUS == "R"','BR_VERMELHO'},;	// Rejeitado
{'VDD->VDD_STATUS == "E"','BR_PRETO'},;		// NF Emitida
{'VDD->VDD_STATUS == "C"','BR_AZUL'} }      // Entrada Confirmada
//
//敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳朕
//� Endereca a funcao de BROWSE                                  �
//青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳潰
//
DBSelectArea("VAI")
DBSetOrder(4)
DBSeek(xFilial("VAI")+__cUserId)

dbSelectArea("VDD")
dbSetOrder(1)

// Ponto de Entrada para Filtro do Browse
If ExistBlock("OX020FBRW")
	cFiltro := ExecBlock("OX020FBRW", .f., .f.)
EndIf

if !Empty(VAI->VAI_TIPTRA)
	cFiltro := "VDD->VDD_STATUS $ VAI->VAI_TIPTRA" + Iif(Empty(cFiltro), "", " .AND. " + cFiltro)
endif

If !Empty(cFiltro)
	FilBrowse("VDD", {}, cFiltro)
EndIf

mBrowse( 6, 1,22,75,"VDD",,,,,,aCores)
dbClearFilter()
//
Return
/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Funcao    � OXA020A  � Autor � Luis Delorme                      � Data � 25/06/12 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descricao � Rotina para aceita艫o do pedido de transfer�ncia                       咳�
臼青陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function OXA020A(cAlias,nReg,nOpc)
Local nTam       := 0
Local aObjects   := {} , aPosObj := {} , aPosObjApon := {} , aInfo := {}
Local aSizeAut   := MsAdvSize(.T.)  // Tamanho Maximo da Janela (.t.=TOOLBAR,.f.=SEM TOOLBAR)
Local lCtrlLote  := GetNewPar("MV_RASTRO","N") == "S"   
Local lOkTela    := .f.
Local ni         := 0
Local _ii        := 0
Local nPos       := 0
Local cB1LocPad  := ""
Local cCor       := ""
Local cQuery     := ""
Local cQAlSQL    := "SQLVDD"
Local aParam     := {}
Local lVDD_TIPTRA:= ( VDD->(FieldPos("VDD_TIPTRA")) > 0 )
Local lVS3_TIPTRA:= ( VS3->(FieldPos("VS3_TIPTRA")) > 0 )
Local lVS3_VENTRA:= ( VS3->(FieldPos("VS3_VENTRA")) > 0 )
Local lVS3_QTDAPR:= ( VS3->(FieldPos("VS3_QTDAPR")) > 0 )
Local aVetOrcSld := {}
Local nAteVDD 	 := 0
Local cFaseConfer := Alltrim(GetNewPar("MV_MIL0095","4")) // Fase de Conferencia e Separacao
Local lFaseConfer := (At(cFaseConfer,GetMv("MV_FASEORC")) <> 0)

Local oFilHlp := DMS_FilialHelper():New()

Private oOkTik   := LoadBitmap( GetResources() , "LBTIK" )
Private oNoTik   := LoadBitmap( GetResources() , "LBNO" )
Private oVerm    := LoadBitmap( GetResources() , "BR_VERMELHO" )
Private aVDD     := {}

Private aSdPLtDMS := {}

cMV_PAR01 := Space(TamSX3("VS3_OPER")[1])
cMV_PAR02 := Space(TamSX3("F4_CODIGO")[1])
cMV_PAR03 := Space(TamSX3("F4_CODIGO")[1])

// -------------------------------------------------------------------
// PE para carregar os valores dos campos da parambox.
// -------------------------------------------------------------------
If ExistBlock("OX020PAR")
	aParam := ExecBlock("OX020PAR",.f.,.f.) 
	cMV_PAR01 := aParam[1]
	cMV_PAR02 := aParam[2]
	cMV_PAR03 := aParam[3]
EndIf  

cQuery := "SELECT "+IIf(lVDD_TIPTRA,"VDD.VDD_TIPTRA , ","")
cQuery += "VDD.VDD_QUANT , VDD.VDD_FILORC , VDD.VDD_NUMORC , VDD.R_E_C_N_O_ AS RECVDD , SB1.R_E_C_N_O_ AS RECSB1 FROM "+RetSqlName("VDD")+" VDD "
cQuery += "JOIN "+RetSqlName("SB1")+" SB1 ON ( SB1.B1_FILIAL='"+xFilial("SB1")+"' AND SB1.B1_GRUPO=VDD.VDD_GRUPO AND SB1.B1_CODITE=VDD.VDD_CODITE AND SB1.D_E_L_E_T_=' ' ) "
cQuery += "WHERE VDD.VDD_FILIAL='"+xFilial("VDD")+"' AND VDD.VDD_STATUS='S' AND VDD.VDD_FILPED='"+cFilAnt+"' AND VDD.D_E_L_E_T_=' ' "
dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQuery ), cQAlSQL , .F., .T. )
While !( cQAlSQL )->( Eof() )
	SB1->(DbGoTo(( cQAlSQL )->( RECSB1 )))
	cB1LocPad := FM_PRODSBZ(SB1->B1_COD,"SB1->B1_LOCPAD")
	SB2->(DbSetOrder(1))
	SB2->(DbSeek(xFilial("SB2")+SB1->B1_COD+cB1LocPad))
	SB3->(DbSetOrder(1))
	SB3->(DbSeek(xFilial("SB3")+SB1->B1_COD))
	cCor := "0" // Nao Selecionado
	If SB2->B2_CM1 <= 0
		cCor := "2" // Nao pode ser selecionado ( custo <= 0 )
	ElseIf ( VDD->VDD_FILORC + VDD->VDD_NUMORC ) == ( ( cQAlSQL )->( VDD_FILORC ) + ( cQAlSQL )->( VDD_NUMORC ) ) // Mesma FILIAL/NRO.ORCAMENTO
		cCor := "1" // Selecionado
	Endif
	aAdd(aVDD,{ cCor , IIf(lVDD_TIPTRA,( cQAlSQL )->( VDD_TIPTRA ),"") , ( cQAlSQL )->( VDD_FILORC ) , ( cQAlSQL )->( VDD_NUMORC ) , SB1->B1_GRUPO , SB1->B1_CODITE , SB1->B1_DESC , ( cQAlSQL )->( VDD_QUANT ) , SB2->B2_QATU , SB2->B2_CM1 , SB3->B3_CLASSE , SB3->B3_MEDIA , cB1LocPad , ( cQAlSQL )->( RECVDD ) , ( cQAlSQL )->( RECSB1 ) })
	( cQAlSQL )->( DbSkip() )
EndDo
( cQAlSQL )->( DbCloseArea() )
DbSelectArea("VDD")

If len(aVDD) <= 0
	MsgAlert("Nenhum registro pendente de transferencia foi encontrado para a Filial do Pedido.","Aten艫o")
	Return .f.
EndIf

aObjects := {}
AAdd( aObjects, { 05 , 17 , .T. , .F. } )  // Cabecalho
AAdd( aObjects, { 01 , 00 , .T. , .T. } )  // Listbox
AAdd( aObjects, { 05 , 17 , .T. , .F. } )  // Legenda
aInfo := {aSizeAut[1] , aSizeAut[2] , aSizeAut[3] , aSizeAut[4] , 2 , 2 }
aPosObj := MsObjSize (aInfo, aObjects,.F.)

DEFINE MSDIALOG oDlg1 TITLE "Aceitar Transferencias de Pe�as entre Filiais"  From aSizeAut[7],000 to aSizeAut[6],aSizeAut[5] of oMainWnd PIXEL // Aceitar Transferencias de Pe�as entre Filiais
//
nTam := ( aPosObj[1,4] / 10 )
@ aPosObj[1,1],aPosObj[1,2] TO aPosObj[1,3],aPosObj[1,4] LABEL ("") OF oDlg1 PIXEL // Caixa Cabecalho
@ aPosObj[1,1]+005,aPosObj[1,2]+(nTam*01) SAY STR0029 SIZE 60,08 OF oDlg1 PIXEL COLOR CLR_BLUE // Tipo de Operacao
@ aPosObj[1,1]+004,aPosObj[1,2]+(nTam*02) MSGET oMV_PAR01 VAR cMV_PAR01 PICTURE "@!" F3 "DJ" VALID OXA020TPOP() SIZE (nTam),08 OF oDlg1 PIXEL COLOR CLR_BLUE HASBUTTON
@ aPosObj[1,1]+005,aPosObj[1,2]+(nTam*04) SAY STR0005 SIZE 60,08 OF oDlg1 PIXEL COLOR CLR_BLUE // TES SAIDA
@ aPosObj[1,1]+004,aPosObj[1,2]+(nTam*05) MSGET oMV_PAR02 VAR cMV_PAR02 PICTURE "@!" F3 "SF4" VALID OXA020VTES() SIZE (nTam),08 OF oDlg1 PIXEL COLOR CLR_BLUE HASBUTTON
@ aPosObj[1,1]+005,aPosObj[1,2]+(nTam*07) SAY STR0033 SIZE 60,08 OF oDlg1 PIXEL COLOR CLR_BLUE // TES ENTRADA
@ aPosObj[1,1]+004,aPosObj[1,2]+(nTam*08) MSGET oMV_PAR03 VAR cMV_PAR03 PICTURE "@!" F3 "SF4" VALID OXA020TESE() SIZE (nTam),08 OF oDlg1 PIXEL COLOR CLR_BLUE HASBUTTON
//
oLbVDD := TWBrowse():New(aPosObj[2,1],aPosObj[2,2],aPosObj[2,4]-2,(aPosObj[2,3]-aPosObj[2,1]),,,,oDlg1,,,,,{ || FS_VDDACEIT(oLbVDD:nAt) },,,,,,,.F.,"",.T.,,.F.,,,)
oLbVDD:SetArray(aVDD)
oLbVDD:addColumn( TCColumn():New( "" , { || IIf(aVDD[oLbVDD:nAt,01]=="1",oOkTik,IIf(aVDD[oLbVDD:nAt,01]=="0",oNoTik,oVerm)) }                  ,,,, "LEFT" ,  08 ,.T.,.F.,,,,.F.,) ) // Tik
oLbVDD:addColumn( TCColumn():New( "Tp.Transf."     , { || IIf(!Empty(aVDD[oLbVDD:nAt,02]),X3CBOXDESC("VDD_TIPTRA",aVDD[oLbVDD:nAt,02]),"") }   ,,,, "LEFT" ,  40 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Filial"         , { || aVDD[oLbVDD:nAt,03] }                                                                ,,,, "LEFT" ,  40 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Or�amento"      , { || aVDD[oLbVDD:nAt,04] }                                                                ,,,, "LEFT" ,  40 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Grupo"          , { || aVDD[oLbVDD:nAt,05] }                                                                ,,,, "LEFT" ,  30 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "C�digo"         , { || aVDD[oLbVDD:nAt,06] }                                                                ,,,, "LEFT" ,  65 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Descri艫o"      , { || aVDD[oLbVDD:nAt,07] }                                                                ,,,, "LEFT" , 110 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Qtd.Solicitada" , { || FG_AlinVlrs(Transform(aVDD[oLbVDD:nAt,08],X3PICTURE("VDD_QUANT"))) }                 ,,,, "RIGHT",  55 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Qtd.Estoque"    , { || FG_AlinVlrs(Transform(aVDD[oLbVDD:nAt,09],X3PICTURE("B2_QATU"))) }                   ,,,, "RIGHT",  55 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Custo Unit�rio" , { || FG_AlinVlrs(Transform(aVDD[oLbVDD:nAt,10],X3PICTURE("B2_CM1"))) }                    ,,,, "RIGHT",  55 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Classif.ABC"    , { || aVDD[oLbVDD:nAt,11] }                                                                ,,,, "LEFT" ,  55 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Giro Estoque"   , { || FG_AlinVlrs(Transform(aVDD[oLbVDD:nAt,12],"@EZ 99999,999,999")) }                    ,,,, "RIGHT",  55 ,.F.,.F.,,,,.F.,) )
//
nTam := ( aPosObj[3,4] / 16 )
@ aPosObj[3,1]+005,aPosObj[3,2]+(nTam*01)+00 BITMAP oxNTik RESOURCE "LBNO" OF oDlg1 NOBORDER SIZE 10,10 when .f. PIXEL
@ aPosObj[3,1]+005,aPosObj[3,2]+(nTam*01)+15 SAY "Pe�a(s) n�o selecionada(s)" SIZE 80,8 OF oDlg1 PIXEL COLOR CLR_BLUE // Pe�a(s) n�o selecionada(s)
@ aPosObj[3,1]+005,aPosObj[3,2]+(nTam*06)+00 BITMAP oxCTik RESOURCE "LBTIK" OF oDlg1 NOBORDER SIZE 10,10 when .f. PIXEL
@ aPosObj[3,1]+005,aPosObj[3,2]+(nTam*06)+15 SAY "Pe�a(s) selecionada(s)" SIZE 80,8 OF oDlg1 PIXEL COLOR CLR_BLUE // Pe�a(s) selecionada(s)
@ aPosObj[3,1]+005,aPosObj[3,2]+(nTam*11)+00 BITMAP oxVerm RESOURCE "BR_VERMELHO" OF oDlg1 NOBORDER SIZE 10,10 when .f. PIXEL
@ aPosObj[3,1]+005,aPosObj[3,2]+(nTam*11)+15 SAY "Pe�a(s) com custo zerado" SIZE 80,8 OF oDlg1 PIXEL COLOR CLR_BLUE // Pe�a(s) com custo zerado
//
ACTIVATE MSDIALOG oDlg1 ON INIT EnchoiceBar(oDlg1, {|| IIf(FS_OKACEITA(),(lOkTela:=.t.,oDlg1:End()),.f.) } , {|| oDlg1:End() },,)

If lOkTela

	If FindFunction( "OX001PrxNro" )
		cNroOrc := OX001PrxNro()
	Else
		cNroOrc := GetSXENum("VS1","VS1_NUMORC")
		ConfirmSX8()
	Endif

	SA1->(DbGoTo( oFilHlp:GetCliente(VDD->VDD_FILORC) ))

	dbSelectArea("VS1")
	RecLock("VS1",.t.)
	VS1->VS1_FILIAL := xFilial("VS1")
	VS1->VS1_NUMORC := cNroOrc
	VS1->VS1_TIPORC := "3" // Transferencia
	VS1->VS1_DATORC := dDataBase
	VS1->VS1_CLIFAT := SA1->A1_COD
	VS1->VS1_LOJA   := SA1->A1_LOJA
	VS1->VS1_NCLIFT := SA1->A1_NOME
	VS1->VS1_STATUS := Iif(lFaseConfer, cFaseConfer,"F") // Aguardando conferencia ou Pronto para Transferir
	VS1->VS1_FILDES := VDD->VDD_FILORC
	MsUnlock()
	
	lUsaVenc := ( SuperGetMv('MV_LOTVENC') == 'S' )
	
	nSeq := 1

	For _ii := 1 to Len(aVDD)
		If aVDD[_ii,1] == "1"
			DbSelectArea("VDD")
			DbGoto(aVDD[_ii,14])
			DbSelectArea("SB1")
			DbGoto(aVDD[_ii,15])            
			If lCtrlLote .and. Rastro( SB1->B1_COD )
				nPos := aScan(aVetOrcSld, {|x| x[1] == SB1->B1_COD}) // Verificar se existe algum registro selecionado
				If nPos == 0
					aadd(aVetOrcSld,{SB1->B1_COD,VDD->VDD_QUANT,aVDD[_ii,13]})
				Else
					aVetOrcSld[nPos,2] += VDD->VDD_QUANT
				Endif
			Endif
		Endif
	Next

	For _ii := 1 to Len(aVetOrcSld)
		aSalOX := SldPorLote(aVetOrcSld[_ii,1],aVetOrcSld[_ii,3],aVetOrcSld[_ii,2],NIL,"","","","",NIL,NIL,NIL,lUsaVenc,nil,nil,dDataBase)
		For ni := 1 to Len(aSalOX)
			aAdd(aSdPLtDMS,{aVetOrcSld[_ii,1],aSalOX[ni,1],aSalOX[ni,5]})
		Next
	Next

	For ni := 1 to len(aVDD)
		If aVDD[ni,1] == "1"
			DbSelectArea("VDD")
			DbGoto(aVDD[ni,14])
			If VDD->VDD_QUANT <> aVDD[ni,8]
				RecLock("VDD",.f.)
				VDD->VDD_QUANT := aVDD[ni,8]
				NsUnlock()                  
			Endif
			DbSelectArea("SB1")
			DbGoto(aVDD[ni,15])            
			nAteVDD := aVDD[ni,8]
			If lCtrlLote .and. Rastro( SB1->B1_COD )
//				aSaldos := SldPorLote(SB1->B1_COD,aVDD[ni,13],VDD->VDD_QUANT,NIL,"","","","",NIL,NIL,NIL,lUsaVenc,nil,nil,dDataBase)
				nPos := aScan(aSdPLtDMS, {|x| x[1] == SB1->B1_COD}) // Verificar se existe algum registro selecionado				
				For _ii := nPos to Len(aSdPLtDMS)
				
					If aSdPLtDMS[_ii,1] == SB1->B1_COD .and. aSdPLtDMS[_ii,3] > 0 .and. nAteVDD > 0
						dbSelectArea("VS3")
						RecLock("VS3",.t.)
						VS3->VS3_FILIAL := xFilial("VS3")
						VS3->VS3_NUMORC := cNroOrc
						VS3->VS3_SEQUEN := strzero(_ii,3)
						VS3->VS3_GRUITE := VDD->VDD_GRUPO
						VS3->VS3_CODITE := VDD->VDD_CODITE
						VS3->VS3_QTDINI := aSdPLtDMS[_ii,3]
						VS3->VS3_TESSAI := cMV_PAR02
						VS3->VS3_TESENT := cMV_PAR03
						VS3->VS3_QTDITE := aSdPLtDMS[_ii,3]
						VS3->VS3_ARMORI := aVDD[ni,13]
						VS3->VS3_LOCAL  := aVDD[ni,13]
						VS3->VS3_VALPEC := SB2->B2_CM1
						VS3->VS3_LOTECT := aSdPLtDMS[_ii,2]
						If lVS3_TIPTRA
							VS3->VS3_TIPTRA := VDD->VDD_TIPTRA
						EndIf
						If lVS3_VENTRA
							VS3->VS3_VENTRA := VDD->VDD_VENTRA
						EndIf
						If lVS3_QTDAPR
							VS3->VS3_QTDAPR := aSdPLtDMS[_ii,3]
						EndIf
						MsUnlock()
						nAteVDD -= aSdPLtDMS[_ii,3]
						aSdPLtDMS[_ii,3] -= aSdPLtDMS[_ii,3]
					Else                              
						If aSdPLtDMS[_ii,1] <> SB1->B1_COD
							exit
						Endif
					EndIf
				Next	
			Else
				dbSelectArea("VS3")
				RecLock("VS3",.t.)
				VS3->VS3_FILIAL := xFilial("VS3")
				VS3->VS3_NUMORC := cNroOrc
				VS3->VS3_SEQUEN := strzero(nSeq++,3)
				VS3->VS3_GRUITE := VDD->VDD_GRUPO
				VS3->VS3_CODITE := VDD->VDD_CODITE
				VS3->VS3_QTDINI := VDD->VDD_QUANT
				VS3->VS3_TESSAI := cMV_PAR02
				VS3->VS3_TESENT := cMV_PAR03
				VS3->VS3_QTDITE := VDD->VDD_QUANT
				VS3->VS3_ARMORI := aVDD[ni,13]
				VS3->VS3_LOCAL  := aVDD[ni,13]
				VS3->VS3_VALPEC := SB2->B2_CM1
				If lVS3_TIPTRA
					VS3->VS3_TIPTRA := VDD->VDD_TIPTRA
				EndIf
				If lVS3_VENTRA
					VS3->VS3_VENTRA := VDD->VDD_VENTRA
				EndIf
				If lVS3_QTDAPR
					VS3->VS3_QTDAPR := VDD->VDD_QUANT
				EndIf
				MsUnlock()
			Endif
			DbSelectArea("VDD")
			reclock("VDD",.f.)
			VDD->VDD_ORCFOR := cNroOrc
			VDD->VDD_STATUS := "A"
			msunlock()
	    EndIf
	Next
	
	MsgInfo("Or�amento Gerado com sucesso!"+CHR(13)+CHR(10)+CHR(13)+CHR(10)+cNroOrc,"Aten艫o")
	
	// PONTO DE ENTRADA PARA ALTERACAO DO OR�AMENTO AO ACEITAR O PEDIDO DE TRANSFERENCIA
	If ExistBlock("OX020ORC")
		ExecBlock("OX020ORC",.f.,.f.)
	EndIf

EndIf

Return .t.

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様曜様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �FS_OKACEITA�Autor � Andre Luis Almeida � Data �  03/11/16   艮�
臼麺様様様様謡様様様様様擁様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Descricao � OK tela do Aceite                                          艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Static Function FS_OKACEITA()
Local ni   := 0
Local lRet := .f.
For ni := 1 to len(aVDD)
	If aVDD[ni,1] == "1" // Pe�a selecionada
		lRet := .t.
		Exit
	EndIf
Next
If lRet
	If Empty(cMV_PAR02)
		MsgStop("Necess�rio preencher o TES de Saida!","Aten艫o")
		lRet := .f.
	EndIf
	If Empty(cMV_PAR03)
		MsgStop("Necess�rio preencher o TES de Entrada!","Aten艫o")
		lRet := .f.
	EndIf
Else
	MsgStop("Necess�rio selecionar uma ou mais Pe�as!","Aten艫o")
EndIf
Return lRet
      
/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様曜様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �FS_VDDACEIT�Autor � Andre Luis Almeida � Data �  03/11/16   艮�
臼麺様様様様謡様様様様様擁様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Descricao � Duplo clique no listbox do Aceite                          艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Static Function FS_VDDACEIT(nLinha)
Local aRet      := {}
Local aParamBox := {}
Local nPos      := 0
If aVDD[nLinha,14] > 0 // Existe registro VDD
	If aVDD[nLinha,1] == "2" // CUSTO <= 0
		MsgStop("Pe�a com custo zerado. Impossivel selecionar.","Aten艫o")
	ElseIf aVDD[nLinha,1] == "1" // Registro selecionado
		aVDD[nLinha,1] := "0"
	Else // Registro nao selecionado
		nPos := aScan(aVDD, {|x| x[1] == "1" }) // Verificar se existe algum registro selecionado
	    If nPos > 0
			If !Empty(aVDD[nLinha,2]) .and. !Empty(aVDD[nPos,2]) .and. aVDD[nLinha,2] <> aVDD[nPos,2]
				MsgStop("Tipo de Transferencia � diferente do Tipo de Transferencia j� selecionado. Impossivel continuar.","Aten艫o")
				Return
			EndIf
			If aVDD[nLinha,3] <> aVDD[nPos,3]
				MsgStop("Filial do Or�amento � diferente da Filial do Or�amento j� selecionada. Impossivel continuar.","Aten艫o")
				Return
			EndIf
			If Left(aVDD[nLinha,4],1) == "P" .and. Left(aVDD[nLinha,4],1) <> Left(aVDD[nPos,4],1)
				MsgStop("N�o � poss�vel selecionar Pedido de Transfer�ncia com origens diferentes. Impossivel continuar.","Aten艫o")
				Return
			EndIf
		EndIf
		If Left(aVDD[nLinha,4],1) == "P"
			AADD(aParamBox,{1,RetTitle("B1_GRUPO") ,aVDD[nLinha,5],"@!",'',"",".F.",030,.F.}) // 1 - Grupo
			AADD(aParamBox,{1,RetTitle("B1_CODITE"),aVDD[nLinha,6],"@!",'',"",".F.",080,.F.}) // 2 - CodIte
			AADD(aParamBox,{1,RetTitle("B1_DESC")  ,aVDD[nLinha,7],"@!",'',"",".F.",120,.F.}) // 3 - Descricao
			AADD(aParamBox,{1,"Qtd.Solicitada"     ,aVDD[nLinha,8],X3PICTURE("VDD_QUANT"),"MV_PAR04>=0" ,"",".T.",070,.F.}) // Qtd.Solicitada
			If ParamBox(aParamBox,"Qtd.Solicitada",@aRet,,,,,,,,.f.)
				aVDD[nLinha,8] := aRet[4]
				If aVDD[nLinha,8] > 0
					aVDD[nLinha,1] := "1"
				EndIf
			EndIf
		Else
			aVDD[nLinha,1] := "1"
		EndIf		
	EndIf
	oLbVDD:Refresh()
EndIf
Return

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Funcao    � OXA020R  � Autor � Luis Delorme                      � Data � 25/06/12 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descricao � Rotina para rejei艫o  do pedido de transfer�ncia                       咳�
臼青陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function OXA020R(cAlias,nReg,nOpc)

Local aParamBox := {}
Local aItensNew   := {}  
Local nTamAEstq := 0
Local nPosAEstq
Local nUltPos
Local l261IntWMS  := a261IntWMS()
Local nQuant      := 0 
Local cBkpFil     := cFilAnt
if cFilAnt != VDD->VDD_FILPED
	MsgStop(STR0026)
	return .f.
endif

if VDD->VDD_STATUS != "S"
	return .f.
endif

if !MsgYesNo(STR0012, STR0013)
	return .f.
endif

// Local tmpPar03 := MV_PAR03
MV_PAR01 := ""
aAdd(aParamBox,{1,STR0014,Space(254),"","","","",120,.F.})
while Empty(MV_PAR01)
	If !(ParamBox(aParamBox,STR0001,,,,,,,,,.f.))
		return .f.
	endif
enddo

///////////////////////////////////////////////////
// Posicoes no Vetor de Integracao com o MATA261 //
///////////////////////////////////////////////////
nTamAEstq := 21
If l261IntWMS
	nTamAEstq += 1
EndIf
nTamAEstq += 1
If SD3->(FieldPos("D3_IDDCF"))>0 .And. l261IntWMS
	nTamAEstq += 1
EndIf
If SD3->(FieldPos("D3_OBSERVA")) <> 0
	nTamAEstq += 1
EndIf
///////////////////////////////////////////////////

DBSelectArea("VDD")
reclock("VDD",.f.)
VDD->VDD_MOTIVO := MV_PAR01
VDD->VDD_STATUS := "R"
msunlock()
                       
DBSelectArea("VS3")
DBSetOrder(2)
if DBSeek(VDD->VDD_FILORC + VDD->VDD_NUMORC + VDD->VDD_GRUPO + VDD->VDD_CODITE)
	nQuant := VS3->VS3_QTDITE - VS3->VS3_QTDTRA 
	reclock("VS3",.f.)
	VS3->VS3_QTDTRA := VS3->VS3_QTDTRA - VDD->VDD_QUANT
	msunlock()
endif

DBSelectArea("SB1")
DBSetOrder(7)
DBSeek(xFilial("SB1")+VDD->VDD_GRUPO+VDD->VDD_CODITE)
dbSetOrder(1)

if nQuant > 0
	cFilAnt := VDD->VDD_FILORC
	cDocumento  := Criavar("D3_DOC")
	cDocumento	:= IIf(Empty(cDocumento),NextNumero("SD3",2,"D3_DOC",.T.),cDocumento)
	cDocumento	:= A261RetINV(cDocumento)
			
	aItensNew := {}
	aadd (aItensNew,{ cDocumento , ddatabase})
	AADD( aItensNew , Array(nTamAEstq) )
	nPosAEstq := Len(aItensNew)

	// Produto Origem
	aItensNew[nPosAEstq,01] := SB1->B1_COD
	aItensNew[nPosAEstq,02] := SB1->B1_DESC
	aItensNew[nPosAEstq,03] := SB1->B1_UM
	aItensNew[nPosAEstq,04] := GetMv( "MV_RESITE" )+Space(TamSx3("BF_LOCAL")[1]-Len(GetMv("MV_RESITE")))
	aItensNew[nPosAEstq,05] := IIf(Localiza(SB1->B1_COD),Posicione("VOI",1,xFilial("VOI")+aPecasAlt[iP,4],"VOI_LOCALI"),Space(15))
	// Produto Destino
	aItensNew[nPosAEstq,06] := SB1->B1_COD
	aItensNew[nPosAEstq,07] := SB1->B1_DESC
	aItensNew[nPosAEstq,08] := SB1->B1_UM
	aItensNew[nPosAEstq,09] := FM_PRODSBZ(SB1->B1_COD,"SB1->B1_LOCPAD")
	aItensNew[nPosAEstq,10] := IIf(Localiza(SB1->B1_COD),Posicione("VOI",1,xFilial("VOI")+VO3->VO3_TIPTEM,"VOI_LOCALI"),Space(15))
	//
	aItensNew[nPosAEstq,11] := ""
	aItensNew[nPosAEstq,12] := ""
	aItensNew[nPosAEstq,13] := ""
	aItensNew[nPosAEstq,14] := criavar('D3_DTVALID')
	aItensNew[nPosAEstq,15] := criavar('D3_POTENCI')
	aItensNew[nPosAEstq,16] := nQuant
	aItensNew[nPosAEstq,17] := criavar("D3_QTSEGUM")
	aItensNew[nPosAEstq,18] := criavar("D3_ESTORNO")
	aItensNew[nPosAEstq,19] := criavar("D3_NUMSEQ")
	aItensNew[nPosAEstq,20] := criavar("D3_LOTECTL")
	aItensNew[nPosAEstq,21] := aItensNew[nPosAEstq,14]

	nUltPos := 21
	If l261IntWMS
		aItensNew[nPosAEstq,++nUltPos] := criavar("D3_SERVIC")
	EndIf
	aItensNew[nPosAEstq,++nUltPos] := criavar("D3_ITEMGRD")
	If SD3->(FieldPos("D3_IDDCF"))>0 .And. l261IntWMS
		aItensNew[nPosAEstq,++nUltPos] := criavar("D3_IDDCF")
	EndIf
	If SD3->(FieldPos("D3_OBSERVA")) <> 0
		aItensNew[nPosAEstq,++nUltPos] := criavar("D3_OBSERVA")
	EndIf
	
	If (ExistBlock("OX020AP"))
		aItensNew := ExecBlock("OX020AP", .f., .f., {aItensNew})
	EndIf

	lMsErroAuto := .F.
	MSExecAuto({|x| MATA261(x)},aItensNew)
	cFilAnt := cBkpFil
	If lMsErroAuto
		&& Cancela Gravacao
		Mostraerro()
		DisarmTransaction()
		Break
	EndIf                  
	DBSelectArea("VS3")
	DBSetOrder(2)
	if DBSeek(VDD->VDD_FILORC + VDD->VDD_NUMORC + VDD->VDD_GRUPO + VDD->VDD_CODITE)
		RecLock("VS3",.F.)
		VS3->VS3_DOCSDB := ""
		MsUnlock()           
	Endif	
Endif	
MsgStop(STR0027)

// PONTO DE ENTRADA AO REJEITAR O PEDIDO DE TRANSFERENCIA
If ExistBlock("OX020REJ")
	ExecBlock("OX020REJ",.f.,.f.)
EndIf


return .t.

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Funcao    � MenuDef  � Autor � Andre Luis Almeida / Luis Delorme � Data � 26/01/09 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descricao � Menu (AROTINA) - Pedidos de Transfer�ncia de Pe�as                     咳�
臼青陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Static Function MenuDef()
Local aRotina := {;
{ (STR0002) ,"AxPesqui"			, 0 , 1},; 	// Pesquisar
{ (STR0003) ,"OXA020A"			, 0 , 4},;	// Aceitar
{ (STR0004) ,"OXA020R"			, 0 , 4},;	// Rejeitar
{ ("Incluir Pedido") ,"OXA020I"	, 0 , 4},;	// Incluir Pedido
{ (STR0007) ,"OXA020LEG"		, 0 , 6}}	// Legenda
//
Return aRotina
/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Funcao    �VXA003LEG � Autor � Andre Luis Almeida / Luis Delorme � Data � 26/01/09 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descricao � Legenda - Pedidos de Transfer�ncia de Pe�as                            咳�
臼青陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function OXA020LEG()
Local aLegenda := {;
{'BR_AMARELO',STR0024},;
{'BR_VERDE',STR0006},;
{'BR_VERMELHO',STR0008},;
{'BR_PRETO',STR0009},;
{'BR_AZUL',STR0010}}
//
BrwLegenda(cCadastro,STR0007,aLegenda)
//
Return
/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Funcao    �OXA020VTES� Autor � Luis Delorme                     � Data � 26/01/09 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descricao � Verifica艫o do TES                                                     咳�
臼青陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function OXA020VTES()
if !Empty(cMV_PAR02)
	DBSelectArea("SF4")
	DBSetOrder(1)
	if DBSeek(xFilial("SF4")+cMV_PAR02)
		if SF4->F4_TIPO != "S" .or. SF4->F4_ESTOQUE != "S"      
			if SF4->F4_TIPO != "S"
				MsgStop(STR0030)
			Else
				MsgStop(STR0031)
			Endif	
			cMV_PAR01 := Space(TamSX3("VS3_OPER")[1])
			cMV_PAR02 := Space(TamSX3("F4_CODIGO")[1])
			oMV_PAR01:Refresh()
			oMV_PAR02:Refresh()
			return .f.
		endif                
		return .t.
	Else
		return .f.
	endif
Endif
return .t.

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Funcao    �OXA020TESE� Autor � Luis Delorme                     � Data � 26/01/09 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descricao � Verifica艫o do TES                                                     咳�
臼青陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function OXA020TESE()
if !Empty(cMV_PAR03)
	DBSelectArea("SF4")
	DBSetOrder(1)
	if DBSeek(xFilial("SF4")+cMV_PAR03)
		if SF4->F4_TIPO != "E" .or. SF4->F4_ESTOQUE != "S"      
			if SF4->F4_TIPO != "E"
				MsgStop(STR0032)
			Else
				MsgStop(STR0031)
			Endif	
			cMV_PAR01 := Space(TamSX3("VS3_OPER")[1])
			cMV_PAR03 := Space(TamSX3("F4_CODIGO")[1])
			oMV_PAR01:Refresh()
			oMV_PAR03:Refresh()
			return .f.
		endif                
		return .t.
	Else
		return .f.
	endif
Endif
return .t.

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Funcao    �OXA020LBOX� Autor � Luis Delorme                      � Data � 25/06/12 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descricao � Listbox para escolha das filiais que possuem pe�as excedentes          咳�
臼青陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function OXA020LBOX(cFilOrc,cCodGrupo,cCodIte, nQuant)

Local aFilAtu   := FWArrFilAtu()
Local aSM0      := FWAllFilial( aFilAtu[3] , aFilAtu[4] , aFilAtu[1] , .f. )
Local cB1LocPad := ""
Local nCont
Local cFilAntAnt := cFilAnt
aIte1 := {}

//Levanta as Filiais
For nCont := 1 to Len(aSM0)
	
	if aSM0[nCont] == cFilOrc
		loop
	endif
	
	cFilAnt := aSM0[nCont]
	
	dbSelectArea("SB1")
	dbSetOrder(7)
	dbSeek(xFilial("SB1")+cCodGrupo+cCodIte)
	cB1LocPad := FM_PRODSBZ(SB1->B1_COD,"SB1->B1_LOCPAD")
	//
	nSaldo := OX001SLDPC(xFilial("SB2")+SB1->B1_COD+cB1LocPad)
	SB2->(DBSetOrder(1))
	SB2->(DBSeek(xFilial("SB2")+SB1->B1_COD+cB1LocPad))
	NNR->(DBSetOrder(1))
	NNR->(dbSeek(xFilial("NNR")+cB1LocPad))
	aAdd(aIte1,{FWFilialName()+"/"+cFilAnt,TRANS(nSaldo,"@E 999,999,999.99"),TRANS(0,"@E 999,999,999.99"),cFilAnt, nSaldo})
Next

cFilAnt := cFilAntAnt
if Len(aIte1) == 0
	MsgStop(STR0015 ,STR0013)
	return .f.
endif
xOpca := 0
 
DEFINE MSDIALOG xDlg TITLE STR0018 From 10,10 to 36,79 of oMainWnd    //"Localizacao do Item"
//
@ 019,002 SAY (STR0020+Alltrim(cCodGrupo) + "/"+Alltrim(cCodIte)+ "    "+STR0019+ Alltrim(Transform(nQuant,"@E999.999,99"))) SIZE 200,08 OF xDlg PIXEL COLOR CLR_BLACK
@ 032, 001 LISTBOX oLbHeadx FIELDS HEADER	(STR0021),;  //"Localizacao"
(STR0022),;
(STR0023);
COLSIZES 80,120,48 SIZE 271,132 OF xDlg PIXEL ON DBLCLICK (OXA020TIK(nQuant))
//
oLbHeadx:SetArray(aIte1)
oLbHeadx:bLine := { || { aIte1[oLbHeadx:nAt,1] , FG_AlinVlrs(aIte1[oLbHeadx:nAt,2]) , FG_AlinVlrs(aIte1[oLbHeadx:nAt,3]) }}

DEFINE SBUTTON FROM 177, 213 TYPE 1 ACTION (xOpca := 1, xDlg:End()) ENABLE OF xDlg
DEFINE SBUTTON FROM 177, 240 TYPE 2 ACTION (xOpca := 2, xDlg:End()) ENABLE OF xDlg

//
ACTIVATE MSDIALOG xDlg CENTER 
//
aRet := {}
//
if xOpca == 2
	return aRet
endif                                         
//
for nCont := 1 to Len(aIte1)
	if Val(aIte1[nCont,3]) > 0
		aAdd(aRet,{ cCodGrupo, cCodite, aIte1[nCont,1], val(aIte1[nCont,3]), aIte1[nCont,4], aIte1[nCont,5] }) //adicionei o saldo aqui
	endif
next
return aRet
/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Funcao    �OXA020TIK � Autor � Luis Delorme                      � Data � 25/06/12 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descricao � Clique na listbox de pe�as excedentes da concessionaria                咳�
臼青陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function OXA020TIK(nQuant)

Local nCont 
MV_PAR01 := ""
aParamBox := {}
aAdd(aParamBox,{1,STR0016,0,"@E 999,999,999.99","MV_PAR01 > 0",,"",0,.T.})
If !(ParamBox(aParamBox,STR0006,,,,,,,,,.f.))
	return .f.
endif
//
nQuantAtu := 0
//
for nCont := 1 to Len(aIte1)
	if oLbHeadx:nAt != nCont
		nQuantAtu += val(aIte1[nCont,3])
	endif
next
if nQuantAtu + MV_PAR01 > nQuant 
	MsgStop(STR0017,STR0013)
	return .f.
endif
//
aIte1[oLbHeadx:nAt,3] := TRANS(MV_PAR01,"@E 999,999,999.99")
//
oLbHeadx:SetArray(aIte1)
oLbHeadx:bLine := { || { aIte1[oLbHeadx:nAt,1] , FG_AlinVlrs(aIte1[oLbHeadx:nAt,2]) , FG_AlinVlrs(aIte1[oLbHeadx:nAt,3]) }}
//
return .t.

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様曜様様様用様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �OXA020TPOP �Autor  �Thiago             � Data �  05/05/15   艮�
臼麺様様様様謡様様様様様擁様様様溶様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Desc.     � Validacao do Tipo de Opercao                               艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function OXA020TPOP()

If !Empty(cMV_PAR01)
	Existcpo("SX5","DJ"+cMV_PAR01)
EndIf

aSM0 := FWArrFilAtu(cEmpAnt,VDD->VDD_FILORC) // Filial Destino
cCGCDest := aSM0[18] // SM0->M0_CGC
dbSelectArea("SA1")
dbSetOrder(3)
dbSeek(xFilial("SA1")+cCGCDest) 

dbSelectArea("SA2")
dbSetOrder(3)
dbSeek(xFilial("SA2")+cCGCDest) 

dbSelectArea("SB1")
dbSetOrder(7)
dbSeek(xFilial("SB1")+VDD->VDD_GRUPO+VDD->VDD_CODITE)
cTESSai := MaTesInt(2,cMV_PAR01,SA1->A1_COD,SA1->A1_LOJA,"C",SB1->B1_COD)
cMV_PAR02 := cTESSai
cTESEnt := MaTesInt(1,cMV_PAR01,SA2->A2_COD,SA2->A2_LOJA,"F",SB1->B1_COD)
cMV_PAR03 := cTESEnt

oMV_PAR02:Refresh()
oMV_PAR03:Refresh()

Return(.t.)

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �  OXA020I �Autor  � Andre Luis Almeida � Data �  01/11/16   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Descricao � Transferencia de Pecas                                     艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function OXA020I()
Local nTam         := 0
Local aObjects     := {} , aPosObj := {} , aPosObjApon := {} , aInfo := {}
Local aSizeAut     := MsAdvSize(.T.)  // Tamanho Maximo da Janela (.t.=TOOLBAR,.f.=SEM TOOLBAR)
Local aFilAux      := FWArrFilAtu()
Local aTipTra      := X3CBOXAVET("VDD_TIPTRA","0") // {"0=Avulsa","1=Reposi艫o","2=Devolu艫o","3=Garantia"}
Private cTipTra    := "0"
Private aFiliais   := FWAllFilial( aFilAux[3] , aFilAux[4] , aFilAux[1] , .f. )
Private cFilOri    := cFilAnt
Private cFilDes    := cFilAnt
Private cVenTra    := FGX_USERVL( xFilial("VAI"),__cUserID,"VAI_CODVEN","?")
//
Private aVDD       := {}
//
FS_VDD("",0)
//
aObjects := {}
AAdd( aObjects, { 05 , 17 , .T. , .F. } )  // Cabecalho
AAdd( aObjects, { 05 , 20 , .T. , .F. } )  // Botoes
AAdd( aObjects, { 01 , 00 , .T. , .T. } )  // Listbox
aInfo := {aSizeAut[1] , aSizeAut[2] , aSizeAut[3] , aSizeAut[4] , 2 , 2 }
aPosObj := MsObjSize (aInfo, aObjects,.F.)
//
SetKey(VK_F5 , { || FS_VDD("I",oLbVDD:nAt)  } )
SetKey(VK_F6 , { || FS_VDD("A",oLbVDD:nAt)  } )
SetKey(VK_F7 , { || FS_VDD("E",oLbVDD:nAt)  } )
//SetKey(VK_F8 , { || FS_VDD("X",oLbVDD:nAt)  } )
//
DEFINE MSDIALOG oDlg1 TITLE "Transfer�ncia de Pe�as entre Filiais"  From aSizeAut[7],000 to aSizeAut[6],aSizeAut[5] of oMainWnd PIXEL
//
nTam := ( ( aPosObj[1,4] - 10 ) / 13 )
@ aPosObj[1,1],aPosObj[1,2] TO aPosObj[1,3],aPosObj[1,4] LABEL ("") OF oDlg1 PIXEL // Caixa Cabecalho
If len(aTipTra) > 0
	@ aPosObj[1,1]+005,aPosObj[1,2]+(nTam*00)+005 SAY "Tp.Transferencia: " SIZE 60,08 OF oDlg1 PIXEL COLOR CLR_BLUE
	@ aPosObj[1,1]+004,aPosObj[1,2]+(nTam*01)+005 MSCOMBOBOX oTipTra VAR cTipTra ITEMS aTipTra SIZE (nTam),08 OF oDlg1 PIXEL
Else
	cTipTra := ""
EndIf
@ aPosObj[1,1]+005,aPosObj[1,2]+(nTam*03)+005 SAY "Filial Origem: " SIZE 60,08 OF oDlg1 PIXEL COLOR CLR_BLUE
@ aPosObj[1,1]+004,aPosObj[1,2]+(nTam*04)+005 MSCOMBOBOX oFilOri VAR cFilOri ITEMS aFiliais VALID FS_TROCAFIL() SIZE (nTam*2),08 OF oDlg1 PIXEL
@ aPosObj[1,1]+005,aPosObj[1,2]+(nTam*07)+005 SAY "Filial Destino: " SIZE 60,08 OF oDlg1 PIXEL COLOR CLR_BLUE
@ aPosObj[1,1]+004,aPosObj[1,2]+(nTam*08)+005 MSCOMBOBOX oFilDes VAR cFilDes ITEMS aFiliais SIZE (nTam*2),08 OF oDlg1 PIXEL 
@ aPosObj[1,1]+005,aPosObj[1,2]+(nTam*11)+005 SAY "Vendedor: " SIZE 60,08 OF oDlg1 PIXEL COLOR CLR_BLUE
@ aPosObj[1,1]+004,aPosObj[1,2]+(nTam*12)+005 MSGET oVenTra VAR cVenTra PICTURE "@!" F3 "SA3" VALID FG_Seek("SA3","cVenTra",1,.f.) SIZE (nTam),08 OF oDlg1 PIXEL COLOR CLR_BLUE HASBUTTON
//
nTam := ( ( aPosObj[2,4] - 10 ) / 08 )
//nTam := ( ( aPosObj[2,4] - 10 ) / 11 )
@ aPosObj[2,1]+004,aPosObj[2,2]+(nTam*00)+005 BUTTON oBotInc PROMPT ("Incluir Pe�a"+" <F5>")          OF oDlg1 SIZE (nTam*2),12 PIXEL ACTION FS_VDD("I",oLbVDD:nAt)
@ aPosObj[2,1]+004,aPosObj[2,2]+(nTam*03)+005 BUTTON oBotAlt PROMPT ("Alterar Pe�a"+" <F6>")          OF oDlg1 SIZE (nTam*2),12 PIXEL ACTION FS_VDD("A",oLbVDD:nAt)
@ aPosObj[2,1]+004,aPosObj[2,2]+(nTam*06)+005 BUTTON oBotExc PROMPT ("Excluir Pe�a"+" <F7>")          OF oDlg1 SIZE (nTam*2),12 PIXEL ACTION FS_VDD("E",oLbVDD:nAt)
//@ aPosObj[2,1]+004,aPosObj[2,2]+(nTam*09)+005 BUTTON oBotAva PROMPT ("Incluir Avan�ado"+" <F8>") OF oDlg1 SIZE (nTam*2),12 PIXEL ACTION FS_VDD("X",oLbVDD:nAt)
//
oLbVDD := TWBrowse():New(aPosObj[3,1],aPosObj[3,2],aPosObj[3,4]-2,(aPosObj[3,3]-aPosObj[3,1]),,,,oDlg1,,,,,{ || FS_VDD("A",oLbVDD:nAt) },,,,,,,.F.,"",.T.,,.F.,,,)
oLbVDD:SetArray(aVDD)
oLbVDD:addColumn( TCColumn():New( "Grupo"          , { || aVDD[oLbVDD:nAt,01] }                                                ,,,, "LEFT" ,  35 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "C�digo"         , { || aVDD[oLbVDD:nAt,02] }                                                ,,,, "LEFT" ,  65 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Descri艫o"      , { || aVDD[oLbVDD:nAt,03] }                                                ,,,, "LEFT" , 125 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Qtd.Solicitada" , { || FG_AlinVlrs(Transform(aVDD[oLbVDD:nAt,04],X3PICTURE("VDD_QUANT"))) } ,,,, "RIGHT",  65 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Qtd.Estoque"    , { || FG_AlinVlrs(Transform(aVDD[oLbVDD:nAt,05],X3PICTURE("B2_QATU"))) }   ,,,, "RIGHT",  65 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Custo Unit�rio" , { || FG_AlinVlrs(Transform(aVDD[oLbVDD:nAt,06],X3PICTURE("B2_CM1"))) }    ,,,, "RIGHT",  65 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Classif.ABC"    , { || aVDD[oLbVDD:nAt,07] }                                                ,,,, "LEFT" ,  65 ,.F.,.F.,,,,.F.,) )
oLbVDD:addColumn( TCColumn():New( "Giro Estoque"   , { || FG_AlinVlrs(Transform(aVDD[oLbVDD:nAt,08],"@EZ 99999,999,999")) }    ,,,, "RIGHT",  65 ,.F.,.F.,,,,.F.,) )
//
ACTIVATE MSDIALOG oDlg1 ON INIT EnchoiceBar(oDlg1, {|| IIf(FS_OK(),oDlg1:End(),.f.) } , {|| oDlg1:End() },,)
//
SetKey(VK_F5 , Nil )
SetKey(VK_F6 , Nil )
SetKey(VK_F7 , Nil )
//SetKey(VK_F8 , Nil )
//
Return

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �  FS_VDD  �Autor  � Andre Luis Almeida � Data �  01/11/16   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Descricao � Inclusao/Alteracao/Exclusao do aVDD                        艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Function FS_VDD(cTp,nLinha)
Local cBkpFil    := cFilAnt
Local aRet       := {}
Local aParamBox  := {}
Local cGruIte    := space(TamSX3("B1_GRUPO")[1])
Local cCodIte    := space(TamSX3("B1_CODITE")[1])
Local cDesIte    := ""
Local nQtdSol    := 0
If len(aVDD) == 1 .and. aVDD[1,9] == 0
	If ( cTp == "I" .or. cTp == "A" )
		cTp := "I"
		aVDD := {}
	ElseIf cTp == "E"
		cTp := ""
	ElseIf cTp == "X"
		aVDD := {}
	EndIf
EndIf
If !Empty(cTp)
	//
	SetKey(VK_F5 , Nil )
	SetKey(VK_F6 , Nil )
	SetKey(VK_F7 , Nil )
//	SetKey(VK_F8 , Nil )
	//
	If cTp == "X" // Incluir Avancado 
		
	Else
		If cTp <> "I"
			cGruIte := aVDD[nLinha,1]
			cCodIte := aVDD[nLinha,2]
			cDesIte := aVDD[nLinha,3]
			nQtdSol := aVDD[nLinha,4]
		EndIf
		//
		cFilAnt := cFilOri
		//
		AADD(aParamBox,{1,RetTitle("B1_GRUPO") ,cGruIte,"@!",'VAZIO() .or. FG_Seek("SBM","MV_PAR01",1,.f.)'                  ,"SBM",IIf(cTp=="I",".T.",".F."),030,.F.}) // 1 - Grupo
		AADD(aParamBox,{1,RetTitle("B1_CODITE"),cCodIte,"@!",'VAZIO() .or. ( FG_POSSB1("MV_PAR02","SB1->B1_CODITE","MV_PAR01") .and. FG_Seek("SB1","MV_PAR01+MV_PAR02",7,.f.,"MV_PAR03","B1_DESC") )',"B11",IIf(cTp=="I",".T.",".F."),080,.F.}) // 2 - CodIte
		AADD(aParamBox,{1,RetTitle("B1_DESC")  ,cDesIte,"@!",''                                                              ,""   ,".F.",120,.F.}) // 3 - Descricao
		AADD(aParamBox,{1,"Qtd.Solicitada"     ,nQtdSol,X3PICTURE("VDD_QUANT"),"MV_PAR04>0" ,"",IIf(cTp<>"E",".T.",".F."),070,.T.}) // Qtd.Solicitada
		If ParamBox(aParamBox,"Transferencia de Pe�as",@aRet,,,,,,,,.f.) // Transferencia de Pe�as
			If cTp <> "E"
				If !Empty(aRet[2])
					SB1->(DbSetOrder(7))
					If SB1->(DbSeek(xFilial("SB1")+aRet[1]+aRet[2]))
						If cTp == "I"
							aAdd(aVDD,{SB1->B1_GRUPO,SB1->B1_CODITE,SB1->B1_DESC,0,0,0,"",0,SB1->(RecNo())})
							nLinha := len(aVDD)
						EndIf
						aVDD[nLinha,4] := aRet[4]
					EndIf
				EndIf	
				FS_TROCAFIL()
			Else
				aDel(aVDD,nLinha)
				aSize(aVDD,Len(aVDD)-1)
			EndIf	
		EndIf
		//
		cFilAnt := cBkpFil
		//
	EndIf
EndIf
If len(aVDD) == 0 
	aAdd(aVDD,{Space(TamSX3("B1_GRUPO")[1]),Space(TamSX3("B1_CODITE")[1]),"",0,0,0,"",0,0})
EndIf
If !Empty(cTp)
	//
	oLbVDD:nAt := 1
	oLbVDD:SetArray(aVDD)
	oLbVDD:Refresh()
	//
	SetKey(VK_F5 , { || FS_VDD("I",oLbVDD:nAt)  } )
	SetKey(VK_F6 , { || FS_VDD("A",oLbVDD:nAt)  } )
	SetKey(VK_F7 , { || FS_VDD("E",oLbVDD:nAt)  } )
//	SetKey(VK_F8 , { || FS_VDD("X",oLbVDD:nAt)  } )
	//
EndIf
Return

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様曜様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �FS_TROCAFIL�Autor � Andre Luis Almeida � Data �  01/11/16   艮�
臼麺様様様様謡様様様様様擁様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Descricao � Troca a Filial de Origem                                   艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Static Function FS_TROCAFIL()
Local lRet    := .t.
Local ni      := 0
Local cBkpFil := cFilAnt
cFilAnt := cFilOri
For ni := 1 to len(aVDD)
	If !Empty(aVDD[ni,1]+aVDD[ni,2])
		SB1->(DbSetOrder(7))
		If SB1->(DbSeek(xFilial("SB1")+aVDD[ni,1]+aVDD[ni,2]))
			aVDD[ni,3] := SB1->B1_DESC
			SB2->(DbSetOrder(1))
			SB2->(DbSeek(xFilial("SB2")+SB1->B1_COD+FM_PRODSBZ(SB1->B1_COD,"SB1->B1_LOCPAD")))
			aVDD[ni,5] := SB2->B2_QATU
			aVDD[ni,6] := SB2->B2_CM1
			SB3->(DbSetOrder(1))
			SB3->(DbSeek(xFilial("SB3")+SB1->B1_COD))
			aVDD[ni,7] := SB3->B3_CLASSE
			aVDD[ni,8] := SB3->B3_MEDIA
			aVDD[ni,9] := SB1->(RecNo())
		Else
			MsgAlert("Pe�a n�o encontrada na Filial de Origem."+CHR(13)+CHR(10)+CHR(13)+CHR(10)+aVDD[ni,1]+" "+aVDD[ni,2],"Aten艫o")
			lRet := .f.
	    EndIf
    EndIf
Next
oLbVDD:nAt := 1
oLbVDD:SetArray(aVDD)
oLbVDD:Refresh()
cFilAnt := cBkpFil
Return(lRet)

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �  FS_OK   �Autor  � Andre Luis Almeida � Data �  01/11/16   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Descricao � OK na tela de Incluir Pedido ( VDD )                       艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Static Function FS_OK()
Local ni   := 0
Local aSM0 := {}
Local lVDD_TIPTRA := ( VDD->(FieldPos("VDD_TIPTRA")) > 0 )
Local lVDD_VENTRA := ( VDD->(FieldPos("VDD_VENTRA")) > 0 )
//
If len(aVDD) == 1 .and. aVDD[1,9] == 0
	MsgStop("N�o h� itens para realizar a transfer�ncia.","Aten艫o")
	Return .f.
Endif
//
If cFilOri == cFilDes
	MsgStop("Filial de Origem n�o pode ser a mesma da Filial de Destino!","Aten艫o")
	Return .f.
EndIf
//
aSM0 := FWArrFilAtu(cEmpAnt,cFilOri) // Filial Origem
dbSelectArea("SA2")
dbSetOrder(3)
If !dbSeek(xFilial("SA2")+aSM0[18])
	MsgStop("Filial de Origem n�o encontrada como Fornecedor!"+CHR(13)+CHR(10)+CHR(13)+CHR(10)+AllTrim(RetTitle("A2_CGC"))+": "+Transform(aSM0[18],x3Picture("A2_CGC")),"Aten艫o")
	Return .f.
EndIf
aSM0 := FWArrFilAtu(cEmpAnt,cFilDes) // Filial Destino
dbSelectArea("SA1")
dbSetOrder(3)
If !dbSeek(xFilial("SA1")+aSM0[18])
	MsgStop("Filial de Destino n�o encontrada como Cliente!"+CHR(13)+CHR(10)+CHR(13)+CHR(10)+AllTrim(RetTitle("A1_CGC"))+": "+Transform(aSM0[18],x3Picture("A1_CGC")),"Aten艫o")
	Return .f.
EndIf
///////////////////////////////////
// Buscar o ultimo nro de Pedido //
///////////////////////////////////
ni := ( TamSX3("VDD_NUMORC")[1] - 1 )
cNumOrc := FM_SQL("SELECT MAX(VDD_NUMORC) FROM "+RetSqlName("VDD")+" WHERE VDD_FILIAL='"+xFilial("VDD")+"' AND VDD_NUMORC LIKE 'P%'")
cNumOrc := "P"+strzero(val(right(cNumOrc,ni))+1,ni)
//
For ni := 1 to len(aVDD)
	dbSelectArea("VDD")
	RecLock("VDD",.t.)
		VDD->VDD_FILIAL := xFilial("VDD")
		VDD->VDD_CODIGO := GetSXENum("VDD","VDD_CODIGO")
		ConfirmSX8()
		VDD->VDD_FILORC := cFilDes
		VDD->VDD_NUMORC := cNumOrc
		VDD->VDD_GRUPO  := aVDD[ni,1]
		VDD->VDD_CODITE := aVDD[ni,2]
		VDD->VDD_QUANT  := aVDD[ni,4]
		VDD->VDD_FILPED := cFilOri
		VDD->VDD_STATUS := "S"
		If lVDD_TIPTRA
			VDD->VDD_TIPTRA := cTipTra
		EndIf
		If lVDD_VENTRA
			VDD->VDD_VENTRA := cVenTra
		EndIf
	MsUnlock()
Next
MsgInfo("Pedido de Transferencia criado com sucesso!","Aten艫o")
Return .t.

/*----------------------------------------------------
 Suavizar a nova verifica艫o de integra艫o com o WMS
------------------------------------------------------*/
Static Function a261IntWMS(cProduto)
Default cProduto := ""
	If FindFunction("IntWMS")
		Return IntWMS(cProduto)
	Else
		Return IntDL(cProduto)
	EndIf
Return
