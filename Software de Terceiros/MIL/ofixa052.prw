#include "protheus.ch"
#include "OFIXA052.ch"
#include "fileio.ch"

/*
===============================================================================
###############################################################################
##+----------+------------+-------+-----------------------+------+----------+##
##|Fun��o    | OFIXA052   | Autor |  Luis Delorme         | Data | 30/04/12 |##
##+----------+------------+-------+-----------------------+------+----------+##
##|Descri��o | Gera��o de Arquivos Hist�ricos do DEF (VDB/VDC)              |##
##|          |                                                              |##
##+----------+--------------------------------------------------------------+##
##|Uso       | Veiculos                                                     |##
##+----------+--------------------------------------------------------------+##
###############################################################################
===============================================================================
*/
Function OFIXA052()
// Variaveis da ParamBox
Local aSay    := {}
Local aButton := {}
Local nOpc    := 0
//
Local cTitulo := STR0001
Local cDesc1  := STR0002
Local cDesc2  := " "
Local cDesc3  := " "
//
Private cPerg := "OXA052"
Private dPARDataFinal
Private cPARCodDef
//
aAdd( aSay, cDesc1 )
aAdd( aSay, cDesc2 )
aAdd( aSay, cDesc3 )
//
aAdd( aButton, { 5, .T., {|| Pergunte(cPerg,.T. )    }} )
aAdd( aButton, { 1, .T., {|| nOpc := 1, FechaBatch() }} )
aAdd( aButton, { 2, .T., {|| FechaBatch()            }} )
//
// Perguntas 
// Codigo DEF - C - TamSX3("VD7_CODDEF")[1]
// Data Final - D 
Pergunte(cPerg,.f.)
//
FormBatch( cTitulo, aSay, aButton )
//
If nOpc <> 1
	Return
Endif
//#############################################################################
//# Chama a rotina de importacao do retorno do aviso de vendas GM             #
//#############################################################################
oProcTTP := MsNewProcess():New({ |lEnd| OX052RunProc() }," ","",.f.)
oProcTTP:Activate()
//
Return
/*
===============================================================================
###############################################################################
##+----------+------------+-------+-----------------------+------+----------+##
##|Fun��o    | RunProc    | Autor |  Luis Delorme         | Data | 30/04/12 |##
##+----------+------------+-------+-----------------------+------+----------+##
##|Descri��o | Gera��o do Hist�rico do DEF                                  |##
##+----------+--------------------------------------------------------------+##
##|Uso       | Veiculos                                                     |##
##+----------+--------------------------------------------------------------+##
###############################################################################
===============================================================================
*/
Static Function OX052RunProc(lEnd, cPCodDef, cPCodCon, dPFechamento, lGravarDados)
	//
	Local nCntFor
	Local lVisualDados := .f.
	//
	Default cPCodDef := MV_PAR01
	Default cPCodCon := ""
	Default dPFechamento := MV_PAR02
	Default lGravarDados := .t.
	//
	Private oVisualLog := OFVisualizaDados():New("OX052DETCALC", STR0018) // "Detalhe do C�lculo"
	//
	Private cPARCodDef    := cPCodDef
	Private dPARDataFinal := dPFechamento
	Private dOXA052 := dPARDataFinal // Legado
	//
	Private dDataFim := dPARDataFinal
	Private dDataIni := ctod("01/"+STRZERO(month(dDataFim),2)+"/"+Right(STRZERO(Year(dDataFim),4),2))
	//
	Private lOA052VAL := ExistBlock("OA052VAL")
	Private lOA052CC1 := ExistBlock("OA052CC1")
	//
	Private lVD9CCUSTA := VD9->(FieldPos("VD9_CCUSTA")) > 0
	Private lVDECCUSTO := VDE->(FieldPos("VDE_CCUSTO")) > 0
	//
//
	lVisualDados := ! lGravarDados

	DBSelectArea("VD8")
	DBSetorder(1)
	If ! DBSeek(xFilial("VD8")+cPARCodDef+cEmpAnt+cFilAnt)
		ShowHelpDlg(;
			STR0032 ,; // "Filial n�o encontrada"
			{ STR0033 },5,; // "Filial n�o encontrada no cadastro do DEF"
			{ STR0034 },5) // "Verifique o cadastro do DEF"
		Return .f.
	EndIf

	// ===========================================================
	// Em caso de reprocessamento deve apagar o hist�rico anterior
	// ===========================================================
	If lGravarDados
		If OX052005_DeletaProc(cPARCodDef, dPARDataFinal) == .f.
			Return .f.
		EndIf
	EndIf

	aRetDEF := {}
	if !Empty(VD8->VD8_FUNGER)
		cStrMcro = "aRetDEF := U_"+Alltrim(VD8->VD8_FUNGER)+"('"+Alltrim(cPARCodDef)+"','"+dtos(dPARDataFinal)+"',"+VD7->VD7_FREQUE+")"
		&(cStrMcro)
	endif

	If lGravarDados
		oProcTTP:IncRegua1(STR0016)

		cQuery := " SELECT COUNT(*) CONTAGEM"
		cQuery += " FROM " + RetSqlName("VD9") + " VD9"
		cQuery += " WHERE VD9_CODDEF='" + cPARCodDef + "'"
		cQuery += " AND VD9_FILIAL = '" + xFilial("VD9") + "'"
		cQuery += " AND VD9.D_E_L_E_T_= ' '"
		oProcTTP:SetRegua2(FM_SQL(cQuery))
	EndIf

	If lVisualDados
		OX052004_VisCabec()
	EndIf

	nCCount:= 0 // armazena a contagem de campos processados
	//
	DBSelectArea("VD9")
	DBSeek(xFilial("VD9")+cPARCodDef)
	//
	lAbortou := .f.
	Private aFaltantes := {} // armazena os campos DEF que ainda n�o foram calculados pois dependem de outros c�lculos
	Private aFaltInfo  := {} // armazena os campos DEF que ainda n�o foram calculados pois dependem de outros c�lculos
	Private aResolvidos := {} // armazena os campos que foram calculados

	DBSelectArea("VDA")
	DbSetOrder(1)
	DBSelectArea("VDC")
	DBSetOrder(1) // VDC_FILIAL+VDC_CODDEF+DTOS(VDC_DATA)+VDC_CODCON
	DBSelectArea("VDE")
	DBSetOrder(1)
	DBSelectArea("CT1")
	DBSetOrder(1)
	DBSelectArea("CTT")
	DBSetOrder(1)

	lItemCtComoFilial := .f.
	If VD7->(FieldPos("VD7_CALICT")) <> 0
		If VD7->(DBSeek(xFilial("VD7")+cPARCodDef))
			lItemCtComoFilial := (VD7->(VD7_CALICT) == "1")
		EndIf
	EndIf

	lPrim := .t.
	lPrimFora := .t.

	BEGIN TRANSACTION
		// ======================================================
		// Enquanto a contagem for zero ou ainda existirem campos
		// faltantes executamos outro la�o para c�lculo
		// ======================================================
		while nCCount == 0 .or. Len(aFaltantes) > 0 .or. lPrimFora

			if !lPrim
				lPrimFora := .f.
			endif

			nFalAntes := Len(aFaltantes) 	// armazena a quantidade de campos faltantes antes do processamento

			VD9->(DBSeek(xFilial("VD9") + cPARCodDef + IIf( lPrim , cPCodCon , "" ) ))

			// =============================
			// Executa la�o calculando o DEF
			// =============================
			while !VD9->(eof()) .and. xFilial("VD9") == VD9->VD9_FILIAL .and. cPARCodDef == VD9->VD9_CODDEF

				if (lPrim .and. VD9->VD9_TIPO $ "25") .or. ( !lPrim .and. !(VD9->VD9_TIPO $ "25") )
					VD9->(DBSkip())
					loop
				endif
				// Pula contas inativas pelo cadastro da conta
				if VD9->VD9_ATIVO == "0"
					VD9->(DBSkip())
					loop
				endif
				if !lPrim .and. aScan(aResolvidos, {|x| Alltrim(x[1]) == Alltrim(VD9->VD9_CPODEF)}) > 0
					VD9->(DBSkip())
					loop
				endif
				If lPrim .and. !Empty(cPCodCon) .and. cPCodCon <> VD9->VD9_CODCON
					VD9->(DBSkip())
					Exit
				endif

				// verifica se a conta existe para a filial
				if VDA->(DBSeek(xFilial("VDA") + cPARCodDef + VD9->VD9_CODCON + cEmpAnt + cFilAnt ))
					// Pula contas inativas para a filial em quest�o
					nCCount ++ // Incrementa o contador
					nVal := 0  // Valor da conta
					//
					Do Case
					Case VD9->VD9_TIPO == "5" // Se a conta representa um acumulado de outra devemos calcular
						If OX052002_CalculaTipo5() == .f.
							VD9->(DBSkip())
							loop
						EndIf

					Case VD9->VD9_TIPO == "2" // Se a conta representa uma express�o devemos calcul�-la
						If OX052003_CalculaTipo2() == .f.
							VD9->(DBSkip())
							loop
						EndIf

					Case VD9->VD9_TIPO $ "34" // Se a conta � tipo 3, pega da contabilidade (CT7)
						OX052001_CalculaCCTERP(lVisualDados)
					EndCase

					if !Empty(VD9->VD9_CODFOR) .and. VD9->VD9_TIPO == "1" // se a conta � formula, basta calcular...
						nPos = VAL(VD9->VD9_CODFOR)
						if nPos == 0
							nVal := 0
						elseif nPos > Len(aRetDef)
							MsgInfo(STR0013+" ("+Alltrim(VD9->VD9_CODFOR)+").",STR0014)
						else
							nVal := aRetDef[nPos]
						endif
					endif

					IF VD9->VD9_TIPO=="4"
						if nVal < 0
							nVal := 0
						endif
					endif

					// cria registro com o valor
					If lGravarDados
						DBSelectArea("VDC")
						reclock("VDC",.t.)
						VDC->VDC_FILIAL := xFilial("VDC")
						VDC->VDC_CODDEF := cPARCodDef
						VDC->VDC_CODCON := VD9->VD9_CODCON
						VDC->VDC_DATA   := dPARDataFinal
						VDC->VDC_VALOR  := nVal
						msunlock()
						oProcTTP:IncRegua2()
					EndIf

					// remove a conta dos faltantes e insere nos resolvidos
					nPos := aScan(aFaltantes,VD9->VD9_CPODEF)
					if nPos > 0
						aDel(aFaltantes,nPos)
						aSize(aFaltantes, Len(aFaltantes)-1)
						aDel(aFaltInfo,nPos)
						aSize(aFaltInfo, Len(aFaltInfo)-1)
					endif
					aAdd(aResolvidos,{VD9->VD9_CPODEF, IIF(nVal==NIL,0,nVal) })
				endif

				VD9->(DBSkip())
			enddo
			//
			lPrim := .f.
			// se o processamento foi feito e as contas faltantes permanecem as mesmas,
			// isso s� pode indicar refer�ncia circular e � imposs�vel calcular tudo
			if nFalAntes > 0 .and. Len(aFaltantes) == nFalAntes
				// mostra a mensagem e aborta
				MsgStop(STR0012 + " : "+STRZERO(Len(aFaltantes),4))
				If lGravarDados
					DisarmTransaction()
				EndIf
				lAbortou := .t.
				IF (nHandle := FCREATE("OXA051DUMP.TXT", FC_NORMAL)) != -1
					for nCntFor = 1 to Len(aFaltInfo)
						FWRITE(nHandle, aFaltInfo[nCntFor]+CHR(13)+CHR(10))
					next
					FCLOSE(nHandle)
				ENDIF
				exit
			endif
			// Se ele rodou e n�o fez nada, aborta
			if nCCount == 0 .or. Len(aResolvidos) == 0
				nCCount := 0
				exit
			endif
		enddo
		If lGravarDados
			// grava cabe�alho
			if nCCount > 0 .and. !lAbortou
				DBSelectArea("VD7")
				DBSeek(xFilial("VD7")+cPARCodDef)
				DBSelectArea("VDB")
				reclock("VDB",.t.)
				VDB->VDB_FILIAL := xFilial("VDB")
				VDB->VDB_CODDEF := cPARCodDef
				VDB->VDB_DATA   := dPARDataFinal
				VDB->VDB_FREQUE := VD7->VD7_FREQUE
				msunlock()
			endif
		EndIf

	END TRANSACTION

	If lGravarDados
		if nCCount > 0 .and. !lAbortou
			MsgInfo(STR0006)
		endif
	Else
		If oVisualLog:HasData()
			oVisualLog:Total( {;
					{ 3 , STR0019 } ; // TOTAL
				})
			oVisualLog:Activate()
		EndIf
	EndIf
Return
/*
===============================================================================
###############################################################################
##+----------+------------+-------+-----------------------+------+----------+##
##|Fun��o    |FMX_CALXPDEF| Autor |  Luis Delorme         | Data | 30/04/12 |##
##+----------+------------+-------+-----------------------+------+----------+##
##|Descri��o | Montagem da Pergunte                                         |##
##+----------+--------------------------------------------------------------+##
##|Uso       | Veiculos                                                     |##
##+----------+--------------------------------------------------------------+##
###############################################################################
===============================================================================
*/
Function FMX_CALXPDEF(cCodDEF,cExprDEF,aResolv,nValCon)
Local nCntFor

Local bBlock:=ErrorBlock(), bErro := ErrorBlock( { |e| FS_CheckBug(e,cCodDEF,cExprDEF) } )

// remove espa�os em branco de tudo
cExprDEF := Alltrim(cExprDEF)
nPos = AT(" ",cExprDEF)
while nPos > 0
	cExprDEF := STUFF(cExprDEF,nPos,1,"")
	nPos = AT(" ",cExprDEF)
enddo
//
// inicia a analise l�xica
//
nPC := 1
cPalavra := ""
aPalavras := {}
//
while nPC <= Len(cExprDEF)
	cLetra := subs(cExprDEF,nPc,1)
	if cLetra $ "()+-*/"
		if Empty(cPalavra)
			aAdd(aPalavras,cLetra)
		else
			if Left(cPalavra,1)=="@"
				cConta := subs(cPalavra,2)
				nPos := aScan(aResolv, {|x| Alltrim(x[1]) == Alltrim(cConta)})
				if nPos == 0
					return STR0015+ Alltrim(cConta)
				endif
			endif
			aAdd(aPalavras,cPalavra)
			aAdd(aPalavras,cLetra)
			cPalavra := ""
		endif
	else
		cPalavra = cPalavra + cLetra
	endif
	nPc++
enddo
if Left(cPalavra,1)=="@"
	cConta := subs(cPalavra,2)
	nPos := aScan(aResolv, {|x| Alltrim(x[1]) == Alltrim(cConta)})
	if nPos == 0
		return STR0015 + Alltrim(cConta)
	endif
endif
aAdd(aPalavras,cPalavra)
//
// Verifica e substitui as contas pelos valores
//
cExpr := ""
for nCntFor := 1 to Len(aPalavras)
	if Left(aPalavras[nCntFor],1)=="@"
		cConta := subs(aPalavras[nCntFor],2)
		nPos := aScan(aResolv, {|x| Alltrim(x[1]) == Alltrim(cConta)})
		aPalavras[nCntFor] := "("+Alltrim(str(aResolv[nPos,2]))+")"
	endif
	cExpr := Alltrim(cExpr + " " + aPalavras[nCntFor])
next
nVal := &(cExpr)
//
ErrorBlock(bBlock)
//
return ""
/*
===============================================================================
###############################################################################
##+----------+------------+-------+-----------------------+------+----------+##
##|Fun��o    | OX052VP    | Autor |  Luis Delorme         | Data | 30/04/12 |##
##+----------+------------+-------+-----------------------+------+----------+##
##|Descri��o | Validacao da Pergunte                                        |##
##+----------+--------------------------------------------------------------+##
##|Uso       | Veiculos                                                     |##
##+----------+--------------------------------------------------------------+##
###############################################################################
===============================================================================
*/
Function OX052VP(nParVP)

if nParVP == 1
	DBSelectArea("VD7")
	if DBSeek(xFilial("VD7")+MV_PAR01) .and. VD7->VD7_ATIVO == "0"
		MsgStop(STR0009)
		return .f.
	endif
	DBSelectArea("VD8")
	if !DBSeek(xFilial("VD8")+MV_PAR01+cEmpAnt+xFilial("VDB"))
		MsgStop(STR0010)
		return .f.
	endif
elseif nParVP == 2
	if Month(MV_PAR02) == Month(MV_PAR02+1)
		MsgStop(STR0011)
		return .f.
	endif
endif

return .t.


Function FS_CheckBug(e,cCodFormul,cDesFormul)
Local lRet := .t.
if e:gencode > 0
	Aviso(cCodFormul,cDesFormul+CHR(13)+CHR(10)+e:errorstack,{""},3)
	lRet := .f.
Endif
Return(lRet)


/*/{Protheus.doc} OX052002_CalculaTipo5
Calculo quando campo VD9_TIPO = 5
@author Rubens
@since 02/10/2018
@version 1.0

@type function
/*/
Function OX052002_CalculaTipo5()
	Local nCntFor
	cCodConAc := FM_SQL(;
		"SELECT VD9.VD9_CODCON " + ;
		 " FROM " + RetSQLName("VD9") + " VD9 " +;
		" WHERE VD9.VD9_FILIAL = '" + xFilial("VD9") + "' " +;
		  " AND VD9.VD9_CPODEF = '" + VD9->VD9_ACUMUL + "' " +;
		  " AND VD9.D_E_L_E_T_=' '")

	nPos := aScan(aResolvidos, {|x| Alltrim(x[1]) == Alltrim(VD9->VD9_ACUMUL)})
	if nPos > 0
		nVal += aResolvidos[nPos,2]
		for nCntFor := Month(dPARDataFinal) to 1 step -1
			dDataSeek := ctod("01/"+ STRZERO(nCntFor,2)+"/"+Right(STRZERO(Year(dPARDataFinal),4),2))-1
			if VDC->(DBSeek(xFilial("VDC")+cPARCodDef+dtos(dDataSeek)+Alltrim(cCodConAc)))
				nVal += VDC->VDC_VALOR
			endif
		next
	else
		nPos := aScan(aFaltantes,VD9->VD9_CPODEF) // verifica se essa conta � faltante para c�lculo
		// adiciona a conta como faltante
		if nPos == 0
			aAdd(aFaltantes,VD9->VD9_CPODEF)
			aAdd(aFaltInfo,VD9->VD9_CPODEF+STR0017+VD9->VD9_ACUMUL)
		else
			aFaltInfo[nPos] := VD9->VD9_CPODEF+STR0017+VD9->VD9_ACUMUL
		endif
		Return .f.
	endif
Return .t.

/*/{Protheus.doc} OX052003_CalculaTipo2
Calculo quando campo VD9_TIPO = 2
@author Rubens
@since 02/10/2018
@version 1.0

@type function
/*/
Function OX052003_CalculaTipo2()
	// se a fun��o FMX_CALXPDEF retornar uma string, a conta � faltante, caso contr�rio o resultado est� em nVal
	cResFalt := FMX_CALXPDEF(VD9->VD9_CODCON,VD9->VD9_EXPRES,aResolvidos,nVal)
	if !Empty(cResFalt)
		nPos := aScan(aFaltantes,VD9->VD9_CPODEF) // verifica se essa conta � faltante para c�lculo
		// adiciona a conta como faltante
		if nPos == 0
			aAdd(aFaltantes,VD9->VD9_CPODEF)
			aAdd(aFaltInfo,VD9->VD9_CPODEF+cResFalt)
		else
			aFaltInfo[nPos] := VD9->VD9_CPODEF+cResFalt
		endif
		Return .f.
	endif

Return .t.

/*/{Protheus.doc} OX052001_CalculaCCTERP
Calculo utilizando valores da Contabilidade Gerencial
@author Rubens
@since 02/10/2018
@version 1.0
@param lVisualDados, logical, descricao
@type function
/*/
Function OX052001_CalculaCCTERP(lVisualDados)
	Local aBkpCenCus

	aCenCus := {}
	If !Empty(VD9->VD9_CCUSTS)
		aCenCus := OX052006_GeraCentroCustoAProcessar()
	EndIf

	// PONTO DE ENTRADA PARA ALTERACAO DO VETOR
	If lOA052CC1
		ExecBlock("OA052CC1",.f.,.f.)
	EndIf
	//

//	If lOA052VAL
//		nVal := ExecBlock("OA052VAL",.f.,.f., { lVisualDados })
//	Else

		VDE->(DBSeek(xFilial("VDE")+VD9->VD9_CODDEF+VD9->VD9_CODCON))
		while !VDE->(eof()) .and. xFilial("VDE") == VDE->VDE_FILIAL .and. VD9->VD9_CODDEF == VDE->VDE_CODDEF .and. VD9->VD9_CODCON == VDE->VDE_CODCON

			//
			if lVDECCUSTO .and. !Empty(VDE->VDE_CCUSTO)
				aBkpCenCus := aClone(aCenCus)
				aCenCus := { VDE->VDE_CCUSTO }
			endif

			OX052012_PercorreCT1( VDE->VDE_CCTERP , lVisualDados )

			if lVDECCUSTO .and. !Empty(VDE->VDE_CCUSTO)
				aCenCus := aClone(aBkpCenCus)
			endif

			VDE->(DBSkip())
		enddo
//	endif
Return


/*/{Protheus.doc} OX052004_VisCabec
Adiciona os campos na janela para visualiza��o detalhada do calculo
@author Rubens
@since 02/10/2018
@version 1.0

@type function
/*/
Function OX052004_VisCabec()

	oVisualLog:AddHeaderMGET({;
		{'TIPO'    , "D"             },;
		{'TAMANHO' , 10              },;
		{'CAMPO'   , 'dOXA052Inicio' },;
		{'TITULO'  , STR0020         },; // 'Data Inicial'
		{'VALOR'   , dDataIni        } ;
	})

	oVisualLog:AddHeaderMGET({;
		{'TIPO'    , "D"            },;
		{'TAMANHO' , 10             },;
		{'CAMPO'   , 'dOXA052Final' },;
		{'TITULO'  , STR0021        },; // 'Data Final'
		{'VALOR'   , dDataFim       } ;
	})

	oVisualLog:AddColumn( { { "TITULO" , STR0022                } , { "TAMANHO" , 6 } } ) // "Id Conta"
	oVisualLog:AddColumn( { { "TITULO" , STR0023                } , { "TAMANHO" , 10 } } ) // "Tipo"
	oVisualLog:AddColumn( { { "TITULO" , STR0024                } , { "TAMANHO" , TamSX3("CT1_CONTA")[1] } } ) // "Conta"
	oVisualLog:AddColumn( { { "TITULO" , STR0025                } , { "TAMANHO" , TamSX3("CTT_CUSTO")[1] } } ) // "Centro de Custo"
	oVisualLog:AddColumn( { { "TITULO" , STR0026                } , { "TAMANHO" , TamSX3("CTD_ITEM")[1]  } } ) // "Item Conta"
	oVisualLog:AddColumn( { { "TITULO" , STR0027                } , { "TIPO" , "N" } , { "TAMANHO" , 17 } , { "DECIMAL",2 } , { "PICTURE" , "@Z 99,999,999,999,999.99" } , { "TOTALIZADOR", .t. }} ) // "Saldo"
	oVisualLog:AddColumn( { { "TITULO" , STR0028                } , { "TIPO" , "N" } , { "TAMANHO" , 17 } , { "DECIMAL",2 } , { "PICTURE" , "@Z 99,999,999,999,999.99" } } ) // "Saldo Inicio"
	oVisualLog:AddColumn( { { "TITULO" , STR0029                } , { "TIPO" , "N" } , { "TAMANHO" , 17 } , { "DECIMAL",2 } , { "PICTURE" , "@Z 99,999,999,999,999.99" } } ) // "Saldo Final"
	oVisualLog:AddColumn( { { "TITULO" , RetTitle("CT1_NORMAL") } , { "TAMANHO" , 10 } } )
	oVisualLog:AddColumn( { { "TITULO" , STR0030                } , { "TAMANHO" , 1 } } ) // "Opera��o"
	oVisualLog:AddColumn( { { "TITULO" , STR0031                } , { "TAMANHO" , 2 } } ) // "Tipo Saldo"

Return


/*/{Protheus.doc} OX052LogCalculo
Executa processamento do calculo para uma determinada linha do DEF para visualizacao detalhada / conferencia do calculo.
@author Rubens
@since 02/10/2018
@version 1.0
@param cCodDef, characters, descricao
@param cCodCon, characters, descricao
@param dDataFechamento, date, descricao
@type function
/*/
Function OX052LogCalculo(cCodDef, cCodCon, dDataFechamento)
	OX052RunProc(, cCodDef, cCodCon, dDataFechamento, .f.)
Return

/*/{Protheus.doc} OX052005_DeletaProc
Deleta processamento gravado na base de dados.
@author Rubens
@since 02/10/2018
@version 1.0
@param cAuxCodDef, characters, descricao
@param dAuxDataFinal, date, descricao
@type function
/*/
Function OX052005_DeletaProc( cAuxCodDef, dAuxDataFinal )

	DBSelectArea("VDB")
	If DBSeek(xFilial("VDB")+cAuxCodDef+dtos(dAuxDataFinal)) == .f.
		Return .t.
	EndIf
	If MsgYesNo(STR0005) == .f.
		return .f.
	EndIf

	reclock("VDB",.f.,.t.)
	dbdelete()
	msunlock()

	DBSelectArea("VDC")
	DBSetOrder(1)
	DBSeek(xFilial("VDC")+cAuxCodDef+dtos(dAuxDataFinal))
	while !eof() .and. xFilial("VDC") == VDC->VDC_FILIAL .and. cAuxCodDef == VDC->VDC_CODDEF .and. dtos(dAuxDataFinal) == dtos(VDC->VDC_DATA)
		reclock("VDC",.f.,.t.)
		dbdelete()
		msunlock()
		DBSkip()
	enddo

Return .t.

/*/{Protheus.doc} OX052006_GeraCentroCustoAProcessar
Gera matriz com Centro de Custo utilizado no processamento do DEF
@author Rubens
@since 02/10/2018
@version 1.0
@return aAuxRetorno , Matriz contendo os Centros de Custos utilizados no calculo

@type function
/*/
Static Function OX052006_GeraCentroCustoAProcessar()

	Local aAuxCenCus := {}
	Local aAuxRetorno := {}
//	Local nCntFor
	Local nLenCCusto
	Local nPosCampos
	Local aCampos

	If lVD9CCUSTA
		aCampos := {;
			"VD9->VD9_CCUSTS",;
			"VD9->VD9_CCUSTA",;
			"VD9->VD9_CCUSTB",;
			"VD9->VD9_CCUSTC" }
	Else
		aCampos := {"VD9->VD9_CCUSTS"}
	EndIf

	For nPosCampos := 1 to Len(aCampos)

		aAuxCenCus := OX052007_SeparaTexto(&(aCampos[nPosCampos]))
		If Len(aAuxCenCus) > 0
			nLenCCusto := Len(aAuxRetorno)
			ASIZE( aAuxRetorno , nLenCCusto + Len(aAuxCenCus) )
			ACOPY( aAuxCenCus , aAuxRetorno , , , nLenCCusto + 1 )
		EndIf
	Next nPosCampos

Return aClone(aAuxRetorno)


/*/{Protheus.doc} OX052007_SeparaTexto
Transforma um texto em uma array. Utiliza o separador passado como parametro para quebrar o texto
@author Rubens
@since 02/10/2018
@version 1.0
@return aRetorno, array com os valores que foram separados
@param cTexto, characters, descricao
@param cSeparador, characters, descricao
@type function
/*/
Static Function OX052007_SeparaTexto(cTexto, cSeparador)

	Local aRetorno := {}

	Default cSeparador := ","

	cTexto := AllTrim(cTexto)
	If Empty(cTexto)
		Return {}
	EndIf

	aRetorno := StrTokArr(cTexto,",")

Return aRetorno

/*/{Protheus.doc} OX052008_CalculaCentroCusto
Calculo por Centro de Custo
@author Rubens
@since 02/10/2018
@version 1.0
@return nRetorno, Valor calculado
@param cParConta, characters, Codigo da Conta
@param cParCenCusto, characters, Codigo do Centro de Custo
@param lVisualDados, logical, Indica se esta sendo executada para visualizacao detalhada do calculo
@type function
/*/
Static Function OX052008_CalculaCentroCusto(cParConta, cParCenCusto, lVisualDados)

	Local nTemp := 0
	Local nRetorno := 0
	Local nSaldoIni := 0
	Local nSaldoFim := 0

	CTT->(dbSetOrder(1))
	if CTT->(DBSeek(xFilial("CTT")+ cParCenCusto ))
		if VDE->VDE_TIPSAL == "1"
			nTemp := SaldoCCus(cParConta, cParCenCusto ,dDataFim,"01","1",1)
			nSaldoIni := nTemp
		elseif VDE->VDE_TIPSAL == "2"
			nSaldoFim := SaldoCCus(cParConta, cParCenCusto ,dDataFim,"01","1",1)
			nSaldoIni := SaldoCCus(cParConta, cParCenCusto ,dDataIni - 1,"01","1",1)
			nTemp := nSaldoFim - nSaldoIni
		endif
		if VDE->VDE_OPER == "1"
			nTemp := -nTemp
		endif
		nRetorno += nTemp
		OX052010_GeraDadosVisual(lVisualDados, nTemp , cParConta , cParCenCusto , "" , nSaldoIni , 0, CT1->CT1_NORMAL )
	endif


Return nRetorno


/*/{Protheus.doc} OX052009_CalculaContaContab
//TODO Descri��o auto-gerada.
@author Rubens
@since 02/10/2018
@version 1.0
@return nRetorno, Valor calculado
@param cParConta, characters, Codigo da Conta
@param lVisualDados, logical, Indica se esta sendo executada para visualizacao detalhada do calculo
@type function
/*/
Static Function OX052009_CalculaContaContab(cParConta, lVisualDados)
	Local nTemp := 0
	Local nRetorno := 0
	Local nSaldoIni := 0
	Local nSaldoFim := 0

	If VDE->VDE_TIPSAL == "1"
		nTemp := SALDOCONTA(cParConta,dDataFim,"01","1",1)
		nSaldoIni := nTemp
	elseif VDE->VDE_TIPSAL == "2"
		nSaldoIni := SaldoConta(cParConta, dDataIni - 1,"01","1",1)
		nSaldoFim := SaldoConta(cParConta, dDataFim    ,"01","1",1)
		nTemp := nSaldoFim - nSaldoIni
	endif
	if VDE->VDE_OPER == "1"
		nTemp := -nTemp
	endif
	nRetorno := nTemp

	OX052010_GeraDadosVisual(lVisualDados, nTemp, cParConta, "", "", nSaldoIni, 0 , CT1->CT1_NORMAL)

Return nRetorno

/*/{Protheus.doc} OX052010_GeraDadosVisual
Cria uma linha na Grid para visualizacao detalhada do calculo
@author Rubens
@since 02/10/2018
@version 1.0
@param lVisualDados, logical, descricao
@param nAuxVal, numeric, descricao
@param cConta, characters, descricao
@param cCentroCusto, characters, descricao
@param cItemConta, characters, descricao
@param nSaldoIni, numeric, descricao
@param nSaldoFim, numeric, descricao
@param cCT1NORMAL, characters, descricao

@type function
/*/
Function OX052010_GeraDadosVisual(lVisualDados, nAuxVal, cConta, cCentroCusto, cItemConta, nSaldoIni, nSaldoFim, cCT1NORMAL )
	Default cCentroCusto := ""
	Default cItemConta := ""
	Default nSaldoFim := 0

	If lVisualDados == .f.
		Return
	EndIf
	If nAuxVal <> 0
		oVisualLog:AddDataRow(;
			{;
				VD9->VD9_CODCON ,;
				X3CBOXDESC("VD9_TIPO",VD9->VD9_TIPO)   ,;
				cConta         ,;
				cCentroCusto     ,;
				cItemConta          ,;
				nAuxVal,;
				nSaldoIni,;
				IIf( VDE->VDE_TIPSAL == "1" , 0       , nSaldoFim) ,;
				X3CBOXDESC("CT1_NORMAL", cCT1NORMAL ) ,;
				X3CBOXDESC("VDE_OPER", VDE->VDE_OPER  ) ,;
				X3CBOXDESC("VDE_TIPSAL", VDE->VDE_TIPSAL) ;
			};
			)

	Endif
Return


/*/{Protheus.doc} OX052012_PercorreCT1
Percorre tabela de conta para calculo do DEF
@author Rubens
@since 02/10/2018
@version 1.0
@param cContaContabil, characters, Conta contabil (Passar valores com AllTrim, para processar todas as contas que comecam com o valor passado como parametro)
@param lVisualDados, logical, Indica se esta sendo executada para visualizacao detalhada do calculo
@type function
/*/
Static Function OX052012_PercorreCT1(cContaContabil , lVisualDados )

	Local nCntFor, nNumFor
	Local nTamConta
	Local cVD8ItemConta := ""
	Local nLenCenCus := Len(aCenCus)
	Local aItensConta
	Local lMultICFil

	If lItemCtComoFilial
		lMultICFil := .f.
		
		VD8->( DBSetorder(1) )
		VD8->( MSSeek( xFilial("VD8") + cPARCodDef + cEmpAnt + cFilAnt ) )
		If At(",",AllTrim(VD8->VD8_ITEMCT)) > 0 .and. Len(AllTrim(VD8->VD8_ITEMCT)) > TamSX3("CTD_ITEM")[1]
			aItensConta := OX052007_SeparaTexto(AllTrim(VD8->VD8_ITEMCT),",")
			lMultICFil := .t.
		Else
			cVD8ItemConta := AllTrim(VD8->VD8_ITEMCT)
		EndIf
	EndIf

	// Monta uma chave de pesquisa utilizando ALLTRIM pois � possivel informar, no cadastro dos Itens do DEF,
	// uma conta Sintetica, e nesses casos iremos processar todas as contas Analiticas
	cChaveCT1 := xFilial("CT1") + ALlTrim( cContaContabil )
	nTamConta := Len( cChaveCT1 )
	CT1->(DBSeek( cChaveCT1 ))
	While !CT1->(eof()) .and. Left(CT1->CT1_FILIAL + CT1->CT1_CONTA, nTamConta) == cChaveCT1

		nRecCT1 := CT1->(Recno())

		// Centro de Custo Envolvido
		if nLenCenCus > 0
			// Calcula por Conta Contabil + Centro de Custo + Item Contabil
			If lItemCtComoFilial
				for nCntFor := 1 to nLenCenCus
					If lMultICFil
						For nNumFor := 1 to Len(aItensConta)
							nVal += OX052011_CalculaItemContabil(CT1->CT1_CONTA, aCenCus[nCntFor] , aItensConta[nNumFor] , lVisualDados)
						Next
					Else
						nVal += OX052011_CalculaItemContabil(CT1->CT1_CONTA, aCenCus[nCntFor] , cVD8ItemConta , lVisualDados)
					EndIf
				next
			// Calcula por Conta Contabil + Centro de Custo
			Else
				for nCntFor := 1 to nLenCenCus
					nVal += OX052008_CalculaCentroCusto(CT1->CT1_CONTA, aCenCus[nCntFor] , lVisualDados)
				next
			EndIf
		// Calcula Pela Conta Contabil
		else
			// Calcula Saldo Pela Conta Contabil + Item Contabil
			If lItemCtComoFilial
				If lMultICFil
					For nNumFor := 1 to Len(aItensConta)
						nVal += OX052011_CalculaItemContabil(CT1->CT1_CONTA, "" , aItensConta[nNumFor] , lVisualDados)
					Next
				Else
					nVal += OX052011_CalculaItemContabil(CT1->CT1_CONTA, "" , cVD8ItemConta , lVisualDados)
				EndIf
			// Calcula Saldo somente pela Conta Contabil
			Else
				nVal += OX052009_CalculaContaContab(CT1->CT1_CONTA, lVisualDados)
			EndIf
		endif

		CT1->(DBGoto(nRecCT1))
		CT1->(DBSkip())
	enddo
	//

Return

/*/{Protheus.doc} OX052011_CalculaItemContabil
Calculo por Item Contabil
@author Rubens
@since 02/10/2018
@version 1.0
@return nRetorno, Valor calculado
@param cParConta, characters, Codigo da Conta
@param cParCenCusto, characters, Codigo do Centro de Custo
@param cParItemContabil, characters, Codigo do Item Contabil
@param lVisualDados, logical, Indica se esta sendo executada para visualizacao detalhada do calculo
@type function
/*/
Static Function OX052011_CalculaItemContabil( cParConta, cParCenCusto, cParItemContabil, lVisualDados)

	Local nTemp := 0
	Local nRetorno := 0
	Local nSaldoIni := 0
	Local nSaldoFim := 0

	nTemp := 0
	If VDE->VDE_TIPSAL == "1"
		nSaldoIni := SaldoItem( cParConta , cParCenCusto , cParItemContabil , dDataFim    , "01" , "1", 1)
		nTemp := nSaldoIni
	ElseIf VDE->VDE_TIPSAL == "2"

		nSaldoIni := SaldoItem( cParConta , cParCenCusto , cParItemContabil , dDataIni - 1, "01" , "1" , 1)
		nSaldoFim := SaldoItem( cParConta , cParCenCusto , cParItemContabil , dDataFim    , "01" , "1" , 1)

		nTemp := nSaldoFim
		nTemp := nTemp - nSaldoIni
	EndIf

	If VDE->VDE_OPER == "1"
		nTemp := -nTemp
	EndIf

	nRetorno += nTemp

	OX052010_GeraDadosVisual(;
		lVisualDados,;
		nTemp		,;
		cParConta,;
		cParCenCusto	,;
		cParItemContabil		,;
		nSaldoIni	,;
		nSaldoFim	,;
		CT1->CT1_NORMAL )

Return nRetorno
