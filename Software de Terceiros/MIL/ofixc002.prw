// 浜様様様曜様様様様�
// � Versao � 5      �
// 藩様様様擁様様様様�
#include "tbiconn.ch"
#include "Protheus.ch"
#include "OFIXC002.ch"
/*
===============================================================================
###############################################################################
##+----------+------------+-------+-----------------------+------+----------+##
##|Funcao    | OFIXC002   | Autor |  Luis Delorme         | Data | 14/02/12 |##
##+----------+------------+-------+-----------------------+------+----------+##
##|Descricao | Consulta Pecas aguardando chegada (RoadMap 70 Sug.Auto)      |##
##+----------+--------------------------------------------------------------+##
##|Uso       | Veiculos                                                     |##
##+----------+--------------------------------------------------------------+##
###############################################################################
===============================================================================
*/
Function OFIXC002()
Local aObjects := {} , aInfo := {}, aPos := {}, nCntFor
Local aSizeHalf := MsAdvSize(.t.)
Local lRet := .f.
Local cBKPCodIte := ""
Private oFnt1 := TFont():New( "System", , 12 )
Private oFnt2 := TFont():New( "Courier New", , 16,.t. )
Private oFnt3 := TFont():New( "Arial", , 14,.t. )
Private oVerd   := LoadBitmap( GetResources() , "BR_VERDE" )	// Selecionado
Private oVerm   := LoadBitmap( GetResources() , "BR_VERMELHO" )	// Nao Selecionado
Private aIteRelP := {{.f.,"","","",stod("00000000"),""}}
Private cDescricao := ""
Private nEstoque
Private xItem := {}
Private aNewBot := {	}   
Private cVend := space(TamSx3("VS1_CODVEN")[1])
//
cOrcamento:= space(TamSx3("VS1_NUMORC")[1])
cVendedor := space(TamSx3("VS1_CODVEN")[1])
//
// Monta os F3 conforme o SX3
//
DBSelectArea("SX3")
DBSetOrder(2)
DBSeek("VS1_CODVEN")
cF3CODVEN := SX3->X3_F3
// Fator de reducao de 0.8
for nCntFor := 1 to Len(aSizeHalf)
	aSizeHalf[nCntFor] := INT(aSizeHalf[nCntFor] * 0.8)
next
// ########################################################################
// # Montagem das informacoes de posicionamento da consulta               #
// ########################################################################
aInfo := { aSizeHalf[ 1 ], aSizeHalf[ 2 ],aSizeHalf[ 3 ] ,aSizeHalf[ 4 ], 3, 3 }// Tamanho total da tela
AAdd( aObjects, { 0, 04, .T., .f. } )
AAdd( aObjects, { 0, 08, .T., .f. } )
AAdd( aObjects, { 0, 08, .T., .f. } )
AAdd( aObjects, { 0, 0,  .T., .T. } )
AAdd( aObjects, { 0, 08, .T., .f. } )

aPos := MsObjSize( aInfo, aObjects )
dyc := (aPos[1,4] - aPos[1,2])
dyc2 := (aPos[1,4] - aPos[1,2]) / 2	// step horizontal
_nSpc := 40
_nLarg := dyc2 - _nSpc - 10
_nLarg1 := dyc2 - _nSpc - 150 
_nLarg2 := dyc2 - _nSpc - 80

nBtnSize := 25
// ########################################################################
// # Montagem da tela com informacoes fixas                               #
// ########################################################################

//DEFINE MSDIALOG oDlgCP FROM aSizeHalf[7],0 TO aSizeHalf[6],aSizeHalf[5] TITLE STR0001 OF oMainWnd PIXEL
//// Linha 1
//oSay01 := TSay():New(aPos[2,1],aPos[2,2],{|| STR0002 },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,_nSpc,8)
//@ aPos[2,1],aPos[2,2] + _nSpc GET o_Chave VAR  cOrcamento SIZE _nLarg,8 OF oDlgCP PIXEL
//o_Chave:bValid  	 := {|U| (IIF(Empty(cOrcamento),.t.,FS_VERORC() ) ) }
////
//oSay02 := TSay():New(aPos[3,1],aPos[3,2],{|| STR0003 },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,_nSpc,8)
//@ aPos[3,1],aPos[3,2] + _nSpc MSGET cVendedor F3 cF3CODVEN VALID (Empty(cVendedor).or.FS_VERVEN()) SIZE _nLarg1,8 PIXEL OF oDlgCP
//@ aPos[3,1],aPos[3,2] + _nSpc+_nLarg1 MSGET cDescricao  SIZE _nLarg2,8 PIXEL OF oDlgCP when .f.
//
//@ aPos[2,1],aPos[2,2]+dyc2  BUTTON oBtn1 PROMPT OemToAnsi(STR0004) OF oDlgCP SIZE nBtnSize,10 PIXEL ACTION FS_FILTRA()
//
//@ aPos[5,1],aPos[5,2]  BITMAP oxLara RESOURCE "BR_VERDE" OF oDlgCP SIZE 10,10 PIXEL NOBORDER
//oSay01 := TSay():New(aPos[5,1],aPos[5,2] + 10,{|| "Or�amento com Produtos Dispon�veis" },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,120,8)
//
//
//@ aPos[5,1],aPos[5,2]+dyc2  BITMAP oxLara RESOURCE "BR_VERMELHO" OF oDlgCP SIZE 10,10 PIXEL NOBORDER
//oSay01 := TSay():New(aPos[5,1],aPos[5,2] + 10+dyc2,{|| "Or�amento Aguardando Produtos" },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,120,8)

DEFINE MSDIALOG oDlgCP TITLE STR0001 FROM  01,11 TO 29,114 OF oMainWnd
// Linha 1
oSay01 := TSay():New(017,004,{|| STR0002 },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,40,8)
@ 017,040 GET o_Chave VAR  cOrcamento SIZE 100,8 OF oDlgCP PIXEL
o_Chave:bValid  	 := {|U| (IIF(Empty(cOrcamento),.t.,FS_VERORC() ) ) }
oSay02 := TSay():New(028,004,{|| STR0003 },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,40,8)
@ 028,040 MSGET cVendedor F3 cF3CODVEN VALID (Empty(cVendedor).or.FS_VERVEN()) SIZE 50,8 PIXEL OF oDlgCP
@ 028,092 MSGET cDescricao  SIZE 100,8 PIXEL OF oDlgCP when .f.
@ 017,200  BUTTON oBtn1 PROMPT OemToAnsi(STR0004) OF oDlgCP SIZE 40,10 PIXEL ACTION FS_FILTRA()
@ 195,004  BITMAP oxLara RESOURCE "BR_VERDE" OF oDlgCP SIZE 10,10 PIXEL NOBORDER
oSay01 := TSay():New(195,015,{|| STR0008 },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,120,8)	// "Or�amento Atendido"
@ 195,150 BITMAP oxLara RESOURCE "BR_VERMELHO" OF oDlgCP SIZE 10,10 PIXEL NOBORDER
oSay01 := TSay():New(195,160,{|| STR0009 },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,120,8)	// "Or�amento n�o Atendido"
/// ########################################################################
/// # Montagem da listbox contendo informacoes dos itens relacionados      #
/// ########################################################################
@ 039,004 LISTBOX oLbIteRelP FIELDS HEADER ;
OemToAnsi(" "), ;
OemToAnsi(STR0002), ;
OemToAnsi(STR0005), ;
OemToAnsi(STR0003), ;
OemToAnsi(STR0006), ;
OemToAnsi(STR0007), ;
COLSIZES 0.02 * dyc, 0.100 * dyc, 0.200 * dyc, 0.250 * dyc, 0.175 * dyc, 0.200 * dyc, ;
SIZE 400, 150 PIXEL OF oDlgCP
///
oLbIteRelP:SetArray(aIteRelP)
//
oLbIteRelP:bLine := { || { IIF(aIteRelP[oLbIteRelP:nAt,1],oVerd,oVerm),;
aIteRelP[oLbIteRelP:nAt,2],;
aIteRelP[oLbIteRelP:nAt,3],;
aIteRelP[oLbIteRelP:nAt,4],;
dtoc(aIteRelP[oLbIteRelP:nAt,5]),;
aIteRelP[oLbIteRelP:nAt,6] }}

ACTIVATE MSDIALOG oDlgCP CENTER ON INIT (EnchoiceBar(oDlgCP,{|| lRet := .t. ,oDlgCP:End()},{ || oDlgCP:End() },,aNewBot))

//DEFINE MSDIALOG oDlgCP FROM aSizeHalf[7],0 TO aSizeHalf[6],aSizeHalf[5] TITLE STR0001 OF oMainWnd PIXEL
//// Linha 1
//oSay01 := TSay():New(aPos[2,1],aPos[2,2],{|| STR0002 },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,_nSpc,8)
//@ aPos[2,1],aPos[2,2] + _nSpc GET o_Chave VAR  cOrcamento SIZE _nLarg,8 OF oDlgCP PIXEL
//o_Chave:bValid  	 := {|U| (IIF(Empty(cOrcamento),.t.,FS_VERORC() ) ) }
////
//oSay02 := TSay():New(aPos[3,1],aPos[3,2],{|| STR0003 },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,_nSpc,8)
//@ aPos[3,1],aPos[3,2] + _nSpc MSGET cVendedor F3 cF3CODVEN VALID (Empty(cVendedor).or.FS_VERVEN()) SIZE _nLarg1,8 PIXEL OF oDlgCP
//@ aPos[3,1],aPos[3,2] + _nSpc+_nLarg1 MSGET cDescricao  SIZE _nLarg2,8 PIXEL OF oDlgCP when .f.
//
//@ aPos[2,1],aPos[2,2]+dyc2  BUTTON oBtn1 PROMPT OemToAnsi(STR0004) OF oDlgCP SIZE nBtnSize,10 PIXEL ACTION FS_FILTRA()
//
//@ aPos[5,1],aPos[5,2]  BITMAP oxLara RESOURCE "BR_VERDE" OF oDlgCP SIZE 10,10 PIXEL NOBORDER
//oSay01 := TSay():New(aPos[5,1],aPos[5,2] + 10,{|| "Or�amento com Produtos Dispon�veis" },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,120,8)
//
//
//@ aPos[5,1],aPos[5,2]+dyc2  BITMAP oxLara RESOURCE "BR_VERMELHO" OF oDlgCP SIZE 10,10 PIXEL NOBORDER
//oSay01 := TSay():New(aPos[5,1],aPos[5,2] + 10+dyc2,{|| "Or�amento Aguardando Produtos" },oDlgCP,,oFnt3,,,,.t.,CLR_BLACK,,120,8)
//
//
// ########################################################################
// # Montagem da listbox contendo informacoes dos itens relacionados      #
// ########################################################################
//@ aPos[4,1],aPos[4,2] LISTBOX oLbIteRelP FIELDS HEADER ;
//OemToAnsi(" "), ;
//OemToAnsi(STR0002), ;
//OemToAnsi(STR0005), ;
//OemToAnsi(STR0003), ;
//OemToAnsi(STR0006), ;
//OemToAnsi(STR0007), ;
//COLSIZES 0.02 * dyc, 0.100 * dyc, 0.200 * dyc, 0.250 * dyc, 0.175 * dyc, 0.200 * dyc, ;
//SIZE aPos[4,4] - aPos[4,2], aPos[4,3] - aPos[4,1] PIXEL OF oDlgCP
////
//oLbIteRelP:SetArray(aIteRelP)
////
//oLbIteRelP:bLine := { || { IIF(aIteRelP[oLbIteRelP:nAt,1],oVerd,oVerm),;
//aIteRelP[oLbIteRelP:nAt,2],;
//aIteRelP[oLbIteRelP:nAt,3],;
//aIteRelP[oLbIteRelP:nAt,4],;
//dtoc(aIteRelP[oLbIteRelP:nAt,5]),;
//aIteRelP[oLbIteRelP:nAt,6] }}
////
////ACTIVATE MSDIALOG oDlgCP CENTER ON INIT (EnchoiceBar(oDlgCP,{|| lRet := .t. ,OC002POSPEC(),oDlgCP:End()},{ || oDlgCP:End() },,aNewBot))
//ACTIVATE MSDIALOG oDlgCP CENTER ON INIT (EnchoiceBar(oDlgCP,{|| lRet := .t. ,oDlgCP:End()},{ || oDlgCP:End() },,aNewBot))
//
return lRet
/*
===============================================================================
###############################################################################
##+----------+------------+-------+-----------------------+------+----------+##
##|Funcao    | FS_VERVEN  | Autor |  Luis Delorme         | Data | 13/02/12 |##
##+----------+------------+-------+-----------------------+------+----------+##
##|Descricao | Valida vendedor digitado                                     |##
##+----------+--------------------------------------------------------------+##
##|Uso       | Veiculos                                                     |##
##+----------+--------------------------------------------------------------+##
###############################################################################
===============================================================================
*/
Static Function FS_VERVEN()

DBSelectArea("SA3")
DBSetOrder(1)
if DBSeek(xFilial("SA3")+Alltrim(cVendedor))
	cVend := Alltrim(cVendedor)+space(TamSx3("VS1_CODVEN")[1]-len(Alltrim(cVendedor)))
	cDescricao := Alltrim(SA3->A3_NOME)
else
	return .f.
endif

return .t.

/*
===============================================================================
###############################################################################
##+----------+------------+-------+-----------------------+------+----------+##
##|Funcao    | FS_VERORC  | Autor |  Luis Delorme         | Data | 13/02/12 |##
##+----------+------------+-------+-----------------------+------+----------+##
##|Descricao | Valida or�amento digitado                                    |##
##+----------+--------------------------------------------------------------+##
##|Uso       | Veiculos                                                     |##
##+----------+--------------------------------------------------------------+##
###############################################################################
===============================================================================
*/
Static Function FS_VERORC()
DBSelectArea("VS1")
DBSetOrder(1)
if DBSeek(xFilial("VS1")+Alltrim(cOrcamento))
	return .t.
endif

return .f.
/*
===============================================================================
###############################################################################
##+----------+------------+-------+-----------------------+------+----------+##
##|Funcao    | FS_FILTRA  | Autor |  Luis Delorme         | Data | 13/02/12 |##
##+----------+------------+-------+-----------------------+------+----------+##
##|Descricao | Filtra os or�amentos conforme vendedor e orcamento (params.) |##
##+----------+--------------------------------------------------------------+##
##|Uso       | Veiculos                                                     |##
##+----------+--------------------------------------------------------------+##
###############################################################################
===============================================================================
*/
Static Function FS_FILTRA()

cQryAl001 := GetNextAlias()
cQuery := "SELECT VS1.VS1_NUMORC NORC,"
cQuery += "       SA1.A1_COD A1COD,"
cQuery += "       SA1.A1_TEL A1TEL,"
cQuery += "       SA1.A1_LOJA A1LOJA,"
cQuery += "       SA1.A1_NOME A1NOME,"
cQuery += "       SA3.A3_COD A3COD,"
cQuery += "       SA3.A3_NOME A3NOME,"
cQuery += "       VS1.VS1_DATORC DATORC,"
cQuery += "       SUM(VS3.VS3_QTDAGU) QAGU"
cQuery += " FROM "+RetSqlName("VS1")+ " VS1 INNER JOIN "+RetSqlName("SA1")+ " SA1 ON"
cQuery += "       ( A1_FILIAL ='" + xFilial("SA1") + "' AND VS1.VS1_CLIFAT = SA1.A1_COD AND VS1.VS1_LOJA = SA1.A1_LOJA AND SA1.D_E_L_E_T_=' ') LEFT OUTER JOIN "+RetSqlName("SA3")+ " SA3 ON"
cQuery += "       ( A3_FILIAL ='" + xFilial("SA3") + "' AND VS1.VS1_CODVEN = SA3.A3_COD AND SA3.D_E_L_E_T_=' ') INNER JOIN " + RetSqlName("VS3") + " VS3 ON"
cQuery += "       ( VS3_FILIAL ='" + xFilial("VS3") + "' AND VS1.VS1_NUMORC = VS3.VS3_NUMORC AND VS3.D_E_L_E_T_=' ')"
cQuery += " WHERE  VS1_STATUS = 'R' AND"
//
if !Empty(cOrcamento)
	cQuery += " VS1_NUMORC = '"+cOrcamento+"' AND"
endif
if !Empty(cVendedor)
	cQuery += " VS1_CODVEN = '"+cVend+"' AND"
endif
//
cQuery += "        VS1_FILIAL ='" + xFilial("VS1") + "' AND VS1.D_E_L_E_T_=' '"
cQuery += " GROUP BY VS1.VS1_NUMORC, SA1.A1_COD, SA1.A1_LOJA, SA1.A1_NOME, SA3.A3_COD," 
cQuery += " SA3.A3_NOME, VS1.VS1_DATORC, SA1.A1_TEL"

dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQuery ), cQryAl001, .F., .T. )

aIteRelP := {}
while !(cQryAl001)->(eof())
	
	aAdd(aIteRelP, {;
	(cQryAl001)->(QAGU) == 0,;
	(cQryAl001)->(NORC),;
	(cQryAl001)->(A1COD)+"/"+(cQryAl001)->(A1LOJA)+"-"+(cQryAl001)->(A1NOME),;
	(cQryAl001)->(A3COD)+"-"+(cQryAl001)->(A3NOME),;
	stod( (cQryAl001)->(DATORC) ),;
	(cQryAl001)->(A1TEL) } )
	
	(cQryAl001)->(dbSkip())
enddo
if Len(aIteRelP) == 0
	aIteRelP := {{.f.,"","","",stod("00000000"),""}}
endif
(cQryAl001)->(dbCloseArea())

oLbIteRelP:SetArray(aIteRelP)
//
oLbIteRelP:bLine := { || { IIF(aIteRelP[oLbIteRelP:nAt,1],oVerd,oVerm),;
aIteRelP[oLbIteRelP:nAt,2],;
aIteRelP[oLbIteRelP:nAt,3],;
aIteRelP[oLbIteRelP:nAt,4],;
dtoc(aIteRelP[oLbIteRelP:nAt,5]),;
aIteRelP[oLbIteRelP:nAt,6] }}
oLbIteRelP:Refresh()

return .t.
