// 浜様様様曜様様様様�
// � Versao � 19     �
// 藩様様様擁様様様様�
#include "Protheus.ch"
/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Funcao    � ORCAMTO  � Autor � Andre                 � Data � 01/08/00 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descricao � Impressao do Orcamento                                     咳�
臼青陳陳陳陳陳珍陳陳陳陳祖陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
User Function ORCAMTO()

SetPrvt("cTamanho,Limite,aOrdem,cTitulo,nLastKey,aReturn,cTitulo")
SetPrvt("cTamanho,cNomProg,cNomeRel,nLastKey,Limite,aOrdem,cAlias")
SetPrvt("cDesc1,cDesc2,cDesc3,lHabil,nOpca,nTipo,aPosGru,Inclui")
SetPrvt("cMarca,cObserv")

cTamanho := "M"                    // P/M/G
Limite   := 132                    // 80/132/220
aOrdem   := {}                     // Ordem do Relatorio
cTitulo  := "Orcamento"
cMarca   := "   "
nLastKey := 0
aReturn  := { "Zebrado", 1,"Administracao", 2, 1, 1, "",1 }
cTitulo  := "Orcamento"
cNomProg := "ORC_"
cNomeRel := "ORC_"
nLastKey := 0
cAlias   := "VS1"
cDesc1   := "Orcamento"
cDesc2   := ""
cDesc3   := ""
cObserv  := ""
lHabil   := .f.
nOpca    := 0
nTipo    := 15
aPosGru  := {}
nSemEstoque := 0
nMenEstoque := 0
lin      := 0
M_PAG    := 1
cCombus  := "" 
Private cPerg := "ORCAMT"
Private dDatVal := ctod("")
Private lImpCab := .t.


if Type("ParamIXB") == "U"
	ParamIXB := Nil
Endif

if ParamIXB == Nil
	Private cCadastro := "Orcamentos"
	Private aRotina := { { "Pesquisar" ,"AxPesqui", 0 , 1},;
	{ "Imprimir"  ,IMPORC()  , 0 , 3}}
	
	mBrowse( 6, 1,22,75,"VS1")
Else
	FS_ORCAMENTO(ParamIXB[1])
Endif

Return

/////////////////
Static Function IMPORC()

FS_ORCAMENTO(VS1->VS1_NUMORC)

Return


//////////////////////////////
Static Function FS_ORCAMENTO(cCodigo)
Local aParamBox := {}
Local aRet := {}
SetPrvt("cTipo,cOrdem,cTpoPad,cCusMed")


cNomProg := "ORC_"+VS1->VS1_NUMORC+"_"+__cUserId
cNomeRel := "ORC_"+VS1->VS1_NUMORC+"_"+__cUserId

dDatVal := VS1->VS1_DATVAL
AADD(aParamBox,{1,"Data Validade",dDatVal,"@D","","",".T.",50,.f.})
If ParamBox(aParamBox,VS1->VS1_NUMORC,@aRet,,,,,,,,.f.)
	dDatVal := aRet[1]
Else
	Return()
EndIf

cPerg := "ORCAMT"
ValidPerg()

Pergunte(cPerg,.f.)

lServer  := ( GetMv("MV_LSERVER") == "S" )

aImp     := RetImpWin(lServer ) // .T. Quando for no SERVER e .F. no CLIENT (Retorna o nome das impressoras instaladas)
cDrive   := GetMv("MV_DRVRLP")
cNomeImp := getmv("MV_PORTORC")  //"LPT2"

cNomeRel := SetPrint(cAlias,cNomeRel,cPerg,@ctitulo,cDesc1,cDesc2,cDesc3,.F.,"",.t.,cTamanho,nil    ,nil    ,nil)

cOrdem  := MV_PAR01
cSeqSer := MV_PAR02
CCusMed := 1

if nLastKey = 27
	Return
Endif

//Posicionamento dos Arquivos
DbSelectArea("VS1")
DbSetOrder(1)
DbSeek(xFilial("VS1")+cCodigo)
cMarca   := VS1->VS1_CODMAR

DbSelectArea("VV1")
FG_SEEK("VV1","VS1->VS1_CHAINT",1,.f.)

DbSelectArea("VV2")
FG_SEEK("VV2","cMarca+VV1->VV1_MODVEI",1,.f.)

DbSelectArea("VVC")
FG_SEEK("VVC","cMarca+VV1->VV1_CORVEI",1,.F.)

DbSelectArea("VS1")
FG_Seek("SE4","VS1->VS1_FORPAG",1,.f.)

DbSelectArea("SA1")
DbSetOrder(1)
DbSeek(xFilial("SA1")+VS1->VS1_CLIFAT+VS1->VS1_LOJA)

nUltKil := 0

if VS1->VS1_KILOME != 0
	nUltKil := VS1->VS1_KILOME
else
	nUltKil := FG_UltKil(VV1->VV1_CHAINT)
endif

SetDefault(aReturn,cAlias)

RptStatus({|lEnd| FS_ORCAMTO(@lEnd,cNomeRel,cAlias)},cTitulo)

MS_FLUSH()

Set Printer to
Set Device  to Screen

If (aReturn[ 5 ] == 1)
	OurSpool(cNomeRel)
Endif

Return


//////////////////////////////////////////////
Static Function FS_ORCAMTO(lEnd,wNRel,cAlias)

SetPrvt("lin,Pag,nTotPec,nTotSer,nTotal,nTotDesP,nTotDesS")
SetPrvt("oPr,nX,aDriver,cCompac,cNormal")

aDriver := LeDriver()
cCompac := aDriver[1]
cNormal := aDriver[2]
cOpcao  := "1"

nTotal   := 0
nTotPec  := nTotSer  := 0
nTotDesS := nTotDesP := 0
nTotGSer := 0
nTGSer   := 0

// Impressao do Orcamento
Set Printer to &wNRel
Set Printer On
Set Device  to Printer

Pag := 1

FS_CABEC(wNRel)
FS_DETALHE(wNRel)
FS_RODAPE(wNRel)

Return


//////////////////////////
Static Function FS_CABEC(wNRel)
Local ctitulo := "ORCAMENTO No. "+VS1->VS1_NUMORC
Local ccabec1 := ""
Local ccabec2 := ""
	
lin := 0
lin += cabec(ctitulo,ccabec1,ccabec2,wNRel,cTamanho,nTipo) + 1

If lImpCab

	cNomeEmp := SM0->M0_NOMECOM+space(50)
	cEndeEmp := SM0->M0_ENDENT+space(50)
	cNomeCid := SM0->M0_CIDENT+space(50)
	cEstaEmp := SM0->M0_ESTENT
	cCep_Emp := SM0->M0_CEPENT
	cFoneEmp := "Fone: " + SM0->M0_TEL
	cFax_Emp := "Fax: " + SM0->M0_FAX
	cCNPJEmp := transform(SM0->M0_CGC,"@R 99.999.999/9999-99")
	cInscEmp := SM0->M0_INSC
	cCodMun  := SM0->M0_CODMUN
	
	@ lin++,000 pSay left(cNomeEmp,55) + left(" CNPJ: " + cCNPJEmp + " IE: " + CInscEmp,42)
	@ lin++,000 pSay left(Alltrim(cFoneEmp)+" "+Alltrim(cFax_Emp)+" - End: " + cEndeEmp,132)
	@ lin++,000 pSay left("CEP: " + transform(cCep_Emp,"@R 99999-999") +  " - " + Alltrim(cNomeCid) + "-" + cEstaEmp ,132)
	@ lin++,000 pSay repl([-],132)
	

	SA3->(DbSetOrder(1))
	SA3->(dbgotop())
	SA3->(DbSeek(xFilial("SA3")+VS1->VS1_CODVEN))
	
	@ lin++,000 pSay left("Data: " + DTOC(VS1->VS1_DATORC)+"                 Validade: " + DTOC(dDatVal)+"                Vendedor: " + VS1->VS1_CODVEN + " " + SA3->A3_NOME,132)
	
	@ lin++,000 pSay repl([-],132)
	
	cCGCCPF1  := subs(transform(SA1->A1_CGC,PicPes(RetPessoa(SA1->A1_CGC))),1,at("%",transform(SA1->A1_CGC,PicPes(RetPessoa(SA1->A1_CGC))))-1)
	cCGCPro   := cCGCCPF1 + space(18-len(cCGCCPF1))

	VAM->( DbSeek( xFilial("VAM") + SA1->A1_IBGE ) )
	
	@ lin++,000 pSay left("Cliente: " + VS1->VS1_CLIFAT + "-" + left(SA1->A1_NOME,50)+" CNPJ/CPF: "+cCGCPro+space(100),100)+" "+"Fone: (" + VAM->VAM_DDD + ") " + Alltrim(SA1->A1_TEL)
	@ lin++,000 pSay left("End: " + SA1->A1_END+space(50),50)+ " - " + left(Alltrim(SA1->A1_MUN)+ "-" +SA1->A1_EST+space(50),47) +" "+"Fax.: (" + VAM->VAM_DDD + ") " + Alltrim(SA1->A1_FAX)
	
	if VS1->VS1_TIPORC == "2"
		cCombus := Padr(X3CBOXDESC("VV1_COMVEI",VV1->VV1_COMVEI),15," ")
		@ lin++,000 pSay repl([-],132)
		@ lin++,000 pSay left("Chassi: " + left(VV1->VV1_CHASSI,25) + " Placa: " + Alltrim(Transform(VV1->VV1_PLAVEI,"@R AAA-9999")) + " Cor: " + left(VVC->VVC_DESCRI,20) + " Comb: " + cCombus,132)
		@ lin++,000 pSay left("Modelo: " + left(Alltrim(VV1->VV1_MODVEI)+"-"+VV2->VV2_DESMOD+space(50),50) + " Fab/Mod: " + left(VV1->VV1_FABMOD,4)+"/"+right(VV1->VV1_FABMOD,4) + " Km: " + str(nUltKil,15),132)
		@ lin++,000 pSay repl([-],132)
		// Daniele - 28/09/2006 Mensagem 
		lin++
		@lin++,002 pSay "Atendendo solicitacao de V.Sa.(s), temos a satisfacao de fornecer a relacao de pecas e servicos necessarios para o veiculo acima " 
		@lin++,002 pSay "especificado.  Estimativa de orcamento sujeito a alteracao apos desmontagem."
		lin++
	endif
	
	lImpCab := .f.
	
EndIf	

Return


////////////////////////////
Static Function FS_DETALHE(wNRel)

Local bCampo := { |nCPO| Field(nCPO) }
Local i := 0

SetPrvt("nSubTot,cGrupo,aStru,cTipSer,nSaldo")

if VS1->VS1_TIPORC == "2"
	
	aStru := {}
	DbSelectArea("SX3")
	DbSetOrder(1)
	DbSeek("VS4")
	Do While !Eof() .And. x3_arquivo == "VS4"
		if x3_context # "V"
			Aadd(aStru,{x3_campo,x3_tipo,x3_tamanho,x3_decimal})
		Endif
		DbSkip()
	EndDo

	oObjTempTable := OFDMSTempTable():New()
	oObjTempTable:cAlias := "TRB"
	oObjTempTable:aVetCampos := aStru
	If cSeqSer == 2
		oObjTempTable:AddIndex(, {"VS4_FILIAL","VS4_NUMORC","VS4_TIPSER","VS4_GRUSER","VS4_CODSER"} )
	Else
		oObjTempTable:AddIndex(, {"VS4_FILIAL","VS4_NUMORC","VS4_SEQUEN"} )
	EndIf
	oObjTempTable:CreateTable(.f.)
	
	DbSelectArea("VS4")
	DbSeek(xFilial("VS4")+VS1->VS1_NUMORC)
	Do While !Eof() .and. VS4->VS4_NUMORC == VS1->VS1_NUMORC .and. VS4->VS4_FILIAL == xFilial("VS4")
		DbSelectArea("TRB")
		RecLock("TRB",.T.)
		For i := 1 to FCOUNT()
			cCpo := aStru[i,1]
			TRB->&(cCpo) := VS4->&(cCpo)
		Next
		MsUnlock()
		DbSelectArea("VS4")
		DbSkip()
	EndDo
	
	DbSelectArea("TRB")
	DbGotop()
	
Endif

//Pecas

DbSelectArea("VS3")
DbGotop()
DbSetOrder(1)
if cOrdem == 2
	DbSetOrder(2)
Endif

if DbSeek(xFilial("VS3")+VS1->VS1_NUMORC)

	lin := FS_TITULO("PEC",lin)	
	
	DbSelectArea("SB1")
	DbSetOrder(7)
	DbSelectArea("SB5")
	DbSelectArea("VS3")
	
	nQtdIte := 0
	nPerDes := 0                                                        
	nTotDes := 0
	Do While !EOF() .and. VS3->VS3_NUMORC == VS1->VS1_NUMORC .AND. VS3->VS3_FILIAL == xFilial("VS3")
		
	   if !Empty(VS3->VS3_MOTPED)
			DbSelectArea("VS3")
			DbSkip()
	      Loop
		Endif
			      
		DbSelectArea("SB1")
		DbGotop()
		DbSeek(xFilial("SB1")+VS3->VS3_GRUITE+VS3->VS3_CODITE)
		
		DbSelectArea("SB5")
		DbGotop()
		DbSeek(xFilial("SB5")+SB1->B1_COD)
		
		DbSelectArea("SBM")
		DbGotop()
		DbSeek(xFilial("SBM")+VS3->VS3_GRUITE)
		
		DbSelectArea("SB2")
		DbSeek(xFilial("SB2")+SB1->B1_COD+If(!Empty(VS3->VS3_LOCAL),VS3->VS3_LOCAL,SB1->B1_LOCPAD))
		nSaldo := SaldoSB2()
		
		DbSelectArea("VS3")
		
		If VS3->VS3_QTDITE == 0 //Pedido de venda cancelado parcialmente (Painel do Or�amento)
			DbSkip()
			Loop
		EndIf
		if lin > 60
			Pag := Pag + 1
			FS_CABEC(wNRel)
			lin := FS_TITULO("PEC",lin)	
		Endif
		cEstq := "  "
		if nSaldo <= 0
			nSemEstoque++
			cEstq := "**"
		elseif nSaldo < VS3->VS3_QTDITE
			nMenEstoque++
			cEstq := " *"
		Endif
		nTamDes := 74
		nIniDes := 00
		If MV_PAR04 # 1
			@ lin,00 pSay left(VS3->VS3_CODITE,21)
			nTamDes := 52
			nIniDes := 22
		EndIf
		If MV_PAR03 # 1
			nValPec := ( VS3->VS3_VALPEC + ( VS3->VS3_VICMSB / VS3->VS3_QTDITE ) )
		Else
			nValPec := ( ( VS3->VS3_VALTOT + VS3->VS3_VICMSB ) / VS3->VS3_QTDITE )
		EndIf
		@ lin++,nIniDes pSay left(SB1->B1_DESC+space(nTamDes),nTamDes)+" "+left(FM_PRODSBZ(SB1->B1_COD,"SB5->B5_LOCALI2")+space(15),15)+" "+cEstq+Transform(VS3->VS3_QTDITE,"@E 99999")+Transform(nValPec,"@E 99999,999.99")+IIF(MV_PAR03#1,Transform(VS3->VS3_VALDES,"@E 999,999.99"),space(10))+Transform(VS3->VS3_VALTOT+VS3->VS3_VICMSB,"@E 99999,999.99")
		
		nTotPec  += ( nValPec * VS3->VS3_QTDITE )
		nTotDesP += VS3->VS3_VALDES

		nQtdIte += VS3->VS3_QTDITE
		nPerDes += VS3->VS3_PERDES
		nTotDes += VS3->VS3_VALDES

		DbSelectArea("VS3")
		DbSkip()
		
		if lin > 60
			Pag := Pag + 1
			FS_CABEC(wNRel)
			lin := FS_TITULO("PEC",lin)	
		Endif
	EndDo
	
	if lin > 60
		Pag := Pag + 1
		FS_CABEC(wNRel)
	Endif
	
	if nTotDesP > 0 .and. MV_PAR03 # 1
		lin++
		@ lin,089 pSay "Desconto em Pecas:"+space(10)+Transform(nTotDesP,"@E 99999999,999.99")
	endif
	lin++ 
	if MV_PAR03 == 2
		@ lin++,089 pSay "Subtotal de Pecas:"+space(10)+Transform(nTotPec-nTotDesP,"@E 99999999,999.99")
    Else
		@ lin++,089 pSay "Subtotal de Pecas:"+space(10)+Transform(nTotPec,"@E 99999999,999.99")
	Endif	
	
Endif

//Servicos

aTipoSer := {}
aVetSer  := {}
if VS1->VS1_TIPORC == "2"
	
	DbSelectArea("TRB")
	DbGotop()
	if reccount() > 0
		
		if lin > 55
			Pag := Pag + 1
			FS_CABEC(wNRel)
		Endif
		lin := FS_TITULO("SRV",lin)	

		DbSelectArea("SB1")
		DbSetOrder(7)
		DbSelectArea("TRB")
		
		cTipSer := TRB->VS4_TIPSER
		Do While !EOF() .and. TRB->VS4_NUMORC == VS1->VS1_NUMORC
			
			DbSelectArea("VO6")
			DbSetOrder(2)
			DbSeek(xFilial("VO6")+FG_MARSRV(cMarca,TRB->VS4_CODSER)+TRB->VS4_CODSER)
			
			DbSelectArea("VOK")
			DbSetOrder(1)
			DbSeek(xFilial("VOK")+TRB->VS4_TIPSER)
			
			DbSelectArea("VOS")
			DbSetOrder(1)
			DbSeek(xFilial("VOS")+cMarca+TRB->VS4_GRUSER)
			
			DbSelectArea("TRB")

			@ lin,00 pSay TRB->VS4_GRUSER
			nTamDes := 81
			nIniDes := 04
			If MV_PAR05 # 1
				@ lin,04 pSay left(TRB->VS4_CODSER,22)
				nTamDes := 58
				nIniDes := 27
			EndIf
			@ lin++,nIniDes pSay left(VO6->VO6_DESSER+space(nTamDes),nTamDes)+" "+;
								TRB->VS4_TIPSER+space(9)+;
								Transform(IIf(VOK->VOK_INCMOB<>"2",TRB->VS4_VALSER,TRB->VS4_VALTOT),"@E 99999,999.99")+;
								IIf(MV_PAR03#1,Transform(TRB->VS4_VALDES,"@E 999,999.99"),space(10))+;
								Transform(TRB->VS4_VALTOT,"@E 99999,999.99")

			nTotSer  += TRB->VS4_VALTOT
			nTotDesS += TRB->VS4_VALDES

			DbSelectArea("TRB")
			DbSkip()
			
			if lin > 60
				Pag := Pag + 1
				FS_CABEC(wNRel)
				lin := FS_TITULO("SRV",lin)	
			Endif
		EndDo
		asort(aVetSer,,,{|x,y| x[1]+x[2] < y[1]+y[2]})
		cSer := ""

		if lin > 60
			Pag := Pag + 1
			FS_CABEC(wNRel)
		Endif

		if nTotDesS > 0 .and. MV_PAR03 # 1
			lin++
			@ lin,089 pSay "Desconto em Servicos:"+space(7)+Transform(nTotDesS,"@E 9999999,999.99")
		endif
		lin++
		@ lin,089 pSay "Subtotal de Servicos:"+space(7)+Transform(nTotSer,"@E 99999999,999.99")
	endif
Endif

if VS1->VS1_TIPORC == "2"
	
	DbSelectArea("TRB")
	oObjTempTable:CloseTable()

endif

lin+=2

if lin > 60
	Pag := Pag + 1
	FS_CABEC(wNRel)
Endif

Return


////////////////////
Static Function FS_RODAPE(wNRel)

lin++

if lin > 60
	Pag := Pag + 1
	FS_CABEC(wNRel)
	lin++
Endif

@ lin++,000 pSay "Observacao:"
if (nSemEstoque+nMenEstoque) > 0
	if nSemEstoque > 0
		@ lin++,000 pSay "ITENS ASSINALADOS COM '**' AO LADO DA QUANTIDADE ESTAO COM SALDO ZERO NO MOMENTO"
	Endif
	if nMenEstoque > 0
		@ lin++,000 pSay "ITENS ASSINALADOS COM '* ' AO LADO DA QUANTIDADE ESTAO INSUFICIENTES NO MOMENTO"
	Endif
EndIf
cKeyAce := VS1->VS1_OBSMEM + [001]

DbSelectArea("SYP")
DbSetOrder(1)
FG_SEEK("SYP","cKeyAce",1,.f.)

do while xFilial("SYP")+VS1->VS1_OBSMEM == SYP->YP_FILIAL+SYP->YP_CHAVE .and. !eof()
	
	nPos := AT("\13\10",SYP->YP_TEXTO)
	if nPos > 0
		nPos-=1
	Else
		nPos := Len(SYP->YP_TEXTO)
	Endif
	cObserv := Substr(SYP->YP_TEXTO,1,nPos)
	
	@ lin++,003 pSay cObserv
	
	SYP->(DbSkip())
	
	if lin > 60
		Pag := Pag + 1
		FS_CABEC(wNRel)
	Endif
	
enddo
lin++

nTotal := nTotPec + nTotSer

if lin > 43
	Pag := Pag + 1
	FS_CABEC(wNRel)
	lin++
	lin++
Endif

if VS1->VS1_TIPORC == "2"
	dbSelectArea("VOI")
	dbSetOrder(1)
	dbSeek(xFilial("VOI")+VS1->VS1_TIPTEM)
	@ lin++,00 pSay left("Ordem de Servico: " + VS1->VS1_NUMOSV + " - TpTempo: "+Alltrim(VS1->VS1_TIPTEM)+" - "+VOI->VOI_DESTTE,132)
	lin++
EndIf

@ lin++,00 pSay "********  T O T A I S  ********"
lin++
if MV_PAR03 == 2
	@ lin++,00 pSay "Pecas..........:" + Transform(nTotPec-nTotDesP,"@E 9999,999,999.99")
Else
	@ lin++,00 pSay "Pecas..........:" + Transform(nTotPec,"@E 9999,999,999.99")
Endif	
@ lin++,00 pSay "Servicos.......:" + Transform(nTotSer,"@E 9999,999,999.99")
@ lin++,00 pSay "Frete..........:" + Transform(VS1->VS1_VALFRE,"@E 9999,999,999.99")
@ lin++,00 pSay "Desp.Acessorias:" + Transform(VS1->VS1_DESACE,"@E 9999,999,999.99")

@ lin++,00 pSay left("Orcamento......:" + Transform(nTotal-IIf(MV_PAR03==2,nTotDesP,0),"@E 9999,999,999.99") + "   Cond.Pagto.: " + VS1->VS1_FORPAG + "-" + SE4->E4_DESCRI,132)
lin++
@ lin++,00 pSay "AUTORIZO(AMOS) O FATURAMENTO DESTE ORCAMENTO."
lin++
@ lin++,00 pSay "Local:_______________________________________, Data:______/_____________/_______"
lin++
@ lin++,35 pSay "CARIMBO"
lin++
lin++
@ lin++,35 pSay "Ass.:________________________________________"

Return


//////////////////////////
Static Function LEDriver()

Local aSettings := {}
Local cStr, cLine, i

if !File(__DRIVER)
	aSettings := {"CHR(15)","CHR(18)","CHR(15)","CHR(18)","CHR(15)","CHR(15)"}
Else
	cStr := MemoRead(__DRIVER)
	For i:= 2 to 7
		cLine := AllTrim(MemoLine(cStr,254,i))
		AADD(aSettings,SubStr(cLine,7))
	Next
Endif

Return(aSettings)
              

Static Function FS_TITULO(cTp,lin)
lin++
If cTp == "PEC"
	@ lin++,00 pSay "******** PECAS ********"
	@ lin++,00 pSay repl([=],132)
	@ lin++,00 pSay If(MV_PAR04#1,"Codigo                Descricao                                            ","Descricao    "+Space(62))+"Local              "+"Qtde  Vlr.Unitar"+IIf(MV_PAR03#1,"  Desconto",space(10))+"   Vlr.Total"
Else
	@ lin++,00 pSay "******** SERVICOS ********"
	@ lin++,00 pSay repl([=],132)
	@ lin++,00 pSay "Grp "+If(MV_PAR05#1,"Codigo                 Descricao                                                 ","Descricao         "+Space(63))+" Tipo          Vlr.Servic"+IIf(MV_PAR03#1,"  Desconto",space(10))+"   Vlr.Total"
EndIf
@ lin++,000 pSay repl([=],132)
Return(lin)


/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼敖陳陳陳陳賃陳陳陳陳陳堕陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳堕陳陳賃陳陳陳陳陳娠�
臼�Fun�ao    � FG_Seek  � Autor �Alvaro/Andre           � Data � 05/07/99 咳�
臼団陳陳陳陳津陳陳陳陳陳祖陳陳陳祖陳陳陳陳陳陳陳陳陳陳陳祖陳陳珍陳陳陳陳陳官�
臼�Descri�ao � Posiciona Reg e permanece nele. Atribui Valor a outro Campo咳�
臼団陳陳陳陳津陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳官�
臼�Sintaxe   � (Alias,Chave,Ordem,.t./.f.-p/softseek on/off,CpoDes,CpoOri)咳�
臼団陳陳陳陳津陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳官�
臼�Uso       � Generico                                                   咳�
臼青陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳抉�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
Sintaxe: FG_Seek( <ExpC1>, <ExpC2>, [ExpN], [ExpL], <ExpC3>, <ExpC4> )
Funcao.: Executa pesquisa em tabelas
ExpC1 = arquivo alvo
ExpC2 = chave de pesquisa
ExpN  = numero do indice associado a ExpC1  (Opcional)
Se nao informado assume 1
ExpL  = se .t. softseek ON                  (Opcional)
Se nao informado assume .f.
ExpC3 = Campo Destino (que recebera conteudo)
ExpC4 = Campo Origem do conteudo
Retorna: .t. se o reg. existir, deixando posicionado no mesmo
.f. se o reg. nao existir, deixando posic. no final do Arquivo
*/

Static Function FG_Seek(cAlias,Chv_,Ord_,Sss_,cCpoDest,cCpoOrig)

Local Atu_:=SELECT(),Ind_, Sem_dbf:=ALIAS(), Achou_
Local i := 0

Ord_:=IF(Ord_=NIL,1,Ord_)
Sss_:=IF(Sss_=NIL,.f.,Sss_)
cCom:=IF(cCpoOrig=NIL," ",cAlias+"->"+cCpoOrig)

Select(cAlias)
Ind_:=IndexOrd()
DbSetOrder(Ord_)
Set SoftSeek (Sss_)

if Type("aCols") == "A"    && Modelo 3
	
	cChave := ""
	
	if type(readvar()) == "U"
		cUlCpo := ""
	Else
		cUlCpo := &(readvar())
	Endif
	
	//k > 0 .and. ( Subs(chv_,k-1,1) == "+" .or. Subs(chv_,k-1,1) == "" .or. !SX2->(dbSeek(Subs(chv_,k-2,3))) )
	
	k := at("M->",chv_)
	if k > 0 .and. ( Subs(chv_,k-1,1) == "+" .or. (k-1 == 0) .or. !SX2->(dbSeek(Subs(chv_,k-2,3))) )
		bCampo := {|x| aHeader[x,2]}
		w1 := READVAR()
		For i=1 to Len(aHeader)
			wVar := "M->"+(EVAL(bCampo,i))
			If wVar != w1
				Private &wVar := aCols[n,i]
			Endif
		Next
	Endif
	
	While .t.
		
		k := at("+",chv_)
		if k > 0
			cCPO := substr(chv_,1,k-1)
			chv_ := substr(chv_,k+1)
			if at("->",cCpo) == 0 .and. type(cCpo) == "U"
				cChave := cChave + FieldGet(FieldPos(cCPO))
			else
				cChave := cChave + &cCpo
			endif
		Else
			if !Chv_ == readvar()
				cUlCpo := &Chv_
			endif
			Exit
		Endif
		
	Enddo
	
	cChv_ := cChave+cUlCpo
	
Else
	
	cChv_ := (&Chv_)
	
Endif

DbGotop()
DbSeek(xFilial(cAlias)+cChv_)
Achou_:=FOUND()

DbSetOrder(ind_)
IF Empty(sem_dbf)
	Sele 0
ELSE
	Sele (Atu_)
ENDI

Set SoftSeek (.f.)

if cCom != " "
	M->&cCpoDest := &cCom
endif

RETU Achou_


/*/
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼�Fun��o    �VALIDPERG � Autor � AP5 IDE            � Data �  13/07/01   艮�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
/*/
Static Function ValidPerg

local _sAlias := Alias()
local aRegs := {}
local i,j

dbSelectArea("SX1")
dbSetOrder(1)
cPerg := left(cPerg+space(15),len(SX1->X1_GRUPO))
If DbSeek(cPerg+"01") .and. ( Alltrim(SX1->X1_PERGUNT) <> "Ordem Itens" )
	While !EOF() .and. SX1->X1_GRUPO == cPerg
		RecLock("SX1",.F.,.T.)
		SX1->(dbDelete())
		MsUnLock()
		DbSelectArea("SX1")
		DbSkip()
	EndDo
EndIf
// Grupo/Ordem/Pergunta/Variavel/Tipo/Tamanho/Decimal/Presel/GSC/Valid/Var01/Def01/Cnt01/Var02/Def02/Cnt02/Var03/Def03/Cnt03/Var04/Def04/Cnt04/Var05/Def05/Cnt05
aAdd(aRegs,{cPerg,"01","Ordem Itens"        ,"","","mv_ch1","N",1,0,1,"C","","MV_PAR1","Sequencia","","","","","Grupo+Codigo","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"02","Ordem Servicos"     ,"","","mv_ch2","N",1,0,1,"C","","MV_PAR2","Sequencia","","","","","Tipo de Servico","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"03","Mostra Desconto"    ,"","","mv_ch3","N",1,0,2,"C","","MV_PAR3","Nao","","","","","Sim","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"04","Imprime Cod Item   ","","","mv_ch4","N",1,0,2,"C","","MV_PAR4","Nao","","","","","Sim","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"05","Imprime Cod Servico","","","mv_ch5","N",1,0,2,"C","","MV_PAR5","Nao","","","","","Sim","","","","","","","","","","","","","","","","","","","","","","","","","","",""})

For i:=1 to Len(aRegs)
	If !dbSeek(cPerg+aRegs[i,2])
		RecLock("SX1",.T.)
		For j:=1 to FCount()
			If j <= Len(aRegs[i])
				FieldPut(j,aRegs[i,j])
			Endif
		Next
		MsUnlock()
	Endif
Next

// Altera艫o da Descri艫o do Primeiro Par�metro
If dbSeek(cPerg+aRegs[1,2]) .and. Alltrim(SX1->X1_DEF02) <> "Grupo+Codigo"
	RecLock("SX1",.f.)
	SX1->X1_DEF02 := "Grupo+Codigo"
	MsUnlock()
Endif

dbSelectArea(_sAlias)

Return
