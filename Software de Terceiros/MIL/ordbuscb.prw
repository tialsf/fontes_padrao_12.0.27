// 浜様様様曜様様様様�
// � Versao �   12   �
// 藩様様様擁様様様様�
/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  � ORDBUSCB � Autor � Andre Luis Almeida � Data �  07/10/05   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Desc.     �Imprime Ordem de Busca                                      艮�
臼麺様様様様謡様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様恒�
臼�Uso       � Balcao                                                     艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
User Function ORDBUSCB(cT)

Local bCampo   := { |nCPO| Field(nCPO) }

Private aVetCampos := {}
Private cX := If(cT==Nil,"",cT)
Private cOper := ""

Private lControlaImpressao := .f.
Private lControlaCancela := .f.
Private lImp := .t.

if Type("ParamIXB") != "U"
	cX := ParamIxb[1]
	If Len(ParamIxb) > 1
		cOper := ParamIxb[2]
		if len(ParamIxb) == 3
			aDelet := ParamIxb[3]
		Endif	
	EndIf
Endif

if cX == "OR"
	cX := "O"
endif	

If FunName() <> "OFIOR700" .and. VS3->(FieldPos("VS3_IMPRES")) <> 0 .and. cOper <> "CANCELA" .and. !Empty(cOper)
	lControlaImpressao := .t.
EndIf

If cOper == "CANCELA" .and. VS3->(FieldPos("VS3_IMPRES")) <> 0
	lControlaCancela := .t.
EndIf

If cX # "O" // Pela NF
	DbSelectArea("VS1")
	DbSetOrder(3)
	DbSeek(xFilial("VS1")+SF2->F2_DOC+SF2->F2_SERIE)
	M->VS1_NUMORC := VS1->VS1_NUMORC
Else
	If Type("M->VS1_NUMORC") == "U"
		M->VS1_NUMORC := VS1->VS1_NUMORC
	Endif
Endif

DbSelectArea("VS3")
DbSetOrder(1)
If !DbSeek(xFilial("VS3")+M->VS1_NUMORC)
	Return()
EndIf
lEntrou := .f.
While !Eof() .and. VS3->VS3_FILIAL == xFilial("VS3") .and. VS3->VS3_NUMORC == VS1->VS1_NUMORC
	if VS3->VS3_QTDITE-VS3->VS3_QTDTRA > 0
		lEntrou := .t.
	Endif
	dbSkip()
Enddo
if !lEntrou
	Return()
Endif	
	   	
SetPrvt("cAlias , cNomRel , cGPerg , cTitulo , cDesc1 , cDesc2 , cDesc3 , aOrdem , lHabil , cTamanho , aReturn , ")
SetPrvt("titulo,cabec1,cabec2,nLastKey,wnrel,tamanho")

cAlias  := "VS3"
cNomRel := "ORDB_"+M->VS1_NUMORC+"_"+__cUserId
cGPerg  := ""
cTitulo := "Req. de Pecas NF Balcao"
cDesc1  := "Ordem de Busca"
cDesc2  := cDesc3 := ""
aOrdem  := {}
lHabil  := .f.
//lServer := ( GetMv("MV_LSERVER") == "S" )
cTamanho:= "P"
nLastKey:=0
//aDriver := LeDriver()
//cDrive  := GetMv("MV_DRVORB")
//cPorta  := GetMv("MV_PORORB")
//aReturn := { OemToAnsi("Zebrado"), 1,OemToAnsi("Administracao"), 1, 3, cPorta, "",1 }  //"Zebrado"###"Administracao"
aReturn := { OemToAnsi("Zebrado"), 1,OemToAnsi("Administracao"), 1, 3, 1, "",1 }  //"Zebrado"###"Administracao"
lVS1_OBSNFI := .f.
DbSelectArea("SX3")
DbSetOrder(2)
If DbSeek("VS1_OBSNFI")
	lVS1_OBSNFI := .t.
EndIf

cNomRel := SetPrint(cAlias,cNomRel,nil,@ctitulo,cDesc1,cDesc2,cDesc3,.F.,"",.f.,cTamanho,nil,nil,nil)
//cNomRel := SetPrint(cAlias,cNomRel,nil,@ctitulo,cDesc1,cDesc2,cDesc3,.F.,"",.f.,cTamanho,nil,nil,nil,cDrive,.T.  ,lServer,cPorta)
 
If nlastkey == 27
	Return(.t.)
EndIf

SetDefault(aReturn,cAlias)
RptStatus({|lEnd| lImp := FS_IMPORDBUSC(@lEnd,"OFIM110",'VS3')},Titulo)

If !lImp
	MsgInfo("As pe�as do or�amento j� foram impressas na Ordem de Busca.","Aten艫o")
EndIf

Set Printer to
Set device to Screen

If aReturn[5] == 1 .and. lImp
	OurSpool( cNomRel )
EndIf

MS_FLUSH()

DbSelectArea("VS1")

Return( .t. )

/*
樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛樛�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
臼浜様様様様用様様様様様僕様様様冤様様様様様様様様様曜様様様冤様様様様様様傘�
臼�Programa  �IMPORDBUSC� Autor � Andre Luis Almeida � Data �  07/10/05   艮�
臼麺様様様様謡様様様様様瞥様様様詫様様様様様様様様様擁様様様詫様様様様様様恒�
臼�Desc.     �Imprime Relatorio Ordem de Busca                            艮�
臼麺様様様様謡様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様恒�
臼�Uso       � Balcao                                                     艮�
臼藩様様様様溶様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様識�
臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼臼�
烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝烝�
*/
Static Function FS_IMPORDBUSC()
Local cQuery   := ""
Local cQAlias  := "SQLSDB"
Local aTRB  := {}
Local i := 0

SetPrvt("cbTxt , cbCont , cString , Li , m_Pag , wnRel , cTitulo , cabec1 , cabec2 , nomeprog , tamanho , nCaracter ")

Caracter := 0
nLin := 1
nTotal := 0

cbTxt    := Space(10)
cbCont   := 0
cString  := "VS3"
Li       := 80
m_Pag    := 1
wnRel    := "OFIM110"
nomeprog := "ORDB_"+VS1->VS1_NUMORC+"_"+__cUserId
tamanho  := "P"
nCaracter:= 18
limite   := 80
nTotal   := 0

DbSelectArea("SA3")
DbSetOrder(1)
DbSeek(xFilial("SA3")+VS1->VS1_CODVEN)
DbSelectArea("SA1")
DbSetOrder(1)
DbSeek(xFilial("SA1")+VS1->VS1_CLIFAT+VS1->VS1_LOJA)
cPlaca := space(8)
DbSelectArea("VV1")
DbSetOrder(1)
DbSeek(xFilial("VV1")+VS1->VS1_CHAINT)
cPlaca := Transform(VV1->VV1_PLAVEI,"@R !!!-!!!!")
cabec2 := ""
If GetNewPar("MV_ORDBUST",80) == 80
	If cX # "O" // Pela NF
		cTitulo:= "NF:"+SF2->F2_DOC+"-"+SF2->F2_SERIE+" Orcamento:"+VS1->VS1_NUMORC+"  Dt NF: "+transform(SF2->F2_EMISSAO,"@E 999,999,999.99")
	Else // Pelo Orcamento
		cTitulo:= "Orcamento:"+VS1->VS1_NUMORC
		cabec2 := "Filial Destino: "+VS1->VS1_FILDES
	EndIf
	cabec1 := "Cliente:"+left(VS1->VS1_CLIFAT+"-"+VS1->VS1_LOJA+" "+SA1->A1_NOME,35)+" Vendedor:"+left(SA3->A3_NOME,10) + "  Placa: "+cPlaca
	ccabx  := "  Seq  Codigo                           Descricao     Locacao            Qtde  "
Else
	If cX # "O" // Pela NF
		cTitulo:= "NF:"+SF2->F2_DOC+"-"+SF2->F2_SERIE+" Orcamto:"+VS1->VS1_NUMORC+" Vended:"+left(SA3->A3_NOME,10)+"  Dt NF: "+transform(SF2->F2_EMISSAO,"@E 999,999,999.99")
	Else // Pelo Orcamento
		cTitulo:= "Orcamento:"+VS1->VS1_NUMORC+"   Vendedor:"+left(SA3->A3_NOME,10)
		cabec2 := "Filial Destino: "+VS1->VS1_FILDES
	EndIf
	cabec1 := left("Cliente:"+VS1->VS1_CLIFAT+"-"+VS1->VS1_LOJA+" "+SA1->A1_NOME,31)+ "  Placa: "+cPlaca
	ccabx  := "Codigo                             Qtde Locacao "
EndIf

DbSelectArea("VS3")
DbSetOrder(1)
DbSeek(xFilial("VS3")+VS1->VS1_NUMORC)
While !Eof() .and. VS3->VS3_FILIAL == xFilial("VS3") .and. VS3->VS3_NUMORC == VS1->VS1_NUMORC

	If (lControlaImpressao .and. VS3->VS3_IMPRES == "1") .or. (lControlaCancela .and. VS3->VS3_IMPRES == "0")
		dbSkip()
		Loop
	EndIf
	if Type("ParamIXB") != "U"
		if len(ParamIxb) == 3
			if aScan(aDelet,{ |x| x == VS3->VS3_SEQUEN } ) == 0
				dbSkip()
				Loop
			EndIf
		Endif
	EndIf
	DbSelectArea("SB1")
	DbSetOrder(7)
	DbSeek( xFilial("SB1") + VS3->VS3_GRUITE + VS3->VS3_CODITE )

	If Localiza(SB1->B1_COD)
	
		cQuery := "SELECT SDB.DB_LOCAL, SDB.DB_LOCALIZ, SDB.DB_QUANT FROM "+RetSqlName("SDB")+" SDB WHERE SDB.DB_FILIAL = '"+xFilial("SDB")+"' AND "
		cQuery += "SDB.DB_PRODUTO = '"+SB1->B1_COD+"' AND SDB.DB_DOC = '"+VS3->VS3_DOCSDB+"' AND SDB.DB_QUANT > 0 AND SDB.DB_TM > '500' AND "
		cQuery += "SDB.D_E_L_E_T_ = ' ' ORDER BY SDB.R_E_C_N_O_"
		dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQuery ), cQAlias, .F., .T. )
		
		If !( cQAlias )->( Eof() )
			Do While !( cQAlias )->( Eof() )
				AADD( aTRB , { VS3->VS3_GRUITE+" "+VS3->VS3_CODITE+" "+Substr(SB1->B1_DESC,1,20) , ( cQAlias )->( DB_LOCALIZ ) , ( cQAlias )->( DB_QUANT ) } )
				( cQAlias )->( DbSkip() )
			Enddo
			
		Else
		
			DbSelectArea("SB5")
			DbSetOrder(1)
			DbSeek( xFilial("SB5") + SB1->B1_COD )
			
			if VS3->VS3_QTDITE-VS3->VS3_QTDTRA > 0 
				AADD( aTRB , { VS3->VS3_GRUITE+" "+VS3->VS3_CODITE+" "+Substr(SB1->B1_DESC,1,20) , FM_PRODSBZ(SB1->B1_COD,"SB5->B5_LOCALI2") , VS3->VS3_QTDITE-VS3->VS3_QTDTRA } )
			Endif		
		Endif
		( cQAlias )->( DbCloseArea() )
		
	Else
	
		DbSelectArea("SB5")
		DbSetOrder(1)
		DbSeek( xFilial("SB5") + SB1->B1_COD )
		
		if VS3->VS3_QTDITE-VS3->VS3_QTDTRA > 0 
			AADD( aTRB , { VS3->VS3_GRUITE+" "+VS3->VS3_CODITE+" "+Substr(SB1->B1_DESC,1,20) , FM_PRODSBZ(SB1->B1_COD,"SB5->B5_LOCALI2") , VS3->VS3_QTDITE-VS3->VS3_QTDTRA } )
		Endif		
	Endif	
	
	DbSelectArea("VS3")
	DbSkip()
	
EndDo

If Len(aTRB) == 0
	Return .f.
EndIf

aSort( aTRB ,,,{|x,y| x[2] + Left(x[1],32) < y[2] + Left(y[1],32) })

Set Printer to &cNomRel
Set Printer On
Set device to Printer
       
cPerg := NIL

If GetNewPar("MV_ORDBUST",80) == 80
	nLin := cabec(ctitulo,cabec1,cabec2,nomeprog,tamanho,nCaracter) + 1
	@ nLin++,0 PSAY ccabx
	For i := 1 to Len(aTRB)
		if nLin > 60
			nLin := cabec(ctitulo,cabec1,cabec2,nomeprog,tamanho,nCaracter) + 1
			@ nLin++,0 PSAY ccabx
		endif
		@ nLin++,0 PSAY "  "+StrZero(i,3)+"  "+Left(aTRB[i,1],45)+"  "+aTRB[i,2]+Transform(aTRB[i,3],"@E 99999.99")
	Next i
	if nLin > 60
		nLin := cabec(ctitulo,cabec1,cabec2,nomeprog,tamanho,nCaracter) + 1
		@ nLin++,0 PSAY ccabx
	Endif
	If lVS1_OBSNFI
		If	!Empty(VS1->VS1_OBSNFI)
			nLin++
			@ nLin++,002 PSAY "OBS: "+VS1->VS1_OBSNFI
		EndIf
	EndIf
	nLin += 2
	@ nLin++,0 PSAY "       ________________________               ________________________        "
	@ nLin++,0 PSAY "              Atendente                             Retirado por              "
Else
	@ nLin++,0 PSAY CHR(15)+" "
	nLin++
	@ nLin++,0 PSAY cTitulo
	@ nLin++,0 PSAY cabec1
	@ nLin++,0 PSAY cabec2
	nLin++
	@ nLin++,0 PSAY ccabx
	For i := 1 to Len(aTRB)
		@ nLin++,0 PSAY left(aTRB[i,1],31)+Transform(aTRB[i,3],"@E 99999.99")+" "+left(aTRB[i,2],8)
		@ nLin++,4 PSAY substr(aTRB[i,1],33,30)
	Next i
	nLin += 2
	@ nLin++,0 PSAY "____________________        ____________________"
	@ nLin++,0 PSAY "     Atendente                  Retirado por    "
	nLin += val(substr(str(GetNewPar("MV_ORDBUST",4010),4),3,2))
	@ nLin++,0 PSAY " "
EndIf

Return .t.
