#INCLUDE "veiva300.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � VEIVA300 � Autor �  Manoel               � Data � 06/06/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Cadastro de Tabelas de Preco                               ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Generico                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function VEIVA300

//��������������������������������������������������������������Ŀ
//� Define o cabecalho da tela de atualizacoes                   �
//����������������������������������������������������������������

PRIVATE cCadastro := OemToAnsi(STR0001)  //Cadastro de Tabelas de Preco

//��������������������������������������������������������������Ŀ
//� Endereca a funcao axCadastro                                 �
//����������������������������������������������������������������
dbSelectArea("VZ0")

axCadastro("VZ0", cCadastro)

Return .T.
