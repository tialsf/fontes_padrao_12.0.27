#Include "Protheus.ch"
#Include "Folder.ch"
#Include "Fileio.ch"
#Include "VEIVR190.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � VEIVR190 � Autor � Manoel             � Data �  06/06/2005 ���
�������������������������������������������������������������������������͹��
���Desc.     � Lista Estoque de Veiculos Novos com Tabelas de Precos	  ���
�������������������������������������������������������������������������͹��
���Uso       � Concessionarias                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function VEIVR190()
// A linha com a variavel cFunJEst estava com comentario ocasionando errorlog
// Fnc 2009/2010 - boby 08/04/10
Private cFunJEst  := GetMv("MV_FUNJEST") // Nome da Funcao que Calcula o Juros de Estoque	
Private cAlias    := "VV1"
Private cDesc1    := STR0001             // Tabelas de Precos de Venda de Veiculos Novos	
Private cDesc2    := ""
Private cDesc3    := ""
Private aRegistros:= {}
Private nLin      := 0
Private Tamanho   := "P"                 // P/M/G
Private Limite    := 132                 // 80/132/220
Private cTitulo   := STR0001             // Tabelas de Precos de Venda de Veiculos Novos	
Private Titulo    := STR0001             // Tabelas de Precos de Venda de Veiculos Novos	
Private cNomeProg := "VEIVR190"
Private cNomeRel  := "VEIVR190"
Private nLastKey  := 0
Private cGruVei   := GetMv("MV_GRUVEI")+space(4-len(GetMv("MV_GRUVEI")))
Private nCaracter := 15
Private cabec1    := ""
Private cabec2    := ""
Private lAbortPrint := .f.
Private cString   := "VV1"
Private Li        := 80
Private m_Pag     := 1
Private wnRel     := "VEIVR190"
Private nPos      := 0
Private ni        := 0
Private cStrCab   := ""
Private aReturn   := { OemToAnsi(STR0003), 1,OemToAnsi(STR0002), 1, 1, 1, "",2 }		//1-ZEBRADO,2-,3-ADMINISTRACAO,4-1:COMPACTA,2:NAO,5-MIDIA 1:DISCO,6-CRYSTAL,7-,8-ORDEM
cTipFat           := "0"

//////////////////////////////////////////////
//antes de remover verificar no ATUSX       //
//FNC-25465 - 15/10/09 - Rafael Goncalves   //
//colocado o campo formula como obrigatorio //
//////////////////////////////////////////////
DbSelectArea("SX3")
DbSetOrder(2)
if DbSeek("VV1_RENAVA")
	cUsado  := SX3->X3_USADO
	cReserv := SX3->X3_RESERV
	if DbSeek("VZ0_FORMUL")
		RecLock("SX3",.F.)
		SX3->X3_USADO := cUsado
		SX3->X3_RESERV := cReserv
		MsUnLock()
	EndIf
EndIf
///////////////////////////////////////////////

cPerg := "PVR190"

ValidPerg()

If !pergunte(cPerg,.t.)
	Return
Else
	Processa( {|| FS_Imprime() } )
Endif

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FS_IMPRIME� Autor � Manoel             � Data �  17/06/2005 ���
�������������������������������������������������������������������������͹��
���Desc.     � Impressao dos Dados de Estoque de Veiculos 			      ���
�������������������������������������������������������������������������͹��
���Uso       � Concessionarias                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function FS_Imprime()
//Local cIndFun:= GetMv("MV_INDFUND")
Local l        := 0
Local m        := 0
Local _i_      := 0
Local cTrans   := ""
Local nQtdMod  := 0
Local nQtdTot  := 0
Local nValMod  := 0
Local nValTot  := 0
Local cForExc  := Space(05)
Local nForExc  := 0
Local nJurCorr := 0
Local nVVG_DIACAR:=0

Private cQAlVV1:= "SQLVV1"
Private cQuery := ""

// Armazena dados da impressao
aVetor := {}
//  1o. Elemento - Modelo e Descricao do Modelo
//  2o. Elemento - Cor
//  3o. Elemento - Combustivel
//  4o. Elemento - Chassi
//  5o. Elemento - Ano / Modelo
//  6o. Elemento - Opcionais
//  7o. Elemento - Localizacao
//  8o. Elemento - Situacao de Localizacao
//  9o. Elemento - Valor 1
// 10o. Elemento - Valor 2
// 11o. Elemento - valor 3
// 12o. Elemento - dias no estoque
// 13o. Elemento - NF Fabrica
// 14o. Elemento - Data de Emissao
// 15o. Elemento - Juros Estoque + Correcao
// 16o. Elemento - Indice de calculo
// 17o. Elemnto  - Dias de Carencia
// 18o. Elemento - Dias para Vencer a Carencia
// 19o. Elemento - Situa��o do ve�culo

Cabec1  := ""
Cabec2  := ""
lErroTab:= .f.
wnrel   := "VEIVR190"
wnrel   := SetPrint(cString,wnrel,,@titulo,cDesc1,cDesc2,cDesc3,.F.,,.T.,Tamanho)

If nLastKey == 27
	Set Filter to
	Return
Endif

Set Printer to &wnrel
Set Printer On
Set Device  to Printer

SetDefault(aReturn,cString)

If nLastKey == 27
	Set Filter To
	Return
Endif

li         := 80
m_pag      := 1
If Mv_Par02 == 3 // Administrador
	Tamanho := "G"
	limite  := 132
Else
	aDriver := LeDriver()
	cCompac := aDriver[1]
	cNormal := aDriver[2]
	Tamanho := "G"
	limite  := 132
Endif

nTipo    := 15
nCaracter:= 15
Caracter := 15
nomeprog := "VEIVR190"
Titulo   := STR0001 //Tabelas de Precos de Venda de Veiculos Novos
nValTab1 := 0
nValTab2 := 0
nValTab3 := 0

//dbSelectArea("VV1")
//DbSetOrder(4)
//dbseek(xFilial("VV1")+"0")

cQuery := "SELECT VV1.VV1_CHAINT , VV1.VV1_CHASSI , VV1.VV1_SITVEI , VV1.VV1_ESTVEI , VV1.VV1_NUMTRA , VV1.VV1_TRACPA , VV1.VV1_CODMAR , VV1.VV1_MODVEI , VV1.VV1_CORVEI , VV1.VV1_FABMOD , VV1.VV1_OPCFAB , VV1.VV1_COMVEI , VV1.VV1_DESLOC "
cQuery += "FROM "+RetSqlName("VV1")+" VV1 WHERE VV1.VV1_FILIAL='"+xFilial("VV1")+"' "
If Empty(mv_par03)
	cQuery += "AND VV1.VV1_SITVEI IN ('0','2','3') "
Else
	cQuery += "AND VV1.VV1_SITVEI IN ('0','1','2','3','4','5','6') "
EndIf
cQuery += "AND VV1.VV1_ESTVEI = '0' AND VV1.D_E_L_E_T_ = ' ' ORDER BY VV1.VV1_CHAINT "
dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQuery ), cQAlVV1 , .F., .T. )

nTot := RecCount()
ProcRegua( nTot )

Mv_Par01 := alltrim(Mv_Par01)

//while !eof() .and. VV1->VV1_FILIAL == xFilial("VV1")
Do While !( cQAlVV1 )->( Eof() )
	IncProc(STR0004)		// Aguarde... Lendo Estoque de Veiculos Novos...
	lPriVez := .t.
	
	If Empty(mv_par03)
		
		DbSelectArea("SB1")
		DbSetOrder(7)
		DbSeek(xFilial("SB1")+cGruVei+( cQAlVV1 )->VV1_CHAINT)
		DbSelectArea("SB5")
		DbSetOrder(1)
		DbSeek(xFilial("SB5")+SB1->B1_COD)
		
		If ( cQAlVV1 )->VV1_SITVEI == "0" .and. SB5->B5_LOCALIZ <> "T"
			cTrans := SB5->B5_LOCALIZ
		EndIf
		If ( cQAlVV1 )->VV1_SITVEI == "2"
			cTrans := "T"
		EndIf
		
	Else
		
		DbSelectArea("VVF")
		DbSetOrder(1)
		If !DbSeek(xFilial("VVF")+( cQAlVV1 )->VV1_TRACPA)
			( cQAlVV1 )->(DbSkip())
			Loop
		EndIf
		If DTOS(VVF->VVF_DATMOV) > DTOS(MV_PAR03)
			( cQAlVV1 )->(DbSkip())
			Loop
		EndIf
		
		DbSelectArea("VV0")
		DbSetOrder(1)
		If DbSeek(xFilial("VV0")+( cQAlVV1 )->VV1_NUMTRA)
			If VV0->VV0_OPEMOV <> "0"
				( cQAlVV1 )->(DbSkip())
				Loop
			EndIf
			
			If DTOS(VV0->VV0_DATEMI) <= DTOS(MV_PAR03) .and. !empty(VV0->VV0_NUMNFI)
				( cQAlVV1 )->(DbSkip())
				Loop
			EndIf
		EndIf
		DbSelectArea("VZK")
		DbSetOrder(1)
		If DbSeek(xFilial("VZK")+( cQAlVV1 )->VV1_CHASSI)
			If DTOS(MV_PAR03) >= DTOS(VZK->VZK_DATENT) .AND. DTOS(MV_PAR03) < DTOS(VZK->VZK_DATSAI)
				cTrans := "T"
			Else
				DbSelectArea("SB1")
				DbSetOrder(7)
				DbSeek(xFilial("SB1")+cGruVei+( cQAlVV1 )->VV1_CHAINT)
				DbSelectArea("SB5")
				DbSetOrder(1)
				DbSeek(xFilial("SB5")+SB1->B1_COD)
				If !Empty(SB5->B5_LOCALIZ)
					cTrans := SB5->B5_LOCALIZ
				Else
					If ( cQAlVV1 )->VV1_SITVEI == "0"
						cTrans := "E"
					EndIf
					If ( cQAlVV1 )->VV1_SITVEI == "2"
						cTrans := "T"
					EndIf
				EndIf
			EndIf
		Else
			DbSelectArea("SB1")
			DbSetOrder(7)
			DbSeek(xFilial("SB1")+cGruVei+( cQAlVV1 )->VV1_CHAINT)
			DbSelectArea("SB5")
			DbSetOrder(1)
			DbSeek(xFilial("SB5")+SB1->B1_COD)
			cTrans := SB5->B5_LOCALIZ
		EndIf
	EndIf
	cStrCab := ""
	If Len(Mv_Par01) > 3
		nTotLen := 3
	Else
		nTotLen := Len(Mv_Par01)
	Endif
	
	dbSelectArea("VVG")
	DbSetOrder(1)
	dbseek(xFilial("VVG")+( cQAlVV1 )->VV1_TRACPA+( cQAlVV1 )->VV1_CHAINT)
	dbSelectArea("VVF")
	DbSetOrder(1)
	dbseek(xFilial("VVF")+VVG->VVG_TRACPA)
	
	cOpcional := ""
	for l := 1 to nTotLen
		cpt :=subs(Mv_Par01,l,1)
		nValTab&cpt := FG_CalTbPrc(cpt)
		If lErroTab
			Return
		Endif
		cStrCab += "       "+Left(VZ0->VZ0_NOMTAB,10)
	Next
	cStrCab := Rtrim(cStrCab)
	
	dbSelectArea("VV2")
	DbSetOrder(1)
	dbseek(xFilial("VV2")+( cQAlVV1 )->VV1_CODMAR+( cQAlVV1 )->VV1_MODVEI)
	dbSelectArea("VVC")
	DbSetOrder(1)
	dbseek(xFilial("VVC")+( cQAlVV1 )->VV1_CODMAR+( cQAlVV1 )->VV1_CORVEI)
	cCombus := FS_Combust()
	cAnoMod := subs(( cQAlVV1 )->VV1_FABMOD,3,2)+"/"+subs(( cQAlVV1 )->VV1_FABMOD,7,2)
	nTamOpc := Alltrim(( cQAlVV1 )->vv1_opcfab)
	cOpcion := ""
	If at("/",( cQAlVV1 )->VV1_OPCFAB) > 0
		for m := 1 to Len(nTamOpc)/4
			cOpcion += subs(( cQAlVV1 )->vv1_opcfab,m*4-3,4)+"/"
		Next
	Else
		for m := 1 to Len(nTamOpc)/3
			cOpcion += subs(( cQAlVV1 )->vv1_opcfab,m*3-2,3)+"/"
		Next
	ENdif
	cOpcion := cOpcion + cOpcional
	cOpcion := subs(cOpcion,1,Len(cOpcion)-1)
	
	nRecVV1 := VV1->(recno())
	nCustoVVG     := VVG->VVG_VCNVEI
	M->VVA_VCAVEI := FG_CusVei(( cQAlVV1 )->VV1_TRACPA,( cQAlVV1 )->VV1_CHAINT,VVF->VVF_DATEMI,dDataBase)
	nJurCorr      := M->VVA_VCAVEI - nCustoVVG
	
	If nJurCorr == 0
		aAreaSF1 := SF1->(Getarea())
		aAreaSE2 := SE2->(Getarea())
		SF1->(dbSetOrder(1))
		SF1->(dbSeek(xFilial("SF1")+VVF->VVF_NUMNFI+VVF->VVF_SERNFI+VVF->VVF_CODFOR+VVF->VVF_LOJA))
		SE2->(dbSetOrder(6))
		if SE2->(dbSeek(xFilial("SE2")+VVF->VVF_CODFOR+VVF->VVF_LOJA+VVF->VVF_SERNFI+VVF->VVF_NUMNFI))
			if SE2->E2_BAIXA # ctod("  /  /  ") .and. (dDataBase - SE2->E2_BAIXA) > 0  //baixado
				If ExistBlock(cFunJEst)   // Se existir este PRW, entao ele sera usado
					nJurCorr := ExecBlock(cFunJEst,.f.,.f.,{( cQAlVV1 )->VV1_TRACPA,( cQAlVV1 )->VV1_CHAINT,SE2->E2_BAIXA,dDataBase,'V'})
				Else
					nJurCorr := FG_JurEst(VV1->VV1_TRACPA,VV1->VV1_CHAINT,SE2->E2_BAIXA,dDataBase,"V")
				Endif
			endif
		else  //nao encontrei o titulo -
			If ExistBlock(cFunJEst)   // Se existir este PRW, entao ele sera usado
				nJurCorr := ExecBlock(cFunJEst,.f.,.f.,{( cQAlVV1 )->VV1_TRACPA,( cQAlVV1 )->VV1_CHAINT,VVF->VVF_DATEMI,dDataBase,'V'})
			Else
				nJurCorr := FG_JurEst(( cQAlVV1 )->VV1_TRACPA,( cQAlVV1 )->VV1_CHAINT,VVF->VVF_DATEMI,dDataBase,"V")
			Endif
		endif
		SF1->(RestArea(aAreaSF1))
		SE2->(RestArea(aAreaSE2))
	Endif
	
	nVVG_DIACAR:=Posicione("VVH",1,xFilial("VVH")+VVG->VVG_CODIND,"VVH_DIACAR")
	
	If Empty(mv_par03)
		If Mv_Par02 == 1 .or. Mv_Par02 == 3// Gerente ou Administrador
			aadd(aVetor,{left(( cQAlVV1 )->VV1_MODVEI,16)+" "+Left(VV2->VV2_DESMOD,25),PadL(VVC->VVC_DESCRI,11),cCombus,PadR(( cQAlVV1 )->VV1_CHASSI,16," "),cAnoMod,cOpcion,Left(cTrans,1),left(( cQAlVV1 )->VV1_DESLOC,10),nValTab1,nValTab2,nValTab3,dDataBase-VVF->VVF_DATEMI,VVF->VVF_NUMNFI+"-"+VVF->VVF_SERNFI,Dtoc(VVF->VVF_DATEMI),IIf(nJurCorr==Nil,nJurCorr:=0,nJurCorr),VVG->VVG_CODIND,nVVG_DIACAR,VVF->VVF_DATEMI+nVVG_DIACAR-dDataBase,( cQAlVV1 )->VV1_SITVEI})
		Else
			aadd(aVetor,{left(( cQAlVV1 )->VV1_MODVEI,16)+" "+Left(VV2->VV2_DESMOD,25),;
			             PadL(VVC->VVC_DESCRI,11),;
			             cCombus,;
			             PadR(( cQAlVV1 )->VV1_CHASSI,16," "),;
			             cAnoMod,cOpcion,Left(cTrans,1),;
			             left(( cQAlVV1 )->VV1_DESLOC,10),;
			             nValTab1,;
			             nValTab2,;
			             nValTab3,;
			             dDataBase-VVF->VVF_DATEMI,;
			             VVF->VVF_NUMNFI+"-"+VVF->VVF_SERNFI,;
			             Dtoc(VVF->VVF_DATEMI),;
			             IIf(nJurCorr==Nil,nJurCorr:=0,nJurCorr),;
			             VVG->VVG_CODIND,;
			             nVVG_DIACAR,;
			             VVF->VVF_DATEMI+nVVG_DIACAR-dDataBase,;
			             ( cQAlVV1 )->VV1_SITVEI})
		Endif
	Else
		If Mv_Par02 == 1 .or. Mv_Par02 == 3
			aadd(aVetor,{left(( cQAlVV1 )->VV1_MODVEI,16)+" "+Left(VV2->VV2_DESMOD,25),PadL(VVC->VVC_DESCRI,11),cCombus,PadR(( cQAlVV1 )->VV1_CHASSI,16," "),cAnoMod,cOpcion,Left(cTrans,1),"          ",                     nValTab1,nValTab2,nValTab3,mv_par03-VVF->VVF_DATMOV,VVF->VVF_NUMNFI+"-"+VVF->VVF_SERNFI,Dtoc(VVF->VVF_DATEMI),IIf(nJurCorr==Nil,nJurCorr:=0,nJurCorr),VVG->VVG_CODIND,nVVG_DIACAR,VVF->VVF_DATEMI+nVVG_DIACAR-mv_par03,( cQAlVV1 )->VV1_SITVEI})
		Else
			aadd(aVetor,{left(( cQAlVV1 )->VV1_MODVEI,16)+" "+Left(VV2->VV2_DESMOD,25),PadL(VVC->VVC_DESCRI,11),cCombus,PadR(( cQAlVV1 )->VV1_CHASSI,16," "),cAnoMod,cOpcion,Left(cTrans,1),"          ",                     nValTab1,nValTab2,nValTab3,mv_par03-VVF->VVF_DATMOV,VVF->VVF_NUMNFI+"-"+VVF->VVF_SERNFI,Dtoc(VVF->VVF_DATEMI),IIf(nJurCorr==Nil,nJurCorr:=0,nJurCorr),VVG->VVG_CODIND,nVVG_DIACAR,VVF->VVF_DATEMI+nVVG_DIACAR-dDataBase,( cQAlVV1 )->VV1_SITVEI})

		Endif
	EndIf
	DbSelectArea("VV1")
	DbSetOrder(4)
	DbGoTo(nRecVV1)
	( cQAlVV1 )->(dbSkip())
Enddo
( cQAlVV1 )->(dbCloseArea())

aSort(aVetor,,,{|x,z|  x[1]+x[2]+strzero(x[12],10)+x[6] < z[1]+z[2]+strzero(z[12],10)+z[6] })

If Mv_Par02 == 3 // Administrador
	//	cCabec1 := "Ind Dias NF Fabrica DatEmiss Modelo    Cor    C Chassi   AnoMod Lc Situac  Jur+Corr"+cStrCab//  Tabela 1   Tabela 2   Tabela 3"
	//	cCabec2 := "--- ---- ---------- -------- ------ --------- - -------- ------ -- ------- --------"        //---------- ---------- ----------
     //    "    Ind Dias  NF Fabrica DatEmiss Modelo                    Cor    C Chassi   AnoMod Lc Situacao   Jur+Corr  "
    //          xx  99999 999999-xxx 99/99/99 xxxxxx                 xxxxxxxxx x xxxxxxxx  99/99 x  xxxxxxx    9,999.99 999,999.99 999,999.99 999,999.99

	cCabec1 := STR0005 +cStrCab//  Tabela 1   Tabela 2   Tabela 3"  ### //Ind Dias  NF Fabrica DatEmiss Modelo                    Cor    C Chassi   AnoMod Lc Situacao   Jur+Corr  "
	cCabec2     := "    --- ----- ----------    -------- ----------------------                     ---------   - --------         ------ -- ----------    --------- "        //---------- ---------- ----------
	//				    123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
	//							10			 20		  30        40        50        60        70        80        90       100       110       120       130
ElseIf Mv_Par02 == 1 // Gerente
	//	cCabec1 := "Ind Dias  Modelo                    Cor    C Chassi   AnoMod Lc Jur+Corr  "+cStrCab//  Tabela 1   Tabela 2   Tabela 3"
	//	cCabec2 := "--- ----- ---------------------- --------- - -------- ------ -- --------- "        //---------- ---------- ----------
	//				xx  99999 xxxxxx xxxxxxxxx x xxxxxxxx  99/99 xx 999,999.99 999,999.99 999,999.99
	//				123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
	//							10			 20		  30        40        50        60        70        80        90       100       110       120       130
	cCabec1 := STR0017
	cCabec2 := STR0018
	cCabec3 := STR0006 +cStrCab//  Tabela 1   Tabela 2   Tabela 3"  ### // Ind Dias  Modelo                    Cor    C Chassi   AnoMod Lc Situacao   Jur+Corr
	cCabec4 := "    --- ----- ----------------------                     ---------  - --------         ------   -- ---------- ---------  ---------       -------------"

Else // Vendedor
	//				9999 xxxxxx xxxxxxxxxxxxxxx xxxxxxxxxxx x xxxxxxxxxxxxxxxxx  99/99 xxx/xxx/xxx/xxx  x xxxxxxxxxx 999,999.99 999,999.99 999,999.99
	//				123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
	//							10			 20		  30        40        50        60        70        80        90       100       110       120       130
	cCabec1 := STR0007 +cStrCab//  Tabela 1   Tabela 2   Tabela 3"  ### //Dias  Modelo                     Cor     C Chassi            AnoMod Lc Situacao
	cCabec2 := "    ----- ----------------------                     ----------- - ----------------- ------ -- ---------- "        //---------- ---------- ----------
Endif
If len(aVetor) > 0
	cMod := aVetor[1,01]
EndIf
nQtdMod := 0
nQtdTot := 0
nValMod := 0
nValTot := 0
nQtdSim := 0
nQtdA30 := 0
nQtdTra := 0
nValTra := 0   //Roberto Carlos - 03/11/2009 Altera��o para trazer o valor total dos ve�culos em tr�nsito
nExcCar := 0
nValA30 := 0
nValCar := 0
nValSim := 0
nForExc := 0
cSemCar := ""
lSemCar := .F.
nQtdApo := 0
nValApo := 0

cabec(titulo,cabec1,cabec2,nomeprog,tamanho,nTipo)
If Mv_Par02 == 1
	@li,00  PSAY cCabec1
	li:=li+1
	@li,00  PSAY cCabec2
	li:=li+1
	@li,00  PSAY cCabec3
	li:=li+1
	@li,00  PSAY cCabec4
	li:=li+2
Else
	@li,00  PSAY cCabec1
	li:=li+1
	@li,00  PSAY cCabec2
	li:=li+2
EndIf

For _i_ := 1 to Len(aVetor)
	lSemCar := .F.
	If cMod != aVetor[_i_,01]
		//   	  If nQtdMod > 1
		//   	     li++
		//		     @ li,00 psay Transform(nQtdMod,"@EZ 999")+" "+STR0009+" "+cMod
		//		     @ li++,86 psay Transform(nValMod,"@E 999,999,999.99")
		//		  Endif
		nQtdMod := 0
		nValMod := 0
		cMod    := aVetor[_i_,01]
//		Li++
	Endif
	
	nQtdMod ++
	nQtdTot ++
	nValMod += aVetor[_i_,09]
	nValTot += aVetor[_i_,09]
	
	If !Empty(aVetor[_i_,08])
		nQtdSim ++
		nValSim += aVetor[_i_,09]
	Endif
	
	If aVetor[_i_,12] > 30
		nQtdA30 ++
		nValA30 += aVetor[_i_,09]
	Endif
	
	If aVetor[_i_,19] == "2" .OR. aVetor[_i_,7] == "T"// Transito
		nQtdTra ++
		nValTra += aVetor[_i_,09] //Roberto Carlos - 03/11/2009 Altera��o para o trazer valor total dos ve�culos em tr�nsito
	Endif
	If aVetor[_i_,17] > 0
		nDiaCar := aVetor[_i_,17]
		If aVetor[_i_,12] < aVetor[_i_,17]
			nExcCar ++
			nValCar += aVetor[_i_,09] //Roberto Carlos - 03/11/2009 Altera��o para o trazer valor total dos ve�culos que n�o excederam o prazo de car�ncia.
		EndIf
	EndIf
	
	If avetor[_i_,18] < 0
		lSemCar := .T.
		cForExc := "S/CAR"
	Else
		nForExc :=  avetor[_i_,18]
	EndIf
	If aVetor[_i_,16] == "  "
		cIndi := "--"
	Else
		cIndi := aVetor[_i_,16]
	EndIf
	//If avetor[_i_,16] == cIndFun+" "
	//   nQtdApo ++
	//   nValApo += aVetor[_i_,09]
	//EndIf
	
	cIt := strzero(_i_,3)
	If Subs(Mv_Par01,1,1) == "1"
		cImpPrTab := Transform(aVetor[_i_,09],"@E 9999,999.99")
	ElseIf Subs(Mv_Par01,1,1) == "2"
		cImpPrTab := Transform(aVetor[_i_,10],"@E 9999,999.99")
	ElseIf Subs(Mv_Par01,1,1) == "3"
		cImpPrTab := Transform(aVetor[_i_,11],"@E 9999,999.99")
	Endif
	If Subs(Mv_Par01,2) == "1"
		cImpPrTab += " " + Transform(aVetor[_i_,09],"@E 9999,999.99")
	ElseIf Subs(Mv_Par01,2,1) == "2"
		cImpPrTab += " " + Transform(aVetor[_i_,10],"@E 9999,999.99")
	ElseIf Subs(Mv_Par01,2,1) == "3"
		cImpPrTab += " " + Transform(aVetor[_i_,11],"@E 9999,999.99")
	Endif
	If Subs(Mv_Par01,3) == "1"
		cImpPrTab += " " + Transform(aVetor[_i_,09],"@E 9999,999.99")
	ElseIf Subs(Mv_Par01,3,1) == "2"
		cImpPrTab += " " + Transform(aVetor[_i_,10],"@E 9999,999.99")
	ElseIf Subs(Mv_Par01,3,1) == "3"
		cImpPrTab += " " + Transform(aVetor[_i_,11],"@E 9999,999.99")
	Endif

 
	If Mv_Par02 == 1 // Gerente
		
		If lSemCar == .T.
	                   //                           /  dias no estoque 08       / Modelo e Descricao do Modelo  21
                       //  Cor   09                 /  Combustivel              / Chassi    09
                       //  Ano / Modelo  05         /  Localizacao              / Situacao de Localizacao       10
                       // Juros Estoque + Correcao  
                       //                           /  Opcionais
	   		@ li++,00 PSAY cIt  + " " + cIndi + " " + Transform(aVetor[_i_,12],"@E 99999") + " "+ aVetor[_i_,01] + " " +;
		               aVetor[_i_,02] + " "      + aVetor[_i_,03]                       + " " + aVetor[_i_,04] + "  " +;
		               aVetor[_i_,05] + "   "    + aVetor[_i_,07]                       + " " + aVetor[_i_,08] + " " +;
		               Transform(aVetor[_i_,15],"@E 999,999.99")   + "   " + cForExc  + "       "+ ;
		               cImpPrTab      + "           " + aVetor[_i_,06] 

		Else
	                   //                           /  dias no estoque 08       / Modelo e Descricao do Modelo  21
                       //  Cor   09                 /  Combustivel              / Chassi    09
                       //  Ano / Modelo  05         /  Localizacao              / Situacao de Localizacao       10
                       // Juros Estoque + Correcao  
                       //                           /  Opcionais
	   		@ li++,00 PSAY cIt  + " " + cIndi + " " + Transform(aVetor[_i_,12],"@E 99999") + " "+ aVetor[_i_,01] + " " +;
		               aVetor[_i_,02] + " "      + aVetor[_i_,03]                       + " " + aVetor[_i_,04] + "  " +;
		               aVetor[_i_,05] + "   "    + aVetor[_i_,07]                       + " " + aVetor[_i_,08] + " " +;
		               Transform(aVetor[_i_,15],"@E 999,999.99") + "   "    + Transform(nForExc,"@E 99999")+ "       "+ ;
		               cImpPrTab      + "           " + aVetor[_i_,06] 
		EndIf
	ElseIf Mv_Par02 == 2 // Vendedor
 	                   //                           /  dias no estoque 08       / Modelo e Descricao do Modelo  21
                       //  Cor   09                 /  Combustivel              / Chassi    09
                       //  Ano / Modelo  05         /  Localizacao              / Situacao de Localizacao       10
                       //                           /  Opcionais
		@ li++,00 PSAY cIt            + " "      + Transform(aVetor[_i_,12],"@E 99999") + " "+ aVetor[_i_,01] + " " +;
		               aVetor[_i_,02] + " "      + aVetor[_i_,03]                       + " " + aVetor[_i_,04] + "  " +;
		               aVetor[_i_,05] + "   "    + aVetor[_i_,07]                       + " " + aVetor[_i_,08] + "    " +;
		               cImpPrTab      + "             " + aVetor[_i_,06] 
		
	ElseIf Mv_Par02 == 3 // Administrador
		
		@ li++,00 PSAY cIt + " " + cindi+"  "+Transform(aVetor[_i_,12],"@E 99999")+" "+aVetor[_i_,13]+" "+aVetor[_i_,14]+" "+aVetor[_i_,01]+" "+aVetor[_i_,02]+" "+aVetor[_i_,03]+" "+aVetor[_i_,04]+"  "+aVetor[_i_,05]+" "+;
		aVetor[_i_,07]+" "+aVetor[_i_,08]+" "+Transform(aVetor[_i_,15],"@E 999,999.99")+"      "+cImpPrTab + "       " + aVetor[_i_,06]//+Transform(aVetor[_i_,09],"@E 999,999.99")+;
	Endif
	If Mv_Par02 == 1
		If li > 62
			cabec(titulo,cabec1,cabec2,nomeprog,tamanho,nTipo)
			@li,00  PSAY cCabec1
			li:=li+1
			@li,00  PSAY cCabec2
			li:=li+1
			@li,00  PSAY cCabec3
			li:=li+1
			@li,00  PSAY cCabec4
			li:=li+2
		EndIf
	Else
		If li > 62
			cabec(titulo,cabec1,cabec2,nomeprog,tamanho,nTipo)
			@li,00  PSAY cCabec1
			li:=li+1
			@li,00  PSAY cCabec2
			li:=li+2
		EndIf
	Endif
Next

li:=li+2
@ li,00 psay Transform(nQtdTot,"@EZ 9999")+" "+STR0010
@ li,86 psay Transform(nValTot,"@E 999,999,999.99")

li:=li+1
If nQtdTra == 0
	@ li,00 psay "   0"+" "+STR0011
Else
	@ li,00 psay Transform(nQtdTra,"@EZ 9999")+" "+STR0011
EndIf
@ li,86 psay Transform(nValTra,"@E 999,999,999.99")//Roberto Carlos - 03/11/2009 Altera��o para trazer o valor total dos ve�culos em tr�nsito
li:=li+1
If Empty(mv_par03)
	@ li,00 psay Transform(nQtdSim,"@EZ 9999")+" "+STR0012
	@ li,86 psay Transform(nValSim,"@E 999,999,999.99")
	li:=li+1
EndIf
@ li,00 psay Transform(nQtdA30,"@EZ 9999")+" "+STR0013
@ li,86 psay Transform(nValA30,"@E 999,999,999.99")
li:=li+1
@ li,00 psay Transform(nExcCar,"@EZ 9999")+" "+STR0016
@ li,86 psay Transform(nValCar,"@E 999,999,999.99")
li:=li+1

ms_flush()

Set Printer to
set Device  to Screen

If aReturn[5] == 1
	Set Printer TO
	dbCommitAll()
	ourspool(wnrel)
Endif

Return .t.



/*
�����������������������������������������������������������������������������
���Fun��o    �VALIDPERG � Autor � AP5 IDE            � Data �  13/07/01   ���
�����������������������������������������������������������������������������
*/
Static Function ValidPerg
local _sAlias := Alias()
local aRegs := {}
local i,j
dbSelectArea("SX1")
dbSetOrder(1)
cPerg := Padr("PVR190",len(SX1->X1_GRUPO))
//se o valid estiver em branco deleta,para que a pergunte seja criada com o valid
//FNC - 25465
if dbseek(cPerg+"01") .and. alltrim(SX1->X1_VALID)==''
	while !eof() .and. SX1->X1_GRUPO == cPerg
		reclock("SX1",.f.,.t.)
		dbdelete()
		msunlock()
		DBSelectArea("SX1")
		DbSkip()
	enddo
endif

// Grupo/Ordem/Pergunta/Variavel/Tipo/Tamanho/Decimal/Presel/GSC/Valid/Var01/Def01/Cnt01/Var02/Def02/Cnt02/Var03/Def03/Cnt03/Var04/Def04/Cnt04/Var05/Def05/Cnt05
aAdd(aRegs,{cPerg,"01","Visualiza Tabelas?","","","mv_ch1","C",3,0,0,"G","naovazio","Mv_Par01","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"02","Listagem para    ?","","","mv_ch2","N",1,1,0,"C","","Mv_Par02","Gerente","","","","","Vendedor","","","","","Administrador","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"03","Data             ?","","","mv_ch3","D",8,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","",""})
For i:=1 to Len(aRegs)
	If !dbSeek(cPerg+aRegs[i,2])
		RecLock("SX1",.T.)
		For j:=1 to FCount()
			If j <= Len(aRegs[i])
				FieldPut(j,aRegs[i,j])
			Endif
		Next
		MsUnlock()
	Endif
Next
dbSelectArea(_sAlias)
Return


Static Function FS_Combust()

cRet := "-"

If Left(( cQAlVV1 )->VV1_COMVEI,1) == "0"
	cRet := "G"
ElseIf Left(( cQAlVV1 )->VV1_COMVEI,1) == "1"
	cRet := "A"
ElseIf Left(( cQAlVV1 )->VV1_COMVEI,1) == "2"
	cRet := "D"
ElseIf Left(( cQAlVV1 )->VV1_COMVEI,1) == "3"
	cRet := "N"
ElseIf Left(( cQAlVV1 )->VV1_COMVEI,1) == "4"
	cRet := "F"
Endif

Return(cRet)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FG_CALTBPRC� Autor � Manoel             � Data �  07/06/2005���
�������������������������������������������������������������������������͹��
���Desc.     � Calcula Precos das Tabelas de Veiculoe          			  ���
�������������������������������������������������������������������������͹��
���Uso       � Concessionarias                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function Fg_CalTbPrc(cParTab)
Local f  := 0
Local m  := 0
Local p_ := 0

nValIC  := 0 // Valor dos Itens
nValPG  := 0 // Valor do  % da Gerencia
nValPD  := 0 // Valor do  % de Desconto
nValBN  := 0 // Valor do  Bonus Nacional
nValBR  := 0 // Valor do  Bonus Regional
nValBC  := 0 // Valor do  Bonus da Concessionaria

DbSelectArea("VZ0")
DbSetOrder(1)
If !DbSeek(xFilial("VZ0")+cParTab)
	MsgStop("Nao existe tabela de preco cadastrada com o codigo "+cParTab)
	lErroTab := .t.
	return .f.
Endif

nCont    := 0//nValor1 := ( cQAlVV1 )->VV1_SUGVDA
cExprVal := ""
cExprFor := alltrim(VZ0->VZ0_FORMUL)
nTotFor  := Len(cExprFor)
for f := 1 to nTotFor
	nPos := at("\F",cExprFor)
	If nPos > 0
		If nPos == 1
			cExprVal := cExprVal+"FG_Formula('"+subs(cExprFor,3,6)+"')"
			nCont += 8
		Else
			cExprVal := cExprVal+Subs(cExprFor,1,nPos-1)+"FG_Formula('"+subs(cExprFor,nPos+2,6)+"')"
			nCont += (nPos-1)+8
		Endif
	Else
		cExprVal := cExprVal+Subs(cExprFor,1)
		nCont += (len(cExprFor))
	Endif
	If nCont >= nTotFor
		exit
	Else
		cExprFor := Subs(cExprFor,nPos+8)
	Endif
Next

nValEqp := 0
dbSelectArea("VVT")
dbSetOrder(1)
dbSeek(xFilial("VVT")+( cQAlVV1 )->VV1_CHAINT)
While VVT->VVT_CHAINT == ( cQAlVV1 )->VV1_CHAINT .and. !eof() .and. VVT->VVT_FILIAL == xFilial("VVT")
	nValEqp += VVT->VVT_VALEQP
	If lPriVez
		cOpcional += Left(VVT->VVT_CODOPC,3)+"/"
	Endif
	dbSelectarea("VVT")
	dbSkip()
Enddo

If nTotFor<=0
	MsgInfo(STR0014,STR0015) //Formula da tabela de pre�o n�o cadastrada.-Aten��o
	//	Help("  ",1,"M160PROABO")
	lRet := .f.
	Break
	return(.t.)
EndIf

lPriVez := .f.
nValor1 := &(cExprVal) + nValEqp

If !Empty(VZ0->VZ0_ACAOVD)
	
	DbSelectArea("VV2")
	dbSetOrder(1)
	DbSeek( xFilial("VV2") + ( cQAlVV1 )->VV1_CODMAR + ( cQAlVV1 )->VV1_MODVEI )
	
	DbSelectArea("VZ5")
	dbSetOrder(2)
	If DbSeek( xFilial("VZ5") + ( cQAlVV1 )->VV1_CODMAR + VV2->VV2_GRUMOD + ( cQAlVV1 )->VV1_MODVEI + ( cQAlVV1 )->VV1_CHASSI )
		while !eof() .and. xFilial("VZ5")==VZ5->VZ5_FILIAL .and. ( VZ5->VZ5_CODMAR + VZ5->VZ5_GRUMOD + VZ5->VZ5_MODVEI + VZ5->VZ5_CHASSI ) == ( ( cQAlVV1 )->VV1_CODMAR + VV2->VV2_GRUMOD + ( cQAlVV1 )->VV1_MODVEI + ( cQAlVV1 )->VV1_CHASSI )
			
			If cTipFat == "2"  //Faturamento Direto
				dDatAcao := dDataBase
			Else
				If VZ5->VZ5_DATVER == "0" // data da Venda
					dDatAcao := dDataBase
				Else //If VZ5->VZ5_DATVER == "1" // data de emissao na Fabrica
					dDatAcao := VVF->VVF_DATEMI
				Endif
			Endif
			
			If dDatAcao >= VZ5->VZ5_DATINI .and. dDatAcao <= VZ5->VZ5_DATFIN .and. If(Empty(VZ5->VZ5_FABMOD),.t.,Alltrim(( cQAlVV1 )->VV1_FABMOD) == Alltrim(VZ5->VZ5_FABMOD))
				nValPG += ((VZ5->VZ5_PERGER*nValor1)/100)
				nValPD += ((VZ5->VZ5_PERDES*nValor1)/100)
				If VZ5->(FieldPos("VZ5_BONFAB"))>0
					nValBN += VZ5->VZ5_BONFAB
				EndIf
				nValBR += VZ5->VZ5_BONREG
				nValBC += VZ5->VZ5_BONCON
				DbSelectArea("VZ6")
				dbSetOrder(1)
				dbSeek(xFilial("VZ6")+VZ5->VZ5_CODACV)
				While !eof() .and. VZ6->VZ6_FILIAL+VZ6->VZ6_CODACV == xFilial("VZ6")+VZ5->VZ5_CODACV
					nValIC += VZ6->VZ6_VALITE
					DbSkip()
				Enddo
			Endif
			DbSelectArea("VZ5")
			DbSkip()
		Enddo
		cAcao  := alltrim(VZ0->VZ0_ACAOVD)
		cExpr  := "nValor1"
		for m := 1 to Len(cAcao)/3
			cExpr1 := subs(cAcao,m*3-2,3)
			cExpr  += subs(cExpr1,1,1)+"nVal"+subs(cExpr1,2,2)
		Next
		nValor1 := &cExpr
	Else
		
		If DbSeek( xFilial("VZ5") + ( cQAlVV1 )->VV1_CODMAR + VV2->VV2_GRUMOD + ( cQAlVV1 )->VV1_MODVEI )
			
			while !eof() .and. xFilial("VZ5")==VZ5->VZ5_FILIAL .and. ( VZ5->VZ5_CODMAR + VZ5->VZ5_GRUMOD + VZ5->VZ5_MODVEI ) == ( ( cQAlVV1 )->VV1_CODMAR + VV2->VV2_GRUMOD + ( cQAlVV1 )->VV1_MODVEI )
				
				If cTipFat == "2"  //Faturamento Direto
					dDatAcao := dDataBase
				Else
					If VZ5->VZ5_DATVER == "0" // data da Venda
						dDatAcao := dDataBase
					Else //If VZ5->VZ5_DATVER == "1" // data de emissao na Fabrica
						dDatAcao := VVF->VVF_DATEMI
					Endif
				Endif
				
				If dDatAcao >= VZ5->VZ5_DATINI .and. dDatAcao <= VZ5->VZ5_DATFIN .and. If(Empty(VZ5->VZ5_FABMOD),.t.,alltrim(( cQAlVV1 )->VV1_FABMOD) == Alltrim(VZ5->VZ5_FABMOD)) .and. Empty(VZ5->VZ5_CHASSI)
					lVai := .t.
					nTtLen := Len(Alltrim(VZ5->VZ5_OPCION))/3
					nAchouOpc := 0
					for p_ := 1 to nTtLen
						cOpcion := subs(VZ5->VZ5_OPCION,p_*3-2,3)
						If at(cOpcion,Alltrim(( cQAlVV1 )->vv1_opcfab)) == 0
							lVai := .f.
							p_ := nTtLen+1
						Endif
					Next
					If !lVai
						DbSelectArea("VZ5")
						DbSkip()
						Loop
					Endif
					nValPG += ((VZ5->VZ5_PERGER*nValor1)/100)
					nValPD += ((VZ5->VZ5_PERDES*nValor1)/100)
					If VZ5->(FieldPos("VZ5_BONFAB"))>0
						nValBN += VZ5->VZ5_BONFAB
					EndIf
					nValBR += VZ5->VZ5_BONREG
					nValBC += VZ5->VZ5_BONCON
					DbSelectArea("VZ6")
					dbSetOrder(1)
					dbSeek(xFilial("VZ6")+VZ5->VZ5_CODACV)
					While !eof() .and. VZ6->VZ6_FILIAL+VZ6->VZ6_CODACV == xFilial("VZ6")+VZ5->VZ5_CODACV
						nValIC += VZ6->VZ6_VALITE
						DbSkip()
					Enddo
				Endif
				DbSelectArea("VZ5")
				DbSkip()
			Enddo
			cAcao  := alltrim(VZ0->VZ0_ACAOVD)
			cExpr  := "nValor1"
			for m := 1 to Len(cAcao)/3
				cExpr1 := subs(cAcao,m*3-2,3)
				If Type(subs(cExpr1,1,1)+"nVal"+subs(cExpr1,2,2)) != "U"
					cExpr  += subs(cExpr1,1,1)+"nVal"+subs(cExpr1,2,2)
				Else
					MsgInfo(STR0008 +": "+VZ0->VZ0_NOMTAB)  //Existe um erro na Definicao da Tabela de Preco
					lErroTab := .t.
					Return 0
				Endif
			Next
			nValor1 := &cExpr
			
		Endif
		
	Endif
	
Endif

nValor := nValor1

return(nValor)
