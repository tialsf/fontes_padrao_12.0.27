#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "MNTA082.CH"

#DEFINE _nVERSAO 2

//------------------------------------------------------------------------------
/*/{Protheus.doc} MNTA082
Cadastro de Veiculos TMS

@author NG Inform�tica Ltda.
@since 01/01/2015
@version P12
@return Nil
/*/
//------------------------------------------------------------------------------
Function MNTA082
Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados da rotina

@author NG Inform�tica Ltda.
@since 01/01/2015
@version P12
@return oModel
/*/
//------------------------------------------------------------------------------
Static Function ModelDef

	Local oModel := FWLoadModel( 'MNTA084' )
	Local aFldNoCopy, aRelation, aUnique, oModelAux, oStructAux, cAlias

	//--------------------------------------------------------------------------
	// Bem
	//--------------------------------------------------------------------------
	cAlias := 'ST9'

	// Cria a estrutura a ser usada no Modelo de Dados
	oStructAux := FWFormStruct( 1, cAlias )

	oStructAux:SetProperty( 'T9_CATBEM' , MODEL_FIELD_INIT, FwBuildFeature( STRUCT_FEATURE_INIPAD,'"2"' ) )
	oStructAux:SetProperty( 'T9_CODTMS' , MODEL_FIELD_OBRIGAT, .T. )

	// Remove campos
	oStructAux:RemoveField("T9_CODESTO")
	oStructAux:RemoveField("T9_NOMESTQ")
	oStructAux:RemoveField("T9_LOCPAD")
	oStructAux:RemoveField("T9_CAPMAX")
	oStructAux:RemoveField("T9_MEDIA")

	// Define campos que n�o podem ser copiados
	aAdd(aFldNoCopy,'T9_ESTRUTU')
	aAdd(aFldNoCopy,'T9_STATUS' )

	// Retorna componente especifico
	oModelAux := oModel:GetModel( oModel:cId + '_' + cAlias )
	oModelAux:SetStruct( oStructAux )

	// Adiciona lista de campos ao modelo
	oModelAux:SetFldNoCopy( aFldNoCopy )

	//--------------------------------------------------------------------------
	// TMS
	//--------------------------------------------------------------------------
	cAlias := 'DA3'

	// Cria a estrutura a ser usada no Modelo de Dados
	oStructAux := FWFormStruct( 1, cAlias )

	// Define relacionamento entre ST9 e DA3
	aRelation := {}
	aAdd( aRelation , { 'DA3_FILIAL' , 'xFilial( "DA3" )' } )
	aAdd( aRelation , { 'DA3_CODBEM' , 'T9_CODBEM' } )

	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	oModel:AddFields( oModel:cId + '_' + cAlias , oModel:cId + '_ST9' , oStructAux )

	oModelAux := oModel:GetModel( oModel:cId + '_' + cAlias )

	// Faz relaciomaneto entre os compomentes do model
	oModelAux:SetRelation( aRelation , ( cAlias )->( IndexKey( 5 ) ) )

	// Adiciona a descricao do Componente do Modelo de Dados
	oModelAux:SetDescription( NgSX2Nome( cAlias ) )

Return oModel

//------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Interface da rotina

@author NG Inform�tica Ltda.
@since 01/01/2015
@version P12
@return oView
/*/
//------------------------------------------------------------------------------
Static Function ViewDef

	Local aWhenBtn, oStructAux, cAlias
	Local oModel := FWLoadModel( 'MNTA082' )
	Local oView	 := FWLoadView( 'MNTA084' )

	oView:SetModel( oModel )

	//--------------------------------------------------------------------------
	// TMS
	//--------------------------------------------------------------------------
	cAlias := 'DA3'

	// Cria a estrutura a ser usada no Modelo de Dados
	oStructAux := FWFormStruct( 2 , cAlias )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	oView:AddField( 'VIEW_' + cAlias , oStructAux , oModel:cId + '_' + cAlias )

	//Adiciona um titulo para o grid
	oView:EnableTitleView('VIEW_' + cAlias , STR0001 ) // "Informa��es de Integra��o TMS"

	// Cria pastas nas folders
	oView:AddSheet( 'MAIN_FOLDER', 'ABA_' + cAlias , STR0002 ) // "Integra��o TMS"

	// Criar "box" horizontal para receber algum elemento da view
	oView:CreateHorizontalBox( 'BOX_' + cAlias , 100,,, 'MAIN_FOLDER', 'ABA_' + cAlias )

	// Relaciona o identificador (ID) da View com o "box" para exibi��o
	oView:SetOwnerView( 'VIEW_' + cAlias , 'BOX_' + cAlias )

Return oView