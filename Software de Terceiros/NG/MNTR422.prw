#Include 'MNTR422.ch'
#Include 'Protheus.ch'


//---------------------------------------------------------------------
/*/{Protheus.doc} MNTR422
Relat�rio de ordem de servi�o por Etapas
@author William Rozin Gaspar
@author Maria Elisandra de Paula
@since 09/05/2014
/*/
//---------------------------------------------------------------------
Function MNTR422(cParam)

	Local aNGBEGINPRM := NGBEGINPRM()

	Local i
	Local wnrel   := "MNTR422"
	Local cDesc1  := STR0001 // "Relat�rio de Ordem de Servi�o por Etapas"
	Local cString := "ST9"

	Private aReturn   := {STR0020 , 1,STR0021, 1, 2, 1, "",1 } // "Zebrado" #"Administracao"
	Private titulo    := STR0001 // "Relat�rio de Ordem de Servi�o por Etapas"
	Private ntipo     := 0
	Private nLastKey  := 0
	Private cPerg     := "MNTR422", aPerg := {}
	Private cAliasQry := GetNextAlias()
	Private cNumOSX   := ""

	Default cParam := ""

	cNumOSX := cParam

	If !AliasInDic("TVT")
		MsgInfo(STR0002) //"Tabela TVT n�o consta no dicion�rio, favor contatar o administrador do sistema."
	Else
		Processa({|lEnd| MNT421IMP()})
	EndIf
	NGRETURNPRM(aNGBEGINPRM)
Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} MNT421IMP

@author William Rozin Gaspar
@since 09/05/2014
/*/
//---------------------------------------------------------------------
Static Function MNT421IMP()

	Local lImp 	:= .F.
	Local cCode := ""
	Local i := 0
	Local nAuxCode 	:= 0
	Local cEmpresa 	:= Alltrim(SM0->M0_NOME)
	Local cFilAtu	:= ""
	Local cFilDesc 	:= ""
	Local nOb := 0
	Local lGrupo	:= .F.
	Local nContLinha:= 0
	Local nLinBarra	:= 0
	Local lMemo 	:= .f.
	Local cEtapaMemo:= ""
	Local nT := 0
	Local cContDif 	:= ""
	Local cEtapa	:= ""
	Local lImpChassi := .f. // indica se chassi j� foi impresso

    //Variaveis do relatorio
	Private oPrint
	Private Lin := 9999

    //Definicao de Fontes
	Private cFonte   := "Courier new"
	Private oFont10  := TFont():New(cFonte,12,10,,.F.,,,,.F.,.F.)
	Private oFont12  := TFont():New(cFonte,12,12,,.F.,,,,.F.,.F.)
	Private oFont14  := TFont():New(cFonte,12,14,,.F.,,,,.F.,.F.)
	Private oFont14b := TFont():New(cFonte,12,14,,.T.,,,,.F.,.F.)
	Private oFont18  := TFont():New(cFonte,12,18,,.T.,,,,.F.,.F.)

	If !ProcTable()
		MsgStop(STR0004,STR0003) // "N�o existem etapas para esta OS." # "Aten��o"
		Return .F.
	EndIf

    //Inicializa Objeto
	oPrint := FWMSPrinter():New(OemToAnsi(titulo))
	/*
	Valores para imprimir o relat�rio corretamente mesmo que usu�rio selecione outra configura��o
	Orienta��o Retrato, folha A4 , tamanhos referente a orienta��o retrato pois SetPortrait apresentou problemas
	*/

	oPrint:SetPortrait() //Retrato
	oPrint:SetMargin(50,50,50,50)// nEsquerda, nSuperior, nDireita, nInferior
	oPrint:setPaperSize(9)//A4
	oPrint:nFactorHor	:= 3.87096774
	oPrint:nFactorVert	:= 3.61643836
	oPrint:nPageHeight	:= 3168
	oPrint:nPageWidth	:= 2400

	Somalinha(3)
	dbSelectArea(cAliasQry)
	dbGoTop()

	dbSelectArea(cAliasQry)
	dbGoTop()
	While !EoF()
		i++
		nContLinha++
		lImp := .T.
		lMemo := .f.
		lImpChassi := .f.
		cCode := (cAliasQry)->TJ_ORDEM + (cAliasQry)->TJ_PLANO + (cAliasQry)->TQ_TAREFA + (cAliasQry)->TQ_ETAPA

		//para calcular a coluna para impress�o da descri��o da barra (24 � o tamanho m�ximo com coluna 1840)


		nTamCode :=  24 - Len(RTRIM(cCode))
		nColCcode := 1840 + (nTamCode * 15)

		if nContLinha == 4
			nContLinha := 1
		EndIf

		If nContLinha == 1
			nLinBarra := 6.5
		ElseIf nContLinha == 2
			nLinBarra := 27.3
		Else
			nLinBarra := 48.0
		Endif

		nColBarra := 23

		If (cAliasQry)->T9_CATBEM == '2'
			dbSelectArea("DA3")
			dbSetOrder(1)
			If dbSeek(xFilial("DA3")+ (cAliasQry)->T9_CODTMS)
				cFilAtu := DA3->DA3_FILBAS
			EndIf
		Else
			If NGSX2MODO("ST9") == "E"
				cFilAtu := (cAliasQry)->T9_FILIAL
			EndIf
		EndIf

		If !Empty(cFilAtu)
			If Len(NGSEEKSM0(cEmpAnt + cFilAtu ,{"M0_NOMECOM"})) > 0
				cFilDesc := NGSEEKSM0(cEmpAnt + cFilAtu ,{"M0_NOMECOM"})[1]
			EndIf
		EndIf

		If i == 1
			cContDif := ContDif((cAliasQry)->TJ_ORDEM)
		EndIf
		cEtapa := STR0025 + Alltrim(NGSEEK("TPA", (cAliasQry)->TQ_ETAPA, 1, "TPA_DESCRI"))


		Somalinha(3)
		oPrint:Say(Lin , 10  , cEmpresa , oFont14b)
		oPrint:Say(Lin , 1050 , STR0005 , oFont14b) //"ETAPA DA ORDEM DE SERVI�O"


		SomaLinha(3)
		SomaLinha(3)

		oPrint:Say(Lin , 10  , STR0006 , oFont10) //"BEM..............:"
		oPrint:Say(Lin , 300  , (cAliasQry)->TJ_CODBEM , oFont18)

		oPrint:FWMSBAR("CODE128" /*cTypeBar*/,nLinBarra /*nRow*/ ,nColBarra/*nCol*/, cCode/*cCode*/,oPrint/*oPrint*/,.T./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02/*nWidth*/,0.8/*nHeigth*/,.f./*lBanner*/,cFonte/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,0.001/*nPFWidth*/,0.001/*nPFHeigth*/,.t./*lCmtr2Pix*/)

		SomaLinha(3)

		SomaLinha(3)
		oPrint:Say(Lin , 10  , STR0007 , oFont10) //"O.S.............:"
		oPrint:Say(Lin , 300  , (cAliasQry)->TJ_ORDEM  , oFont18)



		SomaLinha(3)
		oPrint:Say(Lin , 10 ,  STR0008     , oFont10) //"Centro de Custo..:"
		oPrint:Say(Lin , 300 , Alltrim((cAliasQry)->TJ_CCUSTO)  + " - " + Substring(Alltrim(NGSEEK("CTT",(cAliasQry)->TJ_CCUSTO , 1, "CTT_DESC01")),1,20) , oFont10)
       	oPrint:Say(Lin , nColCcode  , cCode , oFont10)

		If !Empty(cFilDesc)
			oPrint:Say(Lin,1050,STR0009 + Substr(cFilDesc,1,35),oFont10) //"Filial:"
		ElseIf !Empty((cAliasQry)->T9_CHASSI)
			oPrint:Say(Lin,1050,STR0010 + (cAliasQry)->T9_CHASSI , oFont10) //"Chassi:"
			lImpChassi:= .t.
		EndIf

		SomaLinha(3)

		oPrint:Say(Lin,10, STR0022,oFont10)//"Fam�lia"
		oPrint:Say(Lin,300,Alltrim((cAliasQry)->T9_CODFAMI) + " - " + Alltrim(NGSEEK("ST6", (cAliasQry)->T9_CODFAMI, 1, "T6_NOME")) , oFont10)

		If !Empty((cAliasQry)->T9_CHASSI) .and. !lImpChassi
			oPrint:Say(Lin,1050,STR0010 + (cAliasQry)->T9_CHASSI, oFont10) //"Chassi: "
		EndIf

		SomaLinha(3)
		oPrint:Say(Lin , 10  , 		STR0011 , oFont10) //"Modelo...........:"
		oPrint:Say(Lin , 300  , Alltrim((cAliasQry)->T9_TIPMOD) + " - " + 	Alltrim(NGSEEK("TQR", (cAliasQry)->T9_TIPMOD, 1, "TQR_DESMOD")) , oFont10)

		SomaLinha(3)

		If !Empty((cAliasQry)->T9_FABRICA)
			oPrint:Say(Lin , 10  , 		STR0012       , oFont10) //"Fabricante.......:"
			oPrint:Say(Lin , 300  , Alltrim((cAliasQry)->T9_FABRICA) + " - " +	Alltrim(NGSEEK("ST7", (cAliasQry)->T9_FABRICA, 1, "T7_NOME")) , oFont10)
       	EndIf
		SomaLinha(3)
		oPrint:Line( Lin, 10, Lin, 2210)

		SomaLinha(3)
		oPrint:Say(Lin , 10   , STR0013 , oFont10) //"Contador Acumuldado"
		oPrint:Say(Lin , 300  , STR0014 , oFont10) //"Contador"
		oPrint:Say(Lin , 540  , STR0015 , oFont10) //"Tipo Servi�o"
		oPrint:Say(Lin , 1060 , STR0016 , oFont10) //"Entrada"
		oPrint:Say(Lin , 1415 , STR0017 , oFont10) //"Previs�o de Sa�da"

		SomaLinha(3)

		oPrint:Say(Lin , 10   , cValtoChar((cAliasQry)->T9_CONTACU) , oFont10) // "Contador Acumulado"

		oPrint:Say(Lin , 300  , cContDif , oFont10)//Contador
		oPrint:Say(Lin , 540  , SubStr(Alltrim(NGSEEK("ST4", (cAliasQry)->TJ_SERVICO, 1, "T4_NOME")),1,20) , oFont10)// Tipo Servi�o

		dbSelectArea("TTI")
		dbSetOrder(1)
		If dbSeek(xFilial("TTI") + cEmpAnt + cFilAnt + cEmpAnt + (cAliasQry)->T9_FILIAL +  (cAliasQry)->TJ_CODBEM + "1")
			oPrint:Say(Lin , 1060  , DtoC(TTI->TTI_DTENT) + " " + TTI->TTI_HRENT , oFont10)//Entrada

		EndIf
		oPrint:Say(Lin , 1415 , DtoC(StoD((cAliasQry)->TJ_DTMPFIM)) + " " + (cAliasQry)->TJ_HOMPFIM, oFont10) //Previs�o Sa�da

		SomaLinha(3)
		oPrint:Line( Lin, 10, Lin, 2210)
		oPrint:Line( Lin, 1060, Lin+250, 1060) // linha vertical para divis�o do ret�ngulo

      	SomaLinha(3)

       // verifica a quantidade de linhas campo observa��o  - m�ximo 5
       	nRecSTQ := (cAliasQry)->RECSTQ
		aArea := GetArea()
       	dbSelectArea("STQ")
		dbGoTo(nRecSTQ)
		cMemoObs := MemoLine(STQ->TQ_OBSERVA)
		nOb := MlCount(STQ->TQ_OBSERVA,72,,.T.)

       If !EmptY(cMemoObs)
		   If nOb > 5
       		nOb := 5
       	EndIf
		Else
			nOb := 0
       EndIf

       // verifica a quantidade de linhas campo etapa  - m�ximo 3
       nEta := MlCount(cEtapa,65,,.T.)
		If nEta > 3
			nEta := 3
		EndIf

       /*
       Primeira linha de ret�ngulo -> C�digo do grupo.C�digo subgrupo.tarefa (ou somente tarefa.ETAPA)
       */
		lGrupo := .F.
		dbSelectArea("TVS")
		dbSetOrder(4)
		If dbSeek(xFilial("TVS") + (cAliasQry)->TQ_TAREFA + (cAliasQry)->TQ_ETAPA)
			lGrupo := .T.
			oPrint:Say(Lin , 10   , Alltrim(TVS->TVS_TAREFA) + "." + Alltrim(TVS->TVS_GRUPO) + "." + Alltrim(TVS->TVS_ETAPA), oFont10)
		Else
			oPrint:Say(Lin , 10   , Alltrim((cAliasQry)->TQ_TAREFA) + "." + Alltrim(STQ->TQ_ETAPA), oFont10)
		EndIf

		oPrint:Say(Lin , 1410 , STR0018, oFont14)  //"O B S E R V A � � E S"

		For nT := 1 to 5
			SomaLinha(3)

			If nT == 1   //linha 2 imprime descri��o da tarefa
				If lGrupo
					oPrint:Say(Lin , 10  ,STR0023 + Alltrim(NGSEEK("TT9", TVS->TVS_TAREFA, 1, "TT9_DESCRI")) , oFont12)
				Else
					oPrint:Say(Lin , 10  ,STR0023 + Alltrim(NGSEEK("TT9", (cAliasQry)->TQ_TAREFA , 1, "TT9_DESCRI")), oFont12)
				EndIf
			Else
				If nT == 2 // linha 3 imprime descri��o do grupo
					If lGrupo
						oPrint:Say(Lin , 10  ,STR0024 + Alltrim(NGSEEK("TVR", TVS->TVS_GRUPO, 2, "TVR_NOME"))  , oFont12)
					EndIf
				Else

					If nT <= (nEta + 2)

						cEtapaMemo := Padr(MemoLine(cEtapa,65,nT-2,,.t.),65)
	    				oPrint:Say(Lin,10,cEtapaMemo , oFont10)
					EndIf
				EndIf
			EndIf

			If nOb != 0
				cLast := Padr(MemoLine(STQ->TQ_OBSERVA,72,nT,,.t.),72)
				If nOb == nT
					cLast := RTrim(cLast)
				Endif

				oPrint:Say(Lin , 1070, cLast, oFont10)
			EndIf
		Next

		RestArea(aArea)

		oPrint:Say(Lin+50 , 10 , Replicate("-",142) , oFont10)

		Lin += 200
		nAuxCode++

       dbSelectArea(cAliasQry)
		dbSkip()

		If Mod(i, 3) == 0 .And. !Eof()
			Somalinha(0 ,.T.)
			nAuxCode := 0
		EndIf

	EndDo
    //Next nCount

	If lImp
		oPrint:EndPage()
        //oPrint:Print()
		oPrint:Preview()
	Else
		oPrint:EndPage()
		MsgStop(STR0019 ,STR0003) // "N�o existem dados para montar o relat�rio."# "Aten��o"
	Endif
	MS_FLUSH()

Return

//---------------------------------------------------------------------
/*/{Protheus.doc} Somalinha

@author William Rozin Gaspar
@since 09/05/2014
/*/
//---------------------------------------------------------------------
Static Function Somalinha( nQtdLin , lEndPage , nLimite )

	Local nLin := 12

	Default nQtdLin := 1
	Default lEndPage := .F.
	Default nLimite := 2950

	nLin := nLin * nQtdLin
	Lin += nLin
	If lin > nLimite .Or. lEndPage
		oPrint:EndPage()
		oPrint:StartPage()
		lin := 200
	EndIf

Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} ProcTable

@author William Rozin Gaspar
@since 09/05/2014
/*/
//---------------------------------------------------------------------
Static Function ProcTable()

	Local cQuery := ""
	Local lRet := .F.

	cQuery := "SELECT DISTINCT TJ_ORDEM,  "
	cQuery += "       TJ_CODBEM ,  TJ_PLANO, "
	cQuery += "       TJ_CCUSTO, "
	cQuery += "       TJ_SERVICO,"
	cQuery += "       TJ_DTMPFIM,"
	cQuery += "       TJ_HOMPFIM,"
	cQuery += "       TQ_ETAPA,  "
	cQuery += "       T9_CODFAMI, "
	cQuery += "       T9_CODTMS, "
	cQuery += "       T9_CATBEM, "
	cQuery += "       T9_CHASSI, "
	cQuery += "       T9_FILIAL , "
	cQuery += "       T9_FABRICA,"
	cQuery += "       T9_TIPMOD, "
	cQuery += "       T9_CONTACU,"
	cQuery += "       TQ_TAREFA, "
	cQuery += "       STQ.R_E_C_N_O_ AS RECSTQ"
	cQuery += "  FROM "+ RetSqlName("STJ") +" STJ "
	cQuery += "  JOIN "+ RetSqlName("ST9") +" ST9 "
    cQuery += "       ON ST9.T9_FILIAL = " + ValToSql(xFilial("ST9"))
	cQuery += "      AND STJ.TJ_CODBEM = ST9.T9_CODBEM "
	cQuery += "  JOIN "+ RetSqlName("STQ") +" STQ "
    cQuery += "       ON STQ.TQ_FILIAL = " + ValToSql(xFilial("STQ"))
	cQuery += "      AND STJ.TJ_ORDEM  = STQ.TQ_ORDEM "
	cQuery += " WHERE STJ.TJ_FILIAL    = " + ValToSql(xFilial("STJ"))
    cQuery += "   AND STJ.TJ_ORDEM     = " + ValToSql(cNumOSX)
	cQuery += "   AND STJ.D_E_L_E_T_   <> '*'"
	cQuery += "   AND ST9.D_E_L_E_T_   <> '*'"
	cQuery += "   AND STQ.D_E_L_E_T_   <> '*'"

	cQuery :=  ChangeQuery(cQuery)

	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery),cAliasQry, .F., .T.)

	If !EoF()
		lRet := .T.
	EndIf
Return lRet
//---------------------------------------------------------------------
/*/{Protheus.doc} ContDif
Fun��o que retorna a diferen�a do contador atual com o da �ltima manuten��o
ou �ltima OS, dependendo do tipo da manuten��o

@author William Rozin Gaspar
@since 09/12/2014
/*/
//---------------------------------------------------------------------
Static Function ContDif(cOrdem)
	Local aArea := GetArea()
	Local cRet := ""
	Local cTpServ := ""
	Local cServ := STJ->TJ_SERVICO
	Local cBem := STJ->TJ_CODBEM
	Local nPoscont := 0
	Local nDif := 0

	Local cAliasQry := GetNextAlias()
	Local cQuery := ''


	/*
	Diferen�a do km atual com a da �ltima manuten��o (caso preventivo)
	ou diferen�a de km atual com a �ltima O.S com o servi�o (caso corretiva)
	*/

	dbSelectArea("ST9")
	dbSetOrder(1)
	If dbSeek( xFilial("ST9") + STJ->TJ_CODBEM)
		nPoscont := ST9->T9_CONTACU
	EndIf

	dbSelectArea("STE")
	dbSetOrder(1)
	If dbSeek(xFilial("STE") + STJ->TJ_TIPO)
		cTpServ  := STE->TE_CARACTE
	EndIf

	If cTpServ == "C" // CORRETIVA

		cQuery := "SELECT TJ_POSCONT FROM " + RetSqlName("STJ")
		cQuery += " WHERE TJ_FILIAL = " + ValToSql(xFilial("ST9"))
		cQuery += " AND TJ_CODBEM = " + ValToSql(STJ->TJ_CODBEM)
		cQuery += " AND TJ_SERVICO = " + ValToSql(STJ->TJ_SERVICO)
		cQuery += " AND TJ_DTORIGI || TJ_HORACO1 <"  + ValToSql(DtoS(STJ->TJ_DTORIGI) + STJ->TJ_HORACO1)
		cQuery += " AND TJ_SITUACA <> 'C' "
		cQuery += " AND D_E_L_E_T_ <> '*' "
		cQuery += " ORDER BY TJ_DTORIGI || TJ_HORACO1 DESC "

		cQuery :=  ChangeQuery(cQuery)
		dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery),cAliasQry, .F., .T.)

		If !Eof()
			nDif :=  nPoscont - (cAliasQry)->TJ_POSCONT
		Else
			nDif := 0
		EndIf

		cRet := cValtoChar(nDif)

		(cAliasQry)->(dbCloseArea())

	ElseIf cTpServ == "P"//PREVENTIVA

		//Buscar o ultimo contado realizado na manuten��o
		dbSelectArea("STF")
		dbSetOrder(1) //TF_FILIAL+TF_CODBEM+TF_SERVICO+TF_SEQRELA
		If dbSeek( xFilial("STF") + STJ->TJ_CODBEM + STJ->TJ_SERVICO + STJ->TJ_SEQRELA)
			If STF->TF_TIPACOM <> "T"
				nDif := ( nPoscont - STF->TF_CONMANU )
			EndIf
		Else
			nDif := nPoscont
		EndIf

		cRet := cValtoChar(nDif)
	EndIf
	RestArea(aArea)

Return cRet