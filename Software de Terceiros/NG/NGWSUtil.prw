#include "protheus.ch"
#include "fileio.ch"

//---------------------------------------------------------------------
/*/{Protheus.doc} NGIsRoute
Modelo de rotas REST

@author  Marcelo Camargo
@since   15/03/2016
@version P11/P12
@param   aUrl - Lista de paths vindos do webservice
@param   cPath - Caminho esperado com padrao para verificacao
@return  logic
/*/
//---------------------------------------------------------------------
Function NGIsRoute( aUrl, cPath )
    Local nI
    Local aPath := StrTokArr( cPath, '/' )

    If Len( aUrl ) <> Len( aPath )
        Return .F.
    EndIf

    For nI := 1 To Len( aUrl )
        If !( aPath[ nI ] == aUrl[ nI ] ) .And. !( SubStr( aPath[ nI ], 1, 1 ) == '{' )
            Return .F.
        EndIf
    Next nI
    Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} NGWSDownload
Download de arquivo via HTTP (SOAP, REST)

@author  Marcelo Camargo
@since   14/03/2016
@version P12
@return  logic
/*/
//---------------------------------------------------------------------
Function NGWSDownload( oWebService, cPath, cFileName )
    Local xBytes := ReadBytes( cPath )

    If Nil == xBytes .Or. 0 <> FError()
        Return .F.
    EndIf

    oWebService:SetContentType( 'application/octet-stream' )
    oWebService:SetHeader( 'Content-Disposition', 'attachment; filename=' + cFileName )
    oWebService:SetHeader( 'Pragma', 'no-cache' )
    oWebService:SetResponse( xBytes )

    Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} NGWSImage
Exibir imagem de acordo com o protocolo HTTP (SOAP, REST)

@author Marcelo Camargo
@since   14/03/2016
@version P12
@return logic
/*/
//---------------------------------------------------------------------
Function NGWSImage( oWebService, cPath, lCache )
    Local xBytes := ReadBytes( cPath )
    Default lCache := .T.

    If Nil == xBytes .Or. 0 <> FError()
        Return .F.
    EndIf

    oWebService:SetContentType( 'image/png' )
    If .Not. lCache
        oWebService:SetHeader( 'Pragma', 'no-cache' )
    EndIf
    oWebService:SetResponse( xBytes )

    Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} ReadBytes
Captura os bytes de um arquivo

@author  Marcelo Camargo
@since   30/08/2016
@version P11/P12
@return  logic
/*/
//---------------------------------------------------------------------
Static Function ReadBytes( cFileName )
    Local nHandler := FOpen( cFileName, FO_READWRITE + FO_SHARED )
    Local nSize    := 0
    Local xBuffer  := ''

    If -1 == nHandler
        Return Nil
    EndIf

    nSize := FSeek( nHandler, 0, FS_END )
    FSeek( nHandler, 0 )
    FRead( nHandler, xBuffer, nSize )
    FClose( nHandler )
    Return xBuffer

//---------------------------------------------------------------------
/*/{Protheus.doc} NGEscape
Escapa Strings para a gera��o do JSON

@param cStr - String a ser manipulada
@param lUtf8 - Converte uma string para a codifica��o UTF-8  
@author  Marcelo Camargo
@since   02/09/2016
@version P11/P12
@return  string
/*/
//---------------------------------------------------------------------
Function NGEscape(cStr,lUtf8)
	
    Local cStrBkp:= cStr
    Default lUtf8 := .f.

	If cStr != Nil
		cStr:= AllTrim( StrTran( StrTran( cStr, '\', '\\' ), '"', '\"' ) )
		
		If lUtf8
			cStr := EncodeUtf8(cStr)
            If cStr == Nil //somente quando d� erro de convers�o o retorno da EncodeUtf8 � nulo
                cStr := cStrBkp
            EndIf
        EndIf			
	Else
		cStr := ""
	EndIf

Return cStr
