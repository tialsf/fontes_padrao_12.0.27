//--------------------------------------------------------------------
/*/{Protheus.doc} UPDMNTNG
Update de inclus�o dos campos de log de inclus�o e altera��o. 
Estes campos s�o utilizados no web service MNTNG-MOBILE

@author Larissa Tha�s de Farias
@since 08/01/2016
@return Nil
/*/
//--------------------------------------------------------------------
Main Function UPDMNTNG()

	Local cModulo := "MNT"
	Local bPrepar := { || NGUPD() }
	Local nVersao := 7
	Local cObs := Nil

	NGCriaUpd( cModulo , bPrepar , nVersao ,  ,  , cObs )

Return

//--------------------------------------------------------------------
/*/{Protheus.doc} NGUPD
Corpo do update

@author Larissa Tha�s de Farias
@since 09/12/2017
@return Nil
/*/
//--------------------------------------------------------------------
Static Function NGUPD()

	aSix := {}
	aSX2 := {}
	aSX3 := {}
	aSX9 := {}
	aHelp := {}

	aAdd (aSX2, {"CTT"})
	aAdd (aSX2, {"SA2"})
	aAdd (aSX2, {"SB1"})
	aAdd (aSX2, {"SB2"})
	aAdd (aSX2, {"SH4"})
	aAdd (aSX2, {"ST0"})
	aAdd (aSX2, {"ST1"})
	aAdd (aSX2, {"ST4"})
	aAdd (aSX2, {"ST5"})
	aAdd (aSX2, {"ST6"})
	aAdd (aSX2, {"ST9"})
	aAdd (aSX2, {"STD"})
	aAdd (aSX2, {"STF"})
	aAdd (aSX2, {"STG"})
	aAdd (aSX2, {"STH"})
	aAdd (aSX2, {"STJ"})
	aAdd (aSX2, {"STL"})
	aAdd (aSX2, {"STQ"})
	aAdd (aSX2, {"TP7"})
	aAdd (aSX2, {"TPA"})
	aAdd (aSX2, {"TPC"})
	aAdd (aSX2, {"TPQ"})
	aAdd (aSX2, {"TT9"})

	aAdd(aSix,{"ST1","6","T1_FILIAL+T1_CODUSU","C�d. Usu�rio","C�d. Usu�rio","C�d. Usu�rio","S","S"}) //"C�d. Usu�rio"

	aAdd ( aSX3 , { "ST9" , Nil , "T9_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST9" , Nil , "T9_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST0" , Nil , "T0_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST0" , Nil , "T0_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST4" , Nil , "T4_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST4" , Nil , "T4_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STF" , Nil , "TF_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STF" , Nil , "TF_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STG" , Nil , "TG_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STG" , Nil , "TG_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld
	
	aAdd ( aSX3 , { "STH" , Nil , "TH_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STH" , Nil , "TH_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "SB1" , Nil , "B1_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "SB1" , Nil , "B1_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST1" , Nil , "T1_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST1" , Nil , "T1_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "SA2" , Nil , "A2_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "SA2" , Nil , "A2_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST2" , Nil , "T2_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST2" , Nil , "T2_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "SH4" , Nil , "H4_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "SH4" , Nil , "H4_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "TPA" , Nil , "TPA_USERGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "TPA" , Nil , "TPA_USERGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "TT9" , Nil , "TT9_USERGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "TT9" , Nil , "TT9_USERGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "TP7" , Nil , "TP7_USERGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "TP7" , Nil , "TP7_USERGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STJ" , Nil , "TJ_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STJ" , Nil , "TJ_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST5" , Nil , "T5_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST5" , Nil , "T5_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )				// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ACB" , Nil , "ACB_USERGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ACB" , Nil , "ACB_USERGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	/*aAdd ( aSX3 , { "STL" , Nil , "TL_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STL" , Nil , "TL_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld
	
	aAdd ( aSX3 , { "STQ" , Nil , "TQ_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STQ" , Nil , "TQ_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld
	*/
	aAdd ( aSX3 , { "TPC" , Nil , "TPC_USERGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )				// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "TPC" , Nil , "TPC_USERGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "TPQ" , Nil , "TPQ_USERGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "TPQ" , Nil , "TPQ_USERGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "SB2" , Nil , "B2_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "SB2" , Nil , "B2_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "CTT" , Nil , "CTT_USERGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "CTT" , Nil , "CTT_USERGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST6" , Nil , "T6_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "ST6" , Nil , "T6_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STD" , Nil , "TD_USERLGI" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Inclu", "Log de Inclu", "Log de Inclu", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Inclusao", "Log de Inclusao", "Log de Inclusao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;										    	// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;	    	// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld

	aAdd ( aSX3 , { "STD" , Nil , "TD_USERLGA" , "C" , 17 , 0 , ;		// Alias , Ordem , Campo , Tipo , Tamanho , Decimais
					"Log de Alter", "Log de Alter", "Log de Alter", ;	// Tit. Port. , Tit.Esp. , Tit.Ing. | ## "Filial"
					"Log de Alteracao", "Log de Alteracao", "Log de Alteracao", ;	// Desc. Port. , Desc.Esp. , Desc.Ing. | ## "Filial do Sistema"
					"" , ;											// Picture
					" " , ;												// Valid
					X3_NAOUSADO_USADO , ;								// Usado
					"" , ;												// Relacao
					" " , 9 , X3_USADO_RESERV , " " , " " , ;		// F3 , Nivel , Reserv , Check , Trigger
					"L" , "N" , "V" , "R" , "" , ;						// Propri , Browse , Visual , Context , Obrigat
					" " , ;												// VldUser
					" " , " " , " " , ;									// Box Port. , Box Esp. , Box Ing.
					" " , " " , "" , " " , " " , ;						// PictVar , When , Ini BRW , GRP SXG , Folder
					"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld
	
	//-----------------------------------//
	//ST1 - FUNCIONARIOS DA MANUTENCAO   //
	//-----------------------------------//
	If len(TAMSX3("T1_CODUSU")) == 0
		aAdd(aSX3,{	"ST1", Nil, "T1_CODUSU", "C",6,0,; //Alias,Ordem,Campo,Tipo,Tamanho,Decimais
						"C�d. Usu�rio", "C�d. Usu�rio", "C�d. Usu�rio",; //Tit. Port.,Tit.Esp.,Tit.Ing. # "C�d. Usu�rio"
						"C�d. Usu�rio", "C�d. Usu�rio", "C�d. Usu�rio",; //Tit. Port.,Tit.Esp.,Tit.Ing. # "C�d. Usu�rio"
						"@!",;//Picture
						"MNT020VAL()",;//Valid
						X3_EMUSO_USADO,;//Usado
						"",;//Relacao
						"USR", 1, X3_USADO_RESERV, "", "",;//F3,Nivel,Reserv,Check,Trigger
						"S", "N", "A", "R", " ",;//Propri,Browse,Visual,Context,Obrigat
						"",;	//VldUser
						"", "", "",;//Box Port.,Box Esp.,Box Ing.
						"", "", "", "", "",; //PictVar,When,Ini BRW,GRP SXG,Folder
						"N", "", "", "", " ", " "}) //Pyme,CondSQL,ChkSQL,IdxSrv,Ortogra
		
		aADD(aSX9, {"USR","","ST1","USR_CODIGO"	,"T1_CODUSU" ,"S","1","N"," ","S","S"})
		aAdd(aHelp,{"T1_CODUSU"	,"Informe o C�digo do Usu�rio."}) //"Informe o C�digo do Usu�rio."
	endif
	
	If len(TAMSX3("T1_NOMUSU")) == 0
		aAdd(aSX3,{	"ST1",Nil,"T1_NOMUSU","C",40,0,; //Alias,Ordem,Campo,Tipo,Tamanho,Decimais
						"Nome","Nome","Nome",; //Tit. Port.,Tit.Esp.,Tit.Ing. # 	"Nome"
						"Nome Usu�rio","Nome Usu�rio","Nome Usu�rio",; //Tit. Port.,Tit.Esp.,Tit.Ing. # "Nome Usu�rio"
						"@!",;//Picture
						"",;//Valid
						X3_EMUSO_USADO,;//Usado
						'MNT020REL("T1_NOMUSU")',;//Relacao
						"",1,X3_USADO_RESERV,"","",;//F3,Nivel,Reserv,Check,Trigger
						"S","N","V","V"," ",;//Propri,Browse,Visual,Context,Obrigat
						"",;	//VldUser
						"","","",;//Box Port.,Box Esp.,Box Ing.
						"","","","","",; //PictVar,When,Ini BRW,GRP SXG,Folder
						"" , "" , "" , " " , "N" , "N" } )					// Pyme , CondSQL , ChkSQL , IdxSrv , Ortogra , IdXFld
		
		aAdd(aHelp,{"T1_NOMUSU"	,"Informe o Nome Usu�rio."}) //"Informe o Nome Usu�rio."
	endif
	
	If len(TAMSX3("T1_BITMAP")) == 0
		aAdd( aSX3, { "ST1", Nil , "T1_BITMAP", "C", 20, 0,;	   			// Alias, Ordem, Campo, Tipo,Tamanho, Decimais
						"Foto Funcion�rio", "Foto Funcion�rio", "Foto Funcion�rio",;	// Tit. Port., Tit.Esp., Tit.Ing.	   | ## "Imagem Prod."
						"Foto Funcion�rio", "Foto Funcion�rio", "Foto Funcion�rio",;	// Desc. Port., Desc.Esp., Desc.Ing.	| ## "Imagem do Produto"
						"",;												// Picture
						"",;												// Valid
						X3_BITMAP_USADO,;						   			// Usado
						"",;												// Relacao
						"", 1, X3_BITMAP_RESERV, "", "",;		   			// F3, Nivel, Reserv, Check, Trigger
						"S", "N", "A", "R", "",;							// Propri, Browse, Visual, Context, Obrigat
						"",;												// VldUser
						"", "", "",;										// Box Port., Box Esp., Box Ing.
						"", "", "", "", "",;								// PictVar, When,Ini BRW, GRP SXG, Folder
						"N", "", "", "", "", ""})				   			// Pyme, CondSQL, ChkSQL, IdxSrv, Ortogra
						
		aAdd(aHelp,{"T1_BITMAP","Imagem do funcion�rio utilizada no MNT NG."})
	endif
	
Return
