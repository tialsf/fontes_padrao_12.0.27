#Include 'Protheus.ch'
#INCLUDE "MDTA090a.ch"
#Include 'FWMVCDef.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTA090A
Classe interna implementando o FWModelEvent
@author Luis Fellipy Bett
@since 06/04/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Class MDTA090A FROM FWModelEvent

	//Data dDtVigencia AS Date

	Method GridLinePosVld()
	Method ModelPosVld()
	Method AfterTTS()
    Method New() Constructor

End Class

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTA090A
M�todo construtor da classe
@author Luis Fellipy Bett
@since 06/04/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Method New() Class MDTA090A
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} GridLinePosVld
M�todo para fazer a verfica��o de valida��o das linhas da Grid (LinOK)
@author Luis Fellipy Bett
@since 30/04/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Method GridLinePosVld( oModel , cModelId) Class MDTA090A

    Local lRet       := .T.
    Local nI         := 0
    Local nLine      := oModel:nLine
    Local cMatric    := oModel:GetValue( "TN6_MAT" )
    Local dDtInic    := oModel:GetValue( "TN6_DTINIC" )
    Local dDtTerm    := oModel:GetValue( "TN6_DTTERM" )

    If !(oModel:IsDeleted())
        If dDtInic < Posicione( "SRA" , 1 , xFilial("SRA") + cMatric , "RA_ADMISSA" )
            Help( ' ', 1, 'DTINIINVAL', , STR0015 + STR0016 + DToC(SRA->RA_ADMISSA), 5, 5 )
            lRet := .F.
        Else
            For nI := 1 To oModel:Length()

                oModel:GoLine( nI )
                If nI <> nLine .And. cMatric == oModel:GetValue( "TN6_MAT" ) .And. !(oModel:IsDeleted())
                    If Empty( oModel:GetValue( "TN6_DTTERM" ) ) .And. ;
                        ( dDtInic > oModel:GetValue( "TN6_DTINIC" ) .Or. dDtTerm > oModel:GetValue( "TN6_DTINIC" ) )
                        lRet := .F.
                    ElseIf Empty( dDtTerm ) .And. dDtInic < oModel:GetValue( "TN6_DTTERM" )
                        lRet := .F.
                    ElseIf Empty( dDtTerm ) .And. Empty( oModel:GetValue( "TN6_DTTERM" ) )
                        lRet := .F.
                    ElseIf oModel:GetValue( "TN6_DTINIC" ) < dDtTerm .And. ;
                        oModel:GetValue( "TN6_DTTERM" ) > dDtInic
                        lRet := .F.
                    EndIf

                    If !lRet
                        Help( "" , 1 , "PERINVALID" , , STR0008 ,4,5) //"O per�odo cadastrado j� existe para o funcion�rio."
                    EndIf
                EndIf
            Next nI
        EndIf
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelPosVld
Valida��o do campo de data de validade inicial do model.
@author Luis Fellipy Bett
@since 06/04/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Method ModelPosVld( oModel , cModelId) Class MDTA090A

	Local aAreaTN5		:= TN5->( GetArea() )
	Local nOpcx 		:= oModel:GetOperation() // Opera��o de a��o sobre o Modelo // 3 - Insert ; 4 - Update ; 5 - Delete
	Local oModelTN5		:= oModel:GetModel( "TN5MASTER" )
	Local aNaoSX9		:= { "TN6" }
	Local lRet			:= .T.

	Local lCheckTN6  	:= ( NGSX2MODO( "TN5" ) == "C" .And. NGSX2MODO( "TN6" ) != "C" )
	Local lExistTKD  	:= NGCADICBASE( "TKD_NUMFIC" , "D" , "TKD" , .F. )
	Local lDeleta    	:= .F.
	Local lTN6 			:= .F.
	Local lTKD			:= .F.
	Local aFiliais   	:= {}
	Local cMsg			:= ""
	Local cMsg2			:= ""
	Local nCont2		:= 0

	Local leSocial		:= SuperGetMv( "MV_NG2ESOC" , .F. , "2" ) == "1"
	Local oModelTN6		:= oModel:GetModel( "TN6GRID" )
	Local dDtInic		:= SToD("")
	Local dDtTerm		:= SToD("")
	Local aFuncs		:= {}
	Local cMatricula	:= ""
	Local cCodTar		:= ""
	Local cCodUnic		:= ""
	Local cCCusto		:= ""
	Local cFuncao		:= ""
	Local cDepto		:= ""
	Local nCont			:= 0

	Private aCHKSQL 	:= {} // Vari�vel para consist�ncia na exclus�o (via SX9)
	Private aCHKDEL 	:= {} // Vari�vel para consist�ncia na exclus�o (via Cadastro)

	// Recebe SX9 - Formato:
	// 1 - Dom�nio (tabela)
	// 2 - Campo do Dom�nio
	// 3 - Contra-Dom�nio (tabela)
	// 4 - Campo do Contra-Dom�nio
	// 5 - Condi��o SQL
	// 6 - Compara��o da Filial do Dom�nio
	// 7 - Compara��o da Filial do Contra-Dom�nio
	aCHKSQL := NGRETSX9( "TN5" , aNaoSX9 )

	// Recebe rela��o do Cadastro - Formato:
	// 1 - Chave
	// 2 - Alias
	// 3 - Ordem (�ndice)
	aAdd( aCHKDEL , { 'TN5->TN5_CODTAR'	, "TN0" , 4 } )
	aAdd( aCHKDEL , { '"5"+TN5->TN5_CODTAR' , "TOA" , 2 } )

	If nOpcx == MODEL_OPERATION_DELETE //Exclus�o
		If !NGCHKDEL( "TN5" )
			lRet := .F.
		EndIf

		If lRet .And. !NGVALSX9( "TN5" , aNaoSX9 , .T. , .T. )
			lRet := .F.
		EndIf
	Else
		lRet := MDTObriEsoc( "TN5" , , oModelTN5 ) //Verifica se campos obrigat�rios ao eSocial est�o preenchidos
	EndIf

	If lRet .And. leSocial
		cCodTar := oModel:GetValue( "TN5MASTER"	, "TN5_CODTAR"	)

		For nCont := 1 To oModelTN6:Length()
			oModelTN6:GoLine( nCont )
			If ( oModelTN6:IsDeleted() .Or. oModelTN6:IsInserted() .Or. oModelTN6:IsUpdated() .Or. ;
				M->TN5_ESOC <> oModel:GetValue( "TN5MASTER" , "TN5_ESOC" ) .Or. ;
				M->TN5_DESCRI <> oModel:GetValue( "TN5MASTER" , "TN5_DESCRI" ) ) .And. ;
				!Empty( oModel:GetValue( "TN6GRID" , "TN6_MAT" ) )

				cMatricula	:= oModel:GetValue( "TN6GRID"	, "TN6_MAT"		)
				dDtInic		:= oModel:GetValue( "TN6GRID"	, "TN6_DTINIC"	)
				dDtTerm		:= oModel:GetValue( "TN6GRID"	, "TN6_DTTERM"	)

				cCodUnic	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CODUNIC"	)
				cCCusto		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CC"		)
				cFuncao		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CODFUNC"	)
				cDepto		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_DEPTO"	)

				aAdd( aFuncs , { cMatricula , cCodUnic , cCCusto , cFuncao , cCodTar , cDepto , dDtInic , dDtTerm } )
			EndIf
		Next nCont

		If Len( aFuncs ) > 0
			lRet := MDTFatTAF( aFuncs , nOpcx , .T. , , , .T. ) //Verifica inconsist�ncias
		EndIf
	EndIf

	If lRet
		//Verifica se existem funcionarios para a tarefa em outras filiais
		For nCont2 := 1 To Len( aFiliais )
			If ( lTN6 .Or. !lCheckTN6 ) .And. ( !lExistTKD .Or. ( lExistTKD .And. lTKD ) )
				Exit
			Else
				If lCheckTN6 .And. !lTN6 .And. aFiliais[nCont2][1] != cFilAnt
					dbSelectArea( "TN6" )
					dbSetOrder( 1 )
					If dbSeek( xFilial( "TN6" , aFiliais[nCont2][1] ) + TN5->TN5_CODTAR )
						lTN6 := .T.
					Endif
				Endif
				If lExistTKD .And. !lTKD
					dbSelectArea( "TKD" )
					dbSetOrder( 2 )
					If dbSeek( xFilial( "TKD" , aFiliais[nCont2][1] ) + TN5->TN5_CODTAR )
						lTKD := .T.
					Endif
				Endif
			Endif
		Next nCont2

		If lTN6 .Or. lTKD
			If lTN6
				cMsg := STR0001 //"Existem funcion�rios relacionados a esta tarefa em outras filiais."
			Endif
			If lTKD
				cMsg2 := STR0002 //"candidatos relacionados a esta tarefa."
			Endif
			If !Empty( cMsg ) .And. !Empty( cMsg2 )
				cMsg += CHR(13) + STR0003 + cMsg2 //"Tamb�m existem "
			ElseIf Empty( cMsg )
				cMsg := STR0004 + cMsg2 //"Existem "
			Endif
			lDeleta := MsgYesNo( cMsg + CHR(13) + STR0005 + CHR(13) + STR0006 , STR0007 ) //"Deseja mesmo excluir a tarefa?"###"Todas estas informa��es ser�o apagadas."###"Aten��o"
			lRet := lDeleta
		Endif
	EndIf

	//Deleta informacoes das outras filiais
	If lRet .And. lDeleta
		For nCont2 := 1 To Len( aFiliais )
			If aFiliais[nCont2][1] != cFilAnt
				dbSelectArea( "TN6" )
				dbSetOrder( 1 )
				While dbSeek( xFilial( "TN6" , aFiliais[nCont2][1] ) + TN5->TN5_CODTAR )
					RecLock( "TN6" , .F. )
					dbDelete()
					MsUnlock( "TN6" )
				End
			Endif
			If lExistTKD
				dbSelectArea( "TKD" )
				dbSetOrder( 2 )
				While dbSeek( xFilial( "TKD" , aFiliais[nCont2][1] ) + TN5->TN5_CODTAR )
					RecLock( "TKD" , .F. )
					dbDelete()
					MsUnlock( "TKD" )
				End
			Endif
		Next nCont2
	Endif

	RestArea( aAreaTN5 )

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} InTTS
M�todo executado durante o Commit
@author Luis Fellipy Bett
@since 05/02/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Method AfterTTS( oModel , cModelId ) Class MDTA090A

	Local aArea			:= GetArea()
	Local aAreaTNE  	:= TNE->(GetArea())
	Local leSocial		:= SuperGetMv( "MV_NG2ESOC" , .F. , "2" ) == "1"
	Local oModelTN6		:= oModel:GetModel( "TN6GRID" )
	Local nOpcx			:= oModel:GetOperation() //Op��o realizada.
	Local lRet			:= .T.
	Local aFuncs		:= {}
	Local dDtInic		:= SToD("")
	Local dDtTerm		:= SToD("")
	Local cCodTar		:= ""
	Local cMatricula	:= ""
	Local cCodTar		:= ""
	Local cCodUnic		:= ""
	Local cCCusto		:= ""
	Local cFuncao		:= ""
	Local cDepto		:= ""
	Local nCont			:= 0

	//Envio das informa��es ao TAF
	If leSocial
		cCodTar := oModel:GetValue( "TN5MASTER"	, "TN5_CODTAR"	)

		For nCont := 1 To oModelTN6:Length()
			oModelTN6:GoLine( nCont )
			If ( oModelTN6:IsDeleted() .Or. oModelTN6:IsInserted() .Or. oModelTN6:IsUpdated() .Or. ;
				M->TN5_ESOC <> oModel:GetValue( "TN5MASTER" , "TN5_ESOC" ) .Or. ;
				M->TN5_DESCRI <> oModel:GetValue( "TN5MASTER" , "TN5_DESCRI" ) ) .And. ;
				!Empty( oModel:GetValue( "TN6GRID" , "TN6_MAT" ) )

				cMatricula	:= oModel:GetValue( "TN6GRID"	, "TN6_MAT"		)
				dDtInic		:= oModel:GetValue( "TN6GRID"	, "TN6_DTINIC"	)
				dDtTerm		:= oModel:GetValue( "TN6GRID"	, "TN6_DTTERM"	)

				cCodUnic	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CODUNIC"	)
				cCCusto		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CC"		)
				cFuncao		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CODFUNC"	)
				cDepto		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_DEPTO"	)

				aAdd( aFuncs , { cMatricula , cCodUnic , cCCusto , cFuncao , cCodTar , cDepto , dDtInic , dDtTerm } )
			EndIf
		Next nCont

		If Len( aFuncs ) > 0
			lRet := MDTFatTAF( aFuncs , nOpcx , .T. ) //Envia ao TAF
		EndIf
	EndIf

	dbSelectArea( "TNE" )
	RestArea( aAreaTNE )
	RestArea( aArea )

Return