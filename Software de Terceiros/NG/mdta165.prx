#INCLUDE "MDTA165.ch"
#Include "Protheus.ch"
#INCLUDE "FWMVCDEF.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTA165
Programa de Cadastro de Ambiente fisico (MVC)

@sample MDTA165()

@author Julia Kondlatsch
@since 26/10/2017
@version 1.0
/*/
//---------------------------------------------------------------------
Function MDTA165()

	//-----------------------------------------------------
	// Armazena variaveis p/ devolucao (NGRIGHTCLICK)
	//-----------------------------------------------------
	Local aNGBEGINPRM 	:= NGBEGINPRM( )
	Local oBrowse

	oBrowse := FWMBrowse():New()		// Cria o objeto Browse
		oBrowse:SetAlias( "TNE" )			// Alias da tabela utilizada
		oBrowse:SetMenuDef( "MDTA165" )		// Nome do fonte onde esta a fun��o MenuDef
		oBrowse:SetDescription( STR0006 )	// Descri��o do browse - "Ambiente F�sico"
	oBrowse:Activate()

	// Devolve as vari�veis armazenadas
	NGRETURNPRM(aNGBEGINPRM)

Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Cria��o do menu MVC

@return

@sample MenuDef()

@author Julia Kondlatsch
@since 26/10/2017
@return aRotina array com o Menu MVC
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function MenuDef()

	Local leSocial	:= SuperGetMv( "MV_NG2ESOC" , .F. , "2" ) == "1"
	Local aRotina := {}

	aAdd( aRotina, { STR0002	, 'VIEWDEF.MDTA165'	, 0, 2, 0, NIL } )	//Visualizar
	aAdd( aRotina, { STR0003 	, 'VIEWDEF.MDTA165'	, 0, 3, 0, NIL } )	//Incluir
	aAdd( aRotina, { STR0004 	, 'VIEWDEF.MDTA165'	, 0, 4, 0, NIL } )	//Alterar
	aAdd( aRotina, { STR0005 	, 'VIEWDEF.MDTA165'	, 0, 5, 0, NIL } )	//Excluir
	aAdd( aRotina, { STR0011 	, 'VIEWDEF.MDTA165'	, 0, 8, 0, NIL } )	//Imprimir
	aAdd( aRotina, { STR0012 	, 'VIEWDEF.MDTA165'	, 0, 9, 0, NIL } )	//Copiar

	If leSocial
		aAdd( aRotina, { STR0013	, 'MDTGeraXml'		, 0, 9, 0, NIL } )	//Gerar Xml eSocial
	EndIf

Return aRotina

//---------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Cria��o do modelo de dados (MVC)

@sample ModelDef()

@author Julia Kondlatsch
@since 26/10/2017

@return oModel objeto do Modelo MVC
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function ModelDef()

	Local lNR32			:= SuperGetMv( "MV_NG2NR32" , .F. , "2" ) == "1"
	Local lTYG			:= AliasInDic( "TYG" )

	//Criar o evento para integra��o com o eSocial
	Local oEvent 		:= MDTA165A():New()

	// Cria a estrutura a ser usada no Modelo de Dados
	Private oStructTNE := FWFormStruct( 1 /*Usado no Model*/ ,"TNE" )
	Private oStructTI7 := FWFormStruct( 1 /*Usado no Model*/ ,"TI7" )
	Private oStructTYG := FWFormStruct( 1 /*Usado no Model*/ ,"TYG" )

	// Modelo de dados que ser� constru�do
	Private oModel

	// Cria o objeto do Modelo de Dados
	oModel := MPFormModel():New( "MDTA165" , /*bPre*/ , /*bPos*/ , /*bCommit*/ , /*bCancel*/  )

	// Adiciona ao modelo um componente de Formul�rio Principal
	oModel:AddFields( "TNEMASTER" , Nil , oStructTNE , /*bPre*/ , /*bPost*/ , /*bLoad*/ )
	// Adiciona as grids inferiores ao medelo
	If lNR32
		oModel:AddGrid( "TI7DETAIL" , "TNEMASTER" , oStructTI7 , ,/*{|oModelGrid|ValidLinha(oModelGrid)}*/ , /*bLoad*/ )
		//Relaciona os campos das grids com o registro principal
		oModel:SetRelation( 'TI7DETAIL', { { 'TI7_FILIAL', 'xFilial( "TI7" )' },{ 'TI7_CODAMB', 'TNE_CODAMB' } },TI7->( IndexKey( 1 ) ) )
		//"Valida chave duplicada nas linhas das getdados
		oModel:GetModel( "TI7DETAIL" ):SetUniqueLine( {"TI7_PONMED"} )
		//Define Grid como n�o obrigat�ria
		oModel:GetModel( "TI7DETAIL" ):SetOptional( .T. )
	EndIf
	If lTYG
		oModel:AddGrid( "TYGDETAIL" , "TNEMASTER" , oStructTYG , ,/*{|oModelGrid|ValidLinha(oModelGrid)}*/ , /*bLoad*/ )
		//Relaciona os campos das grids com o registro principal
		oModel:SetRelation( 'TYGDETAIL', { { 'TYG_FILIAL', 'xFilial( "TYG" )' },{ 'TYG_CODAMB', 'TNE_CODAMB' } },TYG->( IndexKey( 1 ) ) )
		//"Valida chave duplicada nas linhas das getdados
		oModel:GetModel( "TYGDETAIL" ):SetUniqueLine( {"TYG_AGENTE"} )
		//Define Grid como n�o obrigat�ria
		oModel:GetModel( "TYGDETAIL" ):SetOptional( .T. )
	EndIf
	//Instalar evento no Modelo
	oModel:InstallEvent( "MDTA165A" , /*cOwner*/, oEvent )

Return oModel

//---------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Cria��o da tela (View - MVC)

@sample ViewDef()

@author Julia Kondlatsch
@since 26/10/2017

@return oView - Objeto Contendo a View MVC
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function ViewDef()

	Local lNR32			:= SuperGetMv( "MV_NG2NR32" , .F. , "2" ) == "1"
	Local lTYG			:= AliasInDic( "TYG" )

	//Estrutura valores de cria��o de telas
	Local nViewTNE		:= If( lNR32 .Or. lTYG , 60 , 100 )
	Local nViewInf		:= If( lNR32 .And. lTYG , 50 , 100 )

	// Cria um objeto de Modelo de dados baseado no ModelDef() do fonte informado
	Private oModel 		:= FWLoadModel( "MDTA165" )

	// Cria a estrutura a ser usada na View
	Private oStructTNE 	:= FWFormStruct( 2 /*Usado na View*/, "TNE" )
	Private oStructTI7 	:= FWFormStruct( 2 /*Usado na View*/, "TI7" )
	Private oStructTYG 	:= FWFormStruct( 2 /*Usado na View*/, "TYG" )

	// Interface de visualiza��o que ser� constru�da
	Private oView

	//Remove os campos da estrutura
	If lNR32
		oStructTI7:RemoveField("TI7_CODAMB")
	EndIf
	If lTYG
		oStructTYG:RemoveField("TYG_CODAMB")
	EndIf

	// Cria o objeto de View
	oView := FWFormView():New()
		// Associa o model com a view
		oView:SetModel( oModel )

		// Adiciona no View componentes do model
		oView:AddField( "VIEW_TNE" , oStructTNE , "TNEMASTER" )
		//Adiciona um titulo para o formul�rio
		oView:EnableTitleView( "VIEW_TNE" , STR0006 )	// Descri��o do browse "Ambiente F�sico"
		// Cria os componentes "box" horizontais para receberem elementos da View
		oView:CreateHorizontalBox( "TELATNESUP" , nViewTNE ,/*cIDOwner*/,/*lFixPixel*/,/*cIDFolder*/,/*cIDSheet*/  )
		// Associa um View a um box
		oView:SetOwnerView( "VIEW_TNE" , "TELATNESUP" )

		If lNR32 .Or. lTYG//Cria o box horizontal inferior caso haja filhos
			oView:CreateHorizontalBox( "TELAGRDINF" , 40 ,/*cIDOwner*/,/*lFixPixel*/,/*cIDFolder*/,/*cIDSheet*/ )
		EndIf

		If lNR32
			oView:AddGrid( "VIEW_TI7" , oStructTI7 , "TI7DETAIL")
			//Adiciona um titulo para o formul�rio
			oView:EnableTitleView( "VIEW_TI7" , STR0009 )	//"Radia��o de Fuga"
			// Criar os componentes "box" vertical para receber os elementos de grid na parte inferior da tela
			oView:CreateVerticalBox( "TI7INFESQ", nViewInf, "TELAGRDINF" )
			// Associa um View a um box
			oView:SetOwnerView( "VIEW_TI7" , "TI7INFESQ" )
		EndIf
		If lTYG
			oView:AddGrid( "VIEW_TYG" , oStructTYG , "TYGDETAIL")
			//Adiciona um titulo para o formul�rio
			oView:EnableTitleView( "VIEW_TYG" ,  STR0010 )	//"Ambiente x Agente"
			// Criar os componentes "box" vertical para receber os elementos de grid na parte inferior da tela
			oView:CreateVerticalBox( "TYGINFDIR", nViewInf, "TELAGRDINF" )
			// Associa um View a um box
			oView:SetOwnerView( "VIEW_TYG" , "TYGINFDIR" )
		EndIf

Return oView

//---------------------------------------------------------------------
/*/{Protheus.doc} A165CHAVE
Verifica se existe registro com a mesma chave
(Deixada esta fun��o do antigo fonte para n�o ocorrer falta de fun��o
se n�o for atualizado o dicion�rio)

@return L�gico - Retorna falso caso valor j� exista

@param cCodAmb - C�digo do Ambiente F�sico a ser validado

@obs Utilizado na valida��o do campo TNE_CODAMB

@sample A165CHAVE()

@author Denis Hyroshi de Souza
@since 30/08/2003
/*/
//---------------------------------------------------------------------
Function A165CHAVE( cCodAmb )

	Local lRet  := .T.
	Local aArea := GetArea()

	dbSelectArea( "TNE" )
	dbSetOrder( 1 )
	If dbSeek( xFilial( "TNE" ) + cCodAmb )
		Help( " " , 1 , "JAGRAVADO" )
		lRet  := .T.
	Endif

	RestArea( aArea )

Return lRet