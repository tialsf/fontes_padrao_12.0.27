#Include 'Protheus.ch'
#INCLUDE "MDTA165a.ch"
#Include 'FWMVCDef.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTA165A
Fun��o para for�ar a compila��o do fonte
@author Julia Kondlatsch
@since 30/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function MDTA165A()
Return
//-------------------------------------------------------------------
/*/{Protheus.doc} MDTA165A
Classe interna implementando o FWModelEvent
@author Julia Kondlatsch
@since 30/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Class MDTA165A FROM FWModelEvent

	Method New() Constructor
	Method ModelPosVld() //Valida��o P�s-Valid do Modelo
	Method InTTS() //Method executado durante o Commit

End Class

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTA165A
M�todo construtor da classe
@author Julia Kondlatsch
@since 30/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Method New() Class MDTA165A
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelPosVld
Valida��o do campo de data de validade inicial do model.
@author Julia Kondlatsch
@since 30/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Method ModelPosVld( oModel , cModelId) Class MDTA165A

	Local aArea		:= GetArea()
	Local aAreaTNE  := TNE->(GetArea())
	Local aAreaT04
	Local leSocial	:= SuperGetMv( "MV_NG2ESOC" , .F. , "2" ) == "1"
	Local lReturn	:= .T.
	Local nOperation:= oModel:GetOperation() // Opera��o de a��o sobre o Modelo
	Local dDataIni	:= oModel:GetValue( 'TNEMASTER', 'TNE_DTVINI' )
	Local aNao		:= { "TI7" , "TYG" }

	//Vari�veis de envio ao eSocial
	Local lNovoAmb	:= dDataIni != TNE->TNE_DTVINI .And. !Empty( TNE->TNE_DTVINI ) .And. nOperation != MODEL_OPERATION_INSERT
	Local cDataAnt	:= StrZero( Year( TNE->TNE_DTVINI ) , 4 ) + StrZero( Month( TNE->TNE_DTVINI ) , 2 )
	Local cCodAmb	:= oModel:GetValue( 'TNEMASTER', 'TNE_CODAMB' )
	Local cAnoMes 	:= StrZero( Year( dDataIni ) , 4 ) + StrZero( Month( dDataIni ) , 2 )
	Local cMesAno	:= StrZero( Month( TNE->TNE_DTVINI ) , 2 ) + StrZero( Year( TNE->TNE_DTVINI ) , 4 )
	Local aLogProc	:= {}
	Local cMsgLog	:= ""
	Local cFilEnv   := ""

	Local cDelDataIni := ""
	Local cDelDataFim := ""

	Private aCHKSQL 	:= {} // Vari�vel para consist�ncia na exclus�o (via SX9)
	Private aCHKDEL 	:= {} // Vari�vel para consist�ncia na exclus�o (via Cadastro)

	// Recebe SX9
	aCHKSQL := NGRETSX9( "TNE" , aNao )

	If nOperation == MODEL_OPERATION_DELETE .And. ( !NGCHKDEL( "TNE" ) .Or. !NGVALSX9( "TNE" , aNao , .T. , .T. ) )
		lReturn := .F.
	EndIf

	If leSocial

		If lReturn .And. nOperation != MODEL_OPERATION_DELETE
			lReturn := MDTObriEsoc( "TNE" , , oModel ) //Verifica se campos obrigat�rios ao eSocial est�o preenchidos
		EndIf

		If lReturn .And. ( nOperation == MODEL_OPERATION_INSERT .Or. nOperation == MODEL_OPERATION_UPDATE ) .And. Empty(dDataIni)
			Help(Nil, Nil, "", STR0001 , STR0003, 1 , 0)  //"ATEN��O" ## "A data de validade inicial deve ser preenchida ao ter integra��o com o eSocial"
			lReturn := .F.
		ElseIf lReturn .And. nOperation == MODEL_OPERATION_UPDATE
			If dDataIni < TNE->TNE_DTVINI
				Help(Nil, Nil, "", STR0001 , STR0002, 1 , 0) //"ATEN��O" ## "A nova data de validade inicial n�o pode ser menor que a data anterior."
				lReturn := .F.
			EndIf
		EndIf

		If lReturn .And. nOperation != MODEL_OPERATION_DELETE

			//Busca filial de envio entre a matriz e as filhas
			cFilEnv := MDTBFilEnv()

			lReturn := MDTM001( cAnoMes , 1 , , {} , cFilEnv , .T. , 3 , , oModel , , , , .T. )
		EndIf

	EndIf

	dbSelectArea( "TNE" )
	RestArea( aAreaTNE )
	RestArea( aArea )

Return lReturn

//-------------------------------------------------------------------
/*/{Protheus.doc} InTTS
M�todo executado durante o Commit
@author Luis Fellipy Bett
@since 27/11/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Method InTTS( oModel , cModelId ) Class MDTA165A

	Local aArea			:= GetArea()
	Local aAreaTNE  	:= TNE->(GetArea())
	Local aAreaT04
	Local lRet			:= .T.
	Local nOperation	:= oModel:GetOperation() //Op��o realizada.
	Local leSocial		:= SuperGetMv( "MV_NG2ESOC" , .F. , "2" ) == "1"
	Local lMiddleware	:= IIf( cPaisLoc == 'BRA' .And. Findfunction( "fVerMW" ), fVerMW(), .F. )
	Local dDataIni		:= oModel:GetValue( 'TNEMASTER', 'TNE_DTVINI' )
	Local lNovoAmb		:= dDataIni != TNE->TNE_DTVINI .And. !Empty( TNE->TNE_DTVINI ) .And. nOperation != MODEL_OPERATION_INSERT
	Local cDataAnt		:= StrZero( Year( TNE->TNE_DTVINI ) , 4 ) + StrZero( Month( TNE->TNE_DTVINI ) , 2 )
	Local cCodAmb		:= oModel:GetValue( 'TNEMASTER', 'TNE_CODAMB' )
	Local cAnoMes 		:= StrZero( Year( dDataIni ) , 4 ) + StrZero( Month( dDataIni ) , 2 )
	Local cMesAno		:= StrZero( Month( TNE->TNE_DTVINI ) , 2 ) + StrZero( Year( TNE->TNE_DTVINI ) , 4 )
	Local cDelDataIni	:= ""
	Local cDelDataFim	:= ""
	Local cFilEnv		:= ""

	//Envio das informa��es ao TAF
	If leSocial

		//Busca filial de envio entre a matriz e as filhas
		cFilEnv := MDTBFilEnv()

		If lNovoAmb
			If lMiddleware

				If MDTVerStat( .T., "S1060", AllTrim( cCodAmb ) + cAnoMes )
					//Se for mudada a data de validade inicial adiciona como data final e cria um novo ambiente
					MDTM001( cDataAnt , 1 , , {} , cFilEnv , .T. , 4 , cAnoMes , oModel )
				Else
					MDTM001( cAnoMes , 1 , , {} , cFilEnv , .T. , 3 , , oModel )
				EndIf
			Else
				If TAFGetStat( "S-1060" , cCodAmb + ";" + cMesAno , , cFilEnv ) <> "-1"
					//Se for mudada a data de validade inicial adiciona como data final e cria um novo ambiente
					MDTM001( cDataAnt , 1 , , {} , cFilEnv , .T. , 4 , cAnoMes , oModel )
				Else
					MDTM001( cAnoMes , 1 , , {} , cFilEnv , .T. , 3 , , oModel )
				EndIf
			EndIf
		ElseIf nOperation <> 5
			If nOperation <> 3 .And. IIf( lMiddleware,;
				MDTVerStat( .T., "S1060", AllTrim( cCodAmb ) + cAnoMes ),;
				TAFGetStat( "S-1060" , cCodAmb + ";" + cMesAno , , cFilEnv ) <> "-1" )
				MDTM001( cAnoMes , 1 , , {} , cFilEnv , .T. , nOperation , , oModel )
			Else
				MDTM001( cAnoMes , 1 , , {} , cFilEnv , .T. , 3 , , oModel )
			EndIf
		EndIf

		If nOperation == 5
			If !lMiddleware
				//Se for exclus�o procura todos os registros do ambiente no TAF para excluir
				dbSelectArea( "T04" )
				dbSetOrder( 2 )
				If dbseek( xFilial( "T04" ) + cCodAmb )
					While T04->( !Eof() ) .And. AllTrim( cCodAmb ) == AllTrim( T04->T04_CODIGO )
						aAreaT04 := T04->( GetArea() )
						If T04->T04_ATIVO == "1"
							cDelDataIni	:= Substr( T04->T04_DTINI , 3 , 4 ) + Substr( T04->T04_DTINI , 1 , 2 )
							If !Empty( T04->T04_DTFIN )
								cDelDataFim	:= Substr( T04->T04_DTFIN , 3 , 4 ) + Substr( T04->T04_DTFIN , 1 , 2 )
							Else
								cDelDataFim	:= ""
							EndIf
							If TAFGetStat( "S-1060" , cCodAmb + ";" + cMesAno , , cFilEnv ) <> "-1"
								MDTM001( cDelDataIni , 1 , , {} , cFilEnv , .T. , nOperation , cDelDataFim , oModel )
							EndIf
						EndIf
						dbSelectArea( "T04" )
						RestArea( aAreaT04 )
						T04->( dbSkip() )
					EndDo
				EndIf
			Else
				If MDTVerStat( .T., "S1060", AllTrim( cCodAmb ) + cAnoMes, .T. )
					MDTM001( cAnoMes , 1 , , {} , cFilEnv , .T. , nOperation , "" , oModel )
				EndIf
			EndIf
		EndIf
	EndIf

	dbSelectArea( "TNE" )
	RestArea( aAreaTNE )
	RestArea( aArea )

Return