#include "Mdta660.ch"
#include "Protheus.ch"

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTA660
Programa de Cadastro de Componentes
@type    function
@author  Thiago Olis Machado
@since   03/05/2001
@sample  MDTA660()
@return  L�gico, Sempre verdadeiro
/*/
//-------------------------------------------------------------------
Function MDTA660()

	// Armazena variaveis p/ devolucao (NGRIGHTCLICK)
	Local aNGBEGINPRM := NGBEGINPRM( )
	Local cFiltroTNQ  := ""
	Local lCipatr     := If( SuperGetMv("MV_NG2NR31",.F.,"2") == "1", .T. , .F. )
	lSigaMdtPS := If( SuperGetMv("MV_MDTPS",.F.,"N") == "S", .T. , .F. )

	Private cCadastro
	Private lMdtMin  := If( SuperGetMv("MV_MDTMIN",.F.,"N") == "S", .T. , .F. )
	Private cAliasCC := "CTT"
	Private cFilCC   := "CTT->CTT_FILIAL"
	Private cCodCC   := "CTT->CTT_CUSTO"
	Private cDesCC   := "CTT->CTT_DESC01"
	Private aTROCAF3 := {}
	Private lCpoTNQ  := .F.

	If Alltrim(GETMV("MV_MCONTAB")) != "CTB"
		cAliasCC := "SI3"
		cFilCC   := "SI3->I3_FILIAL"
		cCodCC   := "SI3->I3_CUSTO"
		cDesCC   := "SI3->I3_DESC"
	EndIf

	Private aRotina := MenuDef()

	If lSigaMdtps

		cCadastro := OemtoAnsi(STR0009)  //"Clientes"

		DbSelectArea("SA1")
		DbSetOrder(1)

		mBrowse( 6, 1,22,75,"SA1")

	Else

		If TNS->(FieldPos("TNS_PRESEN")) > 0
			aADD(aRotina,{STR0010,"MDTA660Leg", 0 , 6})  //"Legenda"
		EndIf

		// Define o cabecalho da tela de atualizacoes
		cCadastro := OemtoAnsi(STR0006) //"Componentes"
		Private aCHKDEL := {}, bNGGRAVA
		lCpoTNQ := If(TNQ->(FieldPos("TNQ_FILMAT")) > 0,.T.,.F.)
		Private lCpoTNO := If(TNO->(FieldPos("TNO_FILMAT")) > 0,.T.,.F.)
		Private nIndTNQ := NGRETORDEM("TNQ","TNQ_FILIAL+TNQ_MANDAT+TNQ_FILMAT+TNQ_MAT+DTOS(TNQ_DTSAID)",.T.)
		nIndTNQ := If(nIndTNQ > 0,nIndTNQ,RetIndex("TNQ"))
		Private nIndTNO := NGRETORDEM("TNO","TNO_FILIAL+TNO_MANDAT+TNO_FILMAT+TNO_MAT+DTOS(TNO_DTCAND)",.T.)
		nIndTNO := If(nIndTNO > 0,nIndTNO,RetIndex("TNO"))

		// Aplica filtro quando for aberto apartir da GPEA010
		If IsInCallStack( "MDT660DEMI" )
			cFiltroTNQ:= "TNQ_FILIAL ='" + xFilial("TLM") + "'"
			cFiltroTNQ+= " AND TNQ_MAT ='"+ SRA->RA_MAT + "'"
			cFiltroTNQ+= " AND TNQ_DTSAID =''"
		EndIf

		// Endereca a funcao de BROWSE
		DbSelectArea("TNQ")
		DbSetorder(If(lCpoTNQ,nIndTNQ,1))

		If TNS->(FieldPos("TNS_PRESEN")) > 0
			mBrowse( 6, 1,22,75,"TNQ",,,,,,MDTA660Cor(),,,,,,,,cFiltroTNQ)
		Else
			mBrowse( 6, 1,22,75,"TNQ",,,,,,,,,,,,,,cFiltroTNQ)
		EndIf

		DbSelectArea("TNQ")
		DbSetorder(1)
		DbSelectArea("TNO")
		DbSetorder(1)

	EndIf

	// Devolve variaveis armazenadas (NGRIGHTCLICK)
	NGRETURNPRM(aNGBEGINPRM)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} CHK660MAN
Checa se j� existe registro relacionado

@type    function
@author  Thiago olis Machado
@since   03/05/2001
@sample  CHK660MAN( .T., 3 )

@param   lPasMDT, L�gico,
@param   nTipCad, Num�rico,

@return  L�gico, Sempre verdadeiro
/*/
//-------------------------------------------------------------------
Function CHK660MAN( lPasMDT, nTipCad )

	Local aArea := GetArea()

	If lSigaMdtps

		If !EXISTCHAV("TNQ",cCliMdtps+M->TNQ_MANDAT+M->TNQ_FILMAT+M->TNQ_MAT,6)
			Return .F.
		EndIf

		If lPasMDT
			If !ExCpoMDT("SRA",M->TNQ_MAT,,.F.)
				MsgStop(STR0011,STR0012)  //"Matr�cula de funcion�rio n�o existe."  //"ATEN��O"
				Return .F.
			EndIf
			If SubStr(SRA->RA_CC,1,nSizeSA1+nSizeLoj) <> cCliMdtps
				MsgStop(STR0013,STR0012)  //"Matr�cula n�o pertence ao cliente."    //"ATEN��O"
				Return .F.
			EndIf
		Else
			If !ExCpoMDT("SRA",M->TNQ_MAT)
				Return .F.
			EndIf
			If SubStr(SRA->RA_CC,1,nSizeSA1+nSizeLoj) <> cCliMdtps
				Return .F.
			EndIf
		EndIf

	Else

		If lCpoTNQ
			If nTipCad == 3
				If !EXISTCHAV("TNQ",M->TNQ_MANDAT+M->TNQ_FILMAT+M->TNQ_MAT,nIndTNQ)
					Return .F.
				EndIf

				If M->TNQ_INDICA == "2" .And. !MDT660INC() //Fun��o para verificar se o funcion�rio pode ser reeleito
					Return .F.
				EndIf

			ElseIf nTipCad == 4 .And. Type("cQ_FILMAT") == "C"
				If M->TNQ_FILMAT <> cQ_FILMAT
					If !EXISTCHAV("TNQ",M->TNQ_MANDAT+M->TNQ_FILMAT+M->TNQ_MAT,nIndTNQ)
						Return .F.
					EndIf
				EndIf
				If M->TNQ_INDICA == "2" .And. !MDT660INC() //Fun��o para verificar se o funcion�rio pode ser reeleito
					Return .F.
				EndIf
			EndIf
		Else
			If !EXISTCHAV("TNQ",M->TNQ_MANDAT+M->TNQ_MAT)
				Return .F.
			EndIf
		EndIf
		If lPasMDT
			If !ExCpoMDT("SRA",M->TNQ_MAT,,.F.)
				MsgStop(STR0011,STR0012)  //"Matr�cula de funcion�rio n�o existe."  //"ATEN��O"
				Return .F.
			EndIf
		Else
			If !ExCpoMDT("SRA",M->TNQ_MAT)
				Return .F.
			EndIf
		EndIf
	EndIf

	If lMdtMin
		dbSelectArea("SRA")
		dbSetOrder(1)
		If dbSeek(xFilial("SRA")+M->TNQ_MAT)
			cSetor   := SRA->RA_CC
			cAreaC   := NGSeek('TLJ', M->TNQ_MANDAT + cSetor ,1,'TLJ->TLJ_AREA')
			M->TNQ_AREA   := cAreaC
			M->TNQ_NOAREA := NGSeek(cAliasCC, cAreaC ,1,cDesCC)
		EndIf
	EndIf

	RestArea(aArea)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} CHK660DAT
Checa se a data for informada � maior que data da candidatura

@type    function
@author  Thiago Olis Machado
@since   03/05/2001
@sample  CHK660DAT()

@return  L�gico, Verdadeiro se a data for v�lida
/*/
//-------------------------------------------------------------------
Function CHK660DAT()

	Local cSeek := xFilial("TNO")+M->TNQ_MANDAT+M->TNQ_MAT

	If lSigaMdtps

		dDataSai := M->TNQ_DTSAID
		DbSelectArea("TNQ")
		If !Empty(dDataSai)
			DbSelectArea("TNO")
			DbSetorder(6)
			DbSeek(xFilial("TNO")+cCliMdtps+M->TNQ_MANDAT+M->TNQ_FILMAT+M->TNQ_MAT)
			If M->TNQ_DTSAID < TNO->TNO_DTCAND
				Return .F.
			EndIf
		EndIf

	Else

		If lCpoTNO .And. lCpoTNQ
			cSeek := xFilial("TNO")+M->TNQ_MANDAT+M->TNQ_FILMAT+M->TNQ_MAT
		EndIf

		dDataSai := M->TNQ_DTSAID
		DbSelectArea("TNQ")
		If !Empty(dDataSai)
			DbSelectArea("TNO")
			DbSetorder(If(lCpoTNO .And. lCpoTNQ,nIndTNQ,1))
			DbSeek(cSeek)
			If M->TNQ_DTSAID < TNO->TNO_DTCAND
				Return .F.
			EndIf
		EndIf

	EndIf

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} MDT660PROC
Inclui, altera e exclui os registros

@type    function
@author  Denis Hyroshi de Souza
@since   10/04/2005
@sample  MDT660PROC( 'SRA', 35, 3 )

@param   cAlias, Caractere, Alias da tabela utilizada
@param   nRecno, Num�rico, Recno do Registro
@param   nOpcx, Num�rico, Op��o selecionada

@return  Nil, Sempre Nulo
/*/
//-------------------------------------------------------------------
Function MDT660PROC( cAlias, nRecno, nOpcx )

	Local nRet
	Local aArea := GetArea()
	Local cOldFil := cFilAnt
	Local lCpoFilFun := If( NGCADICBASE("TNQ_FILMAT","A","TNQ",.F.) , .T. , .F. )
	Local cFil1Tmp

	Private nTipInd1
	Private nTipInd2
	Private cTipCand

	//Ao chamar pela fun��o MDT660DEMI, dever� ser feito somente a op��o de altera��o
	If IsInCallStack( "MDT660DEMI" ) .And. nOpcx == 3//Verifica se foi chamado pela MDT660DEMI
		nOpcx := 4 // Muda o nOpcx para Altera��o
		SetAltera()
	EndIf

	If nOpcx == 3
		bNGGRAVA := {|| CHK660MAN(.T.,3)}
	Else
		If lCpoTNQ
			If !Empty(TNQ->TNQ_FILMAT)
				cFilAnt := TNQ->TNQ_FILMAT
			EndIf
		EndIf
		If nOpcx == 4 .And. lCpoTNQ

			//Condi��o feita, caso for feito a rescis�o do funcion�rio pela rotina GPEM040
			//Pois por essa Rotina o Funcion�rio � desabilitado
			If SRA->RA_MSBLQL == "1" .And. SRA->RA_SITFOLH == "D"
				lAtu := MsgYesNo( STR0035 +CRLF+; //"N�o � poss�vel alterar o Componente, pois o Funcion�rio n�o est� habilitado. "
								STR0036 ) //"Deseja preencher o campo Data de Sa�da com a Data de demiss�o do Funcion�rio ?"

				If lAtu //Se confirmar preenche com a Data de demiss�o
					RecLock("TNQ",.F.)
					TNQ->TNQ_DTSAID := SRA->RA_DEMISSA
					MsUnlock("TNQ")
				EndIf

				Return .T.
			EndIf

			cQ_FILMAT:= TNQ->TNQ_FILMAT
			bNGGRAVA := {|| CHK660MAN(.T.,4)}
		EndIf
	EndIf

	nTipInd1 := TNQ->TNQ_INDICA
	nRet     := NGCAD01(cAlias,nRecno,nOpcx)
	nTipInd2 := TNQ->TNQ_INDICA
	cTipCand := TNQ->TNQ_TIPCOM

	DbSelectArea("TNO")
	DbSetOrder(2)
	If DbSeek(xFilial("TNO")+TNQ->TNQ_MAT)
		If !Empty(TNQ->TNQ_INDICA) .And. nOpcx == 3
				RecLock("TNO",.F.)
				TNO->TNO_INDICA := TNQ->TNQ_INDICA
				MsUnlock("TNO")
		EndIf
		If  !Empty(TNQ->TNQ_INDICA) .And. nOpcx == 4
				RecLock("TNO",.F.)
				TNO->TNO_INDICA := TNQ->TNQ_INDICA
				MsUnlock("TNO")
		EndIf
	EndIf
	cFil1Tmp := cFilAnt
	If lCpoFilFun
		If !Empty(TNQ->TNQ_FILMAT)
			cFil1Tmp := TNQ->TNQ_FILMAT
		EndIf
	EndIf

	If nRet = 1 .And. (nOpcx = 3 .Or. nOpcx = 4)
		ADDCCIP660(TNQ->TNQ_MANDAT, TNQ->TNQ_MAT, cFil1Tmp, TNQ->TNQ_TIPCOM)
		cFilAnt := cOldFil
		MDT660SEC( TNQ->TNQ_MANDAT, TNQ->TNQ_FILMAT, TNQ->TNQ_MAT, TNQ->TNQ_INDFUN)
	EndIf

	If nRet = 1 .And. nOpcx = 5
		DELCCIP660(TNQ->TNQ_MANDAT, TNQ->TNQ_MAT, cFil1Tmp, TNQ->TNQ_TIPCOM)
	EndIf

	If cFilAnt <> cOldFil
		cFilAnt := cOldFil
	EndIf

	bNGGRAVA := {}
	RestArea(aArea)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} MDT660FILF
Validacao do campo TNQ_FILMAT

@type    function
@author  Denis Hyroshi de Souza
@since   10/04/2005
@sample  MDT660FILF()
@return  lRet, L�gico, Verdadeiro se o campo for v�lido
/*/
//-------------------------------------------------------------------
Function MDT660FILF

	Local aArea    := GetArea()
	Local aAreaSM0 := SM0->(GetArea())
	Local lRet     := .T.

	Dbselectarea("SM0")
	If !Dbseek(cEmpAnt+M->TNQ_FILMAT)
		Help(" ",1,"REGNOIS")
		lRet := .F.
	Else
		cFilAnt := M->TNQ_FILMAT
		dbSelectArea("SRA")
		dbSetOrder(01)
		If !dbSeek(xFilial("SRA",cFilAnt)+ M->TNQ_MAT )
			M->TNQ_MAT := Space( Len(SRA->RA_MAT) )
			M->TNQ_NOME := " "
		Else
			M->TNQ_NOME := SRA->RA_NOME
		EndIf
	EndIf

	RestArea(aAreaSM0)
	RestArea(aArea)

Return lRet

//---------------------------------------------------------------------
/*/{Protheus.doc} A660DTCAND
Realizaa valida��o das datas de entrada e sa�da do Candidato

@return lRet L�gico Indica se a data informada est� correta (.T.) ou n�o (.F.)

@param nDtVal Num�rico Indica qual data se esta validando: 1 - Data de Sa�da; 2 - Data de Entrada

@sample A660DTCAND()

@author Denis Hyroshi de Souza Refeito por: Jackson Machado
@since 12/09/2006 Refeito em: 23/07/2014
/*/
//---------------------------------------------------------------------
Function A660DTCAND( nDtVal )

	Local cSolucao:= ""//Receber� a mensagem de solu��o de acordo com a data
	Local dDtValid:= StoD( Space( 8 ) )//Receber� a data a ser validada
	Local dDtTmp	:= NGSeek( "SRA" , M->TNQ_MAT , 1 , "SRA->RA_ADMISSA" )//Recebe a data de admiss�o do Funcion�rio
	Local lRet		:= .T.//Controle de Retorno

	Default nDtVal:= 1

	//Define a data que ser� validada
	If nDtVal == 1//Data de Sa�da
		dDtValid := M->TNQ_DTSAID
		cSolucao := STR0024//"Favor informar uma Data de Sa�da maior."
	ElseIf nDtVal == 2//Data de Entrada
		dDtValid := M->TNQ_DTINIC
		cSolucao := STR0025//"Favor informar uma Data de Entrada menor."
	EndIf

	//Verifica se a data � maior que a data de admiss�o
	If !Empty(dDtValid) .And. !Empty( M->TNQ_MAT ) .And. ValType( dDtTmp ) == "D"
		If dDtTmp > dDtValid
			MsgStop( STR0014 )  //"A data n�o pode ser anterior � data de admiss�o do funcion�rio."
			lRet := .F.
		EndIf
	EndIf

	//Verifica se o update foi rodado e se a valida��o anterior est� correta
	If lRet .And. NGCADICBASE( "TNQ_DTINIC" , "A" , "TNQ" , .F. )
		//Valida somente quando as duas datas est�o preenchidas, verifica se data de entrada � superior a data de sa�da
		If !Empty( M->TNQ_DTINIC ) .And. !Empty( M->TNQ_DTSAID ) .And. M->TNQ_DTINIC > M->TNQ_DTSAID
				ShowHelpDlg( STR0012 , ;//"Aten��o"
								{ STR0023 } , 1 , ;//"Data de Sa�da n�o pode ser inferior a Data de Entrada."
								{ cSolucao } , 1 )
				lRet := .F.
		EndIf
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTA660Leg
Cria uma janela contendo a legenda da mBrowse

@type    function
@author  Denis Hyroshi de Souza
@since   12/09/2006
@sample  MDTA660Leg()
@return  L�gico, Sempre verdadeiro
/*/
//-------------------------------------------------------------------
Function MDTA660Leg()

	If Type( "cCadastro" ) == "U"
		Private cCadastro := STR0010  //"Legenda"
	EndIf

	BrwLegenda(	OemToAnsi(cCadastro)	,;	//Titulo do Cadastro
				OemToAnsi( STR0010 )	,; //"Legenda"
				{;
					{"BR_VERDE"		,OemToAnsi(STR0015)	}	,;  //"Titular - Situa��o Normal"
					{"BR_VERMELHO"	,OemToAnsi(STR0016)	}	,;  //"Titular - Com mais de 4 faltas"
					{"BR_AZUL"		,OemToAnsi(STR0017)	}	 ;  //"Suplente"
				};
			)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTA660Cor
Define cores do  semaforo no browse

@type    function
@author  Denis Hyroshi de Souza
@since   12/09/2006
@sample  MDTA660Cor()
@return  aCores, Array, Contem o array de cores
/*/
//-------------------------------------------------------------------
Function MDTA660Cor()
	Local aCores	:=	{ { "MDT660TITU(.T.)"  		 , 'BR_VERDE'		},;
						{ "MDT660TITU(.F.)"  		 , 'BR_VERMELHO'	},;
						{ "TNQ->TNQ_TIPCOM == '2'" , 'BR_AZUL' 		}}
Return aCores

//-------------------------------------------------------------------
/*/{Protheus.doc} MDT660TITU
Verifica se o titular esta normal

@type    function
@author  Denis Hyroshi de Souza
@since   12/09/2006
@sample  MDT660TITU( .T. )
@param   lNormal, L�gico, Verificando se � normal ate 4 faltas

@return  L�gico, Verdadeiro se o titular est� regular
/*/
//-------------------------------------------------------------------
Function MDT660TITU( lNormal )

	Local nFaltas := 0

	lUPDMDTA4 := NGCADICBASE("TNQ_DTINIC","D","TNQ",.F.) //Verifica se existe o campo "TNQ_DTINIC" na base.

	If lSigaMdtps

		If TNQ->TNQ_TIPCOM == '1'
			aAreaLeg := GetArea()

			dbSelectarea("TNR")
			dbSetorder(2)  //TNR_FILIAL+TNR_CLIENT+TNR_LOJA+TNR_MANDAT+DTOS(TNR_DTREUN)+TNR_HRREUN
			dbSeek(xFilial("TNR")+cCliMdtps+TNQ->TNQ_MANDAT)
			While !eof() .And. xFilial("TNR")+cCliMdtps+TNQ->TNQ_MANDAT == TNR_FILIAL+TNR_CLIENT+TNR_LOJA+TNR_MANDAT .And. nFaltas <= 4

				If lUPDMDTA4
					If TNQ->TNQ_DTINIC > TNR->TNR_DTREAL
						dbSelectarea("TNR")
						Dbskip()
					EndIf
				EndIf

				If TNR->TNR_TIPREU == '1' .And. !Empty(TNR->TNR_DTREAL)
					dbSelectarea("TNS")
					dbSetorder(5)  //TNS_FILIAL+TNS_CLIENT+TNS_LOJA+TNS_MANDAT+DTOS(TNS_DTREUN)+TNS_HRREUN+TNS_MAT
					If !dbSeek(xFilial("TNS")+cCliMdtps+TNR->(TNR_MANDAT+DTOS(TNR_DTREUN)+TNR_HRREUN)+TNQ->TNQ_MAT)
						nFaltas++
					ElseIf TNS->TNS_PRESEN == '3'
						nFaltas++
					EndIf
				EndIf

				dbSelectarea("TNR")
				Dbskip()
			End

			RestArea(aAreaLeg)

			If lNormal
				//verificando se � normal (ate 4 faltas)
				Return (nFaltas <= 4)
			Else
				//verificando se tem + de 4 faltas
				Return (nFaltas > 4)
			EndIf
		EndIf

	Else

		If TNQ->TNQ_TIPCOM == '1'
			aAreaLeg := GetArea()

			dbSelectarea("TNR")
			dbSetorder(1)
			dbSeek(xFilial("TNR")+TNQ->TNQ_MANDAT)
			While !eof() .And. xFilial("TNR")+TNQ->TNQ_MANDAT == TNR_FILIAL+TNR_MANDAT .And. nFaltas <= 4

				If lUPDMDTA4
					If TNQ->TNQ_DTINIC > TNR->TNR_DTREAL
						dbSelectarea("TNR")
						Dbskip()
					EndIf
				EndIf

				If TNR->TNR_TIPREU == '1' .And. !Empty(TNR->TNR_DTREAL)
					dbSelectarea("TNS")
					dbSetorder(1)
					If !dbSeek(xFilial("TNS")+TNR->(TNR_MANDAT+DTOS(TNR_DTREUN)+TNR_HRREUN)+TNQ->TNQ_MAT)
						nFaltas++
					ElseIf TNS->TNS_PRESEN == '3'
						nFaltas++
					EndIf
				EndIf

				dbSelectarea("TNR")
				Dbskip()
			End

			RestArea(aAreaLeg)

			If lNormal
				//verificando se � normal (ate 4 faltas)
				Return (nFaltas <= 4)
			Else
				//verificando se tem + de 4 faltas
				Return (nFaltas > 4)
			EndIf
		EndIf

	EndIf

Return .F.

//-------------------------------------------------------------------
/*/{Protheus.doc} ADDCCIP660
Inclusao da data de validade da estabilidade no campo RA_DTVTEST
referente a CIPA

@type    function
@author  Ricardo Dal Ponte
@since   22/09/2006
@sample  ADDCCIP660()

@param   cMandato, Caractere, C�digo do madato
@param   cCandidato, Caractere, C�digo do candidato
@param   cFil1Tmp, Caractere, C�digo da Filial
@param   cTipoEst, Caractere, Tipo do componente
@param   lShowMsg, L�gico, Se apresenta mensagens em tela

@return  L�gico, Sempre verdadeiro
/*/
//-------------------------------------------------------------------
Function ADDCCIP660( cMandato, cCandidato, cFil1Tmp, cTipoEst , lShowMsg )

	Local nTipoEst
	Local dDataVEsta
	Local dDataGrav
	Local dDtCand
	Local cTiptAux
	Local cIntegra := GetMv("MV_MDTGPE")
	Local cSup     := SuperGetMv( "MV_NG2CSUP" , .F. , "1" )
	Local lConsRFX := AliasInDic("RFX") .And. FindFunction("MDTEstFunc") // Variavel de consistencia da RFX

	Default lShowMsg := .T.

	If cIntegra == "S"

		nTipoEst := If( cTipoEst == "1", 8, 9 )

		If lSigaMdtPs
			cSeekTNN := xFilial("TNN")+cCliMdtps+cMandato
			cSeekSRA := xFilial("SRA")+cCandidato
		Else
			cSeekTNN := xFilial("TNN")+cMandato
			cSeekSRA := xFilial("SRA",cFil1Tmp)+cCandidato
		EndIf

		Dbselectarea("TNN")
		DbSetOrder(1)  //TNN_FILIAL+TNN_MANDAT##TNN_FILIAL+TNN_CLIENT+TNN_LOJAC+TNN_MANDAT
		If Dbseek(cSeekTNN)
			dDataVEsta := TNN->TNN_DTTERM


			Dbselectarea("SRA")
			DbSetOrder(1)
			If Dbseek(cSeekSRA) .And. ( cSup == "1" .Or. cTipCand == "1" )

				// Per�odo de estabilidade
				dDataGrav := NGSomaAno( dDataVEsta , 1 )

				If Empty(SRA->RA_DTVTEST) .Or. SRA->RA_DTVTEST <= dDataGrav
					RecLock("SRA",.F.)

					If Altera .And. nTipInd1 == "2" .And. nTipInd2 == "1"
						SRA->RA_DTVTEST := CTOD("  /  /  ")
						If lShowMsg
							MsgInfo(STR0007)//"A partir deste momento o Candidato para CIPA deixa de ter estabilidade!"
						EndIf
						//Cadastra c�digo na Tabela de estabilidade RFX.
						If lConsRFX
							//Busca a data de candidatura do Funcion�rio
							dDtCand		:= NGSeek( "TNO", cMandato + cFil1Tmp + SRA->RA_MAT, 3, "TNO_DTCAND" ) //TNO_FILIAL+TNO_MANDAT+TNO_FILMAT+TNO_MAT+DTOS(TNO_DTCAND)
							If !Empty(dDtCand) .And. dDtCand <= dDataGrav //Caso candidatura seja superior a data de termino da estabilidade, n�o gera
								cTiptAux	:= MDTEstFunc( nTipoEst )//Busca o tipo de estabilidade de acordo com o Tipo do Componente (Titular ou Suplente) - Busca se da pelo tipo de estabilidade eSocial ( 08 - Eleito Titular CIPA;\09 - Eleito Suplente CIPA; )
								If !Empty( cTiptAux )
									dbSelectArea( "RFX" )
									dbSetOrder( 1 ) //RFX_FILIAL+RFX_MAT+RFX_DTOS(RFX_DATI)+RFX_TPESTB
										If dbSeek( xFilial( "RFX" ) + SRA->RA_MAT + DTOS( dDtCand ) + cTiptAux )//Caso j� tenha a estabilidade, remove a estabilidade
											RecLock( "RFX" , .F. )
												RFX->( dbDelete() )
											RFX->( MsUnLock() )
										EndIf
								EndIf
							EndIf
						EndIf
					Else
						If SRA->RA_DTVTEST < dDataGrav .And. nTipInd2 == "2"
							SRA->RA_DTVTEST := dDataGrav
							If lShowMsg
								MsgInfo(STR0008+DTOC(dDataGrav)+"!") //"A partir deste momento o Componente da CIPA ter� estabilidade at� "
							EndIf
							If lConsRFX
								//Busca a data de candidatura do Funcion�rio
								dDtCand		:= NGSeek( "TNO", cMandato + cFil1Tmp + SRA->RA_MAT, 3, "TNO_DTCAND" ) //TNO_FILIAL+TNO_MANDAT+TNO_FILMAT+TNO_MAT+DTOS(TNO_DTCAND)
								If !Empty(dDtCand) .And. dDtCand <= dDataGrav //Caso candidatura seja superior a data de termino da estabilidade, n�o gera
									cTiptAux	:= MDTEstFunc( nTipoEst )//Busca o tipo de estabilidade de acordo com o Tipo do Componente (Titular ou Suplente) - Busca se da pelo tipo de estabilidade eSocial ( 08 - Eleito Titular CIPA;\09 - Eleito Suplente CIPA; )
									If !Empty( cTiptAux )
										dbSelectArea( "RFX" )
										dbSetOrder( 1 ) //RFX_FILIAL+RFX_MAT+RFX_DTOS(RFX_DATI)+RFX_TPESTB
											If dbSeek( xFilial( "RFX" ) + SRA->RA_MAT + DTOS( dDtCand ) + cTiptAux )//Caso j� tenha a estabilidade, altera apenas a data fim
												RecLock( "RFX" , .F. )
											Else
												RecLock( "RFX" , .T. )
											EndIf
											//Caso inclus�o, salva os campos chaves
											RFX->RFX_FILIAL	:= xFilial( "RFX" ) // Obrigat�rio
											RFX->RFX_MAT		:= SRA->RA_MAT // Obrigat�rio
											RFX->RFX_DATAI	:= dDtCand  // Obrigat�rio
											If RFX->( FieldPos( "RFX_HORAI" ) ) > 0
												RFX->RFX_HORAI  := "00:00" // Obrigat�rio
											EndIf
											RFX->RFX_TPESTB	:=	cTiptAux
											RFX->RFX_DATAF	:= dDataGrav
											If RFX->( FieldPos( "RFX_HORAF" ) ) > 0
												RFX->RFX_HORAF := "23:59"
											EndIf
											RFX->( MsUnLock() )
									EndIf
								EndIf
							EndIf
						EndIf
					EndIf
					SRA->( MsUnlock() )
				EndIf
			EndIf
		EndIf
	EndIf

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} DELCCIP660
Exclusao da data de validade da estabilidade no campo RA_DTVTEST
referente a CIPA

@type    function
@author  Ricardo Dal Ponte
@since   22/09/2006
@sample  DELCCIP660( )

@param   cMandato, Caractere, C�digo do madato
@param   cCandidato, Caractere, C�digo do candidato
@param   cFil1Tmp, Caractere, C�digo da Filial
@param   cTipoEst, Caractere, Tipo do componente

@return  L�gico, Sempre verdadeiro
/*/
//-------------------------------------------------------------------
Function DELCCIP660( cMandato, cCandidato, cFil1Tmp, cTipoEst )

	Local nTipoEst
	Local dDataVEsta
	Local dDataGrav
	Local dDtCand
	Local cTiptAux
	Local cIntegra := GetMv("MV_MDTGPE")
	Local lConsRFX := AliasInDic("RFX") .And. FindFunction("MDTEstFunc") // Variavel de consistencia da RFX
	Local lCipatr  := SuperGetMv("MV_NG2NR31",.F.,"2") == "1"
	Local aAreaTNO

	nTipoEst := If( cTipoEst == "1", 8, 9 )

	If cIntegra == "S"
		If lSigaMdtPs
			cSeekTNN := xFilial("TNN")+cCliMdtps+cMandato
			cSeekSRA := xFilial("SRA")+cCandidato
		Else
			cSeekTNN := xFilial("TNN")+cMandato
			cSeekSRA := xFilial("SRA",cFil1Tmp)+cCandidato
		EndIf

		Dbselectarea("TNN")
		DbSetOrder(1)  //TNN_FILIAL+TNN_MANDAT##TNN_FILIAL+TNN_CLIENT+TNN_LOJAC+TNN_MANDAT
		If Dbseek(cSeekTNN)
			dDataVEsta := TNN->TNN_DTTERM

			Dbselectarea("SRA")
			DbSetOrder(1)
			If Dbseek(cSeekSRA)

				aAreaTNO := GetArea()
				dbSelectArea("TNO")
				dbSetOrder(3) //TNO_FILIAL+TNO_MANDAT+TNO_MAT+DTOS(TNO_DTCAND)
				lIsCand := dbSeek(xFilial("TNO") + cMandato + cFil1Tmp + SRA->RA_MAT)
				RestArea(aAreaTNO)

				If lCipatr
					dDataGrav := NGSomaAno( dDataVEsta , 2 )
				Else
					dDataGrav := NGSomaAno( dDataVEsta , 1 )
				EndIf

				IF SRA->RA_DTVTEST = dDataGrav
					RecLock("SRA",.F.)
					SRA->RA_DTVTEST := IIf(lIsCand, TNN->TNN_ELEICA, CTOD("  /  /  "))
					MsUnlock("SRA")
					If !lIsCand
						MsgInfo(STR0007) //"A partir deste momento o Candidato para CIPA deixa de ter estabilidade!"
					EndIf
					//Atualiza tabela de Estabilidade - RFX
					If lConsRFX
					//Busca a data de candidatura do Funcion�rio
						dDtCand		:= NGSeek( "TNO", cMandato + cFil1Tmp + SRA->RA_MAT, 3, "TNO_DTCAND" ) //TNO_FILIAL+TNO_MANDAT+TNO_FILMAT+TNO_MAT+DTOS(TNO_DTCAND)
						If !Empty(dDtCand) .And. dDtCand <= dDataGrav //Caso candidatura seja superior a data de termino da estabilidade, n�o gera
							cTiptAux	:= MDTEstFunc( nTipoEst )//Busca o tipo de estabilidade de acordo com o Tipo do Componente (Titular ou Suplente) - Busca se da pelo tipo de estabilidade eSocial ( 08 - Eleito Titular CIPA;\09 - Eleito Suplente CIPA; )
							If !Empty( cTiptAux )
								dbSelectArea( "RFX" )
								dbSetOrder( 1 ) //RFX_FILIAL+RFX_MAT+RFX_DTOS(RFX_DATI)+RFX_TPESTB
								If dbSeek( xFilial( "RFX" ) + SRA->RA_MAT + DTOS( dDtCand ) + cTiptAux )//Caso j� tenha a estabilidade, remove a estabilidade
									RecLock( "RFX" , .F. )
									RFX->( dbDelete() )
									RFX->( MsUnLock() )
								EndIf
							EndIf
						EndIf
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf

Return .T.


//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Utilizacao de Menu Funcional

@type    function
@author  Rafael Diogo Richter
@since   29/11/2006
@sample  MenuDef(.T.)
@param   lPersonal, L�gico, Utiliza para a cria��o de menu personalizado
para altera��o do componente da CIPA

@Obs Parametros do array a Rotina:
		1. Nome a aparecer no cabecalho
		2. Nome da Rotina associada
		3. Reservado
		4. Tipo de Transa��o a ser efetuada:
				1 - Pesquisa e Posiciona em um Banco de Dados
			2 - Simplesmente Mostra os Campos
			3 - Inclui registros no Bancos de Dados
			4 - Altera o registro corrente
			5 - Remove o registro corrente do Banco de Dados
		5. Nivel de acesso
		6. Habilita Menu Funcional

@return  aRotina, Array, Op��es da rotina
/*/
//-------------------------------------------------------------------
Static Function MenuDef( lPersonal )

	Local lSigaMdtPS := SuperGetMv("MV_MDTPS",.F.,"N") == "S"
	Local aRotina

	Default lPersonal := IsInCallStack( "MDT660DEMI" )//Utiliza para a cria��o de menu personalizado para altera��o do componente da CIPA

	If lSigaMdtps
		aRotina := { { STR0001,   "AxPesqui"  , 0 , 1},; //"Pesquisar"
					{ STR0002,   "NGCAD01"   , 0 , 2},; //"Visualizar"
					{ STR0018,   "MDT660COM" , 0 , 4} } //"Componentes CIPA"
	Else
		If lPersonal //Menu personalizado
			aRotina := { { STR0001,   "AxPesqui"  , 0 , 1},; //"Pesquisar"
							{ STR0002,   "MDT660PROC"   , 0 , 2},; //"Visualizar"
							{ STR0004,   "MDT660PROC"  , 0 , 4} } //"Alterar"
		Else
			aRotina :=	{	{ STR0001,	"AxPesqui"    , 0 , 1},; //"Pesquisar"
							{ STR0002,	"MDT660PROC"  , 0 , 2},; //"Visualizar"
							{ STR0003,	"MDT660PROC"  , 0 , 3},; //"Incluir"
							{ STR0004,	"MDT660PROC"  , 0 , 4},; //"Alterar"
							{ STR0005,	"MDT660PROC"  , 0 , 5, 3} } //"Excluir"

		EndIf

	EndIf
	lPyme := Iif(Type("__lPyme") <> "U",__lPyme,.F.)

	If !lPyme
		AAdd( aRotina, { STR0022, "MsDocument", 0, 4 } )  //"Conhecimento"
	EndIf

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} MDT660COM
Monta um browse com os componentes da CIPA

@type    function
@author  Andre Perez Alvarez
@since   19/10/2007
@sample  MDT660COM()

@return  Nil, Sempre Nulo
/*/
//-------------------------------------------------------------------
Function MDT660COM()

	Local aArea	    := GetArea()
	Local oldROTINA := aCLONE(aROTINA)
	Local oldCad    := cCadastro
	Local aNao      := { 'TNQ_CLIENT', 'TNQ_LOJA', 'TNQ_FILIAL'}
	cCliMdtPs := SA1->A1_COD+SA1->A1_LOJA
	nSizeSA1 := If((TAMSX3("A1_COD")[1]) < 1,6,(TAMSX3("A1_COD")[1]))
	nSizeLoj := If((TAMSX3("A1_LOJA")[1]) < 1,2,(TAMSX3("A1_LOJA")[1]))

	aRotina :=  { { STR0001,	"AxPesqui"    , 0 , 1},; //"Pesquisar"
				{ STR0002,	"MDT660PROC"  , 0 , 2},; //"Visualizar"
				{ STR0003,	"MDT660PROC"  , 0 , 3},; //"Incluir"
				{ STR0004,	"MDT660PROC"  , 0 , 4},; //"Alterar"
				{ STR0005,	"MDT660PROC"  , 0 , 5, 3} } //"Excluir"

	If TNS->(FieldPos("TNS_PRESEN")) > 0
		aADD(aRotina,{STR0010,"MDTA660Leg", 0 , 6})  //"Legenda"
	EndIf
	// Define o cabecalho da tela de atualizacoes
	cCadastro := OemtoAnsi(STR0006) //"Componentes"
	Private aCHKDEL := {}, bNGGRAVA

	aCHOICE := {}

	aCHOICE := NGCAMPNSX3( 'TNQ' , aNao )

	// Endereca a funcao de BROWSE
	DbSelectArea("TNQ")
	Set Filter To TNQ->(TNQ_CLIENT+TNQ_LOJA) == cCliMdtps
	DbSetorder(6)  //TNQ_FILIAL+TNQ_CLIENT+TNQ_LOJA+TNQ_MANDAT+TNQ_FILMAT+TNQ_MAT+DTOS(TNQ_DTSAID)
	If TNS->(FieldPos("TNS_PRESEN")) > 0
		mBrowse( 6, 1,22,75,"TNQ",,,,,,MDTA660Cor())
	Else
		mBrowse( 6, 1,22,75,"TNQ")
	EndIf

	DbSelectArea("TNQ")
	DbSetorder(6)
	DbSelectArea("TNO")
	DbSetorder(6)

	aROTINA := aCLONE(oldROTINA)
	RestArea(aArea)
	cCadastro := oldCad

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} MDT660NOM
Mostra o nome do funcionario no browse da TNQ

@type    function
@author  Andre Perez Alvarez
@since   24/06/2008
@sample  MDT660NOM()
@return  Nil, Sempre Nulo
/*/
//-------------------------------------------------------------------
Function MDT660NOM()

	Local cDesc := ""
	Local aArea := GetArea()

	cDesc := Posicione("SRA",1,xFilial("SRA",TNQ->TNQ_FILMAT)+TNQ->TNQ_MAT,"RA_NOME")

	RestArea(aArea)

Return cDesc

//-------------------------------------------------------------------
/*/{Protheus.doc} A660DESTIN
Troca F3 do campo TNQ_MAT

@type    function
@author  Denis Hyroshi de Souza
@since   30/06/2008
@sample  A660DESTIN()

@return  L�gico, Sempre verdadeiro
/*/
//-------------------------------------------------------------------
Function A660DESTIN()

	Local aArea := GetArea()

	aTROCAF3 := {}

	If lSigaMdtPS
		If SuperGetMv("MV_MDTF3CO",.F.,"2") == "1"
			AADD(aTROCAF3,{"TNQ_MAT","MDTTNO"})
		EndIf
		If Len(aTROCAF3) == 0
			AADD(aTROCAF3,{"TNQ_MAT","MDTNGQ"})
		EndIf
	Else
		If SuperGetMv("MV_MDTF3CO",.F.,"2") == "1"
			AADD(aTROCAF3,{"TNQ_MAT","TNO"})
		EndIf
		If Len(aTROCAF3) == 0
			If lCpoTNQ
				AADD(aTROCAF3,{"TNQ_MAT","NGQ"})
			Else
				AADD(aTROCAF3,{"TNQ_MAT","SRA"})
			EndIf
		EndIf
	EndIf

	RestArea(aArea)

Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} MDT660DEMI
Verifica se o funcion�rio demitido faz parte da CIPA, " componente "

@author Guilherme Freudenburg
@since  02/09/2014
@sample GPEA010, MDTA660
@return
/*/
//---------------------------------------------------------------------
Function MDT660DEMI()

	Local lSigaMdtPS := SuperGetMv("MV_MDTPS",.F.,"N") == "S"
	Local aAreaSRA := GetArea()//Salva o registro posicionado
	Local aAreaTNQ
	Local nAcao   :=0
	Local lComp   := .F.
	Local lCipatr := If( SuperGetMv("MV_NG2NR31",.F.,"2") == "1", .T. , .F. )

	If !Empty(M->RA_DEMISSA) .And. !lSigaMdtps//Verifica se a data de demiss�o esta preenchida
		dbSelectArea("TNQ")
		dbSetOrder(2)//TNQ_FILIAL+TNQ_MAT+TNQ_MANDAT
		If dbSeek(xFilial("TNQ")+SRA->RA_MAT)//Verifica se funcion�rio faz parte dos componentes
			aAreaTNQ := GetArea()
			While TNQ->(!Eof()) .And. xFilial("TNQ") == TNQ->TNQ_FILIAL .And. SRA->RA_MAT == TNQ->TNQ_MAT .And. !lComp
				If Empty(TNQ->TNQ_DTSAID)//Verifica se tem algum mandato com a data de saida vazia
					lComp := .T.
				EndIf
				TNQ->(dbSkip())
			End
			RestArea(aAreaTNQ)
			If lComp
				nAcao := Aviso(STR0012,STR0026+Alltrim(SRA->RA_NOME)+ If(lCipatr,STR0037,STR0027),{STR0030,STR0031,STR0032})//" � um componente da CIPATR, deseja informar a data de Sa�da Manualmente ou pela data de Demiss�o ?" //O Funcion�rio ###" � um componente da CIPA, deseja informar a data de Sa�da Manualmente ou pela data de Demiss�o ?"  ## Manual ## Demiss�o ## Sair
				If nAcao == 1//Manualmente
					MDTA660()//Chama rotina de Componentes da cipa
				EndIf
				If nAcao == 2//Pela data de Demiss�o
					While TNQ->(!Eof()) .And. xFilial("TNQ") == TNQ->TNQ_FILIAL .And. SRA->RA_MAT == TNQ->TNQ_MAT
						If Empty(TNQ->TNQ_DTSAID)//Verifica se a data de saida esta vazia
							Reclock('TNQ',.F.)
								TNQ->TNQ_DTSAID:=SRA->RA_DEMISSA//Prenche a data de Saida com o valo da data de Demiss�o
							TNQ->(MsUnLock())
						EndIf
						TNQ->(dbSkip())
					End
				EndIf
			EndIf
		EndIf
	EndIf

	RestArea(aAreaSRA)

Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} MDT660SAID
Verifica se a data de Demiss�o � menor que a de saida da CIPA

@author Guilherme Freudenburg
@since 02/09/2014
@sample MDTA660
@return
/*/
//---------------------------------------------------------------------
Function MDT660SAID()

	Local lRet:= .T.
	Local aAreaTNQ := GetArea()//Salva o registro posicionado

	If !Empty(M->TNQ_DTSAID)//Verifica se a data de saida nao esta vazia
		dbSelectArea("SRA")
		dbSetOrder(1)
		dbSeek(xFilial("SRA")+M->TNQ_MAT)
		If !Empty(SRA->RA_DEMISSA)//Verifica se a data de demiss�o esta preenchida
			If SRA->RA_DEMISSA <= M->TNQ_DTSAID//Verifica se a data de demiss�o � menor que a de saida
				ShowHelpDlg(STR0012,{STR0028},1,{STR0029},2)//"ATEN��O" ##"A data de Demiss�o � menor que a de sa�da do Mandato."##"Favor informar uma data de Sa�da maior."
				lRet:= .F.
			EndIf
		EndIf
	EndIf
	RestArea(aAreaTNQ)

Return lRet

//---------------------------------------------------------------------
/*/{Protheus.doc} MDT660WHEN
Verifica se o MDTA660 foi chamado pela fun��o MDT660DEMI,
caso isso ocorra fechar� os campos.

@param nCmp Numerico Indica qual campo est� utilizando o When:
1 - TNQ_TIPCOM, 2 - TNQ_INDICA, 3 - TNQ_INDFUN

@author Guilherme Freudenburg
@since  02/09/2014
@sample MDTA660
@return
/*/
//---------------------------------------------------------------------
Function MDT660WHEN(nCmp)
Return !( IsInCallStack( "MDT660DEMI" ) )

//---------------------------------------------------------------------
/*/{Protheus.doc} MDT660INC
Verifica se o Membro esta sendo reeleito pela 2� vez consecutiva.
Conforme a lei n�o � permitido.

@author Jean Pytter da Costa
@since  26/08/2015
@sample MDT660INC
@return
/*/
//---------------------------------------------------------------------
Function MDT660INC()

	Local aArea	:= GetArea()
	Local lRet	:= .T.
	Local nCont	:= 0     // Contador de reelei��es
	Local dMandAnt	:= 0 // Ano do Mandato Anterior do Membro
	Local dMandPost	:= 0 // Ano do Mandato Posterior do Membro
	Local dMandSeg  :=0   // Data dois anos antes ou depois
	Local dMandPri  :=0   // Data um ano antes ou depois
	Local lEleito   := .F.// Se esta sendo eleito, por exemplo entre 2015 e 2017, no caso 2016.
	Local lCipatr   := If( SuperGetMv("MV_NG2NR31",.F.,"2") == "1", .T. , .F. )

	cAliasTNN := GetNextAlias()
	cTabTNN := RetSqlName("TNN")
	cTabTNQ := RetSqlName("TNQ")

	//Filtra todos Mandatos que o Membro atual foi eleito por ordem de Data de Inicio
	cQuery := "SELECT TNN.TNN_FILIAL, TNN.TNN_MANDAT, TNN.TNN_DTINIC, TNQ.TNQ_MAT, TNQ.TNQ_MANDAT "
	cQuery += "FROM " + cTabTNQ + " TNQ "
	cQuery += "INNER JOIN " + cTabTNN + " TNN ON "
	cQuery += 		"TNN.TNN_MANDAT = TNQ.TNQ_MANDAT AND "
	cQuery += 		"TNQ.TNQ_MAT = '" + M->TNQ_MAT + "' AND "
	cQuery += 		"TNN.TNN_FILIAL = '" + xFilial("TNN") + "' AND "
	cQuery += 		"TNN.D_E_L_E_T_ != '*' "
	cQuery += "WHERE "
	cQuery += 		"TNQ.D_E_L_E_T_ != '*' AND TNQ.TNQ_INDICA = '2' "
	cQuery += "ORDER BY TNN.TNN_DTINIC "
	cQuery := ChangeQuery(cQuery)

	MPSysOpenQuery( cQuery , cAliasTNN )

	DbSelectArea( "TNN" )
	DbSetOrder(1)//TNN_FILIAL+TNN_MANDAT

	If DbSeek( xFilial( "TNN" ) + M->TNQ_MANDAT )

		dMandAtu := Year( TNN->TNN_DTINIC ) //Ano do mandato que esta sendo incluso

		DbSelectArea( cAliasTNN )
		DbGoTop()

		While !Eof() .And. ( cAliasTNN )->TNQ_MAT == M->TNQ_MAT

			dMandMem := Year( STOD( ( cAliasTNN )->TNN_DTINIC ) ) //Ano em que o membro participou da CIPA

			If ( dMandMem + 2 == dMandAtu .Or. dMandMem - 2 == dMandAtu ) //Verifica se foi eleito dois anos antes ou depois
				nCont++
				dMandSeg := dMandMem
			EndIf
			If ( dMandMem + 1 == dMandAtu .Or. dMandMem - 1 == dMandAtu ) //Verifica se foi eleito um ano antes ou depois
				nCont++
				dMandPri := dMandMem
			EndIf

			//Caso estiver sendo incluso um Membro no ano retroativo
			//-------------------
			dMandAnt	:= If( dMandAtu - 1 == dMandMem, dMandMem, dMandAnt ) //Data Anterior ao incluso
			dMandPost	:= If( dMandAtu + 1 == dMandMem, dMandMem, dMandPost ) //Data Posterior ao incluso
			//-------------------

			//Verifica se ja foi eleito no ano anterior e posterior
			If dMandAnt + 1 == dMandAtu .And. dMandPost - 1 == dMandAtu
				lEleito := .T.
				exit
			EndIf

			//Verifica se no pr�ximo e no outro ano ele � membro, no caso se atual � 2016 e ele ja foi membro em 2017 e 2018
			//No caso esta sendo feito uma inclus�o retroativa
			If dMandAtu + 2 == dMandSeg .And. dMandAtu + 1 == dMandPri
				lEleito := .T.
				exit
			EndIf

			//Verifica datas retroativas, no caso se atual � 2016 e se ele ja foi membro em 2014 e 2015
			//N�o � possivel mais de uma reelei��o em anos seguidos.
			If dMandAtu - 2 == dMandSeg .And. dMandAtu - 1 == dMandPri
				lEleito := .T.
				exit
			EndIf

			DbSelectArea( cAliasTNN )
			( cAliasTNN )->( DbSkip() )
		End

		//Verifica se data de inclus�o esta entre alguma data, por exemplo entre 2014 e 2017.
		//� zerado o contador, pois � possivel incluir.
		If !( lEleito )
			If dMandAnt == 0 .Or. dMandPost == 0
				nCont := 0
			EndIf
		EndIf

		If nCont > 1 .Or. lEleito
			ShowHelpDlg( STR0012 , ;//"Aten��o"
			{ If( lCipatr, STR0038, STR0033) } , 1 , ; //"O mandato dos membros eleitos da CIPATR ter� a dura��o de dois anos, permitida uma reelei��o." //"O mandato dos membros eleitos da CIPA ter� a dura��o de um ano, permitida uma reelei��o."
			{ STR0034 } , 1 ) //"Selecionar outro membro para o mandato, pois esse membro j� foi reeleito uma vez."
			lRet := .F.
		EndIf

	EndIf

	RestArea(aArea)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} MDT660SEC
Fun��o que valida se o funcion�rio que est� sendo cadastrado como
componente da cipa � igual ao inserido no cadastro do mandato da cipa
de secret�rio titular e suplente.

@type    function
@author  Julia Kondlatsch
@since   18/10/2018
@sample  MDT660SEC( '001', 'D MG 01', '000014', '2' )

@param   cFilComp, Caractere, Filial do componenete
@param   cMatric, Caractere, Matr�cula do componente
@param   cFuncCipa, Caractere, Fun��o do componente dentro da CIPA
( 1-Presidente, 2-Vice-Presidente, 3-Secretario, 4-Secretario Substituto )

@return  Nil, Sempre Nulo
/*/
//-------------------------------------------------------------------
Function MDT660SEC( cMandat, cFilComp, cMatric, cFuncCipa )

	Local cNomeComp := ''
	Local cNomeCipa := ''
	Local lYesNo    := .F.
	Local cTNNFil   := ''
	Local cTNNMat   := ''
	Local cTipo     := ''

	// Se o componente cadastrado for um secret�rio ou secret�rio substituto
	If !Empty(cFuncCipa) .And. (cFuncCipa == '3' .Or. cFuncCipa == '4')

		cTNNFil := IIf( cFuncCipa == '3', 'TNN->TNN_FILRE1', 'TNN->TNN_FILRE2' )
		cTNNMat := IIf( cFuncCipa == '3', 'TNN->TNN_MATRE1', 'TNN->TNN_MATRE2' )
		cTipo   := IIf( cFuncCipa == '3', STR0039, STR0040 ) //'Secret�rio(a)' # 'Secret�rio Substituto'

		dbSelectArea('TNN') // Mandatos CIPA
		dbSetOrder(1)
		If dbSeek( xFilial('TNN') + cMandat )

			// Se n�o for o mesmo funcion�rio cadastadminrado como secret�rio titular no mandato da CIPA
			If !Empty( &(cTNNMat) ) .And. ( &(cTNNFil) <> cFilComp .Or. &(cTNNMat) <> cMatric )

				cNomeComp := Alltrim(Posicione( 'SRA', 1, xFilial('SRA', cFilComp) + cMatric, 'RA_NOME' ))
				cNomeCipa := Alltrim(Posicione( 'SRA', 1, xFilial('SRA', &(cTNNFil)) + &(cTNNMat), 'RA_NOME' ))

				lYesNo := MsgYesNo( STR0041 + cNomeComp + STR0042 + cTipo + STR0043 + STR0039 + ' ' + cNomeCipa + STR0044, STR0012 )
				// 'ATEN��O' # 'Foi identificado que o componente ' #, que foi cadastrado(a) como ' # ', � diferente do(a) ' #
				// 'Secret�rio(a)' # ' que foi definido no mandato CIPA (TNN). Deseja atualizar o cadastro do mandato?'

			EndIf

			// Se o usu�rio esacolheu sobrescrever ou se o registro est� vazio
			If Empty( &(cTNNMat) ) .Or. lYesNo

				// Grava no mandato o componente cadastrado
				Reclock('TNN',.F.)
					&(cTNNFil) := cFilComp
					&(cTNNMat) := cMatric
				TNN->(MsUnLock())

			EndIf

			If lYesNo
				MsgInfo(STR0045) //'O registro do mandarto CIPA foi modificado.'
			EndIf

		EndIf

	EndIf

Return Nil