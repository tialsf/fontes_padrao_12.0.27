#INCLUDE "MDTA881.ch"
#INCLUDE "PROTHEUS.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTA881
Programa que gera a carga inicial das condi��es diferenciadas

@return

@sample MDTA881()

@author
@since 16/04/14
/*/
//---------------------------------------------------------------------
Function MDTA881()

	// Armazena as vari�veis
	Local aNGBEGINPRM := NGBEGINPRM()
	Local aSay		:= {}
	Local aButton	:= {}
	Local nOpc		:= 0
	Local cTitulo	:= STR0001 //"CARGA INICIAL ESOCIAL"
	Local cDesc1	:= STR0002 //"Esta rotina realiza a carga inicial dos itens pertinentes"
	Local cDesc2	:= STR0003 //"a medicina e seguran�a do trabalho para o TAF  (eSocial)."
	Local cDesc3	:= STR0004 //"Importante: Deve ser executado uma �nica vez por empresa."
	Local leSocial	:= SuperGetMv( "MV_NG2ESOC" , .F. , "2" ) == "1"
	Local lFirst	:= SuperGetMv( "MV_NG2BLEV" , .F. , "2" ) == "1"
	Local lOk		:= .T.

	If lFirst
		If leSocial
			If !NGCADICBASE("TOI_ESOC","A","TOI",.F.)
				If !NGINCOMPDIC("UPDMDT84","TPMUF0",.F.)
					//  Devolve variaveis armazenadas (NGRIGHTCLICK)
					NGRETURNPRM(aNGBEGINPRM)
					Return .F.
				EndIf
			EndIf

			aAdd( aSay, cDesc1 )
			aAdd( aSay, cDesc2 )
			aAdd( aSay, cDesc3 )

			aAdd( aButton, { 1 , .T. , { | | nOpc := 1, FechaBatch() } } )
			aAdd( aButton, { 2 , .T. , { | | FechaBatch() } } )

			FormBatch( cTitulo , aSay , aButton )
			If nOpc == 1
				Processa( { | | lOk := fProcReg() }, STR0005 , STR0006 , .F. ) //"Aguarde"###"Processando registros..."
				If !lOk
					MsgStop( STR0011 ) //"Envio ao TAF n�o realizado pois existem inconsist�ncias nos dados a serem enviados, favor verificar!"
				Else
					MsgInfo( STR0012 ) //"Envio realizado com sucesso!"
					PUTMV( "MV_NG2BLEV" , "2" ) //Seta valor para que n�o seja poss�vel abrir a rotina mais de 1 vez
				EndIf
			EndIf
		Else
			Aviso( OemToAnsi( STR0013 ), ; //"Aten��o"
					OemToAnsi( STR0014 + ; //"Esta rotina se destina a realizar a gera��o das informa��o inicias "
							   STR0015 + CRLF + ; //"de Condi��es Diferenciadas de Trabalho (Evento S-2240) para o m�dulo do TAF. "
							   STR0016 ), ; //"Desta forma, � necess�rio habilitar a integra��o com o SIGATAF para envio."
					{OemToAnsi("Ok")})
		EndIf
	Else
		MsgStop( STR0017 ) //"Essa a��o j� foi realizada, n�o � permitido o reprocessamento!"
	EndIf

	// Devolve as vari�veis armazenadas
	NGRETURNPRM(aNGBEGINPRM)

Return
//---------------------------------------------------------------------
/*/{Protheus.doc} fProcReg
Processa os Funcion�rios

@return Nulo

@sample fProcReg()

@author
@since 16/04/14
/*/
//---------------------------------------------------------------------
Static Function fProcReg()

	Local lRet		:= .T.
	Local aFuncs	:= {}
	Local aFunEnv	:= {}
	Local nCont

	//Pega todos os funcion�rios ativos
	aFuncs := MDTGetFunc()

	For nCont := 1 To Len( aFuncs )

		cMatricula	:= aFuncs[ nCont , 1 ]
		cCCusto		:= Posicione( "SRA" , 1 , xFilial("SRA") + cMatricula , "RA_CC"			)
		cFuncao		:= Posicione( "SRA" , 1 , xFilial("SRA") + cMatricula , "RA_CODFUNC"	)
		cDepart		:= Posicione( "SRA" , 1 , xFilial("SRA") + cMatricula , "RA_DEPTO"		)
		cCodUnic	:= Posicione( "SRA" , 1 , xFilial("SRA") + cMatricula , "RA_CODUNIC"	)

		aAdd( aFunEnv , { cMatricula , cCodUnic , cCCusto , cFuncao , , cDepart } )

	Next nCont

	If Len( aFunEnv ) > 0
		lRet := MDTFatTAF( aFunEnv , 3 , .T. , , , .T. ) //Verifica as inconsist�ncias

		If lRet
			lRet := MDTFatTAF( aFunEnv , 3 , .T. ) //Envia ao TAF
		EndIf
	EndIf

Return lRet