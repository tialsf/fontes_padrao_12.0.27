#INCLUDE "PROTHEUS.CH"
#INCLUDE "MDTA882.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTA882
Job que realiza a verifica��o di�ria de poss�veis novos per�odos de tarefas
dos funcion�rios para envio ao TAF (eSocial).

@return

@sample MDTA882()

@author Luis Fellipy Bett
@since 27/03/18
/*/
//---------------------------------------------------------------------
Function MDTA882()

	// Armazena as vari�veis
	Local leSocial	:= SuperGetMv( "MV_NG2ESOC" , .F. , "2" ) == "1"
	Local lRet		:= .T.
	Local aNGBEGINPRM

	//Se possuir integra��o com o TAF
	If leSocial
		If IsBlind() //Se via schedule
			lRet := fTarFunc( .T. ) //Processa gera��o

			If !lRet
				FWLogMsg( 'WARN' , , 'BusinessObject' , 'MDTA882' , '' , '01' , STR0008 , 0 , 0 , {} ) //"Erro na execu��o do Schedule, favor verificar!"
			Else
				FWLogMsg( 'WARN' , , 'BusinessObject' , 'MDTA882' , '' , '01' , STR0009 , 0 , 0 , {} ) //"Execu��o realizada com sucesso!"
			EndIf
		Else //Se via rotina
			aNGBEGINPRM := NGBEGINPRM()

			Processa( { || lRet := fTarFunc() } , STR0010 ) //"Aguarde, processando os registros..."

			If lRet
				MsgInfo( STR0011 ) //"Processamento realizado com sucesso!"
			Else
				MsgStop( STR0012 ) //"Falha no processamento!"
			EndIf

			NGRETURNPRM( aNGBEGINPRM )
		EndIf
	EndIf

Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} fTarFunc
Fun��o que envia os dados da tarefa do funcion�rio ao TAF (eSocial).

@return lRet

@sample fTarFunc()

@author Luis Fellipy Bett
@since 27/03/18
/*/
//---------------------------------------------------------------------
Function fTarFunc( lSchedule )

	Local cMatricula
	Local nCont
	Local aFunc		:= {}
	Local aFunEnv	:= {}
	Local aLogProc	:= {}
	Local lRet		:= .T.
	Local lErro		:= .T.
	Local cMsg		:= ""
	Local cDirFile  := '\esocial_mdt'

	Default lSchedule := .F.

	//Pega todos os funcion�rios ativos
	aFunc := MDTGetFunc()

	For nCont := 1 To Len( aFunc )

		cMatricula	:= aFunc[ nCont , 1 ]
		cCCusto		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CC"		)
		cFuncao		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CODFUNC"	)
		cDepart		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_DEPTO"	)
		cCodUnic	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CODUNIC"	)

		//Tarefas por Funcion�rio
		dbSelectArea( "TN6" )
		dbSetOrder( 2 ) //TN6_FILIAL+TN6_MAT
		dbSeek( xFilial( "TN6" ) + cMatricula )
		While xFilial( "TN6" ) == TN6->TN6_FILIAL .And. TN6->TN6_MAT == cMatricula
			If TN6->TN6_DTINIC = dDataBase .Or. TN6->TN6_DTTERM = dDataBase
				aAdd( aFunEnv , { cMatricula , cCodUnic , cCCusto , cFuncao , TN6->TN6_CODTAR , cDepart , TN6->TN6_DTINIC , TN6->TN6_DTTERM } )
				Exit
			EndIf
			dbSkip()
		End

	Next nCont

	If Len( aFunEnv ) > 0 //Se houverem funcion�rios a serem integrados
		If !lSchedule // Se for execu��o via rotina

			lRet := MDTFatTAF( aFunEnv , 4 , .T. , , , .T. ) //Verifica inconsist�ncias

			If lRet
				lRet := MDTFatTAF( aFunEnv , 4 , .T. ) //Envia ao TAF
			EndIf

		Else // Se for execu��o via Schedule

			If !File( cDirFile )
				MakeDir( cDirFile )
			EndIf

			cArqPesq := cDirFile + "\mdt_evts2240_" + DToS( Date() ) + "_" + StrTran( Time() , ":" , "" ) + ".txt"

			lRet := MDTFatTAF( aFunEnv , 4 , .T. , .T. , , , , @cMsg ) //Envia ao TAF

			nHandle  := FCREATE( cArqPesq , 0 ) //Cria arquivo no diret�rio

			//----------------------------------------------------------------------------------
			// Verifica se o arquivo pode ser criado, caso contrario um alerta sera exibido
			//----------------------------------------------------------------------------------
			If FERROR() <> 0
				lErro := .F.
			Endif

			If lErro
				FWrite( nHandle , cMsg )

				FCLOSE( nHandle )
			Else
				lRet := .F.
			EndIf

		EndIf
	EndIf

Return lRet
//---------------------------------------------------------------------
/*/{Protheus.doc} SchedDef
Execu��o de Par�metros na Defini��o do Schedule

@return aParam, Array, Conteudo com as defini��es de par�metros para WF

@sample SchedDef()

@author Alexandre Santos
@since 04/07/2018
/*/
//---------------------------------------------------------------------
Static Function SchedDef()
Return {"P", "PARAMDEF", "", {}, "Param"}