#INCLUDE "Protheus.ch"
#INCLUDE "MDTESOCIAL.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTESOCIAL
Fonte com as fun��es gen�ricas usadas nos eventos de integra��o do SIGAMDT com o eSocial.

@author Luis Fellipy Bett
@since 16/07/2018
/*/
//---------------------------------------------------------------------

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTGeraXml

Fun��o gen�rica que realiza a exporta��o dos Xml's do eSocial para arquivos .xml

@author  Luis Fellipy Bett
@since   16/07/2018

@return .T. - L�gico - Retorna true ap�s gerar o Xml
/*/
//---------------------------------------------------------------------
Function MDTGeraXml()

	Local cMascara  	:= STR0001 //"Todos os arquivos|."
	Local cTitulo   	:= STR0002 //"Escolha o destino do arquivo"
	Local nMascpad  	:= 0
	Local cDirini   	:= "\"
	Local lSalvar   	:= .F. /*.F. = Salva || .T. = Abre*/
	Local nOpcoes   	:= GETF_LOCALHARD
	Local lArvore   	:= .F. /*.T. = apresenta o �rvore do servidor || .F. = n�o apresenta*/
	Local cDiretorio
	Local lSucess
	Local nHandle
	Local cAliasFunc	:= GetNextAlias()
	Local dDtRis		:= SToD("")
	Local cArqPesq		:= ""
	Local cXml			:= ""
	Local cMatricula	:= ""
	Local cMatFun		:= ""
	Local cCodUnic		:= ""
	Local cCPF			:= ""
	Local cIDFunc		:= ""
	Local cFuncao		:= ""
	Local cCodAmb		:= ""
	Local cDtIni		:= ""
	Local cAnoMes		:= ""
	Local cFilArq		:= ""
	Local cFilEnv		:= ""
	Local aSucess		:= {}
	Local aFailure		:= {}
	Local aCBOMoto		:= {}
	Local aLogProc		:= {}
	Local aFunTre		:= {}
 	Local nCont			:= 0
	Local nCont2		:= 0
	Local lValid		:= .T.
	Local lRet			:= .T.
	Local lFecha		:= .F.
	Local oModelTNE

	//Busca filial de envio entre a matriz e as filhas
	cFilEnv := MDTBFilEnv()

	//----- Vari�veis para busca de informa��es espec�ficas para cada chamada ------
	If IsInCallStack("GPEA010") //Vari�veis de Busca dos Fatores de Risco

		cMatricula := SRA->RA_MAT

	ElseIf IsInCallStack("MDTA120") //Vari�veis de Busca do Exame Toxicol�gico

		cMatricula	:= Posicione( "TM0" , 1 , xFilial( "TM0" ) + TM5->TM5_NUMFIC , "TM0_MAT" )
		cFuncao		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CODFUNC" )
		aCBOMoto	:= {{ "782310" } , { "782320" } , { "782405" } , { "782410" } ,;
						{ "782415" } , { "782505" } , { "782510" } , { "782515" }}
		dbSelectArea( "SRJ" )
		dbSetOrder( 1 )
		dbSeek( xFilial( "SRJ" ) + cFuncao )
		lValid := aScan( aCBOMoto , { | x | x[ 1 ] == SRJ->RJ_CODCBO } ) > 0 .And. !Empty( TM5->TM5_CODDET ) .And. !Empty( TM5->TM5_DTRESU )

		If lValid
			lValid := !Empty( Posicione( "TM0" , 1 , xFilial( "TM0" ) + TM5->TM5_NUMFIC , "TM0_MAT" ) )
			cMsgVld := STR0023 //"A gera��o de Xml para o eSocial s�o apenas para registros de funcion�rios"
		Else
			cMsgVld := STR0003 //A gera��o de Xml para o eSocial s�o apenas para exames Toxicol�gicos! O campo c�digo do DENATRAN deva estar preenchido, o exame deve ter uma data de resultado e o CBO do funcion�rio deve ser de motorista!"
		EndIf

	ElseIf IsInCallStack("MDTA200") //Atestado ASO

		lValid := !Empty( Posicione( "TM0" , 1 , xFilial( "TM0" ) + TMY->TMY_NUMFIC , "TM0_MAT" ) )
		cMsgVld := STR0023 //"A gera��o de Xml para o eSocial s�o apenas para registros de funcion�rios"

	ElseIf IsInCallStack("MDTA640") //Acidentes

		lValid := !Empty( Posicione( "TM0" , 1 , xFilial( "TM0" ) + TNC->TNC_NUMFIC , "TM0_MAT" ) )
		cMsgVld := STR0023 //"A gera��o de Xml para o eSocial s�o apenas para registros de funcion�rios"

	ElseIf IsInCallStack("TRMA070") //Treinamentos e Capacita��es

		lValid := !Empty( RA2->RA2_ESOC ) .And. RA2->RA2_REALIZ == "S"
		cMsgVld := STR0034 //"A gera��o de Xml para o eSocial s�o apenas para treinamentos baixados que possuam um c�digo eSocial vinculado!"

	EndIf

	//Valida��es anteriores a gera��o do Xml de acordo com cada chamada
	If !lValid
		MsgInfo( cMsgVld )
	Else

		If IsInCallStack("GPEA010")
			aOpcMnp := { STR0028, STR0027 , STR0030 } //"Altera��o"###"Inclus�o"###"Fechar"
		Else
			aOpcMnp := { STR0029, STR0028, STR0027 , STR0030 } //"Exclus�o"###"Altera��o"###"Inclus�o"###"Fechar"
		EndIf

		nAviso := Aviso( STR0025 , STR0026 , aOpcMnp ) //"Gera��o Xml eSocial"###"Escolha o tipo de manipula��o a ser considerada na gera��o do Xml"

		If IsInCallStack("GPEA010")
			Do Case
				Case nAviso == 1 ; nOpcMnp := 4
				Case nAviso == 2 ; nOpcMnp := 3
				Case nAviso == 3 ; lFecha := .T.
			End Case
		Else
			Do Case
				Case nAviso == 1 ; nOpcMnp := 5
				Case nAviso == 2 ; nOpcMnp := 4
				Case nAviso == 3 ; nOpcMnp := 3
				Case nAviso == 4 ; lFecha := .T.
			End Case
		EndIf

		If !lFecha
			cDiretorio := cGetFile( cMascara, cTitulo, nMascpad, cDirIni, lSalvar, nOpcoes, lArvore)
			cDiretorio := StrTran( cDiretorio, "\.", "\" )
		Else
			cDiretorio := ""
		EndIf

		If cDiretorio <> ""

			If IsInCallStack("MDTA165") //S-1060 - Ambiente F�sico

				cFilArq := StrTran( AllTrim( xFilial("TNE") ) , " " , "_" ) + "_"

				oModelTNE := FwLoadModel("MDTA165")
				oModelTNE:SetOperation(1)
				oModelTNE:Activate()

				cCodAmb := AllTrim( oModelTNE:GetValue( 'TNEMASTER' , 'TNE_CODAMB' ) )
				cDtIni	:= DToS( oModelTNE:GetValue( 'TNEMASTER', 'TNE_DTVINI' ) )
				cAnoMes := SubStr( cDtIni , 1 , 6 )

				cXml := MDTM001( cAnoMes , 1 , {} , {} , cFilEnv , .T. , nOpcMnp , , oModelTNE , , .T. )
				cArqPesq := cFilArq + "evt_S-1060_" + DToS( date() ) + "_" + StrTran( time() , ":", "" ) + "_" + cCodAmb + cDtIni + ".xml" //evt_S-1060_X_X_X.xml"

			ElseIf IsInCallStack("MDTA640") //S-2210 - Comunica��o de Acidente de Trabalho

				cFilArq := StrTran( AllTrim( xFilial("TNC") ) , " " , "_" ) + "_"

				cMatFun		:= Posicione( "TM0" , 1 , xFilial( "TM0" ) + TNC->TNC_NUMFIC , "TM0_MAT" )
				cCodUnic	:= AllTrim( Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatFun , "RA_CODUNIC" ) )

				If nOpcMnp <> 5 //Se n�o for Xml de exclus�o
					cXml := MDTM002( .T. , cFilEnv , nOpcMnp , TNC->TNC_FILIAL + TNC->TNC_ACIDEN , , , , , .T. , .F. )
				Else
					cXml := MDTM006( "S-2210" , , cMatFun , DToS( TNC->TNC_DTACID ) + StrTran( TNC->TNC_HRACID , ":" , "" ) + TNC->TNC_TIPCAT , cFilEnv , .T. )
				EndIf

				cArqPesq := cFilArq + "evt_S-2210_" + DToS( date() ) + "_" + StrTran( time() , ":", "" ) + "_" + cCodUnic + DToS( TNC->TNC_DTACID ) + StrTran( TNC->TNC_HRACID , ":" , "" ) + ".xml" //evt_S-2210_X_X_X.xml"

			ElseIf IsInCallStack("MDTA200") //S-2220 - Monitoramento de Sa�de do Trabalhador - Exame Ocupacional

				cFilArq := StrTran( AllTrim( xFilial("TMY") ) , " " , "_" ) + "_"

				cMatFun		:= Posicione( "TM0" , 1 , xFilial( "TM0" ) + TMY->TMY_NUMFIC , "TM0_MAT" )
				cCodUnic	:= AllTrim( Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatFun , "RA_CODUNIC" ) )

				If nOpcMnp <> 5 //Se n�o for Xml de exclus�o
					cXml := MDTM003( .T. , cFilEnv , nOpcMnp , TMY->TMY_FILIAL + TMY->TMY_NUMASO , .T. , .F. )
				Else
					cXml := MDTM006( "S-2220" , TMY->TMY_NUMFIC , , DToS( TMY->TMY_DTEMIS ) , cFilEnv , .T. )
				EndIf

				cArqPesq := cFilArq + "evt_S-2220_" + DToS( date() ) + "_" + StrTran( time() , ":", "" ) + "_" + cCodUnic + DToS( TMY->TMY_DTEMIS ) + ".xml" //evt_S-2220_X_X_X.xml"

			ElseIf IsInCallStack("MDTA120") //S-2221 - Monitoramento de Sa�de do Trabalhador - Exame Toxicol�gico

				cFilArq := StrTran( AllTrim( xFilial("SRA") ) , " " , "_" ) + "_"

				cMatFun		:= Posicione( "TM0" , 1 , xFilial( "TM0" ) + TM5->TM5_NUMFIC , "TM0_MAT" )
				cCodUnic	:= AllTrim( Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatFun , "RA_CODUNIC" ) )

				If nOpcMnp <> 5 //Se n�o for Xml de exclus�o
					cXml := MDTM005( cFilEnv , nOpcMnp , .T. , .F. )
				Else
					cXml := MDTM006( "S-2221" , TM5->TM5_NUMFIC , , DToS( TM5->TM5_DTRESU ) , cFilEnv , .T. )
				EndIf

				cArqPesq := cFilArq + "evt_S-2221_" + DToS( date() ) + "_" + StrTran( time() , ":", "" ) + "_" + cCodUnic + DToS( TM5->TM5_DTRESU ) + ".xml" //evt_S-2220_X_X_X.xml"

			ElseIf IsInCallStack("GPEA010") //S-2240 - Condi��es Ambientais de Trabalho - Fatores de Risco

				//Trata mensagem inicial de inconsist�ncias
				aAdd( aLogProc , STR0014 ) //"Inconsist�ncias da Exposi��o"
				aAdd( aLogProc , STR0033 ) //"Os campos abaixo est�o vazios/zerados ou possuem inconsist�ncia com rela��o ao formato padr�o do eSocial: "
				aAdd( aLogProc , "" )
				aAdd( aLogProc , "" )

				cFilArq	 := StrTran( AllTrim( xFilial("SRA") ) , " " , "_" ) + "_"

				cCodUnic	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CODUNIC"	)
				cCPF		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CIC"		)
				cIDFunc		:= FGetIdInt( "cpfTrab" , "matricula" , cCPF , cCodUnic )

				BeginSQL Alias cAliasFunc
					SELECT CM9.CM9_DTINI
						FROM %table:CM9% CM9
						WHERE CM9.CM9_FILIAL = %xFilial:CM9% AND
								CM9.CM9_FUNC = %exp:cIDFunc% AND
								CM9.%NotDel%
						ORDER BY CM9.CM9_DTINI DESC
				EndSQL

				dbSelectArea( cAliasFunc )
				dDtRis := SToD(( cAliasFunc )->( CM9_DTINI )) //�ltima data de exposi��o do funcion�rio
				(cAliasFunc)->(dbCloseArea())

				If Empty( dDtRis )
					dDtRis := dDataBase
				EndIf

				cXml := MDTM004( cFilEnv , cMatricula , nOpcMnp , dDtRis , .T. , aLogProc )

				cArqPesq := cFilArq + "evt_S-2240_" + DToS( date() ) + "_" + StrTran( time() , ":", "" ) + "_" + AllTrim( cCodUnic ) + DToS( dDtRis ) + ".xml" //evt_S-2240_X_X_X.xml"

			ElseIf IsInCallStack("TRMA070") //S-2245 - Treinamentos e Capacita��es

				//Trata mensagem inicial de inconsist�ncias
				aAdd( aLogProc , STR0032 ) //"Inconsist�ncias dos Treinamentos e Capacita��es"
				aAdd( aLogProc , STR0033 ) //"Os campos abaixo est�o vazios/zerados ou possuem inconsist�ncia com rela��o ao formato padr�o do eSocial: "
				aAdd( aLogProc , "" )
				aAdd( aLogProc , "" )

				cFilArq := StrTran( AllTrim( xFilial("RA4") ) , " " , "_" ) + "_"

				//Busca na TN3 os EPIs vinculados ao Fornecedor posicionado
				dbSelectArea( "RA4" )
				dbSetOrder( 3 ) // RA4_FILIAL + RA4_CALEND + RA4_CURSO + RA4_TURMA + RA4_MAT
				If dbSeek( xFilial( "RA4" ) + RA2->RA2_CALEND + RA2->RA2_CURSO + RA2->RA2_TURMA )
					While xFilial( "RA4" )  == RA4->RA4_FILIAL .And. RA4->RA4_CALEND == RA2->RA2_CALEND .And. ;
						RA4->RA4_CURSO == RA2->RA2_CURSO .And. RA4->RA4_TURMA == RA2->RA2_TURMA
						aAdd( aFunTre , { RA4->RA4_MAT , RA4->RA4_CALEND , RA4->RA4_CURSO , RA4->RA4_TURMA } )
						dbSkip()
					End
				EndIf

				If nOpcMnp <> 5 //Se n�o for exclus�o

					For nCont := 1 To Len( aFunTre )
						MDTM008( cFilEnv , aFunTre[ nCont , 1 ] , .T. , aFunTre[ nCont , 2 ] , aFunTre[ nCont , 3 ] , aFunTre[ nCont , 4 ] , nOpcMnp , @aLogProc , .T. )
					Next nCont

					If Len( aLogProc ) > 4 //Se houver inconsist�ncias
						fMakeLog( { aLogProc } , { STR0016 } , Nil , Nil , "MDTM008" , OemToAnsi( STR0017 ) , "M" , "P" , , .F. ) //"Monitoramento Envio de Eventos - TAF"###"Log de Ocorr�ncias - Tabela de Equipamentos de Prote��o"

						Help( ' ', 1, STR0021, , STR0024, 2, 0,,,,,, { STR0035 } ) //"Aten��o" ## "Os dados do Xml n�o est�o de acordo com a estrutura do eSocial" ## "Favor ajustar os valores expostos no relat�rio!"

						lRet := .F.
					Else
						For nCont := 1 To Len( aFunTre )

							cCodUnic := AllTrim( Posicione( "SRA" , 1 , xFilial( "SRA" ) + aFunTre[ nCont , 1 ] , "RA_CODUNIC" ) )

							cXml	 := MDTM008( cFilEnv , aFunTre[ nCont , 1 ] , .T. , aFunTre[ nCont , 2 ] , aFunTre[ nCont , 3 ] , aFunTre[ nCont , 4 ] , nOpcMnp )

							cArqPesq := cFilArq + "evt_S-2245_" + DToS( date() ) + "_" + StrTran( time() , ":", "" ) + "_" + cCodUnic + AllTrim( RA2->RA2_ESOC ) + ".xml" //evt_S-2245_X_X_X.xml"

							nHandle	 := FCREATE(cArqPesq, 0) //Cria arquivo no diret�rio

								//----------------------------------------------------------------------------------
								// Verifica se o arquivo pode ser criado, caso contrario um alerta sera exibido
								//----------------------------------------------------------------------------------
							If FERROR() <> 0
								MsgAlert( STR0004 + cArqPesq ) //"N�o foi poss�vel abrir ou criar o arquivo: "
								Return
							Endif

							FWrite( nHandle, cXml )

							FCLOSE(nHandle)

							lSucess := CpyS2T( cArqPesq , cDiretorio )

							If lSucess
								aAdd( aSucess , { cArqPesq } )
							Else
								aAdd( aFailure , { cArqPesq } )
							Endif

							FERASE( cArqPesq )

						Next nCont
					EndIf

				Else
					For nCont := 1 To Len( aFunTre )

						cCodUnic := AllTrim( Posicione( "SRA" , 1 , xFilial( "SRA" ) + aFunTre[ nCont , 1 ] , "RA_CODUNIC" ) )

						cXml	 := MDTM006( "S-2245" , , aFunTre[ nCont , 1 ] , RA2->RA2_ESOC , cFilEnv , .T. ) //Evento de Exclus�o

						cArqPesq := cFilArq + "evt_S-2245_" + DToS( date() ) + "_" + StrTran( time() , ":", "" ) + "_" + cCodUnic + AllTrim( RA2->RA2_ESOC ) + ".xml" //evt_S-2245_X_X_X.xml"

						nHandle	 := FCREATE(cArqPesq, 0) //Cria arquivo no diret�rio

							//----------------------------------------------------------------------------------
							// Verifica se o arquivo pode ser criado, caso contrario um alerta sera exibido
							//----------------------------------------------------------------------------------
						If FERROR() <> 0
							MsgAlert( STR0004 + cArqPesq ) //"N�o foi poss�vel abrir ou criar o arquivo: "
							Return
						Endif

						FWrite( nHandle, cXml )

						FCLOSE(nHandle)

						lSucess := CpyS2T( cArqPesq , cDiretorio )

						If lSucess
							aAdd( aSucess , { cArqPesq } )
						Else
							aAdd( aFailure , { cArqPesq } )
						Endif

						FERASE( cArqPesq )

					Next nCont
				EndIf
			EndIf

			If lRet .And. IsInCallStack("TRMA070")

				If Len( aFunTre ) > 0
					If Len( aSucess ) > 0
						AutoGrLog( STR0005 + '"' + cDiretorio + '":' ) //Arquivos copiados com sucesso para ###
						For nCont2 := 1 To Len( aSucess )
							AutoGrLog( aSucess[ nCont2 , 1 ] )
						Next
						AutoGrLog( '' )
					EndIf

					If Len( aFailure ) > 0
						AutoGrLog( STR0006 ) //"Erro ao copiar os arquivos: "
						For nCont2 := 1 To Len( aFailure )
							AutoGrLog( aFailure[ nCont2 , 1 ] )
						Next
						AutoGrLog( '' )
					EndIf

					If Len( aSucess ) > 0 .Or. Len( aFailure ) > 0
						MostraErro() //Mostra o Log
					EndIf
				Else
					MsgInfo( STR0007 ) //"N�o existem registros a serem integrados!"
				EndIf

			Else

				If ValType( cXml ) == "C"
					nHandle  := FCREATE( cArqPesq , 0 ) //Cria arquivo no diret�rio

					//----------------------------------------------------------------------------------
					// Verifica se o arquivo pode ser criado, caso contrario um alerta ser� exibido
					//----------------------------------------------------------------------------------
					If FERROR() <> 0
						MsgAlert( STR0004 + cArqPesq ) //"N�o foi poss�vel abrir ou criar o arquivo: "
						Return
					Endif

					FWrite( nHandle, cXml )

					FCLOSE( nHandle )

					lSucess := CpyS2T( cArqPesq , cDiretorio )

					If lSucess
						MsgInfo( STR0008 + '"' + cArqPesq + '"' + STR0009 + '"' + cDiretorio + '"!' ) //'Arquivo "X" copiado com sucesso para "X"!'
					Else
						MsgAlert( STR0010 + '"' + cArqPesq + '"!' ) //'Erro ao copiar o arquivo "X"!'
					Endif

					FERASE( cArqPesq )

				Else
					MsgAlert( STR0010 + '"' + cArqPesq + '"' + STR0012 ) //"Erro ao copiar o arquivo XXX pois ele n�o est� de acordo com a estrutura do eSocial!"
				EndIf

			EndIf
		EndIf
	EndIf

Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTSubTxt
Funcao que substitui os caracteres especiais por espacos

@return cImpLin Caracter Texto sem caracteres especiais

@sample MDTSubTxt( 'Ol�' )

@param cTexto Caracter Texto a ser verificado

@author Jackson Machado
@since 28/01/2015
/*/
//---------------------------------------------------------------------
Function MDTSubTxt( cTexto )

	Local aAcentos	:= {}
	Local aAcSubst	:= {}
	Local cImpCar 	:= Space( 01 )
	Local cImpLin 	:= ""
	Local cAux 	  	:= ""
	Local cAux1	  	:= ""
	Local nTamTxt 	:= Len( cTexto )
	Local nCont
	Local nPos

	// Para alteracao/inclusao de caracteres, utilizar a fonte TERMINAL no IDE com o tamanho
	// maximo possivel para visualizacao dos mesmos.
	// Utilizar como referencia a tabela ASCII anexa a evidencia de teste (FNC 807/2009).

	aAcentos :=	{;
		Chr(199),Chr(231),Chr(196),Chr(197),Chr(224),Chr(229),Chr(225),Chr(228),Chr(170),;
		Chr(201),Chr(234),Chr(233),Chr(237),Chr(244),Chr(246),Chr(242),Chr(243),Chr(186),;
		Chr(250),Chr(097),Chr(098),Chr(099),Chr(100),Chr(101),Chr(102),Chr(103),Chr(104),;
		Chr(105),Chr(106),Chr(107),Chr(108),Chr(109),Chr(110),Chr(111),Chr(112),Chr(113),;
		Chr(114),Chr(115),Chr(116),Chr(117),Chr(118),Chr(120),Chr(122),Chr(119),Chr(121),;
		Chr(065),Chr(066),Chr(067),Chr(068),Chr(069),Chr(070),Chr(071),Chr(072),Chr(073),;
		Chr(074),Chr(075),Chr(076),Chr(077),Chr(078),Chr(079),Chr(080),Chr(081),Chr(082),;
		Chr(083),Chr(084),Chr(085),Chr(086),Chr(088),Chr(090),Chr(087),Chr(089),Chr(048),;
		Chr(049),Chr(050),Chr(051),Chr(052),Chr(053),Chr(054),Chr(055),Chr(056),Chr(057),;
		Chr(038),Chr(195),Chr(212),Chr(211),Chr(205),Chr(193),Chr(192),Chr(218),Chr(220),;
		Chr(213),Chr(245),Chr(227),Chr(252);
		}

	aAcSubst :=	{;
		"C","c","A","A","a","a","a","a","a",;
		"E","e","e","i","o","o","o","o","o",;
		"u","a","b","c","d","e","f","g","h",;
		"i","j","k","l","m","n","o","p","q",;
		"r","s","t","u","v","x","z","w","y",;
		"A","B","C","D","E","F","G","H","I",;
		"J","K","L","M","N","O","P","Q","R",;
		"S","T","U","V","X","Z","W","Y","0",;
		"1","2","3","4","5","6","7","8","9",;
		"E","A","O","O","I","A","A","U","U",;
		"O","o","a","u";
		}

	For nCont := 1 To Len( AllTrim( cTexto ) )
		cImpCar	:= SubStr( cTexto , nCont , 1 )
		//-- Nao pode sair com 2 espacos em branco.
		cAux	:= Space( 01 )
		nPos 	:= 0
		nPos 	:= Ascan( aAcentos , cImpCar )
		If nPos > 0
			cAux := aAcSubst[ nPos ]
		Elseif ( cAux1 == Space( 1 ) .And. cAux == Space( 1 ) ) .Or. Len( cAux1 ) == 0
			cAux :=	""
		EndIf
		cAux1 	:= cAux
		cImpCar	:= cAux
		cImpLin	:= cImpLin + cImpCar

	Next nCont

	//--Volta o texto no tamanho original
	cImpLin := Left( cImpLin + Space( nTamTxt ) , nTamTxt )

Return cImpLin

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTGetEst
Pega a inscri��o e o tipo de inscri��o do local de onde est� sendo enviado o Evento.
Se for evento vinculado ao funcion�rio pega do seu Centro de Custo (CTT) sen�o pega da empresa (SM0)

@author  Luis Fellipy Bett
@since   19/07/2018

@param	cCCusto	- Caracter	- Indica o Centro de Custo do Funcion�rio que ser� verificado
@return	aEstab	- Array		- Array com a Inscri��o e o Tipo de Inscri��o a serem enviados pelo Evento
/*/
//-------------------------------------------------------------------
Function MDTGetEst( cCCusto , cFilEnv )

	Local aEstab	 := {}
	Local aCTT		 := {}
	Local aSM0		 := {}
	Local nPosCmp	 := 0
	Local lCond		 := .F.
	Local cFilLocCTT := FWxFilial("CTT", cFilEnv)
	Local nPosLot    := 0
	Local cCEIObra   := ""
	Local cCAEPF     := ""
	Local aCC        := fGM23CTT()//extrai lista de c.custo da filial conectada "xfilial(CTT)" ...
	Local cTpInsc

	If !Empty(cCCusto) .And. Len(aCC) > 0
		nPosLot := aScan(aCC,{|x| x[1] == cFilLocCTT .And. x[2] == cCCusto })
		If nPosLot > 0
			//CTT->CTT_TPLOT == "01" .And. CTT->CTT_TIPO2 == "4" .And. CTT->CTT_CLASSE == "2"
			If aCC[nPosLot,6] == "01" .And. aCC[nPosLot,3] == "4" .And. aCC[nPosLot,8] == "2"
				aAdd( aEstab , { aCC[nPosLot,3] , aCC[nPosLot,4] } )
			EndIf
		EndIf
	Endif

	If Len( aEstab ) == 0
		If fBuscaOBRA( cFilEnv, @cCEIObra )
			aAdd( aEstab , { "4" , cCEIObra } )
		Elseif fBuscaCAEPF( cFilEnv, @cCAEPF )
			aAdd( aEstab , { "3" , cCAEPF } )
		EndIf
	Endif

	If Len( aEstab ) > 0
		// Se achou o tipo de inscri��o do Centro de Custo mas n�o for CPF ou CNPJ pega da SM0
		lCond := (Empty( aEstab[ 1 , 1 ] ) .Or. Empty( aEstab[ 1 , 2 ] )) .Or. (!Empty( aEstab[ 1 , 1 ] ) .And. aEstab[ 1 , 1 ] # "1/2")
	Else
		lCond := .T.
	EndIf

	If lCond
		aSM0 := fGetSM0()
		nPosCmp := aScan( aSM0 , { | x | x [ 1 ] == AllTrim( cFilEnv ) } )
		If nPosCmp > 0
			aEstab := {}
			aAdd( aEstab , { aSM0[ nPosCmp , 3 ] , aSM0[ nPosCmp , 2 ] } )
		EndIf
	EndIf

Return aEstab

//-------------------------------------------------------------------
/*/{Protheus.doc} fGetCTT

Pega a Inscri��o e o Tipo de Inscri��o do Centro de Custo do Funcion�rio

@author  Luis Fellipy Bett
@since   19/07/2018

@param	cCCusto	- Caracter	- Indica o Centro de Custo do qual ser�o pegas as informa��es
@return	aCTT	- Array		- Retorna a Inscri��o e o Tipo de Inscri��o do Centro de Custo do Funcion�rio
/*/
//-------------------------------------------------------------------
Static Function fGetCTT( cCCusto , cFilEnv )

	Local aArea			:= GetArea()
	Local aCTT			:= {}
	Local cCTT	 		:= ""
	Local lSemFilial	:= .F.

	If FWModeAccess( "CTT" , 1 ) == "C" .And. FWModeAccess( "CTT" , 2 ) == "C" .And. FWModeAccess( "CTT" , 3 ) == "C"
		lSemFilial := .T.
	Endif

	dbSelectArea( "CTT" )
	dbSetOrder( 1 )
	If dbSeek( cFilEnv + cCCusto )
		If ( !Empty( CTT->CTT_NOME ) .And. !Empty( CTT->CTT_TPLOT ) )
		//Consolidada
			If !lSemFilial
				cCTT := CTT->CTT_FILIAL + CTT->CTT_CUSTO
			Else
				cCTT := CTT->CTT_CUSTO
			EndIf
			aCTT := { cCTT , CTT->CTT_TIPO2 , CTT->CTT_CEI2 , CTT->CTT_TPLOT }
		EndIf
	EndIf

	RestArea( aArea )

Return aCTT

//-------------------------------------------------------------------
/*/{Protheus.doc} fGetSM0

Pega a Inscri��o e o Tipo de Inscri��o da Empresa

@author  Luis Fellipy Bett
@since   19/07/2018

@return	aEstabele - Array - Retorna a Inscri��o e o Tipo de Inscri��o da Empresa
/*/
//-------------------------------------------------------------------
Static Function fGetSM0()

	Local aArea			:= GetArea()
	Local aAreaSM0		:= SM0->(GetArea())
	Local aEstabele		:= {}
	Local cCNPJ			:= ""
	Local cFilialLot	:= ""
	Local cTipo			:= ""

	// Preenche um array com as filiais
	dbSelectArea( "SM0" )
	SM0->( dbGoTop() )
	While SM0->( !EoF() )
		cCNPJ	:= ""
		cTipo	:= ""
		cFilialLot := SM0->M0_CODFIL
		If SM0->M0_TPINSC == 2 //CNPJ
			cCNPJ := SM0->M0_CGC
			cTipo := "1"
		ElseIf SM0->M0_TPINSC == 3 //CPF
			cCNPJ := SM0->M0_CGC
			cTipo := "2"
		EndIf

		aAdd( aEstabele , {	AllTrim( cFilialLot ) , ;
							cCNPJ , ;
							cTipo } )
		SM0->( dbSkip() )
	EndDo
	RestArea( aAreaSM0 )
	RestArea( aArea )

Return aEstabele

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTInconEsoc

Fun��o que verifica e valida o array das inconsist�ncias enviados pelos
MDTM's, a fim de n�o enviar dados incorretos ao eSocial

@return lRet L�gico Retorna verdadeiro caso n�o haja inconsist�ncias no array

@sample fInconsis( aArray , aArrayMsg , "MensagemLog" )

@param aVldValues	- Array		- Array com os dados a serem validados
@param aLogProc		- Array		- Array que ir� receber os Logs
@param cMsgLog		- Caracter	- Variavel que recebera a mensagem de Log

@author Luis Fellipy Bett
@since 30/08/2018
/*/
//---------------------------------------------------------------------
Function MDTInconEsoc( aVldValues , aLogProc , cMsgLog )

	Local lRet 			:= .T.
	Local nCampos		:= 0
	Local cInfPar		:= "" //Vari�vel de ajuste

	Help := .T. //Desabilita as mensagens de valida��o
	//Valida��es das obrigatoriedades
	For nCampos := 1 To Len( aVldValues )
		If Eval( aVldValues[ nCampos , 4 ] )
			If Empty( aVldValues[ nCampos , 1 ] ) .Or. !Eval( aVldValues[ nCampos , 5 ] )
				If Empty( aVldValues[ nCampos , 1 ] )
					cInfPar := STR0013 //"Em Branco"
				ElseIf ValType( aVldValues[ nCampos , 1 ] ) == "D"
					cInfPar := DToS( aVldValues[ nCampos , 1 ] )
					cInfPar := SubStr(cInfPar,7,2) + "/" + SubStr(cInfPar,5,2) + "/" + SubStr(cInfPar,1,4)
				ElseIf ValType( aVldValues[ nCampos , 1 ] ) == "N"
					cInfPar := cValToChar( aVldValues[ nCampos , 1 ] )
				Else
					cInfPar := aVldValues[ nCampos , 1 ]
				EndIf
				aAdd( aLogProc , aVldValues[ nCampos , 2 ] + aVldValues[ nCampos , 3 ] + ": " + cInfPar )
				If !Eval( aVldValues[ nCampos , 5 ] )
					aAdd( aLogProc , STR0018 + aVldValues[ nCampos , 6 ] ) //"Valida��o: "
				EndIf
				aAdd( aLogProc , '' )
				If IsInCallStack( "MDTM004" )
					cMsgLog += CRLF + aVldValues[ nCampos , 2 ] + aVldValues[ nCampos , 3 ] + ": " + cInfPar
					If !Eval( aVldValues[ nCampos , 5 ] )
						cMsgLog += CRLF + STR0018 + aVldValues[ nCampos , 6 ]
					EndIf
				Else
					cMsgLog += CRLF + " - " + aVldValues[ nCampos , 3 ]
				EndIf
				lRet := .F.
			EndIf
		EndIf
	Next nCampos
	Help := .F. //Habilita as mensagens de valida��o

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTObriEsoc

Realiza verifica��o dos campos obrigat�rios nos seus respectivos
cadastros, para n�o haver inconsist�ncias no envio ao TAF

@author  Luis Fellipy Bett
@since   25/07/2018

@param   cTabela	- Caracter	- Indica as Tabelas que ser�o verificadas

@return lRet		- L�gico	- Retorna .T. ou .F. de acordo com as verifica��es dos campos
/*/
//-------------------------------------------------------------------
Function MDTObriEsoc( cTabela , lDelete , oModel , aCols , aHeader )

	Local cIncEsoc		:= SuperGetMv( "MV_NG2AVIS" , .F. , "1" )
	Local leSocial		:= SuperGetMv( "MV_NG2ESOC" , .F. , "2" ) == "1"
	Local aCposInc		:= {}
	Local lRet			:= .T.
	Local cMsg			:= ""
	Local nCont			:= 0

	Default lDelete := .F.

	//Se � pra mostrar a mensagem e/ou impedir o processo
	If leSocial .And. cIncEsoc <> "2" .And. !lDelete

		If "TM4" $ cTabela //MDTA020 - Exames
			If( TM4->(ColumnPos("TM4_PROCRE")) > 0 .And. Empty( M->TM4_PROCRE ) , aAdd( aCposInc , { NGRETTITULO( "TM4_PROCRE" ) } ) , )
		EndIf

		If "TMK" $ cTabela //MDTA070 - Usu�rios
			If( TMK->(ColumnPos("TMK_CIC"	))	> 0	.And. M->TMK_RESAMB == "1" .And. Empty( M->TMK_CIC ) , aAdd( aCposInc , { NGRETTITULO( "TMK_CIC"	 )	} ) , )
			If( TMK->(ColumnPos("TMK_NIT"	))	> 0	.And. M->TMK_RESAMB == "1" .And. Empty( M->TMK_NIT ) , aAdd( aCposInc , { NGRETTITULO( "TMK_NIT"	 )	} ) , )
			If( TMK->(ColumnPos("TMK_ENTCLA"))	> 0 .And. Empty( M->TMK_ENTCLA	) , aAdd( aCposInc , { NGRETTITULO( "TMK_ENTCLA" )	} ) , )
			If( TMK->(ColumnPos("TMK_NUMENT"))	> 0	.And. Empty( M->TMK_NUMENT	) , aAdd( aCposInc , { NGRETTITULO( "TMK_NUMENT" )	} ) , )
			If( TMK->(ColumnPos("TMK_UF"	))	> 0	.And. Empty( M->TMK_UF		) , aAdd( aCposInc , { NGRETTITULO( "TMK_UF"	 )	} ) , )
		EndIf

		If "TN5" $ cTabela //MDTA090 - Tarefas do Funcion�rio
			If( TN5->(ColumnPos("TN5_ESOC")) > 0 .And. Empty( oModel:GetValue("TN5_ESOC") ) , aAdd( aCposInc , { NGRETTITULO( "TN5_ESOC" ) } ) , )
		EndIf

		If "TM5" $ cTabela //MDTA120 - Exames do Funcion�rio
			If( TM5->(ColumnPos("TM5_USUARI")) > 0 .And. !Empty( M->TM5_CODDET ) .And. Empty( M->TM5_USUARI ) , aAdd( aCposInc , { NGRETTITULO( "TM5_USUARI" )	} ) , )
		EndIf

		If "TMT" $ cTabela //MDTA155 - Diagn�stico M�dico
			If( TMT->(ColumnPos("TMT_DTATEN")) > 0 .And. Empty( M->TMT_DTATEN ) , aAdd( aCposInc , { NGRETTITULO( "TMT_DTATEN" ) } ) , )
			If( TMT->(ColumnPos("TMT_HRATEN")) > 0 .And. Empty( M->TMT_HRATEN ) .Or. AllTrim( M->TMT_HRATEN ) == ":" , aAdd( aCposInc , { NGRETTITULO( "TMT_HRATEN" )	} ) , )
			If( TMT->(ColumnPos("TMT_CID"	)) > 0 .And. Empty( M->TMT_CID 	  ) , aAdd( aCposInc , { NGRETTITULO( "TMT_CID" )	} ) , )
		EndIf

		If "TNE" $ cTabela //MDTA165 - Ambientes de Trabalho
			If( TNE->(ColumnPos("TNE_LOCAMB"))	> 0 .And. Empty( M->TNE_LOCAMB ) , aAdd( aCposInc , { NGRETTITULO( "TNE_LOCAMB" )	} ) , )
			If( TNE->(ColumnPos("TNE_MEMODS"))	> 0 .And. Empty( M->TNE_MEMODS ) , aAdd( aCposInc , { NGRETTITULO( "TNE_MEMODS" )	} ) , )
			If( TNE->(ColumnPos("TNE_CODLOT"))	> 0 .And. M->TNE_LOCAMB == "2" .And. Empty( M->TNE_CODLOT ) , aAdd( aCposInc , { NGRETTITULO( "TNE_CODLOT" )	} ) , )
			If( TNE->(ColumnPos("TNE_TPINS"))	> 0 .And. M->TNE_LOCAMB == "3" .And. Empty( M->TNE_TPINS )  , aAdd( aCposInc , { NGRETTITULO( "TNE_TPINS" )	} ) , )
			If( TNE->(ColumnPos("TNE_NRINS"))	> 0 .And. M->TNE_LOCAMB == "3" .And. Empty( M->TNE_NRINS )  , aAdd( aCposInc , { NGRETTITULO( "TNE_NRINS" )	} ) , )
		EndIf

		If "TN0" $ cTabela //MDTA180 - Riscos
			If( TN0->(ColumnPos("TN0_CODAMB"))	> 0 .And. Empty( M->TN0_CODAMB	) , aAdd( aCposInc , { NGRETTITULO( "TN0_CODAMB" )	} ) , )
			If( TN0->(ColumnPos("TN0_ATISAL"))	> 0 .And. Empty( M->TN0_ATISAL	) , aAdd( aCposInc , { NGRETTITULO( "TN0_ATISAL" )	} ) , )
			If( TN0->(ColumnPos("TN0_ATIPER"))	> 0 .And. Empty( M->TN0_ATIPER	) , aAdd( aCposInc , { NGRETTITULO( "TN0_ATIPER" )	} ) , )
			If( TN0->(ColumnPos("TN0_APOESP"))	> 0 .And. Empty( M->TN0_APOESP	) , aAdd( aCposInc , { NGRETTITULO( "TN0_APOESP" )	} ) , )

			If Posicione( "TMA" , 1 , xFilial( "TMA" ) + M->TN0_AGENTE , "TMA_AVALIA" ) == "1"
				If Empty( M->TN0_UNIMED	) .Or. Empty( M->TN0_QTAGEN	)
					If( TN0->(ColumnPos("TN0_UNIMED")) > 0 .And. Empty( M->TN0_UNIMED ) , aAdd( aCposInc , { NGRETTITULO( "TN0_UNIMED" )	} ) , )
					If( TN0->(ColumnPos("TN0_QTAGEN")) > 0 .And. Empty( M->TN0_QTAGEN ) , aAdd( aCposInc , { NGRETTITULO( "TN0_QTAGEN" )	} ) , )
				EndIf
			EndIf
		EndIf

		If "TMA" $ cTabela //MDTA182 - Agentes
			If( TMA->(ColumnPos("TMA_ESOC"	))	> 0 .And. Empty( M->TMA_ESOC	) , aAdd( aCposInc , { NGRETTITULO( "TMA_ESOC"	 )	} ) , )
			If( TMA->(ColumnPos("TMA_AVALIA"))	> 0 .And. Empty( M->TMA_AVALIA	) , aAdd( aCposInc , { NGRETTITULO( "TMA_AVALIA" )	} ) , )
			If( TMA->(ColumnPos("TMA_METERG"))	> 0 .And. Empty( M->TMA_METERG	) .And. M->TMA_GRISCO == "4" , aAdd( aCposInc , { NGRETTITULO( "TMA_METERG" )	} ) , )
		EndIf

		If "TNG" $ cTabela //MDTA600 - Tipos de Acidentes
			If( TNG->(ColumnPos("TNG_ESOC")) > 0 .And. Empty( oModel:GetValue("TNG_ESOC") ) .And. Empty( oModel:GetValue("TNG_ESOC1") ) , aAdd( aCposInc , { NGRETTITULO( "TNG_ESOC" ) } ) , )
			If( TNG->(ColumnPos("TNG_ESOC1")) > 0 .And. Empty( oModel:GetValue("TNG_ESOC1") ) .And. Empty( oModel:GetValue("TNG_ESOC") ) , aAdd( aCposInc , { NGRETTITULO( "TNG_ESOC" ) } ) , )
		EndIf

		If "TOI" $ cTabela //MDTA603 - Parte do Corpo Atingida
			If( TOI->(ColumnPos("TOI_ESOC")) > 0 .And. Empty( M->TOI_ESOC ) , aAdd( aCposInc , { NGRETTITULO( "TOI_ESOC" ) } ) , )
		EndIf

		If "TOJ" $ cTabela //MDTA604 - Natureza da Les�o do Acidente
			If( TOJ->(ColumnPos("TOJ_ESOC")) > 0 .And. Empty( M->TOJ_ESOC ) , aAdd( aCposInc , { NGRETTITULO( "TOJ_ESOC" ) } ) , )
		EndIf

		If "TNH" $ cTabela //MDTA605 - Objeto Causador
			If( TNH->(ColumnPos("TNH_ESOC")) > 0 .And. Empty( M->TNH_ESOC ) , aAdd( aCposInc , { NGRETTITULO( "TNH_ESOC" ) } ) , )
		EndIf

		If "TNC" $ cTabela //MDTA640 - Acidentes
			If( TNC->(ColumnPos("TNC_HRACID")) > 0 .And. Empty( M->TNC_HRACID	) .Or. AllTrim( M->TNC_HRACID ) == ":" , aAdd( aCposInc , { NGRETTITULO( "TNC_HRACID" )	} ) , )
			If( TNC->(ColumnPos("TNC_HRTRAB")) > 0 .And. Empty( M->TNC_HRTRAB	) .Or. AllTrim( M->TNC_HRTRAB ) == ":" , aAdd( aCposInc , { NGRETTITULO( "TNC_HRTRAB" )	} ) , )
			If( TNC->(ColumnPos("TNC_DTACID")) > 0 .And. Empty( M->TNC_DTACID	) , aAdd( aCposInc , { NGRETTITULO( "TNC_DTACID" )	} ) , )
			If( TNC->(ColumnPos("TNC_TPACID")) > 0 .And. Empty( M->TNC_TPACID	) , aAdd( aCposInc , { NGRETTITULO( "TNC_TPACID" )	} ) , )
			If( TNC->(ColumnPos("TNC_TIPCAT")) > 0 .And. Empty( M->TNC_TIPCAT	) , aAdd( aCposInc , { NGRETTITULO( "TNC_TIPCAT" )	} ) , )
			If( TNC->(ColumnPos("TNC_MORTE"	)) > 0 .And. Empty( M->TNC_MORTE	) , aAdd( aCposInc , { NGRETTITULO( "TNC_MORTE"  )	} ) , )
			If( TNC->(ColumnPos("TNC_POLICI")) > 0 .And. Empty( M->TNC_POLICI	) , aAdd( aCposInc , { NGRETTITULO( "TNC_POLICI" )	} ) , )
			If( TNC->(ColumnPos("TNC_INDLOC")) > 0 .And. Empty( M->TNC_INDLOC	) , aAdd( aCposInc , { NGRETTITULO( "TNC_INDLOC" )	} ) , )
			If( TNC->(ColumnPos("TNC_DESLOG")) > 0 .And. Empty( M->TNC_DESLOG	) , aAdd( aCposInc , { NGRETTITULO( "TNC_DESLOG" )	} ) , )
			If( TNC->(ColumnPos("TNC_INTERN")) > 0 .And. Empty( M->TNC_INTERN	) , aAdd( aCposInc , { NGRETTITULO( "TNC_INTERN" )	} ) , )
			If( TNC->(ColumnPos("TNC_AFASTA")) > 0 .And. Empty( M->TNC_AFASTA	) , aAdd( aCposInc , { NGRETTITULO( "TNC_AFASTA" )	} ) , )
			If( TNC->(ColumnPos("TNC_TPLOGR")) > 0 .And. Empty( M->TNC_TPLOGR	) , aAdd( aCposInc , { NGRETTITULO( "TNC_TPLOGR" )	} ) , )
			If( TNC->(ColumnPos("TNC_TIPACI")) > 0 .And. Empty( M->TNC_TIPACI	) , aAdd( aCposInc , { NGRETTITULO( "TNC_TIPACI" )	} ) , )
		EndIf

		If "TNP" $ cTabela //MDTA680 - Emitentes de Atestados
			If( TNP->(ColumnPos("TNP_NOME"	))	> 0 .And. Empty( M->TNP_NOME	) , aAdd( aCposInc , { NGRETTITULO( "TNP_NOME"	 )	} ) , )
			If( TNP->(ColumnPos("TNP_ENTCLA"))	> 0 .And. Empty( M->TNP_ENTCLA	) , aAdd( aCposInc , { NGRETTITULO( "TNP_ENTCLA" )	} ) , )
			If( TNP->(ColumnPos("TNP_NUMENT"))	> 0 .And. Empty( M->TNP_NUMENT	) , aAdd( aCposInc , { NGRETTITULO( "TNP_NUMENT" )	} ) , )
			If( TNP->(ColumnPos("TNP_UF"	)) 	> 0 .And. Empty( M->TNP_UF		) , aAdd( aCposInc , { NGRETTITULO( "TNP_UF"	 )	} ) , )
		EndIf

		If "TNY" $ cTabela //MDTA685 - Atestado M�dico
			If( TNY->(ColumnPos("TNY_HRCONS"))	> 0 .And. Empty( M->TNY_HRCONS	) .Or. AllTrim( M->TNY_HRCONS ) == ":" , aAdd( aCposInc , { NGRETTITULO( "TNY_HRCONS" )	} ) , )
			If( TNY->(ColumnPos("TNY_DTCONS"))	> 0 .And. Empty( M->TNY_DTCONS	) , aAdd( aCposInc , { NGRETTITULO( "TNY_DTCONS" )	} ) , )
			If( TNY->(ColumnPos("TNY_CID"))		> 0 .And. Empty( M->TNY_CID		) .And. !Empty( M->TNY_ACIDEN ) , aAdd( aCposInc , { NGRETTITULO( "TNY_CID"	 )	} ) , )
		EndIf

		If "RA2" $ cTabela //TRMA050 - Treinamentos

			nPosHor := GDFieldPos( "RA2_HORAS"	, aHeader )
			nPosMod := GDFieldPos( "RA2_MODTRE"	, aHeader )
			nPosTip := GDFieldPos( "RA2_TPTRE"	, aHeader )

			For nCont := 1 To Len( aCols )
				If RA2->(ColumnPos("RA2_HORAS")) > 0 .And. Empty( aCols[ nCont , nPosHor ] ) .And. aScan( aCposInc , { | x | x[ 1 ] == NGRETTITULO( "RA2_HORAS" ) } ) == 0
					aAdd( aCposInc , { NGRETTITULO( "RA2_HORAS"	 )	} )
				EndIf
				If RA2->(ColumnPos("RA2_MODTRE")) > 0 .And. Empty( aCols[ nCont , nPosMod ] ) .And. aScan( aCposInc , { | x | x[ 1 ] == NGRETTITULO( "RA2_MODTRE" ) } ) == 0
					aAdd( aCposInc , { NGRETTITULO( "RA2_MODTRE" )	} )
				EndIf
				If RA2->(ColumnPos("RA2_TPTRE")) > 0 .And. Empty( aCols[ nCont , nPosTip ] ) .And. aScan( aCposInc , { | x | x[ 1 ] == NGRETTITULO( "RA2_TPTRE" ) } ) == 0
					aAdd( aCposInc , { NGRETTITULO( "RA2_TPTRE"	 )	} )
				EndIf
			Next nCont
		EndIf

		If "RA7" $ cTabela //TRMA140 - Instrutores
			If( RA7->(ColumnPos("RA7_CIC"	)) > 0 .And. Empty( M->RA7_MAT ) .And. Empty( M->RA7_CIC	) .And. M->RA7_NACION == "1" , aAdd( aCposInc , { NGRETTITULO( "RA7_CIC"	 )	} ) , )
			If( RA7->(ColumnPos("RA7_FORINS")) > 0 .And. Empty( M->RA7_MAT ) .And. Empty( M->RA7_FORINS ) , aAdd( aCposInc , { NGRETTITULO( "RA7_FORINS" )	} ) , )
			If( RA7->(ColumnPos("RA7_CBOINS")) > 0 .And. Empty( M->RA7_MAT ) .And. Empty( M->RA7_CBOINS ) , aAdd( aCposInc , { NGRETTITULO( "RA7_CBOINS" )	} ) , )
			If( RA7->(ColumnPos("RA7_NACION")) > 0 .And. Empty( M->RA7_MAT ) .And. Empty( M->RA7_NACION ) , aAdd( aCposInc , { NGRETTITULO( "RA7_NACION" )	} ) , )
		EndIf

		If Len( aCposInc ) > 0

			If cIncEsoc == "0"

				cMsg := STR0019 //"Os campos abaixo s�o de import�ncia para a consist�ncia das informa��es que ser�o enviadas ao eSocial!"
				For nCont := 1 To Len( aCposInc )
					cMsg += CRLF + "- " + aCposInc[ nCont , 1 ]
				Next nCont
				lRet := MsgYesNo( cMsg + CRLF + STR0020 , STR0021 ) //"Deseja continuar mesmo assim?" ## "Aten��o"
				If !lRet
					HELP( ' ' , 1 , STR0021 , , cMsg, 2 , 0 ,,,,,, { STR0022 } ) //"Aten��o" ## "Favor preench�-los!"
				EndIf

			ElseIf cIncEsoc == "1"

				cMsg := STR0019 //"Os campos abaixo s�o de import�ncia para a consist�ncia das informa��es que ser�o enviadas ao eSocial!"
				For nCont := 1 To Len( aCposInc )
					cMsg += CRLF + "- " + aCposInc[ nCont , 1 ]
				Next nCont
				HELP( ' ' , 1 , STR0021 , , cMsg, 2 , 0 ,,,,,, { STR0022 } ) //"Aten��o" ## "Favor preench�-los!"
				lRet := .F.

			EndIf
		EndIf
	EndIf

Return lRet

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTGetFunc
Busca todos os funcion�rios cadastrados

@return aFunc

@sample fBuscaFunc()

@author Luis Fellipy Bett
@since 09/10/2018
/*/
//---------------------------------------------------------------------
Function MDTGetFunc()

	Local aFunc 		:= {}
	Local cAliasSRA 	:= GetNextAlias()

	BeginSQL Alias cAliasSRA
		SELECT SRA.RA_MAT FROM %table:SRA% SRA
			WHERE SRA.RA_FILIAL = %xFilial:SRA%
			AND SRA.RA_SITFOLH <> 'D'
			AND SRA.RA_DEMISSA = %exp:SToD(Space(8))%
			AND SRA.%notDel%
	EndSQL

	dbSelectArea( cAliasSRA )

	While ( cAliasSRA )->( !EoF() )
		aAdd( aFunc , { ( cAliasSRA )->RA_MAT } )
		( cAliasSRA )->( dbSkip() )
	End

	( cAliasSRA )->( dbCloseArea() )

Return aFunc

//---------------------------------------------------------------------
/*/{Protheus.doc} MdtVldRis
Valida se o Risco ser� enviado para o TAF

@return aFunc

@sample MdtVldRis()

@param [dDtRet], Date, Identifica a data de refer�ncia para busca do PPRA

@author Luis Fellipy Bett
@since 09/10/2018
/*/
//---------------------------------------------------------------------
Function MdtVldRis( dDtRef )

	Local lRet     := .T.
	Local cRisTAF  := SuperGetMv( "MV_NG2RIST" , .F. , "3" )
	Local lVldPPRA := SuperGetMv( "MV_NG2VLAU" , .F. , "2" ) == "1"

	Default dDtRef := dDataBase

	// Valida se busca os riscos n�o obrigat�rio
	// MV_NG2RIST - 0 - Nenhum
	// MV_NG2RIST - 1 - Somente Ergonomicos
	// MV_NG2RIST - 2 - Somente Acidentes\Mec�nicos
	// MV_NG2RIST - 3 - Somente Ergonomicos\Acidentes\Mec�nicos
	// MV_NG2RIST - 4 - Somente Perigosos
	// MV_NG2RIST - 5 - Todos
	dbSelectarea( "TMA" )
	dbSetOrder( 1 )
	dbSeek( xFilial( "TMA" ) + TN0->TN0_AGENTE )

	If ( TMA->TMA_GRISCO == "4" .And. ( cRisTAF == "2" .Or. cRisTAF == "0" .Or. cRisTAF == "4" ) ) .Or. ;
		( ( TMA->TMA_GRISCO == "5" .Or. TMA->TMA_GRISCO == "6" ) .And. ( cRisTAF == "1" .Or. cRisTAF == "0" .Or. cRisTAF == "4" ) ) .Or. ;
		( TMA->TMA_GRISCO == "7" .And. ( cRisTAF == "0" .Or. cRisTAF == "1" .Or. cRisTAF == "2" .Or. cRisTAF == "3" ) )
		lRet := .F.
	EndIf

	// Valida se o risco est� em algum PPRA no momento da execu��o
	If lVldPPRA .And. lRet //Caso seja avaliar o Laudo
		lRet := .F. //Indica inicialmente que o Risco n�o ser� setado para envio, caso ache ent�o envia
		dbSelectArea( "TO1" )
		dbSetOrder( 2 ) //TO1_FILIAL+TO1_NUMRIS+TO1_LAUDO
		dbSeek( xFilial( "TO1" ) + TN0->TN0_NUMRIS )
		While TO1->( !EoF() ) .And. TO1->TO1_FILIAL == xFilial( "TO1" ) .And. ;
				TO1->TO1_NUMRIS == TN0->TN0_NUMRIS

			dbSelectArea( "TO0" )
			dbSetOrder( 1 ) //TO0_FILIAL+TO0_LAUDO
			dbSeek( xFilial( "TO0" ) + TO1->TO1_LAUDO )
			If TO0->TO0_TIPREL == "1" .And. TO0->TO0_DTINIC <= dDtRef .And.;
				( TO0->TO0_DTVALI >= dDtRef .Or. Empty( TO0->TO0_DTVALI ) )

				lRet := .T.
				Exit

			EndIf

			TO1->( dbSkip() )

		End
	EndIf

Return lRet

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTEpiTAF
Analisa os Requesitos do EPI eficazes:

@param cRiscoFun	Caracter Risco no qual est� exposto
@param cMat			Caracter Matr�cula do Funcion�rio
@param aCAEPI		Array Dados do Epi

Integra��o das condi��es diferenciais de trabalho, Evento S-2360

@author Guilherme Benkendorf
@since 28/02/2014
/*/
//---------------------------------------------------------------------
Function MDTEpiTAF( cRiscoFun , cMat , aCAEPI )

	// Contadores
	Local nFor1, nFor2, nEpi, nOrdEPi

	// Variaveis de Tamanho de Campo
	Local nSizeCod := If( ( TAMSX3( "B1_COD" )[ 1 ] ) < 1 , 15 , ( TAMSX3( "B1_COD" )[ 1 ] ) )

	//Variaveis do TRB
	Local cAliTRB := GetNextAlias()
	Local aDBF := {}

	Local aAreaEPI := GetArea()

	// Controladores
	Local nPosi, nDiasUtilizados, nQtdeAfast
	Local nPosCAP
	Local cNumCAP

	Local dInicioRis, dFimRis, dtAval, dDtEntr
	Local dDtEficaz, dDtEfica2, dDtTNFfi2

	Local lFirst
	Local lStart, lEpiSub, lEpiAltOK
	Local lEpiEntregue := .F.
	Local lEPCEfic := .F.
	Local lEPCprot := .F.

	// Par�metros
	Local nDias := SuperGetMv("MV_PPPDTRI",.F.,"")
	Local nLimite_Dias_Epi := 30

	Local lConsEPC := SuperGetMv("MV_NG2CEPC",.F.,"N") == "S"

	Local lEpiCompl:= SuperGetMv("MV_NG2KEPI",.F.,"N") == "S"

	Local lConsEPI := SuperGetMv("MV_NG2CEPI",.F.,"N") == "S"

	Local lEPIRis	:= SuperGetMv( "MV_NG2EPIR" , .F. , "1" ) == "2"

	// Modos de Compartilhamento
	Local cModoTN0,cXFilTN0,cModoTNX,cXFilTNX,cModoTNF,cXFilTNF,cModoTN3,cXFilTN3
	Local cFilFun := xFilial( "SRA" )

	// Controle de EPI's Obrigat�rios
	Local aTNFobr, aTNFfam, aTNFalt
	Local aOrdEPIs := {}

	Local cPROTEC
	Local cMedPrt := ""
	Local cCndFun
	Local cUsuIni
	Local cPrzVld
	Local cPerTrc
	Local cHigien

	// Verifica quantidade de dias
	If ValType( nDias ) == "N"
		nLimite_Dias_Epi := nDias
	ElseIf ValType( nDias ) == "C"
		nLimite_Dias_Epi := Val( nDias )
	Endif

	// Monta o TRB
	aAdd( aDBF , { "DTINI"  , "D" , 08			, 0 } )
	aAdd( aDBF , { "NUMCAP" , "C" , 12			, 0 } )
	aAdd( aDBF , { "PROTEC" , "C" , 02			, 0 } )
	aAdd( aDBF , { "DTREAL" , "D" , 08			, 0 } )
	aAdd( aDBF , { "SITUAC" , "C" , 01			, 0 } )
	aAdd( aDBF , { "DTDEVO" , "D" , 08			, 0 } )
	aAdd( aDBF , { "CODEPI" , "C" , nSizeCod	, 0 } )

	oTempAli := FWTemporaryTable():New( cAliTRB, aDBF )
	oTempAli:AddIndex( "1", {"DTINI","NUMCAP","PROTEC","DTREAL"}  )
	oTempAli:AddIndex( "2", {"SITUAC","NUMCAP","DTINI","PROTEC","DTREAL"} )
	oTempAli:Create()

	// Defini��es de compartilhamento do Risco
	Dbselectarea("TN0")
	Dbsetorder(1)
	Dbseek( xFilial( "TN0" ) + cRiscoFun )

	cModoTN0 := NGSEEKDIC( "SX2" , "TN0" , 1 , "X2_MODOEMP+X2_MODOUN+X2_MODO" )
	cXFilTN0 := FwxFilial( "TN0" , cFilFun , Substr( cModoTN0 , 1 , 1 ) , Substr( cModoTN0 , 2 , 1 ) , Substr( cModoTN0 , 3 , 1 ) )

	cModoTNX := NGSEEKDIC( "SX2" , "TNX" , 1 , "X2_MODOEMP+X2_MODOUN+X2_MODO" )
	cXFilTNX := FwxFilial( "TNX" , cFilFun , Substr( cModoTNX , 1 , 1 ) , Substr( cModoTNX , 2 , 1 ) , Substr( cModoTNX , 3 , 1 ) )

	cModoTNF := NGSEEKDIC( "SX2" , "TNF" , 1 , "X2_MODOEMP+X2_MODOUN+X2_MODO" )
	cXFilTNF := FwxFilial( "TNF" , cFilFun , Substr( cModoTNF , 1 , 1 ) , Substr( cModoTNF , 2 , 1 ) , Substr( cModoTNF , 3 , 1 ) )

	cModoTN3 := NGSEEKDIC( "SX2" , "TN3" , 1 , "X2_MODOEMP+X2_MODOUN+X2_MODO" )
	cXFilTN3 := FwxFilial( "TN3" , cFilFun , Substr( cModoTN3 , 1 , 1 ) , Substr( cModoTN3 , 2 , 1 ) , Substr( cModoTN3 , 3 , 1 ) )

	cModoTL0 := NGSEEKDIC( "SX2" , "TL0" , 1 , "X2_MODOEMP+X2_MODOUN+X2_MODO" )
	cXFilTL0 := FwxFilial( "TL0" , cFilFun , Substr( cModoTL0 , 1 , 1 ) , Substr( cModoTL0 , 2 , 1 ) , Substr( cModoTL0 , 3 , 1 ) )

	lStart     := .F.
	dInicioRis := TN0->TN0_DTRECO
	dFimRis    := dDataBase

	dtAval := TN0->TN0_DTRECO

	If dtAval >= dInicioRis  .And. dtAval <= dFimRis
		lStart  := .T.

		If !Empty(TN0->TN0_DTELIM) .And. TN0->TN0_DTELIM < dFimRis
			dFimRis := TN0->TN0_DTELIM
		EndIf

		dInicioRis := dtAval
	ElseIf dtAval < dInicioRis .And. ( Empty(TN0->TN0_DTELIM) .Or. TN0->TN0_DTELIM >= dInicioRis )
		lStart  := .T.

		If !Empty(TN0->TN0_DTELIM) .And. TN0->TN0_DTELIM < dFimRis
			dFimRis := TN0->TN0_DTELIM
		EndIf

	EndIf

	If lStart .And. lConsEPI // Se consiste EPI

		// Apaga todos os registros do arquivo temporario onde estao os EPI's entregues
		aTNFobr := {}
		aTNFalt := {}
		aTNFfam := {}

		lFirst     := .T.

		dbSelectArea("TNX")
		dbSetOrder( 1 ) // TNX_FILIAL+TNX_NUMRIS+TNX_EPI
		dbSeek( cXFilTNX + TN0->TN0_NUMRIS )

		While TNX->( !Eof() ) .And. cXFilTNX == TNX->TNX_FILIAL .And. TN0->TN0_NUMRIS == TNX->TNX_NUMRIS

			If TNX->TNX_TIPO == "1"
				aAdd( aTNFobr, TNX->TNX_EPI )
			Else

				If (nPosi := aSCAN( aTNFfam, { |x| x == TNX->TNX_FAMIL } ) ) > 0
					aAdd( aTNFalt[ nPosi ], TNX->TNX_EPI )
				Else
					aAdd( aTNFfam, TNX->TNX_FAMIL )
					aAdd( aTNFalt, { TNX->TNX_EPI } )
				EndIf

			EndIf

			dbSelectArea("TNX")
			dbSkip()
		End

		// Epi esta previsto p/ funcionario
		lEpiObr   := .T. // Verifica se houve utilizacao de todos os EPIs necessarios
		dDtEficaz := STOD( Space( 8 ) ) // Data inicio Eficaz dos EPIs

		If Len( aTNFobr ) > 0 .Or. Len( aTNFalt ) > 0
			cCndFun := "S"
			cUsuIni := "S"
			cPrzVld := "S"
			cPerTrc := "S"
			cHigien := "S"
		Else
			cCndFun := "N"
			cUsuIni := "N"
			cPrzVld := "N"
			cPerTrc := "N"
			cHigien := "N"
		EndIf

		For nFor1 := 1 To Len( aTNFobr )
			cCndFun := "S"
			cUsuIni := "S"
			cPrzVld := "S"
			cPerTrc := "S"
			cHigien := "S"
			aAdd( aOrdEPIs , {} )
			nPosAdd := Len( aOrdEPIs )
			dbSelectArea( "TN3" )
			dbSetOrder( 2 )
			dbSeek( xFilial( "TN3" ) + aTNFobr[nFor1] )

			While TN3->( !EoF() ) .And. TN3->TN3_FILIAL == xFilial( "TN3" ) .And. TN3->TN3_CODEPI == aTNFobr[nFor1]

				If TN3->TN3_GENERI == "2"
					//Procurar os filhos e adicionar no Array
					dbSelectArea( "TL0" )
					dbSetOrder( 1 ) //TL0_FILIAL+TL0_EPIGEN

					If dbSeek( xFilial( "TL0" ) + TN3->TN3_CODEPI )
						aAdd( aOrdEPIs[ nPosAdd ] , TL0->TL0_NUMCAP )
					EndIf

				Else
					aAdd( aOrdEPIs[ nPosAdd ] , TN3->TN3_NUMCAP )
				EndIf

				TN3->( dbSkip() )
			End

			dDtEfica2 := dFimRis
			lEpiSub := .F.
			dbSelectArea("TNF")
			dbSetOrder(3)  //TNF_FILIAL+TNF_MAT+TNF_CODEPI+DTOS(TNF_DTENTR)+TNF_HRENTR

			If Dbseek( cXFilTNF + cMat + aTNFobr[ nFor1 ] )

				While TNF->( !Eof() ) .And. cXFilTNF + cMat + aTNFobr[nFor1] == TNF->( TNF_FILIAL + TNF_MAT + TNF_CODEPI )

					cNumCAP := Space(12)

					If TNF->TNF_INDDEV == "3" .Or. TNF->TNF_DTENTR > dFimRis
						Dbselectarea("TNF")
						dbSkip()
						Loop
					EndIf

					If TNF->TNF_DTDEVO < dInicioRis .And. !Empty(TNF->TNF_DTDEVO)
						Dbselectarea("TNF")
						dbSkip()
						Loop
					EndIf

					cNumCAP := TNF->TNF_NUMCAP
					lEpiEntregue := .T.

					dbSelectArea("TN3")
					dbSetOrder(1)
					dbSeek( cXFilTN3 + TNF->TNF_FORNEC + TNF->TNF_LOJA + TNF->TNF_CODEPI + TNF->TNF_NUMCAP )

					lEpiSub   := .T.
					lFirst    := .F.
					dDtEfica2 := If( TNF->TNF_DTENTR > dDtEfica2 , dDtEfica2 , TNF->TNF_DTENTR )
					dDtEntr   := If( TNF->TNF_DTENTR < dInicioRis, dInicioRis , TNF->TNF_DTENTR )
					cPROTEC	 := If( TNF->TNF_EPIEFI == "2" , "N" , If( TNF->TNF_EPIEFI == "3" , "N" , "S" ) )

					If ( nPosCAP := aScan( aCAEPI , { | x | x[ 1 ] == TNF->TNF_NUMCAP } ) ) == 0
						aAdd( aCAEPI, { TNF->TNF_NUMCAP ,;
										AllTrim( Posicione( "SB1" , 1 , xFilial("SB1") + TNF->TNF_CODEPI , "B1_DESC" ) ) ,;
										cPROTEC ,;
										cMedPrt ,;
										If( !lEPIRis , cCndFun , If( TN0->TN0_CONFUN == "1" , "S" , "N" ) ) ,;
										If( !lEPIRis , cUsuIni , If( TN0->TN0_CONFUN == "1" , "S" , "N" ) ) ,;
										If( !lEPIRis , cPrzVld , If( TN0->TN0_PRZVLD == "1" , "S" , "N" ) ) ,;
										If( !lEPIRis , cPerTrc , If( TN0->TN0_PERTRC == "1" , "S" , "N" ) ) ,;
										If( !lEPIRis , cHigien , If( TN0->TN0_HIGIEN == "1" , "S" , "N" ) ) } )
						nPosCAP := Len( aCAEPI )
					EndIf

					dDtDevo := dFimRis

					If !Empty( TNF->TNF_DTDEVO ) .And. TNF->TNF_DTDEVO >= dInicioRis .And. TNF->TNF_DTDEVO < dFimRis
						dDtDevo := TNF->TNF_DTDEVO
					ElseIf Empty( TNF->TNF_DTDEVO )
						dbSelectArea("TNF")  //TNF_FILIAL+TNF_MAT+TNF_CODEPI+DTOS(TNF_DTENTR)+TNF_HRENTR
						dbSkip()

						If cXFilTNF + cMat + aTNFobr[ nFor1 ] == TNF->( TNF_FILIAL + TNF_MAT + TNF_CODEPI ) .And. TNF->( !Eof() ) .And. TNF->TNF_INDDEV != "3"

							If dDtEntr < TNF->TNF_DTENTR
								If ( TNF->TNF_DTENTR - 1 ) >= dInicioRis .And. ( TNF->TNF_DTENTR - 1 ) < dFimRis
									dDtDevo := TNF->TNF_DTENTR - 1
								Endif

							ElseIf dDtEntr == TNF->TNF_DTENTR

								If TNF->TNF_DTENTR >= dInicioRis .And. TNF->TNF_DTENTR < dFimRis
									dDtDevo := TNF->TNF_DTENTR
								Endif

							Endif

						Endif

						dbSkip(-1)
					Endif

					// Grava no TRB
					dbSelectArea( cAliTRB )
					dbSetOrder( 1 )

					If !Dbseek( DTOS( dDtEntr ) + cNumCAP + cPROTEC + DTOS( TNF->TNF_DTENTR ) )
						RecLock( cAliTRB , .T. )
						( cAliTRB )->DTINI  := dDtEntr
						( cAliTRB )->NUMCAP := cNumCAP
						( cAliTRB )->PROTEC := cPROTEC
						( cAliTRB )->DTREAL := TNF->TNF_DTENTR
						( cAliTRB )->DTDEVO := dDtDevo
						( cAliTRB )->CODEPI := TNF->TNF_CODEPI
						Msunlock( cAliTRB )
					Else

						If ( cAliTRB )->CODEPI == TNF->TNF_CODEPI .And. dDtDevo > ( cAliTRB )->DTDEVO
							RecLock( cAliTRB , .F. )
							( cAliTRB )->DTDEVO := dDtDevo
							Msunlock( cAliTRB )
						Endif

					Endif

					// Verifica se pelo menos um EPI foi entregue fora do prazo de validade
					If !( TNF->TNF_DTENTR <= TN3->TN3_DTVENC .And. dDtDevo <= TN3->TN3_DTVENC )
						cPrzVld :=  "N"
					Endif

					// Verifica se funcionario ficou afastado
					nQtdeAfast := 0

					If NGCADICBASE( "TN3_TPDURA" , "A" , "TN3" , .F. )

						If TN3->TN3_TPDURA == "U"
							nQtdeAfast := fQtdeAfast( cFilFun , cMat , TNF->TNF_DTENTR , dDtDevo )
						Endif

					Else
						nQtdeAfast := fQtdeAfast( cFilFun , cMat , TNF->TNF_DTENTR , dDtDevo )
					Endif

					// Verifica se pelo menos um EPI foi utilizado mais do que o seu prazo de durabilidade
					nDiasUtilizados := (dDtDevo - TNF->TNF_DTENTR) - nQtdeAfast

					If nDiasUtilizados > TN3->TN3_DURABI
						cPerTrc := "N"
					EndIf

					// Verifica se pelo menos um EPI nao teve higieniza��o
					If TNF->(FieldPos("TNF_DTMANU")) > 0 .And. TN3->(FieldPos("TN3_PERMAN")) > 0

						If TN3->TN3_PERMAN > 0 .And. Empty(TNF->TNF_DTMANU)
							cHigien := "N"
						EndIf

					EndIf

					// Ajusta Informa��es do C.A.
					If cPrzVld == "N"
						aCAEPI[ nPosCAP , 5 ] := cPrzVld
					EndIf

					If cPerTrc == "N"
						aCAEPI[ nPosCAP , 6 ] := cPerTrc
					EndIf

					If cHigien == "N"
						aCAEPI[ nPosCAP , 7 ] := cHigien
					EndIf

					dbSelectArea("TNF")
					dbSkip()
				End

			Else
				dbSelectArea("TN3")
				dbSetOrder( 2 ) //TN3_FILIAL+TN3_CODEPI
				dbSeek( cXFilTN3 + aTNFobr[ nFor1 ] )

				While TN3->( !Eof() ) .And. TN3->TN3_CODEPI == aTNFobr[ nFor1 ]

					If TN3->TN3_GENERI == "2"
						dbSelectArea("TL0")
						dbSetOrder( 1 ) //TL0_FILIAL+TL0_EPIGEN+TL0_FORNEC+TL0_LOJA+TL0_EPIFIL
						dbSeek( cXFilTL0 + aTNFobr[ nFor1 ] )

						While TL0->(!Eof()) .And. TL0->TL0_EPIGEN == aTNFobr[ nFor1 ]
							dbSelectArea("TNF")
							dbSetOrder( 3 ) //TNF_FILIAL+TNF_MAT+TNF_CODEPI+DTOS(TNF_DTENTR)+TNF_HRENTR

							If Dbseek( cXFilTNF + cMat + TL0->TL0_EPIFIL)

								While TNF->(!Eof()) .And. cXFilTNF + cMat + TL0->TL0_EPIFIL == TNF->( TNF_FILIAL + TNF_MAT + TNF_CODEPI )

									cNumCAP := Space(12)

									If TNF->TNF_INDDEV == "3" .Or. TNF->TNF_DTENTR > dFimRis
										dbSelectArea("TNF")
										dbSkip()
										Loop
									EndIf

									If TNF->TNF_DTDEVO < dInicioRis .And. !Empty(TNF->TNF_DTDEVO)
										dbSelectArea("TNF")
										dbSkip()
										Loop
									EndIf

									cNumCAP := TNF->TNF_NUMCAP
									lEpiEntregue := .T.

									lEpiSub   := .T.
									lFirst    := .F.
									dDtEfica2 := If( TNF->TNF_DTENTR > dDtEfica2 , dDtEfica2 , TNF->TNF_DTENTR )
									dDtEntr   := If( TNF->TNF_DTENTR < dInicioRis, dInicioRis , TNF->TNF_DTENTR )
									cPROTEC	  := If( TNF->TNF_EPIEFI == "2" , "N" , If( TNF->TNF_EPIEFI == "3" , "N" , "S" ) )

									If ( nPosCAP := aScan( aCAEPI , { | x | x[ 1 ] == TNF->TNF_NUMCAP } ) ) == 0
										aAdd( aCAEPI, { TNF->TNF_NUMCAP ,;
														AllTrim( Posicione( "SB1" , 1 , xFilial("SB1") + TNF->TNF_CODEPI , "B1_DESC" ) ) ,;
														cPROTEC ,;
														cMedPrt ,;
														If( !lEPIRis , cCndFun , If( TN0->TN0_CONFUN == "1" , "S" , "N" ) ) ,;
														If( !lEPIRis , cUsuIni , If( TN0->TN0_CONFUN == "1" , "S" , "N" ) ) ,;
														If( !lEPIRis , cPrzVld , If( TN0->TN0_PRZVLD == "1" , "S" , "N" ) ) ,;
														If( !lEPIRis , cPerTrc , If( TN0->TN0_PERTRC == "1" , "S" , "N" ) ) ,;
														If( !lEPIRis , cHigien , If( TN0->TN0_HIGIEN == "1" , "S" , "N" ) ) } )
										nPosCAP := Len( aCAEPI )
									EndIf

									dDtDevo := dFimRis

									If !Empty( TNF->TNF_DTDEVO ) .And. TNF->TNF_DTDEVO >= dInicioRis .And. TNF->TNF_DTDEVO < dFimRis
										dDtDevo := TNF->TNF_DTDEVO
									ElseIf Empty( TNF->TNF_DTDEVO )
										dbSelectArea("TNF")
										dbSkip()

										If cXFilTNF + cMat + TL0->TL0_EPIFIL == TNF->( TNF_FILIAL + TNF_MAT + TNF_CODEPI );
												.And. TNF->( !Eof() ) .And. TNF->TNF_INDDEV != "3"

											If dDtEntr < TNF->TNF_DTENTR

												If ( TNF->TNF_DTENTR - 1 ) >= dInicioRis .And. ( TNF->TNF_DTENTR - 1 ) < dFimRis
													dDtDevo := TNF->TNF_DTENTR - 1
												EndIf

											ElseIf dDtEntr == TNF->TNF_DTENTR

												If TNF->TNF_DTENTR >= dInicioRis .And. TNF->TNF_DTENTR < dFimRis
													dDtDevo := TNF->TNF_DTENTR
												EndIf

											EndIf

										EndIf

										dbSkip(-1)
									EndIf

									// Grava no TRB
									dbSelectArea( cAliTRB )
									dbSetOrder( 1 )

									If !dbSeek( DTOS( dDtEntr ) + cNumCAP + cPROTEC + DTOS( TNF->TNF_DTENTR ) )
										RecLock( cAliTRB , .T. )
										( cAliTRB )->DTINI  := dDtEntr
										( cAliTRB )->NUMCAP := cNumCAP
										( cAliTRB )->PROTEC := cPROTEC
										( cAliTRB )->DTREAL := TNF->TNF_DTENTR
										( cAliTRB )->DTDEVO := dDtDevo
										( cAliTRB )->CODEPI := TNF->TNF_CODEPI
										Msunlock( cAliTRB )
									Else

										If ( cAliTRB )->CODEPI == TNF->TNF_CODEPI .And. dDtDevo > ( cAliTRB )->DTDEVO
											RecLock( cAliTRB ,.F.)
											( cAliTRB )->DTDEVO := dDtDevo
											Msunlock( cAliTRB )
										Endif

									Endif

									// Verifica se pelo menos um EPI foi entregue fora do prazo de validade
									If !( TNF->TNF_DTENTR <= TN3->TN3_DTVENC .And. dDtDevo <= TN3->TN3_DTVENC )
										cPrzVld := "N"
									EndIf

									// Verifica se funcionario ficou afastado
									nQtdeAfast := 0

									If NGCADICBASE( "TN3_TPDURA", "A", "TN3", .F. )

										If NGSEEK( "TN3", TL0->TL0_FORNEC+TL0->TL0_LOJA+TL0->TL0_EPIGEN, 1, "TN3->TN3_TPDURA" ) == "U"
											nQtdeAfast := fQtdeAfast( cFilFun , cMat , TNF->TNF_DTENTR , dDtDevo )
										Endif

									Else
										nQtdeAfast := fQtdeAfast( cFilFun , cMat, TNF->TNF_DTENTR , dDtDevo )
									Endif

									// Verifica se pelo menos um EPI foi utilizado mais do que o seu prazo de durabilidade
									nDiasUtilizados := ( dDtDevo - TNF->TNF_DTENTR ) - nQtdeAfast

									If nDiasUtilizados > TN3->TN3_DURABI
										cPerTrc := "2"
									EndIf

									// Verifica se pelo menos um EPI nao teve higieniza��o
									If TNF->(FieldPos("TNF_DTMANU")) > 0 .And. TN3->(FieldPos("TN3_PERMAN")) > 0

										If TN3->TN3_PERMAN > 0 .And. Empty(TNF->TNF_DTMANU)
											cHigien := "N"
										EndIf

									Endif

									// Ajusta Informa��es do C.A.
									If cPrzVld == "N"
										aCAEPI[ nPosCAP , 5 ] := cPrzVld
									EndIf

									If cPerTrc == "N"
										aCAEPI[ nPosCAP , 6 ] := cPerTrc
									EndIf

									If cHigien == "N"
										aCAEPI[ nPosCAP , 7 ] := cHigien
									EndIf

									dbSelectArea("TNF")
									dbSkip()
								End

							EndIf

							dbSelectArea("TL0")
							TL0->(dbSkip())
						End

					EndIf

					dbSelectArea("TN3")
					TN3->(dbSkip())
				End

			EndIf

			If !lEpiSub
				lEpiObr := .F.
			EndIf

			If dDtEfica2 > dDtEficaz
				dDtEficaz := dDtEfica2
			Endif

		Next nFor1

		For nFor1 := 1 To Len(aTNFalt)
			lEpiAltOK := .F.
			dDtEfica2 := dFimRis
			aAdd( aOrdEPIs , {} )
			nPosAdd := Len( aOrdEPIs )

			For nFor2 := 1 To Len( aTNFalt[nFor1] )
				dbSelectArea( "TN3" )
				dbSetOrder( 2 )
				dbSeek( xFilial( "TN3" ) + aTNFalt[nFor1,nFor2] )

				While TN3->( !EoF() ) .And. TN3->TN3_FILIAL == xFilial( "TN3" ) .And. TN3->TN3_CODEPI == aTNFalt[nFor1,nFor2]

					If TN3->TN3_GENERI == "2"
						//Procurar os filhos e adicionar no Array
						dbSelectArea( "TL0" )
						dbSetOrder( 1 ) // TL0_FILIAL + TL0_EPIGEN

						If dbSeek( xFilial( "TL0" ) + TN3->TN3_CODEPI )
							aAdd( aOrdEPIs[ nPosAdd ] , TL0->TL0_NUMCAP )
						Endif

					Else
						aAdd( aOrdEPIs[ nPosAdd ] , TN3->TN3_NUMCAP )
					EndIf

					TN3->( dbSkip() )
				End

				dbSelectArea("TNF")
				dbSetOrder(3)  //TNF_FILIAL+TNF_MAT+TNF_CODEPI+DTOS(TNF_DTENTR)+TNF_HRENTR

				If Dbseek( cXFilTNF + cMat + aTNFalt[nFor1,nFor2] )

					While TNF->( !Eof() ) .And. cXFilTNF + cMat + aTNFalt[nFor1,nFor2] == TNF->( TNF_FILIAL + TNF_MAT + TNF_CODEPI )

						cNumCAP := Space(12)

						If TNF->TNF_INDDEV == "3" .Or. TNF->TNF_DTENTR > dFimRis
							Dbselectarea("TNF")
							Dbskip()
							Loop
						EndIf

						If TNF->TNF_DTDEVO < dInicioRis .And. !Empty(TNF->TNF_DTDEVO)
							Dbselectarea("TNF")
							Dbskip()
							Loop
						EndIf

						cNumCAP := TNF->TNF_NUMCAP
						lEpiEntregue := .T.

						dbSelectArea("TN3")
						dbSetOrder( 1 ) //TN3_FILIAL+TN3_FORNEC+TN3_LOJA+TN3_CODEPI+TN3_NUMCAP
						dbSeek( xFilial("TN3",cFilFun) + TNF->TNF_FORNEC + TNF->TNF_LOJA + TNF->TNF_CODEPI + TNF->TNF_NUMCAP )

						lEpiAltOK := .T.
						lFirst    := .F.
						dDtEfica2 := If( TNF->TNF_DTENTR > dDtEfica2 , dDtEfica2 , TNF->TNF_DTENTR )
						dDtEntr   := If( TNF->TNF_DTENTR < dInicioRis , dInicioRis , TNF->TNF_DTENTR )
						cPROTEC	  := If( TNF->TNF_EPIEFI == "2" , "N" , If( TNF->TNF_EPIEFI == "3" , "N" , "S" ) )

						If ( nPosCAP := aScan( aCAEPI , { | x | x[ 1 ] == TNF->TNF_NUMCAP } ) ) == 0
							aAdd( aCAEPI, { TNF->TNF_NUMCAP ,;
											AllTrim( Posicione( "SB1" , 1 , xFilial("SB1") + TNF->TNF_CODEPI , "B1_DESC" ) ) ,;
											cPROTEC ,;
											cMedPrt ,;
											If( !lEPIRis , cCndFun , If( TN0->TN0_CONFUN == "1" , "S" , "N" ) ) ,;
											If( !lEPIRis , cUsuIni , If( TN0->TN0_CONFUN == "1" , "S" , "N" ) ) ,;
											If( !lEPIRis , cPrzVld , If( TN0->TN0_PRZVLD == "1" , "S" , "N" ) ) ,;
											If( !lEPIRis , cPerTrc , If( TN0->TN0_PERTRC == "1" , "S" , "N" ) ) ,;
											If( !lEPIRis , cHigien , If( TN0->TN0_HIGIEN == "1" , "S" , "N" ) ) } )
							nPosCAP := Len( aCAEPI )
						EndIf

						dDtDevo := dFimRis

						If !Empty( TNF->TNF_DTDEVO ) .And. TNF->TNF_DTDEVO >= dInicioRis .And. TNF->TNF_DTDEVO < dFimRis
							dDtDevo := TNF->TNF_DTDEVO
						ElseIf Empty( TNF->TNF_DTDEVO )
							dbSelectArea("TNF")
							dbSkip()

							If cXFilTNF + cMat + aTNFalt[ nFor1 , nFor2 ] == TNF->( TNF_FILIAL + TNF_MAT + TNF_CODEPI );
									.And. TNF->( Eof() ) .And. TNF->TNF_INDDEV != "3"

								If dDtEntr < TNF->TNF_DTENTR

									If ( TNF->TNF_DTENTR - 1 ) >= dInicioRis .And. ( TNF->TNF_DTENTR - 1 ) < dFimRis
										dDtDevo := TNF->TNF_DTENTR - 1
									Endif

								ElseIf dDtEntr == TNF->TNF_DTENTR

									If TNF->TNF_DTENTR >= dInicioRis .And. TNF->TNF_DTENTR < dFimRis
										dDtDevo := TNF->TNF_DTENTR
									Endif

								Endif

							Endif

							dbSkip(-1)
						Endif

						// Grava no TRB
						dbSelectArea( cAliTRB )
						dbSetOrder( 1 )

						If !dbSeek( DTOS( dDtEntr ) + cNumCAP + cPROTEC + DTOS( TNF->TNF_DTENTR ) )
							RecLock( cAliTRB , .T. )
							( cAliTRB )->DTINI  := dDtEntr
							( cAliTRB )->NUMCAP := cNumCAP
							( cAliTRB )->PROTEC := cPROTEC
							( cAliTRB )->DTREAL := TNF->TNF_DTENTR
							( cAliTRB )->DTDEVO := dDtDevo
							( cAliTRB )->CODEPI := TNF->TNF_CODEPI
							Msunlock( cAliTRB )
						Else

							If ( cAliTRB )->CODEPI == TNF->TNF_CODEPI .And. dDtDevo > ( cAliTRB )->DTDEVO
								RecLock( cAliTRB ,.F.)
								( cAliTRB )->DTDEVO := dDtDevo
								Msunlock( cAliTRB )
							Endif

						Endif

						// Verifica se pelo menos um EPI foi entregue fora do prazo de validade
						If !( TNF->TNF_DTENTR <= TN3->TN3_DTVENC .And. dDtDevo <= TN3->TN3_DTVENC )
							cPrzVld := "N"
						EndIf

						// Verifica se funcionario ficou afastado
						nQtdeAfast := 0

						If NGCADICBASE("TN3_TPDURA","A","TN3",.F.)

							If TN3->TN3_TPDURA == "U"
								nQtdeAfast := fQtdeAfast( cFilFun , cMat , TNF->TNF_DTENTR , dDtDevo )
							Endif

						Else
							nQtdeAfast := fQtdeAfast( cFilFun , cMat , TNF->TNF_DTENTR , dDtDevo )
						Endif

						// Verifica se pelo menos um EPI foi utilizado mais do que o seu prazo de durabilidade
						nDiasUtilizados := ( dDtDevo - TNF->TNF_DTENTR ) - nQtdeAfast

						If nDiasUtilizados > TN3->TN3_DURABI
							cPerTrc := "N"
						EndIf

						// Verifica se pelo menos um EPI nao teve higieniza��o
						If TNF->(FieldPos("TNF_DTMANU")) > 0 .and. TN3->(FieldPos("TN3_PERMAN")) > 0

							If TN3->TN3_PERMAN > 0 .And. Empty(TNF->TNF_DTMANU)
								cHigien := "N"
							EndIf

						EndIf

						//Ajusta Informa��es do C.A.
						If cPrzVld == "N"
							aCAEPI[ nPosCAP , 5 ] := cPrzVld
						EndIf

						If cPerTrc == "N"
							aCAEPI[ nPosCAP , 6 ] := cPerTrc
						EndIf

						If cHigien == "N"
							aCAEPI[ nPosCAP , 7 ] := cHigien
						EndIf

						dbSelectArea("TNF")
						dbSkip()
					End

				Else
					dbSelectArea("TN3")
					dbSetOrder( 2 ) //TN3_FILIAL+TN3_CODEPI
					dbSeek( cXFilTN3 + aTNFalt[ nFor1 , nFor2 ] )

					While TN3->(!Eof()) .And. TN3->TN3_CODEPI == aTNFalt[ nFor1 , nFor2 ]

						If TN3->TN3_GENERI == "2"
							dbSelectArea("TL0")
							dbSetOrder(1) //TL0_FILIAL+TL0_EPIGEN+TL0_FORNEC+TL0_LOJA+TL0_EPIFIL
							dbSeek( cXFilTL0 + aTNFalt[ nFor1 , nFor2 ] )

							While TL0->(!Eof()) .And. TL0->TL0_EPIGEN == aTNFalt[ nFor1 , nFor2 ]
								dbSelectArea("TNF")
								dbSetOrder(3) //TNF_FILIAL+TNF_MAT+TNF_CODEPI+DTOS(TNF_DTENTR)+TNF_HRENTR

								If Dbseek( cXFilTNF + cMat + TL0->TL0_EPIFIL )
									While TNF->(!Eof()) .And. cXFilTNF + cMat + TL0->TL0_EPIFIL == TNF->(TNF_FILIAL + TNF_MAT + TNF_CODEPI)

										cNumCAP := Space(12)

										If TNF->TNF_INDDEV == "3" .Or. TNF->TNF_DTENTR > dFimRis
											Dbselectarea("TNF")
											Dbskip()
											Loop
										Endif

										If TNF->TNF_DTDEVO < dInicioRis .And. !Empty(TNF->TNF_DTDEVO)
											Dbselectarea("TNF")
											Dbskip()
											Loop
										Endif

										cNumCAP := TNF->TNF_NUMCAP
										lEpiEntregue := .T.

										lEpiAltOK := .T.
										lFirst    := .F.
										dDtEfica2 := If( TNF->TNF_DTENTR > dDtEfica2 , dDtEfica2 , TNF->TNF_DTENTR )
										dDtEntr   := If( TNF->TNF_DTENTR < dInicioRis , dInicioRis , TNF->TNF_DTENTR )
										cPROTEC	  := If( TNF->TNF_EPIEFI == "2" , "N" , If( TNF->TNF_EPIEFI == "3" , "N" , "S" ) )

										If ( nPosCAP := aScan( aCAEPI , { | x | x[ 1 ] == TNF->TNF_NUMCAP } ) ) == 0
											aAdd( aCAEPI, { TNF->TNF_NUMCAP ,;
															AllTrim( Posicione( "SB1" , 1 , xFilial("SB1") + TNF->TNF_CODEPI , "B1_DESC" ) ) ,;
															cPROTEC ,;
															cMedPrt ,;
															If( !lEPIRis , cCndFun , If( TN0->TN0_CONFUN == "1" , "S" , "N" ) ) ,;
															If( !lEPIRis , cUsuIni , If( TN0->TN0_CONFUN == "1" , "S" , "N" ) ) ,;
															If( !lEPIRis , cPrzVld , If( TN0->TN0_PRZVLD == "1" , "S" , "N" ) ) ,;
															If( !lEPIRis , cPerTrc , If( TN0->TN0_PERTRC == "1" , "S" , "N" ) ) ,;
															If( !lEPIRis , cHigien , If( TN0->TN0_HIGIEN == "1" , "S" , "N" ) ) } )
											nPosCAP := Len( aCAEPI )
										EndIf

										dDtDevo := dFimRis

										If !Empty( TNF->TNF_DTDEVO ) .And. TNF->TNF_DTDEVO >= dInicioRis .And. TNF->TNF_DTDEVO < dFimRis
											dDtDevo := TNF->TNF_DTDEVO
										Elseif Empty( TNF->TNF_DTDEVO )
											dbSelectArea("TNF")
											dbSkip()

											If cXFilTNF + cMat + TL0->TL0_EPIFIL == TNF->( TNF_FILIAL + TNF_MAT + TNF_CODEPI );
													.And. TNF->( !Eof() ) .And. TNF->TNF_INDDEV != "3"

												If dDtEntr < TNF->TNF_DTENTR

													If ( TNF->TNF_DTENTR - 1 ) >= dInicioRis .And. ( TNF->TNF_DTENTR - 1 ) < dFimRis
														dDtDevo := TNF->TNF_DTENTR - 1
													Endif

												ElseIf dDtEntr == TNF->TNF_DTENTR

													If TNF->TNF_DTENTR >= dInicioRis .And. TNF->TNF_DTENTR < dFimRis
														dDtDevo := TNF->TNF_DTENTR
													Endif

												Endif

											Endif
											dbSkip(-1)
										Endif

										//Grava no TRB
										dbSelectArea( cAliTRB )
										dbSetOrder( 1 )

										If !Dbseek( DTOS( dDtEntr ) + cNumCAP + cPROTEC + DTOS( TNF->TNF_DTENTR ) )
											RecLock( cAliTRB ,.T.)
											( cAliTRB )->DTINI  := dDtEntr
											( cAliTRB )->NUMCAP := cNumCAP
											( cAliTRB )->PROTEC := cPROTEC
											( cAliTRB )->DTREAL := TNF->TNF_DTENTR
											( cAliTRB )->DTDEVO := dDtDevo
											( cAliTRB )->CODEPI := TNF->TNF_CODEPI
											Msunlock( cAliTRB )
										Else

											If ( cAliTRB )->CODEPI == TNF->TNF_CODEPI .And. dDtDevo > ( cAliTRB )->DTDEVO
												RecLock( cAliTRB, .F.)
												( cAliTRB )->DTDEVO := dDtDevo
												Msunlock( cAliTRB )
											Endif

										Endif

										// Verifica se pelo menos um EPI foi entregue fora do prazo de validade
										If !( TNF->TNF_DTENTR <= TN3->TN3_DTVENC .And. dDtDevo <= TN3->TN3_DTVENC )
											cPrzVld := "N"
										EndIf

										// Verifica se funcionario ficou afastado
										nQtdeAfast := 0

										If NGCADICBASE( "TN3_TPDURA", "A", "TN3", .F. )

											If NGSEEK( "TN3", TL0->TL0_FORNEC+TL0->TL0_LOJA+TL0->TL0_EPIGEN, 1, "TN3->TN3_TPDURA" ) == "U"
												nQtdeAfast := fQtdeAfast( cFilFun , cMat , TNF->TNF_DTENTR , dDtDevo )
											Endif

										Else
											nQtdeAfast := fQtdeAfast( cFilFun , cMat , TNF->TNF_DTENTR , dDtDevo )
										Endif

										// Verifica se pelo menos um EPI foi utilizado mais do que o seu prazo de durabilidade
										nDiasUtilizados := (dDtDevo - TNF->TNF_DTENTR) - nQtdeAfast

										If nDiasUtilizados > TN3->TN3_DURABI
											cPerTrc := "N"
										EndIf

										// Verifica se pelo menos um EPI nao teve higieniza��o
										If TNF->( FieldPos( "TNF_DTMANU" ) ) > 0 .And. TN3->( FieldPos( "TN3_PERMAN" ) ) > 0

											If TN3->TN3_PERMAN > 0 .And. Empty(TNF->TNF_DTMANU)
												cHigien := "N"
											EndIf

										Endif

										// Ajusta Informa��es do C.A.
										If cPrzVld == "N"
											aCAEPI[ nPosCAP , 5 ] := cPrzVld
										EndIf

										If cPerTrc == "N"
											aCAEPI[ nPosCAP , 6 ] := cPerTrc
										EndIf

										If cHigien == "N"
											aCAEPI[ nPosCAP , 7 ] := cHigien
										EndIf

										dbSelectArea("TNF")
										TNF->( dbSkip() )
									End

								EndIf

								dbSelectArea("TL0")
								TL0->( dbSkip() )
							End

						EndIf

						dbSelectArea("TN3")
						TN3->( dbSkip() )
					End

				EndIf

			Next nFor2

			If dDtEfica2 > dDtEficaz
				dDtEficaz := dDtEfica2
			Endif

			If !lEpiAltOK
				lEpiObr := .f.
			Endif

		Next nFor1

		For nOrdEPi := 1 To Len( aOrdEPIs ) // Verificar

			dDtEficaz := If( dInicioRis + nLimite_Dias_Epi >= dDtEficaz , dInicioRis , dDtEficaz )

			// Cria historico de epi's entregues
			If !lFirst  //Epi esta previsto p/ funcionario e foi entregue

				If dDtEficaz != dInicioRis
					cCndFun := "N"
					cUsuIni := "N"
				Endif

				If cCndFun == "S"
					fAjustaData( dInicioRis , cAliTRB )
					dbSelectArea( cAliTRB )
					dbSetOrder( 1 )

					// Filtrar o TRB conforme os EPIs do Array
					If dbSeek( xFilial( cAliTRB ) + aOrdEPIs[ nOrdEPi, 1 ] )
						dbGoTop()
						dDtTNFfi2 := ( cAliTRB )->DTDEVO

						While ( cAliTRB )->( !Eof() )
							dDtTNFini := ( cAliTRB )->DTINI

							If ( cAliTRB )->DTDEVO > dDtTNFfi2
								dDtTNFfi2 := ( cAliTRB )->DTDEVO
							Endif

							dbSkip()
							If !Eof()

								If ( cAliTRB )->DTINI > dDtTNFfi2 + nLimite_Dias_Epi .And. ( dDtTNFfi2 + 1 ) <= ( ( cAliTRB )->DTINI - 1 )
									cCndFun		:= "N"
									cUsuIni		:= "N"
									dDtTNFfi2	:= ( cAliTRB )->DTDEVO
									Exit
								Endif

							Else

								If dFimRis > dDtTNFfi2 + nLimite_Dias_Epi
									cCndFun := "N"
									cUsuIni := "N"
									Exit
								Endif

							Endif

						End

						If cCndFun <> "S"
							dbSelectArea( cAliTRB )
							dbSetOrder( 2 )
							dbSeek( " " )

							While ( cAliTRB )->( !Eof() ) .And. ( cAliTRB )->SITUAC == " "

								If lEpiObr

									If AllTrim( ( cAliTRB )->PROTEC ) == "N"
										cCndFun := "N"
										cUsuIni := "N"
										Exit
									Endif

								Else
									cCndFun := "N"
									cUsuIni := "N"
									Exit
								Endif

								dbSelectArea( cAliTRB )
								dbSkip()
							End

						EndIf

					EndIf

				EndIf

			EndIf // Fim - Tem EPI previsto

		Next nOrdEPi

	Endif //Fim - Consiste EPI

	// EPC Eficaz
	If lConsEPC
		cEPCefic := TN0->TN0_EPC

		If cEPCefic == "2"
			cEPCefic := "N"
			lEPCprot := .T.

			If lEpiCompl

				If TN0->(FieldPos("TN0_MEDCON")) > 0 .And. !AliasInDic( "TJF" )

					If NGSEEK( "TO4", TN0->TN0_MEDCON, 1, "TO4->TO4_TIPCTR" ) == "2"  //Tipo da Medida de Controle = "2" (Protecao Coletiva)
						lEPCefic := .T.
					Endif

				Endif

				If AliasInDic( "TJF" )
					dbSelectArea( "TJF" )
					dbSetOrder( 1 )
					dbSeek( xFilial( "TJF" ) + TN0->TN0_NUMRIS )

					While TJF->( !Eof() ) .And. TJF->TJF_FILIAL == xFilial( "TJF" ) .And. TJF->TJF_NUMRIS == TN0->TN0_NUMRIS

						If NGSEEK( "TO4" , TJF->TJF_MEDCON , 1 , "TO4_TIPCTR" ) == "2"
							lEPCefic := .T.
							Exit
						EndIf

						TJF->( dbSkip() )
					End

				EndIf

			EndIf

		Elseif cEPCefic == "1"
			cEPCefic := "S"
			lEPCprot := .T.

			If lEpiCompl .And. !lEPCefic //Indica que empresa utiliza EPI em carater complementar mesmo quando o EPC for eficaz

				If TN0->(FieldPos("TN0_MEDCON")) > 0 .And. !AliasInDic( "TJF" )

					If NGSEEK("TO4",TN0->TN0_MEDCON,1,"TO4->TO4_TIPCTR") == "2"  //Tipo da Medida de Controle = "2" (Protecao Coletiva)
						lEPCefic := .T.
					Endif

				Endif

				If AliasInDic( "TJF" )
					dbSelectArea( "TJF" )
					dbSetOrder( 1 )
					dbSeek( xFilial( "TJF" ) + TN0->TN0_NUMRIS )

					While TJF->( !Eof() ) .And. TJF->TJF_FILIAL == xFilial( "TJF" ) .And. TJF->TJF_NUMRIS == TN0->TN0_NUMRIS

						If NGSEEK( "TO4" , TJF->TJF_MEDCON , 1 , "TO4_TIPCTR" ) == "2"
							lEPCefic := .T.
							Exit
						EndIf

						TJF->( dbSkip() )
					End

				EndIf

			EndIf

		EndIf

	EndIf

	For nEpi := 1 To Len( aCAEPI ) //Verificar

		If lConsEPC .And. lEPCprot
			aCAEPI[ nEpi , 4 ] := If( lEPCefic , "S" , "N" )
		Else
			//cMedPrt := "N"
			//For Array EPIS para Atribuir Medida de Prote��o se cMedPrt == "N"
			aCAEPI[ nEpi , 4 ] := "N"
		EndIf

	Next nEpi

	If !lEpiEntregue
		cCndFun := "N"
		cUsuIni := "N"
		cPrzVld := "N"
		cPerTrc := "N"
		cHigien := "N"
	EndIf

	If Select( cAliTRB ) > 0
		oTempAli:Delete()
	EndIf

	RestArea( aAreaEPI )

Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} fAjustaData
Fun��o replicada de MDTR700, Ajusta os Epis entregues em outra funcao/setor

@param dInicio Data Indica a data de in�cio de exposi��o ao risco.
@param cAliTRB Caracter Alias do TRB

@author Jackson Machado
@since 06/03/2014
/*/
//---------------------------------------------------------------------
Static Function fAjustaData( dInicio , cAliTRB )

Local aRecnos := {}
Local nFor

dbSelectArea( cAliTRB )
dbSetOrder(1)
dbSeek(DTOS(dInicio))
While !Eof() .And. dInicio == ( cAliTRB )->DTINI

	cSvCA     := ( cAliTRB )->NUMCAP
	dDtIniTRB := ( cAliTRB )->DTREAL
	nRecnoTRB := ( cAliTRB )->(Recno())
	lFirstTRB := .T.

	While !Eof() .and. dInicio == ( cAliTRB )->DTINI .And. cSvCA == ( cAliTRB )->NUMCAP
		If !lFirstTRB .and. ( cAliTRB )->DTREAL > dDtIniTRB
			dDtIniTRB := ( cAliTRB )->DTREAL
			nRecnoTRB := ( cAliTRB )->( Recno() )
		Endif

		RecLock( cAliTRB , .F. )
		( cAliTRB )->SITUAC := "S"
		MsUnLock( cAliTRB )

		lFirstTRB := .F.

		dbSelectArea( cAliTRB )
		dbSkip()
	End

	aAdd( aRecnos , nRecnoTRB )
End

For nFor := 1 to Len( aRecnos )
	dbSelectArea( cAliTRB )
	dbGoTo( aRecnos[ nFor ] )
	If ( cAliTRB )->( !Eof() ) .and. ( cAliTRB )->( !Bof() )
		RecLock( cAliTRB , .F. )
		( cAliTRB )->SITUAC := " "
		MsUnLock( cAliTRB )
	Endif
Next nFor

Return

//---------------------------------------------------------------------
/*/{Protheus.doc} fQtdeAfast
Fun��o replicada de MDTR700, Verifica se houve afastamento e retorna a
quantidade de dias.

@param cFilFun   Caracter Indica a filial do funcionario.
@param cMatric   Caracter Indica o c�digo da matricula do funcionario.
@param _dtIniRis Data Indica a data de inicio do risco
@param _dtFimRis Data Indica a data de termino do risco

@author Guilherme Benkendorf
@since 05/03/2014
/*/
//---------------------------------------------------------------------
Static Function fQtdeAfast( cFilFun , cMatric , _dtIniRis , _dtFimRis )

Local nPos, nX, nCont := 0
Local nDias   := 0
Local nVetID  := 0
Local dTmpSR8 := StoD("")
Local dIniAfa := StoD("")
Local dFimAfa := StoD("")
Local cModoSR8, cXFilSR8
Local aAfasta := {}
Local lMudou := .F.

cModoSR8 := NGSEEKDIC( "SX2", "SR8", 1, "X2_MODOEMP+X2_MODOUN+X2_MODO" )
cXFilSR8 := FwxFilial( "SR8", cFilFun, Substr(cModoSR8,1,1), Substr(cModoSR8,2,1), Substr(cModoSR8,3,1) )

dbSelectArea( "SR8" )
dbSetOrder( 1 ) //R8_FILIAL+R8_MAT+DTOS(R8_DATAINI)+R8_TIPO
dbSeek( cXFilSR8 + cMatric )
While SR8->( !Eof() ) .And. SR8->R8_FILIAL + SR8->R8_MAT == cXFilSR8 + cMatric

	dTmpSR8 := If( Empty( SR8->R8_DATAFIM ) , _dtFimRis , SR8->R8_DATAFIM )

	If SR8->R8_DATAINI <= _dtFimRis .And. dTmpSR8 >= _dtIniRis .And. !Empty(SR8->R8_DATAINI)

		dIniAfa := If( SR8->R8_DATAINI < _dtIniRis , _dtIniRis , SR8->R8_DATAINI )
		dFimAfa := If( dTmpSR8 > _dtFimRis , _dtFimRis , dTmpSR8 )
		nVetID++
		aAdd( aAfasta , { dIniAfa , dFimAfa , .T. , nVetID } )

	Endif
	dbSelectArea("SR8")
	dbSkip()
End

While nCont < 1000
	nCont++
	lMudou := .F.
	For nX := 1 To Len( aAfasta )
		If aAfasta[ nX , 3 ]
			nPos := aSCAN( aAfasta , { |x| x[1] <= aAfasta[ nX , 2 ] .And. x[ 2 ] >= aAfasta[ nX , 1 ] .And. nX <> x[ 4 ] .And. x[ 3 ] } )
			If nPos > 0
				aAfasta[ nX , 1 ] := If( aAfasta[ nX , 1 ] > aAfasta[ nPos , 1 ] , aAfasta[ nPos , 1 ] , aAfasta[ nX , 1 ] )
				aAfasta[ nX , 2 ] := If( aAfasta[ nX , 2 ] < aAfasta[ nPos , 2 ] , aAfasta[ nPos , 2 ] , aAfasta[ nX , 2 ] )
				aAfasta[ nPos,3 ] := .T.
				lMudou := .T.
			Endif
		Endif
	Next nX

	If !lMudou
		Exit
	Endif
End
For nX := 1 To Len( aAfasta )
	If aAfasta[ nX , 3 ]
		nDias += ( aAfasta[ nX , 2 ] - aAfasta[ nX , 1 ] ) + 1
	Endif
Next nX

Return nDias

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTFilRel

Busca as filiais relacionadas a filial matriz passada por par�metro

@type Function

@return aFiliais, Aray, Retorna as filias relacionadas � filial passada por par�metro

@sample MDTFilRel( "L MG 01" )

@param cFilial, Caracter, Filial matriz a ser utilizada na busca

@author  Luis Fellipy Bett
@since   01/08/2019
/*/
//-------------------------------------------------------------------
Function MDTFilRel( cFil )

	Local aFiliais	:= {}
	Local nTamEmp	:= 0
	Local nTamReg	:= 0

	//Adiciona a filial de envio no array
	aAdd( aFiliais , { cFil } )

	//Busca filiais associadas a filial de envio
	dbSelectArea( "C1E" )
	dbSetOrder( 3 ) //C1E_FILIAL+C1E_FILTAF+C1E_ATIVO
	dbSeek( xFilial( "C1E" ) + cFil )
	dbSelectArea( "CR9" )
	dbSetOrder( 1 ) //CR9_FILIAL+CR9_ID+CR9_CODFIL+CR9_VERSAO
	dbSeek( xFilial( "CR9" ) + C1E->C1E_ID )
	While xFilial( "CR9" ) == CR9->CR9_FILIAL .And. CR9->CR9_ID == C1E->C1E_ID
		If CR9->CR9_ATIVO == '1'
			nTamEmp := Len( AllTrim( cEmpAnt ) )
			nTamReg	:= Len( CR9->CR9_CODFIL )
			If cEmpAnt $ AllTrim( CR9->CR9_CODFIL )
				aAdd( aFiliais , { AllTrim( Right( CR9->CR9_CODFIL , nTamReg - nTamEmp ) ) } )
			Else
				aAdd( aFiliais , { CR9->CR9_CODFIL } )
			EndIf
		EndIf
		CR9->( dbSkip() )
	End

Return aFiliais

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTBFilEnv

Busca a filial de envio

@type Function

@return cFilEnv, Caracter, Retorna a filial de envio

@sample MDTBFilEnv( .T. )

@param lTrcFil, L�gico, Indica se troca a cFilAnt para a filial matriz

@author  Luis Fellipy Bett
@since   14/08/2019
/*/
//-------------------------------------------------------------------
Function MDTBFilEnv( lTrcFil )

	Local cFilEnv   := ""
	Local aFilInTaf := {}
	Local aArrayFil := {}
	Local nFil1		:= 0
	Local nFil2		:= 0
	Local nPosFil	:= 0
	Local lExit		:= .F.

	Default lTrcFil	:= .F.

	//Busca a filial a ser considerada no envio ao TAF
	fGp23Cons(@aFilInTaf, @aArrayFil, @cFilEnv)

	// Percorre as filiais matriz da SM0
	For nFil1 := 1 To Len( aFilInTaf )
		// Percorre todas as filiais filhas das filiais matriz da SM0
		For nFil2 := 1 To Len( aFilInTaf[ nFil1 , 3 ] )
			// Caso a filial de envio seja uma filial filha considera a matriz dela na condi��o
			If aScan( aFilInTaf[ nFil2 , 3 ] , { |x| x == cFilEnv } ) > 0
				nPosFil := nFil1
				lExit := .T.
				Exit
			EndIf
		Next nFil2
		If lExit
			Exit
		EndIf
	Next nFil1

	If lTrcFil .And. !Empty( cFilEnv ) .And. Len( aFilInTaf ) > 0 .And. nPosFil > 0 .And. ( Len( aFilInTaf[ nPosFil, 3 ] ) > 1 )
		cFilAnt := cFilEnv
	EndIf

	If Empty( cFilEnv )
		cFilEnv := cFilAnt
	EndIf

Return cFilEnv

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTVerTSVE

Fun��o que verifica se o funcion�rio � TSVE - Trabalhador Sem V�nculo Estatut�rio

@return lRet, L�gico, Verdadeiro caso a categoria do trabalhador for de TSVE

@sample MDTVerTSVE( "701" )

@param cCodCateg, Caracter, C�digo da Categoria do Funcion�rio

@author	Luis Fellipy Bett
@since	03/12/2019
/*/
//-------------------------------------------------------------------
Function MDTVerTSVE( cCodCateg )

	Local lRet := .T.

	If !( cCodCateg $ "201/202/401/410/701/711/712/721/722/723/731/734/738/741/751/761/771/901/902/903/904/905" )
		lRet := .F.
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTEnvMid

Realiza o envio dos eventos de SST para o eSocial atrav�s do Middleware

@return .T., sempre verdadeiro

@sample MDTEnvMid( "D MG 01 ", "787878", "S2210", "D MG 01 2019050214001", "<eSocial></eSocial>", 3 )

@param cFilEnv		- Caracter	- Filial de envio
@param cMatricula	- Caracter	- N�mero da Matr�cula do Funcion�rio
@param cEvento		- Caracter	- Nome do Evento
@param cChave		- Caracter	- Chave de Busca do Registro
@param cXml			- Caracter	- Xml a ser enviado ao eSocial
@param nOper		- Num�rico	- Opera��o a ser realizada (3- Inclus�o, 4- Altera��o e 5-Exclus�o)

@author	Luis Fellipy Bett
@since	03/12/2019
/*/
//-------------------------------------------------------------------
Function MDTEnvMid( cFilEnv, cMatricula, cEvento, cChave, cXml, nOper )

	Local aSM0		:= FWLoadSM0( .T., , .T. )
	Local aInfEnv	:= {}
	Local aInfReg	:= { /*cStatMid*/, /*cOpcRJE*/, /*cRetfRJE*/, /*nRecRJE*/ }
	Local lRet		:= .T.
	Local lCadastra	:= .F.
	Local nOpcao	:= 3
	Local cId		:= ""

	Default nOper	:= 5

	//Verifica��o de exist�ncia da tabela RJE
	If !ChkFile( "RJE" ) .And. ChkFile( "RJ9" )

		Help(' ',1,STR0021,,STR0036,2,0,,,,,,{STR0037}) //"Tabela RJE n�o encontrada"##"Execute o UPDDISTR para atualizar o dicion�rio e a base de dados"
		lRet := .F.

	ElseIf ChkFile( "RJE" )

		//Verifica��o dos eventos predecessores - Evento S1000
		cStatus := "-1"

		lS1000 := fVld1000( AnoMes( dDataBase ), @cStatus )
		// 1 - N�o enviado 			- Gravar por cima do registro encontrado
		// 2 - Enviado 				- Aguarda Retorno ( Enviar mensagem em tela e n�o continuar com o processo )
		// 3 - Retorno com Erro		- Gravar por cima do registro encontrado
		// 4 - Retorno com Sucesso	- Efetivar a grava��o

		If lS1000 //Valida Evento S-1000

			aInfoC := fXMLInfos()

			If Len( aInfoC ) >= 4
				cTpInsc  := aInfoC[1]
				cNrInsc  := aInfoC[2]
				cId		 := aInfoC[3]
				lAdmPubl := aInfoC[4]
			Else
				cTpInsc  := ""
				cNrInsc  := "0"
				cId		 := ""
				lAdmPubl := .F.
			EndIf

			//Se for evento de Funcion�rio, valida o evento S-2200/S-2230
			If "S22" $ cEvento
				lCadastra := MDTVld2200( cMatricula, cTpInsc, cNrInsc, lAdmPubl )
			Else
				lCadastra := .T.
			EndIf

			//Cadastra na tabela RJE
			If lCadastra

				If ( nFilEmp := aScan( aSM0, { |x| x[1] == cEmpAnt .And. x[18] == cNrInsc } ) ) > 0
					cFilEmp := aSM0[nFilEmp, 2]
				Else
					cFilEmp := cFilAnt
				EndIf

				//Define status como "-1"
				aInfReg[1]	:= "-1"

				//Busca informa��es do registro
				aInfReg := MDTVerStat( .F., cEvento, cChave )

				//Caso seja Altera��o ou Exclus�o
				If nOper == 4 .Or. nOper == 5

					//Retorno pendente impede o cadastro
					If aInfReg[1] == "2"
						cMsgErro := STR0038 //"Opera��o n�o ser� realizada pois o evento foi transmitido mas o retorno est� pendente"
						lRet	 := .F.
					EndIf

					If nOper == 4 //Altera��o

						If aInfReg[2] == "E" .And. aInfReg[1] != "4" //Evento de exclus�o sem transmiss�o impede o cadastro

							cMsgErro := STR0039 //"Opera��o n�o ser� realizada pois h� evento de exclus�o que n�o foi transmitido ou est� com retorno pendente"
							lRet	 := .F.

						ElseIf aInfReg[1] == "-1" //N�o existe na fila, ser� tratado como inclus�o

							nOpcao 	 := 3
							cOperNew := "I"
							cRetfNew := "1"
							cStatNew := "1"
							lNovoRJE := .T.

						ElseIf aInfReg[1] $ "1/3" //Evento sem transmiss�o, ir� sobrescrever o registro na fila

							If aInfReg[2] == "A"
								nOpcao := 4
							EndIf
							cOperNew := aInfReg[2]
							cRetfNew := aInfReg[3]
							cStatNew := "1"
							lNovoRJE := .F.

						ElseIf aInfReg[2] != "E" .And. aInfReg[1] == "4" //Evento diferente de exclus�o transmitido, ir� gerar uma retifica��o

							nOpcao 	 := 4
							cOperNew := "A"
							cRetfNew := "2"
							cStatNew := "1"
							lNovoRJE := .T.

						ElseIf aInfReg[2] == "E" .And. aInfReg[1] == "4" //Evento de exclus�o transmitido, ser� tratado como inclus�o

							nOpcao 	 := 3
							cOperNew := "I"
							cRetfNew := "1"
							cStatNew := "1"
							lNovoRJE := .T.

						EndIf

					ElseIf nOper == 5 //Exclus�o

						nOpcao := 5

						If aInfReg[2] == "E" .And. aInfReg[1] != "4" //Evento de exclus�o sem transmiss�o impede o cadastro
							cMsgErro := STR0039 //"Opera��o n�o ser� realizada pois h� evento de exclus�o que n�o foi transmitido ou est� com retorno pendente"
							lRet	 := .F.

						ElseIf aInfReg[2] != "E" .And. aInfReg[1] == "4" //Evento diferente de exclus�o transmitido ir� gerar uma exclus�o

							cOperNew := "E"
							cRetfNew := aInfReg[3]
							cStatNew := "1"
							lNovoRJE := .T.

						EndIf
					EndIf

				//Caso seja Inclus�o
				ElseIf nOper == 3

					If aInfReg[1] == "2" //Retorno pendente impede o cadastro

						cMsgErro := STR0038 //"Opera��o n�o ser� realizada pois o evento foi transmitido mas o retorno est� pendente"
						lRet	 := .F.

					ElseIf aInfReg[2] == "E" .And. aInfReg[1] != "4" //Evento de exclus�o sem transmiss�o impede o cadastro

						cMsgErro := STR0039 //"Opera��o n�o ser� realizada pois h� evento de exclus�o que n�o foi transmitido ou est� com retorno pendente"
						lRet	 := .F.

					ElseIf aInfReg[1] $ "1/3" //Evento sem transmiss�o, ir� sobrescrever o registro na fila

						nOpcao		:= Iif( aInfReg[2] == "I", 3, 4 )
						cOperNew 	:= aInfReg[2]
						cRetfNew	:= aInfReg[3]
						cStatNew	:= "1"
						lNovoRJE	:= .F.

					ElseIf aInfReg[2] != "E" .And. aInfReg[1] == "4" //Evento diferente de exclus�o transmitido, ir� gerar uma retifica��o

						cOperNew 	:= "A"
						cRetfNew	:= "2"
						cStatNew	:= "1"
						lNovoRJE	:= .T.

					Else //Ser� tratado como inclus�o

						cOperNew 	:= "I"
						cRetfNew	:= "1"
						cStatNew	:= "1"
						lNovoRJE	:= .T.

					EndIf
				EndIf

				If lRet

					//RJE_FILIAL: Filial do sistema conforme compartilhamento da tabela
					//RJE_FIL: Filial que est� sendo alterada
					//RJE_TPINSC : RJ9_TPINSC
					//RJE_INSCR: RJ9_NRINSC (8 caracteres)
					//RJE_EVENTO: S1030
					//RJE_INI: Data base (AAAAMM)
					//RJE_KEY : C�digo da Filial Completa + C�digo do Cargo
					//RJE_RETKEY: ID do XML
					//RJE_RETF: "1"
					//RJE_VERS: vers�o do Protheus
					//RJE_STATUS: "1"
					//RJE_DTG : Data Gera��o do Evento
					//RJE_HORAG: Hora Gera��o do Evento
					//RJE_OPER: Opera��o a ser realizada (I-Inclus�o, A-Altera��o, E-Exclus�o)

					aAdd( aInfEnv, { xFilial( "RJE", cFilEnv ), cFilEnv, cTpInsc, IIf( cTpInsc == "1" .And. !lAdmPubl, SubStr( cNrInsc, 1, 8 ), cNrInsc ), cEvento, ;
									AnoMes( dDataBase ), cChave, cId, cRetfNew, "12", cStatNew, Date(), Time(), cOperNew } )

					//Se n�o for uma exclus�o de registro n�o transmitido, cria/atualiza registro na fila
					If !( nOpcao == 5 .And. ( ( aInfReg[2] == "E" .And. aInfReg[1] == "4" ) .Or. aInfReg[1] $ "-1/1/3" ) )

						If lRet := fGravaRJE( aInfEnv, cXml, lNovoRJE, aInfReg[4] )
							Help(' ',1,STR0021,,STR0040 + "(" + cEvento + ")" + STR0041,2,0) //"Registro ('evento') cadastrado com sucesso na tabela RJE
						Else
							Help(' ',1,STR0021,,STR0042 + "(" + cEvento + ")" + STR0043,2,0) //"Ocorreu um erro na grava��o do registro ('evento') na tabela RJE
						EndIf

					//Se for uma exclus�o e n�o for de registro de exclus�o transmitido, exclui registro de exclus�o na fila
					ElseIf nOpcao == 5 .And. aInfReg[1] != "-1" .And. !(aInfReg[2] == "E" .And. aInfReg[1] == "4")

						If lRet := fExcluiRJE( aInfReg[4] )
							Help(' ',1,STR0021,,STR0044 + "(" + cEvento + ")" + STR0045,2,0) //"Registro de exclus�o ('evento') cadastrado com sucesso na tabela RJE
						Else
							Help(' ',1,STR0021,,STR0046 + "(" + cEvento + ")" + STR0047,2,0) //"Ocorreu um erro na exclus�o do registro ('evento') da tabela RJE
						EndIf

					EndIf

				Else
					Help(' ',1,STR0021,,cMsgErro,2,0)
				Endif

			Else
				Help(' ',1,STR0021,,STR0048 + "(" + cEvento + ")" + STR0049,2,0) //"N�o ser� poss�vel integrar o evento ('evento') com o Middleware pois o registro de Admiss�o ou Carga Inicial deste Funcion�rio ainda n�o foi efetivado no Middleware"
			EndIf
		Else

			Do Case
				Case cStatus == "-1" .Or. cStatus == "0" // nao encontrado na base de dados
					cMsgRJE := STR0050 //"Registro do evento S-1000 n�o localizado na base de dados"
				Case cStatus == "1" // nao enviado para o governo
					cMsgRJE := STR0051 //"Registro do evento S-1000 n�o transmitido para o governo"
				Case cStatus == "2" // enviado e aguardando retorno do governo
					cMsgRJE := STR0052 //"Registro do evento S-1000 aguardando retorno do governo"
				Case cStatus == "3" // enviado e retornado com erro
					cMsgRJE := STR0053 //"Registro do evento S-1000 retornado com erro do governo"
			EndCase

			Help(' ',1,STR0021 + "! " + STR0054,,cMsgRJE,2,0,,,,,,{STR0055}) //"Problemas com o Evento S-1000"##"Favor enviar primeiramente o evento S-1000"
			lRet := .F.

		Endif
	Endif

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTVerStat

Verifica o status do registro passado por par�metro e retorna se ele
j� foi enviado ao TAF ou n�o

@return xRet, Indefindo, Ou l�gico caso o registro exista na RJE ou um Array

@sample MDTVerStat( .T., "S2210", "D MG 01 2019050214001", .F. )

@param lRetBool, Boolean, Indica se o retorno da fun��o vai ser Booleano, sen�o retorna um Array
@param cEvento, Caracter, Nome do evento
@param cChave, Caracter, Chave de busca pelo registro do evento
@param lExclu, Boolean, Indica se exclui o registro da RJE caso ele n�o esteja transmitido ao governo

@author	Luis Fellipy Bett
@since	03/12/2019
/*/
//-------------------------------------------------------------------
Function MDTVerStat( lRetBool, cEvento, cChave, lExclu )

	Local xRet
	Local cStatMid	:= "-1"
	Local cOpcRJE	:= ""
	Local cRetfRJE	:= ""
	Local nRecRJE	:= 0

	Default lRetBool := .F.
	Default lExclu	 := .F.

	aInfoC := fXMLInfos()

	If Len( aInfoC ) >= 4
		cTpInsc  := aInfoC[1]
		lAdmPubl := aInfoC[4]
		cNrInsc  := aInfoC[2]
	Else
		cTpInsc  := ""
		lAdmPubl := .F.
		cNrInsc  := "0"
	EndIf

	//RJE_TPINSC + RJE_INSCR + RJE_EVENTO + RJE_KEY + RJE_INI
	cChvBus := Padr( cTpInsc, TAMSX3("RJE_TPINSC")[1] ) + ;
			   Padr( IIf( cTpInsc == "1" .And. !lAdmPubl, SubStr( cNrInsc, 1, 8 ), cNrInsc ), TAMSX3("RJE_INSCR")[1] ) + ;
			   cEvento + ;
			   Padr( cChave, TAMSX3("RJE_KEY")[1] )

	GetInfRJE( 2, cChvBus, @cStatMid, @cOpcRJE, @cRetfRJE, @nRecRJE )

	//Caso o retorno seja booleano
	If lRetBool
		If lExclu .And. ( cStatMid <> "4" .And. cStatMid <> "-1" ) .And. nRecRJE > 0 //Caso o registro exista na RJE e n�o esteja transmitido ao governo, exclui
			If fExcluiRJE( nRecRJE )
				xRet := .F.
			EndIf
		ElseIf cStatMid <> "-1" //Caso o registro exista na tabela RJE
			xRet := .T.
		Else
			//Caso o registro n�o exista na RJE
			xRet := .F.
		EndIf
	Else
		xRet := { cStatMid, cOpcRJE, cRetfRJE, nRecRJE }
	EndIf

Return xRet

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTVld2200

Verifica se existe um evento S-2200/S-2230 enviado ao governo

@return	lRet, Boolean, .T. se existir um evento comunicado ao governo, sen�o .F.

@sample MDTVld2200( "787878", "1", "581312150001", .F. )

@param	cMatricula,	Caracter, Matr�cula do funcion�rio (RA_MAT)
@param	cTpInsc,	Caracter, Tipo de Inscri��o
@param	cNrInsc,	Caracter, N�mero de Inscri��o
@param	lAdmPubl,	Boolean, Indica se � Administra��o P�blica

@author	Luis Fellipy Bett
@since	11/12/2019
/*/
//-------------------------------------------------------------------
Function MDTVld2200( cMatricula, cTpInsc, cNrInsc, lAdmPubl )

	Local lRet		:= .T.
	Local cCodCateg	:= Posicione( "SRA", 1, xFilial( "SRA" ) + cMatricula, "RA_CATEFD"	)
	Local cCodUnic	:= Posicione( "SRA", 1, xFilial( "SRA" ) + cMatricula, "RA_CODUNIC"	)
	Local cCPF		:= Posicione( "SRA", 1, xFilial( "SRA" ) + cMatricula, "RA_CIC"		)
	Local dDtAdmis	:= Posicione( "SRA", 1, xFilial( "SRA" ) + cMatricula, "RA_ADMISSA"	)
	Local cStatus	:= "-1"
	Local cChave	:= ""

	If MDTVerTSVE( cCodCateg ) //Caso for Trabalhador Sem V�nculo Estatut�rio
		cChave := cTpInsc + Padr( IIf( !lAdmPubl .And. cTpInsc == "1", SubStr(cNrInsc, 1, 8), cNrInsc), TAMSX3("RJE_INSCR")[1] ) + "S2300" + Padr( AllTrim( cCPF ) + AllTrim( cCodCateg ) + DToS( dDtAdmis ), TAMSX3("RJE_KEY")[1], " " )
	Else
		cChave := cTpInsc + Padr( IIf( !lAdmPubl .And. cTpInsc == "1", SubStr(cNrInsc, 1, 8), cNrInsc), TAMSX3("RJE_INSCR")[1] ) + "S2200" + Padr( AllTrim( cCodUnic ), TAMSX3("RJE_KEY")[1], " " )
	EndIf

	//RJE_TPINSC + RJE_INSCR + RJE_EVENTO + RJE_KEY + RJE_INI
	GetInfRJE( 2, cChave, @cStatus )

	If cStatus != "4"
		lRet := .F.
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTGerCabc

Gera o cabe�alho dos Xml's a serem enviados ao Governo (ID Xml + Evento + Empregador)

@return .T., Sempre verdadeiro

@sample MDTGerCabc( "", "S2210", "1", "548648510000", .T., .F., "3" )

@param	cXml,		 Caracter, Xml a ser carregado
@param	cEvento,	 Caracter, Nome do evento
@param	cTpInsc,	 Caracter, Tipo da inscri��o
@param	cNrInsc,	 Caracter, N�mero da inscri��o
@param	lMiddleware, Boolean,  Indica se a estrutura��o de envio � via Middleware
@param	lXml,		 Boolean,  Indica se � gera��o de Xml
@param	cOper,		 Caracter, Opera��o que est� sendo realizada (3- Inclus�o, 4- Altera��o e 5-Exclus�o)
@param	cChave,		 Caracter, Chave do registro a ser verificado

@author	Luis Fellipy Bett
@since	12/12/2019
/*/
//-------------------------------------------------------------------
Function MDTGerCabc( cXml, cEvento, cTpInsc, cNrInsc, lMiddleware, lXml, cOper, cChave )

	Local cVersMw	:= ""
	Local cId		:= ""
	Local cStatReg	:= "-1"
	Local cOperReg	:= "I"
	Local cRetfReg	:= "1"
	Local nRecReg	:= 0
	Local cRecibReg	:= ""
	Local cRecibAnt	:= ""
	Local cRetfNew	:= "1"
	Local cRecibXML	:= ""
	Local cTag		:= MDTTagEsoc( cEvento ) //Busca a Tag referente ao evento
	Local lAdmPubl	:= .F.

	//Define como padr�o Xml de inclus�o
	Default cOper := "3"

	//Caso envio seja atrav�s do Middleware
	If lMiddleware

		If FindFunction( "fVersEsoc" )
			fVersEsoc( cEvento, .F., /*aRetGPE*/ , /*aRetTAF*/ , Nil, Nil, @cVersMw )
		EndIf

		aInfoC := fXMLInfos()

		If Len( aInfoC ) >= 4
			cTpInsc  := aInfoC[1]
			cNrInsc  := aInfoC[2]
			cId  	 := aInfoC[3]
			lAdmPubl := aInfoC[4]
		Else
			cTpInsc  := ""
			cNrInsc  := "0"
			cId  	 := ""
			lAdmPubl := .F.
		EndIf

		//RJE_TPINSC + RJE_INSCR + RJE_EVENTO + RJE_KEY + RJE_INI
		cChvBus := Padr( cTpInsc, TAMSX3("RJE_TPINSC")[1] ) + ;
				Padr( IIf( cTpInsc == "1" .And. !lAdmPubl, SubStr( cNrInsc, 1, 8 ), cNrInsc ), TAMSX3("RJE_INSCR")[1] ) + ;
				cEvento + ;
				Padr( cChave, TAMSX3("RJE_KEY")[1] )

		GetInfRJE( 2, cChvBus, @cStatReg, @cOperReg, @cRetfReg, @nRecReg, @cRecibReg, @cRecibAnt, Nil, Nil, .T. )

		//Evento sem transmiss�o, ir� sobrescrever o registro na fila
		If cStatReg $ "1/3"
			cOperNew 	:= cOperReg
			cRetfNew	:= cRetfReg
			cStatNew	:= "1"
			lNovoRJE	:= .F.
		//Evento diferente de exclus�o transmitido, ir� gerar uma retifica��o
		ElseIf cOperReg != "E" .And. cStatReg == "4"
			cOperNew 	:= "A"
			cRetfNew	:= "2"
			cStatNew	:=  "1"
			lNovoRJE	:= .T.
		//Ser� tratado como inclus�o
		Else
			cOperNew 	:= "I"
			cRetfNew	:= "1"
			cStatNew	:= "1"
			lNovoRJE	:= .T.
		EndIf
		If cRetfNew == "2"
			If cStatReg == "4"
				cRecibXML 	:= cRecibReg
				cRecibAnt	:= cRecibReg
				cRecibReg	:= ""
			Else
				cRecibXML 	:= cRecibAnt
			EndIf
		EndIf

		cXml := "<eSocial xmlns='http://www.esocial.gov.br/schema/evt/" + cTag + "/v" + cVersMw + "'>"
		cXml += 	"<" + cTag + " Id='" + cId + "'>"
	Else
		cXml := "<eSocial>"
		cXml += 	"<" + cTag + ">"

		//Caso seja uma altera��o, define o Xml como retifica��o
		If cOper == "4"
			cRetfNew := "2"
		EndIf
	EndIf

	If !( "S1060" $ cEvento ) //Evento S-1060 n�o possui a tag <ideEvento>
		//V�nculo Evento
		cXml += 		'<ideEvento>'
		cXml +=				'<indRetif>' + 	cRetfNew	+ '</indRetif>'
		If lMiddleware //Caso seja via Middleware (pega o recibo)
			If cRetfNew == "2" //Caso seja retifica��o
				cXml +=		'<nrRecibo>' + 	cRecibXML	+ '</nrRecibo>'
			EndIf
			cXml +=			'<tpAmb>' + 	"1"			+ '</tpAmb>'
			cXml +=			'<procEmi>' + 	"1"			+ '</procEmi>'
			cXml +=			'<verProc>' + 	"12"		+ '</verProc>'
		EndIf
		cXml += 		'</ideEvento>'
	EndIf

	//V�nculo Empregador
	cXml += 			'<ideEmpregador>'
	cXml += 				'<tpInsc>' + cTpInsc	+ '</tpInsc>'
	cXml += 				'<nrInsc>' + IIf( ( lXml .Or. !lAdmPubl ) .And. cTpInsc == "1", SubStr( cNrInsc, 1, 8 ), cNrInsc )	+ '</nrInsc>'
	cXml += 			'</ideEmpregador>'

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTXmlVal

Busca o valor de uma tag dentro do Xml

@return	 cVal, Indefinido, Valor do n� passado no caminho do Xml

@sample MDTXmlVal( "S2240", "<eSocial></eSocial>", "/ns:eSocial/ns:evtExpRisco", "D" )

@param	 cEvento, Caracter, Nome do evento
@param	 cXml, Caracter, Xml a que ser� buscado o valor
@param	 cCamTag, Caracter, Caminho do Xml onde est� o valor a ser pego
@param	 cTipRet, Caracter, Tipo de retorno do valor

@author  Luis Fellipy Bett
@since   31/01/2020
/*/
//-------------------------------------------------------------------
Function MDTXmlVal( cEvento, cXml, cCamTag, cTipRet )

	Local oXml
	Local cVal		:= ""
	Local cVersMw	:= ""
	Local cTag		:= MDTTagEsoc( cEvento ) //Busca a Tag referente ao evento

	If FindFunction( "fVersEsoc" )
		fVersEsoc( cEvento, .F., /*aRetGPE*/ , /*aRetTAF*/ , Nil, Nil, @cVersMw )
	EndIf

	oXml := TXMLManager():New()

	//Caso o Xml passado n�o esteja vazio
	If !Empty( cXml )
		If !oXml:Parse( cXml )
			Help(' ',1,STR0021,,STR0056,2,0,,,,,,{STR0057}) //"Aten��o"##"Erro na estrutura do Xml"##"Contate o administrador do sistema"
		Else
			oXml:XPathRegisterNs( "ns", "http://www.esocial.gov.br/schema/evt/" + cTag + "/v" + cVersMw )

			If oXml:XPathHasNode( cCamTag )
				cVal := oXml:XPathGetNodeValue( cCamTag )
			EndIf
		EndIf
	EndIf

	//Caso a tag esteja preenchida
	If !Empty( cVal )
		If ValType( cVal ) <> cTipRet
			If cTipRet == "D"
				cVal := SToD( cVal )
			ElseIf cTipRet == "N"
				cVal := Val( cVal )
			EndIf
		EndIf
	Else //Caso a tag esteja vazia
		If cTipRet == "D"
			cVal := SToD("")
		ElseIf cTipRet == "N"
			cVal := 0
		EndIf
	EndIf

Return cVal

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTTagEsoc

Retorna a tag referente ao evento passado por par�metro

@return  cTag, Caracter, Tag referente ao evento passado por par�metro

@sample	 MDTTagEsoc( "S2240" )

@param	 cEvento, Caracter, Evento do eSocial

@author  Luis Fellipy Bett
@since   03/02/2020
/*/
//-------------------------------------------------------------------
Function MDTTagEsoc( cEvento )

	Local cTag := ""

	Do Case
		//Tabela de Ambiente de Trabalho
		Case cEvento == "S1060"
			cTag := "evtTabAmbiente"
		//Comunica��o de Acidente de Trabalho (CAT)
		Case cEvento == "S2210"
			cTag := "evtCAT"
		//Monitoramento de Sa�de do Trabalhador (ASO)
		Case cEvento == "S2220"
			cTag := "evtMonit"
		//Exame Toxicol�gico do Motorista Profissional
		Case cEvento == "S2221"
			cTag := "evtToxic"
		//Condi��o Ambiental de Trabalho (Risco)
		Case cEvento == "S2240"
			cTag := "evtExpRisco"
		//Treinamentos e Capacita��es
		Case cEvento == "S2245"
			cTag := "evtTreiCap"
	EndCase

Return cTag

//-------------------------------------------------------------------
/*/{Protheus.doc} MDTAjsData

Ajusta as datas do sistema no formato de envio do Governo para
composi��o no XML

@return	 cDataRet, Caracter, Data tranformada para a aceita pelo Governo

@sample	 MDTAjsData( 13/05/2020 )

@param	 dData, Date, Data a ser tranformada
@param	 lMiddleware, Boolean, Indica se � envio atrav�s do Middleware

@author  Luis Fellipy Bett
@since   13/05/2020
/*/
//-------------------------------------------------------------------
Function MDTAjsData( dData, lMiddleware )

	Local cDataRet	:= ""
	Local cAno		:= SubStr( DToS( dData ), 1, 4 )
	Local cMes		:= SubStr( DToS( dData ), 5, 2 )
	Local cDia		:= SubStr( DToS( dData ), 7, 2 )

	If lMiddleware
		cDataRet := cAno + "-" + cMes + "-" + cDia //2020-05-13
	Else
		cDataRet := cAno + cMes + cDia //20200513
	EndIf

Return cDataRet