#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "MDTM001.CH"

STATIC aEfd	 := If( cPaisLoc == "BRA" , If( Findfunction( "fEFDSocial" ) , fEFDSocial() , { .F. , .F. , .F. } ) , { .F. , .F. , .F. } )
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//  _______           _______  _       _________ _______             _______  __    _______   ______  _______  ------
// (  ____ \|\     /|(  ____ \( (    /|\__   __/(  ___  )           (  ____ \/  \  (  __   ) / ____ \(  __   ) ------
// | (    \/| )   ( || (    \/|  \  ( |   ) (   | (   ) |           | (    \/\/) ) | (  )  |( (    \/| (  )  | ------
// | (__    | |   | || (__    |   \ | |   | |   | |   | |   _____   | (_____   | | | | /   || (____  | | /   | ------
// |  __)   ( (   ) )|  __)   | (\ \) |   | |   | |   | |  (_____)  (_____  )  | | | (/ /) ||  ___ \ | (/ /) | ------
// | (       \ \_/ / | (      | | \   |   | |   | |   | |                 ) |  | | |   / | || (   ) )|   / | | ------
// | (____/\  \   /  | (____/\| )  \  |   | |   | (___) |           /\____) |__) (_|  (__) |( (___) )|  (__) | ------
// (_______/   \_/   (_______/|/    )_)   )_(   (_______)           \_______)\____/(_______) \_____/ (_______) ------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTM001
Rotina de Envio de Eventos - Ambientes de Trabalho (S-1060)
Realiza carga da tabela de Ambientes de Trabalho para o TAF

@return

@sample MDTM001( '012016' , 1 , @{} , @{} , aFilial )

@param cAnoMes Caracter Indica o ANO/MES de Referencia
@param nOpcA Numerico Indica o tipo de operacao (1-Grava��o;2-Impress�o)
@param aLogProc Array Array que ir� receber o Log
@param aDados Array Array que ir� receber as mensagens de erro da integra��o
@param cFilEnv Caracter Filial de envio referente a matriz do TAF
@param lCadastro Logico Indica se veio de um cadastro do Ambiente
@param nOpcX Num�rico Indica o tipo de opera��o do cadastro (3-Incluir;4-Alterar;5-Excluir)
@param cAnoMesFim indica a nova data do ambiente que ser� usada como data fim do ambiente
atual e data inicial de um novo registro de ambiente
@param oModel Modelo de dados que ser�o pegas as informa��es do ambiente de trabalho
@param dDataRef Data de refer�ncia preenchida na rotina de carga inicial no SIGAGPE (GPEM023)

@author Jackson Machado
@since 28/01/2015
/*/
//---------------------------------------------------------------------
Function MDTM001( cAnoMes , nOpcA , aLogProc , aDados , cFilEnv , lCadastro , nOpcX , cAnoMesFim , oModel , dDataRef , lXml , cMsgLog , lInconsis )

	Local aArea		 	:= GetArea()
	Local aAreaTNE    	:= TNE->(GetArea())
	Local cAliasTNE  	:= GetNextAlias()
	Local lMiddleware	:= IIf( cPaisLoc == 'BRA' .And. Findfunction( "fVerMW" ), fVerMW(), .F. )
	Local cMenIni     	:= ""
	Local cMsgLog		:= ""
	Local cXml			:= ""
	Local cQryWhere  	:= "%"
	Local nCont			:= 0
	Local aErros 	 	:= {}
	Local aRetStatus	:= {}
	Local aFiliais		:= {}
	Local xReturn		:= .T.
	Local lFind			:= .F.
	Local lCarga		:= IsInCallStack("GPEM023")

	Default cAnoMesFim	:= ""
	Default aLogProc 	:= {}
	Default aDados		:= {}
	Default lCadastro   := .F.
	Default lXml		:= .F.
	Default lInconsis	:= .F.
	Default nOpcX		:= 3

	//Caso for carga inicial pega a segunda posi��o da filial passada por array
	If IsInCallStack("GPEM023")
		cFilEnv := cFilEnv[2]
	EndIf

	//Verifica se TAF est� preparado
	aRet:= TafExisEsc( "S1060" )
	If !aRet[ 1 ] .Or. ( aRet[ 2 ] <> "2.4" .And. aRet[ 2 ] <> "2.4.02" .And. aRet[ 2 ] <> "2.5" )
		Help(' ',1,STR0001,,STR0035,2,0,,,,,,{STR0036}) //"Aten��o" ## "O ambiente n�o possui integra��o com o m�dulo do TAF e/ou a vers�o do TAF est� desatualizada!" ## "Favor verificar!"
		xReturn := .F.
	ElseIf !( NGCADICBASE( "TMA_ESOC", "A", "TMA", .F. ) )
		Help(' ',1,STR0001,,STR0002,2,0,,,,,,{STR0045}) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial"
		xReturn := .F.
	ElseIf !AliasInDic("TYG")
		Help(' ',1,STR0001,,STR0003,2,0,,,,,,{STR0046}) //"Aten��o" ## "O Dicion�rio de Dados est� desatualizado!" ## "Favor atualizar o dicion�rio para estar em conformidade de envio ao eSocial!"
		xReturn := .F.
	Else
		//Caso n�o tenha a Filial n�o tem como fazer a Integra��o
		If !Empty( cFilEnv )

			If lCadastro

				cAliasTNE := "TNE"

				//Adiciona cabe�alho das inconsist�ncias
				aAdd( aLogProc , STR0010 )
				aAdd( aLogProc , STR0021 )
				aAdd( aLogProc , "" )
				aAdd( aLogProc , "" )
			Else
				//Busca filiais relacionadas a filial de envio
				aFiliais := MDTFilRel( cFilEnv )

				//For�a sempre inclus�o
				nOpcX := 3

				//Ajusta passagem do par�metro cAnoMes
				cAnoMes := SubStr(cAnoMes,3,4) + SubStr(cAnoMes,1,2)

				//Busca informacoes TNE - Ambientes
				#IFDEF TOP
					If !ExeInAs400()
						// Define os ambientes a serem consideradas
						For nCont := 1 To Len( aFiliais )
							cQryWhere += ValToSQL( xFilial( "TNE" , aFiliais[ nCont , 1 ] ) )
							If nCont <> Len( aFiliais )
								cQryWhere += ","
							Else
								cQryWhere += "%"
							EndIf
						Next nCont
						BeginSql alias cAliasTNE
							SELECT
								TNE.TNE_FILIAL, TNE.TNE_CODAMB, TNE.TNE_NOME, TNE.TNE_LOCAMB, TNE.TNE_TPINS, TNE.TNE_NRINS, TNE.TNE_CODLOT
								FROM
								%table:TNE% TNE
								WHERE
								TNE.%notDel% AND TNE.TNE_FILIAL IN (%exp:cQryWhere%)
								ORDER BY
								TNE.TNE_FILIAL, TNE.TNE_CODAMB
						EndSql
					EndIf
				#ELSE
					cAliasTNE := "TNE"
				#ENDIF

				//Posiciona no arquivo e seta no inicio
				dbSelectArea( cAliasTNE )
				( cAliasTNE )->( dbGoTop() )
			EndIf

			//Inicializa regua de processamento
			ProcRegua( ( cAliasTNE )->( RecCount() ) )

			While ( cAliasTNE )->( !Eof() ) .Or. lCadastro

				If !lCadastro
					dbSelectArea( "TNE" )
					dbSetOrder( 1 )
					lFind := .F.
					For nCont := 1 To Len( aFiliais )
						If dbSeek( xFilial( "TNE", aFiliais[ nCont , 1 ] ) + ( cAliasTNE )->TNE_CODAMB )
							If !Empty( TNE->TNE_DTVINI )
								lFind := .T.
								Exit
							EndIf
						EndIf
					Next nCont
					If lFind
						( cAliasTNE )->( dbSkip() )
						Loop
					EndIf
				EndIf

				If lCadastro
					cCodAmb := oModel:GetValue( 'TNEMASTER', 'TNE_CODAMB' )
				Else
					cCodAmb := ( cAliasTNE )->TNE_CODAMB
				EndIf

				If nOpcA == 1 //Geracao de Carga

					If lInconsis .Or. lCarga
						If lCarga
							aLogProc := {"","","",""} //Adiciona 4 posi��es no Array para verificar se existem inconsist�ncias
						EndIf

						fInconsis( cAliasTNE , cFilEnv , cCodAmb , @aLogProc , @cMsgLog , lCadastro , nOpcX , oModel , cAnoMes )

						If Len( aLogProc ) > 4
							If !IsBlind() .And. !lCarga
								fMakeLog( { aLogProc } , { STR0006 } , Nil , Nil , "MDTM001" , OemToAnsi( STR0007 ) , "M" , "P" , , .F. ) //"Monitoramento Envio de Eventos - TAF" ## "Log de Ocorr�ncias - Ambientes de Trabalho"

								Help(' ',1,STR0001,,STR0037,2,0,,,,,,{STR0038}) //"N�o foi poss�vel o envio ao eSocial pois os dados est�o inconsistentes!" ## "Favor ajustar os valores expostos no relat�rio!"

								xReturn := .F.
							Else
								xReturn := .F.
								aAdd( aDados , OemToAnsi( STR0004 ) + " " + cCodAmb + " " + OemToAnsi( STR0006 ) + "! " + STR0044 ) //"Ambiente" ## "Falha no envio ao TAF" ## "Para verifica��o favor gerar o relat�rio de inconsist�ncias"
							EndIf
						Else
							xReturn := .T.
						EndIf
					EndIf

					If xReturn .And. !lInconsis
						cXml := fCarrAmb( cAliasTNE, cCodAmb, cAnoMes, cValToChar( nOpcX ), cFilEnv, lCadastro, nOpcX, cAnoMesFim, oModel, lXml, lMiddleware )

						If lXml
							fInconsis( cAliasTNE , cFilEnv , cCodAmb , @aLogProc , @cMsgLog , lCadastro , nOpcX , oModel , cAnoMes )

							If Len( aLogProc ) > 4
								fMakeLog( { aLogProc } , { STR0006 } , Nil , Nil , "MDTM001" , OemToAnsi( STR0007 ) , "M" , "P" , , .F. ) //"Monitoramento Envio de Eventos - TAF" ## "Log de Ocorr�ncias - Ambientes de Trabalho"

								Help(' ',1,STR0001,,STR0034,2,0,,,,,,{STR0038}) //"Favor ajustar os valores expostos no relat�rio!"

								xReturn := .F.
							Else
								xReturn := cXml
							EndIf
						Else
							Begin Transaction

							//Caso o envio seja atrav�s do Middleware
							If lMiddleware

								//Envia o evento ao eSocial atrav�s do Middleware
								MDTEnvMid( cFilEnv, /*cMatricula*/ , "S1060", AllTrim( cCodAmb ) + cAnoMes, cXml, nOpcX )

							Else
								//Enviar como parametro a filial do Protheus na posi��o 1 e o XML
								aErros := TafPrepInt( cEmpAnt , cFilEnv , cXml , , "1" , "S1060",,,,@aRetStatus)

								If Len( aErros ) <= 0
									If !lCadastro
										aAdd( aDados , OemToAnsi( STR0004 ) + " " + cCodAmb + " " + OemToAnsi( STR0005 ) ) //"Ambiente" ## "Enviado ao TAF com sucesso."
									Else
										Help(' ',1,STR0001,,STR0048,2,0) //"Aten��o"##"Registro enviado ao TAF com sucesso!"
									EndIf

									If !lCadastro .And. !Empty(dDataRef)
										dbSelectArea( "TNE" )
										dbSetOrder( 1 )
										dbSeek( xFilial( "TNE", ( cAliasTNE )->TNE_FILIAL ) + ( cAliasTNE )->TNE_CODAMB )
										Reclock("TNE", .F.)
											TNE->TNE_DTVINI := dDataRef
										TNE->(MsUnLock())
									EndIf
								Else
									If !lCadastro
										aAdd( aDados , OemToAnsi( STR0004 ) + " " + cCodAmb + " " + OemToAnsi( STR0006 ) + ": " + aErros[ 1 ] ) //"Ambiente" ## "Falha no envio ao TAF"
									Else
										Help(' ',1,STR0001,,STR0039 + CRLF + aErros[ 1 ],2,0,,,,,,{STR0040}) //"Existem inconsist�ncias na integra��o com o TAF!" ## "Para tentativa de reenvio favor manipular o registro!"
									EndIf
								EndIf

								//Incrementa regua
								IncProc( OemToAnsi( STR0009 ) + " " + cCodAmb ) //"Gerando o registro de:"
							EndIf

							End Transaction
						EndIf
					EndIf
				Else
					If Empty(cMenIni)
						cMenIni := STR0010

						aAdd( aLogProc , cMenIni ) //"Inconsist�ncias de Ambientes de Trabalho"
						aAdd( aLogProc , STR0021 ) //"Os campos abaixo est�o vazios ou zerados e s�o de preenchimento obrigat�rios:"
						aAdd( aLogProc , "" )
						aAdd( aLogProc , "" )
					EndIf

					//Mensagem de log que sera gravado
					cMsgLog += STR0004 + ": " + OemToAnsi( ( cAliasTNE )->TNE_CODAMB ) + " - " + STR0011 + ": " + CRLF //"Preenchimento de campos obrigat�rios"

					fInconsis( cAliasTNE , cFilEnv , cCodAmb , @aLogProc , @cMsgLog , lCadastro , , , cAnoMes )
				EndIf

				If lCadastro
					Exit
				Else
					( cAliasTNE )->( dbSkip() )
				EndIf
			EndDo
		Else
			If !lCadastro
				aAdd( aDados , OemToAnsi( STR0004 ) + " - " + OemToAnsi( STR0006 ) + "." ) //"Ambiente" ## "Falha no envio ao TAF"
			Else
				Help(' ',1,STR0001,,STR0041,2,0,,,,,,{STR0036}) // Aten��o ## "A informa��o da filial de envio ao eSocial est� vazia!" ## "Favor verificar!"
				xReturn := .F.
			EndIf
		EndIf

		//Fecha alias em uso
		#IFDEF TOP
			If ( Select( cAliasTNE ) > 0 )
				( cAliasTNE )->( dbCloseArea() )
			EndIf
		#ENDIF
	EndIf

	dbSelectArea( "TNE" )
	RestArea( aAreaTNE )
	RestArea( aArea )

Return xReturn
//---------------------------------------------------------------------
/*/{Protheus.doc} fCarrAmb
Carrega Ambientes

@return cXml Caracter Estrutura XML a ser enviada para o TAF

@sample fCarrAmb( 'TNE' , '000001' , '201601' , '3' , '01' )

@param cAliasTNE Caracter Valor do Alias utilizado para a Tabela TNE
@param cCodAmb Caracter C�digo do Ambiente
@param cAnoMes Caracter C�digo do ANO/MES de competencia
@param cOper Caracter Opera��o a ser realizada
@param cFilEnv Caracter Filial de Envio
@param lCadastro Logico Indica se veio de um cadastro do Ambiente
@param nOpcX Num�rico Indica o tipo de opera��o do cadastro (3-Incluir;4-Alterar;5-Excluir)
@param cAnoMesFim indica a nova data do ambiente que ser� usada como data fim do ambiente
atual e data inicial de um novo registro de ambiente

@author Jackson Machado
@since 28/01/2015
/*/
//---------------------------------------------------------------------
Static Function fCarrAmb( cAliasTNE, cCodAmb, cAnoMes, cOper, cFilEnv, lCadastro, nOpcX, cAnoMesFim, oModel, lXml, lMiddleware )

	Local cTpEmp		:= ""
	Local cInscEmp		:= ""
	Local cTpAux		:= ""
	Local cInscAux		:= ""
	Local cTpAmb		:= ""
	Local cInscAmb		:= ""
	Local cXml 			:= ""
	Local cNomAux		:= ""
	Local cDescAux  	:= ""
	Local cNomAmb 		:= ""
	Local cDescAmb 		:= ""
	Local cCCusto		:= IIf( lCadastro , oModel:GetValue( 'TNEMASTER', 'TNE_CODLOT' ) , ( cAliasTNE )->TNE_CODLOT )
	Local aEstab		:= {}
	Local oModel
	Local cChaveDtFim	:= If( cOper == "5" .And. !Empty(cAnoMesFim), cAnoMesFim ,'')
	Local lCompCTT		:= Empty( xFilial( "CTT" , IIf( lCadastro , cFilAnt , ( cAliasTNE )->TNE_FILIAL ) ) )

	Default cOper := "3"
	Default cAnoMesFim := ""

	//Busca as informa��es do Estabelecimento
	aEstab := MDTGetEst( cCCusto , cFilEnv )

	cTpEmp		:= aEstab[ 1 , 1 ]
	cInscEmp	:= aEstab[ 1 , 2 ]

	//Busca as informa��es da filial do Ambiente
	aEstab := MDTGetEst( "" , IIf( lCadastro , cFilAnt , ( cAliasTNE )->TNE_FILIAL ) )

	cTpAux		:= aEstab[ 1 , 1 ]
	cInscAux	:= aEstab[ 1 , 2 ]

	If lCadastro
		cNomAux		:= oModel:GetValue( 'TNEMASTER', 'TNE_NOME' 	)
		cDescAux	:= oModel:GetValue( 'TNEMASTER', 'TNE_MEMODS' 	)
		cLocalAmb	:= oModel:GetValue( 'TNEMASTER', 'TNE_LOCAMB'	)
		cTpAmb		:= If( cLocalAmb == "1" , cTpAux	, oModel:GetValue( 'TNEMASTER', 'TNE_TPINS' ) )
		cInscAmb	:= If( cLocalAmb == "1" , cInscAux	, oModel:GetValue( 'TNEMASTER', 'TNE_NRINS' ) )
		cCodLot		:= If( lCompCTT , "" , xFilial( "CTT" , cFilAnt ) ) + oModel:GetValue( 'TNEMASTER', 'TNE_CODLOT' )
	Else
		cNomAux		:= ( cAliasTNE )->TNE_NOME
		cDescAux	:= Posicione( "TNE" , 1 , xFilial( "TNE", ( cAliasTNE )->TNE_FILIAL ) + cCodAmb , "TNE_MEMODS" )
		cLocalAmb	:= ( cAliasTNE )->TNE_LOCAMB
		cTpAmb		:= If( cLocalAmb == "1" , cTpAux	, ( cAliasTNE )->TNE_TPINS )
		cInscAmb	:= If( cLocalAmb == "1" , cInscAux	, ( cAliasTNE )->TNE_NRINS )
		cCodLot		:= If( lCompCTT , "" , xFilial( "CTT" , ( cAliasTNE )->TNE_FILIAL ) ) + ( cAliasTNE )->TNE_CODLOT
	EndIf

	cNomAmb		:= AllTrim( MDTSubTxt( cNomAux ) )
	cDescAmb	:= AllTrim( MDTSubTxt( cDescAux ) )

	//----------------------------------------
	// Protheus	 X  eSocial
	// 1- CNPJ		1- CNPJ
	// 2- CAEPF		3- CAEPF
	// 3- CNO		4- CNO
	//----------------------------------------
	Do Case
		Case cTpAmb == "2" ; cTpAmb := "3"
		Case cTpAmb == "3" ; cTpAmb := "4"
	End Case

	//Cria o cabe�alho do Xml com o ID, informa��es do Evento e Empregador
	MDTGerCabc( @cXml, "S1060", cTpEmp, cInscEmp, lMiddleware, lXml, cOper, cCodAmb + cAnoMes )

	cXml += '		<infoAmbiente>'
	If cOper == "3"
		cXml += '		<inclusao>'
	ElseIf cOper == "4"
		cXml += '		<alteracao>'
	Else
		cXml += '		<exclusao>'
	Endif
	cXml += '				<ideAmbiente>'
	cXml += '					<codAmb>'    	+ cCodAmb  			+ '</codAmb>'
	cXml += '					<iniValid>' 	+ cAnoMes			+ '</iniValid>'
	cXml += '					<fimValid>'		+ cChaveDtFim		+ '</fimValid>'
	cXml += '				</ideAmbiente>'

	If cOper <> "5"
		cXml += '			<dadosAmbiente>'
		cXml += '				<nmAmb>'   		+ cNomAmb 			+ '</nmAmb>'
		cXml += '				<dscAmb>'   	+ cDescAmb 			+ '</dscAmb>'
		cXml += '				<localAmb>' 	+ cLocalAmb			+ '</localAmb>'
		If cLocalAmb == "1" .Or. cLocalAmb == "3"
			cXml += '			<tpInsc>'   	+ cTpAmb			+ '</tpInsc>'
			cXml += '			<nrInsc>'   	+ cInscAmb			+ '</nrInsc>'
		ElseIf cLocalAmb == "2"
			cXml += '			<codLotacao>'  	+ cCodLot			+ '</codLotacao>'
		EndIf
		cXml += '			</dadosAmbiente>'
	End

	If !Empty(cAnoMesFim) .And. cOper == "4"
		cXml += '			<novaValidade>'
		cXml += '				<iniValid>' 	+ cAnoMesFim		+ '</iniValid>'
		cXml += '				<fimValid>'		+ ""				+ '</fimValid>'
		cXml += '			</novaValidade>'
	EndIf

	If cOper == "3"
		cXml += '		</inclusao>'
	ElseIf cOper == "4"
		cXml += '		</alteracao>'
	Else
		cXml += '		</exclusao>'
	Endif
	cXml += '		</infoAmbiente>'
	cXml += '	</evtTabAmbiente>'
	cXml += '</eSocial>'

Return cXml
//---------------------------------------------------------------------
/*/{Protheus.doc} fInconsis
Carrega Inconsist�ncias (Se houver)

@return lRet L�gico Retorna verdadeiro caso haja alguma inconsistencia

@sample fInconsis( 'TNE' , '01' , '000001' , .F. )

@param cAliasTNE Caracter Valor do Alias utilizado para a Tabela TNE
@param cFilEnv Caracter Filial de Envio
@param cCodAmb Caracter C�digo do Ambiente
@param aLogProc Array Array que ir� receber os Log
@param cMsgLog Caracter Variavel que recebera a mensagem de Log

@author Jackson Machado
@since 28/01/2015
/*/
//---------------------------------------------------------------------
Static Function fInconsis( cAliasTNE , cFilEnv , cCodAmb , aLogProc , cMsgLog , lCadastro , nOpcX , oModel , cAnoMes )

	Local lRet 			:= .F.
	Local lVldCodAmb	:= .F.
	Local cTpEmp		:= ""
	Local cInscEmp		:= ""
	Local cTpAux		:= ""
	Local cInscAux		:= ""
	Local aEstab		:= {}
	Local cStrAmb		:= STR0004 + ": " + AllTrim( cCodAmb ) //"Ambiente: 000000"
	Local lCompCTT		:= Empty( xFilial( "CTT" , IIf( lCadastro , cFilAnt , ( cAliasTNE )->TNE_FILIAL ) ) )
	Local cCCusto		:= ""
	Local dDtEsoc		:= SuperGetMv( "MV_NG2DTES" , .F. , SToD( "20190701" ) )

	Default cAliasTNE 	:= "TNE"
	Default cMsgLog		:= ""
	Default nOpcX		:= 3
	Default lCadastro	:= .F.
	Default aLogProc  	:= {}

	//Busca as informa��es do Estabelecimento
	aEstab := MDTGetEst( cCCusto , cFilEnv )

	cTpEmp		:= aEstab[ 1 , 1 ]
	cInscEmp	:= aEstab[ 1 , 2 ]

	//Busca as informa��es da filial do Ambiente
	aEstab := MDTGetEst( "" , IIf( lCadastro , cFilAnt , ( cAliasTNE )->TNE_FILIAL ) )

	cTpAux		:= aEstab[ 1 , 1 ]
	cInscAux	:= aEstab[ 1 , 2 ]

	If lCadastro
		cDescAux	:= oModel:GetValue( 'TNEMASTER', 'TNE_MEMODS' 	)
		cLocalAmb	:= oModel:GetValue( 'TNEMASTER', 'TNE_LOCAMB'	)
		cTpAmb		:= If( cLocalAmb == "1" , cTpAux	, oModel:GetValue( 'TNEMASTER', 'TNE_TPINS' ) )
		cInscAmb	:= If( cLocalAmb == "1" , cInscAux	, oModel:GetValue( 'TNEMASTER', 'TNE_NRINS' ) )
		cCodLot		:= If( lCompCTT , "" , xFilial( "CTT" , cFilAnt ) ) + oModel:GetValue( 'TNEMASTER', 'TNE_CODLOT' )
		cCCusto		:= oModel:GetValue( 'TNEMASTER', 'TNE_CODLOT' )
	Else
		cDescAux	:= Posicione( "TNE" , 1 , xFilial( "TNE", ( cAliasTNE )->TNE_FILIAL ) + cCodAmb , "TNE_MEMODS" )
		cLocalAmb	:= ( cAliasTNE )->TNE_LOCAMB
		cTpAmb		:= If( cLocalAmb == "1" , cTpAux	, ( cAliasTNE )->TNE_TPINS )
		cInscAmb	:= If( cLocalAmb == "1" , cInscAux	, ( cAliasTNE )->TNE_NRINS )
		cCodLot		:= If( lCompCTT , "" , xFilial( "CTT" , ( cAliasTNE )->TNE_FILIAL ) ) + ( cAliasTNE )->TNE_CODLOT
		cCCusto		:= ( cAliasTNE )->TNE_CODLOT
	EndIf

	//Tipo de Inscri��o e Inscri��o da empresa
	If Empty( cTpEmp )
		aAdd( aLogProc , cStrAmb + " / " + STR0017 + ": " + STR0022 ) //"Ambiente: 000000 / Tipo Inscri��o Lota��o: Em Branco"
		aAdd( aLogProc , '' )
		cMsgLog += " - " + STR0017 + CRLF //"Tipo Inscri��o Lota��o"
		lRet := .T.
	ElseIf !( cTpEmp $ "1/2" )
		aAdd( aLogProc , cStrAmb + " / " + STR0017 + ": " + cTpEmp ) //"Ambiente: 000000 / Tipo Inscri��o Lota��o: 1"
		aAdd( aLogProc , STR0023 + STR0025 ) //"Valida��o: Deve ser igual a 1 (CNPJ) ou 2 (CPF)"
		aAdd( aLogProc , '' )
		cMsgLog += " - " + STR0017 + CRLF //"Tipo Inscri��o Lota��o"
		lRet := .T.
	EndIf

	cDtEsoc := DToS( dDtEsoc )
	cDtEsoc := SubStr( cDtEsoc , 1 , 6 )

	If Empty( cAnoMes )
		aAdd( aLogProc , cStrAmb + " / " + STR0042 + ": " + STR0022 ) //"Ambiente: 000000 / "Data Validade Inicial": Em Branco"
		aAdd( aLogProc , '' )
		cMsgLog += " - " + STR0042 + CRLF //"Data Validade Inicial"
		lRet := .T.
	ElseIf cAnoMes < cDtEsoc //Se for menor que a data in�cio de obrigatoriedade dos envios do SESMT ao eSocial
		aAdd( aLogProc , cStrAmb + " / " + STR0042 + ": " + SubStr(cAnoMes,5,2) + "/" + SubStr(cAnoMes,1,4) ) //"Ambiente: 000000 / "Data Validade Inicial": 06/2019"
		aAdd( aLogProc , STR0023 + STR0043 + " (" + SubStr(cDtEsoc,5,2) + "/" + SubStr(cDtEsoc,1,4) + ")" ) //"A data de validade do Ambiente deve ser maior que a data de in�cio das obrigatoriedades do eSocial!"
		aAdd( aLogProc , '' )
		cMsgLog += " - " + STR0042 + CRLF //"Data Validade Inicial"
		lRet := .T.
	EndIf

	If Empty( cInscEmp )
		aAdd( aLogProc , cStrAmb + " / " + STR0018 + ": " + STR0022 ) //"Ambiente: 000000 / Inscri��o Lota��o: Em Branco"
		aAdd( aLogProc , '' )
		cMsgLog += " - " + STR0018 + CRLF //"Inscri��o Lota��o"
		lRet := .T.
	Else
		Help := .T.
		If cTpEmp == "1"
			If !CGC( cInscEmp )
				aAdd( aLogProc , cStrAmb + " / " + STR0018 + ": " + cInscEmp ) //"Ambiente: 000000 / Inscri��o Lota��o: 12345678901234"
				aAdd( aLogProc , STR0023 + STR0024 ) //"Valida��o: Deve ser um n�mero de CNPJ ou CPF v�lido"
				aAdd( aLogProc , '' )
				cMsgLog += " - " + STR0018 + CRLF //"Inscri��o Lota��o"
				lRet := .T.
			EndIf
		ElseIf cTpEmp == "2"
			If !CHKCPF( cInscEmp )
				aAdd( aLogProc , cStrAmb + " / " + STR0018 + ": " + cInscEmp ) //"Ambiente: 000000 / Inscri��o Lota��o: 12345678901234"
				aAdd( aLogProc , STR0023 + STR0024 ) //"Valida��o: Deve ser um n�mero de CNPJ ou CPF v�lido"
				aAdd( aLogProc , '' )
				cMsgLog += " - " + STR0018 + CRLF //"Inscri��o Lota��o"
				lRet := .T.
			EndIf
		EndIf
		Help := .F.
	EndIf

	If Empty( cCodAmb )
		aAdd( aLogProc , cStrAmb + " / " + STR0013 + ": " + STR0022 ) //"Ambiente: 000000 / C�digo do Ambiente: Em Branco"
		aAdd( aLogProc , '' )
		cMsgLog += " - " + STR0013 + CRLF //"C�digo do Ambiente"
		lRet := .T.
	ElseIf UPPER( SubStr( cCodAmb , 1 , 7) ) == "ESOCIA"
		aAdd( aLogProc , cStrAmb + " / " + STR0013 + ": " + cCodAmb ) //"Ambiente: 000000 / C�digo do Ambiente: Em Branco"
		aAdd( aLogProc , STR0023 + STR0026 ) //"Ambiente: 000000 / Valida��o: O c�digo atribu�do n�o pode conter a express�o 'eSocial' nas 7(sete) primeiras posi��es"
		aAdd( aLogProc , '' )
		cMsgLog += " - " + STR0013 + CRLF //"C�digo do Ambiente"
		lRet := .T.
	EndIf

	If lCadastro
		lVldCodAmb := Empty(oModel:GetValue( 'TNEMASTER', 'TNE_NOME' ))
	ElseIf Type( "cAmbOldTaf" ) == "C" .And. !Empty( cAmbOldTaf )
		lVldCodAmb := .F.
	ElseIf !IsInCallStack( "MDTA165" )
		lVldCodAmb := Empty( ( cAliasTNE )->TNE_NOME )
	EndIf

	If lVldCodAmb
		aAdd( aLogProc , cStrAmb + " / " + STR0015 + ": " + STR0022 ) //"Ambiente: 000000 / Nome do Ambiente: Em Branco"
		aAdd( aLogProc , '' )
		cMsgLog += " - " + STR0015 + CRLF //"Nome do Ambiente"
		lRet := .T.
	EndIf

	If Empty( cDescAux )
		aAdd( aLogProc , cStrAmb + " / " + STR0029 + ": " + STR0022 ) //"Ambiente: 000000 / Descri��o: Em Branco"
		aAdd( aLogProc , '' )
		cMsgLog += " - " + STR0029 + CRLF //"Descri��o"
		lRet := .T.
	EndIf

	If Empty( cLocalAmb )
		aAdd( aLogProc , cStrAmb + " / " + STR0030 + ": " + STR0022 ) //"Ambiente: 000000 / Local do Ambiente: Em Branco"
		aAdd( aLogProc , '' )
		cMsgLog += " - " + STR0030 + CRLF //"Local do Ambiente"
		lRet := .T.
	Else
		If cLocalAmb == "1" //Se o local do ambiente for estabelecimento da empresa
			If Empty( cTpAmb )
				aAdd( aLogProc , cStrAmb + " / " + STR0032 + ": " + STR0022 ) //"Ambiente: 000000 / Tipo de Inscri��o do Ambiente: Em Branco"
				aAdd( aLogProc , STR0023 + STR0050 ) //"O tipo de inscri��o da filial do ambiente deve ser preenchido/configurado"
				aAdd( aLogProc , '' )
				cMsgLog += " - " + STR0032 + CRLF //"Tipo de Inscri��o do Ambiente"
				lRet := .T.
			ElseIf cTpAmb <> "1" //Se o tipo da inscri��o da filial do ambiente for diferente de CNPJ
				aAdd( aLogProc , cStrAmb + " / " + STR0032 + ": " + cTpAmb ) //"Ambiente: 000000 / Tipo de Inscri��o do Ambiente: 2"
				aAdd( aLogProc , STR0023 + STR0049 ) //"O tipo de inscri��o da filial do ambiente deve ser igual a CNPJ"
				aAdd( aLogProc , '' )
				cMsgLog += " - " + STR0032 + CRLF //"Tipo de Inscri��o do Ambiente"
				lRet := .T.
			EndIf
			If Empty( cInscAmb )
				aAdd( aLogProc , cStrAmb + " / " + STR0033 + ": " + STR0022 ) //"Ambiente: 000000 / Inscri��o do Ambiente: Em Branco"
				aAdd( aLogProc , STR0023 + STR0051 ) //"A inscri��o da filial do ambiente deve ser preenchida/configurada"
				aAdd( aLogProc , '' )
				cMsgLog += " - " + STR0033 + CRLF //"Inscri��o do Ambiente"
				lRet := .T.
			EndIf
		ElseIf cLocalAmb == "2"
			If Empty( cCCusto ) //Verifica o Centro de Custo pois a vari�vel cCodLot pode receber a filial e n�o estar vazia
				aAdd( aLogProc , cStrAmb + " / " + STR0031 + ": " + STR0022 ) //"Ambiente: 000000 / C�digo de Lota��o: Em Branco"
				aAdd( aLogProc , '' )
				cMsgLog += " - " + STR0031 + CRLF //"C�digo de Lota��o"
				lRet := .T.
			ElseIf !( Posicione( "CTT" , 1 , xFilial("CTT") + cCCusto , "CTT_TPLOT" ) $ "03/04/05/06/07/08/09" )
				aAdd( aLogProc , cStrAmb + " / " + STR0031 + ": " + cCCusto ) //"Ambiente: 000000 / C�digo de Lota��o: 10.001"
				aAdd( aLogProc , STR0023 + STR0047 ) //"O tipo da lota��o escolhida deve ser prenchida como 03, 04, 05, 06, 07, 08 ou 09"
				aAdd( aLogProc , '' )
				cMsgLog += " - " + STR0031 + CRLF //"C�digo de Lota��o"
				lRet := .T.
			EndIf
		ElseIf cLocalAmb == "3"
			If Empty( cTpAmb )
				aAdd( aLogProc , cStrAmb + " / " + STR0032 + ": " + STR0022 ) //"Ambiente: 000000 / Tipo de Inscri��o do Ambiente: Em Branco"
				aAdd( aLogProc , '' )
				cMsgLog += " - " + STR0032 + CRLF //"Tipo de Inscri��o do Ambiente"
				lRet := .T.
			EndIf
			If Empty( cInscAmb )
				aAdd( aLogProc , cStrAmb + " / " + STR0033 + ": " + STR0022 ) //"Ambiente: 000000 / Inscri��o do Ambiente: Em Branco"
				aAdd( aLogProc , '' )
				cMsgLog += " - " + STR0033 + CRLF //"Inscri��o do Ambiente"
				lRet := .T.
			EndIf
		EndIf
	EndIf

Return lRet
//---------------------------------------------------------------------
/*/{Protheus.doc} MDTM001EXC
Realiza a limpeza dos valores da TNE_DTVINI atrav�s do GPEM023

@return Nulo, Sempre nulo

@sample MDTM001EXC( 'D MG 01' )

@param cFilExc, Caracter, Filial de Exclus�o

@author Jackson Machado
@since 20/05/2019
/*/
//---------------------------------------------------------------------
Function MDTM001EXC( cFilExc )

	Local nCont		:= 0
	Local aFiliais	:= {}

	//Busca filiais relacionadas a filial de envio
	aFiliais := MDTFilRel( cFilExc )

	For nCont := 1 To Len( aFiliais )
		// Caso seja executada a remo��o via GPEM023, limpa as datas
		dbSelectArea( "TNE" )
		dbSetOrder( 1 )
		If dbSeek( xFilial( "TNE" , aFiliais[ nCont , 1 ] ) )
			While TNE->( !EoF() ) .And. TNE->TNE_FILIAL == xFilial( "TNE", aFiliais[ nCont , 1 ] )
				RecLock( "TNE", .F. )
				TNE->TNE_DTVINI := SToD( Space( 8 ) )
				TNE->( MsUnLock() )
				TNE->( dbSkip() )
			End
		EndIf
	Next nCont

Return