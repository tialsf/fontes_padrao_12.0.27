#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "MDTM002.CH"

STATIC aEfd	 := If( cPaisLoc == "BRA" , If( Findfunction( "fEFDSocial" ) , fEFDSocial() , { .F. , .F. , .F. } ) , { .F. , .F. , .F. } )
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//  _______           _______  _       _________ _______             _______  _______  _______  __    _______  ------
// (  ____ \|\     /|(  ____ \( (    /|\__   __/(  ___  )           (  ____ \/ ___   )/ ___   )/  \  (  __   ) ------
// | (    \/| )   ( || (    \/|  \  ( |   ) (   | (   ) |           | (    \/\/   )  |\/   )  |\/) ) | (  )  | ------
// | (__    | |   | || (__    |   \ | |   | |   | |   | |   _____   | (_____     /   )    /   )  | | | | /   | ------
// |  __)   ( (   ) )|  __)   | (\ \) |   | |   | |   | |  (_____)  (_____  )  _/   /   _/   /   | | | (/ /) | ------
// | (       \ \_/ / | (      | | \   |   | |   | |   | |                 ) | /   _/   /   _/    | | |   / | | ------
// | (____/\  \   /  | (____/\| )  \  |   | |   | (___) |           /\____) |(   (__/\(   (__/\__) (_|  (__) | ------
// (_______/   \_/   (_______/|/    )_)   )_(   (_______)           \_______)\_______/\_______/\____/(_______) ------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MDTM002
Rotina de Envio de Eventos - Comunica��o de Acidente de Trabalho (S-2210)
Realiza o envio das informa��es de CAT para o TAF

@return xRet - Caracter/L�gico - Se for gera��o de Xml retorna o cXml, sen�o retorna .T. ou .F. de acordo com as condi��es

@sample MDTM002( .T. , cFilAnt , 3 , TNC->TNC_FILIAL + TNC->TNC_ACIDEN , .F. , .F. , .T. , oModel , .T. )

@param lMemory		- L�gico	- Indica se o sistema carregar� as vari�veis de mem�ria
@param cFilEnv		- Caracter	- Indica a filial de envio das informa��es
@param nOpcX		- Num�rico 	- Indica o tipo de opera��o do cadastro (3-Incluir;4-Alterar;5-Excluir)
@param cChkCAT		- Caracter	- Indica a express�o caracter que ser� usada para buscar e carregar as vari�veis de mem�ria
@param lDiagnostico	- L�gico 	- Indica se � chamado pelo MDTA155
@param lAtestado	- L�gico 	- Indica se � chamado pelo MDTA685
@param lModel		- L�gico	- Indica se os valores ser�o pegos do Model do MDTA640
@param oModelTNC	- Objeto	- Indica o modelo que � usado para fazer a manipula��o dos registros caso seja chamado pelo MDTA640A
@param lXml			- L�gico	- Indica se a fun��o retornar� o Xml ou se enviar� para o TAF

@author Luis Fellipy Bett
@since 10/07/2018
/*/
//-------------------------------------------------------------------------------------------------------------------
Function MDTM002( lMemory , cFilEnv , nOpcX , cChkCAT , lDiagnostico , lAtestado , lModel , oModelTNC , lXml , lInconsis )

	Local aArea		 	:= GetArea()
	Local lNT1219		:= SuperGetMv( "MV_TAFAMBE", .F., "2" ) $ "2/3" //Ambiente Produ��o Restrita
	Local lMiddleware	:= IIf( cPaisLoc == 'BRA' .And. Findfunction( "fVerMW" ), fVerMW(), .F. )
	Local aAreaTNC	 	:= TNC->( GetArea() )
	Local cXml			:= ""
	Local cMsgLog		:= ""
	Local cChvBus		:= ""
	Local aErros 	 	:= {}
	Local aLogProc		:= {}
	Local aDados		:= {}
	Local aEstab		:= {}
	Local nCont			:= 0
	Local nCont2		:= 0
	Local nLenGrid		:= 0
	Local xRet			:= .T.
	Local oCausa
	Local oParte

	//Variaveis de adequa��o das informa��es.
	Private dDtEsoc				:= SuperGetMv( "MV_NG2DTES" , .F. , SToD( "20190701" ) )
	Private cTpInsc				:= ""
	Private cNrInsc				:= ""
	Private cInscSM0			:= ""
	Private cNumMat				:= "" // Matr�cula do Funcion�rio
	Private cCpfTrab			:= "" // CPF do Funcion�rio
	Private cNisTrab			:= "" // NIS do Funcion�rio
	Private cMatricula			:= "" // Se refere ao C�digo �nico do Funcion�rio
	Private cCodCateg			:= "" // Categoria do Funcion�rio
	Private cCCusto				:= "" // Centro de Custo do Funcion�rio
	Private cTpAcid				:= ""
	Private cIndCatObito		:= ""
	Private cIndComunPolicia	:= ""
	Private cCodSitGeradora		:= "" //C�digo do Tipo de Acidente
	Private cTpLocal			:= ""
	Private cDscLocal			:= ""
	Private cDscLograd			:= ""
	Private cDtAtendimento 		:= SToD( Space( 8 ) ) //Data de Atendimento
	Private cHrAtendimento 		:= "" //Hora de Atendimento
	Private cIndInternacao		:= ""
	Private cDurTrat 			:= "" //Quantidade de Dura��o do Tratamento
	Private cCodMedico			:= "" //C�digo do M�dico
	Private cIndAfast 			:= "" //Indica Afastamento - 1 = Sim / 2 = N�o
	Private cDscLesao			:= "" //C�digo de Descri��o da Les�o
	Private cDscCompLesao 		:= "" //Complemento da Descri��o da Les�o
	Private cDiagProvavel 		:= "" //Diagn�stico
	Private cCodCID				:= "" //CID do Acidente
	Private cObservacao			:= "" //Observa��o geral
	Private cNmEmit 			:= ""
	Private cIdeOC 				:= ""
	Private cNrOC 				:= ""
	Private cUfOC 				:= ""
	Private cNumTrab			:= ""
	Private cNrRecCatOrig		:= ""
	Private dDataCat			:= SToD( "" )
	Private dDtAdm				:= SToD( "" )
	Private cIndEmit			:= ""
	Private cTpInscAci			:= ""
	Private cCodPai				:= ""
	Private cCodMunic			:= ""

	Private aCausa := {}
	Private aParte := {}

	Private lDel		:= .F. //Determina que est� ocorrendo a exclus�o do Diagn�stico

	Default lMemory		 := .F.
	Default lDiagnostico := .F.
	Default lAtestado	 := .F.
	Default lModel 		 := .F.
	Default lXml		 := .F.
	Default lInconsis	 := .F.
	Default nOpcX		 := 3

	//Trata mensagem inicial de inconsist�ncias
	aAdd( aLogProc , STR0001 ) //"Inconsist�ncias da CAT"
	aAdd( aLogProc , STR0034 ) //"Os campos abaixo est�o vazios/zerados ou possuem inconsist�ncia com rela��o ao formato padr�o do eSocial: "
	aAdd( aLogProc , "" )
	aAdd( aLogProc , "" )

	//Verifica se TAF est� preparado
	aRet:= TafExisEsc( "S2210" )
	If !aRet[ 1 ] .Or. ( aRet[ 2 ] <> "2.4" .And. aRet[ 2 ] <> "2.4.02" .And. aRet[ 2 ] <> "2.5" )
		Help(' ',1,STR0002,,STR0003,2,0,,,,,,{STR0091}) //"Aten��o" ## "O ambiente n�o possui integra��o com o m�dulo do TAF e/ou a vers�o do TAF est� desatualizada!" ## "Favor verificar!"
		xRet := .F.
	ElseIf !( NGCADICBASE( "TMA_ESOC", "A", "TMA", .F. ) )
		Help(' ',1,STR0002,,STR0005,2,0,,,,,,{STR0004}) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial"
		xRet := .F.
	ElseIf !( NGCADICBASE( "TNC_TPLOGR", "A", "TNC", .F. ) )
		Help(' ',1,STR0002,,STR0005,2,0,,,,,,{STR0004}) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial"
		xRet := .F.
	Else
		//Caso n�o tenha a Filial n�o tem como fazer a Integra��o
		If !Empty( cFilEnv )
			//Inicializa regua de processamento
			ProcRegua( 1 )

			If lMemory //Alimenta as vari�veis de mem�ria para utiliza��o
				dbSelectArea( "TNC" )
				dbSetOrder( 1 )
				dbSeek( cChkCAT )
				RegToMemory( "TNC" , .F. ) //Busca os valores de Mem�ria.
			EndIf

			cNumMat			:= Posicione( "TM0" , 1 , xFilial( "TM0" ) + M->TNC_NUMFIC	, "TM0_MAT"		)
			cCpfTrab		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat		, "RA_CIC"		)
			cMatricula		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat		, "RA_CODUNIC"	)
			cNisTrab		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat		, "RA_PIS"		)
			cCodCateg		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat		, "RA_CATEFD"	)
			cCCusto			:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat		, "RA_CC"		)
			dDtAdm			:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat		, "RA_ADMISSA"	)
			cDscLesao		:= Posicione( "TOJ" , 1 , xFilial( "TOJ" ) + M->TNC_CODLES	, "TOJ_ESOC"	)
			cDscCompLesao	:= M->TNC_DESLES
			If lNT1219
				If !Empty( Posicione( "TNG" , 1 , xFilial( "TNG" ) + M->TNC_TIPACI	, "TNG_ESOC" ) )
					cCodSitGeradora	:= Posicione( "TNG" , 1 , xFilial( "TNG" ) + M->TNC_TIPACI	, "TNG_ESOC" )
				ElseIf !Empty( Posicione( "TNG" , 1 , xFilial( "TNG" ) + M->TNC_TIPACI	, "TNG_ESOC1" ) )
					cCodSitGeradora	:= Posicione( "TNG" , 1 , xFilial( "TNG" ) + M->TNC_TIPACI	, "TNG_ESOC1" )
				EndIf
			Else
				cCodSitGeradora := Posicione( "TNG" , 1 , xFilial( "TNG" ) + M->TNC_TIPACI	, "TNG_ESOC" )
			EndIf

			//Busca o n�mero de inscri��o apenas da SM0 para valida��o
			cInscSM0 := MDTGetEst( '' , cFilEnv )[ 1 , 2 ]

			//Busca as informa��es do Estabelecimento
			aEstab := MDTGetEst( cCCusto , cFilEnv )

			cTpInsc	:= aEstab[ 1 , 1 ]
			cNrInsc	:= aEstab[ 1 , 2 ]

			//-------------------------------------
			// Tipo do Acidente conforme tabela 24
			//-------------------------------------
			cTpAcid	:= M->TNC_TPACID

			//-------Indica Localiza��o-------
			// 1 - Estab da Empresa;    - 1 - Estabelecimento do empregador no Brasil;
			// 2 - Onde Presta Servi�o;	- 3 - Estabelecimento de terceiros onde o empregador presta servi�os;
			// 3 - Via Publica;			- 4 - Via p�blica;
			// 4 - Area Rural; 			- 5 - �rea rural;
			// 5 - Embarca��o			- 6 - Embarca��o;
			// 6 - Exterior				- 2 - Estabelecimento do empregador no Exterior;
			// 9 - Outros;				- 9 - Outros.
			//--------------------------------
			cTpLocal := M->TNC_INDLOC

			Do Case
				Case cTpLocal == "2" ; cTpLocal := "3"
				Case cTpLocal == "3" ; cTpLocal := "4"
				Case cTpLocal == "4" ; cTpLocal := "5"
				Case cTpLocal == "5" ; cTpLocal := "6"
				Case cTpLocal == "6" ; cTpLocal := "2"
			End Case

			//-------Emitente do Acidente -------
			//	1 - Empregador           1 - EMPREGADOR
			//	2 - Sindicato            7 - ENTIDADE SINDICAL COMPETENTE
			//	3 - M�dico               8 - M�DICO ASSISTENTE
			//	4 - Segurado             6 - DEPENDENTE DO EMPREGADO
			//	5 - Autoridade Publica   9 - AUTORIDADE P�BLICA
			//-----------------------------------
			cIndEmit := M->TNC_EMITEN

			Do Case
				Case cIndEmit == "2" ; cIndEmit := "7"
				Case cIndEmit == "3" ; cIndEmit := "8"
				Case cIndEmit == "4" ; cIndEmit := "6"
				Case cIndEmit == "5" ; cIndEmit := "9"
			End Case

			//------- Tipos de Inscri��o -------
			//	1 - CNPJ			1 - CNPJ
			//	2 - CAEPF			3 - CAEPF
			//	3 - CNO				4 - CNO
			//-----------------------------------
			cTpInscAci := M->TNC_TPINS

			Do Case
				Case cTpInscAci == "2" ; cTpInscAci := "3"
				Case cTpInscAci == "3" ; cTpInscAci := "4"
			End Case

			//Pega o c�digo esperado pelo eSocial
			cCodPai := Posicione( "C08" , 3 , xFilial("C08") + M->TNC_CODPAI , "C08_PAISSX" )

			//Caso for envio pelo Middleware, comp�e o c�digo do estado junto com o da cidade
			If lMiddleware
				Do Case
					Case M->TNC_ESTACI = "AC" ; cCodMunic := "12"
					Case M->TNC_ESTACI = "AL" ; cCodMunic := "27"
					Case M->TNC_ESTACI = "AP" ; cCodMunic := "16"
					Case M->TNC_ESTACI = "AM" ; cCodMunic := "13"
					Case M->TNC_ESTACI = "BA" ; cCodMunic := "29"
					Case M->TNC_ESTACI = "CE" ; cCodMunic := "23"
					Case M->TNC_ESTACI = "DF" ; cCodMunic := "53"
					Case M->TNC_ESTACI = "ES" ; cCodMunic := "32"
					Case M->TNC_ESTACI = "GO" ; cCodMunic := "52"
					Case M->TNC_ESTACI = "MA" ; cCodMunic := "21"
					Case M->TNC_ESTACI = "MT" ; cCodMunic := "51"
					Case M->TNC_ESTACI = "MS" ; cCodMunic := "50"
					Case M->TNC_ESTACI = "MG" ; cCodMunic := "31"
					Case M->TNC_ESTACI = "PA" ; cCodMunic := "15"
					Case M->TNC_ESTACI = "PB" ; cCodMunic := "25"
					Case M->TNC_ESTACI = "PR" ; cCodMunic := "41"
					Case M->TNC_ESTACI = "PE" ; cCodMunic := "26"
					Case M->TNC_ESTACI = "PI" ; cCodMunic := "22"
					Case M->TNC_ESTACI = "RN" ; cCodMunic := "24"
					Case M->TNC_ESTACI = "RS" ; cCodMunic := "43"
					Case M->TNC_ESTACI = "RJ" ; cCodMunic := "33"
					Case M->TNC_ESTACI = "RO" ; cCodMunic := "11"
					Case M->TNC_ESTACI = "RR" ; cCodMunic := "14"
					Case M->TNC_ESTACI = "SC" ; cCodMunic := "42"
					Case M->TNC_ESTACI = "SP" ; cCodMunic := "35"
					Case M->TNC_ESTACI = "SE" ; cCodMunic := "28"
					Case M->TNC_ESTACI = "TO" ; cCodMunic := "17"
				End Case
				cCodMunic += M->TNC_CODCID
			Else
				cCodMunic := M->TNC_CODCID
			EndIf

			//Altera��o de Valores para enviodo do E-Social.
			cIndCatObito		:= If( M->TNC_MORTE  == "1" , "S" , "N" )
			cIndComunPolicia	:= If( M->TNC_POLICI == "1" , "S" , "N" )
			cIndInternacao		:= If( M->TNC_INTERN == "1" , "S" , "N"	)

			If lModel //Caso seja chamado pela rotina MDTA640 - Acidentes (MVC)
				//----------------------------------------------------------------
				// Percorre a Grid para buscar todas as Causa de Acidente
				//----------------------------------------------------------------
				oCausa		:= oModelTNC:GetModel( 'TNMCAUSA' )
				nLenGrid	:= oCausa:Length()

				For nCont := 1 To nLenGrid
					oCausa:GoLine(nCont) //Posiciona na linha desejada.
					If !( oCausa:IsDeleted() ) //Verifica se registro n�o est� deletado.
						aAdd( aCausa , { Posicione( "TNH" , 1 , xFilial( "TNH" ) + oCausa:GetValue( "TYE_CAUSA" ) , "TNH_ESOC" ) } )
					EndIf
				Next nCont

				//----------------------------------------------------------------
				// Percorre a Grid para buscar todas as Partes Atingidas
				//----------------------------------------------------------------
				oParte		:= oModelTNC:GetModel( 'TNMPARTE' )
				nLenGrid	:= oParte:Length()

				For nCont := 1 To nLenGrid
					oParte:GoLine(nCont) //Posiciona na linha desejada.
					If !( oParte:IsDeleted() ) //Verifica se registro n�o est� deletado.
						aAdd( aParte , { Posicione( "TOI" , 1 , xFilial( "TOI" ) + oParte:GetValue( "TYF_CODPAR" ) , "TOI_ESOC") ,;
										oParte:GetValue( "TYF_LATERA" ) } )
					EndIf
				Next nCont
			Else
				dbSelectArea( "TYE" )
				dbSetOrder( 1 )
				If dbSeek( xFilial( "TYE" ) + M->TNC_ACIDEN )
					While !Eof() .And. TYE->TYE_FILIAL == xFilial( "TYE" ) .And. TYE->TYE_ACIDEN == M->TNC_ACIDEN
						aAdd( aCausa , { Posicione( "TNH" , 1 , xFilial( "TNH" ) + TYE->TYE_CAUSA , "TNH_ESOC" ) } )
						TYE->( dbSkip() )
					End
				EndIf

				dbSelectArea( "TYF" )
				dbSetOrder( 1 )
				If dbSeek( xFilial( "TYF" ) + M->TNC_ACIDEN )
					While !Eof() .And. TYF->TYF_FILIAL == xFilial( "TYF" ) .And. TYF->TYF_ACIDEN == M->TNC_ACIDEN
						aAdd( aParte , { Posicione( "TOI" , 1 , xFilial( "TOI" ) + TYF->TYF_CODPAR , "TOI_ESOC" ) , TYF->TYF_LATERA } )
					TYF->( dbSkip() )
					End
				EndIf
			EndIf

			If lDiagnostico //Caso esteja chamando o MDTA155 - Diagn�stico M�dico.

				If nOpcX == 5
					cDtAtendimento	:= StoD( Space( 8 ) )
					cHrAtendimento	:= ""
					cObservacao   	:= ""
					cDiagProvavel 	:= ""
					cIdeOC			:= ""
					cNrOC			:= ""
					cNmEmit			:= ""
					cUfOC			:= ""
					cIndAfast		:= ""
					cDurTrat		:= ""
					cCodCID			:= ""
					nOpcX 			:= 4 //Altera��o
					lDel			:= .T.
				Else
					cDtAtendimento	:= M->TMT_DTATEN
					cHrAtendimento	:= M->TMT_HRATEN
					cCodMedico		:= M->TMT_CODUSU
					cObservacao		:= M->TMT_OUTROS
					cDiagProvavel	:= M->TMT_DIAGNO
					cIdeOC			:= Posicione( "TMK" , 1 , xFilial("TMK") + cCodMedico , "TMK_ENTCLA" )
					cNrOC			:= Posicione( "TMK" , 1 , xFilial("TMK") + cCodMedico , "TMK_NUMENT" )
					cNmEmit			:= Posicione( "TMK" , 1 , xFilial("TMK") + cCodMedico , "TMK_NOMUSU" )
					cUfOC			:= Posicione( "TMK" , 1 , xFilial("TMK") + cCodMedico , "TMK_UF"     )
					cIndAfast		:= If( M->TMT_QTAFAS > 0 , "S" , "N" )
					cDurTrat 		:= cValToChar( M->TMT_QTAFAS )
					cCodCID			:= Posicione( "TNC" , 1 , xFilial("TNC") + M->TMT_ACIDEN , "TNC_CID"	)
					cCodCID			:= If( Empty( cCodCID ) , M->TMT_CID , cCodCID )
					cCodCID			:= SubStr( cCodCID , 1 , 5 )
				EndIf

			ElseIf lAtestado //Caso esteja chamando do MDTA685 - Atestado M�dico

				If nOpcX == 5
					cDtAtendimento	:= StoD( Space( 8 ) )
					cHrAtendimento	:= ""
					cCodCID			:= ""
					cIndAfast		:= ""
					cObservacao		:= ""
					cDiagProvavel	:= ""
					cDurTrat		:= ""
					cIdeOC			:= ""
					cNrOC			:= ""
					cNmEmit			:= ""
					cUfOC			:= ""
					nOpcX 			:= 4 //Altera��o
					lDel			:= .T.
				Else
					cDtAtendimento	:= M->TNY_DTCONS
					cHrAtendimento	:= M->TNY_HRCONS
					cCodMedico		:= M->TNY_EMITEN
					cDurTrat		:= cValToChar( M->TNY_QTDTRA )
					cIndAfast		:= If( !Empty( M->TNY_CODAFA ) , "S" , "N" )
					cObservacao		:= Posicione( "TMT" , 7 , xFilial("TMT") + M->TNY_ACIDEN + M->TNY_NUMFIC	, "TMT_OUTROS"	)
					cDiagProvavel	:= Posicione( "TMT" , 1 , xFilial("TMT") + M->TNY_ACIDEN + M->TNY_NUMFIC	, "TMT_DIAGNO"	)
					cIdeOC			:= Posicione( "TNP" , 1 , xFilial("TNP") + cCodMedico						, "TNP_ENTCLA"	)
					cNrOC			:= Posicione( "TNP" , 1 , xFilial("TNP") + cCodMedico 						, "TNP_NUMENT"	)
					cNmEmit			:= Posicione( "TNP" , 1 , xFilial("TNP") + cCodMedico 						, "TNP_NOME"	)
					cUfOC			:= Posicione( "TNP" , 1 , xFilial("TNP") + cCodMedico 						, "TNP_UF"		)
					cCodCID			:= Posicione( "TNC" , 1 , xFilial("TNC") + M->TNY_ACIDEN 					, "TNC_CID"		)
					cCodCID			:= If( Empty( cCodCID ) , M->TNY_CID , cCodCID )
					cCodCID			:= SubStr( cCodCID , 1 , 5 )
				EndIf
			EndIf

			If !lAtestado .And. !lDiagnostico
				//------Indica Afastamento----
				// 1 - Sim;     - 1 - Sim
				// 2 - N�o;     - 2 - N�o
				// 3 - Branco;  - 2 - N�o;
				//----------------------------
				cIndAfast := If( M->TNC_AFASTA == "1" , "S" , "N" )

				Do Case
					Case Empty( cIndAfast ) ; cIndAfast := "N"
				End Case

				cCodCID	:= M->TNC_CID
				cCodCID	:= SubStr( cCodCID , 1 , 5 )

			EndIf

			//Tratativas dos dados a ser importados para o m�dulo do TAF.
			If nOpcX <> 3 .And. !lAtestado .And. !lDiagnostico

				dbSelectArea( "TMT" )
				dbSetOrder( 7 )
				If dbSeek( xFilial( "TMT" ) + M->TNC_ACIDEN )
					dbSelectArea( "TMK" )
					dbSetOrder( 1 )
					dbSeek( xFilial( "TMK" ) + TMT->TMT_CODUSU )
					cNmEmit	:= TMK->TMK_NOMUSU
					cIdeOC	:= TMK->TMK_ENTCLA
					cNrOC	:= TMK->TMK_NUMENT
					cUfOC	:= TMK->TMK_UF
				Else
					If TNY->( FieldPos( "TNY_ACIDEN" ) ) > 0
						dbSelectArea( "TNY" )
						dbSetOrder( 5 )
						If dbSeek( xFilial( "TNY" ) + M->TNC_ACIDEN )
							dbSelectArea( "TNP" )
							dbSetOrder( 1 )
							dbSeek( xFilial( "TNP" ) + TNY->TNY_EMITEN )
							cNmEmit	:= TNP->TNP_NOME
							cIdeOC	:= TNP->TNP_ENTCLA
							cNrOC	:= TNP->TNP_NUMENT
							cUfOC	:= TNP->TNP_UF
						Endif
					Endif
				Endif
			EndIf

			//Trata o campo para os valores padr�es do eSocial
			If "CRM" $ cIdeOC
				cIdeOC := "1"
			ElseIf "CRO" $ cIdeOC
				cIdeOC := "2"
			ElseIf "RMS" $ cIdeOC
				cIdeOC := "3"
			EndIf

			//Trata acentua��o
			cDscCompLesao	:= Alltrim( MDTSubTxt( cDscCompLesao ) )
			cDscLocal		:= Alltrim( MDTSubTxt( M->TNC_LOCAL  ) )
			cDscLograd		:= Alltrim( MDTSubTxt( M->TNC_DESLOG ) )
			cDiagProvavel	:= Alltrim( MDTSubTxt( cDiagProvavel ) )
			cObservacao		:= Alltrim( MDTSubTxt( cObservacao 	 ) )
			cNmEmit			:= Alltrim( MDTSubTxt( cNmEmit 		 ) )

			If lInconsis
				fInconsis( @aLogProc , @cMsgLog , lAtestado , lDiagnostico , nOpcX , lDel , cFilEnv )

				If Len( aLogProc ) > 4
					If !IsBlind()
						fMakeLog( { aLogProc } , { STR0011 } , Nil , Nil , "MDTM002" , OemToAnsi( STR0012 ) , "M" , "P" , , .F. ) //"Monitoramento Envio de Eventos - TAF""Log de Ocorrencias - CAT"

						Help(' ',1,STR0002,,STR0049 + "!" + CRLF + cMsgLog,2,0,,,,,,{STR0086}) //"Favor ajustar os valores expostos no relat�rio!"

						xRet := .F.
					Else
						xRet := .F.
						aAdd( aDados , OemToAnsi( STR0004 ) + " " + cCodAmb + " " + OemToAnsi( STR0006 ) + "! " + STR0044 ) //"Ambiente" ## "Falha no envio ao TAF" ## "Para verifica��o favor gerar o relat�rio de inconsist�ncias"
					EndIf
				EndIf
			Else
				cXml:= fCarrCAT( cValToChar( nOpcX ), cFilEnv, cCCusto, lAtestado, lDiagnostico, lXml, lMiddleware )

				If lXml
					fInconsis( @aLogProc , @cMsgLog , lAtestado , lDiagnostico , nOpcX , lDel , cFilEnv )

					If Len( aLogProc ) > 4
						fMakeLog( { aLogProc } , { STR0006 } , Nil , Nil , "MDTM002" , OemToAnsi( STR0007 ) , "M" , "P" , , .F. ) //"Monitoramento Envio de Eventos - TAF" ## "Log de Ocorr�ncias - Ambientes de Trabalho"

						Help(' ',1,STR0002,,STR0087,2,0,,,,,,{STR0086}) //"O Xml possui inconsist�ncias de acordo com o formato padr�o do eSocial" ## "Favor ajustar os valores expostos no relat�rio!"

						xRet := .F.
					Else
						xRet := cXml
					EndIf
				Else
					Begin Transaction

						//Caso o envio seja atrav�s do Middleware
						If lMiddleware

							If MDTVerTSVE( cCodCateg ) //Caso seja Trabalhador Sem V�nculo Estatut�rio
								cChvBus := AllTrim( cCpfTrab ) + AllTrim( cCodCateg ) + DToS( dDtAdm ) + DtoS( M->TNC_DTACID ) + StrTran( M->TNC_HRACID, ":", "" ) + M->TNC_TIPCAT
							Else
								cChvBus := AllTrim( cMatricula ) + DtoS( M->TNC_DTACID ) + StrTran( M->TNC_HRACID, ":", "" ) + M->TNC_TIPCAT
							EndIf

							//Envia o evento ao eSocial atrav�s do Middleware
							MDTEnvMid( cFilEnv, cNumMat, "S2210", cChvBus, cXml, nOpcX )

						Else
							//Enviar como parametro a filial do Protheus na posi��o 1 e o XML na posi��o 2
							aErros := TafPrepInt( cEmpAnt, cFilEnv, cXml, , "1", "S2210" )

							If Len( aErros ) <= 0
								//Carrega array de controle de carga
								aAdd( aDados , OemToAnsi( STR0006 ) + " " + DtoS( M->TNC_DTACID ) + " " + M->TNC_HRACID + " " + cCpfTrab + " " + OemToAnsi( STR0007 ) ) //"CAT""Enviado ao TAF com sucesso."
								Help(' ',1,STR0002,,STR0098,2,0) //"Aten��o"#"Registro enviado ao TAF com sucesso!"
							Else
								If lDiagnostico .Or. lAtestado
									Help(' ',1,STR0002 ,,STR0050 + CRLF + aErros[ 1 ],2,0,,,,,,{STR0088}) //"Para tentativa de reenvio favor manipular o registro!"
									xRet := .F.
								Else
									aAdd( aDados , OemToAnsi( STR0006 ) + " " + DtoS( M->TNC_DTACID ) + " " + M->TNC_HRACID + " " + cCpfTrab + " " + OemToAnsi( STR0008 ) + " " + aErros[ 1 ] ) //"CAT" "Falha no envio ao TAF:"
									Help(' ',1,STR0002 ,,STR0050 + CRLF + aErros[ 1 ],2,0,,,,,,{STR0088}) //"Para tentativa de reenvio favor manipular o registro!"
								Endif
							EndIf

							//Incrementa regua
							IncProc( OemToAnsi( STR0010 ) + " " + DtoS( M->TNC_DTACID ) + " " + M->TNC_HRACID + " " + cCpfTrab ) //"Gerando o registro de:"
						EndIf

					End Transaction
				EndIf
			EndIf

		Else
			Help(' ',1,STR0002,,STR0092,2,0,,,,,,{STR0091}) // Aten��o ## "A informa��o da filial de envio ao eSocial est� vazia!" ## "Favor verificar!"
			xRet := .F.
		EndIf

	EndIf

	RestArea( aAreaTNC )
	RestArea( aArea )

Return xRet
//---------------------------------------------------------------------
/*/{Protheus.doc} fCarrCAT

Carrega os Acidentes

@return cXml Caracter Estrutura XML a ser enviada para o TAF

@sample fCarrCAT( "3" , "D MG 01" , "312" )

@param cOper	- Caracter	- Opera��o a ser realizada
@param cFilEnv	- Caracter	- Filial de Envio
@param cCCusto	- Caracter	- Centro de Custo do Funcion�rio

@author Luis Fellipy Bett
@since 30/08/2018
/*/
//---------------------------------------------------------------------
Static Function fCarrCAT( cOper, cFilEnv, cCCusto, lAtestado, lDiagnostico, lXml, lMiddleware )

	Local cXml		:= ""
	Local cChvBus	:= ""
	Local lTSVE		:= MDTVerTSVE( cCodCateg )
	Local nCont		:= 0

	Default cOper := "3"

	If lTSVE //Caso seja Trabalhador Sem V�nculo Estatut�rio
		cChvBus := AllTrim( cCpfTrab ) + AllTrim( cCodCateg ) + DToS( dDtAdm ) + DToS( M->TNC_DTACID ) + StrTran( M->TNC_HRACID, ":", "" ) + M->TNC_TIPCAT
	Else
		cChvBus := AllTrim( cMatricula ) + DToS( M->TNC_DTACID ) + StrTran( M->TNC_HRACID, ":", "" ) + M->TNC_TIPCAT
	EndIf

	//Cria o cabe�alho do Xml com o ID, informa��es do Evento e Empregador
	MDTGerCabc( @cXml, "S2210", cTpInsc, cNrInsc, lMiddleware, lXml, cOper, cChvBus )

	//TRABALHADOR
	cXml += 		'<ideVinculo>'
	cXml += 			'<cpfTrab>'		+ cCpfTrab + '</cpfTrab>'
	If !( cCodCateg $ "901/903/904" )
		cXml += 		'<nisTrab>'		+ AllTrim( cNisTrab ) + '</nisTrab>'
	EndIf
	If !lTSVE //Se n�o for Trabalhador Sem V�nculo Estatut�rio
		cXml +=			'<matricula>'	+ cMatricula	+ '</matricula>'
	Else
		cXml +=			'<codCateg>'	+ cCodCateg		+ '</codCateg>'
	EndIf
	cXml += 		'</ideVinculo>'

	//COMUNICA��O DE ACIDENTE DE TRABALHO
	cXml += 		'<cat>'
	cXml += 			'<dtAcid>'				+ MDTAjsData( M->TNC_DTACID, lMiddleware )	+ '</dtAcid>'
	cXml += 			'<tpAcid>'				+ cTpAcid								+ '</tpAcid>'
	cXml += 			'<hrAcid>'				+ StrTran( M->TNC_HRACID, ":", "" )		+ '</hrAcid>'
	cXml += 			'<hrsTrabAntesAcid>'	+ StrTran( M->TNC_HRTRAB, ":", "" )		+ '</hrsTrabAntesAcid>'
	cXml += 			'<tpCat>'				+ M->TNC_TIPCAT							+ '</tpCat>'
	cXml += 			'<indCatObito>'			+ cIndCatObito							+ '</indCatObito>'
	If M->TNC_MORTE == "1"
		cXml += 		'<dtObito>'				+ DtoS( M->TNC_DTOBIT )					+ '</dtObito>'
	EndIf
	cXml += 			'<indComunPolicia>'		+ cIndComunPolicia						+ '</indComunPolicia>'
	cXml += 			'<codSitGeradora>'		+ cCodSitGeradora						+ '</codSitGeradora>'
	cXml += 			'<iniciatCAT>'			+ "1" 									+ '</iniciatCAT>'
	If !Empty( M->TNC_DETALH )
		cXml +=			'<obsCAT>'				+ Alltrim( MDTSubTxt( Upper( SubStr( M->TNC_DETALH, 1, 254 ) ) ) ) + '</obsCAT>'
	EndIf
	cXml += 			'<localAcidente>'
	cXml += 				'<tpLocal>'			+ cTpLocal						+ '</tpLocal>'
	cXml += 				'<dscLocal>'		+ cDscLocal						+ '</dscLocal>'
	cXml += 				'<codAmb>'			+ AllTrim( M->TNC_CODAMB )		+ '</codAmb>'
	cXml += 				'<tpLograd>'		+ AllTrim( M->TNC_TPLOGR )		+ '</tpLograd>'
	cXml += 				'<dscLograd>'		+ cDscLograd					+ '</dscLograd>'
	If !Empty( M->TNC_NUMLOG )
		cXml += 			'<nrLograd>'		+ cValtoChar( M->TNC_NUMLOG )	+ '</nrLograd>'
	Else
		cXml += 			'<nrLograd>'		+ "S/N"	+ '</nrLograd>'
	EndIf
	cXml += 				'<complemento>'		+ AllTrim( M->TNC_COMPL )		+ '</complemento>'
	cXml += 				'<bairro>'			+ AllTrim( M->TNC_BAIRRO )		+ '</bairro>'
	If cTpLocal $ "1/3/5" //Se for "Estabelecimento do empregador no Brasil", "Estabelecimento de terceiros" ou "�rea rural"
		cXml += 			'<cep>'				+ M->TNC_CEP					+ '</cep>'
	EndIf
	If cTpLocal $ "1/3/4/5" //Se for "Estabelecimento do empregador no Brasil", "Estabelecimento de terceiros", "Via P�blica" ou "�rea rural"
		cXml += 			'<codMunic>'		+ cCodMunic 					+ '</codMunic>'
		cXml += 			'<uf>'				+ M->TNC_ESTACI 				+ '</uf>'
	EndIf
	If cTpLocal == "2" //Se for "Estabelecimento do empregador no Exterior"
		cXml += 			'<pais>'			+ cCodPai		 				+ '</pais>'
		cXml += 			'<codPostal>'		+ M->TNC_CODPOS					+ '</codPostal>'
	EndIf
	If !Empty( cTpInscAci ) .And. !Empty( M->TNC_CGCPRE )
		cXml += 				'<ideLocalAcid>'
		cXml += 					'<tpInsc>' 		+ cTpInscAci					+ '</tpInsc>'
		cXml += 					'<nrInsc>' 		+ M->TNC_CGCPRE					+ '</nrInsc>'
		cXml += 				'</ideLocalAcid>'
	EndIf
	cXml += 			'</localAcidente>'

	For nCont := 1 To Len( aParte )
		cXml += 			'<parteAtingida>'
		cXml += 				'<codParteAting>'	+ aParte[ nCont, 1 ] + '</codParteAting>'
		cXml += 				'<lateralidade>'	+ aParte[ nCont, 2 ] + '</lateralidade>'
		cXml += 			'</parteAtingida>'
	Next nCont

	For nCont := 1 To Len( aCausa )
		cXml += 			'<agenteCausador>'
		cXml += 				'<codAgntCausador>' + aCausa[ nCont, 1 ] + '</codAgntCausador>'
		cXml += 			'</agenteCausador>'
	Next nCont

	//Se for cadastro de Atestado/Diagn�stico ou se o usu�rio tiver setado a data e hora atendimento no pr�prio Acidente
	If lAtestado .Or. lDiagnostico .Or. ( !Empty( M->TNC_DTATEN ) .And. !Empty( M->TNC_HRATEN ) )
		cXml += 			'<atestado>'
		cXml += 				'<codCNES>'			+ M->TNC_CNES							+ '</codCNES>'

		If !Empty( cDtAtendimento ) .And. !Empty( cHrAtendimento )
			cXml += 			'<dtAtendimento>'	+ DtoS( cDtAtendimento )					+ '</dtAtendimento>'
			cXml += 			'<hrAtendimento>'	+ StrTran( cHrAtendimento, ":", "" )	+ '</hrAtendimento>'
		Else
			cXml += 			'<dtAtendimento>'	+ DtoS( M->TNC_DTATEN )					+ '</dtAtendimento>'
			cXml += 			'<hrAtendimento>'	+ StrTran( M->TNC_HRATEN, ":", "" )	+ '</hrAtendimento>'
		EndIf

		cXml += 				'<indInternacao>'	+ cIndInternacao						+ '</indInternacao>'

		If !Empty( cDurTrat )
			cXml += 			'<durTrat>'			+ cDurTrat								+ '</durTrat>'
		ElseIf !Empty( M->TNC_QTAFAS )
			cXml += 			'<durTrat>'			+ Alltrim( Str( M->TNC_QTAFAS, 3 ) )	+ '</durTrat>'
		Else
			cXml += 			'<durTrat>'			+ "0"									+ '</durTrat>'
		EndIf

		cXml += 				'<indAfast>'		+ cIndAfast								+ '</indAfast>'
		cXml += 				'<dscLesao>'		+ cDscLesao								+ '</dscLesao>'
		cXml += 				'<dscCompLesao>'	+ cDscCompLesao							+ '</dscCompLesao>'
		cXml += 				'<diagProvavel>'	+ cDiagProvavel							+ '</diagProvavel>'
		cXml += 				'<codCID>'			+ cCodCID								+ '</codCID>'
		cXml += 				'<observacao>'		+ cObservacao							+ '</observacao>'
		cXml += 				'<emitente>'
		cXml += 					'<nmEmit>'		+ cNmEmit 								+ '</nmEmit>'
		cXml += 					'<ideOC>'		+ cIdeOC 								+ '</ideOC>'
		cXml += 					'<nrOC>' 		+ cNrOC 								+ '</nrOC>'
		cXml += 					'<ufOC>' 		+ cUfOC 								+ '</ufOC>'
		cXml += 				'</emitente>'
		cXml += 			'</atestado>'
	EndIf

	If M->TNC_TIPCAT $ "2/3" //Se for CAT de Reabertura ou Comunica��o de �bito
		If !Empty( M->TNC_DTCATO ) .And. !Empty( M->TNC_CATORI )

			cNumTrab := Posicione( "C9V", 12, xFilial( "C9V", cFilEnv ) + cCpfTrab + cMatricula, "C9V_ID" )
			dbSelectArea( "TNC" )
			dbSetOrder( 13 ) //TNC_FILIAL+TNC_CATINS
			If dbSeek( xFilial( "TNC" ) + M->TNC_CATORI ) //Posiciona no acidente da CAT Origem
				cNrRecCatOrig := AllTrim( cCpfTrab ) + ";" + AllTrim( cMatricula ) + ";" + IIf( lTSVE, AllTrim( cCodCateg ), "" ) + ";" + ;
									DTOS( TNC->TNC_DTACID ) + ";" + StrTran( TNC->TNC_HRACID, ":", "" ) + ";" + TNC->TNC_TIPCAT
				dDataCat := Posicione( "CM0", 4, xFilial( "CM0", cFilEnv ) + cNumTrab + DTOS( TNC->TNC_DTACID ) + StrTran( TNC->TNC_HRACID, ":", "" ), "CM0_DTACID" )
			EndIf

			If !Empty( cNrRecCatOrig ) .And. !Empty( dDataCat ) .And. dDataCat >= dDtEsoc
				cXml += '	<catOrigem>'
				cXml += '		<nrRecCatOrig>' + cNrRecCatOrig + '</nrRecCatOrig>'
				cXml += '	</catOrigem>'
			EndIf

		EndIf
	EndIf

	cXml += 		'</cat>'
	cXml += 	'</evtCAT>'
	cXml += '</eSocial>'

Return cXml

//---------------------------------------------------------------------
/*/{Protheus.doc} fInconsis

Carrega Inconsist�ncias (Se houver)

@return lRet L�gico Retorna falso caso haja alguma inconsist�ncia

@sample fInconsis( aLogProc , "cMsgLog" , .T. , .F. , 3 , .F. )

@param aLogProc		- Array		- Array que ir� receber os Logs
@param cMsgLog		- Caracter	- Variavel que recebera a mensagem de Log
@param lAtestado	- L�gico	- Indica se foi chamado pelo Atestado M�dico
@param lDiagnostico	- L�gico	- Indica se foi chamado pelo Diagn�stico M�dico
@param nOpcX 		- Num�rico	- Tipo de opera��o
@param lDel			- L�gico 	- Indica se � exclus�o de registro

@author Luis Fellipy Bett
@since 30/08/2018
/*/
//---------------------------------------------------------------------
Static Function fInconsis( aLogProc , cMsgLog , lAtestado , lDiagnostico , nOpcX , lDel , cFilEnv )

	Local cFilBkp		:= cFilAnt
	Local lRet 			:= .F.
	Local nCampos		:= 0
	Local nCont			:= 0
	Local aVldValues	:= {}
	Local cDtEsoc		:= ""

	//Condi��es para realizar a valida��o
	Local bCond1	:= { || !( cCodCateg $ "901/903/904" ) }
	Local bCond2	:= { || cIndCatObito == "S" }
	Local bCond3	:= { || cTpLocal == "2" }
	Local bCond4	:= { || dDataCat >= dDtEsoc .And. M->TNC_TIPCAT $ "2/3" }
	Local bCond5	:= { || Posicione( "C9V" , 3 , xFilial( "C9V" , cFilEnv ) + cCpfTrab , "C9V_NOMEVE" ) == "S2300" } //S� deve ser preenchido se tiver sido cadastrado no S-2300
	Local bCond6	:= { || !Empty( M->TNC_CODAMB ) }
	Local bCond7	:= { || ( cTpLocal $ "1/3/5" ) }
	Local bCond8	:= { || !( cTpAcid $ "2.0.01/2.0.02/2.0.03/2.0.04/2.0.05/2.0.06/4.0.01/4.0.02" ) }
	Local bCond9	:= { || ( cTpLocal $ "1/3/4/5" ) }
	Local bCond10	:= { || lAtestado .Or. lDiagnostico }
	Local bCond11	:= { || cTpInsc == "1" .And. cTpLocal $ "1/3" .And. Empty( M->TNC_CODAMB ) }

	//Valida��o
	Local bValid1	:= { || cTpInsc $ "1/2" }
	Local bValid2	:= { || If( cTpInsc $ "1/2" , If( cTpInsc == "1" , CGC( cNrInsc ) , CHKCPF( cNrInsc ) ) , .T. ) }
	Local bValid3	:= { || CHKCPF( cCpfTrab ) }
	Local bValid4	:= { || M->TNC_DTACID >= dDtEsoc .And. M->TNC_DTACID >= cDtAdmis .And. M->TNC_DTACID <= dDataBase }
	Local bValid5	:= { || ExistCPO( "LE5" , cTpAcid , 2 ) }
	Local bValid6	:= { || If( M->TNC_TIPCAT == "3" , cIndCatObito == "S" , If( M->TNC_TIPCAT == "2" , cIndCatObito == "N" , .T. ) ) }
	Local bValid7	:= { || ExistCPO( "C8L" , cCodSitGeradora , 2 ) .Or. ExistCPO( "C8K" , cCodSitGeradora , 2 ) }
	Local bValid8	:= { || cTpInscAci $ "1/3/4" }
	Local bValid9	:= { || If(cTpLocal=="1",ExistCPO("C92",M->TNC_CGCPRE,3),If(cTpLocal=="3",!ExistCPO("C92",M->TNC_CGCPRE,3) .And. If(cTpInscAci=="1",AllTrim(M->TNC_CGCPRE)<>AllTrim(cInscSM0),.T.),.T.)) }
	Local bValid10	:= { || ExistCPO( "C8M" , cDscLesao , 2 ) }
	Local bValid11	:= { || cIdeOC $ "1/2/3" }
	Local bValid12	:= { || ExistCPO( "C9V" , cMatricula , 11 ) } //"Deve ser a mesma matr�cula informada no evento S-2200"
	Local bValid13	:= { || ExistCPO( "C87" , cCodCateg , 2 ) }
	Local bValid14	:= { || M->TNC_DTOBITO >= M->TNC_DTACID }
	Local bValid15	:= { || ExistCPO( "T04" , M->TNC_CODAMB , 2 ) }
	Local bValid16	:= { || ExistCPO( "C08" , cCodPai , 4 ) }
	Local bValid17	:= { || ExistCPO( "C06" , M->TNC_TPLOGR , 4 ) }
	Local bValid18	:= { || If( cIndCatObito == "S" , cIndAfast == "N" , .T. ) }

	Local cStrFunc	:= STR0083 + ": " + AllTrim( cNumMat ) + " - " + AllTrim( Posicione( "SRA" , 1 , xFilial("SRA") + cNumMat , "RA_NOME" ) ) // "Funcion�rio: XXX - XXXXX"
	Local cStrAci	:= STR0046 + ": " + M->TNC_ACIDEN //Acidente: XXX

	Private cDtAdmis	:= Posicione( "SRA" , 5 , xFilial( "SRA" ) + cCpfTrab , "RA_ADMISSA" )

	cDtEsoc := DToS( dDtEsoc )
	cDtEsoc := " (" + SubStr( cDtEsoc , 7 , 2 ) + "/" + SubStr( cDtEsoc , 5 , 2 ) + "/" + SubStr( cDtEsoc , 1 , 4 ) + ")"

	//Seta a filial de envio para as valida��es de tabelas do TAF
	cFilAnt := cFilEnv

	// --- Estrutura do Array de envio das inconsist�ncias ---
	// Posi��o 1 - Campo a ser verificado
	// Posi��o 2 - String indicando o registro chave que est� sendo analisado junto a um " / " para impress�o no Log. Ex: Acidente: 00001 /
	// Posi��o 3 - String indicando o t�tulo do campo que ser� analisado. Ex: Data Acidente
	// Posi��o 4 - Bloco de C�digo com a condi��o para verifica��o do campo
	// Posi��o 5 - Bloco de C�digo com a valida��o do campo
	// Posi��o 6 - Descri��o da valida��o do campo para envio ao TAF
	// Obs: Na impress�o do Log a fun��o juntar� os dados da 2�, 3� e 1� coluna, sucessivamente. Ex: Acidente: 00001 / Data Acidente: 10/08/2017
	// -------------------------------------------------------

	aVldValues := {;
		{ cTpInsc			, cStrFunc + " / " , STR0035 , { || .T. }	, bValid1		, STR0061	} ,; //"Acidente: X / Tipo Inscri��o: ## 'Deve ser igual a 1 (CNPJ) ou 2 (CPF)'
		{ cNrInsc			, cStrFunc + " / " , STR0036 , { || .T. }	, bValid2		, STR0062	} ,; //"Acidente: X / Inscri��o: ## 'Deve ser um n�mero de CNPJ ou CPF v�lido'
		{ cCpfTrab 			, cStrFunc + " / " , STR0014 , { || .T. }	, bValid3		, STR0063	} ,; //"Acidente: X / C.P.F.: ## 'Deve ser um n�mero de CPF v�lido'
		{ cNisTrab 			, cStrFunc + " / " , STR0052 , bCond1		, { || .T. }	,  			} ,; //"Acidente: X / --- N.I.S.:
		{ cMatricula		, cStrFunc + " / " , STR0053 , { || .T. }	, bValid12		, STR0064	} ,; //"Acidente: X / --- Matr�cula: ## 'Deve ser a mesma matr�cula informada no evento S-2200'
		{ cCodCateg			, cStrFunc + " / " , STR0054 , bCond5		, bValid13		, STR0065	} ,; //"Acidente: X / --- Categoria Funcion�rio: ## 'Deve existir na tabela 01 do eSocial'
		{ M->TNC_DTACID		, cStrAci + " / " , STR0016 , { || .T. }	, bValid4		, STR0066 + CRLF + STR0095 + cDtEsoc } ,; //"Acidente: X / Data Acidente: ## 'A data do acidente deve ser maior que a data de admiss�o do funcion�rio e menor ou igual a data atual'
		{ cTpAcid			, cStrAci + " / " , STR0017 , { || .T. }	, bValid5 		, STR0067	} ,; //"Acidente: X / Tipo Acidente: ## 'Deve existir na tabela 24 do eSocial'
		{ M->TNC_HRACID		, cStrAci + " / " , STR0037 , bCond8		, { || .T. } 	,			} ,; //"Acidente: X / Hora Acidente:
		{ M->TNC_HRTRAB		, cStrAci + " / " , STR0038 , { || .T. }	, { || .T. } 	,			} ,; //"Acidente: X / Horas Trab. Antes Acidente:
		{ M->TNC_TIPCAT		, cStrAci + " / " , STR0018 , { || .T. }	, { || .T. }	,			} ,; //"Acidente: X / Tipo de CAT:
		{ cIndCatObito		, cStrAci + " / " , STR0019 , { || .T. }	, bValid6		, STR0068	} ,; //"Acidente: X / �bito: ## "Se o tipo da CAT for 3 (�bito) deve ser igual a 'S' e se for 2 (Reabertura) deve ser igual a 'N'"
		{ M->TNC_DTOBITO	, cStrAci + " / " , STR0055 , bCond2		, bValid14		, STR0069	} ,; //"Acidente: X / --- Data �bito: ## 'A data de �bito deve ser maior ou igual a data do acidente'
		{ cIndComunPolicia	, cStrAci + " / " , STR0020 , { || .T. }	, { || .T. }	,			} ,; //"Acidente: X / Comunica��o Pol�cia:
		{ cCodSitGeradora	, cStrAci + " / " , STR0015 , { || .T. }	, bValid7		, STR0070	} ,; //"Acidente: X / Situa��o Geradora Acidente: ## 'Deve existir na tabela 16 do eSocial'
		{ cTpLocal			, cStrAci + " / " , STR0021 , { || .T. }	, { || .T. }	,			} ,; //"Acidente: X / Tipo Localiza��o:
		{ M->TNC_CODAMB		, cStrAci + " / " , STR0056 , bCond6		, bValid15		, STR0071	} ,; //"Acidente: X / --- C�digo Ambiente: ## Deve existir na tabela S-1060 do eSocial
		{ M->TNC_TPLOGR		, cStrAci + " / " , STR0089 , { || .T. }	, bValid17		, STR0090	} ,; //"Acidente: X / --- Tipo Logradouro: ## Deve existir na tabela 20 do eSocial
		{ cDscLograd		, cStrAci + " / " , STR0039 , { || .T. }	, { || .T. } 	,			} ,; //"Acidente: X / Descri��o Logradouro:
		{ M->TNC_CEP 		, cStrAci + " / " , STR0057 , bCond7		, { || .T. }	, STR0072	} ,; //"Acidente: X / --- C.E.P.: ## 'Deve ser um n�mero de CEP v�ildo'
		{ cCodMunic			, cStrAci + " / " , STR0084 , bCond9		, { || .T. } 	, 			} ,; //"Acidente: X / --- Cidade:
		{ M->TNC_ESTACI		, cStrAci + " / " , STR0085 , bCond9		, { || .T. } 	, 			} ,; //"Acidente: X / --- Estado:
		{ cCodPai 			, cStrAci + " / " , STR0058 , bCond3 		, bValid16 		, STR0073	} ,; //"Acidente: X / --- Pa�s: ## 'Deve existir na tabela 06 do eSocial'
		{ M->TNC_CODPOS 	, cStrAci + " / " , STR0059 , bCond3 		, { || .T. }	,			} ,; //"Acidente: X / --- C�digo Postal:
		{ cTpInscAci 		, cStrAci + " / " , STR0041 , bCond11		, bValid8		, STR0074	} ,; //"Acidente: X / Tipo Inscri��o Local Acidente: ## 'Deve ser igual a 1 (CNPJ), 3 (CAEPF), ou 4 (CNO)'
		{ M->TNC_CGCPRE 	, cStrAci + " / " , STR0042 , bCond11		, bValid9		, STR0075 + CRLF + STR0076 + CRLF + STR0077 } ,; //"Acidente: X / Inscri��o Local Acidente: ## '1) Deve constar na tabela S-1005 se o local do acidente for igual a "Estabelecimento do Empregador no Brasil". 2) Deve ser diferente dos estabelecimentos informados na Tabela S-1005 se o local do acidente for igual a "Estabelecimento de Terceiros" e diferente do CNPJ base indicado em S1000 se o tipo de inscri��o do local do acidente for igual a CNPJ.'
		{ cDtAtendimento 	, cStrAci + " / " , STR0022 , bCond10		, { || .T. }	,			} ,; //"Acidente: X / Data Consulta:
		{ cHrAtendimento 	, cStrAci + " / " , STR0043 , bCond10		, { || .T. }	,			} ,; //"Acidente: X / Hora Consulta:
		{ cIndInternacao 	, cStrAci + " / " , STR0044 , { || .T. }	, { || .T. }	,			} ,; //"Acidente: X / Interna��o:
		{ cIndAfast 		, cStrAci + " / " , STR0025 , { || .T. }	, bValid18		, STR0097	} ,; //"Acidente: X / Tipo Afastamento: Se houve �bito o campo n�o pode ser preenchido como 'S'"
		{ cDscLesao 		, cStrAci + " / " , STR0045 , { || .T. }	, bValid10		, STR0078	} ,; //"Acidente: X / Descri��o Les�o: ## 'Deve existir na tabela 17 do eSocial'
		{ cCodCID 			, cStrAci + " / " , STR0026 , bCond10		, { || .T. }	,			} ,; //"Acidente: X / C.I.D.:
		{ cNmEmit 			, cStrAci + " / " , STR0027 , bCond10		, { || .T. }	,			} ,; //"Acidente: X / Nome Emitente:
		{ cIdeOC 			, cStrAci + " / " , STR0028 , bCond10		, bValid11		, STR0079	} ,; //"Acidente: X / Entidade de Classe: ## 'Deve ser igual a CRM, CRO ou RMS'
		{ cNrOC 			, cStrAci + " / " , STR0029 , bCond10		, { || .T. }	,			} ,; //"Acidente: X / N�mero Emitente:
		{ cNrRecCatOrig		, cStrAci + " / " , STR0060 , bCond4		, { || .T. } 	, STR0096	} }  //"Acidente: X / N�mero Recibo CAT Origem: ## "Ao cadastrar uma CAT do tipo Origem ou �bito, devem ser informados os campos CAT Origem e Dt. CAT Or."

	lRet := MDTInconEsoc( aVldValues , @aLogProc , @cMsgLog )

	//Valida se foi adicionado a Causa do Acidente.
	If Len( aCausa ) == 0
		aAdd( aLogProc , cStrAci + " / " + STR0031 + ": " + STR0051 ) //"Acidente: XXX / Agente Causa: Em Branco"
		aAdd( aLogProc , '' )
		cMsgLog += CRLF + " - " + STR0031 //"Agente Causa"
		lRet := .F.
	Else
		For nCont := 1 To Len( aCausa )
			If Empty( aCausa[ nCont , 1 ] )
				aAdd( aLogProc , cStrAci + " / " + STR0031 + ": " + STR0051 ) //"Acidente: XXX / Agente Causa: Em Branco"
				aAdd( aLogProc , '' )
				cMsgLog += CRLF + " - " + STR0031 //"Agente Causa"
				lRet := .F.
			ElseIf !ExistCPO( "C8J" , aCausa[ nCont , 1 ] , 2 ) .And. !ExistCPO( "C8K" , aCausa[ nCont , 1 ] , 2 )
				aAdd( aLogProc , cStrAci + " / " + STR0031 + ": " + aCausa[ nCont , 1 ] ) //"Acidente: XXX / Agente Causa: 0000000"
				aAdd( aLogProc , STR0080 + STR0081 ) //"Valida��o: Deve ser um c�digo existente na tabela 14 ou 15 do eSocial"
				aAdd( aLogProc , '' )
				cMsgLog += CRLF + " - " + STR0031 //"Agente Causa"
				lRet := .F.
			EndIf
		Next nCont
	EndIf

	//Valida se foi adicionado as Partes Atingidas no Acidente.
	If Len( aParte ) == 0
		aAdd( aLogProc , cStrAci + " / " + STR0032 + ": " + STR0051 ) //"Acidente: XXX / C�digo Parte: Em Branco"
		aAdd( aLogProc , '' )
		cMsgLog += CRLF + " - " + STR0032 //"C�digo Parte"
		lRet := .F.
	Else
		For nCont := 1 To Len( aParte )
			If Empty( aParte[ nCont , 1 ] )
				aAdd( aLogProc , cStrAci + " / " + STR0032 + ": " + STR0051 ) //"Acidente: XXX / C�digo Parte: Em Branco"
				aAdd( aLogProc , '' )
				cMsgLog += CRLF + " - " + STR0032 //"C�digo Parte"
				lRet := .F.
			ElseIf !ExistCPO( "C8I" , aParte[ nCont , 1 ] , 2 )
				aAdd( aLogProc , cStrAci + " / " + STR0032 + ": " + aParte[ nCont , 1 ] ) //"Acidente: XXX / C�digo Parte: 00000"
				aAdd( aLogProc , STR0080 + STR0082 ) //"Valida��o: Deve ser um c�digo existente na tabela 13 do eSocial"
				aAdd( aLogProc , '' )
				cMsgLog += CRLF + " - " + STR0032 //"C�digo Parte"
				lRet := .F.
			EndIf
			If Empty( aParte[ nCont , 2 ] )
				aAdd( aLogProc , cStrAci + " / " + STR0033 + ": " + STR0051 ) //"Acidente: XXX / Lateralidade: Em Branco"
				aAdd( aLogProc , '' )
				cMsgLog += CRLF + " - " + STR0033 //"Lateralidade"
				lRet := .F.
			EndIf
		Next nCont
	EndIf

	//Volta para a filial do registro
	cFilAnt := cFilBkp

Return lRet