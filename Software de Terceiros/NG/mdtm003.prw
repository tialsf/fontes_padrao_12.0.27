#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "mdtm003.CH"

STATIC aEfd	 := If( cPaisLoc == "BRA" , If( Findfunction( "fEFDSocial" ) , fEFDSocial() , { .F. , .F. , .F. } ) , { .F. , .F. , .F. } )
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//  _______           _______  _       _________ _______             _______  _______  _______  _______  _______  ---
// (  ____ \|\     /|(  ____ \( (    /|\__   __/(  ___  )           (  ____ \/ ___   )/ ___   )/ ___   )(  __   ) ---
// | (    \/| )   ( || (    \/|  \  ( |   ) (   | (   ) |           | (    \/\/   )  |\/   )  |\/   )  || (  )  | ---
// | (__    | |   | || (__    |   \ | |   | |   | |   | |   _____   | (_____     /   )    /   )    /   )| | /   | ---
// |  __)   ( (   ) )|  __)   | (\ \) |   | |   | |   | |  (_____)  (_____  )  _/   /   _/   /   _/   / | (/ /) | ---
// | (       \ \_/ / | (      | | \   |   | |   | |   | |                 ) | /   _/   /   _/   /   _/  |   / | | ---
// | (____/\  \   /  | (____/\| )  \  |   | |   | (___) |           /\____) |(   (__/\(   (__/\(   (__/\|  (__) | ---
// (_______/   \_/   (_______/|/    )_)   )_(   (_______)           \_______)\_______/\_______/\_______/(_______) ---
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MDTM003
Rotina de Envio de Eventos - Monitoramento da Sa�de do Trabalhador (S-2220)
Realiza o envio das informa��es de ASO para o TAF

@return xRet - Caracter/L�gico - Se for gera��o de Xml retorna o cXml, sen�o retorna .T. ou .F. de acordo com as condi��es

@sample MDTM003( .T. , "D MG 01" , "3" , "TMY->TMY_FILIAL + TMY->TMY_NUMASO" , .F. )

@param lMemory	- L�gico 	- Indica se far� uso das vari�veis da mem�ria (TMY)
@param cFilEnv	- Caracter 	- Indica a filial de envio das informa��es ao TAF
@param nOpcX 	- Num�rico 	- Indica o tipo de opera��o do cadastro (3-Incluir;4-Alterar;5-Excluir)
@param cChkASO 	- Caracter 	- Indica a string para posicionamento na tabela TMY
@param lXml 	- L�gico 	- Indica se � gera��o de Xml

@author Luis Fellipy Bett
@since 29/11/2017
/*/
//----------------------------------------------------------------------------------------------------
Function MDTM003( lMemory, cFilEnv, nOpcX, cChkASO, lXml, lInconsis )

	Local aArea		 	:= GetArea()
	Local aAreaTMY	 	:= TMY->( GetArea() )
	Local lMiddleware	:= IIf( cPaisLoc == 'BRA' .And. Findfunction( "fVerMW" ), fVerMW(), .F. )
	Local aErros 	 	:= {}
	Local aLogProc		:= {}
	Local aDados		:= {}
	Local aEstab		:= {}
	Local cXml			:= ""
	Local cMsgLog		:= ""
	Local cChvBus		:= ""
	Local xRet			:= .T.

	Private cTpInsc
	Private cNrInsc
	Private cNumMat			//Matr�cula do Funcion�rio
	Private cCpfTrab		//CPF do Funcion�rio
	Private cNisTrab		//N�mero Nis Funcion�rio
	Private cMatricula		//C�digo �nico do Funcion�rio
	Private cCodCateg		//Categoria do Funcion�rio
	Private dDtAdm			//Data de Admiss�o do Funcion�rio
	Private cCCusto			//Centro de Custo do Funcion�rio

	Private cCodMedico		//C�digo do m�dico TMY_CODUSU
	Private cTpExameOcup	//Tipo de Atestado de Sa�de Ocupacional emitido
	Private dDtAso			//Data do Atestado de Sa�de Ocupacional
	Private cResAso			//Resultado do ASO
	Private cOrdExame		//Ordem do Exame (Referencial ou Sequencial)
	Private cCpfMed			//Preencher com o CPF do m�dico emitente do ASO.
	Private cNisMed			//Preencher com NIS do M�dico emitente do ASO.
	Private cNmMed			//Nome do m�dico encarregado do exame m�dico
	Private cNrCrmMed		//Numero da entidade do m�dico
	Private cUfCrmMed		//Sigla da UF de expedi��o do CRM
	Private cCodResp
	Private cCpfResp		//Preencher com o CPF do m�dico respons�vel/coordenador do PCMSO.
	Private cNmResp			//Preencher com o nome do m�dico respons�vel/coordenador do PCMSO.
	Private cNrCrmResp		//N�mero de inscri��o do m�dico respons�vel/coordenador do PCMSO no CRM.
	Private cUfCRMResp		//Preencher com a sigla da UF de expedi��o do CRM.

	Private aAgeASO		:= {} //Agentes do Atestado
	Private aExaAtes	:= {} //Exames do Atestado

	Default lMemory		:= .F.
	Default lXml		:= .F.
	Default lInconsis	:= .F.
	Default nOpcX		:= 3

	//Trata mensagem inicial de inconsist�ncias
	aAdd( aLogProc , STR0001 ) //"Inconsist�ncias do ASO"
	aAdd( aLogProc , STR0002 ) //"Os campos abaixo est�o vazios/zerados ou possuem inconsist�ncia com rela��o ao formato padr�o do eSocial: "
	aAdd( aLogProc , "" )
	aAdd( aLogProc , "" )

	//Verifica se TAF est� preparado
	aRet:= TafExisEsc( "S2220" )
	If !aRet[ 1 ] .Or. ( aRet[ 2 ] <> "2.4" .And. aRet[ 2 ] <> "2.4.02" .And. aRet[ 2 ] <> "2.5" )
		Help(' ',1,STR0003,,STR0004,2,0,,,,,,{STR0008}) //"Aten��o" ## "O ambiente n�o possui integra��o com o m�dulo do TAF e/ou a vers�o do TAF est� desatualizada!" ## "Favor verificar!"
		xRet := .F.
	ElseIf !( NGCADICBASE( "TMA_ESOC", "A", "TMA", .F. ) )
		Help(' ',1,STR0003,,STR0005,2,0,,,,,,{STR0006}) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial"
		xRet := .F.
	ElseIf !( NGCADICBASE( "TNC_TPLOGR", "A", "TNC", .F. ) )
		Help(' ',1,STR0003,,STR0005,2,0,,,,,,{STR0006}) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial"
		xRet := .F.
	Else
		//Caso n�o tenha a Filial n�o tem como fazer a Integra��o
		If !Empty( cFilEnv )
			//Inicializa regua de processamento
			ProcRegua( 1 )

			If lMemory //Alimenta as vari�veis de mem�ria para utiliza��o
				dbSelectArea( "TMY" )
				dbSetOrder( 1 )
				dbSeek( cChkASO )

				aRotSetOpc( "TMY" , TMY->( Recno() ) , 4 )

				RegToMemory( "TMY" , .F. )
			EndIf

			cCodMedico	:= M->TMY_CODUSU

			cNumMat		:= Posicione( "TM0" , 1 , xFilial( "TM0" ) + M->TMY_NUMFIC	, "TM0_MAT"		)
			cCpfTrab	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat		, "RA_CIC"		)
			cNisTrab	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat 		, "RA_PIS" 		)
			cMatricula	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat 		, "RA_CODUNIC" 	)
			cCodCateg 	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat		, "RA_CATEFD"	)
			dDtAdm		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat		, "RA_ADMISSA"	)
			cCCusto		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cNumMat		, "RA_CC"		)

			cTpExameOcup := M->TMY_NATEXA
			dDtAso		 := If( !Empty( M->TMY_DTEMIS ) , M->TMY_DTEMIS , dDataBase )
			cResAso		 := M->TMY_INDPAR
			cOrdExame	 := M->TMY_INDEXA //Referencial ou Sequencial

			cCpfMed		:= Posicione( "TMK" , 1 , xFilial( "TMK" ) + cCodMedico		, "TMK_CIC"		)
			cNisMed		:= Posicione( "TMK" , 1 , xFilial( "TMK" ) + cCodMedico 	, "TMK_NIT"		)
			cNmMed		:= Posicione( "TMK" , 1 , xFilial( "TMK" ) + cCodMedico		, "TMK_NOMUSU" 	)
			cNrCrmMed	:= Posicione( "TMK" , 1 , xFilial( "TMK" ) + cCodMedico		, "TMK_NUMENT" 	)
			cUfCrmMed	:= Posicione( "TMK" , 1 , xFilial( "TMK" ) + cCodMedico		, "TMK_UF"		)

			cCodResp	:= MDTASOCoord( M->TMY_DTGERA ) //Busca o c�digo do M�dico Coordenador do PCMSO

			cCpfResp	:= Posicione( "TMK" , 1 , xFilial( "TMK" ) + cCodResp , "TMK_CIC"		)
			cNmResp		:= Posicione( "TMK" , 1 , xFilial( "TMK" ) + cCodResp , "TMK_NOMUSU"	)
			cNrCrmResp	:= Posicione( "TMK" , 1 , xFilial( "TMK" ) + cCodResp , "TMK_NUMENT"	)
			cUfCRMResp	:= Posicione( "TMK" , 1 , xFilial( "TMK" ) + cCodResp , "TMK_UF"		)

			cNmMed	:= MDTSubTxt( AllTrim( cNmMed ) )
			cNmResp	:= MDTSubTxt( AllTrim( cNmResp ) )

			// Agentes relacionados ao atestado.
			aAgeASO := fSearchAge( nOpcX , M->TMY_NUMASO , lXml )

			// Exames relacionados ao atestado.
			aExaAtes := fSearchExa( nOpcX , M->TMY_NUMASO , lXml )

			//------- Tipo de Atestado -------
			// 1 - Admissional;          = 0 - Admissional
			// 2 - Periodico;            = 1 - Per�odico, conforme planejamento do PCMSO
			// 3 - Mudanca de Funcao;    = 3 - De mudan�a de fun��o
			// 4 - Retorno ao Trabalho;  = 2 - De retorno ao trabalho
			// 5 - Demissional;          = 9 - Demissional
			//							 = 4 - Exame m�dico de monitara��o pontual
			//--------------------------------
			Do Case
				Case cTpExameOcup == "1" ; cTpExameOcup := "0"
				Case cTpExameOcup == "2" ; cTpExameOcup := "1"
				Case cTpExameOcup == "4" ; cTpExameOcup := "2"
				Case cTpExameOcup == "5" ; cTpExameOcup := "9"
			End Case

			//Se o resultado do parecer do ASO for igual a "Apto com Restri��o" verifica o par�metro
			If cResAso == "3"
				If SuperGetMv( "MV_NG2RASO" , .F. , "2" ) == "1"
					cResAso := "1" //Apto
				Else
					cResAso := "2" //Inapto
				EndIf
			EndIf

			//Busca as informa��es do Estabelecimento
			aEstab := MDTGetEst( cCCusto , cFilEnv )

			cTpInsc	:= aEstab[ 1 , 1 ]
			cNrInsc	:= aEstab[ 1 , 2 ]

			If lInconsis
				fInconsis( @aLogProc , @cMsgLog , nOpcX , cFilEnv )

				If Len( aLogProc ) > 4
					If !IsBlind()
						fMakeLog( { aLogProc } , { STR0014 } , Nil , Nil , "MDTM003" , OemToAnsi( STR0015 ) , "M" , "P" , , .F. ) //"Monitoramento Envio de Eventos - TAF"###"Log de Ocorrencias - ASO"

						Help(' ',1,STR0003,,STR0053,2,0,,,,,,{STR0054}) // "N�o foi poss�vel o envio ao eSocial pois os dados est�o inconsistentes!" ## "Favor verificar os �tens expostos no relat�rio!"

						xRet := .F.
					Else
						aAdd( aDados , OemToAnsi( STR0004 ) + " " + cCodAmb + " " + OemToAnsi( STR0006 ) + "! " + STR0044 ) //"Ambiente" ## "Falha no envio ao TAF" ## "Para verifica��o favor gerar o relat�rio de inconsist�ncias"
						xRet := .F.
					EndIf
				EndIf
			Else
				cXml := fCarrASO( cValToChar( nOpcX ), cFilEnv, cCCusto, lXml, lMiddleware )

				If lXml
					fInconsis( @aLogProc , @cMsgLog , nOpcX , cFilEnv )

					If Len( aLogProc ) > 4
						fMakeLog( { aLogProc } , { STR0014 } , Nil , Nil , "MDTM003" , OemToAnsi( STR0015 ) , "M" , "P" , , .F. ) //"Monitoramento Envio de Eventos - TAF"###"Log de Ocorrencias - ASO"

						Help(' ',1,STR0003,,STR0055,2,0,,,,,,{STR0054}) // "O Xml possui inconsist�ncias de acordo com o formato padr�o do eSocial" ## "Favor verificar os valores expostos no relat�rio!"

						xRet := .F.
					Else
						xRet := cXml
					EndIf
				Else
					Begin Transaction

						//Caso o envio seja atrav�s do Middleware
						If lMiddleware

							If MDTVerTSVE( cCodCateg ) //Caso seja Trabalhador Sem V�nculo Estatut�rio
								cChvBus := AllTrim( cCpfTrab ) + AllTrim( cCodCateg ) + DToS( dDtAdm ) + DToS( dDtAso )
							Else
								cChvBus := AllTrim( cMatricula ) + DToS( dDtAso )
							EndIf

							//Envia o evento ao eSocial atrav�s do Middleware
							MDTEnvMid( cFilEnv, cNumMat, "S2220", cChvBus, cXml, nOpcX )

						Else
							//Enviar como parametro a filial do Protheus na posi��o 1 e o XML na posi��o 2
							aErros := TafPrepInt( cEmpAnt , cFilEnv , cXml , , "1" , "S2220" )

							If Len( aErros ) <= 0
								//Carrega array de controle de carga
								aAdd( aDados , OemToAnsi( STR0009 ) + " " + M->TMY_NUMASO + " - " + cCpfTrab + " " + OemToAnsi( STR0010 ) ) //"ASO"###"Enviado ao TAF com sucesso!"
								Help(' ',1,STR0003,,STR0063,2,0) //"Aten��o"##"Registro enviado ao TAF com sucesso!"
							Else
								aAdd( aDados , OemToAnsi( STR0009 ) + " " + M->TMY_NUMASO + " - " + cCpfTrab + " " + OemToAnsi( STR0011 ) + ": " + aErros[ 1 ] ) //"ASO"###"Falha no envio ao TAF: "
								Help(' ',1,STR0003,,STR0013 + CRLF + aErros[ 1 ],2,0,,,,,,{STR0056}) //"Aten��o" ## "Existem inconsist�ncias na comunica��o com o TAF!" ## "Para tentativa de reenvio favor manipular o registro!"
							EndIf

							//Incrementa regua
							IncProc( OemToAnsi( STR0012 ) + " " + M->TMY_NUMASO + " - " + cCpfTrab ) //"Gerando o registro de:"
						EndIf

					End Transaction
				EndIf
			EndIf
		Else
			Help(' ',1,STR0003,,STR0007,2,0,,,,,,{STR0008}) // Aten��o ## "A informa��o da filial de envio ao eSocial est� vazia!" ## "Favor verificar!"
			xRet := .F.
		EndIf
	EndIf

	RestArea( aAreaTMY )
	RestArea( aArea )

Return xRet
//---------------------------------------------------------------------
/*/{Protheus.doc} fCarrASO

Carrega os Atestados ASO's ou os Exames Toxicol�gicos

@return cXml Caracter Estrutura XML a ser enviada para o TAF

@sample fCarrASO( "3" , "D MG 01" , "000001" , .F. )

@param cOper	- Caracter	- Opera��o a ser realizada
@param cFilEnv	- Caracter	- Filial de Envio
@param cCCusto	- Caracter	- Centro de Custo do Funcion�rio

@author Luis Fellipy Bett
@since 30/08/2018
/*/
//---------------------------------------------------------------------
Static Function fCarrASO( cOper, cFilEnv, cCCusto, lXml, lMiddleware )

	Local cXml		:= ""
	Local cChvBus	:= ""
	Local lTSVE		:= MDTVerTSVE( cCodCateg )
	Local nCont

	Default cOper := "3"

	If lTSVE //Caso seja Trabalhador Sem V�nculo Estatut�rio
		cChvBus := AllTrim( cCpfTrab ) + AllTrim( cCodCateg ) + DToS( dDtAdm ) + DToS( dDtAso )
	Else
		cChvBus := AllTrim( cMatricula ) + DToS( dDtAso )
	EndIf

	//Cria o cabe�alho do Xml com o ID, informa��es do Evento e Empregador
	MDTGerCabc( @cXml, "S2220", cTpInsc, cNrInsc, lMiddleware, lXml, cOper, cChvBus )

	//FUNCIONARIO
	cXml += 		'<ideVinculo>'
	cXml += 			'<cpfTrab>'		+ cCpfTrab +		'</cpfTrab>'
	If !( cCodCateg $ "901/903/904" )
		cXml +=			'<nisTrab>'		+ AllTrim( cNisTrab ) +		'</nisTrab>'
	EndIf
	If !lTSVE //Se n�o for Trabalhador Sem V�nculo Estatut�rio
		cXml +=			'<matricula>'	+ cMatricula +		'</matricula>'
	Else
		cXml +=			'<codCateg>'	+ cCodCateg  +		'</codCateg>'
	EndIf
	cXml += 		'</ideVinculo>'

	//ASO
	cXml += 		'<exMedOcup>'
	cXml += 			'<tpExameOcup>' + cTpExameOcup + '</tpExameOcup>'
	cXml += 			'<aso>'
	cXml += 				'<dtAso>'	+ MDTAjsData( dDtAso, lMiddleware ) + '</dtAso>'
	cXml += 				'<resAso>'	+ cResAso		 + '</resAso>'

	//VINCULO - EXAME
	For nCont := 1 To Len( aExaAtes )
		cXml += 			'<exame>' //Exame Ocupacional
		cXml += 				'<dtExm>' 		  + MDTAjsData( aExaAtes[ nCont, 2 ], lMiddleware )	+ '</dtExm>'
		cXml += 				'<procRealizado>' + AllTrim( aExaAtes[ nCont, 3 ] )			+ '</procRealizado>'
		If !Empty( aExaAtes[ nCont, 4 ] )
			cXml += 			'<obsProc>'		  + AllTrim( aExaAtes[ nCont, 4 ] )			+ '</obsProc>'
		EndIf
		cXml += 				'<ordExame>'	  + cOrdExame								+ '</ordExame>'
		cXml += 				'<indResult>'	  + aExaAtes[ nCont, 5 ]					+ '</indResult>'
		cXml += 			'</exame>'
	Next nCont

	cXml += 				'<medico>' //M�dico Emitente do ASO
	cXml += 					'<cpfMed>'	+ cCpfMed						+ '</cpfMed>'
	cXml += 					'<nisMed>'	+ cNisMed						+ '</nisMed>'
	cXml += 					'<nmMed>'	+ cNmMed						+ '</nmMed>'
	cXml += 					'<nrCRM>'	+ SubStr( cNrCrmMed, 1, 8 )		+ '</nrCRM>'
	cXml += 					'<ufCRM>'	+ cUfCrmMed						+ '</ufCRM>'
	cXml += 				'</medico>'
	cXml += 			'</aso>'
	cXml += 			'<respMonit>' //M�dico Respons�vel/Coordenador do PCMSO
	cXml += 				'<cpfResp>'	+ cCpfResp							+ '</cpfResp>'
	cXml += 				'<nmResp>'	+ cNmResp							+ '</nmResp>'
	cXml += 				'<nrCRM>'	+ SubStr( cNrCrmResp, 1, 8 )		+ '</nrCRM>'
	cXml += 				'<ufCRM>'	+ cUfCRMResp						+ '</ufCRM>'
	cXml += 			'</respMonit>'
	cXml += 		'</exMedOcup>'
	cXml += 	'</evtMonit>'
	cXml += '</eSocial>'

Return cXml

//---------------------------------------------------------------------
/*/{Protheus.doc} fSearchExa
Realiza a busca dos exames relacionados no atestado
@param nOpc      -
@param aExaAtes - Array que conter� os valores dos exames relacionados

@author Guilherme Benekendorf
@since 25/11/2013
@version P11
@return boolean

/*/
//---------------------------------------------------------------------
Static Function fSearchExa( nOpc, cNumASO, lXml )

	Local aExaAtes	:= {}
	Local aExame	:= {}

	If nOpc <> 3 .And. Type( "cTRB2200" ) == "C" .And. Select( cTRB2200 ) <> 0 .And. !lXml
		dbSelectArea( cTRB2200 )
		dbGoTop()
		Do While ( cTRB2200 )->( !Eof() )
			If !Empty( ( cTRB2200 )->TM5_OK )
				dbSelectArea( "TM5" )
				dbSetOrder( 8 ) //"TM5_FILIAL+TM5_NUMFIC+DTOS(TM5_DTPROG)+TM5_HRPROG+TM5_EXAME"
				If dbSeek( xFilial( "TM5" ) + ( cTRB2200 )->TM5_NUMFIC + DTOS( ( cTRB2200 )->TM5_DTPROG ) + ( cTRB2200 )->TM5_HRPROG + ( cTRB2200 )->TM5_EXAME )

					aExame := fStructExa()

					If Len( aExame ) > 0
						aAdd( aExaAtes, aExame )
					EndIf

				Endif
			Endif
			dbSelectArea( cTRB2200 )
			( cTRB2200 )->( dbSkip() )
		End
	Else
		dbSelectArea( "TM5" )
		dbSetOrder( 4 ) //TM5_FILIAL+TM5_NUMASO
		dbSeek( xFilial( "TM5" ) + cNumASO )
		While !Eof() .And. TM5->TM5_FILIAL == xFilial( "TM5" ) .And. TM5->TM5_NUMASO == cNumASO

			aExame := fStructExa()

			If Len( aExame ) > 0
				aAdd( aExaAtes, aExame )
			EndIf

			dbSelectArea( "TM5" )
			dbSkip()
		End
	EndIf

Return aExaAtes
//---------------------------------------------------------------------
/*/{Protheus.doc} fStructExa
Retorna a estrutura do exame a ser inserido no evento

@author Luis Fellipy Bett
@since 23/10/2017
@return array
/*/
//---------------------------------------------------------------------
Function fStructExa()

	Local aExame := {}
	Local cNomTM5  	:= ""
	Local cProcReal	//C�digo do procedimento m�dico (TUSS)
	Local cIndResult
	Local cObsProc	//Observa��o sobre o procedimento m�dico

	cNomTM5 	:= Posicione( "TM4", 1, xFilial( "TM4" ) + TM5->TM5_EXAME, "TM4_NOMEXA" )
	cProcReal	:= Posicione( "TM4", 1, xFilial( "TM4" ) + TM5->TM5_EXAME, "TM4_PROCRE" )
	cObsProc	:= TM5->TM5_OBSERV

	//Trata acentua��o
	cObsProc  := AllTrim( MDTSubTxt( cObsProc ) )

	//Indica��o dos Resultados
	/*----------------------
	1=Normal;              		- 1 = Normal
	2=Alterado;            		- 2 = Alterado
	2=Alterado e Agravamento=2 	- 3 = Est�vel
	2=Alterado e Agravamento=1 	- 4 = Agravamento
	----------------------*/
	cIndResult 	:= TM5->TM5_INDRES
	Do Case
		Case cIndResult = "2" .And. TM5->TM5_INDAGR == "2"  ;	cIndResult := "3"
		Case cIndResult = "2" .And. TM5->TM5_INDAGR == "1"  ;	cIndResult := "4"
	End Case

	If aScan( aExaAtes, { | x | x[ 2 ] == TM5->TM5_DTRESU .And. x[ 1 ] == cNomTM5 } ) == 0
		dbSelectArea( "TMW" )
		dbSetOrder( 1 )
		If dbSeek( xFilial( "TMW" ) + TM5->TM5_PCMSO )
			dbSelectArea( "TMK" )
			dbSetOrder( 1 )
			If dbSeek( xFilial( "TMK" ) + TMW->TMW_CODUSU )
				If ( !Empty( cProcReal ) .Or. !Empty( cObsProc ) ) .And. ;
					!Empty( TMK->TMK_NIT ) .And. ;
					!Empty( TMK->TMK_NUMENT )

					aExame	:=  {	cNomTM5 ,;
									TM5->TM5_DTRESU	,;	//dtExm
									cProcReal		,;	//procRealizado
									cObsProc 		,;	//obsProc
									cIndResult }		//indResultado
				EndIf
			EndIf
		EndIf
	EndIf

Return aExame
//---------------------------------------------------------------------
/*/{Protheus.doc} fSearchAge
Realiza a busca dos agentes relacionados no atestado
@param nOpc      -
@param aExaAtes - Array que conter� os valores dos exames relacionados

@author Guilherme Benekendorf
@since 13/01/2013
@version P11
@return boolean

/*/
//---------------------------------------------------------------------
Static Function fSearchAge( nOpc, cNumASO, lXml )

	Local nAge
	Local aAgeAtes := {}

	If nOpc <> 3 .And. Type( "aGETAGE" ) == "A" .And. !lXml
		If Len( aGETAGE ) > 0
			For nAge := 1 To Len( aGETAGE )
				dbSelectArea( "TMA" )
				dbSetOrder( 1 ) //TMA_FILIAL+TMA_AGENTE
				dbSeek( xFilial( "TMA" ) + aGETAGE[ nAge, 1 ] )
				If !Empty( TMA->TMA_ESOC ) .And. aScan( aAgeAtes, { | x | x[1] == TMA->TMA_AGENTE .And. x[2] == TMA->TMA_ESOC } ) == 0
					aAdd( aAgeAtes, { TMA->TMA_AGENTE } )
				EndIf
			Next nAge
		EndIf
	Else
		dbSelectArea( "TMC" )
		dbSetOrder( 1 )//TMC_FILIAL+TMC_NUMASO+TMC_AGENTE
		dbSeek( xFilial( "TMC" ) + cNumASO )
		While TMC->( !Eof() ) .And. TMC->TMC_NUMASO == cNumASO

			dbSelectArea( "TMA" )
			dbSetOrder( 1 ) //TMA_FILIAL+TMA_AGENTE
			dbSeek( xFilial( "TMA" ) + TMC->TMC_AGENTE )
			If !Empty( TMA->TMA_ESOC ) .And. aScan( aAgeAtes, { | x | x[1] == TMA->TMA_AGENTE .And. x[2] == TMA->TMA_ESOC } ) == 0
				aAdd( aAgeAtes, { TMA->TMA_AGENTE } )
			EndIf

			TMC->( dbSkip() )
		End

	EndIf

Return aAgeAtes

//---------------------------------------------------------------------
/*/{Protheus.doc} fInconsis

Carrega Inconsist�ncias (Se houver)

@return lRet L�gico Retorna falso caso haja alguma inconsist�ncia

@sample fInconsis( aLogProc , "cMsgLog" , .F. , "ST9" )

@param aLogProc	- Array		- Array que ir� receber os Logs
@param cMsgLog	- Caracter	- Variavel que recebera a mensagem de Log
@param nOpcX	- Num�rico	- Indica a opera��o realizada

@author Luis Fellipy Bett
@since 30/08/2018
/*/
//---------------------------------------------------------------------
Static Function fInconsis( aLogProc, cMsgLog, nOpcX, cFilEnv )

	Local cFilBkp		:= cFilAnt
	Local lRet 			:= .F.
	Local nCont			:= 0
	Local aVldValues	:= {}
	Local cStrASO		:= ""
	Local cStrFunc		:= ""
	Local cStrEmit		:= ""
	Local cStrResp		:= ""
	Local cIdFunc		:= ""
	Local dDtEsoc		:= SuperGetMv( "MV_NG2DTES", .F., SToD( "20190701" ) )

	//Blocos de C�digo das Condi��es
	Local bCond1 := { || !( cCodCateg $ "901/903/904" ) }
	Local bCond2 := { || Posicione( "C9V", 3, xFilial( "C9V", cFilEnv ) + cCpfTrab, "C9V_NOMEVE" ) == "S2300" } //S� deve ser preenchido se tiver sido cadastrado no S-2300
	Local bCond3 := { || !Empty( cCpfMed ) }
	Local bCond4 := { || !Empty( cCpfResp ) }

	//Blocos de C�digo das Valida��es
	Local bValid1 := { || cTpInsc $ "1/2" }
	Local bValid2 := { || IIf( cTpInsc $ "1/2", IIf( cTpInsc == "1", CGC( cNrInsc ), CHKCPF( cNrInsc ) ), .T. ) }
	Local bValid3 := { || CHKCPF( cCpfTrab ) }
	Local bValid4 := { || ExistCPO( "C9V", cMatricula, 11 ) } //"Deve ser a mesma matr�cula informada no evento S-2200"
	Local bValid5 := { || ExistCPO( "C87", cCodCateg, 2 ) }
	Local bValid6 := { || DToS( dDtAso ) >= DToS( dDtEsoc ) }
	Local bValid7 := { || CHKCPF( cCpfMed ) }
	Local bValid8 := { || CHKCPF( cCpfResp ) }

	cStrFunc	:= STR0052 + ": " + AllTrim( cNumMat ) + " - " + AllTrim( Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat, "RA_NOME" ) ) //"Funcion�rio: XXX - XXXXX
	cStrASO		:= STR0017 + ": " + M->TMY_NUMASO //Atestado ASO: XXX
	cStrEmit	:= STR0057 + ": " + AllTrim( cCodMedico ) + " - " + AllTrim( Posicione( "TMK", 1, xFilial( "TMK" ) + cCodMedico, "TMK_NOMUSU" ) ) //"M�dico: 001 - PAULO JOSE"
	cStrResp	:= STR0057 + ": " + AllTrim( cCodResp ) + " - " + AllTrim( Posicione( "TMK", 1, xFilial( "TMK" ) + cCodResp, "TMK_NOMUSU" ) ) //"M�dico: 001 - PAULO JOSE"
	cDtImp		:= DToS( dDtEsoc )
	cDtImp		:= " (" + SubStr( cDtImp, 7, 2 ) + "/" + SubStr( cDtImp, 5, 2 ) + "/" + SubStr( cDtImp, 1, 4 ) + ")"

	//Seta a filial de envio para as valida��es de tabelas do TAF
	cFilAnt := cFilEnv

	// --- Estrutura do Array de envio das inconsist�ncias ---
	// Posi��o 1 - Campo a ser verificado
	// Posi��o 2 - String indicando o registro chave que est� sendo analisado junto a um " / " para impress�o no Log. Ex: Atestado ASO: 012354632 /
	// Posi��o 3 - String indicando o t�tulo do campo que ser� analisado. Ex: Data Acidente
	// Posi��o 4 - Bloco de C�digo com a condi��o para verifica��o do campo
	// Posi��o 5 - Bloco de C�digo com a valida��o do campo
	// Posi��o 6 - Descri��o da valida��o do campo para envio ao TAF
	// Obs: Na impress�o do Log a fun��o juntar� os dados da 2�, 3� e 1� coluna, sucessivamente. Ex: Atestado ASO: 012354632 / Data Emiss�o ASO: 28/10/2017
	// -------------------------------------------------------

	aVldValues := {;
		{ cTpInsc		, cStrFunc + " / "	, STR0018 , { || .T. }	, bValid1		, STR0036	} ,; //"Atestado ASO: X / Tipo Inscri��o: ## "Deve ser igual a 1 (CNPJ) ou 2 (CPF)"
		{ cNrInsc		, cStrFunc + " / "	, STR0019 , { || .T. }	, bValid2		, STR0037	} ,; //"Atestado ASO: X / Incri��o: ## "Deve ser um n�mero de CNPJ ou CPF v�lido"
		{ cCpfTrab 		, cStrFunc + " / "	, STR0020 , { || .T. }	, bValid3		, STR0038	} ,; //"Atestado ASO: X / C.P.F.: ## "Deve ser um n�mero de CPF v�lido"
		{ cNisTrab 		, cStrFunc + " / "	, STR0033 , bCond1		, { || .T. }	,			} ,; //"Atestado ASO: X / : N.I.S.:
		{ cMatricula	, cStrFunc + " / "	, STR0034 , { || .T. } 	, bValid4		, STR0039	} ,; //"Atestado ASO: X / Matr�cula: ## "Deve ser a mesma matr�cula informada no evento S-2200"
		{ cCodCateg 	, cStrFunc + " / "	, STR0035 , bCond2		, bValid5		, STR0040	} ,; //"Atestado ASO: X / Categoria Funcion�rio: ## "Deve existir na tabela 01 do eSocial"
		{ dDtAso 		, cStrASO + " / "	, STR0022 , { || .T. }	, bValid6		, STR0042 + cDtImp } ,; //"Atestado ASO: X / Data Emiss�o ASO: ## "Deve ser igual ou posterior � data de in�cio da obrigatoriedade dos eventos de SST (201901)"
		{ cResAso 		, cStrASO + " / "	, STR0023 , { || .T. }	, { || .T. }	,			} ,; //"Atestado ASO: X / Resultado ASO:
		{ cOrdExame		, cStrASO + " / "	, STR0049 , { || .T. }	, { || .T. }	,			} ,; //"Atestado ASO: X / Exame de Refer�ncia:
		{ cCpfMed 		, cStrEmit + " / "	, STR0024 , bCond3		, bValid7		, STR0038	} ,; //"Atestado ASO: X / C.P.F Med. Emitente ASO: ## "Deve ser um n�mero de CPF v�lido"
		{ cNmMed		, cStrEmit + " / "	, STR0026 , { || .T. }	, { || .T. }	,			} ,; //"Atestado ASO: X / Nome Med. Emitente ASO:
		{ cNrCrmMed		, cStrEmit + " / "	, STR0027 , { || .T. }	, { || .T. }	,			} ,; //"Atestado ASO: X / N�mero Inscri��o Med. Emitente ASO:
		{ cUfCrmMed		, cStrEmit + " / "	, STR0028 , { || .T. }	, { || .T. }	,			} ,; //"Atestado ASO: X / UF Med. Emitente ASO:
		{ cCpfResp		, cStrResp + " / "	, STR0029 , bCond4		, bValid8		, STR0038	} ,; //"Atestado ASO: X / C.P.F Med. Respons�vel PCMSO: ## "Deve ser um n�mero de CPF v�lido"
		{ cNmResp		, cStrResp + " / "	, STR0030 , { || .T. }	, { || .T. }	,			} ,; //"Atestado ASO: X / Nome Med. Respons�vel PCMSO:
		{ cNrCrmResp	, cStrResp + " / "	, STR0031 , { || .T. }	, { || .T. }	,			} ,; //"Atestado ASO: X / N�mero Inscri��o Med. Resp. PCMSO:
		{ cUfCRMResp	, cStrResp + " / "	, STR0032 , { || .T. }	, { || .T. }	,			} } //"Atestado ASO: X / UF Med. Respons�vel PCMSO:

	lRet := MDTInconEsoc( aVldValues, @aLogProc, @cMsgLog )

	//Se for ASO Admissional
	If cTpExameOcup == "0"
		cIdFunc := FGetIdInt( "cpfTrab", "matricula", cCpfTrab, cMatricula )
		dbSelectArea( "C8B" )
		dbSetOrder( 2 )
		dbSeek( xFilial( "C8B" ) + cIdFunc )
		While xFilial( "C8B" ) == C8B->C8B_FILIAL .And. C8B->C8B_FUNC == cIdFunc
			If C8B->C8B_DTASO < dDtAso
				aAdd( aLogProc, cStrASO + " / " + STR0021 + ": " + STR0061 ) //"Exame: EXAME TOXIC / Tipo Exame Ocupacional: Admissional"
				aAdd( aLogProc, STR0044 + STR0062 ) //"Valida��o: Se ASO Admissional, n�o pode existir outro ASO transmitido para o funcion�rio com data de emiss�o anterior."
				cMsgLog += CRLF + " - " + STR0021 //"Tipo Exame Ocupacional"
				lRet := .F.
				Exit
			Else
				dbSkip()
			EndIf
		End
	EndIf

	If Len( aExaAtes ) > 0
		For nCont := 1 To Len( aExaAtes )
			If Empty( aExaAtes[ nCont, 2 ] ) //<dtExm>
				aAdd( aLogProc, STR0058 + AllTrim( aExaAtes[ nCont, 1 ] ) + " / " + STR0045 + ": " + STR0043 ) //"Exame: EXAME TOXIC / Data Realiza��o Exame: Em Branco"
				cMsgLog += CRLF + " - " + STR0045 //"Data Realiza��o Exame"
				lRet := .F.
			ElseIf aExaAtes[ nCont, 2 ] > dDtAso //<dtExm>
				aAdd( aLogProc, STR0058 + AllTrim( aExaAtes[ nCont, 1 ] ) + " / " + STR0045 + ": " + DToC( aExaAtes[ nCont, 2 ] ) ) //"Exame: EXAME TOXIC / Data Realiza��o Exame: 15/11/2018 (EXAME TOXIC)"
				aAdd( aLogProc, STR0044 + STR0046 ) //"Valida��o: Deve ser uma data igual ou anterior a data de Emiss�o do ASO"
				cMsgLog += CRLF + " - " + STR0045 //"Data Realiza��o Exame"
				lRet := .F.
			EndIf
			If Empty( aExaAtes[ nCont, 3 ] ) //<procRealizado>
				aAdd( aLogProc, STR0058 + AllTrim( aExaAtes[ nCont, 1 ] ) + " / " + STR0047 + ": " + STR0043 ) //"Exame: EXAME TOXIC / Procedimento Realizado: Em Branco (EXAME TOXIC)"
				cMsgLog += CRLF + " - " + STR0047 //"Procedimento Realizado"
				lRet := .F.
			ElseIf !ExistCPO( "V2K", aExaAtes[ nCont, 3 ], 2 ) //<procRealizado>
				aAdd( aLogProc, STR0058 + AllTrim( aExaAtes[ nCont, 1 ] ) + " / " + STR0047 + ": " + aExaAtes[ nCont, 3 ] ) //"Exame: EXAME TOXIC / Procedimento Realizado: (EXAME TOXIC)"
				aAdd( aLogProc, STR0044 + STR0048 ) //"Valida��o: Deve ser um c�digo existente na tabela 27 do eSocial"
				cMsgLog += CRLF + " - " + STR0047 //"Procedimento Realizado"
				lRet := .F.
			EndIf
		Next nCont
	Else
		//"Atestado ASO: XXX / N�o existem exames relacionados ou para os exames relacionados n�o foram indicados o campo 'Proc. Realiz' ou ainda o(s) exame(s) relacionado n�o �(s�o) do tipo 'Ocupacional'"
		aAdd( aLogProc, cStrASO + " / " + STR0051 + CRLF + STR0060 )
		lRet := .F.
	EndIf

	//Volta para a filial do registro
	cFilAnt := cFilBkp

Return lRet