#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "MDTM004.CH"

STATIC aEfd	 := If( cPaisLoc == "BRA" , If( Findfunction( "fEFDSocial" ) , fEFDSocial() , { .F. , .F. , .F. } ) , { .F. , .F. , .F. } )
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//  _______           _______  _       _________ _______             _______  _______  _______    ___    _______  ---
// (  ____ \|\     /|(  ____ \( (    /|\__   __/(  ___  )           (  ____ \/ ___   )/ ___   )  /   )  (  __   ) ---
// | (    \/| )   ( || (    \/|  \  ( |   ) (   | (   ) |           | (    \/\/   )  |\/   )  | / /) |  | (  )  | ---
// | (__    | |   | || (__    |   \ | |   | |   | |   | |   _____   | (_____     /   )    /   )/ (_) (_ | | /   | ---
// |  __)   ( (   ) )|  __)   | (\ \) |   | |   | |   | |  (_____)  (_____  )  _/   /   _/   /(____   _)| (/ /) | ---
// | (       \ \_/ / | (      | | \   |   | |   | |   | |                 ) | /   _/   /   _/      ) (  |   / | | ---
// | (____/\  \   /  | (____/\| )  \  |   | |   | (___) |           /\____) |(   (__/\(   (__/\    | |  |  (__) | ---
// (_______/   \_/   (_______/|/    )_)   )_(   (_______)           \_______)\_______/\_______/    (_)  (_______) ---
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MDTM004
Rotina de Envio de Eventos - Condi��es Ambientais de Trabalho - Fatores de Risco ( S-2240 )
Realiza o envio das informa��es de Risco para o TAF

@return xRet - Caracter/L�gico - Se for gera��o de Xml retorna o cXml, sen�o retorna .T. ou .F. de acordo com as condi��es

@sample MDTM004( 'D MG 01' , '0000001' , 3 , 01/01/2019 , .T. )

@param cFilEnv		- Caracter	- Indica a filial de Envio das Informa��es
@param cMatricula	- Caracter	- Indica a matr�cula do Funcion�rio ao qual ser�o enviadas as informa��es
@param nOpcTAF		- Num�rico	- Indica a opera��o que est� sendo realizada (Retifica��o)
@param dDtRef		- Data		- Indica a data de refer�ncia que o registro utilizar�
@param lXml			- Caracter	- Indica se � gera��o de Xml

@author Luis Fellipy Bett
@since 27/11/2017
/*/
//------------------------------------------------------------------------------------------------------------------
Function MDTM004( cFilEnv , cMatricula , nOpcTAF , dDtRef , lXml , aLogProc , cMsgLog , lSched , lIncons )

	Local aArea		  := GetArea()
	Local nPosRis	  := 0
	Local cXml		  := ""
	Local cChvBus	  := ""
	Local aErros	  := {}
	Local aDados	  := {}
	Local aUsus		  := {}
	Local aRisExp	  := {}
	Local aEstab	  := {}
	Local xRet		  := .T.
	Local cTpDesc	  := SuperGetMv( "MV_NG2TDES" , .F. , "1" )
	Local cTpUsu	  := SuperGetMv( "MV_NG2REST" , .F. , "1" )
	Local lMiddleware := IIf( cPaisLoc == 'BRA' .And. Findfunction( "fVerMW" ), fVerMW(), .F. )
	Local cEfiEPC	  := "S"
	Local cAliasLau	  := GetNextAlias()
	Local lMDTA180	  := IsInCallStack("MDTA180")
	Local lMDTA090	  := IsInCallStack("MDTA090")
	Local cValRisco   := '{ |dData| ( TN0->TN0_DTRECO < SRA->RA_DEMISSA .Or. Empty( SRA->RA_DEMISSA ) ) .And. ' + ;
		  				'( TN0->TN0_DTELIM > dData .Or. Empty( TN0->TN0_DTELIM ) ) .And. ' + ;
						'!Empty( TN0->TN0_DTAVAL ) .And. MdtVldRis(dData) }'
	//Contadores
	Local nCont
	Local nRis
	Local nTar
	Local oModel
	Local oGridTN6

	Default lXml	 := .F.
	Default lIncons	 := .F.
	Default lSched	 := .F.
	Default aLogProc := {"","","",""}
	Default cMsgLog	 := ""

	//Variaveis de adequa��o das informa��es.
	Private lNT1219		:= SuperGetMv( "MV_TAFAMBE" , .F. , "2" ) $ "2/3" //Ambiente Produ��o Restrita
	Private dDtEsoc		:= SuperGetMv( "MV_NG2DTES" , .F. , SToD( "20190701" ) )
	Private cTpInsc
	Private cNrInsc
	Private cCpfTrab
	Private cNisTrab
	Private cCodUnic
	Private cCCusto

	Private aRisTrat 	:= {}
	Private aCAEPI		:= {}
	Private aTarefas	:= {}
	Private aAtivFunc	:= {}
	Private aAmbExp     := {}
	Private aRespAmb    := {}

	Private cCodLau		:= ""
	Private cCodUsu		:= ""
	Private cCargo		:= ""
	Private cFuncao		:= ""
	Private cFldDesc	:= ""
	Private cDscAtivDes	:= ""
	Private cResTaf		:= ""
	Private cCodCateg	:= ""
	Private dDtAdm		:= SToD( "" )
	Private nideOC		:= ""
	Private cMetErg		:= "" //Descri��o da metodologia utilizada para o levantamento dos riscos ergon�micos
	Private cTraco		:= ""
	Private cEsforc		:= ""
	Private lFirst		:= .T.
	Private lEsforc		:= .F.
	Private aRisMetErg	:= {}

	//Verifica se TAF est� preparado
	aRet := TafExisEsc( "S2240" )
	If !aRet[ 1 ] .Or. ( aRet[ 2 ] <> "2.4" .And. aRet[ 2 ] <> "2.4.02" .And. aRet[ 2 ] <> "2.5" )
		If !lSched
			Help( ' ', 1, STR0003, , STR0004, 2, 0, ,,,,, { STR0008 } ) //"Aten��o" ## "O ambiente n�o possui integra��o com o m�dulo do TAF e/ou a vers�o do TAF est� desatualizada!" ## "Favor verificar!"
		EndIf
		xRet := ".F."
	ElseIf !( NGCADICBASE( "TMA_ESOC", "A", "TMA", .F. ) )
		If !lSched
			Help( ' ', 1, STR0003, , STR0005, 2, 0, ,,,,, { STR0006 } ) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial!"
		EndIf
		xRet := ".F."
	ElseIf !( NGCADICBASE( "TNC_TPLOGR", "A", "TNC", .F. ) )
		If !lSched
			Help( ' ', 1, STR0003, , STR0005, 2, 0, ,,,,, { STR0006 } ) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial!"
		EndIf
		xRet := ".F."
	Else
		//Caso n�o tenha a Filial n�o tem como fazer a Integra��o
		If !Empty( cFilEnv )
			//Inicializa regua de processamento
			ProcRegua( 1 )

			dbSelectArea( "SRA" )
			dbSetOrder( 1 )
			dbSeek( xFilial( "SRA" ) + cMatricula )
			cCpfTrab	:= SRA->RA_CIC		// CPF do Funcion�rio
			cNisTrab	:= SRA->RA_PIS		// NIS do Funcion�rio
			cCodUnic	:= SRA->RA_CODUNIC	// C�digo �nico do Funcion�rio
			cCodCateg	:= SRA->RA_CATEFD 	// Categoria do Funcion�rio
			dDtAdm		:= SRA->RA_ADMISSA 	// Data de Admiss�o do Funcion�rio
			cCCusto		:= SRA->RA_CC		// Centro de Custo do Funcion�rio

			//Busca riscos expostos
			aRisExp     := MDTRetRis(dDtRef,,,,,,,.F.,,,,cValRisco)[1]

			//Busca as informa��es do Estabelecimento
			aEstab := MDTGetEst( cCCusto , cFilEnv )

			cTpInsc	:= aEstab[ 1 , 1 ]
			cNrInsc	:= aEstab[ 1 , 2 ]

			//Se a obrigatoriedade do eSocial estiver valendo para SST
			If dDataBase >= dDtEsoc
				If dDtRef < dDtEsoc
					dDtRef := dDtEsoc
				EndIf
			EndIf

			//Verifica se algum dos agentes a que o funcion�rio est� exposto � Calor
			For nCont := 1 To Len( aRisExp )
				If AllTrim( Posicione( "TN0" , 1 , xFilial( "TN0" ) + aRisExp[ nCont , 1 ] , "TN0_AGENTE" ) ) == "01.01.018" .Or. ;
				   AllTrim( Posicione( "TN0" , 1 , xFilial( "TN0" ) + aRisExp[ nCont , 1 ] , "TN0_AGENTE" ) ) == "01.01.023"
					lEsforc := .T.
					Exit
				EndIf
			Next nCont

			//Se for chamado pelo MDTA090 pega as tarefas diretamente da Grid
			If lMDTA090
				oModel := FWModelActive()
				oGridTN6 := oModel:GetModel( "TN6GRID" )

				For nTar := 1 To oGridTN6:Length()
					oGridTN6:GoLine( nTar )
					If oGridTN6:GetValue( "TN6_MAT" ) == cMatricula .And. ;
						aScan( aTarefas, { | x | x[1] == oModel:GetValue( "TN5MASTER" , "TN5_CODTAR" ) } ) == 0 .And. ;
						oGridTN6:GetValue( "TN6_DTINIC" ) <= dDtRef .And. ( oGridTN6:GetValue( "TN6_DTTERM" ) > dDtRef .Or. ;
						Empty( oGridTN6:GetValue( "TN6_DTTERM" ) ) ) .And. !oGridTN6:IsDeleted()

						aAdd( aTarefas , { oModel:GetValue( "TN5MASTER" , "TN5_CODTAR" ) } )
					EndIf
				Next nTar
			EndIf

			dbSelectArea( "TN6" )
			dbSetOrder( 2 )
			dbSeek( xFilial( "TN6" ) + cMatricula )
			While TN6->TN6_FILIAL = xFilial( "TN6" ) .And. TN6->TN6_MAT = cMatricula
				If TN6->TN6_DTINIC <= dDtRef .And. ( TN6->TN6_DTTERM > dDtRef .Or. Empty( TN6->TN6_DTTERM ) ) .And.;
					( !lMDTA090 .Or. TN6->TN6_CODTAR <> oModel:GetValue("TN5MASTER", "TN5_CODTAR") ) .And. ;
					aScan( aTarefas, { | x | x[1] == TN6->TN6_CODTAR } ) == 0

					aAdd( aTarefas , { TN6->TN6_CODTAR } )

				EndIf
				dbSkip()
			End

			//Busca o c�digo eSocial de todas as tarefas realizadas pelo funcion�rio
			dbSelectArea( "TN5" )
			dbSetOrder( 1 )
			For nCont := 1 To Len( aTarefas )
				If lMDTA090 .And. oModel:GetValue("TN5MASTER", "TN5_CODTAR") == aTarefas[ nCont , 1 ]
					If aScan( aAtivFunc, { | x | x[1] == oModel:GetValue( "TN5MASTER" , "TN5_ESOC" ) } ) == 0
						aAdd( aAtivFunc , { oModel:GetValue("TN5MASTER", "TN5_ESOC") } )
					EndIf
				Else
					dbSeek( xFilial( "TN5" ) + aTarefas[ nCont , 1 ] )
					If aScan( aAtivFunc , { | x | x[1] == TN5->TN5_ESOC } ) == 0
						aAdd( aAtivFunc , { TN5->TN5_ESOC } )
					EndIf
				EndIf
			Next nCont

			cFuncao	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CODFUNC"	)
			cCargo	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + cMatricula , "RA_CARGO"	)

			//Par�metro para verificar qual descri��o pegar
			If cTpDesc == "1" //Tarefa

				For nCont := 1 To Len( aTarefas )

					If lMDTA090 .And. AllTrim( aTarefas[ nCont , 1 ] ) == AllTrim( oModel:GetValue( "TN5MASTER" , "TN5_CODTAR" ) )
						cDscAtivDes += cTraco + AllTrim( MDTSubTxt( oModel:GetValue( "TN5MASTER" , "TN5_DESCRI" ) ) )
					Else
						cDscAtivDes += cTraco + AllTrim( MDTSubTxt( Posicione( "TN5" , 1 , xFilial( "TN5" ) + aTarefas[ nCont , 1 ] , "TN5_DESCRI" ) ) ) //TN5_FILIAL+TN5_CODTAR
					EndIf

					If Empty( cDscAtivDes )
						If lMDTA090 .And. AllTrim( aTarefas[ nCont , 1 ] ) == AllTrim( oModel:GetValue( "TN5MASTER" , "TN5_CODTAR" ) )
							cDscAtivDes += cTraco + AllTrim( MDTSubTxt( oModel:GetValue( "TN5MASTER" , "TN5_NOMTAR" ) ) )
						Else
							cDscAtivDes += cTraco + AllTrim( MDTSubTxt( Posicione( "TN5" , 1 , xFilial( "TN5" ) + aTarefas[ nCont , 1 ] , "TN5_NOMTAR" ) ) ) //TN5_FILIAL+TN5_CODTAR
						EndIf
					EndIf

					If lFirst
						cTraco := " / "
						lFirst := .F.
					EndIf

					If lEsforc
						If lMDTA090 .And. AllTrim( aTarefas[ nCont , 1 ] ) == AllTrim( oModel:GetValue( "TN5MASTER" , "TN5_CODTAR" ) )
							cEsforc := oModel:GetValue( "TN5MASTER" , "TN5_ESFORC" )
						Else
							cEsforc := Posicione( "TN5" , 1 , xFilial( "TN5" ) + aTarefas[ nCont , 1 ] , "TN5_ESFORC" )
						EndIf

						If cEsforc == "1"
							cDscAtivDes += " - " + Upper(STR0080) + Upper(STR0081) //"Grau de esfor�o da atividade: Leve"
						ElseIf cEsforc == "2"
							cDscAtivDes += " - " + Upper(STR0080) + Upper(STR0082) //"Grau de esfor�o da atividade: Moderada"
						ElseIf cEsforc == "3"
							cDscAtivDes += " - " + Upper(STR0080) + Upper(STR0083) //"Grau de esfor�o da atividade: Excessiva"
						EndIf
					EndIf
				Next nCont

			ElseIf cTpDesc == "2" //Cargo

				cFldDesc	:= Posicione( "SQ3" , 1 , xFilial( "SQ3" ) + cCargo , "Q3_DESCDET" )
				cDscAtivDes	:= AllTrim( MDTSubTxt( MSMM( cFldDesc , 80 ,,,,,, "SQ3" ,, "RDY" ) ) )

				If Empty( cDscAtivDes )
					cFldDesc	:= Posicione( "SQ3" , 1 , xFilial( "SQ3" ) + cCargo , "Q3_DESCSUM" )
					cDscAtivDes	:= AllTrim( MDTSubTxt( cFldDesc ) ) //Q3_FILIAL+Q3_CARGO
				EndIf

			ElseIf cTpDesc == "3" //Fun��o

				cFldDesc	:= Posicione( "SRJ" , 1 , xFilial( "SRJ" ) + cFuncao , "RJ_DESCREQ" )
				cDscAtivDes	:= AllTrim( MDTSubTxt( MSMM( cFldDesc , 80 ,,,,,, "SRJ" ,, "RDY" ) ) )

				If Empty( cDscAtivDes )
					cFldDesc	:= Posicione( "SRJ" , 1 , xFilial( "SRJ" ) + cFuncao , "RJ_DESC" )
					cDscAtivDes	:= AllTrim( MDTSubTxt( cFldDesc ) ) //RJ_FILIAL+RJ_FUNCAO
				EndIf

			ElseIf cTpDesc == "4" //Cargo e Tarefa

				cFldDesc	:= Posicione( "SQ3" , 1 , xFilial( "SQ3" ) + cCargo , "Q3_DESCDET" )
				cDscAtivDes	:= AllTrim( MDTSubTxt( MSMM( cFldDesc , 80 ,,,,,, "SQ3" ,, "RDY" ) ) )

				If Empty( cDscAtivDes )
					cFldDesc	:= Posicione( "SQ3" , 1 , xFilial( "SQ3" ) + cCargo , "Q3_DESCSUM" )
					cDscAtivDes	:= AllTrim( MDTSubTxt( cFldDesc ) ) //Q3_FILIAL+Q3_CARGO
				EndIf

				If cDscAtivDes <> ""
					cDscAtivDes += " / "
				EndIf

				For nCont := 1 To Len( aTarefas )

					If lMDTA090 .And. AllTrim( aTarefas[ nCont , 1 ] ) == AllTrim( oModel:GetValue( "TN5MASTER" , "TN5_CODTAR" ) )
						cDscAtivDes += cTraco + AllTrim( MDTSubTxt( oModel:GetValue( "TN5MASTER" , "TN5_DESCRI" ) ) )
					Else
						cDscAtivDes += cTraco + AllTrim( MDTSubTxt( Posicione( "TN5" , 1 , xFilial( "TN5" ) + aTarefas[ nCont , 1 ] , "TN5_DESCRI" ) ) ) //TN5_FILIAL+TN5_CODTAR
					EndIf

					If Empty( cDscAtivDes )
						If lMDTA090 .And. AllTrim( aTarefas[ nCont , 1 ] ) == AllTrim( oModel:GetValue( "TN5MASTER" , "TN5_CODTAR" ) )
							cDscAtivDes += cTraco + AllTrim( MDTSubTxt( oModel:GetValue( "TN5MASTER" , "TN5_NOMTAR" ) ) )
						Else
							cDscAtivDes += cTraco + AllTrim( MDTSubTxt( Posicione( "TN5" , 1 , xFilial( "TN5" ) + aTarefas[ nCont , 1 ] , "TN5_NOMTAR" ) ) ) //TN5_FILIAL+TN5_CODTAR
						EndIf
					EndIf

					If lFirst
						cTraco := " / "
						lFirst := .F.
					EndIf

					If lEsforc
						If lMDTA090 .And. AllTrim( aTarefas[ nCont , 1 ] ) == AllTrim( oModel:GetValue( "TN5MASTER" , "TN5_CODTAR" ) )
							cEsforc := oModel:GetValue( "TN5MASTER" , "TN5_ESFORC" )
						Else
							cEsforc := Posicione( "TN5" , 1 , xFilial( "TN5" ) + aTarefas[ nCont , 1 ] , "TN5_ESFORC" )
						EndIf

						If cEsforc == "1"
							cDscAtivDes += " - " + Upper(STR0080) + Upper(STR0081) //"Grau de esfor�o da atividade: Leve"
						ElseIf cEsforc == "2"
							cDscAtivDes += " - " + Upper(STR0080) + Upper(STR0082) //"Grau de esfor�o da atividade: Moderada"
						ElseIf cEsforc == "3"
							cDscAtivDes += " - " + Upper(STR0080) + Upper(STR0083) //"Grau de esfor�o da atividade: Excessiva"
						EndIf
					EndIf
				Next nCont

			EndIf

			//Par�metro que indica que tipo de Usu�rio Respons�vel ser� enviado ao TAF
			If cTpUsu == "1" //M�dico do Trabalho
				cResTaf := "1"
			ElseIf cTpUsu == "2" //Engenheiro do Trabalho
				cResTaf := "4"
			ElseIf cTpUsu == "3" //Ambos
				cResTaf := "1/4"
			ElseIf cTpUsu == "4" //Todos
				cResTaf := "1/2/3/4/5/6/7/8/9/A/B/C"
			Endif

			For nRis := 1 To Len( aRisExp ) //Percorre os Riscos a que o funcion�rio est� exposto

				dbSelectArea( "TN0" )
				dbSetOrder( 1 )
				dbSeek( xFilial( "TN0" ) + aRisExp[ nRis , 1 ] )

				//Se for exclus�o e o funcion�rio estiver exposto ao Risco que estou excluindo, n�o envio o Risco ao eSocial
				If !( lMDTA180 .And. nOpcTAF == 5 .And. TN0->TN0_NUMRIS == M->TN0_NUMRIS )

					If ( aScan( aAmbExp , { | x | x[ 2 ] == TN0->TN0_CODAMB } ) == 0 )
						aAdd( aAmbExp , { TN0->TN0_NUMRIS , AllTrim( TN0->TN0_CODAMB ) } )
					EndIf

					//Epi deve estar entregue ao funcion�rio e vinculado ao risco
					MDTEpiTAF( TN0->TN0_NUMRIS , cMatricula , @aCAEPI )

					dbSelectArea( "TO9" )
					dbSetOrder( 1 )
					dbSeek( xFilial( "TO9" ) + TN0->TN0_NUMRIS )
					While xFilial( "TO9" ) == TO9->TO9_FILIAL .And. TO9->TO9_NUMRIS == TN0->TN0_NUMRIS
						If TO9->TO9_EFIEPC == "2"
							cEfiEPC := "N"
							Exit
						EndIf
						dbSkip()
					End

					If ( nPosRis := aScan( aRisTrat,  { | x | x[2] == Posicione( "TMA" , 1 , xFilial( "TMA" ) + TN0->TN0_AGENTE , "TMA_ESOC" ) } ) ) == 0
						aAdd( aRisTrat , { AllTrim( TN0->TN0_NUMRIS ) ,;
											Posicione( "TMA" , 1 , xFilial( "TMA" ) + TN0->TN0_AGENTE , "TMA_ESOC" ) ,;
											Posicione( "TMA" , 1 , xFilial( "TMA" ) + TN0->TN0_AGENTE , "TMA_AVALIA" ) ,;
											cValToChar( TN0->TN0_QTAGEN ) ,;
											cValToChar( Posicione( "TLK" , 1 , xFilial( "TLK" ) + TN0->TN0_AGENTE , "TLK_DEQTDE") ) ,;
											AllTrim( TN0->TN0_UNIMED ) ,;
											AllTrim( MDTSubTxt( TN0->TN0_TECUTI ) )  ,;
											If( TN0->TN0_ATISAL == "2" , "S" , "N" ) ,;
											If( TN0->TN0_ATIPER == "2" , "S" , "N" ) ,;
											If( TN0->TN0_APOESP == "1" , "S" , "N" ) ,;
											If( Empty( TN0->TN0_EPC ) , "0" , If( TN0->TN0_EPC == "1" , "2" , "1" ) ) ,;
											cEfiEPC ,;
											If( Empty( TN0->TN0_NECEPI ) , "0" , If( TN0->TN0_NECEPI == "1" , "2" , "1" ) ) ,;
											aClone( aCAEPI ) ,;
											AllTrim( TN0->TN0_AGENTE ) ,;
											Posicione( "TMA" , 1 , xFilial( "TMA" ) + TN0->TN0_AGENTE , "TMA_DESCRI" ) } )
					Else
						//Caso exista mais de um Risco com um mesmo fator de risco e caso ele tenha maior quantidade de exposi��o,
						// atribuo os valores dele para serem enviados
						If Val( aRisTrat[ nPosRis , 4 ] ) < TN0->TN0_QTAGEN
							aRisTrat[ nPosRis , 1  ] := AllTrim( TN0->TN0_NUMRIS )
							aRisTrat[ nPosRis , 3  ] := Posicione( "TMA" , 1 , xFilial( "TMA" ) + TN0->TN0_AGENTE , "TMA_AVALIA" )
							aRisTrat[ nPosRis , 4  ] := cValToChar( TN0->TN0_QTAGEN )
							aRisTrat[ nPosRis , 5  ] := cValToChar( Posicione( "TLK" , 1 , xFilial( "TLK" ) + TN0->TN0_AGENTE , "TLK_DEQTDE") )
							aRisTrat[ nPosRis , 6  ] := AllTrim( TN0->TN0_UNIMED )
							aRisTrat[ nPosRis , 7  ] := AllTrim( MDTSubTxt( TN0->TN0_TECUTI ) )
							aRisTrat[ nPosRis , 8  ] := If( TN0->TN0_ATISAL == "2" , "S" , "N" )
							aRisTrat[ nPosRis , 9  ] := If( TN0->TN0_ATIPER == "2" , "S" , "N" )
							aRisTrat[ nPosRis , 10 ] := If( TN0->TN0_APOESP == "1" , "S" , "N" )
							aRisTrat[ nPosRis , 11 ] := If( Empty( TN0->TN0_EPC ) , "0" , If( TN0->TN0_EPC == "1" , "2" , "1" ) )
							aRisTrat[ nPosRis , 13 ] := If( Empty( TN0->TN0_NECEPI ) , "0" , If( TN0->TN0_NECEPI == "1" , "2" , "1" ) )
							aRisTrat[ nPosRis , 15 ] := AllTrim( TN0->TN0_AGENTE )
							aRisTrat[ nPosRis , 16 ] := Posicione( "TMA" , 1 , xFilial( "TMA" ) + TN0->TN0_AGENTE , "TMA_DESCRI" )
						EndIf
						For nCont := 1 To Len( aCAEPI )
							If ( aScan( aRisTrat[ nPosRis, 14 ], { | x | x[ 1 ] == aCAEPI[ nCont, 1 ] } ) ) == 0
								aAdd( aRisTrat[ nPosRis, 14 ], aClone( aCAEPI[ nCont ] ) )
							EndIf
						Next nCont
						If cEfiEPC == "N"
							aRisTrat[ nPosRis, 12 ] := cEfiEPC
						EndIf
					EndIf

					dbSelectArea( "TMA" )
					dbSetOrder( 1 )
					dbSeek( xFilial( "TMA" ) + TN0->TN0_AGENTE )
					If TMA->TMA_GRISCO == "4"
						cMetErg += TMA->TMA_METERG
						aAdd( aRisMetErg , { TN0->TN0_AGENTE , TMA->TMA_METERG } )
					EndIf

				EndIf
			Next nRis

			BeginSQL Alias cAliasLau
				SELECT TO0.TO0_CODUSU
					FROM %table:TO0% TO0
					WHERE TO0.TO0_FILIAL = %xFilial:TO0% AND
							TO0.TO0_DTINIC <= %exp:dDtRef% AND
							TO0.TO0_DTVALI >  %exp:dDtRef% OR
							TO0.TO0_DTVALI = '' AND
							TO0.%NotDel%
			EndSQL

			dbSelectArea( cAliasLau )

			While ( cAliasLau )->( !EoF() )
				If ( aScan( aUsus , { | x | x[ 1 ] == ( cAliasLau )->TO0_CODUSU } ) == 0 )
					aAdd( aUsus , { ( cAliasLau )->TO0_CODUSU } )
				EndIf
				( cAliasLau )->( dbSkip() )
			End

			( cAliasLau )->( dbCloseArea() )

			dbSelectArea( "TMK" ) //Usu�rios
			dbSetOrder( 1 )

			For nCont := 1 To Len( aUsus )
				If dbSeek( xFilial( "TMK" ) + aUsus[ nCont , 1 ] ) .And.;
					TMK->TMK_INDFUN $ cResTaf .And. ;
					TMK->TMK_DTINIC <= dDtRef .And. ;
					TMK->TMK_RESAMB == '1'

					If "CRM" $ TMK->TMK_ENTCLA
						nideOC := "1"
					ElseIf "CREA" $ TMK->TMK_ENTCLA
						nideOC := "4"
					Else
						nideOC := "9"
					EndIf

					aAdd( aRespAmb , { TMK->TMK_CODUSU , TMK->TMK_CIC , AllTrim( TMK->TMK_NIT ) , AllTrim( MDTSubTxt( TMK->TMK_NOMUSU ) ) , ;
									   nideOC , TMK->TMK_ENTCLA , AllTrim( TMK->TMK_NUMENT ) , TMK->TMK_UF } )
				EndIf
			Next nCont

			If lIncons
				fInconsis( @aLogProc , @cMsgLog , dDtRef , cFilEnv )
			Else
				cXml := fCarrRis( aRisExp, cFilEnv, cValToChar( nOpcTAF ), cMatricula, dDtRef, lXml, lMiddleware )

				If lXml
					fInconsis( @aLogProc , @cMsgLog , dDtRef , cFilEnv )

					If Len( aLogProc ) > 4
						fMakeLog( { aLogProc } , { STR0035 } , Nil , Nil , "MDTM004" , OemToAnsi( STR0036 ) , "M" , "P" , , .F. ) //"Monitoramento Envio de Eventos - TAF" ## "Log de Ocorrencias - Fatores de Risco"

						Help(' ',1,STR0003,,STR0076 + CRLF + cMsgLog,2,0,,,,,,{STR0077}) //"O Xml possui inconsist�ncias de acordo com o formato padr�o do eSocial" ## "Favor ajustar os valores expostos no relat�rio!"

						xRet := .F.
					Else
						xRet := cXml
					EndIf
				Else
					Begin Transaction

						//Caso o envio seja atrav�s do Middleware
						If lMiddleware

							If MDTVerTSVE( cCodCateg ) //Caso seja Trabalhador Sem V�nculo Estatut�rio
								cChvBus := AllTrim( cCpfTrab ) + AllTrim( cCodCateg ) + DToS( dDtAdm ) + DToS( dDtRef )
							Else
								cChvBus := AllTrim( cCodUnic ) + DToS( dDtRef )
							EndIf

							//Envia o evento ao eSocial atrav�s do Middleware
							MDTEnvMid( cFilEnv, cMatricula, "S2240", cChvBus, cXml, nOpcTAF )

						Else
							//Enviar como parametro a filial do Protheus na posi��o 1 e o XML na posi��o 2
							aErros := TafPrepInt( cEmpAnt , cFilEnv , cXml , , "1" , "S2240" )

							If Len( aErros ) <= 0
								If !lSched
									//Carrega array de controle de carga
									aAdd( aDados , OemToAnsi( STR0009 ) + " " + cCpfTrab + " " + OemToAnsi( STR0010 ) ) //"Fatores de Risco"###"Enviado ao TAF com sucesso."
								EndIf
								xRet := .T.
							Else
								If !lSched
									aAdd( aDados , OemToAnsi( STR0009 ) + " " + cCpfTrab + " " + OemToAnsi( STR0011 ) + ": " + aErros[ 1 ] ) //"Fatores de Risco"###"Falha no envio ao TAF:"
									Help(' ',1,STR0003,,STR0078 + CRLF + aErros[ 1 ],2,0,,,,,,{STR0079}) //"Existem inconsist�ncias na integra��o com o TAF!" ## "Para tentativa de reenvio favor manipular o registro!"
								EndIf
								xRet := .F.
							EndIf

							//Incrementa regua
							If !IsBlind() //Caso houver intera��o gr�fica
								IncProc( OemToAnsi( STR0013 ) + " " + cCpfTrab ) //"Gerando o registro de:"
							EndIf
						EndIf

					End Transaction
				EndIf
			EndIf
		Else
			If !lSched
				Help(' ',1,STR0003,,STR0007,2,0,,,,,,{STR0008}) // Aten��o ## "A informa��o da filial de envio ao eSocial est� vazia!" ## "Favor verificar!"
			EndIf
			xRet := .F.
		EndIf

	EndIf

	RestArea( aArea )

Return xRet

//---------------------------------------------------------------------
/*/{Protheus.doc} fCarrRis

Carrega os Riscos

@return cXml Caracter Estrutura XML a ser enviada para o TAF

@sample fCarrRis( aArrayRis , "D MG 01" , 3 , "00000001" , 01/01/2019 , "00001" )

@param aRisExp		- Array		- Array com os Riscos a que o funcion�rio est� exposto
@param cFilEnv		- Caracter	- Filial de envio
@param nOpcTAF		- Num�rico	- Indica a opera��o realizada
@param cMatricula	- Caracter	- matr�cula do Funcion�rio
@param dDtRef		- Data		- Data de refer�ncia que o sistema considera como in�cio de exposi��o de cada registro

@author Luis Fellipy Bett
@since 30/08/2018
/*/
//---------------------------------------------------------------------
Static Function fCarrRis( aRisExp, cFilEnv, nOpcTAF, cMatricula, dDtRef, lXml, lMiddleware )

	Local cXml		:= ""
	Local cChvBus	:= ""
	Local lTSVE		:= MDTVerTSVE( cCodCateg )

	//Contadores
	Local nResp, nFat, nCA, nRis, nAtiv

	IIf( nOpcTAF == "5", nOpcTAF := "4", )

	If lTSVE //Caso seja Trabalhador Sem V�nculo Estatut�rio
		cChvBus := AllTrim( cCpfTrab ) + AllTrim( cCodCateg ) + DToS( dDtAdm ) + DToS( dDtRef )
	Else
		cChvBus := AllTrim( cCodUnic ) + DToS( dDtRef )
	EndIf

	//Cria o cabe�alho do Xml com o ID, informa��es do Evento e Empregador
	MDTGerCabc( @cXml, "S2240", cTpInsc, cNrInsc, lMiddleware, lXml, nOpcTAF, cChvBus )

	//FUNCIONARIO
	cXml += 		'<ideVinculo>'
	cXml += 			'<cpfTrab>'		+ cCpfTrab + '</cpfTrab>'
	If !( cCodCateg $ "901/903/904" )
		cXml +=			'<nisTrab>'		+ AllTrim( cNisTrab ) + '</nisTrab>'
	EndIf
	If !lTSVE //Se n�o for Trabalhador Sem V�nculo Estatut�rio
		cXml +=			'<matricula>'	+ cCodUnic  + '</matricula>'
	Else
		cXml +=			'<codCateg>'	+ cCodCateg + '</codCateg>'
	EndIf
	cXml += 		'</ideVinculo>'

	//MONITORAMENTO DA SA�DE DO TRABALHADOR
	cXml += 		'<infoExpRisco>'
	cXml += 			'<dtIniCondicao>' + MDTAjsData( dDtRef, lMiddleware ) + '</dtIniCondicao>' //Obrigat�rio
	For nRis := 1 To Len( aAmbExp )
		cXml += 		'<infoAmb>'
		cXml += 			'<codAmb>' + aAmbExp[ nRis, 2 ] + '</codAmb>' //Obrigat�rio
		cXml += 		'</infoAmb>'
	Next nRis
	cXml += 			'<infoAtiv>'
	cXml += 				'<dscAtivDes>'	+ Upper( cDscAtivDes ) + '</dscAtivDes>' //Obrigat�rio
	If Len( aAtivFunc ) > 0
		For nAtiv := 1 To Len( aAtivFunc )
			cXml += 		'<ativPericInsal>'
			cXml += 			'<codAtiv>'	+ aAtivFunc[ nAtiv, 1 ] + '</codAtiv>' //Obrigat�rio
			cXml += 		'</ativPericInsal>'
		Next nAtiv
	Else
		cXml += 			'<ativPericInsal>'
		cXml += 				'<codAtiv>99.999</codAtiv>'
		cXml += 			'</ativPericInsal>'
	EndIf
	cXml += 			'</infoAtiv>'
	If Len( aRisTrat ) > 0
		For nFat := 1 To Len( aRisTrat )
			cXml += 	'<fatRisco>'
			cXml += 		'<codFatRis>'		+ aRisTrat[ nFat , 2 ]		+ '</codFatRis>' //Obrigat�rio
			If lNT1219 .And. aRisTrat[ nFat , 2 ] $ "01.01.999/02.01.999/03.01.999/04.01.999/04.02.999/04.03.999/04.04.999/04.05.999/05.01.999"
				cXml +=		'<dscFatRisc>'		+ aRisTrat[ nFat , 16 ]		+ '</dscFatRisc>'
			EndIf
			cXml += 		'<tpAval>'			+ aRisTrat[ nFat , 3 ]		+ '</tpAval>' //Obrigat�rio
			If aRisTrat[ nFat , 3 ] == "1" //Se o tipo de avalia��o for quantitativa
				cXml += 	'<intConc>'			+ aRisTrat[ nFat , 4 ]		+ '</intConc>'
				If aRisTrat[ nFat , 2 ] $ "01.01.014/01.01.018/02.01.687/02.01.788"
					cXml +=	'<limTol>'			+ aRisTrat[ nFat , 5 ]		+ '</limTol>'
				EndIf
				cXml += 	'<unMed>'			+ aRisTrat[ nFat , 6 ]		+ '</unMed>'
				cXml += 	'<tecMedicao>'		+ aRisTrat[ nFat , 7 ]		+ '</tecMedicao>'
			EndIf
			cXml += 		'<insalubridade>'	+ aRisTrat[ nFat , 8 ]		+ '</insalubridade>'
			cXml += 		'<periculosidade>'	+ aRisTrat[ nFat , 9 ]		+ '</periculosidade>'
			If ( !Empty( cCodUnic ) .And. cCodCateg <> "104" ) .Or. ( cCodCateg $ "201/202/731/734/738" )
				cXml += 	'<aposentEsp>'		+ aRisTrat[ nFat , 10 ]		+ '</aposentEsp>'
			EndIf
			cXml += 		'<epcEpi>'
			cXml += 			'<utilizEPC>'	+ aRisTrat[ nFat , 11 ]		+ '</utilizEPC>' //Obrigat�rio
			If aRisTrat[ nFat , 11 ] == "2"
				cXml +=			'<eficEpc>'		+ aRisTrat[ nFat , 12 ]		+ '</eficEpc>'
			EndIf
			cXml += 			'<utilizEPI>'	+ aRisTrat[ nFat , 13 ]		+ '</utilizEPI>' //Obrigat�rio
			If aRisTrat[ nFat , 13 ] == "2"
				aCAEPI := aClone( aRisTrat[ nFat , 14 ] )
				For nCA := 1 To Len( aCAEPI )
					cXml += 			'<epi>'
					cXml += 				'<caEPI>'			+ aCAEPI[ nCA , 1 ] + '</caEPI>'
					cXml += 				'<dscEPI>'			+ aCAEPI[ nCA , 2 ] + '</dscEPI>'
					cXml += 				'<eficEpi>'			+ aCAEPI[ nCA , 3 ] + '</eficEpi>'
					cXml += 				'<medProtecao>'		+ aCAEPI[ nCA , 4 ] + '</medProtecao>'
					cXml += 				'<condFuncto>'		+ aCAEPI[ nCA , 5 ] + '</condFuncto>'
					cXml += 				'<usoInint>'		+ aCAEPI[ nCA , 6 ] + '</usoInint>'
					cXml += 				'<przValid>'		+ aCAEPI[ nCA , 7 ] + '</przValid>'
					cXml += 				'<periodicTroca>'	+ aCAEPI[ nCA , 8 ] + '</periodicTroca>'
					cXml += 				'<higienizacao>'	+ aCAEPI[ nCA , 9 ] + '</higienizacao>'
					cXml += 			'</epi>'
				Next nEpi
			EndIf
			cXml += 		'</epcEpi>'
			cXml += 	'</fatRisco>'
		Next nFat
	Else
		cXml += 		'<fatRisco>'
		cXml += 			'<codFatRis>09.01.001</codFatRis>'
		cXml += 			'<tpAval>1</tpAval>'
		cXml += 			'<insalubridade>N</insalubridade>'
		cXml += 			'<periculosidade>N</periculosidade>'
		cXml += 			'<aposentEsp>N</aposentEsp>'
		cXml += 			'<epcEpi>'
		cXml += 				'<utilizEPC>0</utilizEPC>'
		cXml += 				'<eficEpc>S</eficEpc>'
		cXml += 				'<utilizEPI>0</utilizEPI>'
		cXml += 			'</epcEpi>'
		cXml += 		'</fatRisco>'
	EndIf
	For nResp := 1 To Len( aRespAmb )
		cXml += 		'<respReg>'
		cXml += 			'<cpfResp>'	+ aRespAmb[ nResp , 2 ]	+ '</cpfResp>' //Obrigat�rio
		cXml += 			'<nisResp>'	+ aRespAmb[ nResp , 3 ]	+ '</nisResp>' //Obrigat�rio
		cXml += 			'<nmResp>'	+ aRespAmb[ nResp , 4 ]	+ '</nmResp>' //Obrigat�rio
		cXml += 			'<ideOC>'	+ aRespAmb[ nResp , 5 ]	+ '</ideOC>' //Obrigat�rio
		If aRespAmb[ nResp , 5 ] == "9"
			cXml += 		'<dscOC>'	+ aRespAmb[ nResp , 6 ]	+ '</dscOC>'
		EndIf
		cXml += 			'<nrOC>'	+ aRespAmb[ nResp , 7 ]	+ '</nrOC>' //Obrigat�rio
		cXml += 			'<ufOC>'	+ aRespAmb[ nResp , 8 ]	+ '</ufOC>' //Obrigat�rio
		cXml += 		'</respReg>'
	Next nResp
	cXml += 			'<obs>'
	If !Empty( cMetErg )
		cXml +=				'<metErg>'		+ cMetErg	+ '</metErg>'
	EndIf
	cXml += 			'</obs>'
	cXml += 		'</infoExpRisco>'
	cXml += 	'</evtExpRisco>'
	cXml += '</eSocial>'

Return cXml

//---------------------------------------------------------------------
/*/{Protheus.doc} fInconsis

Carrega Inconsist�ncias (Se houver)

@return lRet L�gico Retorna falso caso haja alguma inconsist�ncia

@sample fInconsis( aLogProc , "cMsgLog" , 01/01/2019 )

@param aLogProc	- Array		- Array que ir� receber os Logs
@param cMsgLog	- Caracter	- Variavel que recebera a mensagem de Log
@param dDtRef	- Data		- Data de refer�ncia que o sistema considera como in�cio de exposi��o de cada registro

@author Luis Fellipy Bett
@since 30/08/2018
/*/
//---------------------------------------------------------------------
Static Function fInconsis( aLogProc , cMsgLog , dDtRef , cFilEnv )

	Local cFilBkp		:= cFilAnt //Salva a filial atual
	Local lRet 			:= .F.
	Local nCampos		:= 0
	Local nCont			:= 0
	Local nEpc			:= 0
	Local nEpi			:= 0
	Local aVldValues	:= {}
	Local cStrFunc		:= STR0064 + AllTrim( SRA->RA_MAT ) + " - " + AllTrim( SRA->RA_NOME ) // Funcion�rio: 000001 - ALFREDINHO JOS�
	Local cStrRis		:= STR0065 + TN0->TN0_NUMRIS
	Local lTSVE			:= MDTVerTSVE( cCodCateg )

	//Condi��es
	Local bCond1		:= { || !( cCodCateg $ "901/903/904" ) }
	Local bCond2		:= { || Posicione( "C9V" , 3 , xFilial( "C9V" , cFilEnv ) + cCpfTrab , "C9V_NOMEVE" ) == "S2300" } //S� deve ser preenchido se tiver sido cadastrado no S-2300

	//Valida��es
	Local bValid1		:= { || cTpInsc $ "1/2" }
	Local bValid2		:= { || If( cTpInsc $ "1/2" , If( cTpInsc == "1" , CGC( cNrInsc ) , CHKCPF( cNrInsc ) ) , .T. ) }
	Local bValid3		:= { || CHKCPF( cCpfTrab ) }
	Local bValid4		:= { || If( lTSVE , ExistCPO( "C9V" , cCpfTrab , 3 ) , ExistCPO( "C9V" , cCodUnic , 11 ) ) } //"Deve ser a mesma matr�cula informada no evento S-2200"
	Local bValid5		:= { || ExistCPO( "C87" , cCodCateg , 2 ) }
	Local bValid6

	Private dDtAux		:= dDtRef

	bValid6 := { || dDtAux >= SRA->RA_ADMISSA .And. dDtAux <= dDataBase .And. dDtAux >= dDtEsoc }
	cDtEsoc := DToS( dDtEsoc )
	cDtEsoc := "(" + SubStr( cDtEsoc , 7 , 2 ) + "/" + SubStr( cDtEsoc , 5 , 2 ) + "/" + SubStr( cDtEsoc , 1 , 4 ) + ")"

	//Busca a filial de envio a ser considerada nas valida��es
	cFilAnt := cFilEnv

	// --- Estrutura do Array de envio das inconsist�ncias ---
	// Posi��o 1 - Campo a ser verificado
	// Posi��o 2 - String indicando o registro chave que est� sendo analisado junto a um " / " para impress�o no Log. Ex: Atestado ASO: 012354632 /
	// Posi��o 3 - String indicando o t�tulo do campo que ser� analisado. Ex: Data Acidente
	// Posi��o 4 - Bloco de C�digo com a condi��o para verifica��o do campo
	// Posi��o 5 - Bloco de C�digo com a valida��o do campo
	// Posi��o 6 - Descri��o da valida��o do campo para envio ao TAF
	// Obs: Na impress�o do Log a fun��o juntar� os dados da 2�, 3� e 1� coluna, sucessivamente. Ex: Atestado ASO: 012354632 / Data Emiss�o ASO: 28/10/2017
	// -------------------------------------------------------

	aVldValues := {;
		{ cTpInsc		, cStrFunc + " / " , STR0017 , { || .T. }	, bValid1		, STR0024	} ,; //"Atestado ASO: X / Tipo Inscri��o Lota��o: ## Deve ser igual a 1 (CNPJ) ou 2 (CPF)"
		{ cNrInsc		, cStrFunc + " / " , STR0018 , { || .T. }	, bValid2		, STR0025	} ,; //"Atestado ASO: X / Incri��o Lota��o: ## Deve ser um n�mero de CNPJ ou CPF v�lido"
		{ cCpfTrab 		, cStrFunc + " / " , STR0019 , { || .T. }	, bValid3		, STR0026	} ,; //"Atestado ASO: X / C.P.F.: ## Deve ser um n�mero de CPF v�lido"
		{ cNisTrab 		, cStrFunc + " / " , STR0020 , bCond1		, { || .T. }	, 			} ,; //"Atestado ASO: X / : N.I.S.:
		{ cCodUnic		, cStrFunc + " / " , STR0021 , { || .T. }	, bValid4		, STR0027	} ,; //"Atestado ASO: X / Matr�cula: ## Deve ser a mesma matr�cula informada no evento S-2200"
		{ cCodCateg		, cStrFunc + " / " , STR0022 , bCond2		, bValid5		, STR0028	} ,; //"Atestado ASO: X / Categoria Funcion�rio: ## Deve existir na tabela 01 do eSocial"
		{ dDtRef 		, ""			   , STR0030 , { || .T. }	, bValid6		, STR0031 + CRLF + STR0069 + cDtEsoc } } //"Data In�cio Atividades: ## Deve ser uma data posterior a data de admiss�o do funcion�rio"

	lRet := MDTInconEsoc( aVldValues , @aLogProc , @cMsgLog )

	//Ambiente de trabalho do funcion�rio
	If Len( aAmbExp ) > 0
		For nCont := 1 To Len( aAmbExp )
			If Empty( aAmbExp[ nCont , 2 ] )
				aAdd( aLogProc , STR0065 + aAmbExp[ nCont , 1 ] + " / " + STR0032 + ": " + STR0029 ) //Risco: 000001 / C�digo Ambiente Exposi��o: Em Branco
				cMsgLog += CRLF + STR0065 + aAmbExp[ nCont , 1 ] + " / " + STR0032 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			Else
				Help := .T.
				If !ExistCPO( "T04" , aAmbExp[ nCont , 2 ] , 2 )
					aAdd( aLogProc , STR0065 + aAmbExp[ nCont , 1 ] + " / " + STR0032 + ": " + aAmbExp[ nCont , 2 ] )
					aAdd( aLogProc , STR0023 + STR0050 ) //"Valida��o: Deve ser um c�digo existente na Tabela S-1060"
					cMsgLog += CRLF + STR0065 + aAmbExp[ nCont , 1 ] + " / " + STR0032 + ": " + aAmbExp[ nCont , 2 ]
					cMsgLog += CRLF + STR0023 + STR0050
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf
				Help := .F.
			EndIf
		Next nCont
	ElseIf Len( aRisTrat ) > 0 //Se o funcion�rio estiver exposto a algum Risco e n�o tiver nenhum ambiente vinculado
		aAdd( aLogProc , cStrFunc + " / " + STR0051 ) //"N�o foi vinculado nenhum ambiente de trabalho ao funcion�rio!"
		lRet := .F.
		aAdd( aLogProc , '' )
	EndIf

	//Atividades desempenhadas pelo funcion�rio
	If Len( aAtivFunc ) > 0
		For nCont := 1 To Len( aAtivFunc )
			If Empty( aAtivFunc[ nCont , 1 ] )
				aAdd( aLogProc , cStrFunc + " / " + STR0033 + ": " + STR0029 )
				aAdd( aLogProc , STR0023 + STR0086 ) //"Valida��o: Deve ser vinculada uma atividade ao funcion�rio que tenha o campo C�d. eSoc. preenchido"
				cMsgLog += CRLF + cStrFunc + " / " + STR0033 + ": " + STR0029
				cMsgLog += CRLF + STR0023 + STR0086
				lRet := .F.
				aAdd( aLogProc , '' )
			Else
				Help := .T.
				If !ExistCPO( "V2L" , aAtivFunc[ nCont , 1 ] , 2 )
					aAdd( aLogProc , cStrFunc + " / " + STR0033 + ": " + aAtivFunc[ nCont , 1 ] )
					aAdd( aLogProc , STR0023 + STR0052 ) //"Valida��o: Deve ser um c�digo existente na Tabela 28 do eSocial"
					cMsgLog += CRLF + cStrFunc + " / " + STR0033 + ": " + aAtivFunc[ nCont , 1 ]
					cMsgLog += CRLF + STR0023 + STR0052
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf
				Help := .F.
			EndIf
		Next nCont
	EndIf

	//Fatores de Risco que o funcion�rio est� exposto
	If Len( aRisTrat ) > 0
		For nCont := 1 To Len( aRisTrat )

			If Empty( aRisTrat[ nCont , 2 ] ) //<codFatRis>
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0034 + ": " + STR0029 )
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0034 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			ElseIf aRisTrat[ nCont , 2 ] <> "09.01.001"
				Help := .T.
				If !ExistCPO( "T3E" , aRisTrat[ nCont , 2 ] , 2 ) //Deve existir na Tabela 23 do eSocial
					aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0034 + ": " + aRisTrat[ nCont , 2 ] )
					aAdd( aLogProc , STR0023 + STR0054 ) //"Deve existir na Tabela 23 do eSocial
					cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0034 + ": " + aRisTrat[ nCont , 2 ]
					cMsgLog += CRLF + STR0023 + STR0054
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf
				Help := .F.
			EndIf

			If lNT1219 .And. Empty( aRisTrat[ nCont , 16 ] ) .And. aRisTrat[ nCont , 2 ] $ "01.01.999/02.01.999/03.01.999/04.01.999/04.02.999/04.03.999/04.04.999/04.05.999/05.01.999" //</dscFatRisc>
				aAdd( aLogProc , STR0075 + aRisTrat[ nCont , 15 ] + " / " + STR0085 + ": " + STR0029 ) //"Agente: 001 / Descri��o Fator de Risco: Em Branco"
				cMsgLog += CRLF + STR0075 + aRisTrat[ nCont , 15 ] + " / " + STR0085 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			If Empty( aRisTrat[ nCont , 3 ] ) //</tpAval>
				aAdd( aLogProc , STR0075 + aRisTrat[ nCont , 15 ] + " / " + STR0035 + ": " + STR0029 ) //"Agente: 001 / Tipo Avalia��o: Em Branco"
				cMsgLog += CRLF + STR0075 + aRisTrat[ nCont , 15 ] + " / " + STR0035 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			ElseIf !( aRisTrat[ nCont , 3 ] $ "1/2" )
				aAdd( aLogProc , STR0075 + aRisTrat[ nCont , 15 ] + " / " + STR0035 + ": " + aRisTrat[ nCont , 3 ] ) //"Agente: 001 / Tipo Avalia��o: 3
				aAdd( aLogProc , STR0023 + STR0055 ) //"Deve ser igual a 1 (Quantitativo) ou 2 (Qualitativo)"
				cMsgLog += CRLF + STR0075 + aRisTrat[ nCont , 15 ] + " / " + STR0035 + ": " + aRisTrat[ nCont , 3 ]
				cMsgLog += CRLF + STR0023 + STR0055
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			If Empty( aRisTrat[ nCont , 4 ] ) .And. aRisTrat[ nCont , 3 ] == "1" //<intConc>
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0036 + ": " + STR0029 )
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0036 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			If Empty( aRisTrat[ nCont , 5 ] ) .And. aRisTrat[ nCont , 3 ] == "1" .And. aRisTrat[ nCont , 2 ] $ "01.01.014/01.01.018/02.01.687/02.01.788" //<limTol>
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0070 + ": " + STR0029 ) //"Limite Toler�ncia"
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0070 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			If aRisTrat[ nCont , 3 ] == "1" // Se for um agente do tipo quantitativo
				If Empty( aRisTrat[ nCont , 6 ] ) //<unMed>
					aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0037 + ": " + STR0029 )
					cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0037 + ": " + STR0029
					lRet := .F.
					aAdd( aLogProc , '' )
				Else
					Help := .T.
					If !ExistCPO( "V3F" , aRisTrat[ nCont , 6 ] , 2 )
						aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0037 + ": " + aRisTrat[ nCont , 6 ] )
						aAdd( aLogProc , STR0023 + STR0056 ) //"Deve ser um c�digo existente na tabela de Unidades de Medida do eSocial"
						cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0037 + ": " + aRisTrat[ nCont , 6 ]
						cMsgLog += CRLF + STR0023 + STR0056
						lRet := .F.
						aAdd( aLogProc , '' )
					ElseIf aRisTrat[ nCont , 6 ] == "5" .Or. ;
						   aRisTrat[ nCont , 6 ] == "6" .Or. ;
						   aRisTrat[ nCont , 6 ] == "7" .Or. ;
						   aRisTrat[ nCont , 6 ] == "8" .Or. ;
						   aRisTrat[ nCont , 6 ] == "23" .Or. ;
						   aRisTrat[ nCont , 6 ] == "24" .Or. ;
						   aRisTrat[ nCont , 6 ] == "25" .Or. ;
						   aRisTrat[ nCont , 6 ] == "33" .Or. ;
						   aRisTrat[ nCont , 6 ] == "34" .Or. ;
						   aRisTrat[ nCont , 6 ] == "38" .Or. ;
						   aRisTrat[ nCont , 6 ] == "40" .Or. ;
						   aRisTrat[ nCont , 6 ] == "41" .Or. ;
						   aRisTrat[ nCont , 6 ] == "42" .Or. ;
						   aRisTrat[ nCont , 6 ] == "44" .Or. ;
						   aRisTrat[ nCont , 6 ] == "45" .Or. ;
						   aRisTrat[ nCont , 6 ] == "46" .Or. ;
						   aRisTrat[ nCont , 6 ] == "47"
						aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0037 + ": " + aRisTrat[ nCont , 6 ] )
						aAdd( aLogProc , STR0023 + STR0084 ) //"Esse c�digo de unidade de medida n�o � mais utilizado a partir do leiaute 2.5 do eSocial"
						cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0037 + ": " + aRisTrat[ nCont , 6 ]
						cMsgLog += CRLF + STR0023 + STR0084
						lRet := .F.
						aAdd( aLogProc , '' )
					EndIf
					Help := .F.
				EndIf
			EndIf

			If Empty( aRisTrat[ nCont , 7 ] ) .And. aRisTrat[ nCont , 3 ] == "1" //<tecMedicao>
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0038 + ": " + STR0029 )
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0038 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			// Se tpRegTrab = 1
			If Empty( aRisTrat[ nCont , 8 ] ) .And. ( ( !Empty( cCodUnic ) .And. cCodCateg <> "104" ) .Or. ( cCodCateg $ "201/202/901" ) ) //</insalubridade>
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0071 + ": " + STR0029 ) //"Insalubridade"
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0071 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			// Se tpRegTrab = 1
			If Empty( aRisTrat[ nCont , 9 ] ) .And. ( ( !Empty( cCodUnic ) .And. cCodCateg <> "104" ) .Or. ( cCodCateg $ "201/202/901" ) ) //</periculosidade>
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0072 + ": " + STR0029 ) //"Periculosidade"
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0072 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			// Se tpRegPrev = 1
			If Empty( aRisTrat[ nCont , 10 ] ) .And. ( ( !Empty( cCodUnic ) .And. cCodCateg <> "104" ) .Or. ( cCodCateg $ "201/202/731/734/738" ) ) //</aposentEsp>
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0073 + ": " + STR0029 ) //"Aposentadoria Especial"
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0073 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			If Empty( aRisTrat[ nCont , 11 ] ) //<utilizEPC>
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0039 + ": " + STR0029 )
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0039 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			ElseIf !( aRisTrat[ nCont , 11 ] $ "0/1/2" )
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0039 + ": " + aRisTrat[ nCont , 10 ] )
				aAdd( aLogProc , STR0023 + STR0057 ) //"Deve ser igual a 1 (N�o se aplica), 1 (N�o utilizado) ou 2 (Utilizado)"
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0039 + ": " + aRisTrat[ nCont , 10 ]
				cMsgLog += CRLF + STR0023 + STR0057
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			If Empty( aRisTrat[ nCont , 12 ] ) .And. aRisTrat[ nCont , 11 ] == "2"
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0068 + ": " + STR0029 ) //"Efici�ncia EPC"
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0068 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			If Empty( aRisTrat[ nCont , 13 ] ) //<utilizEPI>
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0040 + ": " + STR0029 )
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0040 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			ElseIf !( aRisTrat[ nCont , 13 ] $ "0/1/2" )
				aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0040 + ": " + aRisTrat[ nCont , 12 ] )
				aAdd( aLogProc , STR0023 + STR0057 ) //"Deve ser igual a 1 (N�o se aplica), 1 (N�o utilizado) ou 2 (Utilizado)"
				cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0040 + ": " + aRisTrat[ nCont , 12 ]
				cMsgLog += CRLF + STR0023 + STR0057
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			//Se o funcion�rio utiliza Epi ( <utilizEPI> = 2 )
			If !Empty( aRisTrat[ nCont , 13 ] ) .And. aRisTrat[ nCont , 13 ] == "2"
				For nEpi := 1 To Len( aRisTrat[ nCont , 14 ] ) //EPI's
					If Empty( aRisTrat[ nCont , 14 , nEpi , 1 ] ) .And. Empty( aRisTrat[ nCont , 14 , nEpi , 2 ] ) //<dscEPI>
						aAdd( aLogProc , STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0067 + ": " + STR0029 ) //"Descri��o do EPI"
						cMsgLog += CRLF + STR0065 + aRisTrat[ nCont , 1 ] + " / " + STR0067 + ": " + STR0029
						lRet := .F.
						aAdd( aLogProc , '' )
					EndIf
				Next nEpi
			EndIf

		Next nCont
	EndIf

	//Metodologia Riscos Ergon�micos
	If Len( aRisMetErg ) > 0
		For nCont := 1 To Len( aRisMetErg )
			If Empty( aRisMetErg[ nCont , 2 ] )
				aAdd( aLogProc , STR0075 + aRisMetErg[ nCont , 1 ] + " / " + STR0074 + ": " + STR0029 ) //"Metodologia Riscos Erg."
				cMsgLog += CRLF + STR0075 + aRisMetErg[ nCont , 1 ] + " / " + STR0074 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf
		Next nCont
	EndIf

	//Respons�vel pelos Registros Ambientais
	If Len( aRespAmb ) > 0
		For nCont := 1 To Len( aRespAmb )
			If Empty( aRespAmb[ nCont , 2 ] ) //<cpfResp>
				aAdd( aLogProc , STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0043 + ": " + STR0029 ) //Respons�vel: 0001 - ALFREDINHO JOS� / CPF Respons�vel: Em Branco
				cMsgLog += CRLF + STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0043 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			ElseIf !Empty( aRespAmb[ nCont , 2 ] ) //<cpfResp>
				Help := .T.
				If !CHKCPF( aRespAmb[ nCont , 2 ] )
					aAdd( aLogProc , STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0043 + ": " + aRespAmb[ nCont , 2 ] )
					aAdd( aLogProc , STR0023 + STR0060 ) //"Deve ser um n�mero de CPF v�lido"
					cMsgLog += CRLF + STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0043 + ": " + aRespAmb[ nCont , 2 ]
					cMsgLog += CRLF + STR0023 + STR0060
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf
				Help := .F.
			EndIf
			If Empty( aRespAmb[ nCont , 3 ] ) //<nisResp>
				aAdd( aLogProc , STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0044 + ": " + STR0029 )
				cMsgLog += CRLF + STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0044 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf
			If Empty( aRespAmb[ nCont , 4 ] ) //<nmResp>
				aAdd( aLogProc , STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0045 + ": " + STR0029 )
				cMsgLog += CRLF + STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0045 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf
			If Empty( aRespAmb[ nCont , 5 ] ) //<ideOC>
				aAdd( aLogProc , STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0046 + ": " + STR0029 )
				cMsgLog += CRLF + STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0046 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			ElseIf !( aRespAmb[ nCont , 5 ] $ "1/4/9" )
				aAdd( aLogProc , STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0046 + ": " + aRespAmb[ nCont , 5 ] )
				aAdd( aLogProc , STR0023 + STR0062 ) //"Deve ser igual a 1 (CRM), 4 (CREA) ou 9 (Outros)"
				cMsgLog += CRLF + STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0046 + ": " + aRespAmb[ nCont , 5 ]
				cMsgLog += CRLF + STR0023 + STR0062
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf
			If Empty( aRespAmb[ nCont , 6 ] ) .And. aRespAmb[ nCont , 5 ] == "9" //<dscOC>
				aAdd( aLogProc , STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0047 + ": " + STR0029 )
				cMsgLog += CRLF + STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0047 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf
			If Empty( aRespAmb[ nCont , 7 ] ) //<nrOC>
				aAdd( aLogProc , STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0048 + ": " + STR0029 )
				cMsgLog += CRLF + STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0048 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf
			If Empty( aRespAmb[ nCont , 8 ] ) //<ufOC>
				aAdd( aLogProc , STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0049 + ": " + STR0029 ) // "Em Branco"
				cMsgLog += CRLF + STR0066 + AllTrim( aRespAmb[ nCont , 1 ] ) + " - " + AllTrim( aRespAmb[ nCont , 4 ] ) + " / " + STR0049 + ": " + STR0029
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf
		Next nCont
	Else
		aAdd( aLogProc , STR0063 ) //"N�o existem Respons�veis Ambientais para o per�odo de exposi��o do funcion�rio!"
		lRet := .F.
		aAdd( aLogProc , '' )
	EndIf

	//Volta para a filial correta
	cFilAnt := cFilBkp

Return lRet
