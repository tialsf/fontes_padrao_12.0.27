#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "mdtm005.ch"

STATIC aEfd	 := If( cPaisLoc == "BRA", If( Findfunction( "fEFDSocial" ), fEFDSocial(), { .F., .F., .F. } ), { .F., .F., .F. } )
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//  _______           _______  _       _________ _______             _______  _______  _______  _______  __    ------
// (  ____ \|\     /|(  ____ \( (    /|\__   __/(  ___  )           (  ____ \/ ___   )/ ___   )/ ___   )/  \   ------
// | (    \/| )   ( || (    \/|  \  ( |   ) (   | (   ) |           | (    \/\/   )  |\/   )  |\/   )  |\/) )  ------
// | (__    | |   | || (__    |   \ | |   | |   | |   | |   _____   | (_____     /   )    /   )    /   )  | |  ------
// |  __)   ( (   ) )|  __)   | (\ \) |   | |   | |   | |  (_____)  (_____  )  _/   /   _/   /   _/   /   | |  ------
// | (       \ \_/ / | (      | | \   |   | |   | |   | |                 ) | /   _/   /   _/   /   _/    | |  ------
// | (____/\  \   /  | (____/\| )  \  |   | |   | (___) |           /\____) |(   (__/\(   (__/\(   (__/\__) (_ ------
// (_______/   \_/   (_______/|/    )_)   )_(   (_______)           \_______)\_______/\_______/\_______/\____/ ------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MDTM005
Rotina de Envio de Eventos - Exame Toxicol�gico do Motorista Profissional (S-2221)
Realiza o envio das informa��es de Exames Toxicol�gicos para o TAF

@return xRet - Caracter/L�gico - Se for gera��o de Xml retorna o cXml, sen�o retorna .T. ou .F. de acordo com as condi��es

@sample MDTM005( "D MG 01", 3, .F. )

@param cFilEnv	- Caracter 	- Indica a filial de envio das informa��es ao TAF
@param nOpcX 	- Num�rico 	- Indica o tipo de opera��o do cadastro (3-Incluir;4-Alterar;5-Excluir)
@param lXml 	- L�gico 	- Indica se � gera��o de Xml

@author Luis Fellipy Bett
@since 18/09/2018
/*/
//----------------------------------------------------------------------------------------------------
Function MDTM005( cFilEnv, nOpcX, lXml, lInconsis )

	Local aArea		 	:= GetArea()
	Local aAreaTM5	 	:= TM5->( GetArea() )
	Local lMiddleware	:= IIf( cPaisLoc == 'BRA' .And. Findfunction( "fVerMW" ), fVerMW(), .F. )
	Local aErros 	 	:= {}
	Local aLogProc		:= {}
	Local aDados		:= {}
	Local aEstab		:= {}
	Local cXml			:= ""
	Local cMsgLog		:= ""
	Local cChvBus		:= ""
	Local xRet			:= .T.

	Private cTpInsc
	Private cNrInsc
	Private cNumMat			//Matr�cula do Funcion�rio
	Private cCpfTrab		//CPF do Funcion�rio
	Private cNisTrab		//N�mero Nis Funcion�rio
	Private cMatricula		//C�digo �nico do Funcion�rio
	Private cCodCateg		//Categoria do Funcion�rio
	Private dDtAdm			//Data de Admiss�o do Funcion�rio
	Private cCCusto			//Centro de Custo do Funcion�rio

	//Vari�veis do Exame Toxicol�gico
	Private dDtExame		//Data da realiza��o do exame toxicol�gico
	Private dDtDem			//Data de demiss�o do funcion�rio
	Private cCnpjLab		//CNPJ do laborat�rio respons�vel pela realiza��o do exame.
	Private cCodSeqExame	//C�digo do exame toxicol�gico. Deve ser informado no formato AA999999999.
	Private cNmMed			//Preencher com o nome do m�dico.
	Private cNrCRM			//N�mero de inscri��o do m�dico no CRM.
	Private cUFCRM			//Preencher com a sigla da UF de expedi��o do CRM
	Private cIndRecusa		//Indica se o trabalhador se recusou a realizar o exame toxicol�gico no desligamento

	Default nOpcX		:= 3
	Default lXml		:= .F.
	Default lInconsis	:= .F.

	//Trata mensagem inicial de inconsist�ncias
	aAdd( aLogProc, STR0001 ) //"Inconsist�ncias do Exame Toxicol�gico"
	aAdd( aLogProc, STR0002 ) //"Os campos abaixo est�o vazios/zerados ou possuem inconsist�ncia com rela��o ao formato padr�o do eSocial: "
	aAdd( aLogProc, "" )
	aAdd( aLogProc, "" )

	//Verifica se TAF est� preparado
	aRet:= TafExisEsc( "S2221" )
	If !aRet[ 1 ] .Or. ( aRet[ 2 ] <> "2.4" .And. aRet[ 2 ] <> "2.4.02" .And. aRet[ 2 ] <> "2.5" )
		Help(' ',1,STR0003,,STR0037,2,0,,,,,,{STR0005}) //"Aten��o" ## "O ambiente n�o possui integra��o com o m�dulo do TAF e/ou a vers�o do TAF est� desatualizada!" ## "Favor verificar!"
		xRet := .F.
	ElseIf !( NGCADICBASE( "TMA_ESOC", "A", "TMA", .F. ) )
		Help(' ',1,STR0003,,STR0004,2,0,,,,,,{STR0046}) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial"
		xRet := .F.
	ElseIf !( NGCADICBASE( "TNC_TPLOGR", "A", "TNC", .F. ) )
		Help(' ',1,STR0003,,STR0004,2,0,,,,,,{STR0046}) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial"
		xRet := .F.
	Else
		//Caso n�o tenha a Filial n�o tem como fazer a Integra��o
		If !Empty( cFilEnv )
			//Inicializa regua de processamento
			ProcRegua( 1 )

			cNumMat			:= Posicione( "TM0", 1, xFilial( "TM0" ) + TM5->TM5_NUMFIC, "TM0_MAT"		)
			cCpfTrab		:= Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat		, "RA_CIC"		)
			cNisTrab		:= Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat 		, "RA_PIS" 		)
			cMatricula		:= Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat 		, "RA_CODUNIC" 	)
			cCodCateg 		:= Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat		, "RA_CATEFD"	)
			dDtAdm			:= Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat		, "RA_ADMISSA"	)
			cCCusto			:= Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat		, "RA_CC"		)
			dDtDem			:= Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat		, "RA_DEMISSA"	)

			dDtExame		:= If( Empty( TM5->TM5_DTRESU ), TM5->TM5_DTPROG, TM5->TM5_DTRESU 		)
			cCnpjLab		:= Posicione( "SA2", 1, xFilial( "SA2" ) + TM5->TM5_FORNEC, "A2_CGC" 	)
			cCodSeqExame	:= SubStr( TM5->TM5_CODDET, 7, 11 )
			cNmMed			:= Posicione( "TNP", 1, xFilial( "TNP" ) + TM5->TM5_USUARI, "TNP_NOME"	)
			cNrCRM			:= Posicione( "TNP", 1, xFilial( "TNP" ) + TM5->TM5_USUARI, "TNP_NUMENT"	)
			cUFCRM			:= Posicione( "TNP", 1, xFilial( "TNP" ) + TM5->TM5_USUARI, "TNP_UF"		)

			cIndRecusa 	:= If( "RECUSA" $ TM5->TM5_CODDET, "S", "N" )

			If cIndRecusa == "S"
				dDtExame := dDtDem
			EndIf

			cNmMed := MDTSubTxt( AllTrim( cNmMed ) )

			//Busca as informa��es do Estabelecimento
			aEstab := MDTGetEst( cCCusto, cFilEnv )

			cTpInsc		:= aEstab[ 1, 1 ]
			cNrInsc		:= aEstab[ 1, 2 ]

			If lInconsis
				fInconsis( @aLogProc , @cMsgLog , nOpcX , cFilEnv )

				If Len( aLogProc ) > 4
					If !IsBLind()
						fMakeLog( { aLogProc }, { STR0012 }, Nil, Nil, "MDTM005", OemToAnsi( STR0013 ), "M", "P",, .F. ) //"Monitoramento Envio de Eventos - TAF"###"Log de Ocorrencias - Exame Toxicol�gico"

						Help(' ',1,STR0003,,STR0041,2,0,,,,,,{STR0042}) //"N�o foi poss�vel o envio ao eSocial pois os dados est�o inconsistentes!" ## "Favor verificar os �tens expostos no relat�rio!"

						xRet := .F.
					Else
						xRet := .F.
						aAdd( aDados, OemToAnsi( STR0004 ) + " " + cCodAmb + " " + OemToAnsi( STR0006 ) + "! " + STR0044 ) //"Ambiente" ## "Falha no envio ao TAF" ## "Para verifica��o favor gerar o relat�rio de inconsist�ncias"
					EndIf
				EndIf
			Else
				cXml := fCarrTox( cValToChar( nOpcX ), lXml, cFilEnv, lMiddleware )

				If lXml
					fInconsis( @aLogProc , @cMsgLog , nOpcX , cFilEnv )

					If Len( aLogProc ) > 4
						fMakeLog( { aLogProc }, { STR0012 }, Nil, Nil, "MDTM005", OemToAnsi( STR0013 ), "M", "P",, .F. ) //"Monitoramento Envio de Eventos - TAF"###"Log de Ocorrencias - Exame Toxicol�gico"

						Help(' ',1,STR0003,,STR0043,2,0,,,,,,{STR0042}) //"O Xml possui inconsist�ncias de acordo com o formato padr�o do eSocial" ## "Favor ajustar os valores expostos no relat�rio!"

						xRet := .F.
					Else
						xRet := cXml
					EndIf
				Else
					Begin Transaction

						//Caso o envio seja atrav�s do Middleware
						If lMiddleware

							If MDTVerTSVE( cCodCateg ) //Caso seja Trabalhador Sem V�nculo Estatut�rio
								cChvBus := AllTrim( cCpfTrab ) + AllTrim( cCodCateg ) + DToS( dDtAdm ) + DToS( dDtExame )
							Else
								cChvBus := AllTrim( cMatricula ) + DToS( dDtExame )
							EndIf

							//Envia o evento ao eSocial atrav�s do Middleware
							MDTEnvMid( cFilEnv, cNumMat, "S2221", cChvBus, cXml, nOpcX )

						Else
							//Enviar como parametro a filial do Protheus na posi��o 1 e o XML na posi��o 2
							aErros := TafPrepInt( cEmpAnt, cFilEnv, cXml,, "1", "S2221" )

							If Len( aErros ) <= 0
								//Carrega array de controle de carga
								aAdd( aDados, OemToAnsi( STR0006 ) + " " + TM5->TM5_EXAME + " - " + cCpfTrab + " " + OemToAnsi( STR0007 ) ) //"Exame Toxicol�gico"###"Enviado ao TAF com sucesso!"
								Help(' ',1,STR0003,,STR0049,2,0) //"Aten��o"##"Registro enviado ao TAF com sucesso!"
							Else
								aAdd( aDados, OemToAnsi( STR0006 ) + " " + TM5->TM5_EXAME + " - " + cCpfTrab + " " + OemToAnsi( STR0008 ) + ": " + aErros[ 1 ] ) //"Exame Toxicol�gico"###"Falha no envio ao TAF"
								Help(' ',1,STR0003,,STR0009 + "!" + CRLF + aErros[ 1 ],2,0,,,,,,{STR0045}) //"Aten��o" ## "Existem inconsist�ncias na comunica��o com o TAF" ## "Para tentativa de reenvio favor manipular o registro!"
							EndIf

							//Incrementa regua
							IncProc( OemToAnsi( STR0011 ) + ": " + TM5->TM5_EXAME + " - " + cCpfTrab ) //"Gerando o registro de"
						EndIf

					End Transaction
				EndIf
			EndIf
		Else
			Help(' ',1,STR0003,,STR0010,2,0,,,,,,{STR0005}) // Aten��o ## "A informa��o da filial de envio ao eSocial est� vazia!" ## "Favor verificar!"
			xRet := .F.
		EndIf

	EndIf

	RestArea( aAreaTM5 )
	RestArea( aArea )

Return xRet
//---------------------------------------------------------------------
/*/{Protheus.doc} fCarrTox

Carrega os Exames Toxicol�gicos

@return cXml Caracter Estrutura XML a ser enviada para o TAF

@sample fCarrTox( "3" )

@param cOper	- Caracter	- Opera��o a ser realizada

@author Luis Fellipy Bett
@since 30/08/2018
/*/
//---------------------------------------------------------------------
Static Function fCarrTox( cOper, lXml, cFilEnv, lMiddleware )

	Local cXml		:= ""
	Local cChvBus	:= ""
	Local lTSVE		:= MDTVerTSVE( cCodCateg )

	Default cOper := "3"

	If lTSVE //Caso seja Trabalhador Sem V�nculo Estatut�rio
		cChvBus := AllTrim( cCpfTrab ) + AllTrim( cCodCateg ) + DToS( dDtAdm ) + DToS( dDtExame )
	Else
		cChvBus := AllTrim( cMatricula ) + DToS( dDtExame )
	EndIf

	//Cria o cabe�alho do Xml com o ID, informa��es do Evento e Empregador
	MDTGerCabc( @cXml, "S2221", cTpInsc, cNrInsc, lMiddleware, lXml, cOper, cChvBus )

	//FUNCIONARIO
	cXml += 		'<ideVinculo>'
	cXml += 			'<cpfTrab>'		+ cCpfTrab 		+	'</cpfTrab>'
	If !( cCodCateg $ "901/903/904" )
		cXml +=			'<nisTrab>'		+ cNisTrab 		+	'</nisTrab>'
	EndIf
	If !lTSVE //Se n�o for Trabalhador Sem V�nculo Estatut�rio
		cXml +=			'<matricula>'	+ cMatricula	+	'</matricula>'
	Else
		cXml +=			'<codCateg>'	+ cCodCateg 	+	'</codCateg>'
	EndIf
	cXml += 		'</ideVinculo>'

	//EXAME TOXICOL�GICO
	cXml += 		'<toxicologico>' //Exame Toxicol�gico
	cXml += 			'<dtExame>'		+ DToS( dDtExame )			+ '</dtExame>'
	If cIndRecusa == "N" // Se o funcion�rio n�o se recusou a realizar o exame toxicol�gico no desligamento
		cXml += 		'<cnpjLab>'		+ cCnpjLab					+ '</cnpjLab>'
		cXml += 		'<codSeqExame>'	+ cCodSeqExame				+ '</codSeqExame>'
		cXml += 		'<nmMed>' 		+ cNmMed					+ '</nmMed>'
		cXml += 		'<nrCRM>' 		+ SubStr( cNrCRM, 1, 8 )	+ '</nrCRM>'
		cXml += 		'<ufCRM>' 		+ cUFCRM					+ '</ufCRM>'
	EndIf
	cXml += 			'<indRecusa>'	+ cIndRecusa				+ '</indRecusa>'
	cXml += 		'</toxicologico>'

	cXml += 	'</evtToxic>'
	cXml += '</eSocial>'

Return cXml

//---------------------------------------------------------------------
/*/{Protheus.doc} fInconsis

Carrega Inconsist�ncias (Se houver)

@return lRet L�gico Retorna falso caso haja alguma inconsist�ncia

@sample fInconsis( aLogProc, "cMsgLog", .F., "ST9" )

@param aLogProc		- Array		- Array que ir� receber os Logs
@param cMsgLog		- Caracter	- Variavel que recebera a mensagem de Log
@param nOpcX		- Num�rico	- Indica a opera��o realizada

@author Luis Fellipy Bett
@since 30/08/2018
/*/
//---------------------------------------------------------------------
Static Function fInconsis( aLogProc , cMsgLog , nOpcX , cFilEnv )

	Local cFilBkp		:= cFilAnt
	Local lRet 			:= .F.
	Local nCampos		:= 0
	Local nCont			:= 0
	Local aVldValues	:= {}
	Local aVetor		:= Array(11)
	Local cStrFunc		:= ""
	Local cStrToxi		:= ""
	Local dDtEsoc		:= SuperGetMv( "MV_NG2DTES", .F., SToD( "20190701" ) )

	//Blocos de C�digo das Condi��es
	Local bCond1 := { || !( cCodCateg $ "901/903/904" ) }
	Local bCond2 := { || Posicione( "C9V", 3, xFilial( "C9V" , cFilEnv ) + cCpfTrab, "C9V_NOMEVE" ) == "S2300" } //S� deve ser preenchido se tiver sido cadastrado no S-2300
	Local bCond3 := { || cIndRecusa == "N" }
	Local bCond4 := { || cIndRecusa == "S" }

	//Blocos de C�digo das Valida��es
	Local bValid1 := { || cTpInsc $ "1/2" }
	Local bValid2 := { || If( cTpInsc $ "1/2", If( cTpInsc == "1", CGC( cNrInsc ), CHKCPF( cNrInsc ) ), .T. ) }
	Local bValid3 := { || CHKCPF( cCpfTrab ) }
	Local bValid4 := { || ExistCPO( "C9V", cMatricula, 11 ) } //"Deve ser a mesma matr�cula informada no evento S-2200"
	Local bValid5 := { || ExistCPO( "C87", cCodCateg, 2 ) }
	Local bValid6 := { || DToS( dDtExame ) >= DToS( dDtEsoc ) .And. DToS( dDtExame ) <= DToS( dDataBase ) }
	Local bValid7 := { || CGC( cCnpjLab ) }
	Local bValid8 := { || cIndRecusa $ "S/N" }
	Local bValid9 := { || !Empty( dDtDem ) }

	cStrFunc := STR0014 + ": " + AllTrim( cNumMat ) + " - " + AllTrim( Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat, "RA_NOME" ) ) //"Funcion�rio"
	cStrToxi := STR0006 + ": " + TM5->TM5_EXAME //"Exame Toxicol�gico"
	cDtImp   := DToS( dDtEsoc )
	cDtImp   := " (" + SubStr( cDtImp, 7, 2 ) + "/" + SubStr( cDtImp, 5, 2 ) + "/" + SubStr( cDtImp, 1, 4 ) + ")"

	//Seta a filial de envio para as valida��es de tabelas do TAF
	cFilAnt := cFilEnv

	// --- Estrutura do Array de envio das inconsist�ncias ---
	// Posi��o 1 - Campo a ser verificado
	// Posi��o 2 - String indicando o registro chave que est� sendo analisado junto a um " / " para impress�o no Log. Ex: Exame Toxicol�gico: 00031520 /
	// Posi��o 3 - String indicando o t�tulo do campo que ser� analisado. Ex: Data Realiza��o Exame Toxicol�gico
	// Posi��o 4 - Bloco de C�digo com a condi��o para verifica��o do campo
	// Posi��o 5 - Bloco de C�digo com a valida��o do campo
	// Posi��o 6 - Descri��o da valida��o do campo para envio ao TAF
	// Obs: Na impress�o do Log a fun��o juntar� os dados da 2�, 3� e 1� coluna, sucessivamente. Ex: Exame Toxicol�gico: 00031520 / Data Realiza��o Exame Toxicol�gico: 01/01/2019
	// -------------------------------------------------------

	aVldValues := {;
		{ cTpInsc		, cStrFunc + " / ", STR0015, { || .T. }	, bValid1		, STR0016	},; //"Tipo Inscri��o"###"Deve ser igual a 1 (CNPJ) ou 2 (CPF)"
		{ cNrInsc		, cStrFunc + " / ", STR0017, { || .T. }	, bValid2		, STR0018	},; //"Incri��o"###"Deve ser um n�mero de CNPJ ou CPF v�lido"
		{ cCpfTrab 		, cStrFunc + " / ", STR0019, { || .T. }	, bValid3		, STR0020	},; //"C.P.F."###"Deve ser um n�mero de CPF v�lido"
		{ cNisTrab 		, cStrFunc + " / ", STR0021, bCond1		, { || .T. }	,			},; //"N.I.S."
		{ cMatricula	, cStrFunc + " / ", STR0022, { || .T. } 	, bValid4		, STR0023	},; //"Matr�cula"###"Deve ser a mesma matr�cula informada no evento S-2200"
		{ cCodCateg 	, cStrFunc + " / ", STR0024, bCond2		, bValid5		, STR0025	},; //"Categoria Funcion�rio"###"Deve existir na tabela 01 do eSocial"
		{ dDtDem		, cStrFunc + " / ", STR0047, bCond4		, bValid9		, STR0048	},; //"Data de Demiss�o" ### "Para poder enviar um exame Toxicol�gico com RECUSA ao TAF, o funcion�rio deve estar demitido"
		{ dDtExame		, cStrToxi + " / ", STR0026, bCond3		, bValid6		, STR0027 + CRLF + STR0044 + cDtImp },; //"Data Realiza��o Exame Toxicol�gico"###"Deve ser igual ou posterior � data de in�cio da obrigatoriedade dos eventos de SST (201901)"
		{ cCnpjLab		, cStrToxi + " / ", STR0028, bCond3 		, bValid7		, STR0029	},; //"CNPJ Laborat�rio Resp. Toxicol�gico"###"Deve ser um n�mero de CNPJ v�lido"
		{ cNmMed 		, cStrToxi + " / ", STR0030, bCond3		, { || .T. } 	,			},; //"Nome Med. Resp. Toxicol�gico"
		{ cNrCRM 		, cStrToxi + " / ", STR0031, bCond3		, { || .T. } 	,			},; //"N�mero Inscri��o Med. Resp. Toxicol�gico"
		{ cUFCRM 		, cStrToxi + " / ", STR0032, bCond3		, { || .T. } 	,			},; //"UF Med. Resp. Toxicol�gico"
		{ cIndRecusa	, cStrToxi + " / ", STR0039, { || .T. } 	, bValid8 		, STR0040	} } //"Indica Trab. Recusou Tox." ## "Deve ser igual a S=Sim ou N=N�o"

	lRet := MDTInconEsoc( aVldValues, @aLogProc, @cMsgLog )

	If cIndRecusa == "N"
		If Empty( cCodSeqExame )
			aAdd( aLogProc, cStrToxi + " / " + STR0033 + ": " + STR0034 ) //"C�digo Exame Toxicol�gico DENATRAN"###"Em Branco"
			cMsgLog += CRLF + " - " + STR0033 //"C�digo Exame Toxicol�gico DENATRAN"
			lRet := .F.
		ElseIf !IsAlpha( SubStr( cCodSeqExame, 1, 1 ) ) .Or. !IsAlpha( SubStr( cCodSeqExame, 2, 1 ) ) .Or. !IsNumeric( SubStr( cCodSeqExame, 3 ) )
			aAdd( aLogProc, cStrToxi + " / " + STR0033 + ": " + cCodSeqExame ) //"C�digo Exame Toxicol�gico DENATRAN"
			aAdd( aLogProc, STR0035 + STR0036 ) //"Valida��o: "###"Deve ser informado no formato AA999999999, sendo AA o serial do sequencial e 999999999 o n�mero sequencial do exame"
			cMsgLog += CRLF + " - " + STR0033 //"C�digo Exame Toxicol�gico DENATRAN"
			lRet := .F.
		EndIf
	EndIf

	//Volta para a filial do registro
	cFilAnt := cFilBkp

Return lRet