#INCLUDE "TOTVS.CH"
#INCLUDE "MDTM006.CH"

STATIC aEfd	 := If( cPaisLoc == "BRA" , If( Findfunction( "fEFDSocial" ) , fEFDSocial() , { .F. , .F. , .F. } ) , { .F. , .F. , .F. } )

//---------------------------------------------------------------------
/*/{Protheus.doc} MDTM006
Rotina de Envio de Eventos - Exclus�o de Eventos (S-3000)
Realiza a exclus�o dos eventos n�o peri�dicos

@return xRet Caracter/L�gico Se for gera��o de Xml retorna o Xml da exclus�o, sen�o .T. ou .F. de acordo com as condi��es

@sample MDTM006( 'S-2210' , '000000001' , , '20170101 , { cEmpAnt, cFilAnt } )

@param cEvento, Caracter, Indica o evento a ser exclu�do
@param [cNumFic], Caracter, N�mero da Ficha M�dica
@param [cMat], Caracter, Matr�cula do Funcion�rio (Obrigat�rio quando cNumFic for vazio)
@param cChvReg, Caracter, Chave �nica de busca
				Se Evento for "2210"
					cChvReg Igual DTOS( TNC->TNC_DTACID ) + TNC->TNC_HRACID
				Se Evento for "2220"
					cChvReg Igual TMY->TMY_DTEMIS
				Se Evento for "2221"
					cChvReg Igual TM5->TM5_DTRESU
				Se Evento for "2240"
					cChvReg Igual TN0->TN0_DTRECO
				Se Evento for "2245"
					cChvReg Igual TY4_CALEND + TY4_CURSO + TY4_TURMA
@param cFilEnv, Caracter, Informa��es de empresa e filial

@author Jackson Machado
@since 25/10/2017
/*/
//---------------------------------------------------------------------
Function MDTM006( cEvento, cNumFic, cMat, cChvReg, cFilEnv, lXml )

	Local cXml		  := ""
	Local cRecibo	  := ""
	Local cTab		  := ""
	Local aErros 	  := {}
	Local aDados	  := {}
	Local lMiddleware := IIf( cPaisLoc == 'BRA' .And. Findfunction( "fVerMW" ), fVerMW(), .F. )
	Local xRet		  := .T.
	Local cNumMat	  := Space( 6 )
	Local cCPF 		  := Space( 11 )
	Local cPis 		  := Space( 12 )
	Local cCodUnic	  := Space( 41 )

	//Vari�veis Middleware
	Local cChvBus	:= ""
	Local cStatReg	:= "-1"
	Local cOperReg	:= "I"
	Local cRetfReg	:= ""
	Local nRecReg	:= 0
	Local cRecibReg	:= ""
	Local cRecibAnt	:= ""
	Local cStatNew	:= ""
	Local cOperNew	:= ""
	Local cRetfNew	:= ""
	Local cRecibAnt	:= ""
	Local cKeyMid	:= ""
	Local nRecEvt	:= 0
	Local lNovoRJE	:= .T.
	Local aInfEnv	:= {}

	Default cNumFic   := Space( 9 )
	Default cMat	  := Space( 6 )
	Default lXml	  := .F.

	If "2210" $ cEvento
		cTab := "CM0"
	ElseIf "2220" $ cEvento
		cTab := "C8B"
	ElseIf "2221" $ cEvento
		cTab := "V3B"
	ElseIf "2240" $ cEvento
		cTab := "CM9"
	ElseIf "2245" $ cEvento
		cTab := "V3C"
	EndIf

	//Caso n�o tenha a Filial n�o tem como fazer a Integra��o
	If !Empty( cFilEnv )

		//Matricula para encontrar chave do funcionario
		If Empty( cNumFic )
			cNumMat := cMat
		Else
			cNumMat := Posicione( "TM0", 1, xFilial( "TM0" ) + cNumFic, "TM0_MAT" )
		EndIf

		//CPF para encontrar chave do funcionario
		cCPF := Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat, "RA_CIC" )

		//PIS para encontrar chave do funcionario
		cPis := Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat, "RA_PIS" )

		//C�digo Unificado para encontrar chave do funcionario
		cCodUnic := Posicione( "SRA", 1, xFilial( "SRA" ) + cNumMat, "RA_CODUNIC" )

		//Caso for gera��o de Xml ou envio ao Governo via TAF, monta Xml de exclus�o
		If !lMiddleware .Or. lXml

			//Trata o recibo conforme chave �nica de busca
			cRecibo := cCPF + cCodUnic + cChvReg

			cXml += '<eSocial>'
			cXml += '	<evtExclusao>'
			cXml += '		<infoExclusao>'
			cXml += '			<tpEvento>' + cEvento + '</tpEvento>'
			cXml += '			<nrRecEvt >' + cRecibo + '</nrRecEvt>'
			cXml += '			<ideTrabalhador>'
			cXml += '				<cpfTrab>' + cCPF + '</cpfTrab>'
			cXml += '				<nisTrab>' + cPis + '</nisTrab>'
			cXml += '			</ideTrabalhador>'
			cXml += '		</infoExclusao>'
			cXml += '	</evtExclusao>'
			cXml += '</eSocial>'

		EndIf

		Begin Transaction

			If lXml
				xRet := cXml
			Else
				//Caso o envio seja atrav�s do Middleware
				If lMiddleware

					aInfoC := fXMLInfos()
					If LEN( aInfoC ) >= 4
						cTpInsc  := aInfoC[1]
						cNrInsc	 := aInfoC[2]
						cId		 := aInfoC[3]
						lAdmPubl := aInfoC[4]
					Else
						cTpInsc  := ""
						cNrInsc  := "0"
						cId		 := ""
						lAdmPubl := .F.
					EndIf

					//RJE_TPINSC + RJE_INSCR + RJE_EVENTO + RJE_KEY + RJE_INI
					cChvBus := Padr( cTpInsc, TAMSX3( "RJE_TPINSC" )[1] ) + ;
							   Padr( IIf( cTpInsc == "1" .And. !lAdmPubl, SubStr( cNrInsc, 1, 8 ), cNrInsc ), TAMSX3( "RJE_INSCR" )[1] ) + ;
							   StrTran( cEvento, "-", "" ) + ;
							   Padr( cChvReg, TAMSX3( "RJE_KEY" )[1] )

					GetInfRJE( 2, cChvBus, @cStatReg, @cOperReg, @cRetfReg, @nRecReg, @cRecibReg, @cRecibAnt, Nil, Nil, .T. )

					//Monta Xml conforme layout de exclus�o
					InExc3000( @cXml, cEvento, cRecibReg, cCPF, cPis, Nil, Nil, Nil, Nil, cFilEnv, lAdmPubl, cTpInsc, cNrInsc, cId, @cStatNew, @cOperNew, @cRetfNew, @nRecEvt, @lNovoRJE, @cKeyMid, @aErros )

					If Len( aErros ) == 0
						aAdd( aInfEnv, { xFilial( "RJE", cFilEnv ), cFilEnv, cTpInsc, Iif( cTpInsc == "1" .And. !lAdmPubl, SubStr( cNrInsc, 1, 8 ), cNrInsc ), "S3000", Space( 6 ), cRecibReg, cId, cRetfNew, "12", cStatNew, Date(), Time(), cOperNew, NIL, NIL } )
						If !( lRet := fGravaRJE( aInfEnv, cXml, lNovoRJE, nRecEvt ) )
							Help( ' ', 1, "Aten��o", , "Ocorreu um erro na grava��o do registro do evento " + cEvento + " na tabela RJE", 2, 0 )
						EndIf
					Else
						Help( ' ', 1, "Aten��o", , aErros[1], 2, 0 )
					EndIf

				Else
					//Envia o evento para o TAF e salva os poss�veis erros
					aErros := TafPrepInt( cEmpAnt, cFilEnv, cXml, , "1", "S3000" )

					If Len( aErros ) <= 0 //Caso array vazio n�o encontrou erro
						aAdd( aDados, OemToAnsi( STR0001 ) + " " + cEvento + " " + OemToAnsi( STR0002 ) ) //"Evento""enviado ao TAF com sucesso."
						Help( ' ', 1, STR0005, , STR0009, 2, 0 ) //"Aten��o"##"Registro enviado ao TAF com sucesso!"
					Else
						aAdd( aDados, OemToAnsi( STR0001 ) + " " + cEvento + " " + OemToAnsi( STR0003 ) + " " + aErros[ 1 ] ) //"Evento""falha no envio ao TAF:"
						Help( ' ', 1, STR0005, , STR0001 + " " + cEvento + " " + STR0003 + CRLF + aErros[ 1 ], 2, 0, , , , , , { STR0007 + cTab + STR0008 } ) //"Aten��o" ## "Favor verificar os registros da tabela XXX do TAF"
						xRet := .F.
					EndIf
				EndIf
			EndIf

		End Transaction
	Else
		aAdd( aDados, OemToAnsi( STR0004 ) + " - " + OemToAnsi( STR0006 ) )//"Ambiente""falha no envio ao TAF. N�o h� filial cadastrada."
		Help( ' ', 1, STR0005, , STR0004, 2, 0, , , , , , { STR0006 } ) //"Aten��o" ## "A informa��o da filial de envio ao eSocial est� vazia!" ## "Favor verificar!"
		xRet := .F.
	EndIf

Return xRet