#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "MDTM008.CH"

STATIC aEfd	 := If( cPaisLoc == "BRA" , If( Findfunction( "fEFDSocial" ) , fEFDSocial() , { .F. , .F. , .F. } ) , { .F. , .F. , .F. } )
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//  _______           _______  _       _________ _______             _______  _______  _______    ___    _______  ---
// (  ____ \|\     /|(  ____ \( (    /|\__   __/(  ___  )           (  ____ \/ ___   )/ ___   )  /   )  (  ____ ) ---
// | (    \/| )   ( || (    \/|  \  ( |   ) (   | (   ) |           | (    \/\/   )  |\/   )  | / /) |  | |    	  ---
// | (__    | |   | || (__    |   \ | |   | |   | |   | |   _____   | (_____     /   )    /   )/ (_) (_ | |_____  ---
// |  __)   ( (   ) )|  __)   | (\ \) |   | |   | |   | |  (_____)  (_____  )  _/   /   _/   /(____   _)(_____  ) ---
// | (       \ \_/ / | (      | | \   |   | |   | |   | |                 ) | /   _/   /   _/      ) (   _    | | ---
// | (____/\  \   /  | (____/\| )  \  |   | |   | (___) |           /\____) |(   (__/\(   (__/\    | |  ( )___| | ---
// (_______/   \_/   (_______/|/    )_)   )_(   (_______)           \_______)\_______/\_______/    (_)  (_______) ---
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MDTM008
Rotina de Envio de Eventos - Treinamentos, Capacita��es, Exerc�cios Simulados e Outras Anota��es ( S-2245 )
Realiza o envio das informa��es de Treinamentos, Capacita��es, Exerc�cios Simulados e Outras Anota��es para o TAF

@return xRet - Caracter/L�gico - Se for gera��o de Xml retorna o cXml, sen�o retorna .T. ou .F. de acordo com as condi��es

@sample MDTM008( 'D MG 01' , '0000001' , .T. )

@param cFilEnv		- Caracter	- Indica a filial de Envio das Informa��es
@param cMatricula	- Caracter	- Indica a matr�cula do Funcion�rio ao qual ser�o enviadas as informa��es
@param lXml			- L�gico	- Indica se � gera��o de Xml

@author Luis Fellipy Bett
@since 03/06/2018
/*/
//-------------------------------------------------------------------------------------------------------------------
Function MDTM008( cFilEnv, cMatricula, lXml, cCalend, cCurso, cTurma, nOpcTAF, aLogProc, lIncons )

	Local aArea		 	:= GetArea()
	Local cXml			:= ""
	Local cMsgLog		:= ""
	Local cChvBus		:= ""
	Local aErros 	 	:= {}
	Local aDados 		:= {}
	Local xRet			:= .T.
	Local dDtEsoc		:= SuperGetMv( "MV_NG2DTES", .F., SToD( "20190701" ) )
	Local lMiddleware	:= IIf( cPaisLoc == 'BRA' .And. Findfunction( "fVerMW" ), fVerMW(), .F. )

	//Contadores
	Local nCont

	Local cCCusto		:= ""
	Local cFuncao		:= ""
	Local cDepto		:= ""
	Local cCPFIns		:= ""
	Local cTpInst		:= "" //Tipo de Instrutor: 1 - Funcion�rio ; 2 - Profissional sem v�nculo
	Local cEscIns		:= "" //Forma��o do Porfessor ( Acad�mica, Pr�tica ou Outra Forma )
	Local cFuncIns		:= ""
	Local cCBOIns		:= "" //Classifica��o Brasileira de Ocupa��o do Professor
	Local cNacIns		:= ""
	Local cIndTreinAnt	:= "" //Indica se o treinamento foi realizado antes da admiss�o em outro empregador
	Local aTreinamentos	:= {}
	Local aRisExp		:= {}
	Local aTarefas		:= {}
	Local aEstab		:= {}
	Local dDtTrei		:= SToD( "" )
	Local cValRisco     := '{ |dData| ( TN0->TN0_DTRECO < SRA->RA_DEMISSA .Or. Empty( SRA->RA_DEMISSA ) ) .And. ' + ;
		  				   '( TN0->TN0_DTELIM > dData .Or. Empty( TN0->TN0_DTELIM ) ) .And. ' + ;
						   '!Empty( TN0->TN0_DTAVAL ) .And. MdtVldRis(dData) }'

	//Variaveis de adequa��o das informa��es
	Private cTpInsc		:= ""
	Private cNrInsc		:= ""
	Private cCpfTrab	:= ""
	Private cNisTrab	:= ""
	Private cCodUnic	:= ""
	Private cCodCateg	:= ""
	Private aTreInf		:= {}
	Private dDtAdm		:= SToD( "" )

	Default lXml		:= .F.
	Default lIncons		:= .F.

	//Verifica se TAF est� preparado
	aRet:= TafExisEsc( "S2245" )
	If !aRet[ 1 ] .Or. ( aRet[ 2 ] <> "2.4" .And. aRet[ 2 ] <> "2.4.02" .And. aRet[ 2 ] <> "2.5" )
		Help(' ',1,STR0003 ,,STR0013,2,0,,,,,,{STR0006}) //"Aten��o" ## "O ambiente n�o possui integra��o com o m�dulo do TAF e/ou a vers�o do TAF est� desatualizada!" ## "Favor verificar!"
		xRet := .F.
	ElseIf !( NGCADICBASE( "TMA_ESOC", "A", "TMA", .F. ) )
		 Help(' ',1,STR0003,,STR0004,2,0,,,,,,{STR0005}) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial"
		xRet := .F.
	ElseIf !( NGCADICBASE( "TNC_TPLOGR", "A", "TNC", .F. ) )
		Help(' ',1,STR0003,,STR0004,2,0,,,,,,{STR0005}) //"Aten��o" ## "A vers�o do MDT est� desatualizada!" ## "Favor aplicar o pacote para atualiza��o dos requisitos de envio ao eSocial"
		xRet := .F.
	Else
		//Caso n�o tenha a Filial n�o tem como fazer a Integra��o
		If !Empty( cFilEnv )
			//Inicializa regua de processamento
			ProcRegua( 1 )

			dbSelectArea( "SRA" )
			dbSetOrder( 1 )
			dbSeek( xFilial( "SRA" ) + cMatricula )
			cCpfTrab	:= SRA->RA_CIC		// CPF do funcion�rio
			cNisTrab	:= SRA->RA_PIS 		// NIS para encontrar chave do Funcion�rio
			cCodUnic	:= SRA->RA_CODUNIC 	// C�digo �nico para encontrar chave do Funcion�rio
			cCodCateg	:= SRA->RA_CATEFD	// C�digo da categoria do Funcion�rio
			cCCusto		:= RA_CC			// Centro de Custo do Funcion�rio
			cFuncao		:= RA_CODFUNC		// Fun��o do Funcion�rio
			cDepto		:= SRA->RA_DEPTO	// Departamento do Funcion�rio
			dDtAdm		:= SRA->RA_ADMISSA	// Data de Admiss�o do Funcion�rio

			//Busca as informa��es do Estabelecimento
			aEstab := MDTGetEst( cCCusto , cFilEnv )

			cTpInsc	:= aEstab[ 1 , 1 ]
			cNrInsc	:= aEstab[ 1 , 2 ]

			aRisExp := MDTRetRis(,,,,,,,.F.,,,,cValRisco)[1] //Riscos do Funcion�rio

			//Busca todas as Tarefas realizadas pelo Funcion�rio
			dbSelectArea( "TN6" )
			dbSetOrder( 2 ) //TN6_FILIAL + TN6_MAT
			dbSeek( xFilial( "TN6" ) + cMatricula )
			While TN6->TN6_FILIAL = xFilial( "TN6" ) .And. TN6->TN6_MAT = cMatricula
				aAdd( aTarefas , { TN6->TN6_CODTAR } ) //Tarefas do Funcion�rio
				dbSkip()
			End

			//Busca os treinamentos necess�rios de acordo com o Centro de Custo do Funcion�rio
			dbSelectArea( "TY4" )
			dbSetOrder( 2 ) //TY4_FILIAL+TY4_TIPO+TY4_CC
			dbSeek( xFilial( "TY4" ) + "1" + cCCusto )
			While TY4->TY4_FILIAL == xFilial( "TY4" ) .And. TY4->TY4_CC == cCCusto
				If ( aScan( aTreinamentos , { | x | x[ 1 ] == TY4->TY4_CALEND + TY4->TY4_CURSO + TY4->TY4_TURMA } ) == 0 )
					aAdd( aTreinamentos , { TY4->TY4_CALEND + TY4->TY4_CURSO + TY4->TY4_TURMA } )
				EndIf
				dbSkip()
			End

			//Busca os treinamentos necess�rios de acordo com a Fun��o do Funcion�rio
			dbSetOrder( 3 ) //TY4_FILIAL+TY4_TIPO+TY4_CODFUN
			dbSeek( xFilial( "TY4" ) + "2" + cFuncao )
			While TY4->TY4_FILIAL == xFilial( "TY4" ) .And. TY4->TY4_CODFUN == cFuncao
				If ( aScan( aTreinamentos , { | x | x[ 1 ] == TY4->TY4_CALEND + TY4->TY4_CURSO + TY4->TY4_TURMA } ) == 0 )
					aAdd( aTreinamentos , { TY4->TY4_CALEND + TY4->TY4_CURSO + TY4->TY4_TURMA } )
				EndIf
				dbSkip()
			End

			//Busca os treinamentos necess�rios de acordo com as Tarefas do Funcion�rio
			dbSetOrder( 4 ) //TY4_FILIAL+TY4_TIPO+TY4_TAREFA
			For nCont := 1 To Len( aTarefas )
				dbSeek( xFilial( "TY4" ) + "3" + aTarefas[ nCont , 1 ] )
				While TY4->TY4_FILIAL == xFilial( "TY4" ) .And. TY4->TY4_TAREFA == aTarefas[ nCont , 1 ]
					If ( aScan( aTreinamentos , { | x | x[ 1 ] == TY4->TY4_CALEND + TY4->TY4_CURSO + TY4->TY4_TURMA } ) == 0 )
						aAdd( aTreinamentos , { TY4->TY4_CALEND + TY4->TY4_CURSO + TY4->TY4_TURMA } )
					EndIf
					dbSkip()
				End
			Next nCont

			//Busca os treinamentos necess�rios de acordo com os Riscos do Funcion�rio
			dbSetOrder( 5 ) //TY4_FILIAL+TY4_TIPO+TY4_NUMRIS
			For nCont := 1 To Len( aRisExp )
				dbSeek( xFilial( "TY4" ) + "4" + aRisExp[ nCont , 1 ] )
				While TY4->TY4_FILIAL == xFilial( "TY4" ) .And. TY4->TY4_NUMRIS == aRisExp[ nCont , 1 ]
					If ( aScan( aTreinamentos , { | x | x[ 1 ] == TY4->TY4_CALEND + TY4->TY4_CURSO + TY4->TY4_TURMA } ) == 0 )
						aAdd( aTreinamentos , { TY4->TY4_CALEND + TY4->TY4_CURSO + TY4->TY4_TURMA } )
					EndIf
					dbSkip()
				End
			Next nCont

			//Busca as informa��es dos Treinamentos necess�rios ao Funcion�rio
			If aScan( aTreinamentos , { | x | x[ 1 ] == cCalend + cCurso + cTurma } ) > 0
				dbSelectArea( "RA2" )
				dbSetOrder( 1 )
				If dbSeek( xFilial( "RA2" ) + cCalend ) .And. !Empty( RA2->RA2_ESOC ) //RA2_FILIAL + RA2_CALEND
					dbSelectArea( "RA7" )
					dbSetOrder( 1 )
					dbSeek( xFilial( "RA7" ) + RA2->RA2_INSTRU ) //RA7_FILIAL + RA7_INSTRU

						If !Empty( RA7->RA7_MAT )
							cCPFIns		:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + RA7->RA7_MAT , "RA_CIC" 	)
							cTpInst		:= "1"
							cEscIns		:= Posicione( "SX5" , 1 , xFilial( "SX5" ) + "26" + Posicione("SRA",1,xFilial("SRA")+RA7->RA7_MAT,"RA_GRINRAI") , "X5Descri()" )
							cFuncIns	:= Posicione( "SRA" , 1 , xFilial( "SRA" ) + RA7->RA7_MAT	, "RA_CODFUNC"	)
							cCBOIns		:= Posicione( "SRJ" , 1 , xFilial( "SRJ" ) + cFuncIns		, "RJ_CODCBO"	)
							cNacIns		:= If( Posicione( "SRA" , 1 , xFilial( "SRA" ) + RA7->RA7_MAT , "RA_NACIONA" ) == "10" , "1" , "2" )
						Else
							cCPFIns		:= RA7->RA7_CIC
							cTpInst		:= "2"
							cEscIns		:= RA7->RA7_FORINS
							cCBOIns		:= RA7->RA7_CBOINS
							cNacIns		:= RA7->RA7_NACION
						EndIf

						dDtTrei := RA2->RA2_DATAIN

						//Se estiver valendo a obrigatoriedade do eSocial
						If dDataBase >= dDtEsoc
							If RA2->RA2_DATAIN < dDtEsoc
								dDtTrei := dDtEsoc
							EndIf
						EndIf

						If dDtTrei < dDtAdm
							cIndTreinAnt := "S"
						Else
							cIndTreinAnt := "N"
						EndIf

						aAdd( aTreInf , { RA2->RA2_ESOC ,;
										MDTSubTxt( RA2->RA2_OBSERV ) ,;
										cIndTreinAnt ,;
										DToS( If( cIndTreinAnt == "S" , dDtAdm , dDtTrei ) ) ,;
										cValToChar( RA2->RA2_HORAS ) ,;
										RA2->RA2_MODTRE ,;
										RA2->RA2_TPTRE ,;
										cCPFIns ,;
										MDTSubTxt( RA7->RA7_NOME ) ,;
										cTpInst ,;
										AllTrim( MDTSubTxt( cEscIns ) ) ,;
										cCBOIns ,;
										cNacIns ,;
										AllTrim( RA2->RA2_DESC ) ,;
										AllTrim( RA7->RA7_NOME ) } )
				EndIf
			EndIf

			If lIncons //Verifica inconsist�ncias dos dados a serem enviados
				fInconsis( @aLogProc , @cMsgLog , cMatricula , cFilEnv )
			Else
				cXml := fCarrTrm( cFilEnv, cValToChar( nOpcTAF ), lXml, lMiddleware )

				If lXml
					xRet := cXml
				Else
					Begin Transaction

						//Caso o envio seja atrav�s do Middleware
						If lMiddleware

							If MDTVerTSVE( cCodCateg ) //Caso seja Trabalhador Sem V�nculo Estatut�rio
								cChvBus := AllTrim( cCpfTrab ) + AllTrim( cCodCateg ) + DToS( dDtAdm ) + aTreInf[ 1, 1 ]
							Else
								cChvBus := AllTrim( cCodUnic ) + aTreInf[ 1, 1 ]
							EndIf

							//Envia o evento ao eSocial atrav�s do Middleware
							MDTEnvMid( cFilEnv, cMatricula, "S2245", cChvBus, cXml, nOpcTAF )

						Else
							//Enviar como parametro a filial do Protheus na posi��o 1 e o XML na posi��o 2
							aErros := TafPrepInt( cEmpAnt, cFilEnv, cXml, , "1", "S2245" )

							If Len( aErros ) <= 0
								//Carrega array de controle de carga
								aAdd( aDados, OemToAnsi( STR0008 ) + " " + cCpfTrab + " " + OemToAnsi( STR0009 ) ) //"Treinamentos e Capacita��es"###"Enviado ao TAF com sucesso."
								Help( ' ', 1, STR0003, , STR0053, 2, 0 ) //"Aten��o"##"Registro enviado ao TAF com sucesso!"
							Else
								aAdd( aDados, OemToAnsi( STR0008 ) + " " + cCpfTrab + " " + OemToAnsi( STR0010 ) + " " + aErros[ 1 ] ) //"Treinamentos e Capacita��es"###"Falha no envio ao TAF:"
								Help( ' ', 1, STR0003, , STR0011 + CRLF + aErros[ 1 ], 2, 0, , , , , , { STR0050 } ) //"Aten��o" ## "Existem inconsist�ncias na comunica��o com o TAF!" ## "Para tentativa de reenvio favor refazer a baixa do treinamento!"
								xRet := .F. //Verificar como ficar� a manipula��o do registro para reenvio
							EndIf

							//Incrementa regua
							IncProc( OemToAnsi( STR0012 ) + " " + cCpfTrab ) //"Gerando o registro de:"
						EndIf

					End Transaction
				EndIf
			EndIf
		Else
			Help(' ',1,STR0003,,STR0047,2,0,,,,,,{STR0006}) // Aten��o ## "A informa��o da filial de envio ao eSocial est� vazia!" ## "Favor verificar!"
			xRet := .F.
		EndIf

	EndIf

	RestArea( aArea )

Return xRet
//---------------------------------------------------------------------
/*/{Protheus.doc} fCarrTrm

Carrega os Treinamentos

@return cXml Caracter Estrutura XML a ser enviada para o TAF

@sample fCarrTrm( "D MG 01" , 3 )

@param cFilEnv	- Caracter	- Filial de envio
@param cOper	- Caracter	- Indica a opera��o realizada

@author Luis Fellipy Bett
@since 03/07/2018
/*/
//---------------------------------------------------------------------
Static Function fCarrTrm( cFilEnv, cOper, lXml, lMiddleware )

	Local cXml	   := ""
	Local cChvBus  := ""
	Local lTSVE	   := MDTVerTSVE( cCodCateg )
	Local lNT1219  := SuperGetMv( "MV_TAFAMBE", .F., "2" ) $ "2/3" //Ambiente Produ��o Restrita
	Local nCont	   := 0

	Default cOper  := "3"

	If lTSVE //Caso seja Trabalhador Sem V�nculo Estatut�rio
		cChvBus := AllTrim( cCpfTrab ) + AllTrim( cCodCateg ) + DToS( dDtAdm ) + aTreInf[ 1, 1 ]
	Else
		cChvBus := AllTrim( cCodUnic ) + aTreInf[ 1, 1 ]
	EndIf

	//Cria o cabe�alho do Xml com o ID, informa��es do Evento e Empregador
	MDTGerCabc( @cXml, "S2245", cTpInsc, cNrInsc, lMiddleware, lXml, cOper, cChvBus )

	//Funcion�rio
	cXml += 		'<ideVinculo>'
	cXml += 			'<cpfTrab>'		+ cCpfTrab		+ '</cpfTrab>'
	If !( cCodCateg $ "901/903/904" )
		cXml +=			'<nisTrab>'		+ cNisTrab		+ '</nisTrab>'
	EndIf
	If !lTSVE //Se n�o for Trabalhador Sem V�nculo Estatut�rio
		cXml +=			'<matricula>'	+ cCodUnic	+ '</matricula>'
	Else
		cXml +=			'<codCateg>'	+ cCodCateg	+ '</codCateg>'
	EndIf
	cXml += 		'</ideVinculo>'

	//Treinamento
	For nCont := 1 To Len( aTreInf )
		cXml += 	'<treiCap>'
		cXml += 		'<codTreiCap>'	+ aTreInf[ nCont, 1 ]	+ '</codTreiCap>' // OBRIGAT�RIO - Informar o c�digo do treinamento/capacita��o, conforme Tabela 29.
		cXml += 		'<obsTreiCap>'	+ aTreInf[ nCont, 2 ]	+ '</obsTreiCap>' //Observa��o referente ao treinamento/capacita��o
		cXml +=			'<infoComplem>'
		If lNT1219
			cXml +=			'<indTreinAnt>'	+ aTreInf[ nCont, 3 ]		+ '</indTreinAnt>' //Indicar se o treinamento ocorreu antes da admiss�o, em outro empregador: S - Sim; N - N�o
		EndIf
		cXml += 			'<dtTreiCap>'	+ aTreInf[ nCont, 4 ]		+ '</dtTreiCap>' //Informar a data de in�cio do treinamento/capacita��o ou a data de in�cio da obrigatoriedade deste evento para o empregador no eSocial, a que for mais recente.
		If !( aTreInf[ nCont, 1 ] $ "1006/1207/3719" )
			cXml +=			'<durTreiCap>'	+ aTreInf[ nCont, 5 ]			+ '</durTreiCap>' //Informar a dura��o do treinamento/capacita��o, em horas.
			cXml +=			'<modTreiCap>'	+ aTreInf[ nCont, 6 ]			+ '</modTreiCap>' //Modalidade do treinamento/capacita��o, conforme op��es abaixo: 1 - Presencial; 2 - Educa��o a Dist�ncia (EaD); 3 - Mista
			cXml +=			'<tpTreiCap>'	+ aTreInf[ nCont, 7 ]			+ '</tpTreiCap>' //Tipo de treinamento/capacita��o, conforme op��es abaixo: 1 - Inicial; 2 - Peri�dico; 3 - Reciclagem; 4 - Eventual. 5 - Outros.
			cXml +=			'<ideProfResp>'
			cXml +=				'<cpfProf>'		+ aTreInf[ nCont, 8  ]		+ '</cpfProf>' //Preencher com o CPF do profissional respons�vel pelo treinamento/capacita��o.
			cXml +=				'<nmProf>'		+ aTreInf[ nCont, 9  ]		+ '</nmProf>' //Nome do profissional respons�vel pelo treinamento/capacita��o
			cXml +=				'<tpProf>'		+ aTreInf[ nCont, 10 ]		+ '</tpProf>' //O treinamento/capacita��o foi ministrado por: 1 - Profissional empregado do declarante; 2 - Profissional sem v�nculo de emprego/estatut�rio com o declarante
			cXml +=				'<formProf>'	+ aTreInf[ nCont, 11 ]		+ '</formProf>' //Forma��o do profissional respons�vel pelo treinamento/capacita��o (seja acad�mica, pr�tica ou outra forma).
			cXml +=				'<codCBO>'		+ aTreInf[ nCont, 12 ]		+ '</codCBO>' //Informar a Classifica��o Brasileira de Ocupa��o - CBO referente � forma��o do profissional respons�vel pelo treinamento/capacita��o
			cXml +=				'<nacProf>'		+ aTreInf[ nCont, 13 ]		+ '</nacProf>' //Indicativo da nacionalidade do profissional respons�vel pelo treinamento/capacita��o/exerc�cio simulado
			cXml +=			'</ideProfResp>'
		EndIf
		cXml +=			'</infoComplem>'
		cXml += 	'</treiCap>'
	Next nCont
	cXml += 	'</evtTreiCap>'
	cXml += '</eSocial>'

Return cXml

//---------------------------------------------------------------------
/*/{Protheus.doc} fInconsis
Carrega Inconsist�ncias (Se houver)

@return lRet L�gico Retorna falso caso haja alguma inconsist�ncia

@sample fInconsis( aLogProc , "cMsgLog" )

@param aLogProc	- Array		- Array que ir� receber os Logs
@param cMsgLog	- Caracter	- Variavel que recebera a mensagem de Log

@author Luis Fellipy Bett
@since 30/08/2018
/*/
//---------------------------------------------------------------------
Static Function fInconsis( aLogProc , cMsgLog , cMatricula , cFilEnv )

	Local cFilBkp		:= cFilAnt
	Local lRet 			:= .F.
	Local nCampos		:= 0
	Local nCont			:= 0
	Local aVldValues	:= {}
	Local cDtEsoc		:= ""
	Local dDtEsoc		:= SuperGetMv( "MV_NG2DTES" , .F. , SToD( "20190701" ) )

	//Condi��es
	Local bCond1		:= { || !( cCodCateg $ "901/903/904" ) }
	Local bCond2		:= { || Posicione( "C9V" , 3 , xFilial( "C9V" , cFilEnv ) + cCpfTrab , "C9V_NOMEVE" ) == "S2300" } //S� deve ser preenchido se tiver sido cadastrado no S-2300

	//Valida��es
	Local bValid1		:= { || cTpInsc $ "1/2" }
	Local bValid2		:= { || If( cTpInsc $ "1/2" , If( cTpInsc == "1" , CGC( cNrInsc ) , CHKCPF( cNrInsc ) ) , .T. ) }
	Local bValid3		:= { || CHKCPF( cCpfTrab ) }
	Local bValid4		:= { || ExistCPO( "C9V" , cCodUnic , 11 ) } //"Deve ser a mesma matr�cula informada no evento S-2200"
	Local bValid5		:= { || ExistCPO( "C87" , cCodCateg , 2 ) }

	Local cStrFunc	:= STR0044 + AllTrim(cMatricula) + " - " + AllTrim(Posicione( "SRA" , 1 , xFilial("SRA") + cMatricula , "RA_NOME" )) //"Funcion�rio: "

	cDtEsoc := DToS( dDtEsoc )
	cDtEsoc := " (" + SubStr( cDtEsoc , 7 , 2 ) + "/" + SubStr( cDtEsoc , 5 , 2 ) + "/" + SubStr( cDtEsoc , 1 , 4 ) + ")"

	//Seta a filial de envio para as valida��es de tabelas do TAF
	cFilAnt := cFilEnv

	// --- Estrutura do Array de envio das inconsist�ncias ---
	// Posi��o 1 - Campo a ser verificado
	// Posi��o 2 - String indicando o registro chave que est� sendo analisado junto a um " / " para impress�o no Log. Ex: Treinamento: 00001 /
	// Posi��o 3 - String indicando o t�tulo do campo que ser� analisado. Ex: Data In�cio Treinamento
	// Posi��o 4 - Bloco de C�digo com a condi��o para verifica��o do campo
	// Posi��o 5 - Bloco de C�digo com a valida��o do campo
	// Posi��o 6 - Descri��o da valida��o do campo para envio ao TAF
	// Obs: Na impress�o do Log a fun��o juntar� os dados da 2�, 3� e 1� coluna, sucessivamente. Ex: Treinamento: 00001 / Data In�cio Treinamento: 10/08/2017
	// -------------------------------------------------------

	aVldValues := {;
		{ cTpInsc	, cStrFunc + " / " , STR0016 , { || .T. }	, bValid1		, STR0023	} ,; //Treinamento: 00001 / Tipo Inscri��o: ## 'Deve ser igual a 1 (CNPJ) ou 2 (CPF)'
		{ cNrInsc	, cStrFunc + " / " , STR0017 , { || .T. }	, bValid2		, STR0024	} ,; //Treinamento: 00001 / Inscri��o: ## 'Deve ser um n�mero de CNPJ ou CPF v�lido'
		{ cCpfTrab 	, cStrFunc + " / " , STR0018 , { || .T. }	, bValid3		, STR0025	} ,; //Treinamento: 00001 / C.P.F.: ## 'Deve ser um n�mero de CPF v�lido'
		{ cNisTrab 	, cStrFunc + " / " , STR0019 , bCond1		, { || .T. }	,  			} ,; //Treinamento: 00001 / N.I.S.:
		{ cCodUnic 	, cStrFunc + " / " , STR0020 , { || .T. }	, bValid4		, STR0026	} ,; //Treinamento: 00001 / Matr�cula: ## 'Deve ser a mesma matr�cula informada no evento S-2200'
		{ cCodCateg , cStrFunc + " / " , STR0021 , bCond2		, bValid5		, STR0027 	} }  //Treinamento: 00001 / Categoria Funcion�rio: ## 'Deve existir na tabela 01 do eSocial'

	lRet := MDTInconEsoc( aVldValues , @aLogProc , @cMsgLog )

	If Len( aTreInf ) > 0
		For nCont := 1 To Len( aTreInf )

			If Empty( aTreInf[ nCont , 1 ] )
				aAdd( aLogProc , STR0045 + aTreInf[ nCont , 14 ] + " / " + STR0031 + ": " + STR0030 ) //"Treinamento: 00001 / C�digo do Treinamento: Em Branco"
				cMsgLog += CRLF + " - " + STR0031 //"C�digo do Treinamento"
				lRet := .F.
				aAdd( aLogProc , '' )
			ElseIf !ExistCPO( "V2M" , aTreInf[ nCont , 1 ] , 2 )
				aAdd( aLogProc , STR0045 + aTreInf[ nCont , 14 ] + " / " + STR0031 + ": " + aTreInf[ nCont , 1 ] ) //"Treinamento: 00001 / C�digo do Treinamento: XXX"
				aAdd( aLogProc , STR0022 + STR0028 ) //"Valida��o: Deve ser um c�digo existente na tabela 29 do eSocial"
				cMsgLog += CRLF + " - " + STR0031 //"C�digo do Treinamento"
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			If Empty( aTreInf[ nCont , 4 ] )
				aAdd( aLogProc , STR0045 + aTreInf[ nCont , 14 ] + " / " + STR0032 + ": " + STR0030 ) //"Treinamento: 00001 / Data In�cio Treinamento: Em Branco"
				cMsgLog += CRLF + " - " + STR0032 //"Data In�cio Treinamento"
				lRet := .F.
				aAdd( aLogProc , '' )
			ElseIf aTreInf[ nCont , 4 ] < DToS( SRA->RA_ADMISSA ) .Or. aTreInf[ nCont , 4 ] > DToS( dDataBase ) .Or. aTreInf[ nCont , 4 ] < DToS( dDtEsoc )
				aAdd( aLogProc , STR0045 + aTreInf[ nCont , 14 ] + " / " + STR0032 + ": " + aTreInf[ nCont , 4 ] ) //"Treinamento: 00001 / Data In�cio Treinamento: Em Branco"
				aAdd( aLogProc , STR0022 + STR0043 + CRLF + STR0051 + cDtEsoc ) //"Valida��o: Deve ser uma data igual ou anterior � data atual, igual ou posterior � data de admiss�o do funcion�rio e igual ou posterior � data de in�cio de obrigatoriedade do eSocial"
				cMsgLog += CRLF + " - " + STR0032 //Data In�cio Treinamento
				lRet := .F.
				aAdd( aLogProc , '' )
			EndIf

			If !( aTreInf[ nCont , 1 ] $ "1006/1207/3719" ) //Caso n�o for Autoriza��o para trabalhar em instala��es el�tricas e Opera��o e realiza��o de interven��es em m�quinas

				If Empty( aTreInf[ nCont , 5 ] )
					aAdd( aLogProc , STR0045 + aTreInf[ nCont , 14 ] + " / " + STR0033 + ": " + STR0030 ) //"Treinamento: 00001 / Dura��o Treinamento: Em Branco"
					cMsgLog += CRLF + " - " + STR0033 //"Dura��o Treinamento"
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf

				If Empty( aTreInf[ nCont , 6 ] )
					aAdd( aLogProc , STR0045 + aTreInf[ nCont , 14 ] + " / " + STR0034 + ": " + STR0030 ) //"Treinamento: 00001 / Modalidade Treinamento: Em Branco"
					cMsgLog += CRLF + " - " + STR0034 //"Modalidade Treinamento"
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf

				If Empty( aTreInf[ nCont , 7 ] )
					aAdd( aLogProc , STR0045 + aTreInf[ nCont , 14 ] + " / " + STR0035 + ": " + STR0030 ) //"Treinamento: 00001 / Tipo Treinamento: Em Branco"
					cMsgLog += CRLF + " - " + STR0035 //"Tipo Treinamento"
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf

				If !Empty( aTreInf[ nCont , 13 ] ) .And. aTreInf[ nCont , 13 ] == "1" //Se o instrutor for brasileiro valida o CPF
					If Empty( aTreInf[ nCont , 8 ] )
						aAdd( aLogProc , STR0046 + aTreInf[ nCont , 15 ] + " / " + STR0036 + ": " + STR0030 ) //"Instrutor: 00023 / CPF Respons�vel Treinamento: Em Branco"
						cMsgLog += CRLF + " - " + STR0036 //"CPF Respons�vel Treinamento"
						lRet := .F.
						aAdd( aLogProc , '' )
					ElseIf aTreInf[ nCont , 8 ] == cCpfTrab //Se o CPF do instrutor for igual ao do participante do curso
						aAdd( aLogProc , STR0046 + aTreInf[ nCont , 15 ] + " / " + STR0036 + ": " + aTreInf[ nCont , 8 ] ) //"Instrutor: 00023 / CPF Respons�vel Treinamento: 88245632103"
						aAdd( aLogProc , STR0022 + STR0052 ) //"Valida��o: O CPF do instrutor n�o pode ser igual ao de um participante do curso"
						cMsgLog += CRLF + " - " + STR0036 //"CPF Respons�vel Treinamento"
						lRet := .F.
						aAdd( aLogProc , '' )
					Else
						Help := .T.
						If !CHKCPF( aTreInf[ nCont , 8 ] )
							aAdd( aLogProc , STR0046 + aTreInf[ nCont , 15 ] + " / " + STR0036 + ": " + aTreInf[ nCont , 8 ] ) //"Instrutor: 00023 / CPF Respons�vel Treinamento: "
							aAdd( aLogProc , STR0022 + STR0025 ) //"Valida��o: Deve ser um n�mero de CPF v�lido"
							cMsgLog += CRLF + " - " + STR0036 //"CPF Respons�vel Treinamento"
							lRet := .F.
							aAdd( aLogProc , '' )
						EndIf
						Help := .F.
					EndIf
				EndIf

				If Empty( aTreInf[ nCont , 9 ] )
					aAdd( aLogProc , STR0046 + aTreInf[ nCont , 15 ] + " / " + STR0037 + ": " + STR0030 ) //"Instrutor: 00023 / Nome Respons�vel Treinamento: Em Branco"
					cMsgLog += CRLF + " - " + STR0037 //"Nome Respons�vel Treinamento"
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf

				If Empty( aTreInf[ nCont , 11 ] )
					aAdd( aLogProc , STR0046 + aTreInf[ nCont , 15 ] + " / " + STR0039 + ": " + STR0030 ) //"Instrutor: 00023 / Forma��o Respons�vel Treinamento: Em Branco"
					cMsgLog += CRLF + " - " + STR0039 //"Forma��o Respons�vel Treinamento"
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf

				If Empty( aTreInf[ nCont , 12 ] )
					aAdd( aLogProc , STR0046 + aTreInf[ nCont , 15 ] + " / " + STR0040 + ": " + STR0030 ) //"Instrutor: 00023 / "CBO Respons�vel Treinamento: Em Branco"
					cMsgLog += CRLF + " - " + STR0040 //"CBO Respons�vel Treinamento"
					lRet := .F.
					aAdd( aLogProc , '' )
				ElseIf !ExistCPO( "C8Z" , aTreInf[ nCont , 12 ] , 2 )
					aAdd( aLogProc , STR0046 + aTreInf[ nCont , 15 ] + " / " + STR0040 + ": " + aTreInf[ nCont , 12 ] ) //"Instrutor: 00023 / CBO Respons�vel Treinamento: XXX"
					aAdd( aLogProc , STR0022 + STR0029 ) //"Valida��o: Deve ser um c�digo existente na tabela de CBO's do TAF"
					cMsgLog += CRLF + " - " + STR0040 //"CBO Respons�vel Treinamento"
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf

				If Empty( aTreInf[ nCont , 13 ] )
					aAdd( aLogProc , STR0046 + aTreInf[ nCont , 15 ] + " / " + STR0048 + ": " + STR0030 ) //"Instrutor: 00023 / "Nacionalidade Respons�vel Treinamento: Em Branco"
					cMsgLog += CRLF + " - " + STR0048 //"Nacionalidade Respons�vel Treinamento"
					lRet := .F.
					aAdd( aLogProc , '' )
				ElseIf !( aTreInf[ nCont , 13 ] $ "1/2" )
					aAdd( aLogProc , STR0046 + aTreInf[ nCont , 15 ] + " / " + STR0048 + ": " + aTreInf[ nCont , 13 ] ) //"Instrutor: 00023 / "Nacionalidade Respons�vel Treinamento: 1"
					aAdd( aLogProc , STR0022 + STR0049 ) //"Valida��o: Deve ser igual a 1 (Brasileiro) ou 2 (Estrangeiro)"
					cMsgLog += CRLF + " - " + STR0048 //"Nacionalidade Respons�vel Treinamento"
					lRet := .F.
					aAdd( aLogProc , '' )
				EndIf

			EndIf

		Next nCont
	Else
		aAdd( aLogProc , cStrFunc + " / " + STR0042 ) //"Funcion�rio: MARQUINHOS JOS� / N�o existem treinamentos relacionados a este funcion�rio!"
		aAdd( aLogProc , '' )
		lRet := .F.
	EndIf

	//Volta para a filial do registro
	cFilAnt := cFilBkp

Return lRet