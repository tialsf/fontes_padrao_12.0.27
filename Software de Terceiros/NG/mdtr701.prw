#INCLUDE "Mdtr701.ch"
#INCLUDE "MSOLE.CH"
#Include "Protheus.ch"

#DEFINE _nVERSAO 2 //Versao do fonte
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � MDTR701  � Autor �	 Jackson Machado    � Data �15/03/2011���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Relatorio de PPP so com dados cadastrais                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function MDTR701()
//�����������������������������������������������������������������������Ŀ
//� Armazena variaveis p/ devolucao (NGRIGHTCLICK) 				  		  	  �
//�������������������������������������������������������������������������
Local aNGBEGINPRM := NGBEGINPRM(_nVERSAO)

PRIVATE cPerg := "MDT701    "

Private lMdtUnix := If( GetRemoteType() == 2 .or. isSRVunix() , .T. , .F. ) //Verifica se servidor ou estacao � Linux

If !MDT210UPD()//Verifica Aplicacao de Update
	NGRETURNPRM(aNGBEGINPRM)
	Return .F.
Endif

lSigaMdtPS := If( SuperGetMv("MV_MDTPS",.F.,"N") == "S", .t. , .f. )
cCliMdtps := ""

If lSigaMdtps
	/* Perguntas
	MDT701    �01      �Funcionario ?
	MDT701    �02      �Impressao ?
	MDT701    �03      �Arquivo Saida ?
	MDT701    �04      �Imprimir comprovante?
	MDT701    �05      �Termo de Responsabilidade
	*/
	cPerg    :=  "MDT701PS  "

	If Pergunte(cPerg,.t.) .and. ExCpoMDT('SRA',Mv_par03)
		FIMP701()
	Endif

Else
	/* Perguntas
	MDT555    �01      �Cliente ?
	MDT555    �02      �Loja

	MDT701    �03      �Funcionario ?
	MDT701    �04      �Impressao ?
	MDT701    �05      �Arquivo Saida ?
	MDT701    �06      �Imprimir comprovante?
	MDT701    �07      �Termo de Responsabilidade
	*/

	If Pergunte(cPerg,.t.) .and. ExCpoMDT('SRA',Mv_par01)
		FIMP701()
	Endif

Endif

//�����������������������������������������������������������������������Ŀ
//� Devolve variaveis armazenadas (NGRIGHTCLICK)                          �
//�������������������������������������������������������������������������
NGRETURNPRM(aNGBEGINPRM)

Return .t.
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �  FIMP70  � Autor �	 Jackson Machado    � Data �15/03/2011���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Impressao do relat�rio	   				                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function FIMP701()

Local cArqDot  := "ppp.dot"							// Nome do arquivo modelo do Word (Tem que ser .dot)
Local cArqComp := "comprovante_ppp.dot"
Local cPathDot := Alltrim(GetMv("MV_DIRACA"))			// Path do arquivo modelo do Word
Local cPathCom := Alltrim(GetMv("MV_DIRACA"))
Local cPathEst := Alltrim(GetMv("MV_DIREST")) 			// PATH DO ARQUIVO A SER ARMAZENADO NA ESTACAO DE TRABALHOZ
Local cBarraRem := "\"
Local cBarraSrv := "\"
Local cBarras    := If(isSRVunix(),"/","\")
Local cRootPath  := Alltrim(GetSrvProfString("RootPath",cBarras))
Local lComprovante := .F.
Local lImp := .F.

If GetRemoteType() == 2  //estacao com sistema operacional unix
	cBarraRem := "/"
Endif
If isSRVunix()  //servidor eh da familia Unix (linux, solaris, free-bsd, hp-ux, etc.)
	cBarraSrv := "/"
Endif

cPathDot += If(Substr(cPathDot,len(cPathDot),1) != cBarraSrv,cBarraSrv,"") + cArqDot
cPathCom += If(Substr(cPathCom,len(cPathCom),1) != cBarraSrv,cBarraSrv,"") + cArqComp
cPathEst += If(Substr(cPathEst,len(cPathEst),1) != cBarraRem,cBarraRem,"")

//Cria diretorio se nao existir
MontaDir(cPathEst)

//Se existir .dot na estacao, apaga!
If File( cPathEst + cArqDot )
	Ferase( cPathEst + cArqDot )
EndIf
If !File(cPathDot)
	MsgStop(STR0013+chr(10)+STR0014,STR0012) // "O arquivo ppp.dot n�o foi encontrado no servidor."###"Verificar par�metro 'MV_DIRACA'."###"ATEN��O"
	Return
EndIf
If !File(cPathCom)
	MsgStop(STR0024+chr(10)+STR0014,STR0012) // "O arquivo comprovante_ppp.dot n�o foi encontrado no servidor."###"Verificar par�metro 'MV_DIRACA'."###"ATEN��O"
	Return
EndIf
CpyS2T(cPathDot,cPathEst,.T.) 	// Copia do Server para o Remote, eh necessario
CpyS2T(cPathCom,cPathEst,.T.) 	// Copia do Server para o Remote, eh necessario
// para que o wordview e o proprio word possam preparar o arquivo para impressao e
// ou visualizacao .... copia o DOT que esta no ROOTPATH Protheus para o PATH da
// estacao , por exemplo C:\WORDTMP

Private cCNPJ, cNomEmp, nSizeCNAE, cCNAE, cBRPD, cRegime

lImp := .T.
If lSigaMdtPs
	lImpress	:= If(mv_par04 == 1,.t.,.f.)	//Verifica se a saida sera em Tela ou Impressora
	cArqSaida	:= If(Empty(mv_par05),"Documento1",AllTrim(mv_par05))	// Nome do arquivo de saida
Else
	lImpress	:= If(mv_par02 == 1,.t.,.f.)	//Verifica se a saida sera em Tela ou Impressora
	cArqSaida	:= If(Empty(mv_par03),"Documento1",AllTrim(mv_par03))	// Nome do arquivo de saida
Endif

oWord := OLE_CreateLink('TMsOleWord97')//Cria link como Word

If lImpress //Impressao via Impressora
	OLE_SetProperty(oWord,oleWdVisible,  .F.)
	OLE_SetProperty(oWord,oleWdPrintBack,.T.)
Else //Impressao na Tela(Arquivo)
	OLE_SetProperty(oWord,oleWdVisible,  .F.)
	OLE_SetProperty(oWord,oleWdPrintBack,.F.)
EndIf
cType := "ppp| *.dot"
OLE_NewFile(oWord,cPathEst + cArqDot) //Abrindo o arquivo modelo automaticamente

If lSigaMdtps
	If !Empty(SA1->A1_CGC)
		cCNPJ := Transform(SA1->A1_CGC,"@R 99.999.999/9999-99")
	Endif
Else
	If !Empty(SM0->M0_CGC)
		If SM0->M0_TPINSC != 2
			cCNPJ := Transform(SM0->M0_CGC,"@R 99.999.99999/99")
		Else
			cCNPJ := Transform(SM0->M0_CGC,"@R 99.999.999/9999-99")
		Endif
	Endif
Endif

If lSigaMdtps
	cNomEmp := Substr(SA1->A1_NOME,1,40)
Else
	cNomEmp := Substr(SM0->M0_NOMECOM,1,40)
Endif

If lSigaMdtps
	If !Empty(SA1->A1_ATIVIDA)
		nSizeCNAE := Len(alltrim(SA1->A1_ATIVIDA))
		If nSizeCNAE > 5
			cCNAE := Transform(SA1->A1_ATIVIDA,"@R 99.99-9/99")
		Else
			cCNAE := Transform(SA1->A1_ATIVIDA,"@R 99.99-9")
		Endif
	Endif
Else
	If !Empty(SM0->M0_CNAE)
		nSizeCNAE := Len(alltrim(SM0->M0_CNAE))
		If nSizeCNAE > 5
			cCNAE := Transform(SM0->M0_CNAE,"@R 99.99-9/99")
		Else
			cCNAE := Transform(SM0->M0_CNAE,"@R 99.99-9")
		EndIf
	Endif
Endif


//Dados Empresa
OLE_SetDocumentVar(oWord,"Emp_Nome",cNomEmp)
OLE_SetDocumentVar(oWord,"Emp_cnpj",cCNPJ)
OLE_SetDocumentVar(oWord,"Emp_cnae",cCNAE)

Dbselectarea("SRA")
Dbsetorder(1)

If lSigaMdtPs
	Dbseek(xFilial("SRA")+Mv_par03)
Else
	Dbseek(xFilial("SRA")+Mv_par01)
Endif

cBRPDH := "NA"
If SRA->RA_BRPDH == "1"
	cBRPDH := "BR"
Elseif SRA->RA_BRPDH == "2"
	cBRPDH := "PDH"
Endif

//Dados Funcionario
OLE_SetDocumentVar(oWord,"Fun_Nome",SRA->RA_NOME)
OLE_SetDocumentVar(oWord,"Fun_mat",SRA->RA_MAT)
OLE_SetDocumentVar(oWord,"Fun_pis",Transform(SRA->RA_PIS,"@R 999.99999.99-9"))
OLE_SetDocumentVar(oWord,"Fun_ctps",SRA->RA_NUMCP+" / "+SRA->RA_SERCP+" - "+SRA->RA_UFCP)
OLE_SetDocumentVar(oWord,"Fun_brpdh",cBRPDH)
OLE_SetDocumentVar(oWord,"Fun_Admissao",SRA->RA_ADMISSA)
OLE_SetDocumentVar(oWord,"Fun_Sexo",SRA->RA_SEXO)
OLE_SetDocumentVar(oWord,"Fun_Nasc",SRA->RA_NASC)
Dbselectarea("SR6")
Dbsetorder(1)
Dbseek(xFilial("SR6")+SRA->RA_TNOTRAB)

If !EMPTY(Substr(SR6->R6_REVEZAM,1,20))
	cRegime :=	Substr(SR6->R6_REVEZAM,1,20)
Else
	cRegime := "NA"
Endif
OLE_SetDocumentVar(oWord,"Fun_Reg",cRegime)

If lSigaMdtPs
	If mv_par06 == 1
		lComprovante := .T.
	Endif
Else
	If mv_par04 == 1
		lComprovante := .T.
	Endif
Endif

If lComprovante
	OLE_SetDocumentVar(oWord,"dir",cPathEst)
	OLE_SetDocumentVar(oWord,"doc",cArqComp)
	OLE_ExecuteMacro(oWord,"NewPage")
	OLE_ExecuteMacro(oWord,"CopyPaste")
	OLE_SetDocumentVar(oWord,"matricula",SRA->RA_MAT)

	dbSelectArea("CTT")
	dbSetOrder(1)
	dbSeek(xFilial("CTT")+SRA->RA_CC)
	OLE_SetDocumentVar(oWord,"cc",CTT->CTT_DESC01)

	dbSelectArea("SRJ")
	dbSetOrder(1)
	dbSeek(xFilial("SRJ")+SRA->RA_CODFUNC)
	OLE_SetDocumentVar(oWord,"func",SRJ->RJ_DESC)

	dbSelectArea("TMZ")
	dbSetOrder(1)
	If lSigaMdtPs
		dbSeek(xFilial("TMZ")+mv_par07)
	Else
		dbSeek(xFilial("TMZ")+mv_par05)
	Endif
	cMemo := TMZ->TMZ_DESCRI

	cMemo := StrTran(cMemo,  Chr(13)+Chr(10) , "#*" )
	cMemo := StrTran(cMemo,  Chr(13) , "#*" )
	cMemo := Alltrim(cMemo)

	If SubStr(cMemo,Len(cMemo)-1,2) == "#*"
	   cMemo := SubStr(cMemo,1,Len(cMemo)-2)
	Endif
	//cMemo := StrTran(cMemo,  Chr(9) , " " )

	OLE_SetDocumentVar(oWord,"termo",cMemo)
	OLE_ExecuteMacro(oWord,"ImpTermo")

	OLE_SetDocumentVar(oWord,"cidade",SA1->A1_MUN)
	OLE_SetDocumentVar(oWord,"dia",Day(dDataBase))
	OLE_SetDocumentVar(oWord,"mes",MesExtenso(dDataBase))
	OLE_SetDocumentVar(oWord,"ano",Year(dDataBase))
Endif

OLE_ExecuteMacro(oWord,"Atualiza") //Executa a macro que atualiza os campos do documento
OLE_ExecuteMacro(oWord,"Begin_Text") //Posiciona o cursor no inicio do documento

cRootPath := GetPvProfString( GetEnvServer(), "RootPath", "ERROR", GetADV97() )
cRootPath := IF( RIGHT(cRootPath,1) == cBarraSrv,SubStr(cRootPath,1,Len(cRootPath)-1), cRootPath)

IF lImpress //Impressao via Impressora
	OLE_SetProperty( oWord, '208', .F. ) ; OLE_PrintFile( oWord, "ALL",,, 1 )
Else //Impressao na Tela(Arquivo)
	OLE_SetProperty(oWord,oleWdVisible,.t.)
	OLE_ExecuteMacro(oWord,"Maximiza_Tela")
	If !lMdtUnix
		If DIRR701(cPathEst)
			OLE_SaveAsFile(oWord,cPathEst+cArqSaida,,,.f.,oleWdFormatDocument)
		Endif
	Endif
	MsgInfo(STR0011) //"Alterne para o programa do Ms-Word para visualizar o documento ou clique no botao para fechar."
EndIF
OLE_CloseFile(oWord) //Fecha o documento
OLE_CloseLink(oWord) //Fecha o documento


If !lImp
	MsgStop(STR0015, STR0012)//"N�o existem dados para imprimr."##"Aten��o"
Endif
Return .t.
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � DIRR701  � Autor �     Jackson Machado   � Data �15/03/2011���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Verifica se o diretorio existe.                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function DIRR701(cCaminho)

Local lDir := .F.
Local cBARRAS   := If(isSRVunix(),"/","\")
Local cBARRAD := If(isSRVunix(),"//","\\")

If !empty(cCaminho) .and. !(cBARRAD$cCaminho)
	cCaminho := alltrim(cCaminho)
	if Right(cCaminho,1) == cBARRAS
		cCaminho := SubStr(cCaminho,1,len(cCaminho)-1)
	Endif
	lDir :=(Ascan( Directory(cCaminho,"D"),{|_Vet | "D" $ _Vet[5] } ) > 0)
EndIf
Return lDir

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � MDT701CLI� Autor �    Jackson Machado    � Data �15/03/2011���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Atualiza a variavel cCliMdtps                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function MDT701CLI(nPar)
Local nTa1 := If((TAMSX3("A1_COD")[1]) < 1,6,(TAMSX3("A1_COD")[1]))
Local nTa1L := If((TAMSX3("A1_LOJA")[1]) < 1,2,(TAMSX3("A1_LOJA")[1]))
Default nPar := 1

If ValCli(nPar,mv_par01,'mv_par02',mv_par01,'mv_par02',nTa1,nTa1L)
	cCliMdtps := mv_par01+mv_par02
Else
	Return .F.
Endif

Return .T.