#INCLUDE "MNTA658.ch"
#Include "Protheus.ch"
#DEFINE _nVERSAO 2 //Versao do fonte
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � MNTA658  � Autor � Marcos Wagner Junior  � Data � 14/10/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Cadastro de Motivos de Saida de Combustivel                ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � SIGAMNT                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function MNTA658()

//�����������������������������������������������������������������������Ŀ
//� Armazena variaveis p/ devolucao (NGRIGHTCLICK) 							  �
//�������������������������������������������������������������������������
Local aNGBEGINPRM := NGBEGINPRM(_nVERSAO)

Private aRotina := MenuDef()
Private cCadastro := STR0001 //"Cadastro de Motivos de Saida de Combustivel"
Private bNGGRAVA := {|| .T. }
aCHKDEL := {{'TTX->TTX_MOTIVO',"TTH",5}}

dbSelectArea("TTX")
dbSetOrder(1)
mBrowse(6,1,22,75,"TTX")

//�����������������������������������������������������������������������Ŀ
//� Devolve variaveis armazenadas (NGRIGHTCLICK)                          �
//�������������������������������������������������������������������������
NGRETURNPRM(aNGBEGINPRM)

Return .T.

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �MenuDef   � Autor � Marcos Wagner Junior  � Data � 14/10/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Utilizacao de menu Funcional                               ���
���          �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Array com opcoes da rotina.                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Parametros do array a Rotina:                               ���
���          �1. Nome a aparecer no cabecalho                             ���
���          �2. Nome da Rotina associada                                 ���
���          �3. Reservado                                                ���
���          �4. Tipo de Transa��o a ser efetuada:                        ���
���          �		1 - Pesquisa e Posiciona em um Banco de Dados           ���
���          �    2 - Simplesmente Mostra os Campos                       ���
���          �    3 - Inclui registros no Bancos de Dados                 ���
���          �    4 - Altera o registro corrente                          ���
���          �    5 - Remove o registro corrente do Banco de Dados        ���
���          �5. Nivel de acesso                                          ���
���          �6. Habilita Menu Funcional                                  ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Static Function MenuDef()

Local aRotina := { { STR0002, "AxPesqui"  , 0 , 1},; //"Pesquisar"
                    { STR0003, "NGCAD01"   , 0 , 2},; //"Visualizar"
                    { STR0004, "NGCAD01"   , 0 , 3},; //"Incluir"
                    { STR0005, "NGCAD01"   , 0 , 4},; //"Alterar"
                    { STR0006, "NGCAD01"   , 0 , 5, 3} } //"Excluir"
Return(aRotina)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �MNTA658BNG� Autor � Marcos Wagner Junior  � Data � 16/10/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Cadastro de Motivos de Saida de Combustivel                ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � SIGAMNT                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function MNTA658TPL()
	
	Local lRet := .T.
	Local cAliasQry := ''
	
	If NGCADICBASE("TTX_MOTTTH","A","TTX",.F.) .And. ALTERA
		
		If TTX->TTX_MOTTTH  <> M->TTX_MOTTTH 
	
			cAliasQry := GetNextAlias()
			cQuery := " SELECT COUNT(*) as TTHCONT FROM " + RetSqlName("TTH")+" TTH "
			cQuery += " WHERE TTH.TTH_MOTIV2 = '"+M->TTX_MOTIVO+"'"
			cQuery += " AND   TTH.D_E_L_E_T_ = ' ' "


			cQuery := ChangeQuery(cQuery)
			dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery),cAliasQry, .F., .T.)
			
			If !Eof()
				If (cAliasQry)->TTHCONT > 0
					MsgStop(STR0007,STR0008) //"ATEN��O" ## "O 'Tipo Lancam.' selecionado j� est� sendo utilizado pelo motivo: "
						 
					lRet := .f.
				EndIf
			Endif	
			
			(cAliasQry)->(dbCloseArea())
			
		EndIf
		
	Endif

Return lRet
