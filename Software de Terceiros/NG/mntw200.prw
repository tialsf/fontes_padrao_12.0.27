#INCLUDE "MNTW200.ch"
#INCLUDE "RWMAKE.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} MNTW200
Workflow - Documentos Vencidos/A Vencer

@type function

@source MNTW200.prx

@author Marcos Wagner Junior
@since 13/08/2008

	Nota: Atualizado para utiliza��o da fun��o de envio de workflow
	NGSendMail() pois o antigo processo TMailMessage() estava gerando
	problemas com as tabelas compartilhadas.
	@author Rodrigo Luan Backes
	@since 15/08/2016
	S.S.: 028323

@sample MNTW200()

@return L�gico
/*/
//---------------------------------------------------------------------
Function MNTW200()

	Local lSchedule 		:= Type("oMainWnd")=="O"
	Local cIniFile			:= GetAdv97()
	Local cCodEmp			:= ""
	Local cCodFil			:= ""

	Private cEmailTAB		:= ""

	//Abre tabelas necessarias
	If !lSchedule
		cCodEmp := GetPvProfString("ONSTART",STR0001,"",cInIfile) //"Empresa"
		cCodFil := GetPvProfString("ONSTART",STR0002,"",cInIfile) //"Filial"

		If cCodEmp == '-1' .Or. cCodFil == '-1'
			Return .F.
		EndIf

		RPCSetType(3)
		RPCSetEnv(cCodEmp,cCodFil,"","","MNT","",{"ST9","TS2","TS0","TSK"})

	EndIf

	cEmailTAB := NgEmailWF("5","MNTW200")

	MNTW200F()

	If !lSchedule
		RpcClearEnv()
	EndIf

Return  .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} MNTW200F
Envio do Workflow

@type function

@source MNTW200.prx

@author Marcos Wagner Junior
@since 13/08/2008

	Nota: Atualizado para utiliza��o da fun��o de envio de workflow
	NGSendMail() pois o antigo processo TMailMessage() estava gerando
	problemas com as tabelas compartilhadas.
	@author Rodrigo Luan Backes
	@since 15/08/2016
	S.S.: 028323

@sample MNTW200F()

@return L�gico
/*/
//---------------------------------------------------------------------
Function MNTW200F()

	Local aArea			:= GetArea()
	Local cSmtp			:= GetNewPar("MV_RELSERV", "") 	//Servidor SMTP
	Local cConta		:= GetNewPar("MV_RELAUSR","") 	// Usu�rio para autentica��o no servidor de e-mail
	Local cCntEmail		:= GetNewPar("MV_RELACNT","")	// Conta de e-mail do usu�rio no servidor de e-mail
	Local lAutentica	:= GetNewPar("MV_RELAUTH",.F.)	// Autentica��o (Sim/N�o)
	Local nSmtpPort		:= GetNewPar("MV_PORSMTP",0)	// Porta Servidor SMTP
	Local cArquivo		:= "MNTW200.htm"
	Local cDir			:= AllTrim(GetMV("MV_WFDIR"))
	Local cAssunto   	:= DtoC(MsDate())+" - "+STR0009 //"Documentos Vencidos/A Vencer"
	Local i 			:= 0
	Local aRegistros 	:= {}
	Local nPos			:= 0
	Local cEMAIL_All	:= ""
	Local lEmailRet		:= .T.

	If (nPos := At(":",cSmtp)) <> 0
		nSmtpPort		:= Val( SubStr( cSmtp, nPos+1, Len( cSmtp ) ) )
		cSmtp			:= SubStr( cSmtp, 1, nPos-1 )
	EndIf

	//Coloca a barra no final do parametro do diretorio
	If Substr(cDir,Len(cDir),1) != "\"
		cDir += "\"
	EndIf

	//Verifica se existe o arquivo de workflow
	If !File(cDir+cArquivo)
		MsgInfo(">>> "+STR0008+" "+cDir+cArquivo) //"Nao foi encontrado o arquivo"
		Return .F.
	EndIf

	dbSelectArea("TS2")
	dbSetOrder(01)
	dbSeek(xFilial("TS2"))
	dbGoTop()
	While !EoF()
		If TS2->TS2_FILIAL == xFilial("TS2")

			dbSelectArea("TS0")
			dbSetOrder(01)
			dbSeek(xFilial("TS0")+TS2->TS2_DOCTO)

			If Empty(TS2->TS2_DTPGTO) .And. Empty(TS2->TS2_NOTFIS) .And. TS2->TS2_DTVENC <= dDATABASE
				aAdd(aRegistros,{	TS2->TS2_PLACA,;
					TS2->TS2_CODBEM,;
					NGSEEK("ST9",TS2->TS2_CODBEM,1,'T9_NOME'),;
					TS2->TS2_DOCTO,;
					TS0->TS0_NOMDOC,;
					TS2->TS2_DTVENC,;
					TS2->TS2_PARCEL,;
					Transform(TS2->TS2_VALOR,"@E 999,999.99")})
			EndIf

		EndIf

		dbSelectArea("TS2")
		dbSkip()
	End

	If Len(aRegistros) = 0
		ApMsgAlert(STR0019) //"N�o existem dados para enviar o workflow!"
		Return .T.
	EndIf

	If ExistBlock("MNTW2001")
		ExecBlock("MNTW2001",.F.,.F.,{aRegistros})
		Return lRetu
	Else
		aSort(aRegistros,,,{|x,y| DtoS(x[6])+x[1] < DtoS(y[6])+y[1] })

		//Inicia o processo
		cMailMsg := '<html>'
		cMailMsg += '<head>'
		cMailMsg += '<meta http-equiv="Content-Language" content="pt-br">'
		cMailMsg += '<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">'
		cMailMsg += '<meta name="GENERATOR" content="Microsoft FrontPage 4.0">'
		cMailMsg += '<meta name="ProgId" content="FrontPage.Editor.Document">'
		cMailMsg += '<title>Aviso sobre Solicita��o de Servi�os</title>'
		cMailMsg += '</head>'
		cMailMsg += '<body bgcolor="#FFFFFF">'
		cMailMsg += '<table border=0 WIDTH=100% cellpadding="1">'
		cMailMsg += '<tr>'
		cMailMsg += '   <td bgcolor="#C0C0C0" align="left"><b><font face="Arial" size="2">'+STR0011+'</font></b></td>' //"Proxima Manutencao"
		cMailMsg += '   <td bgcolor="#C0C0C0" align="left"><b><font face="Arial" size="2">'+STR0012+'</font></b></td>'   //"Bem"
		cMailMsg += '   <td bgcolor="#C0C0C0" align="left"><b><font face="Arial" size="2">'+STR0013+'</font></b></td>'   //"Nome do Bem"
		cMailMsg += '   <td bgcolor="#C0C0C0" align="left"><b><font face="Arial" size="2">'+STR0014+'</font></b></td>'   //"Centro Custo"
		cMailMsg += '   <td bgcolor="#C0C0C0" align="left"><b><font face="Arial" size="2">'+STR0013+'</font></b></td>'   //"Nome do C.C"
		cMailMsg += '   <td bgcolor="#C0C0C0" align="left"><b><font face="Arial" size="2">'+STR0015+'</font></b></td>'   //"Servico"
		cMailMsg += '   <td bgcolor="#C0C0C0" align="left"><b><font face="Arial" size="2">'+STR0016+'</font></b></td>'   //"Nome do Servico"
		cMailMsg += '   <td bgcolor="#C0C0C0" align="left"><b><font face="Arial" size="2">'+STR0017+'</font></b></td>' //"Sequencia"
		cMailMsg += '</tr>'

		ProcRegua(Len(aRegistros))

		For i := 1 to Len(aRegistros)
			IncProc()
			cMailMsg += '<tr>'
			cMailMsg += '   <td bgcolor="#EEEEEE" align="left"><font face="Arial" size="1">'+aRegistros[i,1]+'</font></td>'
			cMailMsg += '   <td bgcolor="#EEEEEE" align="left"><font face="Arial" size="1">'+aRegistros[i,2]+'</font></td>'
			cMailMsg += '   <td bgcolor="#EEEEEE" align="left"><font face="Arial" size="1">'+aRegistros[i,3]+'</font></td>'
			cMailMsg += '   <td bgcolor="#EEEEEE" align="left"><font face="Arial" size="1">'+aRegistros[i,4]+'</font></td>'
			cMailMsg += '   <td bgcolor="#EEEEEE" align="left"><font face="Arial" size="1">'+aRegistros[i,5]+'</font></td>'
			cMailMsg += '   <td bgcolor="#EEEEEE" align="left"><font face="Arial" size="1">'+DTOC(aRegistros[i,6])+'</font></td>'
			cMailMsg += '   <td bgcolor="#EEEEEE" align="left"><font face="Arial" size="1">'+aRegistros[i,7]+'</font></td>'
			cMailMsg += '   <td bgcolor="#EEEEEE" align="left"><font face="Arial" size="1">'+aRegistros[i,8]+'</font></td>'
			cMailMsg += '</tr>'
		Next

		cMailMsg += '</table>'
		cMailMsg += '<br><hr>'
		cMailMsg += '</body>'
		cMailMsg += '</html>'

	EndIf

	If !Empty(cEmailTAB)
		cEMAIL_All 		:= cEmailTAB
	ElseIf !Empty(cCntEmail)
		cEMAIL_All 		:= cCntEmail
	Else
		ShowHelpDlg(STR0028, {STR0026 + STR0022 + "."}, 2, {STR0024}, 1)//"Destinat�rio do E-mail n�o informado."##" Favor, verificar par�metro MV_RELACNT"##"ou se o funcion�rio possui E-mail cadastrado no sistema."##"Envio de E-mail cancelado!"
	EndIf

	If lAutentica
		// Valida��o SMTP, se n�o informado, cancela envio de WF
		If Empty(cSmtp)
			MsgInfo(STR0027 + STR0024) //"Servidor SMTP n�o informado! Favor, verificar par�metro MV_RELSERV."##" Envio do e-mail cancelado!"
			Return .F.
		EndIf

		If Empty(cConta)
			MsgInfo(STR0020 + STR0021 + STR0024) //"Verifique os par�metros de configura��o: MV_RELAUSR e MV_RELAUTH."##" Envio do e-mail cancelado!"
			Return .F.
		EndIf
	EndIf

	//Fun��o de envio de WorkFlow
	lEmailRet := NGSendMail( , cEMAIL_All + Chr(59) , , , OemToAnsi( cAssunto ) , , cMailMsg )

	If lEmailRet
		MsgInfo(STR0009 + STR0029 + ": " + cEMAIL_All + "!") //"Documentos Vencidos/A Vencer"##" enviados para"
	EndIf

Return lEmailRet
