#INCLUDE "PROTHEUS.CH"
#INCLUDE "COLORS.CH"
#INCLUDE "TBICONN.CH"
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �NfdsXml   � Autor � Roberto Souza         � Data �21/05/2009���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Exemplo de geracao da Nota Fiscal Digital de Servi�os       ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Xml para envio                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpC1: Tipo da NF                                           ���
���          �       [0] Entrada                                          ���
���          �       [1] Saida                                            ���
���          �ExpC2: Serie da NF                                          ���
���          �ExpC3: Numero da nota fiscal                                ���
���          �ExpC4: Codigo do cliente ou fornecedor                      ���
���          �ExpC5: Loja do cliente ou fornecedor                        ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���24/11/10  � Vitor Felipe  � Incluido geracao de arquivo XML modelo 102 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Function NFSEXml(cCodMun,cTipo,dDtEmiss,cSerie,cNota,cClieFor,cLoja,cMotCancela,cFuncExec,aAIDF,aTitIssRet)
	local	aRetorno		:= {"",""}
	local	aDados   		:= {}

	default cMotCancela	:= ""
	default cFuncExec		:= ""
	default dDtEmiss		:= date()
	default aAIDF			:= {""}
	default aTitIssRet	:= {}

	aAdd(aDados,cCodMun )
	aAdd(aDados,cTipo   )
	aAdd(aDados,dDtEmiss)
	aAdd(aDados,cSerie  )
	aAdd(aDados,cNota   )
	aAdd(aDados,cClieFor)
	aAdd(aDados,cLoja   )
	aAdd(aDados,cMotCancela)
	aAdd(aDados,aTitIssRet)

	If Empty(cFuncExec)
		cFuncExec := getRDMakeNFSe(cCodMun,cTipo)
	EndIf

	If cFuncExec == "nfseXMLEnv"		
		aRetorno :=  ExecBlock(cFuncExec,.F.,.F.,{cTipo, dDtEmiss, cSerie, cNota, cClieFor, cLoja, cMotCancela, aAIDF})
	ElseIf !Empty(cFuncExec)
		aRetorno 	:= ExecBlock(cFuncExec,.F.,.F.,aDados)
	EndIf
Return(aRetorno)