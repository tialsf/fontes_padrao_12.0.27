#INCLUDE 'PROTHEUS.ch'
#INCLUDE "TBICONN.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "RPTDEF.CH"
#DEFINE LOG_ERROR 	1
#DEFINE LOG_WARNING     2
#DEFINE LOG_INFO 	3
#DEFINE LOG_PRINT	4
static __init
/*/{Protheus.doc} DistMail
    JOB respons�vel por processar a distribui��o de danfe por email 
    para o TSS junto com a danfe gerada no protheus
    @type  Function
    @author Bruno Seiji
    @since 30/05/2019
    @version 12.1.17
/*/
main function DistMail(cSleep)

    local cAliasProc    := getNextAlias()
    local nSleep        := 0
    local cLock 
    default cSleep := "60"

    nSleep := val(cSleep) * 1000 //conversao para milisegundos

    PRTMSG("Verificando estrutura da tabela DISTMAIL", LOG_PRINT)
    
    createThreads()//Cria pool de Threads para PRocesasmenro dos emails
    crtTblMail()//cria o banco sqlite
    startjob( "fillTblMail", getEnvserver(), .F.)//cadastro das empresas para envio de email
    
    while !killApp()
        
        if dbSqlExec(cAliasProc, "SELECT * FROM DISTMAIL WHERE EMPRESA != ' ' AND FILIAL != ' ' AND ENTIDADE != ' ' AND URL != ' ' AND ENABLE = 1 ", "SQLITE_SYS")
            while !(cAliasProc)->(EOF())  
                cLock := "procSendMail-" + alltrim((cAliasProc)->EMPRESA) + alltrim((cAliasProc)->FILIAL)
                if( !jobIsRunning( cLock ) )                                    
                    if !ERPIPCGO("procSendMail", .F., "IPC_DISTMAIL", (cAliasProc)->EMPRESA, (cAliasProc)->FILIAL, (cAliasProc)->ENTIDADE, cLock )                      
                        smartjob( "procSendMail", getEnvserver(), .F., (cAliasProc)->EMPRESA, (cAliasProc)->FILIAL, (cAliasProc)->ENTIDADE, cLock )
                    endif                    
                endif
                (cAliasProc)->(DbSkip())
            end
            sleep(nSleep)
        endif
        
        if select( (cAliasProc) ) > 0
            (cAliasProc)->(DBCloseArea())
        endif            
    enddo

return

/*/{Protheus.doc} procSendMail
    Fun��o respons�vel por executar o processo a partir da chamada do smartjob
    @type  Function
    @author Bruno Seiji
    @since 30/05/2019
    @version 12.1.17
    @param entidade Entidade que ser� processada
/*/
function procSendMail(cEmp, cFil, cEntidade, cLock)

    local aNfe
    local lContinua :=  .T.
    local nSequencia
    local nHdl

    nHdl := jobSetRunning( cLock, .T.) 
    if(nHdl < 0 )
        PRTMSG("Falha ao alocar processo", LOG_ERROR)
        return
    endif

    cEmp := alltrim(cEmp)
    cFil := alltrim(cFil)
    cEntidade := alltrim(cEntidade)
    
    while(lContinua)
        //efetua consulta das NFe que seram processadas
        PRTMSG("Consultando E-mails pendentes. Empresa: " + cEmp + " Filial: " + cFil, LOG_PRINT)
        aNfe := consNfeDnf(cEmp, cFil, @lContinua, @nSequencia)
        if !empty(aNfe)
            //processo de cria��o e distribui��o da DANFE
            PRTMSG("Excutando distribuicao dos E-mails pendentes. Empresa: " + cEmp + " Filial: " + cFil + " Total de Notas: " + alltrim(str(len(aNfe))), LOG_PRINT)
            distNfeDnf(cEmp, cFil, cEntidade, aNfe)
            //incremento para trocar "pagina" da consulta se houver mais resultados a serem consultados
        endif        
    endDo 
    
    jobSetRunning( cLock, .F., nHdl) 
    DelClassIntf()  
    
return

/*/{Protheus.doc} crtTblMail
    Fun��o respons�vel por criar a tabela de distribui��o de danfe por email 
    @type  Function
    @author Bruno Seiji
    @since 30/05/2019
    @version 12.1.17
/*/
function crtTblMail()

    dbSqlExec('', "CREATE TABLE IF NOT EXISTS DISTMAIL ( R_E_C_N_O_ INTEGER PRIMARY KEY AUTOINCREMENT, D_E_L_E_T_ TEXT DEFAULT ' ', R_E_C_D_E_L_ INTEGER DEFAULT 0, ENTIDADE TEXT, URL TEXT, ENABLE INTEGER, EMPRESA TEXT, FILIAL TEXT)", "SQLITE_SYS")
    dbSqlExec('', "CREATE TABLE IF NOT EXISTS DISTMAIL_FIELD_INFO (NAME TEXT, TYPE TEXT, LEN NUMERIC, DEC NUMERIC)", "SQLITE_SYS")

    Use 'DISTMAIL_FIELD_INFO' Alias 'DISTMAIL_FIELD_INFO' EXCLUSIVE NEW Via 'SQLITE_SYS'

    if ( DISTMAIL_FIELD_INFO->(BOF()) .and. DISTMAIL_FIELD_INFO->(EOF()) )
        dbSqlExec('', "INSERT INTO DISTMAIL_FIELD_INFO VALUES('ENTIDADE', 'C', 6, 0)", "SQLITE_SYS")
        dbSqlExec('', "INSERT INTO DISTMAIL_FIELD_INFO VALUES('URL', 'C', 200, 0)", "SQLITE_SYS")
        dbSqlExec('', "INSERT INTO DISTMAIL_FIELD_INFO VALUES('ENABLE', 'N', 1, 0)", "SQLITE_SYS")
        dbSqlExec('', "INSERT INTO DISTMAIL_FIELD_INFO VALUES('EMPRESA', 'C', 15, 0)", "SQLITE_SYS")
        dbSqlExec('', "INSERT INTO DISTMAIL_FIELD_INFO VALUES('FILIAL', 'C', 15, 0)", "SQLITE_SYS")
    endif    

    DISTMAIL_FIELD_INFO->(DBCloseArea())

return

/*/{Protheus.doc} fillTblMail
    Fun��o responsavel por popular a tabela de controle do job de distribui��o 
    @type  Function
    @author Bruno Seiji
    @since 30/05/2019
    @version 12.1.17
/*/
function fillTblMail()

    local aFiliais  := {}
    local aParams   := {}
    local oWSParam  := nil
    local nX        := 0
    local nEnable   := 0
    local cFiliais  := ""
    
    //abre o sigamat para ler todas empresas registradas e que n�o est�o deletadas
    PRTMSG("Realizando leitura de empresas e filials", LOG_PRINT)
    set deleted on
    openSM0()
    aFiliais := fwLoadSM0()

    PRTMSG("Inicializando cadastro/atualizacao de distribuicao de email(DISTMAIL).  Total de empresas/filiais: " + alltrim(str(len(aFiliais))), LOG_PRINT )
    
    if len(aFiliais) > 0

        while len(aFiliais) > countDISTMAIL()
            RPCSetType(3) //Nao faz consumo de licen�a
            RPCSetEnv(aFiliais[1][1], aFiliais[1][2], Nil, Nil, "FAT")
            cEmpAnt     := aFiliais[1][1]
            for nX := 1 to len(aFiliais)
                //posiciona nas filias para efetuar a troca e recuperar o parametro de cada uma        
                if( !aFiliais[nX][1] == cEmpAnt )            
                    RESET ENVIRONMENT
                    RPCSetType(3)
                    RPCSetEnv(aFiliais[nX][1], aFiliais[nX][2], Nil, Nil, "FAT") 
                    cEmpAnt := aFiliais[nX][1]
                endif
                cFilAnt     := aFiliais[nX][2]
                
                cURLSPED    := getNewPar("MV_SPEDURL","", )
                cFiliais := "[" + FWJsonSerialize(aFiliais[nX]) + "]"
                
                if(empty(cURLSPED))
                    insertDist("", 0, "", cEmpAnt, cFilAnt)
                else
                    oWSParam                := WSSPEDCfgNFe():New()
                    oWSParam:cUSERTOKEN     := "TOTVS"
                    oWSParam:_URL           := AllTrim( cURLSPED ) + "/SPEDCFGNFe.apw"
                    oWSParam:CLISTFILIAIS   := cFiliais

                    if oWSParam:GETAUTODIST() 
                        if( empty(oWSParam:oWSGETAUTODISTRESULT:OWSCFGAUTODIST))
                            insertDist("", 0, "", cEmpAnt, cFilAnt)
                        else                        
                            aParams := oWSParam:oWSGETAUTODISTRESULT:OWSCFGAUTODIST
                            nEnable := iif( aParams[1]:LENABLE, 1, 0 )
                            insertDist(aParams[1]:CENTIDADE,nEnable,cURLSPED,aParams[1]:CEMPRESA, aParams[1]:CFILIAL)
                        endif
                    endif            
                endif
            next
        end   
    endif

    fwFreeObj(aFiliais)
    fwFreeObj(aParams)
    fwFreeObj(oWSParam)
    
    PRTMSG("Finalizado cadastro/atualizacao de distribuicao de email(DISTMAIL)", LOG_PRINT)

return

/*/{Protheus.doc} insertDist
    Fun��o respons�vel por incluir ou alterar o registro no 
    banco da tabela de distribui��o de danfe por email
    @type  Function
    @author Bruno Seiji
    @since 30/05/2019
    @version 12.1.17
    @param cEntidade    - Entidade a ser inserida ou atualizada no banco
    @param cDist        - Status para distrubui��o de danfe por email
/*/
function insertDist(cEntidade, nDist, cURL, cEmp, cFil)

    local cQuery        := ""
    local cAlias
    default nDist       := 0
    default cEntidade := ""
    default cURL := ""

    if( empty(cEmp) .or. empty(cFil))
        PRTMSG( "Dados invalido para grava gravacao: Entidade: " + cEntidade + " URL: " + cURL + " Empresa: " + cEmp + " Filal: " + cFil + CRLF + "Verifique retorno do TSS", LOG_ERROR)
        return
    endif

    cEntidade   := alltrim(cEntidade)
    cURL        := alltrim(cURL)
    cEmp        := alltrim(cEmp)
    cFil        := alltrim(cFil)    
    
    cAlias        := getNextAlias()
    if DBSqlExec(cAlias, "SELECT * FROM DISTMAIL WHERE EMPRESA = '"+cEmp+"' AND FILIAL = '"+cfil+"'", 'SQLITE_SYS')
        if !(cAlias)->(EOF())            
            if !nDist = (cAlias)->ENABLE .or. !cURL == alltrim((cAlias)->URL) .or. !cEntidade == alltrim((cAlias)->ENTIDADE) 
                cQuery := "UPDATE DISTMAIL SET ENABLE="+alltrim(Str(nDist))+", URL = '" + cURL + "', ENTIDADE = '" + cEntidade + " ' WHERE EMPRESA = '"+cEmp+"' AND FILIAL = '"+cfil+"';"
                DBSqlExec('DISTMAIL', cQuery, 'SQLITE_SYS')
            endif            
        else
            cQuery := "INSERT INTO DISTMAIL(ENTIDADE, ENABLE, URL, EMPRESA, FILIAL) VALUES('"+cEntidade+"',"+alltrim(STR(nDist))+",'"+cURL+"','"+cEmp+"','"+cFil+"');"
            DBSqlExec('DISTMAIL', cQuery, 'SQLITE_SYS')
        endif    
    endif

    (cAlias)->(DBCloseArea())

return

/*/{Protheus.doc} getURLEnt
    Fun��o respons�vel retornar a URL do TSS da entidade 
    que esta sendo solicitada
    @type  Function
    @author Bruno Seiji
    @since 30/05/2019
    @version 12.1.17
    @param cEntidade    - Entidade a ser pesquisada
    @return cURL        - URL do TSS
/*/
static function getURLEnt(cEmp, cFil)

    local cURL          := ""
    local cAlias        := getNextAlias()

    if DBSqlExec(cAlias, "SELECT * FROM DISTMAIL WHERE EMPRESA = '" + cEmp  + "' AND FILIAL = '" + cFil + "'", 'SQLITE_SYS')
        if !(cAlias)->(EOF())
            cURL := alltrim((cAlias)->URL)
        endif    
    endif
    (cAlias)->(DBCloseArea())

return cURL

/*/{Protheus.doc} consNfeDnf
    Fun��o respons�vel por consultar as NFe's que seram 
    necess�rias a gera��o da danfe
    @type  Function
    @author Bruno Seiji
    @since 30/05/2019
    @version 12.1.17
    @param 
    @return 
/*/
static function consNfeDnf(cEmp, cFil, lContinua, nSequencia)

    local oWs := nil
    local cAlias := getNextAlias()
    default cEmp   := ""
    default cFil   := ""
    default lContinua     := .T.
    default nSequencia := 1

    if !DBSqlExec(cAlias, "SELECT * FROM DISTMAIL WHERE EMPRESA = '" + cEmp + "' AND FILIAL = '" + cFil + "'", 'SQLITE_SYS')    
        return {}
    endif

    if( (cAlias)->(EOF()) )
        (cAlias)->(DBCloseArea())        
        return {}
    endif
        
    oWS := WSNFESBRA():new()
    oWS:_URL := alltrim( (cAlias)->URL )+"/NFESBRA.apw"
    oWS:cUSERTOKEN := "TOTVS"
    oWS:cID_ENT := (cAlias)->ENTIDADE
    oWS:oWSSTATUSDISTMAIL:cMODELO := "55"
    oWS:oWSSTATUSDISTMAIL:nSEQUENCIA := nSequencia
    oWS:oWSSTATUSDISTMAIL:nSTATUS := 0

    if !oWS:STATUSDISTRIBUICAODEDOCUMENTOS()
        (cAlias)->(DBCloseArea())        
        return {}
    endif
        
    lContinua := oWS:oWSSTATUSDISTRIBUICAODEDOCUMENTOSRESULT:LCONTINUA    
    if(lContinua)
        nSequencia := oWS:oWSSTATUSDISTRIBUICAODEDOCUMENTOSRESULT:nSequencia
    endif   

return oWS:oWSSTATUSDISTRIBUICAODEDOCUMENTOSRESULT:OWSDOCUMENTOS:OWSSTATUSDOCMAIL

/*/{Protheus.doc} distNfeDnf
    Fun��o respons�vel por enviar ao TSS as NFe's com as danfes
    geradas no protheus
    @type  Function
    @author Bruno Seiji
    @since 30/05/2019
    @version 12.1.17
    @param cEntidade        Entidade a ser processada
    @param aNfe             Array da consulta do TSS ou Retransmiss�o do Prothues
    @return lExec
/*/

static function distNfeDnf(cEmp, cFil, cEntidade, aNfe, lTransmitir)

    local oWs           := nil
    local cBarra        := if( isSrvUnix(),"/","\")
    local cPath         := cBarra+"danfetemp"+cBarra
    local cFilePath     := ""
    local cPdf          := ""
    local nBuffer       := 0
    local nHdl          := 0
    local nX            := 0
    local cNFE_ID       := ""
    local lExec         := .F.

    default cEntidade   := ""
    default lTransmitir := .F.

    //local onde sera armazenado os pdf's gerados
    cPath := cPath+cEmp+cFil+cBarra

    if( !fwMakeDir(cPath) )
        PRTMSG( "Diretorio nao pode ser criado, verifique as permissoes da pasta do ambiente. Erro: " + cValToChar( FError() ), LOG_PRINT )        
        return .F.
    endif 

    for nX := 1 to len(aNfe)

        //tratamento para quando vier via JOB ou manual
        if ValType(aNfe[1]) == "A"
            cNFE_ID := aNfe[nX][1]
        else    
            cNFE_ID := aNfe[nX]:CDOC_ID  
        endif            
        
        cFilePath := cPath + lower(cNFE_ID + ".pdf")
        if !file(cFilePath)
            //gerar pdf
            createDanfe(cEmp, cFil,aNfe[nX])
        endif

        //sleep(5000)

        if file(cFilePath)
            nHdl := FOpen(cFilePath, 0)
            if nHdl >= 0
                nBuffer := fSeek(nHdl, 0,2)
                fSeek(nHdl,0,0)
                FRead(nHdl,@cPdf,nBuffer)  
                fClose(nHdl)
                fErase(cFilePath)
            endif

            if !empty(cPdf)
                oWS				                    := WSNFESBRA():new()
                oWS:_URL		                    := getURLEnt(cEmp, cFil)+"/NFESBRA.apw"
                oWS:cUSERTOKEN	                    := "TOTVS"
                oWS:cID_ENT		                    := cEntidade
                oWS:oWSDISTEMAIL:oWSLISTADOCUMENTOS := NFESBRA_ARRAYOFDOCUMENTOS():new()
                oWS:oWSDISTEMAIL:cMODELO            := "55"

                aadd(oWS:oWSDISTEMAIL:oWSLISTADOCUMENTOS:oWSDOCUMENTOS, NFESBRA_DOCUMENTOS():new())
                atail(oWS:oWSDISTEMAIL:oWSLISTADOCUMENTOS:oWSDOCUMENTOS):cNFE_ID         := alltrim(cNFE_ID)
                atail(oWS:oWSDISTEMAIL:oWSLISTADOCUMENTOS:oWSDOCUMENTOS):cPDF            := cPdf
                atail(oWS:oWSDISTEMAIL:oWSLISTADOCUMENTOS:oWSDOCUMENTOS):lRETRANSMITIR   := lTransmitir
                // oWS:DISTEMAIL:oWSLISTADOCUMENTOS[n]:cTIPOCOMPRESSAO := "zip"

                if oWS:DISTRIBUICAODEDOCUMENTOS()
                    lExec := .T.
                endif

                oWS := nil
                cPdf := ""
            endif    
        endif

    next
    
return lExec

/*/{Protheus.doc} createDanfe
    Fun��o respons�vel por gerar a danfe em background
    @type  Function
    @author Bruno Seiji
    @since 30/05/2019
    @version 12.1.17
    @param aNfe         Array com as informa��es da Nfe para gerar a DANFE
    @param cEntidade    Entidade a ser processada
    @return 
/*/
static function createDanfe(cEmp, cFil, aNfe)

    local oDanfe        := nil
    local oSetup        := nil    
    local cBarra        := if( isSrvUnix(),"/","\")
    local cPath         :=  cBarra + "danfetemp" + cBarra
    local lEnd          := .F.
    local lExistNFe     := .T.
    local lIsLoja       := .F.
    local cDoc          := ""
    local cSerie        := ""
    local cAlias        := getNextAlias()
    local nTimes := 0
    local lFile := .F.
    default aNfe        := {}

    if DBSqlExec(cAlias, "SELECT * FROM DISTMAIL WHERE EMPRESA = '" + cEmp  + "' AND FILIAL = '" + cFil + "'", 'SQLITE_SYS')

        cPath := cPath+cEmp+cFil+cBarra

        if !(cAlias)->(EOF())
            RPCSetType(3) //Nao faz consumo de licen�a
            if RPCSetEnv(alltrim( (cAlias)->EMPRESA ), alltrim( (cAlias)->FILIAL ), Nil, Nil, "FAT")

                if ValType(aNfe) == "A"
                    cNFE_ID := aNfe[1]
                else    
                    cNFE_ID := aNfe:CDOC_ID  
                endif  

                cDoc := subStr(cNFE_ID,4,len(cNFE_ID))
                cSerie := subStr(cNFE_ID,1,3)

                oDANFE := FWMSPrinter():New(alltrim(cNFE_ID), IMP_PDF, .F. /*lAdjustToLegacy*/,cPath/*cPathInServer*/,.T.,/*lTReport*/,/*oPrintSetup*/,/*cPrinter*/,/*lServer*/,/*lPDFAsPNG*/,/*lRaw*/,.F.,/*nQtdCopy*/)
                oDanfe:SetResolution(78)
                oDanfe:SetPortrait()
                oDanfe:SetPaperSize(DMPAPER_A4)
                oDanfe:SetMargin(60,60,60,60)
                oDanfe:lServer := .T.
                oDanfe:nDevice := IMP_PDF
                oDanfe:cPathPDF := cPath
                oDANFE:SetCopies( 1 )
                            
                //alimenta parametros da tela de configuracao da impressao da DANFE
                MV_PAR01 := cDoc
                MV_PAR02 := cDoc
                MV_PAR03 := cSerie
                MV_PAR04 := 0 //[Operacao] NF de Entrada / Saida
                MV_PAR05 := 2 //[Frente e Verso] Nao
                MV_PAR06 := 2 //[DANFE simplificado] Nao
                if ValType(aNfe) == "O"
                    MV_PAR07 := aNfe:DDATA //[Data] Inicio
                    MV_PAR08 := aNfe:DDATA //[Data] Fim
                endif
                oDanfe:lInJob := .T.
                U_DANFEProc(@oDanfe, @lEnd, (cAlias)->ENTIDADE, Nil, Nil, @lExistNFe, lIsLoja)
                if !oDanfe:Preview() 
                    PRTMSG( "Nao foi possivel gerar a DANFE para Empresa: "+ (cAlias)->EMPRESA + (cAlias)->FILIAL + " nota: " + alltrim(cNFE_ID), LOG_PRINT )
                    return
                endif

                while( !lFile.or. nTimes < 10)
                    lFile := file(cPath+alltrim(cNFE_ID)+".pdf")
                    if(!lFile)
                        nTimes++
                        sleep(500)
                    else
                        exit
                    endif    
                end
            endif    
        endif

        oSetup := nil
        oDanfe := nil

        (cAlias)->(DBCloseArea())
    endif        

return 
/*/{Protheus.doc} countDISTMAIL
    Contagem de registros da tabela DISTMAIL
    @type  Function
    @author Renato Nagib
    @since 19/03/2020
    @version 12.1.27
    @param nil
    @param nCount  Quantidadede regisrtos
    @return 
/*/
static function countDISTMAIL()
    local cAliasProc    := getNextAlias()
    local nCount
    DBSqlExec(cAliasProc, "SELECT count(*) COUNT FROM DISTMAIL", 'SQLITE_SYS')
    nCount := (cAliasProc)->COUNT
    (cAliasProc)->(DBCloseArea())
return nCount


//-------------------------------------------------------------------
/*/{Protheus.doc} PRTMSG()

Executa um Print padronizado 
  

@param 	cMensagem    mensagem a ser printada no console  
@param 	nTypeMsg     Tipo do Conout 

@return nil

@author 	Renato Nagib 
@since		28/11/2016
@version	12.1.15

/*/
//-------------------------------------------------------------------
function PRTMSG(cMensagem, nTypeMsg, cId)

	local cDelConout := replicate("-", 78 )
	default nTypeMsg	:= 1
	default cId := ''
	
	if(nTypeMsg == LOG_ERROR)
		ConOut(CRLF + cDelConout + CRLF + "[ ERROR: " + UPPER(procName(1)) + "] " + (StrZero(ProcLine(1), 4)) + " [Thread: " + alltrim(str(ThreadId())) + "] " + "[" + dtoc(date()) +" "+ time() + "] " + CRLF + "[" + cMensagem + "] " + CRLF + cDelConout)
	elseif( nTypeMsg == LOG_PRINT)
		ConOut(CRLF + cDelConout + CRLF + "[ LOG: " + UPPER(procName(1)) + "] " + (StrZero(ProcLine(1), 4)) + " [Thread: " + alltrim(str(ThreadId())) + "] " + "[" + dtoc(date()) +" "+ time() + "] "  + CRLF +  "[" + cMensagem + "] " + CRLF + cDelConout)
	elseif(nTypeMsg == LOG_WARNING .and. __lWarning )
		ConOut(CRLF + cDelConout + CRLF + "[ WARNING: " + UPPER(procName(1)) + "] " + (StrZero(ProcLine(1), 4)) + " [Thread: " + alltrim(str(ThreadId())) + "] " + "[" + dtoc(date()) +" "+ time() + "] "  + CRLF +  "[" + cMensagem + "] " + CRLF + cDelConout)
	elseif(nTypeMsg == LOG_INFO .and. __lInfo )
		ConOut(CRLF + cDelConout + CRLF + "[ INFO: " + UPPER(procName(1)) + "] " + (StrZero(ProcLine(1), 4)) + " [Thread: " + alltrim(str(ThreadId())) + "] " + "[" + dtoc(date()) +" "+ time() + "] "  + CRLF +  "[" + cMensagem + "] " + CRLF + cDelConout)
	endif
return



//-------------------------------------------------------------------
/*/{Protheus.doc} createThreads()

Cria pool de Threads para processamento de email
  
@param 	ni

@return nil

@author 	Renato Nagib 
@since		20/03/2020
@version	12.1.27

/*/
//-------------------------------------------------------------------
static function createThreads()

	Local cEnv := ""
    local cAmbiente := ""
	if( __init == nil )
		cEnv :=  GetAdv97()


        if( empty( GetPvProfString("DISTMAIL", "ExpirationTime", "",  cEnv ) ) )
			WritePProString ( "DISTMAIL", "ExpirationTime", "300",  cEnv)
		endif

        if( empty( GetPvProfString("DISTMAIL", "ExpirationDelta", "",  cEnv ) ) )
			WritePProString ( "DISTMAIL", "ExpirationDelta", "1",  cEnv)
		endif
		
        if( empty( GetPvProfString("IPC_DISTMAIL", "main", "",  cEnv ) ) )
			WritePProString ( "IPC_DISTMAIL", "main", "prepareIPCWAIT",  cEnv)
		endif

        if( empty( GetPvProfString("IPC_DISTMAIL", "environment", "",  cEnv ) ) )
            cAmbiente := GetPvProfString("DISTMAIL", "environment", "",  cEnv )
			WritePProString ( "IPC_DISTMAIL", "environment", cAmbiente,  cEnv)
		endif
		
        if( empty( GetPvProfString("IPC_DISTMAIL", "instances", "",  cEnv ) ) )
			WritePProString ( "IPC_DISTMAIL", "instances", "1, 10, 1, 1",  cEnv)
		endif

		if( empty( GetPvProfString("IPC_DISTMAIL", "ExpirationTime", "",  cEnv ) ) )
			WritePProString ( "IPC_DISTMAIL", "ExpirationTime", "120",  cEnv)
		endif

        if( empty( GetPvProfString("IPC_DISTMAIL", "ExpirationDelta", "",  cEnv ) ) )
			WritePProString ( "IPC_DISTMAIL", "ExpirationDelta", "1",  cEnv)
		endif

        cJobs := GetPvProfString('ONSTART','JOBS','', cEnv)
        
        if( getSrvProfString("ACTIVATE","") == "") //Ativa IPCGO        
            WriteSrvProfString("ACTIVATE", "ON")
        endif    
		
        if(!"IPC_DISTMAIL" $ cJobs)
			if(!empty(cJobs))
				cJobs += ","
			endif
			cJobs += "IPC_DISTMAIL"
			WritePProString ( "ONSTART", "JOBS", cJobs,  GetAdv97())
		endif
		__init := .T.
	endif
return