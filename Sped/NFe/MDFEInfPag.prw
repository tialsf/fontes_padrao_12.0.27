#include 'protheus.ch' 
#include 'totvs.ch'
 
#define ENTER           CHR(10)+CHR(13)
#define PAG_GRID_PAGTO  1
#define PAG_GRID_COMP   2
#define PAG_GRID_PRAZO  3
 
#define PAG_NOME        1
#define PAG_CPFCNPJ     2
#define PAG_VCONTRA     3
#define PAG_INDPAG      4
#define PAG_CNPJEPEF    5
#define PAG_BANCO       6
#define PAG_AGENCIA     7

#define PAG_TPCOMP      1
#define PAG_VCOMP       2
#define PAG_XCOMP       3

#define PAG_PARC        1
#define PAG_VPARC       2
#define PAG_DVENC       3
 
#define TAMTXT          60

/*/{Protheus.doc} MDFeInfPag
 
Classe para manipula��o das informa��es de pagamento do MDFe.
 
@since 26/03/2020
@version 1.0
/*/
class MDFeInfPag
 
    data cClassName
 
    // Atributos para controle de tela
    data aInfPag
 
    // Atributos para controle de tela
    data oGetDPgto
    data oGetDPComp
    data oGetDPPraz
    data aHeadPgto
    data aHeadComp
    data aHeadPrazo
    data aColsPgto
    data aColsComp
    data aColsPrazo
    data lShow
 
    method new()
 
    // M�todos para controle de dados 
    method XmlInfPag()
    method GetInfPag()
    method SetInfPag()
    method UpdControl()
 
    // M�todos para controle de tela
    method GetHeadPgto()
    method GetHeadComp()
    method GetHeadPraz()
    method GetNewLine()
    method CreateGrid()
    method Show()
    method AtuGridPgt()
    method VldLinePgt()
    method ValidaOk()
    method Refresh()
    method ActDesact()

end class
 
/*/{Protheus.doc} 
Metodo construtor
@since 26/03/2020
@version 1.0
/*/
method new() class MDFeInfPag
    self:cClassName := "MDFeInfPag"
 
    self:oGetDPgto  := nil
    self:oGetDPComp := nil
    self:oGetDPPraz := nil
    self:aHeadPgto  := self:GetHeadPgto()
    self:aHeadComp  := self:GetHeadComp()
    self:aHeadPrazo := self:GetHeadPraz()
    self:aColsPgto  := self:GetNewLine(self:aHeadPgto)
    self:aColsComp  := self:GetNewLine(self:aHeadComp)
    self:aColsPrazo := self:GetNewLine(self:aHeadPrazo)
    self:aInfPag    := {}
    self:lShow      := .F.
 
return self
 
/*/{Protheus.doc} 
Metodo get da estrutura InfPag
@since 26/03/2020
@version 1.0
/*/
method XmlInfPag() class MDFeInfPag
    local cXML      := ""
    local aInfPag   := self:getInfPag()
    local nInfPag   := 0
    local nComp     := 0
    local nPrazo    := 0
    local cInfPrazo := ""
    local lInfPrazo := .F.
    local dDtVenc   := ctod("")
    Local aPgtoEmpt := aClone(self:GetNewLine(self:aHeadPgto)[1])
    Local aCompEmpt := aClone(self:GetNewLine(self:aHeadComp)[1])
    Local aPrazEmpt := aClone(self:GetNewLine(self:aHeadPrazo)[1])

    self:UpdControl(PAG_GRID_PAGTO) //Atualiza a variavel de controle

    for nInfPag := 1 to Len(aInfPag)
        if !aInfPag[nInfPag,PAG_GRID_PAGTO,len(aInfPag[nInfPag,PAG_GRID_PAGTO])]  .And.; //N�o deletada
            !aCompare(aInfPag[nInfPag,PAG_GRID_PAGTO],aPgtoEmpt)

            cXML += '<infPag>'
                if !empty(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_NOME])
                    cXML += '<xNome>' + alltrim(SpecCharc(SubStr(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_NOME],1,TAMTXT))) + '</xNome>'
                endif
                aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_CPFCNPJ] := AllTrim(StrTran(StrTran(StrTran(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_CPFCNPJ],"."),"/"),"-"))
                if Len(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_CPFCNPJ]) == 11
                    cXML += '<CPF>' + AllTrim(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_CPFCNPJ]) + '</CPF>'
                elseif Len(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_CPFCNPJ]) == 14
                    cXML += '<CNPJ>' + AllTrim(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_CPFCNPJ]) + '</CNPJ>'
                else
                    cXML += '<idEstrangeiro>' + alltrim(SubStr(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_CPFCNPJ],1,20)) + '</idEstrangeiro>'
                endif
                for nComp := 1 to Len(aInfPag[nInfPag,PAG_GRID_COMP])
                    if !aInfPag[nInfPag,PAG_GRID_COMP,nComp,Len(aInfPag[nInfPag,PAG_GRID_COMP,nComp])] .And.;//N�o deletada
                        !aCompare(aInfPag[nInfPag,PAG_GRID_COMP,nComp],aCompEmpt)

                        cXML += '<Comp>'
                            If !Empty(aInfPag[nInfPag,PAG_GRID_COMP,nComp,PAG_TPCOMP])
                                cXML += '<tpComp>' + StrZero(Val(aInfPag[nInfPag,PAG_GRID_COMP,nComp,PAG_TPCOMP]),2) + '</tpComp>'
                            EndIf
                            cXML += '<vComp>' + ConvType(aInfPag[nInfPag,PAG_GRID_COMP,nComp,PAG_VCOMP],16,2) + '</vComp>'
                            if !empty(aInfPag[nInfPag,PAG_GRID_COMP,nComp,PAG_XCOMP])
                                cXML += '<xComp>' + alltrim(SpecCharc(SubStr(aInfPag[nInfPag,PAG_GRID_COMP,nComp,PAG_XCOMP],1,TAMTXT))) + '</xComp>'
                            endif
                        cXML += '</Comp>'
                    endif
                next
                cXML += '<vContrato>' + ConvType(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_VCONTRA],16,2)  + '</vContrato>'
                cXML += '<indPag>' + SubStr(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_INDPAG],1,1)  + '</indPag>'
                for nPrazo := 1 to Len(aInfPag[nInfPag,PAG_GRID_PRAZO])
                    cInfPrazo := ""
                    lInfPrazo := .F.
                    dDtVenc := ctod("")
                    if !aInfPag[nInfPag,PAG_GRID_PRAZO,nPrazo,Len(aInfPag[nInfPag,PAG_GRID_PRAZO,nPrazo])] .And.;//N�o deletada                    
                        !aCompare(aInfPag[nInfPag,PAG_GRID_PRAZO,nPrazo],aPrazEmpt)

                        if aInfPag[nInfPag,PAG_GRID_PRAZO,nPrazo,PAG_PARC] > 0
                            cInfPrazo += '<nParcela>' + StrZero(aInfPag[nInfPag,PAG_GRID_PRAZO,nPrazo,PAG_PARC],3)  + '</nParcela>'
                            lInfPrazo := .T.
                        endif
                        if !Empty(aInfPag[nInfPag,PAG_GRID_PRAZO,nPrazo,PAG_DVENC])
                            dDtVenc := aInfPag[nInfPag,PAG_GRID_PRAZO,nPrazo,PAG_DVENC]
                            cInfPrazo += '<dVenc>' + ConvType(dDtVenc ) + '</dVenc>'
                            lInfPrazo := .T.
                        endif
                        if lInfPrazo .or. aInfPag[nInfPag,PAG_GRID_PRAZO,nPrazo,PAG_VPARC] > 0
                            cInfPrazo += '<vParcela>' + ConvType(aInfPag[nInfPag,PAG_GRID_PRAZO,nPrazo,PAG_VPARC],16,2)  + '</vParcela>'
                        endif
                        if !empty(cInfPrazo)
                            cXML += '<infPrazo>'
                            cXML += cInfPrazo
                            cXML += '</infPrazo>'
                        endif
                    endif
                next
                cXML += '<infBanc>'
                if !empty(AllTrim(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_CNPJEPEF]))
                    cXML += '<CNPJIPEF>' + AllTrim(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_CNPJEPEF]) + '</CNPJIPEF>'
                else
                    cXML += '<codBanco>' + AllTrim(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_BANCO]) + '</codBanco>'
                    cXML += '<codAgencia>' + AllTrim(aInfPag[nInfPag,PAG_GRID_PAGTO,PAG_AGENCIA]) + '</codAgencia>'
                endif
                cXML += '</infBanc>'
            cXML += '</infPag>'
        endif
 
    next nInfPag
 
return cXML
 
/*/{Protheus.doc} 
Metodo get da estrutura InfPag
@since 26/03/2020
@version 1.0
/*/
method getInfPag() class MDFeInfPag
self:UpdControl(PAG_GRID_PAGTO) //Atualiza a variavel de controle
return aClone(self:aInfPag)
 
/*/{Protheus.doc} 
Metodo set da estrutura InfPag
@since 26/03/2020
@version 1.0
/*/
method setInfPag(aPgt) class MDFeInfPag
    local nPag       := 0
    local cPag       := ""
    local xInfo      := ""
    local cCnpjEpef  := ""
    local cCodBan    := ""
    local cCodAg     := ""
    local nComp      := 0
    local nPraz      := 0
 
    private aInfPagtos := {}
    private aComp      := {}
    private aPraz      := {} 

    self:aInfPag := {}
 
    If ValType(aPgt) <> "U" 
        If ValType(aPgt) <> "A"
            aPgt := {aPgt}
        EndIf
    Else
        aPgt := {}
    EndIf
  
    aInfPagtos := aPgt
    
    if Len(aInfPagtos) > 0
        self:aColsPgto := {}
        self:aColsComp := {}
        self:aColsPrazo := {}
    endif

    for nPag := 1 to Len(aInfPagtos)
 
        aAdd(self:aInfPag,{ self:GetNewLine( self:aHeadPgto )[1],{},{}})
 
        cPag := AllTrim(Str(nPag))
        
        xInfo := ""
        if ValAtrib("aInfPagtos["+cPag+"]:_XNOME") <> "U"
            xInfo := aInfPagtos[nPag]:_XNOME:TEXT
        endif
        self:aInfPag[nPag,PAG_GRID_PAGTO,PAG_NOME] := PadR(xInfo, TAMTXT)
        
        xInfo := Space(20)
        if ValAtrib("aInfPagtos["+cPag+"]:_CNPJ") <> "U"
            xInfo := aInfPagtos[nPag]:_CNPJ:TEXT
        elseif ValAtrib("aInfPagtos["+cPag+"]:_CPF") <> "U"
            xInfo := aInfPagtos[nPag]:_CPF:TEXT
        elseif ValAtrib("aInfPagtos["+cPag+"]:_IDESTRANGEIRO") <> "U"
            xInfo := aInfPagtos[nPag]:_IDESTRANGEIRO:TEXT
        endif
        self:aInfPag[nPag,PAG_GRID_PAGTO,PAG_CPFCNPJ] := PadR(xInfo,20)
        
        xInfo := 0
        if ValAtrib("aInfPagtos["+cPag+"]:_VCONTRATO") <> "U"
            xInfo := Val(aInfPagtos[nPag]:_VCONTRATO:TEXT)
        endif
        self:aInfPag[nPag,PAG_GRID_PAGTO,PAG_VCONTRA] := xInfo

        xInfo := ""
        if ValAtrib("aInfPagtos["+cPag+"]:_INDPAG") <> "U"
            xInfo := aInfPagtos[nPag]:_INDPAG:TEXT
        endif
        self:aInfPag[nPag,PAG_GRID_PAGTO,PAG_INDPAG] := PadR(AllTrim(xInfo), 1)

        cCnpjEpef   := ""
        cCodBan     := ""
        cCodAg      := ""
        if ValAtrib("aInfPagtos["+cPag+"]:_INFBANC:_CNPJIPEF") <> "U"
            cCnpjEpef := aInfPagtos[nPag]:_INFBANC:_CNPJIPEF:TEXT
        elseif ValAtrib("aInfPagtos["+cPag+"]:_INFBANC:_CODBANCO") <> "U" .and. ValAtrib("aInfPagtos["+cPag+"]:_INFBANC:_CODAGENCIA") <> "U"
            cCodBan := aInfPagtos[nPag]:_INFBANC:_CODBANCO:TEXT
            cCodAg := aInfPagtos[nPag]:_INFBANC:_CODAGENCIA:TEXT
        endif
        self:aInfPag[nPag,PAG_GRID_PAGTO,PAG_CNPJEPEF] := PadR(cCnpjEpef, 14)
        self:aInfPag[nPag,PAG_GRID_PAGTO,PAG_BANCO] := PadR(cCodBan, 5)
        self:aInfPag[nPag,PAG_GRID_PAGTO,PAG_AGENCIA] := PadR(cCodAg, 10)
 
        if ValAtrib("aInfPagtos["+cPag+"]:_COMP") <> "U"
            aComp := aInfPagtos[nPag]:_COMP
            if ValTVar(aInfPagtos[nPag]:_COMP) <> "A"
                aComp := {aInfPagtos[nPag]:_COMP}
            endif
 
            for nComp := 1 to Len(aComp)
                aAdd(self:aInfPag[nPag,PAG_GRID_COMP], self:GetNewLine(self:aHeadComp)[1])
                xInfo := PadR(xInfo,2)
                if ValAtrib("aComp["+AllTrim(Str(nComp))+"]:_TPCOMP") <> "U"
                    xInfo := AllTrim(Str(Val(aComp[nComp]:_TPCOMP:TEXT)))
                endif
                self:aInfPag[nPag,PAG_GRID_COMP,nComp,PAG_TPCOMP] := xInfo

                xInfo := 0
                if ValAtrib("aComp["+AllTrim(Str(nComp))+"]:_VCOMP") <> "U"
                    xInfo := Val(aComp[nComp]:_VCOMP:TEXT)
                endif
                self:aInfPag[nPag,PAG_GRID_COMP,nComp,PAG_VCOMP] := xInfo

                xInfo := ""
                if ValAtrib("aComp["+AllTrim(Str(nComp))+"]:_XCOMP") <> "U"
                    xInfo := aComp[nComp]:_XCOMP:TEXT
                endif
                self:aInfPag[nPag,PAG_GRID_COMP,nComp,PAG_XCOMP] := PadR(xInfo, TAMTXT)
            next
        else
            aAdd(self:aInfPag[nPag,PAG_GRID_COMP], self:GetNewLine(self:aHeadComp)[1])
        endif
 
        if ValAtrib("aInfPagtos["+cPag+"]:_INFPRAZO") <> "U"
 
            aPraz := aInfPagtos[nPag]:_INFPRAZO
            if ValTVar(aInfPagtos[nPag]:_INFPRAZO) <> "A"
                aPraz := {aInfPagtos[nPag]:_INFPRAZO}
            endif
            for nPraz := 1 to Len(aPraz)
                aAdd(self:aInfPag[nPag,PAG_GRID_PRAZO], self:GetNewLine(self:aHeadPrazo)[1])
                xInfo := 0
                if ValAtrib("aPraz["+AllTrim(Str(nPraz))+"]:_NPARCELA") <> "U"
                    xInfo := Val(aPraz[nPraz]:_NPARCELA:TEXT)
                endif
                self:aInfPag[nPag,PAG_GRID_PRAZO,nPraz,PAG_PARC] := xInfo
                
                xInfo := stod("")
                if ValAtrib("aPraz["+AllTrim(Str(nPraz))+"]:_DVENC") <> "U"
                    xInfo := AllTrim(StrTran(aPraz[nPraz]:_DVENC:TEXT,"-",""))
                    xInfo := sToD(xInfo)
                endif
                self:aInfPag[nPag,PAG_GRID_PRAZO,nPraz,PAG_DVENC] := xInfo

                xInfo := 0
                if ValAtrib("aPraz["+AllTrim(Str(nPraz))+"]:_VPARCELA") <> "U"
                    xInfo := Val(aPraz[nPraz]:_VPARCELA:TEXT)
                endif
                self:aInfPag[nPag,PAG_GRID_PRAZO,nPraz,PAG_VPARC] := xInfo
            next
        else
            aAdd(self:aInfPag[nPag,PAG_GRID_PRAZO], self:GetNewLine(self:aHeadPrazo)[1])
        endif

        aAdd( self:aColsPgto , aClone(self:aInfPag[nPag,PAG_GRID_PAGTO]) )
   
    next
 
    if len(self:aInfPag) > 0
        self:aColsComp := aClone(self:aInfPag[1,PAG_GRID_COMP])
        self:aColsPrazo := aClone(self:aInfPag[1,PAG_GRID_PRAZO])
        If Valtype(self:oGetDPgto) <> "U" .And. self:oGetDPgto:nAt <> 1 .And. self:lShow
            self:oGetDPgto:GoTop()
        EndIf
    endif
return 
 
/*/{Protheus.doc} GetHeadPgto
Retorna um array com as colunas a serem exibidas na GetDados de CIOT
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
@return aRet -> Array com estrutura de campos do gride de CIOT
/*/
//-----------------------------------------------------------------------
method GetHeadPgto() class MDFeInfPag
    local aRet := {}
    
    aadd(aRet,{PAG_NOME,{ "Nome Respons�vel Pagamento","MdfePgNome","@!",TAMTXT,0,"","","C","","R","","","","","",""}})
    aadd(aRet,{PAG_CPFCNPJ,{ "CPF/CNPJ/ID Estrangeiro Resp. Pgto" ,"MdfePgCgc" ,"@!",20,0,"","","C","","R","","","","","",""}})
    aadd(aRet,{PAG_VCONTRA,{ "Valor Total Contrato","MdfePgVCon","@E 9,999,999,999,999.99",15,2,"","","N","","R","","","","","",""}})
    aadd(aRet,{PAG_INDPAG,{ "Forma de Pagamento","MdfePgIndP","@!",1,0,"","","C","","R","0=Pagamento � vista;1=Pagamento � prazo","","","","",""}})
    aadd(aRet,{PAG_CNPJEPEF,{ "CNPJ IPEF","MdfePgIPEF","@R 99.999.999/9999-99",14,0,"Empty(MdfePgIPEF).Or.CGC(Strzero(Val(MdfePgIPEF),14))","","C","","R","","","","","",""}})
    aadd(aRet,{PAG_BANCO,{ "Cod. Banco","MdfePgCBan","@!",5,0,"","","C","","R","","","","","",""}})
    aadd(aRet,{PAG_AGENCIA,{ "Cod. Agencia","MdfePgABan","@!",10,0,"","","C","","R","","","","","",""}})
 
    aRet := Asort(aRet,,,{|x,y| x[1] < y[1]}) //Ordena de acordo com as DEFINES
    aEval(aRet, { |x| x := x[2] } ) //Retira o primeiro array (de ordena��o)
 
return aRet
 
//----------------------------------------------------------------------
/*/{Protheus.doc} GetHeadComp
Retorna um array com as colunas a serem exibidas na GetDados de CIOT
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
@return aRet -> Array com estrutura de campos do gride de CIOT
/*/
//-----------------------------------------------------------------------
method GetHeadComp() class MDFeInfPag
    local aRet := {}
 
    aadd(aRet,{PAG_TPCOMP,{"Tipo Componente","MdfePgtCom","@!",2,0,"","","C","","R","1=Vale Ped�gio;2=Impostos, taxas e contribui��es;3=Despesas;99=Outros;","","","","",""}})
    aadd(aRet,{PAG_VCOMP,{"Valor Componente","MdfePgVCom","@E 9,999,999,999.99",13,2,"","","N","","R","","","","","",""}})
    aadd(aRet,{PAG_XCOMP,{"Descri��o Componente","MdfePgXCom","@!",TAMTXT,0,"","","C","","R","","","","","",""}})
 
    aRet := Asort(aRet,,,{|x,y| x[1] < y[1]}) //Ordena de acordo com as DEFINES
    aEval(aRet, { |x| x := x[2] } ) //Retira o primeiro array (de ordena��o)
 
return aRet
 
//----------------------------------------------------------------------
/*/{Protheus.doc} GetHeadPraz
Retorna um array com as colunas a serem exibidas na GetDados de CIOT
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
@return aRet -> Array com estrutura de campos do gride de CIOT
/*/
//-----------------------------------------------------------------------
method GetHeadPraz() class MDFeInfPag
    local aRet := {}
 
    aadd(aRet,{PAG_PARC,{"N�mero da parcela"      ,"MdfePgnPar","@E 999",3,0,"","","N","","R","","","","","",""}})
    aadd(aRet,{PAG_VPARC,{"Valor Parcela"          ,"MdfePgvPar","@E 9,999,999,999.99",13,2,"","","N","","R","","","","","",""}})
    aadd(aRet,{PAG_DVENC,{"Vencimento da parcela"  ,"MdfePgVenc","@D",8,0,"","","D","","R","","ctod('//')","","","",""}})
 
    aRet := Asort(aRet,,,{|x,y| x[1] < y[1]}) //Ordena de acordo com as DEFINES
    aEval(aRet, { |x| x := x[2] } ) //Retira o primeiro array (de ordena��o)
 
return aRet
 
//----------------------------------------------------------------------
/*/{Protheus.doc} GetNewLine
Realiza o carregamento da 1 linha da aLinhas em branco na aCols
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
/*/
//-----------------------------------------------------------------------
method GetNewLine(aHeader) class MDFeInfPag
    local aRet       := {}
    local nI         := 0
    local nLen       := 0
    local nLenHeader := 0
 
    default aHeader := {}
 
    nLenHeader := Len(aHeader)
 
    //Cria um linha do aLinhas em branco
    aAdd(aRet, Array( nLenHeader+1 ) )
    nLen := Len(aRet)
    for nI := 1 to nLenHeader
        if aHeader[nI][8] == "N" //Numericos
            aRet[nLen,nI] := 0
        elseif aHeader[nI][8] == "D"
            aRet[nLen,nI] := ctod("  /  /    ")
        else //Caracter
            aRet[nLen,nI] := space(aHeader[nI][4])
        endif
    next nI
 
    //Atribui .F. para a coluna que determina se alinha do aLinhas esta deletada
    aRet[nLen][nLenHeader+1] := .F.
 
return aRet
 
//----------------------------------------------------------------------
/*/{Protheus.doc} CreateGrid
Realiza a cria��o da grid para manipula��o das informa��es InfPag
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
/*/
//-----------------------------------------------------------------------
method CreateGrid(oPainel, nOpc, aPosPgto, aPosComp, aPosPrazo, lGroup) class MDFeInfPag 
    local lRet       := .F.
    local ntopGD     := 0
    local nLeftGD    := 0
    local nDownGD    := 0
    local nRightGD   := 0
 
    default nOpc        := 3
    default aPosPgto    := {1.3, 0.5, 9.3, 46}
    default aPosComp    := {10.5, 0.6, 21.5, 22.7}
    default aPosPrazo   := {10.5, 23.7, 21.5, 46}
    default lGroup      := .T.

    begin sequence
 
    if valtype(oPainel) <> "O"
        break
    endif

    if lGroup
		oBoxPgto:= TGROUP():Create(oPainel)
		oBoxPgto:cName			    := "oBoxPgto"
		oBoxPgto:cCaption		    := "Informa��es de Pagamentos"
		oBoxPgto:nLeft			    := MDFeResol(0.5,.T.)
		oBoxPgto:nTop			    := MDFeResol(0.3,.F.)
		oBoxPgto:nWidth			    := MDFeResol(91.7,.T.)
		oBoxPgto:nHeight		    := MDFeResol(43.2,.F.)
		oBoxPgto:lShowHint		    := .F.
		oBoxPgto:lReadOnly		    := .F.
		oBoxPgto:Align			    := 0
		oBoxPgto:lVisibleControl    := .T.
	
	    oBoxComp:= TGROUP():Create(oPainel)
		oBoxComp:cName              := "oBoxComp"
		oBoxComp:cCaption           := "Componentes do Pagamento"
		oBoxComp:nLeft              := MDFeResol(0.5,.T.)
		oBoxComp:nTop               := MDFeResol(18.5,.F.)
		oBoxComp:nWidth             := MDFeResol(45.5,.T.)
		oBoxComp:nHeight            := MDFeResol(28.5,.F.)
		oBoxComp:lShowHint          := .F.
		oBoxComp:lReadOnly          := .F.
		oBoxComp:Align              := 0
		oBoxComp:lVisibleControl    := .T.

		oBoxPrazo:= TGROUP():Create(oPainel)
		oBoxPrazo:cName             := "oBoxPrazo"
		oBoxPrazo:cCaption          := "Parcelas do Pagamento"
		oBoxPrazo:nLeft 	        := MDFeResol(46.7,.T.)
		oBoxPrazo:nTop  	        := MDFeResol(18.5,.F.)
		oBoxPrazo:nWidth 	        := MDFeResol(45.5,.T.)
		oBoxPrazo:nHeight 	        := MDFeResol(28.5,.F.)
		oBoxPrazo:lShowHint         := .F.
		oBoxPrazo:lReadOnly         := .F.
		oBoxPrazo:Align             := 0
		oBoxPrazo:lVisibleControl   := .T.
	EndIf
    //Monta a GetDados "Inf pag"
    ntopGD      := MDFeResol(aPosPgto[1],.F.)
    nLeftGD     := MDFeResol(aPosPgto[2],.T.)
    nDownGD     := MDFeResol(aPosPgto[3],.F.)
    nRightGD    := MDFeResol(aPosPgto[4],.T.)
    self:oGetDPgto := MsNewGetDados():New(ntopGD,nLeftGD,nDownGD,nRightGD,nOpc,{|| self:VldLinePgt(PAG_GRID_PAGTO)},,,,,,,,,oPainel,self:aHeadPgto,self:aColsPgto, {|| self:AtuGridPgt() })

    //Monta a GetDados "Comp"
    ntopGD      := MDFeResol(aPosComp[1],.F.)
    nLeftGD     := MDFeResol(aPosComp[2],.T.)
    nDownGD     := MDFeResol(aPosComp[3],.F.)
    nRightGD    := MDFeResol(aPosComp[4],.T.)
    self:oGetDPComp := MsNewGetDados():New(ntopGD,nLeftGD,nDownGD,nRightGD,nOpc,{|| self:VldLinePgt(PAG_GRID_COMP)},,,,,,,,,oPainel,self:aHeadComp,self:aColsComp)

    //Monta a GetDados "Informa��es de Prazo"
    ntopGD      := MDFeResol(aPosPrazo[1],.F.)
    nLeftGD     := MDFeResol(aPosPrazo[2],.T.)
    nDownGD     := MDFeResol(aPosPrazo[3],.F.)
    nRightGD    := MDFeResol(aPosPrazo[4],.T.)
    self:oGetDPPraz := MsNewGetDados():New(ntopGD,nLeftGD,nDownGD,nRightGD,nOpc,{|| self:VldLinePgt(PAG_GRID_PRAZO)},,,,,,,,,oPainel,self:aHeadPrazo,self:aColsPrazo)

    self:refresh()

    lRet := .T.
 
    end sequence
 
return lRet
 
//----------------------------------------------------------------------
/*/{Protheus.doc} Show
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
/*/
//-----------------------------------------------------------------------
method Show() class MDFeInfPag 
    self:oGetDPComp:show()
    self:oGetDPPraz:show()
    self:oGetDPgto:show()
    self:lShow := .T.
return

//----------------------------------------------------------------------
/*/{Protheus.doc} ValidaOk
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
/*/
//-----------------------------------------------------------------------
method ValidaOk(lVldVazio) class MDFeInfPag 
    Local lRet          := .T.
    Local nI            := 0
    Default lVldVazio   := .F.

    //Colocar aqui as valida��es de todos os grides (bot�o OK)
    self:UpdControl(PAG_GRID_PAGTO) //Atualiza a variavel de controle

    For nI := 1 To Len(self:getInfPag())
        lRet := Self:VldLinePgt(PAG_GRID_PAGTO, nI, .T., lVldVazio)
        If !lRet
            Exit
        EndIf
    Next nI

return lRet

//----------------------------------------------------------------------
/*/{Protheus.doc} Refresh
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
/*/
//-----------------------------------------------------------------------
method Refresh() class MDFeInfPag 
    local lRet := .T.
    self:AtuGridPgt(1)
    self:oGetDPPraz:Refresh(.T.)
    self:oGetDPComp:Refresh(.T.)
    self:oGetDPgto:Refresh(.T.)
return lRet

//----------------------------------------------------------------------
/*/{Protheus.doc} ActDesact
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
/*/
//-----------------------------------------------------------------------
method ActDesact(lActivate) class MDFeInfPag 
    local lRet := .T.

    If lActivate
        self:oGetDPPraz:ENABLE()
        self:oGetDPComp:ENABLE()
        self:oGetDPgto:ENABLE()
    Else
        self:oGetDPPraz:DISABLE()
        self:oGetDPComp:DISABLE()
        self:oGetDPgto:DISABLE()
    EndIf
return lRet

//----------------------------------------------------------------------
/*/{Protheus.doc} AtuGridPgt
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
@return aRet -> Array com estrutura de campos do gride
/*/
//-----------------------------------------------------------------------
method AtuGridPgt(nPosPgt) class MDFeInfPag
    local lRet      := .T.
 
    default nPosPgt := self:oGetDPgto:nAt
 
    if Len(self:aInfPag) < nPosPgt
        aAdd(self:aInfPag,{aClone(self:oGetDPgto:aCols[self:oGetDPgto:nAt]),{},{}})
    endif
 
    if Len(self:aInfPag) >= nPosPgt
        self:oGetDPComp:aCols := aClone(iif(Len(self:aInfPag[nPosPgt][PAG_GRID_COMP]) > 0, self:aInfPag[nPosPgt][PAG_GRID_COMP], self:GetNewLine( self:aHeadComp)))
        self:aColsComp := self:oGetDPComp:aCols
 
        self:oGetDPPraz:aCols := aClone(iif(Len(self:aInfPag[nPosPgt][PAG_GRID_PRAZO]) > 0,self:aInfPag[nPosPgt][PAG_GRID_PRAZO], self:GetNewLine(self:aHeadPrazo)))
        self:aColsPrazo := self:oGetDPPraz:aCols

        If self:lShow
            self:oGetDPComp:Refresh(.F.)
            self:oGetDPPraz:Refresh(.F.)
        EndIf
    endif
    self:oGetDPgto:aCols := self:aColsPgto

return lRet
 
//----------------------------------------------------------------------
/*/{Protheus.doc} VldLinePgt
 
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
@return lRet -> .T. / .F.
/*/
//-----------------------------------------------------------------------
method VldLinePgt(nOrig, nLinha, lAtuControl, lVldVazio) class MDFeInfPag
    Local lRet      := .T.
    Local nI        := 0
    Local nQtdLinha := 0
    Local nQtdValido:= 0
    Local cErro     := ""
    Local cErroPg   := ""
    Local cErroCo   := ""
    Local cErroPr   := ""
    Local aVldPag   := {}
    Local aPgtoEmpt := aClone(self:GetNewLine(self:aHeadPgto)[1])
    Local aCompEmpt := aClone(self:GetNewLine(self:aHeadComp)[1])
    Local aPrazEmpt := aClone(self:GetNewLine(self:aHeadPrazo)[1])

    Default nLinha      := self:oGetDPgto:nAt
    Default lAtuControl := .F.
    Default lVldVazio   := .F.

    //Colocar aqui as valida��es das linhas
    If !lAtuControl
        self:UpdControl(nOrig)
    EndIf

    If Self:lShow
        aVldPag := aClone(self:getInfPag())
        If nOrig == PAG_GRID_PAGTO
            If !aVldPag[nLinha,PAG_GRID_PAGTO,Len(aVldPag[nLinha,PAG_GRID_PAGTO])] .And. ;
                !aCompare(aVldPag[nLinha,PAG_GRID_PAGTO],aPgtoEmpt)
                
                If !Empty(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_NOME]) .And. Len(AllTrim(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_NOME])) <= 2
                    cErroPg += "- Nome Respons�vel Pagamento deve ter no m�nimo 3 caracteres." + ENTER
                    lRet := .F.
                Endif

                If Empty(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_CPFCNPJ])
                    cErroPg += "- CPF/CNPJ/ID Estrangeiro Resp. Pgto." + ENTER
                    lRet := .F.
                ElseIf Len(AllTrim(StrTran(StrTran(StrTran(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_CPFCNPJ],"."),"/"),"-"))) <= 4
                    cErroPg += "- CPF/CNPJ/ID Estrangeiro Resp. Pgto dever ter mais que 5 caracteres" + ENTER
                    lRet := .F.
                EndIf

                If Empty(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_VCONTRA])
                    cErroPg += "- Valor Total Contrato." + ENTER
                    lRet := .F.
                EndIf

                If Empty(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_INDPAG])
                    cErroPg += "- Forma de Pagamento." + ENTER
                    lRet := .F.
                EndIf

                If Empty(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_CNPJEPEF]) .And. (Empty(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_BANCO]) .Or.;
                    Empty(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_AGENCIA]))
                    cErroPg += "- CNPJ IPEF ou Cod. Banco e Cod. Agencia." + ENTER
                    lRet := .F.
                ElseIf !Empty(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_BANCO]) .And. Len(AllTrim(aVldPag[nLinha,PAG_GRID_PAGTO,PAG_BANCO])) < 3
                    cErroPg += "- Cod. Banco deve ter de 3 � 5 caracteres." + ENTER
                    lRet := .F.
                EndIf

                If !Empty(cErroPg)
                    cErroPg := "Por favor preencher o(s) seguinte(s) campo(s) das Informa��es de Pagamentos:" + ENTER +  cErroPg    
                EndIf
            
                //Validando o tipo do componente
                nQtdLinha := 0
                For nI := 1 To Len(aVldPag[nLinha,PAG_GRID_COMP])
                    If !aVldPag[nLinha,PAG_GRID_COMP,nI,Len(aVldPag[nLinha,PAG_GRID_COMP,nI])] .And. ;
                        (lVldVazio .Or. !aCompare(aVldPag[nLinha,PAG_GRID_COMP,nI],aCompEmpt))
                        nQtdLinha++
                        if Empty(aVldPag[nLinha,PAG_GRID_COMP,nI,PAG_TPCOMP])
                            cErroCo += " - Tipo Componente." + ENTER
                            lRet := .F.
                        EndIf
                        if Empty(aVldPag[nLinha,PAG_GRID_COMP,nI,PAG_VCOMP])
                            cErroCo += " - Valor Componente." + ENTER
                            lRet := .F.
                        EndIf
                        If aVldPag[nLinha,PAG_GRID_COMP,nI,PAG_TPCOMP] == '99' .And. Empty(aVldPag[nLinha,PAG_GRID_COMP,nI,PAG_XCOMP])
                            cErroCo += " - Descri��o Componente." + ENTER
                            lRet := .F.
                        EndIf
                    EndIf
                    If !lRet
                    Exit
                    EndIf
                Next nI
                If nQtdLinha == 0
                    cErroCo += " - Informar ao menos uma linha de componente do pagamento." + ENTER
                    lRet := .F.
                EndIf
                If !Empty(cErroCo)
                    cErroCo := "Por favor preencher o(s) seguinte(s) campo(s) do Componente do Pagamento - Linha " + AllTrim(Str(nQtdLinha))  + ":" + ENTER + cErroCo
                EndIf

                //Validando o tipo do componente
                nQtdLinha := 0
                nQtdValido := 0
                For nI := 1 To Len(aVldPag[nLinha,PAG_GRID_PRAZO])
                    If !aVldPag[nLinha,PAG_GRID_PRAZO,nI,Len(aVldPag[nLinha,PAG_GRID_PRAZO,nI])] .And.;
                        (lVldVazio .Or. !aCompare(aVldPag[nLinha,PAG_GRID_PRAZO,nI],aPrazEmpt))
                        nQtdLinha++
                        iif(!aCompare(aVldPag[nLinha,PAG_GRID_PRAZO,nI],aPrazEmpt),nQtdValido++,Nil)
                        if Empty(aVldPag[nLinha,PAG_GRID_PRAZO,nI,PAG_VCOMP]) .And. aVldPag[nLinha,PAG_GRID_PAGTO,PAG_INDPAG] == '1'
                            cErroPr += " - Valor parcela."
                            lRet := .F.
                        EndIf
                    EndIf
                    If !lRet
                        Exit
                    EndIf
                Next nI
                If nQtdLinha == 0 .And. aVldPag[nLinha,PAG_GRID_PAGTO,PAG_INDPAG] == '1' //a prazo
                    cErroPr += " - Informar ao menos uma linha da parcela do pagamento quando forma de pagamento � Prazo." + ENTER
                    lRet := .F.
                ElseIf nQtdValido > 0 .And. aVldPag[nLinha,PAG_GRID_PAGTO,PAG_INDPAG] == '0' //a vista
                    cErroPr += " - N�o deve ser informado nenhuma linha da parcela do pagamento quando forma de pagamento � Vista." + ENTER
                    lRet := .F.
                ElseIf !Empty(cErroPr)
                    cErroPr := "Por favor preencher o(s) seguinte(s) campo(s) da Parcela do Pagamento - Linha " + AllTrim(Str(nQtdLinha))  + ":" + ENTER + cErroPr
                EndIf
            EndIf
        EndIf

        aSize(aVldPag,0)
        aVldPag := Nil
        aSize(aPgtoEmpt,0)
        aPgtoEmpt := Nil
        aSize(aCompEmpt,0)
        aCompEmpt := Nil
        aSize(aPrazEmpt,0)
        aPrazEmpt := Nil

        If !lRet
            cErro += iif(!Empty(cErroPg),cErroPg + ENTER+ENTER,"")
            cErro += iif(!Empty(cErroCo),cErroCo + ENTER+ENTER,"")
            cErro += iif(!Empty(cErroPr),cErroPr + ENTER+ENTER,"")
            MsgInfo(cErro,"Aten��o")
        EndIf
    EndIf

return lRet

//----------------------------------------------------------------------
/*/{Protheus.doc} UpdControl
 Atualiza variavel de controle dos grides
 
@author Felipe Sales Martinez
@since 23.03.2020
@version P12
@return Nil
/*/
//-----------------------------------------------------------------------
method UpdControl(nOrig) class MDFeInfPag
    local nPosPgt := self:oGetDPgto:nAt

    if len(self:aInfPag) == 0
        aAdd(self:aInfPag,{aClone(self:oGetDPgto:aCols[self:oGetDPgto:nAt]),{},{}})
    endif

    if nOrig == PAG_GRID_PAGTO
        aSize(self:aInfPag[nPosPgt,PAG_GRID_PAGTO],0)
        self:aInfPag[nPosPgt,PAG_GRID_PAGTO] := aClone(self:oGetDPgto:aCols[nPosPgt])
    endif
 
    aSize(self:aInfPag[nPosPgt,PAG_GRID_COMP],0)
    self:aInfPag[nPosPgt,PAG_GRID_COMP] := aClone(self:oGetDPComp:aCols)
 
    aSize(self:aInfPag[nPosPgt,PAG_GRID_PRAZO],0)
    self:aInfPag[nPosPgt,PAG_GRID_PRAZO] := aClone(self:oGetDPPraz:aCols)
 
return Nil

//----------------------------------------------------------------------
/*/{Protheus.doc} MDFeResol
Montagem da Dialog do Gerenciador do MDFe
 
@author Natalia Sartori
@since 10/02/2014
@version P11
 
@param  nPerc  - Valor em percentual de video desejado
@return lWidht - Flag para controlar se a medida e vertical ou horz
/*/
//-----------------------------------------------------------------------
static function MDFeResol(nPerc,lWidth)
    local nRet       := 0
    local nResHor    := GetScreenRes()[1] //Tamanho resolucao de video horizontal
    local nResVer    := GetScreenRes()[2] //Tamanho resolucao de video vertical
    default lWidth := .F.

    if lWidth
        nRet := nPerc * nResHor / 100
    else
        nRet := nPerc * nResVer / 100
    endif
return nRet
 
//----------------------------------------------------------------------
/*/{Protheus.doc} ConvType
Tratamento da informa��o para o XML
 
 
/*/
//-----------------------------------------------------------------------
static function ConvType(xValor,nTam,nDec)
    local cNovo := ""
 
    default nDec    := 0
 
    do case
        case ValType(xValor)=="N"
            if xValor <> 0
                cNovo := AllTrim(Str(xValor,nTam,nDec)) 
            else
                cNovo := "0"
            endif
        case ValType(xValor)=="D"
            cNovo := FsDateConv(xValor,"YYYYMMDD")
            cNovo := SubStr(cNovo,1,4)+"-"+SubStr(cNovo,5,2)+"-"+SubStr(cNovo,7)
        case ValType(xValor)=="C"
            if nTam==Nil
                xValor := AllTrim(xValor)
            endif
            default nTam := TAMTXT
            cNovo := AllTrim(EnCodeUtf8(NoAcento(SubStr(xValor,1,nTam))))
    endcase
 
return cNovo
 
//----------------------------------------------------------------------
/*/{Protheus.doc} MDFeInfPag
Fun��o para retorna as informa��es do pagamento para o rdmake
 
@since 26/03/2020
@version P12
/*/
//-----------------------------------------------------------------------
function MDFeInfPag()
    local aRet := {"",{}}
 
    if ValAtrib('oDlgPgt') == "O"
        aRet[1] := oDlgPgt:XmlInfPag()
        aRet[2] := oDlgPgt:getInfPag()
    endif
 
return aRet

//----------------------------------------------------------------------
/*/{Protheus.doc} aCompare
Compara os conteudos dos arrays informados
@autor Felipe Sales Martinez
@since 27/03/2020
@version P12
/*/
//-----------------------------------------------------------------------
Static Function aCompare(xVal1,xVal2)
Local cT1 := valtype(xVal1)
Local cT2 := valtype(xVal2)
Local nI 

If cT1 != cT2
	// Os tipos s�o diferentes ? A compara��o retorna .F.
	Return .F.
Endif

If cT1 != 'A'
	// Nao estamos comparando arrays. Utiliza o operador de igualdade entre as vari�veis 
	Return xVal1 == xVal2
Endif

If len(xVal1) != len(xVal2)
	// Estamos comparando arrays. O tamanho deles � diferente ? A compara��o retorna .F. 
	Return .F.
Endif

// Compara cada elemento do array recursivamente 
For nI := 1 to len(xVal1)
	If !ACompare( xVal1[nI] , xVal2[nI] )
		// No primeiro elemento que a comppara��o n�o for verdadeira, j� retorna .F. 
		Return .F. 
	Endif
Next

// Chegou at� o final, os elementos dos dois arrays tem o mesmo conte�do 
Return .T.

/*/{Protheus.doc} ValAtrib
Fun��o utilizada para substituir o type onde n�o seja possiv�l a sua 
retirada para n�o haver ocorrencia indevida pelo SonarQube.

@author 	Felipe Sales Martinez
@since 		30/03/2020
@version 	12
@return 	Nil
/*/
//-----------------------------------------------------------------------
static Function ValAtrib(atributo)
Return (type(atributo) )

/*/{Protheus.doc} ValTVar
Fun��o utilizada para substituir o type onde n�o seja possiv�l a sua 
retirada para n�o haver ocorrencia indevida pelo SonarQube.

@author 	Felipe Sales Martinez
@since 		30/03/2020
@version 	12
@return 	Nil
/*/
static Function ValTVar(xVariavel)
Return Valtype(xVariavel)

/*/{Protheus.doc} ValTVar
Fun��o utilizada para substituir o type onde n�o seja possiv�l a sua 
retirada para n�o haver ocorrencia indevida pelo SonarQube.

@author 	Felipe Sales Martinez
@since 		30/03/2020
@version 	12
@return 	Nil
/*/
static Function VldCnpj(cFiled)

If !Empty(cFiled) .And. CGC(cFiled) .And. Len(AllTrim(cFiled)) == 14
    lRet := .F.
    msginfo("O CNPJ informado n�o � valido","Aten��o")
EndIf

Return lRet

//-----------------------------------------------------------------------
/*/{Protheus.doc} SpecCharc
Fun��o que retira os caracteres especiais de um texto

@author		Felipe Sales Martinez (FSM)
@since 		07.08.2019
@version 	1.00
@param		cTexto = Texto para retirar caracteres especiais
@return		cTexto = Texto sem caracteres especiais
/*/
//-----------------------------------------------------------------------
Static Function SpecCharc(cTexto)

Local nI		:= 0
Local aCarac 	:= {}

Aadd(aCarac,{"�","A"})
Aadd(aCarac,{"�","A"})
Aadd(aCarac,{"�","A"})
Aadd(aCarac,{"�","A"})
Aadd(aCarac,{"�","a"})
Aadd(aCarac,{"�","a"})
Aadd(aCarac,{"�","a"})
Aadd(aCarac,{"�","a"})
Aadd(aCarac,{"�","E"})
Aadd(aCarac,{"�","E"})
Aadd(aCarac,{"�","e"})
Aadd(aCarac,{"�","e"})
Aadd(aCarac,{"�","I"})
Aadd(aCarac,{"�","i"})
Aadd(aCarac,{"�","O"})
Aadd(aCarac,{"�","O"})
Aadd(aCarac,{"�","O"})
Aadd(aCarac,{"�","o"})
Aadd(aCarac,{"�","o"})
Aadd(aCarac,{"�","o"})
Aadd(aCarac,{"�","U"})
Aadd(aCarac,{"�","u"})
Aadd(aCarac,{"�","C"})
Aadd(aCarac,{"�","c"})
Aadd(aCarac,{"<",""})
Aadd(aCarac,{">",""})

// Ignora caracteres Extendidos da tabela ASCII
For nI := 128 To 255
	Aadd(aCarac,{Chr(nI)," "})  // Tab
Next nI

For nI := 1 To Len(aCarac)
	If aCarac[nI, 1] $ cTexto
		cTexto := StrTran(cTexto, aCarac[nI,1], aCarac[nI,2])
	EndIf
Next nI

Return cTexto