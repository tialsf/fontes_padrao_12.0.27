#Include "Protheus.ch" 

//-----------------------------------------------------------------
/*/{Protheus.doc} colAutoRead
Execucao do processo de leitura dos arquivos recebidos via TOTVS Colaboração 2.0.

@author  Rafael Iaquinto
@since   24/07/2014
@version 11.8
/*/
//-----------------------------------------------------------------
Function colAutoRead(aParam)
	
	colReadDocs()
		
return

//-------------------------------------------------------------------
/*/{Protheus.doc} SchedDef
Retorna as perguntas definidas no schedule.

@return aReturn			Array com os parametros

@author  Rafael Iaquinto
@since   24/07/2014
@version 12
/*/
//-------------------------------------------------------------------

Static Function SchedDef()

Local aParam  := {}

aParam := { "P",;			//Tipo R para relatorio P para processo
            "",;	//Pergunte do relatorio, caso nao use passar ParamDef
            ,;			//Alias
            ,;			//Array de ordens
            }				//Titulo

Return aParam
