#include 'Protheus.ch'
#include 'apwizard.ch'
#include 'fileIO.ch'
#include "fwbrowse.ch"

Main function TAFINSTALL( Parameter01 )

Private _UpdRunInUpd := .T.

MsApp():New('SIGATAF')

oApp:cInternet  := Parameter01
oApp:bMainInit  := { || Install( Parameter01 )} 
oApp:CreateEnv()

PtSetTheme("TEMAP10")

SetFunName('TAFINSTALL')

oApp:lMessageBar:= .T.
oApp:Activate() 

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} Install
Wizard de processamento

@author Fabio V Santana
@since 23/02/2017
@version 1.0 

@aParam

@return Nil
/*/
//-------------------------------------------------------------------
Static Function Install( cOpcao as character )

Local	oWizard	 		as	object
Local   oRadio 	 		as	object
Local 	oBtnLink1 	 	as	object

Local	nLinha		 	as numeric 
Local	nColuna	 		as numeric 
Local	nAltLinha	 	as numeric 
Local	nLargLin	 	as numeric 
Local 	nRadio		 	as numeric
Local 	nAt				as numeric

Local   aOpcao	  		as array
Local 	aIni			as array

Local 	lRet 		 	as logical
Local 	lPatch 			as logical
Local   lFullPTM		as logical

Local	cPortaApp	 	as	character
Local 	cPortaDBA	 	as	character
Local 	cPortaSL 	 	as	character
Local	cPortaHtml	 	as	character
Local 	cMask     	 	as	character 
Local	cServerDBA	 	as	character 
Local 	cEndSL   	 	as	character
Local	cNomeODBC	 	as	character 
Local  	cBanco	 	 	as	character 
Local  	cAmbiente	 	as	character 
Local  	cIps		 	as	character 
Local	cUser			as	character 
Local  	cPassword	 	as	character 
Local 	cFile 		 	as	character 
Local 	cFileSdf	 	as	character 
Local 	cWSPort	 		as	character 
Local 	cRootPath		as  character
Local 	cRemotePath		as  character
local 	cFilehlp		as 	character

oWizard	 		:= Nil
oRadio 	 		:= Nil
oBtnLink1 		:= Nil

nLinha			:= 0
nColuna			:= 0
nAltLinha		:= 0
nLargLin		:= 270
nRadio			:= 1
nAt				:= 0

lRet 			:= .F.
lPatch			:= .T.
lFullPTM		:= .T.

cBinPath 		:= ""
cRemotePath		:= GetSrvProfString('ROOTPATH',"")
cPortaSL 		:= Space(200) //PadR( GetProfString( 'TCP', 'PORT', '5555'), 5, " " )
cMask     		:= "Arquivo OCI" + " (*.dll) |*.dll|"
cServerDBA		:= 'localhost'
cEndSL   		:= Space(200)
cNomeODBC		:= Space(200)
cBanco	 		:= Space(200)
cAmbiente		:= Space(200)
cIps			:= Space(200)
cUser			:= Space(200)
cPassword	 	:= Space(200)
cFile 			:= Space(200)
cFileSdf		:= Space(200)
cFilehlp		:= Space(200)
cWSPort			:= Space(200)
cEnvLS			:= Space(200)
aIni	 		:= GetINISessions('bin\appserver\appserver.ini')
cBinPath 		:= cRemotePath +  'bin\appserver\appserver.ini'
cBinDBA 		:= cRemotePath +  'TafDbAccess32\dbaccess.ini'
cBinDBA64 		:= cRemotePath +  'TafDbAccess64\dbaccess.ini'

cPortaApp		:= PadR( GetPvProfString( 'TCP'	 , 'PORT', '', cBinPath , Nil, Nil ), 5, " " )
cPortaDBA		:= PadR( GetPvProfString( 'GENERAL', 'PORT', '', cBinDBA , Nil, Nil ), 5, " " )

If Empty(AllTrim(cPortaDBA))
	cPortaDBA		:= PadR( GetPvProfString( 'GENERAL', 'PORT', '', cBinDBA64 , Nil, Nil ), 5, " " )
EndIf

cPortaHtml		:= PadR( GetProfString( 'TCP', 'PORT', '8095'), 5, " " )
aOpcao		 	:= {}

Default cOpcao := "INSTALL"

lMsFinalAuto:= .F.
lMsErroAuto := .F.
lMsHelpAuto := .F.
__cInternet := Nil 

If cOpcao == "INSTALL"

	Define WIZARD oWizard;
		TITLE 'Assistente de instala��o...';
		HEADER 'T A F';
		MESSAGE "TOTVS Automa��o Fiscal - 12.1.17";			
		TEXT "ao assistente de instala��o e configura��o do <b>TOTVS Automa��o Fiscal</b>.<br>" + ;	
			 "<br>O TAF � a nova plataforma da TOTVS especializada na <b>consolida��o</b> dos movimentos fiscais num formato <b>+compacto</b> para atender as obriga��es acess�rias impostas pelo Governo.<br>" +;
			 "<br>� uma plataforma segregada com controles pr�prios e funcionalidades importantes para <b>conferir as informa��es e agilizar entregas, reduzindo complexidades, removendo e unificando a carga de manuten��o das informa��es</b>, al�m de possuir um <b>reposit�rio segregado para hist�rico das movimenta��es</b> declaradas ao Governo.";
		NEXT { || .T. };
		FINISH { || .T. };
		NOTESC

	CREATE PANEL oWizard;
			HEADER "T A F - TOTVS Automa��o Fiscal - 12.1.17";
			MESSAGE "Preencha corretamente as informa��es solicitadas...";
			BACK { || .T. };			 
			NEXT { || TafValidCon(cBanco,cNomeODBC,cServerDBA,cPortaDBA,cFile,cUser,cPassword,oWizard)};
			FINISH { || .T. }
	
			nLinha		:=	10
			nColuna	:=	10
			nAltLinha	:=	10	
			oPnl1		:=�	TPanel():New( 0, 0, , oWizard:oMPanel[ 2 ],, .F., .F.,,, nLargLin, 270, .T., .F. )
			TSay():New( nLinha, nColuna , { || '<h3><b>ODBC/TNS - IMPORTANTE!</b></h3>' }, oPnl1,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha*2,,,,,,.T. )

			nLinha		+=	15
			TSay():New( nLinha, nColuna, { || 'A configura��o da <b>ODBC</b> ou do <b>TNS</b> (Oracle), � um pr�-requisito indispens�vel para a instala��o do TAF.' }, oPnl1,,,,,, .T., /*CLR_BLUE*/,, nLargLin-5, nAltLinha,,,,,,.T. )
			nLinha	+=	nAltLinha	
			
			TSay():New( nLinha, nColuna, { || 'As informa��es que ser�o preenchidas a seguir,devem estar de acordo com o manual de instala��o do TAF dispon�vel no portal TDN no Link a seguir:' }, oPnl1,,,,,, .T., /*CLR_BLUE*/,, nLargLin-5, nAltLinha*2,,,,,,.T. )
			nLinha	+=	nAltLinha*2	
			
			@nLinha-2,nColuna SAY oBtnLink1 PROMPT "<b><u><font  color='#000092'>"+">> http://tdn.totvs.com.br/pages/viewpage.action?pageId=274305742"+"</font></u></b>" SIZE 300,010 OF oPnl1 HTML PIXEL
			oBtnLink1:bLClicked := {|| ShellExecute("open","http://tdn.totvs.com.br/pages/viewpage.action?pageId=274305742","","",1) }
			nLinha	+=	nAltLinha + 2

			TSay():New( nLinha, nColuna, { || 'Ap�s o preenchimento, faremos a valida��o da conex�o e somente daremos sequ�ncia a instala��o se a conex�o for validada corretamente!' }, oPnl1,,,,,, .T., /*CLR_BLUE*/,, nLargLin-5, nAltLinha*2,,,,,,.T. )
			nLinha	+=	nAltLinha*2	
			
			TSay():New( nLinha, nColuna, { || 'Selecione o <b>SGBD</b>' }, oPnl1,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			nLinha	+=	nAltLinha
	
			aAdd(aOpcao,"MSSQL")
			aAdd(aOpcao,"ORACLE")
			aAdd(aOpcao,"INFORMIX")
			aAdd(aOpcao,"DB2")
			aAdd(aOpcao,"POSTGRES")
			aAdd(aOpcao,"OPENEDGE") 
								
			TCombobox():New(nLinha,nColuna,{|u| if( PCount()>0, cBanco:=u, cBanco )},aOpcao,nLargLin-10,nAltLinha,oPnl1,,,,,,.T.,,,,)

			nLinha		+=	20
			TSay():New( nLinha, nColuna, { || 'Informar o <b>Nome</b> da conex�o (SERVICE_NAME):' }, oPnl1,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			nLinha	+=	nAltLinha
			TGet():New( nLinha, nColuna, {|u| if( PCount()>0, cNomeODBC:=u, cNomeODBC )}, oPnl1, nLargLin-10, nAltLinha, "@",, 0,,, .F.,, .T.,, .F.,, .F., .F.,, .F., .F.,, cNomeODBC,,,, )						
					
			nLinha		+=	20
			TSay():New( nLinha, nColuna, { || 'Informar o <b>Usuario</b>: (Manter em branco se a conex�o com o banco for feita via <b>Windows Authentication</b>)' }, oPnl1,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			nLinha	+=	nAltLinha
			TGet():New( nLinha, nColuna, {|u| if( PCount()>0, cUser:=u, cUser )}, oPnl1, nLargLin-10, nAltLinha, "@",, 0,,, .F.,, .T.,, .F.,, .F., .F.,, .F., .F.,, cUser,,,, )
		
			nLinha		+=	20
			TSay():New( nLinha, nColuna, { || 'Informar a <b>Senha</b>: (Manter em branco se a conex�o com o banco for feita via <b>Windows Authentication</b>)' }, oPnl1,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			nLinha	+=	nAltLinha
			TGet():New( nLinha, nColuna, {|u| if( PCount()>0, cPassword:=u, cPassword )}, oPnl1, nLargLin-10, nAltLinha, "@",, 0,,, .F.,, .T.,, .F.,, .F., .F.,, .F., .T.,,cPassword,,,, )		
						
			nLinha		+=	25
			TSay():New( nLinha, nColuna, { || '<h3><b>Observa��o Importante - Banco ORACLE</b></h3>' }, oPnl1,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )		

			nLinha		+=	12
			TSay():New( nLinha, nColuna, { || 'Ser� necess�rio informar o caminho da OCI (.DLL) para a configura��o da conex�o.' }, oPnl1,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
		
			nLinha		+=	nAltLinha
			TButton():New( nLinha,nColuna,"Oci Oracle:",oPnl1,{||cFile:=cGetFile(cMask,""),.T.},30,nAltLinha + 2,,,,.T.,.F.,,.T., ,, .F.)			
			TGet():New( nLinha, nColuna + 32, {|u| if( PCount()>0, cFile:=u, cFile )}, oPnl1, nLargLin - 42, nAltLinha, "@",, 0,,, .F.,, .T.,, .F.,, .F., .F.,, .F., .F.,, cFile,,,, )			

	CREATE PANEL oWizard;
			HEADER "T A F - TOTVS Automa��o Fiscal - 12.1.17";
			MESSAGE "Preencha corretamente as informa��es solicitadas...";
			BACK { || .T. };
			NEXT { || .T. };
			FINISH { || .t. }
			
			nLinha		:=	10
			nColuna		:=	10
			nAltLinha	:=	10	
			oPnl2		:=�	TPanel():New( 0, 0, , oWizard:oMPanel[ 3 ],, .F., .F.,,, nLargLin, 138, .T., .F. )

			TSay():New( nLinha, nColuna, { || '<h3><b>TOTVS AppServer</b></h3>' }, oPnl2,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			nLinha	+=	15	
			
			TSay():New( nLinha, nColuna, { || 'Informar a porta para a conex�o com o <b>Smart Client HTML</b>' }, oPnl2,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha*2,,,,,,.T. )
			nLinha	+=	nAltLinha
			TGet():New( nLinha, nColuna, {|u| if( PCount()>0, cPortaHtml:=u, cPortaHtml )}, oPnl2, nLargLin-10, nAltLinha, "@",, 0,,, .F.,, .T.,, .F.,, .F., .F.,, .F., .F.,, cPortaHtml,,,, )	
						
			nLinha	+=	20
			TSay():New( nLinha, nColuna, { || '<b>*Opcional</b>' }, oPnl2,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			nLinha	+=	nAltLinha -1
			TSay():New( nLinha, nColuna, { || 'Informar os <b>Hosts</b> dos terminais liberados para aplica��o de patchs nos ambientes do TAF. (AllowApplyPatch)' }, oPnl2,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
						
			nLinha	+=	nAltLinha
			TGet():New( nLinha, nColuna, {|u| if( PCount()>0, cIps:=u, cIps )}, oPnl2, nLargLin-10, nAltLinha, "@",, 0,,, .F.,, .T.,, .F.,, .F., .F.,, .F., .F.,, cIps,,,,,,,,,,,"Exemplo: 192.160.0.100, 192.160.10.101")					

	CREATE PANEL oWizard;
			HEADER "T A F - TOTVS Automa��o Fiscal - 12.1.17";
			MESSAGE "Preencha corretamente as informa��es solicitadas...";
			BACK { || .T. };
			NEXT { || ValidLS( AllTrim( cEndSL ), Val( cPortaSL ), AllTrim( cEnvLS ) ) };
			FINISH { || .T.}
	
			nLinha		:=	10
			nColuna	:=	10
			nAltLinha	:=	10	
			oPnl3		:=�	TPanel():New( 0, 0, , oWizard:oMPanel[ 4 ],, .F., .F.,,, nLargLin, 148, .T., .F. )
			
			TSay():New( nLinha, nColuna, { || '<h3><b>Servidor de Licen�as</b></h3>' }, oPnl3,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			nLinha	+=	15
			
			TSay():New( nLinha, nColuna, { || 'Informe os detalhes de conex�o com o <b>Servidor de Licen�as.</b>' }, oPnl3,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha*2,,,,,,.T. )
	
			nLinha		+=	15
			TSay():New( nLinha, nColuna, { || 'Informar o <b>Endere�o (IP/HOSTNAME)</b> do Servidor de Licen�as:' }, oPnl3,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			nLinha	+=	nAltLinha
			TGet():New( nLinha, nColuna, {|u| if( PCount()>0, cEndSL:=u, cEndSL )}, oPnl3, nLargLin-10, nAltLinha, "@",, 0,,, .F.,, .T.,, .F.,, .F., .F.,, .F., .F.,, cEndSL,,,,,,,,,,,/*"Localhost"*/ )
	
			nLinha		+=	20
			TSay():New( nLinha, nColuna, { || 'Informar a <b>Porta</b> do Servidor de Licen�as:' }, oPnl3,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			nLinha	+=	nAltLinha
			TGet():New( nLinha, nColuna, {|u| if( PCount()>0, cPortaSL:=u, cPortaSL )}, oPnl3, nLargLin-10, nAltLinha, "@",, 0,,, .F.,, .T.,, .F.,, .F., .F.,, .F., .F.,, cPortaSL,,,,,,,,,,,/*"5555"*/ )
			
			nLinha		+=	20
			TSay():New( nLinha, nColuna, { || '<b>*Opcional</b>'  }, oPnl3,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			nLinha	+=	nAltLinha -1
			TSay():New( nLinha, nColuna, { || 'Informar o <b>Ambiente (Environment)</b> do Servidor de Licen�as para que seja feito um teste de conex�o:' }, oPnl3,,,,,, .T., /*CLR_BLUE*/,, nLargLin, nAltLinha,,,,,,.T. )
			
			nLinha	+=	nAltLinha
			TGet():New( nLinha, nColuna, {|u| if( PCount()>0, cEnvLS:=u, cEnvLS )}, oPnl3, nLargLin-10, nAltLinha, "",, 0,,, .F.,, .T.,, .F.,, .F., .F.,, .F., .F.,, cEnvLS,,,,,,,,,,,/*"Environment"*/ )

	CREATE PANEL oWizard;
			HEADER "T A F - TOTVS Automa��o Fiscal - 12.1.17";
			MESSAGE "TOTVS Automa��o Fiscal";
			BACK { || .T. };
			NEXT { || .t. };
			FINISH { || lRet := .T. }
	
			nLinha		:=	10
			nColuna	:=	10
			nAltLinha	:=	10	
			oPnl5		:=�	TPanel():New( 0, 0, , oWizard:oMPanel[ 5 ],, .F., .F.,,, nLargLin, 135, .T., .F. )
			TSay():New( nLinha, nColuna, { || '<h3><b>Menus</b></h3>' }, oPnl5,,,,,, .T., ,, nLargLin, nAltLinha*2,,,,,,.T. )
						
			nLinha		+=	15
			TSay():New( nLinha, nColuna, { || 'Para concluir a instala��o, selecione quais os <b>Menus</b> do TAF ser�o habilitados para utiliza��o:' }, oPnl5,,,,,, .T.,,, nLargLin, nAltLinha,,,,,,.T. )
				
			nLinha		+=	10
			oRadio 	:= tRadMenu():New(nLinha,nColuna,{"Menu Completo - Todas as obriga��es abaixo.","Somente E-Social","Somente Fiscal","Somente ECF"},{|u|IIf(PCount() == 0,nRadio,nRadio := u)},oPnl5,,,,,,,,nLargLin,nAltLinha,,,,.T.)	

	CREATE PANEL oWizard;
			HEADER "T A F - TOTVS Automa��o Fiscal - 12.1.17";
			MESSAGE "TOTVS Automa��o Fiscal";
			BACK { || .T. };
			NEXT { || .t. };
			FINISH { || lRet := .T. }
	
			nLinha		:=	10
			nColuna	:=	10
			nAltLinha	:=	10	
			oPnl6		:=�	TPanel():New( 0, 0, , oWizard:oMPanel[ 6 ],, .F., .F.,,, nLargLin, 135, .T., .F. )
			TSay():New( nLinha, nColuna, { || '<h3><b>Alguns Links �teis.</b></h3>' }, oPnl6,,,,,, .T.,,, nLargLin, nAltLinha*2,,,,,,.T. )
						
			nLinha		+=	15
			TSay():New( nLinha, nColuna, { || 'P�gina principal do TAF no TDN' }, oPnl6,,,,,, .T.,,, nLargLin, nAltLinha,,,,,,.T. )
				
			nLinha		+=	10
			@nLinha,nColuna SAY oBtnLink1 PROMPT "<b><u><font  color='#000092'>"+">> http://tdn.totvs.com/display/TAF/TAF+Home"+"</font></u></b>" SIZE 300,010 OF oPnl6 HTML PIXEL
			oBtnLink1:bLClicked := {|| ShellExecute("open","http://tdn.totvs.com/display/TAF/TAF+Home","","",1) }

			nLinha		+=	15
			TSay():New( nLinha, nColuna, { || 'Perguntas frequentes do TOTVS License Server' }, oPnl6,,,,,, .T.,,, nLargLin, nAltLinha,,,,,,.T. )
						
			nLinha		+=	10
			@nLinha,nColuna SAY oBtnLink1 PROMPT "<b><u><font  color='#000092'>"+">> http://tdn.totvs.com/display/tec/Perguntas+frequentes+do+TOTVS++License+Server"+"</font></u></b>" SIZE 300,010 OF oPnl6 HTML PIXEL
			oBtnLink1:bLClicked := {|| ShellExecute("open","http://tdn.totvs.com/display/tec/Perguntas+frequentes+do+TOTVS++License+Server","","",1) }
			
			nLinha		+=	15
			TSay():New( nLinha, nColuna, { || 'Guia de instala��o DbAccess' }, oPnl6,,,,,, .T.,,, nLargLin, nAltLinha,,,,,,.T. )
			
			nLinha		+=	10
			@nLinha,nColuna SAY oBtnLink1 PROMPT "<b><u><font  color='#000092'>"+">> http://tdn.totvs.com/pages/viewpage.action?pageId=6064461"+"</font></u></b>" SIZE 300,010 OF oPnl6 HTML PIXEL
			oBtnLink1:bLClicked := {|| ShellExecute("open","http://tdn.totvs.com/pages/viewpage.action?pageId=6064461","","",1) }	
			
			nLinha		+=	15
			TSay():New( nLinha, nColuna, { || 'Aplica��o de pacote de atualiza��o' }, oPnl6,,,,,, .T., ,, nLargLin, nAltLinha,,,,,,.T. )
			
			nLinha		+=	10
			@nLinha,nColuna SAY oBtnLink1 PROMPT "<b><u><font  color='#000092'>"+">> http://tdn.totvs.com/pages/viewpage.action?pageId=24347145"+"</font></u></b>" SIZE 300,010 OF oPnl6 HTML PIXEL
			oBtnLink1:bLClicked := {|| ShellExecute("open","http://tdn.totvs.com/pages/viewpage.action?pageId=24347145","","",1) }			
										
	Activate WIZARD oWizard Centered

ElseIf cOpcao == "PATCH"	
	TAFAppPatchApply( @oWizard , @lRet , @cFile , nLargLin , @cAmbiente , @cFileSdf , @cUser , @cPassword, aIni , cBinPath, @lFullPTM, @cFilehlp)
Else
	Final("Par�metro inv�lido!!!")
EndIf

If cOpcao == "INSTALL"
	If lRet
		//------------------------------------------------------------------------------//
		//Fun��es responsaveis pela cria��o dos arquivos de configura��o e inicializa��o//
		//------------------------------------------------------------------------------//        		
		//Renomeia o menu escolhido
		TafMenu(nRadio)
			
		//Cria Appserver.ini e DbAccess.ini
		TafCriaIni( cBanco , cServerDBA , cNomeODBC , cPortaDBA , cPortaApp , cEndSL , cPortaSL , cFile , cIps , cWSPort , cUser , cPassword , cPortaHtml)

		//Apaga os arquivos utilizados somene na instala��o
		TafDelTMP()
			
		Final('Configura��es salvas. Atalho criado na �rea de trabalho.')
	Else
		Final('O assistente de instala��o foi cancelado pelo usu�rio.')
	EndIf	

ElseIf cOpcao == "PATCH" 	

	If lRet
		
		//Fun��o responsavel pela aplica��o de patchs
		PatchApplier( cAmbiente , cFile, cUser, cPassword, lFullPTM )
	
		//Fun��o responsavel pelo update do dicionario de dados
		UpdApplier(cFileSdf, cBinPath, cRemotePath, cAmbiente, cFileHlp)
		
		Final('Fim do Assistente, Aguarde a inicializa��o do UPDTAF')
	Else
		Final('O assistente de atualiza��es foi encerrado pelo usu�rio.')
	EndIf
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} TafCriaIni
Cria o arquivo AppServer.ini

@author Fabio V Santana
@since 21/02/2017
@version 1.0 

@aParam

@return Nil
/*/
//-------------------------------------------------------------------

Static Function TafCriaIni( cBanco as character, cServerDBA as character , cNomeODBC as character , cPortaDBA as character ,;
							   	cPortaApp as character , cEndSL as character , cPortaSL as character , cFile as character ,;
							   	cIps as character , cWSPort as character , cUser as character , cPassword as character , cPortaHtml as character)

Local cString 		as character
Local cRootPath 	as character
Local cStartPath 	as character

Local nHdServer 	as numeric

cString 	:= ""
cRootPath 	:= AllTrim(GetSrvProfString("RootPath",""))
cStartPath	:= AllTrim(GetSrvProfString("StartPath",""))

nHdServer 	:= FCreate( cStartPath + "AppServer.Ini" )

//--------------------------------------------------------------------------------------------------------------------//
//                                                 Cria AppServer.ini                                                 //
//--------------------------------------------------------------------------------------------------------------------//
cString += "[TAF]" 									+ CRLF
cString += "SourcePath=" + cRootPath + "apo" 		+ CRLF
cString += "RootPath="   + cRootPath 				+ CRLF
cString += "StartPath="  + cStartPath 				+ CRLF
cString += "RpoDb=top" 								+ CRLF
cString += "RpoLanguage=portuguese" 				+ CRLF
cString += "RpoVersion=120" 						+ CRLF
cString += "LocalFiles=ctree" 						+ CRLF
cString += "localdbextension=.dtc" 					+ CRLF
cString += "Logprofiler=0" 							+ CRLF
cString += "TOPMEMOMEGA=1" 							+ CRLF
cString += "x2_path=\data\" 						+ CRLF

cString += CRLF
cString += "[Drivers]" 								+ CRLF
cString += "Active=TCP" 							+ CRLF

cString += CRLF
cString += "[TopConnect]" 							+ CRLF
cString += "Database="+ AllTrim(cBanco)				+ CRLF
cString += "Server=" + AllTrim(cServerDBA)			+ CRLF
cString += "Alias=" + AllTrim(cNomeODBC)			+ CRLF
cString += "Port=" + AllTrim(cPortaDBA)				+ CRLF

cString += CRLF
cString += "[TCP]" 									+ CRLF
cString += "TYPE=TCPIP" 							+ CRLF
cString += "Port="+ AllTrim(cPortaApp)				+ CRLF

cString += CRLF
cString += "[General]" 								+ CRLF
cString += "InstallPath=C:\" 						+ CRLF
cString += "BUILDKILLUSERS=1" 						+ CRLF
cString += "consolelog=1" 							+ CRLF
cString += "Segmento=YddTQHWW=VZF=yhu" 				+ CRLF
cString += "Serie===AV" 							+ CRLF
cString += "MaxStringSize=10"						+ CRLF

cString += CRLF
cString += "[LICENSECLIENT]" 						+ CRLF
cString += "SERVER="+ AllTrim(cEndSL)				+ CRLF
cString += "PORT="+ AllTrim(cPortaSL)				+ CRLF

cString += CRLF
cString += "[SERVICE]" 								+ CRLF
cString += "NAME=Taf-AppServer" 					+ CRLF
cString += "DISPLAYNAME=Taf-AppServer" 				+ CRLF

cString += CRLF
cString += "[WEBAPP]" 								+ CRLF
cString += "PORT=" + AllTrim(cPortaHtml)			+ CRLF

If !Empty(AllTrim(cIps))
	cString += CRLF
	cString += "[TDS]" 									+ CRLF
	cString += "ALLOWAPPLYPATCH=" + AllTrim(cIps)		+ CRLF
	cString += "ALLOWEDIT=" 		  + AllTrim(cIps)	+ CRLF
EndIf

If nHdServer <> -1
    FWrite(nHdServer, cString)
    FClose(nHdServer)
    
    if !( CpyT2S( cRootPath + cStartPath + "AppServer.Ini", "\bin\AppServer\" ) )
    	MsgInfo("N�o foi poss�vel criar o arquivo <b>AppServer.ini</b>. Verifique suas permiss�es de grava��o em disco. C�digo do erro: " + str( fError() ), "Falha!")
    endif
    
    //Apago o arquivo temporario
	FErase( cStartPath + "AppServer.Ini")
Else
	MsgInfo("N�o foi poss�vel criar o arquivo <b>AppServer.ini</b>. Verifique suas permiss�es de grava��o em disco.", "Falha!")
Endif

// Conecta no server responsavel pelo webservice para adicionar a sess�o do topconncet
oRpcSrv := TRpc():New( "TAF_WS" )

If (oRpcSrv:Connect( GetPvProfString( 'WS', 'Server', '', GetRemoteIniName() , Nil, Nil ), Val(GetPvProfString( 'WS' , 'PORT', '', GetRemoteIniName() , Nil, Nil )) ))
	//Sess�o do dbaccess no ambiente do WS
	oRpcSrv:CallProc("writePProString", 'TopConnect' , 'Database' 	, AllTrim(cBanco) 	, cRootPath + 'bin\app\appserver.ini')
	oRpcSrv:CallProc("writePProString", 'TopConnect' , 'Server' 	, AllTrim(cServerDBA), cRootPath + 'bin\app\appserver.ini')
	oRpcSrv:CallProc("writePProString", 'TopConnect' , 'Alias' 		, AllTrim(cNomeODBC) , cRootPath + 'bin\app\appserver.ini')
	oRpcSrv:CallProc("writePProString", 'TopConnect' , 'Port' 		, AllTrim(cPortaDBA) , cRootPath + 'bin\app\appserver.ini')
EndIf

// Conecta no server responsavel pelo webservice para adicionar a sess�o do topconncet
oRpcSrv := TRpc():New( "PATCH" )

If (oRpcSrv:Connect( GetPvProfString( 'PATCH', 'Server', '', GetRemoteIniName() , Nil, Nil ), Val(GetPvProfString( 'PATCH', 'PORT', '', GetRemoteIniName() , Nil, Nil )) ))
	//Sess�o do dbaccess no ambiente do PATCH
	oRpcSrv:CallProc("writePProString", 'TopConnect' , 'Database' 	, AllTrim(cBanco) 	 , cRootPath + 'bin\serverpatch\appserver.ini')
	oRpcSrv:CallProc("writePProString", 'TopConnect' , 'Server' 	, AllTrim(cServerDBA), cRootPath + 'bin\serverpatch\appserver.ini')
	oRpcSrv:CallProc("writePProString", 'TopConnect' , 'Alias' 		, AllTrim(cNomeODBC) , cRootPath + 'bin\serverpatch\appserver.ini')
	oRpcSrv:CallProc("writePProString", 'TopConnect' , 'Port' 		, AllTrim(cPortaDBA) , cRootPath + 'bin\serverpatch\appserver.ini')
EndIf

//-----------------------------------------------------------------------------------------------------------------//
//                                                  Cria o DbAccess.ini                                            //
//-----------------------------------------------------------------------------------------------------------------//
if !empty( cPortaDBA ) .and. !empty( cServerDBA )	
	FWMsgRun(,{||buildIniDbAccess( cPortaDBA , cEndSL , cPortaSL , cUser , cPassword , cBanco , cNomeODBC , cFile )},"Configura��es de Ambiente","Ajustando arquivos de configura��o do ambiente...")
endif

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} TafValidCon
Valida a conex�o ODBC

@author Fabio V Santana
@since 13/03/2017
@version 1.0 

@aParam 

@return Nil
/*/
//-------------------------------------------------------------------
Static Function TafValidCon(cBanco as character , cNomeODBC as character , cServerDBA as character , cPortaDBA as character ,;
								cFile as character , cUser as character , cPassword as character , oWizard as object)

Local lRet 		as logical

Local cString 	as character
Local cConn 	 	as character
Local cRootPath  	as character
Local cStartPath 	as character

Local nHwnd 		as numeric

If Upper(AllTrim(cBanco)) <> "OPENEDGE"

	lRet 		:= .F.
	cString 	:= ""
	cConn 		:= AllTrim(cBanco) + "/" + AllTrim(cNomeODBC)
	cRootPath 	:= AllTrim(GetSrvProfString("RootPath",""))
	cStartPath	:= AllTrim(GetSrvProfString("StartPath",""))
	nHwnd 		:= 0
	
	//-----------------------------------------------------------------------------------------------------------------//
	//                                                  Cria o DbAccess.ini                                            //
	//-----------------------------------------------------------------------------------------------------------------//
	FWMsgRun(,{||buildIniDbAccess( cPortaDBA , , , cUser , cPassword , cBanco , cNomeODBC , cFile )},"TOTVS|DbAccess","Criando arquivo de configura��o do TOTVS|DbAccess...")
	
	lRet := runTcLink( cConn , cServerDBA , cPortaDBA, cRootPath )
Else
	lRet := .T.
	MsgInfo("Quando a op��o selecionada for <b>OpenEdge / Progress</b>," + CRLF + " n�o ser� efetuado o teste de conex�o." + CRLF + CRLF + "Verifique a conex�o com o Banco de Dados no final da instala��o.", "OPENEDGE")
EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} TafMenu
Renomeia o menu escolhido na wizard de configura��o

@author Fabio V Santana
@since 27/03/2017
@version 1.0 

@aParam 

@return Nil
/*/
//-------------------------------------------------------------------
Static Function TafMenu(nRadio as numeric)

Local cRootPath as character 
Local cPathMenu as character

Default nRadio 	:= 1

cRootPath := AllTrim(GetSrvProfString("RootPath","")) 
cPathMenu := cRootPath + "Install\Menus\"

//--------------------------------------------------------------------------------------------------------------------//
//                                                 Renomeia o arquivo.XNU                                             //
//--------------------------------------------------------------------------------------------------------------------//
If nRadio == 1
	FRename(cPathMenu + "sigataf_completo.xnu",cPathMenu + "sigataf.xnu")
ElseIf nRadio == 2
	FRename(cPathMenu + "sigataf_social.xnu",cPathMenu + "sigataf.xnu")
ElseIf nRadio == 3
	FRename(cPathMenu + "sigataf_fiscal.xnu",cPathMenu + "sigataf.xnu")
Else
	FRename(cPathMenu + "sigataf_ecf.xnu",cPathMenu + "sigataf.xnu")
EndIF

CpyS2T("Install\Menus\sigataf.xnu", cRootPath + GetSrvProfString("StartPath","") )

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} buildIniDbAccess
Constroi o arquivo de configura��o do TOTVS|DbAccess

@author Fabio V Santana / Luccas Curcio
@since 27/03/2017
@version 1.0 

@aParam 

@return Nil
/*/
//-------------------------------------------------------------------
Static function buildIniDbAccess(	cPortaDBA as character, cEndSL as character, cPortaSL as character, cUser as character,;
 									  	cPassword as character, cBanco as character, cNomeODBC as character, cFile as character )

Local	cString 	as character
Local	cRootPath 	as character

Local	nHdAccess 	as numeric

Default	cEndSL		:=	''
Default	cPortaSL	:=	''

cString 		:=	""
cRootPath 	:=	allTrim(GetSrvProfString("RootPath",""))
nHdAccess 	:=	fCreate( cRootPath + "Install\dbaccess.bat" )

If nHdAccess <> -1
	
	cString :=	 'dbaccesscfg.exe '
	cString +=	 ' -u ' + allTrim( cUser )
	cString +=	 ' -p ' + allTrim( cPassword ) 
	cString +=	 ' -d ' + allTrim( cBanco ) 
	cString +=	 ' -a ' + allTrim( cNomeODBC )
	cString +=	 ' -g "port=' + allTrim( cPortaDBA )
	
	If !empty( cEndSL ) .and. !empty( cPortaSL )
		cString +=	 ';LicenseServer=' + allTrim( cEndSL )
		cString +=	 ';LicensePort=' + allTrim( cPortaSL )
	EndIf
	
	cString += '" '
	
	If !empty( cFile ) // arquivo OCI -> Banco de dados ORACLE
		cString +=	 " -c " + allTrim( cFile )
		cString +=	 ' -o "LogAction=0;MemoAsBlob=1"'
	Endif

	If fWrite( nHdAccess , cString )
    	conOut( "[TAFINSTALL] -> Escreveu Texto no novo dbaccess.ini: " + cString + CRLF + "---> Handle: " + str( nHdAccess ) )
    	
    	fClose( nHdAccess )
    	
    	//Executar a bat para criar o DBAccess.ini
		FWMsgRun(,{|| WaitRunSrv( cRootPath + "Install\DbAccess.bat" , .T. , cRootPath + "Install\" ) },"DBAccess.","Criando o arquivo DBAccess.ini...")

    	If !( cpyT2S( cRootPath + "Install\dbaccess.ini", "\TafDbAccess32\" ) )
    		MsgInfo("N�o foi poss�vel copiar o arquivo <b>dbaccess.ini</b>. Verifique suas permiss�es de grava��o em disco. C�digo do erro: " + str( fError() ), "Falha!")
    	EndIf

		If !( cpyT2S( cRootPath + "Install\dbaccess.ini", "\TafDbAccess64\" ) )
    		MsgInfo("N�o foi poss�vel copiar o arquivo <b>dbaccess.ini</b>. Verifique suas permiss�es de grava��o em disco. C�digo do erro: " + str( fError() ), "Falha!")
    	EndIf
    	
    Else
    	MsgInfo("N�o foi poss�vel editar o arquivo <b>dbaccess.ini</b> no diret�rio \StartPath\. Verifique suas permiss�es de grava��o em disco. C�digo do erro: " + str( fError() ), "Falha!")
    Endif
    
    fErase( cRootPath + "Install\dbaccess.bat" )
    fErase( cRootPath + "Install\dbaccess.ini" )

Else
	MsgInfo("N�o foi poss�vel abrir o arquivo <b>dbaccess.ini</b>. Verifique suas permiss�es de grava��o em disco. C�digo do erro: " + str( fError() ), "Falha!")
Endif

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} runTcLink
Executa o TCLink para testar configura��es do DbAccess

@author Fabio V Santana / Luccas Curcio
@since 27/03/2017
@version 1.0 

@aParam 

@return Nil
/*/
//-------------------------------------------------------------------
Static function runTcLink( cConn as character , cServerDBA as character , cPortaDBA as character, cRootPath as character)

Local	nHwnd	as numeric
Local	nHndBat	as numeric
Local	lRet	as logical
Local 	cString as character

nHwnd	:=	0
nHndBat :=	fCreate( cRootPath + "Install\RService.bat" )

lRet	:=	.F.

//Antes de testar a conex�o, reinicio o servi�o do DBaccess
cString := 'set ServiceName=Taf-DbAccess32' + CRLF
cString += 'SC QUERY %ServiceName% > NUL' 	+ CRLF
cString += 'IF ERRORLEVEL 1060 GOTO fim' 	+ CRLF
cString += 'net stop %ServiceName%' 		+ CRLF
cString += 'net start %ServiceName%' 		+ CRLF
cString += ':fim' 							+ CRLF
cString += 'net stop Taf-DbAccess64' 		+ CRLF
cString += 'net start Taf-DbAccess64' 		+ CRLF

If nHndBat <> -1
    FWrite(nHndBat, cString)
    FClose(nHndBat)

	FWMsgRun(,{|| WaitRunSrv( cRootPath + "Install\RService.bat" , .T. , cRootPath + "Install\" ) },"DBAccess.","Reiniciando o servi�o do DBAccess...")
	
	fErase( cRootPath + "Install\RService.bat" )
EndIf	

FWMsgRun(,{||sleep(1000) , nHwnd := TCLink( cConn , AllTrim( cServerDBA ) , Val( cPortaDBA ) ) },"TOTVS|DbAccess","Testando a conex�o do TOTVS|DbAccess...")

If nHwnd >= 0
	MsgInfo("A conex�o com o DBAccess foi configurada <b>corretamente!</b>", "Sucesso!")
	lRet := .T.
Else
	MsgInfo("A conex�o com o DBAccess <b>FALHOU!<b>", "Falha!")
	lRet := .F.
Endif

TCUnlink()

return lRet
//-------------------------------------------------------------------
/*/{Protheus.doc} TafDelTMP
Rotina que faz a limpeza de arquivos de instala��o desnecess�rios
@author Roberto Souza
@since 10/01/2018
@version 1.0 

@aParam

@return Nil
/*/
//-------------------------------------------------------------------
Static Function TafDelTMP()
	Local lRet 		as Logical
	Local cRootP	as character
	Local cStartP	as character
	Local aObj		as array
	Local nX		as numeric
	Local cResult	as character


	lRet 	:= .F.
	cRootP 	:= GetSrvProfString("RootPath", "\undefined")+"\"
	cStartP	:= ""
	aObj	:= {}
	nx 		:= 0
	cResult	:= ""

	cRootP := StrTran( cRootP ,"\\","\")

	AADD( aObj, { cRootP,	"CriaIni.bat"		,"A","S"})
	AADD( aObj, { cRootP,	"CriaIni_32.bat"	,"A","S"})
	AADD( aObj, { cRootP, 	"CriaIni_64.bat"	,"A","S"})
	AADD( aObj, { cRootP,	"install"			,"D","S"})

	For Nx := 1 To Len( aObj )
		lDeleted := .F.
		IF aObj[nX][03] == "A" // Arquivos
			
			lDeleted := ( FErase( aObj[nX][01] + aObj[nX][02] ) == 0 )
			
			If lDeleted
				cResult += aObj[nX][01] + aObj[nX][02] + " removido."+CRLF			
			Else
				cResult += aObj[nX][01] + aObj[nX][02] + " n�o removido."+CRLF
			EndIf

		ElseIf aObj[nX][03] == "D" // Diretorios
			If aObj[nX][04] == "S" // Recursivo
				lDeleted := TafDelStru( aObj[nX][01] + aObj[nX][02] )
			Else
				lDeleted := DirRemove( aObj[nX][01] + aObj[nX][02] )
			EndIf

			If lDeleted
				cResult += aObj[nX][01] + aObj[nX][02] + " removido."+CRLF			
			Else
				cResult += aObj[nX][01] + aObj[nX][02] + " n�o removido."+CRLF
			EndIf
		EndIf
	Next 

Return( lRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidLS
Rotina que faz a chamada para a fun��o TAFLSCheck exibindo a tela de aguarde do processamento
@author Roberto Souza
@since 12/01/2018
@version 1.0 

@aParam

@return Nil
/*/
//-------------------------------------------------------------------
Static Function ValidLS( cEndSL , nPortaSL, cEnvLS  )
	Local lRet As Logical

	lRet := .F.

	FWMsgRun(,{|| CursorWait(), lRet := TAFLSCheck( AllTrim( cEndSL ), nPortaSL , AllTrim( cEnvLS )),CursorArrow()},,"Validando as configura��es..." ) 

Return( lRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFLSCheck
Rotina que faz a valida��o de um servidor de licen�as TOTVS.
@author Roberto Souza
@since 10/01/2018
@version 1.0 

@aParam	cEndSL 		-	Host do servidor
		cPortaSL	-	Porta do Servidor
		cEnvLS		-	Ambiente do Servidor
		lMsg 		-	Informa se exibe ou n�o mensagem 

@return Nil
/*/
//-------------------------------------------------------------------
Function TAFLSCheck( cEndSL , cPortaSL, cEnvLS , lMsg )
	Local lRet 		As Logical 
	Local nRet 		As numeric
	Local cMsgErro 	As character 
	
	Default lMsg 	:= .T.

	lRet 	:= .F.
	cMsgErro := ""

	If !Empty(cEnvLS)

		oRpcSrv := TRpc():New( cEnvLS )

		If oRpcSrv:Connect( cEndSL, cPortaSL ) 
			nRet := oRpcSrv:CallProcEX('LS_GetAvaible',100000)
			oRpcSrv:Disconnect()
			lRet := nRet > 0

			If !lRet 
				cMsgErro += FWLSCodErr( nRet )
			EndIf	
		Else
			cMsgErro += "Endere�o...: " + cEndSL + CRLF
			cMsgErro += "Porta......: " + cValToChar(cPortaSL) + CRLF
			cMsgErro += "Ambiente...: " + cEnvLS + CRLF	
			cMsgErro += "" + CRLF	
			cMsgErro += "Informe uma configura��o v�lida, ou deixe o campo 'Ambiente' em branco para prosseguir." + CRLF	
			cMsgErro += "" + CRLF	
			cMsgErro += "Caso n�o informe um servidor de licen�as v�lido, ser� necess�rio " + CRLF			
			cMsgErro += "configur�-lo no manualmente no 'appserver.ini' para a correta utiliza��o do sistema." + CRLF	
		Endif
	ElseIf Empty(cEndSL) .Or. Empty(cPortaSL)
			cMsgErro += "Endere�o...: " + cEndSL + CRLF
			cMsgErro += "Porta......: " + cValToChar(cPortaSL) + CRLF
			cMsgErro += "" + CRLF
			cMsgErro += "Informe o Endere�o e a Porta de conex�o para prosseguir com a instala��o." + CRLF
	Else
		lRet := .T.
	EndIf

	If lMsg .And. !Empty( cMsgErro )
		MsgInfo( cMsgErro, "Servidor de licen�as inv�lido!" )
	EndIf

Return( lRet )