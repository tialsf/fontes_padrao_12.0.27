#INCLUDE "PROTHEUS.CH" 
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TAFAPR4040.CH"

#DEFINE TCABC 1,1
#DEFINE TNATR 2,1
#DEFINE TPGTO 3,1 
#DEFINE DELTCABC 1,2
#DEFINE DELTNATR 2,2
#DEFINE DELTPGTO 3,2


Static oModel543 as Object

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFAPR4040
Rotinas de apura��o do Pagamentos a beneficiarios n�o identificados
Registro R-4040 da Reinf 2.0

@author Leticia Campos; Katielly Feitosa
@since  23/08/2019
@version 1.0
/*/

Function TAFAPR4040(cReg, cPeriod,dDtIni,dDtFim,cIdApReinf, aFil,oSay,lValid   )

Private aMovs as array

Default cReg	   := ""
Default cTpAmb     := ""
Default cIdApReinf := ""
Default aMovs      := {}

oModel543 := FWLoadModel("TAFA543")

TAFR4040(cReg, cPeriod, cIdApReinf, aFil, cTpAmb,aMovs,oSay)

oModel543:Destroy() 
oModel543 := Nil

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFR4040
Rotinas de apura��o do Pagamentos a beneficiarios n�o identificados
Registro R-4040 da Reinf 2.0
Contem a inteligencia de verifica��o de cada status do modelo, e toma a a��o necess�rio de acordo com o status retornado

@author Leticia Campos; Katielly Feitosa
@since  23/08/2019
@version 1.0
/*/

Static Function TAFR4040(cReg, cPeriod, cIdApReinf, aFil, cTpAmb,aMovs,oSay )
Local cPeriodGrv as Character
Local cStatus    as Character
Local cVerAnt	 as Character
Local cProTpn    as Character
Local cId		 as Character

Local lExecApr	 as Logical
Local lDsarm     as Logical

Local aApurac    as Array

Default cPeriod	:= "" 
Default cReg    := ""
Default cTpAmb  := ""
Default oSay   	:= Nil

cPeriodGrv 	:= cPeriod 
cStatus     := ""
cVerAnt	    := ""  
cProTpn     := ""    
cId		    := ""  
//lProc		:= oSay <> nil
lExecApr    := .F. 
lDsarm      := .F.
aApurac     := {}
cPeriod 	:= SubSTR(cPeriod,3,4) + SubSTR(cPeriod,1,2) 

If !Empty(cPeriod)
    aApurac	    := Apur4040(cPeriod, aFil,aMovs,oSay,cReg)
    lExecApr 	:= aApurac[4]
EndIf

If lExecApr
    cIdApReinf := TafXLogIni( Nil, cReg )
    V4N->(DbSetOrder(2)) //V4N_FILIAL, V4N_PERAPU, V4N_TPINSC, V4N_NRINSC, V4N_ATIVO
    If !(aApurac[TCABC])->(Eof())

        (aApurac[TCABC])->(DbSetOrder(1))
        (aApurac[TCABC])->(DbGoTop())

        Begin Transaction
        cStatus := Stats4040( cPeriodGrv + (aApurac[TCABC])->( CTPINSC + CNRINSC), @cVerAnt, @cProTpn,@cId )
       
            Do Case
            Case cStatus $ ' |0|1|3|7'
                If ExcluiReg()
                    If Grava4040(MODEL_OPERATION_INSERT,cReg, cPeriod,aApurac,cIdApReinf, , , , , aFil, cTpAmb,aMovs,oSay )
                        lDsarm := .T.
                    EndIf
                Else 
                    lDsarm := .T.
                EndIf

            //Registro transmitido ao governo e sem retorno, n�o deve ser alterado
            Case cStatus $ '2|6'
            //TRATAR S� ERRO AQUI

            Case cStatus == '4'
                FAltRegAnt( 'V4N', '2', .F. ) //Inativa o registro anterior
                If Grava4040(MODEL_OPERATION_INSERT,cReg, cPeriod,aApurac,cIdApReinf, cVerAnt, cProTpn, cStatus, cId, aFil, cTpAmb,aMovs,oSay )
                    lDsarm := .T.
                EndIf

            Case cStatus == "Z" //Commit do modelo em modo de inclus�o	
                If Grava4040(MODEL_OPERATION_INSERT,cReg, cPeriod,aApurac,cIdApReinf, , , , , aFil, cTpAmb,aMovs,oSay ) 
                    lDsarm := .T.
                EndIf   
            EndCase

            If lDsarm
                DisarmTransaction()
            EndIf
    End Transaction 
EndIf        
    //Destr�i as tabelas tempor�rias
	aApurac[DELTCABC]:Delete()
	aApurac[DELTNATR]:Delete()
	aApurac[DELTPGTO]:Delete()
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} Apur4040
Rotinas de apura��o do Pagamentos a beneficiarios n�o identificados
Registro R-4040 da Reinf 2.0
Executa a query principal, esta que � montada por Qury4040()

@author Katielly Feitosa, Leticia Campos
@since  21/08/2019
@version 1.0
/*/ 

Static Function Apur4040(cPeriod, aFil,aMovs,oSay,cReg )
Local cCampos       as  Character
Local cAliasV4K     as	Character
Local cQuery        as  Character
Local cFiliais	    as  Character

Local aRegApur      as  Array

Default cPeriod     := ""
Default aFil        := {}

cFiliais  := RetFil(aFil) //"D MG 01 " // -->Atencao
//cPeriod   := SubSTR(cPeriod,3,4) + SubSTR(cPeriod,1,2) 

oSay:cCaption := STR0001 + cReg
ProcessMessages()

cCampos      := "V4K_FILIAL AS FIL, V4K_CODDOC AS CODDOC, V4K_NATREN AS NATREN, V4K_DTPAG AS DTPAG, V4K_VLRLIQ AS VLRLIQ, V4K_VLREAJ AS VLREAJ, V4K_VLRIR AS VLRIR, V4K_DESCRI AS DESCRI,  V4K.R_E_C_N_O_ AS RECNO"
cAliasV4K	 := GetNextAlias() 
cQuery       := QRY4040(cPeriod, aFil )
aRegApur     := {}

    cCampos 	:= "%" + cCampos + "%"
    cQuery   	:= "%" + cQuery    + "%"

    BeginSql Alias cAliasV4K

        SELECT
            %Exp:cCampos%
        FROM
            %Exp:cQuery%
    EndSql
      
    oSay:cCaption := STR0002 + cReg
    ProcessMessages()

    aRegApur := TTR4040(cPeriod, cAliasV4K,aMovs,oSay,cReg )

Return aRegApur

//-------------------------------------------------------------------
/*/{Protheus.doc} Qury4040
Query do registro R-4040 Pagamentos a beneficiarios n�o identificados da Reinf 2.0

@author Katielly Feitosa; Leticia Campos
@since  20/08/2019
@version 1.0
@return Retorna a string da query
/*/ 

Function QRY4040(cPeriod, aFil )
Local cFiliais	as Character
Local cQuery 	as Character	
Local cBd	    as Character

Local aRegApur	as Array

Default cPeriod := ""   
Default aFil    := {}

cFiliais    := RetFil(aFil)
cQuery      := ""
cBd		    := TcGetDb()
aRegApur	:= {}

cQuery        := RetSqlName( "V4K" ) + " V4K "
cQuery        += " WHERE V4K_FILIAL IN (" + cFiliais + ") AND "

If cBd $ "ORACLE|POSTGRES|DB2"
    cQuery	+=  " SUBSTR(V4K_DTPAG,1,6) = '"+ cPeriod +"'"
        
ElseIf cBd $ "INFORMIX"
    cQuery	+=  " V4K_DTPAG[1,6] = '"+ cPeriod +"'"
Else
    cQuery	+=  " SUBSTRING(V4K_DTPAG,1,6) = '"+ cPeriod +"'"
EndIf

cQuery  += " AND D_E_L_E_T_ = ' ' "

Return cQuery

//-------------------------------------------------------------------
/*/{Protheus.doc} TTR4040
Rotinas de apura��o do Pagamentos a beneficiarios n�o identificados
Registro R-4040 da Reinf 2.0
Chama a fun��o de cria��o das temporary tables e popula as tabelas tempor�rias

@author Leticia Campos e Katielly Feitosa
@since  21/08/2019
@version 1.0
/*/ 

Static Function TTR4040( cPeriod, cAliasV4K,aMovs,oSay,cReg )
Local cAlsTTCabc	as Character
Local cAlsTTNRen	as Character
Local cAlsTTPgto	as Character
Local cTpInsc       as Character
Local cNrInsc       as Character
Local cChvPag       as Character
Local cChvNat       as Character

//Vari�veis para controle o oSay
Local nTotReg       as Numeric
Local nContA 	    As Numeric
Local nQtdPerc	    As Numeric
Local nPercent      As Numeric
Local nRange        As Numeric
Local nQtRegMsg     As Numeric

Local aCampCabc		as Array
Local aCampRend		as Array
Local aCampPgto		as Array
//Local aMovs         as Array

Local oTTCabc		as Object  
Local oTTNR			as Object
Local oTTPG			as Object

Local lAvanc		as Logical

Default cPeriod     := ""   
Default cAliasV4K	:= ""

cAlsTTCabc	:= ""
cAlsTTNRen	:= ""
cAlsTTPgto	:= ""
cTpInsc     := ""
cNrInsc     := ""
cChvPag     := ""
cChvNat     := ""
aCampCabc	:= {}
aCampRend	:= {}
aCampPgto	:= {}

nTotReg     := 0
nContApur	:= 0
nContA	    := 0
nQtdPerc    := 1
nPercent    := 10 //9 = 10% ou 19 = 5%  

oTTCabc	    := Nil	 
oTTNR		:= Nil	
oTTPG		:= Nil	
lAvanc		:= .T.
lPerc	    := .T.

DbSelectArea(cAliasV4K)
(cAliasV4K)->(DbEval({|| ++nTotReg }))
(cAliasV4K)->(DbGoTop())

nRange    := int( nTotReg / nPercent )
nQtRegMsg := nRange

// oSay:cCaption := STR0002 + cReg
// ProcessMessages()

If (cAliasV4K)->(!EOF())
    cAlsTTCabc	:= getNextAlias()
    cAlsTTNRen	:= getNextAlias()
    cAlsTTPgto	:= getNextAlias() 
   
    // Cria a estrutura (array) das temporary table
    TTempCmp(@aCampCabc, @aCampRend, @aCampPgto)

    // Instancia o objeto Temporary Table 
    oTTCabc	:= FWTemporaryTable():New(cAlsTTCabc, aCampCabc)
    oTTNR	:= FWTemporaryTable():New(cAlsTTNRen, aCampRend) 
    oTTPG	:= FWTemporaryTable():New(cAlsTTPgto, aCampPgto)

    // Seta os devidos indices 
	TTempIdx(@oTTCabc, @oTTNR, @oTTPG )
    
    cChvPag  := (cAliasV4K)->FIL+(cAliasV4K)->NATREN+(cAliasV4K)->DTPAG
    cChvNat := (cAliasV4K)->FIL+(cAliasV4K)->NATREN

    DbSelectArea(cAlsTTCabc)
	(cAlsTTCabc)->(DbSetOrder(1)) 

    DbSelectArea(cAlsTTPgto)
    (cAlsTTPgto)->(DbSetOrder(1)) 

    DbSelectArea(cAlsTTNRen)
    (cAlsTTNRen)->(DbSetOrder(1))

    (cAliasV4K)->(DbGoTop()) 
    cNrInsc	:=	Posicione("SM0", 1, cEmpAnt+(cAliasV4K)->FIL, "M0_CGC")   
    cTpInsc:= Iif(LEN(cNrInsc)==14, "1", "2")

    If !(cAlsTTCabc)->(DbSeek(cPeriod + cTpInsc + cNrInsc)) 
        RecLock(cAlsTTCabc,.T.)
        (cAlsTTCabc)->FIL	    	:= (cAliasV4K)->FIL	
        (cAlsTTCabc)->ID			:= (cPeriod + cTpInsc + cNrInsc)
        (cAlsTTCabc)->CTPINSC 		:= cTpInsc 	 					//tpInscEstab
        (cAlsTTCabc)->CNRINSC		:= cNrInsc 						//nrInscEstab	
        (cAlsTTCabc)->CPERAPU		:= SubSTR(cPeriod,5,2) + SubSTR(cPeriod,1,4)
        (cAlsTTCabc)->(MsUnlock())   	
    EndIf

    While !(cAliasV4K)->(EOF())

        nContA++
        if lPerc .And. nQtRegMsg < 1
            oSay:cCaption := STR0003 //"Processando 50% dos registros"
            ProcessMessages()
            lPerc := .F.
        elseif lPerc .And. nContA == nQtRegMsg .And. nQtdPerc <= nPercent
            oSay:cCaption := STR0004 + cValToChar( nQtdPerc * 10 ) + STR0005 //"Processando " # "% dos registros"
            ProcessMessages()
            nQtRegMsg := ( (++nQtdPerc)  * nRange )
        endif
      
        If !(cAlsTTPgto)->(MsSeek(cChvPag))
            RecLock(cAlsTTPgto, .T.)
            (cAlsTTPgto)->ID		 := cChvPag
            (cAlsTTPgto)->NATREN	 := (cAliasV4K)->FIL
            (cAlsTTPgto)->NATREN	 := (cAliasV4K)->NATREN
            (cAlsTTPgto)->DDTFG      := SToD((cAliasV4K)->DTPAG)
            (cAlsTTPgto)->NVLRLIQ    := (cAliasV4K)->VLRLIQ
            (cAlsTTPgto)->NVLRREAJ   := (cAliasV4K)->VLREAJ
            (cAlsTTPgto)->NVLRIR     := (cAliasV4K)->VLRIR
            (cAlsTTPgto)->CDESCR     := Alltrim((cAliasV4K)->DESCRI)
            (cAlsTTPgto)->RECNO 	 := (cAliasV4K)->RECNO

            (cAlsTTPgto)->(MsUnlock())
            aAdd( aMovs, { (cAliasV4K)->RECNO } )

            If !(cAlsTTNRen)->(DBSeek(cChvNat)) 
                RecLock(cAlsTTNRen, .T.)
                (cAlsTTNRen)->ID     := cChvNat
                (cAlsTTNRen)->FIL    := (cAliasV4K)->FIL
                (cAlsTTNRen)->NATREN := (cAliasV4K)->NATREN 
             
                (cAlsTTNRen)->(MsUnlock())
            EndIf
        Elseif (cAlsTTPgto)->(MsSeek(cChvPag))
            RecLock(cAlsTTPgto, .f.)
            (cAlsTTPgto)->NVLRLIQ    += (cAliasV4K)->VLRLIQ
            (cAlsTTPgto)->NVLRREAJ   += (cAliasV4K)->VLREAJ
            (cAlsTTPgto)->NVLRIR     += (cAliasV4K)->VLRIR
            (cAlsTTPgto)->CDESCR     := Alltrim((cAlsTTPgto)->CDESCR) + " | " + alltrim((cAliasV4K)->DESCRI) 

            aAdd(aMovs, {(cAliasV4K)->RECNO})
            (cAlsTTPgto)->(MsUnlock())
        EndIf
        (cAliasV4K)->(DbSkip())
        cChvPag  := (cAliasV4K)->FIL+(cAliasV4K)->NATREN+(cAliasV4K)->DTPAG
        cChvNat  := (cAliasV4K)->FIL+(cAliasV4K)->NATREN        
    EndDo        
Else
    lAvanc := .F.
EndIf

Return {{cAlsTTCabc,oTTCabc}, {cAlsTTNRen,oTTNR}, {cAlsTTPgto,oTTPG}, lAvanc,aMovs }

//-------------------------------------------------------------------
/*/{Protheus.doc} TTempCmp
Popula os 3 array com a estrutura das temporary tables

@author Leticia Campos, Katielly Feitosa
@since 21/08/2019
@version 1.0
/*/
Static Function TTempCmp(aCampCabc, aCampRend, aCampPgto)

aCampCabc := {{'ID'			,'C', 250 ,0},;   
             {'FIL'	    	,'C',TamSX3('V4K_FILIAL')[1] ,0},;	
             {'CTPINSC'		,'C', 01 ,0},; 	//tpInscEstab
             {'CNRINSC'		,'C', 14 ,0},; 	//nrInscEstab
             {'CPERAPU' 	,'C', 06 ,0}}	

aCampRend := {{'ID'			,'C', 250 ,0},; 
              {'FIL'	    ,'C', TamSX3('V4K_FILIAL')[1] ,0},;
              {'NATREN'		,'C', 01 ,0}}  //natRendim

aCampPgto := {{'ID'    	    ,'C', 250 ,0},;
             {'FIL'	        ,'C', TamSX3('V4K_FILIAL')[1] ,0},;
             {'NATREN'	    ,'C', 01 ,0},; //natRendim
             {'DDTFG'		,'D', 08 ,0},; //dtFG
             {'NVLRLIQ'		,'N', 14 ,2},; //vlrLiq
             {'NVLRREAJ'	,'N', 14 ,2},; //vlrReaj
             {'NVLRIR'		,'N', 14 ,2},; //vlrIR
             {'CDESCR'		,'C', 200,0},; //descr
             {'RECNO'		,'N', 10 ,0}} 

Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} TTempIdx
Seta o indice das temporary table

@author Leticia Campos, Katielly Feitosa
@since 21/08/2019
@version 1.0
/*/
Static function TTempIdx(oTTCabc, oTTNR, oTTPG )

oTTCabc:AddIndex("1", {"ID"})

oTTNR:AddIndex("1", {"ID"})

oTTPG:AddIndex("1", {"ID"})

oTTCabc:Create()
oTTNR:Create()
oTTPG:Create()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} Grava4040
Efetua grava��o no modelo da tabela espelho do evento R-4040 da Reinf 2.0
(Rotinas de apura��o do Pagamentos a beneficiarios n�o identificados)

@author Leticia Campos, Katielly Feitosa
@since 21/08/2019
@version 1.0
/*/

Static Function Grava4040(nOpc,cReg, cPeriod,aApurac,cIdApReinf, cVerAnt, cProTpn, cRetStat, cId, aFil, cTpAmb,aMovs,oSay)//aMovs Precisa vir aqui 

Local cEvento	    as Character
Local cTempV4N      as Character
Local cTempV4O      as Character
Local cTempV4P      as Character
Local cChvV4O       as Character

Local nQtd          as Numeric
Local nQtdPG        as Numeric
Local nC1EArray	    as Numeric
Local nTamFil       as Numeric
Local nTamSx3       as Numeric

//Vari�veis para controle o oSay
Local nTotReg       as Numeric
Local nContA 	    As Numeric
Local nQtdPerc	    As Numeric
Local nPercent      As Numeric
Local nRange        As Numeric
Local nQtRegMsg     As Numeric

Local lRollBack     as Logical
Local oModelV4O	    as Object
Local oModelV4P	    as Object
Local oModelV4N	    as Object
Local aInfoC1E      as Array

Default cReg        := "" 
Default cPeriod     := ""
Default	cVerAnt	    := ""
Default cProTpn	    := ""
Default cRetStat    := ""
Default cId		    := ""
Default cIdApReinf  := ""
Default cTpAmb      := ""
Default nOpc        := 0
Default aApurac     := {}
Default aFil        := {}

cEvento	   := "I"
cTempV4N   := aApurac[TCABC]
cTempV4O   := aApurac[TNATR]
cTempV4P   := aApurac[TPGTO]
cChvV4O	   := ""
nQtd       := 0  
nQtdPG     := 0  
nC1EArray  := ASCAN(aFil,{|x|x[7]})
nTamFil    := len( aFil )
nTamSx3    := TamSX3('V4N_FILIAL')[1]
lRollBack  := .F.
oModelV4N  := oModel543:GetModel("MODEL_V4N")
oModelV4O  := oModel543:GetModel("MODEL_V4O")
oModelV4P  := oModel543:GetModel("MODEL_V4P")
aInfoC1E   := {}

nTotReg     := 0
nContApur	:= 0
nContA	    := 0
nQtdPerc    := 1
nPercent    := 10 //9 = 10% ou 19 = 5%  

lPerc	    := .T.

oSay:cCaption := STR0006 + cReg
ProcessMessages()

If nC1EArray > 0 
    AADD(aInfoC1E,aFil[nC1EArray][1])
    AADD(aInfoC1E,aFil[nC1EArray][4])
Elseif nTamFil > 0
	AADD(aInfoC1E,aFil[nTamFil][1])
	AADD(aInfoC1E,aFil[nTamFil][4])  
EndIf

oModel543:SetOperation(nOpc)
oModel543:Activate()

DbselectArea("V4N")
V4N->(DbSetOrder(2))

If !Empty(cVerAnt)
	oModel543:LoadValue( 'MODEL_V4N', 'V4N_VERANT', cVerAnt )
	oModel543:LoadValue( 'MODEL_V4N', 'V4N_PROTPN', cProTpn )
	oModel543:LoadValue( 'MODEL_V4N', 'V4N_ID'	  , cId     )
	If cRetStat == "7"
		cEvento := 'I'		
	Else
		cEvento := 'A'
	EndIf	
EndIf

oModel543:LoadValue('MODEL_V4N', 'V4N_VERSAO'  , xFunGetVer())
oModel543:LoadValue('MODEL_V4N', 'V4N_STATUS'  , '')
oModel543:LoadValue('MODEL_V4N', 'V4N_EVENTO'  , cEvento)
oModel543:LoadValue('MODEL_V4N', 'V4N_ATIVO'   , '1')
oModel543:LoadValue('MODEL_V4N', 'V4N_PERAPU'  , (aApurac[TCABC])->CPERAPU)	
oModel543:LoadValue('MODEL_V4N', 'V4N_TPAMB'   , "1")	
oModel543:LoadValue('MODEL_V4N', 'V4N_IDESTA'  , aInfoC1E[1])
oModel543:LoadValue('MODEL_V4N', 'V4N_DESTA'   , aInfoC1E[2])
oModel543:LoadValue('MODEL_V4N', 'V4N_TPINSC'  , (aApurac[TCABC])->CTPINSC)		//tpInscEstab - CNPJ
oModel543:LoadValue('MODEL_V4N', 'V4N_NRINSC'  , (aApurac[TCABC])->CNRINSC) 	//nrInscEstab													

DbSelectArea(cTempV4O) 
(cTempV4O)->(DbSetOrder(1))
(cTempV4O)->(DbEval({|| ++nTotReg }))
(cTempV4O)->(DbGoTop())

DbSelectArea(cTempV4P) 
(cTempV4P)->(DbSetOrder(1))
(cTempV4P)->(DbGoTop())

nRange    := int( nTotReg / nPercent )
nQtRegMsg := nRange

cChvV4O := (cTempV4O)->FIL + (cTempV4O)->NATREN

If (cTempV4O)->(DbSeek(cChvV4O))

    cQtd    :=  0

    While !(cTempV4O)->(Eof()) .And. (cChvV4O) $ (cTempV4O)->ID 
        ++cQtd
 
        oModelV4O:lValid := .T. 
        If cQtd > 1 .AND. nOpc == MODEL_OPERATION_INSERT
            oModelV4O:AddLine()
        EndIf 
        
        oModel543:LoadValue('MODEL_V4O', 'V4O_NATREN', (cTempV4O)->NATREN )

        cQtdPG := 0

        While !(cTempV4P)->(Eof()) .AND. (cChvV4O) $ SubStr((cTempV4P)->ID, 1, nTamSx3 + 1)
            ++cQtdPG

            oModel543:lValid:= .T. 
            If cQtdPG > 1 .AND. nOpc == MODEL_OPERATION_INSERT 
                oModelV4P:AddLine()
            EndIf

            oModel543:LoadValue('MODEL_V4P', 'V4P_NATREN', (cTempV4P)->NATREN)
            oModel543:LoadValue('MODEL_V4P', 'V4P_DATAFG', (cTempV4P)->DDTFG)
            oModel543:LoadValue('MODEL_V4P', 'V4P_VLRLIQ', (cTempV4P)->NVLRLIQ)
            oModel543:LoadValue('MODEL_V4P', 'V4P_VLRREA', (cTempV4P)->NVLRREAJ)
            oModel543:LoadValue('MODEL_V4P', 'V4P_VLRIR' , (cTempV4P)->NVLRIR)
            oModel543:LoadValue('MODEL_V4P', 'V4P_DESCRI', (cTempV4P)->CDESCR)

            //aAdd(aMovs, {(cTempV4P)->RECNO})

            (cTempV4P)->(DbSkip())
        EndDo
            
            nContApur++
            if lPerc .And. nQtRegMsg < 1
                oSay:cCaption := STR0007 //"Gravando 50% dos registros"
                ProcessMessages()
                lPerc := .F.
            elseif lPerc .And. nContApur == nQtRegMsg
                conout("Entrou")
               // Sleep(1000)
                oSay:cCaption := STR0008 + cValToChar( nQtdPerc * 10 ) + STR0009 //"Gravando " # "% dos registros" 
                ProcessMessages()
                nQtRegMsg := ( (++nQtdPerc)  * nRange )
            endif

        (cTempV4O)->(DbSkip())
        cChvV4O := (cTempV4O)->FIL + (cTempV4O)->NATREN
    EndDo

    If !lRollBack 
        If oModel543:VldData()
            FwFormCommit(oModel543)		
            TafEndGRV( "V4N","V4N_PROCID", cIdApReinf, V4N->(Recno())) //Grava PROCID na espelho
            GravaId(aMovs, cIdApReinf) //Grava PROCID no legado
		Else
            lRollBack := .T.	
        EndIf
        
        oSay:cCaption := "Gravando 100% dos registros" //"Gravando 50% dos registros"
        ProcessMessages()
    EndIf

Else
    lRollBack   :=  .T.
EndIf
oModel543:DeActivate()

Return lRollBack

//-------------------------------------------------------------------
/*/{Protheus.doc} Stats4040
Verifica a exist�ncia ou n�o do registro que ser� apurado

@author Leticia Campos, Katielly Feitosa
@since 26/08/2019
@version 1.0

@parameters Retorna o status do registro encontrado, caso contr�rio retorna status "Z", 
indicando que ainda n�o existe o registro no cadastro espelho
@return
/*/ 
Static Function Stats4040(cChave , cVerAnt, cProTpn, cId )

Local cRetStat   as Character

Default cChave   := "" 
Default cVerAnt  := ""
Default cProTpn  := ""
Default cId		 := ""

cRetStat   := ""

    DbSelectArea("V4N")
    DbSetOrder(2)
    If V4N->( MsSeek( cFilAnt + cChave + '1') ) //cPeriod + cTpInsc + cNrInsc 
        cVerAnt	 := V4N->V4N_VERSAO
        cProTpn  := V4N->V4N_PROTUL
        cRetStat := V4N->V4N_STATUS
        cId		 := V4N->V4N_ID
    Else
        cVerAnt  := ""
        cProTpn  := ""
        cRetStat := "Z"
        cId		 := ""
    EndIf


Return cRetStat 

//-------------------------------------------------------------------
/*/{Protheus.doc} ExcluiReg()
Efetua a exclus�o do modelo conforme par�metro

@author Katielly Feitosa
@since 28/08/2019
@version 1.0
@return
/*/ 

Static Function ExcluiReg()
Local cTpInscEst as Character
Local cNrInscEst as Character
Local oModel     as Object
Local lExcluiu   as Logical

oModel 		:= oModel543 
cTpInscEst	:= V4N->V4N_TPINSC
cNrInscEst	:= V4N->V4N_NRINSC
lExcluiu    := .T.

oModel:SetOperation(5)
oModel:Activate()

If FwFormCommit(oModel)
	lExcluiu := .T.
EndIf

oModel:DeActivate()

Return lExcluiu

//-------------------------------------------------------------------
/*/{Protheus.doc} GravaId()
Fun��o que grava o PROCID na tabela legado

@author Katielly Feitosa
@since 29/08/2019
@version 1.0
@return
/*/ 
Static Function GravaId(aMovs, cIdApur)

Local	nX	as numeric

Default cIdApur	:= ''
//Default aMovs 	:= {}

For nX := 1 to Len(aMovs)
    TafEndGRV( "V4K","V4K_PRID40", cIdApur, aMovs[nX][1]  )
Next nX

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} RetFil()

Trata o array de filiais passado pela tela da apura��o
para que fique no formato de execu��o do IN no SQL

@author Katielly Rezende
@since 30/08/2019
@version 1.0
@return
/*/ 
Static Function RetFil(aFil)

Local cRetFils	as Character
Local nX		as Numeric

cRetFils	:= ""
nX			:= 0

For nX := 1 to Len(aFil)
    If nX > 1
        cRetFils += " , '" + aFil[nX][2] + "'"
    Else
        cRetFils += "'" + aFil[nX][2] + "'"
    EndIf
Next nX

Return cRetFils
