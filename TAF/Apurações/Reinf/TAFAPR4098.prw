#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TAFAPR4098.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFAPR4098
Fun��o que efetua chamada da rotina de apura��o do evento R-4098 

@param cEvento, caracter, Nome do evento a ser processado
@param cPeriodo, caracter, periodo a ser processado MMAAAA
@param cIdLog, caracter, ID de processamento da apura��o

@return Nil

@author Jos� Mauro
@since  19/09/2019
@version 1.1
/*/
//---------------------------------------------------------------------
Function TAFAPR4098( cEvento, cPeriodo , dDtIni, dDtFim, cIdLog, aFiliais, oProcess, lJob )
Local cReturn	:= ''
default lJob	:= .f.

	cReturn := TAFR4098( cEvento, cPeriodo , dDtIni, dDtFim, cIdLog, aFiliais, oProcess, lJob ) 	

Return (cReturn)

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFR4098
Fun��o que gera um evento de reabertura do per�odo
Premissas: deve existir um evento de 2099- Fechamento, transmitido e validado 

@return Nil

@author Jos� Mauro
@since  19/09/2019
@version 1.1
/*/
//---------------------------------------------------------------------
Static Function TAFR4098( cEvento, cPeriodo , dDtIni, dDtFim, cIdLog, aFiliais, oProcess, lJob )

Local cId			as character
Local cKeyProc		as character
Local cVerReg		as character
Local cVerAnt		as character
Local cProtAnt		as character
Local cReturn		as character
Local cErro			as character
Local oModel 		as object
Local lSeekV30		as logical
local lExecuta		as logical
local lGrava		as logical 
Local cPerMsg		as character

cPerMsg := SubStr(cPeriodo,1,2) + "/" + SubStr(cPeriodo,3,4)
lExecuta	:=	.t.
lGrava		:=	.t.
cReturn		:=	''

cErro		:=	''

If !lJob
 	lExecuta := MsgYesNO( STR0001 + cPerMsg + "?", "R-4098") //"Deseja realmente reabrir o per�odo "
Else
	lExecuta	:= .t.
EndIf 

if lExecuta
	DBSelectArea("V30")
	DBSetOrder(2) //V30_FILIAL, V30_PERAPU, V30_ATIVO, R_E_C_N_O_, D_E_L_E_T_
	If V30->(DBSeek( xFilial("V30") + cPeriodo + "1"))
		cKeyProc 	:= V30->V30_FILIAL + V30->V30_ID + V30->V30_VERSAO 
		lSeekV30 := .T.
	EndIf	
	oModel := FWLoadModel("TAFA547")

	cVerAnt		:= ""
	cProtAnt	:= ""
	lGrava		:= .T.
	cID			:= ""

	If lSeekV30
		// Desativa o registro anterior
		If V30->V30_STATUS == "4"
			cVerAnt	:= V30->V30_VERSAO
			cProtAnt:= V30->V30_PROTUL
			cId := V30->V30_ID
			FAltRegAnt( 'V30', '2', .F. )
			
		// Evento transmitido e aguardando retorno, nao permite gravar
		ElseIf V30->V30_STATUS $ '2|6'
			lGrava	:= .F.
		Else
			// Apaga o Registro
			cVerAnt	:= V30->V30_VERANT
			cProtAnt:= V30->V30_PROTPN
			cId     := V30->V30_ID
					
			oModel:SetOperation(MODEL_OPERATION_DELETE)
			oModel:Activate()
			FwFormCommit( oModel )
			oModel:DeActivate()
		EndIf
	EndIf

	If lGrava
		
		//---- Sempre ser� uma inclus�o
		oModel:SetOperation(MODEL_OPERATION_INSERT)
		oModel:Activate()
		
		cVerReg := xFunGetVer()
		oModel:LoadValue('MODEL_V30', "V30_ID", Iif(Empty(cID), TAFGERAID("TAF"),cID) )
		oModel:LoadValue('MODEL_V30', "V30_VERSAO"	, cVerReg )
		oModel:LoadValue('MODEL_V30', "V30_PERAPU"	, cPeriodo )
		oModel:LoadValue('MODEL_V30', "V30_VERANT"	, AllTrim(cVerAnt))
		oModel:LoadValue('MODEL_V30', "V30_PROTPN"	, AllTrim(cProtAnt))
		oModel:LoadValue('MODEL_V30', "V30_EVENTO"	, "I" )
		oModel:LoadValue('MODEL_V30', "V30_ATIVO "	, "1" )
		oModel:LoadValue('MODEL_V30', "V30_STATUS "	, "0" )			
					
		If oModel:VldData() 
			FwFormCommit( oModel )
			TafEndGRV( "V30","V30_PRID40", cIdLog, V30->(Recno()))
			MsgAlert( STR0002 + cPerMsg + STR0003)	//"Per�odo " " reaberto!"
			TafXLog( cIdLog, cEvento, "MSG"			, STR0004+CRLF+"Recno => "+cValToChar(V30->(Recno())) )	//"Registro Gravado com sucesso."					
		Else
			cErro := TafRetEMsg( oModel )
			TafXLog( cIdLog, cEvento, "ERRO"			, STR0005 + CRLF + cErro )	//"Mensagem do erro: "					
		EndIf

		oModel:DeActivate()					
				
	Else
	 	TafXLog( cIdLog, cEvento, "ALERTA"			, STR0006+CRLF+ cKeyProc )	//"Evento transmitido e aguardando retorno:"									
	EndIf
EndIf

if !lGrava	
	cReturn += "Evento transmitido e aguardando retorno, n�o permite gravar"
endif	

if !empty(cErro)	
	cReturn += cErro
endif

Return (cReturn)