#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TAFAPR2099.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFAPR4099
Fun��o que efetua chamada da rotina de apura��o do evento R-4099 

@param cEvento, caracter, Nome do evento a ser processado
@param cPeriodo, caracter, periodo a ser processado MMAAAA
@param cIdLog, caracter, ID de processamento da apura��o

@return Nil

@author Jos� Mauro
@since  23/09/2019
@version 1.1
/*/
//---------------------------------------------------------------------
Function TAFAPR4099( cEvento, cPeriodo , dDtIni, dDtFim, cIdLog, aFiliais, oSayEvt, lJob )
	Local cReturn as character	
	Local lProc     as logical

	default lJob := .f.

	lProc 	:= oSayEvt <> nil
	cReturn	:= ""
	If lProc
			oSayEvt:cCaption := "Processando evento: " + cEvento  //"Processando apura��o dos eventos"
			ProcessMessages()	
	EndIf

	cReturn := TAFR4099( cEvento, cPeriodo , dDtIni, dDtFim, cIdLog, aFiliais, oSayEvt, lJob ) 	

Return( cReturn )

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFR4099
Fun��o que gera um evento de fechamento do per�odo
Premissas: deve existir um evento de 4099- Fechamento, transmitido e validado 

@return Nil

@author Jos� Mauro
@since  23/09/2019
@version 1.1
/*/
//---------------------------------------------------------------------
Static Function TAFR4099( cEvento, cPeriodo , dDtIni, dDtFim, cIdLog, aFiliais, oSayEvt, lJob )

Local cId			as character
Local cKeyProc		as character
Local cVerReg		as character
Local cVerAnt		as character
Local cVerAtu		as character
Local cProtAnt		as character
Local oModel 		as object
Local lSeekV3W		as logical 
Local cPerMsg		as character
Local cStat4010		as character
Local cStat4020		as character
Local cStat4040		as character
Local cSemMovto		as character
Local cMovtoAnt		as character		
Local cReturn		as character
Local nMatriz		as numeric
Local cKeyC1E		as character
Local aContato 		as array
Local cAmbReinf		as character
Local cVerReinf 	as character
Local cComMvt		as character
Local cPerAM    	as character
Local cErro			as character
Local lExecuta		as logical
Local lGrava		as logical


cVerReinf	:= StrTran( SuperGetMv('MV_TAFVLRE',.F.,'1_04_00') ,'_','')
nMatriz		:= aScan(aFiliais,{|x| x[07] })
cPerMsg 	:= SubStr(cPeriodo,1,2) + "/" + SubStr(cPeriodo,3,4)
cReturn		:= ""
cErro		:= ""
cKeyC1E		:= ""
cMovtoAnt	:= ""
aContato 	:= {}
cAmbReinf	:= Left(GetNewPar( "MV_TAFAMBR", "2" ),1)
cPerAM		:= right(cPeriodo,4)+left(cPeriodo,2) //Periodo no formato AAAAMM
lExecuta	:= .t.
lGrava		:= .t.

DBSelectArea("V1O")
V1O->(DBSetOrder(2)) // V1O_FILIAL, V1O_PERAPU, V1O_ATIVO, R_E_C_N_O_, D_E_L_E_T_
If !V1O->( DbSeek( xFilial("V1O") + cPeriodo + "1" + "40" ) )
	Taf503Grv("", cPeriodo,Date(), Time(), "", "40" )
EndIf

If GetFechamento(cEvento,cIdLog,cPeriodo)
	If !lJob 
		lExecuta := MsgYesNO( STR0001 + cPerMsg + "?", "R-4099") //"Deseja realmente fechar o per�odo " 
	endif

	if lExecuta
	
		DBSelectArea("V3W")
		DBSetOrder(2) //V3W_FILIAL, V3W_PERAPU, V3W_ATIVO, R_E_C_N_O_, D_E_L_E_T_
		If V3W->(DBSeek( xFilial("V3W") + cPeriodo + "1"))
			cKeyProc 	:= V3W->V3W_FILIAL + V3W->V3W_ID + V3W->V3W_VERSAO 
			lSeekV3W := .T.
		EndIf	
		oModel := FWLoadModel("TAFA548")
	
		cVerAnt		:= ""
		cVerAtu		:= ""
		cProtAnt	:= ""
		lGrava		:= .T.
		cID			:= ""
		cStat4010	:= ""
		cStat4020	:= ""
		cStat4040	:= ""
		cSemMovto	:= ""
		cXmlId		:= ""
	
		If lSeekV3W
			// Desativa o registro anterior
			If V3W->V3W_STATUS == "4" 
				cVerAnt		:= V3W->V3W_VERSAO
				cProtAnt	:= V3W->V3W_PROTUL
				cId 		:= V3W->V3W_ID
				cMovtoAnt	:= V3W->V3W_COMMVT
				FAltRegAnt( 'V3W', '2', .F. )
				
			// Evento transmitido e aguardando retorno, nao permite gravar
			ElseIf V3W->V3W_STATUS $ '2|6'
				lGrava	:= .F.
			Else
				// Apaga o Registro
				cVerAnt		:= V3W->V3W_VERANT
//				cVerAtu	:= V3W->V3W_VERSAO
				cProtAnt	:= V3W->V3W_PROTPN
				cId     	:= V3W->V3W_ID
				cMovtoAnt	:= V3W->V3W_COMMVT
				cXmlId		:= V3W->V3W_XMLID
						
				oModel:SetOperation(MODEL_OPERATION_DELETE)
				oModel:Activate()
				FwFormCommit( oModel )
				oModel:DeActivate()
			EndIf
		EndIf
	
		If lGrava
			aFiliais := aClone(TafXRFils( aFiliais , 2))
			If FindFunction("TafEvRStat")
				TafEvRStat( aFiliais, cPeriodo, "R-4010", @cStat4010  ) 
				TafEvRStat( aFiliais, cPeriodo, "R-4020", @cStat4020  )
				TafEvRStat( aFiliais, cPeriodo, "R-4040", @cStat4040  )

				cStat4010 := IIf( cStat4010  $ "4|6", "1", "2" )
				cStat4020 := IIf( cStat4020  $ "4|6", "1", "2" )
				cStat4040 := IIf( cStat4040  $ "4|6", "1", "2" )

				
			Else
				cStat4010		:= If(TafRStatEv( cPeriodo,  , ,"R-4010", aFiliais  ) == "5", "2", "1" )
				cStat4020		:= If(TafRStatEv( cPeriodo,  , ,"R-4020", aFiliais  ) == "5", "2", "1" )
				cStat4040		:= If(TafRStatEv( cPeriodo,  , ,"R-4040", aFiliais  ) == "5", "2", "1" )
				
			EndIf

			If cStat4010 == "2" .and. cStat4020 == "2" .and. cStat4040 == "2" 
			   //Informar a primeira compet�ncia a partir da qual n�o houve movimento, cuja situa��o perdura at� a compet�ncia atual.
			   cSemMovto		:= SubStr(cPeriodo,3,4)+"-" + SubStr(cPeriodo,1,2)
			EndIf 
			
			//---- Sempre ser� uma inclus�o
			oModel:SetOperation(MODEL_OPERATION_INSERT)
			oModel:Activate()

			
			cVerReg := IIf( Empty(cVerAtu),xFunGetVer(),cVerAtu)

			oModel:LoadValue('MODEL_V3W', "V3W_ID"		, Iif(Empty(cID), TAFGERAID("TAF") ,cID) )
			oModel:LoadValue('MODEL_V3W', "V3W_VERSAO"	, cVerReg )
			oModel:LoadValue('MODEL_V3W', "V3W_PERAPU"	, cPeriodo )
			oModel:LoadValue('MODEL_V3W', "V3W_VERANT"	, AllTrim(cVerAnt))
			oModel:LoadValue('MODEL_V3W', "V3W_PROTPN"	, AllTrim(cProtAnt))
			oModel:LoadValue('MODEL_V3W', "V3W_EVENTO"	, "I" )
			oModel:LoadValue('MODEL_V3W', "V3W_ATIVO "	, "1" )
			oModel:LoadValue('MODEL_V3W', "V3W_STATUS "	, "0" )
			
			oModel:LoadValue('MODEL_V3W', "V3W_RETPF"	, cStat4010 )
			oModel:LoadValue('MODEL_V3W', "V3W_RETPJ"	, cStat4020 )
			oModel:LoadValue('MODEL_V3W', "V3W_RETNID"	, cStat4040 )

			if cVerReinf < '10400' .Or. cPerAM <= "201810"
				cComMvt := If(!Empty(cMovtoAnt) .and. !Empty(cSemMovto),cMovtoAnt, cSemMovto)
			else	
				cComMvt := ''
			endif
			oModel:LoadValue('MODEL_V3W', "V3W_COMMVT"	, cComMvt )			

			If !Empty(cXmlId)
//				oModel:LoadValue('MODEL_V3W', "V3W_XMLID"	, cXmlId )
			EndIf
			If FindFunction("TafRetCTT")
				cKeyC1E		:= xFilial("C1E")+aFiliais[nMatriz][02]+"1"
				aContato 	:= TafRetCTT("C1E", cKeyC1E, 5, "R" )

				oModel:LoadValue('MODEL_V3W', "V3W_NMRESP"	, Substr(aContato[02],1,TamSx3("V3W_NMRESP")[1] ) )
				oModel:LoadValue('MODEL_V3W', "V3W_CPFRES"	, Substr(aContato[03],1,TamSx3("V3W_CPFRES")[1] ) )
				oModel:LoadValue('MODEL_V3W', "V3W_DDDFON"	, Alltrim(Substr(aContato[04],1,TamSx3("V3W_DDDFON")[1] ) ) )
				oModel:LoadValue('MODEL_V3W', "V3W_FONE"	, Alltrim(Substr(aContato[05],1,TamSx3("V3W_FONE")[1] ) ) )
				oModel:LoadValue('MODEL_V3W', "V3W_EMAIL"	, Substr(aContato[01],1,TamSx3("V3W_EMAIL")[1] ) )

			Else
				DBSelectArea("C1E")
				C1E->( DBSetOrder(3) )
				If C1E->( MSSeek( xFilial("C1E") + cFilAnt + "1" ) )
					oModel:LoadValue('MODEL_V3W', "V3W_NMRESP"	, Substr(C1E->C1E_NOMCNT,1,TamSx3("V3W_NMRESP")[1] ) )
					oModel:LoadValue('MODEL_V3W', "V3W_CPFRES"	, Substr(C1E->C1E_CPFCNT,1,TamSx3("V3W_CPFRES")[1] ) )
					oModel:LoadValue('MODEL_V3W', "V3W_DDDFON"	, Alltrim(Substr(C1E->C1E_DDDFON,1,TamSx3("V3W_DDDFON")[1] ) ) )
					oModel:LoadValue('MODEL_V3W', "V3W_FONE"	, Alltrim(Substr(C1E->C1E_FONCNT,1,TamSx3("V3W_FONE")[1] ) ) )
					oModel:LoadValue('MODEL_V3W', "V3W_EMAIL"	, Substr(C1E->C1E_EMAIL,1,TamSx3("V3W_EMAIL")[1] ) )
				EndIf
			EndIf
			
			// Campo que armazena o ambiente gerado para transmiss�o
			If TafColumnPos("V3W_TPAMB")
				oModel:LoadValue('MODEL_V3W', "V3W_TPAMB", cAmbReinf ) 
			EndIf	

			DBSelectArea("V3W")			
						
			If oModel:VldData() 
				FwFormCommit( oModel )
				//grava _PROCID
				TafEndGRV( "V3W","V3W_PROCID", cIdLog, V3W->(Recno()))
				MsgAlert( STR0002 + cPerMsg + STR0003)	//"Per�odo " " fechado!"
				TafXLog( cIdLog, cEvento, "MSG"			, STR0004+CRLF+"Per�odo "+cPeriodo+" fechado com sucesso."+CRLF+"Recno => "+cValToChar(V3W->(Recno())) )	//"Registro Gravado com sucesso."					
			Else
				cErro := TafRetEMsg( oModel )
				TafXLog( cIdLog, cEvento, "ERRO"			, STR0005 + CRLF + cErro )	//"Mensagem do erro: "					
			EndIf
	
			oModel:DeActivate()					
					
		Else
			TafXLog( cIdLog, cEvento, "ALERTA"			, STR0006+CRLF+ cKeyProc )	//"Evento transmitido e aguardando retorno:"									
		EndIf
	Else
		TafXLog( cIdLog, cEvento, "ALERTA"			, "Fechamento cancelado pelo usu�rio!" )
		cReturn := "Cancelado pelo usu�rio!"
	EndIF

	if !lGrava	
		cReturn += "Evento transmitido e aguardando retorno, nao permite gravar"
	endif	

	if !empty(cErro)	
		cReturn += cErro
	endif

EndIf
Return( cReturn )


//---------------------------------------------------------------------
/*/{Protheus.doc} GetFechamento()

Retorna se existe o 4098 - Reabertura transmitido para haver o fechamento 
@param 

@Author		Jos� Mauro
@Since		23/09/2019
@Version	1.0
/*/
//---------------------------------------------------------------------
Static Function GetFechamento(cEvento,cIdLog,cPeriodo)
Local lRet := .T. 
cPeriodo 	:= StrTran(cPeriodo,"/","")
DBSelectArea("V1O")
DBSetOrder(2) //V1O_FILIAL, V1O_PERAPU, V1O_ATIVO, V1O_BLOCO R_E_C_N_O_, D_E_L_E_T_
If V1O->(DBSeek( xFilial("V1O") + cPeriodo + "1" + "40"))
	If (V1O->V1O_STATUS == "4" .and. V1O->V1O_NOMEVE == "R-4098") .or. Empty(V1O->V1O_NOMEVE) 
		lRet := .T.
	Else	
		lRet := .F.
		MsgAlert(STR0007+CRLF+ STR0008) //"Per�odo n�o est� em aberto para haver o fechamento." "Fechamento de per�odo n�o realizado!"
		TafXLog( cIdLog, cEvento, "ALERTA"			, STR0007+CRLF+ STR0008  )
	EndIf
Else	
	lRet := .T.
EndIf	
If lRet // se existe reabertura transmitido ou nenhuma reabertura, verifica se n�o tem nenhum fechamento em aberto
	DBSelectArea("V3W")
	DBSetOrder(2) //V3W_FILIAL, V3W_PERAPU, V3W_ATIVO, R_E_C_N_O_, D_E_L_E_T_
	If V3W->(DBSeek( xFilial("V3W") + cPeriodo + "1"))
		If V3W->V3W_STATUS == "2" 
			lRet := .F.
			MsgAlert(STR0009+CRLF+ V3W->(V3W_FILIAL + V3W_ID ) + ". "+ STR0008) //"J� existe um evento de fechamento R-4099 em aberto:""Fechamento de per�odo n�o realizado!"
			TafXLog( cIdLog, cEvento, "ALERTA"			, STR0009+CRLF+ V3W->(V3W_FILIAL + V3W_ID )  )
	   	Endif
	EndIf
EndIf

Return lRet