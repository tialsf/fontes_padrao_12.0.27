#INCLUDE "PROTHEUS.CH" 
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "TAFA049.CH" 
//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA049
Cadastro MVC dos Per�odos Dispensados da EFD Contribui��es

@author Fabio V. Santana
@since 12/07/2012
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAFA049()
Local	oBrw		:=	FWmBrowse():New()

oBrw:SetDescription(STR0001) //Per�odos Dispensados da EFD Contribui��es
oBrw:SetAlias('C1Z')
oBrw:SetMenuDef( 'TAFA049' )
oBrw:Activate()

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu
@author Denis R de Oliveira
@since 17/02/2014
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao := {}
Local aRotina := {}

Aadd( aFuncao, { "" , "Taf049Vld" , "2" } )
aRotina	:=	xFunMnuTAF( "TAFA049" , , aFuncao )

Return( aRotina )
//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC

@author Fabio V. Santana
@since 12/07/2012
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
Local oStruC1Z 	:= 	FWFormStruct( 1, 'C1Z' )
Local 	oModel 	:= MPFormModel():New( 'TAFA049' , , , {|oModel| SaveModel( oModel ) } )

oModel:AddFields('MODEL_C1Z', /*cOwner*/, oStruC1Z)  
oModel:GetModel('MODEL_C1Z'):SetPrimaryKey( { "C1Z_PERIOD", "C1Z_MESDIS" } )

Return oModel
//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Fabio V. Santana
@since 12/07/2012
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local 	oModel 		:= 	FWLoadModel( 'TAFA049' )
Local 	oStruC1Z 	:= 	FWFormStruct( 2, 'C1Z' )
Local oView 		:= 	FWFormView():New()

oView:SetModel( oModel )
oView:AddField( 'VIEW_C1Z', oStruC1Z, 'MODEL_C1Z' )

oView:EnableTitleView( 'VIEW_C1Z', STR0001 ) //Per�odos Dispensados da EFD Contribui��es
oView:CreateHorizontalBox( 'FIELDSC1Z', 100 )
oView:SetOwnerView( 'VIEW_C1Z', 'FIELDSC1Z' )

Return oView 

//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@param  oModel -> Modelo de dados
@return .T.

@author Denis R. de Oliveira
@since 17/02/2014
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel( oModel )

Local nOperation := oModel:GetOperation()

Begin Transaction
	If nOperation == MODEL_OPERATION_UPDATE
		//Funcao responsavel por setar o Status do registro para Branco
		TAFAltStat( "C1Z", " " )		
	EndIf
	FwFormCommit( oModel )
End Transaction
       
Return .T.
 
//-------------------------------------------------------------------------------------
/*/{Protheus.doc} Taf049Vld

Funcao que valida os dados do registro posicionado,
verificando se ha incoerencias nas informac�es caso seja necessario gerar um XML

lJob - Informa se foi chamado por Job

@return .T.

@author Denis R. de Oliveira
@since 18/02/2014
@version 1.0
/*/                                                                                                                                          
//------------------------------------------------------------------------------------
Function TAF049Vld(cAlias,nRecno,nOpc,lJob)

Local aLogErro  := {}
Local aCarc_Esp := {} 
Local cPeriodo  := AllTrim(C1Z->C1Z_MESDIS)
Local nlI       := 0

Default lJob := .F.

//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := C1Z->( Recno() )

If C1Z->C1Z_STATUS $ ( " |1" )
	
	//Valida a Data do Movimento Decl.
	If Empty(C1Z->C1Z_PERIOD)
		Aadd( aLogErro, { "C1Z_PERIOD", "000010", "C1Z", nRecno } ) //000010 - Campo Inconsistente ou Vazio	
	EndIf
	
	//Valida o Mes de Referencia
	If Empty(C1Z->C1Z_MESDIS)
		Aadd( aLogErro, { "C1Z_MESDIS", "000010", "C1Z", nRecno } ) //000010 - Campo Inconsistente ou Vazio		
	Else
		
		//Valida se o periodo est� com o ano ateh 2049
		If Substr(cPeriodo,3,4) > "2049" 
			
			AADD(aLogErro,{"C1Z_MESDIS","000586","C1Z", nRecno }) //STR0524 - "Informar o periodo no formato m�s/ano(MMAAAA)" 
			
		EndIf
		
		//Imputa os Caracteres Especiais no Array de Controle
		AADD(aCarc_Esp,{"!", "Exclamacao"})
		AADD(aCarc_Esp,{"@", "Arroba"})
		AADD(aCarc_Esp,{"#", "Sustenido"})
		AADD(aCarc_Esp,{"$", "Cifrao"})
		AADD(aCarc_Esp,{"%", "Porcentagem"})
		AADD(aCarc_Esp,{"*", "Asterisco"})
		AADD(aCarc_Esp,{"/", "Barra"})
		AADD(aCarc_Esp,{"(", "Parentese"})
		AADD(aCarc_Esp,{")", "Parentese"})
		AADD(aCarc_Esp,{"+", "Mais"})
		AADD(aCarc_Esp,{"�", ""})
		AADD(aCarc_Esp,{"=", "Igual"})
		AADD(aCarc_Esp,{"~", "Til"})
		AADD(aCarc_Esp,{"^", "Circunflexo"})
		AADD(aCarc_Esp,{"]", "Chave"})
		AADD(aCarc_Esp,{"[", "Chave"})
		AADD(aCarc_Esp,{"{", "Colchete"})
		AADD(aCarc_Esp,{"}", "Colchete"})
		AADD(aCarc_Esp,{";", "Ponto e Virgula"})
		AADD(aCarc_Esp,{":", "Dois Pontos"})
		AADD(aCarc_Esp,{">", "Maior"})
		AADD(aCarc_Esp,{"<", "Menor"})
		AADD(aCarc_Esp,{"?", "Interrogacao"})
		AADD(aCarc_Esp,{"_", "Underline"})
		AADD(aCarc_Esp,{",", "Virgula"})
		AADD(aCarc_Esp,{" ", "Espaco"})
		AADD(aCarc_Esp,{"'", "Aspas"})
		
		//Verifica se o periodo cont�m algum caracter especial
		For nli:= 1 to Len(aCarc_Esp)
			If At(aCarc_Esp[nlI,1], cPeriodo) <> 0
				AADD(aLogErro,{"C1Z_MESDIS","000419","C1Z", nRecno }) //STR0318 - "O campo possui caracteres especiais"
			EndIf
		Next
		
	EndIf
	
	//Atualizo o Status do Registro
	If Len(aLogErro)>0
		//Utilizo RecLock, pois o SETVALUE somente funciona em campos USADOS
		RecLock("C1Z",.F.)
			C1Z->C1Z_STATUS := "1"
		C1Z->( MsUnlock() )
	Else
		RecLock("C1Z",.F.)
			C1Z->C1Z_STATUS := "0"
		C1Z->( MsUnlock() )
	EndIf
	
Else	
	AADD(aLogErro,{"C1Z_ID","000305", "C1Z", nRecno })//Registros que j� foram transmitidos ao Fisco, n�o podem ser validados
EndIf

//N�o apresento o alert quando utilizo o JOB para validar�
If !lJob
	xValLogEr(aLogErro)
EndIf

Return(aLogErro)
	