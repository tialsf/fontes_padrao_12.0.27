#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TAFA245.CH'

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA245
Tabela de processos - S-1070

@author Leandro Prado
@since 20/08/2013
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAFA245()
Local	oBrw	:= FWmBrowse():New()

oBrw:SetDescription( STR0001 ) //Tabela de processos	
oBrw:SetAlias( 'C93')
oBrw:SetMenuDef( 'TAFA245' )
oBrw:SetFilterDefault( "C93_ATIVO == '1'" ) //Filtro para que apenas os registros ativos sejam exibidos ( 1=Ativo, 2=Inativo )

oBrw:AddLegend( "C93_EVENTO == 'I' ", "GREEN" , STR0006 ) //"Registro Inclu�do"
oBrw:AddLegend( "C93_EVENTO == 'A' ", "YELLOW", STR0007 ) //"Registro Alterado"
oBrw:AddLegend( "C93_EVENTO == 'E' ", "RED"   , STR0008 ) //"Registro Exclu�do"

oBrw:Activate()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Leandro Prado
@since 20/08/2013
@version 1.0

/*/
//-------------------------------------------------------------------                                                                                            
Static Function MenuDef()
Local aFuncao := {}
Local aRotina := {}

Aadd( aFuncao, { "" , "TAF245Xml" , "1" } )
Aadd( aFuncao, { "" , "Taf245Vld" , "2" } )
Aadd( aFuncao, { "" , "xFunHisAlt( 'C93', 'TAFA245' )" , "3" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If lMenuDif
	ADD OPTION aRotina Title STR0009 Action 'VIEWDEF.TAFA245' OPERATION 2 ACCESS 0 //"Visualizar"
Else
	aRotina	:=	xFunMnuTAF( "TAFA245" , , aFuncao)
EndIf

Return( aRotina )                                                                         

//-------------------------------------------------------------------
/*/{Protheus.doc}  ModelDef
Funcao generica MVC do model

@author Leandro Prado
@since 20/08/2013
@version 1.0

/*/
//-------------------------------------------------------------------     
Static Function ModelDef()	
Local oStruC93 := FWFormStruct( 1, 'C93' )// Cria a estrutura a ser usada no Modelo de Dados
Local oModel := MPFormModel():New('TAFA245',,,{|oModel| SaveModel(oModel)} )

lVldModel := Iif( Type( "lVldModel" ) == "U", .F., lVldModel )

If lVldModel
	oStruC93:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel }) 		
EndIf

// Adiciona ao modelo um componente de formul�rio
oModel:AddFields( 'MODEL_C93', /*cOwner*/, oStruC93)
oModel:GetModel( 'MODEL_C93' ):SetPrimaryKey( { 'C93_FILIAL' , 'C93_ID', 'C93_VERSAO' } )

Return oModel             


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@author Leandro Prado
@since 20/08/2013
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oModel		:= FWLoadModel( 'TAFA245' )// objeto de Modelo de dados baseado no ModelDef() do fonte informado
Local oStruC93	:= FWFormStruct( 2, 'C93' )// Cria a estrutura a ser usada na View
Local oView		:= FWFormView():New()

oView:SetModel( oModel )

oView:AddField( 'VIEW_C93', oStruC93, 'MODEL_C93' )

oView:EnableTitleView( 'VIEW_C93',  STR0001 ) //Tabela de processos	

oView:CreateHorizontalBox( 'FIELDSC93', 100 )

oView:SetOwnerView( 'VIEW_C93', 'FIELDSC93' )



lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif
	xFunRmFStr(@oStruC93, 'C93')
EndIf

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@param  oModel -> Modelo de dados
@return .T.

@author Leandro Prado
@since 09/09/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel(oModel)

Local nOperation := oModel:GetOperation()

Begin Transaction 
	
	If nOperation == MODEL_OPERATION_UPDATE 
		TAFAltStat( "C93", " " )
	EndIf  

	FwFormCommit( oModel )
			
End Transaction 

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF245Xml
Funcao de geracao do XML para atender o registro S-1070
Quando a rotina for chamada o registro deve estar posicionado

@Param:

@Return:
cXml - Estrutura do Xml do Layout S-1070

@author Leandro Prado
@since 20/09/2013
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAF245Xml(cAlias,nRecno,nOpc,lJob)

Local cXml    := ""
Local cLayout := "1070"
Local cEvento := ""
Local cReg		:= "TabProcesso"
Local cDtIni  := ""    
Local cDtFin  := ""	  
Local cDtIniAtu  := "" 
Local cDtFinAtu  := "" 
Local cRecno 		:= ""
Local cInfEvt		:= C93->C93_VERSAO
Local cId 	    	:= ""
Local cVerAnt		:= ""

cDtIniAtu  := Iif(!Empty(C93->C93_DTINI),Substr(C93->C93_DTINI,3,4) + "-" + Substr(C93->C93_DTINI,1,2),"") //Fa�o o Iif pois se a data estiver vazia a string recebia '  -  -   '
cDtFinAtu  := Iif(!Empty(C93->C93_DTFIN),Substr(C93->C93_DTFIN,3,4) + "-" + Substr(C93->C93_DTFIN,1,2),"")

If C93->C93_EVENTO $ "I|A"

	If C93->C93_EVENTO == "A"
		cEvento := "alteracao"

		cId := C93->C93_ID 
		cVerAnt := C93->C93_VERANT
		
		BeginSql alias 'C93TEMP'
			SELECT C93.C93_DTINI,C93.C93_DTFIN
			FROM %table:C93% C93
			WHERE C93.C93_FILIAL= %xfilial:C93% AND
			C93.C93_ID = %exp:cId% AND C93.C93_VERSAO = %exp:cVerAnt% AND 
			C93.%notDel%
		EndSql  
		cDtIni := Iif(!Empty(('C93TEMP')->C93_DTINI),Substr(('C93TEMP')->C93_DTINI,3,4) + "-" + Substr(('C93TEMP')->C93_DTINI,1,2),"")
		cDtFIN := Iif(!Empty(('C93TEMP')->C93_DTFIN),Substr(('C93TEMP')->C93_DTFIN,3,4) + "-" + Substr(('C93TEMP')->C93_DTFIN,1,2),"")

		('C93TEMP')->( DbCloseArea() )
	Else
		cEvento := "inclusao"
		cDtIni  := Iif(!Empty(C93->C93_DTINI),Substr(C93->C93_DTINI,3,4) + "-" + Substr(C93->C93_DTINI,1,2),"") //Fa�o o Iif pois se a data estiver vazia a string recebia '  -  -   '
		cDtFin  := Iif(!Empty(C93->C93_DTFIN),Substr(C93->C93_DTFIN,3,4) + "-" + Substr(C93->C93_DTFIN,1,2),"")
	EndIf
	
	cXml +=			"<infoProcesso>"
	cXml +=				"<" + cEvento + ">"
	cXml +=					"<ideProcesso>"
	cXml += 					xTafTag("tpProc",Iif(C93->C93_TPPROC == "A", "1", "2"))
	cXml += 					xTafTag("nrProc",C93->C93_NRPROC)
	cXml +=						xTafTag("iniValid",cDtIni)
	If !Empty(C93->C93_DTFIN)
		cXml +=						xTafTag("fimValid",cDtFin)
	Endif
	cXml +=					"</ideProcesso>"
	cXml +=					"<dadosProcesso>"
	cXml +=						xTafTag("indSusp",POSICIONE("C8S",1, xFilial("C8S")+C93->C93_INDDEC,"C8S_CODIGO"))
	cXml +=						xTafTag("dtDecisao",C93->C93_DTDEC)
	cXml +=						xTafTag("indDeposito",xFunTrcSN(C93->C93_INDDEP,1))
	

	C9A->( DbSetOrder( 1 ) )// Efetua o seek para poder efetuar o posicione para uf e munic�pio.
	If C9A->( DbSeek ( xFilial("C9A")+C93->C93_IDVARA) )
		cXml +=						"<dadosProcJud>"
		cXml +=							xTafTag("ufVara",POSICIONE("C09",3, xFilial("C09")+C9A->C9A_UF,"C09_UF"))
		cXml +=							xTafTag("codMunic",Posicione("C09",3,xFilial("C09")+C9A->C9A_UF,"C09_CODIGO") + POSICIONE("C07",3, xFilial("C07")+C9A->C9A_CODMUN,"C07_CODIGO"))
		cXml +=							xTafTag("idVara",C9A->C9A_CODIGO)
		cXml +=							xTafTag("indAutoria",C93->C93_INDAUT)
		cXml +=						"</dadosProcJud>"	
	Endif
	cXml +=					"</dadosProcesso>"
	
	If C93->C93_EVENTO == "A"
		cXml +=				"<novaValidade>"
		cXml +=					xTafTag("iniValid",cDtIniAtu)
		If !Empty(C93->C93_DTFIN)
			cXml +=					xTafTag("fimValid",cDtFinAtu)
		Endif
		cXml +=				"</novaValidade>"
	EndIf

	cXml +=				"</" + cEvento + ">"
	cXml +=			"</infoProcesso>"

ElseIf C93->C93_EVENTO == "E"
	cXml +=			"<infoProcesso>"
	cXml +=				"<exclusao>"
	cXml += 				xTafTag("tpProc",Iif(C93->C93_TPPROC == "A", "1", "2"))
	cXml += 				xTafTag("nrProc",C93->C93_NRPROC)
	cXml +=					xTafTag("iniValid",cDtIniAtu)
	If !Empty(C93->C93_DTFIN)
		cXml +=					xTafTag("fimValid",cDtFinAtu)
	Endif
	cXml +=				"</exclusao>"
	cXml +=			"</infoProcesso>"
EndIf

//����������������������Ŀ
//�Estrutura do cabecalho�
//������������������������
cXml := xTafCabXml(cXml,"C93", cLayout,cReg, cInfEvt)

//����������������������������Ŀ
//�Executa gravacao do registro�
//������������������������������
If !lJob
	xTafGerXml(cXml,cLayout)
EndIf

Return(cXml)



//-------------------------------------------------------------------
/*/{Protheus.doc} TAF245Grv    
Funcao de gravacao para atender o registro S-1070

@parametros
cLayout - Nome do Layout que esta sendo enviado, existem situacoes onde o mesmo fonte
          alimenta mais de um regsitro do E-Social, para estes casos serao necessarios
          tratamentos de acordo com o layout que esta sendo enviado.
nOpc   -  Opcao a ser realizada ( 3 = Inclusao, 4 = Alteracao, 5 = Exclusao )
cFilEv -  Filial do ERP para onde as informacoes deverao ser importadas
oXML   -  Objeto com as informacoes a serem manutenidas ( Outras Integracoes )  

@Return    
lRet    - Variavel que indica se a importacao foi realizada, ou seja, se as 
		  informacoes foram gravadas no banco de dados
aIncons - Array com as inconsistencias encontradas durante a importacao 

@author Leandro Prado
@since 04/10/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Function TAF245Grv( cLayout, nOpc, cFilEv, oXML )    
       
Local cCmpsNoUpd 	:= "|C93_FILIAL|C93_ID|C93_VERSAO|C93_DTINI|C93_DTFIN|C93_VERANT|C93_PROTPN|C93_EVENTO|C93_STATUS|C93_ATIVO|"
Local cCabec		:= "/eSocial/evtTabProcesso/infoProcesso"
Local cValChv		:= ""
Local cNewDtIni	:= ""
Local cNewDtFin	:= ""
Local nIndex	  	:= 2  // Inicializado com o �ndice completo da Aplica��o
Local nIndIDVer	:= 1
Local nlI       	:= 0  
Local lRet			:= .F. 			
Local aIncons		:= {} 			
Local aRules		:= {} 
Local aChave		:= {}
Local aNewData   := { Nil, Nil }
Local oModel		:= Nil

Private lVldModel := .T. //Caso a chamada seja via integracao seto a variavel de controle de validacao como .T.
Private oDados	:= {}

Default cLayout	:= ""
Default nOpc 		:= 1 			
Default cFilEv 	:= ""			
Default oXML		:= Nil 			

oDados := oXML	

If nOpc == 3
	cTagOper := "/inclusao"	
ElseIf nOpc == 4
	cTagOper := "/alteracao"	
ElseIf nOpc == 5	
	cTagOper := "/exclusao"
EndIf

//Verificar se o tipo de inscricao foi informado para a chave ( Obrigatorio ser informado )
cValChv := FTafGetVal( cCabec + cTagOper + '/ideProcesso/tpProc', 'C', .F., @aIncons, .F., '', '' )
If !Empty( cValChv )
	Aadd( aChave, { "C", "C93_TPPROC", cValChv, .T.} )
	nIndex := 6 //C93_FILIAL+C93_TPPROC+C93_NRPROC+C93_ATIVO
EndIf

//Verificar se o numero de inscricao foi informado para a chave( Obrigatorio ser informado )
cValChv := FTafGetVal( cCabec + cTagOper + '/ideProcesso/nrProc', 'C', .F., @aIncons, .F., '', '' )
If !Empty( cValChv )
	Aadd( aChave, { "C", "C93_NRPROC", cValChv, .T. } )
	nIndex := 6 //C93_FILIAL+C93_TPPROC+C93_NRPROC+C93_ATIVO
EndIf

//Verificar se a data inicial foi informado para a chave( Se nao informado sera adotada a database internamente )
cValChv := FTafGetVal( cCabec + cTagOper + '/ideProcesso/iniValid', 'C', .F., @aIncons, .F., '', '' )
If !Empty( cValChv )
	Aadd( aChave, { "C", "C93_DTINI", cValChv, .T. } )
	nIndex := 7 //C93_FILIAL+C93_TPPROC+C93_NRPROC+C93_DTINI+C93_ATIVO                                                                                                      
EndIf

//Verificar se a data final foi informado para a chave( Se nao informado sera adotado vazio )
cValChv := FTafGetVal( cCabec + cTagOper + '/ideProcesso/fimValid', 'C', .F., @aIncons, .F., '', '' )
If !Empty( cValChv )
	Aadd( aChave, { "C", "C93_DTFIN", cValChv, .T.} )
	nIndex := 2 //C93_FILIAL+C93_TPPROC+C93_NRPROC+C93_DTINI+C93_DTFIN+C93_ATIVO                                                                                      	
EndIf

If nOpc == 4
	If oDados:XPathHasNode( cCabec + cTagOper + "/novaValidade/iniValid", 'C', .F., @aIncons, .F., '', ''  )
		cNewDtIni := FTafGetVal( cCabec + cTagOper + "/novaValidade/iniValid", 'C', .F., @aIncons, .F., '', '' )
		aAdd( aNewData, cNewDtIni )
	EndIf
	
	If oDados:XPathHasNode( cCabec + cTagOper + "/novaValidade/fimValid", 'C', .F., @aIncons, .F., '', ''  )
		cNewDtFin := FTafGetVal( cCabec + cTagOper + "/novaValidade/fimValid", 'C', .F., @aIncons, .F., '', '' )
		aAdd( aNewData, cNewDtFin )
	EndIf
EndIf

Begin Transaction	
	//Funcao para validar se a operacao desejada pode ser realizada
	If FTafVldOpe( "C93", nIndex, @nOpc, cFilEv, @aIncons, aChave, @oModel, "TAFA245", cCmpsNoUpd, nIndIDVer, .T., aNewData )
	   	      				     		    	      	    		    		    		    		    					
		//����������������������������������������������������������������Ŀ
		//�Quando se tratar de uma Exclusao direta apenas preciso realizar �
		//�o Commit(), nao eh necessaria nenhuma manutencao nas informacoes�
		//������������������������������������������������������������������
		If nOpc <> 5
			
			//���������������������������������������������������������������Ŀ
			//�Carrego array com os campos De/Para de gravacao das informacoes�
			//�����������������������������������������������������������������
			aRules := TAF245Rul( cTagOper )								                
				 
		   	oModel:LoadValue( "MODEL_C93", "C93_FILIAL", C93->C93_FILIAL )															
	            			
			//����������������������������������������Ŀ
			//�Rodo o aRules para gravar as informacoes�
			//������������������������������������������
			For nlI := 1 To Len( aRules )                 					
			 	oModel:LoadValue( "MODEL_C93", aRules[ nlI, 01 ], FTafGetVal( aRules[ nlI, 02 ], aRules[nlI, 03], aRules[nlI, 04], @aIncons, .F., ,aRules[ nlI, 01 ] ) )
			Next			        							
		EndIf
			
		//���������������������������Ŀ
		//�Efetiva a operacao desejada�
		//�����������������������������
		FWFormCommit( oModel )	 
		lRet := .T.							 			   
	
		oModel:DeActivate()		         			    								 				
	EndIf                                                                           	
End Transaction  	

//����������������������������������������������������������Ŀ
//�Zerando os arrays e os Objetos utilizados no processamento�
//������������������������������������������������������������
aSize( aRules, 0 ) 
aRules     := Nil

aSize( aChave, 0 ) 
aChave     := Nil    

oModel     := Nil


Return { lRet, aIncons } 

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF245Rul           

Regras para gravacao das informacoes do registro S-1070 do E-Social

@Param
nOper      - Operacao a ser realizada ( 3 = Inclusao / 4 = Alteracao / 5 = Exclusao )

@Return	
aRull  - Regras para a gravacao das informacoes


@author Leandro Prado
@since 04/10/2013
@version 1.0

/*/                        	
//-------------------------------------------------------------------
Static Function TAF245Rul( cTagOper )
Local cIdUF := ""
Local cIdMun := ""
Local aInfComp := {}
Local aRull    := {}

Aadd( aRull, { "C93_TPPROC", "/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/ideProcesso/tpProc", "C", .F. } )
Aadd( aRull, { "C93_NRPROC", "/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/ideProcesso/nrProc", "C", .F. } )
Aadd( aRull, { "C93_INDDEC", FGetIdInt( "indSusp", "", "/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/indSusp"), "C", .T. } )
Aadd( aRull, { "C93_DTDEC" , "/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/dtDecisao", "D", .F. } )
Aadd( aRull, { "C93_INDDEP", xFunTrcSN( TAFExisTag( "/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/indDeposito" ),2), "C", .T. } )

cIdUF := FGetIdInt( "uf", "", "/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/dadosProcJud/ufVara")
Aadd( aInfComp,{'C9A_UF',cIdUF})

cIdMun := FGetIdInt( "uf", "codMunic",;
				"/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/dadosProcJud/ufVara", +;
				"/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/dadosProcJud/codMunic")
Aadd( aInfComp,{'C9A_CODMUN',cIdMun})

If !Empty(cIdMun) .And. !("ERR" $ cIdMun)
	Aadd( aRull, { "C93_IDVARA", +;
				FGetIdInt( "idVara", "codMunic",+;
				"/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/dadosProcJud/idVara",;
				"/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/dadosProcJud/codMunic";
				,,aInfComp), "C", .T. } )
Else
	Aadd( aRull, { "C93_IDVARA", +;
				FGetIdInt( "idVara", "ufVara",+;
				"/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/dadosProcJud/idVara",;
				"/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/dadosProcJud/ufVara";
				,,aInfComp), "C", .T. } )
EndIf

Aadd( aRull, { "C93_INDAUT", "/eSocial/evtTabProcesso/infoProcesso" + cTagOper + "/dadosProcesso/dadosProcJud/indAutoria", "C", .F. } )

Return( aRull )

//-------------------------------------------------------------------
/*/{Protheus.doc} Taf245Vld

Funcao que valida os dados do registro posicionado,
verificando se ha incoerencias nas informac�es caso seja necessario gerar um XML

lJob - Informa se foi chamado por Job

@return .T.

@author Leandro Prado
@since 07/09/2013
@version 1.0
/*/                                                                                                                                          
//-------------------------------------------------------------------
Function Taf245Vld(cAlias,nRecno,nOpc,lJob)

Local aCampos		:= {} //{Campo do registro filho/consulta padrao , opcao de validacao , opcao do combo(opcional)}
Local aLogErro	:= {} 
Local cChave		:= ""
Local aDadosUtil	:= {} 
Local cIndSusp   := ""

Default lJob		:= .F. 

//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := C93->( Recno() )
                 
If (C93->C93_STATUS $ (' 1'))                       
	//Valida��es das regras gen�ricas:
	If C93->C93_EVENTO == "E"
		xVldRegra("C93","REGRA_TABGERAL_EXISTE_REGISTRO_EXCLUIDO",@aLogErro,,"C93")
	ElseIF C93->C93_EVENTO == "A"  
		xVldRegra("C93","REGRA_TABGERAL_EXISTE_REGISTRO_ALTERADO",@aLogErro,,"C93")
		xVldRegra("C93","REGRA_TABGERAL_ALTERACAO_PERIODO_CONFLITANTE",@aLogErro,,"C93")
	ElseIF C93->C93_EVENTO == "I"
		xVldRegra("C93","REGRA_TABGERAL_INCLUSAO_PERIODO_CONFLITANTE",@aLogErro,,"C93")     
	EndIf     
	 
    aAdd(aDadosUtil, {xFunDtPer(C93->C93_DTINI), xFunDtPer(C93->C93_DTFIN,.T.), C93->C93_FILIAL}) 
    xVldRegra("C1E","REGRA_EXISTE_INFO_EMPREGADOR",@aLogErro, aDadosUtil, "C93") 
	
	If Empty(C93->C93_DTINI)                                           
		//Data obrigatoria para a geracao do XML
		AADD(aLogErro,{"C93_DTINI","000003","C93", nRecno }) 	//STR0003 - "Verifique a data Inicial do Registro" 
	ElseIf !xVldFormDt(C93->C93_DTINI, "MMAAAA")
		aAdd( aLogErro, { "C93_DTINI", "000432", "C93", nRecno } ) //000432 - O valor do campo difere do formato esperado.
	EndIf
	
	//Valido o campo do tipo DATA
	If !Empty(C93->C93_DTFIN) 
		If (Len(C93->C93_DTFIN) < 6) .Or. (xFunDtPer(C93->C93_DTFIN,.T.) < xFunDtPer(C93->C93_DTINI))
			AADD(aLogErro,{"C93_DTFIN","000004", "C93", nRecno }) //STR0004 - "Verifique a data Final do Registro"    
		EndIf
		If !xVldFormDt(C93->C93_DTFIN, "MMAAAA")
			aAdd( aLogErro, { "C93_DTFIN", "000432", "C93", nRecno } ) //000432 - O valor do campo difere do formato esperado.
		EndIf
	EndIf   
	cIndSusp:= Posicione("C8S",1,xFilial("C8S") + C93->C93_INDDEC,"C8S_CODIGO")
	If Empty(C93->C93_DTDEC) .AND. cIndSusp $ ("02|05|14|92")
		AADD(aLogErro,{"C93_DTDEC","000010", "C93", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"   
	EndIf    

	//Valido se campos est�o preenchidos
	If Empty(C93->C93_TPPROC) 
		AADD(aLogErro,{"C93_TPPROC","000010", "C93", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"    
	EndIf    
	
	If Empty(C93->C93_NRPROC) 
		AADD(aLogErro,{"C93_NRPROC","000010", "C93", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"    
	EndIf    	
	
	//Valido as op�oes dos campos tipo COMBO
	If !(C93->C93_INDDEP $ "12")
		AADD(aLogErro,{"C93_INDDEP","000006", "C93",nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"   
	EndIf
	If !(C93->C93_INDAUT $ "12")
		AADD(aLogErro,{"C93_INDAUT","000006", "C93",nRecno}) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"   
	EndIf
	               
	//{Campo do registro filho/consulta padrao , opcao de validacao , opcao do combo(opcional)}
	aCampos  := {} 
	AADD(aCampos,{"C8S_CODIGO",3})	//Valida CAMPO VAZIO
	
	//Chave de busca na tabela FILHO ou Consulta padrao
	cChave := C93->C93_INDDEC
	xValRegTab("C8S",cChave,1,aCampos,@aLogErro, xFunDtPer(C93->C93_DTINI), { "C93", "C93_INDDEC", nRecno })
	               
	//{Campo do registro filho/consulta padrao , opcao de validacao , opcao do combo(opcional)}
	aCampos  := {} 
	AADD(aCampos,{"C9A_CODIGO",3})	//Valida CAMPO VAZIO
	
	If !Empty(C93->C93_IDVARA)
		//Chave de busca na tabela FILHO ou Consulta padrao
		cChave := C93->C93_IDVARA
		xValRegTab("C9A",cChave,1,aCampos,@aLogErro, xFunDtPer(C93->C93_DTINI), { "C93", "C93_IDVARA", nRecno } )
	EndIf	
	               
	//ATUALIZO O STATUS DO REGISTRO
	If Len(aLogErro)>0	
		//Utilizo RecLock, pois o SETVALUE somente funciona em campos USADOS
		RecLock("C93",.F.)
	    	C93->C93_STATUS := "1" 
	    C93->( MsUnlock() )    
	Else
		RecLock("C93",.F.)
	    	C93->C93_STATUS := "0" 
	    C93->( MsUnlock() )        
	EndIf
Else
	AADD(aLogErro,{"C93_ID","000305", "C93", nRecno })   
EndIf	

//N�o apresento o alert quando utilizo o JOB para validar
If !lJob
	xValLogEr(aLogErro)
EndIf	

Return(aLogErro)
