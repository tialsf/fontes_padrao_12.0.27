#INCLUDE "PROTHEUS.CH" 
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TAFA250.CH"

#DEFINE CRLF Chr(13)+Chr(10)

#DEFINE ANALITICO_MATRICULA				1
#DEFINE ANALITICO_CATEGORIA				2
#DEFINE ANALITICO_TIPO_ESTABELECIMENTO	3
#DEFINE ANALITICO_ESTABELECIMENTO		4
#DEFINE ANALITICO_LOTACAO				5
#DEFINE ANALITICO_NATUREZA				6
#DEFINE ANALITICO_TIPO_RUBRICA			7
#DEFINE ANALITICO_INCIDENCIA_CP			8
#DEFINE ANALITICO_INCIDENCIA_IRRF		9
#DEFINE ANALITICO_INCIDENCIA_FGTS		10
#DEFINE ANALITICO_DECIMO_TERCEIRO		11
#DEFINE ANALITICO_TIPO_VALOR			12
#DEFINE ANALITICO_VALOR					13

Static slRubERPPad	:=	Nil
Static __cInFiliais	:=	Nil
Static __cFilCache	:=	"" 
Static oReport		:=	Nil
Static __lGrvRPT	:=	Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA250
Cadastro de Folha de Pagamento

@author Vitor Siqueira
@since 08/01/2016
@version 1.0

/*/

//------------------------------------------------------------------
Function TAFA250()

Local aOnlyFields := {'C91_FILIAL','C91_INDAPU','C91_PERAPU', 'C91_CPFV', 'C91_NISV', 'C91_NOMEV', 'C91_MATV', 'C91_OWNER', 'C91_TAFKEY' }

Private oBrw := FWmBrowse():New()

If FindFunction("FilCpfNome") .And. GetSx3Cache("C91_CPFV","X3_CONTEXT") == "V" .AND. !FwIsInCallStack("TAFPNFUNC") .AND. !FwIsInCallStack("TAFMONTES")
	TafNewBrowse( "S-1200",,,, STR0001, aOnlyFields, 2, 2 )
Else
	// Fun��o que indica se o ambiente � v�lido para o eSocial 2.3
	If TafAtualizado() .And. TAF250DicMV(.T.)

		oBrw:SetDescription(STR0001)    //"Cadastro de Folha de Pagamento"
		oBrw:SetAlias( 'C91')
		//oBrw:SetMenuDef( 'TAFA250' )
		If FindFunction('TAFSetFilter')
			oBrw:SetFilterDefault(TAFBrwSetFilter("C91","TAFA250","S-1200"))
		Else
			oBrw:SetFilterDefault( "C91_ATIVO == '1' .and. C91_NOMEVE == 'S1200'" ) //Filtro para que apenas os registros ativos sejam exibidos ( 1=Ativo, 2=Inativo )
		EndIf
		oBrw:SetOnlyFields( { 'C91_FILIAL','C91_TRABAL','C91_INDAPU','C91_PERAPU', 'C91_CPF' } )

		TafLegend(2,"C91",@oBrw)

		oBrw:Activate()

	EndIf

EndIf

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Vitor Siqueira
@since 08/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao := {}
Local aRotina := {}

If FindFunction("FilCpfNome") .And. GetSx3Cache("C91_CPFV","X3_CONTEXT") == "V" .AND. !FwIsInCallStack("TAFPNFUNC") .AND. !FwIsInCallStack("TAFMONTES")

	ADD OPTION aRotina TITLE "Visualizar" ACTION "TAF250View('C91',RECNO())" OPERATION 2 ACCESS 0 //'Visualizar'
	ADD OPTION aRotina TITLE "Incluir"    ACTION "TAF250Inc('C91',RECNO())"  OPERATION 3 ACCESS 0 //'Incluir'
	ADD OPTION aRotina TITLE "Alterar"    ACTION "xTafAlt('C91', 0 , 0)"     OPERATION 4 ACCESS 0 //'Alterar'
	ADD OPTION aRotina TITLE "Imprimir"	  ACTION "VIEWDEF.TAFA250"			 OPERATION 8 ACCESS 0 //'Imprimir'

Else

	Aadd( aFuncao, { "" , "TAF250Xml" , "1" } )
	Aadd( aFuncao, { "" , "TAF250Vld" , "2" } )
	Aadd( aFuncao, { "" , "xFunAltRec( 'C91' )" , "10" } )

	//Chamo a Browse do Hist�rico
	If FindFunction( "xNewHisAlt" )
		Aadd( aFuncao, { "" , "xNewHisAlt( 'C91', 'TAFA250' )" , "3" } )
	Else
		Aadd( aFuncao, { "" , "xFunHisAlt( 'C91', 'TAFA250' )" , "3" } )
	EndIf

	Aadd( aFuncao, { "" , "StaticCall(TAFA250,PreXmlLote)" , "5" } )

	lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

	If lMenuDif
		ADD OPTION aRotina Title "Visualizar" Action 'VIEWDEF.TAFA250' OPERATION 2 ACCESS 0

		// Menu dos extempor�neos
		If FindFunction( "xNewHisAlt" ) .AND. FindFunction( "xTafExtmp" ) .And. xTafExtmp()
			aRotina	:= xMnuExtmp( "TAFA250", "C91" )
		EndIf

	Else
		aRotina	:=	xFunMnuTAF( "TAFA250" , , aFuncao)
	EndIf

EndIf

Return( aRotina )

//---------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Fun��o que chama a TAFXmlLote e limpa slRubERPPad

@author brunno.costa
@since 01/10/2018
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function PreXmlLote()
	TAFXmlLote( 'C91', 'S-1200' , 'evtRemun' , 'TAF250Xml', ,oBrw )
	slRubERPPad := Nil	//Limpa vari�vel no final do processo em lote
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC

@author Vitor Siqueira
@since 08/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

Local oStruC91  :=  FWFormStruct( 1, 'C91' )
Local oStruT6W  :=  FWFormStruct( 1, 'T6W' )
Local oStruC9K  :=  FWFormStruct( 1, 'C9K' )
Local oStruC9L  :=  FWFormStruct( 1, 'C9L' )
Local oStruC9M  :=  FWFormStruct( 1, 'C9M' )
Local oStruC9N  :=  FWFormStruct( 1, 'C9N' )
Local oStruC9O  :=  FWFormStruct( 1, 'C9O' )
Local oStruC9P  :=  FWFormStruct( 1, 'C9P' )
Local oStruC9Q  :=  FWFormStruct( 1, 'C9Q' )
Local oStruC9R  :=  FWFormStruct( 1, 'C9R' )
Local oStruCRN  :=  FWFormStruct( 1, 'CRN' )
Local oStruT14  :=  FWFormStruct( 1, 'T14' )
Local oStruT6Y  :=  FWFormStruct( 1, 'T6Y' )
Local oStruT6Z  :=  FWFormStruct( 1, 'T6Z' )
Local oStruT89
Local oStruV1B
Local oStruV1C
Local oModel    :=  MPFormModel():New( 'TAFA250' ,,,{|oModel| SaveModel(oModel)})
Local aTrigger  := TAF250Trigger()


oStruC91:AddTrigger( aTrigger[1],;
					 aTrigger[2],;
					 aTrigger[3],;
					 aTrigger[4] )
					 

if TafLayESoc("02_04_02")
	oStruV1B := FWFormStruct( 1, 'V1B' )
	oStruV1C := FWFormStruct( 1, 'V1C' )
else
	oStruT89 := FWFormStruct( 1, 'T89' )
endif

lVldModel := Iif( Type( "lVldModel" ) == "U", .F., lVldModel )

If lVldModel
	oStruC91:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruT6W:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruC9K:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruC9L:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruC9M:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruC9N:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruC9O:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruC9P:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruC9Q:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruC9R:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruCRN:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruT14:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruT6Y:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruT6Z:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })

	if TafLayESoc("02_04_02")
		oStruV1B:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
		oStruV1C:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	endif
EndIf

//A Obrigatoriedade est� sendo colocada no fonte por que a tabela T14
//� compartilhada com outros eventos e o campo de categoria � obrigat�rio
//neste evento e em outros n�o.
oStruT14:SetProperty( 'T14_CODCAT'   , MODEL_FIELD_OBRIGAT , .T.  )

oModel:AddFields('MODEL_C91', /*cOwner*/, oStruC91)
oModel:GetModel('MODEL_C91'):SetPrimaryKey({'C91_TRABAL', 'C91_PERAPU', 'C91_NOMEVE'})

//Remunera��o do Trab. de V�nculo com Outras Empresas
oModel:AddGrid('MODEL_T6W', 'MODEL_C91', oStruT6W)
oModel:GetModel('MODEL_T6W'):SetOptional(.T.)
oModel:GetModel('MODEL_T6W'):SetUniqueLine({'T6W_TPINSC', 'T6W_NRINSC'})

If TafLayESoc("02_05_00")
	oModel:GetModel('MODEL_T6W'):SetMaxLine(999)
Else
	oModel:GetModel('MODEL_T6W'):SetMaxLine(10)
EndIf

oModel:SetRelation('MODEL_T6W', {{'T6W_FILIAL' , 'xFilial( "T6W" )'}, {'T6W_ID' , 'C91_ID'}, {'T6W_VERSAO' , 'C91_VERSAO'}}, T6W->(IndexKey(1)))

//Informa��es de Processos Judiciais de Remunera��o
oModel:AddGrid( "MODEL_CRN", "MODEL_C91", oStruCRN )
oModel:GetModel( "MODEL_CRN" ):SetOptional( .T. )
oModel:GetModel( "MODEL_CRN" ):SetUniqueLine( { "CRN_TPTRIB", "CRN_IDPROC", "CRN_CODSUS" } )
oModel:GetModel( "MODEL_CRN" ):SetMaxLine( 99 )
oModel:SetRelation( "MODEL_CRN", { { "CRN_FILIAL", 'xFilial( "CRN" )' }, { "CRN_ID", "C91_ID" }, { "CRN_VERSAO", "C91_VERSAO" } }, CRN->( IndexKey( 1 ) ) )

// IDENTIFICA��O RECIBO DE PAGAMENTO
oModel:AddGrid('MODEL_T14', 'MODEL_C91', oStruT14)
oModel:GetModel('MODEL_T14'):SetOptional(.T.)
oModel:GetModel('MODEL_T14'):SetUniqueLine({'T14_IDEDMD'})
oModel:GetModel('MODEL_T14'):SetMaxLine(999)

//INFORMA��ES DO ESTABELECIMENTO/LOTA��O
oModel:AddGrid( "MODEL_C9K", "MODEL_T14", oStruC9K )
oModel:GetModel( "MODEL_C9K" ):SetOptional( .T. )
oModel:GetModel( "MODEL_C9K" ):SetUniqueLine( { "C9K_ESTABE", "C9K_LOTACA", "C9K_CODLOT", "C9K_TPINSC", "C9K_NRINSC" } )
oModel:GetModel( "MODEL_C9K" ):SetMaxLine( 500 )
oModel:GetModel( 'MODEL_C9K' ):SetLPre({|| PreVldLine()})

//INFORMA��ES DO REMUNERA��O DO PERIODO DE APURA��O
oModel:AddGrid('MODEL_C9L', 'MODEL_C9K', oStruC9L)
oModel:GetModel('MODEL_C9L'):SetOptional(.T.)
oModel:GetModel('MODEL_C9L'):SetUniqueLine( { 'C9L_TRABAL', 'C9L_DTRABA' } )
oModel:GetModel('MODEL_C9L'):SetMaxLine(8)

//INFORMA��ES DOS ITENS DE REMUNERA��O
oModel:AddGrid('MODEL_C9M', 'MODEL_C9L', oStruC9M)
oModel:GetModel('MODEL_C9M'):SetOptional(.T.)
oModel:GetModel('MODEL_C9M'):SetUniqueLine( {'C9M_CODRUB', 'C9M_RUBRIC', 'C9M_IDTABR' } )
oModel:GetModel('MODEL_C9M'):SetMaxLine(200)
oModel:GetModel('MODEL_C9M'):SetPost({|oSubModel| ModelOk(oSubModel)},.F.,.T.)

//INFORMA��ES DO PLANO DE SA�DE
oModel:AddGrid( 'MODEL_T6Y', "MODEL_C9L", oStruT6Y )
oModel:GetModel('MODEL_T6Y'):SetOptional( .T. )
oModel:GetModel('MODEL_T6Y'):SetUniqueLine( { "T6Y_CNPJOP", "T6Y_REGANS" } )
oModel:GetModel('MODEL_T6Y'):SetMaxLine( 99 )
oModel:GetModel('MODEL_T6Y'):SetPost({|oSubModel| ModelOk(oSubModel)},.F.,.T.)

//INFORMA��ES DO PLANO DE SA�DE - Det. Operadora Plano de Sa�de
oModel:AddGrid( "MODEL_T6Z", "MODEL_T6Y", oStruT6Z )
oModel:GetModel( "MODEL_T6Z" ):SetOptional( .T. )
oModel:GetModel( "MODEL_T6Z" ):SetUniqueLine( { "T6Z_DTNDEP", "T6Z_NOMDEP", "T6Z_TPDEP" } )
oModel:GetModel( "MODEL_T6Z" ):SetMaxLine( 99 )

// INFORMA��ES DE ACORDO
oModel:AddGrid('MODEL_C9N', 'MODEL_T14', oStruC9N)
oModel:GetModel('MODEL_C9N'):SetOptional(.T.)
if TafLayESoc("02_04_02")
	oModel:GetModel('MODEL_C9N'):SetUniqueLine({'C9N_DTACOR', 'C9N_TPACOR','C9N_COMPAC','C9N_DTEFAC'})
else
	oModel:GetModel('MODEL_C9N'):SetUniqueLine({'C9N_DTACOR', 'C9N_TPACOR','C9N_COMPAC'})
endif
oModel:GetModel('MODEL_C9N'):SetMaxLine(8)

// Remo��o das obrigatoriedades dos m�ltiplos v�nculos
oStruC9K:SetProperty( 'C9K_ESTABE', MODEL_FIELD_OBRIGAT , .F. )
oStruC9K:SetProperty( 'C9K_LOTACA', MODEL_FIELD_OBRIGAT , .F. )
oStruC9L:SetProperty( 'C9L_TRABAL', MODEL_FIELD_OBRIGAT , .F. )
oStruC9M:SetProperty( 'C9M_CODRUB', MODEL_FIELD_OBRIGAT , .F. )
oStruC9P:SetProperty( 'C9P_LOTACA', MODEL_FIELD_OBRIGAT , .F. )
oStruC9P:SetProperty( 'C9P_ESTABE', MODEL_FIELD_OBRIGAT , .F. )
oStruC9Q:SetProperty( 'C9Q_TRABAL', MODEL_FIELD_OBRIGAT , .F. )
oStruC9R:SetProperty( 'C9R_CODRUB', MODEL_FIELD_OBRIGAT , .F. )


// INFORMA��ES DO PERIODO
oModel:AddGrid('MODEL_C9O', 'MODEL_C9N', oStruC9O)
oModel:GetModel('MODEL_C9O'):SetOptional(.T.)
oModel:GetModel('MODEL_C9O'):SetUniqueLine({'C9O_PERREF'})
oModel:GetModel('MODEL_C9O'):SetMaxLine(180)

//INFORMA��ES DO ESTABELECIMENTO/LOTA��O
oModel:AddGrid( "MODEL_C9P", "MODEL_C9O", oStruC9P )
oModel:GetModel( "MODEL_C9P" ):SetOptional( .T. )
oModel:GetModel( "MODEL_C9P" ):SetUniqueLine( { "C9P_ESTABE", "C9P_LOTACA", "C9P_CODLOT", "C9P_TPINSC", "C9P_NRINSC" } )
oModel:GetModel( "MODEL_C9P" ):SetMaxLine( 500 )

// INFORMA��ES DA REMUNERA��O DO TRABALHADOR
oModel:AddGrid('MODEL_C9Q', 'MODEL_C9P', oStruC9Q)
oModel:GetModel('MODEL_C9Q'):SetOptional(.T.)
oModel:GetModel('MODEL_C9Q'):SetUniqueLine( {'C9Q_TRABAL', "C9Q_DTRABA" } )
oModel:GetModel('MODEL_C9Q'):SetMaxLine(8)

// INFORMA��ES DOS ITENS DA REMUNERA��O
oModel:AddGrid('MODEL_C9R', 'MODEL_C9Q', oStruC9R)
oModel:GetModel('MODEL_C9R'):SetOptional(.T.)
oModel:GetModel('MODEL_C9R'):SetUniqueLine( { 'C9R_CODRUB', 'C9R_RUBRIC', 'C9R_IDTABR' } )
oModel:GetModel('MODEL_C9R'):SetMaxLine(200)


// RELATIONS DO PERIODO DE APURA��O ANTERIOR
oModel:SetRelation('MODEL_C9N', {{'C9N_FILIAL' , 'xFilial( "C9N" )'}, {'C9N_ID' , 'C91_ID'}, {'C9N_VERSAO' , 'C91_VERSAO'}, {'C9N_RECIBO' , 'T14_IDEDMD'}}, C9N->(IndexKey(1)))
oModel:SetRelation('MODEL_C9O', {{'C9O_FILIAL' , 'xFilial( "C9O" )'}, {'C9O_ID' , 'C91_ID'}, {'C9O_VERSAO' , 'C91_VERSAO'}, {'C9O_RECIBO' , 'T14_IDEDMD'}, {'C9O_DTACOR' , 'C9N_DTACOR'}, {'C9O_TPACOR' , 'C9N_TPACOR'}, {'C9O_COMPAC' , 'C9N_COMPAC'}}, C9O->(IndexKey(1)))
oModel:SetRelation('MODEL_C9P', {{'C9P_FILIAL' , 'xFilial( "C9P" )'}, {'C9P_ID' , 'C91_ID'}, {'C9P_VERSAO' , 'C91_VERSAO'}, {'C9P_RECIBO' , 'T14_IDEDMD'}, {'C9P_DTACOR' , 'C9N_DTACOR'}, {'C9P_TPACOR' , 'C9N_TPACOR'}, {'C9P_COMPAC' , 'C9N_COMPAC'}, {'C9P_PERREF' , 'C9O_PERREF'}}, C9P->(IndexKey(1)))
If (TAFColumnPos("C9Q_COMPAC"))
	oModel:SetRelation('MODEL_C9Q', {{'C9Q_FILIAL' , 'xFilial( "C9Q" )'}, {'C9Q_ID' , 'C91_ID'}, {'C9Q_VERSAO' , 'C91_VERSAO'}, {'C9Q_RECIBO' , 'T14_IDEDMD'}, {'C9Q_DTACOR' , 'C9N_DTACOR'}, {'C9Q_TPACOR' , 'C9N_TPACOR'}, {'C9Q_COMPAC' , 'C9N_COMPAC'}, {'C9Q_PERREF' , 'C9O_PERREF'}, {'C9Q_ESTABE' , 'C9P_ESTABE'}, {'C9Q_LOTACA' , 'C9P_LOTACA'}, { "C9Q_CODLOT", "C9P_CODLOT" }, { "C9Q_TPINSC", "C9P_TPINSC" }, { "C9Q_NRINSC", "C9P_NRINSC" } }, C9Q->(IndexKey(3)))
Else
	oModel:SetRelation('MODEL_C9Q', {{'C9Q_FILIAL' , 'xFilial( "C9Q" )'}, {'C9Q_ID' , 'C91_ID'}, {'C9Q_VERSAO' , 'C91_VERSAO'}, {'C9Q_RECIBO' , 'T14_IDEDMD'}, {'C9Q_DTACOR' , 'C9N_DTACOR'}, {'C9Q_TPACOR' , 'C9N_TPACOR'}, {'C9Q_PERREF' , 'C9O_PERREF'}, {'C9Q_ESTABE' , 'C9P_ESTABE'}, {'C9Q_LOTACA' , 'C9P_LOTACA'}, { "C9Q_CODLOT", "C9P_CODLOT" }, { "C9Q_TPINSC", "C9P_TPINSC" }, { "C9Q_NRINSC", "C9P_NRINSC" } }, C9Q->(IndexKey(3)))
Endif

oModel:SetRelation('MODEL_C9R', { {'C9R_FILIAL' , 'xFilial( "C9R" )'}, {'C9R_ID' , 'C91_ID'}, {'C9R_VERSAO' , 'C91_VERSAO'}, {'C9R_RECIBO' , 'T14_IDEDMD'},;
								  {'C9R_DTACOR' , 'C9N_DTACOR'}, {'C9R_TPACOR' , 'C9N_TPACOR'}, {'C9R_COMPAC' , 'C9N_COMPAC'}, {'C9R_PERREF' , 'C9O_PERREF'}, {'C9R_ESTABE' , 'C9P_ESTABE'},;
								  {'C9R_LOTACA' , 'C9P_LOTACA'}, {'C9R_TRABAL' , 'C9Q_TRABAL'}, { "C9R_MATRIC", "C9Q_DTRABA" }, { "C9R_CODLOT", "C9P_CODLOT" },;
								  {"C9R_TPINSC" ,"C9P_TPINSC"}, {"C9R_NRINSC", "C9P_NRINSC"}  }, C9R->(IndexKey(1)))

// RELATIONS DO PERIODO DE APURA��O
oModel:SetRelation('MODEL_T14', {{'T14_FILIAL' , 'xFilial( "T14" )'}, {'T14_ID' , 'C91_ID'}, {'T14_VERSAO' , 'C91_VERSAO'}}, T14->(IndexKey(1)))
oModel:SetRelation('MODEL_C9K', {{'C9K_FILIAL' , 'xFilial( "C9K" )'}, {'C9K_ID' , 'C91_ID'}, {'C9K_VERSAO' , 'C91_VERSAO'}, {'C9K_RECIBO' , 'T14_IDEDMD'}}, C9K->(IndexKey(1)))
oModel:SetRelation('MODEL_C9L', {{'C9L_FILIAL' , 'xFilial( "C9L" )'}, {'C9L_ID' , 'C91_ID'}, {'C9L_VERSAO' , 'C91_VERSAO'}, {'C9L_RECIBO' , 'T14_IDEDMD'} ,{'C9L_ESTABE' , 'C9K_ESTABE'}, {'C9L_LOTACA' , 'C9K_LOTACA'}, {'C9L_CODLOT','C9K_CODLOT'}, {'C9L_TPINSC','C9K_TPINSC'}, {'C9L_NRINSC','C9K_NRINSC'}  }, C9L->(IndexKey(1)))
oModel:SetRelation('MODEL_C9M', {{'C9M_FILIAL' , 'xFilial( "C9M" )'}, {'C9M_ID' , 'C91_ID'}, {'C9M_VERSAO' , 'C91_VERSAO'}, {'C9M_RECIBO' , 'T14_IDEDMD'}, {'C9M_ESTABE' , 'C9K_ESTABE'}, {'C9M_LOTACA' , 'C9K_LOTACA'}, {'C9M_TRABAL' , 'C9L_TRABAL'}, {'C9M_CODLOT','C9K_CODLOT'}, {'C9M_TPINSC','C9K_TPINSC'}, {'C9M_NRINSC','C9K_NRINSC'}, {'C9M_DTRABA', 'C9L_DTRABA'} }, C9M->(IndexKey(3)))

//T6Y_FILIAL+T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_ESTABE+T6Y_LOTACA+T6Y_TRABAL+T6sY_CNPJ+T6Y_REGANS
oModel:SetRelation('MODEL_T6Y', {{'T6Y_FILIAL' , 'xFilial( "T6Y" )'},{'T6Y_ID' , 'C91_ID'}, {'T6Y_VERSAO' , 'C91_VERSAO'}, {'T6Y_RECIBO' , 'T14_IDEDMD'}, {'T6Y_ESTABE' , 'C9K_ESTABE'}, {'T6Y_LOTACA' , 'C9K_LOTACA'}, {'T6Y_TRABAL' , 'C9L_TRABAL'}, {'T6Y_CODLOT', 'C9K_CODLOT'}, {'T6Y_DTRABA', 'C9L_DTRABA'}, {'T6Y_TPINSC' , 'C9K_TPINSC'}, {'T6Y_NRINSC' , 'C9K_NRINSC'} }, T6Y->(IndexKey(1)))
//T6Z_FILIAL+T6Z_ID+T6Z_VERSAO+T6Z_RECIBO+T6Z_ESTABE+T6Z_LOTACA+T6Z_TRABAL+T6Z_CNPJ+T6Z_REGANS+T6Z_SEQUEN
oModel:SetRelation('MODEL_T6Z', {{'T6Z_FILIAL' , 'xFilial( "T6Z" )'},{'T6Z_ID' , 'C91_ID'}, {'T6Z_VERSAO' , 'C91_VERSAO'}, {'T6Z_RECIBO' , 'T14_IDEDMD'}, {'T6Z_ESTABE' , 'C9K_ESTABE'}, {'T6Z_LOTACA' , 'C9K_LOTACA'}, {'T6Z_TRABAL' , 'C9L_TRABAL'}, {'T6Z_CNPJOP' , 'T6Y_CNPJOP'}, {'T6Z_REGANS' , 'T6Y_REGANS'}, {'T6Z_CODLOT', 'C9K_CODLOT'}, {'T6Z_DTRABA', 'C9L_DTRABA'}, {'T6Z_TPINSC' , 'C9K_TPINSC'}, {'T6Z_NRINSC' , 'C9K_NRINSC'} }, T6Z->(IndexKey(1)))

if TafLayESoc("02_04_02")
	//INFORMA��ES DE CONVOCA��O DE TRABALHO INTERMITENTE PERIODO DE APURA��O
	oModel:AddGrid('MODEL_V1B', 'MODEL_C9L', oStruV1B)
	oModel:GetModel('MODEL_V1B'):SetOptional(.T.)
	oModel:GetModel('MODEL_V1B'):SetUniqueLine({'V1B_IDCONV'})
	oModel:GetModel('MODEL_V1B'):SetMaxLine(99)

	oModel:SetRelation('MODEL_V1B', {{'V1B_FILIAL' , 'xFilial( "V1B" )'}, {'V1B_ID' , 'C91_ID'}, {'V1B_VERSAO' , 'C91_VERSAO'}, {'V1B_RECIBO' , 'T14_IDEDMD'}, {'V1B_ESTABE' , 'C9K_ESTABE'}, {'V1B_LOTACA' , 'C9K_LOTACA'}, {'V1B_TRABAL' , 'C9L_TRABAL'}}, V1B->(IndexKey(1)))

	//INFORMA��ES DE CONVOCA��O DE TRABALHO INTERMITENTE PERIODO DE ANTERIOR
	oModel:AddGrid('MODEL_V1C', 'MODEL_C9Q', oStruV1C)
	oModel:GetModel('MODEL_V1C'):SetOptional(.T.)
	oModel:GetModel('MODEL_V1C'):SetUniqueLine({'V1C_IDCONV'})
	oModel:GetModel('MODEL_V1C'):SetMaxLine(200)

	oModel:SetRelation('MODEL_V1C', {{'V1C_FILIAL' , 'xFilial( "V1C" )'}, {'V1C_ID' , 'C91_ID'}, {'V1C_VERSAO' , 'C91_VERSAO'}, {'V1C_RECIBO' , 'T14_IDEDMD'}, {'V1C_DTACOR' , 'C9N_DTACOR'}, {'V1C_TPACOR' , 'C9N_TPACOR'}, {'V1C_PERREF' , 'C9O_PERREF'}, {'V1C_ESTABE' , 'C9P_ESTABE'}, {'V1C_LOTACA' , 'C9P_LOTACA'}, {'V1C_TRABAL' , 'C9Q_TRABAL'}}, V1C->(IndexKey(1)))
else
	//INFORMA��ES DAS CONVOCA��ES DE TRABALHO INTERMITENTE
	oModel:AddGrid('MODEL_T89', 'MODEL_T14', oStruT89)
	oModel:GetModel('MODEL_T89'):SetOptional(.T.)
	oModel:GetModel('MODEL_T89'):SetUniqueLine({'T89_IDCONV'})
	oModel:GetModel('MODEL_T89'):SetMaxLine(99)

	// RELATIONS INF. DA(S) CONVOCA��O(�ES) DE TRABALHO INTERMITENTE
	oModel:SetRelation('MODEL_T89', {{'T89_FILIAL' , 'xFilial( "T89" )'}, {'T89_ID' , 'C91_ID'}, {'T89_VERSAO' , 'C91_VERSAO'}, {'T89_IDEDMD' , 'T14_IDEDMD'}}, T89->(IndexKey(1)))
endif

Return oModel
//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Vitor Siqueira
@since 08/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oModel   := FWLoadModel( 'TAFA250' )

Local oStruC91a := Nil
Local oStruC91b := Nil
Local oStruC91c := Nil
Local oStruC91d	:= Nil
Local oStruT6W 	:= FWFormStruct( 2, 'T6W' )
Local oStruC9K 	:= FWFormStruct( 2, 'C9K' )
Local oStruC9L 	:= FWFormStruct( 2, 'C9L' )
Local oStruC9M 	:= FWFormStruct( 2, 'C9M' )
Local oStruC9N 	:= FWFormStruct( 2, 'C9N' )
Local oStruC9O 	:= FWFormStruct( 2, 'C9O' )
Local oStruC9P 	:= FWFormStruct( 2, 'C9P' )
Local oStruC9Q 	:= FWFormStruct( 2, 'C9Q' )
Local oStruC9R 	:= FWFormStruct( 2, 'C9R' )
Local oStruCRN 	:= FWFormStruct( 2, 'CRN' )
Local oStruT14 	:= FWFormStruct( 2, 'T14' )
Local oStruT6Y 	:= FWFormStruct( 2, 'T6Y' )
Local oStruT6Z 	:= FWFormStruct( 2, 'T6Z' )
Local oStruT89
Local oStruV1B
Local oStruV1C

Local oView    := FWFormView():New()

Local aCmpGrp  	:= {}

Local cCmpFil  	:= ''
Local cGrp1	   	:= ''
Local cGrp2	 	:= ''
Local cGrp3 	:= ''
Local cGrp4	 	:= ''
Local cGrp7		:= ''

Local cGrp5	 	:= ''
Local cGrp6		:= ''

Local nI 		:= 0
Local lIncMV	:= .F.
Local lCallInc	:= .F.

if TafLayESoc("02_04_02")
	oStruV1B := FWFormStruct( 2, 'V1B' )
	oStruV1C := FWFormStruct( 2, 'V1C' )
else
	oStruT89 := FWFormStruct( 2, 'T89' )
endif

oView:SetModel( oModel )
oView:SetContinuousForm(.T.)

// Informa��es de Apura��o/Identifica��o do Trabalhador
cGrp1 := 'C91_ID|C91_INDAPU|C91_PERAPU|'
oStruC91a := FwFormStruct( 2, 'C91', {|x| AllTrim( x ) + "|" $ cGrp1 } )

If FWIsInCallStack( "TAF250Inc" ) .AND. !FWIsInCallStack( "TAF250AUTI" )
	lCallInc 	:= .T.
	lIncMV 		:= Aviso( "Aten��o", "A folha de pagamento a ser inclu�da � de um funcion�rio com ";
					+ "m�ltiplos v�nculos?", {"Sim", "N�o"}, 2 ) == 1
ElseIf FWIsInCallStack( "TAF250AUTI" )
	lCallInc 	:= .T.
EndIf


// --> Tratamento para m�ltiplos v�nculos, remover os campos de label "redundantes" para o cliente
If ( lCallInc .And. !lIncMV ) .Or. ( !lCallInc .And. !Empty( C91->C91_TRABAL ) )
	cGrp2 := 'C91_TRABAL|C91_DTRABA|'
Else
	cGrp2 := 'C91_CPF|C91_NIS|'
	cGrp6 := 'C91_NOME|C91_NASCTO|'
EndIf

oStruC9P:SetProperty( "C9P_TPINSC", MVC_VIEW_COMBOBOX, {"","1=CNPJ","2=CPF","3=CAEPF","4=CNO"}  )

If ( lCallInc .And. !lIncMV ) .Or. ( !lCallInc .And. !Empty( C91->C91_TRABAL ) )

	If !Empty(C9K->C9K_ESTABE) .And. Empty(C9K->C9K_TPINSC)
		oStruC9K:RemoveField( "C9K_CODLOT" )
		oStruC9K:RemoveField( "C9K_TPINSC" )
		oStruC9K:RemoveField( "C9K_NRINSC" ) 
	EndIf
	oStruC9M:RemoveField( "C9M_RUBRIC" )
	oStruC9M:RemoveField( "C9M_IDTABR" )
	oStruC9P:RemoveField( "C9P_CODLOT" )
	oStruC9P:RemoveField( "C9P_TPINSC" )
	oStruC9P:RemoveField( "C9P_NRINSC" )
	oStruC9R:RemoveField( "C9R_RUBRIC" )
	oStruC9R:RemoveField( "C9R_IDTABR" )

Else
	oStruC9K:RemoveField( "C9K_ESTABE" )
	oStruC9K:RemoveField( "C9K_DESTAB" )
	oStruC9K:RemoveField( "C9K_LOTACA" )
	oStruC9K:RemoveField( "C9K_DLOTAC" )
	oStruC9L:RemoveField( "C9L_TRABAL" )
	oStruC9M:RemoveField( "C9M_CODRUB" )
	oStruC9M:RemoveField( "C9M_DCODRU" )
	oStruC9P:RemoveField( "C9P_LOTACA" )
	oStruC9P:RemoveField( "C9P_DLOTAC" )
	oStruC9P:RemoveField( "C9P_ESTABE" )
	oStruC9P:RemoveField( "C9P_DESTAB" )
	oStruC9Q:RemoveField( "C9Q_TRABAL" )
	oStruC9R:RemoveField( "C9R_CODRUB" )
	oStruC9R:RemoveField( "C9R_DCODRU" )

	oStruC9M:SetProperty( "C9M_RUBRIC"	, MVC_VIEW_ORDEM    , "01" )
	oStruC9M:SetProperty( "C9M_IDTABR"	, MVC_VIEW_ORDEM    , "02" )
	oStruC9Q:SetProperty( "C9Q_DTRABA"	, MVC_VIEW_CANCHANGE, .T.  )
EndIf

oStruC9Q:SetProperty('C9Q_COMPAC',MVC_VIEW_PVAR ,{||"@R 9999-99"})

/*
DSERTAF1-4588 - 16-05-2018
Remo��o dos campos C91_QTDTRB, C91_CODCBO, C91_DCODCB, C91_NATATV em virtude
da altera��o no layout do E-Social na vers�o 2.4.02.
*/
//cGrp3 := 'C91_QTDTRB|C91_CODCBO|C91_DCODCB|C91_NATATV|'


cGrp4 := 'C91_INDMVI|'
if TafLayESoc("02_04_02")
	cGrp5 := 'C91_QTDINT|'
endif
cCmpFil := cGrp2 + cGrp3 + cGrp4 + cGrp5

If !Empty(cGrp6)
	cCmpFil += cGrp6
EndIf

If TafLayESoc("02_05_00")
	cGrp7 := 'C91_TPINSC|C91_CNPJEA|C91_MATREA|C91_DTINVI|C91_OBSVIN|'
	cCmpFil += cGrp7
EndIf

oStruC91b := FwFormStruct( 2, 'C91', {|x| AllTrim( x ) + "|" $ cCmpFil } )

// Campos do folder do n�mero do ultimo protocolo
cCmpFil := 'C91_PROTUL|'
oStruC91c := FwFormStruct( 2, 'C91', {|x| AllTrim( x ) + "|" $ cCmpFil } )

/*-----------------------------------------------------------------------------------
									 Grupo de campos Trabalhador
-------------------------------------------------------------------------------------*/
oStruC91b:AddGroup( "GRP_TRABALHADOR_02", STR0063, "", 1 ) //"Identifica��o do Trabalhador"

If !Empty( cGrp6 )
	oStruC91b:AddGroup( "GRP_TRABALHADOR_06", "Informa��es complementares para aut�nomos <infoComplem>", "", 1 ) //"Informa��es complementares para aut�nomos <infoComplem>"
	aCmpGrp := StrToKArr(cGrp6,"|")

	For nI := 1 to Len(aCmpGrp)
		oStruC91b:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_06")
	Next nI
EndIf

oStruC91b:AddGroup( "GRP_TRABALHADOR_03", STR0068, "", 1 ) //'Informa��es Complementares Do Trabalhador Aut�nomo'
oStruC91b:AddGroup( "GRP_TRABALHADOR_04", STR0004, "", 1 ) //"Informa��es de Multiplos V�nculos"

aCmpGrp := StrToKArr(cGrp2,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC91b:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_02")
Next nI

/*
DSERTAF1-4588 - 16-05-2018
Remo��o dos campos C91_QTDTRB, C91_CODCBO, C91_DCODCB, C91_NATATV em virtude
da altera��o no layout do E-Social na vers�o 2.4.02.
*/
/*
aCmpGrp := StrToKArr(cGrp3,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC91b:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_03")
Next nI
*/

aCmpGrp := StrToKArr(cGrp4,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC91b:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_04")
Next nI

if TafLayESoc("02_04_02")
	oStruC91b:AddGroup( "GRP_TRABALHADOR_05", STR0070, "", 1 ) //"Informa��es relativas ao trabalho intermitente"
	aCmpGrp := StrToKArr(cGrp5,"|")
	For nI := 1 to Len(aCmpGrp)
		oStruC91b:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_05")
	Next nI
endif

If TafLayESoc("02_05_00")
	oStruC91b:AddGroup( "GRP_TRABALHADOR_07", STR0076, "", 1 ) //"Informa��es da sucess�o de v�nculo trabalhista/estatut�rio"
	aCmpGrp := StrToKArr(cGrp7,"|")
	For nI := 1 to Len(aCmpGrp)
		oStruC91b:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_07")
	Next nI
EndIf

If FindFunction("TafAjustRecibo")
	TafAjustRecibo(oStruC91c,"C91")
EndIf
/*--------------------------------------------------------------------------------------------
									Esrutura da View
---------------------------------------------------------------------------------------------*/
oView:AddField( 'VIEW_C91a', oStruC91a, 'MODEL_C91' )
oView:EnableTitleView( 'VIEW_C91a', STR0060 ) // Informa��es de Apura��o

oView:AddField( 'VIEW_C91b', oStruC91b, 'MODEL_C91' )
oView:EnableTitleView( 'VIEW_C91b', STR0059 ) //Identifica��o do Trabalhador

oView:AddField( 'VIEW_C91c', oStruC91c, 'MODEL_C91' )
If FindFunction("TafNmFolder")
	oView:EnableTitleView( 'VIEW_C91c', TafNmFolder("recibo",1) ) // "Recibo da �ltima Transmiss�o"
EndIf

oView:AddGrid( 'VIEW_T6W', oStruT6W, 'MODEL_T6W' )
oView:EnableTitleView("VIEW_T6W",STR0064) //"Remunera��o do Trab. de V�nculo com Outras Empresas"

oView:AddGrid( 'VIEW_CRN', oStruCRN, 'MODEL_CRN' )
oView:EnableTitleView("VIEW_CRN",STR0053) //"Informa��es de Processos Judiciarios de Remunera��o"

oView:AddGrid( 'VIEW_T14', oStruT14, 'MODEL_T14' )
oView:EnableTitleView("VIEW_T14",STR0040) //"Informa��es do Recibo de Pagamento"

oView:AddGrid( 'VIEW_C9K', oStruC9K, 'MODEL_C9K' )
oView:EnableTitleView("VIEW_C9K",STR0041) //"Informa��es do Estabelecimento/Lota��o"

oView:AddGrid( 'VIEW_C9L', oStruC9L, 'MODEL_C9L' )
oView:EnableTitleView("VIEW_C9L",STR0042) //"Informa��es da Remunera��o do Trabalhador no Per�odo de Apura��o"

oView:AddGrid( 'VIEW_C9M', oStruC9M, 'MODEL_C9M' )

oView:AddGrid( 'VIEW_T6Y', oStruT6Y, 'MODEL_T6Y' )
oView:EnableTitleView("VIEW_T6Y",STR0067) //"Informa��es Operadoras de Planos de Sa�de"

oView:AddGrid( 'VIEW_T6Z', oStruT6Z, 'MODEL_T6Z' )
oView:AddIncrementField( 'VIEW_T6Z', 'T6Z_SEQUEN' )
oView:EnableTitleView("VIEW_T6Z",STR0043) //"Informa��es do Dependente"

oView:AddGrid( 'VIEW_C9N', oStruC9N, 'MODEL_C9N' )
oView:EnableTitleView("VIEW_C9N",STR0046) //"Informa��es de Acordo"

oView:AddGrid( 'VIEW_C9O', oStruC9O, 'MODEL_C9O' )
oView:EnableTitleView("VIEW_C9O",STR0048) //"Informa��es do Periodo"

oView:AddGrid( 'VIEW_C9P', oStruC9P, 'MODEL_C9P' )
oView:EnableTitleView("VIEW_C9P",STR0041) //"Informa��es da Remunera��o do Trabalhador"

oView:AddGrid( 'VIEW_C9Q', oStruC9Q, 'MODEL_C9Q' )
oView:EnableTitleView("VIEW_C9Q",STR0042) //"Informa��es da Remunera��o do Trabalhador no Per�odo de Apura��o"

oView:AddGrid( 'VIEW_C9R', oStruC9R, 'MODEL_C9R' )
if !TafLayESoc("02_04_02")
	oView:EnableTitleView("VIEW_C9R",STR0036) //"Itens da Remunera��o do Trabalhador"
endif

if TafLayESoc("02_04_02")
	oView:AddGrid( 'VIEW_V1B', oStruV1B, 'MODEL_V1B' )
	oView:AddGrid( 'VIEW_V1C', oStruV1C, 'MODEL_V1C' )
else
	oView:AddGrid( 'VIEW_T89', oStruT89, 'MODEL_T89' )
	oView:EnableTitleView("VIEW_T89",STR0070) //"Informa��es das convoca��es de Trabalho Intermitente"
endif

/*-----------------------------------------------------------------------------------
								Estrutura do Folder
-------------------------------------------------------------------------------------*/

oView:CreateHorizontalBox( 'PAINEL_SUPERIOR', 100 )

oView:CreateFolder( 'FOLDER_SUPERIOR', 'PAINEL_SUPERIOR' )

oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA01', STR0002 )   //"Informa��es da Folha"
oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA02', STR0061 )   //"Recibo de Pagamentos"

If FindFunction("TafNmFolder")
	oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA03', TafNmFolder("recibo") )   //"Numero do Recibo"
Else
	oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA03', STR0052 )   //"Protocolo de Transmiss�o"
EndIf

oView:CreateHorizontalBox( 'C91a',  015,,, 'FOLDER_SUPERIOR', 'ABA01' )
oView:CreateHorizontalBox( 'C91b',  035,,, 'FOLDER_SUPERIOR', 'ABA01' )
oView:CreateHorizontalBox( 'T6W' ,  025,,, 'FOLDER_SUPERIOR', 'ABA01' )
oView:CreateHorizontalBox( 'CRN' ,  025,,, 'FOLDER_SUPERIOR', 'ABA01' )
oView:CreateHorizontalBox( 'C91c',  100,,, 'FOLDER_SUPERIOR', 'ABA03' )

oView:CreateHorizontalBox( 'T14',   020,,, 'FOLDER_SUPERIOR', 'ABA02' )

oView:CreateHorizontalBox("PAINEL_INFOPERAPUR",80,,,"FOLDER_SUPERIOR","ABA02")
oView:CreateFolder( 'FOLDER_INFOPERAPUR', 'PAINEL_INFOPERAPUR' )

oView:AddSheet( 'FOLDER_INFOPERAPUR', 'ABA01', STR0065 )   //"Informa��es relativas a remunera��o do trabalhador no per�odo de apura��o"
oView:CreateHorizontalBox( 'C9K',   025,,, 'FOLDER_INFOPERAPUR', 'ABA01' )
oView:CreateHorizontalBox( 'C9L',   025,,, 'FOLDER_INFOPERAPUR', 'ABA01' )

oView:CreateHorizontalBox("PAINEL_REMUN",50,,,"FOLDER_INFOPERAPUR","ABA01")
oView:CreateFolder( 'FOLDER_REMUN', 'PAINEL_REMUN' )
oView:AddSheet( 'FOLDER_REMUN', 'ABA01', STR0036 ) //"Itens da Remunera��o do Trabalhador"
oView:AddSheet( 'FOLDER_REMUN', 'ABA02', STR0034 ) //"Informa��o do Plano de Sa�de"

oView:CreateHorizontalBox ( 'C9M', 100,,, 'FOLDER_REMUN'  , 'ABA01' )
oView:CreateHorizontalBox ( 'T6Y', 050,,, 'FOLDER_REMUN'  , 'ABA02' )
oView:CreateHorizontalBox ( 'T6Z', 050,,, 'FOLDER_REMUN'  , 'ABA02' )

if TafLayESoc("02_04_02")
	oView:AddSheet( 'FOLDER_REMUN', 'ABA03', STR0070 ) //"Informa��es das convoca��es de Trabalho Intermitente"
	oView:CreateHorizontalBox ( 'V1B', 100,,, 'FOLDER_REMUN'  , 'ABA03' )
endif


/*------------------------
   PERIODO DE APUR. ANT.
--------------------------*/
oView:AddSheet( 'FOLDER_INFOPERAPUR', 'ABA02', STR0066 )   //"Informa��es relativas a remunera��o do trabalhador em per�odos anteriores ao per�odo de apura��o"

oView:CreateHorizontalBox ( 'C9N', 20,,, 'FOLDER_INFOPERAPUR'  , 'ABA02' )
oView:CreateHorizontalBox ( 'C9O', 20,,, 'FOLDER_INFOPERAPUR'  , 'ABA02' )
oView:CreateHorizontalBox ( 'C9P', 20,,, 'FOLDER_INFOPERAPUR'  , 'ABA02' )
oView:CreateHorizontalBox ( 'C9Q', 20,,, 'FOLDER_INFOPERAPUR'  , 'ABA02' )

if TafLayESoc("02_04_02")
	oView:CreateHorizontalBox("PAINEL_REMUNANT",20,,,"FOLDER_INFOPERAPUR","ABA02")
	oView:CreateFolder( 'FOLDER_REMUNANT', 'PAINEL_REMUNANT' )
	oView:AddSheet( 'FOLDER_REMUNANT', 'ABA01', STR0036 ) //"Itens da Remunera��o do Trabalhador"
	oView:AddSheet( 'FOLDER_REMUNANT', 'ABA02', STR0070 ) //"Informa��es das convoca��es de Trabalho Intermitente"

	oView:CreateHorizontalBox ( 'C9R', 100,,, 'FOLDER_REMUNANT'  , 'ABA01' )
	oView:CreateHorizontalBox ( 'V1C', 100,,, 'FOLDER_REMUNANT'  , 'ABA02' )
else
	oView:CreateHorizontalBox ( 'C9R', 20,,, 'FOLDER_INFOPERAPUR'  , 'ABA02' )

	oView:AddSheet( 'FOLDER_INFOPERAPUR', 'ABA03', STR0070 )   //"Informa��es das convoca��es de Trabalho Intermitente"
	oView:CreateHorizontalBox ( 'T89',100,,, 'FOLDER_INFOPERAPUR'  , 'ABA03' )
endif

oView:SetOwnerView( 'VIEW_C91a' , 'C91a')
oView:SetOwnerView( 'VIEW_C91b' , 'C91b')
oView:SetOwnerView( 'VIEW_C91c' , 'C91c')
oView:SetOwnerView( 'VIEW_T6W' , 'T6W')
oView:SetOwnerView( 'VIEW_CRN' , 'CRN')
oView:SetOwnerView( 'VIEW_T14' , 'T14')
oView:SetOwnerView( 'VIEW_C9K' , 'C9K')
oView:SetOwnerView( 'VIEW_C9L' , 'C9L')
oView:SetOwnerView( 'VIEW_C9M' , 'C9M')
oView:SetOwnerView( 'VIEW_T6Y' , 'T6Y')
oView:SetOwnerView( 'VIEW_T6Z' , 'T6Z')

oView:SetOwnerView( 'VIEW_C9N' , 'C9N')
oView:SetOwnerView( 'VIEW_C9O' , 'C9O')
oView:SetOwnerView( 'VIEW_C9P' , 'C9P')

oView:SetOwnerView( 'VIEW_C9Q' , 'C9Q')
oView:SetOwnerView( 'VIEW_C9R' , 'C9R')

if TafLayESoc("02_04_02")
	oView:SetOwnerView( 'VIEW_V1B' , 'V1B')
	oView:SetOwnerView( 'VIEW_V1C' , 'V1C')
else
	oView:SetOwnerView( 'VIEW_T89' , 'T89')
endif

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif
	xFunRmFStr(@oStruC91a,"C91")

	oStruT14:RemoveField('T14_NOMEVE')
	oStruT6W:RemoveField('T6W_NOMEVE')
	oStruT6Y:RemoveField('T6Y_NOMEVE')
	oStruT6Z:RemoveField('T6Z_NOMEVE')
	oStruCRN:RemoveField('CRN_IDSUSP')
	oStruC91a:RemoveField('C91_ID')

	if TafLayESoc("02_04_02")
		oStruV1B:RemoveField('V1B_IDCONV')
		oStruV1C:RemoveField('V1C_IDCONV')
	else
		oStruT89:RemoveField('T89_IDCONV')
	endif
EndIf


If TafLayESoc("02_05_00") .And. TafColumnPos("C91_TPINSC")
	oStruC91b:SetProperty( "C91_TPINSC"	, MVC_VIEW_ORDEM, "42" )
EndIf


//-----------------------------------------------------------------------
// Altera consulta padr�o do benefici�rio para apenas exibir autonomos
//-----------------------------------------------------------------------
If FWIsInCallStack( "TAF250AUTI" )
	oStruC91b:SetProperty( "C91_TRABAL"	, MVC_VIEW_LOOKUP, "C9VAUT" )
EndIf

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF250Xml
Funcao de geracao do XML para atender o registro S-1200
Quando a rotina for chamada o registro deve estar posicionado

@Param:
lRemEmp		-	Exclusivo do Evento S-1000
cSeqXml		-	N�mero sequencial para composi��o da chave ID do XML
lInfoRPT	-	Indica se a gera��o de XML deve gerar informa��es na tabela de relat�rio

@Return:
cXml - Estrutura do Xml do Layout S-1200

@author Vitor Siqueira
@since 08/01/2016
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAF250Xml( cAlias, nRecno, nOpc, lJob, lRemEmp, cSeqXml, lInfoRPT )

Local cXml			:=	""
Local cXmlCompl		:=	""
Local cLayout		:=	"1200"
Local cReg			:=	"Remun"
Local cIDTabRubr	:=	""
Local cEvtTrabC9Q	:=	""
Local cC9LCodCat	:=	""
Local cC9QCodCat	:=	""
Local cCodSusp		:=	""
Local cCodCat		:=	""
Local cSucVinc		:=	""
Local cNISFunc		:=	""
Local cCompConv		:=	""
Local cMatric		:=	""
Local cGrauExp		:=  ""
Local aMensal		:=	{}
Local aAuxPerAnt	:=	{}
Local lFindAuto		:=	.F.
Local lFindMatr		:=	.F.
Local lMV			:= 	.F.
Local lC9K			:=	.T.
Local lC9L			:=	.T.
Local lC9M			:=	.T.
Local lT6Y			:=	.T.
Local lC9N			:=	.T.
Local lC9O			:=	.T.
Local lC9Q			:=	.T.
Local lC9P			:=	.T.
Local lC9R			:=	.T.
Local lV1B			:=	.T.
Local lV1C			:=	.T.
Local lGeraC9Q		:=  .F.
Local lGeraC9R		:=  .F.
Local lInfoCompl	:=	.T.
Local lFindT6Y		:=  .F.
Local lFindT6Z		:= .F.
Local nRecnoSM0 	:= SM0->(Recno())
Local lRubERPPad	:= .T.
Local nMVIDETABR	:= SuperGetMV("MV_IDETABR",.F.,"0")
Local cFilBkp       := cFilAnt
Local lXmlVLd		:=  IIF(FindFunction('TafXmlVLD'),TafXmlVLD('TAF250XML'),.T.)
Local cEvent 		:= ''
Local cRemuns       := ''

//Relat�rio de Confer�ncia de Valores
Local oInfoRPT		:=	Nil
Local cCPF			:=	""
Local cNome			:=	""
Local cTipoEstab	:=	""
Local cEstab		:=	""
Local cLotacao		:=	""
Local cCodRubr		:=	""
Local aAnalitico	:=	{}
Local aRubrica		:=	{}
Local nPosValores	:=	0

Default lInfoRPT	:=	.F.

If lXmlVLd
//Verifica se existe as classes necess�rias para utiliza��o do relat�rio persistido
If lInfoRPT
	If __lGrvRPT == Nil
		TAF250Rpt() //Inicializa a vari�vel static __lGrvRPT
	EndIf

	lInfoRPT := __lGrvRPT
EndIf
If lInfoRPT
	If oReport == Nil
		oReport := TAFSocialReport():New()
	EndIf
EndIf

If IsInCallStack("TafNewBrowse") .And. ( C91->C91_FILIAL <> cFilAnt )
    cFilAnt := C91->C91_FILIAL
EndIf
 

	If IsInCallStack("TAFA250") .AND. !IsInCallStack("XmlErpxTaf") .AND. (IsInCalLStack("TAFXmlLote") .OR. IsInCallStack("TAF250Xml")) 
		If slRubERPPad == Nil
			lRubERPPad := nMVIDETABR == "1" .OR. (nMVIDETABR == "0" .and. ApMsgYesNo("Deseja gerar o conte�do da tag 'ideTabRubr' com o c�digo padr�o deste ERP ou conforme ERP de Origem?"+CRLF+" - Sim para c�digo padr�o(T3M_ID)."+CRLF+" - N�o para conforme ERP de Origem (T3M_CODERP).","Conte�do 'ideTabRubr' padr�o?"))
			If IsInCalLStack("TAFXmlLote")
				slRubERPPad	:= lRubERPPad
			EndIf
		Else
			lRubERPPad	:= slRubERPPad
		EndIf
	EndIf

	If C91->C91_EVENTO $ "I|A" 

		AADD(aMensal,C91->C91_INDAPU)

		If Len(Alltrim(C91->C91_PERAPU)) <= 4
			AADD(aMensal,C91->C91_PERAPU)
		Else
			AADD(aMensal,substr(C91->C91_PERAPU, 1, 4) + '-' + substr(C91->C91_PERAPU, 5, 2) )
		EndIf

		If C91->C91_MV == "1"
			lMV := .T.
		EndIf
		//-----------------------------
		//In�cio da TAG ideTrabalhador
		//-----------------------------
		C9V->( DBSetOrder( 2 ) )
		CUP->( DBSetOrder( 4 ) )

		lFindAuto := .F.
		lFindMatr := .F.

		If C9V->( MsSeek( xFilial( "C9V" ) + C91->C91_TRABAL + "1" ) )
			If C9V->C9V_NOMEVE == "TAUTO"
				lFindAuto := .T.
			Else
				lFindMatr := .T.
			EndIf
		Else 
			C9V->( DbSetOrder (3) )
			If C9V->( MsSeek( xFilial("C9V") + C91->C91_CPF + "1") )
				If C9V->C9V_NOMEVE == "TAUTO"
					lFindAuto := .T.
				EndIf
			EndIf
		EndIf

		//Se encontrar o trabalhador com vinculo ou um trabalhador autonomo ou dados informados direto no s1200
		If lFindAuto .or. lFindMatr .Or. lMV

			cXml +=	"<ideTrabalhador>"

			If lFindMatr .Or. lFindAuto
				cCPF	:=	C9V->C9V_CPF
				cNome	:=	C9V->C9V_NOME
				cEvent  :=	C9V->C9V_NOMEVE

				cXml +=		xTafTag("cpfTrab",cCPF)

				Conout("TAF250Xml |" + FWTimeStamp(3) + " - Chave de busca do funcionario: |" + C9V->C9V_FILIAL + C9V->C9V_ID + C9V->C9V_NIS + "| Recno: " + Str(C9V->(Recno())))

				cNISFunc	:= TAF250Nis(C9V->C9V_FILIAL, C9V->C9V_ID, C9V->C9V_NIS, C91->C91_PERAPU,C9V->C9V_NOMEVE)

				Conout("TAF250Xml |" + FWTimeStamp(3) + " - NIS retornado do funcionario: |" + C9V->C9V_FILIAL + C9V->C9V_ID + C9V->C9V_NIS + "| Recno: " + Str(C9V->(Recno())) + " | NIS: " + cNISFunc )

				cXml +=		xTafTag("nisTrab",cNISFunc,,.T.)
			Else
				cCPF	:=	C91->C91_CPF
				cNome	:=	C91->C91_NOME

				cXml +=		xTafTag("cpfTrab", cCPF)
				cXml +=		xTafTag("nisTrab", C91->C91_NIS,,.T.)
			EndIf

			If !Empty(C91->C91_INDMVI)

				cXml +=		"<infoMV>"
				cXml +=			xTafTag("indMV",C91->C91_INDMVI)

				T6W->( DbSetOrder( 1 ) )
				If T6W->( MsSeek ( xFilial("T6W")+C91->(C91_ID+C91_VERSAO)))
					While !T6W->(Eof()) .And. AllTrim(C91->(C91_ID+C91_VERSAO)) == AllTrim(T6W->(T6W_ID+T6W_VERSAO))

						cXml +=			"<remunOutrEmpr>"
						cXml +=				xTafTag("tpInsc",T6W->T6W_TPINSC)
						cXml +=				xTafTag("nrInsc",T6W->T6W_NRINSC)
						cXml +=				xTafTag("codCateg",POSICIONE("C87",1, xFilial("C87")+T6W->T6W_CODCAT,"C87_CODIGO"))
						cXml +=				xTafTag("vlrRemunOE",T6W->T6W_VLREMU,PesqPict("T6W","T6W_VLREMU"),/*04*/,/*05*/,.T.)
						cXml +=			"</remunOutrEmpr>"

						T6W->(DbSkip())
					EndDo
				EndIf

				cXml +=		"</infoMV>"
			EndIf

			cRemuns := C9N->C9N_REMUNS
			//Somente gera para o trabalhador aut�nomo, a tabela � a mesma C9V, por�m o campo _NOMEVE � gravado como TAUTO
			//E SOMENTE PARA S2200 COM remunSuc = 's'
			If lFindAuto .OR.(cRemuns == '1' .AND. cEvent == "S2200")

				If TafLayESoc("02_05_00") 
					xTafTagGroup( "sucessaoVinc"	, {	{ "tpInscAnt", C91->C91_TPINSC,, .T. };
							,	{ "cnpjEmpregAnt", C91->C91_CNPJEA,, .T. };
							, 	{ "matricAnt", C91->C91_MATREA,, .F. };
							, 	{ "dtAdm", C91->C91_DTINVI,, .T. };
							, 	{ "observacao", C91->C91_OBSVIN,, .F. } };
							, @cSucVinc )

				Else
					xTafTagGroup( "sucessaoVinc"	, {	{ "cnpjEmpregAnt", C91->C91_CNPJEA,, .T. };
							, 	{ "matricAnt", C91->C91_MATREA,, .F. };
							, 	{ "dtAdm", C91->C91_DTINVI,, .T. };
							, 	{ "observacao", C91->C91_OBSVIN,, .F. } };
							, @cSucVinc )
				EndIf

				if TafLayESoc("02_04_02")

					If Empty(C91->C91_NOME )
						xTafTagGroup( "infoComplem"	, {	{ "nmTrab", C9V->C9V_NOME,, .F. };
							, 	{ "dtNascto", C9V->C9V_DTNASC,, .F. } };
							, @cXmlCompl;
							, { { "sucessaoVinc", cSucVinc, 0 } } )
					Else
						xTafTagGroup( "infoComplem"	, {	{ "nmTrab", C91->C91_NOME,, .F. };
							, 	{ "dtNascto", C91->C91_NASCTO,, .F. } };
							, @cXmlCompl;
							, { { "sucessaoVinc", cSucVinc, 0 } } )
					EndIf

				else
					xTafTagGroup( "infoComplem"	, {	{ "nmTrab", C9V->C9V_NOME,, .F. };
						, 	{ "dtNascto", C9V->C9V_DTNASC,, .F. };
						, 	{ "codCBO", Posicione( "C8Z", 1, xFilial( "C8Z" ) + C91->C91_CODCBO, "C8Z_CODIGO" ),, .F. };
						, 	{ "natAtividade", C91->C91_NATATV,, .T. };
						, 	{ "qtdDiasTrab", C91->C91_QTDTRB,, .T. } };
						, @cXmlCompl;
						, { { "sucessaoVinc", cSucVinc, 0 } } )
				endif

				//Inser��o de flag para substitui��o pelo cXmlCompl ao final da montagem do Xml.
				//� necess�rio verificar se o S-1200 � somente de Trabalhador Aut�nomo ( TAUTO ), e neste caso, o grupo infoComplem deve ser enviado.
				//Caso o 1200 possua um S-2200 ou S-2300, n�o � necess�rio enviar o grupo infoComplem.
				cXml += "FlagInfoComplem"
			EndIf

			CRN->( DbSetOrder( 1 ) )
			If CRN->( MsSeek ( xFilial("CRN")+C91->(C91_ID+C91_VERSAO)))
				While !CRN->(Eof()) .And. AllTrim(C91->(C91_ID+C91_VERSAO)) == AllTrim(CRN->(CRN_ID+CRN_VERSAO))

					cXml += "<procJudTrab>"
					cXml +=		xTafTag("tpTrib",CRN->CRN_TPTRIB)

					C1G->(DbSetOrder(8))
					C1G->(MsSeek(xFilial("C1G") + CRN->CRN_IDPROC + "1"))
					cXml +=		xTafTag("nrProcJud",C1G->C1G_NUMPRO)

					cCodSusp    := Posicione("T5L",1,xFilial("T5L")+CRN->CRN_IDSUSP,"T5L_CODSUS")
					If !Empty(cCodSusp)
						cXml += xTafTag("codSusp", Alltrim(cCodSusp))
					EndIf
					cXml +="</procJudTrab>"

					CRN->(DbSkip())
				EndDo
			EndIf

			if TafLayESoc("02_04_02")
				if !Empty(C91->C91_QTDINT)
					cXml +=		"<infoInterm>"
					cXml +=			xTafTag("qtdDiasInterm",C91->C91_QTDINT)
					cXml +=		"</infoInterm>"
				endif
			endif

			cXml +=	"</ideTrabalhador>"

			/*----------------------------------------------------------
					Inicio da TAG DmDev
			----------------------------------------------------------*/
			T14->( DbSetOrder( 1 ) )
			If T14->( MsSeek ( C91->(C91_FILIAL+C91_ID+C91_VERSAO)))

				While !T14->(Eof()) .And. AllTrim(C91->(C91_ID+C91_VERSAO)) == AllTrim(T14->(T14_ID+T14_VERSAO))

					//Variaveis para simula��o do DO-WHILE, os grupos que s�o 1-1 (no Layout e-Social)
					//tenho que gerar pelo menos 1 vez.

					cXml +=	"<dmDev>"

					cXml +=	xTafTag("ideDmDev",T14->T14_IDEDMD,PesqPict("T14","T14_IDEDMD"))
					cCodCat := AllTrim(POSICIONE("C87",1, xFilial("C87")+T14->T14_CODCAT,"C87_CODIGO"))
					cXml +=	xTafTag("codCateg",cCodCat)

					C9K->( DbSetOrder( 1 ) )
					If C9K->( MsSeek ( T14->(T14_FILIAL+T14_ID+T14_VERSAO+T14_IDEDMD)))
						cXml +=		"<infoPerApur>"

						While lC9K
							cXml +=	"<ideEstabLot>"

							If !Empty( C9K->C9K_TPINSC ) .and. !Empty( C9K->C9K_NRINSC ) .and. !Empty( C9K->C9K_CODLOT )
								cTipoEstab	:=	C9K->C9K_TPINSC
								cEstab		:=	C9K->C9K_NRINSC
								cLotacao	:=	C9K->C9K_CODLOT
							Else
								cTipoEstab	:=	Posicione( "C92", 1, xFilial( "C92" ) + C9K->C9K_ESTABE, "C92_TPINSC" )
								cEstab		:=	Posicione( "C92", 1, xFilial( "C92" ) + C9K->C9K_ESTABE, "C92_NRINSC" )
								cLotacao	:=	Posicione( "C99", 1, xFilial( "C99" ) + C9K->C9K_LOTACA, "C99_CODIGO" )
							EndIf

							cXml +=		xTafTag("tpInsc"    , cTipoEstab )
							cXml +=		xTafTag("nrInsc"    , cEstab )
							cXml +=		xTafTag("codLotacao", cLotacao )

							cXml +=		xTafTag("qtdDiasAv"	,C9K->C9K_QTDDIA,,.T.)

							C9L->( DbSetOrder(3) )
							C9L->( MsSeek ( xFilial("C9L",C9K->C9K_FILIAL)+C9K->(C9K_ID+C9K_VERSAO+C9K_RECIBO+C9K_ESTABE+C9K_LOTACA+C9K_TPINSC+C9K_NRINSC+C9K_CODLOT)))
							While lC9L

								cXml +=		"<remunPerApur>"

								If cCodCat $ "101|102|103|104|105|106|107|108|111|301|302|303|304|305|306|307|308|309"
									If Empty(C9L->C9L_DTRABA)
										cMatric := Posicione( "C9V", 1, xFilial( "C9V" ) + C9L->C9L_TRABAL, "C9V_MATRIC" )
									
										If lInfoCompl
											lInfoCompl := IsTAUTO( C9V->C9V_CPF, cMatric )
										EndIf
									Else
										cMatric := C9L->C9L_DTRABA
									
										If lInfoCompl
											lInfoCompl := IsTAUTO( C91->C91_CPF, cMatric )
										EndIf
									EndIf

									cXml +=	xTafTag("matricula", cMatric,,.T.)
								Else
									If lInfoCompl
										lInfoCompl := IsTAUTO( C91->C91_CPF )
									EndIf
								EndIf

								cXml +=	xTafTag("indSimples",C9L->C9L_INDCON,,.T.)

								If !Empty( C9L->C9L_ESTABE ) .and. !Empty( C9L->C9L_LOTACA )
									C9M->( DbSetOrder(1) )
									C9M->( MsSeek ( xFilial("C9M")+C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)))
								Else
									C9M->( DbSetOrder(3) )
									C9M->( MsSeek ( xFilial("C9M")+C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_CODLOT+C9L_TPINSC+C9L_NRINSC+C9L_DTRABA)))
								EndIf

								While lC9M
									cIDTabRubr := Posicione( "C8R", 5, xFilial( "C8R" ) + C9M->C9M_CODRUB + "1", "C8R_IDTBRU" )

									cXml +=		"<itensRemun>"

									If !Empty( C9M->C9M_CODRUB ) .and. !Empty( cIDTabRubr )
										cCodRubr := C9M->C9M_CODRUB

										cXml +=	xTafTag( "codRubr", Posicione( "C8R", 5, xFilial( "C8R" ) + cCodRubr + "1", "C8R_CODRUB" ) )

										If lRubERPPad
											cXml +=	xTafTag( "ideTabRubr", Posicione( "T3M", 1, xFilial( "T3M" ) + cIDTabRubr, "T3M_ID" ),, .T. )
										Else
											cXml +=	xTafTag( "ideTabRubr", Posicione( "T3M", 1, xFilial( "T3M" ) + cIDTabRubr, "T3M_CODERP" ),, .T. )
										EndIf
									Else
										cCodRubr	:=	C9M->C9M_RUBRIC
										cIDTabRubr	:=	C9M->C9M_IDTABR

										cXml +=	xTafTag( "codRubr", cCodRubr )
										cXml +=	xTafTag( "ideTabRubr", cIDTabRubr,, .T. )
									EndIf

									cXml +=			xTafTag("qtdRubr",C9M->C9M_QTDRUB,PesqPict("C9M","C9M_QTDRUB"),.T.,.T.)
									cXml +=			xTafTag("fatorRubr",C9M->C9M_FATORR,PesqPict("C9M","C9M_FATORR"),.T.,.T.)
									cXml +=			xTafTag("vrUnit",C9M->C9M_VLRUNT,PesqPict("C9M","C9M_VLRUNT"),.T.,.T.)
									cXml +=			xTafTag("vrRubr",C9M->C9M_VLRRUB,PesqPict("C9M","C9M_VLRRUB"),,.T.)
									cXml +=		"</itensRemun>"

									If lInfoRPT
										aRubrica := oReport:GetRubrica( cCodRubr, cIDTabRubr, C91->C91_PERAPU, lMV )

										aAdd( aAnalitico, Array( 13 ) )
										nPosValores := Len( aAnalitico )

										aAnalitico[nPosValores][ANALITICO_MATRICULA]			:=	AllTrim( cMatric )
										aAnalitico[nPosValores][ANALITICO_CATEGORIA]			:=	AllTrim( cCodCat )
										aAnalitico[nPosValores][ANALITICO_TIPO_ESTABELECIMENTO]	:=	AllTrim( cTipoEstab )
										aAnalitico[nPosValores][ANALITICO_ESTABELECIMENTO]		:=	AllTrim( cEstab )
										aAnalitico[nPosValores][ANALITICO_LOTACAO]				:=	AllTrim( cLotacao )
										aAnalitico[nPosValores][ANALITICO_NATUREZA]				:=	AllTrim( aRubrica[1] ) //Natureza
										aAnalitico[nPosValores][ANALITICO_TIPO_RUBRICA]			:=	AllTrim( aRubrica[2] ) //Tipo
										aAnalitico[nPosValores][ANALITICO_INCIDENCIA_CP]		:=	AllTrim( aRubrica[3] ) //Incid�ncia CP
										aAnalitico[nPosValores][ANALITICO_INCIDENCIA_IRRF]		:=	AllTrim( aRubrica[4] ) //Incid�ncia IRRF
										aAnalitico[nPosValores][ANALITICO_INCIDENCIA_FGTS]		:=	AllTrim( aRubrica[5] ) //Incid�ncia FGTS
										aAnalitico[nPosValores][ANALITICO_DECIMO_TERCEIRO]		:=	""
										aAnalitico[nPosValores][ANALITICO_TIPO_VALOR]			:=	""
										aAnalitico[nPosValores][ANALITICO_VALOR]				:=	C9M->C9M_VLRRUB
									EndIf

									C9M->(DbSkip())

									If !lMV
										lC9M := !C9M->(Eof()) .And. AllTrim(C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)) ==;
																	AllTrim(C9M->(C9M_ID+C9M_VERSAO+C9M_RECIBO+C9M_ESTABE+C9M_LOTACA+C9M_TRABAL))
									Else
										lC9M := !C9M->(Eof()) .And. AllTrim(C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_DTRABA+C9L_CODLOT+C9L_TPINSC+C9L_NRINSC)) ==;
																	AllTrim(C9M->(C9M_ID+C9M_VERSAO+C9M_RECIBO+C9M_DTRABA+C9M_CODLOT+C9M_TPINSC+C9M_NRINSC))
									EndIf

								EndDo

								lC9M := .T.

								lFindT6Y := .F.

								If !lMV
									T6Y->( DbSetOrder(1) )
									lFindT6Y := T6Y->( MsSeek ( xFilial("T6Y")+C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)))
								Else
									T6Y->( dbSetOrder(5) )
									lFindT6Y := T6Y->( MsSeek ( xFilial("T6Y")+C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_TPINSC+C9L_NRINSC+C9L_DTRABA+C9L_CODLOT)))
								EndIf

								If lFindT6Y
									cXml +=	"<infoSaudeColet>"
									While lT6Y
										cXml +=	"<detOper>"
										cXml +=		xTafTag("cnpjOper"	,T6Y->T6Y_CNPJOP)
										cXml +=		xTafTag("regANS"	,T6Y->T6Y_REGANS)
										cXml +=		xTafTag("vrPgTit"	,T6Y->T6Y_VLPGTI, PesqPict("T6Y","T6Y_VLPGTI" ),,.T.,.T. )

										lFindT6Z := .F.

										If !lMV
											T6Z->( DbSetOrder( 1 ) )
											lFindT6Z := T6Z->( MsSeek ( xFilial("T6Z")+T6Y->(T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_ESTABE+T6Y_LOTACA+T6Y_TRABAL+T6Y_CNPJOP+T6Y_REGANS)))
										Else
											T6Z->( DbSetOrder(4) )
											lFindT6Z := T6Z->( MsSeek ( xFilial("T6Z")+T6Y->(T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_TPINSC+T6Y_NRINSC+T6Y_DTRABA+T6Y_CNPJOP+T6Y_REGANS)))
										EndIf

										If lFindT6Z
											While !T6Z->(Eof()) .And. AllTrim(T6Y->(T6Y_FILIAL+T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_ESTABE+T6Y_LOTACA+T6Y_TRABAL+T6Y_CNPJOP+T6Y_REGANS+T6Y_TPINSC+T6Y_NRINSC+T6Y_DTRABA+T6Y_CODLOT)) ==;
																	AllTrim(T6Z->(T6Z_FILIAL+T6Z_ID+T6Z_VERSAO+T6Z_RECIBO+T6Z_ESTABE+T6Z_LOTACA+T6Z_TRABAL+T6Z_CNPJOP+T6Z_REGANS+T6Z_TPINSC+T6Z_NRINSC+T6Z_DTRABA+T6Z_CODLOT))
												cXml +=	"<detPlano>"
												cXml +=		xTafTag("tpDep"			,POSICIONE("CMI",1, xFilial("CMI")+T6Z->T6Z_TPDEP,"CMI_CODIGO"))
												cXml +=		xTafTag("cpfDep"		,T6Z->T6Z_CPFDEP,,.T.)
												cXml +=		xTafTag("nmDep"			,T6Z->T6Z_NOMDEP)
												cXml +=		xTafTag("dtNascto"  	,T6Z->T6Z_DTNDEP)
												cXml +=		xTafTag("vlrPgDep"		,T6Z->T6Z_VPGDEP,PesqPict("T6Z","T6Z_VPGDEP"),,.T.,.T. )
												cXml +=	"</detPlano>"
												T6Z->(DbSkip())
											EndDo
										EndIf
										cXml +=	"</detOper>"

										T6Y->(DbSkip())

										If !lMV
											lT6Y := !T6Y->(Eof()) .And. AllTrim(C9L->(C9L_FILIAL+C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)) ==;
																		AllTrim(T6Y->(T6Y_FILIAL+T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_ESTABE+T6Y_LOTACA+T6Y_TRABAL))
										Else
											lT6Y := !T6Y->(Eof()) .And. AllTrim( C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_TPINSC+C9L_NRINSC+C9L_DTRABA+C9L_CODLOT) ) ==;
																		AllTrim( T6Y->(T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_TPINSC+T6Y_NRINSC+T6Y_DTRABA+T6Y_CODLOT) )
										EndIf

									EndDo
									lT6Y := .T.
									cXml +=	"</infoSaudeColet>"
								EndIf

								If !Empty(C9L->C9L_GRAEXP)

									cGrauExp := AllTrim(POSICIONE("C88",1, xFilial("C88")+C9L->C9L_GRAEXP,"C88_CODIGO"))

									If Len(cGrauExp) == 2
										cGrauExp :=	StrTran	(cGrauExp,"0","")
									EndIf

									cXml +=		"<infoAgNocivo>"
									cXml +=			xTafTag("grauExp", cGrauExp)
									cXml +=		"</infoAgNocivo>"
								EndIf

								if TafLayESoc("02_04_02")
									lV1B := .T.
									V1B->( DbSetOrder( 1 ) )
									if V1B->( MsSeek ( xFilial("V1B")+C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)))
										
										While lV1B

											cXml +=	"<infoTrabInterm>"
											cXml +=	xTafTag("codConv",Posicione("T87",4,xFilial("T87")+V1B->V1B_IDCONV+"1","T87_CONVOC"))										
											cXml +=	"</infoTrabInterm>"
											
											V1B->( dbSkip() )

											lV1B := V1B->( !Eof() ) .And. C9L->( C9L_FILIAL + C9L_ID + C9L_VERSAO + C9L_RECIBO + C9L_ESTABE + C9L_LOTACA + C9L_TRABAL ) ==;
																		xFilial("V1B") + V1B->( V1B_ID + V1B_VERSAO + V1B_RECIBO + V1B_ESTABE + V1B_LOTACA + V1B_TRABAL )
										EndDo

									endif
								endif

								cXml +=	"</remunPerApur>"

								C9L->(DbSkip())
								lC9L := !C9L->(Eof()) .And. AllTrim(C9K->(C9K_ID+C9K_VERSAO+C9K_RECIBO+C9K_ESTABE+C9K_LOTACA+C9K_TPINSC+C9K_NRINSC+C9K_CODLOT)) ==;
															AllTrim(C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TPINSC+C9L_NRINSC+C9L_CODLOT))
							EndDo
							lC9L := .T.
							cXml +=	"</ideEstabLot>"
							C9K->(DbSkip())
							lC9K := !C9K->(Eof()) .And. AllTrim(C9K->(C9K_ID+C9K_VERSAO+C9K_RECIBO)) == AllTrim(T14->(T14_ID+T14_VERSAO+T14_IDEDMD))
						EndDo
						lC9K := .T.
						cXml +=		"</infoPerApur>"
					EndIf

					/*----------------------------------------------------------
							Inicio da TAG infoPerAnt
					----------------------------------------------------------*/
					C9N->( DbSetOrder( 1 ) )
					If C9N->( MsSeek ( xFilial("C9N")+T14->(T14_ID+T14_VERSAO+T14_IDEDMD)))
						cXml +=	"<infoPerAnt>"
						While lC9N

							cXml +=	"<ideADC>"

							If C9N->C9N_TPACOR $ "ABCDE"
								cXml +=		xTafTag("dtAcConv",C9N->C9N_DTACOR)
							Else
								cXml +=		xTafTag("dtAcConv",C9N->C9N_DTACOR,,.T.)
							EndIf

							cXml +=		xTafTag("tpAcConv",C9N->C9N_TPACOR)

							cCompConv := Iif( Empty( C9N->C9N_COMPAC ), "", SubStr( C9N->C9N_COMPAC, 1, 4 ) + "-" + SubStr( C9N->C9N_COMPAC, 5, 2 ) )

							cXml +=		xTafTag( "compAcConv", cCompConv,, .T. )
							cXml +=		xTafTag( "dtEfAcConv", C9N->C9N_DTEFAC,, .T. )

							cXml +=		xTafTag("dsc",NoAcento(AnsiToOem(C9N->C9N_DSC)) )
							cXml +=		xTafTag("remunSuc",xFunTrcSN(C9N->C9N_REMUNS,1) )

							C9O->( DbSetOrder( 1 ) )
							C9O->( MsSeek ( xFilial("C9O")+C9N->(C9N_ID+C9N_VERSAO+C9N_RECIBO+DTOS(C9N_DTACOR)+C9N_TPACOR+C9N_COMPAC)))
							While lC9O
								cXml +=	"<idePeriodo>"

								If Len(Alltrim(C9O->C9O_PERREF)) <= 4
									cXml +=		xTafTag("perRef",C9O->C9O_PERREF)
								Else
									cXml +=		xTafTag("perRef",substr(C9O->C9O_PERREF, 1, 4) + '-' +  substr(C9O->C9O_PERREF, 5, 2) )
								EndIf

								C9P->( DbSetOrder( 1 ) ) //C9P_FILIAL, C9P_ID, C9P_VERSAO, C9P_RECIBO, C9P_DTACOR, C9P_TPACOR, C9P_COMPAC, C9P_PERREF, C9P_ESTABE, C9P_LOTACA, R_E_C_N_O_, D_E_L_E_T_
								C9P->( MsSeek ( xFilial("C9P")+C9O->(C9O_ID+C9O_VERSAO+C9O_RECIBO+DTOS(C9O_DTACOR)+C9O_TPACOR+C9O_COMPAC+C9O_PERREF)))
								While lC9P
									cXml +=	"<ideEstabLot>"

									If !lMV .or. Empty(C9P->C9P_CODLOT)
										cTipoEstab	:=	Posicione( "C92", 1, xFilial( "C92" ) + C9P->C9P_ESTABE, "C92_TPINSC" )
										cEstab		:=	Posicione( "C92", 1, xFilial( "C92" ) + C9P->C9P_ESTABE, "C92_NRINSC" )
										cLotacao	:=	Posicione( "C99", 1, xFilial( "C99" ) + C9P->C9P_LOTACA, "C99_CODIGO" )
									Else
										cTipoEstab	:=	C9P->C9P_TPINSC
										cEstab		:=	C9P->C9P_NRINSC
										cLotacao	:=	C9P->C9P_CODLOT
									EndIf

									cXml +=		xTafTag("tpInsc"    , cTipoEstab )
									cXml +=		xTafTag("nrInsc"    , cEstab )
									cXml +=		xTafTag("codLotacao", cLotacao )

									C9Q->( DbSetOrder( 1 ) )
									C9Q->( MsSeek ( xFilial("C9Q")+C9P->C9P_ID+C9P->C9P_VERSAO+C9P->C9P_RECIBO+DTOS(C9P->C9P_DTACOR)+C9P->C9P_TPACOR+Iif(TAFColumnPos("C9Q_COMPAC"), C9P->C9P_COMPAC, "")+C9P->C9P_PERREF+C9P->C9P_ESTABE+C9P->C9P_LOTACA))

									While lC9Q

										lGeraC9Q := .F.

										// Sa�da contorno para limita��o do tamanho de �ndice do cTree - Orienta��o FrameWork
										If lMV
											If C9Q->( C9Q_CODLOT+C9Q_TPINSC+C9Q_NRINSC ) == C9P->( C9P_CODLOT+C9P_TPINSC+C9P_NRINSC )
												lGeraC9Q := .T.
											EndIf
										Else
											lGeraC9Q := .T.
										EndIf

										If lGeraC9Q

											cEvtTrabC9Q   := POSICIONE("C9V",1, xFilial("C9V")+C9Q->C9Q_TRABAL,"C9V_NOMEVE")

											cXml +=		"<remunPerAnt>"

											If cCodCat $ "101|102|103|104|105|106|107|108|111|301|302|303|304|305|306|307|308|309"
												
												If !lMV .Or. Empty(C9Q->C9Q_DTRABA)
													cMatric := POSICIONE("C9V",1, xFilial("C9V")+C9Q->C9Q_TRABAL,"C9V_MATRIC")
													cXml += xTafTag( "matricula", cMatric,, .T. )
												Else
													cMatric := C9Q->C9Q_DTRABA
													cXml += xTafTag( "matricula", cMatric )
												EndIf

											EndIf

											cXml +=			xTafTag("indSimples",C9Q->C9Q_INDCON,,.T.)

											C9R->( DbSetOrder( 1 ) ) //C9R_FILIAL, C9R_ID, C9R_VERSAO, C9R_RECIBO, C9R_DTACOR, C9R_TPACOR, C9R_COMPAC, C9R_PERREF, C9R_ESTABE, C9R_LOTACA, C9R_TRABAL, C9R_CODRUB
											C9R->( MsSeek ( xFilial("C9R")+C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+Iif(TAFColumnPos("C9Q_COMPAC"), C9Q_COMPAC, C9P->C9P_COMPAC)+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_TRABAL)))

											While lC9R

												lGeraC9R := .F.

												// Sa�da contorno para limita��o do tamanho de �ndice do cTree - Orienta��o FrameWork
												If lMV
													If C9R->( C9R_ESTABE + C9R_CODLOT + C9R_TPINSC + C9R_NRINSC + C9R_MATRIC ) == C9Q->( C9Q_ESTABE + C9Q_CODLOT + C9Q_TPINSC + C9Q_NRINSC + C9Q_DTRABA )
														lGeraC9R := .T.
													EndIf
												Else
													lGeraC9R := .T.
												EndIf

												If lGeraC9R
													cXml +=		"<itensRemun>"

													

													If lMV .And. !Empty(C9R->C9R_RUBRIC)
														cCodRubr	:=	C9R->C9R_RUBRIC
														cIDTabRubr	:=	C9R->C9R_IDTABR

														cXml +=			xTafTag( "codRubr", cCodRubr )
														cXml +=			xTafTag( "ideTabRubr", cIDTabRubr )
													Else
														cCodRubr	:=	C9R->C9R_CODRUB
														cIDTabRubr	:=	Posicione( "C8R", 5, xFilial( "C8R" ) + cCodRubr + "1", "C8R_IDTBRU" )

														cXml +=			xTafTag( "codRubr", Posicione( "C8R", 5, xFilial( "C8R" ) + cCodRubr + "1", "C8R_CODRUB" ) )

														If lRubERPPad
															cXml +=	xTafTag( "ideTabRubr", Posicione( "T3M", 1, xFilial( "T3M" ) + cIDTabRubr, "T3M_ID" ),, .T. )
														Else
															cXml +=	xTafTag( "ideTabRubr", Posicione( "T3M", 1, xFilial( "T3M" ) + cIDTabRubr, "T3M_CODERP" ),, .T. )
														EndIf
													EndIf

													cXml +=			xTafTag("qtdRubr",C9R->C9R_QTDRUB,PesqPict("C9R","C9R_QTDRUB"),.T.,.T.)
													cXml +=			xTafTag("fatorRubr",C9R->C9R_FATORR,PesqPict("C9R","C9R_FATORR"),.T.,.T.)
													cXml +=			xTafTag("vrUnit",C9R->C9R_VLRUNT,PesqPict("C9R","C9R_VLRUNT"),.T.,.T.)
													cXml +=			xTafTag("vrRubr",C9R->C9R_VLRRUB,PesqPict("C9R","C9R_VLRRUB"),,.T.)
													cXml +=		"</itensRemun>"

													If lInfoRPT
														aRubrica := oReport:GetRubrica( cCodRubr, cIDTabRubr, C91->C91_PERAPU, lMV )

														aAdd( aAnalitico, Array( 13 ) )
														nPosValores := Len( aAnalitico )

														aAnalitico[nPosValores][ANALITICO_MATRICULA]			:=	AllTrim( cMatric )
														aAnalitico[nPosValores][ANALITICO_CATEGORIA]			:=	AllTrim( cCodCat )
														aAnalitico[nPosValores][ANALITICO_TIPO_ESTABELECIMENTO]	:=	AllTrim( cTipoEstab )
														aAnalitico[nPosValores][ANALITICO_ESTABELECIMENTO]		:=	AllTrim( cEstab )
														aAnalitico[nPosValores][ANALITICO_LOTACAO]				:=	AllTrim( cLotacao )
														aAnalitico[nPosValores][ANALITICO_NATUREZA]				:=	AllTrim( aRubrica[1] ) //Natureza
														aAnalitico[nPosValores][ANALITICO_TIPO_RUBRICA]			:=	AllTrim( aRubrica[2] ) //Tipo
														aAnalitico[nPosValores][ANALITICO_INCIDENCIA_CP]		:=	AllTrim( aRubrica[3] ) //Incid�ncia CP
														aAnalitico[nPosValores][ANALITICO_INCIDENCIA_IRRF]		:=	AllTrim( aRubrica[4] ) //Incid�ncia IRRF
														aAnalitico[nPosValores][ANALITICO_INCIDENCIA_FGTS]		:=	AllTrim( aRubrica[5] ) //Incid�ncia FGTS
														aAnalitico[nPosValores][ANALITICO_DECIMO_TERCEIRO]		:=	""
														aAnalitico[nPosValores][ANALITICO_TIPO_VALOR]			:=	""
														aAnalitico[nPosValores][ANALITICO_VALOR]				:=	C9R->C9R_VLRRUB
													EndIf

												EndIf

												C9R->(DbSkip())

												If lMV
													lC9R := !C9R->(Eof()) .And. AllTrim(C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+C9Q_PERREF)) ==;
													AllTrim(C9R->(C9R_ID+C9R_VERSAO+C9R_RECIBO+DTOS(C9R_DTACOR)+C9R_TPACOR+C9R_PERREF))
												Else 
													lC9R := !C9R->(Eof()) .And. AllTrim(C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_TRABAL)) == AllTrim(C9R->(C9R_ID+C9R_VERSAO+C9R_RECIBO+DTOS(C9R_DTACOR)+C9R_TPACOR+C9R_PERREF+C9R_ESTABE+C9R_LOTACA+C9R_TRABAL))	
												EndIf

											EndDo


											lC9R := .T.

											If !Empty(C9Q->C9Q_GRAEXP)

												cGrauExp := AllTrim(POSICIONE("C88",1, xFilial("C88")+C9Q->C9Q_GRAEXP,"C88_CODIGO"))

												If Len(cGrauExp) == 2
													cGrauExp :=	StrTran	(cGrauExp,"0","")
												EndIf

												cXml +=		"<infoAgNocivo>"
												cXml +=			xTafTag("grauExp", cGrauExp)
												cXml +=		"</infoAgNocivo>"
											EndIf

											if TafLayESoc("02_04_02")
												V1C->( DbSetOrder( 1 ) )
												if V1C->( MsSeek ( xFilial("V1C")+C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+'      '+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_TRABAL)))
													cXml +=	"<infoTrabInterm>"
													While lV1C
														cXml +=	xTafTag("codConv",Posicione("T87",4,xFilial("T87")+V1C->V1C_IDCONV+"1","T87_CONVOC"))


														V1C->(DbSkip())
														lV1C := !V1C->(Eof()) .And. AllTrim(C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_TRABAL)) == AllTrim(V1C->(V1C_ID+V1C_VERSAO+V1C_RECIBO+DTOS(V1C_DTACOR)+V1C_TPACOR+V1C_PERREF+V1C_ESTABE+V1C_LOTACA+V1C_TRABAL))
													EndDo
													lV1C := .T.
													cXml +=	"</infoTrabInterm>"
												endif
											endif

											cXml +=	"</remunPerAnt>"

										EndIf

										C9Q->(DbSkip())

										If !lMV
											lC9Q := !C9Q->(Eof()) .And. AllTrim(C9P->(C9P_ID+C9P_VERSAO+C9P_RECIBO+DTOS(C9P_DTACOR)+C9P_TPACOR+C9P_PERREF+C9P_ESTABE+C9P_LOTACA)) == AllTrim(C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA))
										Else
											lC9Q := !C9Q->(Eof()) .And. AllTrim(C9P->(C9P_ID+C9P_VERSAO+C9P_RECIBO+DTOS(C9P_DTACOR)+C9P_TPACOR+C9P_PERREF+C9P_ESTABE+C9P_LOTACA+C9P_CODLOT+C9P_TPINSC+C9P_NRINSC)) ==;
											AllTrim(C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_CODLOT+C9Q_TPINSC+C9Q_NRINSC))
										EndIf

									EndDo

									lC9Q := .T.
									cXml +=	"</ideEstabLot>"
									C9P->(DbSkip())
									lC9P := !C9P->(Eof()) .And. AllTrim(C9O->(C9O_ID+C9O_VERSAO+C9O_RECIBO+DTOS(C9O_DTACOR)+C9O_TPACOR+C9O_PERREF)) == AllTrim(C9P->(C9P_ID+C9P_VERSAO+C9P_RECIBO+DTOS(C9P_DTACOR)+C9P_TPACOR+C9P_PERREF))

								EndDo
								lC9P := .T.
								cXml +=	"</idePeriodo>"
								C9O->(DbSkip())
								lC9O := !C9O->(Eof()) .And.  AllTrim(C9N->(C9N_ID+C9N_VERSAO+C9N_RECIBO+DTOS(C9N_DTACOR)+C9N_TPACOR+C9N_COMPAC)) == AllTrim(C9O->(C9O_ID+C9O_VERSAO+C9O_RECIBO+DTOS(C9O_DTACOR)+C9O_TPACOR+C9O_COMPAC))
							EndDo
							lC9O := .T.
							cXml += "</ideADC>"
							C9N->(Dbskip())
							lC9N := !C9N->(Eof()) .And. AllTrim(T14->(T14_ID+T14_VERSAO+T14_IDEDMD)) == AllTrim(C9N->(C9N_ID+C9N_VERSAO+C9N_RECIBO))
						EndDo
						lC9N := .T.
						cXml += "</infoPerAnt>"
					EndIf

					If !TafLayESoc("02_04_02")
						T89->(dbSetOrder(1))
						If T89->(MsSeek(xFilial("T89")+T14->(T14_ID+T14_VERSAO+T14_IDEDMD)))
							cXml += "<infoTrabInterm>"
							cXml += 	xTafTag("codConv",Posicione("T87",4,xFilial("T87")+T89->T89_IDCONV+"1","T87_CONVOC"))
							cXml += "</infoTrabInterm>"
						EndIf
					EndIf

					If TafLayESoc("02_04_02")
						/*
						DSERTAF1-4588 - 16-05-2018
						Altera��o da C91 p/ a T14 em virtude
						da altera��o no layout do E-Social na vers�o 2.4.02.
						*/
						If T14->(FieldPos("T14_CODCBO")) <> 0 .And. T14->(FieldPos("T14_NATATV")) <> 0 .And. T14->(FieldPos("T14_QTDTRB")) <> 0
							xTafTagGroup( "infoComplCont"	, {	{ "codCBO", Posicione( "C8Z", 1, xFilial( "C8Z" ) + T14->T14_CODCBO, "C8Z_CODIGO" ),, .F. };
								, 	{ "natAtividade", T14->T14_NATATV,, .T. };
								, 	{ "qtdDiasTrab",  T14->T14_QTDTRB,, .T. }};
								, @cXml )
						EndIf
					EndIf
					cXml +=	"</dmDev>"

					T14->(DbSkip())
				EndDo
			EndIf
		EndIf
	EndIf

	// Verifica o "TAUTO" informado diretamente na folha
	If !lInfoCompl .And. lMV .And. !Empty(C91->C91_NOME)
		lInfoCompl := .T.
	EndIf
	//E SOMENTE PARA S2200 COM remunSuc = 's' E TAUTO
	If lInfoCompl .OR. (cRemuns == '1' .AND. cEvent == "S2200")
		cXml := StrTran( cXml, "FlagInfoComplem", cXmlCompl )
	Else
		cXml := StrTran( cXml, "FlagInfoComplem", "" )
	EndIf

	//����������������������Ŀ
	//�Estrutura do cabecalho�
	//������������������������
	If nRecnoSM0 > 0
		SM0->(dbGoto(nRecnoSM0))
	endif
	cXml := xTafCabXml(cXml,"C91",cLayout,cReg,aMensal)

	//����������������������������Ŀ
	//�Executa gravacao do registro�
	//������������������������������
	If !lJob 
		xTafGerXml(cXml,cLayout)
	EndIf

	If lInfoRPT
		If C91->C91_EVENTO $ "I|A"
			oInfoRPT := oReport:oVOReport
			oInfoRPT:SetIndApu( AllTrim( C91->C91_INDAPU ) )
			oInfoRPT:SetPeriodo( AllTrim( C91->C91_PERAPU ) )
			oInfoRPT:SetCPF( AllTrim( cCPF ) )
			oInfoRPT:SetNome( AllTrim( cNome ) )
			oInfoRPT:SetAnalitico( aAnalitico )

			oReport:UpSert( "S-1200", "2", xFilial( "C91" ), oInfoRPT )
		EndIf
	EndIf

	TAFEncArr(aAuxPerAnt)


cFilAnt := cFilBkp
EndIf

Return(cXml)

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF250Grv
@type			function
@description	Fun��o de grava��o para atender o registro S-1200.
@author			Vitor Siqueira
@since			08/01/2016
@version		1.0
@param			cLayout		-	Nome do Layout que est� sendo enviado
@param			nOpc		-	Op��o a ser realizada ( 3 = Inclus�o, 4 = Altera��o, 5 = Exclus�o )
@param			cFilEv		-	Filial do ERP para onde as informa��es dever�o ser importadas
@param			oXML		-	Objeto com as informa��es a serem manutenidas ( Outras Integra��es )
@param			cOwner
@param			cFilTran
@param			cPredeces
@param			nTafRecno
@param			cComplem
@param			cGrpTran
@param			cEmpOriGrp
@param			cFilOriGrp
@param			cXmlID		-	Atributo Id, �nico para o XML do eSocial. Utilizado para importa��o de dados de clientes migrando para o TAF
@return			lRet		-	Vari�vel que indica se a importa��o foi realizada, ou seja, se as informa��es foram gravadas no banco de dados
@param			aIncons		-	Array com as inconsist�ncias encontradas durante a importa��o
/*/
//-------------------------------------------------------------------
Function TAF250Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID, cEvtOri, lMigrador, lDepGPE, cKey )

Local cLogOpeAnt	:=	""
Local oModel		:=	Nil
Local oModelT14		:=	Nil
Local oModelC9K		:=	Nil
Local oModelC9L		:=	Nil
Local oModelC9M		:=	Nil
Local oModelT6Y		:=	Nil
Local oModelT6Z		:=	Nil
Local oModelC9N		:=	Nil
Local oModelC9O		:=	Nil
Local oModelC9P		:=	Nil
Local oModelC9Q		:=	Nil
Local oModelC9R		:=	Nil
Local oModelT89		:=	Nil
Local oModelV1B		:=	Nil
Local oModelV1C		:=	Nil
Local cEnter		:=	Chr( 13 ) + Chr( 10 )
Local cCmpsNoUpd	:=	"|C91_FILIAL|C91_ID|C91_VERSAO|C91_VERANT|C91_PROTPN|C91_EVENTO|C91_STATUS|C91_ATIVO|"
Local cMatricC9L	:=	""
Local cMatricC9Q	:=	""
Local cMensagem		:=	""
Local cT6WPath		:=	""
Local cT14Path		:=	""
Local cC9KPath		:=	""
Local cC9LPath		:=	""
Local cC9MPath		:=	""
Local cT6YPath		:=	""
Local cT6ZPath		:=	""
Local cC9NPath		:=	""
Local cC9OPath		:=	""
Local cC9PPath		:=	""
Local cC9QPath		:=	""
Local cC9RPath		:=	""
Local cCRNPath		:=	""
Local cT89Path		:=	""
Local cV1BPath		:=	""
Local cOwExi		:=  ""
Local cNewOw		:=  ""
Local cV1CPath		:=	""
Local cPeriodo		:=	""
Local cChave		:=	""
Local cInconMsg		:=	""
Local cIdProc		:=	""
Local cIdConv		:=	""
Local cIdTrab		:=	""
Local cCodConv		:=	""
Local cCodEvent		:=	Posicione( "C8E", 2, xFilial( "C8E" ) + "S-" + cLayout, "C8E->C8E_ID" )
Local nT6W			:=	0
Local nT14			:=	0
Local nC9K			:=	0
Local nC9L			:=	0
Local nC9M			:=	0
Local nT6Y			:=	0
Local nT6Z			:=	0
Local nC9N			:=	0
Local nC9O			:=	0
Local nC9P			:=	0
Local nC9Q			:=	0
Local nC9R			:=	0
Local nCRN			:=	0
Local nT89			:=	0
Local nV1B			:=	0
Local nV1C			:=	0
Local nJ			:=	0
Local nlI			:=	0
Local nC9L_TRABAL	:=	0
Local nC9Q_TRABAL	:=	0
Local nSeqErrGrv	:=	0
Local nT6Z_SEQUEN	:=	TamSX3( "T6Z_SEQUEN" )[1]
Local aIncons		:=	{}
Local aRules		:=	{}
Local aChave		:=	{}
Local aRecibo		:=	{}
Local lRet			:=	.F.
Local cIdTabR		:= ""
Local cCodRubr      := ""
Local lInfoCompl	:=	.F.
Local lMultVinc		:=	.F.
Local lExistTrab	:=	.F.
Local lFindTrab		:=	.F.
Local nChvIndex		:=	0
Local cIdC9V        := "" // Variavel utilizada apenas para verificar se o trabalhador possui um registro que seja diferente de TAUTO, com o intuito de evitar a gera��o da tag infoComplem.
Local cPathPerAnt   := "/eSocial/evtRemun/dmDev[1]/infoPerAnt/ideADC[1]/idePeriodo[1]/ideEstabLot[1]/remunPerAnt[1]"
Local cPathPerApu   := "/eSocial/evtRemun/dmDev[1]/infoPerApur/ideEstabLot[1]/remunPerApur"
Local aInfComp      := {}
Local aAuxInfComp   := {}
Local cIDCat		:= ""
Local cIdConvT      := ""
Local cTpLot		:= ""

Local xChkDupl      := {}

//Relat�rio de Confer�ncia de Valores
Local oInfoRPT		:=	Nil
Local cNome			:=	""
Local nPosValores	:=	0
Local aAnalitico	:=	{}
Local aRubrica		:=	{}
Local lInfoRPT		:=	.F.

Local lNovCenar		:= .F.
Local oMdlNvCen		:= Nil

Local cNomEvC9V		:= ""

Private lVldModel	:=	.T. //Caso a chamada seja via integra��o, seto a vari�vel de controle de valida��o como .T.
Private oDados		:=	Nil

Default cLayout		:=	""
Default nOpc		:=	1
Default cFilEv		:=	""
Default oXML		:=	Nil
Default cOwner		:=	""
Default cKey		:=  ""
Default cGrpTran	:=	""
Default cFilTran	:=	""
Default cPredeces	:=	""
Default nTafRecno	:=	0
Default cComplem	:=	""
Default cGrpTran	:=	""
Default cEmpOriGrp	:=	""
Default cFilOriGrp	:=	""
Default cXmlID		:=	""

/*
DSERTAF1-4588
Removido, pois independente do ambiente estar atualizado
integra��o n�o ocorria solicitando a atualiza��o do ambiente.
cMensagem := STR0057 + cEnter // #"Dicion�rio Incompat�vel"
cMensagem += TafAmbInvMsg()
Aadd(aIncons, cMensagem)
*/

If __lGrvRPT == Nil
	TAF250Rpt() //Inicializa a vari�vel static __lGrvRPT
EndIf

lInfoRPT := __lGrvRPT

If lInfoRPT
	If oReport == Nil
		oReport := TAFSocialReport():New()
	EndIf
EndIf

oDados := oXML

//�����������������Ŀ
//�Chave do registro�
//�������������������
cPeriodo  := FTafGetVal( "/eSocial/evtRemun/ideEvento/perApur", "C", .F., @aIncons, .F. )

Aadd( aChave, {"C", "C91_INDAPU", FTafGetVal( "/eSocial/evtRemun/ideEvento/indApuracao", "C", .F., @aIncons, .F. )  , .T.} )
cChave += Padr( aChave[ 1, 3 ], Tamsx3( aChave[ 1, 2 ])[1])

If At("-", cPeriodo) > 0
	Aadd( aChave, {"C", "C91_PERAPU", StrTran(cPeriodo, "-", "" ),.T.} )
	cChave += Padr( aChave[ 2, 3 ], Tamsx3( aChave[ 2, 2 ])[1])
Else
	Aadd( aChave, {"C", "C91_PERAPU", cPeriodo  , .T.} )
	cChave += Padr( aChave[ 2, 3 ], Tamsx3( aChave[ 2, 2 ])[1])
EndIf

If oDados:XPathHasNode("/eSocial/evtRemun/ideTrabalhador/infoComplem")
	if TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/nmTrab" ) )
		cNome := FTafGetVal( "/eSocial/evtRemun/ideTrabalhador/infoComplem/nmTrab", "C", .F.,, .F. )
		aadd( aInfComp, { 'C9V_NOME', cNome } )
	EndIf
	if TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/dtNascto" ) )
		aadd( aInfComp, { 'C9V_DTNASC', FTafGetVal( "/eSocial/evtRemun/ideTrabalhador/infoComplem/dtNascto","D", .F., , .F.) } )
	endif

	If TafLayESoc("02_05_00")
		If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/tpInscAnt" ) )
			aAdd( aInfComp, { "CUP_INSANT", FTAFGetVal( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/tpInscAnt", "C", .F.,, .F. ) } )
		EndIf
	EndIf

	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/cnpjEmpregAnt" ) )
		aAdd( aInfComp, { "CUP_CNPJEA", FTAFGetVal( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/cnpjEmpregAnt", "C", .F.,, .F. ) } )
	EndIf

	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/matricAnt" ) )
		aAdd( aInfComp, { "CUP_MATANT", FTAFGetVal( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/matricAnt", "C", .F.,, .F. ) } )
	EndIf

	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/dtAdm" ) )
		aAdd( aInfComp, { "CUP_DTINVI", FTAFGetVal( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/dtAdm", "D", .F.,, .F. ) } )
	EndIf

	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/observacao" ) )
		aAdd( aInfComp, { "CUP_OBSVIN", FTAFGetVal( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/observacao", "M", .F.,, .F. ) } )
	EndIf

	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/cpfTrab" ) )
		aadd( aInfComp, { 'C9V_CPF', FTafGetVal( "/eSocial/evtRemun/ideTrabalhador/cpfTrab","C", .F., , .F.) } )
	EndIf

	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/nisTrab" ) )
		aadd( aInfComp, { 'C9V_NIS', FTafGetVal( "/eSocial/evtRemun/ideTrabalhador/nisTrab","C", .F., , .F.) } )
	EndIf
	
	aAuxInfComp := aClone( aInfComp )
EndIf

If     (oDados:XPathHasNode(cPathPerApu + "/matricula")) .And. !Empty(FTafGetVal( cPathPerApu + "/matricula","C", .F., , .F.)) .And. AllTrim( Upper( cComplem ) ) != "MV"
	cIdC9V   := FGetIdInt( "matricula", "", FTafGetVal( cPathPerApu + "/matricula", "C", .F., @aIncons, .F. ), , .F.,,@cInconMsg, @nSeqErrGrv)
	
ElseIf (oDados:XPathHasNode(cPathPerAnt + "/matricula")) .And. !Empty(FTafGetVal( cPathPerAnt + "/matricula","C", .F., , .F.)) .And. AllTrim( Upper( cComplem ) ) != "MV"
	cIdC9V   := FGetIdInt( "matricula", "", FTafGetVal( cPathPerAnt + "/matricula", "C", .F., @aIncons, .F. ), , .F.,,@cInconMsg, @nSeqErrGrv)

Else

	cIDCat := FGetIdInt( "codCateg", "", fTafGetVal( "/eSocial/evtRemun/dmDev/codCateg", "C", .F., @aIncons, .F. ), , .F.,,@cInconMsg, @nSeqErrGrv)

	cIdC9V   := TAFGetIdFunc( fTafGetVal( "/eSocial/evtRemun/ideTrabalhador/cpfTrab", "C", .F., @aIncons, .F. ), cPeriodo, Nil, "cpfTrab", "/eSocial/evtRemun/ideTrabalhador/cpfTrab", aInfComp, fTafGetVal( "/eSocial/evtRemun/dmDev/infoPerApur/ideEstabLot/remunPerApur/matricula", "C", .F., @aIncons, .F. ), cIDCat )
	aInfComp := aClone( aAuxInfComp )
Endif

cNomEvC9V := POSICIONE("C9V",2, xFilial("C9V")+cIdC9V+"1","C9V_NOMEVE")

If TAF250DicMV( .F. )

	Aadd( aChave, {"C", "C91_CPF", fTafGetVal( "/eSocial/evtRemun/ideTrabalhador/cpfTrab", "C", .F., @aIncons, .F. ), .T.} )

	cChave += Padr( aChave[ 3, 3 ], Tamsx3(aChave[ 3, 2 ])[1])
	nChvIndex := 7
	
	Aadd( aChave, {"C", "C91_NOMEVE", "S1200" , .T.} )
	
	// --> Sempre grava aberto !!
	If AllTrim( Upper( cComplem ) ) == "MV"
		// N�o dever� gerar a tag infoComplem caso o funcion�rio tenha um S2200 ou um S2300 transmitido ao RET
		lInfoCompl	:= oXML:XPathHasNode( '/eSocial/evtRemun/ideTrabalhador/infoComplem' ) .And. (Empty(cIdC9V) .Or. AllTrim(Posicione("C9V",2,xFilial("C9V") + Padr( cIdC9V, Tamsx3("C9V_ID")[1]) + '1', "C9V_NOMEVE" )) == "TAUTO")
		lMultVinc := .T.
	Else
		
		lMultVinc 	:= Taf250MV( oXML, @lInfoCompl, cChave, nChvIndex, cFilEv, @aIncons )
		
		If !lMultVinc
			cIdTrab := cIdC9V
		EndIf
	EndIf
	
	If !Empty( cIDTrab )
		lExistTrab 	:= .T.
		cNome := GetADVFVal( "C9V", "C9V_NOME", xFilial( "C9V" ) + cIDTrab, 1, "", .T. )
	EndIf

	//Verifica se o evento ja existe na base
	("C91")->( DbSetOrder( nChvIndex ) ) // C91_FILIAL+C91_INDAPU+C91_PERAPU+C91_CPF+C91_NIS+C91_NOMEVE+C91_ATIVO
	If ("C91")->( MsSeek(FTafGetFil(cFilEv,@aIncons,"C91") + cChave + 'S1200' +'1' ) )
		
		If !C91->C91_STATUS $ ( "2|4|6|" )
			nOpc := 4
		ElseIf lMultVinc .AND. C91->C91_STATUS == "4" .AND. FTafGetVal("/eSocial/evtRemun/ideEvento/indRetif", "C", .F.,, .F. ) == "1"
			nOpc := 4
			lNovCenar := .T.
			oMdlNvCen := FWLoadModel( 'TAFA250' )
			oMdlNvCen:Activate()
		EndIf
		
	Elseif nOpc == 4

		/* 
		Trecho inserido para o tratamento de retifica��es do legado no qual o campo C91_CPF e C91_NIS ainda n�o eram preenchidos 
		sempre. DSERTAF1-18945
		*/

		aDel(aChave, Len(aChave))
		aDel(aChave, Len(aChave))

		aSize(aChave, 2)

		Aadd( aChave, {"C", "C91_TRABAL", cIdTrab, .T.} )
		cChave += Padr( aChave[ 3, 3 ], Tamsx3(aChave[ 3, 2 ])[1])

		nChvIndex := 2

		Aadd( aChave, {"C", "C91_NOMEVE", "S1200" , .T.} )

		("C91")->( DbSetOrder( nChvIndex ) ) // C91_FILIAL+C91_INDAPU+C91_PERAPU+C91_CPF+C91_NIS+C91_NOMEVE+C91_ATIVO
		If ("C91")->( MsSeek(FTafGetFil(cFilEv,@aIncons,"C91") + cChave + 'S1200' +'1' ) )
			If !C91->C91_STATUS $ ( "2|4|6|" )
				nOpc := 4
			EndIf
		EndIf
		

	EndIf

	Begin Transaction
		//�������������������������������������������������������������Ŀ
		//�Funcao para validar se a operacao desejada pode ser realizada�
		//���������������������������������������������������������������
		If FTafVldOpe( 'C91', nChvIndex, @nOpc, cFilEv, @aIncons, aChave, @oModel, 'TAFA250', cCmpsNoUpd )

			If TafColumnPos( "C91_LOGOPE" )
				cLogOpeAnt := C91->C91_LOGOPE
			endif
			

			oModel:LoadValue( "MODEL_C91", "C91_NOMEVE", "S1200" )

			If lMultVinc
				oModel:LoadValue( "MODEL_C91", "C91_MV", "1" )
			else
				oModel:SetValue( "MODEL_C91", "C91_MV", "2" )
			EndIf

			//���������������������������������������������������������������Ŀ
			//�Carrego array com os campos De/Para de gravacao das informacoes�
			//�����������������������������������������������������������������
			aRules := TAF250Rul( @cInconMsg, @nSeqErrGrv, cCodEvent, cOwner, lMultVinc, lExistTrab, @lFindTrab, lInfoCompl, aInfComp, cIDCat, cIdTrab, cNomEvC9V )

			// O TAUTO � gerado dentro da fun��o acima e quando ele for gerado � necess�rio validar as informa��es dos cadastros predecessores.
			If lFindTrab
				lExistTrab := .T.
			EndIf

			//����������������������������������������������������������������Ŀ
			//�Quando se tratar de uma Exclusao direta apenas preciso realizar �
			//�o Commit(), nao eh necessaria nenhuma manutencao nas informacoes�
			//������������������������������������������������������������������
			If nOpc <> 5

				/*-----------------------------------------
				C91 - Informa��es de Folha de Pagamento
				-------------------------------------------*/

				oModel:LoadValue( "MODEL_C91", "C91_INDAPU", C91->C91_INDAPU)
				oModel:LoadValue( "MODEL_C91", "C91_PERAPU", C91->C91_PERAPU)
				oModel:LoadValue( "MODEL_C91", "C91_TRABAL", C91->C91_TRABAL)
				oModel:LoadValue( "MODEL_C91", "C91_CPF   ", C91->C91_CPF   )
				oModel:LoadValue( "MODEL_C91", "C91_NIS   ", C91->C91_NIS   )
				
				//tratamento para os campos tafckey e owner
				if TafColumnPos("C91_TAFKEY") .AND.TafColumnPos("C91_OWNER")
				//Pegar valor do owner
						cOwExi := C91->C91_OWNER
		
						oModel:LoadValue( "MODEL_C91", "C91_TAFKEY ", cKey  )

						If Empty(cOwExi) .AND. !Empty(cOwner)
							oModel:LoadValue( "MODEL_C91", "C91_OWNER ", cOwner  )
						else
							if !cOwner $(cOwExi) .AND.!Empty(cOwner)
									cNewOw := Alltrim(cOwExi) + " | " + cOwner
									oModel:LoadValue( "MODEL_C91", "C91_OWNER ", cNewOw )
							endif
						endif
				EndIf

				If TAFColumnPos( "C91_XMLID" )
					oModel:LoadValue( "MODEL_C91", "C91_XMLID", cXmlID )
				EndIf

				//����������������������������������������Ŀ
				//�Rodo o aRules para gravar as informacoes�
				//������������������������������������������
				For nlI := 1 To Len( aRules )
					oModel:LoadValue( "MODEL_C91", aRules[ nlI, 01 ], FTafGetVal( aRules[ nlI, 02 ], aRules[nlI, 03], aRules[nlI, 04], @aIncons, .F. ) )
				Next

				If Findfunction("TAFAltMan")
					if nOpc == 3
						TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_C91', 'C91_LOGOPE' , '1', '' )
					elseif nOpc == 4
						TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_C91', 'C91_LOGOPE' , '', cLogOpeAnt )
					EndIf
				EndIf
				
				If lNovCenar 
					TafCrrMdl(oMdlNvCen, @oModel, C91->C91_ID, C91->C91_VERSAO )
				EndIf
				
				/*------------------------------------------
					Informa��es do periodo de Apura��o
					T14 - Informa��es do Recibo de Pag.
				--------------------------------------------*/
				nT14 := 1
				cT14Path := "/eSocial/evtRemun/dmDev[" + CVALTOCHAR(nT14) + "]"

				cChvFil  := xFilial("C91")
				cChvId   := C91->C91_ID
				cChvVer  := C91->C91_VERSAO
				cChvInd  := Space(TamSX3("T14_INDAPU")[1])
				cChvPer  := Space(TamSX3("T14_PERAPU")[1])
				cChvTrab := Space(TamSX3("T14_TRABAL")[1])				

				oModelT14 := oModel:GetModel( 'MODEL_T14' )
				
				/*If nOpc == 4 .And. !lMultVinc
					For nJ := 1 to oModel:GetModel( 'MODEL_T14' ):Length()
						oModel:GetModel( 'MODEL_T14' ):GoLine(nJ)
						oModel:GetModel( 'MODEL_T14' ):DeleteLine()
					Next nJ
				EndIf*/

				While oDados:XPathHasNode(cT14Path)
					
					oModel:GetModel( 'MODEL_T14' ):LVALID	:= .T.

					cChvIde  := PadR( FTafGetVal( cT14Path + "/ideDmDev", "C", .F., @aIncons, .F. ), TamSX3( "T14_IDEDMD" )[1] )

					//---------------------------------------------------------------------------------------------------------------
					//Caso encontre a chave do registro na tabela fa�o a altera��o, caso contr�rio apenas incluo a linha nova na GRID
					//---------------------------------------------------------------------------------------------------------------
					If !oModelT14:SeekLine( { {"T14_FILIAL", cChvFil } , {"T14_ID", cChvId } ,{"T14_VERSAO", cChvVer },{"T14_IDEDMD", cChvIde },{"T14_INDAPU", cChvInd },{"T14_PERAPU", cChvPer },{"T14_TRABAL", cChvTrab } } )
						If !oModelT14:IsEmpty()
							oModelT14:AddLine()
						EndIf
					Else
						oModelT14:DeleteLine()
						oModelT14:AddLine()
					EndIf

					if oDados:XPathHasNode( cT14Path + "/ideDmDev")
						oModel:LoadValue( "MODEL_T14", "T14_IDEDMD",  FTafGetVal( cT14Path + "/ideDmDev", "C", .F., @aIncons, .F. ) )
					EndIf

					if oDados:XPathHasNode(	cT14Path + "/codCateg")
						oModel:LoadValue( "MODEL_T14", "T14_CODCAT" , FGetIdInt( "codCateg", "",  cT14Path + "/codCateg",,,,@cInconMsg, @nSeqErrGrv))
					EndIf

					If oDados:XPathHasNode(	cT14Path + "/infoComplCont/codCBO")
						oModel:LoadValue( "MODEL_T14", "T14_CODCBO" , FGetIdInt( "codCBO", "",  cT14Path + "/infoComplCont/codCBO",,,,@cInconMsg, @nSeqErrGrv))
					EndIf

					If oDados:XPathHasNode( cT14Path + "/infoComplCont/natAtividade")
						oModel:LoadValue( "MODEL_T14", "T14_NATATV",  FTafGetVal( cT14Path + "/infoComplCont/natAtividade", "C", .F., @aIncons, .F. ) )
					EndIf

					If oDados:XPathHasNode( cT14Path + "/infoComplCont/qtdDiasTrab")
						oModel:LoadValue( "MODEL_T14", "T14_QTDTRB",  FTafGetVal( cT14Path + "/infoComplCont/qtdDiasTrab", "C", .F., @aIncons, .F. ) )
					EndIf

					/*------------------------------------------
						C9K - Informa��es do Estab./Lota��o
					--------------------------------------------*/
					nC9K:= 1
					cC9KPath := cT14Path + "/infoPerApur/ideEstabLot[" + CVALTOCHAR(nC9K) + "]"

					While oDados:XPathHasNode(cC9KPath)

						oModelC9K := oModel:GetModel( 'MODEL_C9K' )
						oModel:GetModel( 'MODEL_C9K' ):LVALID	:= .T.

						If nC9K > 1
							oModel:GetModel( 'MODEL_C9K' ):AddLine()
						EndIf

						If !lMultVinc .And. lExistTrab
							cTpLot := GetADVFVal("C99", "C99_TPLOT", xFilial("C99") + FGetIdInt( "codLotacao", "", cC9KPath + "/codLotacao",,,,@cInconMsg, @nSeqErrGrv) + '1', 4, "")
							If !(cTpLot $ "000002")   
							
								if oDados:XPathHasNode(	cC9KPath + "/tpInsc" , cC9KPath + "/nrInsc" )
									oModel:LoadValue( "MODEL_C9K", "C9K_ESTABE", FGetIdInt( "tpInsc", "nrInsc", cC9KPath + "/tpInsc" , cC9KPath + "/nrInsc",,,@cInconMsg, @nSeqErrGrv) )
								EndIf

							Else
							
								if oDados:XPathHasNode(	cC9KPath + "/tpInsc" )
									oModel:LoadValue( "MODEL_C9K", "C9K_TPINSC", FTafGetVal( cC9KPath + "/tpInsc", "C", .F., @aIncons, .F. ) )
								EndIf

								if oDados:XPathHasNode(	cC9KPath + "/nrInsc" )
									oModel:LoadValue( "MODEL_C9K", "C9K_NRINSC", FTafGetVal( cC9KPath + "/nrInsc", "C", .F., @aIncons, .F. ) )
								EndIf	
								
								if oDados:XPathHasNode(	cC9KPath + "/codLotacao" )
									oModel:LoadValue( "MODEL_C9K", "C9K_CODLOT", FTafGetVal( cC9KPath + "/codLotacao", "C", .F., @aIncons, .F. ) )
								EndIf				
							
							EndIf

							if oDados:XPathHasNode(	cC9KPath + "/codLotacao" )
								oModel:LoadValue( "MODEL_C9K", "C9K_LOTACA", FGetIdInt( "codLotacao", "", cC9KPath + "/codLotacao",,,,@cInconMsg, @nSeqErrGrv) )
							EndIf

						Else
							if oDados:XPathHasNode(	cC9KPath + "/codLotacao" )
								oModel:LoadValue( "MODEL_C9K", "C9K_CODLOT", FTafGetVal( cC9KPath + "/codLotacao", "C", .F., @aIncons, .F. ) )
							EndIf

							if oDados:XPathHasNode(	cC9KPath + "/tpInsc" )
								oModel:LoadValue( "MODEL_C9K", "C9K_TPINSC", FTafGetVal( cC9KPath + "/tpInsc", "C", .F., @aIncons, .F. ) )
							EndIf

							if oDados:XPathHasNode(	cC9KPath + "/nrInsc" )
								oModel:LoadValue( "MODEL_C9K", "C9K_NRINSC", FTafGetVal( cC9KPath + "/nrInsc", "C", .F., @aIncons, .F. ) )
							EndIf

						EndIf

						if oDados:XPathHasNode(cC9KPath + "/qtdDiasAv" )
							oModel:LoadValue( "MODEL_C9K", "C9K_QTDDIA", FTafGetVal( cC9KPath + "/qtdDiasAv", "C", .F., @aIncons, .F. ) )
						EndIf

						/*------------------------------------------
							C9L - Informa��es da Remunera��o Trab.
						--------------------------------------------*/
						nC9L:= 1
						cC9LPath := cC9KPath+"/remunPerApur[" + CVALTOCHAR(nC9L) + "]"
						While oDados:XPathHasNode(cC9LPath)

							oModelC9L := oModel:GetModel( 'MODEL_C9L' )
							oModel:GetModel( 'MODEL_C9L' ):LVALID	:= .T.

							If nC9L > 1
								oModel:GetModel( 'MODEL_C9L' ):AddLine()
							EndIf

							if oDados:XPathHasNode(	cC9LPath + "/infoAgNocivo/grauExp" )
								oModel:LoadValue( "MODEL_C9L", "C9L_GRAEXP", FGetIdInt( "grauExp", "", cC9LPath + "/infoAgNocivo/grauExp",,,,@cInconMsg, @nSeqErrGrv))
							EndIf

							if oDados:XPathHasNode(	cC9LPath + "/indSimples" )
								oModel:LoadValue( "MODEL_C9L", "C9L_INDCON", FTafGetVal( cC9LPath + "/indSimples", "C", .F., @aIncons, .F. ) )
							EndIF

							//Rodrigo Aguilar - 23/05/2017
							//Se existir a TAG sei que se trata de um trabalhador autonomo, sendo assim fa�o o seek
							//na tabela de trabalhador autonomo, para manter a integridade entre as tabelas PAI e FILHO
							//no cadastro
							if oDados:XPathHasNode("/eSocial/evtRemun/ideTrabalhador/infoComplem") .and. ;
									oDados:XPathHasNode("/eSocial/evtRemun/ideTrabalhador/cpfTrab")

								// --> Somente se n�o possuir m�ltiplos v�nculos. Caso existe mv, ent�o utiliza as informa��es do XML ao inv�s de pegar o ID do trabalhador sem v�nculo
								If !lMultVinc
									nC9L_TRABAL := cIdTrab
								Endif

							//Rodrigo Aguilar - 23/05/2017
							//Caso n�o seja um trabalhador autonomo fa�o o seek no trabalhador com a matricula para
							//preencher a tabela
							elseif oDados:XPathHasNode(	cC9LPath + "/matricula" )
								cMatricC9L := FTafGetVal( cC9LPath + "/matricula", "C", .F., @aIncons, .F. )

								// --> Somente se n�o possuir m�ltiplos v�nculos. Caso existe mv, ent�o utiliza as informa��es do XML ao inv�s de pegar o ID do trabalhador
								If !lMultVinc
									nC9L_TRABAL := FGetIdInt( "matricula", "", cMatricC9L, , .F.,,@cInconMsg, @nSeqErrGrv)
								EndIf

							else
								nC9L_TRABAL := ' '
							EndIf

							If !lMultVinc .And. lExistTrab
								oModel:LoadValue("MODEL_C9L", "C9L_TRABAL", nC9L_TRABAL )
								oModel:LoadValue("MODEL_C9L", "C9L_DTRABA", FTafGetVal( cC9LPath + "/matricula", "C", .F., @aIncons, .F. ) )
							Else
								oModel:LoadValue("MODEL_C9L", "C9L_DTRABA", FTafGetVal( cC9LPath + "/matricula", "C", .F., @aIncons, .F. ) )
							EndIf

							/*------------------------------------------
								C9M - Itens da Remunera��o Trab.
							--------------------------------------------*/
							nC9M := 1
							cC9MPath := cC9LPath+"/itensRemun[" + CVALTOCHAR(nC9M) + "]"
							While oDados:XPathHasNode(cC9MPath)

								oModelC9M := oModel:GetModel( 'MODEL_C9M' )
								oModel:GetModel( 'MODEL_C9M' ):LVALID	:= .T.

								If nC9M > 1
									oModel:GetModel( 'MODEL_C9M' ):AddLine()
								EndIf

								if oDados:XPathHasNode(	cC9MPath + "/ideTabRubr")
									cIdTabR := TAFIdTabRub( FTafGetVal( cC9MPath + "/ideTabRubr", "C", .F., @aIncons, .F. ), "T3M", FTafGetVal( cC9MPath + "/codRubr", "C", .F., @aIncons, .F. ) )
									
									If Empty(cIdTabR)
										//Gera mensagem de erro
										TAFMsgIncons( @cInconMsg, @nSeqErrGrv,,, .T., 'ideTabRubr', FTafGetVal( cC9MPath + "/ideTabRubr", "C", .F., @aIncons, .F. ), '', , '', '' )
									EndIf
									oModel:LoadValue( "MODEL_C9M", "C9M_IDTABR"	, cIdTabR )
								else
									cIdTabR := ""

									//Gera mensagem de erro
									TAFMsgIncons( @cInconMsg, @nSeqErrGrv,,, .T., 'ideTabRubr', cIdTabR, '', , '', '' )
								EndIf

								If !lMultVinc .And. lExistTrab
									if oDados:XPathHasNode(	cC9MPath + "/codRubr")
										cCodRubr := FGetIdInt( "codRubr", "ideTabRubr", FTafGetVal( cC9MPath + "/codRubr", "C", .F., @aIncons, .F. ),cIdTabR,.F.,,@cInconMsg, @nSeqErrGrv,/*9*/,/*10*/,/*11*/,/*12*/,/*13*/,StrTran(cPeriodo,"-",""))
										oModel:LoadValue( "MODEL_C9M", "C9M_CODRUB", cCodRubr )
									EndIf
								Else
									if oDados:XPathHasNode(	cC9MPath + "/codRubr")
										cCodRubr := FTafGetVal( cC9MPath + "/codRubr", "C", .F., @aIncons, .F. )
										oModel:LoadValue( "MODEL_C9M", "C9M_RUBRIC"	, cCodRubr )
									EndIf
								EndIf

								if oDados:XPathHasNode(	cC9MPath + "/qtdRubr")
									oModel:LoadValue( "MODEL_C9M", "C9M_QTDRUB"	, FTafGetVal( cC9MPath + "/qtdRubr", "N", .F., @aIncons, .F. ) )
								EndIf

								if oDados:XPathHasNode(	cC9MPath + "/fatorRubr")
									oModel:LoadValue( "MODEL_C9M", "C9M_FATORR"	, FTafGetVal( cC9MPath + "/fatorRubr", "N", .F., @aIncons, .F. ) )
								EndIf

								if oDados:XPathHasNode( cC9MPath + "/vrUnit"	)
									oModel:LoadValue( "MODEL_C9M", "C9M_VLRUNT"	, FTafGetVal( cC9MPath + "/vrUnit", "N", .F., @aIncons, .F. ) )
								EndIf

								if oDados:XPathHasNode( cC9MPath + "/vrRubr")
									oModel:LoadValue( "MODEL_C9M", "C9M_VLRRUB"	, FTafGetVal( cC9MPath + "/vrRubr", "N", .F., @aIncons, .F. ) )
								EndIf

								If lInfoRPT
									aRubrica := oReport:GetRubrica( cCodRubr, cIDTabR, cPeriodo, lMultVinc )

									aAdd( aAnalitico, Array( 13 ) )
									nPosValores := Len( aAnalitico )

									aAnalitico[nPosValores][ANALITICO_MATRICULA]			:=	AllTrim( FTAFGetVal( cC9LPath + "/matricula", "C", .F.,, .F. ) )
									aAnalitico[nPosValores][ANALITICO_CATEGORIA]			:=	AllTrim( FTAFGetVal( cT14Path + "/codCateg", "C", .F.,, .F. ) )
									aAnalitico[nPosValores][ANALITICO_TIPO_ESTABELECIMENTO]	:=	AllTrim( FTAFGetVal( cC9KPath + "/tpInsc", "C", .F.,, .F. ) )
									aAnalitico[nPosValores][ANALITICO_ESTABELECIMENTO]		:=	AllTrim( FTAFGetVal( cC9KPath + "/nrInsc", "C", .F.,, .F. ) )
									aAnalitico[nPosValores][ANALITICO_LOTACAO]				:=	AllTrim( FTAFGetVal( cC9KPath + "/codLotacao", "C", .F.,, .F. ) )
									aAnalitico[nPosValores][ANALITICO_NATUREZA]				:=	AllTrim( aRubrica[1] ) //Natureza
									aAnalitico[nPosValores][ANALITICO_TIPO_RUBRICA]			:=	AllTrim( aRubrica[2] ) //Tipo
									aAnalitico[nPosValores][ANALITICO_INCIDENCIA_CP]		:=	AllTrim( aRubrica[3] ) //Incid�ncia CP
									aAnalitico[nPosValores][ANALITICO_INCIDENCIA_IRRF]		:=	AllTrim( aRubrica[4] ) //Incid�ncia IRRF
									aAnalitico[nPosValores][ANALITICO_INCIDENCIA_FGTS]		:=	AllTrim( aRubrica[5] ) //Incid�ncia FGTS
									aAnalitico[nPosValores][ANALITICO_DECIMO_TERCEIRO]		:=	""
									aAnalitico[nPosValores][ANALITICO_TIPO_VALOR]			:=	""
									aAnalitico[nPosValores][ANALITICO_VALOR]				:=	FTAFGetVal( cC9MPath + "/vrRubr", "N", .F.,, .F. )
								EndIf

								nC9M++
								cC9MPath := cC9LPath+"/itensRemun[" + CVALTOCHAR(nC9M) + "]"
							EndDo


							/*-----------------------------------------
								Informa��es de Planos de Saudade
									T6Y - Valores Pagos
							------------------------------------------*/
							nT6Y:= 1
							cT6YPath := cC9LPath+"/infoSaudeColet/detOper[" + CVALTOCHAR(nT6Y) + "]"
							While oDados:XPathHasNode(cT6YPath)

								oModelT6Y := oModel:GetModel( 'MODEL_T6Y' )
								oModel:GetModel( 'MODEL_T6Y' ):LVALID	:= .T.

								If nT6Y > 1
									oModel:GetModel( 'MODEL_T6Y' ):AddLine()
								EndIf

								if oDados:XPathHasNode( cT6YPath + "/cnpjOper")
									oModel:LoadValue( "MODEL_T6Y", "T6Y_CNPJOP",  FTafGetVal( cT6YPath + "/cnpjOper"   , "C", .F.		, @aIncons, .F. ))
								EndIf

								if oDados:XPathHasNode( cT6YPath + "/regANS" )
									oModel:LoadValue( "MODEL_T6Y", "T6Y_REGANS"	, FTafGetVal( cT6YPath + "/regANS"  , "C", .F.	, @aIncons, .F. ) )
								EndIf

								if oDados:XPathHasNode( cT6YPath + "/vrPgTit")
									oModel:LoadValue( "MODEL_T6Y", "T6Y_VLPGTI"	, FTafGetVal( cT6YPath + "/vrPgTit" , "N", .F.	, @aIncons, .F. ) )
								EndIF

								oModel:LoadValue( "MODEL_T6Y", "T6Y_NOMEVE"	, "S1200" )

								/*----------------------------------------
									T6Z - Informa��es dos dependentes
								------------------------------------------*/
								nT6Z := 1
								cT6ZPath := cT6YPath + "/detPlano[" + cValToChar( nT6Z ) + "]"
								While oDados:xPathHasNode( cT6ZPath )
									oModelT6Z := oModel:GetModel( "MODEL_T6Z" )
									oModel:GetModel( "MODEL_T6Z" ):lValid := .T.

									If nT6Z > 1
										oModel:GetModel( "MODEL_T6Z" ):AddLine()
									EndIf

									oModel:LoadValue( "MODEL_T6Z", "T6Z_SEQUEN", StrZero( nT6Z, nT6Z_SEQUEN ) )

									If oDados:xPathHasNode( cT6ZPath + "/tpDep" )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_TPDEP", FGetIdInt( "tpDep", "", cT6ZPath + "/tpDep",,,, @cInconMsg, @nSeqErrGrv ) )
									EndIf

									If oDados:xPathHasNode( cT6ZPath + "/cpfDep" )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_CPFDEP", FTafGetVal( cT6ZPath + "/cpfDep", "C", .F., @aIncons, .F. ) )
									EndIf

									If oDados:xPathHasNode( cT6ZPath + "/nmDep" )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_NOMDEP", FTafGetVal( cT6ZPath + "/nmDep", "C", .F., @aIncons, .F. ) )
									EndIf

									If oDados:xPathHasNode( cT6ZPath + "/dtNascto" )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_DTNDEP", FTafGetVal( cT6ZPath + "/dtNascto", "D", .F., @aIncons, .F. ) )
									EndIf

									If oDados:xPathHasNode( cT6ZPath + "/vlrPgDep" )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_VPGDEP", FTafGetVal( cT6ZPath + "/vlrPgDep", "N", .F., @aIncons, .F. ) )
									EndIf

									oModel:LoadValue( "MODEL_T6Z", "T6Z_NOMEVE", "S1200" )

									nT6Z ++
									cT6ZPath := cT6YPath + "/detPlano[" + cValToChar( nT6Z ) + "]"
								EndDo

								nT6Y ++
								cT6YPath := cC9LPath + "/infoSaudeColet/detOper[" + cValToChar( nT6Y ) + "]"
							EndDo


							if TafLayESoc("02_04_02")
								/*------------------------------------------
									V1B - Infos Trab Interm Per Apura��o
								--------------------------------------------*/
								nV1B:= 1
								cV1BPath := cC9LPath+"/infoTrabInterm[" + CVALTOCHAR(nV1B) + "]"
								While oDados:XPathHasNode(cV1BPath)

									oModelV1B := oModel:GetModel( 'MODEL_V1B' )
									oModel:GetModel( 'MODEL_V1B' ):LVALID	:= .T.

									If nV1B > 1
										oModel:GetModel( 'MODEL_V1B' ):AddLine()
									EndIf

									if oDados:XPathHasNode(	cV1BPath + "/codConv")                                        
										cCodConv    := FTafGetVal( cV1BPath + "/codConv", "C", .F., @aIncons, .F. )
                                        cIdConvT    := FGetIdInt(   "matricula", "",;
                                                                    fTafGetVal( "/eSocial/evtRemun/dmDev[" + CvalToChar(nT14) + "]/infoPerApur/ideEstabLot/remunPerApur/matricula", "C", .F., @aIncons, .F. ),;
                                                                    ,;
                                                                    .F.,;
                                                                    ,;
                                                                    @cInconMsg,;
                                                                    @nSeqErrGrv)

                                        cIdConv     := FGetIdInt( "codConv", "", cIdConvT, cCodConv, .F.,, @cInconMsg, @nSeqErrGrv )
                                        
										oModel:LoadValue( "MODEL_V1B", "V1B_IDCONV"	, cIdConv )
									EndIf

									nV1B++
									cV1BPath := cC9LPath+"/infoTrabInterm[" + CVALTOCHAR(nV1B) + "]"
								EndDo
							endif

							nC9L++
							cC9LPath := cC9KPath + "/remunPerApur[" + CVALTOCHAR(nC9L) + "]"
						EndDo

						nC9K++
						cC9KPath := cT14Path + "/infoPerApur/ideEstabLot[" + CVALTOCHAR(nC9K) + "]"
					EndDo

					/*-----------------------------------------
					Informa��es de Periodo de Apur Anterior
							C9N - Informa��es do Acordo
					------------------------------------------*/
					nC9N := 1
					cC9NPath := cT14Path + "/infoPerAnt/ideADC[" + CVALTOCHAR(nC9N) + "]"
					While oDados:XPathHasNode(cC9NPath)

						oModelC9N := oModel:GetModel( 'MODEL_C9N' )
						oModel:GetModel( 'MODEL_C9N' ):LVALID	:= .T.

						If nC9N > 1
							oModel:GetModel( 'MODEL_C9N' ):AddLine()
						EndIf

						if oDados:XPathHasNode( cC9NPath + "/dtAcConv")
							oModel:LoadValue( "MODEL_C9N", "C9N_DTACOR"	, FTafGetVal( cC9NPath + "/dtAcConv", "D", .F., @aIncons, .F. ) )
						EndIf

						if oDados:XPathHasNode( cC9NPath + "/tpAcConv")
							oModel:LoadValue( "MODEL_C9N", "C9N_TPACOR"	, FTafGetVal( cC9NPath + "/tpAcConv", "C", .F., @aIncons, .F. ) )
						EndIf

						if oDados:XPathHasNode( cC9NPath + "/compAcConv")
							oModel:LoadValue( "MODEL_C9N", "C9N_COMPAC"	, StrTran(FTafGetVal( cC9NPath + "/compAcConv", "C", .F., @aIncons, .F. ), "-", "" ))
						EndIf

						if oDados:XPathHasNode( cC9NPath + "/dtEfAcConv")
							oModel:LoadValue( "MODEL_C9N", "C9N_DTEFAC"	, FTafGetVal( cC9NPath + "/dtEfAcConv", "D", .F., @aIncons, .F. ) )
						EndIf

						if oDados:XPathHasNode( cC9NPath + "/dsc")
							oModel:LoadValue( "MODEL_C9N", "C9N_DSC"	, FTafGetVal( cC9NPath + "/dsc", "M", .F., @aIncons, .F. ) )
						EndIf

						if oDados:XPathHasNode( cC9NPath + "/remunSuc")
							oModel:LoadValue( "MODEL_C9N", "C9N_REMUNS"	, xFunTrcSN( TAFExisTag( cC9NPath + "/remunSuc" ) ,2))
						EndIf

						nC9O:= 1
						cC9OPath := cC9NPath  + "/idePeriodo[" + CVALTOCHAR(nC9O) + "]"
						While oDados:XPathHasNode(cC9OPath)

							oModelC9O := oModel:GetModel( 'MODEL_C9O' )
							oModel:GetModel( 'MODEL_C9O' ):LVALID	:= .T.

							If nC9O > 1
								oModel:GetModel( 'MODEL_C9O' ):AddLine()
							EndIf

							if oDados:XPathHasNode( cC9OPath + "/perRef" )
								If At("-", FTafGetVal( cC9OPath + "/perRef", "C", .F., @aIncons, .F. )) > 0
									oModel:LoadValue( "MODEL_C9O", "C9O_PERREF"	,StrTran( FTafGetVal( cC9OPath + "/perRef", "C", .F., @aIncons, .F. ), "-", "" ) )
								Else
									oModel:LoadValue( "MODEL_C9O", "C9O_PERREF"	,FTafGetVal( cC9OPath + "/perRef", "C", .F., @aIncons, .F. ) )
								EndIf
							EndIf

							/*------------------------------------------
								C9P - Informa��es do Estab./Lotac.
							--------------------------------------------*/
							nC9P:= 1
							cC9PPath := cC9OPath + "/ideEstabLot[" + CVALTOCHAR(nC9P) + "]
							While oDados:XPathHasNode(cC9PPath)

								oModelC9P := oModel:GetModel( 'MODEL_C9P' )
								oModel:GetModel( 'MODEL_C9P' ):LVALID	:= .T.

								If nC9P > 1
									oModel:GetModel( 'MODEL_C9P' ):AddLine()
								EndIf

								If !lMultVinc .And. lExistTrab

									if oDados:XPathHasNode( cC9PPath + "/tpInsc" ) .or. oDados:XPathHasNode(cC9PPath + "/nrInsc" )
										oModel:LoadValue( "MODEL_C9P", "C9P_ESTABE", FGetIdInt( "tpInsc", "nrInsc", cC9PPath + "/tpInsc" , cC9PPath + "/nrInsc" ,,,@cInconMsg, @nSeqErrGrv))
									EndIf

									if oDados:XPathHasNode( cC9PPath + "/codLotacao" )
										oModel:LoadValue( "MODEL_C9P", "C9P_LOTACA", FGetIdInt( "codLotacao", "", cC9PPath + "/codLotacao",,,,@cInconMsg, @nSeqErrGrv) )
									EndIf

								Else

									If oDados:XPathHasNode(	cC9PPath + "/codLotacao" )
										oModel:LoadValue( "MODEL_C9P", "C9P_CODLOT", FTafGetVal( cC9PPath + "/codLotacao", "C", .F., @aIncons, .F. ) )
									EndIf

									If oDados:XPathHasNode(	cC9PPath + "/tpInsc" )
										oModel:LoadValue( "MODEL_C9P", "C9P_TPINSC", FTafGetVal( cC9PPath + "/tpInsc", "C", .F., @aIncons, .F. ) )
									EndIf

									If oDados:XPathHasNode(	cC9PPath + "/nrInsc" )
										oModel:LoadValue( "MODEL_C9P", "C9P_NRINSC", FTafGetVal( cC9PPath + "/nrInsc", "C", .F., @aIncons, .F. ) )
									EndIf

								EndIf

								/*------------------------------------------
									C9Q - Informa��es da Remunera��o
								--------------------------------------------*/
								nC9Q:= 1
								cC9QPath := cC9PPath+ "/remunPerAnt[" + CVALTOCHAR(nC9Q) + "]"
								While oDados:XPathHasNode(cC9QPath)

									oModelC9Q := oModel:GetModel( 'MODEL_C9Q' )
									oModel:GetModel( 'MODEL_C9Q' ):LVALID	:= .T.

									If nC9Q > 1
										oModel:GetModel( 'MODEL_C9Q' ):AddLine()
									EndIf

									if oDados:XPathHasNode(	cC9QPath + "/infoAgNocivo/grauExp" )
										oModel:LoadValue( "MODEL_C9Q", "C9Q_GRAEXP"	, FGetIdInt( "grauExp", "", cC9QPath + "/infoAgNocivo/grauExp",,,,@cInconMsg, @nSeqErrGrv))
									EndIf

									if oDados:XPathHasNode(	cC9QPath + "/indSimples" )
										oModel:LoadValue( "MODEL_C9Q", "C9Q_INDCON" , FTafGetVal( cC9QPath + "/indSimples", "C", .F., @aIncons, .F. ) )
									EndIf

									If !lMultVinc .And. lExistTrab
										//VERIFICA DE QUAL LUGAR PEGAR O ID DO TRABALHADOR
										if oDados:XPathHasNode( cC9QPath + "/matricula" )
											cMatricC9Q := FTafGetVal( cC9QPath + "/matricula", "C", .F., @aIncons, .F. )
											nC9Q_TRABAL := FGetIdInt( "matricula", "", cMatricC9Q, , .F.,,@cInconMsg, @nSeqErrGrv)
											oModel:LoadValue( "MODEL_C9Q", "C9Q_TRABAL", nC9Q_TRABAL )

										elseif oDados:XPathHasNode( cC9QPath + "/itensRemun[1]" )
											oModel:LoadValue( "MODEL_C9Q", "C9Q_TRABAL", " " )
										EndIf
									Else
										oModel:LoadValue( "MODEL_C9Q", "C9Q_DTRABA", FTafGetVal( cC9QPath + "/matricula", "C", .F., @aIncons, .F. ) )
									EndIf

									/*------------------------------------
										C9R - Itens da Remunera��o
									--------------------------------------*/
									nC9R:= 1
									cC9RPath := cC9QPath + "/itensRemun[" + CVALTOCHAR(nC9R) + "]"
									While oDados:XPathHasNode(cC9RPath)

										oModelC9R := oModel:GetModel( 'MODEL_C9R' )
										oModel:GetModel( 'MODEL_C9R' ):LVALID	:= .T.

										If nC9R > 1
											oModel:GetModel( 'MODEL_C9R' ):AddLine()
										EndIf

										if oDados:XPathHasNode(	cC9RPath + "/ideTabRubr")
											cIdTabR := TAFIdTabRub( FTafGetVal( cC9RPath + "/ideTabRubr", "C", .F., @aIncons, .F. ), "T3M", FTafGetVal( cC9RPath + "/codRubr", "C", .F., @aIncons, .F. )  )
				
											If Empty(cIdTabR)
												//Gera mensagem de erro
												TAFMsgIncons( @cInconMsg, @nSeqErrGrv,,, .T., 'ideTabRubr', FTafGetVal( cC9MPath + "/ideTabRubr", "C", .F., @aIncons, .F. ), '', , '', '' )
											EndIf
										else
											cIdTabR := ""

											//Gera mensagem de erro
											TAFMsgIncons( @cInconMsg, @nSeqErrGrv,,, .T., 'ideTabRubr', cIdTabR, '', , '', '' )
										EndIf

										If !lMultVinc .And. lExistTrab
											if oDados:XPathHasNode( cC9RPath + "/codRubr" )
												cCodRubr := FGetIdInt( "codRubr", "ideTabRubr", FTafGetVal( cC9RPath + "/codRubr", "C", .F., @aIncons, .F. ),cIdTabR,.F.,,@cInconMsg, @nSeqErrGrv,/*9*/,/*10*/,/*11*/,/*12*/,/*13*/,StrTran(cPeriodo,"-",""))
												oModel:LoadValue( "MODEL_C9R", "C9R_CODRUB", cCodRubr)
											EndIf
										Else

											if oDados:XPathHasNode(	cC9RPath + "/codRubr")
												cCodRubr := FTafGetVal( cC9RPath + "/codRubr", "C", .F., @aIncons, .F. )
												oModel:LoadValue( "MODEL_C9R", "C9R_RUBRIC"	, cCodRubr )
											EndIf

											if oDados:XPathHasNode(	cC9RPath + "/ideTabRubr")
												oModel:LoadValue( "MODEL_C9R", "C9R_IDTABR"	, cIdTabR )
											EndIf

										EndIf

										if oDados:XPathHasNode( cC9RPath + "/qtdRubr" )
											oModel:LoadValue( "MODEL_C9R", "C9R_QTDRUB"	, FTafGetVal( cC9RPath + "/qtdRubr", "N", .F., @aIncons, .F. ) )
										EndIf

										if oDados:XPathHasNode(	cC9RPath + "/fatorRubr")
											oModel:LoadValue( "MODEL_C9R", "C9R_FATORR"	, FTafGetVal( cC9RPath + "/fatorRubr", "N", .F., @aIncons, .F. ) )
										EndIf

										if oDados:XPathHasNode( cC9RPath + "/vrUnit" )
											oModel:LoadValue( "MODEL_C9R", "C9R_VLRUNT"	, FTafGetVal( cC9RPath + "/vrUnit", "N", .F., @aIncons, .F. ) )
										EndIf

										if oDados:XPathHasNode( cC9RPath + "/vrRubr" )
											oModel:LoadValue( "MODEL_C9R", "C9R_VLRRUB"	, FTafGetVal( cC9RPath + "/vrRubr", "N", .F., @aIncons, .F. ) )
										EndIf
										
										If lInfoRPT
											aRubrica := oReport:GetRubrica( cCodRubr, cIDTabR, cPeriodo, lMultVinc )

											aAdd( aAnalitico, Array( 13 ) )
											nPosValores := Len( aAnalitico )

											aAnalitico[nPosValores][ANALITICO_MATRICULA]			:=	AllTrim( FTAFGetVal( cC9QPath + "/matricula", "C", .F.,, .F. ) )
											aAnalitico[nPosValores][ANALITICO_CATEGORIA]			:=	AllTrim( FTAFGetVal( cT14Path + "/codCateg", "C", .F.,, .F. ) )
											aAnalitico[nPosValores][ANALITICO_TIPO_ESTABELECIMENTO]	:=	AllTrim( FTAFGetVal( cC9PPath + "/tpInsc", "C", .F.,, .F. ) )
											aAnalitico[nPosValores][ANALITICO_ESTABELECIMENTO]		:=	AllTrim( FTAFGetVal( cC9PPath + "/nrInsc", "C", .F.,, .F. ) )
											aAnalitico[nPosValores][ANALITICO_LOTACAO]				:=	AllTrim( FTAFGetVal( cC9PPath + "/codLotacao", "C", .F.,, .F. ) )
											aAnalitico[nPosValores][ANALITICO_NATUREZA]				:=	AllTrim( aRubrica[1] ) //Natureza
											aAnalitico[nPosValores][ANALITICO_TIPO_RUBRICA]			:=	AllTrim( aRubrica[2] ) //Tipo
											aAnalitico[nPosValores][ANALITICO_INCIDENCIA_CP]		:=	AllTrim( aRubrica[3] ) //Incid�ncia CP
											aAnalitico[nPosValores][ANALITICO_INCIDENCIA_IRRF]		:=	AllTrim( aRubrica[4] ) //Incid�ncia IRRF
											aAnalitico[nPosValores][ANALITICO_INCIDENCIA_FGTS]		:=	AllTrim( aRubrica[5] ) //Incid�ncia FGTS
											aAnalitico[nPosValores][ANALITICO_DECIMO_TERCEIRO]		:=	""
											aAnalitico[nPosValores][ANALITICO_TIPO_VALOR]			:=	""
											aAnalitico[nPosValores][ANALITICO_VALOR]				:=	FTAFGetVal( cC9RPath + "/vrRubr", "N", .F.,, .F. )
										EndIf

										nC9R++
										cC9RPath := cC9QPath + "/itensRemun[" + CVALTOCHAR(nC9R) + "]"
									EndDo

									if TafLayESoc("02_04_02")
										/*---------------------------------------
											V1C - Infos Trab Interm Per Anterior
										----------------------------------------*/
										nV1C:= 1
										cV1CPath := cC9QPath + "/infoTrabInterm[" + CVALTOCHAR(nV1C) + "]"
										While oDados:XPathHasNode(cV1CPath)
											oModelV1C := oModel:GetModel( 'MODEL_V1C' )
											oModel:GetModel( 'MODEL_V1C' ):LVALID	:= .T.

											If nV1C > 1
												oModel:GetModel( 'MODEL_V1C' ):AddLine()
											EndIf

											if oDados:XPathHasNode(	cV1CPath + "/codConv")
												cCodConv := FTafGetVal( cV1CPath + "/codConv", "C", .F., @aIncons, .F. )
												cIdConv := Posicione("T87",2,xFilial("T87")+PADR(cIdTrab,GetSx3Cache("C91_TRABAL","X3_TAMANHO"))+PADR(cCodConv,GetSx3Cache("T87_CONVOC","X3_TAMANHO"))+"1","T87_ID")
												oModel:LoadValue( "MODEL_V1C", "V1C_IDCONV"	, cIdConv )
											EndIf

											nV1C++
											cV1CPath := cC9QPath + "/infoTrabInterm[" + CVALTOCHAR(nV1C) + "]"
										EndDo
									endif

									nC9Q++
									cC9QPath := cC9PPath+ "/remunPerAnt[" + CVALTOCHAR(nC9Q) + "]"
								EndDo

								nC9P++
								cC9PPath := cC9OPath+"/ideEstabLot[" + CVALTOCHAR(nC9P) + "]"
							EndDo

							nC9O++
							cC9OPath := cC9NPath + "/idePeriodo[" + CVALTOCHAR(nC9O) + "]"
						EndDo
						nC9N++
						cC9NPath := cT14Path + "/infoPerAnt/ideADC[" + CVALTOCHAR(nC9N) + "]"

					EndDo

					if !TafLayESoc("02_04_02")
						/*------------------------------------------------------
						Informa��es das Convoca��es de trabalho intermitente
						--------------------------------------------------------*/
						oModelT89 := oModel:GetModel( 'MODEL_T89' )
						oModel:GetModel( 'MODEL_T89' ):LVALID	:= .T.

						nT89 := 1
						cT89Path := cT14Path + "/infoTrabInterm[" + CVALTOCHAR(nT89) + "]"
						While oDados:XPathHasNode(cT89Path)
							If nT89 > 1
								oModel:GetModel('MODEL_T89'):AddLine()
							EndIf

							If oDados:XPathHasNode( cT89Path + "/codConv" )
								cCodConv := FTafGetVal( cT89Path + "/codConv", "C", .F., @aIncons, .F. )
								cIdConv := Posicione("T87",2,xFilial("T87")+PADR(cIdTrab,GetSx3Cache("C91_TRABAL","X3_TAMANHO"))+PADR(cCodConv,GetSx3Cache("T87_CONVOC","X3_TAMANHO"))+"1","T87_ID")

								oModel:LoadValue( "MODEL_T89", "T89_IDCONV"	, cIdConv )
							EndIf

							nT89++
							cT89Path := cT14Path + "/infoTrabInterm/[" + CVALTOCHAR(nT89) + "]"
						EndDo
					endif


					nT14++
					cT14Path := "/eSocial/evtRemun/dmDev[" + CVALTOCHAR(nT14) + "]"
				EndDo

			/*-----------------------------------------
					CRN - Informa��es de Processos Judic.
			------------------------------------------*/
				nCRN := 1
				cCRNPath := "/eSocial/evtRemun/ideTrabalhador/procJudTrab[" + CVALTOCHAR(nCRN) + "]"

				If nOpc == 4
					For nJ := 1 to oModel:GetModel( 'MODEL_CRN' ):Length()
						oModel:GetModel( 'MODEL_CRN' ):GoLine(nJ)
						oModel:GetModel( 'MODEL_CRN' ):DeleteLine()
					Next nJ
				EndIf

				//������������������������������������������������������������Ŀ
				//�Rodo o XML parseado para gravar as novas informacoes no GRID�
				//��������������������������������������������������������������
				nCRN := 1
				While oDados:XPathHasNode(cCRNPath)

					oModel:GetModel( 'MODEL_CRN' ):LVALID	:= .T.

					If nOpc == 4 .Or. nCRN > 1
						oModel:GetModel('MODEL_CRN'):AddLine()
					EndIf

					If oDados:XPathHasNode(cCRNPath + "/tpTrib" )
						oModel:LoadValue("MODEL_CRN", "CRN_TPTRIB", FTafGetVal( cCRNPath + "/tpTrib", "C", .F., @aIncons, .F. ) )
					EndIf

					If oDados:XPathHasNode(cCRNPath + "/nrProcJud" )
						cIdProc := FGetIdInt( "nrProcJ", ,cCRNPath + "/nrProcJud",,,,@cInconMsg, @nSeqErrGrv)
						oModel:LoadValue("MODEL_CRN", "CRN_IDPROC", cIdProc )

					EndIf

					If !Empty(cIdProc)
						If oDados:XPathHasNode(cCRNPath + "/codSusp" )
							oModel:LoadValue("MODEL_CRN", "CRN_IDSUSP", FGetIdInt( "codSusp","",FTafGetVal( cCRNPath + "/codSusp", "C", .F., @aIncons, .F. ),cIdProc,.F.,,@cInconMsg, @nSeqErrGrv) )
						EndIf
					EndIf

					nCRN++
					cCRNPath := "/eSocial/evtRemun/ideTrabalhador/procJudTrab[" + CVALTOCHAR(nCRN) + "]"
				EndDo

				/*--------------------------------
					T6W - Multiplos Vinculos
				---------------------------------*/
				nT6W := 1
				cT6WPath := "/eSocial/evtRemun/ideTrabalhador/infoMV/remunOutrEmpr[" + CVALTOCHAR(nT6W) + "]"
				If nOpc == 4
					For nJ := 1 to oModel:GetModel( 'MODEL_T6W' ):Length()
						oModel:GetModel( 'MODEL_T6W' ):GoLine(nJ)
						oModel:GetModel( 'MODEL_T6W' ):DeleteLine()
					Next nJ
				EndIf

				//������������������������������������������������������������Ŀ
				//�Rodo o XML parseado para gravar as novas informacoes no GRID�
				//��������������������������������������������������������������
				nT6W := 1
				While oDados:XPathHasNode(cT6WPath)

					oModel:GetModel( 'MODEL_T6W' ):LVALID	:= .T.

					If nOpc == 4 .Or. nT6W > 1
						oModel:GetModel( 'MODEL_T6W' ):AddLine()
					EndIf

					if oDados:XPathHasNode( cT6WPath + "/tpInsc" )
						oModel:LoadValue( "MODEL_T6W", "T6W_TPINSC"	, FTafGetVal( cT6WPath + "/tpInsc","C", .F., @aIncons, .F. ) )
					EndIf

					if oDados:XPathHasNode( cT6WPath + "/nrInsc" )
						oModel:LoadValue( "MODEL_T6W", "T6W_NRINSC"	, FTafGetVal( cT6WPath + "/nrInsc","C", .F., @aIncons, .F. ) )
					EndIf

					If oDados:XPathHasNode( cT6WPath + "/codCateg" )
						oModel:LoadValue( "MODEL_T6W", "T6W_CODCAT" , FGetIdInt( "codCateg", "",  cT6WPath + "/codCateg",,,,@cInconMsg, @nSeqErrGrv))
					EndIf

					if oDados:XPathHasNode( cT6WPath + "/vlrRemunOE" )
						oModel:LoadValue( "MODEL_T6W", "T6W_VLREMU"	, FTafGetVal( cT6WPath + "/vlrRemunOE","N", .F., @aIncons, .F. ) )
					EndIf

					oModel:LoadValue( "MODEL_T6W", "T6W_NOMEVE"	, "S1200" )

					nT6W++
					cT6WPath := "/eSocial/evtRemun/ideTrabalhador/infoMV/remunOutrEmpr[" + CVALTOCHAR(nT6W) + "]"
				EndDo

			EndIf

			//���������������������������Ŀ
			//�Efetiva a operacao desejada�
			//�����������������������������
			If Empty(cInconMsg) .And. Empty(aIncons)
                xChkDupl := TafFormCommit( oModel, .T. )
                If ValType( xChkDupl ) == "A"
                    If xChkDupl[1]
                        Aadd(aIncons, "ERRO19" + "|" + xChkDupl[2] + "|" + xChkDupl[3])
                    Else
                        lRet := .T.
                    EndIf
                ElseIf ValType( xChkDupl ) == "L"
                    If xChkDupl
                        Aadd(aIncons, "ERRO19")
                    Else
                        lRet := .T.
                    EndIf
                EndIf

				If lRet .and. lInfoRPT
					oInfoRPT := oReport:oVOReport
					oInfoRPT:SetIndApu( AllTrim( FTAFGetVal( "/eSocial/evtRemun/ideEvento/indApuracao", "C", .F.,, .F. ) ) )
					oInfoRPT:SetPeriodo( AllTrim( StrTran( cPeriodo, "-", "" ) ) )
					oInfoRPT:SetCPF( AllTrim( FTAFGetVal( "/eSocial/evtRemun/ideTrabalhador/cpfTrab", "C", .F.,, .F. ) ) )
					oInfoRPT:SetNome( AllTrim( cNome ) )
					oInfoRPT:SetAnalitico( aAnalitico )

					oReport:UpSert( "S-1200", "1", xFilial( "C91" ), oInfoRPT )

					oInfoRPT:SetIndApu( AllTrim( FTAFGetVal( "/eSocial/evtRemun/ideEvento/indApuracao", "C", .F.,, .F. ) ) )
					oInfoRPT:SetPeriodo( AllTrim( StrTran( cPeriodo, "-", "" ) ) )
					oInfoRPT:SetCPF( AllTrim( FTAFGetVal( "/eSocial/evtRemun/ideTrabalhador/cpfTrab", "C", .F.,, .F. ) ) )
					oInfoRPT:SetNome( AllTrim( cNome ) )
					oInfoRPT:SetAnalitico( aAnalitico )

					oReport:UpSert( "S-1200", "2", xFilial( "C91" ), oInfoRPT )
				EndIf

            Else
                Aadd(aIncons, cInconMsg)
                DisarmTransaction()
            EndIf
		EndIf

	End Transaction

Else
	lRet := .F.
	aAdd( aIncons, STR0074 ) //"O ambiente do TAF encontra-se desatualizado. Para utiliza��o desta funcionalidade, ser� necess�rio executar o compatibilizador de dicion�rio de dados UPDDISTR dispon�vel no portal do cliente do TAF."
EndIf

//����������������������������������������������������������Ŀ
//�Zerando os arrays e os Objetos utilizados no processamento�
//������������������������������������������������������������
aSize( aRules, 0 )
aRules     := Nil

aSize( aChave, 0 )
aChave     := Nil

oModel     := Nil

Return { lRet, aIncons }

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF250Rul

Regras para gravacao das informacoes do registro S-1200 do E-Social

@Param
nOper      - Operacao a ser realizada ( 3 = Inclusao / 4 = Alteracao / 5 = Exclusao )

@Return
aRull  - Regras para a gravacao das informacoes


@author Vitor Siqueira
@since 08/01/2016
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function TAF250Rul( cInconMsg, nSeqErrGrv, cCodEvent, cOwner, lMultVinc, lExistTrab, lFindTrab, lInfoCompl, aInfComp, cIDCat, cIdTrab, cNomEvC9V )

Local aRull		    := {}
Local cPeriodo	    := FTafGetVal("/eSocial/evtRemun/ideEvento/perApur", "C", .F.,, .F. )
Local cIdTrabal	    := ""
Local cPathPerAnt   := "/eSocial/evtRemun/dmDev[1]/infoPerAnt/ideADC[1]/idePeriodo[1]/ideEstabLot[1]/remunPerAnt[1]"
Local cPathPerApu   := "/eSocial/evtRemun/dmDev[1]/infoPerApur/ideEstabLot[1]/remunPerApur"
Local aIncons := {}

Default cInconMsg	:= ""
Default nSeqErrGrv	:= 0
Default cCodEvent	:= ""
Default cOwner		:= ""
Default lMultVinc	:= .F.
Default lExistTrab	:= .T.
Default lFindTrab	:= .F.
Default aInfComp    := {}
Default cIDCat		:= ""
Default cIdTrab		:= ""
Default cNomEvC9V	:= ""

if TafXNode( oDados, cCodEvent, cOwner,("/eSocial/evtRemun/ideEvento/perApur") )
	If At("-", cPeriodo) > 0
		Aadd( aRull, {"C91_PERAPU", StrTran(cPeriodo, "-", "" ) ,"C",.T.} )
	Else
		Aadd( aRull, {"C91_PERAPU", cPeriodo ,"C", .T.} )
	EndIf
EndIf

If oDados:XPathHasNode("/eSocial/evtRemun/ideTrabalhador/infoComplem")

	If TafLayESoc("02_05_00")
		If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/tpInscAnt" ) )
			Aadd( aRull, {"C91_TPINSC", "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/tpInscAnt","C",.F.} )
		EndIf
	EndIf

	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/cnpjEmpregAnt" ) )
		Aadd( aRull, {"C91_CNPJEA", "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/cnpjEmpregAnt","C",.F.} )
	EndIf

	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/matricAnt" ) )
		Aadd( aRull, {"C91_MATREA", "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/matricAnt","C",.F.} )
	EndIf

	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/dtAdm" ) )
		Aadd( aRull, {"C91_DTINVI", "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/dtAdm","D",.F.} )
	EndIf

	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/observacao" ) )
		Aadd( aRull, {"C91_OBSVIN", "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/observacao","C",.F.} )
	EndIf

EndIf

If !lMultVinc .Or. ( lMultVinc .And. lExistTrab )
	If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/cpfTrab" ) )
		
		If (oDados:XPathHasNode(cPathPerApu + "/matricula")) .And. !Empty(FTafGetVal( cPathPerApu + "/matricula","C", .F., , .F.))
			cIdTrabal := FGetIdInt( "matricula", "", FTafGetVal(cPathPerApu + "/matricula","C", .F., , .F.), , .F.,,@cInconMsg, @nSeqErrGrv)
		ElseIf (oDados:XPathHasNode(cPathPerAnt + "/matricula")) .And. !Empty(FTafGetVal( cPathPerAnt + "/matricula","C", .F., , .F.))
			cIdTrabal := FGetIdInt( "matricula", "", FTafGetVal(cPathPerAnt + "/matricula","C", .F., , .F.), , .F.,,@cInconMsg, @nSeqErrGrv)
		ElseIf !Empty(cIdTrab) .AND. cNomEvC9V == "TAUTO"
			cIdTrabal := cIdTrab
		Else
			cIdTrabal := TAFGetIdFunc( FTafGetVal( "/eSocial/evtRemun/ideTrabalhador/cpfTrab","C", .F., , .F.), cPeriodo, Nil, "cpfTrab", "/eSocial/evtRemun/ideTrabalhador/cpfTrab", aInfComp, fTafGetVal( "/eSocial/evtRemun/dmDev/infoPerApur/ideEstabLot/remunPerApur/matricula", "C", .F., @aIncons, .F. ), cIDCat )
		Endif

		Aadd( aRull, {"C91_TRABAL", cIdTrabal, "C", .T. } )

		If !Empty( cIdTrabal )
			lFindTrab := .T.
		EndIf

	EndIf
Else

	If lInfoCompl

		If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/nmTrab" ) )
			Aadd( aRull, {"C91_NOME", "/eSocial/evtRemun/ideTrabalhador/infoComplem/nmTrab","C",.F.} )
		EndIf

		If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/dtNascto" ) )
			Aadd( aRull, {"C91_NASCTO", "/eSocial/evtRemun/ideTrabalhador/infoComplem/dtNascto","D",.F.} )
		EndIf

		If TafLayESoc("02_05_00")
			If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/tpInscAnt" ) )
				Aadd( aRull, {"C91_TPINSC", "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/tpInscAnt","C",.F.} )
			EndIf
		EndIf

		If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/cnpjEmpregAnt" ) )
			Aadd( aRull, {"C91_CNPJEA", "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/cnpjEmpregAnt","C",.F.} )
		EndIf

		If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/matricAnt" ) )
			Aadd( aRull, {"C91_MATREA", "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/matricAnt","C",.F.} )
		EndIf

		If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/dtAdm" ) )
			Aadd( aRull, {"C91_DTINVI", "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/dtAdm","D",.F.} )
		EndIf

		If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/observacao" ) )
			Aadd( aRull, {"C91_OBSVIN", "/eSocial/evtRemun/ideTrabalhador/infoComplem/sucessaoVinc/observacao","C",.F.} )
		EndIf
	
	Else
		
		// Caso j� tenha sido gerado um RPA para o trabalhador e for identificado que ele possui algum S-2200 ou S-2300, as informa��es abaixo ser�o zeradas.
		Aadd( aRull, {"C91_NOME"  , "","C",.T.} )
		Aadd( aRull, {"C91_NASCTO", "","D",.T.} )
		Aadd( aRull, {"C91_CNPJEA", "","C",.T.} )
		Aadd( aRull, {"C91_MATREA", "","C",.T.} )
		Aadd( aRull, {"C91_DTINVI", "","D",.T.} )
		Aadd( aRull, {"C91_OBSVIN", "","C",.T.} )

	EndIf

EndIf

If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/cpfTrab" ) )
	Aadd( aRull, {"C91_CPF", FTafGetVal("/eSocial/evtRemun/ideTrabalhador/cpfTrab", "C", .F.,, .F. ),"C",.T.} )
EndIf

if TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/nisTrab" ) )
	Aadd( aRull, {"C91_NIS", FTafGetVal("/eSocial/evtRemun/ideTrabalhador/nisTrab", "C", .F.,, .F. ),"C",.T.} )
EndIf

if TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideEvento/indApuracao" ) )
	Aadd( aRull, {"C91_INDAPU", "/eSocial/evtRemun/ideEvento/indApuracao","C",.F.} )
EndIf

if TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoMV/indMV" ) )
	Aadd( aRull, {"C91_INDMVI", "/eSocial/evtRemun/ideTrabalhador/infoMV/indMV", "C", .F. } )
EndIf

if !TafLayESoc("02_04_02")

	if TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/codCBO" ) )
		aAdd( aRull, { "C91_CODCBO", FGetIdInt( "codCBO", , "/eSocial/evtRemun/ideTrabalhador/infoComplem/codCBO",,,,@cInconMsg, @nSeqErrGrv ), "C", .T., 'C8V' } ) //codCBO
	EndIf

	if TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/natAtividade" ) )
		Aadd( aRull, {"C91_NATATV", "/eSocial/evtRemun/ideTrabalhador/infoComplem/natAtividade","C",.F.} )
	EndIf

	if TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoComplem/qtdDiasTrab" ) )
		Aadd( aRull, {"C91_QTDTRB", "/eSocial/evtRemun/ideTrabalhador/infoComplem/qtdDiasTrab","C",.F.} )
	EndIf
endif

if TafLayESoc("02_04_02")
	if TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRemun/ideTrabalhador/infoInterm/qtdDiasInterm" ) )
		Aadd( aRull, {"C91_QTDINT", "/eSocial/evtRemun/ideTrabalhador/infoInterm/qtdDiasInterm", "C", .F.} )
	EndIf
endif

Return( aRull )

//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@Param  oModel -> Modelo de dados

@Return .T.

@author Vitor Siqueira
@since 11/01/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel( oModel )

Local cLogOpe		:= ""
Local cLogOpeAnt	:= ""

Local cVerAnt    	:= ""
Local cProtocolo 	:= ""
Local cVersao    	:= ""
Local cChvRegAnt 	:= ""
Local cEvento		:= ""

Local lRetorno		:= .T.

Local nOperation 		:= oModel:GetOperation()
Local nlI  				:= 0
Local nlY  				:= 0
Local nC9O, nC9OAdd 	:= 0
Local nC9Q, nC9QAdd		:= 0
Local nC9R, nC9RAdd 	:= 0
Local nT14, nT14Add 	:= 0
Local nC9L, nC9LAdd 	:= 0
Local nC9M, nC9MAdd 	:= 0
Local nT6Y, nT6YAdd 	:= 0
Local nT6Z, nT6ZAdd 	:= 0
Local nC9P, nC9PAdd 	:= 0
Local nT6W, nT6WAdd 	:= 0
Local nC9N, nC9NAdd 	:= 0
Local nC9K, nC9KAdd 	:= 0
Local nT89, nT89Add  	:= 0
Local nV1B, nV1BAdd 	:= 0
Local nV1C, nV1CAdd 	:= 0

Local aGrava     	:= {}
Local aGravaT14  	:= {}
Local aGravaC9L 	:= {}
Local aGravaC9M 	:= {}
Local aGravaT6Y  	:= {}
Local aGravaT6Z  	:= {}
Local aGravaC9O  	:= {}
Local aGravaC9Q  	:= {}
Local aGravaC9R  	:= {}
Local aGravaCRN  	:= {}
Local aGravaT6W  	:= {}
Local aGravaC9K  	:= {}
Local aGravaC9N  	:= {}
Local aGravaC9P  	:= {}
Local aGravaT89		:= {}
Local aGravaV1B 	:= {}
Local aGravaV1C 	:= {}

Local oModelC91  	:= Nil
Local oModelT14  	:= Nil
Local oModelC9L  	:= Nil
Local oModelC9M  	:= Nil
Local oModelT6Y  	:= Nil
Local oModelT6Z  	:= Nil
Local oModelC9O  	:= Nil
Local oModelC9Q  	:= Nil
Local oModelC9R  	:= Nil
Local oModelCRN  	:= Nil
Local oModelT6W  	:= Nil
Local oModelC9K  	:= Nil
Local oModelC9N  	:= Nil
Local oModelC9P  	:= Nil
Local oModelT89		:= Nil
Local oModelV1B  	:= Nil
Local oModelV1C  	:= Nil

//Relat�rio de Confer�ncia de Valores
Local oInfoRPT	:=	Nil
Local cIndApu	:=	""
Local cPeriodo	:=	""
Local cCPF		:=	""
Local cNome		:=  ""

Local lInfoRPT	:=	.F.

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

Begin Transaction

	If __lGrvRPT == Nil
		TAF250Rpt() //Inicializa a vari�vel static __lGrvRPT
	EndIf

	lInfoRPT := __lGrvRPT

	//Realiza a exclus�o do registro da tabela do relat�rio
	If lInfoRPT
		cIndApu		:=	oModel:GetValue("MODEL_C91","C91_INDAPU")
		cPeriodo	:=	oModel:GetValue("MODEL_C91","C91_PERAPU")

		If oModel:GetValue("MODEL_C91","C91_MV") == "1"
			cCPF := oModel:GetValue("MODEL_C91","C91_CPF")
			cNome := oModel:GetValue("MODEL_C91","C91_NOME")
		Else
			cCPF := GetADVFVal( "C9V", "C9V_CPF", xFilial( "C9V" ) + oModel:GetValue("MODEL_C91","C91_TRABAL") + "1", 2, "", .T. )
			cNome := GetADVFVal( "C9V", "C9V_NOME", xFilial( "C9V" ) + oModel:GetValue("MODEL_C91","C91_TRABAL") + "1", 2, "", .T. )
		EndIf

		If oReport == Nil
			oReport := TAFSocialReport():New()
		EndIf

		oInfoRPT := oReport:oVOReport
		oInfoRPT:SetIndApu( AllTrim( cIndApu ) )
		oInfoRPT:SetPeriodo( AllTrim( cPeriodo ) )
		oInfoRPT:SetCPF( AllTrim( cCPF ) )
		oInfoRPT:SetNome(cNome)

	EndIf

	If nOperation == MODEL_OPERATION_INSERT

		oModel:LoadValue( "MODEL_C91", "C91_VERSAO", xFunGetVer() )
		oModel:LoadValue( "MODEL_C91", "C91_NOMEVE", "S1200" )

		If Empty( oModel:GetValue( "MODEL_C91", "C91_TRABAL" ) )
			oModel:LoadValue( "MODEL_C91", "C91_MV", "1" )
		EndIf

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_C91', 'C91_LOGOPE' , '2', '' )
		Endif

		oModelT6W := oModel:GetModel( "MODEL_T6W" )
		For nT6W := 1 to oModelT6W:Length()
			oModelT6W:GoLine( nT6W )
			If !oModelT6W:IsEmpty() .and. !oModelT6W:IsDeleted()
				oModelT6W:LoadValue( "T6W_NOMEVE", "S1200" )
			EndIf
		Next nT6W

		oModelT6Y := oModel:GetModel( "MODEL_T6Y" )
		For nT6Y := 1 to oModelT6Y:Length()
			oModelT6Y:GoLine( nT6Y )
			If !oModelT6Y:IsEmpty() .and. !oModelT6Y:IsDeleted()
				oModelT6Y:LoadValue( "T6Y_NOMEVE", "S1200" )
			EndIf
		Next nT6Y

		oModelT6Z := oModel:GetModel( "MODEL_T6Z" )
		For nT6Z := 1 to oModelT6Z:Length()
			oModelT6Z:GoLine( nT6Z )
			If !oModelT6Z:IsEmpty() .and. !oModelT6Z:IsDeleted()
				oModelT6Z:LoadValue( "T6Z_NOMEVE", "S1200" )
			EndIf
		Next nT6Z

		If FWFormCommit( oModel )
			aAnalitico := LoadReportData(oModel, lInfoRPT)
			oInfoRPT:SetAnalitico(aAnalitico)
			oReport:UpSert( "S-1200", "2", xFilial( "C91" ), oInfoRPT )
		EndIf

	ElseIf nOperation == MODEL_OPERATION_UPDATE

		//�����������������������������������������������������������������Ŀ
		//�Seek para posicionar no registro antes de realizar as validacoes,�
		//�visto que quando nao esta pocisionado nao eh possivel analisar   �
		//�os campos nao usados como _STATUS                                �
		//�������������������������������������������������������������������
		C91->( DbSetOrder( 3 ) )
		If lGoExtemp .OR. C91->( MsSeek( xFilial( 'C91' ) + M->C91_ID + '1' ) )

			//��������������������������������Ŀ
			//�Se o registro ja foi transmitido�
			//����������������������������������
			If C91->C91_STATUS $ ( "4" )

				oModelC91 := oModel:GetModel( 'MODEL_C91' )
				oModelT6W := oModel:GetModel( 'MODEL_T6W' )
				oModelT14 := oModel:GetModel( 'MODEL_T14' )
				oModelC9K := oModel:GetModel( 'MODEL_C9K' )
				oModelC9L := oModel:GetModel( 'MODEL_C9L' )
				oModelC9M := oModel:GetModel( 'MODEL_C9M' )
				oModelT6Y := oModel:GetModel( 'MODEL_T6Y' )
				oModelT6Z := oModel:GetModel( 'MODEL_T6Z' )
				oModelC9N := oModel:GetModel( 'MODEL_C9N' )
				oModelC9O := oModel:GetModel( 'MODEL_C9O' )
				oModelC9P := oModel:GetModel( 'MODEL_C9P' )
				oModelC9Q := oModel:GetModel( 'MODEL_C9Q' )
				oModelC9R := oModel:GetModel( 'MODEL_C9R' )
				oModelCRN := oModel:GetModel( 'MODEL_CRN' )

				if TafLayESoc("02_04_02")
					oModelV1B := oModel:GetModel( 'MODEL_V1B' )
					oModelV1C := oModel:GetModel( 'MODEL_V1C' )
				else
					oModelT89 := oModel:GetModel( 'MODEL_T89' )
				endif

				//�����������������������������������������������������������Ŀ
				//�Busco a versao anterior do registro para gravacao do rastro�
				//�������������������������������������������������������������
				cVerAnt	 	 := oModelC91:GetValue( "C91_VERSAO" )
				cProtocolo	 := oModelC91:GetValue( "C91_PROTUL" )
				cEvento 	 := oModelC91:GetValue( "C91_EVENTO" )

				If TafColumnPos( "C91_LOGOPE" )
					cLogOpeAnt := oModelC91:GetValue( "C91_LOGOPE" )
				endif

				//�����������������������������������������������������������������Ŀ
				//�Neste momento eu gravo as informacoes que foram carregadas       �
				//�na tela, pois neste momento o usuario ja fez as modificacoes que �
				//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
				//�nao devem ser consideradas neste momento                         �
				//�������������������������������������������������������������������
				For nlI := 1 To 1
					For nlY := 1 To Len( oModelC91:aDataModel[ nlI ] )
						Aadd( aGrava, { oModelC91:aDataModel[ nlI, nlY, 1 ], oModelC91:aDataModel[ nlI, nlY, 2 ] } )
					Next
				Next

				//Posicionando no registro
				DBSelectArea("T14")
				DBSetOrder(1)
				/*------------------------------------------
					T14 - Informa��es do Recibo de Pag.
				--------------------------------------------*/
				If T14->(MsSeek(xFilial("T14")+C91->(C91_ID + C91_VERSAO) ) )

					For nT14 := 1 To oModel:GetModel( 'MODEL_T14' ):Length()
						oModel:GetModel( 'MODEL_T14' ):GoLine(nT14)

						If !oModel:GetModel( 'MODEL_T14' ):IsDeleted()
							aAdd (aGravaT14 ,{oModelT14:GetValue('T14_IDEDMD'),;
								oModelT14:GetValue('T14_CODCAT'),;
                                oModelT14:GetValue('T14_CODCBO'),;
								oModelT14:GetValue('T14_NATATV'),;
								oModelT14:GetValue('T14_QTDTRB')})

							//DSERTAF1-4588 ->incluida linhas 2108 e 2107

							/*------------------------------------------
								C9K - Informa��es do Estab/Lota��o
							--------------------------------------------*/
							For nC9K := 1 to oModel:GetModel( "MODEL_C9K" ):Length()
								oModel:GetModel( "MODEL_C9K" ):GoLine(nC9K)
								If !oModel:GetModel( 'MODEL_C9K' ):IsEmpty()
									If !oModel:GetModel( "MODEL_C9K" ):IsDeleted()
										aAdd (aGravaC9K ,{	oModelT14:GetValue('T14_IDEDMD'),;
															oModelC9K:GetValue('C9K_ESTABE'),;
															oModelC9K:GetValue('C9K_LOTACA'),;
															oModelC9K:GetValue('C9K_QTDDIA'),;
															oModelC9K:GetValue('C9K_TPINSC'),;
															oModelC9K:GetValue('C9K_NRINSC'),;
															oModelC9K:GetValue('C9K_CODLOT');
															})

										/*------------------------------------------
											C9L - Informa��es da Remunera��o Trab.
										--------------------------------------------*/
										For nC9L := 1 to oModel:GetModel( "MODEL_C9L" ):Length()
											oModel:GetModel( "MODEL_C9L" ):GoLine(nC9L)
											If !oModel:GetModel( 'MODEL_C9L' ):IsEmpty()
												If !oModel:GetModel( "MODEL_C9L" ):IsDeleted()
													aAdd (aGravaC9L ,{	oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA'),;
																		oModelC9L:GetValue('C9L_TRABAL'),;
																		oModelC9L:GetValue('C9L_GRAEXP'),;
																		oModelC9L:GetValue('C9L_INDCON'),;
																		oModelC9L:GetValue('C9L_DTRABA');
																		})

													/*------------------------------------------
														   C9M - Itens da Remunera��o Trab.
													--------------------------------------------*/
													For nC9M := 1 to oModel:GetModel( "MODEL_C9M" ):Length()
														oModel:GetModel( "MODEL_C9M" ):GoLine(nC9M)
														If !oModel:GetModel( 'MODEL_C9M' ):IsEmpty()
															If !oModel:GetModel( "MODEL_C9M" ):IsDeleted()
																aAdd (aGravaC9M ,{	oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL'),;
																					oModelC9M:GetValue('C9M_CODRUB'),;
																					oModelC9M:GetValue('C9M_QTDRUB'),;
																					oModelC9M:GetValue('C9M_VLRUNT'),;
																					oModelC9M:GetValue('C9M_FATORR'),;
																					oModelC9M:GetValue('C9M_VLRRUB'),;
																					oModelC9M:GetValue('C9M_CODLOT'),;
																					oModelC9M:GetValue('C9M_RUBRIC'),;
																					oModelC9M:GetValue('C9M_IDTABR'),;
																					oModelC9M:GetValue('C9M_TPINSC'),;
																					oModelC9M:GetValue('C9M_NRINSC');
																				})
															EndIf
														EndIf
													Next //nC9M

													/*------------------------------------------
														 T6Y - Informa��es de Valores Pagos
													--------------------------------------------*/
													For nT6Y := 1 to oModel:GetModel( "MODEL_T6Y" ):Length()
														oModel:GetModel( "MODEL_T6Y" ):GoLine(nT6Y)
														If !oModel:GetModel( 'MODEL_T6Y' ):IsEmpty()
															If !oModel:GetModel( "MODEL_T6Y" ):IsDeleted()
																aAdd (aGravaT6Y ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL'),;
																	oModelT6Y:GetValue('T6Y_CNPJOP'),;
																	oModelT6Y:GetValue('T6Y_REGANS'),;
																	oModelT6Y:GetValue('T6Y_VLPGTI')})

																/*------------------------------------------
																 	T6Z - Informa��es dos Dependentes
																--------------------------------------------*/
																For nT6Z := 1 to oModel:GetModel( "MODEL_T6Z" ):Length()
																	oModel:GetModel( "MODEL_T6Z" ):GoLine(nT6Z)
																	If !oModel:GetModel( 'MODEL_T6Z' ):IsEmpty()
																		If !oModel:GetModel( "MODEL_T6Z" ):IsDeleted()
																			aAdd (aGravaT6Z ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL')+oModelT6Y:GetValue('T6Y_CNPJOP')+oModelT6Y:GetValue('T6Y_REGANS'),;
																				oModelT6Z:GetValue('T6Z_SEQUEN'),;
																				oModelT6Z:GetValue('T6Z_TPDEP') ,;
																				oModelT6Z:GetValue('T6Z_CPFDEP'),;
																				oModelT6Z:GetValue('T6Z_NOMDEP'),;
																				oModelT6Z:GetValue('T6Z_DTNDEP'),;
																				oModelT6Z:GetValue('T6Z_VPGDEP')})
																		EndIf
																	EndIf
																Next //nT6Z
															EndIf
														EndIf
													Next //nT6Y

													if TafLayESoc("02_04_02")
														/*------------------------------------------
															V1B - Infos Trab Interm Per Apura��o
														--------------------------------------------*/
														For nV1B := 1 to oModel:GetModel( "MODEL_V1B" ):Length()
															oModel:GetModel( "MODEL_V1B" ):GoLine(nV1B)
															If !oModel:GetModel( 'MODEL_V1B' ):IsEmpty()
																If !oModel:GetModel( "MODEL_V1B" ):IsDeleted()
																	aAdd (aGravaV1B ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL'),;
																		oModelV1B:GetValue('V1B_IDCONV')})
																EndIf
															EndIf
														Next //nV1B
													endif

												EndIf
											EndIf
										Next //nC9L
									EndIf
								EndIf
							Next //nC9K

							//Posicionando no registro
							//DBSelectArea("T3Y")
							//DBSetOrder(1)
							/*------------------------------------------
								C9N - Informa��es de Acordo
							--------------------------------------------*/
							If C9N->(MsSeek(xFilial("C9N")+C91->(C91_ID + C91_VERSAO) + T14->T14_IDEDMD) )
								For nC9N := 1 To oModel:GetModel( 'MODEL_C9N' ):Length()
									oModel:GetModel( 'MODEL_C9N' ):GoLine(nC9N)
									If !oModel:GetModel( 'MODEL_C9N' ):IsDeleted()
										aAdd (aGravaC9N ,{oModelC9N:GetValue('C9N_DTACOR')	,;
											oModelC9N:GetValue('C9N_TPACOR') 	,;
											oModelC9N:GetValue('C9N_COMPAC') 	,;
											oModelC9N:GetValue('C9N_DTEFAC')  ,;
											oModelC9N:GetValue('C9N_DSC')		,;
											oModelC9N:GetValue('C9N_REMUNS')})

													/*------------------------------------
														C9O - Informa��es do Periodo
													-------------------------------------*/
										For nC9O := 1 to oModel:GetModel( "MODEL_C9O" ):Length()
											oModel:GetModel( "MODEL_C9O" ):GoLine(nC9O)
											If !oModel:GetModel( 'MODEL_C9O' ):IsEmpty()
												If !oModel:GetModel( "MODEL_C9O" ):IsDeleted()
													aAdd (aGravaC9O ,{AllTrim(oModelT14:GetValue('T14_IDEDMD'))+DtoC(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR'),;
														oModelC9O:GetValue('C9O_PERREF')})

																/*------------------------------------
																  C9P - Informa��es do Estab/Lota��o
																-------------------------------------*/
													For nC9P := 1 to oModel:GetModel( "MODEL_C9P" ):Length()
														oModel:GetModel( "MODEL_C9P" ):GoLine(nC9P)
														If !oModel:GetModel( 'MODEL_C9P' ):IsEmpty()
															If !oModel:GetModel( 'MODEL_C9P' ):IsDeleted()
																aAdd (aGravaC9P ,{AllTrim(oModelT14:GetValue('T14_IDEDMD'))+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF'),;
																	oModelC9P:GetValue('C9P_ESTABE'),;
																	oModelC9P:GetValue('C9P_LOTACA')})

																			/*------------------------------------------
																				C9Q - Informa��es da Remunera��o Trab.
																			--------------------------------------------*/
																For nC9Q := 1 to oModel:GetModel( "MODEL_C9Q" ):Length()
																	oModel:GetModel( "MODEL_C9Q" ):GoLine(nC9Q)
																	If !oModel:GetModel( 'MODEL_C9Q' ):IsEmpty()
																		If !oModel:GetModel( "MODEL_C9Q" ):IsDeleted()
																			aAdd (aGravaC9Q ,{AllTrim(oModelT14:GetValue('T14_IDEDMD'))+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF')+oModelC9P:GetValue('C9P_ESTABE')+oModelC9P:GetValue('C9P_LOTACA'),;
																				oModelC9Q:GetValue('C9Q_TRABAL')	,;
																				oModelC9Q:GetValue('C9Q_GRAEXP')	,;
																				oModelC9Q:GetValue('C9Q_INDCON')  })

																						/*------------------------------------------
																							   C9R - Itens da Remunera��o Trab.
																						--------------------------------------------*/
																			For nC9R := 1 to oModel:GetModel( "MODEL_C9R" ):Length()
																				oModel:GetModel( "MODEL_C9R" ):GoLine(nC9R)
																				If !oModel:GetModel( 'MODEL_C9R' ):IsEmpty()
																					If !oModel:GetModel( "MODEL_C9R" ):IsDeleted()
																						aAdd (aGravaC9R ,{AllTrim(oModelT14:GetValue('T14_IDEDMD'))+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF')+oModelC9P:GetValue('C9P_ESTABE')+oModelC9P:GetValue('C9P_LOTACA')+oModelC9Q:GetValue('C9Q_TRABAL'),;
																							oModelC9R:GetValue('C9R_CODRUB'),;
																							oModelC9R:GetValue('C9R_QTDRUB'),;
																							oModelC9R:GetValue('C9R_VLRUNT'),;
																							oModelC9R:GetValue('C9R_FATORR'),;																							
																							oModelC9R:GetValue('C9R_VLRRUB')})
																					EndIf
																				EndIf
																			Next //nC9R

																			if TafLayESoc("02_04_02")
																							/*------------------------------------------
																								V1C - Infos Trab Interm Per Anterior
																							--------------------------------------------*/
																				For nV1C := 1 to oModel:GetModel( "MODEL_V1C" ):Length()
																					oModel:GetModel( "MODEL_V1C" ):GoLine(nV1C)
																					If !oModel:GetModel( 'MODEL_V1C' ):IsEmpty()
																						If !oModel:GetModel( "MODEL_V1C" ):IsDeleted()
																							aAdd (aGravaV1C ,{AllTrim(oModelT14:GetValue('T14_IDEDMD'))+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF')+oModelC9P:GetValue('C9P_ESTABE')+oModelC9P:GetValue('C9P_LOTACA')+oModelC9Q:GetValue('C9Q_TRABAL'),;
																								oModelV1C:GetValue('V1C_IDCONV')})
																						EndIf
																					EndIf
																				Next //nV1C
																			endif

																		EndIf
																	EndIf
																Next //nC9Q
															EndIf
														EndIf
													Next //nC9P
												EndIf
											EndIf
										Next //nC9O
												//EndIf
											//EndIf
										//Next //nT3Y
									EndIf
								Next //nC9N
							EndIf

							if !TafLayESoc("02_04_02")
								For nT89 := 1 to oModel:GetModel( "MODEL_T89" ):Length()
									oModel:GetModel( "MODEL_T89" ):GoLine(nT89)
									If !oModel:GetModel( 'MODEL_T89' ):IsEmpty()
										If !oModel:GetModel( "MODEL_T89" ):IsDeleted()
											aAdd (aGravaT89 ,{oModelT14:GetValue('T14_IDEDMD'),;
												oModelT89:GetValue('T89_IDCONV')})
										EndIf
									EndIf
								Next nT89
							endif

						EndIf
					Next //nT14
				EndIf

				For nlI := 1 To oModel:GetModel( 'MODEL_CRN' ):Length()
					oModel:GetModel( 'MODEL_CRN' ):GoLine(nlI)

					If !oModel:GetModel( 'MODEL_CRN' ):IsDeleted() .And. !oModel:GetModel( 'MODEL_CRN' ):IsEmpty()
						aAdd (aGravaCRN,{	oModelCRN:GetValue('CRN_TPTRIB')	,;
							oModelCRN:GetValue('CRN_IDPROC')	,;
							oModelCRN:GetValue('CRN_IDSUSP')	})
					EndIf
				Next

				For nlI := 1 To oModel:GetModel( 'MODEL_T6W' ):Length()
					oModel:GetModel( 'MODEL_T6W' ):GoLine(nlI)

					If !oModel:GetModel( 'MODEL_T6W' ):IsDeleted() .And. !oModel:GetModel( 'MODEL_T6W' ):IsEmpty()
						aAdd (aGravaT6W ,{	    oModelT6W:GetValue('T6W_TPINSC') ,;
							oModelT6W:GetValue('T6W_NRINSC') ,;
							oModelT6W:GetValue('T6W_CODCAT') ,;
							oModelT6W:GetValue('T6W_VLREMU')	})
					EndIf
				Next

				//�����������������������������������������������������������Ŀ
				//�Seto o campo como Inativo e gravo a versao do novo registro�
				//�no registro anterior                                       �
				//|                                                           |
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������
				FAltRegAnt( 'C91', '2' )

				//��������������������������������������������������Ŀ
				//�Neste momento eu preciso setar a operacao do model�
				//�como Inclusao                                     �
				//����������������������������������������������������
				oModel:DeActivate()
				oModel:SetOperation( 3 )
				oModel:Activate()

				//�������������������������������������������������������Ŀ
				//�Neste momento eu realizo a inclusao do novo registro ja�
				//�contemplando as informacoes alteradas pelo usuario     �
				//���������������������������������������������������������
				For nlI := 1 To Len( aGrava )
					oModel:LoadValue( 'MODEL_C91', aGrava[ nlI, 1 ], aGrava[ nlI, 2 ] )
				Next

				//Necess�rio Abaixo do For Nao Retirar
				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_C91', 'C91_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				For nlI := 1 To Len( aGravaT6W )

                    oModel:GetModel( 'MODEL_T6W' ):LVALID	:= .T.

					If nlI > 1
						oModel:GetModel( 'MODEL_T6W' ):AddLine()
					EndIf
					oModel:LoadValue( "MODEL_T6W", "T6W_TPINSC",	aGravaT6W[nlI][1] )
					oModel:LoadValue( "MODEL_T6W", "T6W_NRINSC",	aGravaT6W[nlI][2] )
					oModel:LoadValue( "MODEL_T6W", "T6W_CODCAT",	aGravaT6W[nlI][3] )
					oModel:LoadValue( "MODEL_T6W", "T6W_VLREMU",	aGravaT6W[nlI][4] )
					oModel:LoadValue( "MODEL_T6W", "T6W_NOMEVE", "S1200")
				Next

				/*------------------------------------------
					T14 - Informa��es do Recibo de Pag.
				--------------------------------------------*/
				For nT14 := 1 to Len( aGravaT14 )

					oModel:GetModel( 'MODEL_T14' ):LVALID	:= .T.

					If nT14 > 1
						oModel:GetModel( "MODEL_T14" ):AddLine()
					EndIf

                    /*
                    DSERTAF1-4588
                    Incluido mais indices no array aGravaT14 devido
                    a cria��o de novos campos na T14.
                    */

					oModel:LoadValue( "MODEL_T14", "T14_IDEDMD", aGravaT14[nT14][1] )
					oModel:LoadValue( "MODEL_T14", "T14_CODCAT", aGravaT14[nT14][2] )
                    oModel:LoadValue( "MODEL_T14", "T14_CODCBO", aGravaT14[nT14][3] )
					oModel:LoadValue( "MODEL_T14", "T14_NATATV", aGravaT14[nT14][4] )
					oModel:LoadValue( "MODEL_T14", "T14_QTDTRB", aGravaT14[nT14][5] )

					/*------------------------------------------
					   C9K - Informa��es do Estab/Lota��o
					--------------------------------------------*/
					nC9KAdd := 1
					For nC9K := 1 to Len( aGravaC9K )
						If  aGravaC9K[nC9K][1] == aGravaT14[nT14][1]
							oModel:GetModel( 'MODEL_C9K' ):LVALID := .T.

							If nC9KAdd > 1
								oModel:GetModel( "MODEL_C9K" ):AddLine()
							EndIf

							oModel:LoadValue( "MODEL_C9K", "C9K_ESTABE", aGravaC9K[nC9K][2] )
							oModel:LoadValue( "MODEL_C9K", "C9K_LOTACA", aGravaC9K[nC9K][3] )
							oModel:LoadValue( "MODEL_C9K", "C9K_QTDDIA", aGravaC9K[nC9K][4] )

							oModel:LoadValue( "MODEL_C9K", "C9K_TPINSC", aGravaC9K[nC9K][5] )
							oModel:LoadValue( "MODEL_C9K", "C9K_NRINSC", aGravaC9K[nC9K][6] )
							oModel:LoadValue( "MODEL_C9K", "C9K_CODLOT", aGravaC9K[nC9K][7] )
							/*------------------------------------------
							   C9L - Informa��es da Remunera��o Trab.
							--------------------------------------------*/
							nC9LAdd := 1
							For nC9L := 1 to Len( aGravaC9L )
								If aGravaC9L[nC9L][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3])
									oModel:GetModel( 'MODEL_C9L' ):LVALID := .T.

									If nC9LAdd > 1
										oModel:GetModel( "MODEL_C9L" ):AddLine()
									EndIf

									oModel:LoadValue( "MODEL_C9L", "C9L_TRABAL", aGravaC9L[nC9L][2] )
									oModel:LoadValue( "MODEL_C9L", "C9L_GRAEXP", aGravaC9L[nC9L][3] )
									oModel:LoadValue( "MODEL_C9L", "C9L_INDCON", aGravaC9L[nC9L][4] )
									oModel:LoadValue( "MODEL_C9L", "C9L_DTRABA", aGravaC9L[nC9L][5] )

									/*------------------------------------------
									   	  C9M - Itens da Remunera��o Trab.
									--------------------------------------------*/
									nC9MAdd := 1
									For nC9M := 1 to Len( aGravaC9M )
										If aGravaC9M[nC9M][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2])
											oModel:GetModel( 'MODEL_C9M' ):LVALID := .T.

											If nC9MAdd > 1
												oModel:GetModel( "MODEL_C9M" ):AddLine()
											EndIf

											oModel:LoadValue( "MODEL_C9M", "C9M_CODRUB",	aGravaC9M[nC9M][2] )
											oModel:LoadValue( "MODEL_C9M", "C9M_QTDRUB",	aGravaC9M[nC9M][3] )
											oModel:LoadValue( "MODEL_C9M", "C9M_VLRUNT",	aGravaC9M[nC9M][4] )
											oModel:LoadValue( "MODEL_C9M", "C9M_FATORR",	aGravaC9M[nC9M][5] )
											oModel:LoadValue( "MODEL_C9M", "C9M_VLRRUB",	aGravaC9M[nC9M][6] )

											oModel:LoadValue( "MODEL_C9M", "C9M_CODLOT",	aGravaC9M[nC9M][7] )
											oModel:LoadValue( "MODEL_C9M", "C9M_RUBRIC",	aGravaC9M[nC9M][8] )
											oModel:LoadValue( "MODEL_C9M", "C9M_IDTABR",	aGravaC9M[nC9M][9] )
											oModel:LoadValue( "MODEL_C9M", "C9M_TPINSC",	aGravaC9M[nC9M][10] )
											oModel:LoadValue( "MODEL_C9M", "C9M_NRINSC",	aGravaC9M[nC9M][11] )

											nC9MAdd++
										EndIf
									Next //nC9M

									/*------------------------------------------
									   	  T6Y - Informa��es de Valores Pagos
									--------------------------------------------*/
									nT6YAdd := 1
									For nT6Y := 1 to Len( aGravaT6Y )
										If aGravaT6Y[nT6Y][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2])
											oModel:GetModel( 'MODEL_T6Y' ):LVALID := .T.

											If nT6YAdd > 1
												oModel:GetModel( "MODEL_T6Y" ):AddLine()
											EndIf

											oModel:LoadValue( "MODEL_T6Y", "T6Y_CNPJOP",	aGravaT6Y[nT6Y][2] )
											oModel:LoadValue( "MODEL_T6Y", "T6Y_REGANS",	aGravaT6Y[nT6Y][3] )
											oModel:LoadValue( "MODEL_T6Y", "T6Y_VLPGTI",	aGravaT6Y[nT6Y][4] )
											oModel:LoadValue( "MODEL_T6Y", "T6Y_NOMEVE", "S1200")

											/*------------------------------------------
											   	  T6Z - Informa��es dos Dependentes
											--------------------------------------------*/
											nT6ZAdd := 1
											For nT6Z := 1 to Len( aGravaT6Z )
												If aGravaT6Z[nT6Z][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2]+aGravaT6Y[nT6Y][2]+aGravaT6Y[nT6Y][3])
													oModel:GetModel( 'MODEL_T6Z' ):LVALID := .T.

													If nT6ZAdd > 1
														oModel:GetModel( "MODEL_T6Z" ):AddLine()
													EndIf

													oModel:LoadValue( "MODEL_T6Z", "T6Z_SEQUEN",	aGravaT6Z[nT6Z][2] )
													oModel:LoadValue( "MODEL_T6Z", "T6Z_TPDEP" ,	aGravaT6Z[nT6Z][3] )
													oModel:LoadValue( "MODEL_T6Z", "T6Z_CPFDEP",	aGravaT6Z[nT6Z][4] )
													oModel:LoadValue( "MODEL_T6Z", "T6Z_NOMDEP",	aGravaT6Z[nT6Z][5] )
													oModel:LoadValue( "MODEL_T6Z", "T6Z_DTNDEP",	aGravaT6Z[nT6Z][6] )
													oModel:LoadValue( "MODEL_T6Z", "T6Z_VPGDEP",	aGravaT6Z[nT6Z][7] )
													oModel:LoadValue( "MODEL_T6Z", "T6Z_NOMEVE",	"S1200" )

													nT6ZAdd++
												EndIf
											Next //nT6Z
											nT6YAdd++
										EndIf
									Next //nT6Y

									if TafLayESoc("02_04_02")
										/*------------------------------------------
											V1B - Infos Trab Interm Per Apura��o
										--------------------------------------------*/
										nV1BAdd := 1
										For nV1B := 1 to Len( aGravaV1B )
											If aGravaV1B[nV1B][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2])
												oModel:GetModel( 'MODEL_V1B' ):LVALID := .T.

												If nV1BAdd > 1
													oModel:GetModel( "MODEL_V1B" ):AddLine()
												EndIf

												oModel:LoadValue( "MODEL_V1B", "V1B_CODCON",	aGravaV1B[nV1B][2] )
												nV1BAdd++
											EndIf
										Next //nV1B
									endif

									nC9LAdd++
								EndIf
							Next //nC9L
							nC9KAdd++
						EndIf
					Next //nC9K

					/*------------------------------------------
						C9N - Informa��es de Acordo
					--------------------------------------------*/
					For nC9N := 1 to Len( aGravaC9N )

						oModel:GetModel( 'MODEL_C9N' ):LVALID	:= .T.

						If nC9N > 1
							oModel:GetModel( "MODEL_C9N" ):AddLine()
						EndIf

						oModel:LoadValue( "MODEL_C9N", "C9N_DTACOR",	aGravaC9N[nC9N][1] )
						oModel:LoadValue( "MODEL_C9N", "C9N_TPACOR",	aGravaC9N[nC9N][2] )
						oModel:LoadValue( "MODEL_C9N", "C9N_COMPAC",	aGravaC9N[nC9N][3] )
						oModel:LoadValue( "MODEL_C9N", "C9N_DTEFAC",	aGravaC9N[nC9N][4] )
						oModel:LoadValue( "MODEL_C9N", "C9N_DSC"   ,	aGravaC9N[nC9N][5] )
						oModel:LoadValue( "MODEL_C9N", "C9N_REMUNS",	aGravaC9N[nC9N][6] )

						nC9OAdd := 1
						For nC9O := 1 to Len( aGravaC9O )
							If  aGravaC9O[nC9O][1] == AllTrim(aGravaT14[nT14][1]) + (DtoC(aGravaC9N[nC9N][1]) + aGravaC9N[nC9N][2])
								oModel:GetModel( 'MODEL_C9O' ):LVALID := .T.

								If nC9OAdd > 1
									oModel:GetModel( "MODEL_C9O" ):AddLine()
								EndIf

								oModel:LoadValue( "MODEL_C9O", "C9O_PERREF", aGravaC9O[nC9O][2] )

								/*------------------------------------------
								   C9P - Informa��es do Estab/Lota��o
								--------------------------------------------*/
								nC9PAdd := 1
								For nC9P := 1 to Len( aGravaC9P )
									If aGravaC9P[nC9P][1] == AllTrim(aGravaT14[nT14][1]) + (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2])
										oModel:GetModel( 'MODEL_C9P' ):LVALID := .T.

										If nC9PAdd > 1
											oModel:GetModel( "MODEL_C9P" ):AddLine()
										EndIf

										oModel:LoadValue( "MODEL_C9P", "C9P_ESTABE",	aGravaC9P[nC9P][2] )
										oModel:LoadValue( "MODEL_C9P", "C9P_LOTACA",	aGravaC9P[nC9P][3] )


										/*------------------------------------------
										   C9Q - Informa��es da Remunera��o Trab.
										--------------------------------------------*/
										nC9QAdd := 1
										For nC9Q := 1 to Len( aGravaC9Q )
											If  aGravaC9Q[nC9Q][1] == AllTrim(aGravaT14[nT14][1]) + (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2]+aGravaC9P[nC9P][2]+aGravaC9P[nC9P][3])
												oModel:GetModel( 'MODEL_C9Q' ):LVALID := .T.

												If nC9QAdd > 1
													oModel:GetModel( "MODEL_C9Q" ):AddLine()
												EndIf

												oModel:LoadValue( "MODEL_C9Q", "C9Q_TRABAL",	aGravaC9Q[nC9Q][2] )
												oModel:LoadValue( "MODEL_C9Q", "C9Q_GRAEXP",	aGravaC9Q[nC9Q][3] )
												oModel:LoadValue( "MODEL_C9Q", "C9Q_INDCON",    aGravaC9Q[nC9Q][4] )

												/*------------------------------------------
												   	  C9R - Itens da Remunera��o Trab.
												--------------------------------------------*/
												nC9RAdd := 1
												For nC9R := 1 to Len( aGravaC9R )
													If  aGravaC9R[nC9R][1] == AllTrim(aGravaT14[nT14][1]) + (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2]+aGravaC9P[nC9P][2]+aGravaC9P[nC9P][3]+aGravaC9Q[nC9Q][2])
														oModel:GetModel( 'MODEL_C9R' ):LVALID := .T.

														If nC9RAdd > 1
															oModel:GetModel( "MODEL_C9R" ):AddLine()
														EndIf

														oModel:LoadValue( "MODEL_C9R", "C9R_CODRUB",	aGravaC9R[nC9R][2] )
														oModel:LoadValue( "MODEL_C9R", "C9R_QTDRUB",	aGravaC9R[nC9R][3] )
														oModel:LoadValue( "MODEL_C9R", "C9R_VLRUNT",	aGravaC9R[nC9R][4] )
														oModel:LoadValue( "MODEL_C9R", "C9R_FATORR",	aGravaC9R[nC9R][5] )
														oModel:LoadValue( "MODEL_C9R", "C9R_VLRRUB",	aGravaC9R[nC9R][6] )

														nC9RAdd++

													EndIf
												Next //nC9R

												if TafLayESoc("02_04_02")
													/*------------------------------------------
														V1C - Infos Trab Interm Per Anterior
													--------------------------------------------*/
													nV1CAdd := 1
													For nV1C := 1 to Len( aGravaV1C )
														If  aGravaV1C[nV1C][1] == AllTrim(aGravaT14[nT14][1]) + (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2]+aGravaC9P[nC9P][2]+aGravaC9P[nC9P][3]+aGravaC9Q[nC9Q][2])
															oModel:GetModel( 'MODEL_V1C' ):LVALID := .T.

															If nV1CAdd > 1
																oModel:GetModel( "MODEL_V1C" ):AddLine()
															EndIf

															oModel:LoadValue( "MODEL_V1C", "V1C_IDCONV",	aGravaV1C[nV1C][2] )

															nV1CAdd++
														EndIf
													Next //nV1C
												endif

												nC9QAdd++
											EndIf
										Next //nC9Q
									EndIf
								Next //nC9P
								nC9OAdd++
							EndIf
						Next //nC9O
					Next //nC9N

					if !TafLayESoc("02_04_02")
						//Informa��es das convoca��es de trabalho Intermitente
						For nT89 :=1 To Len(aGravaT89)
							If  aGravaT89[nT89][1] == aGravaT14[nT14][1]
								oModel:GetModel( 'MODEL_T89' ):LVALID := .T.

								If nT89Add > 1
									oModel:GetModel( "MODEL_T89" ):AddLine()
								EndIf

								oModel:LoadValue( "MODEL_T89", "T89_IDCONV", aGravaT89[nT89][2] )
								nT89Add++
							EndIf
						Next nT89
					endif

				Next //nT14

				For nlI := 1 To Len( aGravaCRN )

                    oModel:GetModel( 'MODEL_CRN' ):LVALID	:= .T.

					If nlI > 1
						oModel:GetModel( 'MODEL_CRN' ):AddLine()
					EndIf
					oModel:LoadValue( "MODEL_CRN", "CRN_TPTRIB" ,	aGravaCRN[nlI][1] )
					oModel:LoadValue( "MODEL_CRN", "CRN_IDPROC" ,	aGravaCRN[nlI][2] )
					oModel:LoadValue( "MODEL_CRN", "CRN_IDSUSP" ,	aGravaCRN[nlI][3] )
				Next


				//�������������������������������Ŀ
				//�Busco a versao que sera gravada�
				//���������������������������������
				cVersao := xFunGetVer()

				//�����������������������������������������������������������Ŀ
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������
				oModel:LoadValue( 'MODEL_C91', 'C91_VERSAO', cVersao )
				oModel:LoadValue( 'MODEL_C91', 'C91_VERANT', cVerAnt )
				oModel:LoadValue( 'MODEL_C91', 'C91_PROTPN', cProtocolo )
				oModel:LoadValue( 'MODEL_C91', 'C91_PROTUL', "" )
				oModel:LoadValue( 'MODEL_C91', 'C91_EVENTO', "A" )

				// Tratamento para limpar o ID unico do xml
				cAliasPai := "C91"
				If TAFColumnPos( cAliasPai+"_XMLID" )
					oModel:LoadValue( 'MODEL_'+cAliasPai, cAliasPai+'_XMLID', "" )
				EndIf

				//Gravo altera��o para o Extempor�neo
				If lGoExtemp
					TafGrvExt( oModel, 'MODEL_C91', 'C91' )
				Endif

				If FWFormCommit( oModel )
					aAnalitico := LoadReportData(oModel, lInfoRPT)
					oInfoRPT:SetAnalitico(aAnalitico)
					oReport:UpSert( "S-1200", "2", xFilial( "C91" ), oInfoRPT )
				EndIf

				TAFAltStat( 'C91', " " )

			ElseIf C91->C91_STATUS == ( "2" )
				TAFMsgVldOp(oModel,"2")//"Registro n�o pode ser alterado. Aguardando processo da transmiss�o."
				lRetorno := .F.
			Elseif C91->C91_STATUS == ( "6" )
				TAFMsgVldOp(oModel,"6")//"Registro n�o pode ser alterado. Aguardando proc. Transm. evento de Exclus�o S-3000"
				lRetorno := .F.
			Elseif C91->C91_STATUS == "7"
				TAFMsgVldOp(oModel,"7") //"Registro n�o pode ser alterado, pois o evento j� se encontra na base do RET"
				lRetorno:= .F.
			Else
				oModel:LoadValue( "MODEL_C91", "C91_NOMEVE", "S1200" )

				If TafColumnPos( "C91_LOGOPE" )
					cLogOpeAnt := C91->C91_LOGOPE
				endif

				oModelT6W := oModel:GetModel( "MODEL_T6W" )
				For nT6W := 1 to oModelT6W:Length()
					oModelT6W:GoLine( nT6W )
					If !oModelT6W:IsEmpty() .and. !oModelT6W:IsDeleted()
						oModelT6W:LoadValue( "T6W_NOMEVE", "S1200" )
					EndIf
				Next nT6W

				oModelT6Y := oModel:GetModel( "MODEL_T6Y" )
				For nT6Y := 1 to oModelT6Y:Length()
					oModelT6Y:GoLine( nT6Y )
					If !oModelT6Y:IsEmpty() .and. !oModelT6Y:IsDeleted()
						oModelT6Y:LoadValue( "T6Y_NOMEVE", "S1200" )
					EndIf
				Next nT6Y

				oModelT6Z := oModel:GetModel( "MODEL_T6Z" )
				For nT6Z := 1 to oModelT6Z:Length()
					oModelT6Z:GoLine( nT6Z )
					If !oModelT6Z:IsEmpty() .and. !oModelT6Z:IsDeleted()
						oModelT6Z:LoadValue( "T6Z_NOMEVE", "S1200" )
					EndIf
				Next nT6Z

				//Gravo altera��o para o Extempor�neo
				If lGoExtemp
					TafGrvExt( oModel, 'MODEL_C91', 'C91' )
				Endif

				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_C91', 'C91_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				If FWFormCommit( oModel )
					aAnalitico := LoadReportData(oModel, lInfoRPT)
					oInfoRPT:SetAnalitico(aAnalitico)
					oReport:UpSert( "S-1200", "2", xFilial( "C91" ), oInfoRPT )
				EndIf
				
				TAFAltStat( "C91", " " )
			EndIf
		EndIf

	ElseIf nOperation == MODEL_OPERATION_DELETE

		cChvRegAnt := C91->(C91_ID + C91_VERANT)

		If !Empty( cChvRegAnt )
			TAFAltStat( 'C91', " " )
			If FwFormCommit( oModel )
				oReport:UpSert( "S-1200", "2", xFilial( "C91" ), oInfoRPT, .T. )
			EndIf
			If nOperation == MODEL_OPERATION_DELETE
				If C91->C91_EVENTO == "A" .Or. C91->C91_EVENTO == "E"
					TAFRastro( 'C91', 1, cChvRegAnt, .T., , IIF(Type("oBrw") == "U", Nil, oBrw) )
				EndIf
			EndIf
		Else
			oModel:DeActivate()
			oModel:SetOperation( 5 )
			oModel:Activate()
			If FwFormCommit( oModel )
				oReport:UpSert( "S-1200", "2", xFilial( "C91" ), oInfoRPT, .T. )
			EndIf
		EndIf

	EndIf

End Transaction

If !lRetorno
	// Define a mensagem de erro que ser� exibida ap�s o Return do SaveModel
	TAFMsgDel(oModel,.T.)
EndIf


If lRetorno .and. FWIsInCallStack( "TAF250AUTI" )

	//Executa atualiza��o dos registros que s�o eleg��veis para aut�nomos do S-1200 
	FwMsgRun(, { || AtuRegC91() }, "Atualizando Registros S-1200 Aut�nomos...", "Aguarde") 	//"Atualizando Registros S-1200 Aut�nomos.."

EndIf

Return ( lRetorno )
//-------------------------------------------------------------------
/*/{Protheus.doc} Taf250Vld

Funcao que valida os dados do registro posicionado,
verificando se ha incoerencias nas informacos
caso seja necessario gerar um XML

lJob - Informa se foi chamado por Job

@return .T.

@author Vitor Siqueira
@since 08/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Function Taf250Vld(cAlias,nRecno,nOpc,lJob)

Local aLogErro   := {}
Local aDadosUtil := {}

Local cChave     	:= ""
Local cCPF      	:= ""
Local cTpRubrica	:= ""
Local cMultRubr	:= ""
Local cClassTrib	:= ""
Local cC9KTpInsc	:= ""
Local cC9LCodCat	:= ""
Local cC9MIdTabRub  := ""
Local cC9QCodCat 	:= ""
Local cC9MIdNat  	:= ""
Local cC9MCodNat 	:= ""
Local cC9MIdCP   	:= ""
Local cC9MCodCP 	:= ""
Local cC9MIdIR  	:= ""
Local cC9MCodIR  	:= ""
Local cC9PTpInsc 	:= ""
Local cC9RIdNat 	:= ""
Local cC9RCodNat 	:= ""
Local cC9RIdCP 	:= ""
Local cC9RCodCP 	:= ""
Local cC9RIdIR 	:= ""
Local cC9RCodIR 	:= ""
Local cDtAcordo  	:= ""
Local cDtApur	   	:= ""
Local cDtPerApur	:= ""
Local cDtIniC1E	:= ""
Local cIdTerm		:= ""
Local cTpLot		:= ""
Local cC9MIndTrb	:= ""
Local cC9MDtIni	:= ""
Local cC9RDtIni	:= ""
Local cC9MDtFim	:= ""
Local cC9RDtFim	:= ""
Local cMesAnoDesl	:= ""
Local dDtIni		:= ctod('//')
Local dDtFim		:= ctod('//')
Local dDtQua		:= ctod('//')
Local dDataApur	:= ctod('//')
Local nRubVenc	:= 0
Local nRubDesc	:= 0
Local nProvento	:= 0
Local nDesconto	:= 0
Local lCat701OK	:= .F.
Local lRubNat9219	:= .F.
Local cCodErro 	:= ""
Local lRubPLR	:= .F.
Local lRemDesl_A	:= .F.
Local lRemDesl_B	:= .F.
Local lRemDesl_D	:= .F.
Local lRemDesl_F	:= .F.
Local lC9QCOMPAC    := TAFColumnPos("C9Q_COMPAC")
Local lMV           := C91->C91_MV == '1'
Local cEmpCPF       := ""

Default lJob   	:= .F.

//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := C91->( Recno() )
If (C91->C91_STATUS) $ " 1" .And. !lMV
	
	//Pega algumas informa��es da C1E para utiliza��o na valida��o
	DbSelectArea('C1E')
	C1E->(DBSetOrder(3))
	If C1E->( MsSeek ( xFilial("C1E") + Left(SM0->M0_CODFIL, FWSizeFilial())))
		cClassTrib := POSICIONE("C8D",1, xFilial("C8D")+C1E->C1E_CLAFIS,"C8D_CODIGO")
		cMultRubr  := C1E->C1E_MTBRUB
		cDtIniC1E  := C1E->C1E_DTINI
		cEmpCPF    := C1E->C1E_NRCPF
	EndIf

	dbSelectArea("C9V")
	dbSetOrder(2)
	C9V->(MsSeek(C91->C91_FILIAL+C91->C91_TRABAL+"1"))

	// ----------------------------------------------------------
	// 				REGRA_EXISTE_INFO_EMPREGADOR
	// ----------------------------------------------------------
	If !Empty(C91->C91_PERAPU)
		If Len(Alltrim(C91->C91_PERAPU)) == 6 //CONSIDERAR MES/ANO
			cDtPerApur	 :=  Substr(C91->C91_PERAPU,5,2) + Substr(C91->C91_PERAPU,1,4)

		ElseIf Len(Alltrim(C91->C91_PERAPU)) == 4 //CONSIDERAR ANO
			cDtPerApur	 := Substr(cDtIniC1E,1,2) + Alltrim(C91->C91_PERAPU) // PEGA O MESMO MES DT INICIO DO COMPLEM. EMPRESA
		EndIf
	EndIf

	aAdd(aDadosUtil, {xFunDtPer(cDtPerApur,.T.), , C91->C91_FILIAL})
	xVldRegra("C1E","REGRA_EXISTE_INFO_EMPREGADOR",@aLogErro, aDadosUtil, "C91")


    // ----------------------------------------------------------
	// 				REGRA_COMPATIB_REGIME_PREV
	// ----------------------------------------------------------
	aDadosUtil := {}
	aAdd(aDadosUtil, {C91->C91_TRABAL})
	xVldRegra("C9V","REGRA_COMPATIB_REGIME_PREV",@aLogErro, aDadosUtil, "C91")


    // ---------------------------------------------------------------------
	// 				REGRA_RUBRICA_COMPATIVEL_DECTERCEIRO
	// ---------------------------------------------------------------------
	C8R->(DBSetOrder(5)) //C8R_FILIAL+C8R_ID+C8R_ATIVO
	C8U->(DBSetOrder(1))

	If C91->C91_INDAPU == "2"
		if TafLayESoc("02_04_02")
			cCodErro := "001132" //STR1132 Se {indApuracao} for igual a [2] (d�cimo terceiro sal�rio), s�o permitidas apenas as rubricas cujo {codIncCP} da tabela de Rubricas seja igual a [00, 01, 12, 22, 26, 32, 92, 94] e cujo {codIncIRRF} seja igual a [00, 01, 09, 12, 14, 32, 42, 47, 52, 62, 64, 71, 77, 81, 82, 83, 92].
		else
			cCodErro := "000202" //STR0202 - Se {indApuracao} for igual a [2] (d�cimo terceiro sal�rio), s�o permitidas apenas as rubricas cujo {codIncCP} da tabela de Rubricas seja igual a [00, 01, 12, 22, 26, 32, 92, 94] e cujo {codIncIRRF} seja igual a [00, 01, 12, 14, 16, 32, 42, 47, 52, 57, 62, 64, 71, 77, 81, 82, 83, 92].
		endif

		C9M->(DBSetOrder(1))
		If C9M->(MsSeek(xFilial("C9M")+C91->C91_ID+C91->C91_VERSAO))
			Do While !("C9M")->(Eof()) .And. C91->C91_ID+C91->C91_VERSAO == C9M->C9M_ID+C9M->C9M_VERSAO
				If C8R->(MsSeek(xFilial("C8R")+C9M->C9M_CODRUB + '1'))
					If C8T->(MsSeek(xFilial("C8T")+C8R->C8R_CINTPS))
						If !(C8T->C8T_CODIGO $ "00|01|12|22|26|32|92|94|")
							aAdd(aLogErro,{"C8T_CODIGO", cCodErro, "C91", nRecno })
						EndIf
					Else
						aAdd(aLogErro,{"C8T_CODIGO", cCodErro, "C91", nRecno })
					EndIf
					If C8U->(MsSeek(xFilial("C8U")+C8R->C8R_CINTIR))
						If !(C8U->C8U_CODIGO $ iif(TafLayESoc("02_04_02"), "00|01|09|12|14|32|42|47|52|62|64|71|77|81|82|83|92|", "00|01|12|14|16|32|42|47|52|57|62|64|71|77|81|82|83|92|"))
							aAdd(aLogErro,{"C8U_CODIGO", cCodErro, "C91", nRecno })
						EndIf
					Else
						aAdd(aLogErro,{"C8U_CODIGO", cCodErro, "C91", nRecno })
					EndIf
				EndIf
				C9M->(dbSkip())
			EndDo
		EndIf

		C9R->(DBSetOrder(1))
		If C9R->(MsSeek(xFilial("C9R")+C91->C91_ID+C91->C91_VERSAO))
			Do While !("C9R")->(Eof ()) .And. C91->C91_ID+C91->C91_VERSAO == C9R->C9R_ID+C9R->C9R_VERSAO
				If C8R->(MsSeek(xFilial("C8R")+C9R->C9R_CODRUB + '1'))
					If C8T->(MsSeek(xFilial("C8T")+C8R->C8R_CINTPS))
						If !(C8T->C8T_CODIGO $ "00|01|12|22|26|32|92|94|")
							AADD(aLogErro,{"C8T_CODIGO", cCodErro, "C91", nRecno  })
						EndIf
					Else
						AADD(aLogErro,{"C8T_CODIGO", cCodErro, "C91", nRecno  })
					EndIf
					If C8U->(MsSeek(xFilial("C8U")+C8R->C8R_CINTIR))
						If !(C8U->C8U_CODIGO $ iif(TafLayESoc("02_04_02"), "00|01|09|12|14|32|42|47|52|62|64|71|77|81|82|83|92|", "00|01|12|14|16|32|42|47|52|57|62|64|71|77|81|82|83|92|"))
							AADD(aLogErro,{"C8U_CODIGO", cCodErro, "C91", nRecno })
						EndIf
					Else
						AADD(aLogErro,{"C8U_CODIGO", cCodErro, "C91", nRecno })
					EndIf
				EndIf
				C9R->(dbSkip())
			EndDo
		EndIf
	EndIf


	// ----------------------------------------------------------
	// 		 		REGRA_VALIDA_PERIODO_APURACAO
	// ----------------------------------------------------------
	If Empty(C91->C91_PERAPU)
		AADD(aLogErro,{"C91_PERAPU","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	Else
		If C91->C91_INDAPU == "1"
			If Substr(C91->C91_PERAPU,5,2) < "01" .Or. Substr(C91->C91_PERAPU,5,2) > "12" .Or. Len(AllTrim(C91->C91_PERAPU)) <> 6 //Formato MMAAAA
				AADD(aLogErro,{"C91_PERAPU","000318","C91", nRecno }) //STR0318 - "Informar o m�s/ano (formato AAAA-MM) de refer�ncia das informa��es, se {indApuracao} for igual a [1], ou apenas o ano (formato AAAA), se {indApuracao} for igual a [2]"
			EndIf
		ElseIf C91->C91_INDAPU == "2"
			If Len(AllTrim(C91->C91_PERAPU)) <> 4 //Formato AAAA
				AADD(aLogErro,{"C91_PERAPU","000318","C91", nRecno }) //STR0318 - "Informar o m�s/ano (formato AAAA-MM) de refer�ncia das informa��es, se {indApuracao} for igual a [1], ou apenas o ano (formato AAAA), se {indApuracao} for igual a [2]"
			EndIf
		EndIf
	EndIf


	// ----------------------------------------------------------
	// 	 		  REGRA_BLOQUEIA_USO_CPF_EMPREGADOR
	// ----------------------------------------------------------
	cCPF := C9V->C9V_CPF
	If !Empty(cCPF)
		If !CGC(cCPF)
			AADD(aLogErro,{"C91_TRABAL","000790","C91", nRecno })//STR0790 - "O CPF do Trabalhador � inv�lido"
		EndIf

		If AllTrim(SM0->M0_CGC) == AllTrim(cCPF)
			AADD(aLogErro,{"C91_TRABAL","000791","C91", nRecno })//STR0791 - "O CPF do Trabalhador � igual ao CPF do empregador"
		EndIf
	Else
		AADD(aLogErro,{"C91_TRABAL","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	EndIf

	// ----------------------------------------------------------
	// 	 		  REGRA_TSV_ATIVO_NA_DTEVENTO
	// ----------------------------------------------------------
	cDtPerApur := ""

	//Formata a data de apura��o para a valida��o
	If !Empty(C91->C91_PERAPU)
		If Len(Alltrim(C91->C91_PERAPU)) == 6 //CONSIDERAR MES/ANO
			cDtPerApur	 :=  "01/" + Substr(C91->C91_PERAPU,5,2) + "/" + Substr(C91->C91_PERAPU,1,4)

		ElseIf Len(Alltrim(C91->C91_PERAPU)) == 4 //CONSIDERAR ANO
			cDtPerApur	 :=  "01/01/" + Alltrim(C91->C91_PERAPU) // PEGA O MESMO MES DT INICIO DO COMPLEM. EMPRESA
		EndIf
	EndIf

	dDataApur := CTOD(cDtPerApur)

	If C9V->C9V_NOMEVE == 'S2300' // Verifica se o trabalhador � sem vinculo

		DBSelectArea("T92")
		T92->(DBSetOrder(3))
		// Verifica se existe existe evento de termino para o trabalhador S-2399
		If T92->(DBSeek(xFilial("T92")+ C9V->C9V_ID + "1"))

			dDtIni = POSICIONE("CUU",1, xFilial("CUU")+C91->C91_TRABAL,"CUU_DTINCI")
			dDtFim = T92->T92_DTERAV
			dDtQua = T92->T92_DTQUA

			//Se existir data de quarentena cadastrada considerar ela como data de fim
			If !Empty(dDtQua)
				dDtFim := dDtQua
			EndIf

			If dDataApur < dDtIni .OR. dDataApur > dDtFim
				aAdd(aLogErro,{"C91_PERAPU","000813","C91", nRecno })//STR0813 - "A data do evento deve estar compreendida entre a data de in�cio e de t�rmino indicadas nos eventos de 'Trabalhador Sem V�nculo'."
			EndIf

		EndIf
		dbSelectArea("C9V")
	EndIf

	// ----------------------------------------------------------
	// 	 		  REGRA_REMUN_JA_EXISTE_DESLIGAMENTO
	// ----------------------------------------------------------
	CMD->(DBSetOrder(5))
	If CMD->( MsSeek ( xFilial("CMD",C91->C91_FILIAL)+C91->C91_TRABAL+"1"))
		If CMD->CMD_STATUS == '4'

			cMesAnoDesl := substr(Dtoc(CMD->CMD_DTDESL),7) + substr(Dtoc(CMD->CMD_DTDESL),4,2)
			If dDataApur >= CMD->CMD_DTDESL .OR. C91->C91_PERAPU == cMesAnoDesl

				/*a) Remunera��o de PLR em {remunPerApur}.
				Para tanto, deve haver ao menos uma rubrica cujo {tpRubr - C8R_INDTRB}=[1] e {codIncIRRF}= [14]*/
				C9M->(DBSetOrder(1))
				If C9M->(MsSeek(xFilial("C9M")+C91->C91_ID+C91->C91_VERSAO))
					Do While !("C9M")->(Eof()) .And. C91->C91_ID+C91->C91_VERSAO == C9M->C9M_ID+C9M->C9M_VERSAO;
						.And. (!lRemDesl_A .OR. !lRubPLR)
						If C8R->(MsSeek(xFilial("C8R")+C9M->C9M_CODRUB + '1'))
							If C8R->C8R_INDTRB == "1" .And. C8R->C8R_CINTIR == "000005" //"14" == Posicione("C8U",1, xFilial("C8U")+C8R->C8R_CINTIR,"C8U_CODIGO")
								lRemDesl_A	:= .T.			//Enquadra-se na regra a)
							EndIf
							If C8R->C8R_NATRUB == "000033"	//� remunera��o em PLR
								lRubPLR		:= .T.
							EndIf
						EndIf
						C9M->(dbSkip())
					EndDo
				EndIf
				C9R->(DBSetOrder(1))
				If C9R->(MsSeek(xFilial("C9R",C91->C91_FILIAL)+C91->C91_ID+C91->C91_VERSAO))
					Do While !("C9R")->(Eof()) .And. C91->C91_ID+C91->C91_VERSAO == C9R->C9R_ID+C9R->C9R_VERSAO;
						.And. (!lRemDesl_A .OR. !lRubPLR)
						If C8R->(MsSeek(xFilial("C8R",C91->C91_FILIAL)+C9R->C9R_CODRUB + '1'))		
							If C8R->C8R_INDTRB == "1" .And. C8R->C8R_CINTIR == "000005" //"14" == Posicione("C8U",1, xFilial("C8U")+C8R->C8R_CINTIR,"C8U_CODIGO")
								lRemDesl_A	:= .T.			//Enquadra-se na regra a)
							EndIf						
							If C8R->C8R_NATRUB == "000033"	//� remunera��o em PLR
								lRubPLR		:= .T.
							EndIf
						EndIf
						C9R->(dbSkip())
					EndDo
				EndIf
					
				/*b) Remunera��o em {remunPerApur} para trabalhador que esteja em "quarentena", ou seja, o grupo
				"quarentena" tiver sido preenchido no respectivo evento de Desligamento/T�rmino de TSV, e a {dtFimQuar}
				for superior ou igual ao per�odo de apura��o indicado no evento de remunera��o*/
				If !Empty(CMD->CMD_DTQUA) .AND. (CMD->CMD_DTQUA >= (StoD(cValToChar(Val(C91->C91_PERAPU)+1)+"01")-1))
					lRemDesl_B	:= .T.
				EndIf

				/*d) Remunera��o relativa a compet�ncia anual ({indApuracao} = [2]);  */
				If C91->C91_INDAPU == '2'
					lRemDesl_D	:= .T.
				EndIf

				cMotDeslig := POSICIONE("C8O",1, xFilial("C8O")+CMD->CMD_MOTDES,"C8O_CODIGO")
				/*f) Se o desligamento n�o implicar rescis�o do contrato de trabalho ({mtvDeslig} = [11, 12, 13, 25, 28, 29, 30, 34]),
				pode existir remunera��o no evento S-1200 no per�odo de apura��o {perApur} correspondente � data de desligamento {dtDeslig}.*/
				If (cMotDeslig $ "11|12|13|25|28|29|30|34")
					lRemDesl_F	:= .T.
				EndIf

				If !(lRemDesl_A .Or. lRemDesl_B .Or. lRemDesl_D .Or. lRemDesl_F)
					If lRubPLR .AND. !lRemDesl_A
						aAdd(aLogErro,{"C91_TRABAL","001152","C91", nRecno })//001152 - Se possui PLR na Remunera��o do Trabalhador (Tag {remunPerApur} - Campo C8R_NATRUB = '000033'), deve haver ao menos uma r�brica do tipo Vencimento (Tag {tpRubr} - Campo C8R_INDTRB, = '1') e C�digo de Incid�ncia Tribut�ria de IRRF (Cd.Inc.Tr.IR) referente 'Base de C�lculo do IRRF - PLR' (Tag {codIncIRRF} = [14] - Campo C8R_CINTIR = '000005').
					EndIf
					If !Empty(CMD->CMD_DTQUA) .AND. !lRemDesl_B
						aAdd(aLogErro,{"C91_PERAPU","001153","C91", nRecno })//001153 - S� � permitida Remunera��o para trabalhador que esteja em "quarentena" quando a Data do Final da Quarentena (Tag {dtFimQuar} - Campo CMD_DTQUA) for superior ou igual ao per�odo de apura��o indicado no evento de remunera��o (Campo C91_PERAPU - �ltimo dia do m�s).
					EndIf
					If	!(lRubPLR .AND. !lRemDesl_A);
						.AND. !(!Empty(CMD->CMD_DTQUA) .AND. !lRemDesl_B)
						aAdd(aLogErro,{"C91_TRABAL","000998","C91", nRecno })//STR0998 - Se existir no RET evento de desligamento para o trabalhador/v�nculo ao qual se refere a remunera��o, cuja data de desligamento seja anterior ou compreendida em {perApur}, n�o deve existir 'remunera��o' para o referido v�nculo. Exceto se atender a regra do eSocial 'REGRA_REMUN_JA_EXISTE_DESLIGAMENTO'.
					EndIf
				EndIf

			EndIf
		EndIf

	EndIf

	// ----------------------------------------------------
	// 			VALIDA��O IND APUR - C91
	// ----------------------------------------------------
	If Empty(C91->C91_INDAPU)
		AADD(aLogErro,{"C91_INDAPU","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	EndIf

	If !C91->C91_INDMVI $ " 123"
		aAdd(aLogErro,{"C91_INDMVI","000006", "C91", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis."
	EndIf

	DbSelectArea("T6W")
	T6W->( DbSetOrder( 1 ) )
	If T6W->( MsSeek ( xFilial("T6W")+C91->(C91_ID+C91_VERSAO)))
		While !T6W->(Eof()) .And. AllTrim(C91->(C91_ID+C91_VERSAO)) == AllTrim(T6W->(T6W_ID+T6W_VERSAO))

			If !T6W->T6W_TPINSC  $ "12"
				aAdd(aLogErro,{"T6W_TPINSC","000006", "C91", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis."
			EndIf

			If !CGC(T6W->T6W_NRINSC,,.F.)
				aAdd(aLogErro,{"T6W_NRINSC","000005", "C91", nRecno }) //"CPF/CNPJ Inconsistente."
			Else
				If (TafLayESoc("02_05_00") .And. C91->C91_INDAPU == "1") .Or. !TafLayESoc("02_05_00") // Para a vers�o 2.5, a valida��o ocorrer� somente para per�odo mensal, conforme a NT 10/2019
					// ----------------------------------------------------
					// 			VALIDA��O NUM.INSCRI��O - T6W
					// ----------------------------------------------------
					
					If Alltrim(T6W->T6W_NRINSC) == Alltrim(SM0->M0_CGC) .Or. (!Empty(cEmpCPF) .And. Alltrim(T6W->T6W_NRINSC) == AllTrim(cEmpCPF))
						aAdd(aLogErro,{"T6W_NRINSC","000791", "C91", nRecno }) //STR0791 - "O n�mero de inscri��o do contribuinte � igual ao n�mero de inscri��o do empregador"
					EndIf
				EndIf
			EndIf

   			// ----------------------------------------------------
			// 		  VALIDA��O VALOR DA REMUNERA��O - T6W
			// ----------------------------------------------------

			if TafLayESoc("02_04_02")
				If T6W->T6W_VLREMU <= 0
					aAdd(aLogErro,{"T6W_VLREMU","000520", "C91", nRecno }) //STR0520 - "O valor informado no campo deve ser maior que 0 (zero)."
				EndIf
			Else
				If !Empty(C91->C91_INDMVI)
					If C91->C91_INDMVI <> '3' .AND. T6W->T6W_VLREMU == 0
						aAdd(aLogErro,{"T6W_VLREMU","000803", "C91", nRecno }) //STR0803 - "Permite o valor igual a zero apenas se {indMV} do registro superior for igual a [3]. Nos demais casos o valor deve ser maior que zero."
					EndIf
				EndIf
			EndIf

	   		//T6W_CODCAT

			T6W->(DbSkip())
		EndDo
	EndIf

	//----------------------------------------------------
	//INFORMA��ES DE PROCESSOS JUDICIARIOS DE REMUNERA��O
	//----------------------------------------------------
	DBSelectArea( "CRN" )
	CRN->( DBSetOrder( 1 ) )
	If CRN->( MsSeek( xFilial( "CRN" ) + C91->( C91_ID + C91_VERSAO ) ) )
		While CRN->( !Eof() ) .and. CRN->( CRN_FILIAL + CRN_ID + CRN_VERSAO ) == xFilial( "CRN" ) + C91->( C91_ID + C91_VERSAO )
			If Empty( CRN->CRN_TPTRIB )
				aAdd( aLogErro, { "CRN_TPTRIB", "000010", "C91", nRecno } ) //STR0010 - Campo Inconsistente ou Vazio
			Else
				If !( CRN->CRN_TPTRIB $ "1234" )
					aAdd( aLogErro, { "CRN_TPTRIB", "000006", "C91", nRecno } ) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis."
				EndIf
			EndIf

			If Empty( CRN->CRN_IDPROC )
				aAdd( aLogErro, { "CRN_IDPROC", "000010", "C91", nRecno } ) //STR0010 - Campo Inconsistente ou Vazio
			Else
				If xValRegTab( "C1G", CRN->CRN_IDPROC, 3,, @aLogErro,, { "C91", "CRN_IDPROC", nRecno } )
					cIndMat := Posicione( "LE7", 3, xFilial( "LE7" ) + C1G->C1G_INDMAT, "LE7_CODIGO" )

					If CRN->CRN_TPTRIB == "1" .or. CRN->CRN_TPTRIB == "2"
						If !( cIndMat == "1" )
							aAdd( aLogErro, { "CRN_IDPROC", "001058", "C91", nRecno } ) //STR0071 - "Informar um n�mero de processo judicial cadastrado atrav�s do evento S-1070, cujo indicativo da mat�ria do processo seja igual a 1."
						EndIf
					ElseIf CRN->CRN_TPTRIB == "3"
						If !( cIndMat $ "1|7" )
							aAdd( aLogErro, { "CRN_IDPROC", "001059", "C91", nRecno } ) //STR0072 - "Informar um n�mero de processo judicial cadastrado atrav�s do evento S-1070, cujo indicativo da mat�ria do processo seja igual a 1 ou 7."
						EndIf
					ElseIf CRN->CRN_TPTRIB == "4"
						If !( cIndMat == "8" )
							aAdd( aLogErro, { "CRN_IDPROC", "001060", "C91", nRecno } ) //STR0073 - "Informar um n�mero de processo judicial cadastrado atrav�s do evento S-1070, cujo indicativo da mat�ria do processo seja igual a 8."
						EndIf
					EndIf
				EndIf
			EndIf


			If Empty( CRN->CRN_IDSUSP )
				if TafLayESoc("02_04_02")
					if CRN->CRN_TPTRIB $ "1/2"
						aAdd( aLogErro, { "CRN_CODSUS", "000010", "C91", nRecno } ) //STR0010 - Campo Inconsistente ou Vazio
					endif
				else
					aAdd( aLogErro, { "CRN_CODSUS", "000010", "C91", nRecno } ) //STR0010 - Campo Inconsistente ou Vazio
				EndIf
			Else
				xValRegTab( "T5L", CRN->CRN_IDSUSP, 1,, @aLogErro,, { "C91", "CRN_CODSUS", nRecno } )
			EndIf

			CRN->( DBSkip() )
		EndDo
	EndIf

	/*---------------------------------------------------------------------------------------------------*/
	/*						    VALIDA��ES DAS INFORMA��ES DO PERIODO DE APURA��O                        */
	/*---------------------------------------------------------------------------------------------------*/

	// ----------------------------------------------------
	// 			VALIDA��O VALOR REC. PAGTO - T14
	// ----------------------------------------------------
	DbSelectArea("T14")
	T14->( DbSetOrder( 1 ) )
	If T14->( MsSeek (C91->(C91_FILIAL+C91_ID+C91_VERSAO)))
		While !T14->(Eof()) .And. AllTrim(C91->(C91_ID+C91_VERSAO)) == AllTrim(T14->(T14_ID+T14_VERSAO))

			DbSelectArea("C9M")
			C9M->( DbSetOrder( 1 ) )
			If C9M->( MsSeek ( xFilial("C9M")+T14->(T14_ID+T14_VERSAO+T14_IDEDMD)))
				While !C9M->(Eof()) .And. AllTrim(T14->(T14_ID+T14_VERSAO+T14_IDEDMD)) == AllTrim(C9M->(C9M_ID+C9M_VERSAO+C9M_RECIBO))

					cTpRubrica := POSICIONE("C8R",5, xFilial("C8R")+C9M->C9M_CODRUB+'1',"C8R_INDTRB") //1 - Vencimento /2 - Desconto
					If cTpRubrica == '1'
						nRubVenc  += C9M->C9M_VLRRUB
					ElseIf cTpRubrica == '2'
						nRubDesc  += C9M->C9M_VLRRUB
					EndIf

					C9M->(DbSkip())
				EndDo

				nRubVenc	:= 0
				nRubDesc	:= 0
			EndIf

			T14->(DbSkip())
		EndDo
	Else
		AADD(aLogErro,{"T14_IDEDMD","001147", "C91", nRecno }) //A identifica��o de demonstrativo de valores � uma informa��o obrigat�ria e n�o foi informada"
	EndIf


	If T14->( MsSeek ( xFilial("T14")+C91->(C91_ID+C91_VERSAO)))
		While !T14->(Eof()) .And. AllTrim(C91->(C91_ID+C91_VERSAO)) == AllTrim(T14->(T14_ID+T14_VERSAO))

			C9K->( DbSetOrder( 1 ) )
			If C9K->( MsSeek ( xFilial("C9K")+T14->(T14_ID+T14_VERSAO+T14_IDEDMD)))
				While !C9K->(Eof()) .And. AllTrim(C9K->(C9K_ID+C9K_VERSAO+C9K_RECIBO)) == AllTrim(T14->(T14_ID+T14_VERSAO+T14_IDEDMD))

					cTpLot := GetADVFVal("C99", "C99_TPLOT", xFilial("C99") + Padr(C9K->C9K_LOTACA,TamSX3("C99_ID")[1]) + '1', 4, "")

					//Informa��o obrigat�ria se {codLotacao} possuir {tpLotacao} = [8, 9] (campo anterior). Se informado deve ser um n�mero maior que zero.
					If GetADVFVal("C8F", "C8F_CODIGO", xFilial("C8F") + cTpLot, 1, "") $ "08|09"
						If Empty(C9K->C9K_QTDDIA)
							AADD(aLogErro,{"C9K_QTDDIA","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
						Else
							If !( Val(C9K->C9K_QTDDIA) > 0)
								aAdd(aLogErro,{"C9K_QTDDIA","000969", "C91", nRecno }) //STR0969 - "Informa��o obrigat�ria se {codLotacao} possuir {tpLotacao} = [8, 9]. Se informado deve ser um n�mero maior que zero"
							EndIf
						EndIf
					EndIf

					// ----------------------------------------------------
					// 			VALIDA��O TIPO INSC. - C9K
					// ----------------------------------------------------
					cC9KTpInsc := POSICIONE("C92",5, xFilial("C92")+Padr(C9K->C9K_ESTABE,TamSX3("C92_ID")[1])+'1',"C92_TPINSC")
					If cClassTrib $ '21|22' .AND. !(cC9KTpInsc $ '3|4')
						aAdd(aLogErro,{"C9K_ESTABE","000794", "C91", nRecno }) //STR0794 - "Se {classTrib} igual a [21,22], exceto empregador dom�stico, deve ser igual [3,4]."
					ElseIf !(cClassTrib $ '21|22') .AND. !(cC9KTpInsc $ '1|4')
						aAdd(aLogErro,{"C9K_ESTABE","000795", "C91", nRecno }) //STR0795 - "Se {classTrib} <> [21,22]) deve ser igual a [1,4]."
					EndIf

					C9L->( DbSetOrder( 1 ) )
					If C9L->( MsSeek ( xFilial("C9L")+C9K->(C9K_ID+C9K_VERSAO+C9K_RECIBO+C9K_ESTABE+C9K_LOTACA)))
						While !C9L->(Eof()) .And. AllTrim(C9K->(C9K_ID+C9K_VERSAO+C9K_RECIBO+C9K_ESTABE+C9K_LOTACA)) == AllTrim(C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA))

		                  // VERIFICA DE QUAL TABELA PEGAR O CODIGO DA CATEGORIA
							If C9V->C9V_NOMEVE $ "S2200"
								cC9LCodCat := POSICIONE("CUP",1, xFilial("CUP")+C9V->C9V_ID,"CUP_CODCAT")
							ElseIf C9V->C9V_NOMEVE == "S2300"
								cC9LCodCat := C9V->C9V_CATCI //POSICIONE("CUU",1, xFilial("CUP")+C9L->C9L_TRABAL,"CUU_CATAV")
							EndIf

							cC9LCodCat := POSICIONE("C87",1, xFilial("C87")+cC9LCodCat,"C87_CODIGO")

							If cC9LCodCat == '701'
								lCat701OK := .T.
							EndIf

							// ----------------------------------------------------
							// 			VALIDA��O MATRICULA. - C9L
							// ----------------------------------------------------
							If Empty(C9V->C9V_MATRIC) .AND. cC9LCodCat $ "101|102|103|104|105|106|107|108|301|302|303|304|305|306|309" .AND. C9V->C9V_NOMEVE <> "TAUTO"
								aAdd(aLogErro,{"C9L_TRABAL","000796", "C91", nRecno }) //STR0796 - "Informa��o obrigat�ria para trabalhadores das categorias "empregados" e "agentes p�blicos"."
							EndIf

							// ----------------------------------------------------
							// 	     VALIDA��O INDIC. CONTRIB. SUBSTITUIDA - C9L
							// ----------------------------------------------------
							If Empty(C9L->C9L_INDCON) .AND. cClassTrib == '03'
								aAdd(aLogErro,{"C9L_INDCON","000797", "C91", nRecno }) //STR0797 - "Informa��o obrigat�ria no caso de {classTrib} = [3]."
							EndIf

							nProvento := 0
							nDesconto := 0
							C9M->( DbSetOrder( 1 ) )
							If C9M->( MsSeek ( xFilial("C9M")+C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)))
								While !C9M->(Eof()) .And. AllTrim(C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)) == AllTrim(C9M->(C9M_ID+C9M_VERSAO+C9M_RECIBO+C9M_ESTABE+C9M_LOTACA+C9M_TRABAL))

									//Busca informa��es que seram utilizadas na valida��o
									If C8R->( MsSeek ( xFilial("C8R")+C9M->C9M_CODRUB + '1'))
										cC9MDtIni  := CTOD("01/"+ substr(C8R->C8R_DTINI,1,2)+ "/" + substr(C8R->C8R_DTINI,3))
										cC9MDtFim  := CTOD("01/"+ substr(C8R->C8R_DTFIN,1,2)+ "/" + substr(C8R->C8R_DTFIN,3))
										cC9MIdNat  := C8R->C8R_NATRUB
										cC9MIdCP   := C8R->C8R_CINTPS
										cC9MIdIR   := C8R->C8R_CINTIR
										cC9MIndTrb := C8R->C8R_INDTRB
										cC9MCodNat := POSICIONE("C89",1, xFilial("C89")+cC9MIdNat,"C89_CODIGO")
										cC9MCodCP  := POSICIONE("C8T",1, xFilial("C8T")+cC9MIdCP,"C8T_CODIGO")
										cC9MCodIR  := POSICIONE("C8U",1, xFilial("C8U")+cC9MIdIR,"C8U_CODIGO")
									EndIf

									If Alltrim(cC9MCodNat) == '9219'
										lRubNat9219 := .T.
									EndIf

									If cC9MCodIR $ "31|32|34|35"
										If cC9MIndTrb $ "2|4"
											nDesconto += C9M->C9M_VLRRUB
										ElseIf cC9MIndTrb $ "1|3"
											nProvento += C9M->C9M_VLRRUB
										EndIf
									EndIf

									 /*If cC9MCodIR $ '51|52|53|54|55'
									 		lCUSOK := .T.
									 EndIf*/

									 // ----------------------------------------------------
									 // 	     VALIDA��O COD. RUBRICA - C9M
									 // ----------------------------------------------------
									If cC9MCodCP $ '23|24|61' .AND. !cC9LCodCat $ '301|303|305|410'
										aAdd(aLogErro,{"C9M_CODRUB","000798", "C91", nRecno }) //STR0798 - "O {codIncCP} da tabela de rubricas s� pode indicar [23, 24, 61] se o trabalhador tiver como {codCateg} = [301, 303, 305, 410]."
									EndIf

									 /* Regra Retirada no Layout 2.4
									 c) cuja natureza de rubrica seja 1409 (sal�rio fam�lia), 1009 (sal�rio fam�lia complemento),
									 4050 (sal�rio maternidade) e 4051 (sal�rio maternidade-13� sal�rio), para os trabalhadores classificados nas
									 categorias de contribuinte individual e bolsistas da tabela 01.
									 */
									 /*
									 If cC9MCodNat $ "1409|1009|4050|4051|" .and. cC9LCodCat $ "771|903|"
									 	aAdd(aLogErro,{"C9M_CODRUB","000995","C91", nRecno }) //STR0995 - "N�o pode ser utilizada rubrica cuja natureza de rubrica seja 1409 (sal�rio fam�lia), 1009 (sal�rio fam�lia complemento), 4050 (sal�rio maternidade) e 4051 (sal�rio maternidade-13� sal�rio), para os trabalhadores classificados nas categorias de contribuinte individual e bolsistas da tabela 01."
									 EndIf
									 */

									 /*
									 c) N�o poder ser utilizada rubrica: cujo {codIncCp} em S-1010 seja igual a [25, 26, 51] se {codCateg} pertencer a um dos grupos 'contribuinte individual' ou 'bolsistas' da Tabela 01 (Categoria de Trabalhadores).
									 */
									If cC9MCodCP $ "25|26|51" .and. cC9LCodCat $ "701|711|712|721|722|723|731|734|738|741|751|761|771|781||901|902|903|904|905"
										aAdd(aLogErro,{"C9M_CODRUB","001061","C91", nRecno }) //STR1061 - "N�o pode ser utilizada rubrica, cujo {codIncCP} em S-1010 seja igual a [25|26|51] se {codCateg} pertencer a um dos grupos 'contribuinte individual' ou 'bolsistas' da Tabela 01 (Categoria de Trabalhadores)."
									EndIf

									 // ----------------------------------------------------
									 // 	     VALIDA��O ID TAB. RUBRICA - C9M
									 // ----------------------------------------------------
									cC9MIdTabRub := POSICIONE("C8R",5, xFilial("C8R")+C9M->C9M_CODRUB + '1',"C8R_IDTBRU")
									If Empty(cC9MIdTabRub)
										aAdd(aLogErro,{"C9M_CODRUB","000999","C91", nRecno }) //STR0999 - "O campo {ideTabRubr} deve ser preenchido para a rubrica informada."
									EndIf

									if !Empty(cC9MDtFim)
										If dDataApur > cC9MDtFim
											aAdd(aLogErro,{"C9R_CODRUB","000996", "C91", nRecno }) //STR0996 - "A rubrica deve possuir um per�odo vigente indicado em {perApur}."
										EndIf
									ElseIf !Empty(cC9MDtIni)
										If dDataApur < cC9MDtIni
											aAdd(aLogErro,{"C9R_CODRUB","000996", "C91", nRecno }) //STR0996 - "A rubrica deve possuir um per�odo vigente indicado em {perApur}."
										EndIf
									EndIf

									 // ----------------------------------------------------
									 // 	     VALIDA��O VALOR TOTAL RUBRICA - C9M
									 // ----------------------------------------------------
									If C9M->C9M_VLRRUB < 0
										aAdd(aLogErro,{"C9M_VLRRUB","000800","C91", nRecno }) //STR0800 - "O valor deve ser maior que zero."
									EndIf

									C9M->(DbSkip())
								EndDo

								//Para as rubricas cujo {codIncCP} = [31, 32, 34, 35], o somat�rio do valor das rubricas cujo ({tpRubr} = [2,4]) dever� ser maior ou igual ao somat�rio do valor das rubricas cujo ({tpRubr} = [1,3])
								If !(nDesconto >= nProvento)
									aAdd(aLogErro,{"C9M_VLRRUB","000972","C91", nRecno }) //O somat�rio dos Vencimentos deve ser maior ou igual ao somat�rio dos Descontos
								EndIf
							Else
	   							AADD(aLogErro,{"C9M_CODRUB","000010","C9M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
		   						//aki aki
							EndIf

							T6Y->( DbSetOrder( 1 ) )
							If T6Y->( MsSeek ( xFilial("T6Y")+C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)))

								//S� pode validar os campos do T6Y e T6Z se existir uma rubrica com natureza 9219
								If lRubNat9219
									While !T6Y->(Eof()) .And. AllTrim(C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)) == AllTrim(T6Y->(T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_ESTABE+T6Y_LOTACA+T6Y_TRABAL))
										T6Z->( DbSetOrder( 1 ) )
										If T6Z->( MsSeek ( xFilial("T6Z")+T6Y->(T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_ESTABE+T6Y_LOTACA+T6Y_TRABAL+T6Y_CNPJOP+T6Y_REGANS)))
											While !T6Z->(Eof()) .And. AllTrim(T6Y->(T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_ESTABE+T6Y_LOTACA+T6Y_TRABAL+T6Y_CNPJOP+T6Y_REGANS)) == AllTrim(T6Z->(T6Z_ID+T6Z_VERSAO+T6Z_RECIBO+T6Z_ESTABE+T6Z_LOTACA+T6Z_TRABAL+T6Z_CNPJOP+T6Z_REGANS))

												// ----------------------------------------------------
												// 	     VALIDA��O CPF DEPENDENTE - T6Z
												// ----------------------------------------------------

												//tpDep
												If Empty(T6Z->T6Z_TPDEP)
													aAdd(aLogErro,{"T6Z_TPDEP","000010","C91",nRecno}) //"Campo Inconsistente ou Vazio"
												EndIf

												If xFunAge(T6Z->T6Z_DTNDEP, dDataBase) >= 18 .And. Empty(T6Z->T6Z_CPFDEP) .And. !TafLayESoc("02_05_00")
													aAdd(aLogErro,{"T6Z_CPFDEP","000801","C91", nRecno }) //STR0801 - "O preenchimento � obrigat�rio se for maior de 12 anos."
												EndIf

				                                If !CGC(T6Z->T6Z_CPFDEP,,.F.)
													aAdd(aLogErro,{"T6Z_CPFDEP","000646","C91",nRecno}) //"O CPF � inv�lido." 
												EndIf

												If xVldCPFDep("T6Z", T6Z->T6Z_CPFDEP, "1200", T6Z->T6Z_CNPJOP, C91->C91_PERAPU )
													Aadd( aLogErro, { "T6Z_CPFDEP", "000968", "C91", nRecno } ) // STR0968 - N�o pode haver mais de um dependente com o mesmo n�mero do CPF.
												EndIf

												T6Z->(DbSkip())
											EndDo
										EndIf

										T6Y->(DbSkip())
									EndDo
								Else

									// ---------------------------------------
									// 	       VALIDA��O CNPJ - T6Y
									// ---------------------------------------
									If !Empty(T6Y->T6Y_CNPJOP)
										aAdd(aLogErro,{"T6Y_CNPJOP","000809","C91", nRecno }) //STR0805 - "S� preencher este campo se houver {codRubr} em {itensRemun}, cuja natureza de rubrica {natRubr} indicada = gual a [9219]."
									EndIf

									// ---------------------------------------
									// 	       VALIDA��O REGANS - T6Y
									// ---------------------------------------
									If !Empty(T6Y->T6Y_REGANS)
										aAdd(aLogErro,{"T6Y_REGANS","000809","C91", nRecno }) //STR0805 - "S� preencher este campo se houver {codRubr} em {itensRemun}, cuja natureza de rubrica {natRubr} indicada = gual a [9219]."
									EndIf

									// ---------------------------------------
									// 	   VALIDA��O VALOR TITULO - T6Y
									// ---------------------------------------
									If !Empty(T6Y->T6Y_VLPGTI)
										aAdd(aLogErro,{"T6Y_VLPGTI","000809","C91", nRecno }) //STR0805 - "S� preencher este campo se houver {codRubr} em {itensRemun}, cuja natureza de rubrica {natRubr} indicada = gual a [9219]."
									EndIf
								EndIf
							EndIf

							if TafLayESoc("02_04_02")
								V1B->( DbSetOrder( 1 ) )
								If V1B->( MsSeek ( xFilial("V1B")+C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)))
									While !V1B->(Eof()) .And. AllTrim(C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)) == AllTrim(V1B->(V1B_ID+V1B_VERSAO+V1B_RECIBO+V1B_ESTABE+V1B_LOTACA+V1B_TRABAL))
										xValRegTab( "T87", V1B->V1B_IDCONV, 1, , @aLogErro, , { "C91", "V1B_CODCON", nRecno } )
										V1B->(DbSkip())
									EndDo
								EndIf
							endif

							If !Empty(C9L->C9L_GRAEXP)
								xValRegTab( "C88", C9L->C9L_GRAEXP, 1, , @aLogErro,, { "C91", "C9L_GRAEXP", nRecno } )

								If !(cC9LCodCat $ "101|102|103|104|105|106|111|201|202|301|302|303|305|306|307|308|309|738|731|734")
									aAdd(aLogErro,{"C9L_GRAEXP","001067","C91", nRecno }) //"Campo preenchido exclusivamente para os trabalhadores enquadrados nas categorias relativas a Empregado, Servidor P�blico, Avulso ou categorias com os codigos 731,734 ou 738."
								EndIf
							EndIf

							C9L->(DbSkip())
						EndDo
					EndIf
					C9K->(DbSkip())
				EndDo
			EndIf

			/*---------------------------------------------------------------------------------------------------*/
			/*					VALIDA��ES DAS INFORMA��ES DO PERIODO DE APURA��O ANTERIOR  		             */
			/*---------------------------------------------------------------------------------------------------*/
			nRubVenc	:= 0
			nRubDesc	:= 0
			lRubNat9219	:= .F.
			C9N->( DbSetOrder( 1 ) )
			If C9N->( MsSeek ( xFilial("C9N")+T14->(T14_ID+T14_VERSAO+T14_IDEDMD)))
				While !C9N->(Eof()) .And. AllTrim(T14->(T14_ID+T14_VERSAO+T14_IDEDMD)) == AllTrim(C9N->(C9N_ID+C9N_VERSAO+C9N_RECIBO))
					// ----------------------------------------------------
					// 			VALIDA��O C9N
					// ----------------------------------------------------
					If Len(Alltrim(C91->C91_PERAPU)) == 6 //CONSIDERAR MES/ANO
						cDtAcordo     := '01/' + Substr(DTOS(C9N->C9N_DTACOR),5,2) + '/' + Substr(DTOS(C9N->C9N_DTACOR),1,4)
						cDtApur	      := '01/' + Substr(C91->C91_PERAPU,5,2) + "/" + Substr(C91->C91_PERAPU,1,4)
					ElseIf Len(Alltrim(C91->C91_PERAPU)) == 4 //CONSIDERAR ANO
						cDtAcordo     := '01/01/' + Substr(DTOS(C9N->C9N_DTACOR),1,4)
						cDtApur	      := '01/01/' + Alltrim(C91->C91_PERAPU)
					EndIf

					//dtAcConv
					If !Empty(C9N->C9N_DTACOR)
						If CTOD(cDtAcordo) > CTOD(cDtApur)
							aAdd(aLogErro,{"C9N_DTACOR","000810", "C91", nRecno }) //STR0875 -  "Deve ser uma data compreendida no per�odo de apura��o informada em {perApur}."
						EndIf
					EndIf

					
					If !(FindFunction("TafNT1519") .And. TafNT1519())
					
						//compAcConv
						If !Empty(C9N->C9N_COMPAC)
							If CTOD('01/' + Substr(C9N->C9N_COMPAC,5,2) + '/' + Substr(C9N->C9N_COMPAC,1,4) ) < CTOD(cDtApur)
								aAdd(aLogErro,{"C9N_COMPAC","001000", "C91", nRecno }) //STR1000 -  "Deve ser maior ou igual ao per�odo de apura��o informado em {perApur}."
							EndIf
						ElseIf C9N->C9N_TPACOR $ "A|B|C|D"
							AADD(aLogErro,{"C9N_COMPAC","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
						EndIf
					
					Else // Op��o ap�s a NT1519
						
						//compAcConv
						If !Empty(C9N->C9N_COMPAC)
							If CTOD('01/' + Substr(C9N->C9N_COMPAC,5,2) + '/' + Substr(C9N->C9N_COMPAC,1,4) ) < CTOD(cDtApur)
								aAdd(aLogErro,{"C9N_COMPAC","001000", "C91", nRecno }) //STR1000 -  "Deve ser maior ou igual ao per�odo de apura��o informado em {perApur}."
							EndIf
						EndIf
					
					EndIf

					If !(FindFunction("TafNT1519") .And. TafNT1519())
						//dtEfAcConv
						If Empty(C9N->C9N_DTEFAC) .And. C9N->C9N_TPACOR $ "A|B|C|D|E"
							AADD(aLogErro,{"C9N_DTEFAC","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
						EndIf
					EndIf

					//remunSuc
					If Empty(C9N->C9N_REMUNS)
						AADD(aLogErro,{"C9N_REMUNS","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
					EndIf

					C9O->( DbSetOrder( 1 ) )
					If C9O->( MsSeek ( xFilial("C9O")+C9N->(C9N_ID+C9N_VERSAO+C9N_RECIBO+DTOS(C9N_DTACOR)+C9N_TPACOR+C9N_COMPAC)))
						While !C9O->(Eof()) .And.  AllTrim(C9N->(C9N_ID+C9N_VERSAO+C9N_RECIBO+DTOS(C9N_DTACOR)+C9N_TPACOR+C9N_COMPAC)) == AllTrim(C9O->(C9O_ID+C9O_VERSAO+C9O_RECIBO+DTOS(C9O_DTACOR)+C9O_TPACOR+C9O_COMPAC))

							// ----------------------------------------------------
							// 		    VALIDA��O PERIODO COMPL. EMP. - C9O
							// ----------------------------------------------------
							If !Empty(C9O->C9O_PERREF)

								If Len(Alltrim(C9O->C9O_PERREF)) == 6 //CONSIDERAR MES/ANO
									cDtAcordo     := '01/' + Substr(C9O->C9O_PERREF,5,2) + '/' + Substr(C9O->C9O_PERREF,1,4)
									cDtApur	      := '01/' + Substr(C91->C91_PERAPU,5,2) + "/" + Substr(C91->C91_PERAPU,1,4)
								Else
									aAdd(aLogErro,{"C9O_PERREF","000805", "C91", nRecno }) //STR0805 - "Deve ser um m�s/ano v�lido e estar no formato AAAA-MM (Ano-M�s)."
								EndIf

								If CTOD(cDtAcordo) > CTOD(cDtApur)
									aAdd(aLogErro,{"C9O_PERREF","001062", "C91", nRecno }) //STR1062 - "Deve ser uma data igual ou anterior ao per�odo de apura��o, informado em {perApur}. Deve ser informado no formato AAAA-MM."
								EndIf
							EndIf

							C9P->( DbSetOrder( 1 ) )
							If C9P->( MsSeek ( xFilial("C9P")+C9O->(C9O_ID+C9O_VERSAO+C9O_RECIBO+DTOS(C9O_DTACOR)+C9O_TPACOR+C9O_COMPAC+C9O_PERREF)))
								While !C9P->(Eof()) .And. AllTrim(C9O->(C9O_ID+C9O_VERSAO+C9O_RECIBO+DTOS(C9O_DTACOR)+C9O_TPACOR+C9O_COMPAC+C9O_PERREF)) == AllTrim(C9P->(C9P_ID+C9P_VERSAO+C9P_RECIBO+DTOS(C9P_DTACOR)+C9P_TPACOR+C9P_COMPAC+C9P_PERREF))

									// ----------------------------------------------------
									// 			VALIDA��O TIPO INSC. - C9P
									// ----------------------------------------------------
									cC9PTpInsc := POSICIONE("C92",1, xFilial("C92")+C9P->C9P_ESTABE,"C92_TPINSC")
									If cClassTrib $ '21|22' .AND. !cC9PTpInsc $ '3|4'
										aAdd(aLogErro,{"C9P_ESTABE","000794", "C91", nRecno }) //STR0794 - "Se {classTrib} igual a [21,22], exceto empregador dom�stico, deve ser igual [3,4]."
									ElseIf !cClassTrib $ '21|22' .AND. !cC9PTpInsc $ '1|4'
										aAdd(aLogErro,{"C9P_ESTABE","000795", "C91", nRecno }) //STR0795 - "Se {classTrib} <> [21,22]) deve ser igual a [1,4]."
									EndIf

									C9Q->( DbSetOrder( 1 ) )
									If C9Q->( MsSeek ( xFilial("C9Q")+C9P->(C9P_ID+C9P_VERSAO+C9P_RECIBO+DTOS(C9P_DTACOR)+C9P_TPACOR+C9P_COMPAC+C9P_PERREF+C9P_ESTABE+C9P_LOTACA)))
										While !C9Q->(Eof()) .And. AllTrim(C9P->(C9P_ID+C9P_VERSAO+C9P_RECIBO+DTOS(C9P_DTACOR)+C9P_TPACOR+Iif(lC9QCOMPAC, C9P_COMPAC, Space(TamSX3("C9P_COMPAC")[1]))+C9P_PERREF+C9P_ESTABE+C9P_LOTACA)) == AllTrim(C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+Iif(lC9QCOMPAC, C9Q_COMPAC, Space(TamSX3("C9P_COMPAC")[1]))+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA))

						                 // VERIFICA DE QUAL TABELA PEGAR O CODIGO DA CATEGORIA
											If C9V->C9V_NOMEVE $ "S2100|S2200"
												cC9QCodCat := POSICIONE("CUP",1, xFilial("CUP")+C9Q->C9Q_TRABAL,"CUP_CODCAT")
											ElseIf C9V->C9V_NOMEVE == "S2300"
												//cC9QCodCat := POSICIONE("CUU",1, xFilial("CUP")+C9Q->C9Q_TRABAL,"CUU_CATAV")
												cC9QCodCat := C9V->C9V_CATCI
											EndIf

											cC9QCodCat := POSICIONE("C87",1, xFilial("C87")+cC9QCodCat,"C87_CODIGO")

											If cC9QCodCat == '701'
												lCat701OK := .T.
											EndIf

											// ----------------------------------------------------
											// 	     VALIDA��O INDIC. CONTRIB. SUBSTITUIDA - C9Q
											// ----------------------------------------------------
											If Empty(C9Q->C9Q_INDCON) .AND. cClassTrib == '03'
												aAdd(aLogErro,{"C9Q_INDCON","000797", "C91", nRecno }) //STR0797 - "Informa��o obrigat�ria no caso de {classTrib} = [3]."
											EndIf

											C9R->( DbSetOrder( 1 ) )
											If C9R->( MsSeek ( xFilial("C9R")+C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+Iif(TAFColumnPos("C9Q_COMPAC"), C9Q_COMPAC, C9P_COMPAC)+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_TRABAL)))
												While !C9R->(Eof()) .And. AllTrim(C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+Iif(TAFColumnPos("C9Q_COMPAC"), C9Q_COMPAC, C9P_COMPAC)+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_TRABAL)) == AllTrim(C9R->(C9R_ID+C9R_VERSAO+C9R_RECIBO+DTOS(C9R_DTACOR)+C9R_TPACOR+C9R_COMPAC+C9R_PERREF+C9R_ESTABE+C9R_LOTACA+C9R_TRABAL))

													//Busca informa��es que seram utilizadas na valida��o
													If C8R->( MsSeek ( xFilial("C8R")+C9R->C9R_CODRUB + '1'))
														cC9RIdNat  := C8R->C8R_NATRUB
														cC9RIdCP   := C8R->C8R_CINTPS
														cC9RIdIR   := C8R->C8R_CINTIR
														cC9RDtIni  := CTOD("01/"+ substr(C8R->C8R_DTINI,1,2)+ "/" + substr(C8R->C8R_DTINI,3))
														cC9RDtFim  := CTOD("01/"+ substr(C8R->C8R_DTFIN,1,2)+ "/" + substr(C8R->C8R_DTFIN,3))
														cC9RCodNat := POSICIONE("C89",1, xFilial("C89")+cC9RIdNat,"C89_CODIGO")
														cC9RCodCP  := POSICIONE("C8T",1, xFilial("C8T")+cC9RIdCP,"C8T_CODIGO")
														cC9RCodIR  := POSICIONE("C8U",1, xFilial("C8U")+cC9RIdIR,"C8U_CODIGO")
													EndIf

													If Alltrim(cC9RCodNat) == '9219'
														lRubNat9219 := .T.
													EndIf

													// ----------------------------------------------------
													// 	     VALIDA��O COD. RUBRICA - C9R
													// ----------------------------------------------------
													If cC9RCodCP $ '23|24|61' .AND. !cC9LCodCat $ '301|303|305|410'
														aAdd(aLogErro,{"C9R_CODRUB","000798", "C91", nRecno }) //STR0798 - "O {codIncCP} da tabela de rubricas s� pode indicar [23, 24, 61] se o trabalhador tiver como {codCateg} = [301, 303, 305, 410]."
													EndIf

													/* Regra Retirada no Layout 2.4
													c) cuja natureza de rubrica seja 1409 (sal�rio fam�lia), 1009 (sal�rio fam�lia complemento),
											  	    4050 (sal�rio maternidade) e 4051 (sal�rio maternidade-13� sal�rio), para os trabalhadores classificados nas
											  		categorias de contribuinte individual e bolsistas da tabela 01.*/
											  		/*
													 If cC9RCodNat $ "1409|1009|4050|4051|" .and. cC9QCodCat $ "771|903|"
													 	aAdd(aLogErro,{"C9R_CODRUB","000995","C91", nRecno }) //STR0995 - "N�o pode ser utilizada rubrica sua natureza seja 1409 (sal�rio fam�lia), 1009 (sal�rio fam�lia complemento), 4050 (sal�rio maternidade) e 4051 (sal�rio maternidade-13� sal�rio), para os trabalhadores classificados nas categorias de contribuinte individual e bolsistas da tabela 01."
													 EndIf
													*/

													/*
													c) N�o poder ser utilizada rubrica: cujo {codIncCp} em S-1010 seja igual a [25, 26, 51] se {codCateg} pertencer a um dos grupos 'contribuinte individual' ou 'bolsistas' da Tabela 01 (Categoria de Trabalhadores).
													*/
													If cC9RCodCP $ "25|26|51" .and. cC9LCodCat $ "701|711|712|721|722|723|731|734|738|741|751|761|771|781||901|902|903|904|905"
														aAdd(aLogErro,{"C9R_CODRUB","001061","C91", nRecno }) //STR1061 - "N�o pode ser utilizada rubrica, cujo {codIncCP} em S-1010 seja igual a [25|26|51] se {codCateg} pertencer a um dos grupos 'contribuinte individual' ou 'bolsistas' da Tabela 01 (Categoria de Trabalhadores)."
													EndIf
													// ----------------------------------------------------
													// 	     VALIDA��O ID TAB. RUBRICA - C9R
													// ----------------------------------------------------
													cC9RIdTabRub := POSICIONE("C8R",5, xFilial("C8R")+C9R->C9R_CODRUB + '1',"C8R_IDTBRU")
													If Empty(cC9RIdTabRub)
														aAdd(aLogErro,{"C9R_CODRUB","000999","C91", nRecno }) //STR0999 - "O campo {ideTabRubr} deve ser preenchido para a rubrica informada."
													EndIf

													if !Empty(cC9RDtFim)
														If CTOD(cDtAcordo) > cC9RDtFim
															aAdd(aLogErro,{"C9R_CODRUB","000997", "C91", nRecno }) //STR0997 - "A rubrica deve possuir um per�odo vigente indicado em {perRef}."
														EndIf
													ElseIf !Empty(cC9RDtIni)
														If CTOD(cDtAcordo) < cC9RDtIni
															aAdd(aLogErro,{"C9R_CODRUB","000997", "C91", nRecno }) //STR0997 - "A rubrica deve possuir um per�odo vigente indicado em {perRef}."
														EndIf
													EndIf

													// ----------------------------------------------------
													// 	     VALIDA��O VALOR TOTAL RUBRICA - C9R
													// ----------------------------------------------------
													If C9R->C9R_VLRRUB < 0
														aAdd(aLogErro,{"C9R_VLRRUB","000800","C91", nRecno }) //STR0800 - "O valor deve ser maior que zero."
													EndIf

													C9R->(DbSkip())
												EndDo
											Else
	   											AADD(aLogErro,{"C9R_CODRUB","000010","C9R", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
												//aki		
											EndIf

											if TafLayESoc("02_04_02")
												V1C->( DbSetOrder( 1 ) )
												If V1C->( MsSeek ( xFilial("V1C")+C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_TRABAL)))
													While !V1C->(Eof()) .And. AllTrim(C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_TRABAL)) == AllTrim(V1C->(V1C_ID+V1C_VERSAO+V1C_RECIBO+DTOS(V1C_DTACOR)+V1C_TPACOR+V1C_PERREF+V1C_ESTABE+V1C_LOTACA+V1C_TRABAL))
														xValRegTab( "T87", V1C->V1C_IDCONV, 1, , @aLogErro, , { "C91", "V1C_CODCON", nRecno } )
														V1C->(DbSkip())
													EndDo
												EndIf
											endif

											C9Q->(DbSkip())
										EndDo
									EndIf
									C9P->(DbSkip())
								EndDo
							EndIf
							C9O->(DbSkip())
						EndDo
					EndIf
					C9N->(Dbskip())
				EndDo
			EndIf

			If !TafLayESoc("02_04_02")
				dbSelectArea("T89")
				T89->(dbSetOrder(1))
				If T89->(MsSeek(xFilial("T89")+T14->T14_ID+T14->T14_VERSAO+T14->T14_IDEDMD))
					While !T89->(Eof()) .And. AllTrim(T14->(T14_ID+T14_VERSAO+T14_IDEDMD)) == AllTrim(T89->(T89_ID+T89_VERSAO+T89_IDEDMD))
						xValRegTab( "T87", T89->T89_IDCONV, 1, , @aLogErro, , { "C9V", "T89_CODCON", nRecno } )
						T89->(dbSkip())
					EndDo
				EndIf
			EndIf

			T14->(DbSkip())
		EndDo
	EndIf

	If TafLayESoc("02_04_02")

        /*
        DSERTAF1-4588 - 16-05-2018
        Altera��o da valida��o da C91 p/ a T14 em virtude
        da altera��o no layout do E-Social na vers�o 2.4.02.
        */


		// ----------------------------------------------------
		// 			VALIDA��O NATUREZA DA ATIVIDADE - C91
		// ----------------------------------------------------
		If FieldPos("T14_NATATV") > 0 .And. Empty(T14->T14_NATATV) .And. (cClassTrib $ '06|07|08|')
			AADD(aLogErro,{"T14_NATATV","000010","T14", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
		EndIf

		// ----------------------------------------------------
		// 			VALIDA��O QTD DIAS INTERMITENTE - C91
		// ----------------------------------------------------
		If !Empty(C91->C91_QTDINT)
			If Val(C91->C91_QTDINT) < 1 .OR. Val(C91->C91_QTDINT) > 31
				aAdd(aLogErro,{"C91_QTDINT","000792", "C91", nRecno }) //STR0792 - "Deve preencher o campo com um n�mero entre 1 e 31."
			EndIf
		EndIf

		// ----------------------------------------------------
		// 			VALIDA��O QTD DIAS TRABALHADOS - T14
		// ----------------------------------------------------
		If FieldPos("T14_QTDTRB") > 0 .And. !Empty(T14->T14_QTDTRB)
			If cClassTrib == '22' .AND. lCat701OK
				If Val(T14->T14_QTDTRB) < 1 .OR. Val(T14->T14_QTDTRB) > 31
					aAdd(aLogErro,{"T14_QTDTRB","000792", "T14", nRecno }) //STR0792 - "Deve preencher o campo com um n�mero entre 1 e 31."
				EndIf
			Else
				aAdd(aLogErro,{"T14_QTDTRB","000804", "T14", nRecno }) //STR0804 - "O campo s� pode ser preenchido se {classTrib} = [22] e exista remunera��o exclusivamente na categoria [701]."
			EndIf
		EndIf
	Else

		// ----------------------------------------------------
		// 			VALIDA��O NATUREZA DA ATIVIDADE - C91
		// ----------------------------------------------------
		If Empty(C91->C91_NATATV) .And. (cClassTrib $ '06|07|08|')
			AADD(aLogErro,{"C91_NATATV","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
		EndIf

		If !Empty(C91->C91_QTDTRB)
			If cClassTrib == '22' .AND. lCat701OK
				If Val(C91->C91_QTDTRB) < 1 .OR. Val(C91->C91_QTDTRB) > 31
					aAdd(aLogErro,{"C91_QTDTRB","000792", "C91", nRecno }) //STR0792 - "Deve preencher o campo com um n�mero entre 1 e 31."
				EndIf
			Else
				aAdd(aLogErro,{"C91_QTDTRB","000804", "C91", nRecno }) //STR0804 - "O campo s� pode ser preenchido se {classTrib} = [22] e exista remunera��o exclusivamente na categoria [701]."
			EndIf
		EndIf
	EndIf

	/*�����������������������������Ŀ
	 �ATUALIZO O STATUS DO REGISTRO�
	 �1 = Registro Invalido        �
	 �0 = Registro Valido          �
	 �������������������������������*/
	cStatus := Iif(Len(aLogErro) > 0,"1","0")

	Begin Transaction
		If RecLock("C91",.F.)
			C91->C91_STATUS := cStatus
			C91->(MsUnlock())
		EndIf
	End Transaction
	
ElseIf !lMv
	AADD(aLogErro,{"C91","000305","C91",nRecno}) //Registros que j� foram transmitidos ao Fisco, n�o podem ser validados
EndIf

//�������������������������������������������������������Ŀ
//�N�o apresento o alert quando utilizo o JOB para validar�
//���������������������������������������������������������
If !lJob
	xValLogEr(aLogErro)
EndIf

Return(aLogErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} xFunRecibo
Esta fun��o tem por finalidade, retornar o numero do recibo de desligamento
do trabalhador empregado ou do evento de t�rmino de contrato no caso do
Diretor N�o Empregado com FGTS, para o autopreenchimento da tag nrReciboDeslig,
no caso da gera��o do XML, ou o campo C9L_NRECDE no caso de Integra��o.

@Param  aRecibo -> Array contendo as seguintes informacoes:

aRecibo[1][1] - Filial da tabela C9V
aRecibo[1][2] - Id da tabela C9V
aRecibo[1][3] - Versao da tabela C9V
aRecibo[1][4] - Matricula do funcionario

@Return .T.

@author Vitor Siqueira
@since 08/01/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
Function xFunRecibo(aRecibo)

Local cMatricula := ""

CUP->(DBSetOrder(1))
If CUP->(MsSeek(aRecibo[1][1]+aRecibo[1][2]+aRecibo[1][3]))

	If AllTrim(aRecibo[1][4]) == AllTrim(CUP->CUP_MATRIC)

		C87->(DBSetOrder(1))
		If C87->(MsSeek(xFilial("C87")+CUP->CUP_CODCAT))

			If C87->C87_CODIGO == "721"
				CRD->( DbSetOrder( 2 ) )
				If CRD->( MsSeek ( xFilial("CRD")+aRecibo[1][2]))
	        		If CRD->CRD_STATUS $ ("2|3|4") .And. CRD->CRD_ATIVO == "1"
						cMatricula := CRD->CRD_PROTUL
				    EndIf
				EndIf
			Else
				CMD->( DbSetOrder( 2 ) )
				If CMD->( MsSeek ( xFilial("CMD")+aRecibo[1][2]))
					If CMD->CMD_STATUS $ ("2|3|4") .And. CMD->CMD_ATIVO == "1"
						cMatricula := CMD->CMD_PROTUL
				    EndIf
				EndIf
			EndIf

		EndIf

    EndIf
EndIf

Return(cMatricula)

//-------------------------------------------------------------------
/*/{Protheus.doc} GerarEvtExc
Funcao que gera a exclus�o do evento

@Param  oModel  -> Modelo de dados
@Param  nRecno  -> Numero do recno
@Param  lRotExc -> Variavel que controla se a function � chamada pelo TafIntegraESocial

@Return .T.

@Author Vitor Henrique Ferreira
@Since 11/01/2015
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function GerarEvtExc( oModel, nRecno, lRotExc )

Local cVerAnt    	:= ""
Local cProtocolo 	:= ""
Local cVersao    	:= ""
Local cChvRegAnt 	:= ""
Local cEvento		:= ""

Local nOperation	:= oModel:GetOperation()
Local nlI  		:= 0
Local nlY  		:= 0

Local nC9O, nC9OAdd := 0
Local nC9Q, nC9QAdd := 0
Local nC9R, nC9RAdd := 0
Local nT14, nT14Add := 0
Local nC9L, nC9LAdd := 0
Local nC9M, nC9MAdd := 0
Local nT6Y, nT6YAdd := 0
Local nT6Z, nT6ZAdd := 0
Local nC9P, nC9PAdd := 0
Local nT6W, nT6WAdd := 0
Local nC9N, nC9NAdd := 0
Local nC9K, nC9KAdd := 0
Local nV1B, nV1BAdd := 0
Local nV1C, nV1CAdd := 0

Local aGrava     	:= {}
Local aGravaT14  	:= {}
Local aGravaC9L 	:= {}
Local aGravaC9M 	:= {}
Local aGravaT6Y  	:= {}
Local aGravaT6Z  	:= {}
Local aGravaC9O  	:= {}
Local aGravaC9Q  	:= {}
Local aGravaC9R  	:= {}
Local aGravaCRN  	:= {}
Local aGravaT6W  	:= {}
Local aGravaC9K  	:= {}
Local aGravaC9N  	:= {}
Local aGravaC9P  	:= {}
Local aGravaV1B 	:= {}
Local aGravaV1C 	:= {}

Local oModelC91  	:= Nil
Local oModelT14  	:= Nil
Local oModelC9L  	:= Nil
Local oModelC9M  	:= Nil
Local oModelT6Y  	:= Nil
Local oModelT6Z  	:= Nil
Local oModelC9O  	:= Nil
Local oModelC9Q  	:= Nil
Local oModelC9R  	:= Nil
Local oModelCRN  	:= Nil
Local oModelT6W  	:= Nil
Local oModelC9K  	:= Nil
Local oModelC9N  	:= Nil
Local oModelC9P  	:= Nil
Local oModelV1B  	:= Nil
Local oModelV1C  	:= Nil

Local lRetorno	:= .T.

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

Begin Transaction

	//Posiciona o item
	("C91")->( DBGoTo( nRecno ) )

	oModelC91 := oModel:GetModel( 'MODEL_C91' )
	oModelT6W := oModel:GetModel( 'MODEL_T6W' )
	oModelT14 := oModel:GetModel( 'MODEL_T14' )
	oModelC9K := oModel:GetModel( 'MODEL_C9K' )
	oModelC9L := oModel:GetModel( 'MODEL_C9L' )
	oModelC9M := oModel:GetModel( 'MODEL_C9M' )
	oModelT6Y := oModel:GetModel( 'MODEL_T6Y' )
	oModelT6Z := oModel:GetModel( 'MODEL_T6Z' )
	oModelC9N := oModel:GetModel( 'MODEL_C9N' )
	oModelC9O := oModel:GetModel( 'MODEL_C9O' )
	oModelC9P := oModel:GetModel( 'MODEL_C9P' )
	oModelC9Q := oModel:GetModel( 'MODEL_C9Q' )
	oModelC9R := oModel:GetModel( 'MODEL_C9R' )
	oModelCRN := oModel:GetModel( 'MODEL_CRN' )

	if TafLayESoc("02_04_02")
		oModelV1B := oModel:GetModel( 'MODEL_V1B' )
		oModelV1C := oModel:GetModel( 'MODEL_V1C' )
	endif

	//�����������������������������������������������������������Ŀ
	//�Busco a versao anterior do registro para gravacao do rastro�
	//�������������������������������������������������������������
	cVerAnt		:= oModelC91:GetValue( "C91_VERSAO" )
	cProtocolo	:= oModelC91:GetValue( "C91_PROTUL" )
	cEvento		:= oModelC91:GetValue( "C91_EVENTO" )

	//�����������������������������������������������������������������Ŀ
	//�Neste momento eu gravo as informacoes que foram carregadas       �
	//�na tela, pois neste momento o usuario ja fez as modificacoes que �
	//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
	//�nao devem ser consideradas neste momento                         �
	//�������������������������������������������������������������������
	For nlI := 1 To 1
		For nlY := 1 To Len( oModelC91:aDataModel[ nlI ] )
			Aadd( aGrava, { oModelC91:aDataModel[ nlI, nlY, 1 ], oModelC91:aDataModel[ nlI, nlY, 2 ] } )
		Next
	Next

	//Posicionando no registro
	DBSelectArea("T14")
	DBSetOrder(1)
	/*------------------------------------------
		T14 - Informa��es do Recibo de Pag.
	--------------------------------------------*/
	If T14->(MsSeek(xFilial("T14")+C91->(C91_ID + C91_VERSAO) ) )

		For nT14 := 1 To oModel:GetModel( 'MODEL_T14' ):Length()
			oModel:GetModel( 'MODEL_T14' ):GoLine(nT14)

			If !oModel:GetModel( 'MODEL_T14' ):IsDeleted()
				aAdd (aGravaT14 ,{oModelT14:GetValue('T14_IDEDMD'),;
							      oModelT14:GetValue('T14_CODCAT')})


				/*------------------------------------------
					C9K - Informa��es do Estab/Lota��o
				--------------------------------------------*/
				For nC9K := 1 to oModel:GetModel( "MODEL_C9K" ):Length()
					oModel:GetModel( "MODEL_C9K" ):GoLine(nC9K)
					If !oModel:GetModel( 'MODEL_C9K' ):IsEmpty()
						If !oModel:GetModel( "MODEL_C9K" ):IsDeleted()
							aAdd (aGravaC9K ,{oModelT14:GetValue('T14_IDEDMD'),;
											  oModelC9K:GetValue('C9K_ESTABE'),;
							 				  oModelC9K:GetValue('C9K_LOTACA'),;
							 				  oModelC9K:GetValue('C9K_QTDDIA'),;
											  oModelC9K:GetValue('C9K_CODLOT'),;
											  oModelC9K:GetValue('C9K_TPINSC'),;
											  oModelC9K:GetValue('C9K_NRINSC') } )

							/*------------------------------------------
								C9L - Informa��es da Remunera��o Trab.
							--------------------------------------------*/
							For nC9L := 1 to oModel:GetModel( "MODEL_C9L" ):Length()
								oModel:GetModel( "MODEL_C9L" ):GoLine(nC9L)
								If !oModel:GetModel( 'MODEL_C9L' ):IsEmpty()
									If !oModel:GetModel( "MODEL_C9L" ):IsDeleted()
										aAdd (aGravaC9L ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA'),;
														  oModelC9L:GetValue('C9L_TRABAL'),;
														  oModelC9L:GetValue('C9L_GRAEXP'),;
														  oModelC9L:GetValue('C9L_INDCON'),;
														  oModelC9L:GetValue('C9L_DTRABA') } )

										/*------------------------------------------
											   C9M - Itens da Remunera��o Trab.
										--------------------------------------------*/
										For nC9M := 1 to oModel:GetModel( "MODEL_C9M" ):Length()
											oModel:GetModel( "MODEL_C9M" ):GoLine(nC9M)
											If !oModel:GetModel( 'MODEL_C9M' ):IsEmpty()
												If !oModel:GetModel( "MODEL_C9M" ):IsDeleted()
													aAdd (aGravaC9M ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL'),;
																	  oModelC9M:GetValue('C9M_CODRUB'),;
																	  oModelC9M:GetValue('C9M_QTDRUB'),;
																	  oModelC9M:GetValue('C9M_VLRUNT'),;
																	  oModelC9M:GetValue('C9M_FATORR'),;
																	  oModelC9M:GetValue('C9M_VLRRUB'),;
																	  oModelC9M:GetValue('C9M_RUBRIC'),;
																	  oModelC9M:GetValue('C9M_IDTABR') } )
												EndIf
											EndIf
										Next //nC9M

										/*------------------------------------------
											 T6Y - Informa��es de Valores Pagos
										--------------------------------------------*/
										For nT6Y := 1 to oModel:GetModel( "MODEL_T6Y" ):Length()
											oModel:GetModel( "MODEL_T6Y" ):GoLine(nT6Y)
											If !oModel:GetModel( 'MODEL_T6Y' ):IsEmpty()
												If !oModel:GetModel( "MODEL_T6Y" ):IsDeleted()
													aAdd (aGravaT6Y ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL'),;
																	  oModelT6Y:GetValue('T6Y_CNPJOP'),;
																	  oModelT6Y:GetValue('T6Y_REGANS'),;
																	  oModelT6Y:GetValue('T6Y_VLPGTI')})

													/*------------------------------------------
													 	T6Z - Informa��es dos Dependentes
													--------------------------------------------*/
													For nT6Z := 1 to oModel:GetModel( "MODEL_T6Z" ):Length()
														oModel:GetModel( "MODEL_T6Z" ):GoLine(nT6Z)
														If !oModel:GetModel( 'MODEL_T6Z' ):IsEmpty()
															If !oModel:GetModel( "MODEL_T6Z" ):IsDeleted()
																aAdd (aGravaT6Z ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL')+oModelT6Y:GetValue('T6Y_CNPJOP')+oModelT6Y:GetValue('T6Y_REGANS'),;
																				  oModelT6Z:GetValue('T6Z_SEQUEN'),;
																				  oModelT6Z:GetValue('T6Z_TPDEP') ,;
																				  oModelT6Z:GetValue('T6Z_CPFDEP'),;
																				  oModelT6Z:GetValue('T6Z_NOMDEP'),;
																				  oModelT6Z:GetValue('T6Z_DTNDEP'),;
																				  oModelT6Z:GetValue('T6Z_VPGDEP')})

															EndIf
														EndIf
													Next //nT6Z
												EndIf
											EndIf
										Next //nT6Y

										if TafLayESoc("02_04_02")
											/*------------------------------------------
												V1B - Infos Trab Interm Per Apura��o
											--------------------------------------------*/
											For nV1B := 1 to oModel:GetModel( "MODEL_V1B" ):Length()
												oModel:GetModel( "MODEL_V1B" ):GoLine(nV1B)
												If !oModel:GetModel( 'MODEL_V1B' ):IsEmpty()
													If !oModel:GetModel( "MODEL_V1B" ):IsDeleted()
														aAdd (aGravaV1B ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL'),;
																		oModelV1B:GetValue('V1B_IDCONV')})
													EndIf
												EndIf
											Next //nV1B
										endif

									EndIf
								EndIf
							Next //nC9L
						EndIf
					EndIf
				Next //nC9K

				//Posicionando no registro
				//DBSelectArea("T3Y")
				//DBSetOrder(1)
				/*------------------------------------------
					C9N - Informa��es de Acordo
				--------------------------------------------*/
				If C9N->(MsSeek(xFilial("C9N")+C91->(C91_ID + C91_VERSAO) ) )
					For nC9N := 1 To oModel:GetModel( 'MODEL_C9N' ):Length()
						oModel:GetModel( 'MODEL_C9N' ):GoLine(nC9N)
						If !oModel:GetModel( 'MODEL_C9N' ):IsDeleted() .And. !oModel:GetModel( 'MODEL_C9N' ):IsEmpty()
							aAdd (aGravaC9N ,{oModelC9N:GetValue('C9N_DTACOR')	,;
										  	  oModelC9N:GetValue('C9N_TPACOR') 	,;
										  	  oModelC9N:GetValue('C9N_COMPAC') 	,;
										  	  oModelC9N:GetValue('C9N_DTEFAC')  ,;
										  	  oModelC9N:GetValue('C9N_DSC')		,;
										  	  oModelC9N:GetValue('C9N_REMUNS')	,;
											  oModelT14:GetValue('T14_IDEDMD') })

							/*------------------------------------
								C9O - Informa��es do Periodo
							-------------------------------------*/
							For nC9O := 1 to oModel:GetModel( "MODEL_C9O" ):Length()
								oModel:GetModel( "MODEL_C9O" ):GoLine(nC9O)
								If !oModel:GetModel( 'MODEL_C9O' ):IsEmpty()
									If !oModel:GetModel( "MODEL_C9O" ):IsDeleted()
										aAdd (aGravaC9O ,{oModelT14:GetValue('T14_IDEDMD')+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR'),;
														  oModelC9O:GetValue('C9O_PERREF')})

										/*------------------------------------
										  C9P - Informa��es do Estab/Lota��o
										-------------------------------------*/
										For nC9P := 1 to oModel:GetModel( "MODEL_C9P" ):Length()
											oModel:GetModel( "MODEL_C9P" ):GoLine(nC9P)
											If !oModel:GetModel( 'MODEL_C9P' ):IsEmpty()
												If !oModel:GetModel( 'MODEL_C9P' ):IsDeleted()
													aAdd (aGravaC9P ,{oModelT14:GetValue('T14_IDEDMD')+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF'),;
																	  oModelC9P:GetValue('C9P_ESTABE'),;
																	  oModelC9P:GetValue('C9P_LOTACA')})

													/*------------------------------------------
														C9Q - Informa��es da Remunera��o Trab.
													--------------------------------------------*/
													For nC9Q := 1 to oModel:GetModel( "MODEL_C9Q" ):Length()
														oModel:GetModel( "MODEL_C9Q" ):GoLine(nC9Q)
														If !oModel:GetModel( 'MODEL_C9Q' ):IsEmpty()
															If !oModel:GetModel( "MODEL_C9Q" ):IsDeleted()
																aAdd (aGravaC9Q ,{oModelT14:GetValue('T14_IDEDMD')+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF')+oModelC9P:GetValue('C9P_ESTABE')+oModelC9P:GetValue('C9P_LOTACA'),;
																				  oModelC9Q:GetValue('C9Q_TRABAL')	,;
																				  oModelC9Q:GetValue('C9Q_GRAEXP')	,;
																				  oModelC9Q:GetValue('C9Q_INDCON')  })

																/*------------------------------------------
																	   C9R - Itens da Remunera��o Trab.
																--------------------------------------------*/
																For nC9R := 1 to oModel:GetModel( "MODEL_C9R" ):Length()
																	oModel:GetModel( "MODEL_C9R" ):GoLine(nC9R)
																	If !oModel:GetModel( 'MODEL_C9R' ):IsEmpty()
																		If !oModel:GetModel( "MODEL_C9R" ):IsDeleted()
																			aAdd (aGravaC9R ,{oModelT14:GetValue('T14_IDEDMD')+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF')+oModelC9P:GetValue('C9P_ESTABE')+oModelC9P:GetValue('C9P_LOTACA')+oModelC9Q:GetValue('C9Q_TRABAL'),;
																							  oModelC9R:GetValue('C9R_CODRUB'),;
																							  oModelC9R:GetValue('C9R_QTDRUB'),;
																							  oModelC9R:GetValue('C9R_VLRUNT'),;
																							  oModelC9R:GetValue('C9R_FATORR'),;
																							  oModelC9R:GetValue('C9R_VLRRUB')})
																		EndIf
																	EndIf
																Next //nC9R

																if TafLayESoc("02_04_02")
																	/*------------------------------------------
																		V1C - Infos Trab Interm Per Anterior
																	--------------------------------------------*/
																	For nV1C := 1 to oModel:GetModel( "MODEL_V1C" ):Length()
																		oModel:GetModel( "MODEL_V1C" ):GoLine(nV1C)
																		If !oModel:GetModel( 'MODEL_V1C' ):IsEmpty()
																			If !oModel:GetModel( "MODEL_V1C" ):IsDeleted()
																				aAdd (aGravaV1C ,{oModelT14:GetValue('T14_IDEDMD')+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF')+oModelC9P:GetValue('C9P_ESTABE')+oModelC9P:GetValue('C9P_LOTACA')+oModelC9Q:GetValue('C9Q_TRABAL'),;
																								oModelV1C:GetValue('V1C_IDCONV')})
																			EndIf
																		EndIf
																	Next //nV1C
																endif

															EndIf
														EndIf
													Next //nC9Q
												EndIf
											EndIf
										Next //nC9P
									EndIf
								EndIf
							Next //nC9O
						EndIf
					Next //nC9N
				EndIf
			EndIf
		Next //nT14
	EndIf

	For nlI := 1 To oModel:GetModel( 'MODEL_CRN' ):Length()
		oModel:GetModel( 'MODEL_CRN' ):GoLine(nlI)

		If !oModel:GetModel( 'MODEL_CRN' ):IsDeleted()
			aAdd (aGravaCRN,{	oModelCRN:GetValue('CRN_TPTRIB')	,;
								oModelCRN:GetValue('CRN_IDPROC')	,;
								oModelCRN:GetValue('CRN_IDSUSP')	})
		EndIf
	Next

	For nlI := 1 To oModel:GetModel( 'MODEL_T6W' ):Length()
		oModel:GetModel( 'MODEL_T6W' ):GoLine(nlI)

		If !oModel:GetModel( 'MODEL_T6W' ):IsDeleted()
			aAdd (aGravaT6W ,{	    oModelT6W:GetValue('T6W_TPINSC')	,;
							  		oModelT6W:GetValue('T6W_NRINSC')	,;
							  		oModelT6W:GetValue('T6W_CODCAT')	,;
							  		oModelT6W:GetValue('T6W_VLREMU')	})
		EndIf
	Next

	//�����������������������������������������������������������Ŀ
	//�Seto o campo como Inativo e gravo a versao do novo registro�
	//�no registro anterior                                       �
	//|                                                           |
	//|ATENCAO -> A alteracao destes campos deve sempre estar     |
	//|abaixo do Loop do For, pois devem substituir as informacoes|
	//|que foram armazenadas no Loop acima                        |
	//�������������������������������������������������������������
	FAltRegAnt( 'C91', '2' )

	//��������������������������������������������������Ŀ
	//�Neste momento eu preciso setar a operacao do model�
	//�como Inclusao                                     �
	//����������������������������������������������������
	oModel:DeActivate()
	oModel:SetOperation( 3 )
	oModel:Activate()

	//�������������������������������������������������������Ŀ
	//�Neste momento eu realizo a inclusao do novo registro ja�
	//�contemplando as informacoes alteradas pelo usuario     �
	//���������������������������������������������������������
	For nlI := 1 To Len( aGrava )
		oModel:LoadValue( 'MODEL_C91', aGrava[ nlI, 1 ], aGrava[ nlI, 2 ] )
	Next

	For nlI := 1 To Len( aGravaT6W )
		If nlI > 1
			oModel:GetModel( 'MODEL_T6W' ):AddLine()
		EndIf
		oModel:LoadValue( "MODEL_T6W", "T6W_TPINSC",	aGravaT6W[nlI][1] )
		oModel:LoadValue( "MODEL_T6W", "T6W_NRINSC",	aGravaT6W[nlI][2] )
		oModel:LoadValue( "MODEL_T6W", "T6W_CODCAT",	aGravaT6W[nlI][3] )
		oModel:LoadValue( "MODEL_T6W", "T6W_VLREMU",	aGravaT6W[nlI][4] )
		oModel:LoadValue( "MODEL_T6W", "T6W_NOMEVE",	"S1200" )
	Next

	/*------------------------------------------
		T14 - Informa��es do Recibo de Pag.
	--------------------------------------------*/
	For nT14 := 1 to Len( aGravaT14 )

		oModel:GetModel( 'MODEL_T14' ):LVALID	:= .T.

		If nT14 > 1
			oModel:GetModel( "MODEL_T14" ):AddLine()
		EndIf

		oModel:LoadValue( "MODEL_T14", "T14_IDEDMD", aGravaT14[nT14][1] )
		oModel:LoadValue( "MODEL_T14", "T14_CODCAT", aGravaT14[nT14][2] )

		/*------------------------------------------
		   C9K - Informa��es do Estab/Lota��o
		--------------------------------------------*/
		nC9KAdd := 1
		For nC9K := 1 to Len( aGravaC9K )
			If  aGravaC9K[nC9K][1] == aGravaT14[nT14][1]
				oModel:GetModel( 'MODEL_C9K' ):LVALID := .T.

				If nC9KAdd > 1
					oModel:GetModel( "MODEL_C9K" ):AddLine()
				EndIf

				oModel:LoadValue( "MODEL_C9K", "C9K_ESTABE", aGravaC9K[nC9K][2] )
				oModel:LoadValue( "MODEL_C9K", "C9K_LOTACA", aGravaC9K[nC9K][3] )
				oModel:LoadValue( "MODEL_C9K", "C9K_QTDDIA", aGravaC9K[nC9K][4] )
				oModel:LoadValue( "MODEL_C9K", "C9K_CODLOT", aGravaC9K[nC9K][5] )
				oModel:LoadValue( "MODEL_C9K", "C9K_TPINSC", aGravaC9K[nC9K][6] )
				oModel:LoadValue( "MODEL_C9K", "C9K_NRINSC", aGravaC9K[nC9K][7] )

				/*------------------------------------------
				   C9L - Informa��es da Remunera��o Trab.
				--------------------------------------------*/
				nC9LAdd := 1
				For nC9L := 1 to Len( aGravaC9L )
					If aGravaC9L[nC9L][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3])
						oModel:GetModel( 'MODEL_C9L' ):LVALID := .T.

						If nC9LAdd > 1
							oModel:GetModel( "MODEL_C9L" ):AddLine()
						EndIf

						oModel:LoadValue( "MODEL_C9L", "C9L_TRABAL", aGravaC9L[nC9L][2] )
						oModel:LoadValue( "MODEL_C9L", "C9L_GRAEXP", aGravaC9L[nC9L][3] )
						oModel:LoadValue( "MODEL_C9L", "C9L_INDCON", aGravaC9L[nC9L][4] )
						oModel:LoadValue( "MODEL_C9L", "C9L_DTRABA", aGravaC9L[nC9L][5] )

						/*------------------------------------------
						   	  C9M - Itens da Remunera��o Trab.
						--------------------------------------------*/
						nC9MAdd := 1
						For nC9M := 1 to Len( aGravaC9M )
							If aGravaC9M[nC9M][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2])
								oModel:GetModel( 'MODEL_C9M' ):LVALID := .T.

								If nC9MAdd > 1
									oModel:GetModel( "MODEL_C9M" ):AddLine()
								EndIf

								oModel:LoadValue( "MODEL_C9M", "C9M_CODRUB",	aGravaC9M[nC9M][2] )
								oModel:LoadValue( "MODEL_C9M", "C9M_QTDRUB",	aGravaC9M[nC9M][3] )
								oModel:LoadValue( "MODEL_C9M", "C9M_VLRUNT",	aGravaC9M[nC9M][4] )
								oModel:LoadValue( "MODEL_C9M", "C9M_FATORR",	aGravaC9M[nC9M][5] )
								oModel:LoadValue( "MODEL_C9M", "C9M_VLRRUB",	aGravaC9M[nC9M][6] )
								oModel:LoadValue( "MODEL_C9M", "C9M_RUBRIC",	aGravaC9M[nC9M][7] )
								oModel:LoadValue( "MODEL_C9M", "C9M_IDTABR",	aGravaC9M[nC9M][8] )

								nC9MAdd++

							EndIf
						Next //nC9M

						/*------------------------------------------
						   	  T6Y - Informa��es de Valores Pagos
						--------------------------------------------*/
						nT6YAdd := 1
						For nT6Y := 1 to Len( aGravaT6Y )
							If aGravaT6Y[nT6Y][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2])
								oModel:GetModel( 'MODEL_T6Y' ):LVALID := .T.

								If nT6YAdd > 1
									oModel:GetModel( "MODEL_T6Y" ):AddLine()
								EndIf

								oModel:LoadValue( "MODEL_T6Y", "T6Y_CNPJOP",	aGravaT6Y[nT6Y][2] )
								oModel:LoadValue( "MODEL_T6Y", "T6Y_REGANS",	aGravaT6Y[nT6Y][3] )
								oModel:LoadValue( "MODEL_T6Y", "T6Y_VLPGTI",	aGravaT6Y[nT6Y][4] )
								oModel:LoadValue( "MODEL_T6Y", "T6Y_NOMEVE",	"S1200" )

								/*------------------------------------------
								   	  T6Z - Informa��es dos Dependentes
								--------------------------------------------*/
								nT6ZAdd := 1
								For nT6Z := 1 to Len( aGravaT6Z )
									If aGravaT6Z[nT6Z][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2]+aGravaT6Y[nT6Y][2]+aGravaT6Y[nT6Y][3])
										oModel:GetModel( 'MODEL_T6Z' ):LVALID := .T.

										If nT6ZAdd > 1
											oModel:GetModel( "MODEL_T6Z" ):AddLine()
										EndIf

										oModel:LoadValue( "MODEL_T6Z", "T6Z_SEQUEN",	aGravaT6Z[nT6Z][2] )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_TPDEP",		aGravaT6Z[nT6Z][3] )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_CPFDEP",	aGravaT6Z[nT6Z][4] )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_NOMDEP",	aGravaT6Z[nT6Z][5] )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_DTNDEP",	aGravaT6Z[nT6Z][6] )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_VPGDEP",	aGravaT6Z[nT6Z][7] )
										oModel:LoadValue( "MODEL_T6Z", "T6Z_NOMEVE",	"S1200" )

										nT6ZAdd++
									EndIf
								Next //nT6Z
								nT6YAdd++
							EndIf
						Next //nT6Y


						if TafLayESoc("02_04_02")
							/*------------------------------------------
								V1B - Infos Trab Interm Per Apura��o
							--------------------------------------------*/
							nV1BAdd := 1
							For nV1B := 1 to Len( aGravaV1B )
								If aGravaV1B[nV1B][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2])
									oModel:GetModel( 'MODEL_V1B' ):LVALID := .T.

									If nV1BAdd > 1
										oModel:GetModel( "MODEL_V1B" ):AddLine()
									EndIf

									oModel:LoadValue( "MODEL_V1B", "V1B_IDCONV",	aGravaV1B[nV1B][2] )

									nV1BAdd++

								EndIf
							Next //nV1B
						endif

						nC9LAdd++
					EndIf
				Next //nC9L
				nC9KAdd++
			EndIf
		Next //nC9K

		/*------------------------------------------
			C9N - Informa��es de Acordo
		--------------------------------------------*/
		For nC9N := 1 to Len( aGravaC9N )

			If aGravaT14[nT14][1] == aGravaC9N[nC9N][7]

				oModel:GetModel( 'MODEL_C9N' ):LVALID	:= .T.

				If !(oModel:GetModel( "MODEL_C9N" ):IsEmpty())
					oModel:GetModel( "MODEL_C9N" ):AddLine()
				EndIf

				oModel:LoadValue( "MODEL_C9N", "C9N_DTACOR",	aGravaC9N[nC9N][1] )
				oModel:LoadValue( "MODEL_C9N", "C9N_TPACOR",	aGravaC9N[nC9N][2] )
				oModel:LoadValue( "MODEL_C9N", "C9N_COMPAC",	aGravaC9N[nC9N][3] )
				oModel:LoadValue( "MODEL_C9N", "C9N_DTEFAC",	aGravaC9N[nC9N][4] )
				oModel:LoadValue( "MODEL_C9N", "C9N_DSC"   ,	aGravaC9N[nC9N][5] )
				oModel:LoadValue( "MODEL_C9N", "C9N_REMUNS",	aGravaC9N[nC9N][6] )

				For nC9O := 1 to Len( aGravaC9O )
					If  aGravaC9O[nC9O][1] == (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2])
						oModel:GetModel( 'MODEL_C9O' ):LVALID := .T.

						If nC9OAdd > 1
							oModel:GetModel( "MODEL_C9O" ):AddLine()
						EndIf

						oModel:LoadValue( "MODEL_C9O", "C9O_PERREF", aGravaC9O[nC9O][2] )

						/*------------------------------------------
							C9P - Informa��es do Estab/Lota��o
						--------------------------------------------*/
						nC9PAdd := 1
						For nC9P := 1 to Len( aGravaC9P )
							If aGravaC9P[nC9P][1] == (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2])
								oModel:GetModel( 'MODEL_C9P' ):LVALID := .T.

								If nC9PAdd > 1
									oModel:GetModel( "MODEL_C9P" ):AddLine()
								EndIf

								oModel:LoadValue( "MODEL_C9P", "C9P_ESTABE",	aGravaC9P[nC9P][2] )
								oModel:LoadValue( "MODEL_C9P", "C9P_LOTACA",	aGravaC9P[nC9P][3] )

								/*------------------------------------------
									C9Q - Informa��es da Remunera��o Trab.
								--------------------------------------------*/
								nC9QAdd := 1
								For nC9Q := 1 to Len( aGravaC9Q )
									If  aGravaC9Q[nC9Q][1] == (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2]+aGravaC9P[nC9P][2]+aGravaC9P[nC9P][3])
										oModel:GetModel( 'MODEL_C9Q' ):LVALID := .T.

										If nC9QAdd > 1
											oModel:GetModel( "MODEL_C9Q" ):AddLine()
										EndIf

										oModel:LoadValue( "MODEL_C9Q", "C9Q_TRABAL",	aGravaC9Q[nC9Q][2] )
										oModel:LoadValue( "MODEL_C9Q", "C9Q_GRAEXP",	aGravaC9Q[nC9Q][3] )
										oModel:LoadValue( "MODEL_C9Q", "C9Q_INDCON",    aGravaC9Q[nC9Q][4] )

										/*------------------------------------------
												C9R - Itens da Remunera��o Trab.
										--------------------------------------------*/
										nC9RAdd := 1
										For nC9R := 1 to Len( aGravaC9R )
											If  aGravaC9R[nC9R][1] == (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2]+aGravaC9P[nC9P][2]+aGravaC9P[nC9P][3]+aGravaC9Q[nC9Q][2])
												oModel:GetModel( 'MODEL_C9R' ):LVALID := .T.

												If nC9RAdd > 1
													oModel:GetModel( "MODEL_C9R" ):AddLine()
												EndIf

												oModel:LoadValue( "MODEL_C9R", "C9R_CODRUB",	aGravaC9R[nC9R][2] )
												oModel:LoadValue( "MODEL_C9R", "C9R_QTDRUB",	aGravaC9R[nC9R][3] )
												oModel:LoadValue( "MODEL_C9R", "C9R_VLRUNT",	aGravaC9R[nC9R][4] )
												oModel:LoadValue( "MODEL_C9R", "C9R_FATORR",	aGravaC9R[nC9R][5] )
												oModel:LoadValue( "MODEL_C9R", "C9R_VLRRUB",	aGravaC9R[nC9R][6] )

												nC9RAdd++

											EndIf
										Next //nC9R

										if TafLayESoc("02_04_02")
											/*------------------------------------------
												V1C - Infos Trab Interm Per Anterior
											--------------------------------------------*/
											nV1CAdd := 1
											For nV1C := 1 to Len( aGravaV1C )
												If  aGravaV1C[nV1C][1] == (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2]+aGravaC9P[nC9P][2]+aGravaC9P[nC9P][3]+aGravaC9Q[nC9Q][2])
													oModel:GetModel( 'MODEL_V1C' ):LVALID := .T.

													If nV1CAdd > 1
														oModel:GetModel( "MODEL_V1C" ):AddLine()
													EndIf

													oModel:LoadValue( "MODEL_V1C", "V1C_IDCONV",	aGravaV1C[nV1C][2] )

													nV1CAdd++
												EndIf
											Next //nV1C
										endif

										nC9QAdd++
									EndIf
								Next //nC9Q
							EndIf
						Next //nC9P
						nC9OAdd++
					EndIf
				Next //nC9O
			EndIf
		Next //nC9N
	Next //nT14

		

	For nlI := 1 To Len( aGravaCRN )
		If nlI > 1
			oModel:GetModel( 'MODEL_CRN' ):AddLine()
		EndIf
		oModel:LoadValue( "MODEL_CRN", "CRN_TPTRIB" ,	aGravaCRN[nlI][1] )
		oModel:LoadValue( "MODEL_CRN", "CRN_IDPROC" ,	aGravaCRN[nlI][2] )
		oModel:LoadValue( "MODEL_CRN", "CRN_IDSUSP" ,	aGravaCRN[nlI][3] )
	Next

	//�������������������������������Ŀ
	//�Busco a versao que sera gravada�
	//���������������������������������
	cVersao := xFunGetVer()

	/*---------------------------------------------------------
	ATENCAO -> A alteracao destes campos deve sempre estar
	abaixo do Loop do For, pois devem substituir as informacoes
	que foram armazenadas no Loop acima
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_C91", "C91_VERSAO", cVersao )
	oModel:LoadValue( "MODEL_C91", "C91_VERANT", cVerAnt )
	oModel:LoadValue( "MODEL_C91", "C91_PROTPN", cProtocolo )
	oModel:LoadValue( "MODEL_C91", "C91_PROTUL", '' )
	oModel:LoadValue( "MODEL_C91", "C91_NOMEVE", "S1200" )


	/*---------------------------------------------------------
	Tratamento para que caso o Evento Anterior fosse de exclus�o
	seta-se o novo evento como uma "nova inclus�o", caso contr�rio o
	evento passar a ser uma altera��o
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_C91", "C91_EVENTO", "E" )
	oModel:LoadValue( "MODEL_C91", "C91_ATIVO", "1" )

	//Gravo altera��o para o Extempor�neo
	If lGoExtemp
		TafGrvExt( oModel, 'MODEL_C91', 'C91' )
	EndIf

	FwFormCommit( oModel )
	TAFAltStat( 'C91',"6" )

End Transaction

Return ( .T. )

//-------------------------------------------------------------------
/*/{Protheus.doc} XRetCatTrab
Funcao que retorna a categoria de um trabalhador dependendo de seu evento

@param cIdTrab    - Id do trabalhador

@return cRet - Retorno com a categoria encontrada.

@author Vitor Siqueira
@since 13/01/2015
@version 1.0

/*/
//-------------------------------------------------------------------
Function XRetCatTrab(cAliasTab, cCodTrab)

Local cRet		:= ""
Local cCodCat	:= ""
Local cCampo 	:= ""
Local cAlias	:= ""

cAlias := "CUP"
cCampo := "CUP_CODCAT"

If !Empty(cAlias)
	cCodCat := POSICIONE(cAlias,1, xFilial(cAlias)+cCodTrab,cCampo)
	cRet    := POSICIONE("C87" ,1, xFilial("C87") +cCodCat,"C87_CODIGO")
	If !Empty(cRet)
		cRet    += " - " + POSICIONE("C87" ,1, xFilial("C87") +cCodCat,"C87_DESCRI")
	EndIf
EndIf

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} Taf250MV
Retorna se o XML possui m�ltiplos v�nculos
@author  Victor A. Barbosa
@since   19/06/2018
@version version
/*/
//-------------------------------------------------------------------
Static Function Taf250MV( oXML, lInfoCompl, cChave, nChvIndex, cFilEv, aIncons )

Local lRet			:= .F.
Local aChildRem		:= {}
Local nVinc			:= 0
Local nEstab		:= 0
Local nDmDev		:= 1
Local cNrInsc		:= ""
Local cMatric       := ""
Local cPathDmDev	:= ""
Local cPathEstab	:= ""

aChildRem	:= oXML:XPathGetChildArray( '/eSocial/evtRemun' )
lInfoCompl	:= oXML:XPathHasNode( '/eSocial/evtRemun/ideTrabalhador/infoComplem' )

aEval( aChildRem, { |x| Iif( x[1] == "dmDev", nVinc++,  ) } )

cPathDmDev	:= "/eSocial/evtRemun/dmDev[" + cValToChar( nDmDev ) + "]"

If nVinc > 1

	cPathEstab	:= cPathDmDev + "/infoPerApur/ideEstabLot[" + cValToChar( 1 ) + "]"
	cNrInsc 	:= FTafGetVal( cPathEstab + "/nrInsc", "C", .F., {}, .F. )
	cMatric     := FTafGetVal( cPathEstab + "/remunPerApur/matricula", "C", .F., {}, .F. )

	While oDados:xPathHasNode( cPathDmDev )

		nEstab		:= 1
		cPathEstab	:= cPathDmDev + "/infoPerApur/ideEstabLot[" + cValToChar( nEstab ) + "]"

		While oDados:xPathHasNode( cPathEstab )

			// --> Se encontrou n�meros de inscri��es diferentes ou matr�culas diferentes, ent�o � sinal que � m�ltiplos v�nculos
			If cNrInsc <> FTafGetVal( cPathEstab + "/nrInsc"                , "C", .F., {}, .F. ) .Or. ;
			   cMatric <> FTafGetVal( cPathEstab + "/remunPerApur/matricula", "C", .F., {}, .F. )

				lRet := .T.
				Exit

			EndIf

			nEstab ++
			cPathEstab := cPathDmDev + "/infoPerApur/ideEstabLot[" + cValToChar( nEstab ) + "]"

		EndDo

		// Se encontrou diferen�as, quebra o loop
		If lRet
			Exit
		EndIf

		nDmDev ++
		cPathDmDev := "/eSocial/evtRemun/dmDev[" + cValToChar( nDmDev ) + "]"

	EndDo
Else

	//Verifica se o evento ja existe na base
	("C91")->( DbSetOrder( nChvIndex ) ) // C91_FILIAL+C91_INDAPU+C91_PERAPU+C91_CPF+C91_NIS+C91_NOMEVE+C91_ATIVO
	If ("C91")->( MsSeek(FTafGetFil(cFilEv,@aIncons,"C91") + cChave + 'S1200' +'1' ) )
	
		("T14")->( DbSetOrder( 1 ) )
		If ("T14")->(MsSeek(FTafGetFil(cFilEv,@aIncons,"C91")+C91->C91_ID+C91->C91_VERSAO ))
			cChvT14 := C91->C91_FILIAL+C91->C91_ID+C91->C91_VERSAO
			While ("T14")->(!EOF()) .AND. T14->(T14_FILIAL+T14_ID+T14_VERSAO) == cChvT14
			
				If AllTrim(T14->T14_IDEDMD) == FTafGetVal("/eSocial/evtRemun/dmDev/ideDmDev", "C", .F., {}, .F. )
					lRet := .F.
					Exit
				Else
					lRet := .T.
				EndIf		
				
			("T14")->(DbSkip())
			End
		EndIf		
		
	EndIf	

	
EndIf

Return( lRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFIdTabRub
Retorna o ID da tabela de R�brica
@author  Victor A. Barbosa
@since   19/06/2018
@version 1
/*/
//-------------------------------------------------------------------
Function TAFIdTabRub( cCodERP, cAliasEve, cCodRubr )

Local aArea 		:= GetArea()
Local cNextAlias	:= GetNextAlias()
Local cCNPJRaiz		:= SubStr( SM0->M0_CGC, 1, 8)
Local cIDRet		:= ""
Local aInfoSM0		:= {}
Local nCount 		:= 0
Local cFilRub		:= ""		

Default cCodRubr    := ""
Default cAliasEve   := "T3M"
If __cFilCache != cEmpAnt+cFilAnt  

	__cInFiliais := ""
	aInfoSM0   := FWLoadSM0()

	aEval( aInfoSM0, { |x| Iif( SubStr( x[18], 1, 8) == cCNPJRaiz .And. x[1] == cEmpAnt, __cInFiliais += xFilial( cAliasEve, x[2] ) + ";", Nil ) } )
	__cInFiliais := FormatIN( __cInFiliais, ";" )
	__cInFiliais := "%" + __cInFiliais + "%"
	__cFilCache  := cEmpAnt+cFilAnt 
EndIf

If Select( cNextAlias ) > 0
	(cNextAlias)->( dbCloseArea() )
EndIf

BeginSQL Alias cNextAlias
	SELECT 	T3M_ID FROM %table:T3M% T3M
	WHERE 	T3M_CODERP = %exp:cCodERP%
	AND 	T3M_FILIAL IN %exp:__cInFiliais%
	AND T3M.%notdel%
EndSQL

(cNextAlias)->( dbGoTop() )

While (cNextAlias)->( !Eof())
	nCount++
	(cNextAlias)->(dBSkip())
EndDo

//Pega o Codigo da tabela de rubricas olhando o status da rubrica
//desta forma quando o cliente estiver com o c�digo ERP duplicado
//o sistema conseguir� pegar o c�digo correto.
If nCount > 1 .And. !EMPTY(cCodRubr)

	cFilRub := xFilial("C8R")

	If Select( cNextAlias ) > 0
		(cNextAlias)->( dbCloseArea() )
	EndIf

	BeginSQL Alias cNextAlias
		SELECT 	T3M_ID FROM %table:T3M% T3M, %table:C8R% C8R
		WHERE 	T3M_CODERP = %exp:cCodERP%
		AND 	T3M_ID = C8R_IDTBRU
		AND 	C8R_CODRUB = %exp:cCodRubr%
		AND 	T3M_FILIAL IN %exp:__cInFiliais%
		AND 	C8R_FILIAL = %exp:cFilRub%
		AND 	C8R_STATUS = '4'
		AND C8R.%notdel%
		AND T3M.%notdel%
	EndSQL
EndIf

(cNextAlias)->(dbGoTop())

cIDRet := (cNextAlias)->T3M_ID

(cNextAlias)->(dbCloseArea())

RestArea( aArea )
If Len(aInfoSM0) > 0
	aSize(aInfoSM0,0)
EndIf 

Return( cIDRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} XC91Valid
Rotina p/ executar os valids presentes no dicion�rio de dados em virtude
do campo X3_VALID n�o possuir caracteres suficientes
@author  Diego Santos
@since   15-06-2018
@version 1.0
/*/
//-------------------------------------------------------------------

Function XC91Valid( cCampo )

Local lRet := .T.

If AllTrim(cCampo) == "C91_INDAPU"
	lRet := Pertence(" 12") .and. XFUNVldUni("C91",2,M->C91_INDAPU+FWFLDGET("C91_PERAPU")+FWFLDGET("C91_TRABAL")+'S1200'+'1')
ElseIf AllTrim(cCampo) == "C91_PERAPU"
	lRet := .T. //XFUNVldUni("C91",2,FWFLDGET("C91_INDAPU")+M->C91_PERAPU+FWFLDGET("C91_TRABAL")+'S1200'+'1')
ElseIf AllTrim(cCampo) == "C91_TRABAL"
	lRet := XFUNVldCmp("C9V",,,,1,,,.T.) .And. XFUNVldUni("C91",2,FWFLDGET("C91_INDAPU")+FWFLDGET("C91_PERAPU")+M->C91_TRABAL+'S1200'+'1')
EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF250DicMV
Verifica se o ambiente possui alguns dos campos que precisam para utiliza��o de m�ltiplos v�nculos
@author  Victor A. Barbosa
@since   29/06/2018
@version 1
/*/
//-------------------------------------------------------------------
Static Function TAF250DicMV( lViewHelp )

Local lRet := .F.

Default lViewHelp := .T.

If TAFColumnPos( "C91_CPF" ) .And. TAFColumnPos( "C91_NIS" ) .And. TAFColumnPos( "C91_MV" ) .And. TafIndexInDic( "C91", 6, .F. ) .And.;
	TAFColumnPos( "C9P_CODLOT" ) .And. TAFColumnPos( "C91_NOME" ) .And. TAFColumnPos( "C9M_CODLOT" ) .And. TAFColumnPos( "C9L_CODLOT" ) .And. TafIndexInDic( "C91", 7, .F. ) 

	lRet := .T.

Else
	If lViewHelp
		MsgInfo( STR0074 , STR0075 ) //"O ambiente do TAF encontra-se desatualizado. Para utiliza��o desta funcionalidade, ser� necess�rio executar o compatibilizador de dicion�rio de dados UPDDISTR dispon�vel no portal do cliente do TAF."
									 // "Ambiente Desatualizado!"
	EndIf
EndIf

Return( lRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF250View
Monta a View din�mica
@author  Victor A. Barbosa
@since   30/07/2018
@version 1
/*/
//-------------------------------------------------------------------
Function TAF250View( cAlias, nRecno )

Local oNewView	:= ViewDef()
Local aArea 	:= GetArea()
Local oExecView	:= Nil

DbSelectArea( cAlias )
(cAlias)->( DbGoTo( nRecno ) )

oExecView := FWViewExec():New()
oExecView:setOperation( 1 )
oExecView:setTitle( STR0001 )
oExecView:setOK( {|| .T. } )
oExecView:setModal(.F.)
oExecView:setView( oNewView )
oExecView:openView(.T.)

RestArea( aArea )

Return( 1 )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF250Inc
Monta a View din�mica
@author  Victor A. Barbosa
@since   30/07/2018
@version 1
/*/
//-------------------------------------------------------------------
Function TAF250Inc( cAlias, nRecno )

Local oNewView	:= ViewDef()
Local aArea 	:= GetArea()
Local oExecView	:= Nil

DbSelectArea( cAlias )
(cAlias)->( DbGoTo( nRecno ) )

oExecView := FWViewExec():New()
oExecView:setOperation( 3 )
oExecView:setTitle( STR0001 )
oExecView:setOK( {|| .T. } )
oExecView:setModal(.F.)
oExecView:setView( oNewView )
oExecView:openView(.T.)

RestArea( aArea )

Return( 3 )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF250Nis
Busca NIS do funcion�rio

@param lC9VPosc - Indica que a tabela C9V j� est� posicionada

@author  Leonardo Kichitaro
@since   30/07/2018
@version 1
/*/
//-------------------------------------------------------------------
Function TAF250Nis( cFilialC9V, cIDC9V, cNisC9V, cPerApu, cEvento,lC9VPosc)

Local cAlias	:=	GetNextAlias()
Local cRetNIS	:=	""
Local dFrtDt	:=	SToD( "" )
Local dLstDt	:=	SToD( "" )
Local aArea		:=	GetArea()
Local aAreaT1V	:=	T1V->( GetArea() )
Local cCPF      :=  ""

Default cFilialC9V	:=	xFilial( "C9V" )
Default cIDC9V		:=	""
Default cNisC9V		:=	""
Default cPerApu		:=	""
Default cEvento		:=	""
Default lC9VPosc 	:= .F.


If Empty( cIDC9V )
	TAFConOut( "TAF250Nis| Erro na busca dos dados do funcionario. ID nao informado." + CRLF ;
				+ "Verifique se o seu cadastro de funcionario esta com todos os ID's preenchidos e se nao ha duplicidade de ID's" + CRLF + CRLF ;
				+ "Tabela envolvida: 'C9V' | Campo: 'C9V_ID'" )
Else
	If lC9VPosc
		cCPF := C9V->C9V_CPF
	Else
		cCPF := POSICIONE("C9V", 2, cFilialC9V+cIDC9V+"1","C9V->C9V_CPF")
	EndIf 

	//Tratamento para eventos que n�o possuem per�odo no formato MM/AAAA
	If Len( cPerApu ) == 8
		dFrtDt := SToD( cPerApu )
		dLstDt := SToD( cPerApu )
	Else
		cPerApu := StrTran( cPerApu, "-", "" )
		
		If Len(AllTrim(cPerApu)) == 4 //Folha Anual
			//Folha Anual
			dFrtDt := CTOD("01/01/" + AllTrim(cPerApu))
			dLstDt := LastDate( CTOD("01/12/" + AllTrim(cPerApu)) )
		Else 
			If !( Empty( cPerApu ) )
				dFrtDt := CToD( "01/" + SubStr( cPerApu, 5, 2 ) + "/" + SubStr( cPerApu, 1, 4 ) )
				dLstDt := LastDate( dFrtDt )
			EndIf
		EndIf 
	EndIf

	TAFConOut( "TAF250Nis |" + FWTimeStamp( 3 ) + " - Chave de busca do funcionario: |" + cFilialC9V + cIDC9V + cNisC9V + "| Recno: " + Str( C9V->( Recno() ) ) )

If FwIsInCallStack("TAFA266") .Or. FwIsInCallStack("TAFA250") //--> Fa�o essa query abaixo para verificar se o ID posicionado tem correspond�ncia na C9V e T1U

	BeginSql Alias cAlias //--> Mesmo que o CPF existir na T1U (com outro ID), se o ID posicionado em tela (S-2299\S-1200) existir somente na C9V, pego o NIS da C9V.
	
		column T1U_DTALT as Date

		SELECT DISTINCT T1U_NIS, T1U_DTALT
		FROM %table:T1U% T1U
		WHERE T1U.T1U_FILIAL = %exp:cFilialC9V%
			AND T1U.T1U_CPF = %exp:cCPF%
			AND T1U.T1U_ATIVO = '1'
			AND T1U.%notdel%
			AND T1U.T1U_ID = %exp:cIDC9V% 
		ORDER BY T1U_DTALT DESC
	EndSql

Else 

	BeginSql Alias cAlias //--> Tratamento de NIS com CPF existente na T1U para todos os eventos, exceto S-1200 e S-2299.
	
		column T1U_DTALT as Date

		SELECT DISTINCT T1U_NIS, T1U_DTALT
		FROM %table:T1U% T1U
		WHERE T1U.T1U_FILIAL = %exp:cFilialC9V%
			AND T1U.T1U_CPF = %exp:cCPF%
			AND T1U.T1U_ATIVO = '1'
			AND T1U.%notdel%
		ORDER BY T1U_DTALT DESC
	EndSql

EndIf 

	While ( cAlias )->( !Eof() )

		If Empty(dLstDt) .Or. ( cAlias )->T1U_DTALT <= dLstDt
			cRetNIS := ( cAlias )->T1U_NIS
			Exit
		EndIf
		( cAlias )->(dBSkip())
	EndDo 

	If Empty( cRetNIS )
		TAFConOut( "TAF250Nis| Erro na busca dos dados do funcionario na tabela de historico. Funcionario nao encontrado." + CRLF ;
					+ "Verifique se o seu cadastro do funcionario possui informacoes na tabela de historico para o ID: " + cIDC9V + " Filial: " + cFilialC9V + CRLF ;
					+ "Caso n�o tenha, realize o ajuste do cadastro dentro do ERP e re-fa�a a integra��o com o TAF." + CRLF + CRLF ;
					+ "Tabelas envolvidas: 'C9V e T1U' | Campos: 'C9V_ID e T1U_ID'" + CRLF ;
					+ "Continuaremos o processo com o NIS informado dentro do cadastro do funcionario: " + cNisC9V + CRLF ; 
					+ "Se o funcionario nao teve altera�oes cadastrais, desconsiderar esta mensagem." )

		cRetNIS := cNisC9V
	EndIf
EndIf

If Select(cAlias) > 0 
	( cAlias )->( DBCloseArea() ) 
EndIf 

RestArea( aAreaT1V )
RestArea( aArea )

Return( cRetNIS )

//---------------------------------------------------------------------
/*/{Protheus.doc} IsTAUTO
@type			function
@description	Fun��o respons�vel por verificar se o Trabalhador � Aut�nomo ( TAUTO )
@author			Felipe C. Seolin
@since			01/09/2018
@version		1.0
@param			cCPF	-	CPF do Trabalhador
				cMatric	-	Matr�cula do Trabalhador
@return			lRet	-	Indica se � Trabalhador Aut�nomo ( TAUTO )
/*/
//---------------------------------------------------------------------
Static Function IsTAUTO( cCPF, cMatric )

Local cAlias	:=	GetNextAlias()
Local cQuery	:=	""
Local cINFilMV	:=	""
Local cCNPJRaiz	:=	SubStr( SM0->M0_CGC, 1, 8 )
Local nI		:=	0
Local aInfoSM0	:=	FWLoadSM0()
Local aFilMV	:=	{}
Local lRet		:=	.F.

Default cCPF	:=	""
Default cMatric	:=	""

aEval( aInfoSM0, { |x| Iif( SubStr( x[18], 1, 8 ) == cCNPJRaiz .and. x[1] == cEmpAnt, aAdd( aFilMV, x[2] ), Nil ) } )

For nI := 1 to Len( aFilMV )
	If nI > 1
		cINFilMV += ","
	EndIf
	cINFilMV += "'" + xFilial( "C9V", aFilMV[nI] ) + "'"
Next nI

cQuery := "SELECT C9V.C9V_NOMEVE "
cQuery += "FROM " + RetSqlName( "C9V" ) + " C9V "
cQuery += "WHERE C9V.C9V_FILIAL IN ( " + cINFilMV + " ) "
cQuery += "  AND C9V.C9V_CPF = '" + cCPF + "' "
If !Empty( cMatric )
	cQuery += "  AND C9V.C9V_MATRIC = '" + cMatric + "' "
EndIf
cQuery += "  AND C9V.C9V_ATIVO = '1' "
cQuery += "  AND C9V.D_E_L_E_T_ = '' "

cQuery := ChangeQuery( cQuery )

TCQuery cQuery New Alias (cAlias)

( cAlias )->( DBGoTop() )

If ( cAlias )->( !Eof() )
	lRet := ( cAlias )->C9V_NOMEVE == "TAUTO"
EndIf

( cAlias )->( DBCloseArea() )

Return( lRet )

//---------------------------------------------------------------------
/*/{Protheus.doc} PreVldLine
@type			function
@description	Bloco de c�digo executado ao realizar alguma a��o na linha do oSubModel
@author			Marcelo Neumann
@since			24/09/2018
@version		1.0
@return			lRet
/*/
//---------------------------------------------------------------------
Static Function PreVldLine()

Local oModel := FWModelActive()

If oModel:GetModel("MODEL_C9L"):IsEmpty()
	oModel:LoadValue("MODEL_C9L","C9L_TRABAL"," ")
EndIf

Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} ModelOk
@type			Function
@description	Bloco de c�digo executado ao Confirmar
@author			Marcelo Neumann
@since			24/09/2018
@version		1.0
@param			oSubModel - SubModelo que chamou a fun��o
@return			lRet
/*/
//---------------------------------------------------------------------
Static Function ModelOk(oSubModel)

Local oModelPai  := FWModelActive()
Local nOperation := oModelPai:GetOperation()
Local lRet       := .T.

If nOperation == MODEL_OPERATION_INSERT .Or. nOperation == MODEL_OPERATION_UPDATE
	AddLinC9L(oModelPai)
EndIf

Return lRet

//---------------------------------------------------------------------
/*/{Protheus.doc} AddLinC9L
@type			function
@description	Adiciona uma linha em branco no model C9L quando algum modelo filho estiver prenchido
@author			Marcelo Neumann
@since			24/09/2018
@version		1.0
@param			oModel - Modelo
@return			NIL
/*/
//---------------------------------------------------------------------
Static Function AddLinC9L(oModel)

Local nT14 := 0
Local nC9K := 0
Local nC9L := 0

For nT14 := 1 to oModel:GetModel("MODEL_T14"):Length()
	oModel:GetModel("MODEL_T14"):GoLine(nT14)

	For nC9K := 1 to oModel:GetModel("MODEL_C9K"):Length()
		oModel:GetModel("MODEL_C9K"):GoLine(nC9K)

		For nC9L := 1 to oModel:GetModel("MODEL_C9L"):Length()
			oModel:GetModel("MODEL_C9L"):GoLine(nC9L)

			If oModel:GetModel("MODEL_C9L"):IsEmpty()
				If !oModel:GetModel("MODEL_C9M"):IsEmpty()
					oModel:LoadValue("MODEL_C9L","C9L_TRABAL"," ")
				EndIf
			EndIf

			If oModel:GetModel("MODEL_C9L"):IsEmpty()
				If !oModel:GetModel("MODEL_T6Y"):IsEmpty()
					oModel:LoadValue("MODEL_C9L","C9L_TRABAL"," ")
				EndIf
			EndIf

		Next nC9L
	Next nC9K
Next nT14

Return NIL

//--------------------------------------------------------------------
/*/{Protheus.doc} SetCssButton

Cria objeto TButton utilizando CSS

@author Eduardo Sukeda
@since 22/03/2019
@version 1.0

@param cTamFonte - Tamanho da Fonte
@param cFontColor - Cor da Fonte
@param cBackColor - Cor de Fundo do Bot�o
@param cBorderColor - Cor da Borda

@return cCss
/*/
//--------------------------------------------------------------------
Static Function SetCssButton(cTamFonte,cFontColor,cBackColor,cBorderColor)

Local cCSS := ""

cCSS := "QPushButton{ background-color: " + cBackColor + "; "
cCSS += "border: none; "
cCSS += "font: bold; "
cCSS += "color: " + cFontColor + ";" 
cCSS += "padding: 2px 5px;" 
cCSS += "text-align: center; "
cCSS += "text-decoration: none; "
cCSS += "display: inline-block; "
cCSS += "font-size: " + cTamFonte + "px; "
cCSS += "border: 1px solid " + cBorderColor + "; "
cCSS += "border-radius: 3px "
cCSS += "}"

Return cCSS

//---------------------------------------------------------------------
/*/{Protheus.doc} TAF250Rpt
@type			function
@description	Inicializa a vari�vel static __lGrvRPT.
@author			Victor A. Barbosa
@since			21/05/2019
/*/
//---------------------------------------------------------------------
Static Function TAF250Rpt()

__lGrvRPT := TAFAlsInDic( "V3N" )

Return()
//-------------------------------------------------------------------
/*/{Protheus.doc} TAF250Trigger
Cria��o da trigger
@author  Karyna R. M. Martins
@since   25/09/2019
@version 1
/*/
//-------------------------------------------------------------------
Static Function TAF250Trigger()

Local aAux :=   FwStruTrigger(;
			"C91_TRABAL" ,; // Campo Dominio
			"C91_CPF" ,; // Campo de Contradominio
			"C9V->C9V_CPF",; // Regra de Preenchimento
			.T. ,; // Se posicionara ou nao antes da execucao do gatilhos
			"C9V" ,; // Alias da tabela a ser posicionada
			2 ,; // Ordem da tabela a ser posicionada
			"xFilial('C9V') + M->C91_TRABAL + '1'" ,; // Chave de busca da tabela a ser posicionada
			"!Empty(M->C91_TRABAL)" ,; // Condicao para execucao do gatilho
			"01" ) // Sequencia do gatilho (usado para identificacao no caso de erro)   

Return aAux

//--------------------------------------------------------------------
/*/{Protheus.doc} TafCrrMdl

Carrega o modelo com os registro para aglutinar no novo cen�rio

@author Bruno Rosa
@since 29/11/2019
@version 1.0

@param oMdlNvCen - Modelo que contem os registros
@param oModel - Modelo que receber� os dados

/*/
//--------------------------------------------------------------------
Static Function TafCrrMdl(oMdlNvCen, oModel, cIDC91, cVersC91 )

Local nT14 := 0
Local nC9K, nC9KAdd := 0
Local nC9L, nC9LAdd  := 0
Local nC9M, nC9MAdd  := 0
Local nT6Y, nT6YAdd  := 0
Local nT6Z, nT6ZAdd  := 0
Local nC9N, nC9NAdd  := 0
Local nC9O, nC9OAdd  := 0
Local nC9P, nC9PAdd  := 0
Local nC9Q, nC9QAdd  := 0
Local nC9R, nC9RAdd  := 0
Local nCRN, nCRNAdd  := 0
Local nT6W, nT6WAdd  := 0
Local nV1B, nV1BAdd  := 0
Local nV1C, nV1CAdd  := 0

Local aGravaT14  	:= {}
Local aGravaC9L 	:= {}
Local aGravaC9M 	:= {}
Local aGravaT6Y  	:= {}
Local aGravaT6Z  	:= {}
Local aGravaC9O  	:= {}
Local aGravaC9Q  	:= {}
Local aGravaC9R  	:= {}
Local aGravaCRN  	:= {}
Local aGravaT6W  	:= {}
Local aGravaC9K  	:= {}
Local aGravaC9N  	:= {}
Local aGravaC9P  	:= {}
Local aGravaT89		:= {}
Local aGravaV1B 	:= {}
Local aGravaV1C 	:= {}


oModelT14 := oMdlNvCen:GetModel( 'MODEL_T14' )
oModelC9K := oMdlNvCen:GetModel( 'MODEL_C9K' )
oModelC9L := oMdlNvCen:GetModel( 'MODEL_C9L' )
oModelC9M := oMdlNvCen:GetModel( 'MODEL_C9M' )
oModelT6Y := oMdlNvCen:GetModel( 'MODEL_T6Y' )
oModelT6Z := oMdlNvCen:GetModel( 'MODEL_T6Z' )
oModelC9N := oMdlNvCen:GetModel( 'MODEL_C9N' )
oModelC9O := oMdlNvCen:GetModel( 'MODEL_C9O' )
oModelC9P := oMdlNvCen:GetModel( 'MODEL_C9P' )
oModelC9Q := oMdlNvCen:GetModel( 'MODEL_C9Q' )
oModelC9R := oMdlNvCen:GetModel( 'MODEL_C9R' )
oModelCRN := oMdlNvCen:GetModel( 'MODEL_CRN' )
oModelT6W := oMdlNvCen:GetModel( 'MODEL_T6W' )



For nT14 := 1 To oModelT14:Length()
	
	oModelT14:GoLine(nT14)

	If !oModelT14:IsDeleted()
		aAdd (aGravaT14 ,{oModelT14:GetValue('T14_IDEDMD'),;
						  oModelT14:GetValue('T14_CODCAT')})

		/*C9K - Informa��es do Estab/Lota��o*/
		For nC9K := 1 to oMdlNvCen:GetModel( "MODEL_C9K" ):Length()
			oMdlNvCen:GetModel( "MODEL_C9K" ):GoLine(nC9K)
			If !oMdlNvCen:GetModel( 'MODEL_C9K' ):IsEmpty()
				If !oMdlNvCen:GetModel( "MODEL_C9K" ):IsDeleted()
					aAdd (aGravaC9K ,{oModelT14:GetValue('T14_IDEDMD'),;
									  oModelC9K:GetValue('C9K_ESTABE'),;
					 				  oModelC9K:GetValue('C9K_LOTACA'),;
					 				  oModelC9K:GetValue('C9K_QTDDIA'),;
									  oModelC9K:GetValue('C9K_CODLOT'),;
									  oModelC9K:GetValue('C9K_TPINSC'),;
									  oModelC9K:GetValue('C9K_NRINSC') } )

					/*C9L - Informa��es da Remunera��o Trab.*/
					For nC9L := 1 to oMdlNvCen:GetModel( "MODEL_C9L" ):Length()
						oMdlNvCen:GetModel( "MODEL_C9L" ):GoLine(nC9L)
						If !oMdlNvCen:GetModel( 'MODEL_C9L' ):IsEmpty()
							If !oMdlNvCen:GetModel( "MODEL_C9L" ):IsDeleted()
								aAdd (aGravaC9L ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA'),;
												  oModelC9L:GetValue('C9L_TRABAL'),;
												  oModelC9L:GetValue('C9L_GRAEXP'),;
												  oModelC9L:GetValue('C9L_INDCON'),;
												  oModelC9L:GetValue('C9L_DTRABA') } )

								/*C9M - Itens da Remunera��o Trab.*/
								For nC9M := 1 to oMdlNvCen:GetModel( "MODEL_C9M" ):Length()
									oMdlNvCen:GetModel( "MODEL_C9M" ):GoLine(nC9M)
									If !oMdlNvCen:GetModel( 'MODEL_C9M' ):IsEmpty()
										If !oMdlNvCen:GetModel( "MODEL_C9M" ):IsDeleted()
											aAdd (aGravaC9M ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL'),;
															  oModelC9M:GetValue('C9M_CODRUB'),;
															  oModelC9M:GetValue('C9M_QTDRUB'),;
															  oModelC9M:GetValue('C9M_VLRUNT'),;
															  oModelC9M:GetValue('C9M_FATORR'),;
															  oModelC9M:GetValue('C9M_VLRRUB'),;
															  oModelC9M:GetValue('C9M_RUBRIC'),;
															  oModelC9M:GetValue('C9M_IDTABR') } )
										EndIf
									EndIf
								Next //nC9M

								/*T6Y - Informa��es de Valores Pagos*/
								For nT6Y := 1 to oMdlNvCen:GetModel( "MODEL_T6Y" ):Length()
									oMdlNvCen:GetModel( "MODEL_T6Y" ):GoLine(nT6Y)
									If !oMdlNvCen:GetModel( 'MODEL_T6Y' ):IsEmpty()
										If !oMdlNvCen:GetModel( "MODEL_T6Y" ):IsDeleted()
											aAdd (aGravaT6Y ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL'),;
															  oModelT6Y:GetValue('T6Y_CNPJOP'),;
															  oModelT6Y:GetValue('T6Y_REGANS'),;
															  oModelT6Y:GetValue('T6Y_VLPGTI')})

											/*T6Z - Informa��es dos Dependentes*/
											For nT6Z := 1 to oMdlNvCen:GetModel( "MODEL_T6Z" ):Length()
												oMdlNvCen:GetModel( "MODEL_T6Z" ):GoLine(nT6Z)
												If !oMdlNvCen:GetModel( 'MODEL_T6Z' ):IsEmpty()
													If !oMdlNvCen:GetModel( "MODEL_T6Z" ):IsDeleted()
														aAdd (aGravaT6Z ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL')+oModelT6Y:GetValue('T6Y_CNPJOP')+oModelT6Y:GetValue('T6Y_REGANS'),;
																		  oModelT6Z:GetValue('T6Z_SEQUEN'),;
																		  oModelT6Z:GetValue('T6Z_TPDEP') ,;
																		  oModelT6Z:GetValue('T6Z_CPFDEP'),;
																		  oModelT6Z:GetValue('T6Z_NOMDEP'),;
																		  oModelT6Z:GetValue('T6Z_DTNDEP'),;
																		  oModelT6Z:GetValue('T6Z_VPGDEP')})
													EndIf
												EndIf
											Next //nT6Z
											
										EndIf
									EndIf
								Next //nT6Y

								If TafLayESoc("02_04_02")
									/*V1B - Infos Trab Interm Per Apura��o*/
									For nV1B := 1 to oMdlNvCen:GetModel( "MODEL_V1B" ):Length()
										oMdlNvCen:GetModel( "MODEL_V1B" ):GoLine(nV1B)
										If !oMdlNvCen:GetModel( 'MODEL_V1B' ):IsEmpty()
											If !oMdlNvCen:GetModel( "MODEL_V1B" ):IsDeleted()
												aAdd (aGravaV1B ,{oModelT14:GetValue('T14_IDEDMD')+oModelC9K:GetValue('C9K_ESTABE')+oModelC9K:GetValue('C9K_LOTACA')+oModelC9L:GetValue('C9L_TRABAL'),;
																  oModelV1B:GetValue('V1B_IDCONV')})
											EndIf
										EndIf
									Next //nV1B
								EndIf

							EndIf
						EndIf
					Next //nC9L
				EndIf
			EndIf
		Next //nC9K
		
		/*C9N - Informa��es de Acordo*/
		If C9N->(MsSeek(xFilial("C9N")+cIDC91+cVersC91 ) )
			For nC9N := 1 To oMdlNvCen:GetModel( 'MODEL_C9N' ):Length()
				oMdlNvCen:GetModel( 'MODEL_C9N' ):GoLine(nC9N)
				If !oMdlNvCen:GetModel( 'MODEL_C9N' ):IsDeleted()
					aAdd (aGravaC9N ,{oModelC9N:GetValue('C9N_DTACOR')	,;
								  	  oModelC9N:GetValue('C9N_TPACOR') 	,;
								  	  oModelC9N:GetValue('C9N_COMPAC') 	,;
								  	  oModelC9N:GetValue('C9N_DTEFAC')  ,;
								  	  oModelC9N:GetValue('C9N_DSC')		,;
								  	  oModelC9N:GetValue('C9N_REMUNS')})

					/*C9O - Informa��es do Periodo*/
					For nC9O := 1 to oMdlNvCen:GetModel( "MODEL_C9O" ):Length()
						oMdlNvCen:GetModel( "MODEL_C9O" ):GoLine(nC9O)
						If !oMdlNvCen:GetModel( 'MODEL_C9O' ):IsEmpty()
							If !oMdlNvCen:GetModel( "MODEL_C9O" ):IsDeleted()
								aAdd (aGravaC9O ,{oModelT14:GetValue('T14_IDEDMD')+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR'),;
												  oModelC9O:GetValue('C9O_PERREF')})

								/*C9P - Informa��es do Estab/Lota��o*/
								For nC9P := 1 to oMdlNvCen:GetModel( "MODEL_C9P" ):Length()
									oMdlNvCen:GetModel( "MODEL_C9P" ):GoLine(nC9P)
									If !oMdlNvCen:GetModel( 'MODEL_C9P' ):IsEmpty()
										If !oMdlNvCen:GetModel( 'MODEL_C9P' ):IsDeleted()
											aAdd (aGravaC9P ,{oModelT14:GetValue('T14_IDEDMD')+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF'),;
															  oModelC9P:GetValue('C9P_ESTABE'),;
															  oModelC9P:GetValue('C9P_LOTACA')})

											/*C9Q - Informa��es da Remunera��o Trab.*/
											For nC9Q := 1 to oMdlNvCen:GetModel( "MODEL_C9Q" ):Length()
												oMdlNvCen:GetModel( "MODEL_C9Q" ):GoLine(nC9Q)
												If !oMdlNvCen:GetModel( 'MODEL_C9Q' ):IsEmpty()
													If !oMdlNvCen:GetModel( "MODEL_C9Q" ):IsDeleted()
														aAdd (aGravaC9Q ,{oModelT14:GetValue('T14_IDEDMD')+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF')+oModelC9P:GetValue('C9P_ESTABE')+oModelC9P:GetValue('C9P_LOTACA'),;
																		  oModelC9Q:GetValue('C9Q_TRABAL')	,;
																		  oModelC9Q:GetValue('C9Q_GRAEXP')	,;
																		  oModelC9Q:GetValue('C9Q_INDCON')  })

														/*C9R - Itens da Remunera��o Trab.*/
														For nC9R := 1 to oMdlNvCen:GetModel( "MODEL_C9R" ):Length()
															oMdlNvCen:GetModel( "MODEL_C9R" ):GoLine(nC9R)
															If !oMdlNvCen:GetModel( 'MODEL_C9R' ):IsEmpty()
																If !oMdlNvCen:GetModel( "MODEL_C9R" ):IsDeleted()
																	aAdd (aGravaC9R ,{oModelT14:GetValue('T14_IDEDMD')+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF')+oModelC9P:GetValue('C9P_ESTABE')+oModelC9P:GetValue('C9P_LOTACA')+oModelC9Q:GetValue('C9Q_TRABAL'),;
																					  oModelC9R:GetValue('C9R_CODRUB'),;
																					  oModelC9R:GetValue('C9R_QTDRUB'),;
																					  oModelC9R:GetValue('C9R_VLRUNT'),;
																					  oModelC9R:GetValue('C9R_FATORR'),;
																					  oModelC9R:GetValue('C9R_VLRRUB')})
																EndIf
															EndIf
														Next //nC9R
														
														If TafLayESoc("02_04_02")
															/*V1C - Infos Trab Interm Per Anterior*/
															For nV1C := 1 to oMdlNvCen:GetModel( "MODEL_V1C" ):Length()
																oMdlNvCen:GetModel( "MODEL_V1C" ):GoLine(nV1C)
																If !oMdlNvCen:GetModel( 'MODEL_V1C' ):IsEmpty()
																	If !oMdlNvCen:GetModel( "MODEL_V1C" ):IsDeleted()
																		aAdd (aGravaV1C ,{oModelT14:GetValue('T14_IDEDMD')+Dtoc(oModelC9N:GetValue('C9N_DTACOR'))+oModelC9N:GetValue('C9N_TPACOR')+oModelC9O:GetValue('C9O_PERREF')+oModelC9P:GetValue('C9P_ESTABE')+oModelC9P:GetValue('C9P_LOTACA')+oModelC9Q:GetValue('C9Q_TRABAL'),;
																						  oModelV1C:GetValue('V1C_IDCONV')})
																	EndIf
																EndIf
															Next //nV1C
														Endif
													EndIf
												EndIf
											Next //nC9Q
										EndIf
									EndIf
								Next //nC9P
							EndIf
						EndIf
					Next //nC9O
				EndIf
			Next //nC9N
		EndIf
	EndIf
Next //nT14

/*CRN - Proc. Judiciario Remunera��o  */
For nCRN := 1 To oMdlNvCen:GetModel( 'MODEL_CRN' ):Length()
	oMdlNvCen:GetModel( 'MODEL_CRN' ):GoLine(nCRN)

	If !oMdlNvCen:GetModel( 'MODEL_CRN' ):IsDeleted()
		aAdd (aGravaCRN,{oModelCRN:GetValue('CRN_TPTRIB')	,;
						 oModelCRN:GetValue('CRN_IDPROC')	,;
						 oModelCRN:GetValue('CRN_IDSUSP')	})
	EndIf
Next

/*T6W - Remunera��o Outras Empresas   */
For nT6W := 1 To oMdlNvCen:GetModel( 'MODEL_T6W' ):Length()
	oMdlNvCen:GetModel( 'MODEL_T6W' ):GoLine(nT6W)

	If !oMdlNvCen:GetModel( 'MODEL_T6W' ):IsDeleted()
		aAdd (aGravaT6W ,{oModelT6W:GetValue('T6W_TPINSC')	,;
						  oModelT6W:GetValue('T6W_NRINSC')	,;
						  oModelT6W:GetValue('T6W_CODCAT')	,;
						  oModelT6W:GetValue('T6W_VLREMU')	})
	EndIf
Next

//----------------------------------------------------------------------

//Grava��o no modelo original
/*T14 - Informa��es do Recibo de Pag.*/
For nT14 := 1 to Len( aGravaT14 )

	oModel:GetModel( 'MODEL_T14' ):LVALID	:= .T.

	If nT14 > 1
		oModel:GetModel( "MODEL_T14" ):AddLine()
	EndIf

	oModel:LoadValue( "MODEL_T14", "T14_IDEDMD", aGravaT14[nT14][1] )
	oModel:LoadValue( "MODEL_T14", "T14_CODCAT", aGravaT14[nT14][2] )

	/*C9K - Informa��es do Estab/Lota��o*/
	nC9KAdd := 1
	For nC9K := 1 to Len( aGravaC9K )
	
		If  aGravaC9K[nC9K][1] == aGravaT14[nT14][1]
			oModel:GetModel( 'MODEL_C9K' ):LVALID := .T.
			
			If nC9KAdd > 1
				oModel:GetModel( "MODEL_C9K" ):AddLine()
			EndIf
			
			oModel:LoadValue( "MODEL_C9K", "C9K_ESTABE", aGravaC9K[nC9K][2] )
			oModel:LoadValue( "MODEL_C9K", "C9K_LOTACA", aGravaC9K[nC9K][3] )
			oModel:LoadValue( "MODEL_C9K", "C9K_QTDDIA", aGravaC9K[nC9K][4] )
			oModel:LoadValue( "MODEL_C9K", "C9K_CODLOT", aGravaC9K[nC9K][5] )
			oModel:LoadValue( "MODEL_C9K", "C9K_TPINSC", aGravaC9K[nC9K][6] )
			oModel:LoadValue( "MODEL_C9K", "C9K_NRINSC", aGravaC9K[nC9K][7] )
			
			/*C9L - Informa��es da Remunera��o Trab.*/
			nC9LAdd := 1
			For nC9L := 1 to Len( aGravaC9L )
				If aGravaC9L[nC9L][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3])
					oModel:GetModel( 'MODEL_C9L' ):LVALID := .T.
					
					If nC9LAdd > 1
						oModel:GetModel( "MODEL_C9L" ):AddLine()
					EndIf
					
					oModel:LoadValue( "MODEL_C9L", "C9L_TRABAL", aGravaC9L[nC9L][2] )
					oModel:LoadValue( "MODEL_C9L", "C9L_GRAEXP", aGravaC9L[nC9L][3] )
					oModel:LoadValue( "MODEL_C9L", "C9L_INDCON", aGravaC9L[nC9L][4] )
					oModel:LoadValue( "MODEL_C9L", "C9L_DTRABA", aGravaC9L[nC9L][5] )
					
					/*C9M - Itens da Remunera��o Trab.*/
					nC9MAdd := 1
					For nC9M := 1 to Len( aGravaC9M )
						If aGravaC9M[nC9M][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2])
							oModel:GetModel( 'MODEL_C9M' ):LVALID := .T.
							
							If nC9MAdd > 1
								oModel:GetModel( "MODEL_C9M" ):AddLine()
							EndIf
							
							oModel:LoadValue( "MODEL_C9M", "C9M_CODRUB",	aGravaC9M[nC9M][2] )
							oModel:LoadValue( "MODEL_C9M", "C9M_QTDRUB",	aGravaC9M[nC9M][3] )
							oModel:LoadValue( "MODEL_C9M", "C9M_VLRUNT",	aGravaC9M[nC9M][4] )
							oModel:LoadValue( "MODEL_C9M", "C9M_FATORR",	aGravaC9M[nC9M][5] )
							oModel:LoadValue( "MODEL_C9M", "C9M_VLRRUB",	aGravaC9M[nC9M][6] )
							oModel:LoadValue( "MODEL_C9M", "C9M_RUBRIC",	aGravaC9M[nC9M][7] )
							oModel:LoadValue( "MODEL_C9M", "C9M_IDTABR",	aGravaC9M[nC9M][8] )
							
							nC9MAdd++
							
						EndIf
					Next //nC9M
					
					/*T6Y - Informa��es de Valores Pagos*/
					nT6YAdd := 1
					For nT6Y := 1 to Len( aGravaT6Y )
						If aGravaT6Y[nT6Y][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2])
							oModel:GetModel( 'MODEL_T6Y' ):LVALID := .T.
							
							If nT6YAdd > 1
								oModel:GetModel( "MODEL_T6Y" ):AddLine()
							EndIf
							
							oModel:LoadValue( "MODEL_T6Y", "T6Y_CNPJOP",	aGravaT6Y[nT6Y][2] )
							oModel:LoadValue( "MODEL_T6Y", "T6Y_REGANS",	aGravaT6Y[nT6Y][3] )
							oModel:LoadValue( "MODEL_T6Y", "T6Y_VLPGTI",	aGravaT6Y[nT6Y][4] )
							oModel:LoadValue( "MODEL_T6Y", "T6Y_NOMEVE",	"S1200" )
							
							/*T6Z - Informa��es dos Dependentes*/
							nT6ZAdd := 1
							For nT6Z := 1 to Len( aGravaT6Z )
								If aGravaT6Z[nT6Z][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2]+aGravaT6Y[nT6Y][2]+aGravaT6Y[nT6Y][3])
									oModel:GetModel( 'MODEL_T6Z' ):LVALID := .T.
									
									If nT6ZAdd > 1
										oModel:GetModel( "MODEL_T6Z" ):AddLine()
									EndIf
									
									oModel:LoadValue( "MODEL_T6Z", "T6Z_SEQUEN",	aGravaT6Z[nT6Z][2] )
									oModel:LoadValue( "MODEL_T6Z", "T6Z_TPDEP",		aGravaT6Z[nT6Z][3] )
									oModel:LoadValue( "MODEL_T6Z", "T6Z_CPFDEP",	aGravaT6Z[nT6Z][4] )
									oModel:LoadValue( "MODEL_T6Z", "T6Z_NOMDEP",	aGravaT6Z[nT6Z][5] )
									oModel:LoadValue( "MODEL_T6Z", "T6Z_DTNDEP",	aGravaT6Z[nT6Z][6] )
									oModel:LoadValue( "MODEL_T6Z", "T6Z_VPGDEP",	aGravaT6Z[nT6Z][7] )
									oModel:LoadValue( "MODEL_T6Z", "T6Z_NOMEVE",	"S1200" )
									
									nT6ZAdd++
								EndIf
							Next //nT6Z
							nT6YAdd++
						EndIf
					Next //nT6Y
					
					
					If TafLayESoc("02_04_02")
					/*V1B - Infos Trab Interm Per Apura��o*/
						nV1BAdd := 1
						For nV1B := 1 to Len( aGravaV1B )
							If aGravaV1B[nV1B][1] == (aGravaT14[nT14][1]+aGravaC9K[nT14][2]+aGravaC9K[nT14][3]+aGravaC9L[nC9L][2])
								oModel:GetModel( 'MODEL_V1B' ):LVALID := .T.
								
								If nV1BAdd > 1
									oModel:GetModel( "MODEL_V1B" ):AddLine()
								EndIf
								
								oModel:LoadValue( "MODEL_V1B", "V1B_IDCONV",	aGravaV1B[nV1B][2] )
								
								nV1BAdd++
								
							EndIf
						Next //nV1B
					EndIf
					
					nC9LAdd++
				EndIf
			Next //nC9L
			nC9KAdd++
		EndIf
	Next //nC9K
Next //nT14

/*C9N - Informa��es de Acordo*/
For nC9N := 1 to Len( aGravaC9N )
	oModel:GetModel( 'MODEL_C9N' ):LVALID	:= .T.
	
	If nC9N > 1
		oModel:GetModel( "MODEL_C9N" ):AddLine()
	EndIf
	
	oModel:LoadValue( "MODEL_C9N", "C9N_DTACOR",	aGravaC9N[nC9N][1] )
	oModel:LoadValue( "MODEL_C9N", "C9N_TPACOR",	aGravaC9N[nC9N][2] )
	oModel:LoadValue( "MODEL_C9N", "C9N_COMPAC",	aGravaC9N[nC9N][3] )
	oModel:LoadValue( "MODEL_C9N", "C9N_DTEFAC",	aGravaC9N[nC9N][4] )
	oModel:LoadValue( "MODEL_C9N", "C9N_DSC"   ,	aGravaC9N[nC9N][5] )
	oModel:LoadValue( "MODEL_C9N", "C9N_REMUNS",	aGravaC9N[nC9N][6] )
	
	For nC9O := 1 to Len( aGravaC9O )
		If  aGravaC9O[nC9O][1] == (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2])
			oModel:GetModel( 'MODEL_C9O' ):LVALID := .T.
			
			If nC9OAdd > 1
				oModel:GetModel( "MODEL_C9O" ):AddLine()
			EndIf
			
			oModel:LoadValue( "MODEL_C9O", "C9O_PERREF", aGravaC9O[nC9O][2] )
			
			/*C9P - Informa��es do Estab/Lota��o*/
			nC9PAdd := 1
			For nC9P := 1 to Len( aGravaC9P )
				If aGravaC9P[nC9P][1] == (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2])
					oModel:GetModel( 'MODEL_C9P' ):LVALID := .T.
					
					If nC9PAdd > 1
						oModel:GetModel( "MODEL_C9P" ):AddLine()
					EndIf
					
					oModel:LoadValue( "MODEL_C9P", "C9P_ESTABE",	aGravaC9P[nC9P][2] )
					oModel:LoadValue( "MODEL_C9P", "C9P_LOTACA",	aGravaC9P[nC9P][3] )
					
					/*C9Q - Informa��es da Remunera��o Trab.*/
					nC9QAdd := 1
					For nC9Q := 1 to Len( aGravaC9Q )
						If  aGravaC9Q[nC9Q][1] == (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2]+aGravaC9P[nC9P][2]+aGravaC9P[nC9P][3])
							oModel:GetModel( 'MODEL_C9Q' ):LVALID := .T.
							
							If nC9QAdd > 1
								oModel:GetModel( "MODEL_C9Q" ):AddLine()
							EndIf
							
							oModel:LoadValue( "MODEL_C9Q", "C9Q_TRABAL",	aGravaC9Q[nC9Q][2] )
							oModel:LoadValue( "MODEL_C9Q", "C9Q_GRAEXP",	aGravaC9Q[nC9Q][3] )
							oModel:LoadValue( "MODEL_C9Q", "C9Q_INDCON",    aGravaC9Q[nC9Q][4] )
							
							/*C9R - Itens da Remunera��o Trab.*/
							nC9RAdd := 1
							For nC9R := 1 to Len( aGravaC9R )
								If  aGravaC9R[nC9R][1] == (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2]+aGravaC9P[nC9P][2]+aGravaC9P[nC9P][3]+aGravaC9Q[nC9Q][2])
									oModel:GetModel( 'MODEL_C9R' ):LVALID := .T.
									
									If nC9RAdd > 1
										oModel:GetModel( "MODEL_C9R" ):AddLine()
									EndIf
									
									oModel:LoadValue( "MODEL_C9R", "C9R_CODRUB",	aGravaC9R[nC9R][2] )
									oModel:LoadValue( "MODEL_C9R", "C9R_QTDRUB",	aGravaC9R[nC9R][3] )
									oModel:LoadValue( "MODEL_C9R", "C9R_VLRUNT",	aGravaC9R[nC9R][4] )
									oModel:LoadValue( "MODEL_C9R", "C9R_FATORR",	aGravaC9R[nC9R][5] )
									oModel:LoadValue( "MODEL_C9R", "C9R_VLRRUB",	aGravaC9R[nC9R][6] )
									
									nC9RAdd++
									
								EndIf
							Next //nC9R
							
							If TafLayESoc("02_04_02")
								/*V1C - Infos Trab Interm Per Anterior*/
								nV1CAdd := 1
								For nV1C := 1 to Len( aGravaV1C )
									If  aGravaV1C[nV1C][1] == (Dtoc(aGravaC9N[nC9N][1])+aGravaC9N[nC9N][2]+aGravaC9O[nC9O][2]+aGravaC9P[nC9P][2]+aGravaC9P[nC9P][3]+aGravaC9Q[nC9Q][2])
										oModel:GetModel( 'MODEL_V1C' ):LVALID := .T.
										
										If nV1CAdd > 1
											oModel:GetModel( "MODEL_V1C" ):AddLine()
										EndIf
										
										oModel:LoadValue( "MODEL_V1C", "V1C_IDCONV",	aGravaV1C[nV1C][2] )
										
										nV1CAdd++
									EndIf
								Next //nV1C
							Endif
							
							nC9QAdd++
						EndIf
					Next //nC9Q
				EndIf
			Next //nC9P
			nC9OAdd++
		EndIf
	Next //nC9O
Next //nC9N

For nCRN := 1 To Len( aGravaCRN )
	If nCRN > 1
		oModel:GetModel( 'MODEL_CRN' ):AddLine()
	EndIf
	oModel:LoadValue( "MODEL_CRN", "CRN_TPTRIB" ,	aGravaCRN[nCRN][1] )
	oModel:LoadValue( "MODEL_CRN", "CRN_IDPROC" ,	aGravaCRN[nCRN][2] )
	oModel:LoadValue( "MODEL_CRN", "CRN_IDSUSP" ,	aGravaCRN[nCRN][3] )
Next

For nT6W := 1 To Len( aGravaT6W )
	If nT6W > 1
		oModel:GetModel( 'MODEL_T6W' ):AddLine()
	EndIf
	oModel:LoadValue( "MODEL_T6W", "T6W_TPINSC",	aGravaT6W[nT6W][1] )
	oModel:LoadValue( "MODEL_T6W", "T6W_NRINSC",	aGravaT6W[nT6W][2] )
	oModel:LoadValue( "MODEL_T6W", "T6W_CODCAT",	aGravaT6W[nT6W][3] )
	oModel:LoadValue( "MODEL_T6W", "T6W_VLREMU",	aGravaT6W[nT6W][4] )
	oModel:LoadValue( "MODEL_T6W", "T6W_NOMEVE",	"S1200" )
Next

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF250AUTI
Monta a View din�mica para trabalhadores aut�nomos
@author  Ricardo Lovrenovic Bueno
@since   27/01/2020
@version 1
/*/
//-------------------------------------------------------------------
Function TAF250AUTI( cAlias, nRecno )

Local oNewView	:= ViewDef()
Local aArea 	:= GetArea()
Local oExecView	:= Nil

DbSelectArea( cAlias )
(cAlias)->( DbGoTo( nRecno ) )

oExecView := FWViewExec():New()
oExecView:setOperation( 3 )
oExecView:setTitle( "Cadastro de Folha de Pagamento Aut�nomos" )
oExecView:setOK( {|| .T. } )
oExecView:setModal(.F.)
oExecView:setView( oNewView )
oExecView:openView(.T.)

RestArea( aArea )

Return( 3 )

//---------------------------------------------------------------------
/*/{Protheus.doc} LoadReportData
@type			function
@description	Inicializa a vari�vel static __lGrvRPT
@author			Diego Santos
@since			22/05/2019
@version		1.0
/*/
//---------------------------------------------------------------------

Static Function LoadReportData(oModel, lInfoRPT)

Local aSaveLines := FwSaveRows(oModel)
Local aArea := GetArea()

Local oModelT14 := oModel:GetModel("MODEL_T14")

Local nT14	:= 1

Local nC9K	:= 1
Local nC9L	:= 1
Local nC9M	:= 1

Local nC9N	:= 1
Local nC9O	:= 1
Local nC9P	:= 1
Local nC9Q	:= 1
Local nC9R	:= 1

Local nPosValores := 1

Local lMv := oModel:GetValue("MODEL_C91","C91_MV" ) == "1"

Local aAnalitico := {}
Local aRubrica := {}

Local cMatric 		:= ""
Local cCodCat 		:= ""
Local cCodRubr 		:= ""
Local cIDTabRubr 	:= ""
Local cTipoEstab	:= ""
Local cEstab 		:= ""
Local cLotacao 		:= ""

For nT14 := 1 To oModelT14:Length()
	oModelT14:GoLine( nT14 )
	If !oModelT14:IsEmpty() .And. !oModelT14:IsDeleted()

		cCodCat := AllTrim(POSICIONE("C87",1, xFilial("C87")+oModelT14:Getvalue("T14_CODCAT"),"C87_CODIGO"))

		//Parte 1 - Come�o
		oModelC9K := oModel:GetModel("MODEL_C9K")
		For nC9K := 1 To oModelC9K:Length()
			oModelC9K:GoLine( nC9K )
			If !oModelC9K:IsEmpty() .And. !oModelC9K:IsDeleted()

				If !Empty( oModelC9K:GetValue("C9K_TPINSC") ) .and. !Empty( oModelC9K:GetValue("C9K_NRINSC") ) .and. !Empty( oModelC9K:GetValue("C9K_CODLOT") )
					cTipoEstab	:=	oModelC9K:GetValue("C9K_TPINSC")
					cEstab		:=	oModelC9K:GetValue("C9K_NRINSC")
					cLotacao	:=	oModelC9K:GetValue("C9K_CODLOT")
				Else
					cTipoEstab	:=	Posicione( "C92", 1, xFilial( "C92" ) + oModelC9K:GetValue("C9K_ESTABE"), "C92_TPINSC" )
					cEstab		:=	Posicione( "C92", 1, xFilial( "C92" ) + oModelC9K:GetValue("C9K_ESTABE"), "C92_NRINSC" )
					cLotacao	:=	Posicione( "C99", 1, xFilial( "C99" ) + oModelC9K:GetValue("C9K_LOTACA"), "C99_CODIGO" )
				EndIf

				oModelC9L := oModel:GetModel("MODEL_C9L")
				For nC9L := 1 To oModelC9L:Length()
					oModelC9L:GoLine( nC9L )
					If !oModelC9L:IsEmpty() .And. !oModelC9L:IsDeleted()
						If Empty(oModelC9L:GetValue("C9L_DTRABA") )
							cMatric := Posicione( "C9V", 1, xFilial( "C9V" ) + oModelC9L:GetValue("C9L_TRABAL"), "C9V_MATRIC" )									
						Else
							cMatric := oModelC9L:GetValue("C9L_DTRABA")
						EndIf

						oModelC9M := oModel:GetModel("MODEL_C9M")
						For nC9M := 1 To oModelC9M:Length()
							oModelC9M:GoLine( nC9M )
							If !oModelC9M:IsEmpty() .And. !oModelC9M:IsDeleted()
								If lInfoRPT

									cIDTabRubr := Posicione( "C8R", 5, xFilial( "C8R" ) + oModelC9M:GetValue("C9M_CODRUB") + "1", "C8R_IDTBRU" )

									If !Empty( oModelC9M:GetValue("C9M_CODRUB") ) .and. !Empty( cIDTabRubr )
										cCodRubr := oModelC9M:GetValue("C9M_CODRUB")
									Else
										cCodRubr	:=	oModelC9M:GetValue("C9M_RUBRIC")
										cIDTabRubr	:=	oModelC9M:GetValue("C9M_IDTABR")
									EndIf

									aRubrica := oReport:GetRubrica( cCodRubr, cIDTabRubr, oModel:GetValue("MODEL_C91","C91_PERAPU" ), lMV )

									aAdd( aAnalitico, Array( 13 ) )
									nPosValores := Len( aAnalitico )

									aAnalitico[nPosValores][ANALITICO_MATRICULA]			:=	AllTrim( cMatric )
									aAnalitico[nPosValores][ANALITICO_CATEGORIA]			:=	AllTrim( cCodCat )
									aAnalitico[nPosValores][ANALITICO_TIPO_ESTABELECIMENTO]	:=	AllTrim( cTipoEstab )
									aAnalitico[nPosValores][ANALITICO_ESTABELECIMENTO]		:=	AllTrim( cEstab )
									aAnalitico[nPosValores][ANALITICO_LOTACAO]				:=	AllTrim( cLotacao )
									aAnalitico[nPosValores][ANALITICO_NATUREZA]				:=	AllTrim( aRubrica[1] ) //Natureza
									aAnalitico[nPosValores][ANALITICO_TIPO_RUBRICA]			:=	AllTrim( aRubrica[2] ) //Tipo
									aAnalitico[nPosValores][ANALITICO_INCIDENCIA_CP]		:=	AllTrim( aRubrica[3] ) //Incid�ncia CP
									aAnalitico[nPosValores][ANALITICO_INCIDENCIA_IRRF]		:=	AllTrim( aRubrica[4] ) //Incid�ncia IRRF
									aAnalitico[nPosValores][ANALITICO_INCIDENCIA_FGTS]		:=	AllTrim( aRubrica[5] ) //Incid�ncia FGTS
									aAnalitico[nPosValores][ANALITICO_DECIMO_TERCEIRO]		:=	""
									aAnalitico[nPosValores][ANALITICO_TIPO_VALOR]			:=	""
									aAnalitico[nPosValores][ANALITICO_VALOR]				:=	oModelC9M:GetValue("C9M_VLRRUB" )
								EndIf								
							EndIf
						Next nC9M
					EndIf
				Next nC9L
			EndIf
		Next nC9K
		//Parte 1 - Fim

		//Parte 2 - Come�o
		oModelC9N := oModel:GetModel("MODEL_C9N")
		For nC9N := 1 To oModelC9N:Length()
			oModelC9N:GoLine(nC9N)
			If !oModelC9N:IsEmpty() .And. !oModelC9N:IsDeleted()
				oModelC9O := oModel:GetModel("MODEL_C9O")
				For nC9O := 1 To oModelC9O:Length()
					oModelC9O:GoLine( nC9O )
					If !oModelC9O:IsEmpty() .And. !oModelC9O:IsDeleted()
						oModelC9P := oModel:GetModel("MODEL_C9P")
						For nC9P := 1 To oModelC9P:Length()
							oModelC9P:GoLine( nC9P )
							If !oModelC9P:IsEmpty() .And. !oModelC9P:IsDeleted()
								If !lMV .Or. Empty(oModelC9P:GetValue("C9P_CODLOT"))
									cTipoEstab	:=	Posicione( "C92", 1, xFilial( "C92" ) + oModelC9P:GetValue("C9P_ESTABE"), "C92_TPINSC" )
									cEstab		:=	Posicione( "C92", 1, xFilial( "C92" ) + oModelC9P:GetValue("C9P_ESTABE"), "C92_NRINSC" )
									cLotacao	:=	Posicione( "C99", 1, xFilial( "C99" ) + oModelC9P:GetValue("C9P_LOTACA"), "C99_CODIGO" )
								Else
									cTipoEstab	:=	oModelC9P:GetValue("C9P_TPINSC")
									cEstab		:=	oModelC9P:GetValue("C9P_NRINSC")
									cLotacao	:=	oModelC9P:GetValue("C9P_CODLOT")
								EndIf							
								oModelC9Q := oModel:GetModel("MODEL_C9Q")
								For nC9Q := 1 To oModelC9Q:Length()
									oModelC9Q:GoLine( nC9Q )
									If !oModelC9Q:IsEmpty() .And. !oModelC9Q:IsDeleted()

										If !lMV .Or. Empty(oModelC9Q:GetValue("C9Q_DTRABA"))
											cMatric := POSICIONE("C9V",1, xFilial("C9V")+oModelC9Q:GetValue("C9Q_TRABAL"),"C9V_MATRIC")
										Else
											cMatric := oModelC9Q:GetValue("C9Q_DTRABA")
										EndIf

										oModelC9R := oModel:GetModel("MODEL_C9R")
										For nC9R := 1 To oModelC9R:Length()
											oModelC9R:GoLine(nC9R)
											If !oModelC9R:IsEmpty() .And. !oModelC9R:IsDeleted()
												If lInfoRPT

													If lMV .And. !Empty(oModelC9R:GetValue("C9R_RUBRIC") )
														cCodRubr	:=	oModelC9R:GetValue("C9R_RUBRIC")
														cIDTabRubr	:=	oModelC9R:GetValue("C9R_IDTABR")
													Else
														cCodRubr	:=	oModelC9R:GetValue("C9R_CODRUB")
														cIDTabRubr	:=	Posicione( "C8R", 5, xFilial( "C8R" ) + cCodRubr + "1", "C8R_IDTBRU" )
													EndIf

													aRubrica := oReport:GetRubrica( cCodRubr, cIDTabRubr, oModel:GetValue("MODEL_C91","C91_PERAPU" ), lMV )

													aAdd( aAnalitico, Array( 13 ) )
													nPosValores := Len( aAnalitico )

													aAnalitico[nPosValores][ANALITICO_MATRICULA]			:=	AllTrim( cMatric )
													aAnalitico[nPosValores][ANALITICO_CATEGORIA]			:=	AllTrim( cCodCat )
													aAnalitico[nPosValores][ANALITICO_TIPO_ESTABELECIMENTO]	:=	AllTrim( cTipoEstab )
													aAnalitico[nPosValores][ANALITICO_ESTABELECIMENTO]		:=	AllTrim( cEstab )
													aAnalitico[nPosValores][ANALITICO_LOTACAO]				:=	AllTrim( cLotacao )
													aAnalitico[nPosValores][ANALITICO_NATUREZA]				:=	AllTrim( aRubrica[1] ) //Natureza
													aAnalitico[nPosValores][ANALITICO_TIPO_RUBRICA]			:=	AllTrim( aRubrica[2] ) //Tipo
													aAnalitico[nPosValores][ANALITICO_INCIDENCIA_CP]		:=	AllTrim( aRubrica[3] ) //Incid�ncia CP
													aAnalitico[nPosValores][ANALITICO_INCIDENCIA_IRRF]		:=	AllTrim( aRubrica[4] ) //Incid�ncia IRRF
													aAnalitico[nPosValores][ANALITICO_INCIDENCIA_FGTS]		:=	AllTrim( aRubrica[5] ) //Incid�ncia FGTS
													aAnalitico[nPosValores][ANALITICO_DECIMO_TERCEIRO]		:=	""
													aAnalitico[nPosValores][ANALITICO_TIPO_VALOR]			:=	""
													aAnalitico[nPosValores][ANALITICO_VALOR]				:=	oModelC9R:GetValue("C9R_VLRRUB")
												EndIf
											EndIf
										Next nC9R
									EndIf
								Next nC9Q
							EndIf
						Next nC9P
					EndIf
				Next nC9O
			EndIf
		Next nC9N
		//Parte 2 - Fim
	EndIf
Next nT06

RestArea(aArea)
FWRestRows( aSaveLines )

Return aAnalitico
