#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TAFA253.CH'

//----------------------------------------------------------------------
/*/{Protheus.doc} TAFA253
Cadastro MVC para atender o registro S - 1005 (Tabela de Estabelecimentos, Obras ou Unidades de �rg�os P�blicos) do e-Social.

@author Leandro Prado 
@since 26/08/2013
@version 1.0
/*/ 
//--------------------------------------------------------------------
Function TAFA253()

Private oBrw := FWmBrowse():New()

// Fun��o que indica se o ambiente � v�lido para o eSocial 2.3
If TafAtualizado() 

	oBrw:SetDescription( STR0001 ) //Tabela de Estabelecimentos
	oBrw:SetAlias( 'C92' )
	oBrw:SetMenuDef( 'TAFA253' )
	oBrw:SetFilterDefault( "C92_ATIVO == '1' .Or. (C92_EVENTO == 'E' .And. C92_STATUS = '4' .And. C92_ATIVO = '2')" ) //Filtro para que apenas os registros ativos sejam exibidos ( 1=Ativo, 2=Inativo )

	oBrw:AddLegend( "C92_EVENTO == 'I' ", "GREEN" , STR0006 ) //"Registro Inclu�do"
	oBrw:AddLegend( "C92_EVENTO == 'A' ", "YELLOW", STR0007 ) //"Registro Alterado"
	oBrw:AddLegend( "C92_EVENTO == 'E' .And. C92_STATUS <> '4' ", "RED"   , STR0008 ) //"Registro exclu�do n�o transmitido"
	oBrw:AddLegend( "C92_EVENTO == 'E' .And. C92_STATUS == '4' .And. C92_ATIVO = '2' ", "BLACK"   , STR0014 ) //"Registro exclu�do transmitido"
	
	oBrw:Activate()

EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc}MenuDef
Funcao generica MVC com as opcoes de menu

@author Leandro Prado
@since 26/08/2013
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao
Local aRotina

aFuncao := {}
aRotina := {}

If FindFunction('TafXmlRet')
	Aadd( aFuncao, { "" , "TafxmlRet('TAF253Xml','1005','C92')" , "1" } )
Else
	Aadd( aFuncao, { "" , "TAF253Xml" , "1" } )
EndIf
Aadd( aFuncao, { "" , "Taf253Vld" , "2" } )
Aadd( aFuncao, { "" , "xFunHisAlt( 'C92', 'TAFA253',,,,'TAF253XML','1005'  )" , "3" } )
aAdd( aFuncao, { "" , "TAFXmlLote( 'C92', 'S-1005' , 'evtTabEstab' , 'TAF253Xml',, oBrw )" , "5" } )
Aadd( aFuncao, { "" , "xFunAltRec( 'C92' )" , "10" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If lMenuDif
	ADD OPTION aRotina Title STR0009 Action 'VIEWDEF.TAFA253' OPERATION 2 ACCESS 0 //"Visualizar"
Else
	aRotina	:=	xFunMnuTAF( "TAFA253" , , aFuncao ) 
EndIf

Return( aRotina )
//-------------------------------------------------------------------
/*/{Protheus.doc}  ModelDef
Funcao generica MVC do model

@author Leandro Prado
@since 26/08/2013
@version 1.0
/*/
//-------------------------------------------------------------------     
Static Function ModelDef()	

Local oStruC92	
Local oStruT0Z	
Local oModel	

oStruC92	:= FWFormStruct( 1, 'C92' )// Cria a estrutura a ser usada no Modelo de Dados
oStruT0Z	:= FWFormStruct( 1, 'T0Z' )
oModel		:= MPFormModel():New("TAFA235",,,{|oModel| SaveModel(oModel)})

lVldModel := Iif( Type( "lVldModel" ) == "U", .F., lVldModel )

If lVldModel
	oStruC92:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
EndIf

// Adiciona ao modelo um componente de formul�rio
oModel:AddFields( 'MODEL_C92', /*cOwner*/, oStruC92)
oModel:GetModel( 'MODEL_C92' ):SetPrimaryKey({ 'C92_TPINSC' , 'C92_NRINSC' , 'C92_DTINI' , 'C92_DTFIN' })

oModel:AddGrid( "MODEL_T0Z", "MODEL_C92", oStruT0Z )
oModel:GetModel( "MODEL_T0Z" ):SetOptional( .T. )
oModel:GetModel( "MODEL_T0Z" ):SetUniqueLine( { "T0Z_CNPJEE"} )

oModel:SetRelation( "MODEL_T0Z",{ { "T0Z_FILIAL", "xFilial('T0Z')" }, { "T0Z_ID", "C92_ID" }, { "T0Z_VERSAO", "C92_VERSAO" } },T0Z->( IndexKey( 1 ) ) )


Return oModel             
//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@author Leandro Prado
@since 26/08/2013
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oModel   	

Local oStruC92	
Local oStruT0Z	

Local oView   	

oModel		:= FWLoadModel( 'TAFA253' )// objeto de Modelo de dados baseado no ModelDef() do fonte informado

oStruC92	:= FWFormStruct( 2, 'C92' )// Cria a estrutura a ser usada na View
oStruT0Z	:= FWFormStruct( 2, 'T0Z' )// Cria a estrutura a ser usada na View

oView		:= FWFormView():New()

oView:SetModel( oModel )

// Seto a ordem dos campos em tela
oStruC92:SetProperty( "C92_TPINSC", MVC_VIEW_ORDEM, "04" )
oStruC92:SetProperty( "C92_NRINSC", MVC_VIEW_ORDEM, "05" )
oStruC92:SetProperty( "C92_DTINI" , MVC_VIEW_ORDEM, "06" )
oStruC92:SetProperty( "C92_DTFIN" , MVC_VIEW_ORDEM, "07" )
oStruC92:SetProperty( "C92_CNAE"  , MVC_VIEW_ORDEM, "08" )
oStruC92:SetProperty( "C92_ALQRAT", MVC_VIEW_ORDEM, "09" )
oStruC92:SetProperty( "C92_FAP"   , MVC_VIEW_ORDEM, "10" )
oStruC92:SetProperty( "C92_AJURAT", MVC_VIEW_ORDEM, "11" )
oStruC92:SetProperty( "C92_PRORAT", MVC_VIEW_ORDEM, "12" )
oStruC92:SetProperty( "C92_DPRORA", MVC_VIEW_ORDEM, "12" )
oStruC92:SetProperty( "C92_CODSUR", MVC_VIEW_ORDEM, "13" )
oStruC92:SetProperty( "C92_PROFAP", MVC_VIEW_ORDEM, "14" )
oStruC92:SetProperty( "C92_DPROFA", MVC_VIEW_ORDEM, "15" )
oStruC92:SetProperty( "C92_CODSUF", MVC_VIEW_ORDEM, "16" )
oStruC92:SetProperty( "C92_TPCAEP", MVC_VIEW_ORDEM, "17" )
oStruC92:SetProperty( "C92_SUBPAT", MVC_VIEW_ORDEM, "18" )
oStruC92:SetProperty( "C92_REGPT" , MVC_VIEW_ORDEM, "19" )
oStruC92:SetProperty( "C92_CONTAP", MVC_VIEW_ORDEM, "20" )
oStruC92:SetProperty( "C92_PROCAP", MVC_VIEW_ORDEM, "21" )
oStruC92:SetProperty( "C92_DPROCA", MVC_VIEW_ORDEM, "22" )
oStruC92:SetProperty( "C92_CTENTE", MVC_VIEW_ORDEM, "23" )
oStruC92:SetProperty( "C92_CONPCD", MVC_VIEW_ORDEM, "24" )
oStruC92:SetProperty( "C92_PROCPD", MVC_VIEW_ORDEM, "25" )
oStruC92:SetProperty( "C92_DPRCPD", MVC_VIEW_ORDEM, "26" )
oStruC92:SetProperty( "C92_INDOBR", MVC_VIEW_ORDEM, "27" )


If FindFunction("TafAjustRecibo")
	TafAjustRecibo(oStruC92,"C92")
EndIf

oView:AddField( 'VIEW_C92', oStruC92, 'MODEL_C92' )
oView:EnableTitleView( 'VIEW_C92',  STR0001 ) //Tabela de Estabelecimentos

oView:AddGrid( 'VIEW_T0Z', oStruT0Z, 'MODEL_T0Z' )
oView:EnableTitleView( 'VIEW_T0Z', STR0013 ) //Identifica��o das entidades educativas

oView:CreateHorizontalBox( 'FIELDSC92', 70 )
oView:CreateHorizontalBox( 'FIELDST0Z', 30 )

oView:SetOwnerView( 'VIEW_C92', 'FIELDSC92' )
oView:SetOwnerView( 'VIEW_T0Z', 'FIELDST0Z' )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif
	xFunRmFStr(@oStruC92, 'C92')
EndIf

If TafColumnPos( "C92_LOGOPE" )
	oStruC92:RemoveField( "C92_LOGOPE")
EndIf

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@Param  oModel -> Modelo de dados

@Return .T.

@Author Felipe C. Seolin
@Since 24/09/2013
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel( oModel )

Local cVerAnt   
Local cProtocolo
Local cEvento   
Local cVersao   
Local cChvRegAnt
Local cLogOpe	
Local cLogOpeAnt
Local nOperation
Local nI        
Local nX		
Local aGrava    
Local aGravaT0Z
Local oModelC92
Local lRetorno 
Local oModelT0Z

cVerAnt    := ""
cProtocolo := ""
cEvento    := ""
cVersao    := ""
cChvRegAnt := ""
cLogOpe    := ""
cLogOpeAnt := ""

nOperation := oModel:GetOperation()
nI         := 0
nX         := 0
aGrava     := {}
oModelC92  := Nil
lRetorno   := .T.
aGravaT0Z	 := {} 
oModelT0Z	 := Nil

Begin Transaction

	If nOperation == MODEL_OPERATION_INSERT

	TafAjustID( "C92", oModel)

		oModel:LoadValue( "MODEL_C92", "C92_VERSAO", xFunGetVer() )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_C92', 'C92_LOGOPE' , '2', '' )
		Endif

		FwFormCommit( oModel )

	ElseIf nOperation == MODEL_OPERATION_UPDATE .or. nOperation == MODEL_OPERATION_DELETE

		//�����������������������������������������������������������������Ŀ
		//�Seek para posicionar no registro antes de realizar as validacoes,�
		//�visto que quando nao esta pocisionado nao eh possivel analisar   �
		//�os campos nao usados como _STATUS                                �
		//�������������������������������������������������������������������
	    C92->( DbSetOrder( 5 ) )
	    If C92->( MsSeek( xFilial( 'C92' ) + FwFldGet( "C92_ID" ) + '1' ) )
	    
			//��������������������������������Ŀ
			//�Se o registro ja foi transmitido�
			//����������������������������������
			If C92->C92_STATUS == "4" 
	
				If nOperation == MODEL_OPERATION_DELETE
					oModel:DeActivate()
					oModel:SetOperation( 4 )
					oModel:Activate()
		       EndIf
	
				oModelT0Z := oModel:GetModel("MODEL_T0Z")	
				oModelC92 := oModel:GetModel( "MODEL_C92" )
	
				//�����������������������������������������������������������Ŀ
				//�Busco a versao anterior do registro para gravacao do rastro�
				//�������������������������������������������������������������
				cVerAnt    := oModelC92:GetValue( "C92_VERSAO" )
				cProtocolo := oModelC92:GetValue( "C92_PROTUL" )
				cEvento    := oModelC92:GetValue( "C92_EVENTO" )

				If TafColumnPos( "C92_LOGOPE" )
					cLogOpeAnt := oModelC92:GetValue( "C92_LOGOPE" )
				endif

				If nOperation == MODEL_OPERATION_DELETE .And. cEvento == "E" 
					// N�o � poss�vel excluir um evento de exclus�o j� transmitido
					TAFMsgVldOp(oModel,"4")
					lRetorno := .F.
				Else
	
					//������������������������������������������������������������������Ŀ
					//�Neste momento eu gravo as informacoes que foram carregadas na tela�
					//��������������������������������������������������������������������
					For nI := 1 to Len( oModelC92:aDataModel[ 1 ] )
						aAdd( aGrava, { oModelC92:aDataModel[ 1, nI, 1 ], oModelC92:aDataModel[ 1, nI, 2 ] } )
					Next nI
					
					// --> Entidade Educativas
					For nX := 1 To oModelT0Z:Length() 
						oModelT0Z:GoLine(nX)
						If !( oModelT0Z:IsEmpty() )
							If !( oModelT0Z:IsDeleted() )
								Aadd(aGravaT0Z, oModelT0Z:GetValue("T0Z_CNPJEE") )
							EndIf
						EndIf
					Next nX					

					//�����������������������������������������������������������Ŀ
					//�Seto o campo como Inativo e gravo a versao do novo registro�
					//�no registro anterior                                       �
					//|                                                           |
					//|ATENCAO -> A alteracao destes campos deve sempre estar     |
					//|abaixo do Loop do For, pois devem substituir as informacoes|
					//|que foram armazenadas no Loop acima                        |
					//�������������������������������������������������������������
					FAltRegAnt( 'C92', '2' ,.F.,FwFldGet("C92_DTFIN"),FwFldGet("C92_DTINI"),C92->C92_DTINI )
	
					//����������������������������������������������������������������Ŀ
					//�Neste momento eu preciso setar a operacao do model como Inclusao�
					//������������������������������������������������������������������
					oModel:DeActivate()
					oModel:SetOperation( 3 )
					oModel:Activate()
	
					//�������������������������������������������������������Ŀ
					//�Neste momento eu realizo a inclusao do novo registro ja�
					//�contemplando as informacoes alteradas pelo usuario     �
					//���������������������������������������������������������
					For nI := 1 to Len( aGrava )
						oModel:LoadValue( "MODEL_C92", aGrava[ nI, 1 ], aGrava[ nI, 2 ] )
					Next nI

					//Necess�rio Abaixo do For Nao Retirar
					If Findfunction("TAFAltMan")
						TAFAltMan( 4 , 'Save' , oModel, 'MODEL_C92', 'C92_LOGOPE' , '' , cLogOpeAnt )
					EndIf

					// --> Entidade Educativas
					For nX := 1 To Len(aGravaT0Z)
						If nX > 1
							oModel:GetModel("MODEL_T0Z"):AddLine()
						EndIf
						
						oModel:LoadValue("MODEL_T0Z", "T0Z_CNPJEE", aGravaT0Z[nX] )
					Next nX					
	
					//�������������������������������Ŀ
					//�Busco a versao que sera gravada�
					//���������������������������������
					cVersao := xFunGetVer()
	
					//�����������������������������������������������������������Ŀ
					//|ATENCAO -> A alteracao destes campos deve sempre estar     |
					//|abaixo do Loop do For, pois devem substituir as informacoes|
					//|que foram armazenadas no Loop acima                        |
					//�������������������������������������������������������������
					oModel:LoadValue( "MODEL_C92", "C92_VERSAO", cVersao )
					oModel:LoadValue( "MODEL_C92", "C92_VERANT", cVerAnt )
					oModel:LoadValue( "MODEL_C92", "C92_PROTPN", cProtocolo )
					oModel:LoadValue( "MODEL_C92", "C92_PROTUL", "" )
					// Tratamento para limpar o ID unico do xml
					cAliasPai := "C92"
					If TAFColumnPos( cAliasPai+"_XMLID" )
						oModel:LoadValue( 'MODEL_'+cAliasPai, cAliasPai+'_XMLID', "" )
					EndIf
	
					If nOperation == MODEL_OPERATION_DELETE
						oModel:LoadValue( "MODEL_C92", "C92_EVENTO", "E" )
					Else
						If cEvento == "E"
							oModel:LoadValue( "MODEL_C92", "C92_EVENTO", "I" )
						Else
							oModel:LoadValue( "MODEL_C92", "C92_EVENTO", "A" )
						EndIf
					EndIf
					FwFormCommit( oModel )
				
				Endif
			
			Elseif C92->C92_STATUS == "2"
				//N�o � poss�vel alterar um registro com aguardando valida��o
				TAFMsgVldOp(oModel,"2")
				lRetorno := .F.	
				
			Else
	
				//�������������������������������������������������������������������Ŀ
				//�Caso o registro nao tenha sido transmitido ainda eu gravo sua chave�
				//���������������������������������������������������������������������
				cChvRegAnt := C92->( C92_ID + C92_VERANT )

				If TafColumnPos( "C92_LOGOPE" )
					cLogOpeAnt := C92->C92_LOGOPE
				endif

				//�����������������������������������������������������������������������������Ŀ
				//�No caso de um evento de Exclusao de um registro com status 'Excluido' deve-se�
				//�perguntar ao usuario se ele realmente deseja realizar a inclusao.            �
				//�������������������������������������������������������������������������������
				If C8V->C8V_EVENTO == "E"
					If nOperation == MODEL_OPERATION_DELETE
						If Aviso( xValStrEr("000754"), xValStrEr("000755"), { xValStrEr("000756"), xValStrEr("000757") }, 1 ) == 2 //##"Registro Exclu�do" ##"O Evento de exclus�o n�o foi transmitido. Deseja realmente exclui-lo ou manter o evento de exclus�o para transmiss�o posterior ?" ##"Excuir" ##"Manter"
								cChvRegAnt := ""
						EndIf
					Else
						oModel:LoadValue( "MODEL_C92", "C92_EVENTO", "A" )
					EndIf
				EndIf

				//Executo a operacao escolhida
				If !Empty( cChvRegAnt )
					//Funcao responsavel por setar o Status do registro para Branco
					TAFAltStat( "C92", " " )

					If nOperation == MODEL_OPERATION_UPDATE .And. Findfunction("TAFAltMan")
						TAFAltMan( 4 , 'Save' , oModel, 'MODEL_C92', 'C92_LOGOPE' , '' , cLogOpeAnt )
					EndIf

					FwFormCommit( oModel )

					//Caso a operacao seja uma exclusao
					If nOperation == MODEL_OPERATION_DELETE
						TAFRastro( "C92", 1, cChvRegAnt, .T. , , IIF(Type("oBrw") == "U", Nil, oBrw) )
					EndIf
				EndIf	
			EndIf   		
		
		Elseif TafIndexInDic("C92", '9', .T.)

			C92->( DbSetOrder( 9 ) )
	    	If C92->( MsSeek( xFilial( 'C92' ) + FwFldGet('C92_ID')+ 'E42' ) ) 

				If nOperation == MODEL_OPERATION_DELETE 
					// N�o � poss�vel excluir um evento de exclus�o j� transmitido
					TAFMsgVldOp(oModel,"4")
					lRetorno := .F.
				EndIf

			EndIF

		EndIf
	EndIf

End Transaction

Return( lRetorno )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF253Grv
@type			function
@description	Fun��o de grava��o para atender o registro S-1005 ( Tabela de Estabelecimentos, Obras ou Unidades de �rg�os P�blicos ).
@author			Felipe C. Seolin
@since			26/09/2013
@version		1.0
@param			cLayout		-	Nome do Layout que est� sendo enviado
@param			nOpc		-	Op��o a ser realizada ( 3 = Inclus�o, 4 = Altera��o, 5 = Exclus�o )
@param			cFilEv		-	Filial do ERP para onde as informa��es dever�o ser importadas
@param			oXML		-	Objeto com as informa��es a serem manutenidas ( Outras Integra��es )
@param			cOwner
@param			cFilTran
@param			cPredeces
@param			nTafRecno
@param			cComplem
@param			cGrpTran
@param			cEmpOriGrp
@param			cFilOriGrp
@param			cXmlID		-	Atributo Id, �nico para o XML do eSocial. Utilizado para importa��o de dados de clientes migrando para o TAF
@return			lRet		-	Vari�vel que indica se a importa��o foi realizada, ou seja, se as informa��es foram gravadas no banco de dados
@param			aIncons		-	Array com as inconsist�ncias encontradas durante a importa��o
/*/
//-------------------------------------------------------------------
Function TAF253Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID )

Local cTagOper		:=	""
Local cCmpsNoUpd	:=	"|C92_FILIAL|C92_ID|C92_VERSAO|C92_DTINI|C92_DTFIN|C92_VERANT|C92_PROTUL|C92_PROTPN|C92_EVENTO|C92_STATUS|C92_ATIVO|C92_CDFPAS|"
Local cCabec		:=	"/eSocial/evtTabEstab/infoEstab"
Local cValChv		:=	""
Local cNewDtIni		:=	""
Local cNewDtFin		:=	""
Local cEnter		:=	Chr( 13 ) + Chr( 10 )
Local cMensagem		:=	""
Local cInconMsg		:=	""
Local cCodEvent		:=	Posicione( "C8E", 2, xFilial( "C8E" ) + "S-" + cLayout, "C8E->C8E_ID" )
Local cChave		:=	""
Local cPerIni		:=	""
Local cPerFin		:=	""
Local cPerIniOri	:=	""
Local cLogOpeAnt	:=	""
Local nIndex		:=	2
Local nIndIDVer		:=	1
Local nI			:=	0
Local nT0Z			:=	1
Local nSeqErrGrv	:=	0
Local nTamTpInsc	:=	TamSX3( "C92_TPINSC" )[1]
Local nTamNrInsc	:=	TamSX3( "C92_NRINSC" )[1]
Local lRet			:=	.F.
Local aIncons		:=	{}
Local aRules		:=	{}
Local aChave		:=	{}
Local aNewData		:=	{ Nil, Nil }
Local oModel		:=	Nil
Local lNewValid		:= .F.

Private oDados		:=	Nil
Private lVldModel	:=	.T. //Caso a chamada seja via integra��o, seto a vari�vel de controle de valida��o como .T.

Default cLayout		:=	""
Default nOpc		:=	1
Default cFilEv		:=	""
Default oXML		:=	Nil
Default cOwner		:=	""
Default cFilTran	:=	""
Default cPredeces	:=	""
Default nTafRecno	:=	0
Default cComplem	:=	""
Default cGrpTran	:=	""
Default cEmpOriGrp	:=	""
Default cFilOriGrp	:=	""
Default cXmlID		:=	""

// Vari�vel que indica se o ambiente � v�lido para o eSocial
If !TafVldAmb("2")
	cMensagem := STR0011 + cEnter // #"Dicion�rio Incompat�vel"
	cMensagem += TafAmbInvMsg()

	Aadd(aIncons, cMensagem)

Else
	oDados := oXML
	
	If nOpc == 3
		cTagOper := "/inclusao"
	ElseIf nOpc == 4
		cTagOper := "/alteracao"
	ElseIf nOpc == 5
		cTagOper := "/exclusao"
	EndIf

	//Verificar se o tpInsc foi informado para a chave ( Obrigatorio ser informado )
	cValChv := FTafGetVal( cCabec + cTagOper + '/ideEstab/tpInsc', 'C', .F., @aIncons, .F., '', '' )
	If !Empty( cValChv )
		Aadd( aChave, { "C", "C92_TPINSC", cValChv, .T.} )
		nIndex := 6 //C92_FILIAL+ C92_TPINSC+ C92_NRINSC+C92_ATIVO
		cChave += Padr(cValChv,nTamTpInsc) 
	EndIf
	
	//Verificar se o nrInsc foi informado para a chave ( Obrigatorio ser informado )
	cValChv := FTafGetVal( cCabec + cTagOper + '/ideEstab/nrInsc', 'C', .F., @aIncons, .F., '', '' )
	If !Empty( cValChv )
		Aadd( aChave, { "C", "C92_NRINSC", cValChv, .T. } )
		nIndex := 6 //C92_FILIAL+ C92_TPINSC+ C92_NRINSC+C92_ATIVO
		cChave += Padr(cValChv,nTamNrInsc)
	EndIf
	
	//Verificar se o iniValid foi informado para a chave 
	cValChv := FTafGetVal( cCabec + cTagOper + '/ideEstab/iniValid', 'C', .F., @aIncons, .F., '', '' )
	cValChv := TAF253Format("C92_DTINI", cValChv)
	If !Empty( cValChv )
		Aadd( aChave, { "C", "C92_DTINI", cValChv, .T. } )
		nIndex := 7 //C92_FILIAL+C92_TPINSC+C92_NRINSC+C92_DTINI+C92_ATIVO
		cPerIni 	:= cValChv
		cPerIniOri	:= cValChv 
	EndIf                                                                                                   
	
	//Verificar se a data final foi informado para a chave( Se nao informado sera adotado vazio )
	cValChv := FTafGetVal( cCabec + cTagOper + '/ideEstab/fimValid', 'C', .F., @aIncons, .F., '', '' )
	cValChv := TAF253Format("C92_DTFIN", cValChv)
	If !Empty( cValChv )
		Aadd( aChave, { "C", "C92_DTFIN", cValChv, .T.} )
		nIndex := 2 //C92_FILIAL+ C92_TPINSC+ C92_NRINSC+C92_DTINI+C92_DTFIN+C92_ATIVO  
		cPerFin := cValChv                                                                                  
	EndIf
	
	If nOpc == 4
		If oDados:XPathHasNode( cCabec + cTagOper + "/novaValidade/iniValid", 'C', .F., @aIncons, .F., '', ''  )
			cNewDtIni 	:= FTafGetVal( cCabec + cTagOper + "/novaValidade/iniValid", 'C', .F., @aIncons, .F., '', '' )
			cNewDtIni 	:= TAF253Format("C92_DTINI", cNewDtIni)
			cPerIni		:= cNewDtIni
			aNewData[1] := cNewDtIni
			lNewValid	:= .T.
		EndIf
		
		If oDados:XPathHasNode( cCabec + cTagOper + "/novaValidade/fimValid", 'C', .F., @aIncons, .F., '', ''  )
			cNewDtFin 	:= 	FTafGetVal( cCabec + cTagOper + "/novaValidade/fimValid", 'C', .F., @aIncons, .F., '', '' )
			cNewDtFin 	:= 	TAF253Format("C92_DTFIN", cNewDtFin)
			cPerFin 	:=	cNewDtFin 
			aNewData[2] := cNewDtFin
			lNewValid	:= .T.
		EndIf
	EndIf
	
	//Valida as regras da nova validade
	If Empty(aIncons)	
		VldEvTab( "C92", 7, cChave, cPerIni, cPerFin, 2, nOpc, @aIncons, cPerIniOri,,, lNewValid )	
	EndIf
	
	If Empty(aIncons)

		Begin Transaction
			
			//�������������������������������������������������������������Ŀ
			//�Funcao para validar se a operacao desejada pode ser realizada�
			//���������������������������������������������������������������
			If FTafVldOpe( "C92", nIndex, @nOpc, cFilEv, @aIncons, aChave, @oModel, "TAFA253", cCmpsNoUpd, nIndIDVer, .T., aNewData )

				If TafColumnPos( "C92_LOGOPE" )
					cLogOpeAnt := C92->C92_LOGOPE
				endif

				//����������������������������������������������������������������Ŀ
				//�Quando se tratar de uma Exclusao direta apenas preciso realizar �
				//�o Commit(), nao eh necessaria nenhuma manutencao nas informacoes�
				//������������������������������������������������������������������
				If nOpc <> 5
					
					//���������������������������������������������������������������Ŀ
					//�Carrego array com os campos De/Para de gravacao das informacoes�
					//�����������������������������������������������������������������
					aRules := TAF253Rul( cTagOper, @cInconMsg, @nSeqErrGrv, cCodEvent, cOwner )

					If TAFColumnPos( "C92_XMLID" )
						oModel:LoadValue( "MODEL_C92", "C92_XMLID", cXmlID )
					EndIf

					//����������������������������������������Ŀ
					//�Rodo o aRules para gravar as informacoes�
					//������������������������������������������
					For nI := 1 to Len( aRules )
					 	oModel:LoadValue( "MODEL_C92", aRules[ nI, 01 ], FTafGetVal( aRules[ nI, 02 ], aRules[nI, 03], aRules[nI, 04], @aIncons, .F., ,aRules[ nI, 01 ] ) )
					Next nI

					If Findfunction("TAFAltMan")
						if nOpc == 3
							TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_C92', 'C92_LOGOPE' , '1', '' )
						elseif nOpc == 4
							TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_C92', 'C92_LOGOPE' , '', cLogOpeAnt )
						EndIf
					EndIf
		
					//���������������������������������������������������������������Ŀ
					//�Quando se trata de uma alteracao deleto todas as linhas do Grid�
					//�����������������������������������������������������������������
					If nOpc == 4
						For nI := 1 to oModel:GetModel("MODEL_T0Z" ):Length()
							oModel:GetModel( "MODEL_T0Z" ):GoLine( nI )
							oModel:GetModel( "MODEL_T0Z" ):DeleteLine()
						Next nI
					EndIf
		
					While oDados:XPathHasNode( cCabec + cTagOper + "/dadosEstab/infoTrab/infoApr/infoEntEduc[" + cValToChar( nT0Z ) + "]")
						If (nT0Z > 1) .or. (nOpc == 4)
							// Necessario atribuir lValid para que permita o Addline
							oModel:GetModel( "MODEL_T0Z" ):lValid := .T.
							oModel:GetModel( "MODEL_T0Z" ):AddLine()
						EndIf
						//Grava as informacoes
						oModel:LoadValue( "MODEL_T0Z", "T0Z_CNPJEE", FTafGetVal(cCabec + cTagOper + "/dadosEstab/infoTrab/infoApr/infoEntEduc[" + cValToChar( nT0Z ) + "]/nrInsc", "C", .F., @aIncons, .F. ) )
						nT0Z ++
					EndDo
				EndIf
		
				//���������������������������Ŀ
				//�Efetiva a operacao desejada�
				//�����������������������������
				If Empty(cInconMsg)	.And. Empty(aIncons)
					If TafFormCommit( oModel )
						Aadd(aIncons, "ERRO19")		
					Else	
						lRet := .T.	 
					EndIf	
				Else		
					Aadd(aIncons, cInconMsg)
					DisarmTransaction()		
				EndIf 
				
				oModel:DeActivate()
				If FindFunction('TafClearModel')
					TafClearModel(oModel)
				EndIf
			EndIf
		
		End Transaction
	
	EndIf

	//����������������������������������������������������������Ŀ
	//�Zerando os arrays e os Objetos utilizados no processamento�
	//������������������������������������������������������������
	aSize( aRules, 0 )
	aRules := Nil
	
	aSize( aChave, 0 )
	aChave := Nil

EndIf

Return{ lRet, aIncons }

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF253Rul

Regras para gravacao das informacoes do registro S-1005 do E-Social

@Param
cTagOper - Tag de indicacao da operacao

@Return
aRull - Regras para a gravacao das informacoes

@author Felipe C. Seolin
@since 26/09/2013
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function TAF253Rul( cTagOper, cInconMsg, nSeqErrGrv, cCodEvent, cOwner  )

Local aRull	
Local cCabec
Local lNt1519

Default cTagOper		:= ""
Default cInconMsg		:= ""
Default nSeqErrGrv	:= 0
Default cCodEvent		:= ""
Default cOwner		:= ""

aRull   := {}
cCabec  := "/eSocial/evtTabEstab/infoEstab"
lNt1519		:= FindFunction("TafNT1519") .And. TAFNT1519()

if TafXNode( oDados, cCodEvent, cOwner,( cCabec + cTagOper + "/ideEstab/tpInsc"  ))
	aAdd( aRull, { "C92_TPINSC", cCabec + cTagOper + "/ideEstab/tpInsc"      , "C", .F. } ) //tpInscricao
EndIf

if TafXNode( oDados, cCodEvent, cOwner,( cCabec + cTagOper + "/ideEstab/nrInsc" ))
	aAdd( aRull, { "C92_NRINSC", cCabec + cTagOper + "/ideEstab/nrInsc"      , "C", .F. } ) //nrInscricao
EndIf

if TafXNode( oDados, cCodEvent, cOwner,( cCabec + cTagOper + "/dadosEstab/cnaePrep" ))
	aAdd( aRull, { "C92_CNAE"  , cCabec + cTagOper + "/dadosEstab/cnaePrep", "C", .F. } ) //cnaePrep
EndIf

if TafXNode( oDados, cCodEvent, cOwner,( cCabec + cTagOper + "/dadosEstab/aliqGilrat/aliqRat" ))
	aAdd( aRull, { "C92_ALQRAT", cCabec + cTagOper + "/dadosEstab/aliqGilrat/aliqRat", "N", .F. } ) //aliqRat
EndIf

if TafXNode( oDados, cCodEvent, cOwner,( cCabec + cTagOper + "/dadosEstab/aliqGilrat/aliqRatAjust" ))
	aAdd( aRull, { "C92_AJURAT", cCabec + cTagOper + "/dadosEstab/aliqGilrat/aliqRatAjust", "N", .F. } ) //aliqRatAjustada
EndIf

if TafXNode( oDados, cCodEvent, cOwner,( cCabec + cTagOper + "/dadosEstab/aliqGilrat/fap" ))
	aAdd( aRull, { "C92_FAP", cCabec + cTagOper + "/dadosEstab/aliqGilrat/fap", "N", .F. } ) //fap
EndIf

If TafXNode( oDados, cCodEvent, cOwner,(	cCabec + cTagOper + "/dadosEstab/infoObra/indSubstPatrObra"	))
	aAdd( aRull, { "C92_SUBPAT", cCabec + cTagOper + "/dadosEstab/infoObra/indSubstPatrObra", "C", .F. } ) //indSubstPatronalObra 
EndIf

If TafXNode( oDados, cCodEvent, cOwner,( cCabec + cTagOper + "/dadosEstab/infoCaepf/tpCaepf" ))
	aAdd( aRull, { "C92_TPCAEP", cCabec + cTagOper + "/dadosEstab/infoCaepf/tpCaepf", "C", .F. } ) //indSubstPatronalObra 
EndIf

If TafXNode( oDados, cCodEvent, cOwner,( cCabec + cTagOper + "/dadosEstab/infoTrab/regPt"))
	aAdd( aRull, { "C92_REGPT", cCabec + cTagOper + "/dadosEstab/infoTrab/regPt", "C", .F. } ) //indSubstPatronalObra 
EndIf

If TafXNode( oDados, cCodEvent, cOwner,( cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudRat/tpProc" )) .or. TafXNode( oDados , cCodEvent, cOwner,( cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudRat/nrProc" )) 
	aAdd( aRull, { "C92_PRORAT", FGetIdInt( "tpProc", "nrProc", cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudRat/tpProc", ;
		cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudRat/nrProc",,,@cInconMsg, @nSeqErrGrv ), "C", .T. } ) //procAdmJudRat 
EndIf

If TafXNode( oDados, cCodEvent, cOwner,(cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudRat/codSusp"))
	aAdd( aRull, { "C92_CODSUR", cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudRat/codSusp", "C", .F. } ) //codSusp
EndIf

If TafXNode( oDados, cCodEvent, cOwner,( cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudFap/tpProc" )) .or. TafXNode( oDados , cCodEvent, cOwner,( cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudFap/nrProc" )) 
	aAdd( aRull, { "C92_PROFAP", FGetIdInt( "tpProc", "nrProc", cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudFap/tpProc",;
	 	cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudFap/nrProc",,,@cInconMsg, @nSeqErrGrv), "C", .T. } ) //procAdmJudFap
EndIf

If TafXNode( oDados, cCodEvent, cOwner,(cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudFap/codSusp"))
	aAdd( aRull, { "C92_CODSUF", cCabec + cTagOper + "/dadosEstab/aliqGilrat/procAdmJudFap/codSusp", "C", .F. } ) //codSusp
EndIf

If TafXNode( oDados, cCodEvent, cOwner,(cCabec + cTagOper + "/dadosEstab/infoTrab/infoApr/contApr"))
	aAdd( aRull, { "C92_CONTAP", cCabec + cTagOper + "/dadosEstab/infoTrab/infoApr/contApr", "C", .F. } )
EndIf
If TafXNode( oDados, cCodEvent, cOwner,(cCabec + cTagOper + "/dadosEstab/infoTrab/infoApr/nrProcJud")) 
	aAdd( aRull, { "C92_PROCAP", FGetIdInt("nrProcJ",,cCabec + cTagOper + "/dadosEstab/infoTrab/infoApr/nrProcJud"), "C", .T. } )
EndIf
If TafXNode( oDados, cCodEvent, cOwner,(cCabec + cTagOper + "/dadosEstab/infoTrab/infoApr/contEntEd"))
	aAdd( aRull, { "C92_CTENTE",xFunTrcSN(TAFExisTag(cCabec + cTagOper + "/dadosEstab/infoTrab/infoApr/contEntEd"),2), "C", .T. } )
EndIf
If TafXNode( oDados, cCodEvent, cOwner,(cCabec + cTagOper + "/dadosEstab/infoTrab/infoPCD/contPCD"))	
	aAdd( aRull, { "C92_CONPCD", cCabec + cTagOper + "/dadosEstab/infoTrab/infoPCD/contPCD", "C", .F. } )
EndIf
If TafXNode( oDados, cCodEvent, cOwner,(cCabec + cTagOper + "/dadosEstab/infoTrab/infoPCD/nrProcJud"))  
	aAdd( aRull, { "C92_PROCPD", FGetIdInt("nrProcJ",,cCabec + cTagOper + "/dadosEstab/infoTrab/infoPCD/nrProcJud"), "C", .T. } )
EndIf

Return( aRull )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF253Format

Formata os campos do registro S-1005 do E-Social

@Param
cCampo 	  - Campo que deve ser formatado
cValorXml - Valor a ser formatado

@Return
cFormatValue - Valor j� formatado

@author Anderson Costa
@since 09/12/2015
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function TAF253Format(cCampo, cValorXml)

Local cFormatValue
Local cRet		

cFormatValue	:= ""
cRet			:= ""

If (cCampo == 'C92_DTINI' .OR. cCampo == 'C92_DTFIN')
	cFormatValue := StrTran( StrTran( cValorXml, "-", "" ), "/", "")
	cRet := Substr(cFormatValue, 5, 2) + Substr(cFormatValue, 1, 4)
Else
	cRet := cValorXml
EndIf

Return( cRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF253Xml
Funcao de geracao do XML para atender o registro S-1005
Quando a rotina for chamada o registro deve estar posicionado

@Param:
cAlias - Alias da Tabela
nRecno - Recno do Registro corrente
nOpc   - Operacao a ser realizada
lJob   - Informa se foi chamado por Job
lRemEmp - Exclusivo do Evento S-1000
cSeqXml - Numero sequencial para composi��o da chave ID do XML

@Return:
cXml - Estrutura do Xml do Layout S-1005

@author Felipe C. Seolin
@since 24/09/2013
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAF253Xml(cAlias,nRecno,nOpc,lJob,lRemEmp,cSeqXml)	

Local cXml		   := ""
Local cLayout 	   := "1005"
Local cReg    	   := "TabEstab"
Local cEvento 	   := ""
Local cDtIni  	   := ""
Local cDtFin  	   := ""
Local cId 		   := ""
Local cVerAnt 	   := ""
Local cTpProc	   := ""
Local cCodSusp	   := ""
Local cCNPJMatriz  := ""
Local cXmlInfoApr  := ""
Local cXmlInfoPCD  := ""
Local cContEndEnt  := ""
Local cInfoEntEduc := ""
Local lNt1519      := FindFunction("TafNT1519") .And. TAFNT1519()
Local nRecnoSM0    := SM0->(Recno())
Local lXmlVLd	   := IIF(FindFunction('TafXmlVLD'),TafXmlVLD('TAF253XML'),.T.)

Default lJob := .F.
Default cSeqXml := ""

DBSelectArea("C1E")
C1E->( DBSetOrder(3) )
If C1E->( MSSeek( xFilial("C1E") + PadR( SM0->M0_CODFIL, TamSX3( "C1E_FILTAF" )[1] ) + "1" ) )
	If C1E->C1E_MATRIZ == .T.
		cCNPJMatriz := SM0->M0_CGC
	EndIf
EndIf

dbSelectArea("C1G")
C1G->( DBSetOrder(8) )

dbSelectArea("T5L")
T5L->( DBSetOrder(1) )
If lXmlVLd
	If C92->C92_EVENTO $ "I|A"

		If C92->C92_EVENTO == "A"
			cEvento := "alteracao"

			cId := C92->C92_ID 
			cVerAnt := C92->C92_VERANT
			
			BeginSql alias 'C92TEMP'
				SELECT C92.C92_DTINI,C92.C92_DTFIN
				FROM %table:C92% C92
				WHERE C92.C92_FILIAL= %xfilial:C92% AND
				C92.C92_ID = %exp:cId% AND C92.C92_VERSAO = %exp:cVerAnt% AND 
				C92.%notDel%
			EndSql  
			
			//***********************************************************************************
			//Tratamento do formato da data (C92_DTINI e C92_DTFIN) para gera��o do XML de acordo 
			//com a nova fomula��o do eSocial. Formato: AAAA-MM 
			//***********************************************************************************
			cDtIni := Substr(('C92TEMP')->C92_DTINI,3,4) +"-"+ Substr(('C92TEMP')->C92_DTINI,1,2)
			If ! Empty(('C92TEMP')->C92_DTFIN)
				cDtFin := Substr(('C92TEMP')->C92_DTFIN,3,4) +"-"+ Substr(('C92TEMP')->C92_DTFIN,1,2)
			EndIf
			//-----------

			('C92TEMP')->( DbCloseArea() )
		Else
			cEvento := "inclusao"
				
			//***********************************************************************************
			//Tratamento do formato da data (C92_DTINI e C92_DTFIN) para gera��o do XML de acordo 
			//com a nova fomula��o do eSocial. Formato: AAAA-MM 
			//***********************************************************************************
			cDtIni := Substr(C92->C92_DTINI,3,4) +"-"+ Substr(C92->C92_DTINI,1,2)
			If ! Empty(C92->C92_DTFIN)
				cDtFin := Substr(C92->C92_DTFIN,3,4) +"-"+ Substr(C92->C92_DTFIN,1,2)
			EndIF
			//-----------
			
		EndIf
		
		cXml +=			"<infoEstab>"
		cXml +=				"<" + cEvento + ">"
		cXml +=					"<ideEstab>"
		cXml +=						xTafTag("tpInsc",C92->C92_TPINSC)
		cXml +=						xTafTag("nrInsc",C92->C92_NRINSC)
		cXml +=						xTafTag("iniValid",cDtIni)

		If !Empty(cDtFin)
			cXml +=				xTafTag("fimValid",cDtFin)
		EndIf
		
		cXml +=					"</ideEstab>"
		cXml +=					"<dadosEstab>"
		cXml +=							xTafTag("cnaePrep",C92->C92_CNAE)
		cXml +=						"<aliqGilrat>"
		cXml +=							xTafTag("aliqRat",C92->C92_ALQRAT,,,,.T.)
		If !Empty(C92->C92_FAP)
			cXml +=							xTafTag("fap",C92->C92_FAP,PesqPict("C92","C92_FAP"))
		EndIf
		
		If !Empty(C92->C92_AJURAT)
			cXml +=							xTafTag("aliqRatAjust",C92->C92_AJURAT,PesqPict("C92","C92_AJURAT"))
		EndIf
		
		If !Empty(C92->C92_PRORAT)
			
			cTpProc	:= Posicione("C1G",8,xFilial("C1G")+C92->C92_PRORAT+"1","C1G_TPPROC")
			//Inverto os c�digos para atender o layout do eSocial
			if !empty( cTpProc )
				cTpProc := Iif(alltrim(cTpProc) == "1", "2", Iif(alltrim(cTpProc) == "2", "1", cTpProc) ) 
			endif
					
			cXml +=								"<procAdmJudRat>"		
			cXml +=									xTafTag("tpProc",cTpProc)																						
			cXml +=									xTafTag("nrProc", Alltrim(Posicione("C1G",8,xFilial("C1G")+C92->C92_PRORAT+"1","C1G_NUMPRO")))
			cXml +=									xTafTag("codSusp",Alltrim(C92->C92_CODSUR),,.T.)
			cXml +=								"</procAdmJudRat>"
		EndIf
				
		If 	!Empty(C92->C92_PROFAP)	
			
			cTpProc	:= Posicione("C1G",8,xFilial("C1G")+C92->C92_PROFAP+"1","C1G_TPPROC")
			//Inverto os c�digos para atender o layout do eSocial
			if !empty( cTpProc )
				cTpProc := Iif(alltrim(cTpProc) == "1", "2", Iif(alltrim(cTpProc) == "2", "1", cTpProc) ) 
			endif
							
			cXml +=								"<procAdmJudFap>"
			cXml +=									xTafTag("tpProc",cTpProc)
			cXml +=									xTafTag("nrProc", Alltrim(Posicione("C1G",8,xFilial("C1G")+C92->C92_PROFAP+"1","C1G_NUMPRO")))
			cXml +=									xTafTag("codSusp", Alltrim(C92->C92_CODSUF),,.T.)
			cXml +=								"</procAdmJudFap>"		
		EndIf
		
		cXml +=						"</aliqGilrat>"
			
		If !Empty(C92->C92_TPCAEP)	
			cXml +=						"<infoCaepf>"
			cXml +=							xTafTag("tpCaepf",C92->C92_TPCAEP)
			cXml +=						"</infoCaepf>"	
		EndIf
		
		If !Empty(C92->C92_SUBPAT)	
			cXml +=						"<infoObra>"
			cXml +=							xTafTag("indSubstPatrObra",C92->C92_SUBPAT)
			cXml +=						"</infoObra>"	
		EndIf
		
		If lNt1519
			
			If C92->C92_CONTAP <> '0' .And. !Empty(C92->C92_CTENTE)
				cContEndEnt := xFunTrcSN(C92->C92_CTENTE,1)
			EndIf

			T0Z->( DBSetOrder( 1 ) )
			If T0Z->( MsSeek ( C92->C92_FILIAL + C92->C92_ID + C92->C92_VERSAO ) )
				While T0Z->( !Eof() ) .and. C92->C92_FILIAL + C92->(C92_ID + C92->C92_VERSAO) == T0Z->(T0Z_FILIAL + T0Z_ID + T0Z_VERSAO)

					cInfoEntEduc += "<infoEntEduc>"
					cInfoEntEduc +=		xTafTag( "nrInsc", T0Z->T0Z_CNPJEE )
					cInfoEntEduc += "</infoEntEduc>"
					T0Z->( DBSkip() )
				EndDo
			EndIf
			
			xTafTagGroup("infoApr", {{"contApr"  , C92->C92_CONTAP, ,.F.};
								,  {"nrProcJud", AllTrim(Posicione("C1G",8,xFilial("C1G")+C92->C92_PROCAP+"1","C1G_NUMPRO")), ,.T.};
								,  {"contEntEd", cContEndEnt    , ,.T.}};
								, @cXmlInfoApr;
								, {{"infoEntEduc",cInfoEntEduc,0}} )

			If Alltrim(cCNPJMatriz) == Alltrim(C92->C92_NRINSC)
				If !Empty(C92->C92_CONPCD) .OR. !Empty(C92->C92_PROCPD)
					cXmlInfoPCD +=	"<infoPCD>"
					cXmlInfoPCD +=		xTafTag("contPCD",C92->C92_CONPCD)
					cXmlInfoPCD += 		xTafTag("nrProcJud",AllTrim(Posicione("C1G",8,xFilial("C1G")+C92->C92_PROCPD+"1","C1G_NUMPRO")),,.T.)
					cXmlInfoPCD +=	"</infoPCD>"
				EndIf
			EndIf

			xTafTagGroup("infoTrab", {{"regPt",C92->C92_REGPT, ,.T.}};
								, @cXml;
								, {{"infoApr",cXmlInfoApr,0},{"infoPCD",cXmlInfoPCD,0}})
		Else
			cXml += 							"<infoTrab>
			cXml +=								xTafTag("regPt",C92->C92_REGPT)
			cXml += 								"<infoApr>"
			
			cXml += 									xTafTag("contApr",C92->C92_CONTAP)
			cXml += 									xTafTag("nrProcJud",AllTrim(Posicione("C1G",8,xFilial("C1G")+C92->C92_PROCAP+"1","C1G_NUMPRO")),,.T.)
			
			If !Empty(C92->C92_CTENTE)
				cXml += 								xTafTag("contEntEd",xFunTrcSN(C92->C92_CTENTE,1),,.T.)
			EndIf
			
			T0Z->( DBSetOrder( 1 ) )
			If T0Z->( MsSeek ( C92->C92_FILIAL + C92->C92_ID + C92->C92_VERSAO ) )
				While T0Z->( !Eof() ) .and. C92->C92_FILIAL + C92->(C92_ID + C92->C92_VERSAO) == T0Z->(T0Z_FILIAL + T0Z_ID + T0Z_VERSAO)

					cXml += "<infoEntEduc>"
					cXml +=		xTafTag( "nrInsc", T0Z->T0Z_CNPJEE )
					cXml += "</infoEntEduc>"
					T0Z->( DBSkip() )
				EndDo
			EndIf
			
			cXml += 							"</infoApr>"
			
			If Alltrim(cCNPJMatriz) == Alltrim(C92->C92_NRINSC)
				If !Empty(C92->C92_CONPCD) .OR. !Empty(C92->C92_PROCPD)
					cXml += 								"<infoPCD>"
					cXml += 									xTafTag("contPCD",C92->C92_CONPCD)
					cXml += 									xTafTag("nrProcJud",AllTrim(Posicione("C1G",8,xFilial("C1G")+C92->C92_PROCPD+"1","C1G_NUMPRO")),,.T.)
					cXml += 								"</infoPCD>"
				EndIf
			EndIf
			cXml += 							"</infoTrab>
		EndIf

		cXml +=					"</dadosEstab>"
		
		If C92->C92_EVENTO == "A"
			If TafAtDtVld("C92", C92->C92_ID, C92->C92_DTINI, C92->C92_DTFIN, C92->C92_VERANT, .T.)
				cXml +=			"<novaValidade>"
				cXml +=				TafGetDtTab(C92->C92_DTINI,C92->C92_DTFIN)
				cXml +=			"</novaValidade>"
			EndIf
		EndIf

		cXml +=				"</" + cEvento + ">"
		cXml +=			"</infoEstab>"

	ElseIf C92->C92_EVENTO == "E"		
		cXml +=			"<infoEstab>"
		cXml +=				"<exclusao>"
		cXml +=					"<ideEstab>"
		cXml +=						xTafTag("tpInsc",C92->C92_TPINSC)
		cXml +=						xTafTag("nrInsc",C92->C92_NRINSC)
		cXml +=						TafGetDtTab(C92->C92_DTINI,C92->C92_DTFIN)
		cXml +=					"</ideEstab>"
		cXml +=				"</exclusao>"
		cXml +=			"</infoEstab>"
	EndIf

	//����������������������Ŀ
	//�Estrutura do cabecalho�
	//������������������������
	If nRecnoSM0 > 0
		SM0->(dbGoto(nRecnoSM0))
	endif
	cXml := xTafCabXml(cXml,"C92",cLayout,cReg,,cSeqXml)
	//����������������������������Ŀ
	//�Executa gravacao do registro�
	//������������������������������
	If !lJob
		xTafGerXml(cXml,cLayout)
	EndIf		
EndIF
Return(cXml)

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF253Vld

Validacao dos dados do registro posicionado, verificando inconsistencias
nas informacos caso seja necessario gerar um XML

@Param:
cAlias - Alias da Tabela
nRecno - Recno do Registro corrente
nOpc   - Operacao a ser realizada
lJob   - Job / Aplicacao

@Return:

@author Felipe C. Seolin
@since 09/10/2013
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAF253Vld(cAlias,nRecno,nOpc,lJob)

Local cChave    	
Local cStatus   	
Local cCodNatJur	
Local lValida   	
Local aDadosUtil	
Local aLogErro		
Local cCNPJMatriz	
Local lMatriz		
Local aAreaC92		
Local aSM0			
Local nPosEmp		
Local lExcecFPAS
Local lNt1519	

aLogErro	:= {}
cChave		:= ""
cStatus		:= ""
cCNPJMatriz := ""
lValida		:= .F.
lMatriz		:= .F.
aDadosUtil	:= {}
aAreaC92	:= C92->(GetArea())
lNt1519		:= FindFunction("TafNT1519") .And. TAFNT1519()

If FindFunction("TAFGetaSM0")
	aSM0	:= TAFGetaSM0()
Else
	aSM0	:= {}
EndIf
nPosEmp		:= 0
lExcecFPAS	:= .F. 

Default lJob    := .F.

//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := C92->( Recno() )

lValida := ( C92->C92_STATUS $ ( " |1" ) )

If !lValida
	aAdd(aLogErro,{"C92","000305", "C92", nRecno }) //STR305 - "Registros que j� foram transmitidos ao Fisco, n�o podem ser validados"
Else

	DBSelectArea("C1E")
	C1E->( DBSetOrder(3) )
	If C1E->( MSSeek( xFilial("C1E") + PadR( SM0->M0_CODFIL, TamSX3( "C1E_FILTAF" )[1] ) + "1" ) )
		If C1E->C1E_MATRIZ == .T.
			cCNPJMatriz := SM0->M0_CGC			
		EndIf
	EndIf
	
	// --> Restaura o Alias anterior, pois pelo posicionamento na C1E acima, estava gerando error.log quando o campo da C92 era macroexecutado na fun��o xVldRegra().
	RestArea(aAreaC92)	

	If C92->C92_EVENTO == "I"
		xVldRegra("C92","REGRA_TABGERAL_INCLUSAO_PERIODO_CONFLITANTE",@aLogErro,,"C92")
	ElseIf C92->C92_EVENTO == "A"
		xVldRegra("C92","REGRA_TABGERAL_EXISTE_REGISTRO_ALTERADO",@aLogErro,,"C92",)
		xVldRegra("C92","REGRA_TABGERAL_ALTERACAO_PERIODO_CONFLITANTE",@aLogErro,, "C92")
	ElseIf C92->C92_EVENTO == "E"
		xVldRegra("C92","REGRA_TABGERAL_EXISTE_REGISTRO_EXCLUIDO",@aLogErro,,"C92")
	EndIf

	aAdd(aDadosUtil, {xFunDtPer(C92->C92_DTINI), xFunDtPer(C92->C92_DTFIN,.T.), C92->C92_FILIAL}) 
	xVldRegra("C1E","REGRA_EXISTE_INFO_EMPREGADOR",@aLogErro, aDadosUtil,"C92") 
	
	//REGRA_TABESTAB_VALIDA_ESTABELECIMENTO
	//Para empregador cuja natureza jur�dica seja igual a [102-3, 103-1, 105-8, 106-6, 108-2, 117-1, 118-0, 119-8, 120-1, 121-0, 123-6, 124-4], 
	//o n�mero de inscri��o do estabelecimento informado deve pertencer ao mesmo EFR - Ente Federativo Respons�vel do Empregador (definido em S-1000)
	//no cadastro CNPJ da RFB.
	If C92->C92_TPINSC == "1"
		aDadosUtil := { "C92_FILIAL", "C92_NRINSC", "C92" }
		xVldRegra( "C92", "REGRA_TABESTAB_VALIDA_ESTABELECIMENTO", @aLogErro, { aDadosUtil }, "C92" )	
	EndIf

	//Tag obrigatoria para a geracao do XML
	If Empty(C92->C92_TPINSC)
		aAdd(aLogErro,{"C92_TPINSC","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
	Else
		If !(C92->C92_TPINSC $ "134")
			aAdd(aLogErro,{"C92_TPINSC","000006","C92",nRecno}) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
		EndIf
	EndIf
	
	If !Empty ( C92->C92_TPCAEP )
		If !(C92->C92_TPCAEP $ "123")
			aAdd(aLogErro,{"C92_TPCAEP","000006","C92",nRecno}) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
		EndIf
	Endif
	
	If Empty(C92->C92_NRINSC)
		aAdd(aLogErro,{"C92_NRINSC","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
	EndIf

	If Empty(C92->C92_DTINI)
		aAdd(aLogErro,{"C92_DTINI","000009","C92",nRecno}) //STR0009 - "Data Inconsistente ou Vazia"
	ElseIf !xVldFormDt(C92->C92_DTINI, "MMAAAA")
		aAdd( aLogErro, { "C92_DTINI", "000432", "C92", nRecno } ) //000432 - O valor do campo difere do formato esperado.
	EndIf

	If Empty(C92->C92_CNAE)
		aAdd(aLogErro,{"C92_CNAE","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
	EndIf

	If !(aSM0 == Nil .OR. Len(aSM0) == 0)
		//Isen��o de al�quota RAT para quem se enquadra no FPAS 825, 604, 833, 639, 647, 779, 620
		nPosEmp		:= aScan(aSM0, { |x| Alltrim(x[1]) == AllTrim(FWGrpCompany()) .AND. AllTrim(x[2]) == AllTrim(C92->C92_FILIAL) } )
		lExcecFPAS	:= Iif(nPosEmp == 0, .F., "|"+Iif(IsDigit(aSM0[nPosEmp][3]),cValToChar(Val(aSM0[nPosEmp][3])),aSM0[nPosEmp][3])+"|" $ "|825|604|833|639|647|779|620|")
	EndIf
	If Empty(C92->C92_ALQRAT) .and. !lExcecFPAS
		If Empty(C92->C92_PRORAT) // Caso haja processo cadastrado para Al�quota, libera lan�amento 0, valida o tipo de processo na procAdmJudRat
			aAdd(aLogErro,{"C92_ALQRAT","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
		EndIf
	EndIf

	If !Empty(C92->C92_AJURAT) .And.  !Empty(C92->C92_ALQRAT)
		If !(C92->C92_AJURAT == C92->C92_ALQRAT * C92->C92_FAP)
			aAdd(aLogErro,{"C92_AJURAT","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
		EndIf
	EndIf
	
	If C92->C92_TPINSC == '1' .AND. Empty(C92->C92_FAP)
		If Empty(C92->C92_PROFAP) // Caso haja processo cadastrado para Al�quota, libera lan�amento 0, valida o tipo de processo na procAdmJudRat
			aAdd(aLogErro,{"C92_FAP","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"	
		EndIf	
	ElseIf	C92->C92_TPINSC == '1' .AND. !Empty(C92->C92_FAP) 
		If  !(C92->C92_FAP >= 0.5 .And. C92->C92_FAP <= 2)
			AADD(aLogErro,{"C92_FAP","000873", "C92", nRecno}) //STR0873 - "Fap Informado deve ser um n�mero maior ou igual a 0,5000 e menor ou igual a 2,0000"       
		EndIf	
	EndIf
	
	// --> Valida o grupo procAdmJudRat
	If !Empty(C92->C92_PRORAT)
		
		If Empty(C92->C92_CODSUR)
			aAdd(aLogErro,{"C92_CODSUR","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
		EndIf
		
		C1G->( DBSetOrder(8) )	
		If C1G->( MsSeek( xFilial("C1G") + C92->C92_PRORAT + '1' ) )
			
			If !( AllTrim( TAFGetValue("LE7", 3, xFilial("LE7")+C1G->C1G_INDMAT, "LE7_CODIGO") ) == "1" ) 
				AADD(aLogErro,{"C92_PRORAT","000980","C92", nRecno }) //000980 - "Informar um n�mero de processo cadastrado atrav�s do evento S-1070, cujo Indicativo da mat�ria do processo seja igual a 1."
			EndIf
			
			T5L->( DBSetOrder(1) )	
			If !T5L->( MsSeek( xFilial("T5L") + C1G->(C1G_ID+C1G_VERSAO)+C92->C92_CODSUR) )							
				AADD(aLogErro,{"C92_CODSUR","000926","C92", nRecno }) //STR0926 - "A informa��o prestada deve estar de acordo com o que foi informado no evento S-1070."
			EndIf
		EndIf
		
	EndIf
	
	// --> Valida o grupo procAdmJudFap
	If !Empty(C92->C92_PROFAP)
	
		If Empty(C92->C92_CODSUF)
			aAdd(aLogErro,{"C92_CODSUF","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
		EndIf  
		
		C1G->( DBSetOrder(8) )
		If C1G->( MsSeek( xFilial("C1G") + C92->C92_PROFAP + '1' ) )
		
			If !( AllTrim( TAFGetValue("LE7", 3, xFilial("LE7")+C1G->C1G_INDMAT, "LE7_CODIGO") ) == "1" ) 
				AADD(aLogErro,{"C92_PRORAT","000980","C92", nRecno }) //000980 - "Informar um n�mero de processo cadastrado atrav�s do evento S-1070, cujo Indicativo da mat�ria do processo seja igual a 1."
			EndIf
		
			T5L->( DBSetOrder(1) )	
			If !T5L->( MsSeek( xFilial("T5L") + C1G->(C1G_ID+C1G_VERSAO)+C92->C92_CODSUF) )							
				AADD(aLogErro,{"C92_CODSUF","000926","C92", nRecno }) //STR0926 - "A informa��o prestada deve estar de acordo com o que foi informado no evento S-1070."
			EndIf
		EndIf
	
	EndIf
	
	//(C92->C92_ALQRAT)
	//Valida��o n�o realizada pois necessita de informa��o do CNAE que vem do governo
	
	If (C92->C92_TPINSC <> '1' .And. C92->C92_TPINSC <> '4') .AND. !Empty(C92->C92_FAP)
		aAdd(aLogErro,{"C92_FAP","000933","C92",nRecno}) //STR0933 - "O campo � de preenchimento obrigat�rio apenas para Pessoa Jur�dica."
	EndIf
	//Valida��o NT 15/2019
	If !lNt1519
		If Empty(C92->C92_REGPT) .Or. !(C92->C92_REGPT $ "0|1|2|3|4|5|6" ) 
			aAdd(aLogErro,{"C92_REGPT","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"	
		EndIf
	EndIf
	
	If Empty(C92->C92_CONTAP)
		If !lNt1519
			aAdd(aLogErro,{"C92_CONTAP","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"	
		EndIf
	ElseIf C92->C92_CONTAP $ '2|1'
		
		//Valida��o NT 15/2019
		If !lNt1519
			If Empty(C92->C92_CTENTE)
			
				aAdd(aLogErro,{"C92_CTENTE","000016","C92",nRecno}) //STR0016 - "O preenchimento � obrigat�rio se o Indicativo de Contrata��o de Aprendiz for igual a 1 ou 2"
			
			ElseIf C92->C92_CTENTE == "1" 
			
				T0Z->( DbSetOrder(1) )
				If !( T0Z->( DbSeek( xFilial("T0Z") + C92->( C92_ID + C92_VERSAO ) ) ) )  
					aAdd(aLogErro,{"T0Z_CNPJEE","000979","C92",nRecno}) //STR0979 - "Preenchimento obrigat�rio quando a empresa contrata e aprendiz por interm�dio de entidade educativa "
				EndIf
					
			EndIf
			
			If C92->C92_CONTAP == '1' .And. Empty(C92->C92_PROCAP)
				aAdd(aLogErro,{"C92_PROCAP","000017","C92",nRecno}) //STR0017 - "O preenchimento � obrigat�rio se o Indicativo de Contrata��o de Aprendiz for igual a 1"
			EndIf
		Else
			If C92->C92_CONTAP == '1'
			
				If Empty(C92->C92_PROCAP)
					aAdd(aLogErro,{"C92_PROCAP","000017","C92",nRecno}) //STR0017 - "O preenchimento � obrigat�rio se o Indicativo de Contrata��o de Aprendiz for igual a 1"
				EndIf
			
			EndIf
			
		EndIf
		
	EndIf
	
	If !lNt1519
		If Empty(C92->C92_CONPCD) .AND. !Empty(C92->C92_PROCPD)
			aAdd(aLogErro,{"C92_CONPCD","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"	
		EndIf
	EndIf
				
	If Alltrim(cCNPJMatriz) == Alltrim(C92->C92_NRINSC)
		If !lNt1519
			If Empty(C92->C92_CONPCD)
				aAdd(aLogErro,{"C92_CONPCD","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"	
			Else
				If C92->C92_CONPCD == '1' .AND. Empty(C92->C92_PROCPD)
					aAdd(aLogErro,{"C92_PROCPD","000967","C92",nRecno}) //STR0967 - "Informa��o obrigat�ria se {contPCD} = [1]."
				EndIf
			EndIf
			
			If Empty(C92->C92_CONPCD) .AND. !Empty(C92->C92_PROCPD)
				aAdd(aLogErro,{"C92_CONPCD","000010","C92",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"	
			EndIf
		Else
			If C92->C92_CONPCD == '1'
				If Empty(C92->C92_PROCPD) 
						aAdd(aLogErro,{"C92_PROCPD","000967","C92",nRecno}) //STR0966 - "Essa informa��o deve ser prestada apenas no estabelecimento 'Matriz'."	
				EndIf
			EndIf
		EndIf
	Else
	
		If !Empty(C92->C92_CONPCD)
			aAdd(aLogErro,{"C92_CONPCD","000966","C92",nRecno}) //STR0966 - "Essa informa��o deve ser prestada apenas no estabelecimento 'Matriz'."	
		EndIf
		
		If !Empty(C92->C92_PROCPD) 
			aAdd(aLogErro,{"C92_PROCPD","000966","C92",nRecno}) //STR0966 - "Essa informa��o deve ser prestada apenas no estabelecimento 'Matriz'."	
		EndIf
	EndIf

	//Campos nao obrigatorios serao validados somente se nao estiverem vazios
	If !Empty(C92->C92_DTFIN)
		If (Len(C92->C92_DTFIN) < 6) .Or. (xFunDtPer(C92->C92_DTFIN,.T.) < xFunDtPer(C92->C92_DTINI))
			AADD(aLogErro,{"C92_DTFIN","000004", "C92", nRecno}) //STR0004 - "Verifique a data Final do Registro"    
		EndIf
		If !xVldFormDt(C92->C92_DTFIN, "MMAAAA")
			aAdd( aLogErro, { "C92_DTFIN", "000432", "C92", nRecno } ) //000432 - O valor do campo difere do formato esperado.
		EndIf
	EndIf

	//ATUALIZO O STATUS DO REGISTRO
	cStatus := Iif(Len(aLogErro) > 0,"1","0")

	If RecLock("C92",.F.)
		C92->C92_STATUS := cStatus
		C92->(MsUnlock())
	EndIf
EndIf

//Nao apresento o alert quando utilizo o JOB para validar
If !lJob
	xValLogEr(aLogErro)
EndIf

Return(aLogErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} VldChvEst           
Fun��o que chama a valida��o das regras inclus�o e altera��o de eventos de tabelas 
do e-social (VldEvTab), para a rotina de Estabelecimentos, Obras ou Unidades 
de �rg�os P�blicos

@Param
cCampo		- Campo posicionado na tela

@author Denis R. de Oliveira
@since 28/12/2017
@version 1.0

/*/                        	
//-------------------------------------------------------------------
Function VldChvEst( cCampo )

Local lRet 

Default cCampo := ""

lRet := .T.
		
	If cCampo == "C92_TPINSC"  
		lRet	:= VldEvTab("C92",2,M->C92_TPINSC+FWFLDGET("C92_NRINSC"),FWFLDGET("C92_DTINI"),FWFLDGET("C92_DTFIN"),1)        
	ElseIf cCampo == "C92_NRINSC" 
		lRet 	:= VldEvTab("C92",2,FWFLDGET("C92_TPINSC")+M->C92_NRINSC,FWFLDGET("C92_DTINI"),FWFLDGET("C92_DTFIN"),1)  
		If lRet .And. FWFLDGET("C92_TPINSC") $"1|2"
			If FWFLDGET("C92_TPINSC") == '1'		//CNPJ
				lRet := XFUNVldPJF( "" ,  2, .F. )
			ElseIf FWFLDGET("C92_TPINSC") == '2'	//CPF
				lRet := XFUNVldPJF( "" ,  1, .F. )
			EndIf
		EndIf
	ElseIf cCampo == "C92_DTINI"
	 	lRet	:= VldEvTab("C92",2,FWFLDGET("C92_TPINSC")+FWFLDGET("C92_NRINSC"),M->C92_DTINI,FWFLDGET("C92_DTFIN"),1)  
	ElseIf cCampo == "C92_DTFIN"
		lRet	:= VldEvTab("C92",2,FWFLDGET("C92_TPINSC")+FWFLDGET("C92_NRINSC"),FWFLDGET("C92_DTINI"),M->C92_DTFIN,1)  
	EndIf

Return lRet