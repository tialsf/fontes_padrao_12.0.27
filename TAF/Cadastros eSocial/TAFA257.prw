#INCLUDE "PROTHEUS.CH"  
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "TAFA257.CH" 

Static __lLay0205	:= TafLayESoc("02_05_00")

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA257
Comunica��o de Acidente de Trabalho (S-2210)

@author Leandro Prado
@since 05/09/2013
@version 1.0

/*/ 
//-------------------------------------------------------------------
Function TAFA257()

Private oBrw :=  FWmBrowse():New()

// Fun��o que indica se o ambiente � v�lido para o eSocial 2.3
If TafAtualizado()
	oBrw:SetDescription(STR0001)	//"Comunica��o de Acidente de Trabalho"
	oBrw:SetAlias( 'CM0')
	oBrw:SetMenuDef( 'TAFA257' )
	
	If FindFunction('TAFSetFilter')
		oBrw:SetFilterDefault(TAFBrwSetFilter("CM0","TAFA257","S-2210"))
	Else
		oBrw:SetFilterDefault( "CM0_ATIVO == '1'" )	//Filtro para que apenas os registros ativos sejam exibidos ( 1=Ativo, 2=Inativo )
	EndIf
	
	TafLegend(2,"CM0",@oBrw)
	oBrw:Activate()
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Leandro Prado
@since 05/09/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
                                               
Local aRotina := {}
Local aFuncao := {}

If FindFunction('TafXmlRet')
	Aadd( aFuncao, { "" , "TafxmlRet('TAF257Xml','2210','CM0')" , "1" } )
Else
	Aadd( aFuncao, { "" , "TAF257Xml" , "1" } )
EndIf
Aadd( aFuncao, { "" , "Taf257Vld" , "2" } )

//Chamo a Browse do Hist�rico
If FindFunction( "xNewHisAlt" ) 
	Aadd( aFuncao, { "" , "xNewHisAlt('CM0', 'TAFA257' ,,,,,,'2210','TAF257Xml' )" , "3" } )
Else					
	Aadd( aFuncao, { "" , "xFunHisAlt('CM0', 'TAFA257' ,,,, 'TAF257XML','2210' )" , "3" } )
EndIf

aAdd( aFuncao, { "" , "TAFXmlLote( 'CM0', 'S-2210' , 'evtCAT' , 'TAF257Xml' )" , "5" } )
Aadd( aFuncao, { "" , "xFunAltRec( 'CM0' )" , "10" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If lMenuDif
	ADD OPTION aRotina Title "Visualizar" Action 'VIEWDEF.TAFA257' OPERATION 2 ACCESS 0
	
	// Menu dos extempor�neos
	If FindFunction( "xNewHisAlt" ) .AND. FindFunction( "xTafExtmp" ) .And. xTafExtmp()
		aRotina	:= xMnuExtmp( "TAFA257", "CM0" )
	EndIf
	
Else
	aRotina	:=	xFunMnuTAF( "TAFA257" , , aFuncao)
EndIf

Return( aRotina )

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC

@author Leandro Prado
@since 05/09/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

Local oStruCM0  :=  FWFormStruct( 1, 'CM0' )
Local oStruCM1  :=  FWFormStruct( 1, 'CM1' )
Local oStruCM2  :=  FWFormStruct( 1, 'CM2' )
Local oModel    :=  MPFormModel():New( 'TAFA257'  ,,,{|oModel| SaveModel(oModel)})

lVldModel := Iif( Type( "lVldModel" ) == "U", .F., lVldModel )
            
If lVldModel
	oStruCM0:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel }) 		
EndIf

//Remo��o do GetSX8Num quando se tratar da Exclus�o de um Evento Transmitido.
//Necess�rio para n�o incrementar ID que n�o ser� utilizado.
If Upper( ProcName( 2 ) ) == Upper( "GerarExclusao" )
	oStruCM0:SetProperty( "CM0_ID", MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD, "" ) )
EndIf

oStruCM0:AddTrigger( "CM0_CODCAT" , "CM0_DCATEG", {|| .T. }, {|oModCM0| GetAdvFVal("C87", "C87_DESCRI", xFilial( "C87" ) + oModCM0:GetValue('CM0_CODCAT'), 1, "") } )

oModel:AddFields('MODEL_CM0', /*cOwner*/, oStruCM0)
oModel:GetModel('MODEL_CM0'):SetPrimaryKey({'CM0_FILIAL', 'CM0_ID', 'CM0_VERSAO'})

oModel:AddGrid('MODEL_CM1', 'MODEL_CM0', oStruCM1)
oModel:GetModel('MODEL_CM1'):SetUniqueLine({'CM1_CODPAR'})    

oModel:AddGrid('MODEL_CM2', 'MODEL_CM0', oStruCM2)
oModel:GetModel('MODEL_CM2'):SetUniqueLine({'CM2_CODAGE'})  

oModel:SetRelation('MODEL_CM1', {{'CM1_FILIAL' , 'xFilial( "CM1" )'}, {'CM1_ID' , 'CM0_ID'}, {'CM1_VERSAO' , 'CM0_VERSAO'}}, CM1->(IndexKey(1)))
oModel:SetRelation('MODEL_CM2', {{'CM2_FILIAL' , 'xFilial( "CM2" )'}, {'CM2_ID' , 'CM0_ID'}, {'CM2_VERSAO' , 'CM0_VERSAO'}}, CM2->(IndexKey(1)))

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Leandro Prado
@since 05/09/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oModel   	:= FWLoadModel( 'TAFA257' )
Local oStruCM0 	:= FWFormStruct( 2, 'CM0' )
Local oStruCM1 	:= FWFormStruct( 2, 'CM1' )
Local oStruCM2 	:= FWFormStruct( 2, 'CM2' )
Local oStruCM0a := Nil
Local oStruCM0b := Nil
Local oView    	:= FWFormView():New()

Local nI		:= 1
Local nY		:= 4

Local aCampos	:= {}
Local aCmpGrp 	:= {}

Local cCmpFil  	:= "" 

oView:SetModel( oModel )

oStruCM0:AddGroup("GRP_REGISTRADOR"	, "Registrador CAT"	, "", 1 ) //"Registrador CAT"
oStruCM0:AddGroup("GRP_CAT" 	 		, "CAT"				, "", 1 ) //"Grupo Atestado"
oStruCM0:AddGroup("GRP_LOCAL"    		, "Local do Acidente", "", 1 ) //"Local do Acidente"
oStruCM0:AddGroup("GRP_ATESTADO" 		, "Atestado M�dico"  , "", 1 ) //"Atestado M�dico"
oStruCM0:AddGroup("GRP_EMITENTE" 		, "Emitente"		  	, "", 1 ) //"Emitente"
oStruCM0:AddGroup("GRP_CATORIGEM"		, "CAT Origem"		, "", 1 ) //"CAT Origem"

cCmpFil  := 'CM0_ID|CM0_TPREG|CM0_DTPRE|CM0_INSREG|CM0_NRIREG|'
aCmpGrp := StrToKArr(cCmpFil,"|")
For nI := 1 to Len(aCmpGrp)
	oStruCM0:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_REGISTRADOR")
Next nI

cCmpFil  := 'CM0_TRABAL|CM0_DTRABA|CM0_DTACID|'
cCmpFil  += 'CM0_TPACID|CM0_DTPACI|CM0_HRACID|CM0_HRTRAB|CM0_TPCAT|'
cCmpFil  += 'CM0_INDOBI|CM0_DTOBIT|CM0_COMPOL|CM0_CODSIT|CM0_DCODSI|CM0_INICAT|CM0_OBSCAT|'
If __lLay0205
	cCmpFil  += 'CM0_CODCAT|CM0_DCATEG'
EndIf
aCmpGrp := StrToKArr(cCmpFil,"|")
For nI := 1 to Len(aCmpGrp)
	oStruCM0:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_CAT")
Next nI

cCmpFil  := 'CM0_TPLOC|CM0_DESLOC|CM0_DESLOG|CM0_NRLOG|CM0_CODPAI|CM0_DCODPA|CM0_UF|CM0_DUF|CM0_CODMUN|'
cCmpFil  += 'CM0_DCODMU|CM0_CNPJLO|CM0_CODPOS|'
If __lLay0205
	cCmpFil  += 'CM0_CODAMB|CM0_TPLOGR|CM0_COMLOG|CM0_BAIRRO|CM0_CEP|CM0_INSACI|CM0_NRIACI|'
EndIf
aCmpGrp := StrToKArr(cCmpFil,"|")
For nI := 1 to Len(aCmpGrp)
	oStruCM0:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_LOCAL")
Next nI

If __lLay0205
	//Ordem dos campos na tela
	oStruCM0:SetProperty( "CM0_TPLOC" 	, MVC_VIEW_ORDEM	, "01"	)
	oStruCM0:SetProperty( "CM0_DESLOC"	, MVC_VIEW_ORDEM	, "02"	)
	oStruCM0:SetProperty( "CM0_CODAMB"	, MVC_VIEW_ORDEM	, "03"	)
	oStruCM0:SetProperty( "CM0_TPLOGR"	, MVC_VIEW_ORDEM	, "04"	)
	oStruCM0:SetProperty( "CM0_DESLOG"	, MVC_VIEW_ORDEM	, "05"	)
	oStruCM0:SetProperty( "CM0_NRLOG" 	, MVC_VIEW_ORDEM	, "06"	)
	oStruCM0:SetProperty( "CM0_COMLOG"	, MVC_VIEW_ORDEM	, "07"	)
	oStruCM0:SetProperty( "CM0_BAIRRO"	, MVC_VIEW_ORDEM	, "08"	)
	oStruCM0:SetProperty( "CM0_CEP"			, MVC_VIEW_ORDEM	, "09"	)
	oStruCM0:SetProperty( "CM0_CODMUN"	, MVC_VIEW_ORDEM	, "10"	)
	oStruCM0:SetProperty( "CM0_DCODMU"	, MVC_VIEW_ORDEM	, "11"	)
	oStruCM0:SetProperty( "CM0_INSACI"	, MVC_VIEW_ORDEM	, "12"	)
	oStruCM0:SetProperty( "CM0_NRIACI"	, MVC_VIEW_ORDEM	, "13"	)

	oStruCM0:SetProperty( "CM0_CODCAT" 	, MVC_VIEW_INSERTLINE, .T.)
	oStruCM0:SetProperty( "CM0_INSACI" 	, MVC_VIEW_INSERTLINE, .T.)
EndIf

cCmpFil  := 'CM0_CODCNE|CM0_DTATEN|CM0_HRATEN|'
cCmpFil  += 'CM0_INDINT|CM0_DURTRA|CM0_INDAFA|CM0_NATLES|CM0_DNATLE|CM0_DESLES|CM0_DIAPRO|'
cCmpFil  += 'CM0_CODCID|CM0_DCODCI|CM0_OBSERV
aCmpGrp := StrToKArr(cCmpFil,"|")
For nI := 1 to Len(aCmpGrp)
	oStruCM0:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_ATESTADO")
Next nI

cCmpFil := 'CM0_CODCID|CM0_DCODCI|CM0_OBSERV|CM0_IDPROF|CM0_DIDPRO|'
aCmpGrp := StrToKArr(cCmpFil,"|")
For nI := 1 to Len(aCmpGrp)
	oStruCM0:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_EMITENTE")
Next nI

cCmpFil  := "CM0_ID|CM0_TRABAL|CM0_DTRABA|CM0_DTACID|CM0_HRACID|CM0_HRTRAB|CM0_TPACID|CM0_TPCAT|CM0_INDOBI|CM0_COMPOL|CM0_CODSIT|CM0_DCODSI|CM0_INICAT|CM0_OBSCAT|" 
aCmpGrp := StrToKArr(cCmpFil,"|")
For nI := 1 to Len(aCmpGrp)
	oStruCM0:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_CAT")
Next nI

cCmpFil  := "CM0_DTCAT|CM0_NRCAT|CM0_DNRCAT|" 
aCmpGrp := StrToKArr(cCmpFil,"|")
For nI := 1 to Len(aCmpGrp)
	oStruCM0:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_CATORIGEM")
Next nI


// Campos do folder do n�mero do ultimo protocolo
cCmpFil   := 'CM0_PROTUL|
oStruCM0b := FwFormStruct( 2, 'CM0', {|x| AllTrim( x ) + "|" $ cCmpFil } )

If FindFunction("TafAjustRecibo")
	TafAjustRecibo(oStruCM0b,"CM0")
EndIf 

oView:AddField( 'VIEW_CM0', oStruCM0, 'MODEL_CM0' )
oView:EnableTitleView( 'VIEW_CM0', STR0001)//'Comunica��o de Acidente de Trabalho'

oView:AddField( 'VIEW_CM0b', oStruCM0b, 'MODEL_CM0' )
If FindFunction("TafNmFolder")
	oView:EnableTitleView( 'VIEW_CM0b', TafNmFolder("recibo",1) ) // "Recibo da �ltima Transmiss�o"  
EndIf 

oView:AddGrid ( 'VIEW_CM1', oStruCM1, 'MODEL_CM1' )
oView:EnableTitleView( 'VIEW_CM1', STR0002) //"Parte Atingida"

oView:AddGrid ( 'VIEW_CM2', oStruCM2, 'MODEL_CM2' )
oView:EnableTitleView( 'VIEW_CM2', STR0003)//'Agente Causador'
	
oView:CreateFolder( 'PASTAS' )
oView:AddSheet( 'PASTAS', 'ABA00', STR0001)//Comunica��o de Acidente de Trabalhol' 

If FindFunction("TafNmFolder")
	oView:AddSheet( 'PASTAS', 'ABA01', TafNmFolder("recibo") )   //"Numero do Recibo"
Else
	oView:AddSheet( 'PASTAS', 'ABA01', STR0017)//'Protocolo da �ltima Transmiss�o'
EndIf 

oView:CreateHorizontalBox( 'FIELDSCM0a', 065,,,'PASTAS','ABA00')  
oView:CreateHorizontalBox( 'FIELDSCM0b', 100,,,'PASTAS','ABA01') 

oView:CreateHorizontalBox("PAINEL_INFERIOR",035,,,"PASTAS","ABA00")
oView:CreateFolder( 'FOLDER_INFERIOR', 'PAINEL_INFERIOR' )

oView:AddSheet( 'FOLDER_INFERIOR', 'ABA01', STR0002)//'Parte Atingida' 
oView:AddSheet( 'FOLDER_INFERIOR', 'ABA02', STR0003)//'Agente Causador'

oView:CreateHorizontalBox( 'GRIDCM1', 100,,,'FOLDER_INFERIOR','ABA01') 
oView:CreateHorizontalBox( 'GRIDCM2', 100,,,'FOLDER_INFERIOR','ABA02') 
	
oView:SetOwnerView( 'VIEW_CM0', 'FIELDSCM0a' )
oView:SetOwnerView( 'VIEW_CM0b', 'FIELDSCM0b' )
oView:SetOwnerView( 'VIEW_CM1', 'GRIDCM1' )
oView:SetOwnerView( 'VIEW_CM2', 'GRIDCM2' )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif .OR. ( FindFunction( "xTafExtmp" ) .And. xTafExtmp() )
	xFunRmFStr(oStruCM0, 'CM0')//Retira campos de controle da visualiza��o da tela 
	oStruCM0:RemoveField('CM0_PROTUL')	
EndIf

If TafColumnPos( "CM0_LOGOPE" )
	oStruCM0:RemoveField( "CM0_LOGOPE" )
EndIf

If __lLay0205
	oStruCM0:RemoveField('CM0_DTCAT')
	oStruCM0:RemoveField('CM0_TPREG')
	oStruCM0:RemoveField('CM0_INSREG')
	oStruCM0:RemoveField('CM0_DTPRE')
	oStruCM0:RemoveField('CM0_NRIREG')
Else
	oStruCM0:RemoveField('CM0_CODAMB')
	oStruCM0:RemoveField('CM0_TPLOGR')
	oStruCM0:RemoveField('CM0_COMLOG')
	oStruCM0:RemoveField('CM0_BAIRRO')
	oStruCM0:RemoveField('CM0_CEP')	
	If TafColumnPos( "CM0_CODCAT" )
		oStruCM0:RemoveField('CM0_CODCAT')	
		oStruCM0:RemoveField('CM0_DCATEG')
	EndIf
EndIf
	
Return oView
//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@Param  oModel -> Modelo de dados

@Return .T.

@Author Fabio V. Santana
@Since 08/10/2013
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel( oModel )

Local cLogOpe	:=	""
Local cLogOpeAnt:=  ""
Local cVerAnt   := ""  
Local cProtocolo:= ""
Local cVersao   := ""  
Local cChvRegAnt:= ""
Local cEvento	:= ""

Local nOperation:= oModel:GetOperation()
Local nlI 		:= 0
Local nlY   	:= 0   

Local aGrava   	:= {}
Local aGravaCM1	:= {}
Local aGravaCM2	:= {}

Local oModelCM0	:= Nil
Local oModelCM1	:= Nil
Local oModelCM2	:= Nil

Local lRetorno	:= .T.

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

Begin Transaction 
	
	If nOperation == MODEL_OPERATION_INSERT

	    TafAjustID( "CM0", oModel)
		
		oModel:LoadValue( 'MODEL_CM0', 'CM0_VERSAO', xFunGetVer() )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_CM0', 'CM0_LOGOPE' , '2', '' )
		Endif

		FwFormCommit( oModel )  
		
	ElseIf nOperation == MODEL_OPERATION_UPDATE

		//�����������������������������������������������������������������Ŀ
		//�Seek para posicionar no registro antes de realizar as validacoes,�
		//�visto que quando nao esta pocisionado nao eh possivel analisar   �
		//�os campos nao usados como _STATUS                                �
		//�������������������������������������������������������������������
	    CM0->( DbSetOrder( 5 ) )
	    If lGoExtemp .OR. CM0->( MsSeek( xFilial( 'CM0' ) + M->CM0_ID + '1' ) )
	    	    
			//��������������������������������Ŀ
			//�Se o registro ja foi transmitido�
			//����������������������������������
		    If CM0->CM0_STATUS $ ( "4" ) 
				
				oModelCM0 := oModel:GetModel( 'MODEL_CM0' )     
				oModelCM1 := oModel:GetModel( 'MODEL_CM1' )     
				oModelCM2 := oModel:GetModel( 'MODEL_CM2' ) 
										
				//�����������������������������������������������������������Ŀ
				//�Busco a versao anterior do registro para gravacao do rastro�
				//�������������������������������������������������������������
				cVerAnt    := oModelCM0:GetValue( "CM0_VERSAO" )				
				cProtocolo := oModelCM0:GetValue( "CM0_PROTUL" )
				cEvento	   := oModelCM0:GetValue( "CM0_EVENTO" )

				If TafColumnPos( "CM0_LOGOPE" )
					cLogOpeAnt := oModelCM0:GetValue( "CM0_LOGOPE" )
				endif

				//�����������������������������������������������������������������Ŀ
				//�Neste momento eu gravo as informacoes que foram carregadas       �
				//�na tela, pois neste momento o usuario ja fez as modificacoes que �
				//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
				//�nao devem ser consideradas neste momento                         �
				//�������������������������������������������������������������������
				For nlI := 1 To 1
					For nlY := 1 To Len( oModelCM0:aDataModel[ nlI ] )			
						Aadd( aGrava, { oModelCM0:aDataModel[ nlI, nlY, 1 ], oModelCM0:aDataModel[ nlI, nlY, 2 ] } )									
					Next
				Next 
				  																			
	
				For nlI := 1 To oModel:GetModel( 'MODEL_CM1' ):Length() 
					oModel:GetModel( 'MODEL_CM1' ):GoLine(nlI)
	
					If !oModel:GetModel( 'MODEL_CM1' ):IsDeleted()								
						aAdd (aGravaCM1 ,{oModelCM1:GetValue('CM1_CODPAR'),;
										  oModelCM1:GetValue('CM1_LATERA')} )
					EndIf					
				Next
				
				For nlI := 1 To oModel:GetModel( 'MODEL_CM2' ):Length() 
					oModel:GetModel( 'MODEL_CM2' ):GoLine(nlI)
					
					If !oModel:GetModel( 'MODEL_CM2' ):IsDeleted()								
						aAdd (aGravaCM2 ,{oModelCM2:GetValue('CM2_CODAGE')} )
					EndIf					
				Next			
				                                                        					                                                     
	
				//�����������������������������������������������������������Ŀ
				//�Seto o campo como Inativo e gravo a versao do novo registro�
				//�no registro anterior                                       � 
				//|                                                           |
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������
				FAltRegAnt( 'CM0', '2' )
				
				//��������������������������������������������������Ŀ
				//�Neste momento eu preciso setar a operacao do model�
				//�como Inclusao                                     �
				//����������������������������������������������������
				oModel:DeActivate()
				oModel:SetOperation( 3 ) 	
				oModel:Activate()		
								
				//�������������������������������������������������������Ŀ
				//�Neste momento eu realizo a inclusao do novo registro ja�
				//�contemplando as informacoes alteradas pelo usuario     �
				//���������������������������������������������������������
				For nlI := 1 To Len( aGrava )	
					oModel:LoadValue( 'MODEL_CM0', aGrava[ nlI, 1 ], aGrava[ nlI, 2 ] )
				Next

				//Necess�rio Abaixo do For Nao Retirar
				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_CM0', 'CM0_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				For nlI := 1 To Len( aGravaCM1 )
					If nlI > 1
						oModel:GetModel( 'MODEL_CM1' ):AddLine()
					EndIf
					oModel:LoadValue( "MODEL_CM1", "CM1_CODPAR", aGravaCM1[nlI][1] )
					oModel:LoadValue( "MODEL_CM1", "CM1_LATERA", aGravaCM1[nlI][2] )
	            Next		
				     
				For nlI := 1 To Len( aGravaCM2 )
					If nlI > 1
						oModel:GetModel( 'MODEL_CM2' ):AddLine()
					EndIf					
					oModel:LoadValue( "MODEL_CM2", "CM2_CODAGE", aGravaCM2[nlI][1] )   
				Next
								
				//�������������������������������Ŀ
				//�Busco a versao que sera gravada�
				//���������������������������������
				cVersao := xFunGetVer()		 
				                                   
				//�����������������������������������������������������������Ŀ		
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������		                                                                      				         
				oModel:LoadValue( 'MODEL_CM0', 'CM0_VERSAO', cVersao )
				oModel:LoadValue( 'MODEL_CM0', 'CM0_VERANT', cVerAnt )
				oModel:LoadValue( 'MODEL_CM0', 'CM0_PROTPN', cProtocolo )
				oModel:LoadValue( 'MODEL_CM0', 'CM0_PROTUL', "" )
				
				// Tratamento para limpar o ID unico do xml
				cAliasPai := "CM0"
				If TAFColumnPos( cAliasPai+"_XMLID" )
					oModel:LoadValue( 'MODEL_'+cAliasPai, cAliasPai+'_XMLID', "" )
				EndIf
				
				If nOperation == MODEL_OPERATION_DELETE 		
					oModel:LoadValue( 'MODEL_CM0', 'CM0_EVENTO', "E" )                                               		                    		
				Else
					If cEvento == "E"
						oModel:LoadValue( 'MODEL_CM0', 'CM0_EVENTO', "I" )
					Else
						oModel:LoadValue( 'MODEL_CM0', 'CM0_EVENTO', "A" )
					EndIf	
				EndIf
				    
			ElseIf CM0->CM0_STATUS == ( "2" )
				TAFMsgVldOp( oModel, "2" )//"Registro n�o pode ser alterado. Aguardando processo da transmiss�o."
				lRetorno := .F.
			Elseif CM0->CM0_STATUS == ( "6" )
				TAFMsgVldOp( oModel, "6" )//"Registro n�o pode ser alterado. Aguardando processo de transmiss�o do evento de Exclus�o S-3000"
				lRetorno := .F.
			ElseIf CM0->CM0_STATUS == "7"
				TAFMsgVldOp( oModel, "7" )//"Registro n�o pode ser alterado, pois o evento de exclus�o j� se encontra na base do RET"
				lRetorno := .F.
			Else 
				//altera��o sem transmiss�o
				If TafColumnPos( "CM0_LOGOPE" )
					cLogOpeAnt := CM0->CM0_LOGOPE
				endif
				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_CM0', 'CM0_LOGOPE' , '' , cLogOpeAnt )
				EndIf
				
				// Limpa campo de Status ap�s altera��o
				If RecLock( "CM0", .F. )
					CM0->CM0_STATUS := " "
					CM0->( MsUnlock() )
				EndIf
										
			Endif

			If lRetorno

				//Gravo altera��o para o Extempor�neo
				If lGoExtemp
					TafGrvExt( oModel, 'MODEL_CM0', 'CM0' )
				Endif

				FwFormCommit( oModel )
			Endif
		
		EndIf						
	ElseIf nOperation == MODEL_OPERATION_DELETE 
	
		cChvRegAnt := CM0->(CM0_ID + CM0_VERANT)
		
		If !Empty( cChvRegAnt )
			TAFAltStat( 'CM0', " " )
			FwFormCommit( oModel )				
			If nOperation == MODEL_OPERATION_DELETE
				If CM0->CM0_EVENTO == "A" .Or. CM0->CM0_EVENTO == "E"
					TAFRastro( 'CM0', 1, cChvRegAnt, .T., , IIF(Type ("oBrw") == "U", Nil, oBrw ))
				EndIf
			EndIf
		Else
			oModel:DeActivate()
			oModel:SetOperation( 5 ) 	
			oModel:Activate()
			FwFormCommit( oModel )
		EndIf
	EndIf    		

End Transaction 

Return ( lRetorno )
 
//-------------------------------------------------------------------
/*/{Protheus.doc} TAF257Xml
Funcao de geracao do XML para atender o registro S-2210
Quando a rotina for chamada o registro deve estar posicionado

@Param:
lRemEmp - Exclusivo do Evento S-1000
cSeqXml - Numero sequencial para composi��o da chave ID do XML

@Return:
cXml - Estrutura do Xml do Layout S-2210

@author Fabio V. Santana
@since 07/10/2013
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAF257Xml(cAlias,nRecno,nOpc,lJob,lRemEmp,cSeqXml)

Local cXmlEmi	:= ""
Local cXml		:= ""
Local cLayout	:= "2210"
Local cReg		:= "CAT"
Local cRegistrador := {}
Local cIdCateg		:= ""
Local cCodCat		:= ""
Local nRecno        := 0
Local lXmlVLd		:= IIF(FindFunction('TafXmlVLD'),TafXmlVLD('TAF257XML'),.T.)


Default lJob   := .F.
Default cSeqXml := ""

If lXmlVLd
	If CM0->CM0_EVENTO $ "I|A"	

		C9V->( DbSetOrder( 1 ) )// No caso do Funcionario fa�o o seek para evitar que sejam efetuados mais de um posicione
		C9V->( MsSeek ( xFilial("C9V")+CM0->CM0_TRABAL) )
		If __lLay0205

			cXml +=		"<ideVinculo>"
			cXml +=	 		xTafTag("cpfTrab",C9V->C9V_CPF )
			cXml +=			xTafTag("nisTrab",C9V->C9V_NIS,,.T. )
			cXml +=			xTafTag("matricula",C9V->C9V_MATRIC,,.T.)

		If C9V->C9V_NOMEVE == "S2300"
			cIdCateg := C9V->C9V_CATCI
			If !Empty( cIdCateg )
				cCodCat := Posicione("C87",1,xFilial("C87") + cIdCateg, "C87_CODIGO")
			EndIf
			cXml +=	xTafTag("codCateg",cCodCat)
		EndIf
		
			cXml +=		"</ideVinculo>"

		Else

			cRegistrador :=		"<ideRegistrador>"
			cRegistrador +=			xTafTag("tpRegistrador",POSICIONE("T0I",1, xFilial("T0I")+CM0->CM0_TPREG ,"T0I_CODIGO") )
			cRegistrador +=			xTafTag("tpInsc",CM0->CM0_INSREG,,.T. )
			cRegistrador +=			xTafTag("nrInsc",CM0->CM0_NRIREG,,.T. )
			cRegistrador +=		"</ideRegistrador>"

			cXml +=		"<ideTrabalhador>"
			cXml +=	 		xTafTag("cpfTrab",C9V->C9V_CPF )
			cXml +=			xTafTag("nisTrab",C9V->C9V_NIS,,.T. )
			cXml +=		"</ideTrabalhador>"

		EndIf

		cXml +=		"<cat>"	
		cXml +=			xTafTag("dtAcid",CM0->CM0_DTACID)
		cXml +=			xTafTag("tpAcid", POSICIONE("LE5", 1, xFilial("LE5")+CM0->CM0_TPACID, "LE5_CODIGO") )

		LE5->(DBSetOrder(1))
		LE5->(MSSeek(xFilial("LE5") + CM0->CM0_TPACID))
		If (!LE5->LE5_CODIGO $ "2.0.01|2.0.02|2.0.03|2.0.04|2.0.05|2.0.06|4.0.01|4.0.02")
			cXml +=			xTafTag("hrAcid",StrTran(CM0->CM0_HRACID, ":", ""),,.T.)
		EndIf

		cXml +=			xTafTag("hrsTrabAntesAcid",StrTran(CM0->CM0_HRTRAB, ":", ""))
		cXml +=			xTafTag("tpCat",CM0->CM0_TPCAT)	
		cXml +=			xTafTag("indCatObito",xFunTrcSN(CM0->CM0_INDOBI))
		cXml +=			xTafTag("dtObito",CM0->CM0_DTOBIT,,.T. )
		cXml +=			xTafTag("indComunPolicia",xFunTrcSN(CM0->CM0_COMPOL))		
		cXml +=			xTafTag("codSitGeradora",POSICIONE("C8L",1, xFilial("C8L")+CM0->CM0_CODSIT ,"C8L_CODIGO"),,.T. )
		cXml +=			xTafTag("iniciatCAT",CM0->CM0_INICAT)
		
		If __lLay0205
			cXml +=			xTafTag("obsCAT",CM0->CM0_OBSCAT,,.T. )
		Else
			cXml +=			xTafTag("observacao",CM0->CM0_OBSCAT,,.T. )
		EndIf
				
		cXml +=			"<localAcidente>"
		cXml +=				xTafTag("tpLocal",CM0->CM0_TPLOC)
		cXml +=				xTafTag("dscLocal",CM0->CM0_DESLOC,,.T. )
		
		If __lLay0205 .And. TafColumnPos( "CM0_CODAMB" ) .And. TafColumnPos( "CM0_TPLOGR" )
			cXml += xTafTag("codAmb", Posicione( "T04", 1, xFilial("T04") + CM0->CM0_CODAMB, "T04_CODIGO" ),,.T.)
			cXml += xTafTag("tpLograd",Posicione("C06",3,xFilial("C06")+CM0->CM0_TPLOGR,"C06_CESOCI"))				
		EndIf
		
		If __lLay0205
			cXml +=				xTafTag("dscLograd",CM0->CM0_DESLOG )
			cXml +=				xTafTag("nrLograd",CM0->CM0_NRLOG )	
		Else
			cXml +=				xTafTag("dscLograd",CM0->CM0_DESLOG,,.T. )
			cXml +=				xTafTag("nrLograd",CM0->CM0_NRLOG,,.T. )	
		EndIf

		If __lLay0205 .And. TafColumnPos( "CM0_COMLOG" ) .And. TafColumnPos( "CM0_BAIRRO" ) .And. TafColumnPos( "CM0_CEP" )
			cXml += xTafTag("complemento",CM0->CM0_COMLOG,,.T.)
			cXml += xTafTag("bairro",CM0->CM0_BAIRRO,,.T.)
			cXml += xTafTag("cep",CM0->CM0_CEP,,.T.)
		EndIf
		
		cXml +=				xTafTag("codMunic",Posicione("C09",3,xFilial("C09")+CM0->CM0_UF,"C09_CODIGO")+POSICIONE("C07",3, xFilial("C07")+CM0->CM0_CODMUN,"C07_CODIGO"),,.T. )
		cXml +=				xTafTag("uf",POSICIONE("C09",3, xFilial("C09")+CM0->CM0_UF,"C09_UF"),,.T. )
		
		If !__lLay0205
			cXml +=				xTafTag("cnpjLocalAcid",CM0->CM0_CNPJLO,,.T. )
		EndIf
		
		cXml +=				xTafTag("pais",POSICIONE("C08",3, xFilial("C08")+CM0->CM0_CODPAI,"C08_PAISSX"),,.T. )
		cXml +=				xTafTag("codPostal",CM0->CM0_CODPOS,,.T. )
		
		If __lLay0205 .And. TafColumnPos( "CM0_INSACI" ) .And. TafColumnPos( "CM0_NRIACI" ) .And. (!Empty(CM0->CM0_INSACI) .Or. !Empty(CM0->CM0_NRIACI))
			cXml += "<ideLocalAcid>"
			cXml +=		xTafTag("tpInsc",CM0->CM0_INSACI )
			cXml +=		xTafTag("nrInsc",CM0->CM0_NRIACI)
			cXml += "</ideLocalAcid>"
		EndIf

		cXml +=			"</localAcidente>"

		CM1->( DbSetOrder( 1 ) )
		CM1->( MsSeek ( xFilial("CM1")+CM0->CM0_ID+CM0->CM0_VERSAO) )
		While !CM1->(Eof()) .And. CM0->(CM0_ID+CM0_VERSAO) == CM1->(CM1_ID+CM1_VERSAO)
		
			cXml +=			"<parteAtingida>"
			cXml +=				xTafTag("codParteAting",POSICIONE("C8I",1, xFilial("C8I")+CM1->CM1_CODPAR,"C8I_CODIGO"))
			cXml +=				xTafTag("lateralidade",CM1->CM1_LATERA)
			cXml +=			"</parteAtingida>"
		
			CM1->(DbSkip())
		EndDo
		
		CM2->( DbSetOrder( 1 ) )
		CM2->( MsSeek ( xFilial("CM2")+CM0->CM0_ID+CM0->CM0_VERSAO) )
		While !CM2->(Eof()) .And. CM0->(CM0_ID+CM0_VERSAO) == CM2->(CM2_ID+CM2_VERSAO)
		
			cXml +=			"<agenteCausador>"
			cXml +=				xTafTag("codAgntCausador",POSICIONE("C8J",1, xFilial("C8J")+CM2->CM2_CODAGE,"C8J_CODIGO"))
			cXml +=			"</agenteCausador>"
		
			CM2->(DbSkip())
		EndDo
		
		CM7->( DbSetOrder( 1 ) )// No caso do Medico fa�o o seek para evitar que sejam efetuados mais de um posicione
		CM7->( MsSeek ( xFilial("CM7")+CM0->CM0_IDPROF) )	
		xTafTagGroup("emitente",{ { "nmEmit"	    ,CM7->CM7_NOME				 							,,.F. }; 
								,  { "ideOC" ,CM7->CM7_IDEOC			                                   ,,.F. };
								,  {	"nrOC" ,CM7->CM7_NRIOC                                  			,,.F. };
								,  {	"ufOC" ,POSICIONE("C09",3, xFilial("C09")+CM7->CM7_NRIUF,"C09_UF") ,,.T. }};
								,  @cXmlEmi)
		
		
		xTafTagGroup("atestado",{ 	 { "codCNES"	    ,CM0->CM0_CODCNE				 													,,.T. }; 
								,  { "dtAtendimento" 	,CM0->CM0_DTATEN				                                   					,,.F. };
								,  {	"hrAtendimento" ,StrTran(CM0->CM0_HRATEN, ":", "")                                  				,,.F. };
								,  {	"indInternacao" ,xFunTrcSN(CM0->CM0_INDINT)                                         				,,.F. };
								,  {	"durTrat"       ,CM0->CM0_DURTRA                                                    				,,.F. };
								,  {	"indAfast"	    ,xFunTrcSN(CM0->CM0_INDAFA)                                        					,,.F. };
								,  {	"dscLesao"      ,POSICIONE("C8M",1, xFilial("C8M")+CM0->CM0_NATLES,"C8M_CODIGO")  					,,.T. };
								,  {	"dscCompLesao"  ,CM0->CM0_DESLES                                                   					,,.T. };
								,  {	"diagProvavel"  ,CM0->CM0_DIAPRO                                                    				,,.T. };
								,  {	"codCID"  	    ,StrTran(POSICIONE("CMM",1, xFilial("CMM")+CM0->CM0_CODCID,"CMM_CODIGO"), ".", "")  ,,.F. };
								,  {	"observacao"    ,CM0->CM0_OBSERV                                                    				,,.T. }};
								,  @cXml, { {"emitente", cXmlEmi, 1 } })
		If __lLay0205 .AND. CM0->CM0_TPCAT $'2|3'
			nRecno := CM0->(Recno())
			xTafTagGroup("catOrigem"	, {{	 "nrRecCatOrig",POSICIONE("CM0",5, xFilial("CM0")+ Padr(CM0->CM0_NRCAT, TamSx3("CM0_ID")[1]) + "1","CM0_PROTUL"),,.F.}};
										,@cXml)
			CM0->(DbGoTo(nRecno))
		Else
			xTafTagGroup("catOrigem"	, {	 { 	"dtCatOrig",CM0->CM0_DTCAT	,,.F.};
										, {   	"nrCatOrig",CM0->CM0_NRCAT 	,,.T.}};
										,@cXml)
		EndIf

		cXml +=	"</cat>"	

	EndIf
		
	//����������������������Ŀ
	//�Estrutura do cabecalho�

	cXml := xTafCabXml(cXml,"CM0", cLayout,cReg,,cSeqXml,cRegistrador)

	//����������������������������Ŀ
	//�Executa gravacao do registro�
	//������������������������������
	If !lJob
		xTafGerXml(cXml,cLayout)
	EndIf
EndIf
Return(cXml) 
//-------------------------------------------------------------------
/*/{Protheus.doc} TAF257Rul           

Regras para gravacao das informacoes do registro S-2210 do E-Social

@Param
nOper      - Operacao a ser realizada ( 3 = Inclusao / 4 = Alteracao / 5 = Exclusao )

@Return	
aRull  - Regras para a gravacao das informacoes


@author Fabio V. Santana
@since 07/10/2013
@version 1.0

/*/                        	
//-------------------------------------------------------------------
Static Function TAF257Rul( cInconMsg, nSeqErrGrv, cCodEvent, cOwner )                               

Local aRull 	  := {}     
Local aInfComp	  := {} 
Local aIncons	  := {}   
Local cNrOC		  := ""
Local cTpCat	  := ""
Local cLograd	  := ""
Local cCodAmb	  := ""
Local cIdCatAnt   := ""

Default cInconMsg	:= ""
Default nSeqErrGrv	:= 0
Default cCodEvent	:= ""
Default cOwner		:= ""
                                 
If __lLay0205
	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/ideVinculo/cpfTrab"))
		Aadd( aRull, {"CM0_TRABAL", FGetIdInt( "cpfTrab", "", "/eSocial/evtCAT/ideVinculo/cpfTrab",,,,@cInconMsg, @nSeqErrGrv), "C", .T. } )   
	EndIf
	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/ideVinculo/codCateg"))
		cCodCat :=  FTafGetVal( "/eSocial/evtCAT/ideVinculo/codCateg","C", .F., aIncons, .F.) 
		aAdd( aRull, { "CM0_CODCAT"	, POSICIONE("C87",2, xFilial("C87")+cCodCat,"C87_ID")	, "C", .T. } ) // Grava o ID da categoria
	EndIf
Else
	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/ideRegistrador/tpRegistrador"))
		Aadd( aRull, {"CM0_TPREG", FGetIdInt( "tpRegistrador", "", "/eSocial/evtCAT/ideRegistrador/tpRegistrador",,,,@cInconMsg, @nSeqErrGrv), "C", .T. } )
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/ideRegistrador/tpInsc"))
		Aadd( aRull, {"CM0_INSREG","/eSocial/evtCAT/ideRegistrador/tpInsc","C", .F.} )
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/ideRegistrador/nrInsc")) 
		Aadd( aRull, {"CM0_NRIREG","/eSocial/evtCAT/ideRegistrador/nrInsc","C", .F.} )
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/ideTrabalhador/cpfTrab"))
		Aadd( aRull, {"CM0_TRABAL", FGetIdInt( "cpfTrab", "", "/eSocial/evtCAT/ideTrabalhador/cpfTrab",,,,@cInconMsg, @nSeqErrGrv), "C", .T. } )   
	EndIf
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/dtAcid"))
	Aadd( aRull, {"CM0_DTACID","/eSocial/evtCAT/cat/dtAcid","D", .F.} ) 	
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/tpAcid"))
	Aadd( aRull, {"CM0_TPACID",FGetIdInt( "tpAcid", "", "/eSocial/evtCAT/cat/tpAcid",,,,@cInconMsg, @nSeqErrGrv), "C", .T. } )
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/hrAcid"))
	Aadd( aRull, {"CM0_HRACID","/eSocial/evtCAT/cat/hrAcid","C", .F.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/hrsTrabAntesAcid"))
	Aadd( aRull, {"CM0_HRTRAB","/eSocial/evtCAT/cat/hrsTrabAntesAcid","C", .F.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/tpCat"))
	Aadd( aRull, {"CM0_TPCAT" ,"/eSocial/evtCAT/cat/tpCat","C", .F.} )
	cTpCat := FTafGetVal( "/eSocial/evtCAT/cat/tpCat","C", .F., aIncons, .F.)
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ( "/eSocial/evtCAT/cat/indCatObito"))
	Aadd( aRull, {"CM0_INDOBI", +; 
					xFunTrcSN( TAFExisTag( "/eSocial/evtCAT/cat/indCatObito" ) ,2), "C", .T. } )
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/dtObito"))
	Aadd( aRull, {"CM0_DTOBIT","/eSocial/evtCAT/cat/dtObito","D", .F.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/indComunPolicia"))
	Aadd( aRull, { "CM0_COMPOL", +; 
					xFunTrcSN( TAFExisTag( "/eSocial/evtCAT/cat/indComunPolicia" ) ,2), "C", .T. } )
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/codSitGeradora"))
	Aadd( aRull,  { "CM0_CODSIT", +;
				 	FGetIdInt( "codSitGeradora", "", +;
    			 	"/eSocial/evtCAT/cat/codSitGeradora",,,,@cInconMsg, @nSeqErrGrv), "C", .T. } ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/iniciatCAT"))
	Aadd( aRull, {"CM0_INICAT" ,"/eSocial/evtCAT/cat/iniciatCAT","C", .F.} )
EndIf

// Vers�o 2.5 alterado descri��o do campo observacao
If 	__lLay0205
	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/obsCAT"))
		Aadd( aRull, {"CM0_OBSCAT"	,"/eSocial/evtCAT/cat/obsCAT","C", .F.} )
	EndIf
Else
	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/observacao"))
		Aadd( aRull, {"CM0_OBSCAT"	,"/eSocial/evtCAT/cat/observacao","C", .F.} )
	EndIf
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/tpLocal"))	                                                                           
	Aadd( aRull, {"CM0_TPLOC"	,"/eSocial/evtCAT/cat/localAcidente/tpLocal","C", .F.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/dscLocal"))
	Aadd( aRull, {"CM0_DESLOC"	,"/eSocial/evtCAT/cat/localAcidente/dscLocal","C", .F.} ) 
Else
	Aadd( aRull, {"CM0_DESLOC"	,"","C", .T.} ) 
EndIf

If __lLay0205
	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/codAmb"))
		cCodAmb := FTAFGetVal( "/eSocial/evtCAT/cat/localAcidente/codAmb"	, "C", .F.,, .F. )
		Aadd( aRull, {"CM0_CODAMB"	,Posicione( "T04", 4, xFilial("T04") + cCodAmb, "T04_ID" ),"C", .T.} ) 
	Else
		Aadd( aRull, {"CM0_CODAMB"	,"","C", .T.} ) 
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/tpLograd"))
		cLograd := FTAFGetVal( "/eSocial/evtCAT/cat/localAcidente/tpLograd"	, "C", .F.,, .F. )
		Aadd( aRull, {"CM0_TPLOGR"	,Posicione("C06",4,xFilial("C06")+cLograd,"C06_ID"),"C", .T.} ) 
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/ideLocalAcid/tpInsc"))
		Aadd( aRull, {"CM0_INSACI"	,"/eSocial/evtCAT/cat/localAcidente/ideLocalAcid/tpInsc","C", .F.} ) 
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/ideLocalAcid/nrInsc"))	
		Aadd( aRull, {"CM0_NRIACI"	,"/eSocial/evtCAT/cat/localAcidente/ideLocalAcid/nrInsc","C", .F.} ) 
	EndIf

EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/dscLograd"))
	Aadd( aRull, {"CM0_DESLOG"	,"/eSocial/evtCAT/cat/localAcidente/dscLograd","C", .F.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/nrLograd"))
	Aadd( aRull, {"CM0_NRLOG"	,"/eSocial/evtCAT/cat/localAcidente/nrLograd","C", .F.} ) 
EndIf

If __lLay0205 
	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/complemento"))
		Aadd( aRull, {"CM0_COMLOG"	,"/eSocial/evtCAT/cat/localAcidente/complemento","C", .F.} )
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/bairro"))
		Aadd( aRull, {"CM0_BAIRRO"	,"/eSocial/evtCAT/cat/localAcidente/bairro","C", .F.} ) 
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/cep"))
		Aadd( aRull, {"CM0_CEP"	,"/eSocial/evtCAT/cat/localAcidente/cep","C", .F.} ) 
	EndIf
EndIf


If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/uf"))
	Aadd( aRull,  { "CM0_UF", +;
				 	FGetIdInt( "uf", "", +;
    			 	"/eSocial/evtCAT/cat/localAcidente/uf",,,,@cInconMsg, @nSeqErrGrv) , "C", .T. } ) 		
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/codMunic"))   			 	
	Aadd( aRull, {"CM0_CODMUN", +; 
					FGetIdInt( "codMunic", "uf", +; 
					"/eSocial/evtCAT/cat/localAcidente/uf", +; 
					"/eSocial/evtCAT/cat/localAcidente/codMunic",,,@cInconMsg, @nSeqErrGrv), "C", .T. } )
EndIf

If !__lLay0205 .And. TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/cnpjLocalAcid"))
	Aadd( aRull, {"CM0_CNPJLO"	,"/eSocial/evtCAT/cat/localAcidente/cnpjLocalAcid","C", .F.} )  	
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/pais"))
	Aadd( aRull, {"CM0_CODPAI"  , FGetIdInt( "codPais", "", "/eSocial/evtCAT/cat/localAcidente/pais",,,,@cInconMsg, @nSeqErrGrv), "C", .T. } )
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/localAcidente/codPostal"))
	Aadd( aRull, {"CM0_CODPOS"	,"/eSocial/evtCAT/cat/localAcidente/codPostal","C", .F.} )  
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/codCNES"))
	Aadd( aRull, {"CM0_CODCNE"	,"/eSocial/evtCAT/cat/atestado/codCNES","C", .F.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/dtAtendimento"))
	Aadd( aRull, {"CM0_DTATEN"	,"/eSocial/evtCAT/cat/atestado/dtAtendimento","D", .F.} ) 
Endif

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/hrAtendimento"))
	Aadd( aRull, {"CM0_HRATEN"	,"/eSocial/evtCAT/cat/atestado/hrAtendimento","C", .F.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/indInternacao"))
	Aadd( aRull, {"CM0_INDINT", +; 
					xFunTrcSN( TAFExisTag( "/eSocial/evtCAT/cat/atestado/indInternacao" ) ,2),"C", .T.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/durTrat"))
	Aadd( aRull, {"CM0_DURTRA"	,"/eSocial/evtCAT/cat/atestado/durTrat","C", .F.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/indAfast"))
	Aadd( aRull, {"CM0_INDAFA", +; 
						xFunTrcSN( TAFExisTag( "/eSocial/evtCAT/cat/atestado/indAfast" ) ,2),"C", .T.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/dscLesao"))
	Aadd( aRull, {"CM0_NATLES"	,FGetIdInt( "dscLesao", "", "/eSocial/evtCAT/cat/atestado/dscLesao",,,,@cInconMsg, @nSeqErrGrv),"C", .T.} )
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/dscCompLesao"))
	Aadd( aRull, {"CM0_DESLES"	,"/eSocial/evtCAT/cat/atestado/dscCompLesao","C", .F.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/diagProvavel"))
	Aadd( aRull, {"CM0_DIAPRO"	,"/eSocial/evtCAT/cat/atestado/diagProvavel","C", .F.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/codCID"))
	Aadd( aRull, {"CM0_CODCID"	,FGetIdInt( "codCID", "", "/eSocial/evtCAT/cat/atestado/codCID",,,,@cInconMsg, @nSeqErrGrv),"C", .T.} ) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/observacao"))
	Aadd( aRull, {"CM0_OBSERV"	,"/eSocial/evtCAT/cat/atestado/observacao","C", .F.} ) 
EndIf

/*
Cadastro do m�dico
*/
If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/emitente/ufOC"))
	Aadd( aInfComp,{'CM7_NRIUF',FGetIdInt( "uf", "", +;
						"/eSocial/evtCAT/cat/atestado/emitente/ufOC",,,,@cInconMsg, @nSeqErrGrv)}) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/emitente/nmEmit"))		
	Aadd( aInfComp,{'CM7_NOME',;
			FTafGetVal( "/eSocial/evtCAT/cat/atestado/emitente/nmEmit",;
				"C", .F., aIncons, .F.)}) 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/emitente/ideOC"))		 
	Aadd( aInfComp,{'CM7_IDEOC',;
			FTafGetVal( "/eSocial/evtCAT/cat/atestado/emitente/ideOC",;
				"C", .F., aIncons, .F.)}) 		 
EndIf

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/emitente/nrOC"))		 
	Aadd( aInfComp,{'CM7_NRIOC',;
			FTafGetVal( "/eSocial/evtCAT/cat/atestado/emitente/nrOC",;
				"C", .F., aIncons, .F.)}) 
EndIf
/*
Cadastro do m�dico
*/
			
//Preenchendo array com informa��es que dever�o ser gravadas caso n�o encontre o m�dico.
//cNrOC:= FGetIdInt( "nrIOC", "", "/eSocial/evtCAT/cat/atestado/emitente/nrOc",,, aInfComp, @cInconMsg, @nSeqErrGrv)
//Fim do preenchimento do array auxiliar.

If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/atestado/emitente/nrOC"))
	Aadd( aRull, {"CM0_IDPROF", FGetIdInt( "nrOC", "", "/eSocial/evtCAT/cat/atestado/emitente/nrOC",,,aInfComp,@cInconMsg, @nSeqErrGrv), "C", .T. } )   
EndIf

If __lLay0205 .AND. FTafGetVal( "/eSocial/evtCAT/cat/tpCat","C", .F., , .F.) $ '2|3'
	// Preenche o registro que indica a CAT de origem, no caso de CAT de reabertura ou de comunica��o de �bito
	cNrRecCat := TAFA257NrRec("/eSocial/evtCAT/",cCodEvent,cOwner)
	cIdCatAnt := Posicione("CM0",6, xFilial("CM0") + cNrRecCat, "CM0_ID")
	Aadd( aRull, {"CM0_NRCAT"	, cIdCatAnt ,"C", .T.} ) 
Else
	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/catOrigem/dtCatOrig"))
		Aadd( aRull, {"CM0_DTCAT"	,"/eSocial/evtCAT/cat/catOrigem/dtCatOrig","D", .F.} ) 											
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, ("/eSocial/evtCAT/cat/catOrigem/nrCatOrig"))
		Aadd( aRull, {"CM0_NRCAT"	,"/eSocial/evtCAT/cat/catOrigem/nrCatOrig","C", .F.} ) 											
	EndIf
EndIf

Return ( aRull )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF257Grv    
Funcao de gravacao para atender o registro S-2210

@parametros:                
cLayout - Nome do Layout que esta sendo enviado, existem situacoes onde o mesmo fonte
          alimenta mais de um regsitro do E-Social, para estes casos serao necessarios
          tratamentos de acordo com o layout que esta sendo enviado.
nOpc   -  Opcao a ser realizada ( 3 = Inclusao, 4 = Alteracao, 5 = Exclusao )
cFilEv -  Filial do ERP para onde as informacoes deverao ser importadas
oDados -  Objeto com as informacoes a serem manutenidas ( Outras Integracoes )  

@Return    
lRet    - Variavel que indica se a importacao foi realizada, ou seja, se as 
		  informacoes foram gravadas no banco de dados
aIncons - Array com as inconsistencias encontradas durante a importacao 

@author Fabio V Santana
@since 26/09/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Function TAF257Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID )   

Local cLogOpeAnt 		:= ""
Local cCmpsNoUpd  		:= "|CM0_FILIAL|CM0_ID|CM0_VERSAO|CM0_VERANT|CM0_PROTUL|CM0_PROTPN|CM0_EVENTO|CM0_STATUS|CM0_ATIVO|"
Local cInconMsg			:= ""
Local cIdFunc			:= ""
Local cCodEvent 		:= Posicione("C8E",2,xFilial("C8E")+"S-"+cLayout,"C8E->C8E_ID")
Local cNrRecCat         := ""
Local cIdCatAnt         := ""

Local nlI				:= 0  
Local nJ				:= 0
Local nSeqErrGrv		:= 0

Local lRet				:= .F. 	
		
Local aIncons			:= {} 			
Local aRules			:= {} 
Local aChave			:= {}

Local oModel  			:= Nil  

Private lVldModel		:= .T. //Caso a chamada seja via integracao seto a variavel de controle de validacao como .T.
Private oDados			:= oXML

Default cLayout			:= ""
Default nOpc			:= 1 			
Default cFilEv			:= ""			
Default oXML			:= Nil 
Default cOwner			:= ""
Default cFilTran		:= ""
Default cPredeces		:=	""
Default nTafRecno		:=	0
Default cComplem		:=	""
Default cGrpTran		:=	""
Default cEmpOriGrp		:=	""
Default cFilOriGrp		:=	""
Default cXmlID			:=	""

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

//------------------------------------------------------------------------------------------
// Atribuicao neste ponto pois precisa da variavel oDados declarada para uso na FGetIdInt()
//------------------------------------------------------------------------------------------
If __lLay0205
	cIdFunc := FGetIdInt("cpfTrab","","/eSocial/evtCAT/ideVinculo/cpfTrab")
Else
	cIdFunc := FGetIdInt("cpfTrab","","/eSocial/evtCAT/ideTrabalhador/cpfTrab")
EndIf
//�����������������Ŀ
//�Chave do registro�
//�������������������
Aadd( aChave, {"C", "CM0_TRABAL"  ,cIdFunc					                , .T.	} )
Aadd( aChave, {"D", "CM0_DTACID","/eSocial/evtCAT/cat/dtAcid"               , .F.	} ) 	
Aadd( aChave, {"C", "CM0_HRACID","/eSocial/evtCAT/cat/hrAcid"               , .F.	} )
Aadd( aChave, {"C", "CM0_TPCAT","/eSocial/evtCAT/cat/tpCat"                 , .F.	} )

If __lLay0205 .AND. FTafGetVal( "/eSocial/evtCAT/cat/tpCat","C", .F., , .F.) $'2|3'
	cNrRecCat   := TAFA257NrRec("/eSocial/evtCAT/",cCodEvent,cOwner)
	cIdCatAnt   := Posicione("CM0",6, xFilial("CM0") + cNrRecCat, "CM0_ID")
	
    Aadd( aChave, {"C", "CM0_NRCAT", cIdCatAnt, .T.	} )
EndIf
		
Begin Transaction	
	
	//�������������������������������������������������������������Ŀ
	//�Funcao para validar se a operacao desejada pode ser realizada�
	//���������������������������������������������������������������
	If FTafVldOpe( 'CM0', Iif( __lLay0205 .AND. FTafGetVal( "/eSocial/evtCAT/cat/tpCat","C", .F., , .F.) $'2|3', 7, 4 ) , @nOpc, cFilEv, @aIncons, aChave, @oModel, 'TAFA257', cCmpsNoUpd )		    	      				     		    	      	    		    		    		    		    																					

			If TafColumnPos( "CM0_LOGOPE" )
				cLogOpeAnt := CM0->CM0_LOGOPE
			endif

			//���������������������������������������������������������������Ŀ
			//�Carrego array com os campos De/Para de gravacao das informacoes�
			//�����������������������������������������������������������������
			aRules := TAF257Rul(@cInconMsg, @nSeqErrGrv, cCodEvent, cOwner )								                
	        		
			//����������������������������������������������������������������Ŀ
			//�Quando se tratar de uma Exclusao direta apenas preciso realizar �
			//�o Commit(), nao eh necessaria nenhuma manutencao nas informacoes�
			//������������������������������������������������������������������
			If nOpc <> 5 
                
				oModel:LoadValue( "MODEL_CM0", "CM0_FILIAL", CM0->CM0_FILIAL )              

				If TAFColumnPos( "CM0_XMLID" )
					oModel:LoadValue( "MODEL_CM0", "CM0_XMLID", cXmlID )
				EndIf				
    
				//����������������������������������������Ŀ
				//�Rodo o aRules para gravar as informacoes�
				//������������������������������������������
				For nlI := 1 To Len( aRules )                 					
				 	oModel:LoadValue( "MODEL_CM0", aRules[ nlI, 01 ], FTafGetVal( aRules[ nlI, 02 ], aRules[nlI, 03], aRules[nlI, 04], @aIncons, .F. ) )
				Next

				If Findfunction("TAFAltMan")
					if nOpc == 3
						TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_CM0', 'CM0_LOGOPE' , '1', '' )
					elseif nOpc == 4
						TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_CM0', 'CM0_LOGOPE' , '', cLogOpeAnt )
					EndIf
				EndIf
				
				//��������������Ŀ
				//�Parte Atingida�
				//����������������
				//����������������������������������������������������������������Ŀ
				//�Quando se trata de uma alteracao, deleto todas as linhas do Grid�
				//������������������������������������������������������������������							
				If nOpc == 4
					For nJ := 1 to oModel:GetModel( 'MODEL_CM1' ):Length()
						oModel:GetModel( 'MODEL_CM1' ):GoLine(nJ)
						oModel:GetModel( 'MODEL_CM1' ):DeleteLine()
					Next nJ
				EndIf
                
				//������������������������������������������������������������Ŀ
				//�Rodo o XML parseado para gravar as novas informacoes no GRID�
				//��������������������������������������������������������������
				nJ := 1
				While oDados:XPathHasNode('/eSocial/evtCAT/cat/parteAtingida[' + CVALTOCHAR(nJ)+ "]" )
										
					oModel:GetModel( 'MODEL_CM1' ):LVALID	:= .T.					
	
					If nOpc == 4 .Or. nJ > 1
						oModel:GetModel( 'MODEL_CM1' ):AddLine()
    				EndIf
			
				   	If oDados:XPathHasNode( "/eSocial/evtCAT/cat/parteAtingida[" + CVALTOCHAR(nJ) + "]/codParteAting" )
						oModel:LoadValue( "MODEL_CM1", "CM1_CODPAR", FGetIdInt( "codParteAting", "", "/eSocial/evtCAT/cat/parteAtingida[" + CVALTOCHAR(nJ) + "]/codParteAting",,,,@cInconMsg, @nSeqErrGrv) )
				 	EndIF
				 	
				 	If oDados:XPathHasNode( "/eSocial/evtCAT/cat/parteAtingida[" + CVALTOCHAR(nJ) + "]/lateralidade" )
				 		oModel:LoadValue( "MODEL_CM1", "CM1_LATERA", FTafGetVal( "/eSocial/evtCAT/cat/parteAtingida[" + CVALTOCHAR(nJ) + "]/lateralidade", "C", .F., @aIncons, .F. ))
	              EndIf
	                 
					nJ++

				EndDo				 
				
				//���������������Ŀ
				//�Agente Causador�
				//�����������������
				//����������������������������������������������������������������Ŀ
				//�Quando se trata de uma alteracao, deleto todas as linhas do Grid�
				//������������������������������������������������������������������			
				If nOpc == 4		
					For nJ := 1 to oModel:GetModel( 'MODEL_CM2' ):Length()
						oModel:GetModel( 'MODEL_CM2' ):GoLine(nJ)
						oModel:GetModel( 'MODEL_CM2' ):DeleteLine()
					Next nJ
				EndIf				

				//������������������������������������������������������������Ŀ
				//�Rodo o XML parseado para gravar as novas informacoes no GRID�
				//��������������������������������������������������������������				
				nJ := 1
				While oDados:XPathHasNode('/eSocial/evtCAT/cat/agenteCausador[' + CVALTOCHAR(nJ)+ "]" )

					oModel:GetModel( 'MODEL_CM2' ):LVALID	:= .T.					
					
					If nOpc == 4 .Or. nJ > 1
						oModel:GetModel( 'MODEL_CM2' ):AddLine()
					EndIf
					
					If oDados:XPathHasNode( "/eSocial/evtCAT/cat/agenteCausador[" + CVALTOCHAR(nJ) + "]/codAgntCausador" )
						oModel:LoadValue( "MODEL_CM2", "CM2_CODAGE", FGetIdInt( "codAgntCausador", "", "/eSocial/evtCAT/cat/agenteCausador[" + CVALTOCHAR(nJ) + "]/codAgntCausador",,,,@cInconMsg, @nSeqErrGrv) ) 
	              EndIf
	              
					nJ++

				EndDo	
						
			EndIf
			
			///���������������������������Ŀ
			//�Efetiva a operacao desejada�
			//�����������������������������
			If Empty(cInconMsg)	.And. Empty(aIncons)
				If TafFormCommit( oModel )
					Aadd(aIncons, "ERRO19")
				Else
					lRet := .T.
				EndIf	 
			Else			
				Aadd(aIncons, cInconMsg)		
				DisarmTransaction()
			EndIf								 			   
	
			oModel:DeActivate()	
			If FindFunction('TafClearModel')
				TafClearModel(oModel)
			EndIf         			    								 				
	EndIf   
	                                                                        	
End Transaction  	

//����������������������������������������������������������Ŀ
//�Zerando os arrays e os Objetos utilizados no processamento�
//������������������������������������������������������������
aSize( aRules, 0 ) 
aRules	:= Nil

aSize( aChave, 0 ) 
aChave	:= Nil    

Return { lRet, aIncons }  

//-------------------------------------------------------------------
/*/{Protheus.doc} Taf257Vld
Funcao que valida os dados do registro posicionado,
verificando se ha incoerencias nas informacos 
caso seja necessario gerar um XML

lJob - Informa se foi chamado por Job

@return .T.

@author Fabio V. Santana
@since 05/09/2013
@version 1.0
/*/                                                                                                                                          
//-------------------------------------------------------------------
Function Taf257Vld(cAlias,nRecno,nOpc,lJob)

Local aCampos 	:= {} //{Campo do registro filho/consulta padrao , opcao de validacao , opcao do combo(opcional)}
Local aLogErro	:= {} 
Local cChave 	:= ""
Local cChvWhile := "" 
Local nRecnoCM0	:= 0
Local aDadosUtil:= {}
Local cCGC      := AllTrim(SM0->M0_CGC)
Local cTpInscr  := Iif(Len(cCgc) == 14,"1","2") 
Local cIdCateg	:= ""
Local cCodCat	:= ""
Local lValid	:= .T.
Local lVlsSST	:= SuperGetMv('MV_TAFSST',.F.,.F.) // Par�metro que liga altera��es referentes ao SST NDE 01.2018             
Default lJob   	:= .F.

//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := CM0->( Recno() ) 
     
If (CM0->CM0_STATUS) $ " 1"

    //REGRA_EXISTE_CAT_OBITO_ANTERIOR
    aAdd(aDadosUtil, {CM0->CM0_TRABAL, CM0->CM0_DTACID})  
    xVldRegra("CM0","REGRA_EXISTE_CAT_OBITO_ANTERIOR",@aLogErro, aDadosUtil,"CM0") 
    
    //REGRA_EXISTE_INFO_EMPREGADOR
    aDadosUtil	:= {}
    aAdd(aDadosUtil, {CM0->CM0_DTACID, CM0->CM0_DTACID, CM0->CM0_FILIAL}) 
    xVldRegra("C1E","REGRA_EXISTE_INFO_EMPREGADOR",@aLogErro, aDadosUtil,"CM0") 
    
	If Empty(DTOS(CM0->CM0_DTACID))                                           
		//Data obrigatoria para a geracao do XML
		AADD(aLogErro,{"CM0_DTACID","000009","CM0", nRecno }) 	//STR0009 - "Data Inconsistente ou Vazia" 
	Else
		//REGRA_RETIFICA_DT_ACIDENTE  																						
		//Em caso de retifica��o do evento de ASO para alterar a {dtAcid}, verificar a exist�ncia de evento subsequente
		//de afastamento em decorr�ncia de acidente de trabalho                                                             		
		//Neste caso somente verifico a data de afastamento se for uma alteracao
		If CM0->CM0_EVENTO == "A"	        
			CM6->(DBSetOrder(2))		
			If CM6->(msSeek(xFilial("CM6")+CM0->CM0_TRABAL))   		
				cChvWhile := CM0->CM0_TRABAL	   			
				Do While !("CM6")->(Eof ()) .And. cChvWhile == CM6->CM6_FUNC
					//Comparo a data do acidente com a data do afastamento.         
					//A data do acidente nao deve ser superior a data de afastamento
					If DTOS(CM0->CM0_DTACID) > DTOS(CM6->CM6_DTAFAS)
						AADD(aLogErro,{"CM6_DTAFAS", "000013", "CM0", nRecno })   										
			       EndIf			        
				Exit
		 		EndDo		 		
			EndIf 			
		EndIf
	EndIf
	
	//A data da CAT anterior nao eh obrigatoria, soh valido se estiver preenchido
	If !Empty(CM0->CM0_DTCAT) 
		If Len(DTOS(CM0->CM0_DTCAT)) < 8
			AADD(aLogErro,{"CM0_DTCAT", "000009", "CM0", nRecno }) // "Data Inconsistente ou Vazia"    
		EndIf
	EndIf
	
	  //Em caso de Reabertura/�bito, a CAT s� deve ser enviada atrav�s do E-Social se a data da ocorr�ncia for superior a Julho/2019    
    If !Empty(CM0->CM0_DTACID) .AND. (CM0->CM0_TPCAT) <> '1'
        If DTOS(CM0->CM0_DTACID) < '20190701'
            AADD(aLogErro,{"CM0_DTACID", "001210", "CM0", nRecno }) // "Em caso de Reabertura/�bito, a CAT s� deve ser enviada atrav�s do E-Social se a data da ocorr�ncia for superior a Julho/2019."     
        EndIf
    EndIf	

	//Se indica �bito igual a SIM devo validar se a data do �bito � maior ou igual a data do acidente.
	If !Empty(CM0->CM0_DTOBIT)
		If CM0->CM0_DTOBIT < CM0->CM0_DTACID
			AADD(aLogErro,{"CM0_DTOBIT", "001149", "CM0", nRecno }) //"Existe um Evento de Afastamento Tempor�rio com Data Anterior a Data de Acidente Informada."	
		EndIf
	EndIf
	 
	//REGRA_RETIFICA_DT_ACIDENTE
	IF !(TafVDCAT(CM0->CM0_TRABAL,CM0->CM0_DTACID,.F.))
		AADD(aLogErro,{"CM0_DTACID", "000854", "CM0", nRecno }) //"Existe um Evento de Afastamento Tempor�rio com Data Anterior a Data de Acidente Informada."
	EndIf
	
	If !Empty(CM0->CM0_DTACID)
		If (CM0->CM0_DTACID > Date())
			aAdd(aLogErro,{"CM0_DTACID","000730","CM0",nRecno}) // "Deve ser uma data posterior a data de admiss�o do trabalhador e n�o pode ser superior � data atual."
		EndIf
		
		DBSelectArea("C9V")
		C9V->(DBSetOrder(1))
		IF C9V->(MSSeek(xFilial("C9V") + ALLTRIM(CM0->CM0_TRABAL)))
			DBSelectArea("CUP")
			CUP->(DBSetOrder(1))	
			IF CUP->CUP_FILIAL + CUP->CUP_ID + CUP->CUP_VERSAO == xFILIAL("C9V") + C9V->C9V_ID + C9V->C9V_VERSAO
				IF CM0->CM0_DTACID < CUP->CUP_DTADMI
					aAdd(aLogErro,{"CM0_DTACID","000730","CM0",nRecno}) //"Deve ser uma data posterior a data de admiss�o do trabalhador e n�o pode ser superior � data atual." 
				Endif
			Endif
		Endif
	EndIf
	
	//tpAcid Valida��o: Dever ser um c�digo da tabela 24.
	LE5->(DBSetOrder(1))
	If !LE5->(MSSeek(xFilial("LE5") + CM0->CM0_TPACID))
		aAdd(aLogErro,{"CM0_TPACID","000010","CM0",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
	EndIf
	
	//hrsTrabAntesAcid Valida��o: Deve ser um valor entre 00:00 e 23:59.
	If Len(CM0->CM0_HRTRAB) = 4
		If !((Val(SubStr(CM0->CM0_HRTRAB,1,2)) < 24 .And. Val(SubStr(CM0->CM0_HRTRAB,1,2)) >= 0) .And. (Val(SubStr(CM0->CM0_HRTRAB,3,2)) < 60 .And. Val(SubStr(CM0->CM0_HRTRAB,3,2)) >= 0))
			AADD(aLogErro,{"CM0_HRTRAB", "000007", "CM0", nRecno }) //STR0007 - "Hora Inconsistente ou Vazia" 
		EndIf
	Else
		AADD(aLogErro,{"CM0_HRTRAB", "000007", "CM0", nRecno }) //STR0007 - "Hora Inconsistente ou Vazia" 	
	EndIf
	
	//hrAcid Valida��o: Deve ser um valor entre 00:00 e 23:59.
	If __lLay0205 
	
		LE5->(DBSetOrder(1))
		LE5->(MSSeek(xFilial("LE5") + CM0->CM0_TPACID))
		
		If (!LE5->LE5_CODIGO $ "2.0.01|2.0.02|2.0.03|2.0.04|2.0.05|2.0.06|4.0.01|4.0.02") .And. ( Empty(CM0->CM0_HRACID) .Or. !(xFunVldHor("CM0->CM0_HRACID")) )  		  
			AADD(aLogErro,{"CM0_HRACID", "000007", "CM0", nRecno }) //STR0007 - "Hora Inconsistente ou Vazia"  
		EndIf
	ElseIf Empty(CM0->CM0_HRACID) .Or. !(xFunVldHor("CM0->CM0_HRACID"))		  
		AADD(aLogErro,{"CM0_HRACID", "000007", "CM0", nRecno }) //STR0007 - "Hora Inconsistente ou Vazia"   		
	EndIf
		
	If (CM0->CM0_TPCAT $ "3") .And. !(CM0->CM0_INDOBI $ "1" )
		AADD(aLogErro,{"CM0_INDOBI", "000848", "CM0", nRecno }) //"Se o Tipo de CAT for igual a 3-Comunica��o de �bito, o campo dever� sempre ser preenchido com "S"."	
	EndIf
			
	//Valido as op�oes dos campos tipo COMBO
	If !(CM0->CM0_TPCAT $ "123")
		AADD(aLogErro,{"CM0_TPCAT", "000006", "CM0", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"       
	EndIf
	                                          
	//Valido as op�oes dos campos tipo COMBO
	If !(CM0->CM0_INDOBI $ "12")
		AADD(aLogErro,{"CM0_INDOBI", "000006", "CM0", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"       
	EndIf
	 
	//Valido as op�oes dos campos tipo COMBO
	If !(CM0->CM0_COMPOL $ "12")
		AADD(aLogErro,{"CM0_COMPOL", "000006", "CM0", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"       
	EndIf
	
	//Valido o campo de CNPJ, passando o terceiro parametro da funcao CGC como .F.
	//Para que nao seja apresentado o alert de validacao                          
	
	 If !Empty(CM0->CM0_CNPJLO) 
		If !CGC( ( CM0->CM0_CNPJLO),,.F. ) 
			AADD(aLogErro,{"CM0_CNPJLO", "000005", "CM0", nRecno }) //STR0005 - "CPF/CNPJ Inconsistente"    
		EndIf
	EndIf
	
	//Valido as op�oes dos campos tipo COMBO
	If Empty(CM0->CM0_TPLOC) .Or. !(CM0->CM0_TPLOC $ "1234569")
		AADD(aLogErro,{"CM0_TPLOC", "000006", "CM0", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis" 	
	Else			
		If CM0->CM0_TPLOC == '2'
		
			If Empty(CM0->CM0_CODPAI) //OK
				aAdd(aLogErro,{"CM0_CODPAI","000010","CM0",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
			EndIf
			
			If Empty(CM0->CM0_CODPOS) //OK
				aAdd(aLogErro,{"CM0_CODPOS","000010","CM0",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
			EndIf
			
		ElseIf ( Alltrim(Posicione("T0I",1,xFilial("T0I") + CM0->CM0_TPREG, "T0I_CODIGO")) == "1" ) .And. ( CM0->CM0_TPLOC $ "1|3" ) .And.  cTpInscr == "1"
			If Empty(CM0->CM0_CNPJLO) 
				AADD(aLogErro,{"CM0_CNPJLO", "000850", "CM0", nRecno }) //"O Preenchimento do campo CNPJ Local � Obrigat�rio quando o Tipo do Registrador for igual a 1, Tipo do Local igual 1 ou 3 e Tipo de Inscri��o do grupo Identifica��o do empregador for igual a 1."
			Else
				DBSelectArea("C92")
				C92->(DBSetOrder(6))
				If CM0->CM0_TPLOC == '1'
					If C92->(MSSeek(xFilial("C92") + "1" + ALLTRIM(CM0->CM0_CNPJLO)))
							If !(C92->C92_ATIVO == "1" .AND. C92->C92_STATUS == '4')
								AADD(aLogErro,{"CM0_CNPJLO", "000851", "CM0", nRecno }) //"Deve ser um CNPJ v�lido informado atrav�s do evento S-1005 se Tipo Local for  igual a 1."				
							EndIf
					Else
						AADD(aLogErro,{"CM0_CNPJLO", "000851", "CM0", nRecno }) //"Deve ser um CNPJ v�lido informado atrav�s do evento S-1005 se Tipo Local for  igual a 1."
					EndIf
				ElseIf CM0->CM0_TPLOC == '3'
					If C92->(MSSeek(xFilial("C92") + "1" + ALLTRIM(CM0->CM0_CNPJLO)))
						If C92->C92_ATIVO == "1" .AND. C92->C92_STATUS == '4'
							AADD(aLogErro,{"CM0_CNPJLO", "000852", "CM0", nRecno }) //"N�o deve ser um CNPJ informado atrav�s do evento S-1005 quando o Tipo Local for  igual a 3."				
						EndIf
					EndIf
				Endif
			EndIf
	
		EndIf
	EndIf

    If !__lLay0205

        If Alltrim(Posicione("T0I",1,xFilial("T0I") + CM0->CM0_TPREG, "T0I_CODIGO")) <> "1" 

            If Empty(CM0->CM0_INSREG)
                aAdd(aLogErro,{"CM0_INSREG","000010","CM0",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
            EndIf

            If Empty(CM0->CM0_NRIREG)
                aAdd(aLogErro,{"CM0_NRIREG","000010","CM0",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
            EndIf
            
        EndIf
    
    EndIf
	
	//Valido as op�oes dos campos tipo COMBO
	If !Empty(CM0->CM0_INDINT)
		If !(CM0->CM0_INDINT $ "12")
			AADD(aLogErro,{"CM0_INDINT", "000006", "CM0", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"       
		EndIf
	EndIf 
	
	//Valido as op�oes dos campos tipo COMBO
	If !Empty(CM0->CM0_INDAFA)
		If !(CM0->CM0_INDAFA $ "12") 
			AADD(aLogErro,{"CM0_INDAFA", "000006", "CM0", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"      
		EndIf
	EndIf
	
	//Valido o campo do tipo DATA
	If !Empty(CM0->CM0_DTATEN) 
		If Len(DTOS(CM0->CM0_DTATEN)) < 8 
			AADD(aLogErro,{"CM0_DTATEN", "000009", "CM0", nRecno }) //STR0009 - "Data Inconsistente ou Vazia"    
		EndIf
	EndIf    
	
	//Funcao validadora de campos com conteudo HORA
	If !Empty(CM0->CM0_HRATEN) 
		If !(xFunVldHor("CM0->CM0_HRATEN"))
			AADD(aLogErro,{"CM0_HRATEN", "000007", "CM0", nRecno }) //STR0007 - "Hora Inconsistente ou Vazia"    
		EndIf
	EndIF
	
    If !__lLay0205

        //Valido as op�oes dos campos tipo COMBO
        If !(CM0->CM0_INICAT $ " 123")
            AADD(aLogErro,{"CM0_TPCAT", "000006", "CM0", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"       
        EndIf
        
        IF Alltrim(Posicione("T0I",1,xFilial("T0I") + CM0->CM0_TPREG, "T0I_CODIGO")) <> '1' .And. !Empty(CM0->CM0_INICAT)
            AADD(aLogErro,{"CM0_INICAT", "000849", "CM0", nRecno }) //"O campo Indic. CAT n�o pode ser preenchido quando o Tipo Registrador for Diferente de 1- Empregador. "
        EndIf
	
	EndIf 

	If __lLay0205
		// Valida o CEP
		If CM0->CM0_TPLOC $ '1|3|5' .And. Empty(CM0->CM0_CEP)
			AADD(aLogErro,{"CM0_CEP", "001190", "CM0", nRecno }) //STR1190 - "O Preenchimento do campo CEP � Obrigat�rio quando o Tipo de Local de Acidente � igual a 1-3-5"
		EndIf
		
		// Valida o CodMun e UF
		If CM0->CM0_TPLOC $ '1|3|4|5' .And. (Empty(CM0->CM0_CODMUN) .Or. Empty(CM0->CM0_UF))
			AADD(aLogErro,{"CM0_CODMUN", "001191", "CM0", nRecno }) //STR1191 - "O Preenchimento do campo Cod. Municipio � Obrigat�rio quando o Tipo de Local de Acidente � igual a 1, 3, 4 ou 5"
			AADD(aLogErro,{"CM0_UF", "001192", "CM0", nRecno }) //STR1192 - "O Preenchimento do campo UF � Obrigat�rio quando o Tipo de Local de Acidente � igual a 1, 3, 4 ou 5"
		EndIf
		
		// Valida a inscri��o do estabelecimento 
		If CM0->CM0_TPLOC $ '1' 
            C92->( DbSetOrder( 6 ) )
            If !C92->( MsSeek( xFilial( "C92" ) + CM0->CM0_INSACI + CM0->CM0_NRIACI + " 1" ) )
                AADD(aLogErro,{"CM0_INSACI","001163","CMR", nRecno }) //"Campo vazio ou inscri��o inexistente na tabela de estabelecimentos (TAFA253 - Evento S-1005)"
            EndIf
		EndIf

		// Valida o Pais
		If CM0->CM0_TPLOC $ '2' .And. Empty(CM0->CM0_CODPAI)
			AADD(aLogErro,{"CM0_CODPAI", "001193", "CM0", nRecno }) //STR1193 - "O Preenchimento do campo Pa�s � Obrigat�rio quando o Tipo de Local de Acidente � igual a 2"
		EndIf
		
		// Valida o tipo de logradouro
		C06->(DbSetOrder(3))
		If Empty(CM0->CM0_TPLOGR)
			AADD(aLogErro,{"CM0_TPLOGR", "000010", "CM0", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio."
		ElseIf !Empty(CM0->CM0_TPLOGR) .And. !C06->(MSSeek(xFilial("C06") + CM0->CM0_TPLOGR)) //Posicione("C06",3,xFilial("C06")+CM0->CM0_TPLOGR,"C06_CESOCI")
			AADD(aLogErro,{"CM0_TPLOGR", "001194", "CM0", nRecno }) //STR1194 - "Deve representar neste campo o tipo de logradouro, conforme tabela 20"
		EndIf
		
		If Empty(CM0->CM0_DESLOG)
			AADD(aLogErro,{"CM0_DESLOG", "000010", "CM0", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio."
		EndIf
		
		If Empty(CM0->CM0_NRLOG)
			AADD(aLogErro,{"CM0_NRLOG", "000010", "CM0", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio."
		EndIf
		
	EndIf
	
	   
	//{Campo do registro filho/consulta padrao , opcao de validacao , opcao do combo(opcional)}
	aCampos  := {} 
	AADD(aCampos,{"C9V_CPF",1})  	//Valida CPF
	AADD(aCampos,{"C9V_DTNASC",3}) //Valida campo vazio
	AADD(aCampos,{"C9V_NOME",3})   //Valida campo vazio
	AADD(aCampos,{"C9V_NIS",3})   	//Valida campo vazio
	AADD(aCampos,{"C9V_CODCGO",3})	//Valida campo vazio
	AADD(aCampos,{"C9V_CODCAT",3}) //Valida campo vazio
	
	//Chave de busca na tabela FILHO ou Consulta padrao
	cChave := CM0->CM0_TRABAL+"1"
	
	//Funcao responsavel por validar os registros filhos ou 				 	 
	//consulta padrao das respectivas tabelas               				 	  
	//																		 	  
	//			xValRegTab("C9V" - Alias da tabela filho, 			 			  
	//			cChave 			 - Chave de Busca								  
	//			,3 				 - Indice da tabela Filho						  
	//			,aCampos 		 - Array com os campos e opcoes a serem validadas 
	//			,aLogErro 		 - Array de Log de Erros						  
	//			,CM0->CM0_DTACID)- Data do evento 							      			
	xValRegTab("C9V",cChave,2,aCampos,@aLogErro,CM0->CM0_DTACID, { "CM0", "CM0_TRABAL", nRecno } )
	
	If !Empty(CM0->CM0_UF)
		cChave := CM0->CM0_UF
		xValRegTab("C09",cChave,3,,@aLogErro,CM0->CM0_DTACID, { "CM0", "CM0_UF", nRecno } )	
	EndIf 
	 
	If !Empty(CM0->CM0_CODMUN) 
		cChave := CM0->CM0_CODMUN
		xValRegTab("C07",cChave,3,,@aLogErro,CM0->CM0_DTACID, { "CM0", "CM0_CODMUN", nRecno } )	
	EndIf 
	
	If !Empty(CM0->CM0_CODSIT)
		cChave := CM0->CM0_CODSIT
		xValRegTab("C8L",cChave,1,,@aLogErro,CM0->CM0_DTACID, { "CM0", "CM0_CODSIT", nRecno } )	
	Else
		AADD(aLogErro,{"CM0_CODSIT", "000010", "CM0", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio."
	EndIf 
	
	If !Empty(CM0->CM0_IDPROF)
		cChave := CM0->CM0_IDPROF
		xValRegTab("CM7",cChave,1,,@aLogErro,CM0->CM0_DTACID, { "CM0", "CM0_IDPROF", nRecno } )	
	EndIf 
	
	If !Empty(CM0->CM0_NATLES)
		cChave := CM0->CM0_NATLES
		xValRegTab("C8M",cChave,1,,@aLogErro,CM0->CM0_DTACID, { "CM0", "CM0_NATLES", nRecno } )	
	EndIf 
	
	If !Empty(CM0->CM0_TPREG)
		cChave := CM0->CM0_TPREG
		xValRegTab("T0I",cChave,1,,@aLogErro,CM0->CM0_DTACID, { "CM0", "CM0_TPREG", nRecno } )	
	EndIf   
	
	//Registros Filhos (GRID)                                                  
	//Posiciono nos registros FILHOS e crio um laco com a chave do registro PAI
	CM1->(DBSetOrder(1))
	If CM1->(msSeek(xFilial("CM1")+CM0->CM0_ID+CM0->CM0_VERSAO))
	
		//Chave do registro PAI                                     
		cChvWhile := CM0->CM0_ID+CM0->CM0_VERSAO
	   
		Do While !("CM1")->(Eof ()) .And. cChvWhile == CM1->CM1_ID+CM1->CM1_VERSAO
			
			cChave := CM1->CM1_CODPAR		
			xValRegTab("C8I",cChave,1,,@aLogErro,CM0->CM0_DTACID, { "CM0", "CM1_CODPAR", nRecno } )	
			
			//Valido as op�oes dos campos tipo COMBO
			If !Empty(CM1->CM1_LATERA)
				If !(CM1->CM1_LATERA $ "0123") 
					AADD(aLogErro,{"CM1_LATERA", "000006", "CM1", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"      
				EndIf
			EndIf
			
			CM1->(dbSkip())
		EndDo
	
	EndIf 
	  
	//Registros Filhos (GRID)                                                  
	//Posiciono nos registros FILHOS e crio um laco com a chave do registro PAI 
	CM2->(DBSetOrder(1))
	If CM2->(msSeek(xFilial("CM2")+CM0->CM0_ID+CM0->CM0_VERSAO))
	                                     
		//Chave do registro PAI                                     
		cChvWhile := CM0->CM0_ID+CM0->CM0_VERSAO
	   
		Do While !("CM2")->(Eof ()) .And. cChvWhile == CM2->CM2_ID+CM2->CM2_VERSAO
	
			cChave := CM2->CM2_CODAGE			
			xValRegTab("C8J",cChave,1,,@aLogErro,CM0->CM0_DTACID, { "CM0", "CM2_CODAGE", nRecno } )	
			
			CM2->(dbSkip())
		EndDo
	
	EndIf

	//REGRA_VALIDA_TRABALHADOR_BASE_CNIS
	lValid := .T.
	C9V->( DbSetOrder( 2 ) )
	If C9V->( MsSeek ( xFilial("C9V") + CM0->CM0_TRABAL + "1") )
		cIdCateg := Posicione("CUP",1,xFilial("CUP") + C9V->(C9V_ID + C9V_VERSAO), "CUP_CODCAT")

		If !Empty( cIdCateg )
			cCodCat := Posicione("C87",1,xFilial("C87") + cIdCateg, "C87_CODIGO")

			If !Empty(cCodCat) .And. !(cCodCat $ "901|903|904") .And. Empty( C9V->C9V_NIS )
				lValid := .F.
				AADD(aLogErro,{"C9V_NIS", "001049", "C9V", nRecno }) //STR1049 - "O preenchimento � obrigat�rio, exceto se o c�digo de categoria do trabalhador for igual a [901, 903, 904]."
			Endif
		EndIf

		If (TafColumnPos('C9V_CATCI')) .And. lValid
			cCodCat := Posicione("C87",1,xFilial("C87") + C9V->C9V_CATCI, "C87_CODIGO")

			If !Empty(cCodCat) .And. !(cCodCat $ "901|903|904") .And. Empty( C9V->C9V_NIS )
				AADD(aLogErro,{"C9V_NIS", "001049", "C9V", nRecno }) //STR1049 - "O preenchimento � obrigat�rio, exceto se o c�digo de categoria do trabalhador for igual a [901, 903, 904]."
			Endif
		EndIf
	Endif

	//ATUALIZO O STATUS DO REGISTRO
	If Len(aLogErro)>0	
		//Utilizo RecLock, pois o SETVALUE somente funciona em campos USADOS
		RecLock("CM0",.F.)
	    	CM0->CM0_STATUS := "1" 
	    CM0->( MsUnlock() )    
	Else
		RecLock("CM0",.F.)
	    	CM0->CM0_STATUS := "0" 
	    CM0->( MsUnlock() )        
	EndIf 	
Else
	AADD(aLogErro,{"CM0","000305","CM0",nRecno}) //Registros que j� foram transmitidos ao Fisco, n�o podem ser validados		
EndIf	

//N�o apresento o alert quando utilizo o JOB para validar
If !lJob
	xValLogEr(aLogErro)
EndIf	

Return(aLogErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} GerarEvtExc
Funcao que 

@Param  oModel  -> Modelo de dados
@Param  nRecno  -> Numero do recno
@Param  lRotExc -> Variavel que controla se a function � chamada pelo TafIntegraESocial

@Return .T.

@Author Vitor Henrique Ferreira
@Since 30/06/2015
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function GerarEvtExc( oModel, nRecno, lRotExc )

Local oModelCM0	:= oModel:GetModel("MODEL_CM0") 
Local oModelCM1	:= oModel:GetModel("MODEL_CM1") 
Local oModelCM2	:= oModel:GetModel("MODEL_CM2")
Local oModelCM0p:= oModel:GetModel("MODEL_CM0p") 

Local cVerAnt	:= ""  
Local cProtocolo:= ""
Local cVersao	:= ""    
Local cEvento	:= "" 
Local cChvRegAnt:= ""

Local nlI		:= 0
Local nlY		:= 0

Local aGrava	:= {}
Local aGravaCM1	:= {}
Local aGravaCM2	:= {}

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

dbSelectArea("CM0")
("CM0")->( DBGoTo( nRecno ) )

Begin Transaction	

	oModelCM0 := oModel:GetModel( 'MODEL_CM0' )     
	oModelCM1 := oModel:GetModel( 'MODEL_CM1' )     
	oModelCM2 := oModel:GetModel( 'MODEL_CM2' ) 
	   					
	//�����������������������������������������������������������Ŀ
	//�Busco a versao anterior do registro para gravacao do rastro�
	//�������������������������������������������������������������
	cVerAnt   	:= oModelCM0:GetValue( "CM0_VERSAO" )				
	cProtocolo	:= oModelCM0:GetValue( "CM0_PROTUL" )
	cEvento	:= oModelCM0:GetValue( "CM0_EVENTO" )
	
	//�����������������������������������������������������������������Ŀ
	//�Neste momento eu gravo as informacoes que foram carregadas       �
	//�na tela, pois neste momento o usuario ja fez as modificacoes que �
	//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
	//�nao devem ser consideradas neste momento                         �
	//�������������������������������������������������������������������
	For nlI := 1 To 1
		For nlY := 1 To Len( oModelCM0:aDataModel[ nlI ] )			
			Aadd( aGrava, { oModelCM0:aDataModel[ nlI, nlY, 1 ], oModelCM0:aDataModel[ nlI, nlY, 2 ] } )									
		Next
	Next   																			

	For nlI := 1 To oModel:GetModel( 'MODEL_CM1' ):Length() 
		oModel:GetModel( 'MODEL_CM1' ):GoLine(nlI)

		If !oModel:GetModel( 'MODEL_CM1' ):IsDeleted()								
			aAdd (aGravaCM1 ,{oModelCM1:GetValue('CM1_CODPAR')} )
		EndIf					
	Next
	
	For nlI := 1 To oModel:GetModel( 'MODEL_CM2' ):Length() 
		oModel:GetModel( 'MODEL_CM2' ):GoLine(nlI)
		
		If !oModel:GetModel( 'MODEL_CM2' ):IsDeleted()								
			aAdd (aGravaCM2 ,{oModelCM2:GetValue('CM2_CODAGE')} )
		EndIf					
	Next			                                                    					                                                     

	//�����������������������������������������������������������Ŀ
	//�Seto o campo como Inativo e gravo a versao do novo registro�
	//�no registro anterior                                       � 
	//|                                                           |
	//|ATENCAO -> A alteracao destes campos deve sempre estar     |
	//|abaixo do Loop do For, pois devem substituir as informacoes|
	//|que foram armazenadas no Loop acima                        |
	//�������������������������������������������������������������
	FAltRegAnt( 'CM0', '2' )

	//��������������������������������������������������Ŀ
	//�Neste momento eu preciso setar a operacao do model�
	//�como Inclusao                                     �
	//����������������������������������������������������
	oModel:DeActivate()
	oModel:SetOperation( 3 ) 	
	oModel:Activate()		
					
	//�������������������������������������������������������Ŀ
	//�Neste momento eu realizo a inclusao do novo registro ja�
	//�contemplando as informacoes alteradas pelo usuario     �
	//���������������������������������������������������������
	For nlI := 1 To Len( aGrava )	
		oModel:LoadValue( 'MODEL_CM0', aGrava[ nlI, 1 ], aGrava[ nlI, 2 ] )
	Next        

	For nlI := 1 To Len( aGravaCM1 )
		If nlI > 1
			oModel:GetModel( 'MODEL_CM1' ):AddLine()
		EndIf
		oModel:LoadValue( "MODEL_CM1", "CM1_CODPAR", aGravaCM1[nlI][1] )
    Next		
	     
	For nlI := 1 To Len( aGravaCM2 )
		If nlI > 1
			oModel:GetModel( 'MODEL_CM2' ):AddLine()
		EndIf					
		oModel:LoadValue( "MODEL_CM2", "CM2_CODAGE", aGravaCM2[nlI][1] )   
	Next
		
	//�������������������������������Ŀ
	//�Busco a versao que sera gravada�
	//���������������������������������
	cVersao := xFunGetVer()		 
	                                   
	//�����������������������������������������������������������Ŀ		
	//|ATENCAO -> A alteracao destes campos deve sempre estar     |
	//|abaixo do Loop do For, pois devem substituir as informacoes|
	//|que foram armazenadas no Loop acima                        |
	//�������������������������������������������������������������		                                                                      				         
	oModel:LoadValue( 'MODEL_CM0',  'CM0_VERSAO', cVersao )
	oModel:LoadValue( 'MODEL_CM0',  'CM0_VERANT', cVerAnt )
	oModel:LoadValue( 'MODEL_CM0',  'CM0_PROTPN', cProtocolo )
	oModel:LoadValue( 'MODEL_CM0',  'CM0_PROTUL', "" )

	/*---------------------------------------------------------
	Tratamento para que caso o Evento Anterior fosse de exclus�o
	seta-se o novo evento como uma "nova inclus�o", caso contr�rio o
	evento passar a ser uma altera��o
	-----------------------------------------------------------*/
	oModel:LoadValue( 'MODEL_CM0', 'CM0_EVENTO', "E" )
	oModel:LoadValue( 'MODEL_CM0', 'CM0_ATIVO', "1" )

	//Gravo altera��o para o Extempor�neo
	If lGoExtemp
		TafGrvExt( oModel, 'MODEL_CM0', 'CM0' )	
	EndIf

	FwFormCommit( oModel )
	TAFAltStat( 'CM0',"6" )
				
End Transaction       

Return .T.	

//-------------------------------------------------------------------
/*/{Protheus.doc} TafVDCAT
Rotina para validar a altera��o da Data de acidente de Trabalho.

Caso houver evento de afastamento tempor�rio por acidente de trabalho, 
a Data de Acidente somente poder� ser
retificada para uma data anterior � data de afastamento
 
REGRA_RETIFICA_DT_ACIDENTE

@Param		cIDTrab	-	C�digo do Trabalhador
			cDtaCid	-	Data de Acidente do Trabalhor
			lDic	-	Informa se � ou n�o Dicion�rio

@Return	aRet		-	Array com estrutura de campos e conte�do da tabela

@Author	Mick William da Silva
@Since		25/02/2016
@Version	1.0
/*/
//-------------------------------------------------------------------
Function TafVDCAT( cIDTrab, cDtaCid, lDic )
	
Local lRetorno	:= .T.

Default lDic	:= .F.

lRetorno	:= .T.
	
	dbSelectArea("CM6")
	DbSetOrder(2)
	
	If CM6->(MSSeek(xFilial("CM6") + cIDTrab))
		IF cDtaCid >= CM6->CM6_DTAFAS 
			IF lDic == .T. 
				MsgAlert("Existe um Evento de Afastamento Tempor�rio com Data Anterior a Data de Acidente Informada.")
				lRetorno := .F.
			Else
				lRetorno := .F.
			EndIf
		EndIf
	EndIf

Return( lRetorno )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA257NrRec
Rotina para informar o n�mero do recibo da CAT de origem.

@Param		cCabec		-	Arquivo de importa��o
			
@Return		cNrRecCat	-	Recibo da CAT de origem

@Author		Karyna R. M. Martins
@Since		20/02/2019
@Version	1.0
/*/
//-------------------------------------------------------------------
Static Function TAFA257NrRec(cCabec,cCodEvent,cOwner)

Local aIncons	:= {}

Local cNrRecCat := ""
Local cIdFunc	:= FGetIdInt("cpfTrab","", cCabec + "ideVinculo/cpfTrab")
Local cDtAcid	:= StrTran(FTafGetVal( cCabec + "cat/dtAcid","C", .F., aIncons, .F.),"-","")
Local cHrAcid	:= FTafGetVal( cCabec + "cat/hrAcid","C", .F., aIncons, .F.)
Local cTpCat    := FTafGetVal( cCabec + "cat/tpCat" ,"C", .F., aIncons, .F.)

Local aCM0Area  := CM0->(GetArea())

Local cSearch       := ""
Local cQry          := ""
Local cAliasQry     := GetNextAlias()
Local cAliasTafKey  := ""

Default cCabec 		:= ""
Default cCodEvent	:= ""
Default cOwner		:= ""

If cTpCat $ '2|3'
	If !__lLay0205
	
	    If !Empty(cHrAcid)
	        cHrAcid	:= Transform(FTafGetVal( cCabec + "cat/hrAcid","C", .F., aIncons, .F.),"@R 99:99")
	    EndIf
	
	    CM0->( DbSetOrder(4) )
	    If CM0->(MSSeek(xFilial("CM0") + cIdFunc + cDtAcid + cHrAcid ))
	        cNrRecCat := CM0->CM0_PROTUL            
	    Else
	        If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "cat/catOrigem/nrRecCatOrig"))
	            cNrRecCat := FTafGetVal( cCabec + "cat/catOrigem/nrRecCatOrig","C", .F., aIncons, .F.)													
	        EndIf
	    EndIf
	
	Else
	    cSearch := oDados:XPathGetNodeValue( cCabec + "cat/catOrigem/nrRecCatOrig")
	
	    //Tratamento para quando enviarem o recibo
	    CM0->(DbSetOrder(6))
	    If CM0->(MsSeek( xFilial("CM0") + Padr(cSearch, TamSx3("CM0_PROTUL")[1]) + "1") )
	        cNrRecCat := CM0->CM0_PROTUL
	    Else
	
	        //Tratamento realizado quando as linhas enviarem o TAFKEY 
	
	        cQry += "SELECT * FROM TAFXERP TAFXERP "
	        cQry += "	WHERE TAFALIAS = 'CM0'"
	        cQry += "   AND TAFXERP.TAFKEY IN ( '" + cSearch + "' ) "
	        cQry += "   AND TAFXERP.TAFRECNO <> '0' "
	        cQry += "   AND TAFXERP.D_E_L_E_T_ = '' "
	        cQry += "   ORDER BY R_E_C_N_O_ DESC"
	
	        cQry := ChangeQuery(cQry)
	
	        dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQry ) , cAliasQry, .F., .T. )
	
	        If (cAliasQry)->(!Eof())
	            If !Empty((cAliasQry)->TAFALIAS)
	                cAliasTafKey := (cAliasQry)->TAFALIAS
	                (cAliasTafKey)->(dbGoTo((cAliasQry)->TAFRECNO))
	                cNrRecCat := ( cAliasTafKey )->&( cAliasTafKey + "_PROTUL" )        
	            EndIf
	        Else
	
	            //Tratamento para o SIGAGPE
	            /*
	            Estrutura esperada no aSearch
	                1 - Cpf do Trabalhador
	                2 - Matricula
	                3 - Categoria
	                4 - Data do Acidente
	                5 - Hora do Acidente
	                6 - Tipo da Cat
	            */
	
	            aSearch := Separa( cSearch, ";" ) 
	            If Len(aSearch) == 6
	
	                CM0->( DbSetOrder(4) )
	                If CM0->(MSSeek(xFilial("CM0") + cIdFunc + aSearch[4] + aSearch[5] + aSearch[6] ))
	                    cNrRecCat := CM0->CM0_PROTUL            
	                EndIf
	
	            EndIf
	
	        EndIf
	
	        (cAliasQry)->(DbCloseArea())
	
	    EndIf
	
	EndIf
EndIf

RestArea(aCM0Area)
  
Return cNrRecCat