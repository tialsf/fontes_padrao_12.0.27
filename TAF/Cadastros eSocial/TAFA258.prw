#INCLUDE "PROTHEUS.CH" 
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "TAFA258.CH" 

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA258
Monitoramento da Sa�de do Trabalhador (S-2220)

@author Vitor Siqueira
@since 03/03/2016
@version 1.0

/*/ 
//-------------------------------------------------------------------
Function TAFA258()

Private oBrw := FWmBrowse():New()

// Fun��o que indica se o ambiente � v�lido para o eSocial 2.3
If TafAtualizado()
	oBrw:SetDescription(STR0001)    //"Monitora��o da Sa�de do Trabalhador"
	oBrw:SetAlias( 'C8B')
	oBrw:SetMenuDef( 'TAFA258' )
	
	If FindFunction('TAFSetFilter')
		oBrw:SetFilterDefault(TAFBrwSetFilter("C8B","TAFA258","S-2220"))
	Else
		oBrw:SetFilterDefault( "C8B_ATIVO == '1'" ) //Filtro para que apenas os registros ativos sejam exibidos ( 1=Ativo, 2=Inativo )
	EndIf

	If TafLayESoc ("02_05_00")
		oBrw:SetOnlyFields( { 'C8B_FILIAL','C8B_ID', 'C8B_FUNC', 'C8B_DTASO', 'C8B_RESULT' } )
	EndIf
	
	TafLegend(2,"C8B",@oBrw)
	
	oBrw:Activate()
EndIf

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Vitor Siqueira
@since 03/03/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao	:= {}
Local aRotina	:= {}

If FindFunction('TafXmlRet')
	Aadd( aFuncao, { "" , "TafxmlRet('TAF258Xml','2220','C8B')" , "1" } )
Else
	Aadd( aFuncao, { "" , "TAF258Xml" , "1" } )
EndIf 
Aadd( aFuncao, { "" , "Taf258Vld" , "2" } )

//Chamo a Browse do Hist�rico
If FindFunction( "xNewHisAlt" ) 
	Aadd( aFuncao, { "" , "xNewHisAlt('C8B', 'TAFA258' ,,,,,,'2220','TAF258Xml' )" , "3" } )
Else					
	Aadd( aFuncao, { "" , "xFunHisAlt('C8B', 'TAFA258' ,,,, 'TAF258XML','2220' )" , "3" } )
EndIf

aAdd( aFuncao, { "" , "TAFXmlLote( 'C8B', 'S-2220' , 'evtMonit' , 'TAF258Xml',, oBrw )" , "5" } )
Aadd( aFuncao, { "" , "xFunAltRec( 'C8B' )" , "10" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If lMenuDif
	ADD OPTION aRotina Title "Visualizar" Action 'VIEWDEF.TAFA258' OPERATION 2 ACCESS 0
	
	// Menu dos extempor�neos
	If FindFunction( "xNewHisAlt" ) .AND. FindFunction( "xTafExtmp" ) .And. xTafExtmp()
		aRotina	:= xMnuExtmp( "TAFA258","C8B" )
	EndIf
	
Else
	aRotina	:=	xFunMnuTAF( "TAFA258" , , aFuncao)
EndIf
	
Return (aRotina )
//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC

@author Vitor Siqueira
@since 03/03/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

Local oStruC8B  :=  FWFormStruct( 1, 'C8B' )
Local oStruC9W  :=  FWFormStruct( 1, 'C9W' )
Local oStruCRP  :=  FWFormStruct( 1, 'CRP' )

Local oModel    :=  MPFormModel():New( 'TAFA258'  ,,,{|oModel| SaveModel(oModel)})

//Remo��o do GetSX8Num quando se tratar da Exclus�o de um Evento Transmitido.
//Necess�rio para n�o incrementar ID que n�o ser� utilizado.
If Upper( ProcName( 2 ) ) == Upper( "GerarExclusao" )
	oStruC8B:SetProperty( "C8B_ID", MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD, "" ) )
EndIf

oModel:AddFields('MODEL_C8B', /*cOwner*/, oStruC8B)
oModel:GetModel('MODEL_C8B'):SetPrimaryKey({'C8B_FILIAL', 'C8B_ID', 'C8B_VERSAO'})  

oModel:AddGrid('MODEL_C9W', 'MODEL_C8B', oStruC9W)
oModel:GetModel('MODEL_C9W'):SetUniqueLine({'C9W_DTEXAM', 'C9W_CODPRO'})
oModel:GetModel( 'MODEL_C9W' ):SetOptional( .T. )    

If TafLayESoc("02_05_00")
	oStruC8B:SetProperty( 'C8B_TPASO'   , MODEL_FIELD_OBRIGAT , .F.  )
	oStruC8B:SetProperty( 'C8B_TPEXAM'  , MODEL_FIELD_OBRIGAT , .T.  )
	oStruC8B:SetProperty( 'C8B_CONTAT'  , MODEL_FIELD_OBRIGAT , .F.  )

	oStruC9W:SetProperty( 'C9W_INTERP'  , MODEL_FIELD_OBRIGAT , .F.  )
	oStruC9W:SetProperty( 'C9W_CRMUF'   , MODEL_FIELD_OBRIGAT , .F.  )
	oStruC9W:SetProperty( 'C9W_CRMRES'  , MODEL_FIELD_OBRIGAT , .F.  )
	oStruC9W:SetProperty( 'C9W_NISRES'  , MODEL_FIELD_OBRIGAT , .F.  )
	oStruC9W:SetProperty( 'C9W_DTFIMO'  , MODEL_FIELD_OBRIGAT , .F.  )
	oStruC9W:SetProperty( 'C9W_DTINMO'  , MODEL_FIELD_OBRIGAT , .F.  )
	oStruC9W:SetProperty( 'C9W_DCRMU'   , MODEL_FIELD_OBRIGAT , .F.  )
EndIf

oModel:SetRelation('MODEL_C9W', {{'C9W_FILIAL' , 'xFilial( "C9W" )'}, {'C9W_ID' , 'C8B_ID'}, {'C9W_VERSAO' , 'C8B_VERSAO'}}, C9W->(IndexKey(1)))

Return oModel
//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Vitor Siqueira
@since 03/03/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef() 

Local oModel    := FWLoadModel( 'TAFA258' )
Local oStruC8B  := FWFormStruct( 2, 'C8B' )
Local oStruC9W  := FWFormStruct( 2, 'C9W' )
Local oStruCRP  := FWFormStruct( 2, 'CRP' )
Local oStruC8Ba := Nil
Local oStruC8Bb := Nil
Local oView     := FWFormView():New()
Local cCmpFil   := ''
	
oView:SetModel( oModel )      

If TafLayESoc("02_05_00")	

// Campos do folder Informacoes da Monitora��o da Sa�de do Trabalhador
	cCmpFil   := 'C8B_ID|C8B_FUNC|C8B_DFUNC|C8B_DTASO|C8B_RESULT|C8B_CPFRES|C8B_NOMRES|C8B_NRCRM|C8B_CRMUF|C8B_CODMED|C8B_TPEXAM|'
	oStruC8Ba := FwFormStruct( 2, 'C8B', {|x| AllTrim( x ) + "|" $ cCmpFil } )

	oStruC8Ba:SetProperty( "C8B_CPFRES"	, MVC_VIEW_ORDEM, "12" )
	oStruC8Ba:SetProperty( "C8B_NRCRM"	, MVC_VIEW_ORDEM, "13" )
	oStruC8Ba:SetProperty( "C8B_NOMRES"	, MVC_VIEW_ORDEM, "14" )

	// Campos do folder do n�mero do ultimo protocolo
	cCmpFil   := 'C8B_PROTUL|
	oStruC8Bb := FwFormStruct( 2, 'C8B', {|x| AllTrim( x ) + "|" $ cCmpFil } )

	If FindFunction("TafAjustRecibo")
		TafAjustRecibo(oStruC8Bb,"C8B")
	EndIf 

	oView:AddField( 'VIEW_C8Ba', oStruC8Ba, 'MODEL_C8B' )
	oView:EnableTitleView( 'VIEW_C8Ba', STR0001 ) //Atestado de Sa�de Ocupacional

	oView:AddField( 'VIEW_C8Bb', oStruC8Bb, 'MODEL_C8B' )

	If FindFunction("TafNmFolder")
		oView:EnableTitleView( 'VIEW_C8Bb',  TafNmFolder("recibo",1) ) // "Recibo da �ltima Transmiss�o"  
	EndIf 

	oStruC8Ba:AddGroup ( 'GRP_TRABALHADOR', "Informa��es de Identifica��o do Trabalhador e do V�nculo"     , '' , 1 )  //Informa��es de Identifica��o do Trabalhador e do V�nculo
	oStruC8Ba:AddGroup ( 'GRP_EXAME'  	  , "Informa��es do exame m�dico ocupacional"   , '' , 1 )	//DInforma��es do exame m�dico ocupacional
	oStruC8Ba:AddGroup ( 'GRP_ASO'        , "Detalhamento das informa��es do ASO"   , '' , 1 )	//Detalhamento das informa��es do ASO
	oStruC8Ba:AddGroup ( 'GRP_MEDICO'     , "Informa��es sobre o m�dico emitente do ASO"   , '' , 1 )	//Informa��es sobre o m�dico emitente do ASO
	oStruC8Ba:AddGroup ( 'GRP_RESPMED'    , "Informa��es sobre o m�dico respons�vel/coordenador do PCMSO"   , '' , 1 )	//Informa��es sobre o m�dico respons�vel/coordenador do PCMSO

	oStruC8Ba:SetProperty( 'C8B_ID'     , MVC_VIEW_GROUP_NUMBER , 'GRP_TRABALHADOR')
	oStruC8Ba:SetProperty( 'C8B_FUNC'   , MVC_VIEW_GROUP_NUMBER , 'GRP_TRABALHADOR')
	oStruC8Ba:SetProperty( 'C8B_DFUNC'  , MVC_VIEW_GROUP_NUMBER , 'GRP_TRABALHADOR')

	oStruC8Ba:SetProperty( 'C8B_TPEXAM'  , MVC_VIEW_GROUP_NUMBER , 'GRP_EXAME')	

	oStruC8Ba:SetProperty( 'C8B_DTASO'   , MVC_VIEW_GROUP_NUMBER , 'GRP_ASO')
	oStruC8Ba:SetProperty( 'C8B_RESULT'  , MVC_VIEW_GROUP_NUMBER , 'GRP_ASO')

	oStruC8Ba:SetProperty( 'C8B_CODMED'  , MVC_VIEW_GROUP_NUMBER , 'GRP_MEDICO')

	oStruC8Ba:SetProperty( 'C8B_CPFRES' , MVC_VIEW_GROUP_NUMBER , 'GRP_RESPMED')
	oStruC8Ba:SetProperty( 'C8B_NOMRES' , MVC_VIEW_GROUP_NUMBER , 'GRP_RESPMED')
	oStruC8Ba:SetProperty( 'C8B_NRCRM'  , MVC_VIEW_GROUP_NUMBER , 'GRP_RESPMED')
	oStruC8Ba:SetProperty( 'C8B_CRMUF'  , MVC_VIEW_GROUP_NUMBER , 'GRP_RESPMED')

	//C9W

	oStruC9W:RemoveField( "C9W_INTERP" )
	oStruC9W:RemoveField( "C9W_CRMUF"  )
	oStruC9W:RemoveField( "C9W_CRMRES" )
	oStruC9W:RemoveField( "C9W_NISRES" )
	oStruC9W:RemoveField( "C9W_DTFIMO" )
	oStruC9W:RemoveField( "C9W_DTINMO" )
	oStruC9W:RemoveField( "C9W_DCRMU"  )

	oView:AddGrid ( 'VIEW_C9W', oStruC9W, 'MODEL_C9W' )
	oView:EnableTitleView( 'VIEW_C9W', STR0002) //"Exames"

	oView:CreateFolder( 'PASTAS' )
	oView:AddSheet( 'PASTAS', 'ABA00', STR0001)//'Atestado de sa�de Ocupacional' 

	If FindFunction("TafNmFolder")
		oView:AddSheet( 'PASTAS', 'ABA01', TafNmFolder("recibo") )   //"Numero do Recibo"
	Else
		oView:AddSheet( 'PASTAS', 'ABA01', STR0013)//'Protocolo da �ltima Transmiss�o'
	EndIf

	oView:CreateHorizontalBox( 'FIELDSC8Ba', 045,,,'PASTAS','ABA00')  
	oView:CreateHorizontalBox( 'FIELDSC8Bb', 100,,,'PASTAS','ABA01') 

	oView:CreateHorizontalBox("PAINEL_INFERIOR",055,,,"PASTAS","ABA00")
	oView:CreateFolder( 'FOLDER_INFERIOR', 'PAINEL_INFERIOR' )

	oView:AddSheet( 'FOLDER_INFERIOR', 'ABA01', STR0002)//'Exames' 
		
	oView:CreateHorizontalBox( 'GRIDC9W', 100,,,'FOLDER_INFERIOR','ABA01') 
		
	oView:SetOwnerView( 'VIEW_C8Ba', 'FIELDSC8Ba' )
	oView:SetOwnerView( 'VIEW_C8Bb', 'FIELDSC8Bb' )
	oView:SetOwnerView( 'VIEW_C9W', 'GRIDC9W' )

Else
	// Campos do folder Informacoes da Monitora��o da Sa�de do Trabalhador
	cCmpFil   := 'C8B_ID|C8B_FUNC|C8B_DFUNC|C8B_DTASO|C8B_TPASO|C8B_RESULT|C8B_CODMED|C8B_DCODME|C8B_CODCNE|C8B_CONTAT|C8B_EMAIL|'
	oStruC8Ba := FwFormStruct( 2, 'C8B', {|x| AllTrim( x ) + "|" $ cCmpFil } )

	// Campos do folder do n�mero do ultimo protocolo
	cCmpFil   := 'C8B_PROTUL|
	oStruC8Bb := FwFormStruct( 2, 'C8B', {|x| AllTrim( x ) + "|" $ cCmpFil } )

	If FindFunction("TafAjustRecibo")
		TafAjustRecibo(oStruC8Bb,"C8B")
	EndIf 

	oView:AddField( 'VIEW_C8Ba', oStruC8Ba, 'MODEL_C8B' )
	oView:EnableTitleView( 'VIEW_C8Ba', STR0001 ) //Atestado de Sa�de Ocupacional

	oView:AddField( 'VIEW_C8Bb', oStruC8Bb, 'MODEL_C8B' )

	If FindFunction("TafNmFolder")
		oView:EnableTitleView( 'VIEW_C8Bb',  TafNmFolder("recibo",1) ) // "Recibo da �ltima Transmiss�o"  
	EndIf 

	oView:AddGrid ( 'VIEW_C9W', oStruC9W, 'MODEL_C9W' )
	oView:EnableTitleView( 'VIEW_C9W', STR0002) //"Exames"

	oView:CreateFolder( 'PASTAS' )
	oView:AddSheet( 'PASTAS', 'ABA00', STR0001)//'Atestado de sa�de Ocupacional' 

	If FindFunction("TafNmFolder")
		oView:AddSheet( 'PASTAS', 'ABA01', TafNmFolder("recibo") )   //"Numero do Recibo"
	Else
		oView:AddSheet( 'PASTAS', 'ABA01', STR0013)//'Protocolo da �ltima Transmiss�o'
	EndIf

	oView:CreateHorizontalBox( 'FIELDSC8Ba', 045,,,'PASTAS','ABA00')  
	oView:CreateHorizontalBox( 'FIELDSC8Bb', 100,,,'PASTAS','ABA01') 

	oView:CreateHorizontalBox("PAINEL_INFERIOR",055,,,"PASTAS","ABA00")
	oView:CreateFolder( 'FOLDER_INFERIOR', 'PAINEL_INFERIOR' )

	oView:AddSheet( 'FOLDER_INFERIOR', 'ABA01', STR0002)//'Exames' 
		
	oView:CreateHorizontalBox( 'GRIDC9W', 100,,,'FOLDER_INFERIOR','ABA01') 
		
	oView:SetOwnerView( 'VIEW_C8Ba', 'FIELDSC8Ba' )
	oView:SetOwnerView( 'VIEW_C8Bb', 'FIELDSC8Bb' )
	oView:SetOwnerView( 'VIEW_C9W', 'GRIDC9W' )

EndIf
	
	
Return oView
//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@param  oModel -> Modelo de dados
@return .T.

@author Vitor Siqueira
@since 03/03/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel(oModel)

Local nOperation	:= oModel:GetOperation()
Local nX			:= 0
Local nY			:= 0

Local oModelC8B 	:= oModel:GetModel("MODEL_C8B") 

Local cVerAnt   	:= ""  
Local cProtocolo	:= ""
Local cVersao   	:= ""    
Local cEvento		:= "" 
Local cChvRegAnt	:= ""

Local aGrava	 	:= {}
Local aGravaC9W		:= {}
Local aGravaCRP		:= {}

Local lRetorno  	:= .T.

Local cLogOpe		:= ""
Local cLogOpeAnt	:= ""

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )
	
Begin Transaction	

	If nOperation == MODEL_OPERATION_INSERT 

	TafAjustID( "C8B", oModel)

		oModel:LoadValue( 'MODEL_C8B', 'C8B_VERSAO', xFunGetVer() )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_C8B', 'C8B_LOGOPE' , '2', '' )
		Endif

		FwFormCommit( oModel )    
		
	ElseIf nOperation == MODEL_OPERATION_UPDATE 
	
		//�����������������������������������������������������������������Ŀ
		//�Seek para posicionar no registro antes de realizar as validacoes,�
		//�visto que quando nao esta pocisionado nao eh possivel analisar   �
		//�os campos nao usados como _STATUS                                �
		//�������������������������������������������������������������������
	    C8B->( DbSetOrder( 3 ) )
	    If lGoExtemp .OR. C8B->( MsSeek( xFilial( 'C8B' ) + M->C8B_ID + '1' ) )			
			
			//��������������������������������Ŀ
			//�Se o registro ja foi transmitido�
			//����������������������������������
			If C8B->C8B_STATUS $ ( "4" )
			
				oModelC8B := oModel:GetModel( 'MODEL_C8B' )
				oModelC9W := oModel:GetModel( 'MODEL_C9W' ) 
				
				//�Busco a versao anterior do registro para gravacao do rastro�
				cVerAnt   	:= oModelC8B:GetValue( "C8B_VERSAO" )				
				cProtocolo	:= oModelC8B:GetValue( "C8B_PROTUL" )
				cEvento		:= oModelC8B:GetValue( "C8B_EVENTO" )

				If TafColumnPos( "C8B_LOGOPE" )
					cLogOpeAnt := oModelC8B:GetValue( "C8B_LOGOPE" )
				endif

				//�Neste momento eu gravo as informacoes que foram carregadas    �
				//�na tela, pois o usuario ja fez as modificacoes que precisava  �
				//�mesmas estao armazenadas em memoria, ou seja, nao devem ser   �
				//� consideradas agora.					                         �
				For nX := 1 To 1
					For nY := 1 To Len( oModelC8B:aDataModel[ nX ] )			
						Aadd( aGrava, { oModelC8B:aDataModel[ nX, nY, 1 ], oModelC8B:aDataModel[ nX, nY, 2 ] } )									
					Next
				Next  
				
				For nX := 1 To oModel:GetModel( 'MODEL_C9W' ):Length() 
					oModel:GetModel( 'MODEL_C9W' ):GoLine(nX)
					
					If !oModel:GetModel( 'MODEL_C9W' ):IsDeleted()								
						aAdd (aGravaC9W ,{oModelC9W:GetValue('C9W_DTEXAM'),;
											oModelC9W:GetValue('C9W_CODPRO'),;
											oModelC9W:GetValue('C9W_OBS'),;
											oModelC9W:GetValue('C9W_INTERP'),;
											oModelC9W:GetValue('C9W_ORDEXA'),;
											oModelC9W:GetValue('C9W_DTINMO'),;
											oModelC9W:GetValue('C9W_DTFIMO'),;
											oModelC9W:GetValue('C9W_INDRES'),;
											oModelC9W:GetValue('C9W_NISRES'),;
											oModelC9W:GetValue('C9W_CRMRES'),;
											oModelC9W:GetValue('C9W_CRMUF')} )
					EndIf					
				Next nX
					
				
				//�����������������������������������������������������������Ŀ
				//�Seto o campo como Inativo e gravo a versao do novo registro�
				//�no registro anterior                                       � 
				//|                                                           |
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������
				FAltRegAnt( 'C8B', '2' ) 
						
				//��������������������������������������������������Ŀ
				//�Neste momento eu preciso setar a operacao do model�
				//�como Inclusao                                     �
				//����������������������������������������������������
				oModel:DeActivate()
				oModel:SetOperation( 3 ) 	
				oModel:Activate() 
				
				//�������������������������������������������������������Ŀ
				//�Neste momento eu realizo a inclusao do novo registro ja�
				//�contemplando as informacoes alteradas pelo usuario     �
				//���������������������������������������������������������
				For nX := 1 To Len( aGrava )	
					oModel:LoadValue( 'MODEL_C8B', aGrava[ nX, 1 ], aGrava[ nX, 2 ] )
				Next

				//Necess�rio Abaixo do For Nao Retirar
				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_C8B', 'C8B_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				For nX := 1 To Len( aGravaC9W )
					If nX > 1
						oModel:GetModel( 'MODEL_C9W' ):AddLine()
					EndIf
					oModel:LoadValue( "MODEL_C9W", "C9W_DTEXAM", 		aGravaC9W[nX][1] )
					oModel:LoadValue( "MODEL_C9W", "C9W_CODPRO", 		aGravaC9W[nX][2] )						
					oModel:LoadValue( "MODEL_C9W", "C9W_OBS", 		aGravaC9W[nX][3] )						
					oModel:LoadValue( "MODEL_C9W", "C9W_INTERP", 		aGravaC9W[nX][4] )						
					oModel:LoadValue( "MODEL_C9W", "C9W_ORDEXA", 		aGravaC9W[nX][5] )						
					oModel:LoadValue( "MODEL_C9W", "C9W_DTINMO", 		aGravaC9W[nX][6] )						
					oModel:LoadValue( "MODEL_C9W", "C9W_DTFIMO", 		aGravaC9W[nX][7] )						
					oModel:LoadValue( "MODEL_C9W", "C9W_INDRES", 		aGravaC9W[nX][8] )						
					oModel:LoadValue( "MODEL_C9W", "C9W_NISRES", 		aGravaC9W[nX][9] )						
					oModel:LoadValue( "MODEL_C9W", "C9W_CRMRES", 		aGravaC9W[nX][10] )						
					oModel:LoadValue( "MODEL_C9W", "C9W_CRMUF", 		aGravaC9W[nX][11] )			
				Next 
				
				//�������������������������������Ŀ
				//�Busco a versao que sera gravada�
				//���������������������������������
				cVersao := xFunGetVer()	
				
				//�����������������������������������������������������������Ŀ		
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������		                                                                      				         
				oModel:LoadValue( 'MODEL_C8B', 'C8B_VERSAO', cVersao )
				oModel:LoadValue( 'MODEL_C8B', 'C8B_VERANT', cVerAnt )
				oModel:LoadValue( 'MODEL_C8B', 'C8B_PROTPN', cProtocolo )
				oModel:LoadValue( 'MODEL_C8B', 'C8B_PROTUL', "" )
				oModel:LoadValue( 'MODEL_C8B', 'C8B_EVENTO', "A" )
				
				// Tratamento para limpar o ID unico do xml
				cAliasPai := "C8B"
				If TAFColumnPos( cAliasPai+"_XMLID" )
					oModel:LoadValue( 'MODEL_'+cAliasPai, cAliasPai+'_XMLID', "" )
				EndIf
				
				//Gravo altera��o para o Extempor�neo
				If lGoExtemp			
					TafGrvExt( oModel, 'MODEL_C8B', 'C8B' )				
				Endif	
				
				FwFormCommit( oModel )
				TAFAltStat( 'C8B', " " )  
				
			ElseIf	C8B->C8B_STATUS == "2"                                                                 
				TAFMsgVldOp(oModel,"2")//"Registro n�o pode ser alterado. Aguardando processo da transmiss�o."
				lRetorno:= .F.
			ElseIf C8B->C8B_STATUS == "6"                                                                                                                                                                                                                                                                                                                               
				TAFMsgVldOp(oModel,"6")//"Registro n�o pode ser alterado. Aguardando proc. Transm. evento de Exclus�o S-3000"
				lRetorno:= .F.
			Elseif C8B->C8B_STATUS == "7"
				TAFMsgVldOp(oModel,"7") //"Registro n�o pode ser alterado, pois o evento j� se encontra na base do RET"  
				lRetorno:= .F.
			Else
				//altera��o sem transmiss�o

				If TafColumnPos( "C8B_LOGOPE" )
					cLogOpeAnt := oModelC8B:GetValue( "C8B_LOGOPE" )
				endif

				//Gravo altera��o para o Extempor�neo
				If lGoExtemp
					TafGrvExt( oModel, 'MODEL_C8B', 'C8B' )				
				Endif	

				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_C8B', 'C8B_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				FwFormCommit( oModel )
				TAFAltStat( 'C8B', " " )  
			EndIf
		EndIf
	ElseIf nOperation == MODEL_OPERATION_DELETE 
	
		cChvRegAnt := C8B->(C8B_ID + C8B_VERANT)              
											
		If !Empty( cChvRegAnt ) 
			TAFAltStat( 'C8B', " " )
			FwFormCommit( oModel )				
			If nOperation == MODEL_OPERATION_DELETE
				If C8B->C8B_EVENTO == "A" .Or. C8B->C8B_EVENTO == "E"
					TAFRastro( 'C8B', 1, cChvRegAnt, .T., , IIF(Type ("oBrw") == "U", Nil, oBrw ))
				EndIf
			EndIf
		Else
			oModel:DeActivate()
			oModel:SetOperation( 5 ) 	
			oModel:Activate()
			FwFormCommit( oModel )
		EndIf
				
	EndIf
		    			
End Transaction        	
			
If !lRetorno 
	// Define a mensagem de erro que ser� exibida ap�s o Return do SaveModel
	TAFMsgDel(oModel,.T.)
EndIf

Return( lRetorno )
//-------------------------------------------------------------------
/*/{Protheus.doc} TAF258Xml
@author Vitor Siqueira
@since 03/03/2016
@version 1.0
		
@Param:
lJob - Informa se foi chamado por Job
lRemEmp - Exclusivo do Evento S-1000
cSeqXml - Numero sequencial para composi��o da chave ID do XML

@return
cXml - Estrutura do Xml do Layout S-2220 
/*/
//-------------------------------------------------------------------
Function TAF258Xml(cAlias,nRecno,nOpc,lJob,lRemEmp,cSeqXml)

Local cXmlResp	:= ""
Local cXml    	:= ""
Local cLayout 	:= "2220"
Local cReg    	:= "Monit" 
Local cIdCateg	:= ""
Local cCodCat		:= ""
Local lXmlVLd		:= IIF(FindFunction('TafXmlVLD'),TafXmlVLD('TAF258XML'),.T.)


Default lJob 		:= .F.
Default cAlias 	:= "C8B"
Default nRecno	:= 1
Default nOpc		:= 1
Default cSeqXml := ""
If lXmlVLd

	If TafLayESoc("02_05_00")
		//ideVinculo
		DBSelectArea("C9V")
		C9V->(DBSetOrder(2))
		C9V->(MsSeek(C8B->C8B_FILIAL+C8B->C8B_FUNC+"1"))
		cXml +=		"<ideVinculo>"
		cXml +=	    xTafTag("cpfTrab",C9V->C9V_CPF,,.T.)
		cXml +=			xTafTag("nisTrab",C9V->C9V_NIS,,.T. )
		cXml +=			xTafTag("matricula",C9V->C9V_MATRIC,,.T.)

		If C9V->C9V_NOMEVE == "S2300"
			cIdCateg := C9V->C9V_CATCI
			If !Empty( cIdCateg )
				cCodCat := Posicione("C87",1,xFilial("C87") + cIdCateg, "C87_CODIGO")
			EndIf
			cXml +=	xTafTag("codCateg",cCodCat)
		EndIf
		
		cXml +=		"</ideVinculo>"

		//exMedOcup
		cXml +=		"<exMedOcup>"
		cXml +=			xTafTag("tpExameOcup",C8B->C8B_TPEXAM,,.T.)

		//aso
		cXml +=			"<aso>"
		cXml +=				xTafTag("dtAso",C8B->C8B_DTASO,,.T.)
		cXml +=				xTafTag("resAso",C8B->C8B_RESULT,,.T.)

		//exame
		DBSelectArea("C9W")  
		C9W	->(DBSetOrder(1))	
		If C9W->(MsSeek(C8B->C8B_FILIAL+C8B->C8B_ID+C8B->C8B_VERSAO ))
			While C9W->(!Eof()) .And. C9W->C9W_ID == C8B->C8B_ID .And. C9W->C9W_VERSAO == C8B->C8B_VERSAO 

			cXml +=				"<exame>"
			cXml +=					xTafTag("dtExm", C9W->C9W_DTEXAM,,.T.)
			cXml +=					xTafTag("procRealizado", AllTrim(Posicione("V2K",1,xFilial("V2K")+ C9W->C9W_CODPRO, "V2K_CODIGO")),,.T.)
			cXml +=					xTafTag("obsProc", C9W->C9W_OBS,,.T.)
			cXml +=					xTafTag("ordExame", C9W->C9W_ORDEXA,,.T.)
			cXml +=					xTafTag("indResult", C9W->C9W_INDRES,,.T.)
			cXml +=				"</exame>"

			C9W->(DbSkip())

			EndDo
		EndIf

		//medico
		DBSelectArea("CM7")
		CM7	->(DBSetOrder(1))
		CM7->(MsSeek(xFilial('CM7')+C8B->C8B_CODMED))
		cXml +=				"<medico>"
		cXml +=					xTafTag("cpfMed", CM7->CM7_CPF,,.T.)
		cXml +=					xTafTag("nisMed", CM7->CM7_NIS,,.T.)
		cXml +=					xTafTag("nmMed", CM7->CM7_NOME,,.T.)
		cXml +=					xTafTag("nrCRM", CM7->CM7_NRIOC,,.T.)
		cXml +=					xTafTag("ufCRM", Posicione("C09",3, xFilial("C09")+CM7->CM7_NRIUF ,"C09_UF"),,.T.)
		cXml +=				"</medico>"
		cXml +=			"</aso>"

		//respMonit
		cXml +=			"<respMonit>"
		cXml +=					xTafTag("cpfResp",C8B->C8B_CPFRES,,.T.)
		cXml +=					xTafTag("nmResp",	C8B->C8B_NOMRES,,.T.)
		cXml +=					xTafTag("nrCRM", C8B->C8B_NRCRM,,.T. )
		cXml +=					xTafTag("ufCRM", Posicione("C09",3, xFilial("C09")+C8B->C8B_CRMUF ,"C09_UF"),,.T.)
		cXml +=			"</respMonit>"
		cXml +=		"</exMedOcup>"

	Else
		DBSelectArea("C9V")  
		C9V->(DBSetOrder(2))
		C9V->(MsSeek(C8B->C8B_FILIAL+C8B->C8B_FUNC+"1"))
		cXml +=		"<ideVinculo>"    
		cXml +=	    	xTafTag("cpfTrab",C9V->C9V_CPF)  
		cXml +=			xTafTag("nisTrab",C9V->C9V_NIS,,.T.)
		cXml +=			xTafTag("matricula",C9V->C9V_MATRIC,,.T.)	

		cXml +=		"</ideVinculo>"
		cXml +=		"<aso>"
		cXml +=			xTafTag("dtAso",C8B->C8B_DTASO)
		cXml +=			xTafTag("tpAso",C8B->C8B_TPASO)
		cXml +=			xTafTag("resAso",C8B->C8B_RESULT)
			
		DBSelectArea("C9W")  
		C9W	->(DBSetOrder(1))	
		If C9W->(MsSeek(C8B->C8B_FILIAL+C8B->C8B_ID+C8B->C8B_VERSAO ))
			While C9W->(!Eof()) .And. C9W->C9W_ID == C8B->C8B_ID .And. C9W->C9W_VERSAO == C8B->C8B_VERSAO 
				
				xTafTagGroup("respMonit",{ { "nisResp"	    	,C9W->C9W_NISRES				 										,,.F. }; 
												,  { "nrConsClasse" 	,C9W->C9W_CRMRES			                                  		,,.F. };
													, {  "ufConsClasse" 	,POSICIONE("C09",3, xFilial("C09")+C9W->C9W_CRMUF ,"C09_UF")	,,.T. }};
												,  @cXmlResp)
				
				xTafTagGroup("exame",{ { "dtExm"	    		,C9W->C9W_DTEXAM			 											,,.T. }; 
											,  { "procRealizado" 		,C9W->C9W_CODPRO				                                   ,,.T. };
											,  {	"obsProc" 				,C9W->C9W_OBS                                 					,,.F. };
											,  {	"interprExm" 			,C9W->C9W_INTERP                                        		,,.F. };
											,  {	"ordExame"       		,C9W->C9W_ORDEXA                                               ,,.F. };
											,  {	"dtIniMonit"	    	,C9W->C9W_DTINMO                                        		,,.F. };
											,  {	"dtFimMonit"      	,C9W->C9W_DTFIMO  													,,.T. };
											,  {	"indResult"  			,C9W->C9W_INDRES                                               ,,.T. }};
											,  @cXml, { {"respMonit", cXmlResp, 1 } })		
				
				cXmlResp := ""
				C9W->(DbSkip())
			EndDo
		EndIf
							
		cXml +=			"<ideServSaude>"			
		cXml +=				xTafTag("codCNES",C8B->C8B_CODCNE,,.T.)
		cXml +=				xTafTag("frmCtt" ,C8B->C8B_CONTAT) 
		cXml +=				xTafTag("email"  ,C8B->C8B_EMAIL,,.T.)


		DBSelectArea("CM7")  
		CM7	->(DBSetOrder(1))
		CM7->(MsSeek(xFilial('CM7')+C8B->C8B_CODMED))	
		cXml +=				"<medico>"			
		cXml +=					xTafTag("nmMed",CM7->CM7_NOME) 
		cXml +=					"<crm>"	
		cXml +=						xTafTag("nrCRM",CM7->CM7_NRIOC)  
		cXml +=						xTafTag("ufCRM",POSICIONE("C09",3, xFilial("C09")+CM7->CM7_NRIUF ,"C09_UF"))
		cXml += 				"</crm>"				
		cXml +=				"</medico>"
		cXml +=			"</ideServSaude>"
		cXml += 	"</aso>"
	EndIf

	/*����������������������Ŀ
	�Estrutura do cabecalho�
	������������������������*/
	cXml := xTafCabXml(cXml,"C8B",cLayout,cReg,,cSeqXml)
		
	/*����������������������������Ŀ
	�Executa gravacao do registro�
	������������������������������*/
	If !lJob
		xTafGerXml(cXml,cLayout)
	EndIf
EndIf
Return(cXml)
//-------------------------------------------------------------------	
/*/{Protheus.doc} TAF258Vld
@author Vitor Siqueira
@since 03/03/2016
@version 1.0

@Param:
lJob - Informa se foi chamado por Job

@return:
.T.
/*/
//-------------------------------------------------------------------
Function TAF258Vld(cAlias,nRecno,nOpc,lJob)

Local aLogErro  	:= {}
Local aAux			:= {}
Local aCampos		:= {}
Local aDadosUtil 	:= {}

Local cStatus		:= ""
Local cIdCateg		:= ""
Local cCodCat	 	:= ""

Local lValid	 	:= .T.
Local dDataAdm  	:= CTOD(" / / ")

Default lJob	:= .F.

//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := C8B->( Recno() ) 
	
If C8B->C8B_STATUS $ ( " |1" )

	//REGRA_EXISTE_CAT_OBITO_ANTERIOR
    aAdd(aDadosUtil, {C8B->C8B_FUNC, C8B->C8B_DTASO})  
    xVldRegra("CM0","REGRA_EXISTE_CAT_OBITO_ANTERIOR",@aLogErro, aDadosUtil,"CM0") 

	If nOpc <> 5	
		DBSelectArea("C9V")  
		C9V->(DBSetOrder(2))
		If !C9V->(MsSeek(C8B->C8B_FILIAL+C8B->C8B_FUNC+"1"))
			aAdd(aLogErro,{"C8B_FUNC", "000010", "C8B", nRecno})	//STR0010 - Campo inconsistente ou vazio			
		EndIf
		
		If TafLayESoc("02_05_00")
			If !(C8B->C8B_TPEXAM $ "0|1|2|3|4|9")
				aAdd(aLogErro,{"C8B_TPEXAM", "000006", "C8B", nRecno})	//STR0006 - Conteudo do campo nao condiz com as opcoes possiveis			
			EndIf
			
			If Empty(C8B->C8B_NOMRES)
				aAdd(aLogErro,{"C8B_NOMRES", "000010", "C8B", nRecno})	//STR0010 - Campo inconsistente ou vazio			
			EndIf
			
			If Empty(C8B->C8B_NRCRM)
				aAdd(aLogErro,{"C8B_NRCRM", "000010", "C8B", nRecno})	//STR0010 - Campo inconsistente ou vazio			
			EndIf
			
			If Empty(C8B->C8B_CRMUF)
				aAdd(aLogErro,{"C8B_CRMUF", "000010", "C8B", nRecno})	//STR0010 - Campo inconsistente ou vazio			
			EndIf
			
		Else
			If !(C8B->C8B_TPASO $ "0|1|2|3|4|8")
				aAdd(aLogErro,{"C8B_TPASO"	,"000006","C8B",nRecno})	//STR0006 - Conteudo do campo nao condiz com as opcoes possiveis 
			EndIf
		EndIf
		
		If Empty(DtoS(C8B->C8B_DTASO))                                           
			aAdd(aLogErro,{"C8B_DTASO"	,"000009","C8B",nRecno})	//STR0009 - Data Inconsistente ou Vazia	
		EndIf
				
		If !(C8B->C8B_RESULT $ "1|2")
			aAdd(aLogErro,{"C8B_RESULT"	,"000006","C8B",nRecno})	//STR0006 - Conteudo do campo nao condiz com as opcoes possiveis
		EndIf	
		
		//Exame
	 	DBSelectArea("C9W")  
		C9W->(DBSetOrder(1))
		If C9W->(MsSeek(C8B->C8B_FILIAL+C8B->C8B_ID+C8B->C8B_VERSAO ))
			While C9W->(!Eof()) .And. C9W->C9W_ID == C8B->C8B_ID .And. C9W->C9W_VERSAO == C8B->C8B_VERSAO 
				If Empty(DtoS(C9W->C9W_DTEXAM))                                          
					aAdd(aLogErro,{"C9W_DTEXAM"	,"000009","C8B",nRecno })	  //STR0009 - Data Inconsistente ou Vazia	
				EndIf
				If C9W->C9W_DTEXAM > C8B->C8B_DTASO 
					aAdd(aLogErro,{"C9W_DTEXAM"	,"000308","C8B",nRecno})	  //STR0308 - Data do exame deve ser anterior ou igual a data do ASO 
				Endif
				
				If Empty(C9W->C9W_CODPRO)                                          
					aAdd(aLogErro,{"C9W_OBS","000010","C8B",nRecno })	  //STR0010 - Campo inconsistente ou vazio
				Else		
					If Len(Alltrim(C9W->C9W_CODPRO)) <> 6                                         
						aAdd(aLogErro,{"C9W_CODPRO"	,"000010","C8B",nRecno }) //STR0861 - Deve ser preenchido com um n�mero de 8 posi��es
					EndIf
				EndIf
		
				If Empty(C9W->C9W_ORDEXA)
					aAdd(aLogErro,{"C9W_ORDEXA"	,"000010","C8B",nRecno })	//STR0010 - Campo Inconsistente ou Vazio  
				EndIf
				
				If !TafLayESoc("02_05_00")
					If Empty(C9W->C9W_INTERP)
						aAdd(aLogErro,{"C9W_INTERP"	,"000010","C8B",nRecno })	//STR0010 - Campo Inconsistente ou Vazio  
					EndIf
					If Empty(C9W->C9W_DTINMO)
						aAdd(aLogErro,{"C9W_DTINMO"	,"000010","C8B",nRecno })	//STR0010 - Campo Inconsistente ou Vazio  
					EndIf
					
					//Valida se a data inicial da monitora��o � menor do que a data final
					If !Empty(C9W->C9W_DTINMO) .AND. !Empty(C9W->C9W_DTFIMO)
						If (C9W->C9W_DTINMO) > (C9W->C9W_DTFIMO)
							aAdd(aLogErro,{"C9W_DTFIMO"	,"000435","C8B",nRecno })//STR0435 - "A data de t�rmino da monitora��o biol�gica deve ser superior a data de in�cio da monitora��o."
						ElseIf !Empty(dDataAdm) .And. (C9W->C9W_DTINMO) < (dDataAdm)
							aAdd(aLogErro,{"C9W_DTINMO"	,"000436","C8B",nRecno })//STR0436 - "A data de in�cio da monitora��o biol�gica deve ser superior a data de admiss�o do funcion�rio."
						EndIf 
					EndIf 
							
					//RespMonit
					If Empty(C9W->C9W_NISRES)
						aAdd(aLogErro,{"C9W_NISRES",'000010',"C8B",nRecno})	//STR0010 - Campo Inconsistente ou Vazio 
					EndIf 
					If Empty(C9W->C9W_CRMRES)
						aAdd(aLogErro,{"C9W_CRMRES",'000010',"C8B",nRecno})	//STR0010 - Campo Inconsistente ou Vazio
					EndIf 
					If Empty(C9W->C9W_CRMUF)
						aAdd(aLogErro,{"C9W_CRMUF" ,'000010',"C8B",nRecno }) //STR0010 - Campo Inconsistente ou Vazio
					EndIf
				EndIf
							
				C9W->(DbSkip())
			EndDo
		EndIf
		
		
		//M�dico
		DBSelectArea("CM7")  
		CM7->(DBSetOrder(1))
		CM7->(MsSeek(xFilial('CM7')+C8B->C8B_CODMED))	
		If Empty(CM7->CM7_NOME)
			aAdd(aLogErro,{"CM7_NOME"	,'000010',"C8B",nRecno})	//STR0010 - Campo Inconsistente ou Vazio 
		EndIf 
		If Empty(CM7->CM7_NRIOC)
			aAdd(aLogErro,{"CM7_NRIOC"	,'000010',"C8B",nRecno})	//STR0010 - Campo Inconsistente ou Vazio
		EndIf 
		If Empty(CM7->CM7_NRIUF)
			aAdd(aLogErro,{"CM7_NRIUF",'000010',"C8B",nRecno })	//STR0010 - Campo Inconsistente ou Vazio
		EndIf 
		
		If !TafLayESoc("02_05_00")	
			//RespMonit
			If Empty(C8B->C8B_CONTAT)
				aAdd(aLogErro,{"C8B_CONTAT",'000010',"C8B",nRecno})	//STR0010 - Campo Inconsistente ou Vazio 
			EndIf 
			
			If !Empty(C8B->C8B_EMAIL)
				If !xVldMail(C8B->C8B_EMAIL)
					aAdd(aLogErro,{"C8B_EMAIL" ,'000862',"C8B",nRecno }) //STR0862 - O e-mail deve possuir o caractere '@' e este n�o pode estar no in�cio e no fim do e-mail. Deve possuir no m�nimo um caractere '.' depois do @ e n�o pode estar no fim do e-mail.
				EndIf
			EndIf
		EndIf

		//REGRA_VALIDA_TRABALHADOR_BASE_CNIS
		lValid := .T.
		C9V->( DbSetOrder( 2 ) )
		If C9V->( MsSeek ( xFilial("C9V") + C8B->C8B_FUNC + "1") )
			cIdCateg := Posicione("CUP",1,xFilial("CUP") + C9V->(C9V_ID + C9V_VERSAO), "CUP_CODCAT")
	
			If !Empty( cIdCateg )
				cCodCat := Posicione("C87",1,xFilial("C87") + cIdCateg, "C87_CODIGO")
	
				If !Empty(cCodCat) .And. !(cCodCat $ "901|903|904") .And. Empty( C9V->C9V_NIS )
					lValid := .F.
					AADD(aLogErro,{"C9V_NIS", "001049", "C9V", nRecno }) //STR1049 - "O preenchimento � obrigat�rio, exceto se o c�digo de categoria do trabalhador for igual a [901, 903, 904]."
				Endif
			EndIf

			If (TafColumnPos('C9V_CATCI')) .And. lValid
				cCodCat := Posicione("C87",1,xFilial("C87") + C9V->C9V_CATCI, "C87_CODIGO")

				If !Empty(cCodCat) .And. !(cCodCat $ "901|903|904") .And. Empty( C9V->C9V_NIS )
					AADD(aLogErro,{"C9V_NIS", "001049", "C9V", nRecno }) //STR1049 - "O preenchimento � obrigat�rio, exceto se o c�digo de categoria do trabalhador for igual a [901, 903, 904]."
				Endif
			EndIf
		Endif

		//ATUALIZO O STATUS DO REGISTRO
		cStatus := Iif(Len(aLogErro) > 0,"1","0")
		
		Begin Transaction 
			If RecLock("C8B",.F.)
				C8B->C8B_STATUS := cStatus
				C8B->(MsUnlock())
			EndIf
		End Transaction 
	EndIf
Else
	AADD(aLogErro,{"C8B","000305","C8B",nRecno}) //Registros que j� foram transmitidos ao Fisco, n�o podem ser validados	
EndIf
	
//Nao apresento o alert quando utilizo o JOB para validar
If !lJob
	xValLogEr(aLogErro)
EndIf

Return(aLogErro)
//-------------------------------------------------------------------	
/*/{Protheus.doc} TAF258Grv
Funcao de gravacao para atender o registro S-2220	
@author Vitor Siqueira
@since 03/03/2016
@version 1.0
		
@parametros
cLayout - Nome do Layout que esta sendo enviado, existem situacoes onde o mesmo fonte
          alimenta mais de um regsitro do E-Social, para estes casos serao necessarios
          tratamentos de acordo com o layout que esta sendo enviado.
nOpc   -  Opcao a ser realizada ( 3 = Inclusao, 4 = Alteracao, 5 = Exclusao )
cFilEv -  Filial do ERP para onde as informacoes deverao ser importadas
oXML   -  Objeto com as informacoes a serem manutenidas ( Outras Integracoes )  

@Return    
lRet    - Variavel que indica se a importacao foi realizada, ou seja, se as 
		  informacoes foram gravadas no banco de dados
aIncons - Array com as inconsistencias encontradas durante a importacao 
/*/
//-------------------------------------------------------------------
Function TAF258Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID )

Local cLogOpeAnt 	:= ""

Local cCmpsNoUpd 	:= "|C8B_FILIAL|C8B_ID|C8B_VERSAO|C8B_VERANT|C8B_PROTUL|C8B_PROTPN|C8B_EVENTO|C8B_STATUS|C8B_ATIVO|"
Local cCabec     	:= "/eSocial/evtMonit"
Local cIdCUA		:= ""
Local cIdCUM		:= ""
Local cMatBio		:= ""	
Local cChave		:= ""
Local cInconMsg   	:= ""
Local cCodEvent  	:= Posicione("C8E",2,xFilial("C8E")+"S-"+cLayout,"C8E->C8E_ID")

Local lRet       	:= .F.
Local aIncons    	:= {}
Local aRules     	:= {}
Local aChave     	:= {}
Local oModel     	:= Nil

Local nX			:= 0
Local nJ			:= 0
Local nSeqErrGrv  	:= 0

Private oDados 		:= oXML
Private lVldModel 	:= .T. //Caso a chamada seja via integracao seto a variavel de controle de validacao como .T.
	
Default cLayout 	:= ""
Default nOpc     	:= 1
Default cFilEv   	:= ""
Default oXML     	:= Nil
Default cOwner		:= ""
Default cFilTran	:= ""
Default cPredeces	:=	""
Default nTafRecno	:=	0
Default cComplem	:=	""
Default cGrpTran	:=	""
Default cEmpOriGrp	:=	""
Default cFilOriGrp	:=	""
Default cXmlID		:=	""

Aadd( aChave, { "C", "C8B_FUNC"	, FGetIdInt( "cpfTrab", "", cCabec + "/ideVinculo/cpfTrab"), .T. } )
	cChave += Padr( aChave[ 1, 3 ], Tamsx3( aChave[ 1, 2 ])[1])

If TafLayESoc("02_05_00")
	Aadd( aChave, { "D", "C8B_DTASO", FTafGetVal( cCabec +  "/exMedOcup/aso/dtAso", "D", .F., @aIncons, .F. ), .T. } )
		cChave +=  DTOS(aChave[ 2, 3 ])
Else
	Aadd( aChave, { "D", "C8B_DTASO", FTafGetVal( cCabec +  "/aso/dtAso", "D", .F., @aIncons, .F. ), .T. } )
		cChave +=  DTOS(aChave[ 2, 3 ])
EndIf

//Verifica se o evento ja existe na base
("C8B")->( DbSetOrder( 2 ) )
If ("C8B")->( MsSeek( xFilial("C8B") + cChave +'1' ) )
	If !C8B->C8B_STATUS $ ( "2|4|6|" )
		nOpc := 4
	EndIf
EndIf
	
	
Begin Transaction 

	//Funcao para validar se a operacao desejada pode ser realizada
	If FTafVldOpe( "C8B", 2, @nOpc, cFilEv, @aIncons, aChave, @oModel, "TAFA258", cCmpsNoUpd )

		If TafColumnPos( "C8B_LOGOPE" )
			cLogOpeAnt := C8B->C8B_LOGOPE
		endif

		//����������������������������������������������������������������Ŀ
		//�Quando se tratar de uma Exclusao direta apenas preciso realizar �
		//�o Commit(), nao eh necessaria nenhuma manutencao nas informacoes�
		//������������������������������������������������������������������
		If nOpc <> 5
			
			/*�����������������������������������������������������������������Ŀ                                                    
			  �Carrego array com os campos De/Para de gravacao das informacoes  �
			  �������������������������������������������������������������������*/
			aRules := TAF258Rul(@cInconMsg, @nSeqErrGrv, cCodEvent, cOwner )
			oModel:LoadValue( "MODEL_C8B", "C8B_FILIAL", C8B->C8B_FILIAL )
			
			If TAFColumnPos( "C8B_XMLID" )
				oModel:LoadValue( "MODEL_C8B", "C8B_XMLID", cXmlID )
			EndIf															
	            			
			/*����������������������������������������Ŀ
			  �Rodo o aRules para gravar as informacoes�
			  ������������������������������������������*/
			For nX := 1 To Len( aRules )                 					
			 	oModel:LoadValue( "MODEL_C8B", aRules[ nX, 01 ], FTafGetVal( aRules[ nX, 02 ], aRules[nX, 03], aRules[nX, 04], @aIncons, .F. ) )
			Next

			If TafLayESoc("02_05_00")
				oModel:LoadValue( "MODEL_C8B", "C8B_CRMUF", FGetIdInt( "ufCRM", "", cCabec + "/exMedOcup/respMonit/ufCRM",,,,@cInconMsg, @nSeqErrGrv) )
			EndIf

			If Findfunction("TAFAltMan")
				if nOpc == 3
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_C8B', 'C8B_LOGOPE' , '1', '' )
				elseif nOpc == 4
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_C8B', 'C8B_LOGOPE' , '', cLogOpeAnt )
				EndIf
			EndIf

			/*������������������ ����������������������������������������������Ŀ
			  �Quando se trata de uma alteracao, deleto todas as linhas do Grid�
			  ������������������������������������������������������������������*/				
		   	If nOpc == 4
				For nJ := 1 to oModel:GetModel( 'MODEL_C9W' ):Length()  
					oModel:GetModel( 'MODEL_C9W' ):GoLine(nJ)
					oModel:GetModel( 'MODEL_C9W' ):DeleteLine() 
				Next nJ
		 	EndIf      
	
			/*������������������������������������������������������������Ŀ
			  �Rodo o XML parseado para gravar as novas informacoes no GRID�
			  ��������������������������������������������������������������*/
		  	nJ := 1
			
			If TafLayESoc("02_05_00")

				While oDados:XPathHasNode(cCabec +  "/exMedOcup/aso/exame[" + CVALTOCHAR(nJ)+ "]" )			
					
					oModel:GetModel( 'MODEL_C9W' ):LVALID	:= .T.				
					If nOpc == 4 .Or. nJ > 1
						oModel:GetModel( 'MODEL_C9W' ):AddLine()
					EndIf	       
					oModel:LoadValue( "MODEL_C9W", "C9W_DTEXAM"		, FTafGetVal( cCabec + "/exMedOcup/aso/exame[" + CVALTOCHAR(nJ)+ "]/dtExm" 	        , "D", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_CODPRO"		, FGetIdInt( "procRealizado", "", cCabec + "/exMedOcup/aso/exame[" + CVALTOCHAR(nJ)+ "]/procRealizado",,,,@cInconMsg, @nSeqErrGrv ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_OBS"		, FTafGetVal( cCabec + "/exMedOcup/aso/exame[" + CVALTOCHAR(nJ)+ "]/obsProc"  	    , "C", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_ORDEXA"		, FTafGetVal( cCabec + "/exMedOcup/aso/exame[" + CVALTOCHAR(nJ)+ "]/ordExame"  	    , "C", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_INDRES"		, FTafGetVal( cCabec + "/exMedOcup/aso/exame[" + CVALTOCHAR(nJ)+ "]/indResult"  		, "C", .F., @aIncons, .F. ) )
						
					nJ++ 
				EndDo	

			Else
			
				While oDados:XPathHasNode(cCabec +  "/aso/exame[" + CVALTOCHAR(nJ)+ "]" )			
					
					oModel:GetModel( 'MODEL_C9W' ):LVALID	:= .T.				
					If nOpc == 4 .Or. nJ > 1
						oModel:GetModel( 'MODEL_C9W' ):AddLine()
					EndIf	       
						oModel:LoadValue( "MODEL_C9W", "C9W_DTEXAM"		, FTafGetVal( cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/dtExm" 	        , "D", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_CODPRO"		, FTafGetVal( cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/procRealizado"    , "C", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_OBS"			, FTafGetVal( cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/obsProc"  	    , "C", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_INTERP"		, FTafGetVal( cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/interprExm"  	    , "C", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_ORDEXA"		, FTafGetVal( cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/ordExame"  	    , "C", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_DTINMO"		, FTafGetVal( cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/dtIniMonit"  		, "D", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_DTFIMO"		, FTafGetVal( cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/dtFimMonit"  	    , "D", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_INDRES"		, FTafGetVal( cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/indResult"  		, "C", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_NISRES"		, FTafGetVal( cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/respMonit/nisResp"       , "C", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_CRMRES"		, FTafGetVal( cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/respMonit/nrConsClasse"  , "C", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_C9W", "C9W_CRMUF"		, FGetIdInt( "uf", "", cCabec + "/aso/exame[" + CVALTOCHAR(nJ)+ "]/respMonit/ufConsClasse",,,,@cInconMsg, @nSeqErrGrv) )
						
					nJ++ 
				EndDo		 					      					

			EndIf
		
		EndIf
		
		///���������������������������Ŀ
		//�Efetiva a operacao desejada�
		//�����������������������������
		If Empty(cInconMsg)	.And. Empty(aIncons)
			If TafFormCommit( oModel )
				Aadd(aIncons, "ERRO19")
			Else
				lRet := .T.
			EndIf	 
		Else			
			Aadd(aIncons, cInconMsg)	
			DisarmTransaction()	
		EndIf
								 			   
		oModel:DeActivate()
		If FindFunction('TafClearModel')
			TafClearModel(oModel)
		EndIf
				
	EndIf
End Transaction	

//����������������������������������������������������������Ŀ
//�Zerando os arrays e os Objetos utilizados no processamento�
//������������������������������������������������������������
aSize( aRules, 0 ) 
aRules     := Nil

aSize( aChave, 0 ) 
aChave     := Nil    

oModel     := Nil
	

Return { lRet, aIncons } 
//-------------------------------------------------------------------
/*/{Protheus.doc} TAF258Rul
Regras para gravacao das informacoes do registro S-2220 do E-Social
	
@author Vitor Siqueira
@since 03/03/2016
@version 1.0		

@return
aRull  - Regras para a gravacao das informacoes
/*/
//-------------------------------------------------------------------
Function TAF258Rul( cInconMsg, nSeqErrGrv, cCodEvent, cOwner )

Local aRull     := {}
Local aIncons	:= {}
Local aInfComp	:= {}
Local cCabec 	:= "/eSocial/evtMonit"

Default cInconMsg	:= ""
Default nSeqErrGrv	:= 0
Default cCodEvent	:= ""
Default cOwner		:= ""

If TafLayESoc("02_05_00")

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/ideVinculo/cpfTrab"))
		aAdd( aRull, { "C8B_FUNC"  , FGetIdInt( "cpfTrab", "", cCabec + "/ideVinculo/cpfTrab",,,,@cInconMsg, @nSeqErrGrv)  , "C", .T. } ) //cpfTrab
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/tpExameOcup"))
		aAdd( aRull, { "C8B_TPEXAM"  , cCabec + "/exMedOcup/tpExameOcup"  , "C", .F. } ) //tpExameOcup
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/aso/dtAso"))
		aAdd( aRull, { "C8B_DTASO" , cCabec + "/exMedOcup/aso/dtAso"							, "D", .F. } ) //dtAso
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/aso/resAso"))
		aAdd( aRull, { "C8B_RESULT", cCabec + "/exMedOcup/aso/resAso"							, "C", .F. } ) //resAso				
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/respMonit/cpfResp"))
		aAdd( aRull, { "C8B_CPFRES", cCabec + "/exMedOcup/respMonit/cpfResp"							, "C", .F. } ) //cpfResp							
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/respMonit/nrCRM"))
		aAdd( aRull, { "C8B_NRCRM", cCabec + "/exMedOcup/respMonit/nrCRM"							, "C", .F. } ) //nrCRM							
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/respMonit/nmResp"))
		aAdd( aRull, { "C8B_NOMRES", cCabec + "/exMedOcup/respMonit/nmResp"							, "C", .F. } ) //nmResp							
	EndIf

		//Preenchendo array com informa��es que dever�o ser gravadas caso n�o encontre o m�dico.

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/aso/medico/cpfMed"))		 
		Aadd( aInfComp,{'CM7_CPF', FTafGetVal( cCabec + "/exMedOcup/aso/medico/cpfMed", "C", .F., aIncons, .F.)}) 
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/aso/medico/nisMed"))		 
		Aadd( aInfComp,{'CM7_NIS', FTafGetVal( cCabec + "/exMedOcup/aso/medico/nisMed", "C", .F., aIncons, .F.)}) 
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/aso/medico/nmMed"))		 
		Aadd( aInfComp,{'CM7_NOME', FTafGetVal( cCabec + "/exMedOcup/aso/medico/nmMed", "C", .F., aIncons, .F.)}) 
	EndIf

	
	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/aso/medico/nrCRM"))		 
		Aadd( aInfComp,{'CM7_NRIOC', FTafGetVal( cCabec + "/exMedOcup/aso/medico/nrCRM", "C", .F., aIncons, .F.)}) 
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/exMedOcup/aso/medico/ufCRM"))
		Aadd( aInfComp,{'CM7_NRIUF',FGetIdInt( "ufCRM", "", +;
						cCabec + "/exMedOcup/aso/medico/ufCRM",,,,@cInconMsg, @nSeqErrGrv)}) 
	EndIf

	aAdd( aRull, { "C8B_CODMED"	, FGetIdInt( "nrCRM", "", cCabec + "/exMedOcup/aso/medico/nrCRM",,,aInfComp,@cInconMsg, @nSeqErrGrv)	, "C", .T. } ) //Codigo do Medico	

Else

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/ideVinculo/cpfTrab"))
		aAdd( aRull, { "C8B_FUNC"  , FGetIdInt( "cpfTrab", "", cCabec + "/ideVinculo/cpfTrab",,,,@cInconMsg, @nSeqErrGrv)  , "C", .T. } ) //Id.Funcion�rio
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/aso/dtAso"))
		aAdd( aRull, { "C8B_DTASO" , cCabec + "/aso/dtAso"										, "D", .F. } ) //Data ASO
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/aso/tpAso"))
		aAdd( aRull, { "C8B_TPASO" , cCabec + "/aso/tpAso"						   				, "C", .F. } ) //Tipo da  ASO	  
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/aso/resAso"))
		aAdd( aRull, { "C8B_RESULT", cCabec + "/aso/resAso"										, "C", .F. } ) //Resultado da  ASO							
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/aso/ideServSaude/codCNES"))
		aAdd( aRull, { "C8B_CODCNE", cCabec + "/aso/ideServSaude/codCNES"						, "C", .F. } ) //Cod CNES
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/aso/ideServSaude/frmCtt"))
		aAdd( aRull, { "C8B_CONTAT", cCabec + "/aso/ideServSaude/frmCtt"						, "C", .F. } ) //Inf. Contato
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/aso/ideServSaude/email"))
		aAdd( aRull, { "C8B_EMAIL" , cCabec + "/aso/ideServSaude/email"							, "C", .F. } ) //Email
	EndIf 	
			
	//Preenchendo array com informa��es que dever�o ser gravadas caso n�o encontre o m�dico.
	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/aso/ideServSaude/medico/crm/ufCRM"))
		Aadd( aInfComp,{'CM7_NRIUF',FGetIdInt( "uf", "", +;
						cCabec + "/aso/ideServSaude/medico/crm/ufCRM",,,,@cInconMsg, @nSeqErrGrv)}) 
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/aso/ideServSaude/medico/nmMed"))		
		Aadd( aInfComp,{'CM7_NOME',;
				FTafGetVal( cCabec + "/aso/ideServSaude/medico/nmMed",;
				"C", .F., aIncons, .F.)}) 
	EndIf

	If TafXNode( oDados , cCodEvent, cOwner, (cCabec + "/aso/ideServSaude/medico/crm/nrCRM"))		 
		Aadd( aInfComp,{'CM7_NRIOC',;
				FTafGetVal( cCabec + "/aso/ideServSaude/medico/crm/nrCRM",;
				"C", .F., aIncons, .F.)}) 
	EndIf

	//Fim do preenchimento do array auxiliar.
		
	aAdd( aRull, { "C8B_CODMED"	, FGetIdInt( "nrOC", "", cCabec + "/aso/ideServSaude/medico/crm/nrCRM",,,aInfComp,@cInconMsg, @nSeqErrGrv)	, "C", .T. } ) //Codigo do Medico	
		
EndIf

Return(aRull)

//-------------------------------------------------------------------
/*/{Protheus.doc} GerarEvtExc
Funcao que gera a exclus�o do evento (S-3000)

@Param  oModel  -> Modelo de dados
@Param  nRecno  -> Numero do recno
@Param  lRotExc -> Variavel que controla se a function � chamada pelo TafIntegraESocial

@Return .T.

@Author Vitor Henrique Ferreira
@Since 04/03/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function GerarEvtExc( oModel, nRecno, lRotExc )

Local cVerAnt		:= ""
Local cProtocolo	:= ""
Local cVersao  		:= ""
Local cEvento		:= ""
Local cId			:= ""

Local nX			:= 0
Local nY			:= 0
Local nC8B  		:= 0 

Local aGrava    	:= {}
Local aGravaC9W		:= {}
Local aGravaCRP 	:= {}

Local oModelC8B	:= Nil

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

dbSelectArea("C8B")

Begin Transaction

	//Posiciona o item
	("C8B")->( DBGoTo( nRecno ) )
	
	oModelC8B := oModel:GetModel( 'MODEL_C8B' )
	oModelC9W := oModel:GetModel( 'MODEL_C9W' ) 
	
	//�Busco a versao anterior do registro para gravacao do rastro�
	cVerAnt   	:= oModelC8B:GetValue( "C8B_VERSAO" )				
	cProtocolo	:= oModelC8B:GetValue( "C8B_PROTUL" )
	cEvento		:= oModelC8B:GetValue( "C8B_EVENTO" )   
	
	//�Neste momento eu gravo as informacoes que foram carregadas    �
	//�na tela, pois o usuario ja fez as modificacoes que precisava  �
	//�mesmas estao armazenadas em memoria, ou seja, nao devem ser   �
	//� consideradas agora.					                         �
	For nC8B := 1 to Len( oModelC8B:aDataModel[ 1 ] )
		aAdd( aGrava, { oModelC8B:aDataModel[ 1, nC8B, 1 ], oModelC8B:aDataModel[ 1, nC8B, 2 ] } )
	Next nC8B	 
	
	If C9W->(MsSeek(xFilial("C9W")+C8B->(C8B_ID + C8B_VERSAO) ) )
		For nX := 1 To oModel:GetModel( 'MODEL_C9W' ):Length() 
			oModel:GetModel( 'MODEL_C9W' ):GoLine(nX)
			
			If !oModel:GetModel( 'MODEL_C9W' ):IsDeleted()								
				aAdd (aGravaC9W ,{oModelC9W:GetValue('C9W_DTEXAM'),oModelC9W:GetValue('C9W_CODPRO')} )
			EndIf					
		Next nX
	EndIf
	
	
	//�����������������������������������������������������������Ŀ
	//�Seto o campo como Inativo e gravo a versao do novo registro�
	//�no registro anterior                                       � 
	//|                                                           |
	//|ATENCAO -> A alteracao destes campos deve sempre estar     |
	//|abaixo do Loop do For, pois devem substituir as informacoes|
	//|que foram armazenadas no Loop acima                        |
	//�������������������������������������������������������������
	FAltRegAnt( 'C8B', '2' ) 
	
	//��������������������������������������������������Ŀ
	//�Neste momento eu preciso setar a operacao do model�
	//�como Inclusao                                     �
	//����������������������������������������������������
	oModel:DeActivate()
	oModel:SetOperation( 3 ) 	
	oModel:Activate() 
	
	//�������������������������������������������������������Ŀ
	//�Neste momento eu realizo a inclusao do novo registro ja�
	//�contemplando as informacoes alteradas pelo usuario     �
	//���������������������������������������������������������
	For nX := 1 To Len( aGrava )	
		oModel:LoadValue( 'MODEL_C8B', aGrava[ nX, 1 ], aGrava[ nX, 2 ] )
	Next        		
		
	For nX := 1 To Len( aGravaC9W )
		
		oModel:GetModel( 'MODEL_C9W' ):LVALID	:= .T.
		
		If nX > 1
			oModel:GetModel( 'MODEL_C9W' ):AddLine()
		EndIf
		oModel:LoadValue( "MODEL_C9W", "C9W_DTEXAM", 		aGravaC9W[nX][1] )
		oModel:LoadValue( "MODEL_C9W", "C9W_CODPRO", 		aGravaC9W[nX][2] )						
	Next 		
																				
	//�������������������������������Ŀ
	//�Busco a versao que sera gravada�
	//���������������������������������
	cVersao := xFunGetVer()
	
	/*---------------------------------------------------------
	ATENCAO -> A alteracao destes campos deve sempre estar     
	abaixo do Loop do For, pois devem substituir as informacoes
	que foram armazenadas no Loop acima                        
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_C8B", "C8B_VERSAO", cVersao )
	oModel:LoadValue( "MODEL_C8B", "C8B_VERANT", cVerAnt )
	oModel:LoadValue( "MODEL_C8B", "C8B_PROTPN", cProtocolo )
	
	
	/*---------------------------------------------------------
	Tratamento para que caso o Evento Anterior fosse de exclus�o
	seta-se o novo evento como uma "nova inclus�o", caso contr�rio o
	evento passar a ser uma altera��o
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_C8B", "C8B_EVENTO", "E" )
	oModel:LoadValue( "MODEL_C8B", "C8B_ATIVO", "1" )
		
	//Gravo altera��o para o Extempor�neo
	If lGoExtemp
		TafGrvExt( oModel, 'MODEL_C8B', 'C8B' )	
	EndIf
	
	FwFormCommit( oModel )
	TAFAltStat( 'C8B',"6" )
	
End Transaction

Return ( .T. )