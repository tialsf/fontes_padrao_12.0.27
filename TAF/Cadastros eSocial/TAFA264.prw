#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFA264.CH"

Static __lLay0205	:= TafLayESoc("02_05_00")

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFA264
Cadastro MVC de Condi��es Ambientais do Trabalho - Fatores de Risco (S-2240)

@author Mick William 
@since 15/03/2016
@version 1.0
/*/
//---------------------------------------------------------------------
Function TAFA264()

Private oBrw := FwMBrowse():New()

// Fun��o que indica se o ambiente � v�lido para o eSocial 2.3
If TafAtualizado() .And. TAF264Struct(.T.)
	oBrw:SetDescription( "Condi��es Ambientais do Trabalho - Fatores de Risco" ) //"Condi��es Ambientais do Trabalho - Fatores de Risco"
	oBrw:SetAlias( "CM9" )
	oBrw:SetMenuDef( "TAFA264" )
	oBrw:SetOnlyFields( { 'CM9_FILIAL', 'CM9_ID','CM9_FUNC','CM9_DTINI', 'CM9_PROTUL'} )
	
	If FindFunction('TAFSetFilter')
		oBrw:SetFilterDefault(TAFBrwSetFilter("CM9","TAFA264","S-2240"))
	Else
		oBrw:SetFilterDefault( "(CM9_ATIVO == '1')" ) //Filtro para que apenas os registros ativos sejam exibidos ( 1 = Ativo, 2 = Inativo )
	EndIf
	
	TafLegend(3,"CM9",@oBrw)
	oBrw:Activate()
EndIf

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} MenuDef

Funcao generica MVC com as opcoes de menu

@author Mick William
@since 15/03/2016
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao := {}
Local aRotina := {}

If FindFunction('TafXmlRet')
	aAdd( aFuncao, { "" , "TafxmlRet('TAF264Xml','2240','CM9')"	, "1" } )
Else 
	aAdd( aFuncao, { "" , "TAF264Xml"	, "1" } )
EndIf 
aAdd( aFuncao, { "" , "Taf264Vld" 	, "2" } )

//Chamo a Browse do Hist�rico
If FindFunction( "xNewHisAlt" ) 
	Aadd( aFuncao, { "" , "xNewHisAlt( 'CM9', 'TAFA264' ,,,,,,'2240','TAF264Xml')" , "3" } )
Else
	Aadd( aFuncao, { "" , "xFunHisAlt( 'CM9', 'TAFA264' ,,,, 'TAF264XML','2240' )" , "3" } )
EndIf

Aadd( aFuncao, { "" , "TAFXmlLote( 'CM9', 'S-2240' , 'evtExpRisco' , 'TAF264Xml',, oBrw )" , "5" } )
Aadd( aFuncao, { "" , "xFunAltRec( 'CM9' )" , "10" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If lMenuDif
	ADD OPTION aRotina Title STR0005 Action 'VIEWDEF.TAFA264' OPERATION 2 ACCESS 0 //"Visualizar"
	
	// Menu dos extempor�neos
	If FindFunction( "xNewHisAlt" ) .AND. FindFunction( "xTafExtmp" ) .And. xTafExtmp()
		aRotina	:= xMnuExtmp( "TAFA264", "CM9" )
	EndIf
	
Else
	aRotina := xFunMnuTAF( "TAFA264" , , aFuncao)
EndIf

Return( aRotina )

//---------------------------------------------------------------------
/*/{Protheus.doc} ModelDef

Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC
@author Mick William
@since 15/03/2016
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function ModelDef()

Local oStruCM9 := FwFormStruct( 1, "CM9" )
Local oStruT3S := FwFormStruct( 1, "T3S" )
Local oStruT0Q := FwFormStruct( 1, "T0Q" )
Local oStruCMA := FwFormStruct( 1, "CMA" )
Local oStruLEA := FwFormStruct( 1, "LEA" )
Local oStruCMB := FwFormStruct( 1, "CMB" )
Local oStruLEB := FwFormStruct( 1, "LEB" )
Local oStruV3E := FwFormStruct( 1, "V3E" )
Local oModel   := MpFormModel():New( "TAFA264", , , { |oModel| SaveModel( oModel ) } )
Local aTrigger := TAF264Trigger()


oStruV3E:AddTrigger( aTrigger[1],;
					 aTrigger[2],;
					 aTrigger[3],;
					 aTrigger[4] )

lVldModel := Iif( Type( "lVldModel" ) == "U", .F., lVldModel )

If lVldModel
	oStruCM9:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel } )
	oStruCMB:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel } )
EndIf

//Remo��o do GetSX8Num quando se tratar da Exclus�o de um Evento Transmitido.
//Necess�rio para n�o incrementar ID que n�o ser� utilizado.
If Upper( ProcName( 2 ) ) == Upper( "GerarExclusao" )
	oStruCM9:SetProperty( "CM9_ID", MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD, "" ) )
EndIf

oModel:AddFields( "MODEL_CM9", /*cOwner*/, oStruCM9 )

oModel:AddGrid("MODEL_T3S", "MODEL_CM9", oStruT3S)
oModel:GetModel( "MODEL_T3S" ):SetUniqueLine({"T3S_NISRES"})
oModel:GetModel( 'MODEL_T3S' ):SetMaxLine(9)

oModel:AddGrid("MODEL_V3E", "MODEL_CM9", oStruV3E)
oModel:GetModel( "MODEL_V3E" ):SetUniqueLine({"V3E_IDATV"})
oModel:GetModel( 'MODEL_V3E' ):SetMaxLine(99)

oModel:AddGrid("MODEL_T0Q", "MODEL_CM9", oStruT0Q)
oModel:GetModel( "MODEL_T0Q" ):SetUniqueLine( { "T0Q_CODAMB" } )
oModel:GetModel( "MODEL_T0Q" ):SetOptional( .T. )

oModel:AddGrid( "MODEL_CMA", "MODEL_CM9", oStruCMA )
oModel:GetModel( "MODEL_CMA" ):SetOptional( .T. )
oModel:GetModel( "MODEL_CMA" ):SetUniqueLine( { "CMA_CODFAT" } )
oModel:GetModel( 'MODEL_CMA' ):SetMaxLine(999)

oModel:AddGrid( "MODEL_LEA", "MODEL_CMA", oStruLEA )
oModel:GetModel( "MODEL_LEA" ):SetOptional( .T. )
oModel:GetModel( "MODEL_LEA" ):SetUniqueLine( { "LEA_UTZEPI","LEA_UTZEPC", "LEA_EFIEPC" } )
oModel:GetModel( 'MODEL_LEA' ):SetMaxLine(1)

oModel:AddGrid(  "MODEL_CMB", "MODEL_LEA", oStruCMB )
oModel:GetModel( "MODEL_CMB" ):SetOptional( .T. )
oModel:GetModel( "MODEL_CMB" ):SetUniqueLine( { "CMB_CAEPI", "CMB_IDDESC" } )
oModel:GetModel( 'MODEL_CMB' ):SetMaxLine(50)

oModel:AddGrid( "MODEL_LEB", "MODEL_LEA", oStruLEB)
oModel:GetModel( "MODEL_LEB" ):SetOptional( .T. )
oModel:GetModel( "MODEL_LEB" ):SetUniqueLine( { "LEB_DESEPC" } )
oModel:GetModel( 'MODEL_LEB' ):SetMaxLine(50)

oModel:SetRelation( "MODEL_T3S" , { { "T3S_FILIAL", "xFilial('T3S')" }, { "T3S_ID", "CM9_ID" }, { "T3S_VERSAO", "CM9_VERSAO" } }, T3S->(IndexKey(1) ) )
oModel:SetRelation( "MODEL_V3E" , { { "V3E_FILIAL", "xFilial('V3E')" }, { "V3E_ID", "CM9_ID" }, { "V3E_VERSAO", "CM9_VERSAO" } }, V3E->(IndexKey(1) ) )
oModel:SetRelation( "MODEL_T0Q" , { { "T0Q_FILIAL", "xFilial('T0Q')" }, { "T0Q_ID", "CM9_ID" }, { "T0Q_VERSAO", "CM9_VERSAO" } }, T0Q->(IndexKey(1) ) )
oModel:SetRelation( "MODEL_CMA" , { { "CMA_FILIAL", "xFilial('CMA')" }, { "CMA_ID", "CM9_ID" }, { "CMA_VERSAO", "CM9_VERSAO" } }, CMA->( IndexKey(1) ) )
oModel:SetRelation( "MODEL_LEA" , { { "LEA_FILIAL", "xFilial('LEA')" }, { "LEA_ID", "CM9_ID" }, { "LEA_VERSAO", "CM9_VERSAO" }, { "LEA_CODFAT", "CMA_CODFAT" }}, LEA->( IndexKey( 1 ) ) )
oModel:SetRelation( "MODEL_CMB" , { { "CMB_FILIAL", "xFilial('CMB')" }, { "CMB_ID", "CM9_ID" }, { "CMB_VERSAO", "CM9_VERSAO" }, { "CMB_CODFAT", "CMA_CODFAT" }, { "CMB_UTZEPI", "LEA_UTZEPI" }}, CMB->( IndexKey( 1 ) ) )
oModel:SetRelation( "MODEL_LEB" , { { "LEB_FILIAL", "xFilial('LEB')" }, { "LEB_ID", "CM9_ID" }, { "LEB_VERSAO", "CM9_VERSAO" }, { "LEB_CODFAT", "CMA_CODFAT" }, { "LEB_UTZEPC", "LEA_UTZEPC" }}, LEB->( IndexKey( 1 ) ) )

oModel:GetModel( "MODEL_CM9" ):SetPrimaryKey( { "CM9_FUNC" } )

If Type("cOperEvnt") <> "U"
	
	If cOperEvnt == '1' .AND. CM9->CM9_STATUS <> '4' .AND. CM9->CM9_EVENTO == 'I'
		//oStruCM9:SetProperty( 'CM9_DTALT' , MODEL_FIELD_WHEN,{|| .F. })	
		//oStruCM9:SetProperty( 'CM9_DTFIM' , MODEL_FIELD_WHEN,{|| .F. })
		
	ElseIf cOperEvnt == '2'
		oStruCM9:SetProperty( 'CM9_DTINI' , MODEL_FIELD_WHEN,{|| .F. })
		//oStruCM9:SetProperty( 'CM9_DTFIM' , MODEL_FIELD_WHEN,{|| .F. })
	
	ElseIf cOperEvnt == '3'	
		
		oStruCM9:SetProperty( 'CM9_DTINI' , MODEL_FIELD_WHEN,{|| .F. })
		//oStruCM9:SetProperty( 'CM9_DTALT' , MODEL_FIELD_WHEN,{|| .F. })
		
		oStruT0Q:SetProperty( 'T0Q_CODAMB' , MODEL_FIELD_WHEN,{|| .F. })
		
		oStruCMA:SetProperty( 'CMA_CODFAT' , MODEL_FIELD_WHEN,{|| .F. })
		oStruCMA:SetProperty( 'CMA_INTCON' , MODEL_FIELD_WHEN,{|| .F. })
		oStruCMA:SetProperty( 'CMA_TECMED' , MODEL_FIELD_WHEN,{|| .F. })
	
		oStruCMB:SetProperty( 'CMB_CAEPI' , MODEL_FIELD_WHEN,{|| .F. })
		oStruCMB:SetProperty( 'CMB_EFIEPI' , MODEL_FIELD_WHEN,{|| .F. })
		oStruCMB:SetProperty( 'CMB_MEDPRT' , MODEL_FIELD_WHEN,{|| .F. })
		oStruCMB:SetProperty( 'CMB_CNDFUN' , MODEL_FIELD_WHEN,{|| .F. })
		oStruCMB:SetProperty( 'CMB_PRZVLD' , MODEL_FIELD_WHEN,{|| .F. })
		oStruCMB:SetProperty( 'CMB_PERTRC' , MODEL_FIELD_WHEN,{|| .F. })
		oStruCMB:SetProperty( 'CMB_HIGIEN' , MODEL_FIELD_WHEN,{|| .F. })
		
		oStruLEA:SetProperty( 'LEA_UTZEPI' , MODEL_FIELD_WHEN,{|| .F. })
		oStruLEA:SetProperty( 'LEA_UTZEPC' , MODEL_FIELD_WHEN,{|| .F. })
		
		oStruLEB:SetProperty( 'LEB_DESEPC' , MODEL_FIELD_WHEN,{|| .F. })
		oStruLEB:SetProperty( 'LEB_EFIEPC' , MODEL_FIELD_WHEN,{|| .F. })	
		
	EndIf
	
	oStruCM9:SetProperty( 'CM9_FUNC'  , MODEL_FIELD_WHEN,{|| .F. })
Else
	//Em caso de inclus�o eu bloqueio os campos de altera��o e finaliza��o	
	//oStruCM9:SetProperty( 'CM9_DTALT' , MODEL_FIELD_WHEN,{|| .F. })	
	//oStruCM9:SetProperty( 'CM9_DTFIM' , MODEL_FIELD_WHEN,{|| .F. })
EndIf

Return( oModel )

//---------------------------------------------------------------------
/*/{Protheus.doc} ViewDef

Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Mick William
@since 15/03/2016
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function ViewDef()

Local oModel    := FWLoadModel("TAFA264")
Local oStruCM9	:= Nil
Local oStruCM9p	:= Nil
Local oStruT3S	:= Nil 
Local oStruT0Q	:= Nil
Local oStruCMA	:= Nil
Local oStruLEA  := Nil
Local oStruCMB	:= Nil
Local oStruLEB	:= Nil
Local oStruV3E	:= Nil
Local cCmpFil   := ""

oView		:= FWFormView():New()

oView:SetModel( oModel )
oView:SetContinuousForm()


oStruT3S := FWFormStruct(2,"T3S")

cCmpFil  	:= "CM9_ID|CM9_VERSAO|CM9_FUNC|CM9_DFUNC|CM9_DTINI|CM9_DTALT|CM9_DTFIM|CM9_STATUS|CM9_EVENTO|CM9_ATIVO|CM9_DATIVD|CM9_OBSCMP|CM9_METERG|"

oStruCM9 	:= FwFormStruct( 2, "CM9", { |x| AllTrim( x ) + "|" $ cCmpFil } ) //Campos de Inclus�o

cCmpFil  	:= "CM9_PROTUL|"
oStruCM9p 	:= FwFormStruct( 2, 'CM9', {|x| AllTrim( x ) + "|" $ cCmpFil } ) //Protocolo de Transmiss�o

cCmpFil		:="T0Q_CODAMB|T0Q_DCODAM|"	
oStruT0Q	:= FwFormStruct( 2, "T0Q", { |x| AllTrim( x ) + "|" $ cCmpFil } ) //Campos da Aba 'Informa��es do ambiente do trabalho'

If TafColumnPos("CMA_DSCFAT") .And. __lLay0205
    cCmpFil  	:= "CMA_CODFAT|CMA_DCODFA|CMA_DSCFAT|CMA_INTCON|CMA_TECMED|CMA_UTLEPI|CMA_LIMTOL|CMA_UNMED|CMA_INSALU|CMA_PERICU|CMA_APOESP|CMA_TPAVAL|"
Else
    cCmpFil  	:= "CMA_CODFAT|CMA_DCODFA|CMA_INTCON|CMA_TECMED|CMA_UTLEPI|CMA_LIMTOL|CMA_UNMED|CMA_INSALU|CMA_PERICU|CMA_APOESP|CMA_TPAVAL|"
EndIf

oStruCMA 	:= FwFormStruct( 2, "CMA", { |x| AllTrim( x ) + "|" $ cCmpFil } ) //Campos do Aba Fatores de Riscos

cCmpFil 	:= "LEA_UTZEPC|LEA_UTZEPI|LEA_EFIEPC|"

oStruLEA    := FwFormStruct( 2, "LEA", { |x| AllTrim( x ) + "|" $ cCmpFil } ) //Informa��es relativas ao IPI

cCmpFil  	:= "CMB_CAEPI|CMB_EFIEPI|CMB_MEDPRT|CMB_CNDFUN|CMB_PRZVLD|CMB_PERTRC|CMB_HIGIEN|CMB_USOINI|CMB_IDDESC|"

oStruCMB 	:= FwFormStruct( 2, "CMB", { |x| AllTrim( x ) + "|" $ cCmpFil } ) //Campos do Aba Epis

oStruV3E    := FwFormStruct( 2, "V3E" ) //Informa��es relativas ao IPI

If FindFunction("TafAjustRecibo")
	TafAjustRecibo(oStruCM9p,"CM9")
EndIf

oView:AddField( "VIEW_CM9"	, oStruCM9	, "MODEL_CM9" )//"Condi��es Ambientais do Trabalho - Fatores de Risco"
oView:AddField( 'VIEW_CM9p' , oStruCM9p	, 'MODEL_CM9' )//'Protocolo de �ltima Transmiss�o'

If FindFunction("TafNmFolder")
	oView:EnableTitleView( 'VIEW_CM9p', TafNmFolder("recibo",1) ) // "Recibo da �ltima Transmiss�o"  
EndIf 

If __lLay0205
	oStruT3S:RemoveField('T3S_DTINI')
	oStruT3S:RemoveField('T3S_DTFIM')
EndIf

oView:AddGrid( 'VIEW_T3S'	, oStruT3S 	, 'MODEL_T3S' )//'Informa��es relativas ao respons�vel pelos registros ambientais'
oView:AddGrid( 'VIEW_T0Q'	, oStruT0Q 	, 'MODEL_T0Q' )//'Informa��es do ambiente do trabalho'
oView:AddGrid( 'VIEW_CMA'	, oStruCMA 	, 'MODEL_CMA' )//'Fatores de Riscos'
oView:AddGrid( 'VIEW_LEA'  	, oStruLEA	, 'MODEL_LEA' )//'Informa��es relativas ao EPI/EPC'
oView:AddGrid( 'VIEW_CMB'  	, oStruCMB	, 'MODEL_CMB' )//'EPI(s)'
oView:AddGrid( 'VIEW_V3E' , oStruV3E 	, 'MODEL_V3E' )//'Informa��o das atividades perigosas, insalubres ou especiais desempenhadas'


//Painel Superior
oView:CreateHorizontalBox( 'PAINEL_SUPERIOR', 100 )

oView:CreateFolder( 'FOLDER_SUPERIOR', 'PAINEL_SUPERIOR' )

oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA01', STR0014) //"Informa��es sobre o ambiente de trabalho e exposi��o a fatores de risco"
oView:CreateHorizontalBox( 'PAINEL_01', 25,,, 'FOLDER_SUPERIOR', 'ABA01' )

If FindFunction("TafNmFolder")
	oView:AddSheet( 'FOLDER_SUPERIOR', "ABA02", TafNmFolder("recibo") )   //"Numero do Recibo"
Else
	oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA02', STR0015 ) //"Protocolo"
EndIf 

oView:CreateHorizontalBox( 'PAINEL_02', 100,,, 'FOLDER_SUPERIOR', 'ABA02' )

//Painel Superior/Painel Inferior

oView:CreateHorizontalBox( 'PAINEL_INFERIOR', 75,,, 'FOLDER_SUPERIOR', 'ABA01' ) 

oView:CreateFolder( 'FOLDER_INFERIOR', 'PAINEL_INFERIOR' )

oView:AddSheet( 'FOLDER_INFERIOR', 'ABA01', STR0016) //'Informa��es do ambiente do trabalho'
oView:CreateHorizontalBox( 'GRIDC_01', 25,,, 'FOLDER_INFERIOR', 'ABA01' )

oView:AddSheet( 'FOLDER_INFERIOR', 'ABA04', "Fator de Risco") //"Fator de Risco"

oView:AddSheet( 'FOLDER_INFERIOR', 'ABA02', STR0017) //'Informa��es relativas ao respons�vel pelos registros ambientais'
oView:CreateHorizontalBox( 'GRIDR_01', 100,,, 'FOLDER_INFERIOR', 'ABA02' )

oView:AddSheet( 'FOLDER_INFERIOR', 'ABA03', STR0022) //'Infos das atividades perigosas, insalubres, ou especiais desempenhadas'
oView:CreateHorizontalBox( 'GRIDG_01', 100,,, 'FOLDER_INFERIOR', 'ABA03' )

//Fator de Risco					
oView:EnableTitleView('VIEW_CMA',STR0018)						
oView:CreateHorizontalBox('GRIDC_02', 30,,, 'FOLDER_INFERIOR', 'ABA04' )	

//Informa��es relativas ao EPI/EPC
oView:EnableTitleView('VIEW_LEA',STR0019)
oView:CreateHorizontalBox('GRIDC_03', 35,,, 'FOLDER_INFERIOR', 'ABA04' )	

//Folder obrigat�ria para o sheet
oView:EnableTitleView('VIEW_CMB',STR0011)
oView:CreateHorizontalBox('GRIDC_04', 35,,, 'FOLDER_INFERIOR', 'ABA04' )

oView:SetOwnerView( 'VIEW_CM9' 	, 'PAINEL_01')
oView:SetOwnerView( 'VIEW_CM9p'	, 'PAINEL_02')
oView:SetOwnerView( 'VIEW_T3S' 	, 'GRIDR_01' )
oView:SetOwnerView( 'VIEW_T0Q' 	, 'GRIDC_01' )
oView:SetOwnerView( 'VIEW_CMA' 	, 'GRIDC_02' )
oView:SetOwnerView( 'VIEW_LEA' 	, 'GRIDC_03' )
oView:SetOwnerView( 'VIEW_CMB' 	, 'GRIDC_04' )
oView:SetOwnerView( 'VIEW_V3E'	, 'GRIDG_01' )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif
	xFunRmFStr( @oStruCM9, "CM9" )
	xFunRmFStr( @oStruT0Q, "T0Q" )
	xFunRmFStr( @oStruCMA, "CMA" )
	xFunRmFStr( @oStruCMB, "CMB" ) 	
EndIf

Return( oView )
//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no fin1al, no momento da
confirmacao do modelo

@param  oModel -> Modelo de dados
@return .T.

@author Mick William
@since 15/03/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel( oModel )

Local cVerAnt   	:= ""
Local cProtocolo	:= ""
Local cVersao  		:= ""
Local cChvRegAnt	:= ""
Local cEvento   	:= ""
Local cLogOpeAnt	:= ""


Local nOperation	:= oModel:GetOperation()
Local nI        	:= 0
Local nJ        	:= 0
Local nQ		 	:= 0
Local nIgAdd	 	:= 0
Local nI5Add	 	:= 0
Local nlI			:= 0
Local nlY   		:= 0
Local nT0Q			:= 0
Local nT3S			:= 0
Local nCMA			:= 0
Local nCMAAdd 		:= 0
Local nLEA			:= 0
Local nLEAAdd 		:= 0
Local nCMB			:= 0
Local nCMBAdd 		:= 0
Local nLEB			:= 0
Local nLEBAdd 		:= 0
Local nV3E			:= 0

Local aGravaCM9		:= {}
Local aGravaT0Q		:= {}
Local aGravaCMA		:= {}
Local aGravaCMB 	:= {}
Local aGravaLEA		:= {}
Local aGravaLEB		:= {}
Local aGravaT3S		:= {}
Local aGravaV3E		:= {}

Local oModelCM9 	:= Nil
Local oModelT0Q 	:= Nil
Local oModelCMA 	:= Nil
Local oModelLEA 	:= Nil 
Local oModelCMB 	:= Nil
Local oModelLEB 	:= Nil 
Local oModelT3S		:= Nil
Local oModelV3E		:= Nil

Local lRetorno	:= .T.

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

Begin Transaction

	If nOperation == MODEL_OPERATION_INSERT

	TafAjustID( "CM9", oModel)

		oModel:LoadValue( "MODEL_CM9", "CM9_VERSAO", xFunGetVer() )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_CM9', 'CM9_LOGOPE' , '2', '' )
		Endif

		FwFormCommit( oModel )

	ElseIf nOperation == MODEL_OPERATION_UPDATE

		//�����������������������������������������������������������������Ŀ
		//�Seek para posicionar no registro antes de realizar as validacoes,�
		//�visto que quando nao esta pocisionado nao eh possivel analisar   �
		//�os campos nao usados como _STATUS                                �
		//�������������������������������������������������������������������
		CM9->( DBSetOrder( 3 ) )
		If lGoExtemp .OR. CM9->( MsSeek( xFilial( "CM9" ) + M->CM9_ID + "1" ) )

			//��������������������������������Ŀ
			//�Se o registro ja foi transmitido�
			//����������������������������������
			If CM9->CM9_STATUS == "4"

				oModelCM9 := oModel:GetModel( "MODEL_CM9" )
				oModelT0Q := oModel:GetModel( "MODEL_T0Q" )
				oModelCMA := oModel:GetModel( "MODEL_CMA" )
				oModelLEA := oModel:GetModel( "MODEL_LEA" )
				oModelCMB := oModel:GetModel( "MODEL_CMB" )
				oModelLEB := oModel:GetModel( "MODEL_LEB" )
				oModelT3S := oModel:GetModel( "MODEL_T3S" )	
				oModelV3E := oModel:GetModel( "MODEL_V3E" )	
				
				//�����������������������������������������������������������Ŀ
				//�Busco a versao anterior do registro para gravacao do rastro�
				//�������������������������������������������������������������
				cVerAnt    := oModelCM9:GetValue( "CM9_VERSAO" )
				cProtocolo := oModelCM9:GetValue( "CM9_PROTUL" )
				
				If TafColumnPos( "CM9_LOGOPE" )
					cLogOpeAnt := oModelCM9:GetValue( "CM9_LOGOPE" )	
				endif
				
				//�����������������������������������������������������������������Ŀ
				//�Neste momento eu gravo as informacoes que foram carregadas       �
				//�na tela, pois neste momento o usuario ja fez as modificacoes que �
				//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
				//�nao devem ser consideradas neste momento                         �
				//�������������������������������������������������������������������
				//�����������������������������������������������������������������Ŀ
				//�Neste momento eu gravo as informacoes que foram carregadas       �
				//�na tela, pois neste momento o usuario ja fez as modificacoes que �
				//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
				//�nao devem ser consideradas neste momento                         �
				//�������������������������������������������������������������������
				For nI := 1 to Len( oModelCM9:aDataModel[ 1 ] )
					aAdd( aGravaCM9, { oModelCM9:aDataModel[ 1, nI, 1 ], oModelCM9:aDataModel[ 1, nI, 2 ] } )
				Next
				
				//T3S				  
				For nT3S := 1 To oModel:GetModel( 'MODEL_T3S' ):Length() 
					oModel:GetModel( 'MODEL_T3S' ):GoLine(nT3S)
					
					If !oModel:GetModel( 'MODEL_T3S' ):IsDeleted()
						aAdd( aGravaT3S, {  oModelT3S:GetValue( "T3S_NISRES"	),;
											oModelT3S:GetValue( "T3S_NROC"  	),;
											oModelT3S:GetValue( "T3S_UFOC"  	),;
											oModelT3S:GetValue( "T3S_CPFRES"  ),;
											oModelT3S:GetValue( "T3S_NMRES"  	),;
											oModelT3S:GetValue( "T3S_IDEOC"  	),;
											oModelT3S:GetValue( "T3S_DSCOC"  	)})
					EndIf										
				Next nT3S //Fim - T3S

				//V3E
				For nV3E := 1 To oModelV3E:Length() 
					oModelV3E:GoLine(nV3E)
					
					If !oModelV3E:IsDeleted()
						aAdd( aGravaV3E, {  oModelV3E:GetValue( "V3E_IDATV" ) } )
					EndIf
				Next nV3E //Fim - nV3E

				For nCMA := 1 To oModel:GetModel( 'MODEL_CMA' ):Length() 
					oModel:GetModel( 'MODEL_CMA' ):GoLine(nCMA)
							
					If !oModel:GetModel( 'MODEL_CMA' ):IsDeleted()
                            If TafColumnPos("CMA_DSCFAT") .And. __lLay0205
                                aAdd( aGravaCMA, { oModelCMA:GetValue( "CMA_CODFAT" ),;                                                    
                                                    oModelCMA:GetValue( "CMA_INTCON" ),;
                                                    oModelCMA:GetValue( "CMA_TECMED" ),;
                                                    oModelCMA:GetValue( "CMA_LIMTOL" ),;
                                                    oModelCMA:GetValue( "CMA_UNMED"  ),;
                                                    oModelCMA:GetValue( "CMA_INSALU" ),;
                                                    oModelCMA:GetValue( "CMA_PERICU" ),;
                                                    oModelCMA:GetValue( "CMA_APOESP" ),;
                                                    oModelCMA:GetValue( "CMA_TPAVAL" ),;
                                                    oModelCMA:GetValue( "CMA_DSCFAT" )} )
                            Else
                                aAdd( aGravaCMA, { oModelCMA:GetValue( "CMA_CODFAT" ),;                                                    
                                                    oModelCMA:GetValue( "CMA_INTCON" ),;
                                                    oModelCMA:GetValue( "CMA_TECMED" ),;
                                                    oModelCMA:GetValue( "CMA_LIMTOL" ),;
                                                    oModelCMA:GetValue( "CMA_UNMED"  ),;
                                                    oModelCMA:GetValue( "CMA_INSALU" ),;
                                                    oModelCMA:GetValue( "CMA_PERICU" ),;
                                                    oModelCMA:GetValue( "CMA_APOESP" ),;
                                                    oModelCMA:GetValue( "CMA_TPAVAL" )} )
                            EndIf
			
						For nLEA := 1 To oModel:GetModel( 'MODEL_LEA' ):Length() 
							oModel:GetModel( 'MODEL_LEA' ):GoLine(nLEA)
									
							If !oModel:GetModel( 'MODEL_LEA' ):IsDeleted()
							 	aAdd( aGravaLEA, { oModelCMA:GetValue( "CMA_CODFAT" ),;
													 oModelLEA:GetValue( "LEA_UTZEPC" ),;
													 oModelLEA:GetValue( "LEA_UTZEPI" ),;
													 oModelLEA:GetValue( "LEA_EFIEPC" )} )
										
								//CMB					  
								For nCMB := 1 To oModel:GetModel( 'MODEL_CMB' ):Length() 
									oModel:GetModel( 'MODEL_CMB' ):GoLine(nCMB)
											
									If !oModel:GetModel( 'MODEL_CMB' ):IsDeleted()
										aAdd (aGravaCMB ,{	  oModelCMA:GetValue( "CMA_CODFAT"),;
																oModelLEA:GetValue( "LEA_UTZEPC"),;
																oModelCMB:GetValue( "CMB_CAEPI" ),;
																oModelCMB:GetValue( "CMB_EFIEPI"),;
																oModelCMB:GetValue( "CMB_MEDPRT"),;
																oModelCMB:GetValue( "CMB_CNDFUN"),;
																oModelCMB:GetValue( "CMB_PRZVLD"),;
																oModelCMB:GetValue( "CMB_PERTRC"),;
																oModelCMB:GetValue( "CMB_HIGIEN"),;
																oModelCMB:GetValue( "CMB_IDDESC"),;
																oModelCMB:GetValue( "CMB_USOINI")})
									EndIf										
								Next nCMB //Fim - CMB										 
							EndIf
						Next nLEA //Fim - LEA
					EndIf
				Next nCMA //Fim - CMA
							
				
				For nT0Q := 1 To oModel:GetModel( 'MODEL_T0Q' ):Length()
					oModel:GetModel( 'MODEL_T0Q' ):Goline(nT0Q) 
		
					If !oModel:GetModel( 'MODEL_T0Q' ):IsDeleted()								
						aAdd( aGravaT0Q, {  oModelT0Q:GetValue( "T0Q_CODAMB" ) } )
			
					EndIf
				Next nT0Q //Fim - T0Q	
				
				//�����������������������������������������������������������Ŀ
				//�Seto o campo como Inativo e gravo a versao do novo registro�
				//�no registro anterior                                       �
				//|                                                           |
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������
				FAltRegAnt( "CM9", "2" )	

				//��������������������������������������������������Ŀ
				//�Neste momento eu preciso setar a operacao do model�
				//�como Inclusao                                     �
				//����������������������������������������������������
				oModel:DeActivate()
				oModel:SetOperation( 3 )
				oModel:Activate()

				//�������������������������������������������������������Ŀ
				//�Neste momento eu realizo a inclusao do novo registro ja�
				//�contemplando as informacoes alteradas pelo usuario     �
				//���������������������������������������������������������
				For nlI := 1 To Len( aGravaCM9 )	
					oModel:LoadValue( 'MODEL_CM9', aGravaCM9[ nlI, 1 ], aGravaCM9[ nlI, 2 ] )
				Next  

				//Necess�rio Abaixo do For Nao Retirar
				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_CM9', 'CM9_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				For nT3S := 1 To Len( aGravaT3S )
					oModel:GetModel( 'MODEL_T3S' ):LVALID	:= .T.
		
					If nT3S > 1
						oModel:GetModel( 'MODEL_T3S' ):AddLine()
					EndIf				
				
					oModel:LoadValue( "MODEL_T3S", "T3S_NISRES"	, aGravaT3S[nT3S][1] ) 
					oModel:LoadValue( "MODEL_T3S", "T3S_NROC"  	, aGravaT3S[nT3S][2] ) 
					oModel:LoadValue( "MODEL_T3S", "T3S_UFOC"  	, aGravaT3S[nT3S][3] )
					oModel:LoadValue( "MODEL_T3S", "T3S_CPFRES"	, aGravaT3S[nT3S][4] )
					oModel:LoadValue( "MODEL_T3S", "T3S_NMRES"  , aGravaT3S[nT3S][5] )
					oModel:LoadValue( "MODEL_T3S", "T3S_IDEOC"  , aGravaT3S[nT3S][6] )
					oModel:LoadValue( "MODEL_T3S", "T3S_DSCOC"  , aGravaT3S[nT3S][7] )
							 
				Next nT3S //Fim - T3S

				For nV3E := 1 To Len( aGravaV3E )
					oModel:GetModel( 'MODEL_V3E' ):LVALID	:= .T.
		
					If nV3E > 1
						oModel:GetModel( 'MODEL_V3E' ):AddLine()
					EndIf
					
					oModel:LoadValue( "MODEL_V3E", "V3E_IDATV" , aGravaV3E[nV3E][1] )
							 
				Next nV3E //Fim - V3E
				
				
				For nT0Q := 1 To Len( aGravaT0Q )
					oModel:GetModel( 'MODEL_T0Q' ):LVALID	:= .T.
		
					If nT0Q > 1
						oModel:GetModel( 'MODEL_T0Q' ):AddLine()
					EndIf
					oModel:LoadValue( "MODEL_T0Q", "T0Q_CODAMB", aGravaT0Q[nT0Q][1] )
		
				Next nT0Q //Fim - T0Q

				nCMAAdd := 1
				For nCMA := 1 To Len( aGravaCMA )
					oModel:GetModel( 'MODEL_CMA' ):LVALID	:= .T.
		
					If nCMAAdd > 1
						oModel:GetModel( 'MODEL_CMA' ):AddLine()
					EndIf
					
					oModel:LoadValue( "MODEL_CMA", "CMA_CODFAT", aGravaCMA[nCMA][1] )

                    If TafColumnPos("CMA_DSCFAT") .And. __lLay0205
                        oModel:LoadValue( "MODEL_CMA", "CMA_DSCFAT", aGravaCMA[nCMA][10] )
                    EndIf

					oModel:LoadValue( "MODEL_CMA", "CMA_INTCON", aGravaCMA[nCMA][2] )
					oModel:LoadValue( "MODEL_CMA", "CMA_TECMED", aGravaCMA[nCMA][3] )
					oModel:LoadValue( "MODEL_CMA", "CMA_LIMTOL", aGravaCMA[nCMA][4] )
					oModel:LoadValue( "MODEL_CMA", "CMA_UNMED" , aGravaCMA[nCMA][5] )
					oModel:LoadValue( "MODEL_CMA", "CMA_INSALU", aGravaCMA[nCMA][6] )
					oModel:LoadValue( "MODEL_CMA", "CMA_PERICU", aGravaCMA[nCMA][7] )
					oModel:LoadValue( "MODEL_CMA", "CMA_APOESP", aGravaCMA[nCMA][8] )
					oModel:LoadValue( "MODEL_CMA", "CMA_TPAVAL", aGravaCMA[nCMA][9] )


					nLEAAdd := 1
					For nLEA := 1 To Len( aGravaLEA )
						If aGravaLEA[nLEA][1] == aGravaCMA[nCMA][1]
							oModel:GetModel( 'MODEL_LEA' ):LVALID	:= .T.
			
							If nLEAAdd > 1
								oModel:GetModel( 'MODEL_LEA' ):AddLine()
							EndIf
									
								oModel:LoadValue( "MODEL_LEA", "LEA_UTZEPC", aGravaLEA[nLEA][2] )
								oModel:LoadValue( "MODEL_LEA", "LEA_UTZEPI", aGravaLEA[nLEA][3] )
								oModel:LoadValue( "MODEL_LEA", "LEA_EFIEPC", aGravaLEA[nLEA][4] )
		
								nCMBAdd := 1
								For nCMB := 1 To Len( aGravaCMB )
									If aGravaCMB[nCMB][1] == aGravaCMA[nCMA][1] .And. aGravaCMB[nCMB][2] == aGravaLEA[nLEA][2]
										oModel:GetModel( 'MODEL_CMB' ):LVALID	:= .T.
					
										If nCMBAdd > 1
											oModel:GetModel( 'MODEL_CMB' ):AddLine()
										EndIf
											oModel:LoadValue( "MODEL_CMB", "CMB_CAEPI",  aGravaCMB[nCMB][3] )
											oModel:LoadValue( "MODEL_CMB", "CMB_EFIEPI", aGravaCMB[nCMB][4] )
											oModel:LoadValue( "MODEL_CMB", "CMB_MEDPRT", aGravaCMB[nCMB][5] )
											oModel:LoadValue( "MODEL_CMB", "CMB_CNDFUN", aGravaCMB[nCMB][6] )
											oModel:LoadValue( "MODEL_CMB", "CMB_PRZVLD", aGravaCMB[nCMB][7] )
											oModel:LoadValue( "MODEL_CMB", "CMB_PERTRC", aGravaCMB[nCMB][8] )
											oModel:LoadValue( "MODEL_CMB", "CMB_HIGIEN", aGravaCMB[nCMB][9] )
											oModel:LoadValue( "MODEL_CMB", "CMB_IDDESC", aGravaCMB[nCMB][10])
											oModel:LoadValue( "MODEL_CMB", "CMB_USOINI", aGravaCMB[nCMB][11])
				
										nCMBAdd++
									EndIf
											Next nCMB //Fim - CMB
							nLEAAdd++
						EndIf
						Next nLEA //Fim - LEA
		
					nCMAAdd++
				
				Next nCMA //Fim - CMA				
				
				//�������������������������������Ŀ
				//�Busco a versao que sera gravada�
				//���������������������������������
				cVersao := xFunGetVer()
				
				//�����������������������������������������������������������Ŀ
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������
				oModel:LoadValue( "MODEL_CM9" , "CM9_VERSAO", cVersao    )
				oModel:LoadValue( "MODEL_CM9" , "CM9_VERANT", cVerAnt    )
				oModel:LoadValue( "MODEL_CM9" , "CM9_PROTPN", cProtocolo )
				oModel:LoadValue( "MODEL_CM9" , "CM9_PROTUL", ""         )
				oModel:LoadValue( 'MODEL_CM9' , 'CM9_EVENTO', "A" 		 )
				
				// Tratamento para limpar o ID unico do xml
				cAliasPai := "CM9"
				If TAFColumnPos( cAliasPai+"_XMLID" )
					oModel:LoadValue( 'MODEL_'+cAliasPai, cAliasPai+'_XMLID', "" )
				EndIf

			ElseIf CM9->CM9_STATUS == ( "2" )
				TAFMsgVldOp( oModel, "2" )//"Registro n�o pode ser alterado. Aguardando processo da transmiss�o."
				lRetorno := .F.
			Elseif CM9->CM9_STATUS == ( "6" )
				TAFMsgVldOp( oModel, "6" )//"Registro n�o pode ser alterado. Aguardando processo de transmiss�o do evento de Exclus�o S-3000"
				lRetorno := .F.
			ElseIf CM9->CM9_STATUS == "7"
				TAFMsgVldOp( oModel, "7" )//"Registro n�o pode ser alterado, pois o evento de exclus�o j� se encontra na base do RET"
				lRetorno := .F.
			Else
				If TafColumnPos( "CM9_LOGOPE" )
					cLogOpeAnt := CM9->CM9_LOGOPE
				endif

				TAFAltStat( 'CM9', " " )

				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_CM9', 'CM9_LOGOPE' , '' , cLogOpeAnt )
				EndIf
			Endif
			
			If lRetorno
				
				//Gravo altera��o para o Extempor�neo
				If lGoExtemp
					TafGrvExt( oModel, 'MODEL_CM9', 'CM9' )			
				Endif	
				
				FwFormCommit( oModel )
			EndIf			
		
		EndIf 
	ElseIf nOperation == MODEL_OPERATION_DELETE 
		cChvRegAnt := CM9->(CM9_ID + CM9_VERANT)              
											
		If !Empty( cChvRegAnt ) 
			TAFAltStat( 'CM9', " " )
			FwFormCommit( oModel )				
			If nOperation == MODEL_OPERATION_DELETE
				If CM9->CM9_EVENTO == "A" .Or. CM9->CM9_EVENTO == "E"
					TAFRastro( 'CM9', 1, cChvRegAnt, .T. , , IIF(Type("oBrw") == "U", Nil, oBrw) )
				EndIf
			EndIf
		Else
			oModel:DeActivate()
			oModel:SetOperation( 5 ) 	
			oModel:Activate()
			FwFormCommit( oModel )
		EndIf
	
	EndIf  

End Transaction

Return ( lRetorno )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF264Xml

Funcao de geracao do XML para aten der o registro S-2240/S-2365
Quando a rotina for chamada o registro deve estar posicionado

@Param:
lJob - Informa se foi chamado por Job
lRemEmp - Exclusivo do Evento S-1000
cSeqXml - Numero sequencial para composi��o da chave ID do XML

@Return:
cXml - Estrutura do Xml do Layout S-2240/S-2365
lRemEmp - Exclusivo do Evento S-1000
cSeqXml - Numero sequencial para composi��o da chave ID do XML

@author Mick William
@since 15/03/2016
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAF264Xml(cAlias,nRecno,nOpc,lJob,lRemEmp,cSeqXml)

Local cXml		:= ""
Local cCodCateg	:= ""
Local cAlias	:= GetNextAlias()
Local nTamAmb	:= TamSX3("T0Q_CODAMB")[1]
Local cLayout 	:= "2240"
Local lXmlVLd	:= IIF(FindFunction('TafXmlVLD'),TafXmlVLD('TAF264XML'),.T.)

Default lJob    := .F.
Default cSeqXml := ""

If lXmlVLd
	dbSelectArea("CMA")
	CMA-> (DbSetOrder(1))

	dbSelectArea("LEA")
	LEA->(DbSetOrder(1))

	dbSelectArea("LEB")
	LEB->( DBSetOrder( 1 ) )
							
	dbSelectArea("CMB")
	CMB->( DBSetOrder( 1 ) )

	dbSelectArea("V3E")
	V3E->( dbSetOrder(1) )


	C9V->( DBSetOrder( 2 ) )

	If C9V->( MsSeek( xFilial("C9V") + CM9->CM9_FUNC + "1" ) )
		cXml := "<ideVinculo>"
		cXml += 	xTafTag( "cpfTrab"	, C9V->C9V_CPF )
		cXml += 	xTafTag( "nisTrab"	, C9V->C9V_NIS,,.T. )
		cXml += 	xTafTag( "matricula", C9V->C9V_MATRIC,,.T. )

		If C9V->C9V_NOMEVE == "S2300"
			cCodCateg	:= TAFGetValue( "C87", 1, xFilial("C87") + C9V->C9V_CATCI, "C87_CODIGO" )
			cXml 		+= 	xTafTag( "codCateg", cCodCateg,,.T. )
		EndIf

		cXml += "</ideVinculo>"
	EndIf
		
	cXml += "<infoExpRisco>"
	cXml +=		xTafTag( "dtIniCondicao", CM9->CM9_DTINI )

	dbSelectArea("T0Q")
	T0Q->(dBSetOrder( 1 ))	
	If T0Q->(MsSeek( xFilial("T0Q") + CM9->CM9_ID + CM9->CM9_VERSAO ))
				
		While T0Q->( !Eof() ) .and. T0Q->(T0Q_FILIAL + T0Q_ID + T0Q_VERSAO) == xFilial("T0Q") + CM9->(CM9_ID + CM9_VERSAO)
					
			cXml += "<infoAmb>"
			cXml +=		xTafTag( "codAmb",Posicione( "T04", 1, xFilial("T04") + T0Q->T0Q_CODAMB, "T04_CODIGO" ) )
			cXml += "</infoAmb>"

			T0Q->( DBSkip() )
				
		EndDo
	EndIf

	cXml += 	"<infoAtiv>"
	cXml +=			xTafTag( "dscAtivDes", AllTrim(CM9->CM9_DATIVD) )
			
	If V3E->( dbSeek( xFilial("V3E") + CM9->( CM9_ID + CM9_VERSAO ) ) )			
		
		While V3E->( !Eof() ) .And. xFilial("V3E") + V3E->( V3E_ID + V3E_VERSAO) == CM9->( CM9_FILIAL + CM9_ID + CM9_VERSAO )

			cXml += 	"<ativPericInsal>"
			cXml += xTafTag( "codAtiv", AllTrim( TAFGetValue( "V2L", 1, xFilial("V2L") + V3E->V3E_IDATV, "V2L_CODIGO", "" ) ) )
			cXml += 	"</ativPericInsal>"
			
			V3E->( dbSkip() )
		EndDo

	EndIf

	cXml += 	"</infoAtiv>"

	If CMA->(MsSeek( xFilial("CMA") + CM9->(CM9_ID + CM9->CM9_VERSAO) ))
		While CMA->( !EOF() ) .And. CMA->(CMA_FILIAL + CMA_ID + CMA_VERSAO ) == xFilial("CMA") + CM9->(CM9_ID + CM9_VERSAO)
			cXml += "<fatRisco>"
			cXml +=		xTafTag( "codFatRis"		, Posicione( "T3E", 1, xFilial("T3E") + CMA->CMA_CODFAT, "T3E_CODIGO" ))

			If TafColumnPos("CMA_DSCFAT") .And. __lLay0205
				If SuperGetMv('MV_TAFAMBE',.F.,"2") == "2" .Or. ( SuperGetMv('MV_TAFAMBE',.F.,"2") == "1" .And. DtoS(Date()) >= '20190710' )
					cXml += xTafTag( "dscFatRisc"			, FwCutOff(CMA->CMA_DSCFAT, .T.), , .T. )
				EndIf
			EndIf

			cXml +=		xTafTag( "tpAval"			, CMA->CMA_TPAVAL	,,.T. )
			cXml +=		xTafTag( "intConc"			, CMA->CMA_INTCON	,"@E 99999.9999",.T. )
			cXml +=		xTafTag( "limTol"			, CMA->CMA_LIMTOL 	,"@E 99999.9999",.T. )
			cXml +=		xTafTag( "unMed"			, AllTrim(TAFGetValue( "V3F", 1, xFilial("V3F") + CMA->CMA_UNMED, "V3F_CODIGO" )) ,,.T. )
			cXml +=		xTafTag( "tecMedicao"		, CMA->CMA_TECMED	,,.T. )
			cXml +=		xTafTag( "insalubridade"	, xFunTrcSN(CMA->CMA_INSALU, 1)	,,.T. )
			cXml +=		xTafTag( "periculosidade"	, xFunTrcSN(CMA->CMA_PERICU, 1)	,,.T. )
			cXml +=		xTafTag( "aposentEsp"		, xFunTrcSN(CMA->CMA_APOESP, 1)	,,.T. )

			If LEA->(MsSeek(xFilial("LEA")+CM9->(CM9_ID + CM9->CM9_VERSAO) + Space(nTamAmb ) + CMA->CMA_CODFAT))
							
				While LEA->(!Eof()) .And. LEA->(LEA_FILIAL + LEA_ID + LEA_VERSAO + LEA_CODAMB + LEA_CODFAT ) == xFilial("CM9") + CM9->(CM9_ID + CM9_VERSAO) + Space(nTamAmb ) + CMA->CMA_CODFAT 
					cXml += "<epcEpi>"
					cXml += xTafTag( "utilizEPC" 	, LEA->LEA_UTZEPC )
					cXml += xTafTag( "eficEpc" 		, xFunTrcSN(LEA->LEA_EFIEPC, 1),,.T.)
					cXml += xTafTag( "utilizEPI" 	, LEA->LEA_UTZEPI )
						

					If CMB->( MsSeek( xFilial("CMB") + CM9->(CM9_ID + CM9->CM9_VERSAO) + Space(nTamAmb ) + CMA->CMA_CODFAT + LEA->LEA_UTZEPI) )
						While CMB->( !Eof() ) .And. CMB->(CMB_FILIAL + CMB_ID + CMB_VERSAO + PadR(CMB_CODAMB, nTamAmb ) + CMB_CODFAT + CMB_UTZEPI ) == xFilial("CM9") + CM9->(CM9_ID + CM9_VERSAO) + Space(nTamAmb ) + CMA->CMA_CODFAT + LEA->LEA_UTZEPI
							cXml += "<epi>"
							cXml +=		xTafTag( "caEPI"			, CMB->CMB_CAEPI,,.T. )
							cXml +=		xTafTag( "dscEPI"			, GetDescEPI(CMB->CMB_IDDESC),,.T. )
							cXml +=		xTafTag( "eficEpi"			, xFunTrcSN(CMB->CMB_EFIEPI, 1) )
							cXml +=		xTafTag( "medProtecao"		, xFunTrcSN(CMB->CMB_MEDPRT, 1) )
							cXml +=		xTafTag( "condFuncto"		, xFunTrcSN(CMB->CMB_CNDFUN, 1) )
							cXml +=		xTafTag( "usoInint"			, xFunTrcSN(CMB->CMB_USOINI, 1) )
							cXml +=		xTafTag( "przValid"			, xFunTrcSN(CMB->CMB_PRZVLD, 1) )
							cXml +=		xTafTag( "periodicTroca"	, xFunTrcSN(CMB->CMB_PERTRC, 1) )
							cXml +=		xTafTag( "higienizacao"		, xFunTrcSN(CMB->CMB_HIGIEN, 1) )
							cXml +=	"</epi>"
							CMB->( DBSkip() )
						EndDo
					EndIf
								
					cXml += "</epcEpi>"
					LEA->(dbSkip())
				EndDo
			EndIf
								
			cXml += "</fatRisco>"
			CMA->(DbSkip() )
		EndDo
	EndIf
		
	dbSelectArea("T3S")
	T3S->(dBSetOrder( 1 ))
	If T3S->(MsSeek( xFilial("T3S") + CM9->CM9_ID + CM9->CM9_VERSAO ))
		While T3S->( !Eof() ) .and. T3S->(T3S_FILIAL + T3S_ID + T3S_VERSAO) == xFilial("T3S") + CM9->(CM9_ID + CM9_VERSAO)
			cXml += "<respReg>"
			cXml += xTafTag( "cpfResp"	, T3S->T3S_CPFRES )
			cXml += xTafTag( "nisResp"	, T3S->T3S_NISRES )
			cXml += xTafTag( "nmResp"	, T3S->T3S_NMRES  )
			cXml += xTafTag( "ideOC"	, T3S->T3S_IDEOC  )
			cXml += xTafTag( "dscOC"	, T3S->T3S_DSCOC ,,.T.)
			cXml += xTafTag( "nrOC"		, T3S->T3S_NROC )
			cXml += xTafTag( "ufOC"		, Posicione("C09",3,xFilial("C09")+T3S->T3S_UFOC,"C09_UF"),,.T.) 
			cXml += "</respReg>"
			T3S->(dbSkip())
		EndDo
	EndIf

	If !Empty( CM9->CM9_METERG ) .Or. !Empty( CM9->CM9_OBSCMP )
		cXml += "<obs>"

		If !Empty( CM9->CM9_METERG )
			cXml += xTafTag( "metErg"	, AllTrim(CM9->CM9_METERG) )
		EndIf

		If !Empty( CM9->CM9_OBSCMP )
			cXml += xTafTag( "obsCompl"	, CM9->CM9_OBSCMP )
		EndIf


		cXml += "</obs>"
	EndIf

	cXml += "</infoExpRisco>"

	//����������������������Ŀ
	//�Estrutura do cabecalho�
	//������������������������
	cXml := xTafCabXml(cXml,"CM9",cLayout,"ExpRisco",,cSeqXml)

	cXml := FwNoAccent( FwCutOff( cXML, .T. ) )

	//����������������������������Ŀ
	//�Executa gravacao do registro�
	//������������������������������
	If !lJob
		xTafGerXml( cXml, cLayout )
	EndIf
EndIf
return(cXml)

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF264Grv
Funcao de gravacao para atender o registro S-2240/2365

@parametros:
cLayout - Nome do Layout que esta sendo enviado, existem situacoes onde o mesmo fonte
					alimenta mais de um regsitro do E-Social, para estes casos serao necessarios
					tratamentos de acordo com o layout que esta sendo enviado.
nOpc   -  Opcao a ser realizada ( 3 = Inclusao, 4 = Alteracao, 5 = Exclusao )
cFilEv -  Filial do ERP para onde as informacoes deverao ser importadas
oDados -  Objeto com as informacoes a serem manutenidas ( Outras Integracoes )

@Return
lRet    - Variavel que indica se a importacao foi realizada, ou seja, se as
			informacoes foram gravadas no banco de dados
aIncons - Array com as inconsistencias encontradas durante a importacao

@author Mick William
@since 15/03/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Function TAF264Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID  )

Local cLogOpeAnt	:= ""
Local cCmpsNoUpd	:= "|CM9_FILIAL|CM9_ID|CM9_VERSAO|CM9_DTINI|CM9_VERANT|CM9_PROTPN|CM9_EVENTO|CM9_STATUS|CM9_ATIVO|"
Local cCabec    	:= "/eSocial/evtExpRisco/infoExpRisco"
Local cIdFunc 		:= ""
Local cEvento		:= ""
Local cDtInicio		:= ""
Local cExpRisco		:= ""
Local cInconMsg 	:= ""
Local cPathLEA 		:= ""
Local cCodEvent		:= Posicione("C8E",2,xFilial("C8E")+"S-"+cLayout,"C8E->C8E_ID")
Local cV3EPath		:= ""

Local nV3E			:= 0
Local nCMA      	:= 0
Local nCMB      	:= 0
Local nLEA			:= 0
Local nT3S			:= 0
Local nI        	:= 0
Local nJ        	:= 0
Local nIndChv		:= 5
Local nSeqErrGrv	:= 0
Local nRecno	 	:= CM9->(Recno())

Local lRet      	:= .F.
Local lRetif		:= .F.

Local aIncons   	:= {}
Local aRules    	:= {}
Local aChave    	:= {}

Local oModel    	:= Nil

Private lVldModel	:= .T. //Caso a chamada seja via integracao seto a variavel de controle de validacao como .T.
Private oDados 		:= oXML

Default cLayout		:= ""
Default cFilEv 		:= ""
Default nOpc    	:= 1
Default oXML    	:= Nil
Default cOwner		:= ""
Default cFilTran	:= ""
Default cPredeces	:= ""
Default nTafRecno	:= 0
Default cComplem	:= ""
Default cGrpTran	:= ""
Default cEmpOriGrp	:= ""
Default cFilOriGrp	:= ""
Default cXmlID		:= ""

//�����������������Ŀ
//�Chave do registro�
//�������������������
If TAF264Struct(.F.)
	If oDados:XPathHasNode("/eSocial/evtExpRisco/ideVinculo/cpfTrab") .And. oDados:XPathHasNode("/eSocial/evtExpRisco/ideVinculo/matricula")
		cIdFunc	:= FGetIdInt( "cpfTrab"	, "matricula", "/eSocial/evtExpRisco/ideVinculo/cpfTrab", "/eSocial/evtExpRisco/ideVinculo/matricula")
	ElseIf oDados:XPathHasNode("/eSocial/evtExpRisco/ideVinculo/cpfTrab") .And. !oDados:XPathHasNode("/eSocial/evtExpRisco/ideVinculo/matricula")
		cIdFunc	:= FGetIdInt( "cpfTrab"	,, "/eSocial/evtExpRisco/ideVinculo/cpfTrab")
	EndIf

	cDtInicio	:= FTafGetVal( cCabec + "/dtIniCondicao", 'C', .F., @aIncons, .F., '', '' )
	cDtInicio	:= StrTran( cDtInicio, "-", "" )

	Aadd( aChave, { "C", "CM9_FUNC"  , cIdFunc, .T.} ) 
	Aadd( aChave, { "C", "CM9_DTINI", cDtInicio, .T. } )

	cChave	:= Padr( cIdFunc, Tamsx3( aChave[ 1, 2 ])[1] ) + Padr( cDtInicio, Tamsx3( aChave[ 2, 2 ])[1] )

	//Verifica se o evento ja existe na base
	CM9->( DbSetOrder( 5 ) )
	If CM9->(MsSeek(FTafGetFil( cFilEv , @aIncons , "CM9" )+cChave+'1' ))
		nOpc 	:= 4

		If CM9->CM9_STATUS == "4"
			lRetif := .T.
		EndIf
		
	EndIf	

	If Empty(aIncons)

		CM9->(dBGoTo(nRecno))
		Begin Transaction
			
			//Funcao para validar se a operacao desejada pode ser realizada
			If FTafVldOpe( "CM9", nIndChv, @nOpc, cFilEv, @aIncons, aChave, @oModel, "TAFA264", cCmpsNoUpd, 1, .F. )

				If TafColumnPos( "CM9_LOGOPE" )
					cLogOpeAnt := CM9->CM9_LOGOPE
				endif

				//���������������������������������������������������������������Ŀ
				//�Carrego array com os campos De/Para de gravacao das informacoes�
				//�����������������������������������������������������������������
				aRules := TAF264Rul(cDtInicio,@cInconMsg, @nSeqErrGrv, cCodEvent, cOwner )
		
				//����������������������������������������������������������������Ŀ
				//�Quando se tratar de uma Exclusao direta apenas preciso realizar �
				//�o Commit(), nao eh necessaria nenhuma manutencao nas informacoes�
				//������������������������������������������������������������������
				If nOpc <> 5
					oModel:LoadValue( "MODEL_CM9", "CM9_FILIAL", CM9->CM9_FILIAL )

					If TAFColumnPos( "CM9_XMLID" )
						oModel:LoadValue( "MODEL_CM9", "CM9_XMLID", cXmlID )
					EndIf

					if TafXNode( oDados, cCodEvent, cOwner,("/eSocial/evtExpRisco/infoExpRisco/infoAtiv/dscAtivDes") )
						oModel:LoadValue( "MODEL_CM9", "CM9_DATIVD", FTafGetVal( "/eSocial/evtExpRisco/infoExpRisco/infoAtiv/dscAtivDes", "C", .F., @aIncons, .F. ) )
					EndIf

					if TafXNode( oDados, cCodEvent, cOwner,("/eSocial/evtExpRisco/infoExpRisco/obs/metErg") )
						oModel:LoadValue( "MODEL_CM9", "CM9_METERG", FTafGetVal( "/eSocial/evtExpRisco/infoExpRisco/obs/metErg", "C", .F., @aIncons, .F. ) )
					EndIf

					if TafXNode( oDados, cCodEvent, cOwner,("/eSocial/evtExpRisco/infoExpRisco/obs/obsCompl") )
						oModel:LoadValue( "MODEL_CM9", "CM9_OBSCMP", FTafGetVal( "/eSocial/evtExpRisco/infoExpRisco/obs/obsCompl", "C", .F., @aIncons, .F. ) )
					EndIf
					
					//����������������������������������������Ŀ
					//�Rodo o aRules para gravar as informacoes�
					//������������������������������������������
					For nI := 1 to Len( aRules )
						oModel:LoadValue( "MODEL_CM9", aRules[ nI, 01 ], FTafGetVal( aRules[ nI, 02 ], aRules[ nI, 03 ], aRules[ nI, 04 ], @aIncons, .F. ) )
					Next nI

					If Findfunction("TAFAltMan")
						if nOpc == 3
							TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_CM9', 'CM9_LOGOPE' , '1', '' )
						elseif nOpc == 4
							TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_CM9', 'CM9_LOGOPE' , '', cLogOpeAnt )
						EndIf
					EndIf

					//���������������������������������������������������������������Ŀ
					//�Quando se trata de uma alteracao deleto todas as linhas do Grid�
					//�����������������������������������������������������������������
					nT0Q := 1
					cT0QPath := cCabec + cExpRisco + "/infoAmb[" + AllTrim(Str(nT0Q)) + "]"
					
					If nOpc == 4 		
						For nJ := 1 to oModel:GetModel( "MODEL_T0Q" ):Length()
							oModel:GetModel( "MODEL_T0Q" ):GoLine(nJ)
							oModel:GetModel( "MODEL_T0Q" ):DeleteLine()
						Next nJ
					EndIf
					
					While oDados:XPathHasNode(cT0QPath)

						If nOpc == 4 .Or. nT0Q > 1
							oModel:GetModel( "MODEL_T0Q" ):lValid:= .T.
							oModel:GetModel( "MODEL_T0Q" ):AddLine()
						EndIf
						
						if oDados:XPathHasNode(cT0QPath + "/codAmb")
							oModel:LoadValue( "MODEL_T0Q", "T0Q_CODAMB", FGetIdInt( "codAmb"	 , "", cT0QPath + "/codAmb",,,,@cInconMsg, @nSeqErrGrv) )
						EndIf
						
						nT0Q++
						cT0QPath := cCabec + cExpRisco + "/infoAmb[" + AllTrim(Str(nT0Q)) + "]"
					EndDo

					nCMA := 1
					cCMAPath := cCabec + "/fatRisco[" + AllTrim(Str(nCMA)) + "]"
					
					If nOpc == 4
						For nJ := 1 to oModel:GetModel( "MODEL_CMA" ):Length()
							oModel:GetModel( "MODEL_CMA" ):GoLine( nJ )
							oModel:GetModel( "MODEL_CMA" ):DeleteLine()
						Next nJ
					EndIf

					
					While oDados:XPathHasNode(cCMAPath)
							
						oModel:GetModel( "MODEL_CMA" ):lValid:= .T.
						If nOpc == 4 .Or. nCMA > 1
							oModel:GetModel( "MODEL_CMA" ):AddLine()
						EndIf				
							
						if oDados:XPathHasNode(cCMAPath + "/codFatRis")
							oModel:LoadValue( "MODEL_CMA", "CMA_CODFAT", FGetIdInt( "codFatRis"	 , "", cCMAPath + "/codFatRis",,,,@cInconMsg, @nSeqErrGrv))
						EndIf

                        If TafColumnPos("CMA_DSCFAT") .And. __lLay0205
                            If oDados:XPathHasNode(cCMAPath + "/dscFatRisc")
    							oModel:LoadValue( "MODEL_CMA", "CMA_DSCFAT", FTafGetVal( cCMAPath + "/dscFatRisc" 	 , "C", .F., @aIncons, .F. ) )
	    					EndIf
                        EndIf
						
						if oDados:XPathHasNode(cCMAPath +"/intConc")
							oModel:LoadValue( "MODEL_CMA", "CMA_INTCON", FTafGetVal( cCMAPath + "/intConc" 	 , "N", .F., @aIncons, .F. ) )
						EndIf
							
						if oDados:XPathHasNode( cCMAPath + "/tecMedicao")
							oModel:LoadValue( "MODEL_CMA", "CMA_TECMED", FTafGetVal( cCMAPath + "/tecMedicao", "C", .F., @aIncons, .F. ) )
						EndIf
							
							
						If oDados:XPathHasNode( cCMAPath + "/limTol")
							oModel:LoadValue( "MODEL_CMA", "CMA_LIMTOL", FTafGetVal( cCMAPath + "/limTol", "N", .F., @aIncons, .F. ) )
						EndIf

						If oDados:XPathHasNode( cCMAPath + "/unMed")
							oModel:LoadValue( "MODEL_CMA", "CMA_UNMED", FGetIdInt( "unMed"	 , "", cCMAPath + "/unMed",,,,@cInconMsg, @nSeqErrGrv))
						EndIf

						If oDados:XPathHasNode( cCMAPath + "/tpAval")
							oModel:LoadValue( "MODEL_CMA", "CMA_TPAVAL", FTafGetVal( cCMAPath + "/tpAval", "C", .F., @aIncons, .F. ))
						EndIf

						If oDados:XPathHasNode( cCMAPath + "/insalubridade")
							oModel:LoadValue( "MODEL_CMA", "CMA_INSALU", xFunTrcSN( FTafGetVal( cCMAPath + "/insalubridade", "C", .F., @aIncons, .F. ), 2) )
						EndIf

						If oDados:XPathHasNode( cCMAPath + "/periculosidade")
							oModel:LoadValue( "MODEL_CMA", "CMA_PERICU", xFunTrcSN( FTafGetVal( cCMAPath + "/periculosidade", "C", .F., @aIncons, .F. ), 2) )
						EndIf

						If oDados:XPathHasNode( cCMAPath + "/aposentEsp")
							oModel:LoadValue( "MODEL_CMA", "CMA_APOESP", xFunTrcSN( FTafGetVal( cCMAPath + "/aposentEsp", "C", .F., @aIncons, .F. ), 2) )
						EndIf

						nLEA := 1
						cPathLEA := cCMAPath + "/epcEpi[" + cValToChar(nLEA) + "]"
						If nOpc == 4
							For nJ := 1 to oModel:GetModel( "MODEL_LEA" ):Length()
								oModel:GetModel( "MODEL_LEA" ):GoLine( nJ )
								//oModel:GetModel( "MODEL_LEA" ):DeleteLine()
							Next nJ
						EndIf
							
						
						While oDados:XPathHasNode(cPathLEA)
								
							oModel:GetModel( "MODEL_LEA" ):lValid:= .T.
							
							// Como esse grupo s� possui 1 linha, nunca faz o AddLine
							//If nOpc == 4 .Or. nLEA > 1
							//	oModel:GetModel( "MODEL_LEA" ):AddLine()
							//EndIf
						
							if oDados:XPathHasNode(cPathLEA + "/utilizEPC")
								oModel:LoadValue( "MODEL_LEA", "LEA_UTZEPC" , FTafGetVal(cPathLEA + "/utilizEPC" , "C", .F., @aIncons, .F. ) )
							EndIf
							
							if oDados:XPathHasNode(cPathLEA + "/utilizEPI")
								oModel:LoadValue( "MODEL_LEA", "LEA_UTZEPI" , FTafGetVal(cPathLEA + "/utilizEPI" , "C", .F., @aIncons, .F. ) )
							EndIf

							if oDados:XPathHasNode(cPathLEA + "/eficEpc")
								oModel:LoadValue( "MODEL_LEA", "LEA_EFIEPC" , xFunTrcSN( FTafGetVal(cPathLEA + "/eficEpc" , "C", .F., @aIncons, .F. ), 2 ) )
							EndIf

							nCMB := 1
							cCMBPath := cPathLEA + "/epi[" + AllTrim(Str(nCMB)) + "]"
							If nOpc == 4
								For nJ := 1 to oModel:GetModel( "MODEL_CMB" ):Length()
									oModel:GetModel( "MODEL_CMB" ):GoLine( nJ )
									oModel:GetModel( "MODEL_CMB" ):DeleteLine()
								Next nJ
							EndIf
								
											
							While oDados:XPathHasNode(cCMBPath)
								
								oModel:GetModel( "MODEL_CMB" ):lValid:= .T.
								If nOpc == 4 .Or. nCMB > 1
									oModel:GetModel( "MODEL_CMB" ):AddLine()
								EndIf				
									
								If oDados:XPathHasNode(cCMBPath + "/caEPI")
									oModel:LoadValue( "MODEL_CMB", "CMB_CAEPI" , FTafGetVal( cCMBPath + "/caEPI" 	 	 			, "C", .F., @aIncons, .F. ) )
								EndIf

								If oDados:XPathHasNode(cCMBPath + "/dscEPI")
									oModel:LoadValue( "MODEL_CMB", "CMB_IDDESC" , SetDescEPI( oModel:GetValue( "MODEL_CMB", "CMB_IDDESC" ), lRetif, FTafGetVal( cCMBPath + "/dscEPI", "C", .F., @aIncons, .F. ) ) )
								EndIf

								If oDados:XPathHasNode(cCMBPath + "/eficEpi")
									oModel:LoadValue( "MODEL_CMB", "CMB_EFIEPI", xFunTrcSN(FTafGetVal( cCMBPath + "/eficEpi" 	 	, "C", .F., @aIncons, .F. ),2) )
								EndIf

								If oDados:XPathHasNode(cCMBPath + "/medProtecao" )
									oModel:LoadValue( "MODEL_CMB", "CMB_MEDPRT", xFunTrcSN(FTafGetVal( cCMBPath + "/medProtecao" 	, "C", .F., @aIncons, .F. ),2) )
								EndIf

								If oDados:XPathHasNode(cCMBPath + "/condFuncto" )
									oModel:LoadValue( "MODEL_CMB", "CMB_CNDFUN", xFunTrcSN(FTafGetVal( cCMBPath + "/condFuncto" 	, "C", .F., @aIncons, .F. ),2) )
								EndIf
									
								If oDados:XPathHasNode(cCMBPath + "/usoInint" )
									oModel:LoadValue( "MODEL_CMB", "CMB_USOINI", xFunTrcSN(FTafGetVal( cCMBPath + "/usoInint" 	, "C", .F., @aIncons, .F. ),2) )
								EndIf

								If oDados:XPathHasNode(cCMBPath + "/przValid"	)
									oModel:LoadValue( "MODEL_CMB", "CMB_PRZVLD", xFunTrcSN(FTafGetVal( cCMBPath + "/przValid"		, "C", .F., @aIncons, .F. ),2) )
								EndIf

								If oDados:XPathHasNode(cCMBPath + "/periodicTroca")
									oModel:LoadValue( "MODEL_CMB", "CMB_PERTRC", xFunTrcSN(FTafGetVal( cCMBPath + "/periodicTroca"	, "C", .F., @aIncons, .F. ),2) )
								EndIf

								If oDados:XPathHasNode(cCMBPath + "/higienizacao" )
									oModel:LoadValue( "MODEL_CMB", "CMB_HIGIEN", xFunTrcSN(FTafGetVal( cCMBPath + "/higienizacao"	, "C", .F., @aIncons, .F. ),2) )
								EndIf

								nCMB++
								cCMBPath := cPathLEA + "/epi[" + AllTrim(Str(nCMB)) + "]"
							EndDo	
								
							nLEA++
							cPathLEA := cCMAPath + "/epcEpi[" + cValToChar(nLEA) + "]"
						EndDo

						nCMA++
						cCMAPath := cCabec + "/fatRisco[" + AllTrim(Str(nCMA)) + "]"
					EndDo


					nT3S := 1
					cT3SPath := cCabec + "/respReg[" + AllTrim(Str(nT3S)) + "]"				

					If nOpc == 4
						For nJ := 1 to oModel:GetModel( "MODEL_T3S" ):Length()
							oModel:GetModel( "MODEL_T3S" ):GoLine(nJ)
							oModel:GetModel( "MODEL_T3S" ):DeleteLine()
						Next nJ
					EndIf					
					
					While oDados:XPathHasNode(cT3SPath)
											
						If nOpc == 4 .Or. nT3S > 1					
							oModel:GetModel( "MODEL_T3S" ):lValid:= .T.
							oModel:GetModel( "MODEL_T3S" ):AddLine()
						EndIf
																
						If oDados:XPathHasNode(cT3SPath + "/nisResp")
							oModel:LoadValue( "MODEL_T3S", "T3S_NISRES", FTafGetVal( cT3SPath + "/nisResp", "C", .F., @aIncons, .F. ) )
						EndIf
						
						If oDados:XPathHasNode(cT3SPath + "/nrOC")
							oModel:LoadValue( "MODEL_T3S", "T3S_NROC", FTafGetVal( cT3SPath + "/nrOC", "C", .F., @aIncons, .F. ) )
						EndIf
												
						If oDados:XPathHasNode(cT3SPath + "/ufOC")
							oModel:LoadValue( "MODEL_T3S", "T3S_UFOC", FGetIdInt( "uf","", cT3SPath + "/ufOC",,,,@cInconMsg, @nSeqErrGrv) )
						EndIf
							
						If oDados:XPathHasNode(cT3SPath + "/cpfResp")
							oModel:LoadValue( "MODEL_T3S", "T3S_CPFRES", FTafGetVal( cT3SPath + "/cpfResp", "C", .F., @aIncons, .F. ) )
						EndIf

						If oDados:XPathHasNode(cT3SPath + "/nmResp")
							oModel:LoadValue( "MODEL_T3S", "T3S_NMRES", FTafGetVal( cT3SPath + "/nmResp", "C", .F., @aIncons, .F. ) )
						EndIf

						If oDados:XPathHasNode(cT3SPath + "/ideOC")
							oModel:LoadValue( "MODEL_T3S", "T3S_IDEOC", FTafGetVal( cT3SPath + "/ideOC", "C", .F., @aIncons, .F. ) )
						EndIf

						If oDados:XPathHasNode(cT3SPath + "/dscOC")
							oModel:LoadValue( "MODEL_T3S", "T3S_DSCOC", FTafGetVal( cT3SPath + "/dscOC", "C", .F., @aIncons, .F. ) )
						EndIf

						nT3S++
				
						cT3SPath := cCabec + "/respReg[" + AllTrim(Str(nT3S)) + "]"

					EndDo

					nV3E := 1
					cV3EPath := cCabec + "/infoAtiv/ativPericInsal[" + AllTrim(Str(nV3E)) + "]"
					
					If nOpc == 4
						For nJ := 1 to oModel:GetModel( "MODEL_V3E" ):Length()
							oModel:GetModel( "MODEL_V3E" ):GoLine(nJ)
							oModel:GetModel( "MODEL_V3E" ):DeleteLine()
						Next nJ
					EndIf

					While oDados:XPathHasNode(cV3EPath)
							
						If nOpc == 4 .Or. nV3E > 1
							oModel:GetModel( "MODEL_V3E" ):lValid:= .T.
							oModel:GetModel( "MODEL_V3E" ):AddLine()
						EndIf

						If oDados:XPathHasNode(cV3EPath + "/codAtiv")
							oModel:LoadValue( "MODEL_V3E", "V3E_IDATV", FGetIdInt( "codAtiv", "", cV3EPath + "/codAtiv",,,,@cInconMsg, @nSeqErrGrv)  )
						EndIf
							
						nV3E++
						cV3EPath := cCabec + "/infoAtiv/ativPericInsal[" + AllTrim(Str(nV3E)) + "]"

					EndDo

				Endif 
				
				
				///���������������������������Ŀ
				//�Efetiva a operacao desejada�
				//�����������������������������
				If Empty(cInconMsg)	.And. Empty(aIncons)
					If TafFormCommit( oModel )
						Aadd(aIncons, "ERRO19")
					Else
						lRet := .T.
					EndIf	 
				Else			
					Aadd(aIncons, cInconMsg)
					DisarmTransaction()	
				EndIf								 			   
				oModel:DeActivate()

			EndIf
		End Transaction
	EndIf
Else
	Aadd(aIncons, STR0023)
EndIf

//����������������������������������������������������������Ŀ
//�Zerando os arrays e os Objetos utilizados no processamento�
//������������������������������������������������������������
aSize( aRules, 0 )
aRules := Nil

aSize( aChave, 0 )
aChave := Nil

Return{ lRet, aIncons }

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF264Rul

Regras para gravacao das informacoes do registro S-2240/2365

@Param
cCabec - Cabecalho do caminho do arquivo XML

@Return
aRull  - Regras para a gravacao das informacoes

@author Mick William
@since 15/03/2016
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function TAF264Rul( cDtInicio, cInconMsg, nSeqErrGrv, cCodEvent, cOwner )

Local aRull	:= {}

Default cDtInicio		:= ""
Default cInconMsg		:= ""
Default nSeqErrGrv		:= 0
Default cCodEvent		:= ""
Default cOwner			:= ""

//Campos da tabela CM9
If TafXNode( oDados, cCodEvent, cOwner,("/eSocial/evtExpRisco/ideVinculo/cpfTrab") ) .And. TafXNode( oDados, cCodEvent, cOwner,("/eSocial/evtExpRisco/ideVinculo/matricula") )
	aAdd( aRull, {"CM9_FUNC", FGetIdInt( "cpfTrab", "matricula","/eSocial/evtExpRisco/ideVinculo/cpfTrab" ,"/eSocial/evtExpRisco/ideVinculo/matricula",,,@cInconMsg, @nSeqErrGrv),"C",.T.}) //cpfTrab
ElseIf TafXNode( oDados, cCodEvent, cOwner, ("/eSocial/evtExpRisco/ideVinculo/cpfTrab"))
	aAdd( aRull, { "CM9_FUNC", FGetIdInt( "cpfTrab",, "/eSocial/evtExpRisco/ideVinculo/cpfTrab" ,,,,@cInconMsg, @nSeqErrGrv)                , "C", .T. } ) //cpfTrab
EndIf

aAdd( aRull, { "CM9_DTINI", STOD(cDtInicio)	, "C", .T. } ) //dtIniCondicao

Return( aRull )

//-------------------------------------------------------------------
/*/{Protheus.doc} Taf264Vld
Funcao que valida os dados do registro posicionado,
verificando se ha incoerencias nas informac�es caso seja necessario gerar um XML

lJob - Informa se foi chamado por Job

@return .T.

@author Leandro Prado
@since 09/10/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Function Taf264Vld( cAlias, nRecno, nOpc, lJob )

Local aLogErro   := {}
Local cIdCateg	 := ""
Local cCodCat	 := ""
Local cCodFat    := ""
Local lValid	 := .T.
Local nTamAmb	 := TamSX3("LEA_CODAMB")[1]

Default lJob := .F.

//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := CM9->( Recno() )

T0Q->( DBSetOrder(1) )
T04->( DBSetOrder(1) )
V3E->( DBSetOrder(1) )
V2L->( DBSetOrder(1) )
CMA->( DBSetOrder(1) )
T3E->( DBSetOrder(1) )
V3F->( DBSetOrder(1) )
LEA->( DBSetOrder(1) )
CMB->( DBSetOrder(1) )

If CM9->CM9_STATUS $ " 1"

	If Empty( CM9->CM9_FUNC )
		aAdd( aLogErro, { "CM9_FUNC", "000295", "CM9", nRecno } ) //STR0295  //Informa��o obrigat�ria para gera��o do arquivo XML
	Else

		xVldRegra( "C1E", "REGRA_EXISTE_INFO_EMPREGADOR"   , @aLogErro, { { CM9->CM9_DTINI , CM9->CM9_DTINI , CM9->CM9_FILIAL } }, "CM9" )
		xVldRegra( "CM9", "REGRA_EXISTE_VINCULO"           , @aLogErro, { { CM9->CM9_FUNC  , CM9->CM9_DTINI } }, "CM9" )
		xVldRegra( "CM9", "REGRA_VINCULO_ATIVO_NA_DTEVENTO", @aLogErro, { { CM9->CM9_FUNC  , CM9->CM9_DTINI } }, "CM9" )
		xVldRegra( "CM9", "REGRA_RETIFICA_MESMO_VINCULO"   , @aLogErro, { { CM9->CM9_EVENTO, CM9->CM9_FUNC, CM9->CM9_ID, CM9->CM9_VERANT } }, "CM9" )
		xVldRegra( "CM9", "REGRA_EXISTE_CAT_OBITO_ANTERIOR", @aLogErro, { { CM9->CM9_FUNC, CM9->CM9_DTINI } }, "CM9" )

		C9V->( DBSetOrder( 2 ) )
		If C9V->( MsSeek( xFilial("C9V") + CM9->CM9_FUNC + "1" ) )
			
			If C9V->C9V_NOMEVE == "S2200"
				If Empty(C9V->C9V_MATRIC)
					AADD(aLogErro,{"C9V_MATRIC", "001196", "C9V", nRecno }) //STR1049 - "O preenchimento � obrigat�rio, exceto se o c�digo de categoria do trabalhador for igual a [901, 903, 904]."
				EndIf
			EndIf


			If CGC( C9V->C9V_CPF )
				
				cIdCateg := Posicione("CUP",1,xFilial("CUP") + C9V->(C9V_ID + C9V_VERSAO), "CUP_CODCAT")

				If !Empty( cIdCateg )
					cCodCat := Posicione("C87",1,xFilial("C87") + cIdCateg, "C87_CODIGO")

					If !Empty(cCodCat) .And. !(cCodCat $ "901|903|904") .And. Empty( C9V->C9V_NIS )
						lValid := .F.
						AADD(aLogErro,{"C9V_NIS", "001049", "C9V", nRecno }) //STR1049 - "O preenchimento � obrigat�rio, exceto se o c�digo de categoria do trabalhador for igual a [901, 903, 904]."
					Endif

					If (TafColumnPos('C9V_CATCI')) .And. lValid
						cCodCat := Posicione("C87",1,xFilial("C87") + C9V->C9V_CATCI, "C87_CODIGO")

						If !Empty(cCodCat) .And. !(cCodCat $ "901|903|904") .And. Empty( C9V->C9V_NIS )
							AADD(aLogErro,{"C9V_NIS", "001049", "C9V", nRecno }) //STR1049 - "O preenchimento � obrigat�rio, exceto se o c�digo de categoria do trabalhador for igual a [901, 903, 904]."
						Endif
					EndIf
				EndIf

				CUP->( DBSetOrder( 1 ) )
				If CUP->( MsSeek( xFilial("CUP") + C9V->C9V_ID + C9V->C9V_VERSAO ) )
					If DToS( CM9->CM9_DTINI ) < DToS( CUP->CUP_DTADMI )
						aAdd( aLogErro, { "CM9_DTINI", "000003", "CM9", nRecno } ) //STR0003 - "Verifique a data Inicial do Registro"
					EndIf
				Else
					aAdd( aLogErro, { "CM9_FUNC", "000295", "CM9", nRecno } ) //STR0295  //Informa��o obrigat�ria para gera��o do arquivo XML
				EndIf
			Else

				aAdd( aLogErro, { "C9V_CPF", "001195", "C9V", nRecno } ) //STR0295  //Informa��o obrigat�ria para gera��o do arquivo XML
			EndIf
		EndIf

		If T0Q->( MsSeek( xFilial( "T0Q" ) + CM9->CM9_ID + CM9->CM9_VERSAO ) )
			
			While T0Q->( !Eof() ) .and. CM9->(CM9_FILIAL + CM9_ID + CM9->CM9_VERSAO) == T0Q->(T0Q_FILIAL + T0Q_ID + T0Q_VERSAO)
				
				If Empty(T0Q->T0Q_CODAMB)
					aAdd( aLogErro, { "T0Q_CODAMB", "000010", "T0Q", nRecno } ) //Campo Inconsistente ou Vazio
				EndIf
				
				If !T04->( MsSeek( xFilial( "T04" ) + T0Q->T0Q_CODAMB ) )
					aAdd( aLogErro, { "T0Q_CODAMB", "000008", "T0Q", nRecno } ) //STR0008   //Registro n�o encontrado na tabela
				EndIf

				T0Q->( DBSkip() )
			EndDo
		EndIf

		If V3E->( MsSeek( xFilial("V3E") + CM9->CM9_ID + CM9->CM9_VERSAO ) )
			While V3E->( !Eof() ) .and. CM9->(CM9_FILIAL + CM9_ID + CM9_VERSAO) == V3E->( V3E_FILIAL + V3E_ID + V3E_VERSAO )

				If !V2L->( MsSeek( xFilial("V2L") + V3E->V3E_IDATV ) )
					aAdd( aLogErro, { "V3E_IDATV", "001197", "V3E", nRecno } ) //Identificar a atividade desempenhada dentre as descritas na Tabela 28
				EndIf
				
				V3E->( dbSkip() )

			EndDo
		EndIf

		If CMA->( MsSeek( xFilial( "CMA" ) + CM9->CM9_ID + CM9->CM9_VERSAO ) )
			While CMA->( !Eof() ) .and. ( CM9->CM9_FILIAL + CM9->CM9_ID + CM9->CM9_VERSAO == CMA->CMA_FILIAL + CMA->CMA_ID + CMA->CMA_VERSAO )
				
				If !Empty( CMA->CMA_CODFAT )
					If !T3E->( MsSeek( xFilial("T3E") + CMA->CMA_CODFAT ) )
						aAdd( aLogErro, { "CMA_CODFAT", "000008", "CMA", nRecno } ) //Registro n�o encontrado na tabela
					Else
						cCodFat := AllTrim(T3E->T3E_CODIGO)
					EndIf
				Else
					aAdd( aLogErro, { "CMA_CODFAT", "000010", "CMA", nRecno } ) //Campo Inconsistente ou Vazio
				EndIf

				If CMA->CMA_INTCON > 0 .And. CMA->CMA_TPAVAL == "2"
					aAdd( aLogErro, { "CMA_INTCON", "001198", "CMA", nRecno } ) //N�o informar se tpAval = 2. 
				ElseIf CMA->CMA_INTCON <= 0 .And. CMA->CMA_TPAVAL == "1"
					aAdd( aLogErro, { "CMA_INTCON", "001202", "CMA", nRecno } ) // Preenchimento obrigat�rio se {tpAval} = [1]
				EndIf

				If CMA->CMA_LIMTOL > 0 .And. !(CMA->CMA_TPAVAL == "1" .And. cCodFat $ "01.01.014|01.01.018|02.01.687|02.01.788")
					aAdd( aLogErro, { "CMA_LIMTOL", "001199", "CMA", nRecno } ) //Somente preencher se tpAval = 1 e codFatRis = 02.01.687 ou 02.01.788.
				ElseIf CMA->CMA_LIMTOL <= 0 .And. CMA->CMA_TPAVAL == "1" .And. cCodFat $ "01.01.014|01.01.018|02.01.687|02.01.788" 
					aAdd( aLogErro, { "CMA_LIMTOL", "001201", "CMA", nRecno } ) //Preenchimento obrigat�rio se tpAval = 1 e codFatRis = 02.01.687 ou 02.01.788.
				EndIf

				If !Empty( CMA->CMA_UNMED )
					If !V3F->( MsSeek( xFilial("V3F") + CMA->CMA_UNMED ) )
						aAdd( aLogErro, { "CMA_UNMED", "000008", "CMA", nRecno } ) //Registro n�o encontrado na tabela
					EndIf
				EndIf

				If !Empty( CMA->CMA_TECMED ) .And. CMA->CMA_TPAVAL == "2"
					aAdd( aLogErro, { "CMA_TECMED", "001200", "CMA", nRecno } ) //Preenchimento obrigat�rio se tpAval = 1. N�o informar se tpAval = 2. 
				EndIf

				If LEA->( MsSeek( xFilial("LEA") + CMA->( CMA_ID + CMA_VERSAO ) + PadR( " ", nTamAmb ) + CMA->CMA_CODFAT ) )
					
					If !( LEA->LEA_UTZEPC $ "012" )
						aAdd( aLogErro, { "LEA_UTZEPC", "000006", "LEA", nRecno } ) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
					EndIf

					If !( LEA->LEA_UTZEPI $ "012" )
						aAdd( aLogErro, { "LEA_UTZEPI", "000006", "LEA", nRecno } ) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
					EndIf

					If (!Empty(LEA->LEA_EFIEPC) .And. LEA->LEA_UTZEPC <> "2") .Or. (Empty(LEA->LEA_EFIEPC) .And. LEA->LEA_UTZEPC == "2")
						aAdd( aLogErro, { "LEA_EFIEPC", "001206", "LEA", nRecno } ) //Preenchimento obrigat�rio e exclusivo se {utilizEPC} = [2]
					EndIf

					If CMB->( MsSeek( xFilial("CMB") + LEA->( LEA_ID + LEA_VERSAO + LEA_CODAMB + LEA_CODFAT + LEA_UTZEPI ) ) )
						While CMB->( !Eof() ) .And. CMB->( CMB_FILIAL + CMB_ID + CMB_VERSAO + CMB_CODAMB + CMB_CODFAT + CMB_UTZEPI ) == ;
													LEA->( LEA_FILIAL + LEA_ID + LEA_VERSAO + LEA_CODAMB + LEA_CODFAT + LEA_UTZEPI )
							
							/*If Empty( CMB->CMB_CAEPI )
								aAdd( aLogErro, { "CMB_CAEPI", "000006", "CM9", nRecno } ) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
							EndIf*/
							
							If !( CMB->CMB_EFIEPI $ "12" )
								aAdd( aLogErro, { "CMB_EFIEPI", "000006", "CMB", nRecno } ) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
							EndIf
							
							If !( CMB->CMB_MEDPRT $ "12" )
								aAdd( aLogErro, { "CMB_MEDPRT", "000006", "CMB", nRecno } ) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
							EndIf

							If !( CMB->CMB_CNDFUN $ "12" )
								aAdd( aLogErro, { "CMB_CNDFUN", "000006", "CMB", nRecno } ) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
							EndIf

							If !( CMB->CMB_PRZVLD $ "12" )
								aAdd( aLogErro, { "CMB_PRZVLD", "000006", "CMB", nRecno } ) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
							EndIf

							If !( CMB->CMB_PERTRC $ "12" )
								aAdd( aLogErro, { "CMB_PERTRC", "000006", "CMB", nRecno } ) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
							EndIf

							If !( CMB->CMB_HIGIEN $ "12" )
								aAdd( aLogErro, { "CMB_HIGIEN", "000006", "CMB", nRecno } ) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
							EndIf

							CMB->( dbSkip() )

						EndDo
					EndIf

				EndIf
				CMA->( DBSkip() )
			EndDo
		EndIf

	EndIf

	//ATUALIZO O STATUS DO REGISTRO
	If Len( aLogErro ) > 0
		//Utilizo RecLock, pois o SETVALUE somente funciona em campos USADOS
		If RecLock( "CM9", .F. )
				CM9->CM9_STATUS := "1"
			CM9->( MsUnlock() )
		EndIf
	Else
		If RecLock( "CM9", .F. )
				CM9->CM9_STATUS := "0"
			CM9->( MsUnlock() )
		EndIf
	EndIf

Else
	aAdd( aLogErro, { "CM9", "000305", "CM9", nRecno } ) //Registros que j� foram transmitidos ao Fisco, n�o podem ser validados
EndIf

//N�o apresento o alert quando utilizo o JOB para validar
If !lJob
	xValLogEr( aLogErro )
EndIf

Return( aLogErro )


//-------------------------------------------------------------------
/*/{Protheus.doc} GerarEvtExc
Funcao que gera a exclus�o do evento (S-3000)

@Param  oModel  -> Modelo de dados
@Param  nRecno  -> Numero do recno
@Param  lRotExc -> Variavel que controla se a function � chamada pelo TafIntegraESocial

@Return .T.

@author Mick William
@since 15/03/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GerarEvtExc( oModel, nRecno, lRotExc )

Local cVerAnt    	:= ""
Local cProtocolo 	:= ""
Local cVersao    	:= ""
Local cChvRegAnt 	:= ""
Local cEvento    	:= ""

Local nOperation 	:= oModel:GetOperation()
Local nlI			:= 0
Local nlY			:= 0
Local nI        	:= 0
Local nJ        	:= 0
Local nQ		 	:= 0
Local nIgAdd	 	:= 0
Local nI5Add	 	:= 0
Local nT0Q			:= 0
Local nT3S			:= 0
Local nCMA			:= 0
Local nCMAAdd 		:= 0
Local nLEA			:= 0
Local nLEAAdd 		:= 0
Local nCMB			:= 0
Local nCMBAdd 		:= 0
Local nLEB			:= 0
Local nLEBAdd 		:= 0
Local nV3E			:= 0

Local aGravaCM9 	:= {}
Local aGravaT0Q		:= {}
Local aGravaCMA 	:= {}
Local aGravaCMB 	:= {}
Local aGravaLEB		:= {}
Local aGravaLEA		:= {}
Local aGravaT3S		:= {}
Local aGravaV3E		:= {}

Local oModelCM9  	:= Nil
Local oModelT0Q  	:= Nil
Local oModelCMA  	:= Nil
Local oModelCMB  	:= Nil
Local oModelLEB		:= Nil
Local oModelLEA		:= Nil 
Local oModelT3S		:= Nil
Local oModelV3E		:= Nil

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

Begin Transaction

	//Posiciona o item
	dbSelectArea("CM9")
	CM9->( DBGoTo( nRecno ) )

	//Carrego a Estrutura dos Models a serem gravados	

	oModelCM9 := oModel:GetModel( "MODEL_CM9" )
	oModelT0Q := oModel:GetModel( "MODEL_T0Q" )
	oModelCMA := oModel:GetModel( "MODEL_CMA" )
	oModelCMB := oModel:GetModel( "MODEL_CMB" )
	oModelLEA := oModel:GetModel( "MODEL_LEA" )
	oModelLEB := oModel:GetModel( "MODEL_LEB" )
	oModelT3S := oModel:GetModel( "MODEL_T3S" )
	oModelV3E := oModel:GetModel( "MODEL_V3E" )
	
	//�����������������������������������������������������������Ŀ
	//�Busco a versao anterior do registro para gravacao do rastro�
	//�������������������������������������������������������������
	cVerAnt    := oModelCM9:GetValue( "CM9_VERSAO" )
	cProtocolo := oModelCM9:GetValue( "CM9_PROTUL" )
	cEvento    := oModelCM9:GetValue( "CM9_EVENTO" )

	//�����������������������������������������������������������������Ŀ
	//�Neste momento eu gravo as informacoes que foram carregadas       �
	//�na tela, pois neste momento o usuario ja fez as modificacoes que �
	//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
	//�nao devem ser consideradas neste momento                         �
	//�������������������������������������������������������������������
	For nI := 1 to Len( oModelCM9:aDataModel[ 1 ] )
		aAdd( aGravaCM9, { oModelCM9:aDataModel[ 1, nI, 1 ], oModelCM9:aDataModel[ 1, nI, 2 ] } )
	Next
				
	//T3S				  
	For nT3S := 1 To oModel:GetModel( 'MODEL_T3S' ):Length() 
		oModel:GetModel( 'MODEL_T3S' ):GoLine(nT3S)
					
		If !oModel:GetModel( 'MODEL_T3S' ):IsDeleted()
			aAdd( aGravaT3S, {  oModelT3S:GetValue( "T3S_NISRES"	),;
								oModelT3S:GetValue( "T3S_NROC"  	),;
								oModelT3S:GetValue( "T3S_UFOC"  	),;
								oModelT3S:GetValue( "T3S_CPFRES"  	),;
								oModelT3S:GetValue( "T3S_NMRES"  	),;
								oModelT3S:GetValue( "T3S_IDEOC"  	),;
								oModelT3S:GetValue( "T3S_DSCOC"  	)})
		EndIf										
	Next nT3S //Fim - T3S

	//V3E
	For nV3E := 1 To oModelV3E:Length() 
		oModelV3E:GoLine(nV3E)
					
		If !oModelV3E:IsDeleted()
			aAdd( aGravaV3E, {  oModelV3E:GetValue( "V3E_IDATV" ) } )
		EndIf
	Next nV3E //Fim - nV3E

	For nCMA := 1 To oModel:GetModel( 'MODEL_CMA' ):Length() 
		oModel:GetModel( 'MODEL_CMA' ):GoLine(nCMA)

        If !oModel:GetModel( 'MODEL_CMA' ):IsDeleted()

            If TafColumnPos("CMA_DSCFAT") .And. __lLay0205
                aAdd( aGravaCMA, {  oModelCMA:GetValue( "CMA_CODFAT" ),;                                                    
                                    oModelCMA:GetValue( "CMA_INTCON" ),;
                                    oModelCMA:GetValue( "CMA_TECMED" ),;
                                    oModelCMA:GetValue( "CMA_LIMTOL" ),;
                                    oModelCMA:GetValue( "CMA_UNMED"  ),;
                                    oModelCMA:GetValue( "CMA_INSALU" ),;
                                    oModelCMA:GetValue( "CMA_PERICU" ),;
                                    oModelCMA:GetValue( "CMA_APOESP" ),;
                                    oModelCMA:GetValue( "CMA_TPAVAL" ),;
                                    oModelCMA:GetValue( "CMA_DSCFAT" )} )
            Else
                aAdd( aGravaCMA, {  oModelCMA:GetValue( "CMA_CODFAT" ),;                                                    
                                    oModelCMA:GetValue( "CMA_INTCON" ),;
                                    oModelCMA:GetValue( "CMA_TECMED" ),;
                                    oModelCMA:GetValue( "CMA_LIMTOL" ),;
                                    oModelCMA:GetValue( "CMA_UNMED"  ),;
                                    oModelCMA:GetValue( "CMA_INSALU" ),;
                                    oModelCMA:GetValue( "CMA_PERICU" ),;
                                    oModelCMA:GetValue( "CMA_APOESP" ),;
                                    oModelCMA:GetValue( "CMA_TPAVAL" )} )
            EndIf
										
			For nLEA := 1 To oModel:GetModel( 'MODEL_LEA' ):Length() 
				oModel:GetModel( 'MODEL_LEA' ):GoLine(nLEA)
									
				If !oModel:GetModel( 'MODEL_LEA' ):IsDeleted()
				 	aAdd( aGravaLEA, { oModelCMA:GetValue( "CMA_CODFAT" ),;
										 oModelLEA:GetValue( "LEA_UTZEPC" ),;
										 oModelLEA:GetValue( "LEA_UTZEPI" ),;
										 oModelLEA:GetValue( "LEA_EFIEPC" )} )
										
					//CMB					  
					For nCMB := 1 To oModel:GetModel( 'MODEL_CMB' ):Length() 
						oModel:GetModel( 'MODEL_CMB' ):GoLine(nCMB)
											
						If !oModel:GetModel( 'MODEL_CMB' ):IsDeleted()
							aAdd (aGravaCMB ,{	  oModelCMA:GetValue( "CMA_CODFAT"),;
													oModelLEA:GetValue( "LEA_UTZEPC"),;
													oModelCMB:GetValue( "CMB_CAEPI" ),;
													oModelCMB:GetValue( "CMB_EFIEPI"),;
													oModelCMB:GetValue( "CMB_MEDPRT"),;
													oModelCMB:GetValue( "CMB_CNDFUN"),;
													oModelCMB:GetValue( "CMB_PRZVLD"),;
													oModelCMB:GetValue( "CMB_PERTRC"),;
													oModelCMB:GetValue( "CMB_HIGIEN"),;
													oModelCMB:GetValue( "CMB_IDDESC"),;
													oModelCMB:GetValue( "CMB_USOINI")})
							EndIf										
					Next nCMB //Fim - CMB										 
				EndIf
			Next nLEA //Fim - LEA
		EndIf
	Next nCMA //Fim - CMA
							
				
	For nT0Q := 1 To oModel:GetModel( 'MODEL_T0Q' ):Length()
		oModel:GetModel( 'MODEL_T0Q' ):Goline(nT0Q) 
	
		If !oModel:GetModel( 'MODEL_T0Q' ):IsDeleted()								
				aAdd( aGravaT0Q, {  oModelT0Q:GetValue( "T0Q_CODAMB" ) } )
		
		EndIf
	Next nT0Q //Fim - T0Q	
	
	//�����������������������������������������������������������Ŀ
	//�Seto o campo como Inativo e gravo a versao do novo registro�
	//�no registro anterior                                       �
	//|                                                           |
	//|ATENCAO -> A alteracao destes campos deve sempre estar     |
	//|abaixo do Loop do For, pois devem substituir as informacoes|
	//|que foram armazenadas no Loop acima                        |
	//�������������������������������������������������������������
	FAltRegAnt( "CM9", "2" )

	//��������������������������������������������������Ŀ
	//�Neste momento eu preciso setar a operacao do model�
	//�como Inclusao                                     �
	//����������������������������������������������������
	oModel:DeActivate()
	oModel:SetOperation( 3 )
	oModel:Activate()

	//�������������������������������������������������������Ŀ
	//�Neste momento eu realizo a inclusao do novo registro ja�
	//�contemplando as informacoes alteradas pelo usuario     �
	//���������������������������������������������������������
	For nlI := 1 To Len( aGravaCM9 )	
		oModel:LoadValue( 'MODEL_CM9', aGravaCM9[ nlI, 1 ], aGravaCM9[ nlI, 2 ] )
	Next  

	For nT3S := 1 To Len( aGravaT3S )
		oModel:GetModel( 'MODEL_T3S' ):LVALID	:= .T.
	
		If nT3S > 1
			oModel:GetModel( 'MODEL_T3S' ):AddLine()
		EndIf

		oModel:LoadValue( "MODEL_T3S", "T3S_NISRES"	, aGravaT3S[nT3S][1] ) 
		oModel:LoadValue( "MODEL_T3S", "T3S_NROC"  	, aGravaT3S[nT3S][2] ) 
		oModel:LoadValue( "MODEL_T3S", "T3S_UFOC"  	, aGravaT3S[nT3S][3] )
		oModel:LoadValue( "MODEL_T3S", "T3S_CPFRES"	, aGravaT3S[nT3S][4] )
		oModel:LoadValue( "MODEL_T3S", "T3S_NMRES"  , aGravaT3S[nT3S][5] )
		oModel:LoadValue( "MODEL_T3S", "T3S_IDEOC"  , aGravaT3S[nT3S][6] )
		oModel:LoadValue( "MODEL_T3S", "T3S_DSCOC"  , aGravaT3S[nT3S][7] )
							 
	Next nT3S //Fim - T3S

	For nV3E := 1 To Len( aGravaV3E )
		oModel:GetModel( 'MODEL_V3E' ):LVALID	:= .T.
		
		If nV3E > 1
			oModel:GetModel( 'MODEL_V3E' ):AddLine()
		EndIf
					
		oModel:LoadValue( "MODEL_V3E", "V3E_IDATV" , aGravaV3E[nV3E][1] )
						 
	Next nV3E //Fim - V3E
				
				
	For nT0Q := 1 To Len( aGravaT0Q )
		oModel:GetModel( 'MODEL_T0Q' ):LVALID	:= .T.
		
		If nT0Q > 1
			oModel:GetModel( 'MODEL_T0Q' ):AddLine()
		EndIf
	
		oModel:LoadValue( "MODEL_T0Q", "T0Q_CODAMB", aGravaT0Q[nT0Q][1] )
		
	Next nT0Q //Fim - T0Q

	nCMAAdd := 1
	For nCMA := 1 To Len( aGravaCMA )
		oModel:GetModel( 'MODEL_CMA' ):LVALID	:= .T.
		
		If nCMAAdd > 1
			oModel:GetModel( 'MODEL_CMA' ):AddLine()
		EndIf

		If TafColumnPos("CMA_DSCFAT") .And. __lLay0205
            oModel:LoadValue( "MODEL_CMA", "CMA_DSCFAT", aGravaCMA[nCMA][10] )
        EndIf
					
		oModel:LoadValue( "MODEL_CMA", "CMA_CODFAT", aGravaCMA[nCMA][1] )
		oModel:LoadValue( "MODEL_CMA", "CMA_INTCON", aGravaCMA[nCMA][2] )
		oModel:LoadValue( "MODEL_CMA", "CMA_TECMED", aGravaCMA[nCMA][3] )
		oModel:LoadValue( "MODEL_CMA", "CMA_LIMTOL", aGravaCMA[nCMA][4] )
		oModel:LoadValue( "MODEL_CMA", "CMA_UNMED" , aGravaCMA[nCMA][5] )
		oModel:LoadValue( "MODEL_CMA", "CMA_INSALU", aGravaCMA[nCMA][6] )
		oModel:LoadValue( "MODEL_CMA", "CMA_PERICU", aGravaCMA[nCMA][7] )
		oModel:LoadValue( "MODEL_CMA", "CMA_APOESP", aGravaCMA[nCMA][8] )
		oModel:LoadValue( "MODEL_CMA", "CMA_TPAVAL", aGravaCMA[nCMA][9] )
        


		nLEAAdd := 1
		For nLEA := 1 To Len( aGravaLEA )
			If aGravaLEA[nLEA][1] == aGravaCMA[nCMA][1]
				oModel:GetModel( 'MODEL_LEA' ):LVALID	:= .T.
			
				If nLEAAdd > 1
					oModel:GetModel( 'MODEL_LEA' ):AddLine()
				EndIf
									
				oModel:LoadValue( "MODEL_LEA", "LEA_UTZEPC", aGravaLEA[nLEA][2] )
				oModel:LoadValue( "MODEL_LEA", "LEA_UTZEPI", aGravaLEA[nLEA][3] )
				oModel:LoadValue( "MODEL_LEA", "LEA_EFIEPC", aGravaLEA[nLEA][4] )
		
				nCMBAdd := 1
				For nCMB := 1 To Len( aGravaCMB )
					If aGravaCMB[nCMB][1] == aGravaCMA[nCMA][1] .And. aGravaCMB[nCMB][2] == aGravaLEA[nLEA][2]
						oModel:GetModel( 'MODEL_CMB' ):LVALID	:= .T.
					
						If nCMBAdd > 1
							oModel:GetModel( 'MODEL_CMB' ):AddLine()
						EndIf
						
						oModel:LoadValue( "MODEL_CMB", "CMB_CAEPI",  aGravaCMB[nCMB][3] )
						oModel:LoadValue( "MODEL_CMB", "CMB_EFIEPI", aGravaCMB[nCMB][4] )
						oModel:LoadValue( "MODEL_CMB", "CMB_MEDPRT", aGravaCMB[nCMB][5] )
						oModel:LoadValue( "MODEL_CMB", "CMB_CNDFUN", aGravaCMB[nCMB][6] )
						oModel:LoadValue( "MODEL_CMB", "CMB_PRZVLD", aGravaCMB[nCMB][7] )
						oModel:LoadValue( "MODEL_CMB", "CMB_PERTRC", aGravaCMB[nCMB][8] )
						oModel:LoadValue( "MODEL_CMB", "CMB_HIGIEN", aGravaCMB[nCMB][9] )
						oModel:LoadValue( "MODEL_CMB", "CMB_IDDESC", aGravaCMB[nCMB][10])
						oModel:LoadValue( "MODEL_CMB", "CMB_USOINI", aGravaCMB[nCMB][11])
				
						nCMBAdd++
					EndIf
				Next nCMB //Fim - CMB
				nLEAAdd++
			EndIf
		Next nLEA //Fim - LEA
		
		nCMAAdd++
				
	Next nCMA //Fim - CMA							

	//�������������������������������Ŀ
	//�Busco a versao que sera gravada�
	//���������������������������������
	cVersao := xFunGetVer()

	//�����������������������������������������������������������Ŀ
	//|ATENCAO -> A alteracao destes campos deve sempre estar     |
	//|abaixo do Loop do For, pois devem substituir as informacoes|
	//|que foram armazenadas no Loop acima                        |
	//�������������������������������������������������������������
	oModel:LoadValue( "MODEL_CM9"	, "CM9_VERSAO", cVersao    )
	oModel:LoadValue( "MODEL_CM9"	, "CM9_VERANT", cVerAnt    )
	oModel:LoadValue( "MODEL_CM9"	, "CM9_PROTPN", cProtocolo )
	oModel:LoadValue( "MODEL_CM9"	, "CM9_PROTUL", ""         )
	
	/*---------------------------------------------------------
	Tratamento para que caso o Evento Anterior fosse de exclus�o
	seta-se o novo evento como uma "nova inclus�o", caso contr�rio o
	evento passar a ser uma altera��o
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_CM9", "CM9_EVENTO", "E" )
	oModel:LoadValue( "MODEL_CM9", "CM9_ATIVO" , "1" )

	//Gravo altera��o para o Extempor�neo
	If lGoExtemp
		TafGrvExt( oModel, 'MODEL_CM9', 'CM9' )	
	EndIf

	FwFormCommit( oModel )
	TAFAltStat( 'CM9',"6" )
	
End Transaction

Return ( .T. )

//-------------------------------------------------------------------
/*/{Protheus.doc} GetDescEPI
Retorna a descri��o do EPI
@author  Victor A. Barbosa

cIdDesc  - ID da descri��o gravado na tabela CMB (Utilizado para retornar o conte�do)

@since   28/01/2019
@version 1
/*/
//-------------------------------------------------------------------
Static Function GetDescEPI(cIDDesc)

Local aArea 	:= GetArea()
Local cDescRet	:= ""

dbSelectArea("V3D")
V3D->( dbSetOrder(1) )

If V3D->( MsSeek( xFilial("V3D") + cIDDesc ) )
	cDescRet := V3D->V3D_DSCEPI 
EndIf

RestArea(aArea)

Return(cDescRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} SetDescEPI
Insere uma nova descri��o de EPI
@author  Victor A. Barbosa

cIdDesc  - ID da descri��o gravado na tabela CMB (Caso seja altera��o)
cDescEPI - Descri��o a ser gravado na tabela CMB

@since   28/01/2019
@version 1
/*/
//-------------------------------------------------------------------
Static Function SetDescEPI(cIdDesc, lRetif, cDescEPI)

Local aArea 	:= GetArea()
Local lLock		:= .T.
Local cIDV3D	:= ""

Default cIDDesc := ""
Default cIdDesc	:= ""
Default lRetif	:= .F.

dbSelectArea("V3D")
V3D->( dbSetOrder(1) )

If !Empty(cIDDesc) .And. !lRetif
	
	If V3D->( MsSeek( xFilial("V3D") + cIDDesc ) )
		lLock 	:= .F.
		cIDV3D 	:= cIDDesc
	EndIf

Else

	// Busca a nova numera��o	
	cIDV3D := GetSX8Num( "V3D", "V3D_ID" )

	While V3D->( MsSeek( xFilial("V3D") + cIDV3D ) )
		cIDV3D := GetSX8Num( "V3D", "V3D_ID" )
		ConfirmSX8()
	EndDo

EndIf

RecLock("V3D", lLock)
V3D->V3D_FILIAL := xFilial("V3D")
V3D->V3D_ID		:= cIDV3D
V3D->V3D_DSCEPI	:= cDescEPI
V3D->( MsUnlock() )

V3D->( MsUnlock() )

RestArea(aArea)

Return(cIDV3D)

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF264Struct
Valida se o ambiente possui a estrutura correta
@author  Victor A. Barbosa
@since   31/01/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function TAF264Struct(lViewHelp)

Local lRet	:= .F.

If TafColumnPos("V3D_ID") .And. TafColumnPos("CMB_IDDESC") .And. TafColumnPos("LEA_EFIEPC") .And. TafColumnPos("V3E_IDATV")
	lRet := .T.
Else
	If lViewHelp
		MsgInfo( STR0023 , STR0024 ) //"O ambiente do TAF encontra-se desatualizado. Para utiliza��o desta funcionalidade, ser� necess�rio executar o compatibilizador de dicion�rio de dados UPDDISTR dispon�vel no portal do cliente do TAF."
									 // "Ambiente Desatualizado!"
	EndIf
EndIf

Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF264Trigger
Cria��o da trigger
@author  Victor A. Barbosa
@since   01/02/2019
@version 1
/*/
//-------------------------------------------------------------------
Static Function TAF264Trigger()

Local aAux :=   FwStruTrigger(;
			"V3E_IDATV" ,; // Campo Dominio
			"V3E_DESCAT" ,; // Campo de Contradominio
			"Iif(!Empty(M->V3E_IDATV), V2L->V2L_CODIGO + ' - ' + V2L->V2L_DESCRI, '') ",; // Regra de Preenchimento
			.F. ,; // Se posicionara ou nao antes da execucao do gatilhos
			"V2L" ,; // Alias da tabela a ser posicionada
			1 ,; // Ordem da tabela a ser posicionada
			"xFilial('V2L') + M->V3E_IDATV" ,; // Chave de busca da tabela a ser posicionada
			"!Empty(M->V3E_IDATV)" ,; // Condicao para execucao do gatilho
			"01" ) // Sequencia do gatilho (usado para identificacao no caso de erro)   

Return aAux