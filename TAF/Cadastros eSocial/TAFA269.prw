#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFA269.CH"
#INCLUDE "TOPCONN.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA269
Exclus�o de Evento Enviado Indevidamente (S-3000)

@author Anderson Costa
@since 12/09/2013
@version 1.0

/*/ 
//-------------------------------------------------------------------
Function TAFA269()

Private oBrw := FWmBrowse():New()

// Fun��o que indica se o ambiente � v�lido para o eSocial 2.3
If TafAtualizado()

	oBrw:SetDescription(STR0001)    //"Exclus�o de Evento Enviado Indevidamente"
	oBrw:SetAlias( 'CMJ')
	oBrw:SetMenuDef( 'TAFA269' )

	If FindFunction('TAFSetFilter')
		oBrw:SetFilterDefault(TAFBrwSetFilter("CMJ","TAFA269","S-3000"))
	Else
		oBrw:SetFilterDefault( "CMJ_ATIVO == '1'" ) //Filtro para que apenas os registros ativos sejam exibidos ( 1=Ativo, 2=Inativo )
	EndIf

	TafLegend(2,"CMJ",@oBrw)
	oBrw:Activate()

EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Anderson Costa
@since 12/09/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao := {}
Local aRotina := {}

If FindFunction('TafXmlRet')
	Aadd( aFuncao, { "" , "TafxmlRet('TAF269Xml','3000','CMJ')" , "1" } )
Else
	Aadd( aFuncao, { "" , "TAF269Xml" , "1" } )
EndIf
Aadd( aFuncao, { "" , "Taf269Vld" , "2" } )
Aadd( aFuncao, { "" , "xFunHisAlt( 'CMJ', 'TAFA269' ,,,, 'TAF269XML','3000' )" , "3" } )
Aadd( aFuncao, { "" , "TAFXmlLote( 'CMJ', 'S-3000' , 'evtExclusao' , 'TAF269Xml',, oBrw )" , "5" } )
Aadd( aFuncao, { "" , "Processa( {||TAF269Ajust(),'Processando', 'Iniciando Rotina de Ajuste' } )"  	, "6" } )
//Aadd( aFuncao, { "" , "Processa( {||TAF269CPFAjust(),'Processando', 'Iniciando Ajuste de CPF/NIS' } )"  , "7" } )  //Tem que alterar para olhar a T1U/T1V
Aadd( aFuncao, { "" , "xFunAltRec( 'CMJ' )" , "10" } )

lMenuDIf := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDIf )

If lMenuDif
	ADD OPTION aRotina Title "Visualizar"       Action 'VIEWDEF.TAFA269' OPERATION 2 ACCESS 0
Else
	aRotina	:=	xFunMnuTAF( "TAFA269" , , aFuncao)
EndIf

Return (aRotina )

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC

@author Anderson Costa
@since 12/09/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

Local oStruCMJ	:=	FWFormStruct( 1, 'CMJ' )
Local oModel    :=	MPFormModel():New( 'TAFA269' ,,,{|oModel| SaveModel(oModel)})

oModel:AddFields('MODEL_CMJ', /*cOwner*/, oStruCMJ)
oModel:GetModel('MODEL_CMJ'):SetPrimaryKey({'CMJ_FILIAL', 'CMJ_ID', 'CMJ_VERSAO'})

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Anderson Costa
@since 12/09/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oModel      :=  FWLoadModel( 'TAFA269' )
Local oStruCMJ    :=  FWFormStruct( 2, 'CMJ' )

Local oView       :=  FWFormView():New()

If FindFunction("TafAjustRecibo")
	TafAjustRecibo(oStruCMJ,"CMJ")
EndIf

oView:SetModel( oModel )
oView:AddField( 'VIEW_CMJ', oStruCMJ, 'MODEL_CMJ' )

oView:EnableTitleView( 'VIEW_CMJ', STR0001 )    //"Exclus�o de Evento Enviado Indevidamente"
oView:CreateHorizontalBox( 'FIELDSCMJ', 100 )
oView:SetOwnerView( 'VIEW_CMJ', 'FIELDSCMJ' )

xFunRmFStr(@oStruCMJ, 'CMJ')
oStruCMJ:RemoveField('CMJ_REGREF') //N�o mostra o campo que tem o R_E_C_N_O_ da origem.

If TafColumnPos( "CMJ_LOGOPE" )
	oStruCMJ:RemoveField( "CMJ_LOGOPE" )
EndIf

Return oView

//-------------------------------------------------------------------	
/*/{Protheus.doc} TAF269Xml
@author Evandro dos Santos Oliveira
@since 23/10/2013
@version 1.0
		
@Param:
lJob   - Informa se foi chamado por Job
nOpc   - Opcao a ser realizada ( 3 = Inclusao, 4 = Alteracao, 5 = Exclusao )
lRemEmp - Exclusivo do Evento S-1000
cSeqXml - Numero sequencial para composi��o da chave ID do XML

@return
cXml - Estrutura do Xml do Layout S-3000 
/*/
//-------------------------------------------------------------------
Function TAF269Xml(cAlias,nRecno,nOpc,lJob,lRemEmp,cSeqXml)

Local cXml    	:= ""
Local cLayout 	:= "3000"
Local cReg    	:= "Exclusao" 
Local cPerApu	:= ""
Local lXmlVLd	:= IIF(FindFunction('TafXmlVLD'),TafXmlVLD('TAF269XML'),.T.)

Default cAlias 	:= "CMJ"
Default nRecno	:= 1
Default nOpc	:= 1
Default lJob 	:= .F.
Default cSeqXml := ""

If lXmlVLd
	If CMJ->CMJ_INDAPU == '1' .AND. !Empty(CMJ->CMJ_PERAPU)
		If Len(Alltrim(CMJ->CMJ_PERAPU)) > 4
			cPerApu := substr(CMJ->CMJ_PERAPU,1,4) + '-' + substr(CMJ->CMJ_PERAPU,5,7)
		Else
			cPerApu := substr(CMJ->CMJ_PERAPU,1,4) 
		EndIf
	ElseIf CMJ->CMJ_INDAPU == '2' .AND. !Empty(CMJ->CMJ_PERAPU)
		cPerApu := substr(CMJ->CMJ_PERAPU,1,4)
	Else
		cPerApu := CMJ->CMJ_PERAPU
	EndIf

	cXml :=		"<infoExclusao>"    
	cXml +=	 		xTafTag("tpEvento",Posicione("C8E",1,xFilial("C8E") + CMJ->CMJ_TPEVEN,"C8E_CODIGO"))
	cXml +=			xTafTag("nrRecEvt",CMJ->CMJ_NRRECI)


	xTafTagGroup("ideTrabalhador"	,{{"cpfTrab" 	,CMJ->CMJ_CPF,,.F.};
										, {"nisTrab"	,CMJ->CMJ_NIS,,.T.}};
										, @cXml)	

	xTafTagGroup("ideFolhaPagto"	,{{"indApuracao" 	,CMJ->CMJ_INDAPU,,.F.};
										, {"perApur"		,cPerApu,,.F.}};
										, @cXml)	

	cXml +=		"</infoExclusao>"

	/*����������������������Ŀ
	�Estrutura do cabecalho�
	������������������������*/
	cXml := xTafCabXml(cXml,"CMJ",cLayout,cReg,,cSeqXml)

	/*����������������������������Ŀ
	�Executa gravacao do registro�
	������������������������������*/
	If !lJob
		xTafGerXml(cXml,cLayout)
	EndIf
EndIf
Return(cXml)

//-------------------------------------------------------------------	
/*/{Protheus.doc} TAF269Vld	
@author Evandro dos Santos Oliveira
@since 23/10/2013
@version 1.0

@param		
lJob   - Informa se foi chamado por Job
nOpc   - Opcao a ser realizada ( 3 = Inclusao, 4 = Alteracao, 5 = Exclusao )
/*/
//-------------------------------------------------------------------
Function TAF269Vld( cAlias, nRecno, nOpc, lJob )

Local aLogErro	:= {} 
Local aCampos 	:= {}
Local aInfo  	:= {}
 Local aAreaC9v	:= {}
 
Local cStatus	:= ""
Local cIdCat	:= ""
Local cCodCat	:= ""
Local cEvento	:= ""
Local cTabela 	:= ""

Local nIndice 	:= 1

Local lObrFolha	:= .F.
Local lObrTrab	:= .F.

Default cAlias	:= ""
Default nRecno	:= 1
Default nOpc	:= 1
Default lJob	:= .F.

DbselectArea('CMJ')

//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno 		:= CMJ->( Recno() )
aAreaC9v 	:= C9V->( GetArea() )

If (CMJ->CMJ_STATUS $ (' 1'))	

	// posiciono no trabalhador para garantir os dados no momento da integra��o
	C9V->(DbSetOrder(3))
	C9V->(DbSeek( CMJ->CMJ_FILIAL + CMJ->CMJ_CPF + '1' ))
	
	/*******************************
	****      infoExclusao      ****
	********************************/
	cEvento := GetADVFVal("C8E","C8E_CODIGO", xFilial("C8E")+CMJ->CMJ_TPEVEN,1,"")
	
	//tpEvento
	If Empty(CMJ->CMJ_TPEVEN)
		aAdd( aLogErro, {"CMJ_TPEVEN","000010","CMJ", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	Else
		If xValRegTab( "C8E", CMJ->CMJ_TPEVEN, 1, , @aLogErro, /*Data validacao periodo*/, { "CMJ", "CMJ_TPEVEN", nRecno })
			If !(cEvento $ "S-1200|S-1202|S-1207|S-1210|S-1250|S-1260|S-1270|S-1280|S-1300|S-2190|S-2400" .or. "S-22" $ cEvento .or. "S-23" $ cEvento)
				aAdd( aLogErro, {"CMJ_TPEVEN","001096","CMJ", nRecno }) //STR1096 - "Podem ser exclu�dos apenas os eventos relacionados a N�o peri�dicos (S-2190 a S-2400) e os Peri�dicos (S-1200 a S-1280 e S-1300)."	
			EndIf
		EndIf
	EndIf

	//nrRecEvt
	If Empty(CMJ->CMJ_NRRECI)
		aAdd( aLogErro, {"CMJ_TPEVEN","000010","CMJ", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	Else

		aInfo 		:= TAFRotinas( cEvento, 4, .F., 2 )
		cTabela		:= aInfo[3]
		nIndice 	:= aInfo[13]
		
		DbSelectArea(cTabela)
		(cTabela)->(DBSetOrder(nIndice))
		
		If (cTabela)->(DBSeek( xFilial(cTabela) +  CMJ->CMJ_NRRECI ))
			If TafColumnPos(cTabela + "_EVENTO") .and. !((cTabela)->&(cTabela + "_EVENTO") $ "I|A|F|E" )
				aAdd( aLogErro, {"CMJ_TPEVEN","001097","CMJ", nRecno }) //STR1097 - "O evento do recibo n�o deve constar como exclu�do ou retificado." 
			EndIf  
		Else
			aAdd( aLogErro, {"CMJ_TPEVEN","001098","CMJ", nRecno }) //STR1098 - "N�mero do recibo n�o encontrado no evento."
		EndIf
	
	EndIf

	/******************************
	****     ideTrabalhador    ****
	*******************************/
	lObrTrab := cEvento $ "S-1200|S-1202|S-1210|S-2190|S-2400" .OR. "S-22" $ cEvento .OR. "S-23" $ cEvento 
	
	//cpfTrab	
	If Empty(CMJ->CMJ_CPF) .AND. lObrTrab 
		AADD(aLogErro,{"CMJ_CPF","001105", "CMJ", nRecno }) //STR1105 - "O preenchimento � obrigat�rio se {tpEvento} corresponder a um dos dos eventos n�o peri�dicos (S-2190 a S-2400), ou um dos eventos peri�dicos (S-1200, S-1202 ou S-1210)."
	ElseIf !Empty(CMJ->CMJ_CPF) .AND. !lObrTrab 
		AADD(aLogErro,{"CMJ_CPF","001106", "CMJ", nRecno }) //STR1106 - "O campo n�o pode ser preenchido se {tpEvento} for diferente dos eventos n�o peri�dicos (S-2190 a S-2400), ou um dos eventos peri�dicos (S-1200, S-1202 ou S-1210)."
	ElseIf !Empty(CMJ->CMJ_CPF)
		
		If !CGC(CMJ->CMJ_CPF)
			AADD(aLogErro,{"CMJ_CPF","000646", "CMJ", nRecno }) //STR0646 - "O CPF � inv�lido."
		EndIf

	EndIf	

	//nisTrab
	If !Empty(CMJ->CMJ_NIS) .AND. (cEvento $ "S-1210|S-2190")
		AADD(aLogErro,{"CMJ_NIS","001095", "CMJ", nRecno }) //STR0195 - "Campo NIS n�o deve ser preenchido no caso de exclus�o de pagamento de rendimentos do trabalho (S-1210) ou de admiss�o preliminar (S-2190)."
	ElseIf !Empty(CMJ->CMJ_NIS) .AND. !lObrTrab 
		AADD(aLogErro,{"CMJ_NIS","001106", "CMJ", nRecno }) //STR1106 - "O campo n�o pode ser preenchido se {tpEvento} for diferente dos eventos n�o peri�dicos (S-2190 a S-2400), ou um dos eventos peri�dicos (S-1200, S-1202 ou S-1210)."
	Else
		
		//O preenchimento � obrigat�rio, exceto se o c�digo de categoria do trabalhador for igual a [901, 903, 904]. 
		If Empty(CMJ->CMJ_NIS) .AND. lObrTrab .AND. !(cEvento $ "S-2190|S-1210") .AND. !(cCodCat $ "901|903|904")
			AADD(aLogErro,{"CMJ_NIS","001068", "CMJ", nRecno }) //STR1068 - "O preenchimento do campo 'NIS' � obrigat�rio, exceto se o c�digo de categoria do trabalhador for igual a [901, 903, 904]."  
		ElseIf !Empty(CMJ->CMJ_NIS) .And. CMJ->CMJ_NIS != C9V->C9V_NIS
			AADD(aLogErro,{"CMJ_NIS","000725", "CMJ", nRecno }) //STR0725 - "Deve ser um NIS v�lido e pertencente ao trabalhador"
		EndIf
	
	EndIf
	
	/**************************
	****   ideFolhaPagto   ****
	***************************/
	lObrFolha := cEvento $ "S-1200|S-1202|S-1207|S-1210|S-1250|S-1260|S-1270|S-1280|S-1300|" 
	
	//indApuracao
	If Empty(CMJ->CMJ_INDAPU) .AND. lObrFolha 
		AADD(aLogErro,{"CMJ_INDAPU","001107", "CMJ", nRecno }) //STR1107 - "O preenchimento � obrigat�rio se {tpEvento} corresponder a um dos eventos peri�dicos (S-1200 a S-1280 e S-1300)."
	ElseIf !Empty(CMJ->CMJ_INDAPU) .AND. !lObrFolha 
		AADD(aLogErro,{"CMJ_INDAPU","001108", "CMJ", nRecno }) //STR1108 - "O campo n�o pode ser preenchido se {tpEvento} for diferente dos eventos peri�dicos (S-1200 a S-1280 e S-1300)."
	Else
	
		If !Empty(CMJ->CMJ_INDAPU) .and. !( CMJ->CMJ_INDAPU $ "1|2|" )
			AADD(aLogErro,{"CMJ_INDAPU","000006","CM6", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis "
		EndIf
	
	EndIf
	
	//perApur
	If Empty(CMJ->CMJ_PERAPU) .AND. lObrFolha
		AADD(aLogErro,{"CMJ_PERAPU","000010", "CMJ", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio." 
	ElseIf !Empty(CMJ->CMJ_PERAPU) .AND. !lObrFolha	
		AADD(aLogErro,{"CMJ_PERAPU","001108", "CMJ", nRecno }) //STR1108 - "O campo n�o pode ser preenchido se {tpEvento} for diferente dos eventos peri�dicos (S-1200 a S-1280 e S-1300)."
	Else
	
		If !Empty(CMJ->CMJ_PERAPU)
			If CMJ->CMJ_INDAPU == "1"
				If Substr(CMJ->CMJ_PERAPU,5,2) < "01" .OR. Substr(CMJ->CMJ_PERAPU,5,2) > "12" .OR. Len(AllTrim(CMJ->CMJ_PERAPU)) <> 6 //Formato MMAAAA
					AADD(aLogErro,{"CMJ_PERAPU","000318","CMJ", nRecno }) //STR0318 - "Informar o m�s/ano (formato AAAA-MM) de refer�ncia das informa��es, se {indApuracao} for igual a [1], ou apenas o ano (formato AAAA), se {indApuracao} for igual a [2]."
				EndIf
			ElseIf CMJ->CMJ_INDAPU == "2"
				If Len(AllTrim(CMJ->CMJ_PERAPU)) <> 4 //Formato AAAA
					AADD(aLogErro,{"CMJ_PERAPU","000318","CMJ", nRecno }) //STR0318 - "Informar o m�s/ano (formato AAAA-MM) de refer�ncia das informa��es, se {indApuracao} for igual a [1], ou apenas o ano (formato AAAA), se {indApuracao} for igual a [2]."
				EndIf
			EndIf
		EndIf
	
	EndIf
	
	//ATUALIZO O STATUS DO REGISTRO�
	cStatus := Iif(Len(aLogErro) > 0,"1","0")
	
	Begin Transaction 
		If RecLock("CMJ",.F.)
			CMJ->CMJ_STATUS := cStatus
			CMJ->(MsUnlock())
		EndIf
	End Transaction 

Else
	AADD(aLogErro,{"CMJ","000305", "CMJ", nRecno })   
EndIf
		
//Nao apresento o alert quando utilizo o JOB para validar�
If !lJob
	xValLogEr(aLogErro)
EndIf

RestArea(aAreaC9v)

Return(aLogErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@author Evandro dos Santos oliveira
@since 23/10/2013
@version 1.0

@param   oModel -  Modelo de dados
@return .T.
/*/
//-------------------------------------------------------------------
Static Function SaveModel(oModel)

Local cVerAnt    	:= "" 
Local cChave		:= ""  
Local cEvento	 	:= "" 
Local cAlias		:= ""
Local cNmFun		:= ""
Local cInd			:= ""
Local cSelect   	:= ""
Local cFrom   		:= ""
Local cWhere   		:= ""
Local cAliasQry		:= ""
Local cLogOpe       := ""
Local cLogOpeAnt    := ""

Local nOperation 	:= oModel:GetOperation()

Local lReturn   	:= .T.

Local aTafRotn		:= {}
Local aCampos		:= {}

Default oModel	:= Nil

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

Begin Transaction

	If nOperation == MODEL_OPERATION_INSERT
		
		TafAjustID(�"CMJ", oModel)
		
		oModel:LoadValue( 'MODEL_CMJ', 'CMJ_VERSAO', xFunGetVer() )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_CMJ', 'CMJ_LOGOPE' , '2', '' )
		Endif

		FwFormCommit( oModel )
		
		cEvento := Posicione("C8E",1,xFilial("C8E") + CMJ->CMJ_TPEVEN,"C8E_CODIGO")
		GerarExclusao(cEvento, CMJ->CMJ_NRRECI, .T.)
		
	ElseIf nOperation == MODEL_OPERATION_UPDATE

		//�����������������������������������������������������������������Ŀ
		//�Seek para posicionar no registro antes de realizar as validacoes,�
		//�visto que quando nao esta pocisionado nao eh possivel analisar   �
		//�os campos nao usados como _STATUS                                �
		//�������������������������������������������������������������������
		CMJ->( DbSetOrder( 3 ) )
		If lGoExtemp .OR. CMJ->( MsSeek( xFilial( 'CMJ' ) + CMJ->CMJ_ID + "1" ) )
	    	
			If CMJ->CMJ_STATUS $ ( "4" )
				MsgAlert(xValStrEr("000749"))
				lReturn := .F.
			
			ElseIf	CMJ->CMJ_STATUS == ( "2" )
				MsgAlert(xValStrEr("000727"))
				lReturn := .F.
			EndIf
			
			If lReturn
				If TafColumnPos( "CMJ_LOGOPE" )
					cLogOpeAnt := CMJ->CMJ_LOGOPE
				endif

				//Gravo altera��o para o Extempor�neo
				If lGoExtemp
					TafGrvExt( oModel, 'MODEL_CMJ', 'CMJ' )			
				EndIf

				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_CMJ', 'CMJ_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				FwFormCommit( oModel )
				TAFAltStat( 'CMJ', " " )
			EndIf
		EndIf
		
	ElseIf nOperation == MODEL_OPERATION_DELETE
		
		If CMJ->CMJ_STATUS $ ( "2|4" )
			Aviso( STR0002, STR0006, {STR0007}, 1 )
		Else
			oModel:DeActivate()
			oModel:SetOperation( 5 )
			oModel:Activate()
			FwFormCommit( oModel )
			
			//Restaura o registro que havia sido excluido
			cEvento  := Posicione("C8E",1,xFilial("C8E") + CMJ->CMJ_TPEVEN,"C8E_CODIGO")
			aTafRotn := TAFRotinas( cEvento ,4,.F.,2)
			
			If !Empty(aTafRotn)
				cAlias	 := aTafRotn[3]
				cNmFun	 := aTafRotn[1]
				cInd	 := aTafRotn[13]
			
				DbselectArea(cAlias)
				(cAlias)->( DbSetOrder( cInd ) )
				If (cAlias)->( MsSeek( xFilial( cAlias ) + CMJ->CMJ_NRRECI + '1'  ) )
					If &(cAlias+"->"+cAlias+"_STATUS") == "6"
						cChave := &(cAlias+"->"+cAlias+"_ID") + &(cAlias+"->"+cAlias+"_VERANT")
						oModel := FWLoadModel(cNmFun)
						oModel:SetOperation(5)
						oModel:Activate()
						FwFormCommit( oModel )
						TAFRastro(cAlias,1,cChave, .T., , IIF(Type("oBrw") == "U", Nil, oBrw))
					EndIf
				Else
					//Se a exclus�o for pelo savemodel
					cSelect  	:= ""
					cAliasQry	:= GetNextAlias()
					
					cSelect := cAlias + "_ID ID"
					cFrom	:= RetSqlName(cAlias)
					cWhere	+= cAlias + "_ATIVO = '1' "
					cWhere	+= " AND " + cAlias + "_STATUS = '6' "
					cWhere	+= " AND " + cAlias + "_PROTPN = '" + CMJ->CMJ_NRRECI + "' "
					cWhere	+= " AND D_E_L_E_T_= '' "
					
					cSelect  := "%" + cSelect  + "%"
					cFrom    := "%" + cFrom    + "%"
					cWhere   := "%" + cWhere   + "%"
					
					BeginSql Alias cAliasQry
						SELECT
							%Exp:cSelect%
						FROM
							%Exp:cFrom%
						WHERE
							%EXP:cWhere%
					EndSql
					
					(cAlias)->( DbSetOrder( 2 ) )
					If (cAlias)->( MsSeek( xFilial( cAlias ) + (cAliasQry)->ID + '1'  ) )
						cChave := &(cAlias+"->"+cAlias+"_ID") + &(cAlias+"->"+cAlias+"_VERANT")
						oModel := FWLoadModel(cNmFun)
						oModel:SetOperation(5)
						oModel:Activate()
						FwFormCommit( oModel )
						TAFRastro(cAlias,1,cChave, .T., , IIF(Type ("oBrw") == "U", Nil, oBrw ) )
					EndIf
					
				EndIf
			EndIf
		
		EndIf
		
	EndIf
	
End Transaction       
	    
Return (.T.)

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF269Grv
@author Vitor Siqueira
@since 16/11/2015
@version 1.0
		
@param
cLayout	- Nome do Layout que esta sendo enviado, existem situacoes onde o mesmo fonte
        		alimenta mais de um regsitro do E-Social, para estes casos serao necessarios
           		tratamentos de acordo com o layout que esta sendo enviado.
nOpc  		- Opcao a ser realizada ( 3 = Inclusao, 4 = Alteracao, 5 = Exclusao )
cFilEv 		- Filial do ERP para onde as informacoes deverao ser importadas
oXML   		- Objeto com as informacoes a serem manutenidas ( Outras Integracoes )  
cTAFKEY 	- Chave do registro que est� sendo exclu�do.

@Return    
lRet    - Variavel que indica se a importacao foi realizada, ou seja, se as 
		  informacoes foram gravadas no banco de dados
aIncons - Array com as inconsistencias encontradas durante a importacao 
/*/
//-------------------------------------------------------------------
Function TAF269Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID )

Local cCmpsNoUpd 		:= "|CMJ_FILIAL|CMJ_ID|CMJ_VERSAO|CMJ_VERANT|CMJ_PROTUL|CMJ_PROTPN|CMJ_EVENTO|CMJ_STATUS|CMJ_ATIVO|CMJ_TRABAL|CMJ_DTRABA|"
Local cCabec     		:= "/eSocial/evtExclusao/infoExclusao"
Local cLogOpeAnt		:= ""

//Layouts que podem ser exclu�dos atrav�s desse evento.
Local cLaysOK			:= "S-1200|S-1202|S-1207|S-1210|S-1250|S-1260|S-1270|S-1280|S-1300|S-2190|S-2200|S-2205|S-2206|S-2210|S-2220|S-2230|S-2240|S-2241|S-2250|S-2298|S-2299|S-2300|S-2306|S-2399|S-2260|S-2221|S-2245"
				
Local cValChv 			:= ""
Local cValorXml  		:= ""
Local cChave	   		:= ""
Local cRecChv	   		:= ""
Local cIdFunc	   		:= ""
Local cFldsIndex 		:= ""
Local cAlias	   		:= ""
Local cTpOper	   		:= ""
Local cIdEvento  		:= ""
Local cVersaoEvt	 	:= ""
Local cInconMsg  		:= ""
Local cQry				:= ""
Local cAliasTafKey	    := ""
Local cAliasQry		    := GetNextAlias()
Local cCmpTrab		    := ""
Local cStatus	   		:= ""
Local cTAFKEY			:= ""
Local cCodEvent  		:= ""
Local cEvento			:= ""
Local cDtAfast			:= ""
Local cIndApur 	        := ""
Local cPerApur 	        := ""
Local cBanco	 		:= Upper(TcGetDb())

Local nIndChv    		:= 2
Local nIndIDVer  		:= 1
Local nI         		:= 0
Local nSeqErrGrv		:= 0
Local nIndExc	  		:= 0
Local nIndProt		    := 0
Local nIndApp	   		:= 0
Local nTamCmp			:= 0
Local nPosChave			:= 1
Local nTamCampo			:= 0

Local lRet       		:= .F.
Local lTafKey			:= .F.
Local lRecibo			:= .F.
Local lPadProtu		    := .T. 

Local aIncons    		:= {}
Local aRules     		:= {}
Local aChave     		:= {}
Local aFldsIndex 		:= {}
Local aArea      		:= GetArea()
Local aTafRotn   		:= {}
Local cEvtExclu			:= ""
Local cChaveEvt		 	:= ""

Local oModel     		:= Nil

Private oDados   		:= oXML
Private lVldModel		:= .T. //Caso a chamada seja via integracao seto a variavel de controle de validacao como .T.

Default cLayout 		:= ""
Default nOpc     		:= 1
Default cFilEv   		:= ""
Default oXML     		:= Nil
Default cOwner			:= ""
Default cFilTran		:=	""
Default cPredeces		:=	""
Default nTafRecno		:=	0
Default cComplem		:=	""
Default cGrpTran		:=	""
Default cEmpOriGrp		:=	""
Default cFilOriGrp		:=	""
Default cXmlID			:=	""


cTpOper		:= TAFIdNfe(oDados:Save2String(),"tpOper") 
cEvento  	:= FTafGetVal( cCabec + "/tpEvento", "C", .F., @aIncons, .F. )
aTafRotn 	:= TAFRotinas( cEvento,4,.F.,2)
cCodEvent  	:= Posicione("C8E",2,xFilial("C8E")+"S-"+cLayout,"C8E->C8E_ID")
//A informa��o enviada em nrRecEvt pode ser o protocolo do registro que deseja excluir ou
//a chave do registro ( alternativa de integra��o do TAF com o ERP de origem n�o tem o protocolo )
cRecChv := oDados:XPathGetNodeValue( cCabec + "/nrRecEvt")

If !Empty(AllTrim(cRecChv))
	
	If !Empty(aTafRotn)
		
		cAlias		:= aTafRotn[ 3 ]
		cNmFun		:= aTafRotn[ 1 ]
		nIndProt	:= aTafRotn[ 13 ]
		nIndApp		:= aTafRotn[ 10 ]
		cCmpTrab	:= aTafRotn[ 11 ]
		
		dbSelectArea( cAlias )
		
		//Primeiro tentar encontrar o registro pelo indice e chave de protocolo.
		( cAlias )->( dbSetOrder( nIndProt ) )
		
		If ( cAlias )->( MsSeek( xFilial( cAlias ) +Padr( cRecChv, TamSx3( cAlias + "_PROTUL" )[1] )  + '1' ) )
			
			cIdEvento	:= ( cAlias )->&( cAlias + "_ID" )
			cVersaoEvt 	:= ( cAlias )->&( cAlias + "_VERSAO" )
			cStatus		:= ( cAlias )->&( cAlias + "_STATUS" )
			nIndExc		:= nIndProt
			lRecibo		:= .T.
		
		//Se n�o encontrou com o protocolo, tenta encontrar pelo indice e chave de negocio ( alternativa de integra��o do TAF com o ERP de origem n�o tem o protocolo )
		Else
	
			If oDados:xPathHasNode(cCabec+"/tpEvento")
				cEvtExclu := oDados:XPathGetNodeValue(cCabec+"/tpEvento")
			EndIf
		
			( cAlias )->( dbSetOrder( nIndApp ) )
		
			cFldsIndex := ( cAlias )->( IndexKey() )
			cFldsIndex := StrTran( cFldsIndex	, "DTOS("		, "" )
			cFldsIndex := StrTran( cFldsIndex	, "STR("		, "" )
			cFldsIndex := StrTran( cFldsIndex	, "DESCEND("	, "" )
			cFldsIndex := StrTran( cFldsIndex	, ")"			, "" )
			aFldsIndex := Str2Arr( cFldsIndex 	, "+" )
				
			For nI:= 1 To Len( aFldsIndex )

				If !(cEvtExclu $ 'S-2205|S-2206') .And. (aFldsIndex[ nI ] == ( cAlias + "_IDTRAB" ) .or. aFldsIndex[ nI ] == ( cAlias + "_TRABAL" ) .or. aFldsIndex[ nI ]==( cAlias + "_FUNC" ) .or.;
						aFldsIndex[ nI ]==( cAlias + "_CPF" ))
					
					cIdFunc := FGetIdInt( "cpfTrab/Recibo" , "matricula/Recibo" , cCabec + "/ideTrabalhador/cpfTrab" , cCabec + "/nrRecEvt",,,,,,,,,,,,@nPosChave)
					cChaveEvt += cIdFunc

				Else

					If !(aFldsIndex[nI] == (cAlias+"_FILIAL") .Or. aFldsIndex[nI] == (cAlias+"_ATIVO"))

						nTamCampo := GetSx3Cache(aFldsIndex[nI],"X3_TAMANHO")
						
						If aFldsIndex[ nI ] == cAlias + "_NOMEVE"
							cChaveEvt += StrTran( cEvento , "-" , "" )
						Else
							cChaveEvt += Substr(cRecChv,nPosChave,nTamCampo)
						EndIf 
						nPosChave += nTamCampo
					EndIf 
				EndIf
				
				If !aFldsIndex[ nI ] == cAlias + "_PROTUL"
					lPadProtu := .F.
				Else
					lPadProtu := .T.
				EndIf
			Next

			If !Empty(cChaveEvt) .And. ( cAlias )->( msSeek( xFilial( cAlias ) + cChaveEvt + '1' ) ) 
				cIdEvento	:= ( cAlias )->&( cAlias + "_ID" )
				cVersaoEvt := ( cAlias )->&( cAlias + "_VERSAO" )
				cStatus	:= ( cAlias )->&( cAlias + "_STATUS" )
				nIndExc	:= nIndApp
				cRecChv := cChaveEvt
			EndIf
			
			If Empty(cIdEvento) //Caso seja enviado o TAFKEY na tag de recibo(nrRecEvt)
				
				cTAFKEY := FTafGetVal( cCabec + "/nrRecEvt", "C", .F., @aIncons, .F. )
				
				cQry += "SELECT * FROM TAFXERP TAFXERP "
				cQry += "	WHERE TAFALIAS = '" + cAlias + "'"

				If cBanco == "ORACLE"
					cQry += "   AND TAFXERP.TAFKEY IN ( '" + Padr(cTAFKEY, 100) + "' ) "
				Else
					cQry += "   AND TAFXERP.TAFKEY IN ( '" + cTAFKEY + "' ) "
				EndIf

				cQry += "   AND TAFXERP.TAFRECNO <> '0' "
				cQry += "   AND TAFXERP.D_E_L_E_T_ = '' "
				cQry += "   ORDER BY R_E_C_N_O_ DESC"
				
				cQry := ChangeQuery(cQry)
				
				dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQry ) , cAliasQry, .F., .T. )

				If !Empty((cAliasQry)->TAFALIAS)
					cAliasTafKey := (cAliasQry)->TAFALIAS
								
					(cAliasTafKey)->(dbGoTo((cAliasQry)->TAFRECNO))
					
					cIdEvento	:= ( cAliasTafKey )->&( cAliasTafKey + "_ID" )
					cVersaoEvt := ( cAliasTafKey )->&( cAliasTafKey + "_VERSAO" )
					cStatus		:= ( cAliasTafKey )->&( cAliasTafKey + "_STATUS" )
					cRecChv		:= ( cAliasTafKey )->&( cAliasTafKey + "_PROTUL" )
					nIndExc		:= nIndApp
					lTafKey		:= .T.
					
					If Empty(cRecChv) //Se protocolo estiver vazio
						cRecChv := ''
						For nI:= 1 To Len( aFldsIndex )
							If aFldsIndex[nI] <> (cAliasTafKey + "_FILIAL") .AND. aFldsIndex[nI] <> (cAliasTafKey + "_ATIVO")
								cTipoCmp := GetSx3Cache(aFldsIndex[nI],"X3_TIPO")
								If cTipoCmp == "D"
									nTamCmp := TamSx3(aFldsIndex[nI])[1] + 2
									cRecChv += DTOS(( cAliasTafKey) -> &( aFldsIndex[nI] ))
								Else
									cRecChv += Padr( ( cAliasTafKey) -> &( aFldsIndex[nI] ), TamSx3(aFldsIndex[nI])[1] )
								EndIf
							EndIf
						Next nI
					EndIf
				
				EndIf
								
			EndIf
		
		EndIf
	
	EndIf
	
	If !Empty( cIdEvento )

		cValChv := FGetIdInt( "tpEvento" , , cCabec + "/tpEvento" , , , , @cInconMsg , @nSeqErrGrv )

		If Empty( cValChv )
			
			If !Empty( cInconMsg )
				aAdd( aIncons , cInconMsg )
			EndIf
		
		ElseIf ( cEvento $ cLaysOK ) .And. ( nOpc <> 5 )// .And. cStatus == "4"
		
			//Quando o Evento que deseja excluir estiver com status diferente de '2' = Aguardando Retorno, '4' = Transmitido '6' = Pendente Transmiss�o S-3000 e 
			//'7' = Transmiss�o S-3000 com sucesso, devo realizar uma exclus�o direta
			If !( cStatus $ '4|2|6|7' )
				
				//Utilizo a vari�vel aIncons para fazer o controle de retorno para TAFPrepInt. Apesar de n�o se tratar de uma inconsist�ncia ou erro
				// essa vari�vel auxilia no processo de exclus�o direta de registros no TAF atrav�s do Evento S-3000.
				//Atrav�s dela consigo identificar essa opera��o e setar o Status '5' na TAFXERP ( exclus�o direta via S-3000 )
				GerarExclusao( cEvento , cRecChv , lPadProtu , @aIncons , nIndExc  )
			
			//Quando o Evento que deseja excluir estiver com status '2' = Aguardando Retorno
			ElseIf cStatus == '2'
				
				aAdd( aIncons , '000025' ) //'N�o � permitido a integra��o deste evento, enquanto outro estiver pendente de transmiss�o.'
			
			Else
				/*----------------------------
				CAMPOS DA CHAVE
				-----------------------------*/
				If !Empty( cValChv )
					Aadd( aChave, { "C", "CMJ_TPEVEN", cValChv, .T. } )
					cChave	+= Padr( cValChv, Tamsx3( aChave[ 1, 2 ])[1] )
					nIndChv := 2
				EndIf
				
				cValChv := FTafGetVal( cCabec + "/nrRecEvt", "C", .F., @aIncons, .F. )

				If !Empty( cValChv )
					Aadd( aChave, { "C", "CMJ_NRRECI", cValChv, .T.} )
					cChave += Padr( cValChv, Tamsx3( aChave[ 2, 2 ])[1] )
					nIndChv := 2
				EndIf
				
				("CMJ")->( DbSetOrder( 2 ) )
				If ("CMJ")->( MsSeek( xFilial("CMJ") + cChave + "1" ) )
					nOpc := 4
				EndIf
				RestArea(aArea)
				
				cAliasEvent := TafSelecEvt("U",cAlias,cIdEvento,cVersaoEvt)
				          	
				Begin Transaction
					
					//�������������������������������������������������������������Ŀ
					//�Funcao para validar se a operacao desejada pode ser realizada�
					//���������������������������������������������������������������
					If FTafVldOpe( "CMJ", nIndChv, @nOpc, cFilEv, @aIncons, aChave, @oModel, "TAFA269", cCmpsNoUpd, nIndIDVer, .F.)

						If TafColumnPos( "CMJ_LOGOPE" )
							cLogOpeAnt := CMJ->CMJ_LOGOPE
						endif

						//����������������������������������������������������������������Ŀ
						//�Quando se tratar de uma Exclusao direta apenas preciso realizar �
						//�o Commit(), nao eh necessaria nenhuma manutencao nas informacoes�
						//������������������������������������������������������������������
						If nOpc <> 5


							If (cAliasEvent)->(!Eof())

								//���������������������������������������������������������������Ŀ
								//�Carrego array com os campos De/Para de gravacao das informacoes�
								//�����������������������������������������������������������������
								aRules := TAF269Rul(nOpc, cCodEvent, cOwner, cAlias, nIndProt, cRecChv)
								oModel:LoadValue( "MODEL_CMJ", "CMJ_FILIAL", xFilial("CMJ"))

								If TAFColumnPos( "CMJ_XMLID" )
									oModel:LoadValue( "MODEL_CMJ", "CMJ_XMLID", cXmlID )
								EndIf			

							
								While (cAliasEvent)->(!EOF())
									//����������������������������������������Ŀ
									//�Rodo o aRules para gravar as informacoes�
									//������������������������������������������
									For nI := 1 To Len( aRules )
										If aRules[ nI, 01 ] == "CMJ_NRRECI"
											cValorXml := (cAliasEvent)->(RECIBO)
										Else
											cValorXml := FTafGetVal( aRules[ nI, 02 ], aRules[nI, 03], aRules[nI, 04], @aIncons, .F., , aRules[ nI, 01 ] )
										EndIf
										oModel:LoadValue("MODEL_CMJ", aRules[ nI, 01 ], cValorXml)
									Next
									(cAliasEvent)->(dbSkip())
								Enddo

								If Findfunction("TAFAltMan")
									if nOpc == 3
										TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_CMJ', 'CMJ_LOGOPE' , '1', '' )
									elseif nOpc == 4
										TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_CMJ', 'CMJ_LOGOPE' , '', cLogOpeAnt )
									EndIf
								EndIf
							Else
								If cAlias == "CM6"
									cInconMsg := "Existe um termino de afastamento pendente de envio. Realize a exclus�o do termino para excluir o seu respectivo inicio."								
								EndIf
							EndIf

						EndIf
						
						//���������������������������Ŀ
						//�Efetiva a operacao desejada�
						//�����������������������������	
						If Empty(cInconMsg) .And. Empty(aIncons)
							If TafFormCommit ( oModel )
								Aadd(aIncons, "ERRO19")
							Else
								lRet  := .T.
							EndIf
						Else
							Aadd (aIncons, cInconMsg)
							DisarmTransaction()
						EndIf
		 
				       //N�o gera registro de exclus�o no evento
						If nOpc == 3
							If lTafKey
								GerarExclusao(cEvento, cRecChv,,,nIndProt)
							Else
								GerarExclusao(cEvento, cRecChv, lRecibo)
							EndIf
						EndIf
						
						oModel:DeActivate()
						If FindFunction('TafClearModel')
							TafClearModel(oModel)
						EndIf
					
					EndIf
				
				End Transaction
				
				//����������������������������������������������������������Ŀ
				//�Zerando os arrays e os Objetos utilizados no processamento�
				//������������������������������������������������������������
				aSize( aRules, 0 )
				aRules     := Nil
				
				aSize( aChave, 0 )
				aChave     := Nil
			EndIf
		EndIf
	Else
		lRet := .F.
		aAdd( aIncons , STR0012 + cEvento) //'Recibo/chave  n�o encontrado(a) no cadastro do evento '
	EndIf
Else
	lRet := .F.
	aAdd( aIncons ,STR0011)//'Para eventos S-3000 � obrigat�rio o envio da tag  nrRecEvt contendo o numero do recibo ou a chave do registro a ser excluido'
EndIf
			
Return { lRet, aIncons }    

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF269Rul           
Regras para gravacao das informacoes do registro S-3000

@param nOpc - N�mero da opera��o

@Return	
aRull  - Regras para a gravacao das informacoes

@author Vitor Siqueira
@since 17/11/2015
@version 1.0

/*/                        	
//-------------------------------------------------------------------
Static Function TAF269Rul( nOpc, cCodEvent, cOwner, cAliasExc, nIndExc, cRecChv )	

Local aArea     := GetArea()
Local cCabec	:= "/eSocial/evtExclusao/infoExclusao"
Local cIndApu	:= ""
Local cPerApu	:= ""
Local cApu		:= ""
Local nRegRef   := 0
Local aRull		:= {}
Local aIncons	:= {} 
Local cChvSeek  := ""

Default nOpc 		:= 1
Default cCodEvent	:= ""
Default cOwner		:= ""
Default cAliasExc   := ""
Default nIndExc     := 0
Default cRecChv     := ""

If TafXNode( oDados, cCodEvent, cOwner, (cCabec + "/ideFolhaPagto/indApuracao"))
	cIndApu	:= FTafGetVal( cCabec + "/ideFolhaPagto/indApuracao", 'C', .F., @aIncons, .F. )
EndIf

If TafXNode( oDados, cCodEvent, cOwner, (cCabec + "/ideFolhaPagto/perApur"))
	cPerApu	:= FTafGetVal( cCabec + "/ideFolhaPagto/perApur", 'C', .F., @aIncons, .F.)
EndIf

If cIndApu == '1' 
	cApu := substr(cPerApu,1,4) + substr(cPerApu,6,7)
ElseIf cIndApu == '2'
	cApu := substr(cPerApu,1,4) 
EndIf 

If nOpc == 3
	If TafXNode( oDados, cCodEvent, cOwner, (cCabec + "/nrRecEvt"))
		aAdd( aRull,{ "CMJ_NRRECI" , cCabec + "/nrRecEvt", "C", .F. } )	// Recibo
	EndIf
	
	If TafXNode( oDados, cCodEvent, cOwner, (cCabec + "/tpEvento"))
		aAdd( aRull,{ "CMJ_TPEVEN" , FGetIdInt( "tpEvento", ,cCabec + "/tpEvento",,,,), "C", .T. } ) 	// Tipo do evento
	EndIf
EndIf

If TafXNode( oDados, cCodEvent, cOwner, (cCabec + "/ideTrabalhador/cpfTrab"))
	aAdd( aRull,{ "CMJ_CPF"    , cCabec + "/ideTrabalhador/cpfTrab", "C", .F. } ) 	// CPF do trabalhador
EndIf

If TafXNode( oDados, cCodEvent, cOwner, (cCabec + "/ideTrabalhador/nisTrab"))
	aAdd( aRull,{ "CMJ_NIS"    , cCabec + "/ideTrabalhador/nisTrab", "C", .F. } ) 	// NIS do trabalhador
EndIf

aAdd( aRull,{ "CMJ_INDAPU" , cIndApu, "C", .T. } ) 	// Ind. Per. Apura��o 
aAdd( aRull,{ "CMJ_PERAPU" , cApu   , "C", .T. } ) 	// Per. Apura��o

If !Empty(cAliasExc) .And. nIndExc > 0
	
	DbselectArea(cAliasExc)
	(cAliasExc)->( DbSetOrder( nIndExc ) )
	
	cChvSeek := xFilial( cAliasExc ) + Padr( cRecChv, TamSx3( cAliasExc + "_PROTUL" )[1] )
	
	If cAliasExc <> "CM6"
		cChvSeek += '1'
	EndIf
		
	If (cAliasExc)->( MsSeek( cChvSeek ) )
	
		nRegRef := (cAliasExc)->(Recno())
		
	EndIf
	
EndIf

aAdd( aRull,{ "CMJ_REGREF" , nRegRef, "N", .T. } )  // Registro Refer�ncia

RestArea( aArea )
			 
Return ( aRull ) 

//-------------------------------------------------------------------
/*/{Protheus.doc} GerarExclusao()          
Function que gera o registro de exclus�o para um determinado evento
 
@param cEvento - Tipo de evento a ser excluido
@param cRecChv - Chave que pode ser o protocolo ou a chave do evento
@param lSaveModel - 
@param aIncons - Array auxiliar de inconsistencias
@param nIndex - Indice para busca do registro
@Return	

@author Vitor Siqueira
@since 03/12/2015
@version 1.0

/*/                        	
//-------------------------------------------------------------------
Function GerarExclusao( cEvento, cRecChv, lSaveModel, aIncons, nIndex, lPadProtu )

Local cVerAnt		:= "" 
Local cAlias		:= ""
Local cNmFun		:= ""

Local nInd			:= 0
Local lReturn   	:= .T.
Local aTafRotn 		:= TAFRotinas( cEvento ,4,.F.,2)

Private nRecno		:= 0

Default cEvento		:= ""
Default cRecChv		:= ""	
Default lSaveModel	:= .F.
Default aIncons		:= {}
Default nIndex		:= 0
Default lPadProtu	:= .T.

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

If !Empty(aTafRotn)
	cAlias	 := aTafRotn[3]
	cNmFun	 := aTafRotn[1]
	
	//tratamento para quando o �ndice � enviado na chamada da fun��o e n�o precisa ser pesquisado no TAFRotinas()
	If nIndex > 0
		nInd	 := nIndex
	Else 
		nInd	 := IIf ( lSaveModel , aTafRotn[ 13 ] , aTafRotn[ 10 ] )
	EndIf
	
	DbselectArea(cAlias)
	(cAlias)->( DbSetOrder( nInd ) )
	If lGoExtemp .OR. (cAlias)->( MsSeek( xFilial( cAlias ) + Iif(lSaveModel,Padr( cRecChv, TamSx3( cAlias + "_PROTUL" )[1] ),cRecChv) + '1' ) )
		
		nRecno := (cAlias)->(Recno())	
		oModel := FWLoadModel(cNmFun)
		
		If ( cAlias )->&( cAlias + "_STATUS" ) == '4'
			oModel:SetOperation( 4 )
			oModel:Activate()
			&(�"StaticCall( "�+�cNmFun�+�", GerarEvtExc , oModel, nRecno ,.T. )"�)
		Else
			oModel:SetOperation( 5 )
			oModel:Activate()
			FwFormCommit( oModel )
			TAFRastro( cAlias , nInd , cRecChv , .T. , .F.,  IIF(Type ("oBrw") == "U", Nil, oBrw) )
			
			//Utilizo a vari�vel aIncons para fazer o controle de retorno para TAFPrepInt. Apesar de n�o se tratar de uma inconsist�ncia ou erro
			// essa vari�vel auxilia no processo de exclus�o direta de registros no TAF atrav�s do Evento S-3000.
			//Atrav�s dela consigo identificar essa opera��o e setar o Status '5' na TAFXERP ( exclus�o direta via S-3000 )
			aAdd( aIncons , '000026' )
		EndIf
		
		oModel:DeActivate()
		If FindFunction('TafClearModel')
			TafClearModel(oModel)
		EndIf
	EndIf

EndIf				    		
			 
Return (.T.)  

//-------------------------------------------------------------------
/*/{Protheus.doc} xSelecEvent()          
Seleciona os registros a serem exclu�dos
 
@param cTpOper - Tipo de Opera��o de Exclus�o
				   T = Todo Hist�rico
				   U = Ultimo Registro
				   
@param cAlias - Alias da Tabela do Evento que deve ser exclu�do
@param cIdEvento - Id do evento que deve ser exclu�do

@Return	

@author Paulo Santana
@since 03/12/2015
@version 1.0

/*/                        	
//-------------------------------------------------------------------
Static Function TafSelecEvt( cTpOper, cAlias, cIdEvento,cVersaoEvt)

Local cSelect  	:= ""
Local cAliasQry	:= GetNextAlias()

Default cTpOper	:= "U"
Default cAlias	:= ""
Default cIdEvento	:= ""

cSelect:= cAlias + "_PROTUL RECIBO"
cFrom	:= RetSqlName(cAlias)
cWhere	:= cAlias + "_FILIAL = '" +  xFilial(cAlias) + "'"
cWhere  += " AND " + cAlias + "_ID = '" 		+ cIdEvento  + "'"
cWhere  += " AND " + cAlias +  "_VERSAO = '" 	+ cVersaoEvt + "'"  

cWhere  += " AND D_E_L_E_T_='' "
/*
If cTpOper == "U"
	cWhere	+= " AND " + cAlias + "_ATIVO = '1'"
EndIf
*/
cSelect  := "%" + cSelect  + "%"
cFrom    := "%" + cFrom    + "%"
cWhere   := "%" + cWhere   + "%"

BeginSql Alias cAliasQry
	SELECT
		%Exp:cSelect%
	FROM
		%Exp:cFrom%
	WHERE
		%EXP:cWhere%
EndSql
			 
Return (cAliasQry)  

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFAlw3000
Indica os Eventos permitidos para utiliza��o do Evento S-3000.

@Return		cRet - Eventos permitidos para utiliza��o do Evento S-3000

@Author		Felipe C. Seolin
@Since		07/12/2017
@Version	1.0
/*/
//---------------------------------------------------------------------
Function TAFAlw3000()

Local cPeriod		:=	"S-1200|S-1202|S-1207|S-1210|S-1250|S-1260|S-1270|S-1280|S-1300|"
Local cNaoPeriod	:=	"S-2190|S-2200|S-2205|S-2206|S-2210|S-2220|S-2230|S-2240|S-2241|S-2250|S-2260|S-2298|S-2299|S-2300|S-2306|S-2399|S-2400|"
Local cRet			:=	cPeriod + cNaoPeriod

Return( cRet )

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFEvt3000
Verifica se o Evento pertence aos Eventos permitido pelo S-3000.

@Param		cEvento	-	Evento a ser verificado

@Return		lRet	-	Indica se o Evento � permitido ao S-3000

@Author		Felipe C. Seolin
@Since		07/12/2017
@Version	1.0
/*/
//---------------------------------------------------------------------
Function TAFEvt3000( cEvento )

Local cAllowed	:=	TAFAlw3000()
Local lRet		:=	.F.

Default cEvento	:=	""

lRet := Posicione( "C8E", 1, xFilial( "C8E" ) + cEvento, "C8E_CODIGO" ) $ cAllowed

Return( lRet )

//---------------------------------------------------------------------
/*/{Protheus.doc} TAF269Ajust
Rotina para ajuste de Status dos registros Excluidos.
Issue:
DSERTAF1-3419
* Retirar essa rotina quando expedir o release 12.1.21

@Author		Evandro dos Santos Oliveira
@Since		22/03/2018
@Version	1.0
/*/
//---------------------------------------------------------------------
Function TAF269Ajust()

	Local aEvtEsocial := {}
	Local nX := 0 
	Local cQuery := ""
	Local cTipEvt := ""
	Local cAliasEvt := ""
	Local cAliasQry := ""
	Local cLayout := ""
	Local cMsg := ""
	Local nQtdAjus := 0

	aEvtEsocial := TAFRotinas(,,.T.,2)
	
	ProcRegua(Len(aEvtEsocial))

	BEGIN TRANSACTION 

	For nX := 1 To Len(aEvtEsocial)

		cTipEvt   := aEvtEsocial[nX][12] 
		cAliasEvt := aEvtEsocial[nX][3] 
		cLayout   := aEvtEsocial[nX][4] 

		IncProc("Analisando/Ajustando Evento " + cLayout) 

		If cTipEvt $ "EM"

			If !Empty(cLayout) .And. cAliasEvt != "CMJ"

				cAliasQry := GetNextAlias()

				cQuery := " SELECT CMJ_NRRECI "
				cQuery += ", " + cAliasEvt + "_PROTPN "
				cQuery += ", " + cAliasEvt + "_FILIAL "
				cQuery += ", " + cAliasEvt + "_ID "
				cQuery += ", " + cAliasEvt + "_VERSAO "
				cQuery += ", " + cAliasEvt + "_STATUS "
				cQuery += ", " + cAliasEvt + "_ATIVO "
				cQuery += ", " + cAliasEvt + ".R_E_C_N_O_ RECNO"
				
				cQuery += " FROM " + RetSqlName("CMJ") + " CMJ " 
				cQuery += " LEFT JOIN " + RetSqlName(cAliasEvt) + " " + cAliasEvt 
				cQuery += " ON CMJ_NRRECI = " + cAliasEvt + "_PROTPN AND " + cAliasEvt + "_PROTPN != ' ' "
				cQuery += " WHERE " + cAliasEvt + ".D_E_L_E_T_ = ' ' AND CMJ.D_E_L_E_T_ = ' ' "

				If cAliasEvt $ "C9V|C91"
					cQuery += " AND " + cAliasEvt + "_NOMEVE = '" + StrTran(cLayout,"-","") + "'"
				EndIf

//				cQuery += " AND CMJ.CMJ_FILIAL = '" + xFilial("CMJ") + "' AND " + cAliasEvt + "." + cAliasEvt + "_FILIAL = '" + xFilial(cAliasEvt) + "'"
				cQuery += " AND CMJ.CMJ_STATUS = '4' "

				TcQuery cQuery New Alias (cAliasQry)

				While (cAliasQry)->(!Eof())

					(cAliasEvt)->(dbGoto((cAliasQry)->RECNO))

					If &(cAliasEvt + "->" + cAliasEvt+"_STATUS") != '7' .Or. &(cAliasEvt + "->" + cAliasEvt+"_ATIVO") == '1'

						ConOut("Corre��o Status Exclusao- Found " + cAliasEvt + " - RecnO: " + AllTrim(Str((cAliasQry)->RECNO)))

						RecLock(cAliasEvt,.F.)
						&(cAliasEvt+"_STATUS") := '7'
						&(cAliasEvt+"_ATIVO") := '2'

						(cAliasEvt)->(MsUnlock())
						nQtdAjus++

                         If (cAliasEvt) $ "CM6"
                            (cAliasEvt)->(DbSetOrder(4))
                            If  (cAliasEvt)-> (DbSeek(&(cAliasEvt + "->" + (cAliasEvt+"_FILIAL + "+cAliasEvt+"_PROTPN"))))                      
                                (cAliasEvt)->(DbSetOrder(4))
                                If (cAliasEvt)-> (DbSeek(&(cAliasEvt + "->" + (cAliasEvt+"_FILIAL + "+cAliasEvt+"_PROTPN"))))
                                    RecLock(cAliasEvt,.F.)
                                    &(cAliasEvt+"_ATIVO") := '1'
                                    (cAliasEvt)->(MsUnlock())
                                EndIf
                            EndIf
                        EndIf
					EndIf

					(cAliasQry)->(dbSkip())
				EndDo

				(cAliasQry)->(dbCloseArea())
			EndIf  

		ElseIf !Empty(cLayout) .And.cTipEvt == "C"

			cAliasQry := GetNextAlias()

			cQuery := " SELECT R_E_C_N_O_ RECNO "
 			cQuery += " FROM " + RetSqlName(cAliasEvt) + " " +  cAliasEvt
  			cQuery += " WHERE " + cAliasEvt + "_EVENTO = 'E' "
 			cQuery += " AND " + cAliasEvt + "_STATUS = '4' " 
  			cQuery += " AND " + cAliasEvt + "_PROTUL != ' ' "
  			cQuery += "	AND " + cAliasEvt + "_ATIVO = '1' "
  			cQuery += " AND " + cAliasEvt + "_FILIAL = '" + xFilial(cAliasEvt) + "'"
  			cQuery += " AND D_E_L_E_T_ = ' ' "

			TcQuery cQuery New Alias (cAliasQry)

			While (cAliasQry)->(!Eof())

				ConOut("Corre��o Status Exclusao - Found " + cAliasEvt + " - Tipo de Evento: " + cTipEvt +" - RecnO: " + AllTrim(Str((cAliasQry)->RECNO)))

				(cAliasEvt)->(dbGoto((cAliasQry)->RECNO))

				RecLock(cAliasEvt,.F.)
				&(cAliasEvt+"_ATIVO") := '2'
				(cAliasEvt)->(MsUnlock())

				(cAliasQry)->(dbSkip())
				nQtdAjus++

			EndDo

			(cAliasQry)->(dbCloseArea())

		EndIf 

	Next nX

	END TRANSACTION

	If nQtdAjus > 0 
		cMsg := "Realizado ajuste em " + AllTrim(Str(nQtdAjus)) + " Registro(os)."
	Else
		cMsg := "N�o foram encontrados registos com Status Incorretos."
	EndIf 

	MsgInfo(cMsg)

Return .T. 

//---------------------------------------------------------------------
/*/{Protheus.doc} TAF269CPFAjust
Rotina para ajuste do CPNJ/NIS dos eventos que tem relacao com trabalhador

@Author		Evandro dos Santos Oliveira
@Since		18/07/2018
@Version	1.0
/*/
//---------------------------------------------------------------------
Function TAF269CPFAjust()

	Local aEvtEsocial := {}
	Local nX := 0 
	Local cQuery := ""
	Local cTipEvt := ""
	Local cAliasEvt := ""
	Local cAliasQry := ""
	Local cLayout := ""
	Local cMsg := ""
	Local cCmpTrab := ""
	Local nQtdAjus := 0

	aEvtEsocial := TAFRotinas(,,.T.,2)
	
	ProcRegua(Len(aEvtEsocial))

	BEGIN TRANSACTION 

	For nX := 1 To Len(aEvtEsocial)

		cTipEvt   := aEvtEsocial[nX][12] 
		cAliasEvt := aEvtEsocial[nX][3] 
		cLayout   := aEvtEsocial[nX][4] 
		cCmpTrab  := aEvtEsocial[nX][11]

		IncProc("Analisando/Ajustando Evento " + cLayout) 

		If cTipEvt $ "EM"

			If !Empty(cLayout) .And. !(cAliasEvt $ "CMJ|T5T") .And. !Empty(cCmpTrab) 

				cAliasQry := GetNextAlias()

				cQuery := " SELECT CMJ_NRRECI "
				cQuery += ", C9V_CPF "
				cQuery += ", C9V_NIS "
				cQuery += ", CMJ.R_E_C_N_O_ RECNOCMJ "
				
				cQuery += " FROM " + RetSqlName("CMJ") + " CMJ " 
				cQuery += " LEFT JOIN " + RetSqlName(cAliasEvt) + " " + cAliasEvt 
				cQuery += " ON CMJ_NRRECI = " + cAliasEvt + "_PROTUL AND " + cAliasEvt + "_PROTUL != ' ' "

				If cAliasEvt != "C9V"
					cQuery += " INNER JOIN " + RetSqlName("C9V") + " C9VA ON C9VA.C9V_FILIAL = " + cAliasEvt + "_FILIAL AND C9VA.C9V_ID = " + cCmpTrab 
					cQuery += " AND C9VA.D_E_L_E_T_ = ' ' "
				EndIf 

				cQuery += " WHERE " + cAliasEvt + ".D_E_L_E_T_ = ' ' AND CMJ.D_E_L_E_T_ = ' ' "

				If cAliasEvt $ "C9V|C91"
					cQuery += " AND " + cAliasEvt + "_NOMEVE = '" + StrTran(cLayout,"-","") + "'"
				EndIf

				//cQuery += " AND CMJ.CMJ_FILIAL = '" + xFilial("CMJ") + "' AND " + cAliasEvt + "." + cAliasEvt + "_FILIAL = '" + xFilial(cAliasEvt) + "'"
				cQuery += " AND CMJ.CMJ_STATUS != '4' "

				TcQuery cQuery New Alias (cAliasQry)

				dbSelectArea("CMJ")
				CMJ->(dbSetOrder(1))

				//Nao precisa controlar transa�ao

				While (cAliasQry)->(!Eof())

					CMJ->(dbGoto((cAliasQry)->RECNOCMJ))

						ConOut("Corre��o CPF/NIS - Found " + cAliasEvt + " - Recno: " + AllTrim(Str((cAliasQry)->RECNOCMJ)))

						CMJ->(RecLock("CMJ",.F.))
						CMJ->CMJ_CPF := (cAliasQry)->C9V_CPF

						If cLayout == "S-1210"
							CMJ->CMJ_NIS := " "
						Else
							CMJ->CMJ_NIS := (cAliasQry)->C9V_NIS
						EndIf 

						CMJ->(MsUnlock())
						nQtdAjus++
	
					(cAliasQry)->(dbSkip())
				EndDo

				(cAliasQry)->(dbCloseArea())
			EndIf  
		EndIf 
	Next nX

	END TRANSACTION

	If nQtdAjus > 0 
		cMsg := "Realizado verifica��o  em " + AllTrim(Str(nQtdAjus)) + " Registro(os)."
	Else
		cMsg := "N�o foram encontrados registos com Status de Inconsist�ncia (Status - 3)."
	EndIf 

	MsgInfo(cMsg)

Return .T. 
