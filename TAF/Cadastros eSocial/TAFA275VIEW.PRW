#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFA275VIEW.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@Return oView - Objeto da View MVC

@author Felipe C. Seolin / Rodrigo Aguilar
@since 23/10/2013
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function ViewDef()

Local cCmpFil   := ""
Local nI        := 0
Local lSelFunc  := .F.
Local lBtnOk    := .F.
Local aCmpGrp   := {}
Local oModel    := FWLoadModel( "TAFA275" )
Local oView     := FWFormView():New()
Local oStruC9Y  := FWFormStruct( 2, "C9Y" )
Local oStruC9Va := Nil
Local oStruC9Vb := Nil
Local oStruC9Vc := Nil
Local oStruC9Vd := Nil
Local oStruC9Ve := Nil

//Seto o Model que ser� utilizado no processamento
oView:SetModel( oModel )

oView:SetCloseOnOk( { || .T. } )

/*-----------------------------------------------------------------------------------
							Estrutura da View do Trabalhador
-------------------------------------------------------------------------------------*/
cGrpTra1  := "C9V_ID|C9V_VERSAO|C9V_VERANT|C9V_PROTPN|C9V_EVENTO|C9V_ATIVO|C9V_CPF|C9V_DTALT|"
cGrpTra2  := "C9V_NIS|C9V_NOME|C9V_SEXO|C9V_RCCOR|C9V_ESTCIV|C9V_GRINST|C9V_DGRINS|"
cGrpTra3  := "C9V_NRCTPS|C9V_SERCTP|C9V_UFCTPS|C9V_DUFCTP|"
cGrpTra4  := "C9V_NRRIC|C9V_OREMRI|C9V_DTEXRI|"
cGrpTra5  := "C9V_NRRG|C9V_OREMRG|C9V_DTEMRG|"
cGrpTra6  := "C9V_NRRNE|C9V_OREMRN|C9V_DTEMRN|"
cGrpTra7  := "C9V_NUMOC|C9V_OREMOC|C9V_DTEXOC|C9V_DTVLOC|"
cGrpTra8  := "C9V_NRCNH|C9V_DTEXCN|C9V_UFCNH|C9V_DUFCN|C9V_DTVLCN|C9V_DTPCNH|C9V_CATCNH|"
cGrpTra9  := "C9V_PAIS|C9V_DPAIS|C9V_UF|C9V_DUF|C9V_MUN|C9V_DMUN|C9V_NOMCID|C9V_TPLOGR|C9V_DTPLOG|C9V_LOGRAD|C9V_NRLOG|C9V_COMLOG|C9V_BAIRRO|C9V_CEP|"
cGrpTra10 := "C9V_APOSEN|"

cCmpFil   := cGrpTra1 + cGrpTra2 + cGrpTra3 + cGrpTra4 + cGrpTra5 + cGrpTra6 + cGrpTra7 + cGrpTra8 + cGrpTra9 + cGrpTra10
oStruC9Va := FwFormStruct( 2, "C9V",{ |x| AllTrim( x ) + "|" $ cCmpFil } ) //Campos do folder Informacoes do Trabalhador

cCmpFil   := "C9V_DTCHEG|C9V_CCTRAE|C9V_DCONTE|C9V_CASBRA|C9V_FILBRA|"
oStruC9Vb := FwFormStruct( 2, "C9V",{|x| AllTrim( x ) + "|" $ cCmpFil } ) //Campos do folder Informacoes de Trabalhador Estrangeiro

cCmpFil   := "C9V_DEFINT|C9V_DEFFIS|C9V_DEFMOT|C9V_DEFVIS|C9V_DEFAUD|C9V_REABIL|C9V_DEFMEN|C9V_OBSDEF|"
oStruC9Vc := FwFormStruct( 2, "C9V",{|x| AllTrim( x ) + "|" $ cCmpFil } ) //Campos do folder Informacoes de Deficiencias

If TafColumnPos("C9V_DDIFPR")
	cCmpFil   := "C9V_DDIFPR|C9V_DDDFPR|C9V_FONPRC|C9V_DDIFAL|C9V_DDDFAL|C9V_FONALT|C9V_EMAILP|C9V_EMAILA|"
Else
	cCmpFil   := "C9V_DDDFPR|C9V_FONPRC|C9V_DDDFAL|C9V_FONALT|C9V_EMAILP|C9V_EMAILA|"
EndIf

oStruC9Vd := FwFormStruct( 2, "C9V",{|x| AllTrim( x ) + "|" $ cCmpFil } ) //Campos do folder Informa��es de Contato

cCmpFil   := "C9V_PROTUL|"
oStruC9Ve := FwFormStruct( 2, "C9V",{|x| AllTrim( x ) + "|" $ cCmpFil } ) //Campos do folder Protocolo de Transmiss�o

If TafColumnPos("C9V_DDIFPR")
	oStruC9Vd:SetProperty( "C9V_DDIFPR", MVC_VIEW_ORDEM	, "71"	)
	oStruC9Vd:SetProperty( "C9V_DDIFAL", MVC_VIEW_ORDEM	, "73"	)
EndIf

/*-----------------------------------------------------------------------------------
							Grupo de campos do Trabalhador
-------------------------------------------------------------------------------------*/
oStruC9Va:AddGroup( "GRP_TRABALHADOR_01", STR0022, "", 1 ) //Identifica��o do Trabalhador / Data da Altera��o das Informa��es
oStruC9Va:AddGroup( "GRP_TRABALHADOR_02", STR0001, "", 1 ) //Dados do Trabalhador
oStruC9Va:AddGroup( "GRP_TRABALHADOR_03", STR0016, "", 1 ) //Informa��es da Carteira de Trabalho
oStruC9Va:AddGroup( "GRP_TRABALHADOR_04", STR0017, "", 1 ) //Informa��es do Documento Nacional de Identidade (DNI)
oStruC9Va:AddGroup( "GRP_TRABALHADOR_05", STR0018, "", 1 ) //Informa��es do Registro Geral (RG)
oStruC9Va:AddGroup( "GRP_TRABALHADOR_06", STR0019, "", 1 ) //Informa��es do Registro Nacional de Estrangeiro
oStruC9Va:AddGroup( "GRP_TRABALHADOR_07", STR0020, "", 1 ) //Informa��es do n�mero de registro em �rg�o de Classe (OC)
oStruC9Va:AddGroup( "GRP_TRABALHADOR_08", STR0021, "", 1 ) //Informa��es da Carteira Nacional de Habilita��o (CNH)
oStruC9Va:AddGroup( "GRP_TRABALHADOR_09", STR0004, "", 1 ) //Endere�o
oStruC9Va:AddGroup( "GRP_TRABALHADOR_10", STR0014, "", 1 ) //Informa��es sobre Aposentadoria

aCmpGrp := StrToKArr(cGrpTra1,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC9Va:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_01")
Next nI

aCmpGrp := StrToKArr(cGrpTra2,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC9Va:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_02")
Next nI

aCmpGrp := StrToKArr(cGrpTra3,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC9Va:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_03")
Next nI

aCmpGrp := StrToKArr(cGrpTra4,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC9Va:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_04")
Next nI

aCmpGrp := StrToKArr(cGrpTra5,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC9Va:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_05")
Next nI

aCmpGrp := StrToKArr(cGrpTra6,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC9Va:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_06")
Next nI

aCmpGrp := StrToKArr(cGrpTra7,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC9Va:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_07")
Next nI

aCmpGrp := StrToKArr(cGrpTra8,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC9Va:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_08")
Next nI

aCmpGrp := StrToKArr(cGrpTra9,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC9Va:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_09")
Next nI

aCmpGrp := StrToKArr(cGrpTra10,"|")
For nI := 1 to Len(aCmpGrp)
	oStruC9Va:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_TRABALHADOR_10")
Next nI

If FindFunction("TafAjustRecibo")
	TafAjustRecibo(oStruC9Ve,"T1U")
EndIf 

/*-----------------------------------------------------------------------------------
							Esrutura da View
-------------------------------------------------------------------------------------*/
oView:AddField( "VIEW_C9Va", oStruC9Va, "MODEL_C9V" )
oView:AddField( "VIEW_C9Vb", oStruC9Vb, "MODEL_C9V" )
oView:AddField( "VIEW_C9Vc", oStruC9Vc, "MODEL_C9V" )
oView:AddField( "VIEW_C9Vd", oStruC9Vd, "MODEL_C9V" )
oView:AddField( "VIEW_C9Ve", oStruC9Ve, "MODEL_C9V" )

If FindFunction("TafNmFolder")
	oView:EnableTitleView( 'VIEW_C9Ve', TafNmFolder("recibo",1) ) // "Recibo da �ltima Transmiss�o" 
EndIf 

oView:AddGrid( "VIEW_C9Y", oStruC9Y, "MODEL_C9Y" )
oView:AddIncrementField( "VIEW_C9Y", "C9Y_IDDEP" )

/*-----------------------------------------------------------------------------------
							Estrutura do Folder
-------------------------------------------------------------------------------------*/
oView:CreateHorizontalBox("PAINEL_PRINCIPAL",100)
oView:CreateFolder("FOLDER_PRINCIPAL","PAINEL_PRINCIPAL")

//////////////////////////////////////////////////////////////////////////////////

oView:AddSheet("FOLDER_PRINCIPAL","ABA01",STR0007) //Trabalhador

If FindFunction("TafNmFolder")
	oView:AddSheet("FOLDER_PRINCIPAL","ABA02",TafNmFolder("recibo") )   //"Numero do Recibo"
Else
	oView:AddSheet("FOLDER_PRINCIPAL","ABA02",STR0015)//"Protocolo de Transmiss�o" 
EndIf 

oView:CreateHorizontalBox("C9Ve",100,,,"FOLDER_PRINCIPAL","ABA02")

//////////////////////////////////////////////////////////////////////////////////

oView:CreateHorizontalBox("PAINEL_TRABALHADOR",100,,,"FOLDER_PRINCIPAL","ABA01")
oView:CreateFolder("TRABALHADOR","PAINEL_TRABALHADOR")

oView:AddSheet("TRABALHADOR","ABA01",STR0008) //Informa��es do Trabalhador
oView:CreateHorizontalBox("C9Va",100,,,"TRABALHADOR","ABA01")

oView:AddSheet("TRABALHADOR","ABA02",STR0009) //Informa��es de Trabalhador Estrangeiro
oView:CreateHorizontalBox("C9Vb",100,,,"TRABALHADOR","ABA02")

oView:AddSheet("TRABALHADOR","ABA03",STR0010) //Informa��es Defici�ncia
oView:CreateHorizontalBox("C9Vc",100,,,"TRABALHADOR","ABA03")

oView:AddSheet("TRABALHADOR","ABA04",STR0011) //Dependentes
oView:CreateHorizontalBox("C9Y" ,100,,,"TRABALHADOR","ABA04")

oView:AddSheet("TRABALHADOR","ABA05",STR0005) //Informa��es de Contato
oView:CreateHorizontalBox("C9Vd",100,,,"TRABALHADOR","ABA05")

	
/*-----------------------------------------------------------------------------------
							Amarra��o para exibi��o das informa��es
-------------------------------------------------------------------------------------*/
oView:SetOwnerView( "VIEW_C9Va", "C9Va" )
oView:SetOwnerView( "VIEW_C9Vb", "C9Vb" )
oView:SetOwnerView( "VIEW_C9Vc", "C9Vc" )
oView:SetOwnerView( "VIEW_C9Vd", "C9Vd" )
oView:SetOwnerView( "VIEW_C9Ve", "C9Ve" )
oView:SetOwnerView( "VIEW_C9Y", "C9Y"  )

lPainel := Iif( Type( "lPainel" ) == "U", .F., lPainel )

If !lPainel
	aButtons := {}
	aAdd( aButtons , { 1 , .T. , { | o | nOpca := 1 , lBtnOk := .T., o:oWnd:End() } } )
	aAdd( aButtons , { 2 , .T. , { | o | nOpca := 2 , lBtnOk := .F., o:oWnd:End() } } )
	PergTAF( 'TAFA275' , STR0012, { STR0013 }, aButtons, {|| FVldFunc( @lExistAlt, lBtnOk ) } ) //"Inclus�o da Altera��o de Funcion�rios"#"Informe o CPF do trabalhador a ser alterado:"

	If !lExistAlt
		//Cancela a ativa��o da view.
		oView:SetViewCanActivate( { || .F. } )
	EndIf
Else
	lExistAlt := .T.
EndIf

//Tratamento para que alguns campos de controle interno n�o sejam exibidos na tela
lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif
	xFunRmFStr( @oStruC9Va, "C9V" )
EndIf

Return( oView )