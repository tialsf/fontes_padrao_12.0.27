#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFA276VIEW.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View Auxiliar criado para a inclus�o de altera��o contratual ( TAFA275 )

@return oView - Objeto da View MVC

@author Rodrigo Aguilar
@since 29/01/2014
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function ViewDef()

Local cGrpVinc1 := ""
Local cGrpVinc2 := ""
Local cGrpVinc4 := ""
Local cGrpVinc5 := ""
Local cGrpVinc9 := ""
Local cCmpFil   := ""
Local cGrpC9V   := ""
Local nI        := 0

Local lBtnOk    := .F.
Local aCmpGrp   := {}


Local oModel    := FWLoadModel( "TAFA276" )
Local oView     := FWFormView():New()
Local oStruCRQ  := FWFormStruct( 2, "CRQ" )
Local oStruCUP	:= Nil
Local oStruC9V  := Nil

oView:SetModel( oModel )

cGrpC9V := "C9V_DTALT|C9V_CPF|C9V_NIS|"
oStruC9V := FwFormStruct( 2, "C9V",{|x| AllTrim( x ) + "|" $ cGrpC9V } )

oStruC9V:AddGroup( "GRP_DTALT", STR0001, "", 1 ) //"Informa��es de Altera��o"

aCmpGrp := StrToKarr( cGrpC9V, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruC9V:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_DTALT" )
Next nI

cGrpVinc1 := "CUP_MATRIC|"
cGrpVinc2 := "CUP_TPREGT|CUP_TPREGP|"
cGrpVinc3 := "CUP_TPREGJ|CUP_NATATV|CUP_DATAB|CUP_CNPJCP|"
cGrpVinc4 := "CUP_CODCGO|CUP_DCODCG|CUP_CODFUN|CUP_DCODFU|CUP_CODCAT|CUP_DCODCA|"
cGrpVinc5 := "CUP_VLSLFX|CUP_UNSLFX|CUP_DESSVR|"
cGrpVinc6 := "CUP_TPCONT|CUP_DTTERM|"
cGrpVinc7 := "CUP_TPINSC|CUP_NRINSC|CUP_DESLOT|"
cGrpVinc8 := "CUP_TPLOGD|CUP_DSCTPL|CUP_DELOGD|CUP_NRLOGD|CUP_COMLGD|CUP_BAIRTD|CUP_CEPLTD|CUP_CMUNTD|CUP_DSCMTD|CUP_UFTRBD|CUP_DUFTRD|"
cGrpVinc9 := "CUP_QTDHJS|CUP_TPJORN|CUP_DTPJOR|"
cGrpVinc10 := "CUP_CNPJSD|"
cGrpVinc11 := "CUP_ALVJUD|CUP_DALVJU|"


cGrpVinc1 += cGrpVinc2 + cGrpVinc3 +cGrpVinc4 + cGrpVinc5 + cGrpVinc6 + cGrpVinc7 + cGrpVinc8 + cGrpVinc9 + cGrpVinc10 + cGrpVinc11
oStruCUP := FwFormStruct( 2, "CUP",{|x| AllTrim( x ) + "|" $ cGrpVinc1 } )

oStruCUP:AddGroup( "GRP_VINCULO_01", STR0002, "", 1 ) //"Identifica��o do Trabalhador"
oStruCUP:AddGroup( "GRP_VINCULO_02", STR0007, "", 1 ) //"Informa��es do Trabalhador e do V�nculo" 
oStruCUP:AddGroup( "GRP_VINCULO_03", STR0015, "", 1 ) //"Informa��es de Trabalhador Celetista" 
oStruCUP:AddGroup( "GRP_VINCULO_04", STR0018, "", 1 ) //"Informa��es do Contrato de Trabalho"
oStruCUP:AddGroup( "GRP_VINCULO_05", STR0004, "", 1 ) //"Remunera��o" 
oStruCUP:AddGroup( "GRP_VINCULO_06", STR0003, "", 1 ) //"Dura��o"
oStruCUP:AddGroup( "GRP_VINCULO_07", STR0006, "", 1 ) //"Informa��es do Local de Trabalho"
oStruCUP:AddGroup( "GRP_VINCULO_08", STR0014, "", 1 ) //"Informa��es do Local de Trabalho Dom�stico" 
oStruCUP:AddGroup( "GRP_VINCULO_09", STR0009, "", 1 ) //"Informa��es do Hor�rio Contratual do Trabalhador"	
oStruCUP:AddGroup( "GRP_VINCULO_10", STR0016, "", 1 ) //"Filia��o Sindical do Trabalhador"
oStruCUP:AddGroup( "GRP_VINCULO_11", STR0017, "", 1 ) //"Informa��es do Alvar� Judicial"

aCmpGrp := StrToKarr( cGrpVinc1, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_01" )
Next nI

aCmpGrp := StrToKarr( cGrpVinc2, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_02" )
Next nI

aCmpGrp := StrToKarr( cGrpVinc3, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_03" )
Next nI

aCmpGrp := StrToKarr( cGrpVinc4, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_04" )
Next nI

aCmpGrp := StrToKarr( cGrpVinc5, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_05" )
Next nI

aCmpGrp := StrToKarr( cGrpVinc6, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_06" )
Next nI

aCmpGrp := StrToKarr( cGrpVinc7, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_07" )
Next nI

aCmpGrp := StrToKarr( cGrpVinc8, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_08" )
Next nI

aCmpGrp := StrToKarr( cGrpVinc9, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_09" )
Next nI

aCmpGrp := StrToKarr( cGrpVinc10, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_10" )
Next nI

aCmpGrp := StrToKarr( cGrpVinc11, "|" )
For nI := 1 to Len( aCmpGrp )
	oStruCUP:SetProperty( aCmpGrp[nI], MVC_VIEW_GROUP_NUMBER, "GRP_VINCULO_11" )
Next nI


oView:AddField( "VIEW_CUP", oStruCUP, "MODEL_CUP" )
oView:AddField( "VIEW_C9V", oStruC9V, "MODEL_C9V" )
oView:AddGrid( "VIEW_CRQ", oStruCRQ, "MODEL_CRQ" )

oView:CreateHorizontalBox( "PAINEL_PRINCIPAL", 100 )
oView:CreateFolder( "FOLDER_PRINCIPAL", "PAINEL_PRINCIPAL" )
	
oView:AddSheet( "FOLDER_PRINCIPAL", "ABA01", STR0007 ) //"V�nculo - Informa��es do Contrato " 	

oView:CreateHorizontalBox( "C9V", 10, , , "FOLDER_PRINCIPAL" , "ABA01" )	 		
oView:CreateHorizontalBox( "CUP", 70, , , "FOLDER_PRINCIPAL" , "ABA01" )

oView:CreateHorizontalBox( "GRIDS", 20, , , "FOLDER_PRINCIPAL", "ABA01" )	
oView:CreateFolder( "FOLDER_PRINCIPAL_VINCULO" , "GRIDS" )


oView:AddSheet( "FOLDER_PRINCIPAL_VINCULO", 'ABA02', STR0019 ) //"Informa��es Di�rias do Hor�rio Contratual"
oView:CreateHorizontalBox( 'CRQ', 100,,, "FOLDER_PRINCIPAL_VINCULO", 'ABA02' )

oView:SetOwnerView( "VIEW_CUP", "CUP" )
oView:SetOwnerView( "VIEW_C9V", "C9V" )

oView:SetOwnerView( "VIEW_CRQ", "CRQ" )

lPainel := Iif( Type( "lPainel" ) == "U", .F., lPainel )

If !lPainel
	aButtons := {}
	aAdd( aButtons , { 1 , .T. , { | o | nOpca := 1 , lBtnOk := .T., o:oWnd:End() } } )
	aAdd( aButtons , { 2 , .T. , { | o | nOpca := 2 , lBtnOk := .F., o:oWnd:End() } } )
	PergTAF( 'TAFA276' ,  STR0012, { STR0013 }, aButtons, {|| FVldFunc( @lExistAlt, lBtnOk ) } ) //"Inclus�o da Altera��o de Funcion�rios"#"Informe o CPF do trabalhador a ser alterado:"
	
	If !lExistAlt
			// Cancela a ativa��o da view.
				oView:SetViewCanActivate( { || .F. } )
	EndIf

Else
	lExistAlt := .T.
EndIf

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

Return( oView )