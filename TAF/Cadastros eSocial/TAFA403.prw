#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFA403.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA403
Cadastro de Admiss�o Preliminar (S-2190)

@author Vitor Henrique
@since 23/10/2015
@version 1.0
/*/
//-------------------------------------------------------------------
Function TAFA403()

Private oBrw := FWmBrowse():New()

If TafAtualizado()

	If FindFunction("TAFSetEpoch")
		TAFSetEpoch()
	EndIf
	
	oBrw:SetDescription(STR0001) //"Admiss�o Preliminar"
	oBrw:SetAlias( "T3A" )
	oBrw:SetMenuDef( "TAFA403" )

	If FindFunction('TAFSetFilter')
		oBrw:SetFilterDefault(TAFBrwSetFilter("T3A","TAFA403","S-2190"))
	Else
		oBrw:SetFilterDefault( "T3A_ATIVO == '1'" ) //Filtro para que apenas os registros ativos sejam exibidos ( 1=Ativo, 2=Inativo )
	EndIf

	TafLegend(2,"T3A",@oBrw)

	oBrw:Activate()

EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Vitor Henrique
@since 23/10/2015
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao := {}
Local aRotina := {}

If FindFunction('TafXmlRet')
	Aadd( aFuncao, { "" , "TafxmlRet('TAF403Xml','2190','T3A')" 									, "1" } )
Else
	Aadd( aFuncao, { "" , "TAF403Xml" 									, "1" } )
EndIf 
Aadd( aFuncao, { "" , "Taf403Vld" 																, "2" } )

//Chamo a Browse do Hist�rico
//If FindFunction( "xNewHisAlt" )
//	Aadd( aFuncao, { "" , "xNewHisAlt( 'T3A', 'TAFA403' )" , "3" } )
//Else
//	Aadd( aFuncao, { "" , "xFunHisAlt( 'T3A', 'TAFA403' )" , "3" } )
//EndIf

aAdd( aFuncao, { "" , "TAFXmlLote( 'T3A', 'S-2190' , 'evtAdmPrelim' , 'TAF403Xml',, oBrw )" 	, "5" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

Aadd( aFuncao, { "" , "xFunAltRec( 'T3A' )" , "10" } )

If lMenuDif
	ADD OPTION aRotina Title "Visualizar" Action 'VIEWDEF.TAFA403' OPERATION 2 ACCESS 0

	// Menu dos extempor�neos
	If FindFunction( "xNewHisAlt" ) .AND. FindFunction( "xTafExtmp" ) .And. xTafExtmp()
		aRotina	:= xMnuExtmp( "TAFA403", "T3A" )
	EndIf

Else
	aRotina	:=	xFunMnuTAF( "TAFA403" , , aFuncao)
EndIf

Return( aRotina )

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC

@author Vitor Henrique
@since 23/10/2015
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

Local oStruT3A 	:= FWFormStruct( 1, 'T3A' )
Local oModel   	:= MPFormModel():New( 'TAFA403' , , , {|oModel| SaveModel( oModel ) } )

lVldModel 	:= Iif( Type( "lVldModel" ) == "U", .F., lVldModel )

If lVldModel
	oStruT3A:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
EndIf

//Remo��o do GetSX8Num quando se tratar da Exclus�o de um Evento Transmitido.
//Necess�rio para n�o incrementar ID que n�o ser� utilizado.
If Upper( ProcName( 2 ) ) == Upper( "GerarExclusao" )
	oStruT3A:SetProperty( "T3A_ID", MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD, "" ) )
EndIf

oModel:AddFields('MODEL_T3A', /*cOwner*/, oStruT3A)
oModel:GetModel('MODEL_T3A'):SetPrimaryKey({'T3A_FILIAL', 'T3A_CPF', 'T3A_DTADMI'})

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Vitor Henrique
@since 23/10/2015
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oModel   := FWLoadModel( 'TAFA403' )
Local oStruT3A := FWFormStruct( 2, 'T3A' )
Local oView    := FWFormView():New()

oStruT3A :RemoveField("T3A_ID")

oView:SetModel( oModel )
oView:AddField( 'VIEW_T3A', oStruT3A, 'MODEL_T3A' )

oView:EnableTitleView( 'VIEW_T3A', STR0001 )    //"Admiss�o Preliminar"
oView:CreateHorizontalBox( 'FIELDST3A', 100 )
oView:SetOwnerView( 'VIEW_T3A', 'FIELDST3A' )

If FindFunction("TafAjustRecibo")
	TafAjustRecibo(oStruT3A,"T3A")
EndIf

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif .OR. ( FindFunction( "xTafExtmp" ) .And. xTafExtmp() )
	xFunRmFStr(@oStruT3A, 'T3A')
EndIf

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF403Grv
Funcao de gravacao para atender o registro S-2190 (Tabela de Admiss�o Preliminar)

@parametros:
cLayout - Nome do Layout que esta sendo enviado, existem situacoes onde o mesmo fonte
          alimenta mais de um regsitro do E-Social, para estes casos serao necessarios
          tratamentos de acordo com o layout que esta sendo enviado.
nOpc   -  Opcao a ser realizada ( 3 = Inclusao, 4 = Alteracao, 5 = Exclusao )
cFilEv -  Filial do ERP para onde as informacoes deverao ser importadas
oXML   -  Objeto com as informacoes a serem manutenidas ( Outras Integracoes )

@Return
lRet    - Variavel que indica se a importacao foi realizada, ou seja, se as
		  informacoes foram gravadas no banco de dados
aIncons - Array com as inconsistencias encontradas durante a importacao 

@author Vitor Henrique
@since 04/10/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Function TAF403Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID ) //Function TAF403Grv( cLayout, nOpc, cFilEv, oXML )

Local cTagOper   	:= ""
Local cCabec     	:= "/eSocial/evtAdmPrelim/infoRegPrelim"
Local cCmpsNoUpd 	:= "|T3A_FILIAL|T3A_ID|T3A_VERSAO|T3A_VERANT|T3A_PROTUL|T3A_PROTPN|T3A_STATUS|T3A_ATIVO|"
Local cValChv    	:= ""
Local cNewDtIni  	:= ""
Local cNewDtFin  	:= ""
Local cValorXml  	:= ""
Local cChave	   	:= ""
Local cInconMsg  	:= ""
Local nIndChv    	:= 2
Local nIndIDVer  	:= 1
Local nI         	:= 0
Local lRet       	:= .F.
Local aIncons    	:= {}
Local aRules     	:= {}
Local aChave     	:= {}
Local aArea      	:= GetArea()
Local oModel     	:= Nil

Private oDados   	:= oXML
Private lVldModel	:= .T. //Caso a chamada seja via integracao seto a variavel de controle de validacao como .T.

/*
Default cLayout 	:= ""
Default nOpc     	:= 1
Default cFilEv   	:= ""
Default oXML     	:= Nil
*/

Default cLayout		:=	""
Default nOpc		:=	1
Default cFilEv		:=	""
Default oXML		:=	Nil
Default cOwner		:=	""
Default cFilTran	:=	""
Default cPredeces	:=	""
Default nTafRecno	:=	0
Default cComplem	:=	""
Default cGrpTran	:=	""
Default cEmpOriGrp	:=	""
Default cFilOriGrp	:=	""
Default cXmlID		:=	""

cTagOper   	:= ""
cCabec     	:= "/eSocial/evtAdmPrelim/infoRegPrelim"
cCmpsNoUpd 	:= "|T3A_FILIAL|T3A_ID|T3A_VERSAO|T3A_VERANT|T3A_PROTUL|T3A_PROTPN|T3A_STATUS|T3A_ATIVO|"
cValChv    	:= ""
cNewDtIni  	:= ""
cNewDtFin  	:= ""
cValorXml  	:= ""
cChave	   		:= ""
cInconMsg  	:= ""
cLogOpeAnt 	:= ""

nIndChv    	:= 2
nIndIDVer  	:= 1
nI         	:= 0

lRet       	:= .F.

aIncons    	:= {}
aRules     	:= {}
aChave     	:= {}
aArea      	:= GetArea()

oModel     	:= Nil

oDados 		:= oXML
lVldModel  	:= .T. //Caso a chamada seja via integracao seto a variavel de controle de validacao como .T.

/*---------------------------------------------------------
Verificar se o cpf foi informado para a chave
---------------------------------------------------------*/
cValChv := FTafGetVal( cCabec + "/cpfTrab", 'C', .F., @aIncons, .F., '', '' )

If !Empty( cValChv )
	Aadd( aChave, { "C", "T3A_CPF", cValChv, .T.} )
	cChave	+= Padr( cValChv, Tamsx3( aChave[ 1, 2 ])[1] )
EndIf

/*---------------------------------------------------------
Verificar se a data de admissao foi informada para a chave
---------------------------------------------------------*/
cValChv := FTafGetVal( cCabec + "/dtAdm", 'C', .F., @aIncons, .F., '', '' )
cValChv := StrTran( cValChv, "-", "" )
cValChv := Substr(cValChv, 1, 4) + Substr(cValChv, 5,2) + Substr(cValChv, 7,2)

If !Empty( cValChv )
	Aadd( aChave, { "C", "T3A_DTADMI", cValChv, .T. } )
	cChave += Padr( cValChv, Tamsx3( aChave[ 2, 2 ])[1] )
	nIndChv := 2 //T3A_FILIAL+T3A_CPF+DTOS(T3A_DTADMI)+T3A_ATIVO 
EndIf

/*---------------------------------------------------------
Verifica se o evento n�o foi transmitido
---------------------------------------------------------*/
DbSelectArea("T3A")
T3A->(DbSetOrder(nIndChv)) //T3A_FILIAL+T3A_CPF+DTOS(T3A_DTADMI)+T3A_ATIVO 
If T3A->( MsSeek(FTafGetFil(cFilEv,@aIncons,"T3A") + cChave + '1' ) )
	If !T3A->T3A_STATUS $ ( "2|4|6" )
		nOpc := 4
	EndIf

	RestArea(aArea)
EndIf

Begin Transaction

	//---------------------------------------------------------------
	//Funcao para validar se a operacao desejada pode ser realizada
	//---------------------------------------------------------------
	If FTafVldOpe( "T3A", nIndChv, @nOpc, cFilEv, @aIncons, aChave, @oModel, "TAFA403", cCmpsNoUpd, nIndIDVer, .F. )

		If TafColumnPos( "T3A_LOGOPE" )
			cLogOpeAnt := T3A->T3A_LOGOPE
		endif

		/*---------------------------------------------------------
		/Quando se tratar de uma Exclusao direta apenas preciso
		realizar o Commit(), nao eh necessaria nenhuma manutencao
		nas informacoes
		---------------------------------------------------------*/
		If nOpc <> 5

			/*---------------------------------------------------------
			Carrego array com os campos De/Para de gravacao das informacoes
			---------------------------------------------------------*/
			aRules := TAF403Rul( cTagOper )

		   	oModel:LoadValue( "MODEL_T3A", "T3A_FILIAL", xFilial("T3A"))

			/*Implementa��o para o projeto de migra��o TAF x Smart E-Social e Outros Softwares x TAF*/ 

			If TAFColumnPos( "T3A_XMLID" )
				oModel:LoadValue( "MODEL_T3A", "T3A_XMLID", cXmlID )
			EndIf

			/*---------------------------------------------------------
			Rodo o aRules para gravar as informacoes
			---------------------------------------------------------*/
			For nI := 1 To Len( aRules )

				cValorXml := FTafGetVal( aRules[ nI, 02 ], aRules[nI, 03], aRules[nI, 04], @aIncons, .F., , aRules[ nI, 01 ] )
			 	oModel:LoadValue("MODEL_T3A", aRules[ nI, 01 ], cValorXml)

			Next

			If Findfunction("TAFAltMan")
				if nOpc == 3
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_T3A', 'T3A_LOGOPE' , '1', '' )
				elseif nOpc == 4
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_T3A', 'T3A_LOGOPE' , '', cLogOpeAnt )
				EndIf
			EndIf
		EndIf

		/*---------------------------------------------------------
		Efetiva a operacao desejada
		---------------------------------------------------------*/
		If Empty(cInconMsg) .And. Empty(aIncons)
			If TafFormCommit( oModel )
				Aadd(aIncons, "ERRO19")
			Else
				lRet := .T.
			EndIf
		Else
			Aadd(aIncons, cInconMsg)
			DisarmTransaction()
		EndIf

		oModel:DeActivate()
		If FindFunction('TafClearModel')
			TafClearModel(oModel)
		EndIf
	EndIf

End Transaction

/*---------------------------------------------------------
Zerando os arrays e os Objetos utilizados no processamento
---------------------------------------------------------*/
aSize( aRules, 0 )
aRules     := Nil

aSize( aChave, 0 )

aChave     := Nil

Return { lRet, aIncons }

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF403Rul
Regras para gravacao das informacoes do registro S-1040 do E-Social

@Param
cTagOper - Qual a estrutura de Tags que serao lidas para
gravacao das informacoes

@Return
aRull  - Regras para a gravacao das informacoes

@author Vitor Henrique
@since 23/10/2015
@version 1.0

/*/
//-------------------------------------------------------------------

Static Function TAF403Rul( cTagOper )

Local aRull  := {}
Local cCabec := "/eSocial/evtAdmPrelim/infoRegPrelim"

If oDados:XPathHasNode(cCabec + "/cpfTrab")
	aAdd( aRull, { "T3A_CPF"    , cCabec + "/cpfTrab"   , "C", .F. } ) 		//codFuncao
EndIf

If oDados:XPathHasNode(cCabec + "/dtNascto")
	aAdd( aRull, { "T3A_DTNASC" , cCabec + "/dtNascto"  , "D", .F. } ) 		//iniValid
EndIf

If oDados:XPathHasNode(cCabec + "/dtAdm")
	aAdd( aRull, { "T3A_DTADMI" , cCabec + "/dtAdm"     , "D", .F. } ) 		//fimValid
EndIf

Return( aRull )

//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@Param  oModel -> Modelo de dados

@Return .T.

@Author Vitor Henrique Ferreira
@Since 04/10/2013
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel(oModel)

Local cVerAnt   	:= ""
Local cProtocolo	:= ""
Local cVersao   	:= ""
Local cEvento		:= ""
Local cChvRegAnt	:= ""
Local cLogOpeAnt	:= ""
Local cAliasQry 	:= GetNextAlias()
Local nOperation	:= oModel:GetOperation()
Local nlI		   	:= 0
Local nlY   	   	:= 0
Local lRetorno  	:= .T.
Local lExecAltMan	:= .F.
Local aGrava    	:= {}
Local aCampos   	:= {}
Local oModelT3A 	:= Nil
//Controle se o evento � extempor�neo
Local lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

Begin Transaction
	
	If nOperation == MODEL_OPERATION_INSERT

	TafAjustID( "T3A", oModel)

		oModel:LoadValue( 'MODEL_T3A', 'T3A_VERSAO', xFunGetVer() )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_T3A', 'T3A_LOGOPE' , '2', '' )
		Endif

		FwFormCommit( oModel )

	ElseIf nOperation == MODEL_OPERATION_UPDATE

		/*---------------------------------------------------------
		Seek para posicionar no registro antes de realizar as
		validacoes visto que quando nao esta pocisionado nao eh
		possivel analisar os campos nao usados como _STATUS
		---------------------------------------------------------*/
		T3A->( DbSetOrder( 3 ) )
		If lGoExtemp .OR. T3A->( MsSeek( xFilial( 'T3A' ) + FwFldGet('T3A_ID') + '1' ) )

			If TafColumnPos( "T3A_LOGOPE" )
				cLogOpeAnt := T3A->T3A_LOGOPE
			EndIf

			//Gravo altera��o para o Extempor�neo
			If lGoExtemp

				//��������������������������������Ŀ
				//�Se o registro ja foi transmitido�
				//����������������������������������
			  	If T3A->T3A_STATUS $ ( "4" )

					oModelT3A := oModel:GetModel( 'MODEL_T3A' )

					//�����������������������������������������������������������Ŀ
					//�Busco a versao anterior do registro para gravacao do rastro�
					//�������������������������������������������������������������
					cVerAnt   	:= oModelT3A:GetValue( "T3A_VERSAO" )
					cProtocolo	:= oModelT3A:GetValue( "T3A_PROTUL" )
					cEvento	:= oModelT3A:GetValue( "T3A_EVENTO" )

					//�����������������������������������������������������������������Ŀ
					//�Neste momento eu gravo as informacoes que foram carregadas       �
					//�na tela, pois neste momento o usuario ja fez as modificacoes que �
					//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
					//�nao devem ser consideradas neste momento                         �
					//�������������������������������������������������������������������
					For nlI := 1 To 1
						For nlY := 1 To Len( oModelT3A:aDataModel[ nlI ] )
							Aadd( aGrava, { oModelT3A:aDataModel[ nlI, nlY, 1 ], oModelT3A:aDataModel[ nlI, nlY, 2 ] } )
						Next
					Next

					//�����������������������������������������������������������Ŀ
					//�Seto o campo como Inativo e gravo a versao do novo registro�
					//�no registro anterior                                       �
					//|                                                           |
					//|ATENCAO -> A alteracao destes campos deve sempre estar     |
					//|abaixo do Loop do For, pois devem substituir as informacoes|
					//|que foram armazenadas no Loop acima                        |
					//�������������������������������������������������������������
					FAltRegAnt( 'T3A', '2' )

					//��������������������������������������������������Ŀ
					//�Neste momento eu preciso setar a operacao do model�
					//�como Inclusao                                     �
					//����������������������������������������������������
					oModel:DeActivate()
					oModel:SetOperation( 3 )
					oModel:Activate()

					//�������������������������������������������������������Ŀ
					//�Neste momento eu realizo a inclusao do novo registro ja�
					//�contemplando as informacoes alteradas pelo usuario     �
					//���������������������������������������������������������
					For nlI := 1 To Len( aGrava )
						oModel:LoadValue( 'MODEL_T3A', aGrava[ nlI, 1 ], aGrava[ nlI, 2 ] )
					Next

					//Necess�rio Abaixo do For Nao Retirar
					If Findfunction("TAFAltMan")
						TAFAltMan( 4 , 'Save' , oModel, 'MODEL_T3A', 'T3A_LOGOPE' , '' , cLogOpeAnt )
						lExecAltMan := .T.
					EndIf

					//�������������������������������Ŀ
					//�Busco a versao que sera gravada�
					//���������������������������������
					cVersao := xFunGetVer()

					//�����������������������������������������������������������Ŀ
					//|ATENCAO -> A alteracao destes campos deve sempre estar     |
					//|abaixo do Loop do For, pois devem substituir as informacoes|
					//|que foram armazenadas no Loop acima                        |
					//�������������������������������������������������������������
					oModel:LoadValue( 'MODEL_T3A', 'T3A_VERSAO', cVersao )
					oModel:LoadValue( 'MODEL_T3A', 'T3A_VERANT', cVerAnt )
					oModel:LoadValue( 'MODEL_T3A', 'T3A_PROTPN', cProtocolo )
					oModel:LoadValue( 'MODEL_T3A', 'T3A_PROTUL', "" )

					TafGrvExt( oModel, 'MODEL_T3A', 'T3A' )
				Endif
			EndIf

			If !lExecAltMan
				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_T3A', 'T3A_LOGOPE' , '' , cLogOpeAnt )
				EndIf
			EndIf

			//Gravo a altera��o
			FwFormCommit( oModel )
			TAFAltStat( 'T3A', " " )

		EndIf

	ElseIf nOperation == MODEL_OPERATION_DELETE

		oModel:DeActivate()
		oModel:SetOperation( 5 )
		oModel:Activate()

		FwFormCommit( oModel )
	EndIf

End Transaction

Return ( lRetorno )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF403Xml
Funcao de geracao do XML para atender o registro S-1040
Quando a rotina for chamada o registro deve estar posicionado

@Param:
cAlias - Alias corrente (Parametro padrao MVC)
nRecno - Recno corrente (Parametro padrao MVC)
nOpc   - Opcao selecionada (Parametro padrao MVC)
lJob   - Informa se foi chamado por Job
lRemEmp - Exclusivo do Evento S-1000
cSeqXml - Numero sequencial para composi��o da chave ID do XML

@Return:
cXml - Estrutura do Xml do Layout S-1040

@author Vitor Henrique
@since 04/10/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Function TAF403Xml(cAlias,nRecno,nOpc,lJob,lRemEmp,cSeqXml)

Local cXml     := ""
Local cLayout  := "2190"
Local cReg     := "AdmPrelim"
Local cId      := ""
Local cVerAnt  := ""
Local aAreaT3A := {}
Local lXmlVLd	:= IIF(FindFunction('TafXmlVLD'),TafXmlVLD('TAF403XML'),.T.)

Default lJob   := .F.
Default cSeqXml := ""

If lXmlVLd
	aAreaT3A := {}

	cXml +=			"<infoRegPrelim>"
	cXml +=				xTafTag("cpfTrab" ,T3A->T3A_CPF)
	cXml +=				xTafTag("dtNascto",T3A->T3A_DTNASC)
	cXml +=				xTafTag("dtAdm"   ,T3A->T3A_DTADMI)
	cXml +=			"</infoRegPrelim>"

	/*---------------------------------------------------------
	Estrutura do cabecalho
	---------------------------------------------------------*/
	cXml := xTafCabXml(cXml,"T3A",cLayout,cReg,,cSeqXml)

	/*---------------------------------------------------------
	Executa gravacao do registro
	---------------------------------------------------------*/
	If !lJob
		xTafGerXml(cXml,cLayout)
	EndIf
EndIf

Return(cXml)


//-------------------------------------------------------------------
/*/{Protheus.doc} Taf403Vld
Funcao que valida os dados do registro posicionado,
verificando se ha incoerencias nas informacos
caso seja necessario gerar um XML

lJob - Informa se foi chamado por Job

@return .T.

@author Vitor Henrique
@since 23/10/2015
@version 1.0
/*/
//-------------------------------------------------------------------
Function Taf403Vld( cAlias, nRecno, nOpc, lJob )

Local cStatus  	:= ""
Local aLogErro  	:= {} 
Local aDadosUtil	:= {} 
Local lValida   	:= .F.
Local dIniESoc 	:= TAFGetIniESocial()
Local dIniNPer	:= xIniNPer()
Local nRecno		:= 0

Default lJob 		:= .F. 

/*---------------------------------------------------------
Garanto que o Recno seja da tabela referente ao cadastro
principal
---------------------------------------------------------*/
nRecno 	:= T3A->( Recno() )
lValida 	:= ( T3A->T3A_STATUS $ ( " |1" ) )
dIniESoc 	:= TAFGetIniESocial()
dIniNPer	:= xIniNPer()

If lValida

	aAdd(aDadosUtil, {T3A->T3A_DTADMI, T3A->T3A_DTADMI, T3A->T3A_FILIAL})

	xVldRegra("C1E","REGRA_EXISTE_INFO_EMPREGADOR",@aLogErro,aDadosUtil,"T3A")

	If T3A->T3A_DTADMI <= T3A->T3A_DTNASC
		AADD(aLogErro,{"T3A_DTADMI","000699","T3A",nRecno}) //STR000699 - " A data de admiss�o deve ser posterior � data de nascimento do trabalhador"
	EndIf

	/* Retirado Valida��o no 2.4.1
	If T3A->T3A_DTADMI < dIniESoc
		AADD(aLogErro,{"T3A_DTADMI","000982","T3A",nRecno}) //STR0982 - "A data de admiss�o do trabalhador deve ser igual ou posterior � data de in�cio da obrigatoriedade do empregador ao eSocial."
	EndIf
	*/
	If T3A->T3A_DTADMI < dIniNPer
		AADD(aLogErro,{"T3A_DTADMI","001087","T3A",nRecno}) //STR1087 - "A data de admiss�o do trabalhador deve ser igual ou posterior � data de in�cio da obrigatoriedade dos eventos n�o peri�dicos para o empregador."
	EndIf

	/*---------------------------------------------------------
	ATUALIZO O STATUS DO REGISTRO
	---------------------------------------------------------*/
	cStatus := Iif(Len(aLogErro) > 0,"1","0")

	If RecLock("T3A",.F.)
		T3A->T3A_STATUS := cStatus
		T3A->(MsUnlock())
	EndIf
Else
	AADD(aLogErro,{"T3A_ID","000305","T3A",nRecno}) //Registros que j� foram transmitidos ao Fisco, n�o podem ser validados
EndIf

/*---------------------------------------------------------
N�o apresento o alert quando utilizo o JOB para validar
---------------------------------------------------------*/
If !lJob
	xValLogEr(aLogErro)
EndIf

Return(aLogErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} GerarEvtExc
Funcao que gera a exclus�o do evento (S-3000)

@Param  oModel  -> Modelo de dados
@Param  nRecno  -> Numero do recno
@Param  lRotExc -> Variavel que controla se a function � chamada pelo TafIntegraESocial

@Return .T.

@Author Vitor Henrique Ferreira
@Since 30/06/2015
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function GerarEvtExc( oModel, nRecno, lRotExc )

Local cVerAnt    	:= ""
Local cProtocolo 	:= ""
Local cVersao    	:= ""
Local cChvRegAnt 	:= ""
Local cEvento	   	:= ""
Local cId		  	:= ""
Local nOperation 	:= oModel:GetOperation()
Local nlY        	:= 0
Local nlI		   	:= 0
Local aCampos    	:= {}
Local aGrava     	:= {}
Local aGravaT3A  	:= {}
Local oModelT3A  	:= Nil
//Controle se o evento � extempor�neo
Local lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

Begin Transaction

	/*---------------------------------------------------------
	Posiciona o item
	---------------------------------------------------------*/
	("T3A")->( DBGoTo( nRecno ) )
	
	oModelT3A	:= oModel:GetModel( 'MODEL_T3A' )
										
	/*---------------------------------------------------------
	Busco a versao anterior do registro para gravacao do rastro
	---------------------------------------------------------*/
	cVerAnt   	:= oModelT3A:GetValue( "T3A_VERSAO" )
	cProtocolo	:= oModelT3A:GetValue( "T3A_PROTUL" )
	cEvento	:= oModelT3A:GetValue( "T3A_EVENTO" )
		       		        						
	/*---------------------------------------------------------
	Neste momento eu gravo as informacoes que foram carregadas
	na tela, pois neste momento o usuario ja fez as modificacoes
	que precisava e as mesmas estao armazenadas em memoria,
	ou seja, nao devem ser consideradas neste momento
	---------------------------------------------------------*/
	For nlI := 1 To 1
		For nlY := 1 To Len( oModelT3A:aDataModel[ nlI ] )
			Aadd( aGrava, { oModelT3A:aDataModel[ nlI, nlY, 1 ], oModelT3A:aDataModel[ nlI, nlY, 2 ] } )
		Next
	Next
	  																									                                                     
	/*---------------------------------------------------------
	Seto o campo como Inativo e gravo a versao do novo registro
	no registro anterior
	ATENCAO -> A alteracao destes campos deve sempre estar
	abaixo do Loop do For, pois devem substituir as informacoes
	que foram armazenadas no Loop acima
	---------------------------------------------------------*/
	FAltRegAnt( 'T3A', '2' )

	/*---------------------------------------------------------
	Neste momento eu preciso setar a operacao do modeL como
	Inclusao
	---------------------------------------------------------*/
	oModel:DeActivate()
	oModel:SetOperation( 3 )
	oModel:Activate()
					
	/*---------------------------------------------------------
	Neste momento eu realizo a inclusao do novo registro ja
	contemplando as informacoes alteradas pelo usuario
	---------------------------------------------------------*/
	For nlI := 1 To Len( aGrava )
		oModel:LoadValue( 'MODEL_T3A', aGrava[ nlI, 1 ], aGrava[ nlI, 2 ] )
	Next					         
	       
	/*---------------------------------------------------------
	Busco a nova versao do registro
	---------------------------------------------------------*/
	cVersao := xFunGetVer()
	
	/*---------------------------------------------------------
	ATENCAO -> A alteracao destes campos deve sempre estar     
	abaixo do Loop do For, pois devem substituir as informacoes
	que foram armazenadas no Loop acima                        
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_T3A", "T3A_VERSAO", cVersao )
	oModel:LoadValue( "MODEL_T3A", "T3A_VERANT", cVerAnt )
	oModel:LoadValue( "MODEL_T3A", "T3A_PROTPN", cProtocolo )
	oModel:LoadValue( "MODEL_T3A", "T3A_PROTUL", "" )
	// Tratamento para limpar o ID unico do xml
	cAliasPai := "T3A"
	If TAFColumnPos( cAliasPai+"_XMLID" )
		oModel:LoadValue( 'MODEL_'+cAliasPai, cAliasPai+'_XMLID', "" )
	EndIf
	
	/*---------------------------------------------------------
	Tratamento para que caso o Evento Anterior fosse de exclus�o
	seta-se o novo evento como uma "nova inclus�o", caso contr�rio o
	evento passar a ser uma altera��o
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_T3A", "T3A_EVENTO", "E" )
	oModel:LoadValue( "MODEL_T3A", "T3A_ATIVO", "1" )		

	//Gravo altera��o para o Extempor�neo
	If lGoExtemp
		TafGrvExt( oModel, 'MODEL_T3A', 'T3A' )	
	EndIf
	
	FwFormCommit( oModel )
	TAFAltStat( 'T3A',"6" )
	
End Transaction
	
Return ( .T. )
//-------------------------------------------------------------------