#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFA250.CH"

Static lTAFCodRub	:= FindFunction("TAFCodRub")

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA413
S-1202 - Remunera��o de servidor vinculado a Regime Pr�prio de Previd. Social

@author Vitor Siqueira
@since 08/01/2016
@version 1.0

/*/ 
//------------------------------------------------------------------
Function TAFA413()

Local cTitulo, cMensagem := ""

Private oBrw	:=  FWmBrowse():New()

// Fun��o que indica se o ambiente � v�lido para o eSocial 2.3
If TafAtualizado()

	oBrw:SetDescription(STR0055)    //"Cadastro de Remunera��o RPPS"
	oBrw:SetAlias( 'C91')
	oBrw:SetMenuDef( 'TAFA413' )
	If FindFunction('TAFSetFilter')
		oBrw:SetFilterDefault(TAFBrwSetFilter("C91","TAFA413","S-1202"))
	Else
		oBrw:SetFilterDefault( "C91_ATIVO == '1' .and. C91_NOMEVE == 'S1202'" ) //Filtro para que apenas os registros ativos sejam exibidos ( 1=Ativo, 2=Inativo )
	EndIf
	oBrw:SetOnlyFields( { 'C91_FILIAL', 'C91_ID', 'C91_TRABAL', 'C91_INDAPU', 'C91_PERAPU' } )
	TafLegend(2,"C91",@oBrw)
	
	oBrw:Activate()
	
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Vitor Siqueira
@since 08/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao := {}
Local aRotina := {}

If FindFunction('TafXmlRet')
	Aadd( aFuncao, { "" , "TafxmlRet('TAF413Xml','1202','C91')" , "1" } )
Else 
	Aadd( aFuncao, { "" , "TAF413Xml" , "1" } )
EndIf 

Aadd( aFuncao, { "" , "TAF413Vld" , "2" } )
Aadd( aFuncao, { "" , "xFunHisAlt( 'C91', 'TAFA413' ,,,, 'TAF413XML','1202'  )", "3" } )
Aadd( aFuncao, { "" , "TAFXmlLote( 'C91', 'S-1202' , 'evtRmnRPPS' , 'TAF413Xml',, oBrw )" , "5" } )
Aadd( aFuncao, { "" , "xFunAltRec( 'C91' )" , "10" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If lMenuDif
	ADD OPTION aRotina Title "Visualizar" Action 'VIEWDEF.TAFA413' OPERATION 2 ACCESS 0
Else
	aRotina	:=	xFunMnuTAF( "TAFA413" , , aFuncao)
EndIf

Return( aRotina )
//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC

@author Vitor Siqueira
@since 08/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

Local oStruCRN  :=  FWFormStruct( 1, 'CRN' )
Local oStruC91	:=  FWFormStruct( 1, 'C91' )

Local oStruT6C  :=  FWFormStruct( 1, 'T6C' )
Local oStruT6D  :=  FWFormStruct( 1, 'T6D' )
Local oStruT6E  :=  FWFormStruct( 1, 'T6E' )
Local oStruT6F  :=  FWFormStruct( 1, 'T6F' )
Local oStruT6G  :=  FWFormStruct( 1, 'T6G' )

Local oStruT14  :=  FWFormStruct( 1, 'T14' )

Local oStruT61  :=  FWFormStruct( 1, 'T61' )
Local oStruT6H  :=  FWFormStruct( 1, 'T6H' )
Local oStruT6I  :=  FWFormStruct( 1, 'T6I' )
Local oStruT6J  :=  FWFormStruct( 1, 'T6J' )
Local oStruT6K  :=  FWFormStruct( 1, 'T6K' )

Local oModel    :=  MPFormModel():New( 'TAFA413' ,,,{|oModel| SaveModel(oModel)})


lVldModel := Iif( Type( "lVldModel" ) == "U", .F., lVldModel )

If lVldModel
	oStruC91:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruCRN:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel }) 
		 
	oStruT6C:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruT6D:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel }) 		
	oStruT6E:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruT6F:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruT6G:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })

	oStruT14:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	
	oStruT61:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })	
	oStruT6H:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel })
	oStruT6I:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel }) 			
	oStruT6J:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel }) 		
	oStruT6K:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel }) 		


EndIf    

// INFORMA��ES DE IDENTIFICA��O DO EVENTO 
oModel:AddFields('MODEL_C91', /*cOwner*/, oStruC91)

// INFORMA��ES DE PAGAMENTOS EFETUADOS
oModel:AddGrid('MODEL_CRN', 'MODEL_C91', oStruCRN)
oModel:GetModel('MODEL_CRN'):SetOptional(.T.)
oModel:GetModel('MODEL_CRN'):SetUniqueLine({'CRN_TPTRIB', 'CRN_IDPROC'})
oModel:GetModel('MODEL_CRN'):SetMaxLine(99) 

// DEMONSTRATIVOS DE VALORES DEVIDOS AO TRABALHADOR 
oModel:AddGrid('MODEL_T14', 'MODEL_C91', oStruT14)
oModel:GetModel('MODEL_T14'):SetOptional(.T.)
oModel:GetModel('MODEL_T14'):SetUniqueLine({'T14_IDEDMD'})
oModel:GetModel('MODEL_T14'):SetMaxLine(99)

// INFORMA��ES DO ESTABELECIMENTO/LOTA��O
oModel:AddGrid('MODEL_T6C', 'MODEL_T14', oStruT6C)
oModel:GetModel('MODEL_T6C'):SetOptional(.T.)
oModel:GetModel('MODEL_T6C'):SetUniqueLine({'T6C_ESTABE'})
oModel:GetModel('MODEL_T6C'):SetMaxLine(24)

//INFORMA��ES DO REMUNERA��O DO PERIODO DE APURA��O
oModel:AddGrid('MODEL_T6D', 'MODEL_T6C', oStruT6D)
oModel:GetModel('MODEL_T6D'):SetOptional(.T.)
oModel:GetModel('MODEL_T6D'):SetUniqueLine({'T6D_IDTRAB', 'T6D_CODCAT'})
oModel:GetModel('MODEL_T6D'):SetMaxLine(10)    

//INFORMA��ES DOS ITENS DE REMUNERA��O
oModel:AddGrid('MODEL_T6E', 'MODEL_T6D', oStruT6E)
oModel:GetModel('MODEL_T6E'):SetOptional(.T.)
oModel:GetModel('MODEL_T6E'):SetUniqueLine({'T6E_IDRUBR'})
oModel:GetModel('MODEL_T6E'):SetMaxLine(200) 

// INFORMA��ES DO PLANO DE SA�DE  
oModel:AddGrid('MODEL_T6F', 'MODEL_T6D', oStruT6F)
oModel:GetModel('MODEL_T6F'):SetOptional(.T.)
oModel:GetModel('MODEL_T6F'):SetUniqueLine({'T6F_CNPJOP'})
oModel:GetModel('MODEL_T6F'):SetMaxLine(99) 

// INFORMA��ES DO PLANO DE SA�DE - Det. Operadora Plano de Sa�de 
oModel:AddGrid('MODEL_T6G', 'MODEL_T6F', oStruT6G)
oModel:GetModel('MODEL_T6G'):SetOptional(.T.)
oModel:GetModel('MODEL_T6G'):SetUniqueLine({'T6G_TPDEP','T6G_NOMDEP','T6G_DTNDEP'})
oModel:GetModel('MODEL_T6G'):SetMaxLine(99) 


// IDENTIFICA��O DA LEI QUE DETERMINOU REMUNERA��O EM PER�ODOS ANTERIORES 
oModel:AddGrid('MODEL_T61', 'MODEL_T14', oStruT61)
oModel:GetModel('MODEL_T61'):SetOptional(.T.)
oModel:GetModel('MODEL_T61'):SetUniqueLine({'T61_DTLEI','T61_NUMLEI'}) 
oModel:GetModel('MODEL_T61'):SetMaxLine(8) 

// INFORMA��ES DO PERIODO
oModel:AddGrid('MODEL_T6H', 'MODEL_T61', oStruT6H)
oModel:GetModel('MODEL_T6H'):SetOptional(.T.)
oModel:GetModel('MODEL_T6H'):SetUniqueLine({'T6H_PERREF'})
oModel:GetModel('MODEL_T6H'):SetMaxLine(200) 

// INFORMA��ES DO ESTABELECIMENTO/LOTA��O
oModel:AddGrid('MODEL_T6I', 'MODEL_T6H', oStruT6I)
oModel:GetModel('MODEL_T6I'):SetOptional(.T.)
oModel:GetModel('MODEL_T6I'):SetUniqueLine({'T6I_ESTABE'})
oModel:GetModel('MODEL_T6I'):SetMaxLine(24)

// INFORMA��ES DA REMUNERA��O DO TRABALHADOR
oModel:AddGrid('MODEL_T6J', 'MODEL_T6I', oStruT6J)
oModel:GetModel('MODEL_T6J'):SetOptional(.T.)
oModel:GetModel('MODEL_T6J'):SetUniqueLine({'T6J_IDTRAB','T6J_CODCAT'})
oModel:GetModel('MODEL_T6J'):SetMaxLine(10) 

// INFORMA��ES DOS ITENS DA REMUNERA��O
oModel:AddGrid('MODEL_T6K', 'MODEL_T6J', oStruT6K)
oModel:GetModel('MODEL_T6K'):SetOptional(.T.)
oModel:GetModel('MODEL_T6K'):SetUniqueLine({'T6K_IDRUBR'})
oModel:GetModel('MODEL_T6K'):SetMaxLine(200) 


//PRIMARY KEY
oModel:GetModel('MODEL_C91'):SetPrimaryKey({'C91_FILIAL', 'C91_ID', 'C91_VERSAO'})

// RELATIONS
oModel:SetRelation('MODEL_CRN', {{'CRN_FILIAL','xFilial("CRN")'}, {'CRN_ID','C91_ID'}, {'CRN_VERSAO','C91_VERSAO'}}, CRN->(IndexKey(1)))

oModel:SetRelation('MODEL_T14', {{'T14_FILIAL','xFilial("T14")'}, {'T14_ID','C91_ID'}, {'T14_VERSAO','C91_VERSAO'}, {'T14_INDAPU','C91_INDAPU'}, {'T14_PERAPU','C91_PERAPU'}, {'T14_TRABAL','C91_TRABAL'}}, T14->(IndexKey(1)))

oModel:SetRelation('MODEL_T6C', {{'T6C_FILIAL','xFilial("T6C")'}, {'T6C_ID','C91_ID'}, {'T6C_VERSAO','C91_VERSAO'}, {'T6C_INDAPU','C91_INDAPU'}, {'T6C_PERAPU','C91_PERAPU'}, {'T6C_TRABAL','C91_TRABAL'}, {'T6C_DEMPAG','T14_IDEDMD'}}, T6C->(IndexKey(1)))
oModel:SetRelation('MODEL_T6D', {{'T6D_FILIAL','xFilial("T6D")'}, {'T6D_ID','C91_ID'}, {'T6D_VERSAO','C91_VERSAO'}, {'T6D_INDAPU','C91_INDAPU'}, {'T6D_PERAPU','C91_PERAPU'}, {'T6D_TRABAL','C91_TRABAL'}, {'T6D_DEMPAG','T14_IDEDMD'}, {'T6D_ESTABE','T6C_ESTABE'}}, T6D->(IndexKey(1)))  
oModel:SetRelation('MODEL_T6E', {{'T6E_FILIAL','xFilial("T6E")'}, {'T6E_ID','C91_ID'}, {'T6E_VERSAO','C91_VERSAO'}, {'T6E_INDAPU','C91_INDAPU'}, {'T6E_PERAPU','C91_PERAPU'}, {'T6E_TRABAL','C91_TRABAL'}, {'T6E_DEMPAG','T14_IDEDMD'}, {'T6E_ESTABE','T6C_ESTABE'}, {'T6E_IDTRAB','T6D_IDTRAB'}, {'T6E_CODCAT','T6D_CODCAT'}}, T6E->(IndexKey(1)))
oModel:SetRelation('MODEL_T6F', {{'T6F_FILIAL','xFilial("T6F")'}, {'T6F_ID','C91_ID'}, {'T6F_VERSAO','C91_VERSAO'}, {'T6F_INDAPU','C91_INDAPU'}, {'T6F_PERAPU','C91_PERAPU'}, {'T6F_TRABAL','C91_TRABAL'}, {'T6F_DEMPAG','T14_IDEDMD'}, {'T6F_ESTABE','T6C_ESTABE'}, {'T6F_IDTRAB','T6D_IDTRAB'}, {'T6F_CODCAT','T6D_CODCAT'}}, T6F->(IndexKey(1)))
oModel:SetRelation('MODEL_T6G', {{'T6G_FILIAL','xFilial("T6G")'}, {'T6G_ID','C91_ID'}, {'T6G_VERSAO','C91_VERSAO'}, {'T6G_INDAPU','C91_INDAPU'}, {'T6G_PERAPU','C91_PERAPU'}, {'T6G_TRABAL','C91_TRABAL'}, {'T6G_DEMPAG','T14_IDEDMD'}, {'T6G_ESTABE','T6C_ESTABE'}, {'T6G_IDTRAB','T6D_IDTRAB'}, {'T6G_CODCAT','T6D_CODCAT'}, {'T6G_CNPJOP','T6F_CNPJOP'}}, T6G->(IndexKey(1)))

oModel:SetRelation('MODEL_T61', {{'T61_FILIAL','xFilial("T61")'}, {'T61_ID','C91_ID'}, {'T61_VERSAO','C91_VERSAO'}, {'T61_INDAPU','C91_INDAPU'}, {'T61_PERAPU','C91_PERAPU'}, {'T61_TRABAL','C91_TRABAL'}, {'T61_DEMPAG','T14_IDEDMD'}}, T61->(IndexKey(1)))
oModel:SetRelation('MODEL_T6H', {{'T6H_FILIAL','xFilial("T6H")'}, {'T6H_ID','C91_ID'}, {'T6H_VERSAO','C91_VERSAO'}, {'T6H_INDAPU','C91_INDAPU'}, {'T6H_PERAPU','C91_PERAPU'}, {'T6H_TRABAL','C91_TRABAL'}, {'T6H_DEMPAG','T14_IDEDMD'}, {'T6H_DTLEI','T61_DTLEI'}, {'T6H_NUMLEI','T61_NUMLEI'}}, T6H->(IndexKey(1)))
oModel:SetRelation('MODEL_T6I', {{'T6I_FILIAL','xFilial("T6I")'}, {'T6I_ID','C91_ID'}, {'T6I_VERSAO','C91_VERSAO'}, {'T6I_INDAPU','C91_INDAPU'}, {'T6I_PERAPU','C91_PERAPU'}, {'T6I_TRABAL','C91_TRABAL'}, {'T6I_DEMPAG','T14_IDEDMD'}, {'T6I_DTLEI','T61_DTLEI'}, {'T6I_NUMLEI','T61_NUMLEI'}, {'T6I_PERREF','T6H_PERREF'}}, T6I->(IndexKey(1)))
oModel:SetRelation('MODEL_T6J', {{'T6J_FILIAL','xFilial("T6J")'}, {'T6J_ID','C91_ID'}, {'T6J_VERSAO','C91_VERSAO'}, {'T6J_INDAPU','C91_INDAPU'}, {'T6J_PERAPU','C91_PERAPU'}, {'T6J_TRABAL','C91_TRABAL'}, {'T6J_DEMPAG','T14_IDEDMD'}, {'T6J_DTLEI','T61_DTLEI'}, {'T6J_NUMLEI','T61_NUMLEI'}, {'T6J_PERREF','T6H_PERREF'}, {'T6J_ESTABE','T6I_ESTABE'}}, T6J->(IndexKey(1)))
oModel:SetRelation('MODEL_T6K', {{'T6K_FILIAL','xFilial("T6K")'}, {'T6K_ID','C91_ID'}, {'T6K_VERSAO','C91_VERSAO'}, {'T6K_INDAPU','C91_INDAPU'}, {'T6K_PERAPU','C91_PERAPU'}, {'T6K_TRABAL','C91_TRABAL'}, {'T6K_DEMPAG','T14_IDEDMD'}, {'T6K_DTLEI','T61_DTLEI'}, {'T6K_NUMLEI','T61_NUMLEI'}, {'T6K_PERREF','T6H_PERREF'}, {'T6K_ESTABE','T6I_ESTABE'}, {'T6K_IDTRAB','T6J_IDTRAB'}, {'T6K_CODCAT','T6J_CODCAT'}}, T6K->(IndexKey(1)))


Return oModel
//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Vitor Siqueira
@since 08/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oModel   	:= FWLoadModel( 'TAFA413' )

Local oView    	:= Nil
Local oStruC91a	:= Nil
Local oStruC91b := Nil
Local oStruCRN 	:= FWFormStruct( 2, 'CRN' )

Local oStruT14 	:= FWFormStruct( 2, 'T14' )

Local oStruT6C 	:= FWFormStruct( 2, 'T6C' )
Local oStruT6D 	:= FWFormStruct( 2, 'T6D' )
Local oStruT6E 	:= FWFormStruct( 2, 'T6E' )
Local oStruT6F 	:= FWFormStruct( 2, 'T6F' )
Local oStruT6G 	:= FWFormStruct( 2, 'T6G' )

Local oStruT61 	:= FWFormStruct( 2, 'T61' )
Local oStruT6H 	:= FWFormStruct( 2, 'T6H' )
Local oStruT6I 	:= FWFormStruct( 2, 'T6I' )
Local oStruT6J 	:= FWFormStruct( 2, 'T6J' )
Local oStruT6K 	:= FWFormStruct( 2, 'T6K' )

Local cCmpFil	:= ""

oView := FWFormView():New()
oView:SetModel( oModel )
oView:SetContinuousForm()

// Campos do folder Informacoes da Folha
cCmpFil := 'C91_ID|C91_INDAPU|C91_PERAPU|C91_TRABAL|C91_DTRABA|'
oStruC91a := FwFormStruct( 2, 'C91', {|x| AllTrim( x ) + "|" $ cCmpFil } )

// Campos do folder N�mero de protocolo
cCmpFil := 'C91_PROTUL|
oStruC91b := FwFormStruct( 2, 'C91', {|x| AllTrim( x ) + "|" $ cCmpFil } )


/*--------------------------------------------------------------------------------------------
									Esrutura da View
---------------------------------------------------------------------------------------------*/
oView:AddField( 'VIEW_C91a', oStruC91a, 'MODEL_C91' )
oView:EnableTitleView( 'VIEW_C91a', STR0056 ) // "Informa��es da Remunera��o RPPS" 

oView:AddField( 'VIEW_C91b', oStruC91b, 'MODEL_C91' )

If FindFunction('TafNmFolder')
	oView:EnableTitleView( 'VIEW_C91b', TafNmFolder("recibo",1) ) // "Recibo da �ltima Transmiss�o"  
EndIf

oView:AddGrid( 'VIEW_CRN', oStruCRN, 'MODEL_CRN' )
oView:EnableTitleView("VIEW_CRN",STR0053) //"Informa��es de Processos Judiciarios de Remunera��o"

oView:AddGrid( 'VIEW_T14', oStruT14, 'MODEL_T14' )
oView:EnableTitleView("VIEW_T14",STR0040) //"Informa��es do Recibo de Pagamento"

oView:AddGrid( 'VIEW_T6C', oStruT6C, 'MODEL_T6C' )
oView:EnableTitleView("VIEW_T6C",STR0041) //"Informa��es do Estabelecimento/Lota��o"

oView:AddGrid( 'VIEW_T6D', oStruT6D, 'MODEL_T6D' )
oView:EnableTitleView("VIEW_T6D",STR0042) //"Informa��es da Remunera��o do Trabalhador"

oView:AddGrid( 'VIEW_T6E', oStruT6E, 'MODEL_T6E' ) //"Informa��es do Plano de Sa�de"

oView:AddGrid( 'VIEW_T6F', oStruT6F, 'MODEL_T6F' )
oView:EnableTitleView("VIEW_T6F",STR0044) //"Detalhamento dos Valores Pagos"

oView:AddGrid( 'VIEW_T6G', oStruT6G, 'MODEL_T6G' )
oView:EnableTitleView("VIEW_T6G",STR0043) //"Informa��es do Dependente"

oView:AddGrid( 'VIEW_T61', oStruT61, 'MODEL_T61' )
oView:EnableTitleView("VIEW_T61",STR0040) //"Identifica��o da Lei que Determinou Remunera��o em Per�odos Anteriores "

oView:AddGrid( 'VIEW_T6H', oStruT6H, 'MODEL_T6H' )
oView:EnableTitleView("VIEW_T6H",STR0048) //"Informa��es do Periodo"

oView:AddGrid( 'VIEW_T6I', oStruT6I, 'MODEL_T6I' )
oView:EnableTitleView("VIEW_T6I",STR0041) //"Informa��es do Estabelecimento/Lota��o"

oView:AddGrid( 'VIEW_T6J', oStruT6J, 'MODEL_T6J' )
oView:EnableTitleView("VIEW_T6J","Remunera��o do Per�odo Anterior") //"Informa��es da Remunera��o do Trabalhador"

oView:AddGrid( 'VIEW_T6K', oStruT6K, 'MODEL_T6K' )
oView:EnableTitleView("VIEW_T6K","Itens de Remunera��o") //"Itens de Rumenera��o"

If FindFunction('TafAjustRecibo')
	TafAjustRecibo(oStruC91b,"C91")
EndIf 

/*-----------------------------------------------------------------------------------
								Estrutura do Folder
-------------------------------------------------------------------------------------*/
oView:CreateHorizontalBox( 'PAINEL_SUPERIOR', 100 )

oView:CreateFolder( 'FOLDER_SUPERIOR', 'PAINEL_SUPERIOR' )

oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA01', STR0056 )   //"Informa��es da Remunera��o RPPS"
oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA02', STR0038 )   //"Informa��es do Periodo de Apura��o"

If FindFunction('TafNmFolder')
	oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA03', TafNmFolder("recibo") )   //"Numero do Recibo"
Else
	oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA03', STR0052 )   //"Protocolo de Transmiss�o"
EndIf

oView:CreateHorizontalBox( 'C91a',  25,,, 'FOLDER_SUPERIOR', 'ABA01' )
oView:CreateHorizontalBox( 'CRN' ,  75,,, 'FOLDER_SUPERIOR', 'ABA01' )

oView:CreateHorizontalBox( 'PAINEL_DEMONST' ,	15,,, 'FOLDER_SUPERIOR', 'ABA02' )
oView:CreateHorizontalBox( 'PAINEL_APURACAO',	85,,, 'FOLDER_SUPERIOR', 'ABA02' )

oView:CreateHorizontalBox( 'C91b',  15,,, 'FOLDER_SUPERIOR', 'ABA03' )

oView:CreateFolder( 'FOLDER_APURACAO', 'PAINEL_APURACAO' )
oView:AddSheet( 'FOLDER_APURACAO', 'ABA01', STR0038 )   //"Informa��es do Periodo de Apur. Anterior"
oView:AddSheet( 'FOLDER_APURACAO', 'ABA02', STR0039 )   //"Informa��es do Periodo de Apur. Anterior"

oView:CreateHorizontalBox('PAINEL_ESTABELEC'  ,25,,,	'FOLDER_APURACAO','ABA01')                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
oView:CreateHorizontalBox('PAINEL_REMUN_PER'  ,25,,,	'FOLDER_APURACAO','ABA01')
oView:CreateHorizontalBox('PAINEL_ITENS_REMUN',50,,,	'FOLDER_APURACAO','ABA01')

oView:CreateFolder( 'FOLDER_REMUN', 'PAINEL_ITENS_REMUN' )
oView:AddSheet( 'FOLDER_REMUN', 'ABA01', STR0036 ) //"Itens Rem. Trab."
oView:AddSheet( 'FOLDER_REMUN', 'ABA02', STR0034 ) //"Informa��o do Plano de Sa�de"  

oView:CreateHorizontalBox ( 'T6E', 100,,,'FOLDER_REMUN'  , 'ABA01' )
oView:CreateHorizontalBox ( 'T6F', 50,,, 'FOLDER_REMUN'  , 'ABA02' )
oView:CreateHorizontalBox ( 'T6G', 50,,, 'FOLDER_REMUN'  , 'ABA02' )


/*------------------------
   PERIODO DE APUR. ANT.
--------------------------*/
oView:CreateHorizontalBox ( 'T61', 20,,, 'FOLDER_APURACAO' 	, 'ABA02' )
oView:CreateHorizontalBox ( 'T6H', 20,,, 'FOLDER_APURACAO'	, 'ABA02' )
oView:CreateHorizontalBox ( 'T6I', 20,,, 'FOLDER_APURACAO' 	, 'ABA02' )
oView:CreateHorizontalBox ( 'T6J', 20,,, 'FOLDER_APURACAO'	, 'ABA02' )
oView:CreateHorizontalBox ( 'T6K', 20,,, 'FOLDER_APURACAO' 	, 'ABA02' )

oView:SetOwnerView( 'VIEW_C91a', 'C91a')
oView:SetOwnerView( 'VIEW_C91b', 'C91b')
oView:SetOwnerView( 'VIEW_CRN' , 'CRN')

oView:SetOwnerView( 'VIEW_T14' , 'PAINEL_DEMONST')

oView:SetOwnerView( 'VIEW_T6C' , 'PAINEL_ESTABELEC')
oView:SetOwnerView( 'VIEW_T6D' , 'PAINEL_REMUN_PER')
oView:SetOwnerView( 'VIEW_T6E' , 'T6E')
oView:SetOwnerView( 'VIEW_T6F' , 'T6F')
oView:SetOwnerView( 'VIEW_T6G' , 'T6G')
oView:SetOwnerView( 'VIEW_T61' , 'T61')

oView:SetOwnerView( 'VIEW_T6H' , 'T6H')
oView:SetOwnerView( 'VIEW_T6I' , 'T6I')
oView:SetOwnerView( 'VIEW_T6J' , 'T6J')
oView:SetOwnerView( 'VIEW_T6K' , 'T6K')


lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif
	xFunRmFStr(@oStruC91a,"C91")
	
	oStruC91a:RemoveField('C91_ID')
	oStruT6D:RemoveField('T6D_ID')
	oStruCRN:RemoveField('CRN_IDSUSP')
	oStruT14:RemoveField('T14_CODCAT')
	oStruT14:RemoveField('T14_DCATEG')
	oStruT14:RemoveField('T14_NOMEVE')
	
EndIf


Return oView
//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@Param  oModel -> Modelo de dados

@Return .T.

@author Vitor Siqueira
@since 11/01/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel( oModel )

Local cVerAnt    	:= ""
Local cProtocolo 	:= ""
Local cVersao    	:= ""
Local cChvRegAnt 	:= ""
Local cEvento	   	:= ""
Local cId		  	:= ""
Local cLogOpe		:= ""
Local cLogOpeAnt	:= ""

Local nOperation	:= 0
Local nlI  		    := 0
Local nlY  		    := 0
Local nCRN		   	:= 0
Local nCRNAdd 	    := 0
Local nT14		   	:= 0
Local nT14Add	  	:= 0
Local nT6C       	:= 0
Local nT6CAdd    	:= 0
Local nT6D       	:= 0
Local nT6DAdd    	:= 0
Local nT6E       	:= 0
Local nT6EAdd    	:= 0
Local nT6F       	:= 0
Local nT6FAdd    	:= 0
Local nT6G       	:= 0
Local nT6GAdd   	:= 0
Local nT61       	:= 0
Local nT61Add    	:= 0
Local nT6H       	:= 0
Local nT6HAdd    	:= 0
Local nT6I       	:= 0
Local nT6IAdd    	:= 0
Local nT6J       	:= 0
Local nT6JAdd    	:= 0
Local nT6K       	:= 0
Local nT6KAdd    	:= 0

Local aGrava     	:= {}
Local aGravaCRN  	:= {}
Local aGravaT14  	:= {}
Local aGravaT6C  	:= {}
Local aGravaT6D  	:= {}
Local aGravaT6E  	:= {}
Local aGravaT6F  	:= {}
Local aGravaT6G  	:= {}
Local aGravaT61  	:= {}
Local aGravaT6H  	:= {}
Local aGravaT6I  	:= {}
Local aGravaT6J  	:= {}
Local aGravaT6K  	:= {}

Local oModelC91  	:= Nil
Local oModelCRN  	:= Nil
Local oModelT14  	:= Nil
Local oModelT6C  	:= Nil
Local oModelT6D  	:= Nil
Local oModelT6E  	:= Nil
Local oModelT6F  	:= Nil
Local oModelT6G  	:= Nil
Local oModelT61  	:= Nil
Local oModelT6H  	:= Nil
Local oModelT6I  	:= Nil
Local oModelT6J  	:= Nil
Local oModelT6K	    := Nil

Local lRetorno	    := .T.

Default oModel	    := Nil

nOperation	:= oModel:GetOperation()

Begin Transaction 
	
	If nOperation == MODEL_OPERATION_INSERT

		oModel:LoadValue( 'MODEL_C91', 'C91_VERSAO', xFunGetVer() ) 
		oModel:LoadValue( "MODEL_C91", "C91_NOMEVE", "S1202" )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_C91', 'C91_LOGOPE' , '2', '' )
		Endif

		If !oModel:GetModel( 'MODEL_T14' ):IsEmpty() .And. !oModel:GetModel( "MODEL_T14" ):IsDeleted()
          	
			For nT14 := 1 to oModel:GetModel( "MODEL_T14" ):Length()
              oModel:GetModel( "MODEL_T14" ):GoLine(nT14)
              oModel:LoadValue( "MODEL_T14", "T14_NOMEVE", "S1202" )
			Next nT14
       
		EndIf

		FwFormCommit( oModel )  
		
	ElseIf nOperation == MODEL_OPERATION_UPDATE 

		//�����������������������������������������������������������������Ŀ
		//�Seek para posicionar no registro antes de realizar as validacoes,�
		//�visto que quando nao esta pocisionado nao eh possivel analisar   �
		//�os campos nao usados como _STATUS                                �
		//�������������������������������������������������������������������
	    C91->( DbSetOrder( 3 ) )
	    If C91->( MsSeek( xFilial( 'C91' ) + M->C91_ID + '1' ) )
	    	    	    
			//��������������������������������Ŀ
			//�Se o registro ja foi transmitido�
			//����������������������������������
		    If C91->C91_STATUS $ ( "4" ) 
						        
				oModelC91 := oModel:GetModel( 'MODEL_C91' ) 
				oModelCRN := oModel:GetModel( 'MODEL_CRN' )    
				oModelT14 := oModel:GetModel( 'MODEL_T14' ) 
				oModelT6C := oModel:GetModel( 'MODEL_T6C' )         
				oModelT6D := oModel:GetModel( 'MODEL_T6D' )     
				oModelT6E := oModel:GetModel( 'MODEL_T6E' )  
				oModelT6F := oModel:GetModel( 'MODEL_T6F' )
				oModelT6G := oModel:GetModel( 'MODEL_T6G' )   
				
				oModelT61 := oModel:GetModel( 'MODEL_T61' )  						     						
				oModelT6H := oModel:GetModel( 'MODEL_T6H' )  
				oModelT6I := oModel:GetModel( 'MODEL_T6I' )    									    						
				oModelT6J := oModel:GetModel( 'MODEL_T6J' )     						
				oModelT6K := oModel:GetModel( 'MODEL_T6K' )    
				 										     						    						 																				
				//�����������������������������������������������������������Ŀ
				//�Busco a versao anterior do registro para gravacao do rastro�
				//�������������������������������������������������������������
				cVerAnt   	:= oModelC91:GetValue( "C91_VERSAO" )				
				cProtocolo	:= oModelC91:GetValue( "C91_PROTUL" )
				cEvento	:= oModelC91:GetValue( "C91_EVENTO" )

				If TafColumnPos( "C91_LOGOPE" )
					cLogOpeAnt := oModelC91:GetValue( "C91_LOGOPE" )
				endif

				//�����������������������������������������������������������������Ŀ
				//�Neste momento eu gravo as informacoes que foram carregadas       �
				//�na tela, pois neste momento o usuario ja fez as modificacoes que �
				//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
				//�nao devem ser consideradas neste momento                         �
				//�������������������������������������������������������������������
				/*------------------------------------------
					C91 - Folha de Pagamento  
				--------------------------------------------*/
				For nlI := 1 To 1
					For nlY := 1 To Len( oModelC91:aDataModel[ nlI ] )			
						Aadd( aGrava, { oModelC91:aDataModel[ nlI, nlY, 1 ], oModelC91:aDataModel[ nlI, nlY, 2 ] } )									
					Next
				Next 
						
				/*------------------------------------------
					CRN - Proc. Judiciario Remunera��o   
				--------------------------------------------*/
				For nlI := 1 To oModel:GetModel( 'MODEL_CRN' ):Length()				 
					oModel:GetModel( 'MODEL_CRN' ):GoLine(nlI)
					
					If !oModel:GetModel( 'MODEL_CRN' ):IsEmpty()
						If !oModel:GetModel( 'MODEL_CRN' ):IsDeleted()								
							aAdd (aGravaCRN,{	oModelCRN:GetValue('CRN_TPTRIB'),;
											 	oModelCRN:GetValue('CRN_IDPROC'),;
											  	oModelCRN:GetValue('CRN_IDSUSP')})
						EndIf
					EndIf					
				
				Next	
					
				/*------------------------------------------
					T14 - Ident. Demonstr. Val. Trabal. 
				--------------------------------------------*/
				For nT14 := 1 To oModel:GetModel( 'MODEL_T14' ):Length() 
					oModel:GetModel( 'MODEL_T14' ):GoLine(nT14)
					
					If !oModel:GetModel( 'MODEL_T14' ):IsEmpty()
						If !oModel:GetModel( 'MODEL_T14' ):IsDeleted()								
							aAdd (aGravaT14,{	oModelC91:GetValue('C91_INDAPU'),;
												oModelC91:GetValue('C91_PERAPU'),;
												oModelC91:GetValue('C91_TRABAL'),;
										       oModelT14:GetValue('T14_IDEDMD')})
									  
										  
						/*------------------------------------------
							T6C - Identifica��o do Estabelecimen
						--------------------------------------------*/			  		
						For nT6C := 1 to oModel:GetModel( "MODEL_T6C" ):Length()
							oModel:GetModel( "MODEL_T6C" ):GoLine(nT6C)
							
							If !oModel:GetModel( 'MODEL_T6C' ):IsEmpty()	
								If !oModel:GetModel( "MODEL_T6C" ):IsDeleted()
									aAdd (aGravaT6C,{oModelC91:GetValue('C91_INDAPU'),;
														oModelC91:GetValue('C91_PERAPU'),;
												       oModelC91:GetValue('C91_TRABAL'),;
														oModelT14:GetValue('T14_IDEDMD'),;
														oModelT6C:GetValue('T6C_ESTABE')}) 
										
										
										
										/*------------------------------------------
											T6D - Remun Trabal. Per�odo Apura��o
										--------------------------------------------*/			  		
										For nT6D := 1 to oModel:GetModel( "MODEL_T6D" ):Length()
											oModel:GetModel( "MODEL_T6D" ):GoLine(nT6D)
											
											If !oModel:GetModel( 'MODEL_T6D' ):IsEmpty()	
												If !oModel:GetModel( "MODEL_T6D" ):IsDeleted()
													aAdd (aGravaT6D ,{oModelC91:GetValue('C91_INDAPU'),;
																		oModelC91:GetValue('C91_PERAPU'),;
																		oModelC91:GetValue('C91_TRABAL'),; 
																		oModelT14:GetValue('T14_IDEDMD'),;
																		oModelT6C:GetValue('T6C_ESTABE'),;
																	   	oModelT6D:GetValue('T6D_IDTRAB'),;
																	   	oModelT6D:GetValue('T6D_CODCAT')})
											
													/*------------------------------------------
														T6E - Itens Remun. do Trabalhador   
													--------------------------------------------*/
													For nT6E := 1 to oModel:GetModel( "MODEL_T6E" ):Length()
														oModel:GetModel( "MODEL_T6E" ):GoLine(nT6E)
														
														If !oModel:GetModel( 'MODEL_T6E' ):IsEmpty()	
															If !oModel:GetModel( "MODEL_T6E" ):IsDeleted()
																aAdd (aGravaT6E ,{oModelC91:GetValue('C91_INDAPU'),;
																					oModelC91:GetValue('C91_PERAPU'),;
																					oModelC91:GetValue('C91_TRABAL'),;
																					oModelT14:GetValue('T14_IDEDMD'),;
																					oModelT6C:GetValue('T6C_ESTABE'),;
																					oModelT6D:GetValue('T6D_IDTRAB'),;
																					oModelT6D:GetValue('T6D_CODCAT'),;
																				   	oModelT6E:GetValue('T6E_IDRUBR'),;
																				   	oModelT6E:GetValue('T6E_QTDRUB'),;
																				   	oModelT6E:GetValue('T6E_FATORR'),;
																				   	oModelT6E:GetValue('T6E_VLRUNT'),;
																				   	oModelT6E:GetValue('T6E_VLRRUB')}) 
															EndIf
														EndIf
													Next //nT6E						
													
												
													/*------------------------------------------
														 T6F - Informa��es do Plano de Sa�de 
													--------------------------------------------*/
													For nT6F := 1 to oModel:GetModel( "MODEL_T6F" ):Length()
														oModel:GetModel( "MODEL_T6F" ):GoLine(nT6F)
														
														If !oModel:GetModel( 'MODEL_T6F' ):IsEmpty()	
															If !oModel:GetModel( "MODEL_T6F" ):IsDeleted()
																aAdd (aGravaT6F ,{oModelC91:GetValue('C91_INDAPU'),;
																					oModelC91:GetValue('C91_PERAPU'),;
																					oModelC91:GetValue('C91_TRABAL'),;
																					oModelT14:GetValue('T14_IDEDMD'),;
																					oModelT6C:GetValue('T6C_ESTABE'),;
																					oModelT6D:GetValue('T6D_IDTRAB'),;
																					oModelT6D:GetValue('T6D_CODCAT'),;
																				   	oModelT6F:GetValue('T6F_CNPJOP'),;
																				   	oModelT6F:GetValue('T6F_REGANS'),;
																				   	oModelT6F:GetValue('T6F_VLPGTI')})
											
											
																/*------------------------------------------
																 	T6G - Informa��es dos Dependentes
																--------------------------------------------*/				  
																For nT6G := 1 to oModel:GetModel( "MODEL_T6G" ):Length()
																	oModel:GetModel( "MODEL_T6G" ):GoLine(nT6G)
																
																	If !oModel:GetModel( 'MODEL_T6G' ):IsEmpty()	
																		If !oModel:GetModel( "MODEL_T6G" ):IsDeleted()
																			aAdd (aGravaT6G ,{oModelC91:GetValue('C91_INDAPU'),;	//1
																								oModelC91:GetValue('C91_PERAPU'),;	//2 
																								oModelC91:GetValue('C91_TRABAL'),;	//3
																								oModelT14:GetValue('T14_IDEDMD'),;	//4
																								oModelT6C:GetValue('T6C_ESTABE'),;	//5
																								oModelT6D:GetValue('T6D_IDTRAB'),;	//6
																								oModelT6D:GetValue('T6D_CODCAT'),;	//7
																								oModelT6F:GetValue('T6F_CNPJOP'),;	//8
																							   	oModelT6G:GetValue('T6G_CPFDEP'),;	//9
																							   	oModelT6G:GetValue('T6G_NOMDEP'),;	//10
																							   	oModelT6G:GetValue('T6G_DTNDEP'),;	//11
																							   	oModelT6G:GetValue('T6G_VPGDEP'),;	//12 
																							   	oModelT6G:GetValue('T6G_TPDEP')})   //13
																		EndIf
																	EndIf	
																Next //nT6G 						
													
															EndIf
														EndIf
													Next //nT6F
																					
												EndIf
											EndIf
										Next //nT6D		
							
									EndIf
								EndIf
							Next //nT6C		
							
							/*------------------------------------------
								T61 - Ident Lei  Remun. Per.  Anter.
							--------------------------------------------*/
							For nT61 := 1 To oModel:GetModel( 'MODEL_T61' ):Length() 
								oModel:GetModel( 'MODEL_T61' ):GoLine(nT61)
								If !oModel:GetModel( 'MODEL_T61' ):IsEmpty()
									If !oModel:GetModel( 'MODEL_T61' ):IsDeleted()								
										aAdd (aGravaT61 ,{oModelC91:GetValue('C91_INDAPU'),;
											 				oModelC91:GetValue('C91_PERAPU'),;
											 				oModelC91:GetValue('C91_TRABAL'),;
											 				oModelT14:GetValue('T14_IDEDMD'),;
													       oModelT61:GetValue('T61_DTLEI'),;
													       oModelT61:GetValue('T61_NUMLEI'),;
													    	oModelT61:GetValue('T61_DTEFET')})
										
										/*------------------------------------
											T6H - Informa��es do Periodo
										-------------------------------------*/			  		
										For nT6H := 1 to oModel:GetModel( "MODEL_T6H" ):Length()
											oModel:GetModel( "MODEL_T6H" ):GoLine(nT6H)
											If !oModel:GetModel( 'MODEL_T6H' ):IsEmpty()	
												If !oModel:GetModel( "MODEL_T6H" ):IsDeleted()
													aAdd(aGravaT6H ,{oModelC91:GetValue('C91_INDAPU'),;
																	   	oModelC91:GetValue('C91_PERAPU'),;
																	   	oModelC91:GetValue('C91_TRABAL'),;
																	   	oModelT14:GetValue('T14_IDEDMD'),;
																	 	oModelT61:GetValue('T61_DTLEI'),;
																	   	oModelT61:GetValue('T61_NUMLEI'),;
																	   	oModelT6H:GetValue('T6H_PERREF')})
																	  
													/*------------------------------------
														T6I - Ident. do Estabelecimento
													-------------------------------------*/			  		
													For nT6I := 1 to oModel:GetModel( "MODEL_T6I" ):Length()
														oModel:GetModel( "MODEL_T6I" ):GoLine(nT6I)
														If !oModel:GetModel( 'MODEL_T6I' ):IsEmpty()	
															If !oModel:GetModel( "MODEL_T6I" ):IsDeleted()
																aAdd (aGravaT6I ,{oModelC91:GetValue('C91_INDAPU'),;
																					oModelC91:GetValue('C91_PERAPU'),;
																					oModelC91:GetValue('C91_TRABAL'),;
																					oModelT14:GetValue('T14_IDEDMD'),;
																					oModelT61:GetValue('T61_DTLEI'),;
																					oModelT61:GetValue('T61_NUMLEI'),;
																					oModelT6H:GetValue('T6H_PERREF'),;
																				  	oModelT6I:GetValue('T6I_ESTABE')})
															  
																/*------------------------------------------
																	T6J - Informa��es da Remunera��o Trab.
																--------------------------------------------*/			  		
																For nT6J := 1 to oModel:GetModel( "MODEL_T6J" ):Length()
																	oModel:GetModel( "MODEL_T6J" ):GoLine(nT6J)
																	If !oModel:GetModel( 'MODEL_T6J' ):IsEmpty()	
																		If !oModel:GetModel( "MODEL_T6J" ):IsDeleted()
																			aAdd (aGravaT6J ,{oModelC91:GetValue('C91_INDAPU'),;
																								oModelC91:GetValue('C91_PERAPU'),;
																								oModelC91:GetValue('C91_TRABAL'),; 
																								oModelT14:GetValue('T14_IDEDMD'),;
																								oModelT61:GetValue('T61_DTLEI'),;
																								oModelT61:GetValue('T61_NUMLEI'),;
																								oModelT6H:GetValue('T6H_PERREF'),;
																								oModelT6I:GetValue('T6I_ESTABE'),;
																	   						   	oModelT6J:GetValue('T6J_IDTRAB'),;
																	                        	oModelT6J:GetValue('T6J_CODCAT')})
											
																
																			/*------------------------------------------
																				 T6K - Itens da Remunera��o Trab.
																			--------------------------------------------*/
																			For nT6K := 1 to oModel:GetModel( "MODEL_T6K" ):Length()
																				oModel:GetModel( "MODEL_T6K" ):GoLine(nT6K)
																				If !oModel:GetModel( 'MODEL_T6K' ):IsEmpty()	
																					If !oModel:GetModel( "MODEL_T6K" ):IsDeleted()
																						aAdd (aGravaT6K ,{oModelC91:GetValue('C91_INDAPU'),;
																											oModelC91:GetValue('C91_PERAPU'),;
																											oModelC91:GetValue('C91_TRABAL'),;
																											oModelT14:GetValue('T14_IDEDMD'),;
																											oModelT61:GetValue('T61_DTLEI'),;
																											oModelT61:GetValue('T61_NUMLEI'),;
																											oModelT6H:GetValue('T6H_PERREF'),;
																											oModelT6I:GetValue('T6I_ESTABE'),;
																											oModelT6J:GetValue('T6J_IDTRAB'),;
																											oModelT6J:GetValue('T6J_CODCAT'),;
																											oModelT6K:GetValue('T6K_IDRUBR'),;
																		    				              oModelT6K:GetValue('T6K_QTDRUB'),;
																		    						       oModelT6K:GetValue('T6K_FATORR'),;
																		    						       oModelT6K:GetValue('T6K_VLRUNT'),;
																		    						       oModelT6K:GetValue('T6K_VLRRUB')}) 
																					EndIf
																				EndIf
																			Next //nT6K
															
																		EndIf
																	EndIf
																Next //nT6J
																
															EndIf
														EndIf
													Next //nT6I																	
												
												EndIf
											EndIf
										Next //nT6H			
									
									EndIf
								EndIf
							Next // nT61	
						
						EndIf
					EndIf
				Next //nT14									
							  				
				//�����������������������������������������������������������Ŀ
				//�Seto o campo como Inativo e gravo a versao do novo registro�
				//�no registro anterior                                       � 
				//|                                                           |
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������
				FAltRegAnt( 'C91', '2' )
			
				//��������������������������������������������������Ŀ
				//�Neste momento eu preciso setar a operacao do model�
				//�como Inclusao                                     �
				//����������������������������������������������������
				oModel:DeActivate()
				oModel:SetOperation( 3 ) 	
				oModel:Activate()		
								
				//�������������������������������������������������������Ŀ
				//�Neste momento eu realizo a inclusao do novo registro ja�
				//�contemplando as informacoes alteradas pelo usuario     �
				//���������������������������������������������������������
				/*------------------------------------------
					C91 - Folha de Pagamento  
				--------------------------------------------*/
				For nlI := 1 To Len( aGrava )	
					oModel:LoadValue( 'MODEL_C91', aGrava[ nlI, 1 ], aGrava[ nlI, 2 ] )
				Next

				//Necess�rio Abaixo do For Nao Retirar
				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_C91', 'C91_LOGOPE' , '' , cLogOpeAnt )	
				EndIf

				/*------------------------------------------
					CRN - Proc. Judiciario Remunera��o   
				--------------------------------------------*/				
				For nlI := 1 To Len( aGravaCRN )
					If nlI > 1
						oModel:GetModel( 'MODEL_CRN' ):AddLine()
					EndIf
					oModel:LoadValue( "MODEL_CRN", "CRN_TPTRIB" ,	aGravaCRN[nlI][1] )
					oModel:LoadValue( "MODEL_CRN", "CRN_IDPROC" ,	aGravaCRN[nlI][2] )
					oModel:LoadValue( "MODEL_CRN", "CRN_IDSUSP" ,	aGravaCRN[nlI][3] )
				Next			
				
				/*------------------------------------------
					T14 - Ident. Demonstr. Val. Trabal. 
				--------------------------------------------*/
				For nT14 := 1 to Len( aGravaT14 )
								
					oModel:GetModel( 'MODEL_T14' ):LVALID	:= .T.
					
					If nT14 > 1
						oModel:GetModel( "MODEL_T14" ):AddLine()
					EndIf
					
					oModel:LoadValue( "MODEL_T14", "T14_IDEDMD", aGravaT14[nT14][4] )
					oModel:LoadValue( "MODEL_T14", "T14_NOMEVE" ,	"S1202" )
					
					/*------------------------------------------
					   T6C - Informa��es da Remunera��o Trab.
					--------------------------------------------*/
					nT6CAdd := 1	
					For nT6C := 1 to Len( aGravaT6C )		
						If aGravaT14[nT14][1]+aGravaT14[nT14][2]+aGravaT14[nT14][3]+aGravaT14[nT14][4] == aGravaT6C[nT6C][1]+aGravaT6C[nT6C][2]+aGravaT6C[nT6C][3]+aGravaT6C[nT6C][4]
							oModel:GetModel( 'MODEL_T6C' ):LVALID := .T.
							
							If nT6CAdd > 1
								oModel:GetModel( "MODEL_T6C" ):AddLine()
							EndIf
							
							oModel:LoadValue( "MODEL_T6C", "T6C_ESTABE", aGravaT6C[nT6C][5] )

							/*------------------------------------------
							   T6D - Informa��es da Remunera��o Trab.
							--------------------------------------------*/
							nT6DAdd := 1	
							For nT6D := 1 to Len( aGravaT6D )		
								If aGravaT6C[nT6C][1]+aGravaT6C[nT6C][2]+aGravaT6C[nT6C][3]+aGravaT6C[nT6C][4]+aGravaT6C[nT6C][5] == aGravaT6D[nT6D][1]+aGravaT6D[nT6D][2]+aGravaT6D[nT6D][3]+aGravaT6D[nT6D][4]+aGravaT6D[nT6D][5]
									oModel:GetModel( 'MODEL_T6D' ):LVALID := .T.
									
									If nT6DAdd > 1
										oModel:GetModel( "MODEL_T6D" ):AddLine()
									EndIf
									
									oModel:LoadValue( "MODEL_T6D", "T6D_IDTRAB", aGravaT6D[nT6D][6] )
									oModel:LoadValue( "MODEL_T6D", "T6D_CODCAT", aGravaT6D[nT6D][7] )
								
									/*------------------------------------------
									   	  T6E - Itens da Remunera��o Trab.
									--------------------------------------------*/
									nT6EAdd := 1	
									For nT6E := 1 to Len( aGravaT6E )		
										If aGravaT6D[nT6D][1]+aGravaT6D[nT6D][2]+aGravaT6D[nT6D][3]+aGravaT6D[nT6D][4]+aGravaT6D[nT6D][5]+aGravaT6D[nT6D][6]+aGravaT6D[nT6D][7] == aGravaT6E[nT6E][1]+aGravaT6E[nT6E][2]+aGravaT6E[nT6E][3]+aGravaT6E[nT6E][4]+aGravaT6E[nT6E][5]+aGravaT6E[nT6E][6]+aGravaT6E[nT6E][7]
											oModel:GetModel( 'MODEL_T6E' ):LVALID := .T.
											
											If nT6EAdd > 1
												oModel:GetModel( "MODEL_T6E" ):AddLine()
											EndIf
											
											oModel:LoadValue( "MODEL_T6E", "T6E_IDRUBR",	aGravaT6E[nT6E][8] )
											oModel:LoadValue( "MODEL_T6E", "T6E_QTDRUB",	aGravaT6E[nT6E][9] )
											oModel:LoadValue( "MODEL_T6E", "T6E_FATORR",	aGravaT6E[nT6E][10] )
											oModel:LoadValue( "MODEL_T6E", "T6E_VLRUNT",	aGravaT6E[nT6E][11] )
											oModel:LoadValue( "MODEL_T6E", "T6E_VLRRUB",	aGravaT6E[nT6E][12] )
											
											nT6EAdd++
											
										EndIf			
									Next //nT6E
								
									/*------------------------------------------
									   	  T6F - Informa��es do Plano de Sa�de 
									--------------------------------------------*/
									nT6FAdd := 1	
									For nT6F := 1 to Len( aGravaT6F )		
										If aGravaT6D[nT6D][1]+aGravaT6D[nT6D][2]+aGravaT6D[nT6D][3]+aGravaT6D[nT6D][4]+aGravaT6D[nT6D][5]+aGravaT6D[nT6D][6]+aGravaT6D[nT6D][7] == aGravaT6F[nT6F][1]+aGravaT6F[nT6F][2]+aGravaT6F[nT6F][3]+aGravaT6F[nT6F][4]+aGravaT6F[nT6F][5]+aGravaT6F[nT6F][6]+aGravaT6F[nT6F][7]
											oModel:GetModel( 'MODEL_T6F' ):LVALID := .T.
											
											If nT6FAdd > 1
												oModel:GetModel( "MODEL_T6F" ):AddLine()
											EndIf
											
											oModel:LoadValue( "MODEL_T6F", "T6F_CNPJOP",	aGravaT6F[nT6F][8] )
											oModel:LoadValue( "MODEL_T6F", "T6F_REGANS",	aGravaT6F[nT6F][9] )
											oModel:LoadValue( "MODEL_T6F", "T6F_VLPGTI",	aGravaT6F[nT6F][10] )
											
											/*------------------------------------------
											   	  T6G - Informa��es dos Dependentes
											--------------------------------------------*/
											nT6GAdd := 1	
											For nT6G := 1 to Len( aGravaT6G )		
												If aGravaT6F[nT6F][1]+aGravaT6F[nT6F][2]+aGravaT6F[nT6F][3]+aGravaT6F[nT6F][4]+aGravaT6F[nT6F][5]+aGravaT6F[nT6F][6]+aGravaT6F[nT6F][7]+aGravaT6F[nT6F][8] == aGravaT6G[nT6G][1]+aGravaT6G[nT6G][2]+aGravaT6G[nT6G][3]+aGravaT6G[nT6G][4]+aGravaT6G[nT6G][5]+aGravaT6G[nT6G][6]+aGravaT6G[nT6G][7]+aGravaT6G[nT6G][8]
													oModel:GetModel( 'MODEL_T6G' ):LVALID := .T.
													
													If nT6GAdd > 1
														oModel:GetModel( "MODEL_T6G" ):AddLine()
													EndIf
													
													oModel:LoadValue( "MODEL_T6G", "T6G_CPFDEP",	aGravaT6G[nT6G][9]  )
													oModel:LoadValue( "MODEL_T6G", "T6G_NOMDEP",	aGravaT6G[nT6G][10] )
													oModel:LoadValue( "MODEL_T6G", "T6G_DTNDEP",	aGravaT6G[nT6G][11] )
													oModel:LoadValue( "MODEL_T6G", "T6G_VPGDEP",	aGravaT6G[nT6G][12] )
													oModel:LoadValue( "MODEL_T6G", "T6G_TPDEP" ,	aGravaT16[nT16][13] )
													nT6GAdd++		
												EndIf			
											Next //nT6G													
								
											nT6FAdd++		
										EndIf			
									Next //nT6F
																	
									nT6DAdd++		
								EndIf			
							Next //nT6D
						
							nT6CAdd++		
						EndIf		
					Next //nT6C
				
				
					/*------------------------------------------
						T61 - Ident Lei  Remun. Per.  Anter.
					--------------------------------------------*/
					For nT61 := 1 to Len( aGravaT61 )
					
						If aGravaT14[nT14][1]+aGravaT14[nT14][2]+aGravaT14[nT14][3]+aGravaT14[nT14][4] == aGravaT61[nT61][1]+aGravaT61[nT61][2]+aGravaT61[nT61][3]+aGravaT61[nT61][4]								
							oModel:GetModel( 'MODEL_T61' ):LVALID	:= .T.
							
							If nT61 > 1
								oModel:GetModel( "MODEL_T61" ):AddLine()
							EndIf
							
							oModel:LoadValue( "MODEL_T61", "T61_DTLEI ", aGravaT61[nT61][5] )
							oModel:LoadValue( "MODEL_T61", "T61_NUMLEI", aGravaT61[nT61][6] )
							oModel:LoadValue( "MODEL_T61", "T61_DTEFET", aGravaT61[nT61][7] )
							
							/*------------------------------------------
							   T6H - Informa��es do Periodo
							--------------------------------------------*/
							nT6HAdd := 1	
							For nT6H := 1 to Len( aGravaT6H )		
								If aGravaT61[nT61][1]+aGravaT61[nT61][2]+aGravaT61[nT61][3]+aGravaT61[nT61][4]+DtoC(aGravaT61[nT61][5])+aGravaT61[nT61][6] == aGravaT6H[nT6H][1]+aGravaT6H[nT6H][2]+aGravaT6H[nT6H][3]+aGravaT6H[nT6H][4]+DtoC(aGravaT6H[nT6H][5])+aGravaT6H[nT6H][6]
									oModel:GetModel( 'MODEL_T6H' ):LVALID := .T.
									
									If nT6HAdd > 1
										oModel:GetModel( "MODEL_T6H" ):AddLine()
									EndIf
									
									oModel:LoadValue( "MODEL_T6H", "T6H_PERREF", aGravaT6H[nT6H][7] )
									
									/*------------------------------------------
									   T6I - Ident. do Estabelecimento
									--------------------------------------------*/
									nT6IAdd := 1	
									For nT6I := 1 to Len( aGravaT6I )		
										If aGravaT6H[nT6H][1]+aGravaT6H[nT6H][2]+aGravaT6H[nT6H][3]+aGravaT6H[nT6H][4]+DtoC(aGravaT6H[nT6H][5])+aGravaT6H[nT6H][6]+aGravaT6H[nT6H][7] == aGravaT6I[nT6I][1]+aGravaT6I[nT6I][2]+aGravaT6I[nT6I][3]+aGravaT6I[nT6I][4]+DtoC(aGravaT6I[nT6I][5])+aGravaT6I[nT6I][6]+aGravaT6I[nT6I][7]
											oModel:GetModel( 'MODEL_T6I' ):LVALID := .T.
											
											If nT6IAdd > 1
												oModel:GetModel( "MODEL_T6I" ):AddLine()
											EndIf
											
											oModel:LoadValue( "MODEL_T6I", "T6I_ESTABE", aGravaT6I[nT6I][8] )
							
											/*------------------------------------------
											   T6J - Informa��es da Remunera��o Trab.
											--------------------------------------------*/
											nT6JAdd := 1	
											For nT6J := 1 to Len( aGravaT6J )		
												If aGravaT6I[nT6I][1]+aGravaT6I[nT6I][2]+aGravaT6I[nT6I][3]+aGravaT6I[nT6I][4]+DtoC(aGravaT6I[nT6I][5])+aGravaT6I[nT6I][6]+aGravaT6I[nT6I][7] == aGravaT6J[nT6J][1]+aGravaT6J[nT6J][2]+aGravaT6J[nT6J][3]+aGravaT6J[nT6J][4]+DtoC(aGravaT6J[nT6J][5])+aGravaT6J[nT6J][6]+aGravaT6J[nT6J][7]
													oModel:GetModel( 'MODEL_T6J' ):LVALID := .T.
													
													If nT6JAdd > 1
														oModel:GetModel( "MODEL_T6J" ):AddLine()
													EndIf
													
													oModel:LoadValue( "MODEL_T6J", "T6J_IDTRAB", aGravaT6J[nT6J][8] )
													oModel:LoadValue( "MODEL_T6J", "T6J_CODCAT", aGravaT6J[nT6J][9] )
													
													/*------------------------------------------
													   	  T6K - Itens da Remunera��o Trab.
													--------------------------------------------*/
													nT6KAdd := 1	
													For nT6K := 1 to Len( aGravaT6K )		
														If aGravaT6J[nT6J][1]+aGravaT6J[nT6J][2]+aGravaT6J[nT6J][3]+aGravaT6J[nT6J][4]+DtoC(aGravaT6J[nT6J][5])+aGravaT6J[nT6J][6]+aGravaT6J[nT6J][7]+aGravaT6J[nT6J][8]+aGravaT6J[nT6J][9]+aGravaT6J[nT6J][10] == aGravaT6K[nT6K][1]+aGravaT6K[nT6K][2]+aGravaT6K[nT6K][3]+aGravaT6K[nT6K][4]+DtoC(aGravaT6K[nT6K][5])+aGravaT6K[nT6K][6]+aGravaT6K[nT6K][7]+aGravaT6K[nT6K][8]+aGravaT6K[nT6K][9]+aGravaT6K[nT6K][10]
															oModel:GetModel( 'MODEL_T6K' ):LVALID := .T.
															
															If nT6KAdd > 1
																oModel:GetModel( "MODEL_T6K" ):AddLine()
															EndIf
															
															oModel:LoadValue( "MODEL_T6K", "T6K_IDRUBR",	aGravaT6K[nT6K][11] )
															oModel:LoadValue( "MODEL_T6K", "T6K_QTDRUB",	aGravaT6K[nT6K][12] )
															oModel:LoadValue( "MODEL_T6K", "T6K_FATORR",	aGravaT6K[nT6K][13] )
															oModel:LoadValue( "MODEL_T6K", "T6K_VLRUNT",	aGravaT6K[nT6K][14] )
															oModel:LoadValue( "MODEL_T6K", "T6K_VLRRUB",	aGravaT6K[nT6K][15] )
															
															nT6KAdd++															
														EndIf			
													Next //nT6K
															
													nT6JAdd++		
												EndIf			
											Next //nT6J
											
											nT6IAdd++
										EndIf			
									Next //nT6I
						
									nT6HAdd++
								EndIf			
							Next //nT6H
					
							nT61Add++
						EndIf					
					Next //nT61
					
				Next //nT14
						 			
																							
				//�������������������������������Ŀ
				//�Busco a versao que sera gravada�
				//���������������������������������
				cVersao := xFunGetVer()		 
				                                   
				//�����������������������������������������������������������Ŀ		
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������		                                                                      				         
				oModel:LoadValue( 'MODEL_C91', 'C91_VERSAO', cVersao )
				oModel:LoadValue( 'MODEL_C91', 'C91_VERANT', cVerAnt )
				oModel:LoadValue( 'MODEL_C91', 'C91_PROTPN', cProtocolo )
				oModel:LoadValue( 'MODEL_C91', 'C91_PROTUL', "" )
				oModel:LoadValue( 'MODEL_C91', 'C91_EVENTO', "A" )
				oModel:LoadValue( "MODEL_C91", 'C91_NOMEVE',	"S1202" )
				// Tratamento para limpar o ID unico do xml
				cAliasPai := "C91"
				If TAFColumnPos( cAliasPai+"_XMLID" )
					oModel:LoadValue( 'MODEL_'+cAliasPai, cAliasPai+'_XMLID', "" )
				EndIf
				
				FwFormCommit( oModel )
				TAFAltStat( 'C91', " " )
	
			ElseIf C91->C91_STATUS == ( "2" )
				TAFMsgVldOp(oModel,"2")//"Registro n�o pode ser alterado. Aguardando processo da transmiss�o."
				lRetorno := .F.
			Elseif C91->C91_STATUS == ( "6" )
				TAFMsgVldOp(oModel,"6")//"Registro n�o pode ser alterado. Aguardando proc. Transm. evento de Exclus�o S-3000"
				lRetorno := .F.
			Elseif C91->C91_STATUS == "7"
				TAFMsgVldOp(oModel,"7") //"Registro n�o pode ser alterado, pois o evento j� se encontra na base do RET"  
				lRetorno:= .F.
			Else
				//altera��o sem transmiss�o
				If TafColumnPos( "C91_LOGOPE" )
					cLogOpeAnt := C91->C91_LOGOPE
				endif

				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_C91', 'C91_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				FwFormCommit( oModel )
				TAFAltStat( 'C91', " " )  
			EndIf
		EndIf	
	
	ElseIf nOperation == MODEL_OPERATION_DELETE 
	
		cChvRegAnt := C91->(C91_ID + C91_VERANT)              
											
		If !Empty( cChvRegAnt ) 
			TAFAltStat( 'C91', " " )
			FwFormCommit( oModel )				
			If nOperation == MODEL_OPERATION_DELETE
				If C91->C91_EVENTO == "A" .Or. C91->C91_EVENTO == "E"
					TAFRastro( 'C91', 1, cChvRegAnt, .T. , , IIF(Type("oBrw") == "U", Nil, oBrw) )
				EndIf
			EndIf
		Else
			oModel:DeActivate()
			oModel:SetOperation( 5 ) 	
			oModel:Activate()
			FwFormCommit( oModel )
		EndIf
		
	EndIf
		    			
End Transaction 

If !lRetorno 
	// Define a mensagem de erro que ser� exibida ap�s o Return do SaveModel
	TAFMsgDel(oModel,.T.)
EndIf


Return ( lRetorno ) 
//-------------------------------------------------------------------
/*/{Protheus.doc} TAF413Xml
Funcao de geracao do XML para atender o registro S-1200
Quando a rotina for chamada o registro deve estar posicionado

@Param:

@Return:
cXml - Estrutura do Xml do Layout S-2320

@author Vitor Siqueira
@since 08/01/2016
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAF413Xml( cAlias, nRecno, nOpc, lJob, lAutomato, cFile )

Local cXml		 	:= ""
Local cXmlItRem	    := ""
Local cXmlEstab	    := ""
Local cXmlRemPAnt	:= ""
Local cLayout	 	:= "1202"
Local cReg		 	:= "RmnRPPS"
Local cIdTabRub  	:= ""
Local cCodSusp	    := ""
Local lXmlVLd		:= IIF(FindFunction('TafXmlVLD'),TafXmlVLD('TAF413XML'),.T.)

Local nQtdSF	 	:= 0
Local nQtdIRRF	    := 0

Local aMensal	 	:= {}

Default cAlias	    :=	""
Default nRecno	    :=	0
Default nOpc		:=	1
Default lJob		:=	.F.
Default lAutomato	:=	.F.
Default cFile		:=	""

 If  lXmlVLd

	If  C91->C91_EVENTO $ "I|A" 

		AADD(aMensal,C91->C91_INDAPU) 
		
		If Len(Alltrim(C91->C91_PERAPU)) <= 4
			AADD(aMensal,C91->C91_PERAPU)  
		Else
			AADD(aMensal,substr(C91->C91_PERAPU, 1, 4) + '-' + substr(C91->C91_PERAPU, 5, 2) )
		EndIf   
			
		/*----------------------------------------------------------
				Inicio da TAG ideTrabalhador
		----------------------------------------------------------*/		
			
		C9V->( DbSetOrder( 1 ) )
		C9V->( MsSeek ( xFilial("C9V")+C91->C91_TRABAL) )    	
		
		cXml +=	"<ideTrabalhador>"
	
		cXml +=		xTafTag("cpfTrab",C9V->C9V_CPF)
		cXml +=		xTafTag("nisTrab",C9V->C9V_NIS,,.T.)
																
		C9Y->( DbSetOrder( 1 ) )
											
			If C9Y->( MsSeek ( xFilial("C9Y")+C9V->(C9V_ID+C9V_VERSAO)))    				
				While !C9Y->(Eof()) .And. AllTrim(C9V->(C9V_ID+C9V_VERSAO)) == AllTrim(C9Y->(C9Y_ID+C9Y_VERSAO))	    			
					nQtdSF++        
					If C9Y->C9Y_DEPIRF == "1"
						nQtdIRRF++
					EndIf							
				C9Y->(DbSkip())
				EndDo		
			EndIf
			
			cXml +=		xTafTag("qtdDepFP",nQtdIRRF,,.T.)
				
			CRN->( DbSetOrder( 1 ) )	                            
			If CRN->( MsSeek ( xFilial("CRN")+C91->(C91_ID+C91_VERSAO)))    				
				While !CRN->(Eof()) .And. AllTrim(C91->(C91_ID+C91_VERSAO)) == AllTrim(CRN->(CRN_ID+CRN_VERSAO))	 	
						
					cXml += "<procJudTrab>"
					cXml +=		xTafTag( "tpTrib" , CRN->CRN_TPTRIB , , .F. )			

					C1G->(DbSetOrder(8))
					C1G->(MsSeek(xFilial("C1G") + CRN->CRN_IDPROC + "1"))
					cXml +=		xTafTag( "nrProcJud" , C1G->C1G_NUMPRO , , .F. )

					cCodSusp    := Posicione("T5L",1,xFilial("T5L")+CRN->CRN_IDSUSP,"T5L_CODSUS")
					If !Empty(cCodSusp) 
					cXml += xTafTag( "codSusp" , Alltrim( cCodSusp ) , , .T. ) 
					EndIf					
					cXml +="</procJudTrab>"				
				
					CRN->(DbSkip())
			
				EndDo 	       	      
			EndIf	
			
			cXml +=	"</ideTrabalhador>"
			
			/*----------------------------------------------------------
					Inicio da TAG dmDev
			----------------------------------------------------------*/
			T14->( DbSetOrder( 1 ) )	                            
			If T14->( MsSeek ( xFilial("T14")+C91->(C91_ID+C91_VERSAO+C91_INDAPU+C91_PERAPU+C91_TRABAL)))   					
				While !T14->(Eof()) .And. AllTrim(C91->(C91_ID+C91_VERSAO+C91_INDAPU+C91_PERAPU+C91_TRABAL)) == AllTrim(T14->(T14_ID+T14_VERSAO+T14_INDAPU+T14_PERAPU+T14_TRABAL))		

					cXml +=	"<dmDev>"	
					
					cXml +=	xTafTag( "ideDmDev" , T14->T14_IDEDMD , , .F. )	
			
					/*----------------------------------------------------------
							Inicio da TAG infoPerApur
					----------------------------------------------------------*/
					T6C->( DbSetOrder( 1 ) )	                            
					If T6C->( MsSeek ( xFilial("T6C")+T14->(T14_ID+T14_VERSAO+T14_INDAPU+T14_PERAPU+T14_TRABAL+T14_IDEDMD)))   
						cXml +=	"<infoPerApur>"	 				
						
						/*----------------------------------------------------------
							Inicio da TAG ideEstab
						----------------------------------------------------------*/
						While !T6C->(Eof()) .And. AllTrim(T14->(T14_ID+T14_VERSAO+T14_INDAPU+T14_PERAPU+T14_TRABAL+T14_IDEDMD)) == AllTrim(T6C->(T6C_ID+T6C_VERSAO+T6C_INDAPU+T6C_PERAPU+T6C_TRABAL+T6C_DEMPAG))						
							cXml +=	"<ideEstab>"				
							cXml +=		xTafTag("tpInsc", POSICIONE("C92",1, xFilial("C92")+T6C->T6C_ESTABE,"C92_TPINSC") , , .F. )
							cXml +=		xTafTag("nrInsc", POSICIONE("C92",1, xFilial("C92")+T6C->T6C_ESTABE,"C92_NRINSC") , , .F. )

							/*----------------------------------------------------------
								Inicio da TAG remunPerApur
							----------------------------------------------------------*/									
							T6D->( DbSetOrder( 1 ) )	                            
							If T6D->( MsSeek ( xFilial("T6D")+T6C->(T6C_ID+T6C_VERSAO+T6C_INDAPU+T6C_PERAPU+T6C_TRABAL+T6C_DEMPAG+T6C_ESTABE)) )    				
								While !T6D->(Eof()) .And. AllTrim(T6C->(T6C_ID+T6C_VERSAO+T6C_INDAPU+T6C_PERAPU+T6C_TRABAL+T6C_DEMPAG+T6C_ESTABE)) == AllTrim(T6D->(T6D_ID+T6D_VERSAO+T6D_INDAPU+T6D_PERAPU+T6D_TRABAL+T6D_DEMPAG+T6D_ESTABE))	 
																																			
									cXml +=	"<remunPerApur>"
									cXml +=		xTafTag("matricula",POSICIONE("C9V",1, xFilial("C9V")+T6D->T6D_IDTRAB,"C9V_MATRIC"),,.T.)		
									cXml +=		xTafTag("codCateg" ,POSICIONE("C87",1, xFilial("C87")+T6D->T6D_CODCAT,"C87_CODIGO"),,.F.)									
						
									/*----------------------------------------------------------
										Inicio da TAG itensRemun
									----------------------------------------------------------*/		
									T6E->( DbSetOrder( 1 ) )	                            
									If T6E->( MsSeek ( xFilial("T6E")+T6D->(T6D_ID+T6D_VERSAO+T6D_INDAPU+T6D_PERAPU+T6D_TRABAL+T6D_DEMPAG+T6D_ESTABE+T6D_IDTRAB+T6D_CODCAT)))
										While !T6E->(Eof()) .And. AllTrim(T6D->(T6D_ID+T6D_VERSAO+T6D_INDAPU+T6D_PERAPU+T6D_TRABAL+T6D_DEMPAG+T6D_ESTABE+T6D_IDTRAB+T6D_CODCAT)) == AllTrim(T6E->(T6E_ID+T6E_VERSAO+T6E_INDAPU+T6E_PERAPU+T6E_TRABAL+T6E_DEMPAG+T6E_ESTABE+T6E_IDTRAB+T6E_CODCAT))
											
											cIdTabRub := POSICIONE("C8R",1, xFilial("C8R")+T6E->T6E_IDRUBR,"C8R_IDTBRU")
											
											cXml +=		"<itensRemun>"
											cXml +=			xTafTag("codRubr"		,POSICIONE("C8R",1, xFilial("C8R")+T6E->T6E_IDRUBR,"C8R_CODRUB"))	  
											cXml +=			xTafTag("ideTabRubr"	,POSICIONE("T3M",1, xFilial("T3M")+cIdTabRub,"T3M_ID"),,.T.)
											cXml +=			xTafTag("qtdRubr"		,T6E->T6E_QTDRUB,PesqPict("T6E","T6E_QTDRUB"),.T.,.T.)
											cXml +=			xTafTag("fatorRubr"	,T6E->T6E_FATORR,PesqPict("T6E","T6E_FATORR"),.T.,.T.)
											cXml +=			xTafTag("vrUnit"		,T6E->T6E_VLRUNT,PesqPict("T6E","T6E_VLRUNT"),.T.,.T.)
											cXml +=			xTafTag("vrRubr"		,T6E->T6E_VLRRUB,PesqPict("T6E","T6E_VLRRUB"))											
											cXml +=		"</itensRemun>" 
											
											T6E->(DbSkip())
										EndDo 						
																										
									Else
									
										//Seek retorna .f. -> grupo de tags obrigatorio
										xTafTagGroup("itensRemun"	,{ {"codRubr"		,	''	, , .F. } ;
																	,  {"ideTabRubr"		,	''	, , .T. } ;
																	,  {"qtdRubr"			,	''	, , .T. } ;
																	,  {"fatorRubr"		,	''	, , .T. } ;
																	,  {"vrUnit"			,	''	, , .T. } ;
																	,  {"vrRubr"			,	''	, , .F. } } ;
																	,  @cXml , , .T. )
									
									EndIf
									
									/*----------------------------------------------------------
										Inicio da TAG infoSaudeColet
									----------------------------------------------------------*/		
									T6F->( DbSetOrder( 1 ) )	
									If T6F->( MsSeek ( xFilial("T6F")+T6D->(T6D_ID+T6D_VERSAO+T6D_INDAPU+T6D_PERAPU+T6D_TRABAL+T6D_DEMPAG+T6D_ESTABE+T6D_IDTRAB+T6D_CODCAT))) 
										
										cXml +=	"<infoSaudeColet>"
										
										While !T6F->(Eof()) .And. AllTrim(T6D->(T6D_ID+T6D_VERSAO+T6D_INDAPU+T6D_PERAPU+T6D_TRABAL+T6D_DEMPAG+T6D_ESTABE+T6D_IDTRAB+T6D_CODCAT)) == AllTrim(T6F->(T6F_ID+T6F_VERSAO+T6F_INDAPU+T6F_PERAPU+T6F_TRABAL+T6F_DEMPAG+T6F_ESTABE+T6F_IDTRAB+T6F_CODCAT))	
											cXml +=	"<detOper>"								
											cXml +=		xTafTag("cnpjOper"		,T6F->T6F_CNPJOP,,.F.)
											cXml +=		xTafTag("regANS"		,T6F->T6F_REGANS,,.F.)
											cXml +=		xTafTag("vrPgTit"		,T6F->T6F_VLPGTI,PesqPict("T6F","T6F_VLPGTI"),,.F.)

											/*----------------------------------------------------------
												Inicio da TAG detPlano
											----------------------------------------------------------*/								
											T6G->( DbSetOrder( 1 ) )	
											If T6G->( MsSeek ( xFilial("T6G")+T6F->(T6F_ID+T6F_VERSAO+T6F_INDAPU+T6F_PERAPU+T6F_TRABAL+T6F_DEMPAG+T6F_ESTABE+T6F_IDTRAB+T6F_CODCAT+T6F_CNPJOP)))  
												While !T6G->(Eof()) .And. AllTrim(T6F->(T6F_ID+T6F_VERSAO+T6F_INDAPU+T6F_PERAPU+T6F_TRABAL+T6F_DEMPAG+T6F_ESTABE+T6F_IDTRAB+T6F_CODCAT+T6F_CNPJOP)) == AllTrim(T6G->(T6G_ID+T6G_VERSAO+T6G_INDAPU+T6G_PERAPU+T6G_TRABAL+T6G_DEMPAG+T6G_ESTABE+T6G_IDTRAB+T6G_CODCAT+T6G_CNPJOP))											
													cXml +=	"<detPlano>"
													cXml +=		xTafTag("tpDep"			,Posicione( "CMI", 1, xFilial( "CMI" ) + T6G->T6G_TPDEP, "CMI_CODIGO" ) )
													cXml +=		xTafTag("cpfDep"		,T6G->T6G_CPFDEP,,.T.)
													cXml +=		xTafTag("nmDep"		   	,T6G->T6G_NOMDEP,,.F.)
													cXml +=		xTafTag("dtNascto"		,T6G->T6G_DTNDEP,,.F.)
													cXml +=		xTafTag("vlrPgDep"	   	,T6G->T6G_VPGDEP,PesqPict("T6G","T6G_VPGDEP"),,.F.)
													cXml +=	"</detPlano>"
												
													T6G->(DbSkip())				
												EndDo
											EndIf																																					
										
											cXml +=	"</detOper>"
											T6F->(DbSkip())		
										EndDo
									
										cXml +=	"</infoSaudeColet>"
									EndIf
																	
									cXml +=	"</remunPerApur>"			
									
									T6D->(DbSkip())
								EndDo
							
							Else
							
								//Seek retorna .f. -> grupo de tags obrigatorio
								xTafTagGroup("itensRemun"	,{ {"codRubr"		,	''	, , .F. } ;
															,  {"ideTabRubr"	,	''	, , .T. } ;
															,  {"qtdRubr"		,	''	, , .T. } ;
															,  {"fatorRubr"		,	''	, , .T. } ;
															,  {"vrUnit"		,	''	, , .T. } ;
															,  {"vrRubr"		,	''	, , .F. } } ;
															,  @cXmlItRem , , .T. )
								
								//Seek retorna .f. -> grupo de tags obrigatorio
								xTafTagGroup("remunPerApur"	,{ {"matricula"		,	''	, , .T. } ;
															,  {"codCateg"		,	''	, , .F. } } ;
															,  @cXml ;
															, { { "itensRemun" , cXmlItRem , 1 } } ;
															, .T. )
															
							EndIf			
							
							cXml +=	"</ideEstab>"
							
							T6C->(DbSkip())
						EndDo
						
						cXml +=	"</infoPerApur>"
					EndIf		
			
					/*----------------------------------------------------------
							Inicio da TAG infoPerAnt
					----------------------------------------------------------*/
					T61->( DbSetOrder( 1 ) )	                            
					If T61->( MsSeek ( xFilial("T61")+T14->(T14_ID+T14_VERSAO+T14_INDAPU+T14_PERAPU+T14_TRABAL+T14_IDEDMD)))  
						cXml +=	"<infoPerAnt>"	 				
									
						/*----------------------------------------------------------
							Inicio da TAG ideADC
						----------------------------------------------------------*/
						While !T61->(Eof()) .And. AllTrim(T14->(T14_ID+T14_VERSAO+T14_INDAPU+T14_PERAPU+T14_TRABAL+T14_IDEDMD)) == AllTrim(T61->(T61_ID+T61_VERSAO+T61_INDAPU+T61_PERAPU+T61_TRABAL+T61_DEMPAG))							
							cXml +=	"<ideADC>"				
							cXml +=		xTafTag("dtLei", T61->T61_DTLEI,,.F.)
							cXml +=		xTafTag("nrLei", T61->T61_NUMLEI,,.F.)
							cXml +=		xTafTag("dtEf" , T61->T61_DTEFET,,.T.)
				
							/*----------------------------------------------------------
								Inicio da TAG idePeriodo
							----------------------------------------------------------*/							
							T6H->( DbSetOrder( 1 ) )
							If T6H->( MsSeek ( xFilial("T6H")+T61->(T61_ID+T61_VERSAO+T61_INDAPU+T61_PERAPU+T61_TRABAL+T61_DEMPAG+DTOS(T61_DTLEI)+T61_NUMLEI)))
								While !T6H->(Eof()) .And.  AllTrim(T61->(T61_ID+T61_VERSAO+T61_INDAPU+T61_PERAPU+T61_TRABAL+T61_DEMPAG+DTOS(T61_DTLEI)+T61_NUMLEI)) == AllTrim(T6H->(T6H_ID+T6H_VERSAO+T6H_INDAPU+T6H_PERAPU+T6H_TRABAL+T6H_DEMPAG+DTOS(T6H_DTLEI)+T6H_NUMLEI))							
									cXml +=	"<idePeriodo>"
									
									If Len(Alltrim(T6H->T6H_PERREF)) <= 4
										cXml +=		xTafTag("perRef",T6H->T6H_PERREF,,.F.)
									Else
										cXml +=		xTafTag("perRef",Substr(T6H->T6H_PERREF, 1, 4) + '-' +  substr(T6H->T6H_PERREF, 5, 2) ,,.F.)
									EndIf
				
									/*----------------------------------------------------------
										Inicio da TAG ideEstab
									----------------------------------------------------------*/
									T6I->( DbSetOrder( 1 ) )
									If T6I->( MsSeek ( xFilial("T6I")+T6H->(T6H_ID+T6H_VERSAO+T6H_INDAPU+T6H_PERAPU+T6H_TRABAL+T6H_DEMPAG+DTOS(T6H_DTLEI)+T6H_NUMLEI+T6H_PERREF)))
										While !T6I->(Eof()) .And. AllTrim(T6H->(T6H_ID+T6H_VERSAO+T6H_INDAPU+T6H_PERAPU+T6H_TRABAL+T6H_DEMPAG+DTOS(T6H_DTLEI)+T6H_NUMLEI+T6H_PERREF)) == AllTrim(T6I->(T6I_ID+T6I_VERSAO+T6I_INDAPU+T6I_PERAPU+T6I_TRABAL+T6I_DEMPAG+DTOS(T6I_DTLEI)+T6I_NUMLEI+T6I_PERREF))						
											cXml +=	"<ideEstab>"				
											cXml +=		xTafTag("tpInsc", POSICIONE("C92",1, xFilial("C92")+T6I->T6I_ESTABE,"C92_TPINSC"),,.F.)
											cXml +=		xTafTag("nrInsc", POSICIONE("C92",1, xFilial("C92")+T6I->T6I_ESTABE,"C92_NRINSC"),,.F.)
										
											/*----------------------------------------------------------
												Inicio da TAG remunPerAnt
											----------------------------------------------------------*/   			
											T6J->( DbSetOrder( 1 ) )
											If T6J->( MsSeek ( xFilial("T6J")+T6I->(T6I_ID+T6I_VERSAO+T6I_INDAPU+T6I_PERAPU+T6I_TRABAL+T6I_DEMPAG+DTOS(T6I_DTLEI)+T6I_NUMLEI+T6I_PERREF+T6I_ESTABE)))	
												While !T6J->(Eof()) .And. AllTrim(T6I->(T6I_ID+T6I_VERSAO+T6I_INDAPU+T6I_PERAPU+T6I_TRABAL+T6I_DEMPAG+DTOS(T6I_DTLEI)+T6I_NUMLEI+T6I_PERREF+T6I_ESTABE)) == AllTrim(T6J->(T6J_ID+T6J_VERSAO+T6J_INDAPU+T6J_PERAPU+T6J_TRABAL+T6J_DEMPAG+DTOS(T6J_DTLEI)+T6J_NUMLEI+T6J_PERREF+T6J_ESTABE))	 
																																		
													cXml +=		"<remunPerAnt>"
													cXml +=			xTafTag("matricula", POSICIONE("C9V",1, xFilial("C9V")+T6J->T6J_IDTRAB,"C9V_MATRIC"),,.T.)	
													cXml +=			xTafTag("codCateg" , POSICIONE("C87",1, xFilial("C87")+T6J->T6J_CODCAT,"C87_CODIGO"),,.F.)									
								
													/*----------------------------------------------------------
														Inicio da TAG itensRemun
													----------------------------------------------------------*/ 		
													T6K->( DbSetOrder( 1 ) )	                            
													If T6K->( MsSeek ( xFilial("T6K")+T6J->(T6J_ID+T6J_VERSAO+T6J_INDAPU+T6J_PERAPU+T6J_TRABAL+T6J_DEMPAG+DTOS(T6J_DTLEI)+T6J_NUMLEI+T6J_PERREF+T6J_ESTABE+T6J_IDTRAB+T6J_CODCAT)))
														While !T6K->(Eof()) .And. AllTrim(T6J->(T6J_ID+T6J_VERSAO+T6J_INDAPU+T6J_PERAPU+T6J_TRABAL+T6J_DEMPAG+DTOS(T6J_DTLEI)+T6J_NUMLEI+T6J_PERREF+T6J_ESTABE+T6J_IDTRAB+T6J_CODCAT)) == AllTrim(T6K->(T6K_ID+T6K_VERSAO+T6K_INDAPU+T6K_PERAPU+T6K_TRABAL+T6K_DEMPAG+DTOS(T6K_DTLEI)+T6K_NUMLEI+T6K_PERREF+T6K_ESTABE+T6K_IDTRAB+T6K_CODCAT))
				
															cIdTabRub := POSICIONE("C8R",1, xFilial("C8R")+T6K->T6K_IDRUBR,"C8R_IDTBRU")
															
															cXml +=		"<itensRemun>"
															cXml +=			xTafTag("codRubr"		, POSICIONE("C8R",1, xFilial("C8R")+T6K->T6K_IDRUBR,"C8R_CODRUB"),,.F.)  												
															cXml +=			xTafTag("ideTabRubr"	, POSICIONE("T3M",1, xFilial("T3M")+cIdTabRub,"T3M_CODERP"),,.T.)
															cXml +=			xTafTag("qtdRubr"		, T6K->T6K_QTDRUB,PesqPict("T6K","T6K_QTDRUB"),.T.,.T.)
															cXml +=			xTafTag("fatorRubr"	, T6K->T6K_FATORR,PesqPict("T6K","T6K_FATORR"),.T.,.T.)
															cXml +=			xTafTag("vrUnit"		, T6K->T6K_VLRUNT,PesqPict("T6K","T6K_VLRUNT"),.T.,.T.)
															cXml +=			xTafTag("vrRubr"		, T6K->T6K_VLRRUB,PesqPict("T6K","T6K_VLRRUB"),,.F.)				
															cXml +=		"</itensRemun>" 
													
															T6K->(DbSkip())
														EndDo 																
													
													Else
													
														//Seek retorna .f. -> grupo de tags obrigatorio
														xTafTagGroup("itensRemun"	,{ {"codRubr"		,	''	, , .F. } ;
																					,  {"ideTabRubr"	,	''	, , .T. } ;
																					,  {"qtdRubr"		,	''	, , .T. } ;
																					,  {"fatorRubr"		,	''	, , .T. } ;
																					,  {"vrUnit"		,	''	, , .T. } ;
																					,  {"vrRubr"		,	''	, , .F. } } ;
																					,  @cXml , , .T.)
													
													EndIf
																																					
													cXml +=	"</remunPerAnt>"			
													
													T6J->(DbSkip())
												EndDo
											
											Else
											
												//Seek retorna .f. -> grupo de tags obrigatorio
												xTafTagGroup("itensRemun"	,{ {"codRubr"		,	''	, , .F. } ;
																			,  {"ideTabRubr"	,	''	, , .T. } ;
																			,  {"qtdRubr"		,	''	, , .T. } ;
																			,  {"fatorRubr"		,	''	, , .T. } ;
																			,  {"vrUnit"		,	''	, , .T. } ;
																			,  {"vrRubr"		,	''	, , .F. } } ;
																			,  @cXmlItRem , , .T. )
												
												//Seek retorna .f. -> grupo de tags obrigatorio
												xTafTagGroup("remunPerAnt"	,{ {"matricula"		,	''	, , .T. } ;
																			,  {"codCateg"		,	''	, , .F. } } ;
																			,  @cXml ;
																			, { { "itensRemun" , cXmlItRem , 1 } } ;
																			, .T. )
															
											
											EndIf	
											
											cXml +=	"</ideEstab>"
											
											T6I->(DbSkip())
										EndDo
									
									Else
									
										//Seek retorna .f. -> grupo de tags obrigatorio
										xTafTagGroup("ideEstab"		,{ {"tpInsc"		,	''	, , .F. } ;
																	,  {"nrInsc"		,	''	, , .F. } } ;
																	,  @cXml , , .T.)
									
									EndIf
											
									cXml +=	"</idePeriodo>"
									
									T6H->(DbSkip())
								EndDo
							
							Else
							
								//Seek retorna .f. -> grupo de tags obrigatorio
								xTafTagGroup("itensRemun"	,{ {"codRubr"		,	''	, , .F. } ;
															,  {"ideTabRubr"	,	''	, , .T. } ;
															,  {"qtdRubr"		,	''	, , .T. } ;
															,  {"fatorRubr"		,	''	, , .T. } ;
															,  {"vrUnit"		,	''	, , .T. } ;
															,  {"vrRubr"		,	''	, , .F. } } ;
															,  @cXmlItRem , , .T. )
								
								//Seek retorna .f. -> grupo de tags obrigatorio
								xTafTagGroup("remunPerAnt"	,{ {"matricula"		,	''	, , .T. } ;
															,  {"codCateg"		,	''	, , .F. } } ;
															,  @cXmlRemPAnt ;
															, { { "itensRemun" , cXmlItRem , 1 } } ;
															, .T. )
								
								//Seek retorna .f. -> grupo de tags obrigatorio
								xTafTagGroup("ideEstab"		,{ {"tpInsc"		,	''	, , .F. } ;
															,  {"nrInsc"		,	''	, , .F. } } ;
															,  @cXmlEstab ;
															, { { "remunPerAnt" , cXmlRemPAnt , 1 } } ;
															, .T.)
								
								//Seek retorna .f. -> grupo de tags obrigatorio
								xTafTagGroup("idePeriodo"	,{ {"perRef"		,	''	, , .F. } } ;
															,  @cXml ;
															, { { "ideEstab" , cXmlEstab , 1 } } ;
															, .T.)
							
							EndIf		
							
							cXml +=	"</ideADC>"
							
							T61->(DbSkip())
						EndDo					
					
						cXml +=	"</infoPerAnt>"
					EndIf	     
				
					cXml +=	"</dmDev>"	
					T14->(DbSkip())
				
				EndDo
			
			Else
			
				//Seek retorna .f. -> grupo de tags obrigatorio
				xTafTagGroup("dmDev"	,{ {"ideDmDev"		,	''	, , .F. } } ;
										,  @cXml , , .T. )
			
			EndIf
																																					
		EndIf
		
		//����������������������Ŀ
		//�Estrutura do cabecalho�
		//������������������������
		cXml := xTafCabXml(cXml,"C91",cLayout,cReg,aMensal)

		//����������������������������Ŀ
		//�Executa gravacao do registro�
		//������������������������������
		If !lJob
			If lAutomato
				xTafGerXml( cXml, cLayout,,, .F.,, @cFile )
			Else
				xTafGerXml( cXml, cLayout )
			EndIf
		EndIf
EndIf
	
Return( cXml )
//-------------------------------------------------------------------
/*/{Protheus.doc} TAF413Grv
@type			function
@description	Fun��o de grava��o para atender o registro S-1200.
@author			Vitor Siqueira
@since			08/01/2016
@version		1.0
@param			cLayout		-	Nome do Layout que est� sendo enviado
@param			nOpc		-	Op��o a ser realizada ( 3 = Inclus�o, 4 = Altera��o, 5 = Exclus�o )
@param			cFilEv		-	Filial do ERP para onde as informa��es dever�o ser importadas
@param			oXML		-	Objeto com as informa��es a serem manutenidas ( Outras Integra��es )
@param			cOwner
@param			cFilTran
@param			cPredeces
@param			nTafRecno
@param			cComplem
@param			cGrpTran
@param			cEmpOriGrp
@param			cFilOriGrp
@param			cXmlID		-	Atributo Id, �nico para o XML do eSocial. Utilizado para importa��o de dados de clientes migrando para o TAF
@return			lRet		-	Vari�vel que indica se a importa��o foi realizada, ou seja, se as informa��es foram gravadas no banco de dados
@param			aIncons		-	Array com as inconsist�ncias encontradas durante a importa��o
/*/
//-------------------------------------------------------------------
Function TAF413Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID )

Local cCmpsNoUpd	:=	"|C91_FILIAL|C91_ID|C91_VERSAO|C91_NOMEVE|C91_VERANT|C91_PROTPN|C91_EVENTO|C91_STATUS|C91_ATIVO|"
Local cMatrcT6D		:=	""
Local cIdTrbT6D		:=	""
Local cMatrcT6J		:=	""
Local cIdTrbT6J		:=	""
Local cIdProc		:=	""
Local cCRNPath		:=	""
Local cT14Path		:=	""
Local cT6CPath		:=	""
Local cT6DPath		:=	""
Local cT6EPath		:=	""
Local cT6FPath		:=	""
Local cT6GPath		:=	""
Local cT61Path		:=	""
Local cT6HPath		:=	""
Local cT6IPath		:=	""
Local cT6JPath		:=	""
Local cT6KPath		:=	""
Local cEnter		:=	Chr( 13 ) + Chr( 10 )
Local cMensagem		:=	""
Local cInconMsg		:=	""
Local cPeriodo		:=	""
Local cChave		:=	""
Local cEvtTrab		:=	""
Local cCodEvent		:=	""
Local cLogOpeAnt	:=	""
Local nJ			:=	0
Local nlI			:=	0
Local nSeqErrGrv	:=	0
Local nCRN			:=	0
Local nT14			:=	0
Local nT6C			:=	0
Local nT6D			:=	0
Local nT6E			:=	0
Local nT6F			:=	0
Local nT6G			:=	0
Local nT61			:=	0
Local nT6H			:=	0
Local nT6I			:=	0
Local nT6J			:=	0
Local nT6K			:=	0
Local aIncons		:=	{}
Local aRules		:=	{}
Local aChave		:=	{}
Local lRet			:=	.F.
Local oModel		:=	Nil

Private lVldModel	:=	.T.
Private oDados		:=	Nil

Default cLayout		:=	""
Default nOpc		:=	1
Default cFilEv		:=	""
Default oXML		:=	Nil
Default cOwner		:=	""
Default cFilTran	:=	""
Default cPredeces	:=	""
Default nTafRecno	:=	0
Default cComplem	:=	""
Default cGrpTran	:=	""
Default cEmpOriGrp	:=	""
Default cFilOriGrp	:=	""
Default cXmlID		:=	""

cCodEvent := Posicione( "C8E", 2, xFilial( "C8E" ) + "S-" + cLayout, "C8E->C8E_ID" )
oDados := oXML

//�����������������Ŀ
//�Chave do registro�
//�������������������
cPeriodo  := FTafGetVal( "/eSocial/evtRmnRPPS/ideEvento/perApur", "C", .F., @aIncons, .F. )
  
Aadd( aChave, {"C", "C91_INDAPU", FTafGetVal( "/eSocial/evtRmnRPPS/ideEvento/indApuracao", "C", .F., @aIncons, .F. )  , .T.} ) 
cChave += Padr( aChave[ 1, 3 ], Tamsx3( aChave[ 1, 2 ])[1])

If At("-", cPeriodo) > 0
	Aadd( aChave, {"C", "C91_PERAPU", StrTran(cPeriodo, "-", "" ),.T.} )
	cChave += Padr( aChave[ 2, 3 ], Tamsx3( aChave[ 2, 2 ])[1])	
Else
	Aadd( aChave, {"C", "C91_PERAPU", cPeriodo  , .T.} ) 
	cChave += Padr( aChave[ 2, 3 ], Tamsx3( aChave[ 2, 2 ])[1])		
EndIf

Aadd( aChave, {"C", "C91_TRABAL", FGetIdInt("cpfTrab","","/eSocial/evtRmnRPPS/ideTrabalhador/cpfTrab") , .T.} )
Aadd( aChave, {"C", "C91_NOMEVE", "S1202" , .T.} )
cChave += Padr( aChave[ 3, 3 ], Tamsx3( aChave[ 3, 2 ])[1])	

//Verifica se o evento ja existe na base
("C91")->( DbSetOrder( 2 ) )
If ("C91")->( MsSeek( FTafGetFil( cFilEv , @aIncons , "C91" ) + cChave + 'S1202' + '1' ) )
	If !C91->C91_STATUS $ ( "2|4|6|" )
		nOpc := 4
	EndIf
EndIf

Begin Transaction	
	
	//�������������������������������������������������������������Ŀ
	//�Funcao para validar se a operacao desejada pode ser realizada�
	//���������������������������������������������������������������
	If FTafVldOpe( 'C91', 2, @nOpc, cFilEv, @aIncons, aChave, @oModel, 'TAFA413', cCmpsNoUpd )		    	      				     		    	      	    		    		    		    		    																					

		If TafColumnPos( "C91_LOGOPE" )
			cLogOpeAnt := C91->C91_LOGOPE
		endif

		//���������������������������������������������������������������Ŀ
		//�Carrego array com os campos De/Para de gravacao das informacoes�
		//�����������������������������������������������������������������
		aRules := TAF413Rul( @cInconMsg, @nSeqErrGrv, cCodEvent, cOwner )								                
        		
		//����������������������������������������������������������������Ŀ
		//�Quando se tratar de uma Exclusao direta apenas preciso realizar �
		//�o Commit(), nao eh necessaria nenhuma manutencao nas informacoes�
		//������������������������������������������������������������������
		If nOpc <> 5 
						
			/*-----------------------------------------
			   C91 - Informa��es de Folha de Pagamento
			-------------------------------------------*/	
			oModel:LoadValue( "MODEL_C91", "C91_INDAPU", C91->C91_INDAPU)              
			oModel:LoadValue( "MODEL_C91", "C91_PERAPU", C91->C91_PERAPU)              
			oModel:LoadValue( "MODEL_C91", "C91_TRABAL", C91->C91_TRABAL)   
			oModel:LoadValue( "MODEL_C91", "C91_NOMEVE", "S1202" )            

			If TAFColumnPos( "C91_XMLID" )
				oModel:LoadValue( "MODEL_C91", "C91_XMLID", cXmlID )
			EndIf

			//����������������������������������������Ŀ
			//�Rodo o aRules para gravar as informacoes�
			//������������������������������������������
			For nlI := 1 To Len( aRules )                 					
			 	oModel:LoadValue( "MODEL_C91", aRules[ nlI, 01 ], FTafGetVal( aRules[ nlI, 02 ], aRules[nlI, 03], aRules[nlI, 04], @aIncons, .F. ) )
			Next

			If Findfunction("TAFAltMan")
				if nOpc == 3
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_C91', 'C91_LOGOPE' , '1', '' )
				elseif nOpc == 4
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_C91', 'C91_LOGOPE' , '', cLogOpeAnt )
				EndIf
			EndIf

			cEvtTrab   := POSICIONE("C9V",1, FTafGetFil( cFilEv , @aIncons , "C9V" ) +FWFLDGET("C91_TRABAL"),"C9V_NOMEVE")	

			/*-----------------------------------------	 		
	 			CRN - Informa��es de Processos Judic.
			------------------------------------------*/
			nCRN := 1
			cCRNPath := "/eSocial/evtRmnRPPS/ideTrabalhador/procJudTrab[" + CVALTOCHAR(nCRN) + "]"
						
			If nOpc == 4 
				For nJ := 1 to oModel:GetModel( 'MODEL_CRN' ):Length()
					oModel:GetModel( 'MODEL_CRN' ):GoLine(nJ)
					oModel:GetModel( 'MODEL_CRN' ):DeleteLine()
				Next nJ
			EndIf

			nCRN := 1
			While oDados:XPathHasNode(cCRNPath)  
									
				oModel:GetModel( 'MODEL_CRN' ):LVALID	:= .T.					

				If nOpc == 4 .Or. nCRN > 1
					oModel:GetModel( 'MODEL_CRN' ):AddLine()
   				EndIf  
				
				If oDados:XPathHasNode(cCRNPath + "/tpTrib" )
					oModel:LoadValue("MODEL_CRN", "CRN_TPTRIB", FTafGetVal( cCRNPath + "/tpTrib", "C", .F., @aIncons, .F. ) )				
				EndIf
				
				If oDados:XPathHasNode(cCRNPath + "/nrProcJud" )
					cIdProc := FGetIdInt( "nrProcJ", ,cCRNPath + "/nrProcJud",,,,@cInconMsg, @nSeqErrGrv)
					oModel:LoadValue("MODEL_CRN", "CRN_IDPROC", cIdProc )	
											
				EndIf
				
				If !Empty(cIdProc)
					If oDados:XPathHasNode(cCRNPath + "/codSusp" )
						oModel:LoadValue("MODEL_CRN", "CRN_IDSUSP", FGetIdInt( "codSusp","",FTafGetVal( cCRNPath + "/codSusp", "C", .F., @aIncons, .F. ),cIdProc,.F.,,@cInconMsg, @nSeqErrGrv) )							
					EndIf
				EndIf
						                         
              nCRN++
				cCRNPath := "/eSocial/evtRmnRPPS/ideTrabalhador/procJudTrab[" + CVALTOCHAR(nCRN) + "]"
			EndDo 
			
			/*------------------------------------------
			   	 T14 - Ident. demonstrativos de valores 
			--------------------------------------------*/		
			nT14 := 1
			cT14Path := "/eSocial/evtRmnRPPS/dmDev[" + CVALTOCHAR(nT14) + "]"
			
			If nOpc == 4
				For nJ := 1 to oModel:GetModel( 'MODEL_T14' ):Length()
					oModel:GetModel( 'MODEL_T14' ):GoLine(nJ)
					oModel:GetModel( 'MODEL_T14' ):DeleteLine()
				Next nJ
			EndIf
			
			nT14 := 1
			While oDados:XPathHasNode( cT14Path ) 
			
				oModel:GetModel( 'MODEL_T14' ):LVALID	:= .T.					

				If nOpc == 4 .Or. nT14 > 1
					oModel:GetModel( 'MODEL_T14' ):AddLine()
   				EndIf	
				
				If oDados:XPathHasNode( cT14Path )
					oModel:LoadValue( "MODEL_T14", "T14_IDEDMD",  FTafGetVal( cT14Path + "/ideDmDev", "C", .F., @aIncons, .F. ) )
					oModel:LoadValue( "MODEL_T14", "T14_NOMEVE", "S1202" )
				EndIf
				
				/*------------------------------------------------
		   	 		   Info. relativas ao per�odo de apura��o
		   	 			 T6C - Identifica��o do Estabelecimen
				-------------------------------------------------*/          
              nT6C:= 1
				cT6CPath := cT14Path +"/infoPerApur/ideEstab[" + CVALTOCHAR(nT6C) + "]"
				
				If nOpc == 4 
					For nJ := 1 to oModel:GetModel( 'MODEL_T6C' ):Length()
						oModel:GetModel( 'MODEL_T6C' ):GoLine(nJ)
						oModel:GetModel( 'MODEL_T6C' ):DeleteLine()
					Next nJ
				EndIf
				
				nT6C := 1
				While oDados:XPathHasNode(cT6CPath)
										
					oModel:GetModel( 'MODEL_T6C' ):LVALID	:= .T.					
        	
					If nOpc == 4 .Or. nT6C > 1
						oModel:GetModel( 'MODEL_T6C' ):AddLine()
    				EndIf
    				
					if oDados:XPathHasNode(	cT6CPath + "/tpInsc" , cT6CPath + "/nrInsc" )
						oModel:LoadValue( "MODEL_T6C", "T6C_ESTABE", FGetIdInt( "tpInsc", "nrInsc", cT6CPath + "/tpInsc" , cT6CPath + "/nrInsc",,,@cInconMsg, @nSeqErrGrv) ) 
					EndIf
						      
	              /*------------------------------------------
			   	 		T6D - Remun Trabal. Per�odo Apura��o
					--------------------------------------------*/      
	              nT6D:= 1
					cT6DPath := cT6CPath+"/remunPerApur[" + CVALTOCHAR(nT6D) + "]"
					
					If nOpc == 4 
						For nJ := 1 to oModel:GetModel( 'MODEL_T6D' ):Length()
							oModel:GetModel( 'MODEL_T6D' ):GoLine(nJ)
							oModel:GetModel( 'MODEL_T6D' ):DeleteLine()
						Next nJ
					EndIf
					
					nT6D := 1
					While oDados:XPathHasNode(cT6DPath)
											
						oModel:GetModel( 'MODEL_T6D' ):LVALID	:= .T.					
	        	
						If nOpc == 4 .Or. nT6D > 1
							oModel:GetModel( 'MODEL_T6D' ):AddLine()
	    				EndIf
	    						    				
	    				If oDados:XPathHasNode(cT6DPath + "/matricula")
						   	cMatrcT6D := FTafGetVal( cT6DPath + "/matricula", "C", .F., @aIncons, .F. )			  		
							cIdTrbT6D := FGetIdInt( "matricula", "", cMatrcT6D, , .F.,,@cInconMsg, @nSeqErrGrv) 	
							oModel:LoadValue( "MODEL_T6D", "T6D_IDTRAB", cIdTrbT6D )
						EndIf													
						
						If oDados:XPathHasNode(cT6DPath + "/codCateg")
							oModel:LoadValue( "MODEL_T6D", "T6D_CODCAT", FGetIdInt( "codCateg", "", cT6DPath + "/codCateg",,,,@cInconMsg, @nSeqErrGrv))
						EndIf
												
						/*------------------------------------------
			   	 			T6E - Itens da Remunera��o Trab.
						--------------------------------------------*/								
		              nT6E:= 1
						cT6EPath := cT6DPath+"/itensRemun[" + CVALTOCHAR(nT6E) + "]"
						
						If nOpc == 4 
							For nJ := 1 to oModel:GetModel( 'MODEL_T6E' ):Length()
								oModel:GetModel( 'MODEL_T6E' ):GoLine(nJ)
								oModel:GetModel( 'MODEL_T6E' ):DeleteLine()
							Next nJ
						EndIf
	
						nT6E := 1
						While oDados:XPathHasNode(cT6EPath)
												
							oModel:GetModel( 'MODEL_T6E' ):LVALID	:= .T.					
		        	
							If nOpc == 4 .Or. nT6E > 1
								oModel:GetModel( 'MODEL_T6E' ):AddLine()
		    				EndIf	
		    						
	    					if oDados:XPathHasNode(	cT6EPath + "/ideTabRubr")
								cIdTabR := TAFIdTabRub( FTafGetVal( cT6EPath + "/ideTabRubr", "C", .F., @aIncons, .F. ) )
							else
								cIdTabR := ""
							EndIf

							if oDados:XPathHasNode(cT6EPath + "/codRubr")
								oModel:LoadValue( "MODEL_T6E", "T6E_IDRUBR"  ,  FGetIdInt( "codRubr", "ideTabRubr", FTafGetVal( cT6EPath + "/codRubr", "C", .F., @aIncons, .F. ),cIdTabR,.F.,,@cInconMsg, @nSeqErrGrv,/*9*/,/*10*/,/*11*/,/*12*/,/*13*/,StrTran(cPeriodo,"-","")))
							EndIf
							
							if oDados:XPathHasNode(cT6EPath + "/qtdRubr")
								oModel:LoadValue( "MODEL_T6E", "T6E_QTDRUB"	, FTafGetVal( cT6EPath + "/qtdRubr", "N", .F., @aIncons, .F. ) )
							EndIf
							
							if oDados:XPathHasNode(cT6EPath + "/fatorRubr")
								oModel:LoadValue( "MODEL_T6E", "T6E_FATORR"	, FTafGetVal( cT6EPath + "/fatorRubr", "N", .F., @aIncons, .F. ) )
							EndIf
							
							if oDados:XPathHasNode(cT6EPath + "/vrUnit")
								oModel:LoadValue( "MODEL_T6E", "T6E_VLRUNT"	, FTafGetVal( cT6EPath + "/vrUnit", "N", .F., @aIncons, .F. ) )
							EndIf
							
							if oDados:XPathHasNode(cT6EPath + "/vrRubr")
								oModel:LoadValue( "MODEL_T6E", "T6E_VLRRUB"	, FTafGetVal( cT6EPath + "/vrRubr", "N", .F., @aIncons, .F. ) )
							EndIf
							
			              nT6E++
							cT6EPath := cT6DPath+"/itensRemun[" + CVALTOCHAR(nT6E) + "]"
						EndDo
						
						/*-----------------------------------------
			   	 			T6F - Informa��es de Planos de Saudade 
						------------------------------------------*/	
						nT6F:= 1
						cT6FPath := cT6DPath+"/infoSaudeColet/detOper[" + CVALTOCHAR(nT6F) + "]"
						
						If nOpc == 4
							For nJ := 1 to oModel:GetModel( 'MODEL_T6F' ):Length()
								oModel:GetModel( 'MODEL_T6F' ):GoLine(nJ)
								oModel:GetModel( 'MODEL_T6F' ):DeleteLine()
							Next nJ
						EndIf
						
						nT6F := 1
						While oDados:XPathHasNode(cT6FPath)
												
							oModel:GetModel( 'MODEL_T6F' ):LVALID	:= .T.					
		        	
							If nOpc == 4 .Or. nT6F > 1
								oModel:GetModel( 'MODEL_T6F' ):AddLine()
		    				EndIf	
		    						
							if oDados:XPathHasNode(cT6FPath + "/cnpjOper" )
								oModel:LoadValue( "MODEL_T6F", "T6F_CNPJOP"	, FTafGetVal( cT6FPath + "/cnpjOper", "C", .F.	, @aIncons, .F. ) )												
							EndIF
							
							if oDados:XPathHasNode(cT6FPath + "/regANS" )
								oModel:LoadValue( "MODEL_T6F", "T6F_REGANS"	, FTafGetVal( cT6FPath + "/regANS"  , "C", .F.	, @aIncons, .F. ) )
							EndIF
							
							if oDados:XPathHasNode(cT6FPath + "/vrPgTit" )
								oModel:LoadValue( "MODEL_T6F", "T6F_VLPGTI"	, FTafGetVal( cT6FPath + "/vrPgTit" , "N", .F.	, @aIncons, .F. ) )
							EndIf
							
							/*----------------------------------------
			   	 				 T6G - Informa��es dos dependentes
							------------------------------------------*/
							nT6G:= 1
							cT6GPath := cT6FPath+"/detPlano[" + CVALTOCHAR(nT6G) + "]"
							
							If nOpc == 4
								For nJ := 1 to oModel:GetModel( 'MODEL_T6G' ):Length()
									oModel:GetModel( 'MODEL_T6G' ):GoLine(nJ)
									oModel:GetModel( 'MODEL_T6G' ):DeleteLine()
								Next nJ
							EndIf
		
							nT6G := 1
							While oDados:XPathHasNode(cT6GPath)
								
								oModel:GetModel( 'MODEL_T6G' ):LVALID	:= .T.					
		        	
								If nOpc == 4 .Or. nT6G > 1
									oModel:GetModel( 'MODEL_T6G' ):AddLine()
			    				EndIf			

								If oDados:xPathHasNode( cT6GPath + "/tpDep" )
									oModel:LoadValue( "MODEL_T6G", "T6G_TPDEP", FGetIdInt( "tpDep", "", cT6GPath + "/tpDep",,,, @cInconMsg, @nSeqErrGrv ) )
								EndIf
								
								if oDados:XPathHasNode( cT6GPath + "/cpfDep" )
									oModel:LoadValue( "MODEL_T6G", "T6G_CPFDEP"	, FTafGetVal( cT6GPath + "/cpfDep"   	, "C", .F.			, @aIncons, .F. ) )												
								EndIf
																
								if oDados:XPathHasNode( cT6GPath + "/nmDep" )
									oModel:LoadValue( "MODEL_T6G", "T6G_NOMDEP"	, FTafGetVal( cT6GPath + "/nmDep"      , "C", .F.		, @aIncons, .F. ) )
								EndIf
								
								if oDados:XPathHasNode(	cT6GPath + "/dtNascto" )
									oModel:LoadValue( "MODEL_T6G", "T6G_DTNDEP"	, FTafGetVal( cT6GPath + "/dtNascto"   , "D", .F.	    , @aIncons, .F. ) )
								EndIf
																
								if oDados:XPathHasNode( cT6GPath + "/vlrPgDep" )
									oModel:LoadValue( "MODEL_T6G", "T6G_VPGDEP"	, FTafGetVal( cT6GPath + "/vlrPgDep"   , "N", .F.		, @aIncons, .F. ) )
								EndIf
								
								nT6G++
								cT6GPath := cT6FPath+"/detPlano[" + CVALTOCHAR(nT6G) + "]"
							EndDo
												
			              nT6F++
							cT6FPath := cT6DPath+"/infoSaudeColet/detOper[" + CVALTOCHAR(nT6F) + "]"
						EndDo
						
		              nT6D++
						cT6DPath := cT6CPath+"/remunPerApur[" + CVALTOCHAR(nT6D) + "]"
					EndDo
					
					nT6C++
					cT6CPath := cT14Path+"/infoPerApur/ideEstab[" + CVALTOCHAR(nT6C) + "]"
				EndDo
					
	
				/*-----------------------------------------------
					Remunera��o relativa a Per�odos Anteriores.
		 				T61 - Ident Lei  Remun. Per.  Anter.
				------------------------------------------------*/
				nT61 := 1
				cT61Path := cT14Path+"/infoPerAnt/ideADC[" + CVALTOCHAR(nT61) + "]"
													
				If nOpc == 4
					For nJ := 1 to oModel:GetModel( 'MODEL_T61' ):Length()
						oModel:GetModel( 'MODEL_T61' ):GoLine(nJ)
						oModel:GetModel( 'MODEL_T61' ):DeleteLine()
					Next nJ
				EndIf
				
				nT61 := 1
				While oDados:XPathHasNode(cT61Path)
										
					oModel:GetModel( 'MODEL_T61' ):LVALID	:= .T.					
	    	
					If nOpc == 4 .Or. nT61 > 1
						oModel:GetModel( 'MODEL_T61' ):AddLine()
					EndIf
		            
					if oDados:XPathHasNode( cT61Path + "/dtLei" )
						oModel:LoadValue( "MODEL_T61", "T61_DTLEI" ,  FTafGetVal( cT61Path + "/dtLei", "D", .F., @aIncons, .F. ) )
					EndIf
					
					if oDados:XPathHasNode( cT61Path + "/nrLei" )
						oModel:LoadValue( "MODEL_T61", "T61_NUMLEI",  FTafGetVal( cT61Path + "/nrLei", "C", .F., @aIncons, .F. ) )
					EndIf
					
					if oDados:XPathHasNode( cT61Path + "/dtEf" )
						oModel:LoadValue( "MODEL_T61", "T61_DTEFET",  FTafGetVal( cT61Path + "/dtEf", "D" , .F., @aIncons, .F. ) )
					EndIf
					
					/*------------------------------------------
			   	 			T6H - Informa��es do Periodo
					--------------------------------------------*/
					nT6H:= 1
					cT6HPath := cT61Path + "/idePeriodo[" + CVALTOCHAR(nT6H) + "]"
							
					If nOpc == 4
						For nJ := 1 to oModel:GetModel( 'MODEL_T6H' ):Length()
							oModel:GetModel( 'MODEL_T6H' ):GoLine(nJ)
							oModel:GetModel( 'MODEL_T6H' ):DeleteLine()
						Next nJ
					EndIf
					
					nT6H := 1
					While oDados:XPathHasNode(cT6HPath)
											
						oModel:GetModel( 'MODEL_T6H' ):LVALID	:= .T.					
	        	
						If nOpc == 4 .Or. nT6H > 1
							oModel:GetModel( 'MODEL_T6H' ):AddLine()
	    				EndIf
			            
			          if oDados:XPathHasNode(  cT6HPath + "/perRef" )
			            If At("-", FTafGetVal( cT6HPath + "/perRef", "C", .F., @aIncons, .F. )) > 0
			            	oModel:LoadValue( "MODEL_T6H", "T6H_PERREF"	, StrTran( FTafGetVal( cT6HPath + "/perRef", "C", .F., @aIncons, .F. ), "-", "" ) )
			            Else
							oModel:LoadValue( "MODEL_T6H", "T6H_PERREF"	,FTafGetVal( cT6HPath + "/perRef", "C", .F., @aIncons, .F. ) )		
						  EndIf
						EndIf
									
						/*---------------------------------------
				   	 		T6I - Ident. do Estabelecimento
						---------------------------------------*/          
		              nT6I := 1
						cT6IPath := cT6HPath +"/ideEstab[" + CVALTOCHAR(nT6I) + "]"
						
						If nOpc == 4 .And. oDados:XPathHasNode( cT6IPath )
							For nJ := 1 to oModel:GetModel( 'MODEL_T6I' ):Length()
								oModel:GetModel( 'MODEL_T6I' ):GoLine(nJ)
								oModel:GetModel( 'MODEL_T6I' ):DeleteLine()
							Next nJ
						EndIf
						
						nT6I := 1
						While oDados:XPathHasNode(cT6IPath)
												
							oModel:GetModel( 'MODEL_T6I' ):LVALID	:= .T.					
		        	
							If nOpc == 4 .Or. nT6I > 1
								oModel:GetModel( 'MODEL_T6I' ):AddLine()
		    				EndIf
		    				
							if oDados:XPathHasNode(	cT6IPath + "/tpInsc" , cT6IPath + "/nrInsc" )
								oModel:LoadValue( "MODEL_T6I", "T6I_ESTABE", FGetIdInt( "tpInsc", "nrInsc", cT6IPath + "/tpInsc" , cT6IPath + "/nrInsc",,,@cInconMsg, @nSeqErrGrv) ) 
							EndIf
									
							/*------------------------------------------
			   	 				T6J - Remunera��o Per�odo Anterior  
							--------------------------------------------*/
							nT6J:= 1
							cT6JPath := cT6IPath+ "/remunPerAnt[" + CVALTOCHAR(nT6J) + "]"
							
							If nOpc == 4
								For nJ := 1 to oModel:GetModel( 'MODEL_T6J' ):Length()
									oModel:GetModel( 'MODEL_T6J' ):GoLine(nJ)
									oModel:GetModel( 'MODEL_T6J' ):DeleteLine()
								Next nJ
							EndIf
							
							nT6J := 1
							While oDados:XPathHasNode(cT6JPath)
													
								oModel:GetModel( 'MODEL_T6J' ):LVALID	:= .T.					
			        	
								If nOpc == 4 .Or. nT6J > 1
									oModel:GetModel( 'MODEL_T6J' ):AddLine()
			    				EndIf
			    						    				
			    				If oDados:XPathHasNode(cT6JPath + "/matricula")
								   	cMatrcT6J := FTafGetVal( cT6JPath + "/matricula", "C", .F., @aIncons, .F. )			  		
									cIdTrbT6J := FGetIdInt( "matricula", "", cMatrcT6J, , .F.,,@cInconMsg, @nSeqErrGrv) 	
									oModel:LoadValue( "MODEL_T6J", "T6J_IDTRAB", cIdTrbT6J )
								EndIf													
								
								If oDados:XPathHasNode(cT6JPath + "/codCateg")
									oModel:LoadValue( "MODEL_T6J", "T6J_CODCAT", FGetIdInt( "codCateg", "", cT6JPath + "/codCateg",,,,@cInconMsg, @nSeqErrGrv))
								EndIf													
							
								/*------------------------------------------
			   	 					 T6K - Itens Remun. Per�odo Anterior 
								------------------------------------------*/
								nT6K:= 1
								cT6KPath := cT6JPath + "/itensRemun[" + CVALTOCHAR(nT6K) + "]"
							    													
								If nOpc == 4
									For nJ := 1 to oModel:GetModel( 'MODEL_T6K' ):Length()
										oModel:GetModel( 'MODEL_T6K' ):GoLine(nJ)
										oModel:GetModel( 'MODEL_T6K' ):DeleteLine()
									Next nJ
								EndIf
								
								nT6K := 1								        
								While oDados:XPathHasNode(cT6KPath) 
												
									oModel:GetModel( 'MODEL_T6K' ):LVALID	:= .T.					
				        	
									If nOpc == 4 .Or. nT6K > 1
										oModel:GetModel( 'MODEL_T6K' ):AddLine()
				    				EndIf			
				    				
			    					if oDados:XPathHasNode(	cT6KPath + "/ideTabRubr")
			    						cIdTabR := TAFIdTabRub( FTafGetVal( cT6KPath + "/ideTabRubr", "C", .F., @aIncons, .F. ) )
									else
										cIdTabR := ""
									EndIf

									if oDados:XPathHasNode(	cT6KPath + "/codRubr" )
										oModel:LoadValue( "MODEL_T6K", "T6K_IDRUBR"	,;
										 FGetIdInt( "codRubr", "ideTabRubr", FTafGetVal( cT6KPath + "/codRubr", "C", .F., @aIncons, .F. ),cIdTabR,.F.,,@cInconMsg, @nSeqErrGrv,/*9*/,/*10*/,/*11*/,/*12*/,/*13*/,StrTran(cPeriodo,"-","")))
									EndIf
									
									if oDados:XPathHasNode( cT6KPath + "/qtdRubr" )
										oModel:LoadValue( "MODEL_T6K", "T6K_QTDRUB"	, FTafGetVal( cT6KPath + "/qtdRubr", "N", .F., @aIncons, .F. ) )					
									EndIf
									
									if oDados:XPathHasNode( cT6KPath + "/fatorRubr")
										oModel:LoadValue( "MODEL_T6K", "T6K_FATORR"	, FTafGetVal( cT6KPath + "/fatorRubr", "N", .F., @aIncons, .F. ) )
									EndIf
									
									if oDados:XPathHasNode( cT6KPath + "/vrUnit")
										oModel:LoadValue( "MODEL_T6K", "T6K_VLRUNT"	, FTafGetVal( cT6KPath + "/vrUnit", "N", .F., @aIncons, .F. ) )					
									EndIf
									
									if oDados:XPathHasNode( cT6KPath + "/vrRubr")
										oModel:LoadValue( "MODEL_T6K", "T6K_VLRRUB"	, FTafGetVal( cT6KPath + "/vrRubr", "N", .F., @aIncons, .F. ) )					
									EndIf
															
					             	nT6K++
									cT6KPath := cT6JPath + "/itensRemun[" + CVALTOCHAR(nT6K) + "]"
								EndDo			
																																														
				              nT6J++
								cT6JPath := cT6IPath+ "/remunPerAnt[" + CVALTOCHAR(nT6J) + "]"
							EndDo					
		 
			              nT6I++
							cT6IPath := cT6HPath + "/ideEstab[" + CVALTOCHAR(nT6I) + "]"
						EndDo
						
		              nT6H++
						cT6HPath := cT61Path + "/idePeriodo[" + CVALTOCHAR(nT6H) + "]"
					EndDo
					
					nT61++
					cT61Path := cT14Path+"/infoPerAnt/ideADC[" + CVALTOCHAR(nT61) + "]"
				EndDo
	
				nT14++
				cT14Path := "/eSocial/evtRmnRPPS/dmDev[" + CVALTOCHAR(nT14) + "]"
			EndDo
									
		EndIf
		
		//���������������������������Ŀ
		//�Efetiva a operacao desejada�
		//�����������������������������
		If Empty(cInconMsg)	.And. Empty(aIncons)
			If TafFormCommit( oModel )
				Aadd(aIncons, "ERRO19")		
			Else	
				lRet := .T.	 
			EndIf	
		Else			
			Aadd(aIncons, cInconMsg)	
			DisarmTransaction()	
		EndIf	
		
		oModel:DeActivate()
		If FindFunction('TafClearModel')
			TafClearModel(oModel)
		EndIf
				         			    								 				
	EndIf                                                                           	

End Transaction  	

//����������������������������������������������������������Ŀ
//�Zerando os arrays e os Objetos utilizados no processamento�
//������������������������������������������������������������
aSize( aRules, 0 ) 
aRules     := Nil

aSize( aChave, 0 ) 
aChave     := Nil    
    
Return { lRet, aIncons }     
          
//-------------------------------------------------------------------
/*/{Protheus.doc} TAF413Rul           
Regras para gravacao das informacoes do registro S-1200 do E-Social

@Param
nOper  - Operacao a ser realizada ( 3 = Inclusao / 4 = Alteracao / 5 = Exclusao )

@Return	
aRull  - Regras para a gravacao das informacoes


@author Vitor Siqueira
@since 08/01/2016
@version 1.0

/*/                        	
//-------------------------------------------------------------------
Static Function TAF413Rul( cInconMsg, nSeqErrGrv, cCodEvent, cOwner )
                                 
Local aRull    := {}  
Local cPeriodo := ""

Default cInconMsg		:= ""
Default nSeqErrGrv	:= 0
Default cCodEvent		:= ""
Default cOwner		:= ""

//indApuracao
If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRmnRPPS/ideEvento/indApuracao" ) )
	Aadd( aRull, {"C91_INDAPU", "/eSocial/evtRmnRPPS/ideEvento/indApuracao","C",.F.} ) 	 	     	
EndIF

//perApur
If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRmnRPPS/ideEvento/perApur" ) )
	cPeriodo := FTafGetVal("/eSocial/evtRmnRPPS/ideEvento/perApur", "C", .F.,, .F. )
	
	If At("-", cPeriodo) > 0
		Aadd( aRull, {"C91_PERAPU", StrTran(cPeriodo, "-", "" ) ,"C",.T.} )	
	Else
		Aadd( aRull, {"C91_PERAPU", cPeriodo ,"C", .T.} )		
	EndIf      
EndIf

//cpfTrab
If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtRmnRPPS/ideTrabalhador/cpfTrab" ) )
	Aadd( aRull, {"C91_TRABAL", FGetIdInt( "cpfTrab", "", "/eSocial/evtRmnRPPS/ideTrabalhador/cpfTrab",,,,@cInconMsg, @nSeqErrGrv), "C", .T. } )   
EndIF

Return ( aRull )

//-------------------------------------------------------------------
/*/{Protheus.doc} Taf413Vld

Funcao que valida os dados do registro posicionado,
verificando se ha incoerencias nas informacos 
caso seja necessario gerar um XML

lJob - Informa se foi chamado por Job

@return .T.

@author Vitor Siqueira
@since 08/01/2016
@version 1.0
/*/                                                                                                                                          
//-------------------------------------------------------------------
Function Taf413Vld(cAlias,nRecno,nOpc,lJob)

Local aLogErro   	:= {}
Local aAreaCRN		:= {}
Local aDadosUtil	:= {}

Local cChave     	:= ""
Local cMes			:= ""
Local cAno			:= ""
Local cIniESoc		:= ""

Default cAlias	:= ""
Default nRecno	:= 0
Default nOpc		:= 1
Default lJob		:= .F. 

//-- Abro as tabelas
DbSelectArea("T6C")                                                                                        
T6C->( DbSetOrder( 1 ) ) 

DbSelectArea("T6D")	
T6D->( DbSetOrder( 1 ) )	   

DbSelectArea("T6E")	
T6E->( DbSetOrder( 1 ) )	   

DbSelectArea("T6F")	
T6F->( DbSetOrder( 1 ) )	   

DbSelectArea("T6G")	
T6G->( DbSetOrder( 1 ) )	 

DbSelectArea("T61")	
T61->( DbSetOrder( 1 ) )	  

DbSelectArea("T6H")	
T6H->( DbSetOrder( 1 ) )	   

DbSelectArea("T6I")	
T6I->( DbSetOrder( 1 ) )	   

DbSelectArea("T6J")	
T6J->( DbSetOrder( 1 ) )	 

DbSelectArea("T6K")	
T6K->( DbSetOrder( 1 ) )	     
     
//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := C91->( Recno() )
If (C91->C91_STATUS) $ " 1"

	// ----------------------------------------------------
	// 			VALIDA��O REGRA_COMPATIB_REGIME_PREV
	// ----------------------------------------------------
	aDadosUtil := {}
	aAdd(aDadosUtil, {C91->C91_TRABAL})
	xVldRegra("C9V","REGRA_COMPATIB_REGIME_PREV",@aLogErro, aDadosUtil, "C91")

	// ----------------------------------------------------
	// 			VALIDA��O IND APUR - C91
	// ----------------------------------------------------                           	   
	//-- ideEvento
	//indApuracao
	If Empty(C91->C91_INDAPU)
		AADD(aLogErro,{"C91_INDAPU","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	Else
		
		If !(C91->C91_INDAPU $ ("1|2|"))
			aAdd( aLogErro, { "C91_INDAPU", "000006", "C91", nRecno } ) //STR0006 - Conte�do do campo n�o condiz com as op��es poss�veis
		Else
		
			//perApur
			If Empty(C91->C91_PERAPU)
				AADD(aLogErro,{"C91_PERAPU","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
			Else
				
				If C91->C91_INDAPU == "1"
					If !xVldFormDt(C91->C91_PERAPU, "AAAAMM") 
						AADD(aLogErro,{"C91_PERAPU","000318","C91", nRecno }) //"Informar o m�s/ano (formato MMAAAA) da folha de pagamento, se {indApuracao} for igual a [1], ou apenas o ano (formato AAAA), se {indApuracao} for igual a [2]"
					Else
					
						cMes 		:= Substr(C91->C91_PERAPU,5,2)
						cAno 		:= Substr(C91->C91_PERAPU,1,4)	
						cIniESoc	:= DTOS(xIniESoc())
				
						If !(cAno >= Substr(cIniESoc,1,4))
							AADD(aLogErro,{"C91_PERAPU","000929","C91", nRecno }) //"O per�odo de apura��o informado deve ser igual ou posterior a implementa��o do eSocial.".
						ElseIf cAno == Substr(cIniESoc,1,4)
							If cMes < Substr(cIniESoc,5,2) 
								AADD(aLogErro,{"C91_PERAPU","000929","C91", nRecno }) //"O per�odo de apura��o informado deve ser igual ou posterior a implementa��o do eSocial.".
							EndIf	
						EndIf
					
					EndIf
								
				ElseIf C91->C91_INDAPU == "2"
					If !xVldFormDt(C91->C91_PERAPU, "AAAA") 
						AADD(aLogErro,{"C91_PERAPU","000318","C91", nRecno }) //"Informar o m�s/ano (formato MMAAAA) da folha de pagamento, se {indApuracao} for igual a [1], ou apenas o ano (formato AAAA), se {indApuracao} for igual a [2]"
					Else
					
						cAno := Substr(C91->C91_PERAPU,1,4)
				
						If !(cAno >= xIniESoc())
							AADD(aLogErro,{"C91_PERAPU","000929","C91", nRecno }) //"O per�odo de apura��o informado deve ser igual ou posterior a implementa��o do eSocial.".
						EndIf
					
					EndIf
				
				EndIf
									
			EndIf
			
		EndIf

	EndIf	
	
	//--ideTrabalhador
	//cpfTrab-nisTrab-qtdDepFP
	If Empty(C91->C91_TRABAL)
		AADD(aLogErro,{"C91_TRABAL","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
	EndIf


	// ----------------------------------------------------
	// 			CRN - Processos Judiciais do Trabalhador 
	// ----------------------------------------------------  	
	DbSelectArea("CRN")
	CRN->( DbSetOrder( 1 ) )	                                                       
	
	If CRN->( MsSeek ( xFilial("CRN")+C91->(C91_ID+C91_VERSAO)))   	 				
		While !CRN->(Eof()) .And. AllTrim(C91->(C91_ID+C91_VERSAO)) == AllTrim(CRN->(CRN_ID+CRN_VERSAO))								
			
			//-- procJudTrab
			//tpTrib
			If Empty(CRN->CRN_TPTRIB)
				aAdd( aLogErro, { "CRN_TPTRIB", "000010", "C91", nRecno } ) //STR0010 - "Campo Inconsistente ou Vazio"
			Else
				If !(CRN->CRN_TPTRIB $ ("1|2|3|4|"))
					aAdd( aLogErro, { "CRN_TPTRIB", "000006", "C91", nRecno } ) //STR0006 - Conte�do do campo n�o condiz com as op��es poss�veis
				EndIf
			EndIf
			
			//nrProcJud
			If Empty(CRN->CRN_IDPROC)
				AADD(aLogErro,{"T6D_CODCAT","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
			Else
				xValRegTab( "C1G", CRN->CRN_IDPROC+"1", 8, , @aLogErro, /*Data validacao periodo*/, { "C91", "CRN_IDPROC", nRecno } )
			EndIf
			
			//codSusp	
			If Empty( CRN->CRN_IDSUSP )
				if TafLayESoc("02_04_02")
					if CRN->CRN_TPTRIB $ "1/2"
						aAdd( aLogErro, { "CRN_CODSUS", "000010", "C91", nRecno } ) //STR0010 - Campo Inconsistente ou Vazio
					endif
				else
					aAdd( aLogErro, { "CRN_CODSUS", "000010", "C91", nRecno } ) //STR0010 - Campo Inconsistente ou Vazio
				EndIf
			Else
				xValRegTab( "T5L", CRN->CRN_IDSUSP, 1, , @aLogErro, /*Data validacao periodo*/, { "C91", "CRN_IDSUSP", nRecno } )
			EndIf
					
			CRN->(DbSkip())
		EndDo 	
	EndIf	
	
	// ----------------------------------------------------
	// 			T14 - Ident. Demonstr. Val. Trabal. 
	// ----------------------------------------------------  	
	DbSelectArea("T14")
	T14->( DbSetOrder( 1 ) )	                                                        
	
	If T14->( MsSeek ( xFilial("T14")+C91->(C91_ID+C91_VERSAO)))   	 				
		While !T14->(Eof()) .And. AllTrim(C91->(C91_ID+C91_VERSAO)) == AllTrim(T14->(T14_ID+T14_VERSAO))								
			
			//-- dmDev
			//ideDmDev
			
			// ----------------------------------------------------
			// 			T6C - Informa��o do Estabelecimento/Lota��o
			// ----------------------------------------------------  
			If T6C->( MsSeek ( xFilial("T6C") + T14->(T14_ID+T14_VERSAO+T14_INDAPU+T14_PERAPU+T14_TRABAL+T14_IDEDMD) ) )   	 				
				While !T6C->(Eof()) .And. AllTrim(T6C->(T6C_ID+T6C_VERSAO+T6C_INDAPU+T6C_PERAPU+T6C_TRABAL+T6C_DEMPAG)) == AllTrim(T14->(T14_ID+T14_VERSAO+T14_INDAPU+T14_PERAPU+T14_TRABAL+T14_IDEDMD))	
					
					//-- ideEstab
					//tpInsc-nrInsc
					If Empty(T6C->T6C_ESTABE)
						AADD(aLogErro,{"T6C_ESTABE","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
					Else
						xValRegTab( "C92", T6C->T6C_ESTABE+"1", 5, , @aLogErro, /*Data validacao periodo*/, { "C91", "T6C_ESTABE", nRecno } )
					EndIf
					
					// ----------------------------------------------------
					// 			T6D - Inf. da Rem. do Trab. no Per de Apur.
					// ----------------------------------------------------                                                                                                  		
					If T6D->( MsSeek ( xFilial("T6D") + T6C->(T6C_ID+T6C_VERSAO+T6C_INDAPU+T6C_PERAPU+T6C_TRABAL+T6C_DEMPAG+T6C_ESTABE) ) )   	 				
						While !T6D->(Eof()) .And. AllTrim( T6D->(T6D_ID+T6D_VERSAO+T6D_INDAPU+T6D_PERAPU+T6D_TRABAL+T6D_DEMPAG+T6D_ESTABE) ) == AllTrim( T6C->(T6C_ID+T6C_VERSAO+T6C_INDAPU+T6C_PERAPU+T6C_TRABAL+T6C_DEMPAG+T6C_ESTABE) )	
							
							//-- remunPerApur
							//matricula
							If !Empty( T6D->T6D_IDTRAB )
								xValRegTab( "C9V", T6D->T6D_IDTRAB+"1", 2, , @aLogErro, /*Data validacao periodo*/, { "C91", "T6D_IDTRAB", nRecno } )
							EndIf
							
							//codCateg
							If Empty(T6D->T6D_CODCAT)
								AADD(aLogErro,{"T6D_CODCAT","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
							Else
								xValRegTab( "C87", T6D->T6D_CODCAT, 1, , @aLogErro, /*Data validacao periodo*/, { "C91", "T6D_CODCAT", nRecno } )
							EndIf
							
							
							// ----------------------------------------------------
							// 			T6E - Remunera��o do Trabalhador - Rubricas
							// ---------------------------------------------------- 							                                                                                                                                              
							If T6E->( MsSeek ( xFilial("T6D") + T6D->(T6D_ID+T6D_VERSAO+T6D_INDAPU+T6D_PERAPU+T6D_TRABAL+T6D_DEMPAG+T6D_ESTABE+T6D_IDTRAB+T6D_CODCAT) ) )   	 				
								While !T6E->(Eof()) .And. AllTrim( T6E->(T6E_ID+T6E_VERSAO+T6E_INDAPU+T6E_PERAPU+T6E_TRABAL+T6E_DEMPAG+T6E_ESTABE+T6E_IDTRAB+T6E_CODCAT) ) == AllTrim( T6D->(T6D_ID+T6D_VERSAO+T6D_INDAPU+T6D_PERAPU+T6D_TRABAL+T6D_DEMPAG+T6D_ESTABE+T6D_IDTRAB+T6D_CODCAT) )	
									
									//-- itensRemun
									//codRubr
									If Empty(T6E->T6E_IDRUBR)
										AADD(aLogErro,{"T6E_IDRUBR","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
									Else
										xValRegTab( "C8R", T6E->T6E_IDRUBR+"1", 5, , @aLogErro, /*Data validacao periodo*/, { "C91", "T6E_IDRUBR", nRecno } )
									EndIf
									
									//vrRubr
									If !(T6E->T6E_VLRRUB > 0)
										AADD(aLogErro,{"T6E_VLRRUB","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
									EndIf
							
								T6E->(DbSkip())
								EndDo
							EndIf
							
							
							// ------------------------------------------------------------------------------
							// 			T6F - Detalhamento dos valores pagos a Operadoras de Planos de Sa�de
							// ------------------------------------------------------------------------------									                                                                                                                                            
							If T6F->( MsSeek ( xFilial("T6F") + T6D->(T6D_ID+T6D_VERSAO+T6D_INDAPU+T6D_PERAPU+T6D_TRABAL+T6D_DEMPAG+T6D_ESTABE+T6D_IDTRAB+T6D_CODCAT) ) )   	 				
								While !T6F->(Eof()) .And. AllTrim( T6F->(T6F_ID+T6F_VERSAO+T6F_INDAPU+T6F_PERAPU+T6F_TRABAL+T6F_DEMPAG+T6F_ESTABE+T6F_IDTRAB+T6F_CODCAT) ) == AllTrim( T6D->(T6D_ID+T6D_VERSAO+T6D_INDAPU+T6D_PERAPU+T6D_TRABAL+T6D_DEMPAG+T6D_ESTABE+T6D_IDTRAB+T6D_CODCAT) )	
									
									//-- detOper
									//cnpjOper
									If Empty(T6F->T6F_CNPJOP)
										AADD(aLogErro,{"T6F_CNPJOP","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
									Else
										If !CGC(T6F->T6F_CNPJOP,,.F.)
											AADD(aLogErro,{"T6F_CNPJOP","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
										EndIF
									EndIf
									
									//regANS
									If Empty(T6F->T6F_REGANS)
										AADD(aLogErro,{"T6F_CNPJOP","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
									EndIf
									
									//vrPgTit
									If Empty(T6F->T6F_VLPGTI)
										AADD(aLogErro,{"T6F_CNPJOP","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
									EndIf
							
									// -----------------------------------------------------------------
									// 	     T6F - Informa��es do dependente do plano privado de sa�de.
									// -----------------------------------------------------------------		              
									If T6G->( MsSeek ( xFilial("T6F") + T6F->(T6F_ID+T6F_VERSAO+T6F_INDAPU+T6F_PERAPU+T6F_TRABAL+T6F_DEMPAG+T6F_ESTABE+T6F_IDTRAB+T6F_CODCAT+T6F_CNPJOP) ) )   	 				
										While !T6G->(Eof()) .And. AllTrim( T6G->(T6G_ID+T6G_VERSAO+T6G_INDAPU+T6G_PERAPU+T6G_TRABAL+T6G_DEMPAG+T6G_ESTABE+T6G_IDTRAB+T6G_CODCAT+T6G_CNPJOP) ) == AllTrim( T6F->(T6F_ID+T6F_VERSAO+T6F_INDAPU+T6F_PERAPU+T6F_TRABAL+T6F_DEMPAG+T6F_ESTABE+T6F_IDTRAB+T6F_CODCAT+T6F_CNPJOP) )	
												
												//-- cpfDep	
												//a)
												If Empty(T6G->T6G_CPFDEP) .And. xFunAge(T6G->T6G_DTNDEP, dDataBase) >= 12
													aAdd(aLogErro,{"T6G_CPFDEP","000939","C91", nRecno }) //"O preenchimento � obrigat�rio se for maior de doze anos."
												Else
													If !Empty(T6G->T6G_CPFDEP)	 
														If !CGC(T6G->T6G_CPFDEP,,.F.)
															aAdd(aLogErro,{"T6G_CPFDEP","000891","C91", nRecno }) //"O CPF � inv�lido."													                       
														EndIF
													EndIf
												EndIf
												
												//b)
																								
												
												//-- nmDep
												If Empty(T6G->T6G_NOMDEP) 
													aAdd(aLogErro,{"T6G_NOMDEP","000010","T6G",nRecno}) //"Campo Inconsistente ou Vazio"        
												EndIf		
												
												//-- dtNascto
												If Empty(T6G->T6G_DTNDEP)
													AADD(aLogErro,{"T6G_DTNDEP","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
												EndIf
												
												//-- vlrPgDep
												If Empty(T6G->T6G_VPGDEP)
													AADD(aLogErro,{"T6G_VPGDEP","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
												EndIf
													
										T6G->(DbSkip())
										EndDo
									EndIf				
							
								T6F->(DbSkip())
								EndDo
							EndIf

						T6D->(DbSkip())
						EndDo
					EndIf
					
				T6C->(DbSkip())
				EndDo
			EndIf
			
			// ----------------------------------------------------
			// 		 T61 - Ident Lei  Remun. Per.  Anter.
			// ----------------------------------------------------  
			If T61->( MsSeek ( xFilial("T61") + T14->(T14_ID+T14_VERSAO+T14_INDAPU+T14_PERAPU+T14_TRABAL+T14_IDEDMD) ) )   	 				
				While !T61->(Eof()) .And. AllTrim(T61->(T61_ID+T61_VERSAO+T61_INDAPU+T61_PERAPU+T61_TRABAL+T61_DEMPAG)) == AllTrim(T14->(T14_ID+T14_VERSAO+T14_INDAPU+T14_PERAPU+T14_TRABAL+T14_IDEDMD))	
					
					//dtLei
					If Empty(T61->T61_DTLEI)
						AADD(aLogErro,{"T61_DTLEI","000010","T61", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
					EndIf
					
					//nrLei
					If Empty(T61->T61_NUMLEI)
						AADD(aLogErro,{"T61_NUMLEI","000010","T61", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
					EndIf
					
					//dtEf
					
					
					// ----------------------------------------------------
					// 		   T6H - Informa��es do Periodo
					// ----------------------------------------------------  
					If T6H->( MsSeek ( xFilial("T6H")+T61->(T61_ID+T61_VERSAO+T61_INDAPU+T61_PERAPU+T61_TRABAL+T61_DEMPAG+DTOS(T61_DTLEI)+T61_NUMLEI) ) )   	 				
						While !T6H->(Eof()) .And. AllTrim(T6H->(T6H_ID+T6H_VERSAO+T6H_INDAPU+T6H_PERAPU+T6H_TRABAL+T6H_DEMPAG+DTOS(T6H_DTLEI)+T6H_NUMLEI)) == AllTrim(T61->(T61_ID+T61_VERSAO+T61_INDAPU+T61_PERAPU+T61_TRABAL+T61_DEMPAG+DTOS(T61_DTLEI)+T61_NUMLEI))	
							
							//perRef
							If Empty(T6H->T6H_PERREF)
								AADD(aLogErro,{"T6H_PERREF","000010","T6H", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
							Else
								If (Len(Alltrim(T6H->T6H_PERREF)) <> 6 .And. T6H->T6H_INDAPU == '1') .OR. (Len(Alltrim(T6H->T6H_PERREF)) <> 4 .And. T6H->T6H_INDAPU == '2' ) 
									aAdd(aLogErro,{"T1S_PERAPU","000816","T1S", nRecno }) //"Informar o m�s/ano (formato AAAA-MM) de refer�ncia das informa��es, se {indApuracao} for igual a [1], ou apenas o ano (formato AAAA), se {indApuracao} for igual a [2]" 
								Endif	
							EndIf
											
							// ----------------------------------------------------
							// 			T6I - Informa��o do Estabelecimento/Lota��o
							// ----------------------------------------------------  
							If T6I->( MsSeek ( xFilial("T6I") + T6H->(T6H_ID+T6H_VERSAO+T6H_INDAPU+T6H_PERAPU+T6H_TRABAL+T6H_DEMPAG+DTOS(T6H_DTLEI)+T6H_NUMLEI+T6H_PERREF)) )   	 				
								While !T6I->(Eof()) .And. AllTrim(T6I->(T6I_ID+T6I_VERSAO+T6I_INDAPU+T6I_PERAPU+T6I_TRABAL+T6I_DEMPAG+DTOS(T6I_DTLEI)+T6I_NUMLEI+T6I_PERREF)) == AllTrim(T6H->(T6H_ID+T6H_VERSAO+T6H_INDAPU+T6H_PERAPU+T6H_TRABAL+T6H_DEMPAG+DTOS(T6H_DTLEI)+T6H_NUMLEI+T6H_PERREF))	
									
									//tpInsc-nrInsc
									If Empty(T6I->T6I_ESTABE)
										AADD(aLogErro,{"T6I_ESTABE","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
									Else
										xValRegTab( "C92", T6I->T6I_ESTABE+"1", 5, , @aLogErro, /*Data validacao periodo*/, { "C91", "T6I_ESTABE", nRecno } )
									EndIf
									
									// ----------------------------------------------------
									// 			T6J - Inf. da Rem. do Trab. no Per de Apur.
									// ---------------------------------------------------- 	                                                                                                  			
									If T6J->( MsSeek ( xFilial("T6J") + T6I->(T6I_ID+T6I_VERSAO+T6I_INDAPU+T6I_PERAPU+T6I_TRABAL+T6I_DEMPAG+DTOS(T6I_DTLEI)+T6I_NUMLEI+T6I_PERREF+T6I_ESTABE)) )   	 				
										While !T6J->(Eof()) .And. AllTrim(T6J->(T6J_ID+T6J_VERSAO+T6J_INDAPU+T6J_PERAPU+T6J_TRABAL+T6J_DEMPAG+DTOS(T6J_DTLEI)+T6J_NUMLEI+T6J_PERREF+T6J_ESTABE)) == AllTrim(T6I->(T6I_ID+T6I_VERSAO+T6I_INDAPU+T6I_PERAPU+T6I_TRABAL+T6I_DEMPAG+DTOS(T6I_DTLEI)+T6I_NUMLEI+T6I_PERREF+T6I_ESTABE))	
											
											//matricula
											If !Empty( T6J->T6J_IDTRAB )
												xValRegTab( "C9V", T6J->T6J_IDTRAB+"1", 2, , @aLogErro, /*Data validacao periodo*/, { "C91", "T6J_IDTRAB", nRecno } )
											EndIf
											
											//codCateg
											If Empty(T6J->T6J_CODCAT)
												AADD(aLogErro,{"T6J_CODCAT","000010","C91", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
											Else
												xValRegTab( "C87", T6J->T6J_CODCAT, 1, , @aLogErro, /*Data validacao periodo*/, { "C91", "T6J_CODCAT", nRecno } )
											EndIf
											
											
											// ----------------------------------------------------
											// 			T6K - Itens da Remunera��o Trab.
											// ---------------------------------------------------- 							                                                                                                                                              
											If T6K->( MsSeek ( xFilial("T6K") + T6J->(T6J_ID+T6J_VERSAO+T6J_INDAPU+T6J_PERAPU+T6J_TRABAL+T6J_DEMPAG+DTOS(T6J_DTLEI)+T6J_NUMLEI+T6J_PERREF+T6J_ESTABE+T6J_IDTRAB+T6J_CODCAT)) )   	 				
												While !T6K->(Eof()) .And. AllTrim(T6K->(T6K_ID+T6K_VERSAO+T6K_INDAPU+T6K_PERAPU+T6K_TRABAL+T6K_DEMPAG+DTOS(T6K_DTLEI)+T6K_NUMLEI+T6K_PERREF+T6K_ESTABE+T6K_IDTRAB+T6K_CODCAT)) == AllTrim(T6J->(T6J_ID+T6J_VERSAO+T6J_INDAPU+T6J_PERAPU+T6J_TRABAL+T6J_DEMPAG+DTOS(T6J_DTLEI)+T6J_NUMLEI+T6J_PERREF+T6J_ESTABE+T6J_IDTRAB+T6J_CODCAT))	
													
													//codRubr
													If Empty(T6K->T6K_IDRUBR)
														AADD(aLogErro,{"T6K_IDRUBR","000010","T6K", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
													Else
														xValRegTab( "C8R", T6K->T6K_IDRUBR+"1", 5, , @aLogErro, /*Data validacao periodo*/, { "C91", "T6K_IDRUBR", nRecno } )
													EndIf
													
													//vrRubr
													If !(T6K->T6K_VLRRUB > 0)
														AADD(aLogErro,{"T6K_VLRRUB","000010","T6K", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"	
													EndIf
											
												T6K->(DbSkip())
												EndDo
											EndIf			
											
										T6J->(DbSkip())
										EndDo
									EndIf
									
								T6I->(DbSkip())
								EndDo
							EndIf
							
						T6H->(DbSkip())
						EndDo
					EndIf						
							
				T61->(DbSkip())
				EndDo
			EndIf			
					
		T14->(DbSkip())
		EndDo 	
	EndIf	
	            	
	//*�����������������������������Ŀ
	// �ATUALIZO O STATUS DO REGISTRO�
	// �1 = Registro Invalido        �
	// �0 = Registro Valido          �
	// �������������������������������*/
	cStatus := Iif(Len(aLogErro) > 0,"1","0")

	If RecLock("C91",.F.)
		C91->C91_STATUS := cStatus
		C91->(MsUnlock())
	EndIf
	
Else
	AADD(aLogErro,{"C91","000305","C91",nRecno}) //Registros que j� foram transmitidos ao Fisco, n�o podem ser validados		
EndIf	

//�������������������������������������������������������Ŀ
//�N�o apresento o alert quando utilizo o JOB para validar�
//���������������������������������������������������������
If !lJob
	xValLogEr(aLogErro)
EndIf	

Return(aLogErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} GerarEvtExc
Funcao que gera a exclus�o do evento

@Param  oModel  -> Modelo de dados
@Param  nRecno  -> Numero do recno
@Param  lRotExc -> Variavel que controla se a function � chamada pelo TafIntegraESocial

@Return .T.

@Author Vitor Henrique Ferreira
@Since 11/01/2015
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function GerarEvtExc( oModel, nRecno, lRotExc )

Local cVerAnt    	:= ""
Local cProtocolo	:= ""
Local cVersao   	:= ""
Local cChvRegAnt 	:= ""
Local cEvento	  	:= ""
Local cId		  	:= ""

local nOperation	:= 0
Local nlI  		:= 0
Local nlY  		:= 0
Local nCRN		   	:= 0
Local nCRNAdd 	:= 0
Local nT14		  	:= 0
Local nT14Add	   	:= 0
Local nT6C      	:= 0
Local nT6CAdd   	:= 0
Local nT6D       	:= 0
Local nT6DAdd    	:= 0
Local nT6E       	:= 0
Local nT6EAdd   	:= 0
Local nT6F       	:= 0
Local nT6FAdd    	:= 0
Local nT6G       	:= 0
Local nT6GAdd   	:= 0
Local nT61      	:= 0
Local nT61Add    	:= 0
Local nT6H       	:= 0
Local nT6HAdd    	:= 0
Local nT6I       	:= 0
Local nT6IAdd   	:= 0
Local nT6J       	:= 0
Local nT6JAdd    	:= 0
Local nT6K       	:= 0
Local nT6KAdd    	:= 0

Local aGrava    	:= {}
Local aGravaCRN  	:= {}
Local aGravaT14 	:= {}
Local aGravaT6C  	:= {}
Local aGravaT6D  	:= {}
Local aGravaT6E  	:= {}
Local aGravaT6F 	:= {}
Local aGravaT6G  	:= {}
Local aGravaT61  	:= {}
Local aGravaT6H  	:= {}
Local aGravaT6I  	:= {}
Local aGravaT6J  	:= {}
Local aGravaT6K  	:= {}

Local oModelC91  	:= Nil
Local oModelCRN  	:= Nil
Local oModelT14  	:= Nil
Local oModelT6C  	:= Nil
Local oModelT6D  	:= Nil
Local oModelT6E  	:= Nil
Local oModelT6F  	:= Nil
Local oModelT6G  	:= Nil
Local oModelT61  	:= Nil
Local oModelT6H  	:= Nil
Local oModelT6I  	:= Nil
Local oModelT6J  	:= Nil
Local oModelT6K  	:= Nil

Default oModel	:= Nil
Default nRecno	:= 0
Default lRotExc	:= .F.

nOperation	:= oModel:GetOperation()

Begin Transaction

	//Posiciona o item
	("C91")->( DBGoTo( nRecno ) )
	
	oModelC91 := oModel:GetModel( 'MODEL_C91' )     
	oModelCRN := oModel:GetModel( 'MODEL_CRN' ) 
	
	oModelT14 := oModel:GetModel( 'MODEL_T14' ) 
	oModelT6C := oModel:GetModel( 'MODEL_T6C' )          
	oModelT6D := oModel:GetModel( 'MODEL_T6D' )     
	oModelT6E := oModel:GetModel( 'MODEL_T6E' )  
	oModelT6F := oModel:GetModel( 'MODEL_T6F' )
	oModelT6G := oModel:GetModel( 'MODEL_T6G' )
	
	oModelT61 := oModel:GetModel( 'MODEL_T61' )   						     						
	oModelT6H := oModel:GetModel( 'MODEL_T6H' )  
	oModelT6I := oModel:GetModel( 'MODEL_T6I' )    									    						
	oModelT6J := oModel:GetModel( 'MODEL_T6J' )     						
	oModelT6K := oModel:GetModel( 'MODEL_T6K' )    					  						    						  																
							
	//�����������������������������������������������������������Ŀ
	//�Busco a versao anterior do registro para gravacao do rastro�
	//�������������������������������������������������������������
	cVerAnt   	:= oModelC91:GetValue( "C91_VERSAO" )				
	cProtocolo	:= oModelC91:GetValue( "C91_PROTUL" )
	cEvento	:= oModelC91:GetValue( "C91_EVENTO" )
	
	//�����������������������������������������������������������������Ŀ
	//�Neste momento eu gravo as informacoes que foram carregadas       �
	//�na tela, pois neste momento o usuario ja fez as modificacoes que �
	//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
	//�nao devem ser consideradas neste momento                         �
	//�������������������������������������������������������������������

	/*------------------------------------------
		C91 - Folha de Pagamento  
	--------------------------------------------*/
	For nlI := 1 To 1
		For nlY := 1 To Len( oModelC91:aDataModel[ nlI ] )			
			Aadd( aGrava, { oModelC91:aDataModel[ nlI, nlY, 1 ], oModelC91:aDataModel[ nlI, nlY, 2 ] } )									
		Next
	Next 
			
	/*------------------------------------------
		CRN - Proc. Judiciario Remunera��o   
	--------------------------------------------*/
	For nlI := 1 To oModel:GetModel( 'MODEL_CRN' ):Length()				 
		oModel:GetModel( 'MODEL_CRN' ):GoLine(nlI)
		
		If !oModel:GetModel( 'MODEL_CRN' ):IsEmpty()
			If !oModel:GetModel( 'MODEL_CRN' ):IsDeleted()								
				aAdd (aGravaCRN,{	oModelCRN:GetValue('CRN_TPTRIB')	,;
								 	oModelCRN:GetValue('CRN_IDPROC')	,;
								  	oModelCRN:GetValue('CRN_IDSUSP')	})
			EndIf
		EndIf					
	
	Next	
		
	/*------------------------------------------
		T14 - Ident. Demonstr. Val. Trabal. 
	--------------------------------------------*/
	For nT14 := 1 To oModel:GetModel( 'MODEL_T14' ):Length() 
		oModel:GetModel( 'MODEL_T14' ):GoLine(nT14)
		
		If !oModel:GetModel( 'MODEL_T14' ):IsEmpty()
			If !oModel:GetModel( 'MODEL_T14' ):IsDeleted()								
				aAdd (aGravaT14 ,{oModelC91:GetValue('C91_INDAPU'),;
									oModelC91:GetValue('C91_PERAPU'),;
									oModelC91:GetValue('C91_TRABAL'),;
							       oModelT14:GetValue('T14_IDEDMD')})
						  
							  
			/*------------------------------------------
				T6C - Identifica��o do Estabelecimen
			--------------------------------------------*/			  		
			For nT6C := 1 to oModel:GetModel( "MODEL_T6C" ):Length()
				oModel:GetModel( "MODEL_T6C" ):GoLine(nT6C)
				
				If !oModel:GetModel( 'MODEL_T6C' ):IsEmpty()	
					If !oModel:GetModel( "MODEL_T6C" ):IsDeleted()
						aAdd (aGravaT6C ,{oModelC91:GetValue('C91_INDAPU'),;
											oModelC91:GetValue('C91_PERAPU'),;
											oModelC91:GetValue('C91_TRABAL'),;
											oModelT14:GetValue('T14_IDEDMD'),;
											oModelT6C:GetValue('T6C_ESTABE')}) 
							
							
							
							/*------------------------------------------
								T6D - Remun Trabal. Per�odo Apura��o
							--------------------------------------------*/			  		
							For nT6D := 1 to oModel:GetModel( "MODEL_T6D" ):Length()
								oModel:GetModel( "MODEL_T6D" ):GoLine(nT6D)
								
								If !oModel:GetModel( 'MODEL_T6D' ):IsEmpty()	
									If !oModel:GetModel( "MODEL_T6D" ):IsDeleted()
										aAdd (aGravaT6D ,{oModelC91:GetValue('C91_INDAPU'),;
															oModelC91:GetValue('C91_PERAPU'),;
															oModelC91:GetValue('C91_TRABAL'),; 
															oModelT14:GetValue('T14_IDEDMD'),;
															oModelT6C:GetValue('T6C_ESTABE'),;
														   	oModelT6D:GetValue('T6D_IDTRAB'),;
														   	oModelT6D:GetValue('T6D_CODCAT')})
								
										/*------------------------------------------
											T6E - Itens Remun. do Trabalhador   
										--------------------------------------------*/
										For nT6E := 1 to oModel:GetModel( "MODEL_T6E" ):Length()
											oModel:GetModel( "MODEL_T6E" ):GoLine(nT6E)
											
											If !oModel:GetModel( 'MODEL_T6E' ):IsEmpty()	
												If !oModel:GetModel( "MODEL_T6E" ):IsDeleted()
													aAdd (aGravaT6E ,{oModelC91:GetValue('C91_INDAPU'),;
																		oModelC91:GetValue('C91_PERAPU'),;
																		oModelC91:GetValue('C91_TRABAL'),;
																		oModelT14:GetValue('T14_IDEDMD'),;
																		oModelT6C:GetValue('T6C_ESTABE'),;
																		oModelT6D:GetValue('T6D_IDTRAB'),;
																		oModelT6D:GetValue('T6D_CODCAT'),;
																	   	oModelT6E:GetValue('T6E_IDRUBR'),;
																	   	oModelT6E:GetValue('T6E_QTDRUB'),;
																	   	oModelT6E:GetValue('T6E_FATORR'),;
																	   	oModelT6E:GetValue('T6E_VLRUNT'),;
																	   	oModelT6E:GetValue('T6E_VLRRUB')}) 
												EndIf
											EndIf
										Next //nT6E						
										
									
										/*------------------------------------------
											 T6F - Informa��es do Plano de Sa�de 
										--------------------------------------------*/
										For nT6F := 1 to oModel:GetModel( "MODEL_T6F" ):Length()
											oModel:GetModel( "MODEL_T6F" ):GoLine(nT6F)
											
											If !oModel:GetModel( 'MODEL_T6F' ):IsEmpty()	
												If !oModel:GetModel( "MODEL_T6F" ):IsDeleted()
													aAdd (aGravaT6F ,{oModelC91:GetValue('C91_INDAPU'),;
																		oModelC91:GetValue('C91_PERAPU'),;
																		oModelC91:GetValue('C91_TRABAL'),;
																		oModelT14:GetValue('T14_IDEDMD'),;
																		oModelT6C:GetValue('T6C_ESTABE'),;
																		oModelT6D:GetValue('T6D_IDTRAB'),;
																		oModelT6D:GetValue('T6D_CODCAT'),;
																	   	oModelT6F:GetValue('T6F_CNPJOP'),;
																	   	oModelT6F:GetValue('T6F_REGANS'),;
																	   	oModelT6F:GetValue('T6F_VLPGTI')})
								
								
													/*------------------------------------------
													 	T6G - Informa��es dos Dependentes
													--------------------------------------------*/				  
													For nT6G := 1 to oModel:GetModel( "MODEL_T6G" ):Length()
														oModel:GetModel( "MODEL_T6G" ):GoLine(nT6G)
													
														If !oModel:GetModel( 'MODEL_T6G' ):IsEmpty()	
															If !oModel:GetModel( "MODEL_T6G" ):IsDeleted()
																aAdd (aGravaT6G ,{oModelC91:GetValue('C91_INDAPU'),;	//1
																					oModelC91:GetValue('C91_PERAPU'),;	//2
																					oModelC91:GetValue('C91_TRABAL'),;	//3
																					oModelT14:GetValue('T14_IDEDMD'),;	//4
																					oModelT6C:GetValue('T6C_ESTABE'),;	//5
																					oModelT6D:GetValue('T6D_IDTRAB'),;	//6
																					oModelT6D:GetValue('T6D_CODCAT'),;	//7
																					oModelT6F:GetValue('T6F_CNPJOP'),;	//8
																				   	oModelT6G:GetValue('T6G_CPFDEP'),;	//9
																				   	oModelT6G:GetValue('T6G_DTNDEP'),;	//10
																				   	oModelT6G:GetValue('T6G_NOMDEP'),;	//11
																				   	oModelT6G:GetValue('T6G_VPGDEP'),;  //12
																				   	oModelT6G:GetValue('T6G_TPDEP')})	//13 
															EndIf
														EndIf	
													Next //nT6G 						
										
												EndIf
											EndIf
										Next //nT6F
																		
									EndIf
								EndIf
							Next //nT6D		
				
						EndIf
					EndIf
				Next //nT6C		
				
				/*------------------------------------------
					T61 - Ident Lei  Remun. Per.  Anter.
				--------------------------------------------*/
				For nT61 := 1 To oModel:GetModel( 'MODEL_T61' ):Length() 
					oModel:GetModel( 'MODEL_T61' ):GoLine(nT61)
					If !oModel:GetModel( 'MODEL_T61' ):IsEmpty()
						If !oModel:GetModel( 'MODEL_T61' ):IsDeleted()								
							aAdd (aGravaT61 ,{oModelC91:GetValue('C91_INDAPU'),;
								 				oModelC91:GetValue('C91_PERAPU'),;
								 				oModelC91:GetValue('C91_TRABAL'),;
								 				oModelT14:GetValue('T14_IDEDMD'),;
										       oModelT61:GetValue('T61_DTLEI'),;
										       oModelT61:GetValue('T61_NUMLEI'),;
										    	oModelT61:GetValue('T61_DTEFET')})
							
							/*------------------------------------
								T6H - Informa��es do Periodo
							-------------------------------------*/			  		
							For nT6H := 1 to oModel:GetModel( "MODEL_T6H" ):Length()
								oModel:GetModel( "MODEL_T6H" ):GoLine(nT6H)
								If !oModel:GetModel( 'MODEL_T6H' ):IsEmpty()	
									If !oModel:GetModel( "MODEL_T6H" ):IsDeleted()
										aAdd(aGravaT6H ,{	oModelC91:GetValue('C91_INDAPU'),;
														   	oModelC91:GetValue('C91_PERAPU'),;
														   	oModelC91:GetValue('C91_TRABAL'),;
														   	oModelT14:GetValue('T14_IDEDMD'),;
														 	oModelT61:GetValue('T61_DTLEI'),;
														   	oModelT61:GetValue('T61_NUMLEI'),;
														   	oModelT6H:GetValue('T6H_PERREF')})
														  
										/*------------------------------------
											T6I - Ident. do Estabelecimento
										-------------------------------------*/			  		
										For nT6I := 1 to oModel:GetModel( "MODEL_T6I" ):Length()
											oModel:GetModel( "MODEL_T6I" ):GoLine(nT6I)
											If !oModel:GetModel( 'MODEL_T6I' ):IsEmpty()	
												If !oModel:GetModel( "MODEL_T6I" ):IsDeleted()
													aAdd (aGravaT6I ,{oModelC91:GetValue('C91_INDAPU'),;
																		oModelC91:GetValue('C91_PERAPU'),;
																		oModelC91:GetValue('C91_TRABAL'),;
																		oModelT14:GetValue('T14_IDEDMD'),;
																		oModelT61:GetValue('T61_DTLEI'),;
																		oModelT61:GetValue('T61_NUMLEI'),;
																		oModelT6H:GetValue('T6H_PERREF'),;
																	   	oModelT6I:GetValue('T6I_ESTABE')})
												  
													/*------------------------------------------
														T6J - Informa��es da Remunera��o Trab.
													--------------------------------------------*/			  		
													For nT6J := 1 to oModel:GetModel( "MODEL_T6J" ):Length()
														oModel:GetModel( "MODEL_T6J" ):GoLine(nT6J)
														If !oModel:GetModel( 'MODEL_T6J' ):IsEmpty()	
															If !oModel:GetModel( "MODEL_T6J" ):IsDeleted()
																aAdd (aGravaT6J ,{oModelC91:GetValue('C91_INDAPU'),;
																					oModelC91:GetValue('C91_PERAPU'),;
																					oModelC91:GetValue('C91_TRABAL'),; 
																					oModelT14:GetValue('T14_IDEDMD'),;
																					oModelT61:GetValue('T61_DTLEI'),;
																					oModelT61:GetValue('T61_NUMLEI'),;
																					oModelT6H:GetValue('T6H_PERREF'),;
																					oModelT6I:GetValue('T6I_ESTABE'),;
														   						   	oModelT6J:GetValue('T6J_IDTRAB'),;
														                        	oModelT6J:GetValue('T6J_CODCAT')})
								
													
																/*------------------------------------------
																	 T6K - Itens da Remunera��o Trab.
																--------------------------------------------*/
																For nT6K := 1 to oModel:GetModel( "MODEL_T6K" ):Length()
																	oModel:GetModel( "MODEL_T6K" ):GoLine(nT6K)
																	If !oModel:GetModel( 'MODEL_T6K' ):IsEmpty()	
																		If !oModel:GetModel( "MODEL_T6K" ):IsDeleted()
																			aAdd (aGravaT6K ,{oModelC91:GetValue('C91_INDAPU'),;
																								oModelC91:GetValue('C91_PERAPU'),;
																								oModelC91:GetValue('C91_TRABAL'),;
																								oModelT14:GetValue('T14_IDEDMD'),;
																								oModelT61:GetValue('T61_DTLEI'),;
																								oModelT61:GetValue('T61_NUMLEI'),;
																								oModelT6H:GetValue('T6H_PERREF'),;
																								oModelT6I:GetValue('T6I_ESTABE'),;
																								oModelT6J:GetValue('T6J_IDTRAB'),;
																								oModelT6J:GetValue('T6J_CODCAT'),;
																								oModelT6K:GetValue('T6K_IDRUBR'),;
															    				              oModelT6K:GetValue('T6K_QTDRUB'),;
															    						       oModelT6K:GetValue('T6K_FATORR'),;
															    						       oModelT6K:GetValue('T6K_VLRUNT'),;
															    						       oModelT6K:GetValue('T6K_VLRRUB')}) 
																		EndIf
																	EndIf
																Next //nT6K
												
															EndIf
														EndIf
													Next //nT6J
													
												EndIf
											EndIf
										Next //nT6I																	
									
									EndIf
								EndIf
							Next //nT6H			
						
						EndIf
					EndIf
				Next // nT61	
			
			EndIf
		EndIf
	Next //nT14
										  				
	//�����������������������������������������������������������Ŀ
	//�Seto o campo como Inativo e gravo a versao do novo registro�
	//�no registro anterior                                       � 
	//|                                                           |
	//|ATENCAO -> A alteracao destes campos deve sempre estar     |
	//|abaixo do Loop do For, pois devem substituir as informacoes|
	//|que foram armazenadas no Loop acima                        |
	//�������������������������������������������������������������
	FAltRegAnt( 'C91', '2' )

	//��������������������������������������������������Ŀ
	//�Neste momento eu preciso setar a operacao do model�
	//�como Inclusao                                     �
	//����������������������������������������������������
	oModel:DeActivate()
	oModel:SetOperation( 3 ) 	
	oModel:Activate()		
					
	//�������������������������������������������������������Ŀ
	//�Neste momento eu realizo a inclusao do novo registro ja�
	//�contemplando as informacoes alteradas pelo usuario     �
	//���������������������������������������������������������
	/*------------------------------------------
		C91 - Folha de Pagamento  
	--------------------------------------------*/
	For nlI := 1 To Len( aGrava )	
		oModel:LoadValue( 'MODEL_C91', aGrava[ nlI, 1 ], aGrava[ nlI, 2 ] )
	Next   
	
	/*------------------------------------------
		CRN - Proc. Judiciario Remunera��o   
	--------------------------------------------*/				
	For nlI := 1 To Len( aGravaCRN )
		If nlI > 1
			oModel:GetModel( 'MODEL_CRN' ):AddLine()
		EndIf
		oModel:LoadValue( "MODEL_CRN", "CRN_TPTRIB" ,	aGravaCRN[nlI][1] )
		oModel:LoadValue( "MODEL_CRN", "CRN_IDPROC" ,	aGravaCRN[nlI][2] )
		oModel:LoadValue( "MODEL_CRN", "CRN_IDSUSP" ,	aGravaCRN[nlI][3] )
	Next			
	
	/*------------------------------------------
		T14 - Ident. Demonstr. Val. Trabal. 
	--------------------------------------------*/
	For nT14 := 1 to Len( aGravaT14 )
					
		oModel:GetModel( 'MODEL_T14' ):LVALID	:= .T.
		
		If nT14 > 1
			oModel:GetModel( "MODEL_T14" ):AddLine()
		EndIf
		
		oModel:LoadValue( "MODEL_T14", "T14_IDEDMD", aGravaT14[nT14][4] )
		
		/*------------------------------------------
		   T6C - Informa��es da Remunera��o Trab.
		--------------------------------------------*/
		nT6CAdd := 1	
		For nT6C := 1 to Len( aGravaT6C )		
			If aGravaT14[nT14][1]+aGravaT14[nT14][2]+aGravaT14[nT14][3]+aGravaT14[nT14][4] == aGravaT6C[nT6C][1]+aGravaT6C[nT6C][2]+aGravaT6C[nT6C][3]+aGravaT6C[nT6C][4]
				oModel:GetModel( 'MODEL_T6C' ):LVALID := .T.
				
				If nT6CAdd > 1
					oModel:GetModel( "MODEL_T6C" ):AddLine()
				EndIf
				
				oModel:LoadValue( "MODEL_T6C", "T6C_ESTABE", aGravaT6C[nT6C][5] )

				/*------------------------------------------
				   T6D - Informa��es da Remunera��o Trab.
				--------------------------------------------*/
				nT6DAdd := 1	
				For nT6D := 1 to Len( aGravaT6D )		
					If aGravaT6C[nT6C][1]+aGravaT6C[nT6C][2]+aGravaT6C[nT6C][3]+aGravaT6C[nT6C][4]+aGravaT6C[nT6C][5] == aGravaT6D[nT6D][1]+aGravaT6D[nT6D][2]+aGravaT6D[nT6D][3]+aGravaT6D[nT6D][4]+aGravaT6D[nT6D][5]
						oModel:GetModel( 'MODEL_T6D' ):LVALID := .T.
						
						If nT6DAdd > 1
							oModel:GetModel( "MODEL_T6D" ):AddLine()
						EndIf
						
						oModel:LoadValue( "MODEL_T6D", "T6D_IDTRAB", aGravaT6D[nT6D][6] )
						oModel:LoadValue( "MODEL_T6D", "T6D_CODCAT", aGravaT6D[nT6D][7] )
					
						/*------------------------------------------
						   	  T6E - Itens da Remunera��o Trab.
						--------------------------------------------*/
						nT6EAdd := 1	
						For nT6E := 1 to Len( aGravaT6E )		
							If aGravaT6D[nT6D][1]+aGravaT6D[nT6D][2]+aGravaT6D[nT6D][3]+aGravaT6D[nT6D][4]+aGravaT6D[nT6D][5]+aGravaT6D[nT6D][6]+aGravaT6D[nT6D][7] == aGravaT6E[nT6E][1]+aGravaT6E[nT6E][2]+aGravaT6E[nT6E][3]+aGravaT6E[nT6E][4]+aGravaT6E[nT6E][5]+aGravaT6E[nT6E][6]+aGravaT6E[nT6E][7]
								oModel:GetModel( 'MODEL_T6E' ):LVALID := .T.
								
								If nT6EAdd > 1
									oModel:GetModel( "MODEL_T6E" ):AddLine()
								EndIf
								
								oModel:LoadValue( "MODEL_T6E", "T6E_IDRUBR",	aGravaT6E[nT6E][8] )
								oModel:LoadValue( "MODEL_T6E", "T6E_QTDRUB",	aGravaT6E[nT6E][9] )
								oModel:LoadValue( "MODEL_T6E", "T6E_FATORR",	aGravaT6E[nT6E][10] )
								oModel:LoadValue( "MODEL_T6E", "T6E_VLRUNT",	aGravaT6E[nT6E][11] )
								oModel:LoadValue( "MODEL_T6E", "T6E_VLRRUB",	aGravaT6E[nT6E][12] )
								
								nT6EAdd++
								
							EndIf			
						Next //nT6E
					
						/*------------------------------------------
						   	  T6F - Informa��es do Plano de Sa�de 
						--------------------------------------------*/
						nT6FAdd := 1	
						For nT6F := 1 to Len( aGravaT6F )		
							If aGravaT6D[nT6D][1]+aGravaT6D[nT6D][2]+aGravaT6D[nT6D][3]+aGravaT6D[nT6D][4]+aGravaT6D[nT6D][5]+aGravaT6D[nT6D][6]+aGravaT6D[nT6D][7] == aGravaT6F[nT6F][1]+aGravaT6F[nT6F][2]+aGravaT6F[nT6F][3]+aGravaT6F[nT6F][4]+aGravaT6F[nT6F][5]+aGravaT6F[nT6F][6]+aGravaT6F[nT6F][7]
								oModel:GetModel( 'MODEL_T6F' ):LVALID := .T.
								
								If nT6FAdd > 1
									oModel:GetModel( "MODEL_T6F" ):AddLine()
								EndIf
								
								oModel:LoadValue( "MODEL_T6F", "T6F_CNPJOP",	aGravaT6F[nT6F][8] )
								oModel:LoadValue( "MODEL_T6F", "T6F_REGANS",	aGravaT6F[nT6F][9] )
								oModel:LoadValue( "MODEL_T6F", "T6F_VLPGTI",	aGravaT6F[nT6F][10] )
								
								/*------------------------------------------
								   	  T6G - Informa��es dos Dependentes
								--------------------------------------------*/
								nT6GAdd := 1	
								For nT6G := 1 to Len( aGravaT6G )		
									If aGravaT6F[nT6F][1]+aGravaT6F[nT6F][2]+aGravaT6F[nT6F][3]+aGravaT6F[nT6F][4]+aGravaT6F[nT6F][5]+aGravaT6F[nT6F][6]+aGravaT6F[nT6F][7]+aGravaT6F[nT6F][8]+aGravaT6F[nT6F][9] == aGravaT6G[nT6G][1]+aGravaT6G[nT6G][2]+aGravaT6G[nT6G][3]+aGravaT6G[nT6G][4]+aGravaT6G[nT6G][5]+aGravaT6G[nT6G][6]+aGravaT6G[nT6G][7]+aGravaT6G[nT6G][8]+aGravaT6G[nT6G][9]
										oModel:GetModel( 'MODEL_T6G' ):LVALID := .T.
										
										If nT6GAdd > 1
											oModel:GetModel( "MODEL_T6G" ):AddLine()
										EndIf
										oModel:LoadValue( "MODEL_T6G", "T6G_TPDEP",		aGravaT6G[nT6G][14] )
										oModel:LoadValue( "MODEL_T6G", "T6G_CPFDEP",	aGravaT6G[nT6G][10] )
										oModel:LoadValue( "MODEL_T6G", "T6G_NOMDEP",	aGravaT6G[nT6G][11] )
										oModel:LoadValue( "MODEL_T6G", "T6G_DTNDEP",	aGravaT6G[nT6G][12] )
										oModel:LoadValue( "MODEL_T6G", "T6G_VPGDEP",	aGravaT6G[nT6G][13] )
										
										nT6GAdd++		
									EndIf			
								Next //nT6G													
					
								nT6FAdd++		
							EndIf			
						Next //nT6F
														
						nT6DAdd++		
					EndIf			
				Next //nT6D
			
				nT6CAdd++		
			EndIf		
		Next //nT6C
	
	
		/*------------------------------------------
			T61 - Ident Lei  Remun. Per.  Anter.
		--------------------------------------------*/
		For nT61 := 1 to Len( aGravaT61 )
		
			If aGravaT14[nT14][1]+aGravaT14[nT14][2]+aGravaT14[nT14][3]+aGravaT14[nT14][4] == aGravaT61[nT61][1]+aGravaT61[nT61][2]+aGravaT61[nT61][3]+aGravaT61[nT61][4]								
				oModel:GetModel( 'MODEL_T61' ):LVALID	:= .T.
				
				If nT61 > 1
					oModel:GetModel( "MODEL_T61" ):AddLine()
				EndIf
				
				oModel:LoadValue( "MODEL_T61", "T61_DTLEI ", aGravaT61[nT61][5] )
				oModel:LoadValue( "MODEL_T61", "T61_NUMLEI", aGravaT61[nT61][6] )
				oModel:LoadValue( "MODEL_T61", "T61_DTEFET", aGravaT61[nT61][7] )
				
				/*------------------------------------------
				   T6H - Informa��es do Periodo
				--------------------------------------------*/
				nT6HAdd := 1	
				For nT6H := 1 to Len( aGravaT6H )		
					If aGravaT61[nT61][1]+aGravaT61[nT61][2]+aGravaT61[nT61][3]+aGravaT61[nT61][4]+DtoC(aGravaT61[nT61][5])+aGravaT61[nT61][6] == aGravaT6H[nT6H][1]+aGravaT6H[nT6H][2]+aGravaT6H[nT6H][3]+aGravaT6H[nT6H][4]+DtoC(aGravaT6H[nT6H][5])+aGravaT6H[nT6H][6]
						oModel:GetModel( 'MODEL_T6H' ):LVALID := .T.
						
						If nT6HAdd > 1
							oModel:GetModel( "MODEL_T6H" ):AddLine()
						EndIf
						
						oModel:LoadValue( "MODEL_T6H", "T6H_PERREF", aGravaT6H[nT6H][7] )
						
						/*------------------------------------------
						   T6I - Ident. do Estabelecimento
						--------------------------------------------*/
						nT6IAdd := 1	
						For nT6I := 1 to Len( aGravaT6I )		
							If aGravaT6H[nT6H][1]+aGravaT6H[nT6H][2]+aGravaT6H[nT6H][3]+aGravaT6H[nT6H][4]+DtoC(aGravaT6H[nT6H][5])+aGravaT6H[nT6H][6]+aGravaT6H[nT6H][7] == aGravaT6I[nT6I][1]+aGravaT6I[nT6I][2]+aGravaT6I[nT6I][3]+aGravaT6I[nT6I][4]+DtoC(aGravaT6I[nT6I][5])+aGravaT6I[nT6I][6]+aGravaT6I[nT6I][7]
								oModel:GetModel( 'MODEL_T6I' ):LVALID := .T.
								
								If nT6IAdd > 1
									oModel:GetModel( "MODEL_T6I" ):AddLine()
								EndIf
								
								oModel:LoadValue( "MODEL_T6I", "T6I_ESTABE", aGravaT6I[nT6I][8] )
				
								/*------------------------------------------
								   T6J - Informa��es da Remunera��o Trab.
								--------------------------------------------*/
								nT6JAdd := 1	
								For nT6J := 1 to Len( aGravaT6J )		
									If aGravaT6I[nT6I][1]+aGravaT6I[nT6I][2]+aGravaT6I[nT6I][3]+aGravaT6I[nT6I][4]+DtoC(aGravaT6I[nT6I][5])+aGravaT6I[nT6I][6]+aGravaT6I[nT6I][7] == aGravaT6J[nT6J][1]+aGravaT6J[nT6J][2]+aGravaT6J[nT6J][3]+aGravaT6J[nT6J][4]+DtoC(aGravaT6J[nT6J][5])+aGravaT6J[nT6J][6]+aGravaT6J[nT6J][7]
										oModel:GetModel( 'MODEL_T6J' ):LVALID := .T.
										
										If nT6JAdd > 1
											oModel:GetModel( "MODEL_T6J" ):AddLine()
										EndIf
										
										oModel:LoadValue( "MODEL_T6J", "T6J_IDTRAB", aGravaT6J[nT6J][8] )
										oModel:LoadValue( "MODEL_T6J", "T6J_CODCAT", aGravaT6J[nT6J][9] )
										
										/*------------------------------------------
										   	  T6K - Itens da Remunera��o Trab.
										--------------------------------------------*/
										nT6KAdd := 1	
										For nT6K := 1 to Len( aGravaT6K )		
											If aGravaT6J[nT6J][1]+aGravaT6J[nT6J][2]+aGravaT6J[nT6J][3]+aGravaT6J[nT6J][4]+DtoC(aGravaT6J[nT6J][5])+aGravaT6J[nT6J][6]+aGravaT6J[nT6J][7]+aGravaT6J[nT6J][8]+aGravaT6J[nT6J][9]+aGravaT6J[nT6J][10] == aGravaT6K[nT6K][1]+aGravaT6K[nT6K][2]+aGravaT6K[nT6K][3]+aGravaT6K[nT6K][4]+DtoC(aGravaT6K[nT6K][5])+aGravaT6K[nT6K][6]+aGravaT6K[nT6K][7]+aGravaT6K[nT6K][8]+aGravaT6K[nT6K][9]+aGravaT6K[nT6K][10]
												oModel:GetModel( 'MODEL_T6K' ):LVALID := .T.
												
												If nT6KAdd > 1
													oModel:GetModel( "MODEL_T6K" ):AddLine()
												EndIf
												
												oModel:LoadValue( "MODEL_T6K", "T6K_IDRUBR",	aGravaT6K[nT6K][11] )
												oModel:LoadValue( "MODEL_T6K", "T6K_QTDRUB",	aGravaT6K[nT6K][12] )
												oModel:LoadValue( "MODEL_T6K", "T6K_FATORR",	aGravaT6K[nT6K][13] )
												oModel:LoadValue( "MODEL_T6K", "T6K_VLRUNT",	aGravaT6K[nT6K][14] )
												oModel:LoadValue( "MODEL_T6K", "T6K_VLRRUB",	aGravaT6K[nT6K][15] )
												
												nT6KAdd++
												
											EndIf			
										Next //nT6K
												
										nT6JAdd++		
									EndIf			
								Next //nT6J
								
								nT6IAdd++
							EndIf			
						Next //nT6I
			
						nT6HAdd++
					EndIf			
				Next //nT6H
		
				nT61Add++
			EndIf					
		Next //nT61
		
	Next //nT14
		
																				
	//�������������������������������Ŀ
	//�Busco a versao que sera gravada�
	//���������������������������������
	cVersao := xFunGetVer()
	
	/*---------------------------------------------------------
	ATENCAO -> A alteracao destes campos deve sempre estar     
	abaixo do Loop do For, pois devem substituir as informacoes
	que foram armazenadas no Loop acima                        
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_C91", "C91_VERSAO", cVersao )
	oModel:LoadValue( "MODEL_C91", "C91_VERANT", cVerAnt )
	oModel:LoadValue( "MODEL_C91", "C91_PROTPN", cProtocolo )
	
	
	/*---------------------------------------------------------
	Tratamento para que caso o Evento Anterior fosse de exclus�o
	seta-se o novo evento como uma "nova inclus�o", caso contr�rio o
	evento passar a ser uma altera��o
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_C91", "C91_EVENTO", "E" )
	oModel:LoadValue( "MODEL_C91", "C91_ATIVO", "1" )
		
	
	FwFormCommit( oModel )
	TAFAltStat( 'C91',"6" )
	

End Transaction


Return( .T. )