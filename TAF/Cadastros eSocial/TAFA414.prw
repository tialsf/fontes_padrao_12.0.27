#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFA414.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA414
Cadastro MVC dos  Comercializa��o Prod. Rural PF - S-1260

@author Daniel Schmidt
@since 11/01/2016
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAFA414()

Private	oBrw    := FWmBrowse():New()

oBrw:SetDescription( STR0001 )	//"Comercializa��o da Produ��o Rural Pessoa F�sica"
oBrw:SetAlias( 'T1M')
oBrw:SetMenuDef( 'TAFA414' )

If FindFunction('TAFSetFilter')
	oBrw:SetFilterDefault(TAFBrwSetFilter("T1M","TAFA414","S-1260"))
Else
	oBrw:SetFilterDefault( "T1M_ATIVO == '1'" )
EndIf

TafLegend(2,"T1M",@oBrw)
oBrw:Activate()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Daniel Schmidt
@since 11/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao 
Local aRotina 

aFuncao := {}
aRotina := {}

If FindFunction('TafXmlRet')
	Aadd( aFuncao, { "" , "TafxmlRet('TAF414Xml','1260','T1M')" , "1" } )
Else 
	Aadd( aFuncao, { "" , "TAF414Xml" , "1" } )
EndIf

Aadd( aFuncao, { "" , "Taf414Vld" , "2" } )
Aadd( aFuncao, { "" , "xFunAltRec( 'T1M' )" , "10" } )

//Chamo a Browse do Hist�rico
If FindFunction( "xNewHisAlt" )
	Aadd( aFuncao, { "" , "xNewHisAlt( 'T1M', 'TAFA414' ,,,,,,'1260','TAF414Xml' )" , "3" } )
Else
	Aadd( aFuncao, { "" , "xFunHisAlt( 'T1M', 'TAFA414' ,,,, 'TAF414XML','1260 )" , "3" } )
EndIf

Aadd( aFuncao, { "" , "TAFXmlLote( 'T1M', 'S-1260' , 'evtComProd' , 'TAF414Xml',, oBrw )" , "5" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If lMenuDif
	ADD OPTION aRotina Title "Visualizar"	Action 'VIEWDEF.TAFA414' OPERATION 2 ACCESS 0

	If FindFunction( "xNewHisAlt" ) .AND. FindFunction( "xTafExtmp" ) .And. xTafExtmp()
		aRotina	:= xMnuExtmp( "TAFA414", "T1M" )
	EndIf
Else
	aRotina	:=	xFunMnuTAF( "TAFA414" , , aFuncao)
EndIf

Return (aRotina )

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC

@author Daniel Schmidt
@since 11/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
Local oStruT1M  
Local oStruT1N  
Local oStruT1O  
Local oStruT1P  
Local oStruT6B  
Local oModel	  

oStruT1M  :=  FWFormStruct( 1, 'T1M' )
oStruT1N  :=  FWFormStruct( 1, 'T1N' )
oStruT1O  :=  FWFormStruct( 1, 'T1O' )
oStruT1P  :=  FWFormStruct( 1, 'T1P' )
oStruT6B  :=  FWFormStruct( 1, 'T6B' )
oModel    :=  MPFormModel():New( 'TAFA414' ,,,{|oModel| SaveModel(oModel)})

//Remo��o do GetSX8Num quando se tratar da Exclus�o de um Evento Transmitido.
//Necess�rio para n�o incrementar ID que n�o ser� utilizado.
If Upper( ProcName( 2 ) ) == Upper( "GerarExclusao" )
	oStruT1M:SetProperty( "T1M_ID", MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD, "" ) )
EndIf

//T1M � Info. Comercializa��o Produ��o
oModel:AddFields('MODEL_T1M', /*cOwner*/, oStruT1M)
oModel:GetModel('MODEL_T1M'):SetPrimaryKey({'T1M_FILIAL', 'T1M_ID', 'T1M_VERSAO'})

//T1N � Tipo de Comercializa��o
oModel:AddGrid('MODEL_T1N', 'MODEL_T1M', oStruT1N)
oModel:GetModel('MODEL_T1N'):SetOptional(.T.)
oModel:GetModel('MODEL_T1N'):SetUniqueLine({'T1N_INDCOM'})
oModel:GetModel('MODEL_T1N'):SetMaxLine(5)


//T1O � Ident. Adquirentes da Produ��o
oModel:AddGrid('MODEL_T1O', 'MODEL_T1N', oStruT1O)
oModel:GetModel('MODEL_T1O'):SetOptional(.T.)
oModel:GetModel('MODEL_T1O'):SetUniqueLine({'T1O_TPINSA', 'T1O_NRINSA'})
oModel:GetModel('MODEL_T1O'):SetMaxLine(9999)

//T1P � Info. Proc. Judiciais Incid. Comer.
oModel:AddGrid('MODEL_T1P', 'MODEL_T1N', oStruT1P)
oModel:GetModel('MODEL_T1P'):SetOptional(.T.)
oModel:GetModel('MODEL_T1P'):SetUniqueLine({'T1P_IDPROC'})
oModel:GetModel('MODEL_T1P'):SetMaxLine(10)

//Modelo de Notas fiscais
oModel:AddGrid('MODEL_T6B', 'MODEL_T1O', oStruT6B)
oModel:GetModel('MODEL_T6B'):SetOptional(.T.)
oModel:GetModel('MODEL_T6B'):SetUniqueLine({'T6B_SERIE', 'T6B_NUMDOC'})
oModel:GetModel('MODEL_T6B'):SetMaxLine(9999)

oModel:SetRelation("MODEL_T1N",{ {"T1N_FILIAL","xFilial('T1N')"}, {"T1N_ID","T1M_ID"}, {"T1N_VERSAO","T1M_VERSAO"} , {"T1N_IDESTA","T1M_IDESTA"}}, T1N->(IndexKey(1)) )
oModel:SetRelation("MODEL_T1O",{ {"T1O_FILIAL","xFilial('T1O')"}, {"T1O_ID","T1M_ID"}, {"T1O_VERSAO","T1M_VERSAO"} , {"T1O_IDESTA","T1M_IDESTA"}, {"T1O_INDCOM","T1N_INDCOM"} }, T1O->(IndexKey(1)) )
oModel:SetRelation("MODEL_T1P",{ {"T1P_FILIAL","xFilial('T1P')"}, {"T1P_ID","T1M_ID"}, {"T1P_VERSAO","T1M_VERSAO"} , {"T1P_IDESTA","T1M_IDESTA"}, {"T1P_INDCOM","T1N_INDCOM"} }, T1P->(IndexKey(1)) )
oModel:SetRelation('MODEL_T6B',{ {'T6B_FILIAL',"xFilial('T6B')"}, {'T6B_ID','T1M_ID'}, {'T6B_VERSAO','T1M_VERSAO'} , {"T6B_IDESTA","T1M_IDESTA"}, {"T6B_INDCOM","T1N_INDCOM"},{'T6B_TPINSA','T1O_TPINSA'},{'T6B_NRINSA','T1O_NRINSA'}}, T6B->(IndexKey(1)))

Return oModel

//---------------------------------------------------------------------
/*/{Protheus.doc} ViewDef

Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Daniel Schmidt
@since 11/01/2016
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function ViewDef()

Local oModel   	
Local oStruT1Ma	 
Local oStruT1Mb	 
Local oStruT1N	 
Local oStruT1O	 
Local oStruT1P	 
Local oStruT6B	 
Local oView		 
Local cCmpFil  	 
Local nI       	 
Local aCmpGrp  	 
Local cGrpCom1	 
Local cGrpCom2	 
Local cGrpCom3 	 

oModel   	:= FWLoadModel( 'TAFA414' )
oStruT1Ma	:= Nil
oStruT1Mb	:= Nil
oStruT1N	:= FWFormStruct( 2, 'T1N' )
oStruT1O	:= FWFormStruct( 2, 'T1O' )
oStruT1P	:= FWFormStruct( 2, 'T1P' )
oStruT6B  	:= FWFormStruct( 2, 'T6B' )
oView		:= FWFormView():New()
cCmpFil  	:= ''
nI        	:= 0
aCmpGrp   	:= {}
cGrpCom1  	:= ""
cGrpCom2  	:= ""
cGrpCom3	:= ""

oView:SetModel( oModel )
oView:SetContinuousForm(.T.)

//Informa��es de Apura��o/Identifica��o do Estabelecimento que Comercializou a Produ��o
cGrpCom1  := 'T1M_ID|T1M_VERSAO|T1M_VERANT|T1M_PROTPN|T1M_EVENTO|T1M_ATIVO|T1M_INDAPU|T1M_PERAPU|'
cGrpCom2  := 'T1M_IDESTA|T1M_NRCAEP|'
cCmpFil   := cGrpCom1 + cGrpCom2
oStruT1Ma := FwFormStruct( 2, 'T1M', {|x| AllTrim( x ) + "|" $ cCmpFil } )

//"Protocolo de Transmiss�o"
cGrpCom3 := 'T1M_PROTUL|'
cCmpFil   := cGrpCom3
oStruT1Mb := FwFormStruct( 2, 'T1M', {|x| AllTrim( x ) + "|" $ cCmpFil } )

/*-----------------------------------------------------------------------------------
			      Grupo de campos da Comercializa��o de Produ��o
-------------------------------------------------------------------------------------*/

oStruT1Ma:AddGroup( "GRP_COMERCIALIZACAO_01", STR0005, "", 1 ) //Informa��es de Apura��o
oStruT1Ma:AddGroup( "GRP_COMERCIALIZACAO_02", STR0006, "", 1 ) //Identifica��o do Estabelecimento que Comercializou a Produ��o

aCmpGrp := StrToKArr(cGrpCom1,"|")
For nI := 1 to Len(aCmpGrp)
	oStruT1Ma:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_COMERCIALIZACAO_01")
Next nI

aCmpGrp := StrToKArr(cGrpCom2,"|")
For nI := 1 to Len(aCmpGrp)
	oStruT1Ma:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_COMERCIALIZACAO_02")
Next nI

If FindFunction("TafAjustRecibo")
	TafAjustRecibo(oStruT1Mb,"T1M")
EndIf

/*--------------------------------------------------------------------------------------------
									Esrutura da View
---------------------------------------------------------------------------------------------*/

oView:AddField( "VIEW_T1Ma", oStruT1Ma, "MODEL_T1M" )

oView:AddField( "VIEW_T1Mb", oStruT1Mb, "MODEL_T1M" )

If FindFunction("TafNmFolder")
	oView:EnableTitleView( 'VIEW_T1Mb', TafNmFolder("recibo",1) ) // "Recibo da �ltima Transmiss�o"
EndIf

oView:AddGrid( "VIEW_T1N", oStruT1N, "MODEL_T1N" )
oView:EnableTitleView("MODEL_T1N",STR0007) //"Total da Comercializa��o por Tipo de Comercializa��o"

oView:AddGrid( "VIEW_T1O", oStruT1O, "MODEL_T1O" )
oView:AddGrid( "VIEW_T1P", oStruT1P, "MODEL_T1P" )
oView:AddGrid( "VIEW_T6B", oStruT6B, 'MODEL_T6B' )


/*-----------------------------------------------------------------------------------
								Estrutura do Folder
-------------------------------------------------------------------------------------*/
oView:CreateHorizontalBox("PAINEL_PRINCIPAL",100)
oView:CreateFolder("FOLDER_PRINCIPAL","PAINEL_PRINCIPAL")

//////////////////////////////////////////////////////////////////////////////////

oView:AddSheet("FOLDER_PRINCIPAL","ABA01",STR0010) //"Info. Comercializa��o Produ��o"
oView:CreateHorizontalBox("T1Ma",20,,,"FOLDER_PRINCIPAL","ABA01") //Informa��es de Apura��o/Identifica��o do Estabelecimento que Comercializou a Produ��o
oView:CreateHorizontalBox("T1N" ,30,,,"FOLDER_PRINCIPAL","ABA01") //Total da Comercializa��o por Tipo de Comercializa��o

oView:CreateHorizontalBox("PAINEL_TPCOM",50,,,"FOLDER_PRINCIPAL","ABA01")
oView:CreateFolder( 'FOLDER_TPCOM', 'PAINEL_TPCOM' )
oView:AddSheet( 'FOLDER_TPCOM', 'ABA01', STR0008 ) //"Identifica��o dos Adquirentes da Produ��o"
oView:AddSheet( 'FOLDER_TPCOM', 'ABA02', STR0009 ) //"Informa��es de Processos Judiciais"
oView:CreateHorizontalBox ( 'T1O', 50,,, 'FOLDER_TPCOM'  , 'ABA01' )
oView:CreateHorizontalBox ( 'T1P', 100,,, 'FOLDER_TPCOM'  , 'ABA02' )

oView:CreateHorizontalBox("PAINEL_NF",50,,,"FOLDER_TPCOM","ABA01")
oView:CreateFolder( 'FOLDER_NF', 'PAINEL_NF' )
oView:AddSheet( 'FOLDER_NF', 'ABA01', STR0013 ) //"Notas Fiscais"
oView:CreateHorizontalBox ( 'T6B', 100,,, 'FOLDER_NF'  , 'ABA01' )

If FindFunction("TafNmFolder")
	oView:AddSheet("FOLDER_PRINCIPAL","ABA02",TafNmFolder("recibo") ) //"Numero do Recibo"
Else
	oView:AddSheet("FOLDER_PRINCIPAL","ABA02",STR0012) //"Protocolo de Transmiss�o"
EndIf

oView:CreateHorizontalBox("T1Mb",100,,,"FOLDER_PRINCIPAL","ABA02")

//////////////////////////////////////////////////////////////////////////////////

/*-----------------------------------------------------------------------------------
							Amarra��o para exibi��o das informa��es
-------------------------------------------------------------------------------------*/

oView:SetOwnerView( "VIEW_T1Ma", "T1Ma")
oView:SetOwnerView( "VIEW_T1Mb", "T1Mb")
oView:SetOwnerView( "VIEW_T1N",  "T1N" )
oView:SetOwnerView( "VIEW_T1O",  "T1O" )
oView:SetOwnerView( "VIEW_T1P",  "T1P" )
oView:SetOwnerView( "VIEW_T6B",  "T6B" )

//Processar Dados Autom�ticamente
oView:AddUserButton( STR0017, 'CLIPS', {|oView| ImportDataNF(oModel) } ) //"Buscar Docs Fiscais"

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif .OR. ( FindFunction( "xTafExtmp" ) .And. xTafExtmp() )
	xFunRmFStr(@oStruT1Ma,"T1M")
	oStruT1P:RemoveField('T1P_IDSUSP')
EndIf

Return (oView)

//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da confirmacao do modelo

@param  oModel -> Modelo de dados
@return .T.

@author Daniel Schmidt
@since 11/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel( oModel )

Local cVerAnt    	 
Local cProtocolo	 
Local cVersao    	 
Local cEvento	 	 
Local cChvRegAnt	 
Local cLogOpe		 
Local cLogOpeAnt	 

Local nOperation	 
Local nI			 
Local nT1N			 
Local nT1O			 
Local nT1P			 
Local nT6B			 

Local aGrava		 
Local aGravaT1M	 
Local aGravaT1N 	 
Local aGravaT1O	 
Local aGravaT1P	 
Local aGravaT6B	 

Local oModelT1M 	 
Local oModelT1N 	 
Local oModelT1O 	 
Local oModelT1P 	 
Local oModelT6B 	 

Local lRetorno	 

cVerAnt   	:= ""
cProtocolo	:= ""
cVersao   	:= ""
cEvento	:= ""
cChvRegAnt	:= ""
cLogOpe    	:= ""
cLogOpeAnt 	:= ""

nOperation	:= oModel:GetOperation()
nI			:= 0
nT1N		:= 0
nT1O		:= 0
nT1P		:= 0
nT6B		:= 0

aGrava		:= {}
aGravaT1M	:= {}
aGravaT1N 	:= {}
aGravaT1O	:= {}
aGravaT1P	:= {}
aGravaT6B	:= {}

oModelT1M 	:= Nil
oModelT1N 	:= Nil
oModelT1O 	:= Nil
oModelT1P 	:= Nil
oModelT6B 	:= Nil

lRetorno	:= .T.

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

Begin Transaction

	//Inclusao Manual do Evento
	If nOperation == MODEL_OPERATION_INSERT

		TafAjustID(�"T1M", oModel)
		
		oModel:LoadValue( "MODEL_T1M", "T1M_VERSAO", xFunGetVer() )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_T1M', 'T1M_LOGOPE' , '2', '' )
		Endif

		FwFormCommit( oModel )

	//Altera��o Manual do Evento
	ElseIf nOperation == MODEL_OPERATION_UPDATE

		//�����������������������������������������������������������������Ŀ
		//�Seek para posicionar no registro antes de realizar as validacoes,�
		//�visto que quando nao esta pocisionado nao eh possivel analisar  	 �
		//�os campos nao usados como _STATUS                                �
		//�������������������������������������������������������������������
		//Posicionando no registro para que nao ocorra erros duranteo processo de valida��o
		//T1M_FILIAL+T1M_ID+T1M_ATIVO
	    T1M->( DbSetOrder( 4 ) )
	    If lGoExtemp .OR. T1M->( MsSeek( xFilial( 'T1M' ) + T1M->T1M_ID + '1' ) )

			//Verifica se o evento ja foi trasmitido ao RET
		    If T1M->T1M_STATUS $ ( "4" )

				//Carrego a Estrutura dos Models a serem gravados
				oModelT1M := oModel:GetModel( "MODEL_T1M" )
				oModelT1N := oModel:GetModel( "MODEL_T1N" )
				oModelT1O := oModel:GetModel( "MODEL_T1O" )
				oModelT1P := oModel:GetModel( "MODEL_T1P" )
				oModelT6B := oModel:GetModel( "MODEL_T6B" )


				//Guardo as informa��es do registro corrente para rastro do registro
				cVerAnt   	:= oModelT1M:GetValue( "T1M_VERSAO" )
				cProtocolo	:= oModelT1M:GetValue( "T1M_PROTUL" )
				cEvento		:= oModelT1M:GetValue( "T1M_EVENTO" )

				If TafColumnPos( "T1M_LOGOPE" )
					cLogOpeAnt := oModelT1M:GetValue( "T1M_LOGOPE" )
				endif

				//Armazeno as informa��es correntes do cadastro( Depois da altera��o do Usu�rio )

				//***********
				//Informa��o da Comercializa��o de Produ��o
				//***********
				For nI := 1 to Len( oModelT1M:aDataModel[ 1 ] )
					Aadd( aGrava, { oModelT1M:aDataModel[ 1, nI, 1 ], oModelT1M:aDataModel[ 1, nI, 2 ] } )
				Next nI
				//------------------

				//***********
				//Tipo de Comercializa��o
				//***********
				For nT1N := 1 to oModel:GetModel( "MODEL_T1N" ):Length()
					oModel:GetModel( "MODEL_T1N" ):GoLine(nT1N)
					If !oModel:GetModel( 'MODEL_T1N' ):IsEmpty()
						If !oModel:GetModel( "MODEL_T1N" ):IsDeleted()
							aAdd(aGravaT1N,{oModelT1N:GetValue("T1N_INDCOM"),;
							                oModelT1N:GetValue("T1N_VLRTOT")})

							//***********
							//Identifica��o dos Adquirentes da Produ��o
							//***********
							For nT1O := 1 to oModel:GetModel( "MODEL_T1O" ):Length()
								oModel:GetModel( "MODEL_T1O" ):GoLine(nT1O)
								If !oModel:GetModel( 'MODEL_T1O' ):IsEmpty()
									If !oModel:GetModel( "MODEL_T1O" ):IsDeleted()
										aAdd(aGravaT1O,{oModelT1N:GetValue("T1N_INDCOM"),;
														oModelT1O:GetValue("T1O_TPINSA"),;
							                		    oModelT1O:GetValue("T1O_NRINSA"),;
							                			oModelT1O:GetValue("T1O_VLRCOM")})


						                For nT6B := 1 To oModel:GetModel( 'MODEL_T6B' ):Length()
											oModel:GetModel( 'MODEL_T6B' ):GoLine(nT6B)

											If !oModel:GetModel( 'MODEL_T6B' ):IsDeleted()
												aAdd (aGravaT6B ,{ oModelT1N:GetValue("T1N_INDCOM"),;
																   oModelT1O:GetValue("T1O_TPINSA"),;
										                		   oModelT1O:GetValue("T1O_NRINSA"),;
																   oModelT6B:GetValue("T6B_SERIE"),;
																   oModelT6B:GetValue("T6B_NUMDOC"),;
																   oModelT6B:GetValue("T6B_DTEMIS"),;
																   oModelT6B:GetValue("T6B_VLBRUT"),;
																   oModelT6B:GetValue("T6B_VLCONT"),;
																   oModelT6B:GetValue("T6B_VLGILR"),;
																   oModelT6B:GetValue("T6B_VLSENA")} )
											EndIf
										Next nT6B
									EndIf
								EndIf
							Next nT1O
							//-----------
							//***********
							//Informa��es de Processos Judiciais
							//***********
							For nT1P := 1 to oModel:GetModel( "MODEL_T1P" ):Length()
								oModel:GetModel( "MODEL_T1P" ):GoLine(nT1P)
								If !oModel:GetModel( 'MODEL_T1P' ):IsEmpty()
									If !oModel:GetModel( 'MODEL_T1P' ):IsDeleted()
										aAdd(aGravaT1P,{oModelT1N:GetValue("T1N_INDCOM"),;
														oModelT1P:GetValue("T1P_IDPROC"),;
							                			oModelT1P:GetValue("T1P_VLRPRV"),;
							             	  			oModelT1P:GetValue("T1P_VLRRAT"),;
							                			oModelT1P:GetValue("T1P_VLRSEN")})
									EndIf
								EndIf
							Next nT1P
							//-----------

						EndIf
					EndIf
				Next nT1N
				//-----------

				/*----------------------------------------------------------
				Seto o campo como Inativo e gravo a versao do novo registro
				no registro anterior

				ATENCAO -> A alteracao destes campos deve sempre estar
				abaixo do Loop do For, pois devem substituir as informacoes
				que foram armazenadas no Loop acima
				-----------------------------------------------------------*/
				FAltRegAnt( "T1M", "2" )

				/*----------------------------------------------------------
				Apos deixar o registro corrente como inativo eu seto a
				opera��o de inclus�o para o novo registro
				-----------------------------------------------------------*/
				oModel:DeActivate()
				oModel:SetOperation( 3 )
				oModel:Activate()

				/*----------------------------------------------------------
				Neste momento eu realizo a grava��o de um novo registro id�ntico
				ao original, apenas com as altera��es nos campos modificados
				pelo usu�rio no cadastro
				-----------------------------------------------------------*/
				//***********
				//Informa��o da Comercializa��o de Produ��o
				//***********
				For nI := 1 to Len( aGrava )
					oModel:LoadValue( "MODEL_T1M", aGrava[ nI, 1 ], aGrava[ nI, 2 ] )
				Next nI

				//Necess�rio Abaixo do For Nao Retirar
				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_T1M', 'T1M_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				//***********
				//Tipo de Comercializa��o
				//***********
				For nT1N := 1 to Len( aGravaT1N )
					If nT1N > 1
						oModel:GetModel( "MODEL_T1N" ):AddLine()
					EndIf
					oModel:LoadValue( "MODEL_T1N", "T1N_INDCOM" , aGravaT1N[nT1N][1] )
					oModel:LoadValue( "MODEL_T1N", "T1N_VLRTOT" , aGravaT1N[nT1N][2] )
					//***********
					//Identifica��o dos Adquirentes da Produ��o
					//***********
					For nT1O := 1 to Len( aGravaT1O )
						If  aGravaT1N[nT1N][1] == aGravaT1O[nT1O][1]
							If nT1O > 1
								oModel:GetModel( "MODEL_T1O" ):AddLine()
							EndIf
							oModel:LoadValue( "MODEL_T1O", "T1O_TPINSA" , aGravaT1O[nT1O][2] )
							oModel:LoadValue( "MODEL_T1O", "T1O_NRINSA" , aGravaT1O[nT1O][3] )
							oModel:LoadValue( "MODEL_T1O", "T1O_VLRCOM" , aGravaT1O[nT1O][4] )

							For nT6B := 1 To Len( aGravaT6B )
								If aGravaT1N[nT1N][1] == aGravaT6B[nT6B][1] .AND. aGravaT1O[nT1O][2] == aGravaT6B[nT6B][2] .AND. aGravaT1O[nT1O][3] == aGravaT6B[nT6B][3]

									If nT6B > 1
										oModel:GetModel( 'MODEL_T6B' ):AddLine()
									EndIf
									oModel:LoadValue( "MODEL_T6B", "T6B_SERIE",  aGravaT6B[nT6B][4] )
									oModel:LoadValue( "MODEL_T6B", "T6B_NUMDOC", aGravaT6B[nT6B][5] )
									oModel:LoadValue( "MODEL_T6B", "T6B_DTEMIS", aGravaT6B[nT6B][6] )
									oModel:LoadValue( "MODEL_T6B", "T6B_VLBRUT", aGravaT6B[nT6B][7] )
									oModel:LoadValue( "MODEL_T6B", "T6B_VLCONT", aGravaT6B[nT6B][8] )
									oModel:LoadValue( "MODEL_T6B", "T6B_VLGILR", aGravaT6B[nT6B][9] )
									oModel:LoadValue( "MODEL_T6B", "T6B_VLSENA", aGravaT6B[nT6B][10] )
								EndIf

				            Next nT6B
				        EndIf
			   		Next nT1O

					//***********
					//Informa��es de Processos Judiciais
					//***********
					For nT1P := 1 to Len( aGravaT1P )
						If  aGravaT1N[nT1N][1] == aGravaT1P[nT1P][1]
							oModel:GetModel( 'MODEL_T1P' ):LVALID	:= .T.

							If nT1P > 1
								oModel:GetModel( "MODEL_T1P" ):AddLine()
							EndIf
							oModel:LoadValue( "MODEL_T1P", "T1P_IDPROC",	aGravaT1P[nT1P][2] )
							oModel:LoadValue( "MODEL_T1P", "T1P_VLRPRV",	aGravaT1P[nT1P][3] )
							oModel:LoadValue( "MODEL_T1P", "T1P_VLRRAT",	aGravaT1P[nT1P][4] )
							oModel:LoadValue( "MODEL_T1P", "T1P_VLRSEN",	aGravaT1P[nT1P][5] )
						EndIf
			  		Next nT1P

			   Next nT1N

				//Busco a nova versao do registro
				cVersao := xFunGetVer()

				/*---------------------------------------------------------
				ATENCAO -> A alteracao destes campos deve sempre estar
				abaixo do Loop do For, pois devem substituir as informacoes
				que foram armazenadas no Loop acima
				-----------------------------------------------------------*/
				oModel:LoadValue( "MODEL_T1M", "T1M_VERSAO", cVersao )
				oModel:LoadValue( "MODEL_T1M", "T1M_VERANT", cVerAnt )
				oModel:LoadValue( "MODEL_T1M", "T1M_PROTPN", cProtocolo )
				oModel:LoadValue( "MODEL_T1M", "T1M_PROTUL", "" )
				oModel:LoadValue( "MODEL_T1M", "T1M_EVENTO", "A" )

				// Tratamento para limpar o ID unico do xml
				cAliasPai := "T1M"
				If TAFColumnPos( cAliasPai+"_XMLID" )
					oModel:LoadValue( 'MODEL_'+cAliasPai, cAliasPai+'_XMLID', "" )
				EndIf

				//Gravo altera��o para o Extempor�neo
				If lGoExtemp
					TafGrvExt( oModel, 'MODEL_T1M', 'T1M' )
				Endif

				FwFormCommit( oModel )
				TAFAltStat( 'T1M', " " )
			ElseIf	T1M->T1M_STATUS == "2"
				TAFMsgVldOp(oModel,"2")//"Registro n�o pode ser alterado. Aguardando processo da transmiss�o."
				lRetorno:= .F.
			ElseIf T1M->T1M_STATUS == "6"
				TAFMsgVldOp(oModel,"6")//"Registro n�o pode ser alterado. Aguardando proc. Transm. evento de Exclus�o S-3000"
				lRetorno:= .F.
			Elseif T1M->T1M_STATUS == "7"
				TAFMsgVldOp(oModel,"7") //"Registro n�o pode ser alterado, pois o evento j� se encontra na base do RET"
				lRetorno:= .F.
			Else
				//altera��o sem transmiss�o
				If TafColumnPos( "T1M_LOGOPE" )
					cLogOpeAnt := T1M->T1M_LOGOPE
				endif

				//Gravo altera��o para o Extempor�neo
				If lGoExtemp
					TafGrvExt( oModel, 'MODEL_T1M', 'T1M' )
				Endif

				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_T1M', 'T1M_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				FwFormCommit( oModel )
				TAFAltStat( 'T1M', " " )
			EndIf
		EndIf

	//Exclus�o Manual do Evento
	ElseIf nOperation == MODEL_OPERATION_DELETE

		cChvRegAnt := T1M->(T1M_ID + T1M_VERANT)

		If !Empty( cChvRegAnt )
			TAFAltStat( 'T1M', " " )
			FwFormCommit( oModel )
			If nOperation == MODEL_OPERATION_DELETE
				If T1M->T1M_EVENTO == "A" .Or. T1M->T1M_EVENTO == "E"
					TAFRastro( 'T1M', 1, cChvRegAnt, .T. , , IIF(Type("oBrw") == "U", Nil, oBrw) )
				EndIf
			EndIf
		Else
			oModel:DeActivate()
			oModel:SetOperation( 5 )
			oModel:Activate()
			FwFormCommit( oModel )
		EndIf

	EndIf

End Transaction

If !lRetorno
	// Define a mensagem de erro que ser� exibida ap�s o Return do SaveModel
	TAFMsgDel(oModel,.T.)
EndIf

Return (lRetorno)

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF414Grv
@type			function
@description	Fun��o de grava��o para atender o registro S-1260.
@author			Daniel Schmidt
@since			11/01/2016
@version		1.0
@param			cLayout		-	Nome do Layout que est� sendo enviado
@param			nOpc		-	Op��o a ser realizada ( 3 = Inclus�o, 4 = Altera��o, 5 = Exclus�o )
@param			cFilEv		-	Filial do ERP para onde as informa��es dever�o ser importadas
@param			oXML		-	Objeto com as informa��es a serem manutenidas ( Outras Integra��es )
@param			cOwner
@param			cFilTran
@param			cPredeces
@param			nTafRecno
@param			cComplem
@param			cGrpTran
@param			cEmpOriGrp
@param			cFilOriGrp
@param			cXmlID		-	Atributo Id, �nico para o XML do eSocial. Utilizado para importa��o de dados de clientes migrando para o TAF
@return			lRet		-	Vari�vel que indica se a importa��o foi realizada, ou seja, se as informa��es foram gravadas no banco de dados
@param			aIncons		-	Array com as inconsist�ncias encontradas durante a importa��o
/*/
//-------------------------------------------------------------------
Function TAF414Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID )

Local cCmpsNoUpd	:=	"|T1M_FILIAL|T1M_ID|T1M_VERSAO|T1M_VERANT|T1M_PROTUL|T1M_PROTPN|T1M_EVENTO|T1M_STATUS|T1M_ATIVO|"
Local cCabec		:=	"/eSocial/evtComProd/infoComProd/ideEstabel"
Local cT1NPath		:=	""
Local cT1OPath		:=	""
Local cT1PPath		:=	""
Local cT6BPath		:=	""
Local cPeriodo		:=	""
Local cChave		:=	""
Local cInconMsg		:=	""
Local cCodEvent		:=	Posicione( "C8E", 2, xFilial( "C8E" ) + "S-" + cLayout, "C8E->C8E_ID" )
Local nI			:=	0
Local nJ			:=	0
Local nT1N			:=	0
Local nT1O			:=	0
Local nT1P			:=	0
Local nT6B			:=	0
Local nSeqErrGrv	:=	0
Local lRet			:=	.F.
Local aIncons		:=	{}
Local aRulesCad		:=	{}
Local aChave		:=	{}
Local aCmpsCM7		:=	{}
Local aCmpsC9W		:=	{}
Local oModel		:=	Nil

Private lVldModel	:=	.T. //Caso a chamada seja via integra��o, seto a vari�vel de controle de valida��o como .T.
Private oDados		:=	oXML

Default cLayout		:=	"1260"
Default nOpc		:=	1
Default cFilEv		:=	""
Default oXML		:=	Nil
Default cOwner		:=	""
Default cFilTran	:=	""
Default cPredeces	:=	""
Default nTafRecno	:=	0
Default cComplem	:=	""
Default cGrpTran	:=	""
Default cEmpOriGrp	:=	""
Default cFilOriGrp	:=	""
Default cXmlID		:=	""

//Chave do Registro
cPeriodo  := FTafGetVal( "/eSocial/evtComProd/ideEvento/perApur", "C", .F., @aIncons, .F. )

Aadd( aChave, {"C", "T1M_INDAPU", FTafGetVal( "/eSocial/evtComProd/ideEvento/indApuracao", "C", .F., @aIncons, .F. )  , .T.} )
cChave += Padr( aChave[ 1, 3 ], Tamsx3( aChave[ 1, 2 ])[1])

If At("-", cPeriodo) > 0
	Aadd( aChave, {"C", "T1M_PERAPU", StrTran(cPeriodo, "-", "" ),.T.} )
	cChave += Padr( aChave[ 2, 3 ], Tamsx3( aChave[ 2, 2 ])[1])
Else
	Aadd( aChave, {"C", "T1M_PERAPU", cPeriodo  , .T.} )
	cChave += Padr( aChave[ 2, 3 ], Tamsx3( aChave[ 2, 2 ])[1])
EndIf

//Aadd( aChave, { "C", "T1M_INDAPU", "/eSocial/evtComProd/ideEvento/indApuracao", .F. } )
//Aadd( aChave, { "C", "T1M_PERAPU", "/eSocial/evtComProd/ideEvento/perApur", .F. } )
aAdd( aChave, { "C", "T1M_IDESTA", FGetIdInt( "nrInscEstabRural","",cCabec + "/nrInscEstabRural") , .T. } )
cChave += Padr( aChave[ 3, 3 ], Tamsx3( aChave[ 3, 2 ])[1])

//Verifica se o evento ja existe na base
("T1M")->( DbSetOrder( 2 ) )
If ("T1M")->( MsSeek( xFilial("T1M") + cChave + '1' ) )
	nOpc := 4
EndIf

Begin Transaction

	//Funcao para validar se a operacao desejada pode ser realizada
	If FTafVldOpe( "T1M", 2, @nOpc, cFilEv, @aIncons, aChave, @oModel, "TAFA414", cCmpsNoUpd , , , , )

		If TafColumnPos( "T1M_LOGOPE" )
			cLogOpeAnt := T1M->T1M_LOGOPE
		endif

		//Carrego array com os campos De/Para de gravacao das informacoes ( Cadastrais )
		aRulesCad := TAF414Rul( cCabec, @cInconMsg, @nSeqErrGrv, cCodEvent, cOwner )

		//Quando se tratar de uma Exclusao direta apenas preciso realizar
		//o Commit(), nao eh necessaria nenhuma manutencao nas informacoes
		If nOpc <> 5

			oModel:LoadValue( "MODEL_T1M", "T1M_FILIAL", T1M->T1M_FILIAL )

			If TAFColumnPos( "T1M_XMLID" )
				oModel:LoadValue( "MODEL_T1M", "T1M_XMLID", cXmlID )
			EndIf

			//Rodo o aRulesCad para gravar as informacoes
			For nI := 1 to Len( aRulesCad )
			 	oModel:LoadValue( "MODEL_T1M", aRulesCad[ nI, 01 ], FTafGetVal( aRulesCad[ nI, 02 ], aRulesCad[nI, 03], aRulesCad[nI, 04], @aIncons, .F. ) )
			Next nI

			If Findfunction("TAFAltMan")
				if nOpc == 3
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_T1M', 'T1M_LOGOPE' , '1', '' )
				elseif nOpc == 4
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_T1M', 'T1M_LOGOPE' , '', cLogOpeAnt )
				EndIf
			EndIf

			//***********
			//eSocial/evtComProd/infoComProd/ideEstabel/tpComerc
			//***********
			nT1N := 1
			cT1NPath := cCabec + "/tpComerc[" + CVALTOCHAR(nT1N) + "]"

			If nOpc == 4
				For nJ := 1 to oModel:GetModel( "MODEL_T1N" ):Length()
					oModel:GetModel( "MODEL_T1N" ):GoLine(nJ)
					oModel:GetModel( "MODEL_T1N" ):DeleteLine()
				Next nJ
			EndIf

			//Rodo o XML parseado para gravar as novas informacoes no GRID ( Cadastro de Dependentes )
			nT1N := 1
			While oDados:XPathHasNode(cT1NPath)

				If nOpc == 4 .or. nT1N > 1
					oModel:GetModel( "MODEL_T1N" ):lValid:= .T.
					oModel:GetModel( "MODEL_T1N" ):AddLine()
				EndIf

				if oDados:XPathHasNode(cT1NPath +"/indComerc")
					oModel:LoadValue( "MODEL_T1N", "T1N_INDCOM", FGetIdInt( "indComerc", "", cT1NPath +"/indComerc",,,,@cInconMsg, @nSeqErrGrv))
 				EndIf

 				if oDados:XPathHasNode(cT1NPath + "/vrTotCom")
 					oModel:LoadValue( "MODEL_T1N", "T1N_VLRTOT", FTafGetVal( cT1NPath + "/vrTotCom",  "N", .F., @aIncons, .F. ) )
				EndIf

				//----------
				//***********
				//eSocial/evtComProd/infoComProd/ideEstabel/tpComerc/ideAdquir
				//***********
				nT1O 	 := 1
				cT1OPath := cT1NPath + "/ideAdquir[" + CVALTOCHAR(nT1O) + "]"

				If nOpc == 4
					For nJ := 1 to oModel:GetModel( "MODEL_T1O" ):Length()
						oModel:GetModel( "MODEL_T1O" ):GoLine(nJ)
						oModel:GetModel( "MODEL_T1O" ):DeleteLine()
					Next nJ
				EndIf

				nT1O := 1
				While oDados:XPathHasNode(cT1OPath)

					If nOpc == 4 .or. nT1O > 1
						oModel:GetModel( "MODEL_T1O" ):lValid:= .T.
						oModel:GetModel( "MODEL_T1O" ):AddLine()
					EndIf

					if oDados:XPathHasNode(cT1OPath + "/tpInsc")
						oModel:LoadValue( "MODEL_T1O", "T1O_TPINSA", FTafGetVal( cT1OPath + "/tpInsc",		"C", .F., @aIncons, .F. ) )
	 				EndIf
	 				if oDados:XPathHasNode(cT1OPath + "/nrInsc")
	 					oModel:LoadValue( "MODEL_T1O", "T1O_NRINSA", FTafGetVal( cT1OPath + "/nrInsc",  	"C", .F., @aIncons, .F. ) )
	 				EndIf

	 				if oDados:XPathHasNode(cT1OPath + "/vrComerc")
	 					oModel:LoadValue( "MODEL_T1O", "T1O_VLRCOM", FTafGetVal( cT1OPath + "/vrComerc",	"N", .F., @aIncons, .F. ) )
					EndIf


					//----------
					//***********
					//eSocial/evtComProd/infoComProd/ideEstabel/tpComerc/ideAdquir/nfs
					//***********
					nT6B := 1
					cT6BPath := cT1OPath + "/nfs[" + CVALTOCHAR(nT6B) + "]"

					//Deleta as linhas existentes se for alteracao
					If nOpc == 4
						For nJ := 1 to oModel:GetModel( 'MODEL_T6B' ):Length()
							oModel:GetModel( 'MODEL_T6B' ):GoLine(nJ)
							oModel:GetModel( 'MODEL_T6B' ):DeleteLine()
						Next nJ
					EndIf

					While oDados:XPathHasNode( cT6BPath)
						If nOpc == 4 .Or. nT6B > 1
							oModel:GetModel( "MODEL_T6B" ):LVALID := .T.
							oModel:GetModel( "MODEL_T6B" ):AddLine()
						EndIf

						// Grava dados no model
						If oDados:XPathHasNode(cT6BPath + "/serie")
							oModel:LoadValue( "MODEL_T6B", "T6B_SERIE ", FTafGetVal( cT6BPath  + "/serie"           	, "C", .F., @aIncons, .T. ) )
						EndIf

						If oDados:XPathHasNode(cT6BPath + "/nrDocto")
							oModel:LoadValue( "MODEL_T6B", "T6B_NUMDOC", FTafGetVal( cT6BPath  + "/nrDocto"        		, "C", .F., @aIncons, .T. ) )
						EndIf

						If oDados:XPathHasNode(cT6BPath + "/dtEmisNF")
							oModel:LoadValue( "MODEL_T6B", "T6B_DTEMIS", FTafGetVal( cT6BPath  + "/dtEmisNF"     		, "D", .F., @aIncons, .T. ) )
						EndIf

						If oDados:XPathHasNode(cT6BPath + "/vlrBruto")
							oModel:LoadValue( "MODEL_T6B", "T6B_VLBRUT", FTafGetVal( cT6BPath  + "/vlrBruto"     		, "N", .F., @aIncons, .T. ) )
						EndIf

						If oDados:XPathHasNode(cT6BPath + "/vrCPDescPR")
							oModel:LoadValue( "MODEL_T6B", "T6B_VLCONT", FTafGetVal( cT6BPath  + "/vrCPDescPR"			, "N", .F., @aIncons, .T. ) )
						EndIf

						If oDados:XPathHasNode(cT6BPath + "/vrRatDescPR")
							oModel:LoadValue( "MODEL_T6B", "T6B_VLGILR", FTafGetVal( cT6BPath  + "/vrRatDescPR"			, "N", .F., @aIncons, .T. ) )
						EndIf

						If oDados:XPathHasNode(cT6BPath + "/vrSenarDesc")
							oModel:LoadValue( "MODEL_T6B", "T6B_VLSENA", FTafGetVal( cT6BPath  + "/vrSenarDesc"  		, "N", .F., @aIncons, .T. ) )
						EndIf

						nT6B++
						cT6BPath := cT1OPath + "/nfs[" + CVALTOCHAR(nT6B) + "]"
					EndDo

					nT1O++
					cT1OPath := cT1NPath + "/ideAdquir[" + CVALTOCHAR(nT1O) + "]"
				EndDo

				//----------
				//***********
				//eSocial/evtComProd/infoComProd/ideEstabel/tpComerc/infoProcJud
				//***********
				nT1P := 1
				cT1PPath := cT1NPath + "/infoProcJud[" + CVALTOCHAR(nT1P) + "]"

				If nOpc == 4
					For nJ := 1 to oModel:GetModel( "MODEL_T1P" ):Length()
						oModel:GetModel( "MODEL_T1P" ):GoLine(nJ)
						oModel:GetModel( "MODEL_T1P" ):DeleteLine()
					Next nJ
				EndIf

				nT1P := 1
				While oDados:XPathHasNode(cT1PPath)

					If nOpc == 4 .or. nT1P > 1
						oModel:GetModel( "MODEL_T1P" ):lValid:= .T.
						oModel:GetModel( "MODEL_T1P" ):AddLine()
					EndIf

					If oDados:XPathHasNode(cT1PPath + "/tpProc") .or. oDados:XPathHasNode(cT1PPath +"/nrProc")
						cIdProc := FGetIdInt("nrProc" , "tpProc", cT1PPath + "/tpProc", cT1PPath +"/nrProc",,,@cInconMsg, @nSeqErrGrv)
						oModel:LoadValue( "MODEL_T1P", "T1P_IDPROC",	cIdProc )
	 				EndIf

	 				if !Empty(cIdProc)
						If oDados:XPathHasNode(cT1PPath + "/codSusp" )
  							oModel:LoadValue("MODEL_T1P", "T1P_IDSUSP", 	FGetIdInt( "codSusp","", FTafGetVal( cT1PPath + "/codSusp", "C", .F., @aIncons, .F. ),cIdProc,.F.,,@cInconMsg, @nSeqErrGrv) )
 						EndIf
 					Endif

	 				If oDados:XPathHasNode(cT1PPath + "/vrCPSusp")
	 					oModel:LoadValue( "MODEL_T1P", "T1P_VLRPRV", 	FTafGetVal( cT1PPath + "/vrCPSusp",	  "N", .F., @aIncons, .F.	) )
	 				EndIf

	 				if oDados:XPathHasNode(cT1PPath + "/vrRatSusp")
	 					oModel:LoadValue( "MODEL_T1P", "T1P_VLRRAT",	FTafGetVal( cT1PPath + "/vrRatSusp", 	  "N", .F., @aIncons, .F.	) )
	 				EndIf

	 				if oDados:XPathHasNode(cT1PPath + "/vrSenarSusp")
	 					oModel:LoadValue( "MODEL_T1P", "T1P_VLRSEN", 	FTafGetVal( cT1PPath + "/vrSenarSusp",  "N", .F., @aIncons, .F.	) )
	 				EndIf

	 				nT1P++
					cT1PPath := cT1NPath + "/infoProcJud[" + CVALTOCHAR(nT1P)  +"]"
				EndDo

				nT1N++
				cT1NPath := cCabec + "/tpComerc[" + CVALTOCHAR(nT1N) + "]"
			EndDo

		EndIf

		//���������������������������Ŀ
		//�Efetiva a operacao desejada�
		//�����������������������������
		If Empty(cInconMsg) .And. Empty(aIncons)
			If TafFormCommit( oModel )
				Aadd(aIncons, "ERRO19")
			Else
				lRet := .T.
			EndIf
		Else
			Aadd(aIncons, cInconMsg)
			DisarmTransaction()
		EndIf

		oModel:DeActivate()
		If FindFunction('TafClearModel')
			TafClearModel(oModel)
		EndIf

	EndIf
End Transaction

//Zerando os arrays e os Objetos utilizados no processamento
aSize( aRulesCad, 0 )
aRulesCad := Nil

aSize( aChave, 0 )
aChave	:= Nil

Return { lRet, aIncons }

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF414Rul
Regras para gravacao das informacoes do registro S-1260

@Param
nOper      - Operacao a ser realizada ( 3 = Inclusao / 4 = Alteracao / 5 = Exclusao )

@Return
aRull  - Regras para a gravacao das informacoes

@author Daniel Schmidt
@since 11/01/2016
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function TAF414Rul( cCabec, cInconMsg, nSeqErrGrv, cCodEvent, cOwner )

Local cCabec    	 
Local cPeriodo	 
Local aRull		 

Default cCabec		:= ""
Default cInconMsg		:= ""
Default nSeqErrGrv	:= 0
Default cCodEvent		:= ""
Default cOwner		:= ""

cCabec  	:= "/eSocial/evtComProd/infoComProd/ideEstabel"
cPeriodo 	:= ""
aRull		:= {}

//**********************************
//eSocial/evtComProd/ideEvento/
//**********************************
If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtComProd/ideEvento/indApuracao" ) )
	Aadd( aRull, { "T1M_INDAPU", "/eSocial/evtComProd/ideEvento/indApuracao", 		"C",  .F. } )	//indApuracao
EndIf

If TafXNode( oDados, cCodEvent, cOwner,( "/eSocial/evtComProd/ideEvento/perApur" ) )
	cPeriodo 	:= FTafGetVal("/eSocial/evtComProd/ideEvento/perApur", "C", .F.,, .F. )

	If At("-", cPeriodo) > 0
		Aadd( aRull, {"T1M_PERAPU", StrTran(cPeriodo, "-", "" ), "C", .T.} )
	Else
		Aadd( aRull, {"T1M_PERAPU", cPeriodo, "C", .T.} )
	EndIf
EndIf

//----------------------------------

//**********************************
//eSocial/evtComProd/infoComProd/ideEstabel
//**********************************
If TafXNode( oDados, cCodEvent, cOwner,( cCabec + "/nrInscEstabRural" ) )
	Aadd( aRull,{"T1M_IDESTA", FGetIdInt( "nrInscEstabRural","",cCabec + "/nrInscEstabRural",,,,@cInconMsg, @nSeqErrGrv),"C", .T.} ) //nrInscEstabRural
EndIf

Return ( aRull )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF414Xml
Retorna o Xml do Registro Posicionado

@author Daniel Schmidt
@since 11/01/2016
@version 1.0

@Param:
lJob - Informa se foi chamado por Job
lRemEmp - Exclusivo do Evento S-1000
cSeqXml - Numero sequencial para composi��o da chave ID do XML

@return
cXml - Estrutura do Xml do Layout S-1260
/*/
//-------------------------------------------------------------------
Function TAF414Xml(cAlias,nRecno,nOpc,lJob,lRemEmp,cSeqXml)

Local cXml    	 
Local cLayout 	 
Local cReg    	 
Local aMensal		 
Local cXmlNfs		 
Local cTpProc  	 
Local lXmlVLd		:= FindFunction('TafXmlVLD') .and. Tafxmlvld('TAF414XML')

Default lJob 		:= .F.
Default cAlias 	:= "T1M"
Default nRecno	:= 1
Default nOpc		:= 1
Default cSeqXml := ""

cXmlNfs	:= ""
cXml    	:= ""
cLayout 	:= "1260"
cReg    	:= "ComProd"
aMensal	:= {}
cTpProc  	:= ""

DBSelectArea("T1N")
T1N->(DBSetOrder(1))

DBSelectArea("T1O")
T1O->(DBSetOrder(1))

DBSelectArea("T6B")
T6B->(DBSetOrder(1))

DBSelectArea("T1P")
T1P->(DBSetOrder(1))

If lXmlVLd
	If T1M->T1M_INDAPU == '1'
		aAdd(aMensal, T1M->T1M_INDAPU)
		If Len(Alltrim(T1M->T1M_PERAPU)) <= 4
			AADD(aMensal,T1M->T1M_PERAPU)
		Else
			AADD(aMensal,substr(T1M->T1M_PERAPU, 1, 4) + '-' + substr(T1M->T1M_PERAPU, 5, 2) )
		EndIf
	EndIf

	cXml +=		"<infoComProd>"
	cXml +=			"<ideEstabel>"
	cXml +=				xTafTag("nrInscEstabRural",Posicione("C92",5,xFilial("C92") + T1M->T1M_IDESTA + '1',"C92_NRINSC"))

	If T1N->( MsSeek( xFilial( "T1N" ) + ('T1M')->( &( "T1M_ID") + &( "T1M_VERSAO" ) + &( "T1M_IDESTA" ) ) ) )
		While T1N->(!Eof()) .And. T1N->( T1N_FILIAL + T1N_ID + T1N_VERSAO + T1N_IDESTA ) == xFilial( "T1N" ) + ('T1M')->( &( "T1M_ID") + &( "T1M_VERSAO" ) + &( "T1M_IDESTA" ) )
			cXml +=			"<tpComerc>"
			cXml +=				xTafTag("indComerc"	,Posicione("T1T",1,xFilial("T1T") + T1N->T1N_INDCOM, "T1T_CODIGO") )
			cXml +=				xTafTag("vrTotCom"	,T1N->T1N_VLRTOT, PesqPict("T1N","T1N_VLRTOT") )


			If T1O->( MsSeek( xFilial( "T1O" ) + ('T1N')->( &( "T1N_ID") + &( "T1N_VERSAO" ) + &( "T1N_IDESTA" ) + &( "T1N_INDCOM" ) ) ) )
				While T1O->(!Eof()) .And. T1O->( T1O_FILIAL + T1O_ID + T1O_VERSAO + T1O_IDESTA + T1O_INDCOM ) == xFilial( "T1O" ) + ('T1N')->( &( "T1N_ID") + &( "T1N_VERSAO" ) + &( "T1N_IDESTA" ) + &( "T1N_INDCOM" )  )

					If T6B->( MsSeek( xFilial( "T6B" ) +  T1O->(T1O_ID + T1O_VERSAO + T1O_IDESTA + T1O_INDCOM + T1O_TPINSA + T1O_NRINSA ) ) )
						Do While !T6B->( Eof() ) .And. T1O->(T1O_ID + T1O_VERSAO + T1O_IDESTA + T1O_INDCOM + T1O_TPINSA + T1O_NRINSA) == T6B->(T6B_ID + T6B_VERSAO + T6B_IDESTA + T6B_INDCOM + T6B_TPINSA + T6B_NRINSA)

							cXmlNfs +=         "<nfs>"
							cXmlNfs +=           xTafTag("serie"         ,T6B->T6B_SERIE,,.T.)
							cXmlNfs +=           xTafTag("nrDocto"       ,T6B->T6B_NUMDOC)
							cXmlNfs +=           xTafTag("dtEmisNF"      ,T6B->T6B_DTEMIS)
							cXmlNfs +=           xTafTag("vlrBruto"      ,T6B->T6B_VLBRUT,PesqPict("T6B","T6B_VLBRUT"))
							cXmlNfs +=           xTafTag("vrCPDescPR"		,T6B->T6B_VLCONT,PesqPict("T6B","T6B_VLCONT"))
							cXmlNfs +=           xTafTag("vrRatDescPR"   ,T6B->T6B_VLGILR,PesqPict("T6B","T6B_VLGILR"))
							cXmlNfs +=           xTafTag("vrSenarDesc"   ,T6B->T6B_VLSENA,PesqPict("T6B","T6B_VLSENA"))
							cXmlNfs +=         "</nfs>"

							T6B->( DbSkip() )
						EndDo
					EndIf

					xTafTagGroup("ideAdquir"	,{{"tpInsc"	,T1O->T1O_TPINSA,,.F.};
												, {"nrInsc"	,T1O->T1O_NRINSA,,.F.};
												, {"vrComerc"	,T1O->T1O_VLRCOM,PesqPict("T1O","T1O_VLRCOM"),.F.}};
												, @cXml,{{"nfs",cXmlNfs,0}})

					T1O->(DBSkip())
				EndDo

			EndIf


			If T1P->( MsSeek( xFilial( "T1P" ) + ('T1N')->( &( "T1N_ID") + &( "T1N_VERSAO" ) + &( "T1N_IDESTA" ) + &( "T1N_INDCOM" ) ) ) )
				While T1P->(!Eof()) .And. T1P->( T1P_FILIAL + T1P_ID + T1P_VERSAO + T1P_IDESTA + T1P_INDCOM ) == xFilial( "T1P" ) + ('T1N')->( &( "T1N_ID") + &( "T1N_VERSAO" ) + &( "T1N_IDESTA" ) + &( "T1N_INDCOM" ) )
					cXml +=			"<infoProcJud>"

					//Inverto os c�digos para atender o layout do eSocial
					cTpProc := Posicione("C1G",8,xFilial("C1G") + T1P->T1P_IDPROC,"C1G_TPPROC")

					If !Empty( cTpProc )
						cTpProc := Iif(Alltrim(cTpProc) == "1", "2", Iif(Alltrim(cTpProc) == "2", "1", cTpProc) )
					EndIf

					cXml +=				xTafTag("tpProc",		cTpProc )
					cXml +=				xTafTag("nrProc",		Posicione("C1G",8,xFilial("C1G") + T1P->T1P_IDPROC,"C1G_NUMPRO") )

						cCodSusp  	:= Posicione("T5L",1,xFilial("T5L")+T1P->T1P_IDSUSP,"T5L_CODSUS")

						If !Empty(cCodSusp)
						cXml += xTafTag("codSusp", Alltrim(cCodSusp))
						EndIf

						cXml +=				xTafTag("vrCPSusp"	, T1P->T1P_VLRPRV , PesqPict("T1P","T1P_VLRPRV"),,.T.)
						cXml +=				xTafTag("vrRatSusp"	, T1P->T1P_VLRRAT , PesqPict("T1P","T1P_VLRRAT"),,.T.)
						cXml +=				xTafTag("vrSenarSusp", T1P->T1P_VLRSEN , PesqPict("T1P","T1P_VLRSEN"),,.T.)
						cXml +=			"</infoProcJud>"
						T1P->(DBSkip())
				EndDo
			EndIf

			cXml +=			"</tpComerc>"
			T1N->(DBSkip())
		EndDo
	EndIf
	T1N->(DbCloseArea())

	cXml +=			"</ideEstabel>"
	cXml +=		"</infoComProd>"

	/*����������������������Ŀ
	�Estrutura do cabecalho�
	������������������������*/
	cXml := xTafCabXml(cXml,"T1M",cLayout,cReg,aMensal,cSeqXml)


	/*����������������������������Ŀ
	�Executa gravacao do registro�
	������������������������������*/
	If !lJob
		xTafGerXml(cXml,cLayout)
	EndIf
EndIf

Return(cXml)
//-------------------------------------------------------------------------------------
/*/{Protheus.doc} TAF414Vld

@author Daniel Schmidt
@since 11/01/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------
Function TAF414Vld(cAlias,nRecno,nOpc,lJob)

Local aLogErro    
Local aDadosUtil  
Local aAreaT3Q	  
Local cStatus     
Local cIdNtJur    
Local cNatJur	  
Local cMes		  
Local cAno		  
Local lOrgPub	  
Local lValida     

Default lJob := .F.

aLogErro   := {}
aDadosUtil := {}
aAreaT3Q   := {}
cStatus    := ""
cIdNtJur   := ""
cNatJur	   := ""
cMes       := ""
cAno	   := ""
lOrgPub	   := ""
lValida    := T1M->T1M_STATUS $ ( " |1" )

//verificar se Org�o P�blico
cIdNtJur := Posicione("C1E",3,xFilial("C1E")+SM0->M0_CODFIL+"1","C1E_NATJUR")
cNatJur  := Posicione("C8P",1,xFilial("C8P")+cIdNtJur,"C8P_CODIGO")
lOrgPub	 := cNatJur $ "1015|1040|1074|1163"

//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := T1M->( Recno() )

If !lValida
	aAdd( aLogErro, { "T1M_ID", "000305", "T1M", nRecno } ) //Registros que j� foram transmitidos ao Fisco, n�o podem ser validados
Else

	//###############################################################################
	//Regras
	//###############################################################################

	/*
	REGRA_EXISTE_INFO_EMPREGADOR
	REGRA_EVE_FOPAG_COMERC_PROD
	//N�o � permitido o envio de mais de um evento no mesmo per�odo de apura��o pelo mesmo CAEPF.
	REGRA_EVE_FOPAG_IND_RETIFICACAO

	REGRA_EVE_FOPAG_INFO_COMPAT_CLASSTRIB
	//Verificar compatibilidade entre o tipo de evento e a classifica��o tribut�ria do
	contribuinte, conforme abaixo:
	a) O evento S-1250 (Aquis. Prod Rural) n�o deve ser informado pela classTrib [22];
	b) O evento S-1260 (Comerc. Prod Rural) somente pode ser informado pelas classTrib [21,22];
	c) O evento S-1280 - Informa��es Complementares aos Eventos Peri�dicos � permitido apenas se {classTrib} = [03,09] ou ({classTrib} = [02,99] e {indDesFolha} = [1]).

	REGRA_EVE_FOPAG_PERMITE_EXCLUSAO
	//Se o evento que est� sendo exclu�do pertencer a um per�odo de apura��o para o
  	qual j� exista encerramento das informa��es, o evento de exclus�o somente ser�
	aceito se encaminhado ap�s o evento de "reabertura" das informa��es (S-1298).

	*/

	//REGRA_EVE_FOPAG_INFO_COMPAT_CLASSTRIB
	aCampos := {}
	AADD(aCampos,{"S1260"})
	xVldRegra("C1E","REGRA_EVE_FOPAG_INFO_COMPAT_CLASSTRIB",@aLogErro,aCampos, "T1M")

	//###############################################################################
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	//Valida��o dos campos
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	//**********************************
	//eSocial/evtComProd/ideEvento/
	//**********************************
	//indApuracao
	If Empty(T1M->T1M_INDAPU)
		AADD(aLogErro,{"T1M_INDAPU","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	Else
		If T1M->T1M_INDAPU <> "1"
			aAdd( aLogErro, { "T1M_INDAPU", "000006", "T1M", nRecno } ) //STR0006 - Conte�do do campo n�o condiz com as op��es poss�veis
		EndIf
	EndIf
	//perApur
	If Empty(T1M->T1M_PERAPU)
		AADD(aLogErro,{"T1M_PERAPU","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	Else
		If !(xVldFormDt(T1M->T1M_PERAPU, "AAAAMM"))
			AADD(aLogErro,{"T1M_PERAPU","000805","T1M", nRecno }) //"Deve ser um m�s/ano v�lido e estar no formato AAAA-MM (Ano-M�s)."
		Else
			cMes := Substr(T1M->T1M_PERAPU,5,2)
			cAno := Substr(T1M->T1M_PERAPU,1,4)
			If cMes < '09' .AND. cAno < '2016'
				AADD(aLogErro,{"T1M_PERAPU","000815","T1M", nRecno })	//Deve ser um m�s/ano v�lido, igual ou posterior a implementa��o do eSocial.
			EndIf
		EndIF
	EndIf
	//----------------------------------

	//**********************************
	//eSocial/evtComProd/infoComProd/ideEstabel/
	//**********************************
	//nrInscEstabRural
	If Empty(T1M->T1M_IDESTA)
		AADD(aLogErro,{"T1M_IDESTA","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	Else
		DBSelectArea( "C92" )
		C92->( DBSetOrder( 5 ))
		if !(C92->( DbSeek( xFilial("C92") + T1M->T1M_IDESTA + "1")))
   			AADD(aLogErro,{"T1M_IDESTA","000704","T1M",nRecno })//"O n�mero de inscri��o n�o consta na tabela S-1005(Tabela de Estabelecimentos e Obras)."
		Else
			if Empty(C92->C92_STATUS) .or. C92->C92_STATUS == "2"
				AADD(aLogErro,{"T1M_IDESTA","000724","T1M",nRecno }) //"N�mero de inscri��o n�o v�lido. Fa�a a valida��o do registro S-1005 (Tabela de Estabelecimentos e Obras)."
			EndIf
		EndIf
	EndIf
	//----------------------------------

	//**********************************
	//eSocial/evtComProd/infoComProd/ideEstabel/tpComerc
	//**********************************
	T1N->(DBSetOrder(1))
	If T1N->(MsSeek(xFilial("T1N")+T1M->T1M_ID+T1M->T1M_VERSAO+T1M->T1M_IDESTA))
		Do While !("T1N")->(Eof()) .And. T1N->T1N_ID+T1N->T1N_VERSAO+T1N->T1N_IDESTA == T1M->T1M_ID+T1M->T1M_VERSAO+T1M->T1M_IDESTA
			//indComerc
			If Empty(T1N->T1N_INDCOM)
				AADD(aLogErro,{"T1N_INDCOM","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
			Else
				xValRegTab( "T1T", T1N->T1N_INDCOM, 1, , @aLogErro, /*Data validacao periodo*/, { "T1M", "T1N_INDCOM", nRecno } )
			EndIf
			//vrTotCom
			If Empty(T1N->T1N_VLRTOT)
				AADD(aLogErro,{"T1N_VLRTOT","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
			Else
				If T1N->T1N_VLRTOT <= 0
					AADD(aLogErro,{"T1N_VLRTOT","000520","T1M", nRecno }) //"O valor informado no campo deve ser maior que 0 (zero)"
				EndIf
			EndIf

			//**********************************
			//eSocial/evtComProd/infoComProd/ideEstabel/tpComerc/ideAdquir
			//**********************************
			cIndComerc	:= T1N->T1N_INDCOM
			nVlrTot	:= T1N->T1N_VLRTOT
			T1O->(DBSetOrder(1))
			If T1O->(MsSeek(xFilial("T1O")+T1N->T1N_ID+T1N->T1N_VERSAO+T1N->T1N_IDESTA+T1N->T1N_INDCOM))
				Do While !("T1O")->(Eof()) .And. T1O->T1O_ID+T1O->T1O_VERSAO+T1O->T1O_IDESTA+T1O->T1O_INDCOM == T1N->T1N_ID+T1N->T1N_VERSAO+T1N->T1N_IDESTA+T1N->T1N_INDCOM
					//tpInsc
					If !(T1O->T1O_TPINSA $ ("1|2|"))
						aAdd( aLogErro, { "T1O_TPINSA", "000006", "T1M", nRecno } ) //STR0006 - Conte�do do campo n�o condiz com as op��es poss�veis
					Else
						If Posicione("T1T",1,xFilial("T1T") + cIndComerc,"T1T_CODIGO") $ ("3|7") 
							If !(T1O->T1O_TPINSA $ ("1|2|"))
							 	aAdd( aLogErro, { "T1O_TPINSA", "000820", "T1M", nRecno } ) //Se Indicativo de Comercializa��o for igual a [3,7],o tipo de inscri��o da identifica��o dos adquirentes da produ��o deve ser igual a [1,2].
							EndIf
						ElseIf Posicione("T1T",1,xFilial("T1T") + cIndComerc,"T1T_CODIGO") == "8"
							If !(T1O->T1O_TPINSA == "1")
							 	aAdd( aLogErro, { "T1O_TPINSA", "000819", "T1M", nRecno } ) //"Se Indicativo de Comercializa��o for igual a [8], o tipo de inscri��o da identifica��o dos adquirentes da produ��o deve ser igual a [1]."
							EndIf
						EndIf
					EndIf
					//nrInsc
					If Empty(T1O->T1O_NRINSA)
						AADD(aLogErro,{"T1O_NRINSA","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
					EndIf
					//vrComerc
					If Empty(T1O->T1O_VLRCOM)
						AADD(aLogErro,{"T1O_VLRCOM","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
					Else
						If T1O->T1O_VLRCOM < 0
							aAdd( aLogErro, { "T1O_TPINSA", "000520", "T1M", nRecno } ) //"O valor informado no campo deve ser maior que 0 (zero)"
						EndIf
					EndIf

					T6B->( DBSetOrder(1) )
					If T6B->( MsSeek( xFilial("T6B") + xFilial("T1O")+T1N->T1N_ID+T1N->T1N_VERSAO+T1N->T1N_IDESTA+T1N->T1N_INDCOM ) )

						nSumVlBrut := 0

						Do While !T6B->( Eof() ) .And. T1O->( T1O_FILIAL + T1O_ID + T1O_VERSAO + T1O_IDESTA + T1O_INDCOM + T1O_TPINSA + T1O_NRINSA) == T6B->(T6B_FILIAL + T6B_ID + T6B_VERSAO + T6B_IDESTA + T6B_INDCOM + T6B_TPINSA + T6B_NRINSA)

							//nrDocto
							//--------
							//Numero da Nota Fiscal/Fatura
							If Empty(T6B->T6B_NUMDOC)
								AADD(aLogErro,{"T6B_NUMDOC","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
							EndIf

							//dtEmisNF
							//-----------
							//Data de Emissao da Nota Fiscal/Fatura
							//Validacao: O mes/ano da emissao da nota fiscal deve ser igual
							//ao mes/ano indicado no registro de abertura do arquivo.
							If Empty(T6B->T6B_DTEMIS)
								AADD(aLogErro,{"T6B_DTEMIS","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
							Else
								//Folha de pagamento mensal: Formato MMAAAA
								If T1M->T1M_INDAPU == "1"
									//Encontro o Ano e o Mes do periodo e converto para numerico
									nAnoApu := Val( SubStr(T1M->T1M_PERAPU,3,4) )
									nMesApu := Val( SubStr(T1M->T1M_PERAPU,1,2) )

									If nAnoApu <> Year(T6B->T6B_DTEMIS) .Or. nMesApu <> Month(T6B->T6B_DTEMIS)
										AADD(aLogErro,{"T6B_DTEMIS","000009","T1M", nRecno }) //STR0009 - "Data Inconsistente ou Vazia"
									EndIf

								//Folhe de decimo terceiro salario: Formato AAAA
								Else
									nAnoApu := Val( Alltrim(T1M->T1M_PERAPU) )
									If nAnoApu <> Year(T6B->T6B_DTEMIS)
										AADD(aLogErro,{"T6B_DTEMIS","000009","T1M", nRecno  }) //STR0009 - "Data Inconsistente ou Vazia"
									EndIf
								EndIf
							EndIf

							//vlrBruto
							//--------
							//Preencher com o valor bruto da(s) nota(s) fiscal(is)
							If Empty(T6B->T6B_VLBRUT)
								AADD(aLogErro,{"T6B_VLBRUT","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
							Else
								nSumVlBrut += T6B->T6B_VLBRUT
							EndIf

							T6B->( DbSkip() )
						EndDo
					EndIf

					T1O->(dbSkip())
				EndDo
			EndIf


			//**********************************
			//eSocial/evtComProd/infoComProd/ideEstabel/tpComerc/infoProcJud
			//**********************************
			T1P->(DBSetOrder(1))
			//T1P_FILIAL+T1P_ID+T1P_VERSAO+T1P_NRINSC+T1P_INDCOM+T1P_IDPROC
			If T1P->(MsSeek(xFilial("T1P")+T1N->T1N_ID+T1N->T1N_VERSAO+T1N->T1N_IDESTA+T1N->T1N_INDCOM))
				Do While !("T1P")->(Eof()) .And. T1P->T1P_ID+T1P->T1P_VERSAO+T1P->T1P_IDESTA+T1P->T1P_INDCOM == T1N->T1N_ID+T1N->T1N_VERSAO+T1N->T1N_IDESTA+T1N->T1N_INDCOM
					//nrProc
					//Deve ser um n�mero de processo administrativo ou judicial v�lido e existente na Tabela de Processos (S-1070).
					DBSelectArea( "C1G" )
					C1G->( DBSetOrder( 8 ))
					//C1G_FILIAL+C1G_ID+C1G_ATIVO
					If !(C1G->( DbSeek( xFilial("C1G") + T1P->T1P_IDPROC + "1")))
   						AADD(aLogErro,{"T1P_IDPROC","000817","T1M",nRecno })//"O Processo Judicial deve existir na Tabela de Processos (S-1070)."
					Else
						If !(C1G->C1G_STATUS == "0" .or. C1G->C1G_STATUS == "2")
							AADD(aLogErro,{"T1P_IDPROC","000818","T1M",nRecno }) //"N�mero de inscri��o n�o v�lido. Fa�a a valida��o do registro S-1070 (Tabela de Processos Administrativos/Judiciais)."
						EndIf
					EndIf
					//vrCPSusp
					//Preenchimento obrigat�rio se {vrRatSusp} e/ou {vrSenarSusp} n�o tiver(em) sido preenchido(s).
					If Empty(T1P->T1P_VLRPRV) .OR. Empty(T1P->T1P_VLRSEN)
						If Empty(T1P->T1P_VLRPRV)
							AADD(aLogErro,{"T1P_VLRPRV","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
						EndIf
					EndIf
					//vrRatSusp
					//Preenchimento obrigat�rio se {vrCPSusp} e/ou {vrSenarSusp} n�o tiver(em) sido preenchido(s).
					If Empty(T1P->T1P_VLRPRV) .OR. Empty(T1P->T1P_VLRSEN)
						If Empty(T1P->T1P_VLRRAT)
							AADD(aLogErro,{"T1P_VLRRAT","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
						EndIf
					EndIf
					//vrSenarSusp
					//Preenchimento obrigat�rio se {vrCPSusp} e/ou {vrRatSusp} n�o tiver(em) sido preenchido(s).
					If Empty(T1P->T1P_VLRPRV) .OR. Empty(T1P->T1P_VLRRAT)
						If Empty(T1P->T1P_VLRSEN)
							AADD(aLogErro,{"T1P_VLRSEN","000010","T1M", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
						EndIf
					EndIf
					T1P->(dbSkip())
				EndDo
			EndIf
			T1N->(dbSkip())
		EndDo
	EndIf
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

	//�������������������������������
	//�ATUALIZO O STATUS DO REGISTRO�
	//�1 = Registro Invalido        �
	//�0 = Registro Valido          �
	//�������������������������������
	cStatus := Iif( Len( aLogErro ) > 0, "1", "0" )
	Begin Transaction
		If RecLock( "T1M", .F. )
			T1M->T1M_STATUS := cStatus
			T1M->( MsUnlock() )
		EndIf
	End Transaction

EndIf

//�������������������������������������������������������Ŀ
//�Nao apresento o alert quando utilizo o JOB para validar�
//���������������������������������������������������������
If !lJob
	xValLogEr( aLogErro )
EndIf

Return(aLogErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} GerarEvtExc
Funcao que gera a exclus�o do evento (S-3000)

@Param  oModel  -> Modelo de dados
@Param  nRecno  -> Numero do recno
@Param  lRotExc -> Variavel que controla se a function � chamada pelo TafIntegraESocial

@Return .T.

@author Daniel Schmidt
@since 11/01/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GerarEvtExc( oModel, nRecno, lRotExc )

Local cVerAnt    	 
Local cProtocolo 	 
Local cVersao    	 
Local cChvRegAnt 	 
Local cEvento	 	 
Local cId		 	 

Local nOperation 	 
Local nI         	 
Local nT1N		 	 
Local nT1O		 	 
Local nT1P		 	 
Local nT6B		 	 

Local aCampos    	 
Local aGrava     	 
Local aGravaT1N  	 
Local aGravaT1O  	 
Local aGravaT1P  	 
Local aGravaT6B  	 

Local oModelT1M  	 
Local oModelT1N  	 
Local oModelT1O  	 
Local oModelT1P  	 
Local oModelT6B  	 

cVerAnt   	:= ""
cProtocolo	:= ""
cVersao   	:= ""
cChvRegAnt	:= ""
cEvento	:= ""
cId		   	:= ""

nOperation	:= oModel:GetOperation()
nI        	:= 0
nT1N	   	:= 0
nT1O	   	:= 0
nT1P	   	:= 0
nT6B	   	:= 0

aCampos    := {}
aGrava     := {}
aGravaT1N  := {}
aGravaT1O  := {}
aGravaT1P  := {}
aGravaT6B  := {}

oModelT1M  := Nil
oModelT1N  := Nil
oModelT1O  := Nil
oModelT1P  := Nil
oModelT6B  := Nil

//Controle se o evento � extempor�neo
lGoExtemp	:= Iif( Type( "lGoExtemp" ) == "U", .F., lGoExtemp )

Begin Transaction

	//Posiciona o item
	("T1M")->( DBGoTo( nRecno ) )

	//Carrego a Estrutura dos Models a serem gravados
	oModelT1M := oModel:GetModel( "MODEL_T1M" )
	oModelT1N := oModel:GetModel( "MODEL_T1N" )
	oModelT1O := oModel:GetModel( "MODEL_T1O" )
	oModelT1P := oModel:GetModel( "MODEL_T1P" )
	oModelT6B := oModel:GetModel( "MODEL_T6B" )


	//Guardo as informa��es do registro corrente para rastro do registro
	cVerAnt   	:= oModelT1M:GetValue( "T1M_VERSAO" )
	cProtocolo	:= oModelT1M:GetValue( "T1M_PROTUL" )
	cEvento	:= oModelT1M:GetValue( "T1M_EVENTO" )

	//Armazeno as informa��es correntes do cadastro( Depois da altera��o do Usu�rio )

	//***********
	//Informa��o da Comercializa��o de Produ��o
	//***********
	For nI := 1 to Len( oModelT1M:aDataModel[ 1 ] )
		Aadd( aGrava, { oModelT1M:aDataModel[ 1, nI, 1 ], oModelT1M:aDataModel[ 1, nI, 2 ] } )
	Next nI
	//------------------

	//***********
	//Tipo de Comercializa��o
	//***********
	//***********
	For nT1N := 1 to oModel:GetModel( "MODEL_T1N" ):Length()
		oModel:GetModel( "MODEL_T1N" ):GoLine(nT1N)
		If !oModel:GetModel( 'MODEL_T1N' ):IsEmpty()
			If !oModel:GetModel( "MODEL_T1N" ):IsDeleted()
				aAdd(aGravaT1N,{oModelT1N:GetValue("T1N_INDCOM"),;
				                oModelT1N:GetValue("T1N_VLRTOT")})

				//***********
				//Identifica��o dos Adquirentes da Produ��o
				//***********
				For nT1O := 1 to oModel:GetModel( "MODEL_T1O" ):Length()
					oModel:GetModel( "MODEL_T1O" ):GoLine(nT1O)
					If !oModel:GetModel( 'MODEL_T1O' ):IsEmpty()
						If !oModel:GetModel( "MODEL_T1O" ):IsDeleted()
							aAdd(aGravaT1O,{oModelT1N:GetValue("T1N_INDCOM"),;
											oModelT1O:GetValue("T1O_TPINSA"),;
				                		    oModelT1O:GetValue("T1O_NRINSA"),;
				                			oModelT1O:GetValue("T1O_VLRCOM")})


			                For nT6B := 1 To oModel:GetModel( 'MODEL_T6B' ):Length()
								oModel:GetModel( 'MODEL_T6B' ):GoLine(nT6B)

								If !oModel:GetModel( 'MODEL_T6B' ):IsDeleted()
									aAdd (aGravaT6B ,{ oModelT1N:GetValue("T1N_INDCOM"),;
													   oModelT1O:GetValue("T1O_TPINSA"),;
							                		   oModelT1O:GetValue("T1O_NRINSA"),;
													   oModelT6B:GetValue("T6B_SERIE"),;
													   oModelT6B:GetValue("T6B_NUMDOC"),;
													   oModelT6B:GetValue("T6B_DTEMIS"),;
													   oModelT6B:GetValue("T6B_VLBRUT"),;
													   oModelT6B:GetValue("T6B_VLCONT"),;
													   oModelT6B:GetValue("T6B_VLGILR"),;
													   oModelT6B:GetValue("T6B_VLSENA")} )
								EndIf
							Next nT6B
						EndIf
					EndIf
				Next nT1O
				//-----------
				//***********
				//Informa��es de Processos Judiciais
				//***********
				For nT1P := 1 to oModel:GetModel( "MODEL_T1P" ):Length()
					oModel:GetModel( "MODEL_T1P" ):GoLine(nT1P)
					If !oModel:GetModel( 'MODEL_T1P' ):IsEmpty()
						If !oModel:GetModel( 'MODEL_T1P' ):IsDeleted()
							aAdd(aGravaT1P,{oModelT1N:GetValue("T1N_INDCOM"),;
											oModelT1P:GetValue("T1P_IDPROC"),;
				                			oModelT1P:GetValue("T1P_VLRPRV"),;
				             	  			oModelT1P:GetValue("T1P_VLRRAT"),;
				                			oModelT1P:GetValue("T1P_VLRSEN")})
						EndIf
					EndIf
				Next nT1P
				//-----------

			EndIf
		EndIf
	Next nT1N
	//-----------

	/*----------------------------------------------------------
	Seto o campo como Inativo e gravo a versao do novo registro
	no registro anterior

	ATENCAO -> A alteracao destes campos deve sempre estar
	abaixo do Loop do For, pois devem substituir as informacoes
	que foram armazenadas no Loop acima
	-----------------------------------------------------------*/
	FAltRegAnt( "T1M", "2" )

	/*----------------------------------------------------------
	Apos deixar o registro corrente como inativo eu seto a
	opera��o de inclus�o para o novo registro
	-----------------------------------------------------------*/
	oModel:DeActivate()
	oModel:SetOperation( 3 )
	oModel:Activate()

	/*----------------------------------------------------------
	Neste momento eu realizo a grava��o de um novo registro id�ntico
	ao original, apenas com as altera��es nos campos modificados
	pelo usu�rio no cadastro
	-----------------------------------------------------------*/
	//***********
	//Informa��o da Comercializa��o de Produ��o
	//***********
	For nI := 1 to Len( aGrava )
		oModel:LoadValue( "MODEL_T1M", aGrava[ nI, 1 ], aGrava[ nI, 2 ] )
	Next nI

	//***********
	//Tipo de Comercializa��o
	//***********
	For nT1N := 1 to Len( aGravaT1N )
		If nT1N > 1
			oModel:GetModel( "MODEL_T1N" ):AddLine()
		EndIf
		oModel:LoadValue( "MODEL_T1N", "T1N_INDCOM" , aGravaT1N[nT1N][1] )
		oModel:LoadValue( "MODEL_T1N", "T1N_VLRTOT" , aGravaT1N[nT1N][2] )

		//***********
		//Identifica��o dos Adquirentes da Produ��o
		//***********
		For nT1O := 1 to Len( aGravaT1O )
			If  aGravaT1N[nT1N][1] == aGravaT1O[nT1O][1]
				If nT1O > 1
					oModel:GetModel( "MODEL_T1O" ):AddLine()
				EndIf
				oModel:LoadValue( "MODEL_T1O", "T1O_TPINSA" , aGravaT1O[nT1O][2] )
				oModel:LoadValue( "MODEL_T1O", "T1O_NRINSA" , aGravaT1O[nT1O][3] )
				oModel:LoadValue( "MODEL_T1O", "T1O_VLRCOM" , aGravaT1O[nT1O][4] )

				For nT6B := 1 To Len( aGravaT6B )
					If aGravaT1N[nT1N][1] == aGravaT6B[nT6B][1] .AND. aGravaT1O[nT1O][2] == aGravaT6B[nT6B][2] .AND. aGravaT1O[nT1O][3] == aGravaT6B[nT6B][3]

						If nT6B > 1
							oModel:GetModel( 'MODEL_T6B' ):AddLine()
						EndIf
						oModel:LoadValue( "MODEL_T6B", "T6B_SERIE",  aGravaT6B[nT6B][4] )
						oModel:LoadValue( "MODEL_T6B", "T6B_NUMDOC", aGravaT6B[nT6B][5] )
						oModel:LoadValue( "MODEL_T6B", "T6B_DTEMIS", aGravaT6B[nT6B][6] )
						oModel:LoadValue( "MODEL_T6B", "T6B_VLBRUT", aGravaT6B[nT6B][7] )
						oModel:LoadValue( "MODEL_T6B", "T6B_VLCONT", aGravaT6B[nT6B][8] )
						oModel:LoadValue( "MODEL_T6B", "T6B_VLGILR", aGravaT6B[nT6B][9] )
						oModel:LoadValue( "MODEL_T6B", "T6B_VLSENA", aGravaT6B[nT6B][10] )
					EndIf

				Next nT6B
			EndIf
		Next nT1O

		//***********
		//Informa��es de Processos Judiciais
		//***********
		For nT1P := 1 to Len( aGravaT1P )
			If  aGravaT1N[nT1N][1] == aGravaT1P[nT1P][1]
				oModel:GetModel( 'MODEL_T1P' ):LVALID	:= .T.

				If nT1P > 1
					oModel:GetModel( "MODEL_T1P" ):AddLine()
				EndIf
				oModel:LoadValue( "MODEL_T1P", "T1P_IDPROC",	aGravaT1P[nT1P][2] )
				oModel:LoadValue( "MODEL_T1P", "T1P_VLRPRV",	aGravaT1P[nT1P][3] )
				oModel:LoadValue( "MODEL_T1P", "T1P_VLRRAT",	aGravaT1P[nT1P][4] )
				oModel:LoadValue( "MODEL_T1P", "T1P_VLRSEN",	aGravaT1P[nT1P][5] )
			EndIf
		Next nT1P

	Next nT1N

	//Busco a nova versao do registro
	cVersao := xFunGetVer()

	/*---------------------------------------------------------
	ATENCAO -> A alteracao destes campos deve sempre estar
	abaixo do Loop do For, pois devem substituir as informacoes
	que foram armazenadas no Loop acima
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_T1M", "T1M_VERSAO", cVersao )
	oModel:LoadValue( "MODEL_T1M", "T1M_VERANT", cVerAnt )
	oModel:LoadValue( "MODEL_T1M", "T1M_PROTPN", cProtocolo )
	oModel:LoadValue( "MODEL_T1M", "T1M_PROTUL", "" )

	/*---------------------------------------------------------
	Tratamento para que caso o Evento Anterior fosse de exclus�o
	seta-se o novo evento como uma "nova inclus�o", caso contr�rio o
	evento passar a ser uma altera��o
	-----------------------------------------------------------*/
	oModel:LoadValue( "MODEL_T1M", "T1M_EVENTO","E" )
	oModel:LoadValue( "MODEL_T1M", "T1M_ATIVO", "1" )

	//Gravo altera��o para o Extempor�neo
	If lGoExtemp
		TafGrvExt( oModel, 'MODEL_T1M', 'T1M' )
	EndIf

	FwFormCommit( oModel )
	TAFAltStat( 'T1M',"6" )

End Transaction

Return ( .T. )

//-------------------------------------------------------------------
/*/{Protheus.doc} ImportDataNF

Fun��o respons�vel por realizar importa��o de Notas Fiscais para
Consumidor / Comerciante Rural.
Eventos : S-2150 e S-2160

@author Ricardo
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------

Function ImportDataNF(oModel)

Local oModelT1M 	 
Local cTemp			 
Local cArea			 
Local cQtdRet		 
Local cQuery		 
Local cPerApur		 
Local nTamCGC		 
Local cRetEmp		 
Local cRetFil		 
Local aInfSM0		 
Local cTpEstab		 
Local cTpEsInsc		 

oModelT1M	:= Nil
cTemp		:= GetNextAlias()
cArea		:= ""
cQtdRet		:= ""
cQuery		:= ""
cPerApur	:= ""
nTamCGC		:= 0
cRetEmp		:= ""
cRetFil		:= ""
aInfSM0		:= {}
cTpEstab	:= ""
cTpEsInsc	:= ""

//Guarda valores de vari�veis globais
cRetEmp := cEmpAnt
cRetFil := cFilAnt

//Carrega vari�veis para localizar CGC no Sigamat e posicionar.
oModelCMR	:= oModel:GetModel( 'MODEL_T1M' )							// Model infos do Estabelecimento
cPerApur	:= oModelCMR:GetValue("T1M_PERAPU")							// Periodo de apura��o
cCGC		:= AllTrim(oModelCMR:GetValue("T1M_NRCAEP"))				// Numero do Doc do Estabelecimento no Model
cIdEstab	:= AllTrim(oModelCMR:GetValue("T1M_IDESTA"))				// Id do Estabelecimento do Model
cTpEstab 	:= POSICIONE("C92",1, xFilial("C92")+cIdEstab,"C92_TPCAEP") // Solicita o tipo do produtor no S-1005
cTpEsInsc 	:= POSICIONE("C92",1, xFilial("C92")+cIdEstab,"C92_TPINSC") // Tipo da Inscri��o Pj ou PF

If cTpEsInsc == "1" //1-CNPJ
	//Chama Fun��o que varre as informa��es do SM0 - Sigamat
	aInfSM0 	:= SearchSM0(cCGC)
	If Len(aInfSM0) > 0
		cEmpAnt := aInfSM0[1,1]
		cFilAnt := aInfSM0[1,2]
	Else
		MsgAlert(STR0014) //"N�o encontrado informa��es desta filial no Sistema com este CGC cadastrado"
		Return
	EndIf
EndIf

If !EMPTY(cPerApur)

	ImportT1N(oModel,cPerApur,cTpEstab,cTpEsInsc)// Informa��es do Tipo
	ImportT1O(oModel,cPerApur)// Informa��es dos Adquirentes
	ImportT6B(oModel,cPerApur)// Informa��es das NFs de Sa�da

	MsgAlert(STR0015)//"Processo de Importa��o de Notas Fiscais Conclu�do"

Else
	MsgAlert(STR0016)//"Informar o periodo para importa��o das Notas Fiscais( Per.Apura��o )"
EndIf

cEmpAnt := cRetEmp
cFilAnt := cRetFil

Return(.T.)


//-------------------------------------------------------------------
/*/{Protheus.doc} ImportT1N

Fun��o respons�vel por realizar importa��o de Notas Fiscais para
Consumidor / Comerciante Rural.
Eventos : S-2160

@author Ricardo
@since 23/09/2017
@version 1.0
/*/
//-------------------------------------------------------------------

Function ImportT1N(oModel,cPerApur,cTpEstab,cTpEsInsc)

Local oModelT1N 	 
Local cTemp			 
Local cArea			 
Local cQtdRet		 
Local cQuery		 
Local cTipoPart		 
Local lRet			 
Local nT1N			 

Default cPerApur	:= ""
Default cTpEstab	:= ""
Default cTpEsInsc	:= ""

oModelT1N 			:= Nil
cTemp				:= GetNextAlias()
cArea				:= ""
cQtdRet				:= ""
cQuery				:= ""
cTipoPart			:= "1"


oModelT1N := oModel:GetModel( 'MODEL_T1N' )


	cQuery	+=	"SELECT "
	cQuery	+=	"C1H.C1H_CNPJ,"
	cQuery	+=	"C1H.C1H_CPF,"
	cQuery	+=	"C1H.C1H_PAA," // Criar Campo na Totvs - ATUSX
	cQuery	+=	"C1H.C1H_PPES,"
	cQuery	+=	"C1H.C1H_RAMO,"
	cQuery	+=	"C1H.C1H_CODPAI,"
	cQuery	+=	"SUM(C20.C20_VLDOC) AS VALTOT "
	cQuery	+=	"FROM " + RetSqlName( 'C20' ) + " C20 "
	cQuery	+=	"JOIN " + RetSqlName( 'C1H' ) + " C1H "
	cQuery  +=  " ON(C1H.C1H_FILIAL='" + xFilial('C1H') + "' AND "
	cQuery  +=  	"C1H.C1H_ID = C20.C20_CODPAR "
	cQuery  +=		"AND C1H.D_E_L_E_T_=' ') "
	cQuery	+=	"WHERE  "
	cQuery	+=	"C20.C20_FILIAL='" + xFilial('C20') + "' AND "
	cQuery	+=	"C20.C20_INDOPE ='1' AND " //Sa�da
	cQuery	+=	"SUBSTRING(C20.C20_DTDOC,1,6) >='" + cPerApur + "' AND "
	cQuery	+=	"SUBSTRING(C20.C20_DTDOC,1,6) <='" + cPerApur + "' AND "
	cQuery	+=	"C20.D_E_L_E_T_=' ' "
	cQuery	+=  "GROUP BY C1H.C1H_CNPJ, C1H.C1H_CPF, C1H.C1H_PAA, C1H.C1H_PPES, C1H.C1H_RAMO, C1H.C1H_CODPAI"

	// Executa a Query.
	DBUseArea( .T., "TOPCONN", TCGenQry( ,, ChangeQuery( cQuery ) ), cTemp, .F., .T. )

	nT1N := 1
	While ( cTemp )->( !Eof() )
		/*
		2 - Comercializa��o da Produ��o efetuada diretamente no varejo a consumidor final ou a outro produtor rural pessoa f�sica por Produtor Rural Pessoa F�sica, inclusive por Segurado Especial ou por Pessoa F�sica n�o produtor rural;
			c92 igual a produtor rural, c1h=consumidor final
				ou
			c92 igual PF e c1h = produtor rural PF ou produtor rural pessoa fisica e nao se enquadrar nas opcoes abaixo
		3 - Comercializa��o da Produ��o por Prod. Rural PF/Seg. Especial - Vendas a PJ (exceto Entidade inscrita no Programa de Aquisi��o de Alimentos - PAA) ou a Intermedi�rio PF;
			c92 - produtor rural ou segurado especial e c1h = PJ e que nao seja PAA
		7 - Comercializa��o da Produ��o Isenta de acordo com a Lei n� 13.606/2018;
		8 - Comercializa��o da Produ��o da Pessoa F�sica/Segurado Especial para Entidade inscrita no Programa de Aquisi��o de Alimentos - PAA;
   			c92 - produtor rural ou segurado especial e c1h = paa
		9 - Comercializa��o da Produ��o no Mercado Externo.
    		c92 - produtor rural e c1h-exterior
		*/
		IF (cTpEstab = "2") .AND. ((cTemp)->C1H_PPES = "1")
			cTipoPart := "2"
		ElseIf (cTpEstab = "1") .AND. ((cTemp)->C1H_RAMO = "4") .AND. ((cTemp)->C1H_PPES = "1")
			cTipoPart := "2"
		ElseIf (cTpEsInsc = "3") .AND.  (ctipoPArt = "2" .OR. cTipoPart = "3") .AND. ((cTemp)->C1H_PPES = "2" .AND. !(cTemp)->C1H_PAA="1")
			cTipoPart := "3"
		ElseIf (cTipoPart = "3" .AND. cTpEsInsc = "3" ) .AND. ((cTemp)->C1H_PAA="1")
			cTipoPart := "8"
		ElseIf ((cTemp)->C1H_RAMO = "4") .AND. ((cTemp)->C1H_PPES = "2") .AND. ((cTemp)->C1H_CODPAI <> "000001") // 000001 - BRASIL
			cTipoPart := "9"			
		Else
			cTipoPart := "7"
		EndIf

		If nT1N > 1
			oModel:GetModel( 'MODEL_T1N' ):AddLine()
		EndIf

		oModel:LoadValue( "MODEL_T1N", "T1N_INDCOM", cTipoPart )
		oModel:LoadValue( "MODEL_T1N", "T1N_VLRTOT", (cTemp)->VALTOT )


		( cTemp )->( dbSkip() )
		nT1N++
	Enddo

	(cTemp)->( DBCloseArea() )
	oModel:GetModel( 'MODEL_T1N' ):GoLine( 1 )


Return(Nil)


//-------------------------------------------------------------------
/*/{Protheus.doc} ImportT1O

Fun��o respons�vel por realizar importa��o de Notas Fiscais para
Consumidor / Comerciante Rural.
Eventos : S-2160

@author Ricardo
@since 23/09/2017
@version 1.0
/*/
//-------------------------------------------------------------------

Function ImportT1O(oModel,cPerApur)

Local oModelT1O 	 
Local cTemp			 
Local cArea			 
Local cQtdRet		 
Local cQuery		 
Local cTpInscr		 
Local cInscrPr		 
Local cTipoDoc		 
Local cNumeDoc		 
Local lRet			 
Local nT1O			 

Default cPerApur	:= ""

oModelT1O 			:= Nil
cTemp				:= GetNextAlias()
cArea				:= ""
cQtdRet				:= ""
cQuery				:= ""
cTpInscr			:= ""
cInscrPr			:= ""
cTipoDoc			:= ""
cNumeDoc			:= ""


oModelT1O := oModel:GetModel( 'MODEL_T1O' )


	cQuery	+=	"SELECT "
	cQuery	+=	"C1H.C1H_CNPJ,"
	cQuery	+=	"C1H.C1H_CPF,"
	cQuery	+=	"SUM(C20.C20_VLDOC) AS VALTOT "
	cQuery	+=	"FROM " + RetSqlName( 'C20' ) + " C20 "
	cQuery	+=	"JOIN " + RetSqlName( 'C1H' ) + " C1H "
	cQuery  +=  " ON(C1H.C1H_FILIAL='" + xFilial('C1H') + "' AND "
	cQuery  +=  	"C1H.C1H_ID = C20.C20_CODPAR AND "
	cQuery  +=		"C1H.C1H_RAMO = '4' AND C1H.D_E_L_E_T_=' ') "
	cQuery	+=	"WHERE  "
	cQuery	+=	"C20.C20_FILIAL='" + xFilial('C20') + "' AND "
	cQuery	+=	"C20.C20_INDOPE ='1' AND " //Sa�da
	cQuery	+=	"SUBSTRING(C20.C20_DTDOC,1,6) >='" + cPerApur + "' AND "
	cQuery	+=	"SUBSTRING(C20.C20_DTDOC,1,6) <='" + cPerApur + "' AND "
	cQuery	+=	"C20.D_E_L_E_T_=' ' "
	cQuery	+=  "GROUP BY C1H.C1H_CNPJ, C1H.C1H_CPF"

	// Executa a Query.
	DBUseArea( .T., "TOPCONN", TCGenQry( ,, ChangeQuery( cQuery ) ), cTemp, .F., .T. )

	nT1O := 1
	While ( cTemp )->( !Eof() )

		//Refazer esta l�gica  - 1= CNPJ, 2= CPF, 3= CAEPF, 4= CNO
		If !EMPTY((cTemp)->C1H_CNPJ)
			cTipoDoc := "1"
			cNumeDoc := (cTemp)->C1H_CNPJ
		Else
			cTipoDoc := "2"
			cNumeDoc := (cTemp)->C1H_CPF
		End

		If nT1O > 1
			oModel:GetModel( 'MODEL_T1O' ):AddLine()
		EndIf
		oModel:LoadValue( "MODEL_T1O", "T1O_TPINSA", '1'/*cTipoDoc*/	)
		oModel:LoadValue( "MODEL_T1O", "T1O_NRINSA", cNumeDoc	)
		oModel:LoadValue( "MODEL_T1O", "T1O_VLRCOM", (cTemp)->VALTOT )

		( cTemp )->( dbSkip() )
		nT1O++
	Enddo

	(cTemp)->( DBCloseArea() )
	oModel:GetModel( 'MODEL_T1O' ):GoLine( 1 )

Return(Nil)

//-------------------------------------------------------------------
/*/{Protheus.doc} ImportT6B

Fun��o respons�vel por realizar importa��o de Notas Fiscais para
Consumidor / Comerciante Rural.
Eventos : S-2150 e S-2160

@author Ricardo
@since 09/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------

Function ImportT6B(oModel,cPerApur)

Local oModelT6B 	
Local cTemp			 
Local cArea			 
Local cQtdRet		 
Local cQuery		 
Local cTipoPart		 
Local lRet			 
Local nT6B			 

Default cPerApur	:= ""

oModelT6B 			:= Nil
cTemp				:= GetNextAlias()
cArea				:= ""
cQtdRet				:= ""
cQuery				:= ""
cTipoPart			:= "1"


oModelT6B := oModel:GetModel( 'MODEL_T6B' )


	cQuery	+=	"SELECT "
	cQuery	+=	"C20.C20_SERIE, "
	cQuery	+=	"C20.C20_NUMDOC, "
	cQuery	+=	"C20.C20_DTDOC, "
	cQuery	+=	"C20.C20_DTES, "
	cQuery	+=	"C20.C20_VLDOC, "
	cQuery	+=	"(SELECT SUM(TRIB.C35_VALOR) FROM " + RetSqlName( 'C35' ) + " TRIB, " + RetSqlName( 'C3S' ) + " C3S WHERE C3S.C3S_FILIAL='" + xFilial('C3S') + "' AND TRIB.C35_CODTRI=C3S.C3S_ID AND C3S.D_E_L_E_T_=' ' AND C3S.C3S_CODIGO='24' AND TRIB.C35_FILIAL=C20.C20_FILIAL AND TRIB.C35_CHVNF=C20.C20_CHVNF AND TRIB.D_E_L_E_T_=' ') GILRAT, "
	cQuery	+=	"(SELECT SUM(TRIB.C35_VALOR) FROM " + RetSqlName( 'C35' ) + " TRIB, " + RetSqlName( 'C3S' ) + " C3S WHERE C3S.C3S_FILIAL='" + xFilial('C3S') + "' AND TRIB.C35_CODTRI=C3S.C3S_ID AND C3S.D_E_L_E_T_=' ' AND C3S.C3S_CODIGO='25' AND TRIB.C35_FILIAL=C20.C20_FILIAL AND TRIB.C35_CHVNF=C20.C20_CHVNF AND TRIB.D_E_L_E_T_=' ') SENAR "
	cQuery	+=	"FROM " + RetSqlName( 'C20' ) + " C20 "
	cQuery	+=	"JOIN " + RetSqlName( 'C1H' ) + " C1H "
	cQuery  +=  " ON(C1H.C1H_FILIAL='" + xFilial('C1H') + "' AND "
	cQuery  +=  	"C1H.C1H_ID = C20.C20_CODPAR AND "
	cQuery  +=  	"C1H.C1H_RAMO = '4' AND C1H.D_E_L_E_T_=' ') "
	cQuery	+=	"WHERE  "
	cQuery	+=	"C20.C20_FILIAL='" + xFilial('C20') + "' AND "
	cQuery	+=	"C20.C20_INDOPE ='1' AND " //Sa�da
	cQuery	+=	"SUBSTRING(C20.C20_DTDOC,1,6) >='" + cPerApur + "' AND "
	cQuery	+=	"SUBSTRING(C20.C20_DTDOC,1,6) <='" + cPerApur + "' AND "
	cQuery	+=	"C20.D_E_L_E_T_=' '"

	// Executa a Query.
	DBUseArea( .T., "TOPCONN", TCGenQry( ,, ChangeQuery( cQuery ) ), cTemp, .F., .T. )

	nT6B := 1
	While ( cTemp )->( !Eof() )

		If nT6B > 1
			oModel:GetModel( 'MODEL_T6B' ):AddLine()
		EndIf
		oModel:LoadValue( "MODEL_T6B", "T6B_SERIE",  ALLTRIM((cTemp)->C20_SERIE))
		oModel:LoadValue( "MODEL_T6B", "T6B_NUMDOC", ALLTRIM((cTemp)->C20_NUMDOC))
		oModel:LoadValue( "MODEL_T6B", "T6B_DTEMIS", STOD((cTemp)->C20_DTDOC))
		oModel:LoadValue( "MODEL_T6B", "T6B_VLBRUT", (cTemp)->C20_VLDOC )
		oModel:LoadValue( "MODEL_T6B", "T6B_VLCONT", (cTemp)->C20_VLDOC )
		oModel:LoadValue( "MODEL_T6B", "T6B_VLGILR", (cTemp)->GILRAT )
		oModel:LoadValue( "MODEL_T6B", "T6B_VLSENA", (cTemp)->SENAR )

		( cTemp )->( dbSkip() )
		nT6B++
	Enddo

	(cTemp)->( DBCloseArea() )
	oModel:GetModel( 'MODEL_T6B' ):GoLine( 1 )

Return(Nil)


//-------------------------------------------------------------------
/*/{Protheus.doc} SearchSM0

Fun��o respons�vel por localizar C�digo de Empresa e Filial do CGC
selecionado

Eventos : S-2150 e S-2160

@author Ricardo
@since 09/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function SearchSM0(cCGC)
Local lOk		 
Local aRet  	 
Local aAreaSM0 	 

Default cCGC := ""

aAreaSM0 := {}
aAreaSM0 := SM0->( getArea() )

lOk	:= .F.
aRet := {}

If !EMPTY(cCGC)
	//Procura Empresa/Filial com as informa��es passadas no Model T1M
	SM0->(DBGOTOP())

	While !SM0->(Eof()) .And. !lOk
		If !Empty(cCGC)
			If AllTrim(SM0->M0_CGC) == AllTrim(cCGC)

				lOk	:= .T.
				aadd(aRet,{SM0->M0_CODIGO,SM0->M0_CODFIL})

			EndIf
		EndIf
		SM0->(DbSkip())
	EndDo
EndIf

RestArea( aAreaSM0 )

Return(aRet)