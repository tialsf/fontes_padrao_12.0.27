#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TAFA426.CH'

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA426
Informa��es do IRRF Consolidadas por contribuinte
Pai/Filho em MVC

@author Daniel Schmidt
@since 29/05/2017
@version P11
/*/
//-------------------------------------------------------------------
Function TAFA426()

Private oBrowse := Nil

//Fun��o que indica se o ambiente � v�lido para o eSocial 2.3
If TafAtualizado()

	oBrowse := FWmBrowse():New()
	oBrowse:SetAlias( 'T0G' )
	oBrowse:SetDescription( STR0006 )

	If FindFunction('TAFSetFilter')
		oBrowse:SetFilterDefault(TAFBrwSetFilter("T0G","TAFA426","S-5012"))
	Else
		oBrowse:SetFilterDefault( "T0G_ATIVO == '1'" ) //Filtro para que apenas os registros ativos sejam exibidos ( 1=Ativo, 2=Inativo )
	EndIf

	oBrowse:AddLegend( "T0G_EVENTO == 'I' ", "GREEN" 	, STR0004 ) //"Registro Inclu�do"
	oBrowse:AddLegend( "T0G_EVENTO == 'A' ", "YELLOW" 	, STR0008 ) //"Registro Alterado"
	oBrowse:AddLegend( "T0G_EVENTO == 'E' ", "RED"   	, STR0005 ) //"Registro Exclu�do"
	oBrowse:Activate()

EndIf

Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Daniel Schmidt
@since 29/05/2017
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao  
Local aRotina  

aFuncao := {}
aRotina := {}

Aadd( aFuncao, { "" , "TAF426Xml" , "1" } )
Aadd( aFuncao, { "" , "xFunHisAlt( 'T0G' , 'TAFA426' )" , "3" } )
Aadd( aFuncao, { "" , "TAFXmlLote( 'T0G', 'S-5012' , 'evtIrrf' , 'TAF426Xml', , oBrowse )" , "5" } )
Aadd( aFuncao, { "" , "xFunAltRec( 'T0G' )" , "10" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If lMenuDif
	ADD OPTION aRotina Title "Visualizar" Action 'VIEWDEF.TAFA426' OPERATION 2 ACCESS 0 //"Visualizar"
Else
	aRotina	:=	xFunMnuTAF( "TAFA426" , , aFuncao)

	nPosDel	:=	aScan( aRotina , { | aX | AllTrim( aX[ 1 ] ) == "Exibir Hist�rico de Altera��es" } )
	If nPosDel > 0
		aDel( aRotina , nPosDel )
		aSize( aRotina , Len( aRotina ) - 1 )
	EndIf

EndIf

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc}  ModelDef
Funcao generica MVC do model

@author Daniel Schmidt
@since 29/05/2017
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function ModelDef()

// Cria a estrutura a ser usada no Modelo de Dados
Local oStruT0G 
Local oStruT0H 
Local oModel   

oStruT0G := FWFormStruct( 1, 'T0G', /*bAvalCampo*/, /*lViewUsado*/ )
oStruT0H := FWFormStruct( 1, 'T0H', /*bAvalCampo*/, /*lViewUsado*/ )
oModel   := Nil

// Cria o objeto do Modelo de Dados
oModel := MPFormModel():New( 'TAFA426', /*bPreValidacao*/, /*bPosValidacao*/, {|oModel| SaveModel(oModel)}, /*bCancel*/ )

// Modelo de Informa��es relativas ao Imposto de Renda Retido na Fonte.
oModel:AddFields( 'MODEL_T0G', /*cOwner*/, oStruT0G )

// Informa��es consolidadas do IRRF por c�digo de Receita - CR. Origem S-5002. (GRID)
oModel:AddGrid( 'MODEL_T0H', 'MODEL_T0G', oStruT0H, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
oModel:GetModel( 'MODEL_T0H' ):SetDescription( STR0007 )	// Adiciona a descricao do Modelo de Dados
oModel:GetModel( 'MODEL_T0H' ):SetUniqueLine( { 'T0H_IDCODR' } )						// Liga o controle de nao repeticao de linha
oModel:GetModel( 'MODEL_T0H' ):SetOptional(.T.)											// Indica que � opcional ter dados informados na Grid
oModel:GetModel( 'MODEL_T0H' ):SetMaxLine(9)												// Limita o numero m�ximo de linha da Grid

// Faz relaciomaneto entre os compomentes do model
oModel:SetRelation( 'MODEL_T0H', { { 'T0H_FILIAL', 'xFilial( "T0H" )' }, { 'T0H_ID', 'T0G_ID' }, { 'T0H_PERAPU', 'T0G_PERAPU' }, { 'T0H_VERSAO', 'T0G_VERSAO' } }, T0H->( IndexKey( 1 ) ) )

oModel:GetModel('MODEL_T0G'):SetPrimaryKey({ 'T0G_PERAPU' })


Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@author Daniel Schmidt
@since 29/05/2017
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oStruT0Ga 	
Local oStruT0Gb 	
Local oStruT0Gc 	
Local oStruT0H 		
Local oModel   		
Local oView    		
Local cCmpFil		

oStruT0Ga	:= Nil
oStruT0Gb	:= Nil
oStruT0Gc	:= Nil
oStruT0H 	:= FWFormStruct( 2, 'T0H' )
oModel   	:= FWLoadModel( 'TAFA426' )
oView    	:= FWFormView():New()
cCmpFil	:= ""

// Define qual o Modelo de dados ser� utilizado
oView:SetModel( oModel )

//Campos do folder da Identifica��o do Evento
cCmpFil := 'T0G_ID|T0G_PERAPU|'
oStruT0Ga := FwFormStruct( 2, 'T0G', {|x| AllTrim( x ) + "|" $ cCmpFil } )

//Campos do folder das Informa��es relativas ao Imposto de Renda Retido na Fonte
cCmpFil := 'T0G_IDARQB|T0G_NRARQB|T0G_INDEXI|'
oStruT0Gb := FwFormStruct( 2, 'T0G', {|x| AllTrim( x ) + "|" $ cCmpFil } )

//Campos do folder do Protocolo
cCmpFil := 'T0G_PROTUL|'
oStruT0Gc := FwFormStruct( 2, 'T0G', {|x| AllTrim( x ) + "|" $ cCmpFil } )

If FindFunction("TafAjustRecibo")
	TafAjustRecibo(oStruT0Gc,"T0G")
EndIf

/*--------------------------------------------------------------------------------------------
									Esrutura da View
---------------------------------------------------------------------------------------------*/

oView:AddField( 'VIEW_T0Ga', oStruT0Ga, 'MODEL_T0G' )
oView:EnableTitleView( 'VIEW_T0Ga', STR0010 ) //Identifica��o do Evento

oView:AddField( 'VIEW_T0Gb', oStruT0Gb, 'MODEL_T0G' )
oView:EnableTitleView( 'VIEW_T0Gb', STR0011 ) //Informa��es do Imposto de Renda Retido na Fonte

oView:AddField( 'VIEW_T0Gc', oStruT0Gc, 'MODEL_T0G' )

If FindFunction("TafNmFolder")
	oView:EnableTitleView( 'VIEW_T0Gc', TafNmFolder("recibo",1) ) // "Recibo da �ltima Transmiss�o"
Else
	oView:EnableTitleView( 'VIEW_T0Gc', STR0012 ) //Protocolo da �ltima Transmiss�o
EndIf

oView:AddGrid(  'VIEW_T0H', oStruT0H, 'MODEL_T0H' )
oView:EnableTitleView("VIEW_T0H",STR0013) //Informa��es consolidadas do IRRF por c�digo de Receita - CR

/*-----------------------------------------------------------------------------------
								Estrutura do Folder
-------------------------------------------------------------------------------------*/
oView:CreateHorizontalBox( 'PAINEL_SUPERIOR', 100 )

oView:CreateFolder( 'FOLDER_SUPERIOR', 'PAINEL_SUPERIOR')

oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA01', STR0009 ) //Informa��es do IRRF

oView:CreateHorizontalBox( 'T0Ga'	,  	15 ,,, 'FOLDER_SUPERIOR', 'ABA01' )
oView:CreateHorizontalBox( 'T0Gb'	,  	15 ,,, 'FOLDER_SUPERIOR', 'ABA01' )
oView:CreateHorizontalBox( 'T0H'	,	70 ,,, 'FOLDER_SUPERIOR', 'ABA01' )

If FindFunction("TafNmFolder")
	oView:AddSheet('FOLDER_SUPERIOR', "ABA02", TafNmFolder("recibo") )   //"Numero do Recibo"
Else
	oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA02', STR0014 )  //"Protocolo de Transmiss�o"
EndIf

oView:CreateHorizontalBox( 'T0Gc'	, 100,,.T., 'FOLDER_SUPERIOR', 'ABA02' )

// Relaciona o ID da View com o "box" para exibicao
oView:SetOwnerView( 'VIEW_T0Ga',	'T0Ga' )
oView:SetOwnerView( 'VIEW_T0Gb',	'T0Gb' )
oView:SetOwnerView( 'VIEW_T0Gc',	'T0Gc' )
oView:SetOwnerView( 'VIEW_T0H',		'T0H'  )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif
	xFunRmFStr(@oStruT0Ga, 'T0G')
	xFunRmFStr(@oStruT0Gb, 'T2M')
	xFunRmFStr(@oStruT0Gc, 'T2M')
	//Remover Campos da tela
	oStruT0Ga:RemoveField(�'T0G_VERSAO' )
	oStruT0Ga:RemoveField(�'T0G_EVENTO' )
	oStruT0Ga:RemoveField(�'T0G_ATIVO' )
	oStruT0H:RemoveField(�'T0H_VERSAO' )
EndIf

Return oView

///-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@Param  oModel -> Modelo de dados

@Return .T.

@author Daniel Schmidt
@since 29/05/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel(oModel)

Local lRetorno		
Local nOperation 	
Local cLogOpe		
Local cLogOpeAnt	

lRetorno	 := .T.
nOperation   := oModel:GetOperation()

cLogOpe    := ""
cLogOpeAnt := ""

Begin Transaction

	If nOperation == MODEL_OPERATION_INSERT
		oModel:LoadValue( 'MODEL_T0G', 'T0G_VERSAO', xFunGetVer() )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_T0G', 'T0G_LOGOPE' , '2', '' )
		Endif

		FwFormCommit( oModel )

	ElseIf nOperation == MODEL_OPERATION_UPDATE

		If TafColumnPos( "T0G_LOGOPE" )
			cLogOpeAnt := T0G->T0G_LOGOPE
		endif

		If Findfunction("TAFAltMan")
			TAFAltMan( 4 , 'Save' , oModel, 'MODEL_T0G', 'T0G_LOGOPE' , '' , cLogOpeAnt )
		EndIf

		TAFAltStat( 'T0G', " " )
		FwFormCommit( oModel )

	ElseIf nOperation == MODEL_OPERATION_DELETE
		oModel:DeActivate()
		oModel:SetOperation( 5 )
		oModel:Activate()

		FwFormCommit( oModel )
	EndIf
End Transaction

Return ( lRetorno )


//-------------------------------------------------------------------
/*/{Protheus.doc} TAF426Grv

Funcao de gravacao para atender o registro S-5012

@Param:
cLayout - Nome do Layout que esta sendo enviado, existem situacoes onde o mesmo fonte
          alimenta mais de um regsitro do E-Social, para estes casos serao necessarios
          tratamentos de acordo com o layout que esta sendo enviado.
nOpc   -  Opcao a ser realizada ( 3 = Inclusao, 4 = Alteracao, 5 = Exclusao )
cFilEv -  Filial do ERP para onde as informacoes deverao ser importadas
oDados -  Objeto com as informacoes a serem manutenidas ( Outras Integracoes )

@Return
lRet    - Variavel que indica se a importacao foi realizada, ou seja, se as
		  informacoes foram gravadas no banco de dados
aIncons - Array com as inconsistencias encontradas durante a importacao

@author Daniel Schmidt
@since 29/05/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function TAF426Grv( cLayout, nOpc , cFilEv , oXML  )

Local cCmpsNoUpd	
Local nI			
Local nJ			
Local lRet			
Local aIncons		
Local aRules		
Local aChave		
Local oModel		
Local cCabec 		
Local cIdFunc 	
Local cInconMsg	
Local nSeqErrGrv	
Local cPeriodo	
Local cLogOpeAnt	

cCmpsNoUpd	:= "|T0G_FILIAL|T0G_ID|T0G_VERSAO|T0G_PROTUL|T0G_EVENTO|T0G_STATUS|T0G_ATIVO|"
nI			:= 0
nJ			:= 0
lRet		:= .F.
aIncons	:= {}
aRules		:= {}
aChave		:= {}
oModel		:= Nil
cCabec 	:= "/eSocial/evtIrrf/"
cIdFunc 	:= ""
cInconMsg	:= ""
nSeqErrGrv	:= 0
cPeriodo	:= ""
cLogOpeAnt 	:= ""

Private lVldModel := .T. //Caso a chamada seja via integracao seto a variavel de controle de validacao como .T.
Private oDados    := {}

Default cLayout   := ""
Default nOpc      := 1
Default cFilEv    := ""
Default oXML      := Nil

oDados := oXML

//Para o cadastro de complemento de empresa a chave de busca
//de informacoes na tabela eh o codigo da filial, nao se considera a
//data inicial e final constante no layout do E-Social

cPeriodo  := FTafGetVal(  cCabec + "ideEvento/perApur", "C", .F., @aIncons, .F. )

If At("-", cPeriodo) > 0
	Aadd( aChave, {"C", "T0G_PERAPU", StrTran(cPeriodo, "-", "" ),.T.} )
Else
	Aadd( aChave, {"C", "T0G_PERAPU", cPeriodo  , .T.} )
EndIf

Begin Transaction
	//Funcao para validar se a operacao desejada pode ser realizada
	If FTafVldOpe( 'T0G', 2 , @nOpc, cFilEv, @aIncons, aChave, @oModel, 'TAFA426', cCmpsNoUpd )

		If TafColumnPos( "T0G_LOGOPE" )
			cLogOpeAnt := T0G->T0G_LOGOPE
		endif

		//���������������������������������������������������������������Ŀ
		//�Carrego array com os campos De/Para de gravacao das informacoes�
		//�����������������������������������������������������������������
		aRules := TAF426Rul()

		//����������������������������������������������������������������Ŀ
		//�Quando se tratar de uma Exclusao direta apenas preciso realizar �
		//�o Commit(), nao eh necessaria nenhuma manutencao nas informacoes�
		//������������������������������������������������������������������
		If nOpc <> 5

			oModel:LoadValue( "MODEL_T0G", "T0G_FILIAL", T0G->T0G_FILIAL )

			//����������������������������������������Ŀ
			//�Rodo o aRules para gravar as informacoes�
			//������������������������������������������
			For nI := 1 to Len( aRules )
			 	oModel:LoadValue( "MODEL_T0G", aRules[ nI, 01 ], FTafGetVal( aRules[ nI, 02 ], aRules[nI, 03], aRules[nI, 04], @aIncons, .F. ) )
			Next nI

			If Findfunction("TAFAltMan")
				if nOpc == 3
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_T0G', 'T0G_LOGOPE' , '1', '' )
				elseif nOpc == 4
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_T0G', 'T0G_LOGOPE' , '', cLogOpeAnt )
				EndIf
			EndIf

			/*----------------------------------------
			T0H - Identifica��o Estabelecimento
			------------------------------------------*/

			nT0H := 1
			cT0HPath := cCabec + "infoIRRF/infoCRContrib[" + CVALTOCHAR(nT0H) + "]"

			If nOpc == 4 .And. oDados:XPathHasNode( cT0HPath )
				For nJ := 1 to oModel:GetModel( 'MODEL_T0H' ):Length()
					oModel:GetModel( 'MODEL_T0H' ):GoLine(nJ)
					oModel:GetModel( 'MODEL_T0H' ):DeleteLine()
				Next nJ
			EndIf

			nT0H := 1
			While oDados:XPathHasNode(cT0HPath)

				If nOpc == 4 .Or. nT0H > 1
					oModel:GetModel( 'MODEL_T0H' ):LVALID	:= .T.
					oModel:GetModel( 'MODEL_T0H' ):AddLine()
			    EndIf

				oModel:LoadValue( "MODEL_T0H", "T0H_IDCODR", FGetIdInt( "tpCR","",cT0HPath + "/tpCR",,,,@cInconMsg, @nSeqErrGrv))
				oModel:LoadValue( "MODEL_T0H", "T0H_VRCOCR", FTafGetVal( cT0HPath + "/vrCR" , "N", .F., @aIncons, .F. ) )

				nT0H++
				cT0HPath := cCabec + "infoIRRF/infoCRContrib[" + CVALTOCHAR(nT0H) + "]"
			EndDo

		EndIf

		//���������������������������Ŀ
		//�Efetiva a operacao desejada�
		//�����������������������������
		If Empty(cInconMsg)
			If TafFormCommit( oModel )
				Aadd(aIncons, "ERRO19")
			Else
				lRet := .T.
			EndIf
		Else
			Aadd(aIncons, cInconMsg)
		EndIf

		oModel:DeActivate()

		// Atualiza o status do registro - utiliza RecLock, pois o setvalue somente funciona em campos usados
		/*DBSelectArea("T0G")
		T0G->(DbSetOrder( 1 ))

		If RecLock("T0G",.F.)
			T0G->T0G_STATUS := "4"
			T0G->( MsUnlock() )
		EndIf*/
	Endif
End Transaction

//����������������������������������������������������������Ŀ
//�Zerando os arrays e os Objetos utilizados no processamento�
//������������������������������������������������������������
aSize( aRules, 0 )
aRules := Nil

aSize( aChave, 0 )
aChave := Nil

Return { lRet, aIncons }

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF426Rul

Regras para gravacao dos Imposto de Renda Retido na Fonte S-5012 do E-Social

@Param

@Return
aRull - Regras para a gravacao das informacoes

@author Daniel Schmidt
@since 29/05/2017
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function TAF426Rul()

Local aRull		
Local cCabec 		
Local cPeriodo	

aRull		:= {}
cCabec 	:= "/eSocial/evtIrrf/"
cPeriodo 	:= FTafGetVal("/eSocial/evtIrrf/ideEvento/perApur", "C", .F.,, .F. )

If At("-", cPeriodo) > 0
	Aadd( aRull, {"T0G_PERAPU", StrTran(cPeriodo, "-", "" ) ,"C",.T.} )
Else
	Aadd( aRull, {"T0G_PERAPU", cPeriodo ,"C", .T.} )
EndIf

Aadd( aRull, {"T0G_NRARQB",  FTafGetVal(cCabec + "infoIRRF/nrRecArqBase", "C", .F.,, .F. ), "C", .T. } )
Aadd( aRull, {"T0G_INDEXI",  FTafGetVal(cCabec + "infoIRRF/indExistInfo", "C", .F.,, .F. ), "C", .T. } )

Return( aRull )


//-------------------------------------------------------------------
/*/{Protheus.doc} TAF426Xml
Funcao de geracao do XML para atender o registro S-1200
Quando a rotina for chamada o registro deve estar posicionado

@Param:

@Return:
cXml - Estrutura do Xml do Layout S-5012

@author Daniel Schmidt
@since 29/05/2017
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAF426Xml(cAlias, nRecno, nOpc, lJob )

Local cXml		 	
Local cLayout	 	
Local cReg		 	
Local aMensal	 	

cXml		 	:= ""
cLayout	 	:= "5012"
cReg		 	:= "Irrf"
aMensal	 	:= {}

Default cAlias := "T0G"

If Len(Alltrim(T0G->T0G_PERAPU)) <= 4
	AADD(aMensal,T0G->T0G_PERAPU)
Else
	AADD(aMensal,substr(T0G->T0G_PERAPU, 1, 4) + '-' + substr(T0G->T0G_PERAPU, 5, 2) )
EndIf

cXml +=	"<infoIRRF>"
cXml +=		xTafTag("nrRecArqBase",T0G->T0G_NRARQB,,.T.)
cXml +=		xTafTag("indExistInfo",T0G->T0G_INDEXI)

T0H->( DbSetOrder( 1 ) )
If T0H->( MsSeek ( xFilial("T0H")+T0G->(T0G_ID+T0G_VERSAO)))
	While !T0H->(Eof()) .And. AllTrim(T0H->(T0H_ID+T0H_VERSAO)) == AllTrim(T0G->(T0G_ID+T0G_VERSAO))

		cXml +=	"<infoCRContrib>"
		cXml +=		xTafTag("tpCR",Posicione("C6R",3,xFilial("C6R")+T0H->T0H_IDCODR,"C6R_CODIGO"))
		cXml +=		xTafTag("vrCR",T0H->T0H_VRCOCR,PesqPict("T0H","T0H_VRCOCR"))
		cXml +=	"</infoCRContrib>"

	T0H->(DbSkip())
	EndDo
EndIf

cXml +=	"</infoIRRF>"

//����������������������Ŀ
//�Estrutura do cabecalho�
//������������������������
cXml := xTafCabXml(cXml,"T0G",cLayout,cReg,aMensal)

//����������������������������Ŀ
//�Executa gravacao do registro�
//������������������������������
If !lJob
	xTafGerXml(cXml,cLayout)
EndIf

Return(cXml)

