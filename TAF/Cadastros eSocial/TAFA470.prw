#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TAFA470.CH'
#INCLUDE "TAFA407.CH"

Static lTAFCodRub	:= FindFunction("TAFCodRub")
Static slRubERPPad := Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA470
Cadastro MVC para atender o registro S-1207 (Benef�cios previdenci�rios - RPPS) do e-Social.

@author denis.oliveira
@since 13/03/2017
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAFA470()
			
Local cTitulo		 
Local cMensagem	

cTitulo	:= ""
cMensagem	:= ""

Private oBrw := FWmBrowse():New()

// Vari�vel que indica se o ambiente � v�lido para o eSocial
If !TafVldAmb("2")

	cTitulo :=  STR0006 // #"Dicion�rio Incompat�vel"
	cMensagem := TafAmbInvMsg()
	
	Aviso( cTitulo, cMensagem, { STR0007 }, 3 ) // #"Encerrar"
	
Else

	oBrw:SetDescription( STR0001 ) //Benef�cios previdenci�rios - RPPS
	oBrw:SetAlias( 'T62')
	oBrw:SetMenuDef( 'TAFA470' )
	
	If FindFunction('TAFSetFilter')
		oBrw:SetFilterDefault(TAFBrwSetFilter("T62","TAFA470","S-1207"))
	Else
		oBrw:SetFilterDefault( "T62_ATIVO == '1'" ) //Filtro para que apenas os registros ativos sejam exibidos ( 1=Ativo, 2=Inativo )
	EndIf

	TafLegend(2,"T62",@oBrw)
	
	oBrw:Activate()

EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc}MenuDef                                     
Funcao generica MVC com as opcoes de menu

@author denis.oliveira
@since 13/03/2017
@version 1.0

/*/
//-------------------------------------------------------------------                                                                                            
Static Function MenuDef()

Local aFuncao 
Local aRotina 

aFuncao := {}
aRotina := {}
If FindFunction('TafXmlRet')
	Aadd( aFuncao, { "" , "TafxmlRet('TAF470Xml','1207','T62')" , "1" } )
Else 
	Aadd( aFuncao, { "" , "TAF470Xml" , "1" } )
EndIf

Aadd( aFuncao, { "" , "TAF470Vld" , "2" } )
Aadd( aFuncao, { "" , "xFunHisAlt( 'T62', 'TAFA470' ,,,, 'TAF470XML','1207'  )" , "3" } )
Aadd( aFuncao, { "" , "TAFXmlLote( 'T62', 'S-1207' , 'evtBenPrRP' , 'TAF470Xml',, oBrw )" , "5" } )
Aadd( aFuncao, { "" , "xFunAltRec( 'T62' )" , "10" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If lMenuDif
	ADD OPTION aRotina Title "Visualizar" Action 'VIEWDEF.TAFA470' OPERATION 2 ACCESS 0
Else
	aRotina	:=	xFunMnuTAF( "TAFA470" , , aFuncao)
EndIf

Return( aRotina )      
//-------------------------------------------------------------------
/*/{Protheus.doc}  ModelDef
Funcao generica MVC do model

@author denis.oliveira
@since 13/03/2017
@version 1.0

/*/
//-------------------------------------------------------------------     
Static Function ModelDef()	

Local oStruT62 	:= Nil
Local oStruT63 	:= Nil
Local oStruT6O	:= Nil
Local oModel		:= Nil

oStruT62 := FWFormStruct( 1, 'T62' )// Cria a estrutura a ser usada no Modelo de Dados
oStruT63 := FWFormStruct( 1, 'T63' )
oStruT6O := FWFormStruct( 1, 'T6O' )

oModel	:= MPFormModel():New('TAFA470',,,{|oModel| SaveModel(oModel)} )

lVldModel := Iif( Type( "lVldModel" ) == "U", .F., lVldModel )

If lVldModel
	oStruT62:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel }) 	
	oStruT63:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel }) 
	oStruT6O:SetProperty( "*", MODEL_FIELD_VALID, {|| lVldModel }) 	
EndIf

//Remo��o do GetSX8Num quando se tratar da Exclus�o de um Evento Transmitido.
//Necess�rio para n�o incrementar ID que n�o ser� utilizado.
If Upper( ProcName( 2 ) ) == Upper( "GerarExclusao" )
	oStruT62:SetProperty( "T62_ID", MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD, "" ) )
EndIf

// Adiciona ao modelo um componente de formul�rio
oModel:AddFields( 'MODEL_T62', /*cOwner*/, oStruT62)

oModel:AddGrid('MODEL_T63', 'MODEL_T62',oStruT63)  
oModel:GetModel('MODEL_T63'):SetOptional( .T. )
oModel:GetModel('MODEL_T63'):SetUniqueLine({'T63_IDBENE'})
oModel:GetModel('MODEL_T63'):SetMaxLine(99)

oModel:AddGrid('MODEL_T6O', 'MODEL_T63', oStruT6O)
oModel:GetModel('MODEL_T6O'):SetOptional(.T.)
oModel:GetModel('MODEL_T6O'):SetUniqueLine({'T6O_IDRUBR'})
oModel:GetModel('MODEL_T6O'):SetMaxLine(99)

/*--------------------------------------------------------
        Abaixo realiza-se a amarra��o das tabelas
----------------------------------------------------------*/
// Define a chave �nica de grava��o das informa��es
oModel:GetModel( 'MODEL_T62' ):SetPrimaryKey( { 'T62_FILIAL' , 'T62_ID', 'T62_VERSAO' } )

oModel:SetRelation( 'MODEL_T63', {{'T63_FILIAL',"xFilial('T63')" },{'T63_ID','T62_ID'},{'T63_VERSAO','T62_VERSAO'},{'T63_INDAPU','T62_INDAPU'},{'T63_PERAPU','T62_PERAPU'},{'T63_CPF','T62_CPF'}}, T63->( IndexKey(1)))
oModel:SetRelation( 'MODEL_T6O', {{'T6O_FILIAL',"xFilial('T6O')" },{'T6O_ID','T62_ID'},{'T6O_VERSAO','T62_VERSAO'},{'T6O_INDAPU','T62_INDAPU'},{'T6O_PERAPU','T62_PERAPU'},{'T6O_CPF','T62_CPF'},{'T6O_IDBENE','T63_IDBENE'} }, T6O->( IndexKey(1)))
    
                                                                                                                                                                         
Return oModel             

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@author denis.oliveira
@since 13/03/2017
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oModel		:= Nil
Local oStruT62	:= Nil
Local oStruT63	:= Nil
Local oStruT6O	:= Nil

Local oView		:= Nil

oModel		:= FWLoadModel( 'TAFA470' )// objeto de Modelo de dados baseado no ModelDef() do fonte informado
oStruT62	:= FWFormStruct( 2, 'T62' )// Cria a estrutura a ser usada na View
oStruT63	:= FWFormStruct( 2, 'T63' )
oStruT6O	:= FWFormStruct( 2, 'T6O' )

oView		:= FWFormView():New()

oView:SetModel( oModel )
oView:SetContinuousForm()

If FindFunction("TafAjustRecibo")
	TafAjustRecibo(oStruT62,"T62")
EndIf

/*-------------------------------------------
		     Esrutura da View
---------------------------------------------*/
// Cabe�alho
oView:AddField( 'VIEW_T62',oStruT62, 'MODEL_T62' )
oView:EnableTitleView( 'VIEW_T62',  STR0001 ) //Benef�cios previdenci�rios - RPPS

// Grids
oView:AddGrid( 'VIEW_T63', oStruT63, 'MODEL_T63' )
oView:EnableTitleView( 'VIEW_T63',  STR0008 ) //Demonstrativos de Valores

oView:AddGrid( 'VIEW_T6O', oStruT6O, 'MODEL_T6O' )
oView:EnableTitleView( 'VIEW_T6O',  STR0009 ) //Detalhamento dos valores devidos ao benefici�rio


/*-----------------------------------------
		   Estrutura do Folder
-------------------------------------------*/
oView:CreateHorizontalBox( 'FIELDST62', 20 )
oView:CreateHorizontalBox( 'GRIDT63',   40 )
oView:CreateHorizontalBox( 'GRIDT6O',   40 )


/*-----------------------------------------
  Amarra��o para exibi��o das informa��es
-------------------------------------------*/
oView:SetOwnerView( 'VIEW_T62', 'FIELDST62' )
oView:SetOwnerView( 'VIEW_T63', 'GRIDT63' ) 
oView:SetOwnerView( 'VIEW_T6O', 'GRIDT6O' ) 

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If !lMenuDif
	
	xFunRmFStr(@oStruT62, 'T62')	
	xFunRmFStr(@oStruT63, 'T63')
	xFunRmFStr(@oStruT6O, 'T6O')
	
EndIf

If TafColumnPos( "T62_LOGOPE" )
	oStruT62:RemoveField( "T62_LOGOPE" )
EndIf

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo

@param  oModel -> Modelo de dados
@return .T.

@author denis.oliveira
@since 08/10/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel(oModel)
                     
Local cVerAnt   	
Local cProtocolo	
Local cVersao   	
Local cEvento		
Local cChvRegAnt	
Local cLogOpe		
Local cLogOpeAnt	

Local nOperation	
Local nlI			
Local nlY			
Local nT62			
Local nT63			
Local nT6O			
Local nT6OAdd		

Local aGravaT62	
Local aGravaT63 	
Local aGravaT6O	

Local oModelT62 	
Local oModelT63 	
Local oModelT6O 	

Local lRetorno  	

cVerAnt   	:= ""  
cProtocolo	:= ""
cVersao	:= ""
cEvento	:= ""  
cChvRegAnt	:= ""
cLogOpe    	:= ""
cLogOpeAnt 	:= ""

nOperation	:= oModel:GetOperation()
nT62		:= 0  
nT63		:= 0   
nT6O		:= 0  
nT6OAdd	:= 0
nlI			:= 0
nlY			:= 0

aGravaT62	:= {}
aGravaT63 	:= {}
aGravaT6O 	:= {}

oModelT62 	:= Nil
oModelT63 	:= Nil
oModelT6O 	:= Nil

lRetorno  	:= .T.


Begin Transaction 
	
	//Inclusao Manual do Evento
	If nOperation == MODEL_OPERATION_INSERT

		TafAjustID(�"T62", oModel)
		
		oModel:LoadValue( 'MODEL_T62', 'T62_VERSAO', xFunGetVer() )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_T62', 'T62_LOGOPE' , '2', '' )
		Endif

		FwFormCommit( oModel )

	//Altera��o Manual do Evento
	ElseIf nOperation == MODEL_OPERATION_UPDATE

		//�����������������������������������������������������������������Ŀ
		//�Seek para posicionar no registro antes de realizar as validacoes,�
		//�visto que quando nao esta pocisionado nao eh possivel analisar   �
		//�os campos nao usados como _STATUS                                �
		//�������������������������������������������������������������������
	    T62->( DbSetOrder( 3 ) )
	    If T62->( MsSeek( xFilial( 'T62' ) + FwFldGet('T62_ID')+ '1' ) )
	    	       	    	    
			//��������������������������������Ŀ
			//�Se o registro ja foi transmitido�
			//����������������������������������
		    If T62->T62_STATUS == "4" 
							    
				oModelT62	:= oModel:GetModel( 'MODEL_T62' ) 
				oModelT63	:= oModel:GetModel( 'MODEL_T63' ) 
				oModelT6O	:= oModel:GetModel( 'MODEL_T6O' ) 
										
				//�����������������������������������������������������������Ŀ
				//�Busco a versao anterior do registro para gravacao do rastro�
				//�������������������������������������������������������������
				cVerAnt		:= oModelT62:GetValue( "T62_VERSAO" )
				cProtocolo	:= oModelT62:GetValue( "T62_PROTUL" )
				cEvento		:= oModelT62:GetValue( "T62_EVENTO" )

				If TafColumnPos( "T62_LOGOPE" )
					cLogOpeAnt := oModelT62:GetValue( "T62_LOGOPE" )
				endif

				//�����������������������������������������������������������������Ŀ
				//�Neste momento eu gravo as informacoes que foram carregadas       �
				//�na tela, pois neste momento o usuario ja fez as modificacoes que �
				//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
				//�nao devem ser consideradas neste momento                         �
				//�������������������������������������������������������������������
				For nlI := 1 To 1
					For nlY := 1 To Len( oModelT62:aDataModel[ nlI ] )			
						Aadd( aGravaT62, { oModelT62:aDataModel[ nlI, nlY, 1 ], oModelT62:aDataModel[ nlI, nlY, 2 ] } )									
					Next
				Next	       						
				
				For nT63 := 1 To oModel:GetModel( 'MODEL_T63' ):Length()
					oModel:GetModel( 'MODEL_T63' ):Goline(nT63) 
		
					If !oModel:GetModel( 'MODEL_T63' ):IsDeleted()
						aAdd (aGravaT63, {oModelT63:GetValue('T63_IDBENE'),;
						    		       oModelT63:GetValue('T63_DEMPAG') }) 
						
						For nT6O := 1 To oModel:GetModel( 'MODEL_T6O' ):Length() 
							oModel:GetModel( 'MODEL_T6O' ):GoLine(nT6O)
							
							If !oModel:GetModel( 'MODEL_T6O' ):IsDeleted()
								aAdd (aGravaT6O, {oModelT63:GetValue('T63_IDBENE') ,;
												  oModelT6O:GetValue('T6O_IDRUBR') ,; 
							                 	  oModelT6O:GetValue('T6O_VLRRUB') }) 
							EndIf		
						
						Next // Fim - T6O
					EndIf
				
				Next // Fim - T63

				//�����������������������������������������������������������Ŀ
				//�Seto o campo como Inativo e gravo a versao do novo registro�
				//�no registro anterior                                       � 
				//|                                                           |
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������
				FAltRegAnt( 'T62', '2' )
			
				//��������������������������������������������������Ŀ
				//�Neste momento eu preciso setar a operacao do model�
				//�como Inclusao                                     �
				//����������������������������������������������������
				oModel:DeActivate()
				oModel:SetOperation( 3 ) 	
				oModel:Activate()		
								
				//�������������������������������������������������������Ŀ
				//�Neste momento eu realizo a inclusao do novo registro ja�
				//�contemplando as informacoes alteradas pelo usuario     �
				//���������������������������������������������������������
				For nT62 := 1 to Len( aGravaT62 )
						oModel:LoadValue( "MODEL_T62", aGravaT62[ nT62, 1 ], aGravaT62[ nT62, 2 ] )
				Next nT62

				//Necess�rio Abaixo do For Nao Retirar
				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_T62', 'T62_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				For nT63 := 1 To Len( aGravaT63 )
					oModel:GetModel( 'MODEL_T63' ):LVALID	:= .T.
		
					If nT63 > 1
						oModel:GetModel( 'MODEL_T63' ):AddLine()
					EndIf
					oModel:LoadValue( "MODEL_T63", "T63_IDBENE", aGravaT63[nT63][1])	
					oModel:LoadValue( "MODEL_T63", "T63_DEMPAG", aGravaT63[nT63][2])	
							
					nT6OAdd := 1
					For nT6O := 1 To Len( aGravaT6O )
						// Grava apenas o T6O pertencente ao T63
						If aGravaT6O[nT6O][1] == aGravaT63[nT63][1]
							oModel:GetModel( 'MODEL_T6O' ):LVALID	:= .T.
		
							If nT6OAdd > 1
								oModel:GetModel( 'MODEL_T6O' ):AddLine()
							EndIf
							oModel:LoadValue( "MODEL_T6O", "T6O_IDRUBR", aGravaT6O[nT6O][2] )
							oModel:LoadValue( "MODEL_T6O", "T6O_VLRRUB", aGravaT6O[nT6O][3] )
		
							nT6OAdd++
						EndIf
			        Next // Fim - T6O
				
				Next // Fim - T63
				     
				//�������������������������������Ŀ
				//�Busco a versao que sera gravada�
				//���������������������������������
				cVersao := xFunGetVer()		 
				                                   
				//�����������������������������������������������������������Ŀ		
				//|ATENCAO -> A alteracao destes campos deve sempre estar     |
				//|abaixo do Loop do For, pois devem substituir as informacoes|
				//|que foram armazenadas no Loop acima                        |
				//�������������������������������������������������������������		                                                                      				         
				oModel:LoadValue( 'MODEL_T62', 'T62_VERSAO', cVersao )  
				oModel:LoadValue( 'MODEL_T62', 'T62_VERANT', cVerAnt )									          				    
				oModel:LoadValue( 'MODEL_T62', 'T62_PROTPN', cProtocolo )									          						
				oModel:LoadValue( 'MODEL_T62', 'T62_PROTUL', "" )									          				

				oModel:LoadValue( "MODEL_T62", "T62_EVENTO", "A" )
				// Tratamento para limpar o ID unico do xml
				cAliasPai := "T62"
				If TAFColumnPos( cAliasPai+"_XMLID" )
					oModel:LoadValue( 'MODEL_'+cAliasPai, cAliasPai+'_XMLID', "" )
				EndIf
				
				FwFormCommit( oModel )
				TAFAltStat( 'T62', " " ) 					

			ElseIf	T62->T62_STATUS == "2"                                                                 
				TAFMsgVldOp(oModel,"2")//"Registro n�o pode ser alterado. Aguardando processo da transmiss�o."
				lRetorno:= .F.
			ElseIf T62->T62_STATUS == "6"                                                                                                                                                                                                                                                                                                                               
				TAFMsgVldOp(oModel,"6")//"Registro n�o pode ser alterado. Aguardando proc. Transm. evento de Exclus�o S-3000"
				lRetorno:= .F.
			Elseif T62->T62_STATUS == "7"
				TAFMsgVldOp(oModel,"7") //"Registro n�o pode ser alterado, pois o evento j� se encontra na base do RET"  
				lRetorno:= .F.
			Else
				If TafColumnPos( "T62_LOGOPE" )
					cLogOpeAnt := T62->T62_LOGOPE
				endif

				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_T62', 'T62_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				FwFormCommit( oModel )
				TAFAltStat( 'T62', " " )  
			EndIf
		EndIf
		//Exclus�o Manual do Evento
	ElseIf nOperation == MODEL_OPERATION_DELETE	  

		cChvRegAnt := T62->(T62_ID + T62_VERANT)  	  

		If !Empty( cChvRegAnt ) 
			TAFAltStat( 'T62', " " )
			FwFormCommit( oModel )				
			If nOperation == MODEL_OPERATION_DELETE
				If T62->T62_EVENTO == "A" .Or. T62->T62_EVENTO == "E"
					TAFRastro( 'T62', 1, cChvRegAnt, .T. , , IIF(Type("oBrw") == "U", Nil, oBrw) )
				EndIf
			EndIf
		Else
			oModel:DeActivate()
			oModel:SetOperation( 5 ) 	
			oModel:Activate()
			FwFormCommit( oModel )
		EndIf

	EndIf

End Transaction 


Return ( lRetorno )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF470Grv
@type			function
@description	Fun��o de grava��o para atender o registro S-1207.
@author			denis.oliveira
@since			13/03/2017
@version		1.0
@param			cLayout		-	Nome do Layout que est� sendo enviado
@param			nOpc		-	Op��o a ser realizada ( 3 = Inclus�o, 4 = Altera��o, 5 = Exclus�o )
@param			cFilEv		-	Filial do ERP para onde as informa��es dever�o ser importadas
@param			oXML		-	Objeto com as informa��es a serem manutenidas ( Outras Integra��es )
@param			cOwner
@param			cFilTran
@param			cPredeces
@param			nTafRecno
@param			cComplem
@param			cGrpTran
@param			cEmpOriGrp
@param			cFilOriGrp
@param			cXmlID		-	Atributo Id, �nico para o XML do eSocial. Utilizado para importa��o de dados de clientes migrando para o TAF
@return			lRet		-	Vari�vel que indica se a importa��o foi realizada, ou seja, se as informa��es foram gravadas no banco de dados
@param			aIncons		-	Array com as inconsist�ncias encontradas durante a importa��o
/*/
//-------------------------------------------------------------------
Function TAF470Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID )

Local cCmpsNoUpd	:=	"|T62_FILIAL|T62_ID|T62_VERSAO|T62_VERANT|T62_PROTPN|T62_EVENTO|T62_STATUS|T62_ATIVO|"
Local cCabec		:=	"/eSocial/evtBenPrRP"
Local cMensagem		:=	""
Local cInconMsg		:=	""
Local cT63Path		:=	""
Local cT6OPath		:=	""
Local cEnter		:=	Chr( 13 ) + Chr( 10 )
Local cPeriodo		:=	""
Local cChave		:=	""
Local cCodEvent		:=	Posicione( "C8E", 2, xFilial( "C8E" ) + "S-" + cLayout, "C8E->C8E_ID" )
Local cLogOpeAnt	:=	""
Local nIndChv		:=	2
Local nIndIDVer		:=	1
Local nlI			:=	0
Local nlJ			:=	0
Local nlX			:=	0
Local nSeqErrGrv	:=	0
Local lRet			:=	.F.
Local aIncons		:=	{}
Local aRules		:=	{}
Local aChave		:=	{}
Local oModel		:=	Nil

Private lVldModel	:=	.T. //Caso a chamada seja via integra��o, seto a vari�vel de controle de valida��o como .T.
Private oDados		:=	Nil

Default cLayout		:=	""
Default nOpc		:=	1
Default cFilEv		:=	""
Default oXML		:=	Nil
Default cOwner		:=	""
Default cFilTran	:=	""
Default cPredeces	:=	""
Default nTafRecno	:=	0
Default cComplem	:=	""
Default cGrpTran	:=	""
Default cEmpOriGrp	:=	""
Default cFilOriGrp	:=	""
Default cXmlID		:=	""
// Vari�vel que indica se o ambiente � v�lido para o eSocial
If !TafVldAmb("2")
	cMensagem := STR0006 + cEnter // #"Dicion�rio Incompat�vel"
	cMensagem += TafAmbInvMsg()

	Aadd(aIncons, cMensagem)

Else

	oDados := oXML
	
	cPeriodo  := FTafGetVal( cCabec + "/ideEvento/perApur", "C", .F., @aIncons, .F. )
	
	//indApuracao
	Aadd( aChave, {"C", "T62_INDAPU", FTafGetVal( cCabec + "/ideEvento/indApuracao", "C", .F., @aIncons, .F. )  , .T.} ) 
	cChave += Padr( aChave[ 1, 3 ], Tamsx3( aChave[ 1, 2 ])[1])
	
	//perApur
	If At("-", cPeriodo) > 0
		Aadd( aChave, {"C", "T62_PERAPU", StrTran(cPeriodo, "-", "" ),.T.} )
		cChave += Padr( aChave[ 2, 3 ], Tamsx3( aChave[ 2, 2 ])[1])	
	Else
		Aadd( aChave, {"C", "T62_PERAPU", cPeriodo  , .T.} ) 
		cChave += Padr( aChave[ 2, 3 ], Tamsx3( aChave[ 2, 2 ])[1])		
	EndIf
	
	//indApuracao
	Aadd( aChave, {"C", "T62_CPF", FTafGetVal( cCabec + "/ideBenef/cpfBenef", "C", .F., @aIncons, .F. )  , .T.} ) 
	cChave += Padr( aChave[ 3, 3 ], Tamsx3( aChave[ 3, 2 ])[1])	
	
	//Verifica se o evento ja existe na base
	("T62")->( DbSetOrder( 2 ) )
	If ("T62")->( MsSeek( FTafGetFil( cFilEv , @aIncons , "T62" ) + cChave + '1' ) )
		If !T62->T62_STATUS $ ( "2|4|6|" )
			nOpc := 4
		EndIf
	EndIf
	
	
	Begin Transaction	
		
		//�������������������������������������������������������������Ŀ
		//�Funcao para validar se a operacao desejada pode ser realizada�
		//���������������������������������������������������������������
		If FTafVldOpe( "T62", nIndChv, @nOpc, cFilEv, @aIncons, aChave, @oModel, "TAFA470", cCmpsNoUpd, nIndIDVer )

			If TafColumnPos( "T62_LOGOPE" )
				cLogOpeAnt := T62->T62_LOGOPE
			endif

			//���������������������������������������������������������������Ŀ
			//�Carrego array com os campos De/Para de gravacao das informacoes�
			//�����������������������������������������������������������������
			aRules := TAF470Rul( cCodEvent, cOwner )								                
																																											
			//����������������������������������������������������������������Ŀ
			//�Quando se tratar de uma Exclusao direta apenas preciso realizar �
			//�o Commit(), nao eh necessaria nenhuma manutencao nas informacoes�
			//������������������������������������������������������������������
			If nOpc <> 5 

				oModel:LoadValue( "MODEL_T62", "T62_FILIAL", T62->T62_FILIAL )															

				If TAFColumnPos( "T62_XMLID" )
					oModel:LoadValue( "MODEL_T62", "T62_XMLID", cXmlID )
				EndIf

				//����������������������������������������Ŀ
				//�Rodo o aRules para gravar as informacoes�
				//������������������������������������������
				For nlI := 1 To Len( aRules )
					oModel:LoadValue( "MODEL_T62", aRules[ nlI, 01 ], FTafGetVal( aRules[ nlI, 02 ], aRules[nlI, 03], aRules[nlI, 04], @aIncons, .F. ) )
				Next

				If Findfunction("TAFAltMan")
					if nOpc == 3
						TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_T62', 'T62_LOGOPE' , '1', '' )
					elseif nOpc == 4
						TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_T62', 'T62_LOGOPE' , '', cLogOpeAnt )
					EndIf
				EndIf

				/*----------------------------------------------------------
							Informa��es do registro Filho T63
				----------------------------------------------------------*/
				//Deleto todas as linhas do Grid
				nlJ := 1
				cT63Path := cCabec + "/dmDev[" + cValToChar(nlJ)+ "]" 
	
				If nOpc == 4
					For nlJ := 1 to oModel:GetModel( "MODEL_T63" ):Length()
						oModel:GetModel( "MODEL_T63" ):GoLine(nlJ)
						oModel:GetModel( "MODEL_T63" ):DeleteLine()
					Next nlJ
				EndIf				
	
				//Rodo o XML parseado para gravar as novas informacoes no GRID
				nlJ := 1
				While oDados:XPathHasNode(cT63Path)
	
					If nOpc == 4 .or. nlJ > 1
						oModel:GetModel( "MODEL_T63" ):lValid:= .T.
						oModel:GetModel( "MODEL_T63" ):AddLine()
					EndIf
					
					//dmDev				
					If oDados:XPathHasNode(cT63Path + "/tpBenef") .OR. oDados:XPathHasNode(cT63Path + "/nrBenefic")
						oModel:LoadValue( "MODEL_T63", "T63_IDBENE", FGetIdInt( "tpBenef","nrBenefic", cT63Path + "/tpBenef", cT63Path + "/nrBenefic",,,@cInconMsg, @nSeqErrGrv)) 
					EndIF
					
					If oDados:XPathHasNode(cT63Path + "/ideDmDev")
						oModel:LoadValue( "MODEL_T63", "T63_DEMPAG", FTafGetVal( cT63Path + "/ideDmDev", "C", .F., @aIncons, .F. ) )
					EndIF
											
					/*----------------------------------------------------------
							Informa��es do registro Filho T6O
					----------------------------------------------------------*/
					//Deleto todas as linhas do Grid
					nlX := 1
					cT6OPath := cT63Path + "/itens[" + cValToChar(nlX)+ "]" 
		
					If nOpc == 4
						For nlX := 1 to oModel:GetModel( "MODEL_T6O" ):Length()
							oModel:GetModel( "MODEL_T6O" ):GoLine(nlX)
							oModel:GetModel( "MODEL_T6O" ):DeleteLine()
						Next nlX
					EndIf				
		
					//Rodo o XML parseado para gravar as novas informacoes no GRID
					nlX := 1
					While oDados:XPathHasNode(cT6OPath)
		
						If nOpc == 4 .or. nlX > 1
							oModel:GetModel( "MODEL_T6O" ):lValid:= .T.
							oModel:GetModel( "MODEL_T6O" ):AddLine()
						EndIf

						if oDados:XPathHasNode(	cT6OPath + "/ideTabRubr")
							cIdTabR := TAFIdTabRub( FTafGetVal( cT6OPath + "/ideTabRubr", "C", .F., @aIncons, .F. ) )
						else
							cIdTabR := ""
						EndIf

						//itens
						If oDados:XPathHasNode(cT6OPath + "/codRubr")
							oModel:LoadValue( "MODEL_T6O", "T6O_IDRUBR", FGetIdInt( "codRubr","ideTabRubr", FTafGetVal( cT6OPath + "/codRubr", "C", .F., @aIncons, .F. ),cIdTabR,.F.,,@cInconMsg, @nSeqErrGrv,/*9*/,/*10*/,/*11*/,/*12*/,/*13*/,StrTran(cPeriodo,"-","")))
						EndIF
								
						If oDados:XPathHasNode(cT6OPath + "/vrRubr")
							oModel:LoadValue( "MODEL_T6O", "T6O_VLRRUB", FTafGetVal( cT6OPath + "/vrRubr", "N", .F., @aIncons, .F. ) )
						EndIF
										
						nlX ++
						cT6OPath := cT63Path + "/itens[" + cValToChar(nlX)+ "]" 
					EndDo
												
					nlJ ++
					cT63Path := cCabec + "/dmDev[" + cValToChar(nlJ)+ "]" 
				EndDo
							
			EndIf
			
			//���������������������������Ŀ
			//�Efetiva a operacao desejada�
			//�����������������������������
			If Empty(cInconMsg) .And. Empty(aIncons)
				If TafFormCommit( oModel )
					Aadd(aIncons, "ERRO19")		
				Else	
					lRet := .T.	 
				EndIf	
			Else			
				Aadd(aIncons, cInconMsg)	
				DisarmTransaction()	
			EndIf	
			
			oModel:DeActivate()
			If FindFunction('TafClearModel')
				TafClearModel(oModel)
			EndIf		    								 				
		EndIf        
		                                                                   	
	End Transaction  	
	
	//����������������������������������������������������������Ŀ
	//�Zerando os arrays e os Objetos utilizados no processamento�
	//������������������������������������������������������������
	aSize( aRules, 0 ) 
	aRules     := Nil
	
	aSize( aChave, 0 ) 
	aChave     := Nil    	

EndIf

Return { lRet, aIncons } 

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF470Rul           
Regras para gravacao das informacoes do registro S-1207 do E-Social

@Param
nOper      - Operacao a ser realizada ( 3 = Inclusao / 4 = Alteracao / 5 = Exclusao )

@Return	
aRull  - Regras para a gravacao das informacoes


@author denis.oliveira
@since 13/03/2017
@version 1.0
/*/                        	
//-------------------------------------------------------------------
Static Function TAF470Rul( cCodEvent, cOwner )
                             
Local aRull		 
Local cCabec		
Local cPeriodo	

Default cCodEvent	:= ""
Default cOwner	:= ""

aRull		:= {}   
cCabec		:= "/eSocial/evtBenPrRP"
cPeriodo	:= ""

//-- ideBenef
If TafXNode( oDados, cCodEvent, cOwner,( cCabec + "/ideEvento/indApuracao" ) )
	Aadd( aRull, {"T62_INDAPU", cCabec + "/ideEvento/indApuracao","C",.F.} ) 	 	    
EndIf

If TafXNode( oDados, cCodEvent, cOwner,( cCabec + "/ideEvento/perApur" ) )
	cPeriodo	:= FTafGetVal( cCabec + "/ideEvento/perApur", "C", .F.,, .F. )

	If At("-", cPeriodo) > 0
		Aadd( aRull, {"T62_PERAPU", StrTran(cPeriodo, "-", "" ) ,"C",.T.} )	
	Else
		Aadd( aRull, {"T62_PERAPU", cPeriodo ,"C", .T.} )		
	EndIf     
EndIf 

If TafXNode( oDados, cCodEvent, cOwner,( cCabec + "/ideBenef/cpfBenef" ) )
	aAdd( aRull, { "T62_CPF", cCabec + "/ideBenef/cpfBenef", "C", .F. } ) //cpfBenef
EndIf
                    
Return ( aRull )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF470Xml
Funcao de geracao do XML para atender o registro S-1207
Quando a rotina for chamada o registro deve estar posicionado

@Param:
lRemEmp - Exclusivo do Evento S-1000
cSeqXml - Numero sequencial para composi��o da chave ID do XML

@Return:
cXml - Estrutura do Xml do Layout S-1000

@author denis.oliveira
@since 13/03/2017
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAF470Xml(cAlias,nRecno,nOpc,lJob,lRemEmp,cSeqXml)

Local cXml    	
Local cLayout 	
Local cReg	  		
Local cInfEvt		
Local cIdTpBen 	
Local cIdNrBen 	
Local nMVIDETABR	:= SuperGetMV("MV_IDETABR",.F.,"0")
Local aMensal		
Local lXmlVLd		:= IIF(FindFunction('TafXmlVLD'),TafXmlVLD('TAF470XML'),.T.)

cXml    	:= ""
cLayout 	:= "1207"
cReg	  	:= "BenPrRP"
cInfEvt	:= T62->T62_VERSAO
cIdTpBen	:= ""
cIdNrBen 	:= ""

aMensal	:= {}

Default cAlias	:= ""
Default cSeqXml := ""
Default nRecno	:= 0
Default nOpc		:= 1
Default lJob		:= .F.


//-- Abro as tabelas
dbSelectArea("T5T")
T5T->( DBSetOrder(1) )

dbSelectArea("T5G")
T5G->( DBSetOrder(1) )

dbSelectArea("C8R")
C8R->( DBSetOrder(1) )

	If lXmlVLd
		If  IsInCallStack("TAFA470") .AND. (IsInCalLStack("TAFXmlLote") .OR. IsInCallStack("TAF470Xml")) //Execu��o Manual
			If slRubERPPad == Nil
				lRubERPPad := nMVIDETABR == "1" .OR. (nMVIDETABR == "0" .and. ApMsgYesNo(STR0035 + CRLF + STR0036 + CRLF + STR0037, STR0038)) //"Deseja gerar o conte�do da tag 'ideTabRubr' com o c�digo padr�o deste ERP ou conforme ERP de Origem?" , " - Sim para c�digo padr�o (T3M_ID)." , " - N�o para conforme ERP de Origem (T3M_CODERP)." , "Conte�do 'ideTabRubr' padr�o?"
				If IsInCalLStack("TAFXmlLote")
					slRubERPPad	:= lRubERPPad
				EndIf
			Else
				lRubERPPad	:= slRubERPPad
			EndIf

		EndIf



		//*******************
		//-- ideBenef
		//*******************
		cXml +=			"<ideBenef>"
		cXml +=				xTafTag("cpfBenef"	,T62->T62_CPF)
		cXml +=			"</ideBenef>"

		//*******************
		//-- dmDev
		//*******************
		If T63->(MsSeek(xFilial("T63") + T62->(T62_ID+T62_VERSAO) ))

			While !T63->(Eof()) .And. AllTrim(T63->(T63_ID+T63_VERSAO)) == AllTrim(T62->(T62_ID+T62_VERSAO))
			
				cXml +=		"<dmDev>"
				
				//-- Busco o Id do Tipo e N�mero de Benef�cio
				cIdTpBen := Posicione("T5T",3,xFilial("T5T")+T63->T63_IDBENE+"1","T5T_TPBENE")
				cIdNrBen := T5T->T5T_NUMBEN

				cXml +=		xTafTag("tpBenef"		,Posicione("T5G",1,xFilial("T5G")+cIdTpBen,"T5G_CODIGO"))
				cXml +=		xTafTag("nrBenefic" 	,cIdNrBen)
				cXml +=		xTafTag("ideDmDev"	,T63->T63_DEMPAG)
				
				//*******************
				//	 itens
				//*******************
				If T6O->(MsSeek(xFilial("T6O") + T63->(T63_ID+T63_VERSAO+T63_INDAPU+T63_PERAPU+T63_CPF+T63_IDBENE) ))
					While !T6O->(Eof()) .And. AllTrim(T6O->(T6O_ID+T6O_VERSAO+T6O_INDAPU+T6O_PERAPU+T6O_CPF+T6O_IDBENE)) == AllTrim(T63->(T63_ID+T63_VERSAO+T63_INDAPU+T63_PERAPU+T63_CPF+T63_IDBENE))                                                  
					
						cXml +=			"<itens>"
						cXml +=				xTafTag("codRubr"		,Posicione("C8R",5,xFilial("C8R")+T6O->T6O_IDRUBR+"1","C8R_CODRUB"))

						If lRubERPPad
							cXml +=	xTafTag("ideTabRubr",C8R->C8R_IDTBRU,,.T.)
						Else
							cXml +=	xTafTag("ideTabRubr", Posicione("T3M",1,xFilial("T3M") + C8R->C8R_IDTBRU, "T3M_CODERP"),,.T.)
						EndIf
				
						cXml +=				xTafTag("vrRubr"		,T6O->T6O_VLRRUB,PesqPict("T6O","T6O_VLRRUB"))	
						cXml +=			"</itens>"
						
						T6O->( dbSkip() )	
					EndDo
				EndIf
				
				cXml +=		"</dmDev>"
				
				T63->( dbSkip() )
			EndDo
			
		EndIf	

		//-- Gravo no array o indicativo e o per�odo de apura��o
		If T62->T62_INDAPU == '1' //Mensal	 

			aAdd(aMensal,T62->T62_INDAPU) 
			aAdd(aMensal,Substr(T62->T62_PERAPU, 1, 4) + '-' + Substr(T62->T62_PERAPU, 5, 2) )


		ElseIf T62->T62_INDAPU == '2' //Anual	
			
			aAdd(aMensal,T62->T62_INDAPU)  
			aAdd(aMensal,Alltrim(T62->T62_PERAPU))

		Endif


		//����������������������Ŀ
		//�Estrutura do cabecalho�
		//������������������������
		cXml := xTafCabXml(cXml,"T62", cLayout, cReg, aMensal,cSeqXml)

		//����������������������������Ŀ
		//�Executa gravacao do registro�
		//������������������������������
		If !lJob
			xTafGerXml(cXml,cLayout)
		EndIf
	EndIf

Return(cXml)
//-------------------------------------------------------------------
/*/{Protheus.doc} TAF470Vld

Funcao que valida os dados do registro posicionado,
verificando se ha incoerencias nas informac�es caso seja necessario gerar um XML

lJob - Informa se foi chamado por Job

@return .T.

@author denis.oliveira
@since 13/03/2017
@version 1.0
/*/                                                                                                                                          
//-------------------------------------------------------------------
Function TAF470Vld(cAlias,nRecno,nOpc,lJob)

Local aLogErro 
                 
aLogErro	:= {} 
 
Default cAlias	:= ""
Default nRecno	:= 0	
Default nOpc		:= 0
Default lJob   	:= .F.

//-- Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := T62->( Recno() )

//-- Abro as tabelas
DBSelectArea("T63")
DBSelectArea("T6O")
 
 
If (T62->T62_STATUS $ (' 1'))	     
	
	//indApuracao
	//-----------
	If Empty(T62->T62_INDAPU)
		AADD(aLogErro,{"T62_INDAPU","000010","T62", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	ElseIf !( T62->T62_INDAPU $ "1|2" )
		AADD(aLogErro,{"T62_INDAPU","000006","T62", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"
	EndIf

	//perApuracao
	//-----------
	If Empty(T62->T62_PERAPU)
		AADD(aLogErro,{"T62_PERAPU","000010","T62", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	Else
		If T62->T62_INDAPU == "1"
			If Len(AllTrim(T62->T62_PERAPU)) <> 6 //Formato MMAAAA
				AADD(aLogErro,{"T62_PERAPU","000318","T62", nRecno }) //STR0318 - "Informar o m�s/ano (formato MMAAAA) da folha de pagamento, se {indApuracao} = [1], ou apenas o ano (formato AAAA), se {indApuracao} = [2]"
			Else
				If Substr( AllTrim(T62->T62_PERAPU), 1, 4 ) <= '2017' .Or. Substr( AllTrim(T62->T62_PERAPU), 5, 2 ) < '05'
					AADD(aLogErro,{"T62_PERAPU","001148","T62", nRecno }) //O M�s/Ano de apura��o deve ser maior ou igual a Maio de 2018, data de in�cio da obrigatoriedade do envio do evento ao e-Social"
				Endif
			EndIf
		ElseIf T62->T62_INDAPU == "2"
			If Len(AllTrim(T62->T62_PERAPU)) <> 4 //Formato AAAA
				AADD(aLogErro,{"T62_PERAPU","000318","T62", nRecno }) //STR0318 - "Informar o m�s/ano (formato MMAAAA) da folha de pagamento, se {indApuracao} = [1], ou apenas o ano (formato AAAA), se {indApuracao} = [2]"
			EndIf
		EndIf
	EndIf
	
	//********************************************
	//eSocial/.../ideBenef 
	//********************************************
	//cpfBenef
	If Empty(T62->T62_CPF)
		aAdd(aLogErro,{"T62_CPF","000010","T62",nRecno}) //STR0010 - "Campo Inconsistente ou Vazio"
	Else
		If !CGC(T62->T62_CPF,,.F.)
			aAdd(aLogErro,{"T62->T62_CPF","000646","T62",nRecno}) //"O CPF � inv�lido."
		EndIF
	EndIf    
	
	//********************************************
	//eSocial/.../dmDev
	//********************************************
	If T63->( MSSeek( xFilial("T63") + T62->(T62_ID+T62_VERSAO) ))		
		While T63->(!Eof()) .and. AllTrim(T63->(T63_ID+T63_VERSAO)) == AllTrim(T62->(T62_ID+T62_VERSAO))
	
			//tpBenef-nrBenefic
			If Empty(T63->T63_IDBENE)
				aAdd( aLogErro, { "T63_IDBENE", "000010", "T63", nRecno } ) //STR0010 - Campo Inconsistente ou Vazio
			Else
				xValRegTab( "T5T", T63->T63_IDBENE, 3, , @aLogErro, /*Data validacao periodo*/, { "T5T", "T63_IDBENE", nRecno } )
			EndIf
		
			//ideDmDev
			If Empty(T63->T63_DEMPAG)
				aAdd( aLogErro, { "T63_DEMPAG", "000010", "T63", nRecno } ) //STR0010 - Campo Inconsistente ou Vazio
			EndIf
			
			//********************************************
			//eSocial/.../dmDev/itens
			//********************************************
			If T6O->( MSSeek( xFilial("T6O") + T63->(T63_ID+T63_VERSAO+T63_INDAPU+T63_PERAPU+T63_CPF+T63_IDBENE) ))
				While T6O->(!Eof()) .and. AllTrim(T6O->(T6O_ID+T6O_VERSAO+T6O_INDAPU+T6O_PERAPU+T6O_CPF+T6O_IDBENE)) == AllTrim(T63->(T63_ID+T63_VERSAO+T63_INDAPU+T63_PERAPU+T63_CPF+T63_IDBENE))
				
					//codRubr-ideTabRubr
					If Empty(T6O->T6O_IDRUBR)
						aAdd(aLogErro,{"T6O_IDRUBR","000010","T6O",nRecno}) //Campo Inconsistente ou Vazio
					Else
						DBSelectArea("C8R")
						C8R->(DBSetOrder(5)) //C8R_FILIAL+C8R_ID+C8R_ATIVO
						
						IF C8R->(MSSeek(xFilial("C8R")+T6O->T6O_IDRUBR+"1"))
							If !(C8R->C8R_STATUS $ ("0|4|"))
								aAdd(aLogErro,{"T6O_IDRUBR","000882","T6O",nRecno})//"O c�digo n�o � v�lido. Fa�a a valida��o no evento S-1010 - Tabela de Rubricas."
							EndIf
						Else 
							aAdd(aLogErro,{"T6O_IDRUBR","000889","T63",nRecno}) //"O c�digo informado deve existir na tabela S-1010 - Tabela de Rubricas."
						EndIf
						
						C8R->(DbCloseArea())
					EndIf
					
					//vrRubr	
					If Empty(T6O->T6O_VLRRUB)
						aAdd(aLogErro,{"T6O_VLRRUB","000010","T6O",nRecno}) //Campo Inconsistente ou Vazio
					Else
						If ! T6O->T6O_VLRRUB > 0 
							aAdd(aLogErro,{"T6O_VLRRUB","000520","T6O",nRecno})  //"O valor informado no campo deve ser maior que 0 (zero)"	
						EndIf
					EndIf
					
					T6O->(DBSkip())
			
				EndDo
			EndIf
	
			T63->(DBSkip())
		
		EndDo
	EndIf
		
	//ATUALIZO O STATUS DO REGISTRO
	//Utilizo RecLock, pois o SETVALUE somente funciona em campos USADOS
	RecLock("T62",.F.)
	T62->T62_STATUS := Iif(Len(aLogErro) > 0, "1", "0") 
	T62->( MsUnlock() )    

Else

	AADD(aLogErro,{"T62_ID","000305","T62",nRecno })   

EndIf	

//N�o apresento o alert quando utilizo o JOB para validar
If !lJob
	xValLogEr(aLogErro)
EndIf	


Return(aLogErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} GerarEvtExc
Funcao que gera o evento de exclus�o do evento

@Param  oModel  -> Modelo de dados
@Param  nRecno  -> Numero do recno
@Param  lRotExc -> Variavel que controla se a function � chamada pelo TafIntegraESocial

@Return .T.

@Author denis.oliveira
@Since 13/03/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function GerarEvtExc( oModel, nRecno, lRotExc  )

Local cVerAnt   	 
Local cProtocolo	
Local cVersao   	
Local cEvento		 
Local cChvRegAnt	

Local nOperation	
Local nT62			
Local nT63			 
Local nT6O			
Local nT6OAdd		
Local nlI			
Local nlY			

Local aGravaT62	
Local aGravaT63 	
Local aGravaT6O	

Local oModelT62 	
Local oModelT63 	
Local oModelT6O 	

Local lRetorno  	

cVerAnt   	:= ""  
cProtocolo	:= ""
cVersao	:= ""
cEvento	:= ""  
cChvRegAnt	:= ""

nOperation	:= oModel:GetOperation()
nT62		:= 0  
nT63		:= 0   
nT6O		:= 0  
nT6OAdd	:= 0 
nlI 		:= 0
nlY			:= 0

aGravaT62	:= {}
aGravaT63 	:= {}
aGravaT6O 	:= {}

oModelT62 	:= Nil
oModelT63 	:= Nil
oModelT6O 	:= Nil

lRetorno  	:= .T.

Default oModel	:= Nil
Default nRecno	:= Nil
Default lRotExc	:= Nil


Begin Transaction

	//Posiciona o item
	("T62")->( DBGoTo( nRecno ) )
		
					
	//Carrego a Estrutura dos Models a serem gravados
	oModelT62	:= oModel:GetModel( 'MODEL_T62' ) 
	oModelT63	:= oModel:GetModel( 'MODEL_T63' ) 
	oModelT6O	:= oModel:GetModel( 'MODEL_T6O' ) 
	
	
	//�����������������������������������������������������������Ŀ
	//�Busco a versao anterior do registro para gravacao do rastro�
	//�������������������������������������������������������������
	cVerAnt	:= oModelT62:GetValue( "T62_VERSAO" )				
	cProtocolo	:= oModelT62:GetValue( "T62_PROTUL" )				
	cEvento	:= oModelT62:GetValue( "T62_EVENTO" )	
	
	
	//�����������������������������������������������������������������Ŀ
	//�Neste momento eu gravo as informacoes que foram carregadas       �
	//�na tela, pois neste momento o usuario ja fez as modificacoes que �
	//�precisava e as mesmas estao armazenadas em memoria, ou seja,     �
	//�nao devem ser consideradas neste momento                         �
	//�������������������������������������������������������������������
	For nlI := 1 To 1
		For nlY := 1 To Len( oModelT62:aDataModel[ nlI ] )			
			Aadd( aGravaT62, { oModelT62:aDataModel[ nlI, nlY, 1 ], oModelT62:aDataModel[ nlI, nlY, 2 ] } )									
		Next
	Next	       						
				
	For nT63 := 1 To oModel:GetModel( 'MODEL_T63' ):Length()
		oModel:GetModel( 'MODEL_T63' ):Goline(nT63) 
		
		If !oModel:GetModel( 'MODEL_T63' ):IsDeleted()
			aAdd (aGravaT63, {oModelT63:GetValue('T63_IDBENE'),;
		        		       oModelT63:GetValue('T63_DEMPAG') }) 
						
			For nT6O := 1 To oModel:GetModel( 'MODEL_T6O' ):Length() 
				oModel:GetModel( 'MODEL_T6O' ):GoLine(nT6O)
							
				If !oModel:GetModel( 'MODEL_T6O' ):IsDeleted()
					aAdd (aGravaT6O, {oModelT63:GetValue('T63_IDBENE') ,;
				    			    	  oModelT6O:GetValue('T6O_IDRUBR') ,; 
							          	  oModelT6O:GetValue('T6O_VLRRUB') }) 
				EndIf		
						
			Next // Fim - T60
		EndIf
	
	Next // Fim - CMS

	//�����������������������������������������������������������Ŀ
	//�Seto o campo como Inativo e gravo a versao do novo registro�
	//�no registro anterior                                       �
	//|                                                           |
	//|ATENCAO -> A alteracao destes campos deve sempre estar     |
	//|abaixo do Loop do For, pois devem substituir as informacoes|
	//|que foram armazenadas no Loop acima                        |
	//�������������������������������������������������������������
	FAltRegAnt( 'T62', '2' )
			
	//��������������������������������������������������Ŀ
	//�Neste momento eu preciso setar a operacao do model�
	//�como Inclusao                                     �
	//����������������������������������������������������
	oModel:DeActivate()
	oModel:SetOperation( 3 ) 	
	oModel:Activate()		
								
	//�������������������������������������������������������Ŀ
	//�Neste momento eu realizo a inclusao do novo registro ja�
	//�contemplando as informacoes alteradas pelo usuario     �
	//���������������������������������������������������������
	For nT62 := 1 to Len( aGravaT62 )
		oModel:LoadValue( "MODEL_T62", aGravaT62[ nT62, 1 ], aGravaT62[ nT62, 2 ] )
	Next nT62
					
	For nT63 := 1 To Len( aGravaT63 )
		oModel:GetModel( 'MODEL_T63' ):LVALID	:= .T.
		
		If nT63 > 1
			oModel:GetModel( 'MODEL_T63' ):AddLine()
		EndIf
		oModel:LoadValue( "MODEL_T63", "T63_IDBENE", aGravaT63[nT63][1])	
		oModel:LoadValue( "MODEL_T63", "T63_DEMPAG", aGravaT63[nT63][2])	
							
		nT6OAdd := 1
		For nT6O := 1 To Len( aGravaT6O )
		// Grava apenas o T6O pertencente ao T63
			If aGravaT6O[nT6O][1] == aGravaT63[nT63][1]
				oModel:GetModel( 'MODEL_T6O' ):LVALID	:= .T.
		
				If nT6OAdd > 1
					oModel:GetModel( 'MODEL_T6O' ):AddLine()
				EndIf
				
				oModel:LoadValue( "MODEL_T6O", "T6O_IDRUBR", aGravaT6O[nT6O][2] )
				oModel:LoadValue( "MODEL_T6O", "T6O_VLRRUB", aGravaT6O[nT6O][3] )
		
				nT6OAdd++
			EndIf
		Next // Fim - T6O
	Next // Fim - T63


        
    //�������������������������������Ŀ
	//�Busco a versao que sera gravada�
	//���������������������������������
	cVersao := xFunGetVer()		 
	                                       
	//������������������������������������������������������������		
	//|ATENCAO -> A alteracao destes campos deve sempre estar     |
	//|abaixo do Loop do For, pois devem substituir as informacoes|
	//|que foram armazenadas no Loop acima                        |
	//�������������������������������������������������������������		                                                                      				         
	oModel:LoadValue( 'MODEL_T62', 'T62_VERSAO', cVersao	)  
	oModel:LoadValue( 'MODEL_T62', 'T62_VERANT', cVerAnt	)									          				    
	oModel:LoadValue( 'MODEL_T62', 'T62_PROTPN', cProtocolo )									          						
	oModel:LoadValue( "MODEL_T62", "T62_PROTUL", "" )


	//�������������������������������������������������������������������Ŀ
	//|Tratamento para que caso o Evento Anterior fosse de exclus�o	 	|
	//|seta-se o novo evento como uma "nova inclus�o", caso contr�rio o 	|
	//|evento passar a ser uma altera��o										|
	//���������������������������������������������������������������������	
	oModel:LoadValue( "MODEL_T62", "T62_EVENTO", "E" )
	oModel:LoadValue( "MODEL_T62", "T62_ATIVO" , "1" )
	
	
	//���������������������������������������������	
	//�Gravo as informa��es e o status do registro�
	//���������������������������������������������	
	FwFormCommit( oModel )
	TAFAltStat( 'T62',"6" )
		
	
End Transaction


Return ( .T. )