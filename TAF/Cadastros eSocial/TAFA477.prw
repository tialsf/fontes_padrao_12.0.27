#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFA477.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA477
Cadastro MVC de Solicita��o de Totaliza��o para Pagamento em Conting�ncia - S-1295
 
@author Denis R. de Oliveira
@since 16/08/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function TAFA477()

Private oBrw := Nil

If TafAtualizado()
	oBrw := FWmBrowse():New()

	oBrw:SetDescription( STR0001 )	//"Solicita��o de Totaliza��o para Pagamento em Conting�ncia"
	oBrw:SetAlias( 'T72')
	oBrw:SetMenuDef( 'TAFA477' )	
	
	If FindFunction('TAFSetFilter')
		oBrw:SetFilterDefault(TAFBrwSetFilter("T72","TAFA477","S-1295"))
	Else
		oBrw:SetFilterDefault( "T72_ATIVO == '1'" )
	EndIf

	TafLegend(2,"T72",@oBrw)

	oBrw:Activate()
EndIf

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef

Funcao generica MVC com as opcoes de menu

@author Denis R. de Oliveira
@since 16/08/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

Local aFuncao := {}
Local aRotina := {}
If FindFunction('TafXmlRet')
	Aadd( aFuncao, { "" , "TafxmlRet('TAF477Xml','1295','T72')" , "1" } )
Else 
	Aadd( aFuncao, { "" , "TAF477Xml" , "1" } )
EndIf

Aadd( aFuncao, { "" , "TAF477Vld" , "2" } )
Aadd( aFuncao, { "" , "xFunHisAlt( 'T72', 'TAFA477' ,,,, 'TAF477XML','1295'  )" , "3" } )
Aadd( aFuncao, { "" , "TAFXmlLote( 'T72', 'S-1295' , 'evtTotConting' , 'TAF477Xml',, oBrw )" , "5" } )
Aadd( aFuncao, { "" , "xFunAltRec( 'T72' )" , "10" } )

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

If lMenuDif
	ADD OPTION aRotina Title STR0002 Action "VIEWDEF.TAFA477" OPERATION 2 ACCESS 0 //"Visualizar"
Else
	aRotina := xFunMnuTAF( "TAFA477" , , aFuncao)
EndIf

Return( aRotina )
//------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC

@author Denis R. de Oliveira
@since 16/08/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

Local oStruT72 := FWFormStruct( 1, 'T72' )
LocaL oModel   := MPFormModel():New( 'TAFA477',,,{|oModel| SaveModel( oModel ) } )  

// Adiciona ao modelo um componente de formul�rio
oModel:AddFields("MODEL_T72",/*cOwner*/,oStruT72)
oModel:GetModel("MODEL_T72"):SetPrimaryKey({"T72_FILIAL","T72_ID","T72_VERSAO"})

oStruT72:SetProperty( 'T72_IDRESP', MODEL_FIELD_OBRIGAT , .T. )

Return oModel

//---------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Denis R. de Oliveira
@since 16/08/2017
@version 1.0
/*/
//---------------------------------------------------------------------
Static Function ViewDef()

Local oModel    := FWLoadModel( 'TAFA477' )
Local oStruT72a	:= Nil
Local oStruT72b	:= Nil
Local oStruT72c	:= Nil
Local oView    	:= FWFormView():New()

Local cCmpFil 	:= ""
Local nI       	:= 0
Local aCmpGrp   := {}

Local cGrpCom1	:= ""
Local cGrpCom2	:= ""
Local cGrpCom3 	:= ""

oView:SetModel( oModel )

//"Informa��es de Apura��o"
cGrpCom1  := 'T72_ID|T72_VERSAO|T72_VERANT|T72_PROTPN|T72_EVENTO|T72_ATIVO|T72_INDAPU|T72_PERAPU|'
cCmpFil   := cGrpCom1 
oStruT72a := FwFormStruct( 2, 'T72', {|x| AllTrim( x ) + "|" $ cCmpFil } )

//"Respons�vel pelas informa��es"
cGrpCom2 := 'T72_IDRESP|T72_DESCRE|'
cCmpFil   := cGrpCom2
oStruT72b := FwFormStruct( 2, 'T72', {|x| AllTrim( x ) + "|" $ cCmpFil } )

//"Protocolo de Transmiss�o"
cGrpCom3 := 'T72_PROTUL|'
cCmpFil   := cGrpCom3
oStruT72c := FwFormStruct( 2, 'T72', {|x| AllTrim( x ) + "|" $ cCmpFil } )

/*--------------------------------------------------------------------------------------------
	    Grupo de campos da Solicita��o de Totaliza��o para Pagamento em Conting�ncia
---------------------------------------------------------------------------------------------*/

oStruT72a:AddGroup( "GRP_APURACAO", STR0003, "", 1 ) //Informa��es de Apura��o 

aCmpGrp := StrToKArr(cGrpCom1,"|")
For nI := 1 to Len(aCmpGrp)
	oStruT72a:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_APURACAO")
Next nI

oStruT72b:AddGroup( "GRP_RESPONSAVEL", STR0004, "", 1 ) //Respons�vel pelas Informa��es

aCmpGrp := StrToKArr(cGrpCom2,"|")
For nI := 1 to Len(aCmpGrp)
	oStruT72b:SetProperty(aCmpGrp[nI],MVC_VIEW_GROUP_NUMBER,"GRP_RESPONSAVEL")
Next nI

If FindFunction('TafAjustRecibo')
	TafAjustRecibo(oStruT72c,"T72")
EndIf 
/*--------------------------------------------------------------------------------------------
									    Esrutura da View
---------------------------------------------------------------------------------------------*/

oView:AddField( 'VIEW_T72a', oStruT72a, 'MODEL_T72' )
oView:AddField( 'VIEW_T72b', oStruT72b, 'MODEL_T72' )
oView:AddField( 'VIEW_T72c', oStruT72c, 'MODEL_T72' )

If FindFunction('TafNmFolder')
	oView:EnableTitleView( 'VIEW_T72c', TafNmFolder("recibo",1) ) // "Recibo da �ltima Transmiss�o"  
Endif 
/*--------------------------------------------------------------------------------------------
								      Estrutura do Folder
---------------------------------------------------------------------------------------------*/

oView:CreateHorizontalBox("PAINEL_SUPERIOR",100)
oView:CreateFolder("FOLDER_SUPERIOR","PAINEL_SUPERIOR")

oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA01', STR0001 )   //"Solicita��o de Totaliza��o para Pagamento em Conting�ncia"

If FindFunction('TafNmFolder')
	oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA02', TafNmFolder("recibo") )   //"Numero do Recibo"
Else
	oView:AddSheet( 'FOLDER_SUPERIOR', 'ABA02', STR0005 )   //"Protocolo de Transmiss�o"
EndIf 

oView:CreateHorizontalBox( 'T72a',  035,,, 'FOLDER_SUPERIOR', 'ABA01' )
oView:CreateHorizontalBox( 'T72b',  065,,, 'FOLDER_SUPERIOR', 'ABA01' )
oView:CreateHorizontalBox( 'T72c',  100,,, 'FOLDER_SUPERIOR', 'ABA02' )

oView:SetOwnerView( "VIEW_T72a", "T72a")
oView:SetOwnerView( "VIEW_T72b", "T72b")
oView:SetOwnerView( "VIEW_T72c", "T72c")

lMenuDif := Iif( Type( "lMenuDif" ) == "U", .F., lMenuDif )

oStruT72a:RemoveField( "T72_ID" )

If !lMenuDif
	xFunRmFStr(@oStruT72a, 'T72')
	xFunRmFStr(@oStruT72b, 'T72')
EndIf

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} SaveModel
Funcao de gravacao dos dados, chamada no final, no momento da
confirmacao do modelo
	
@Param  oModel -> Modelo de dados

@Return .T.

@author Denis R. de Oliveira
@since 16/08/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function SaveModel( oModel )
	
Local cVerAnt    	:= ""
Local cProtocolo 	:= ""
Local cVersao    	:= ""
Local cChvRegAnt 	:= ""
Local cEvento	   	:= ""
Local cLogOpe		:= ""
Local cLogOpeAnt	:= ""
Local nOperation 	:= oModel:GetOperation()
Local nI         	:= 0
Local aGrava     	:= {}
Local oModelT72		:= Nil
Local lRetorno		:= .T.

Begin Transaction

	//Inclusao Manual do Evento
	If nOperation == MODEL_OPERATION_INSERT

		oModel:LoadValue( "MODEL_T72", "T72_VERSAO", xFunGetVer() )

		If Findfunction("TAFAltMan")
			TAFAltMan( 3 , 'Save' , oModel, 'MODEL_T72', 'T72_LOGOPE' , '2', '' )
		Endif

		FwFormCommit( oModel )

	//Altera��o Manual do Evento
	ElseIf nOperation == MODEL_OPERATION_UPDATE 

		//�����������������������������������������������������������������Ŀ
		//�Seek para posicionar no registro antes de realizar as validacoes,�
		//�visto que quando nao esta pocisionado nao eh possivel analisar   �
		//�os campos nao usados como _STATUS                                �
		//�������������������������������������������������������������������
		//Posicionando no registro para que nao ocorra erros duranteo processo de valida��o
	    T72->( DbSetOrder( 4 ) )
	    If T72->( MsSeek( xFilial( 'T72' ) + T72->T72_ID + '1' ) )		
			//Verifica se o evento ja foi trasmitido ao RET
		    If T72->T72_STATUS $ ( "4" )	    		
				//Carrego a Estrutura dos Models a serem gravados
				oModelT72 := oModel:GetModel( "MODEL_T72" )		
				
				//Guardo as informa��es do registro corrente para rastro do registro
				cVerAnt		:= oModelT72:GetValue( "T72_VERSAO" )
				cProtocolo	:= oModelT72:GetValue( "T72_PROTUL" )
				cEvento		:= oModelT72:GetValue( "T72_EVENTO" )

				If TafColumnPos( "T72_LOGOPE" )
					cLogOpeAnt := oModelT72:GetValue( "T72_LOGOPE" )
				endif

				//Armazeno as informa��es correntes do cadastro( Depois da altera��o do Usu�rio )
			                     
				For nI := 1 to Len( oModelT72:aDataModel[ 1 ] )
					Aadd( aGrava, { oModelT72:aDataModel[ 1, nI, 1 ], oModelT72:aDataModel[ 1, nI, 2 ] } )
				Next nI
	
				/*---------------------------------------------------------- 
				Seto o campo como Inativo e gravo a versao do novo registro
				no registro anterior                                       
				          
				ATENCAO -> A alteracao destes campos deve sempre estar     
				abaixo do Loop do For, pois devem substituir as informacoes
				que foram armazenadas no Loop acima 
				-----------------------------------------------------------*/                        
	
				FAltRegAnt( "T72", "2" )
	
				/*----------------------------------------------------------
				Apos deixar o registro corrente como inativo eu seto a 
				opera��o de inclus�o para o novo registro
				-----------------------------------------------------------*/
				oModel:DeActivate()
				oModel:SetOperation( 3 )
				oModel:Activate()
	
				/*----------------------------------------------------------
				Neste momento eu realizo a grava��o de um novo registro id�ntico
				ao original, apenas com as altera��es nos campos modificados
				pelo usu�rio no cadastro
				-----------------------------------------------------------*/			

				For nI := 1 to Len( aGrava )
					oModel:LoadValue( "MODEL_T72", aGrava[ nI, 1 ], aGrava[ nI, 2 ] )
				Next nI

				//Necess�rio Abaixo do For Nao Retirar
				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_T72', 'T72_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				//Busco a nova versao do registro
				cVersao := xFunGetVer()
	
				/*---------------------------------------------------------
				ATENCAO -> A alteracao destes campos deve sempre estar     
				abaixo do Loop do For, pois devem substituir as informacoes
				que foram armazenadas no Loop acima                        
				-----------------------------------------------------------*/
				oModel:LoadValue( "MODEL_T72", "T72_VERSAO", cVersao )
				oModel:LoadValue( "MODEL_T72", "T72_VERANT", cVerAnt )
				oModel:LoadValue( "MODEL_T72", "T72_PROTPN", cProtocolo )
				oModel:LoadValue( "MODEL_T72", "T72_PROTUL", "" )
				oModel:LoadValue( "MODEL_T72", "T72_EVENTO", "A" )
				// Tratamento para limpar o ID unico do xml
				cAliasPai := "T72"
				If TAFColumnPos( cAliasPai+"_XMLID" )
					oModel:LoadValue( 'MODEL_'+cAliasPai, cAliasPai+'_XMLID', "" )
				EndIf
				
				FwFormCommit( oModel )
				TAFAltStat( 'T72', " " )  
			ElseIf	T72->T72_STATUS == "2"                                                                 
				TAFMsgVldOp(oModel,"2")//"Registro n�o pode ser alterado. Aguardando processo da transmiss�o."
				lRetorno:= .F.
			ElseIf T72->T72_STATUS == "6"                                                                                                                                                                                                                                                                                                                               
				TAFMsgVldOp(oModel,"6")//"Registro n�o pode ser alterado. Aguardando proc. Transm. evento de Exclus�o S-3000"
				lRetorno:= .F.
			Elseif T72->T72_STATUS == "7"
				TAFMsgVldOp(oModel,"7") //"Registro n�o pode ser alterado, pois o evento j� se encontra na base do RET"  
				lRetorno:= .F.
			Else
				//Altera��o Sem Transmiss�o
				If TafColumnPos( "T72_LOGOPE" )
					cLogOpeAnt := T72->T72_LOGOPE
				endif

				If Findfunction("TAFAltMan")
					TAFAltMan( 4 , 'Save' , oModel, 'MODEL_T72', 'T72_LOGOPE' , '' , cLogOpeAnt )
				EndIf

				FwFormCommit( oModel )
				TAFAltStat( 'T72', " " )  
			EndIf
		EndIf
	
	//Exclus�o Manual do Evento
	ElseIf nOperation == MODEL_OPERATION_DELETE	  

		cChvRegAnt := T72->(T72_ID + T72_VERANT)              
											
		If !Empty( cChvRegAnt ) 
			TAFAltStat( 'T72', " " )
			FwFormCommit( oModel )				
			If nOperation == MODEL_OPERATION_DELETE
				If T72->T72_EVENTO == "A" .Or. T72->T72_EVENTO == "E"
					TAFRastro( 'T72', 1, cChvRegAnt, .T. , , IIF(Type("oBrw") == "U", Nil, oBrw) )
				EndIf
			EndIf
		Else
			oModel:DeActivate()�
���			oModel:SetOperation(�5�)��������
���			oModel:Activate()�
���			FwFormCommit(�oModel�)
		EndIf

	EndIf
	
End Transaction

Return ( lRetorno )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF477Grv
@type			function
@description	Fun��o de grava��o para atender o registro S-1295.
@author			Denis R. de Oliveira
@since			16/08/2017
@version		1.0
@param			cLayout		-	Nome do Layout que est� sendo enviado
@param			nOpc		-	Op��o a ser realizada ( 3 = Inclus�o, 4 = Altera��o, 5 = Exclus�o )
@param			cFilEv		-	Filial do ERP para onde as informa��es dever�o ser importadas
@param			oXML		-	Objeto com as informa��es a serem manutenidas ( Outras Integra��es )
@param			cOwner
@param			cFilTran
@param			cPredeces
@param			nTafRecno
@param			cComplem
@param			cGrpTran
@param			cEmpOriGrp
@param			cFilOriGrp
@param			cXmlID		-	Atributo Id, �nico para o XML do eSocial. Utilizado para importa��o de dados de clientes migrando para o TAF
@return			lRet		-	Vari�vel que indica se a importa��o foi realizada, ou seja, se as informa��es foram gravadas no banco de dados
@param			aIncons		-	Array com as inconsist�ncias encontradas durante a importa��o
/*/
//-------------------------------------------------------------------
Function TAF477Grv( cLayout, nOpc, cFilEv, oXML, cOwner, cFilTran, cPredeces, nTafRecno, cComplem, cGrpTran, cEmpOriGrp, cFilOriGrp, cXmlID )

Local cCmpsNoUpd	:=	"|T72_FILIAL|T72_ID|T72_VERSAO|T72_VERANT|T72_PROTUL|T72_PROTPN|T72_EVENTO|T72_STATUS|T72_ATIVO|"
Local cCabec		:=	"/eSocial/evtTotConting"
Local cPeriodo		:=	""
Local cChave		:=	""
Local cInconMsg		:=	""
Local cCodEvent		:=	Posicione( "C8E", 2, xFilial( "C8E" ) + "S-" + cLayout, "C8E->C8E_ID" )
Local cLogOpeAnt	:=	""
Local nSeqErrGrv	:=	0
Local nI			:=	0
Local lRet			:=	.F.
Local aIncons		:=	{}
Local aRules		:=	{}
Local aChave		:=	{}
Local oModel		:=	Nil

Private lVldModel	:=	.T. //Caso a chamada seja via integra��o, seto a vari�vel de controle de valida��o como .T.
Private oDados		:=	oXML

Default cLayout		:=	"1295"
Default nOpc		:=	1
Default cFilEv		:=	""
Default oXML		:=	Nil
Default cOwner		:=	""
Default cFilTran	:=	""
Default cPredeces	:=	""
Default nTafRecno	:=	0
Default cComplem	:=	""
Default cGrpTran	:=	""
Default cEmpOriGrp	:=	""
Default cFilOriGrp	:=	""
Default cXmlID		:=	""

//Periodo
cPeriodo	:= FTafGetVal( cCabec + "/ideEvento/perApur", "C", .F., @aIncons, .F. )

Aadd( aChave, {"C", "T72_INDAPU", FTafGetVal( cCabec + "/ideEvento/indApuracao", "C", .F., @aIncons, .F. )  , .T.} ) 
 	cChave := Padr( aChave[ 1, 3 ], Tamsx3( aChave[ 1, 2 ])[1])
		
	If At("-", cPeriodo) > 0
	Aadd( aChave, {"C", "T72_PERAPU", StrTran(cPeriodo, "-", "" ),.T.} )
	cChave += Padr( aChave[ 2, 3 ], Tamsx3( aChave[ 2, 2 ])[1])	
Else
	Aadd( aChave, {"C", "T72_PERAPU", cPeriodo  , .T.} ) 
	cChave += Padr( aChave[ 2, 3 ], Tamsx3( aChave[ 2, 2 ])[1])		
EndIf

Begin Transaction
	
	//Funcao para validar se a operacao desejada pode ser realizada
	If FTafVldOpe( "T72", 2, @nOpc, cFilEv, @aIncons, aChave, @oModel, "TAFA477", cCmpsNoUpd , , , , )      

		If TafColumnPos( "T72_LOGOPE" )
			cLogOpeAnt := T72->T72_LOGOPE
		endif

		//Carrego array com os campos De/Para de gravacao das informacoes ( Cadastrais )
		aRules := TAF477Rul( cCabec, @cInconMsg, @nSeqErrGrv, @aIncons, cCodEvent, cOwner )			
				
		//Quando se tratar de uma Exclusao direta apenas preciso realizar
		//o Commit(), nao eh necessaria nenhuma manutencao nas informacoes
		If nOpc <> 5
		
			oModel:LoadValue( "MODEL_T72", "T72_FILIAL", T72->T72_FILIAL )

			If TAFColumnPos( "T72_XMLID" )
				oModel:LoadValue( "MODEL_T72", "T72_XMLID", cXmlID )
			EndIf

			//Rodo o aRules para gravar as informacoes
			For nI := 1 to Len( aRules )
			 	oModel:LoadValue( "MODEL_T72", aRules[ nI, 01 ], FTafGetVal( aRules[ nI, 02 ], aRules[nI, 03], aRules[nI, 04], @aIncons, .F. ) )
			Next nI	

			If Findfunction("TAFAltMan")
				if nOpc == 3
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_T72', 'T72_LOGOPE' , '1', '' )
				elseif nOpc == 4
					TAFAltMan( nOpc , 'Grv' , oModel, 'MODEL_T72', 'T72_LOGOPE' , '', cLogOpeAnt )
				EndIf
			EndIf
		EndIf

		//���������������������������Ŀ
		//�Efetiva a operacao desejada�
		//�����������������������������
		If Empty(aIncons) .And. Empty(aIncons)
			If TafFormCommit( oModel )
				Aadd(aIncons, "ERRO19")
			Else
				lRet := .T.
			EndIf	 		
		Else			
			Aadd(aIncons, cInconMsg)	
			DisarmTransaction()	
		EndIf	

		oModel:DeActivate()	
		If FindFunction('TafClearModel')
			TafClearModel(oModel)
		EndIf
		
	EndIf
	
End Transaction

//Zerando os arrays e os Objetos utilizados no processamento
aSize( aRules, 0 )
aRules := Nil

aSize( aChave, 0 )
aChave := Nil

oModel := Nil

Return { lRet, aIncons }

//-------------------------------------------------------------------
/*/{Protheus.doc} TAF477Rul
Regras para gravacao das informacoes de trabalhador do cadastro dos registros S1295

@Param
aRull  - Regras para a gravacao das informacoes

@Return
aRull - Array com as regras do cadastro para integra��o

@author Denis R. de Oliveira
@since 16/08/2017
@version 1.0

/*/
//-------------------------------------------------------------------
Function TAF477Rul( cCabec, cInconMsg, nSeqErrGrv, aIncons, cCodEvent, cOwner )

Local aRull		:= {}
Local aInfComp	:= {} 
Local cPeriodo	:= ""

Default cCabec		:= ""
Default cInconMsg		:= ""
Default nSeqErrGrv	:= 0
Default aIncons		:= {}
Default cCodEvent		:= ""
Default cOwner		:= ""

//**********************************
//eSocial/evtTotConting/ideEvento/
//**********************************	
If TafXNode( oDados, cCodEvent, cOwner, ("/eSocial/evtTotConting/ideEvento/indApuracao") )
	Aadd( aRull, { "T72_INDAPU", cCabec + "/ideEvento/indApuracao", 		"C",  .F. } )		//indApuracao
EndIf

If TafXNode( oDados, cCodEvent, cOwner, ("/eSocial/evtTotConting/ideEvento/perApur") )
	cPeriodo 	:= FTafGetVal(cCabec + "/ideEvento/perApur", "C", .F.,, .F. )
	
	If At("-", cPeriodo) > 0
		Aadd( aRull, {"T72_PERAPU", StrTran(cPeriodo, "-", "" ) ,"C",.T.} )	
	Else
		Aadd( aRull, {"T72_PERAPU", cPeriodo ,"C", .T.} )		
	EndIf
EndIf

//**********************************
//eSocial/evtTotConting/ideRespInf/
//**********************************	
//Inclui em aInfComp informa��es que devem ser utilizadas quando for necessario incluir novo contabilista
Aadd( aInfComp , { 'C2J_NOME' , FTafGetVal( cCabec + "/ideRespInf/nmResp"		, "C", .F., @aIncons, .T.)})
Aadd( aInfComp , { 'C2J_FONE' , FTafGetVal( cCabec + "/ideRespInf/telefone"	, "C", .F., @aIncons, .T.)})
Aadd( aInfComp , { 'C2J_EMAIL', FTafGetVal( cCabec + "/ideRespInf/email"		, "C", .F., @aIncons, .F.)})

If TafXNode( oDados, cCodEvent, cOwner, (cCabec + "/ideRespInf/cpfResp") )	
	Aadd( aRull, { "T72_IDRESP", FGetIdInt( "cpfResp" , "" , cCabec + "/ideRespInf/cpfResp" , , , aInfComp , @cInconMsg , @nSeqErrGrv ) , "C" , .T. } )
EndIf


Return( aRull )

//-------------------------------------------------------------------	
/*/{Protheus.doc} TAF477Xml
Retorna o Xml do Registro Posicionado 
	
@author Denis R. de Oliveira
@since 16/08/2017
@version 1.0
		
@Param:
lJob - Informa se foi chamado por Job

@return
cXml - Estrutura do Xml do Layout S-1295

/*/
//-------------------------------------------------------------------
Function TAF477Xml(cAlias,nRecno,nOpc,lJob,lAutomato,cFile)

Local cXml    	:= ""
Local cLayout 	:= "1295"
Local cReg    	:= "TotConting"
Local aMensal	:= {}
Local lXmlVLd	:= IIF(FindFunction('TafXmlVLD'),TafXmlVLD('TAF477XML'),.T.)


Default lJob 		:= .F.
Default cAlias 	:= "T72"
Default nRecno	:= 1
Default nOpc		:= 1
Default lAutomato	:= .F.
Default cFile		:= ""

//ideEvento
AADD(aMensal,T72->T72_INDAPU)
If lXmlVLd	
	If Len(Alltrim(T72->T72_PERAPU)) <= 4
		AADD(aMensal,T72->T72_PERAPU)
	Else
		AADD(aMensal,substr(T72->T72_PERAPU, 1, 4) + '-' + substr(T72->T72_PERAPU, 5, 2) )
	EndIf
			
	//ideRespInf		
	C2J->( DbSetOrder( 5 ) )
	If C2J->( MsSeek ( xFilial( "C2J" )+T72->( T72_IDRESP ) ) )
		cXml +=	"<ideRespInf>"
		cXml +=		xTafTag("nmResp" 		, C2J->C2J_NOME , , .F. )
		cXml +=		xTafTag("cpfResp"		, C2J->C2J_CPF , , .F.  )
		cXml +=		xTafTag("telefone"	, AllTrim(C2J->C2J_DDD) + StrTran(C2J->C2J_FONE,"-","") , , .F. )
		cXml +=		xTafTag("email"		, C2J->C2J_EMAIL , , .T. )
		cXml +=	"</ideRespInf>"   
	EndIf


	//����������������������Ŀ
	// Estrutura do cabecalho�
	//�����������������������*/
	cXml := xTafCabXml(cXml,"T72",cLayout,cReg,aMensal)
		
		
	T72->(DbCloseArea())
		
	//����������������������������Ŀ
	//�Executa gravacao do registro�
	//�����������������������������*/
	If !lJob
		If lAutomato
			xTafGerXml( cXml, cLayout,,, .F.,, @cFile )
		Else
			xTafGerXml( cXml, cLayout )
		EndIf
	EndIf
EndIf

Return(cXml)
//-------------------------------------------------------------------------------------
/*/{Protheus.doc} TAF477Vld

Validacao dos dados do registro posicionado, verificando inconsistencias
nas informacos caso seja necessario gerar um XML

@author Denis R. de Oliveira
@since 16/08/2017
@version 1.0
/*/                                                                                                                                          
//------------------------------------------------------------------------------------
Function TAF477Vld( cAlias, nRecno, nOpc, lJob )

Local cStatus	 := ""
Local cTpInscr	 := Iif(Len( AllTrim(SM0->M0_CGC) ) == 14,"1","2")
Local cTelefone	 := ""
Local cDatAPU	 := ""
Local aLogErro	 := {}
Local aDadosUtil := {}
Local lValida	 := T72->T72_STATUS $ ( " |1" )
Local lErro		 := .F.

Default cAlias	:= ""
Default nRecno	:= 1
Default nOpc		:= 1
Default lJob		:= .F.

//Garanto que o Recno seja da tabela referente ao cadastro principal
nRecno := T72->( Recno() )

If !lValida
	aAdd( aLogErro, { "T72_ID", "000305", "T72", nRecno } ) //Registros que j� foram transmitidos ao Fisco, n�o podem ser validados
Else

	//----------------------------------
	// Valida��es das regras gen�ricas
	//----------------------------------   
	IF Empty(Substr(T72->T72_PERAPU,5,6))
		cDatAPU:= StoD(Substr(T72->T72_PERAPU,1,4)+ "01" + "01" )
	Else
		cDatAPU:= StoD(Substr(T72->T72_PERAPU,1,4)+ Substr(T72->T72_PERAPU,5,6) + "01" )
	Endif
	
	// S� � permitido enviar um S-1295 se o S-1299 correspondente estiver inv�lido 
	// pela regra REGRA_VALIDA_FECHAMENTO_FOPAG
	aDadosUtil := {}
	aAdd(aDadosUtil, {T72->T72_PERAPU, cDatAPU, T72->T72_INDAPU, ,T72->T72_FILIAL} )
	xVldRegra("C9V","REGRA_VALIDA_FECHAMENTO_FOPAG",{}, aDadosUtil, "T72", @lErro )
    
    aDadosUtil := {}
	aAdd(aDadosUtil, {T72->T72_ID, T72->T72_INDAPU, T72->T72_PERAPU} ) 
	xVldRegra("T72","REGRA_TOTALIZ_CONTING",@aLogErro, aDadosUtil, "T72", lErro )

	//----------------------
	// Valida��es de campo
	//---------------------- 
	//indApuracao
	If Empty(T72->T72_INDAPU)
		AADD(aLogErro,{"T72_INDAPU","000010","T72", nRecno }) //STR0010 - "Campo Inconsistente ou Vazio"
	Else
		If !(T72->T72_INDAPU $ "1|2|")
			AADD(aLogErro,{"T72_INDAPU","000006", "T72", nRecno }) //STR0006 - "Conte�do do campo n�o condiz com as op��es poss�veis"   
		EndIf	
	EndIf

	//perApuracao
	If Empty( T72->T72_PERAPU )
		aAdd( aLogErro, { "T72_PERAPU", "000010", "T72", nRecno } ) //STR0010 - "Campo Inconsistente ou Vazio"
	Else

		If T72->T72_INDAPU == "1"
			If Empty( SubStr( T72->T72_PERAPU, 5, 6 ) )
				aAdd(aLogErro,{"T72_PERAPU","000821","T72",nRecno}) // "Indicativo de per�odo de apura��o(Ind.Per.Apur) igual � 1, o formato do Per�odo de Solicita��o(Per.Apura��o) deve ser AAAA-MM (Ano e M�s)."
			EndIf

			cMes := SubStr( T72->T72_PERAPU, 5, 2 )
			cAno := SubStr( T72->T72_PERAPU, 1, 4 )
			cIniESoc := DToS( xIniESoc() )

			If !( cAno >= SubStr( cIniESoc, 1, 4 ) )
				aAdd( aLogErro, { "T72_PERAPU", "000929", "T72", nRecno } ) //"O per�odo de apura��o informado deve ser igual ou posterior a implementa��o do eSocial.".
			ElseIf cAno == SubStr( cIniESoc, 1, 4 )
				If cMes < SubStr( cIniESoc, 5, 2 )
					aAdd( aLogErro, { "T72_PERAPU", "000929", "T72", nRecno } ) //"O per�odo de apura��o informado deve ser igual ou posterior a implementa��o do eSocial.".
				EndIf
			EndIf

		ElseIf T72->T72_INDAPU == "2"
			If !( Empty( SubStr( T72->T72_PERAPU, 5, 6 ) ) )
				aAdd( aLogErro, { "T72_PERAPU", "000822", "T72", nRecno } ) //"Indicativo de per�odo de apura��o(Ind.Per.Apur) igual � 2, o formato do Per�odo de Solicita��o(Per.Apura��o) deve ser AAAA (Ano)."
			EndIf

			cAno := SubStr( T72->T72_PERAPU, 1, 4 )
			cIniESoc := DToS( xIniESoc() )

			If !( cAno >= SubStr( cIniESoc, 1, 4 ) )
				aAdd( aLogErro, { "T72_PERAPU", "000929", "T72", nRecno } ) //"O per�odo de apura��o informado deve ser igual ou posterior a implementa��o do eSocial.".
			EndIf
		EndIf
	EndIf

	//ideRespInf
	If cTpInscr == "1"
		If Empty(T72->T72_IDRESP)
			AADD(aLogErro,{"T72_IDRESP","001005","T72", nRecno }) //STR1000 - "Se o tipo de inscri��o {tpInsc} = [1](CNPJ), o grupo de TAGs {ideRespInf}(Respons�vel pelas informa��es) � obrigat�rio." 
		EndIf
	EndIf

	If !Empty( T72->T72_IDRESP )
	
		//Chave de busca na tabela Filho ou Consulta Padr�o
		cChave := T72->T72_IDRESP
	
		If xValRegTab( "C2J", cChave, 5,, @aLogErro,, { "T72", "T72_IDRESP", nRecno } )
			if !TafLayESoc("02_04_02")
				xVldRegra( "T72", "REGRA_GERAL_VALIDA_NOME", @aLogErro, { C2J->C2J_NOME, "T72_IDRESP", "C2J" }, "T72" )
			endif

			If Empty(C2J->C2J_CPF)
				aAdd( aLogErro, { "T72_IDRESP", "001018", "T72", nRecno } ) //"O preenchimento do CPF do Respons�vel � obrigat�rio."	
			Else
				If !CGC( C2J->C2J_CPF,, .F. )
					aAdd( aLogErro, { "T72_IDRESP", "001006", "T72", nRecno } ) //"O CPF do Respons�vel � inv�lido."
				EndIf
			EndIf

			cTelefone := AllTrim( C2J->C2J_DDD ) + AllTrim( C2J->C2J_FONE )
			If Empty(cTelefone)
				aAdd( aLogErro, { "T72_IDRESP", "001019", "T72", nRecno } ) //"O preenchimento do N�mero do Telefone do Respons�vel � obrigat�rio."
			Else			
				If Len( cTelefone ) < 10
					aAdd( aLogErro, { "T72_IDRESP", "001007", "T72", nRecno } ) //"O Telefone do Respons�vel deve conter apenas n�meros, com o m�nimo de dez d�gitos."
				Else
					While !Empty( cTelefone )
						If !IsDigit( cTelefone )
							aAdd( aLogErro, { "T72_IDRESP", "001007", "T72", nRecno } ) //"O Telefone do Respons�vel deve conter apenas n�meros, com o m�nimo de dez d�gitos."
							Exit
						EndIf
						cTelefone := SubStr( cTelefone, 2 )
					EndDo
				EndIf
			EndIf	

			If !xVldMail( C2J->C2J_EMAIL )
				aAdd( aLogErro, { "T72_IDRESP", "000862", "T72", nRecno } ) //"O e-mail deve possuir o caractere '@' e este n�o pode estar no in�cio e no fim do e-mail. Deve possuir no m�nimo um caractere '.' depois do @ e n�o pode estar no fim do e-mail."
			EndIf
		
		EndIf
	
	EndIf

	//�������������������������������
	//�ATUALIZO O STATUS DO REGISTRO�
	//�1 = Registro Invalido        �
	//�0 = Registro Valido          �
	//�������������������������������
	cStatus := Iif( Len( aLogErro ) > 0, "1", "0" )

	If RecLock( "T72", .F. )
		T72->T72_STATUS := cStatus
		T72->( MsUnlock() )
	EndIf

EndIf

//�������������������������������������������������������Ŀ
//�Nao apresento o alert quando utilizo o JOB para validar�
//���������������������������������������������������������
If !lJob
	xValLogEr( aLogErro )
EndIf

Return(aLogErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} GerarEvtExc
Funcao que realiza a exclus�o do registro e gera o evento S-3000.

@Param  oModel  -> Modelo de dados
@Param  nRecno  -> Numero do recno
@Param  lRotExc -> Variavel que controla se a function � chamada pelo TafIntegraESocial

@Return .T.

@Author Denis R. de Oliveira
@Since 16/08/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function GerarEvtExc( oModel, nRecno, lRotExc  )

Local cVerAnt		:= ""
Local cProtocolo	:= ""
Local cVersao    	:= ""
Local cChvRegAnt 	:= ""
Local cEvento	   	:= ""
Local cId		   	:= ""
Local nOperation 	:= oModel:GetOperation()
Local nI	    	:= 0
Local aGrava    	:= {}
Local oModelT72 	:= Nil
Local lRetorno      := .T.

Default oModel	:= Nil
Default nRecno	:= 1
Default lRotExc	:= .F.

Begin Transaction

	//Posiciona o item
	("T72")->( DBGoTo( nRecno ) )
					
	//Carrego a Estrutura dos Models a serem gravados
	oModelT72 := oModel:GetModel( "MODEL_T72" )			
				
	//Guardo as informa��es do registro corrente para rastro do registro
	cVerAnt     	:= oModelT72:GetValue( "T72_VERSAO" )
	cProtocolo		:= oModelT72:GetValue( "T72_PROTUL" )
	cEvento	  	:= oModelT72:GetValue( "T72_EVENTO" )
	
	//Armazeno as informa��es correntes do cadastro( Depois da altera��o do Usu�rio )
				                        
	For nI := 1 to Len( oModelT72:aDataModel[ 1 ] )
			Aadd( aGrava, { oModelT72:aDataModel[ 1, nI, 1 ], oModelT72:aDataModel[ 1, nI, 2 ] } )
	Next nI
	
	//---------------------------------------------------------- 
	//Seto o campo como Inativo e gravo a versao do novo registro
	//no registro anterior                                       
	//	          
	//ATENCAO -> A alteracao destes campos deve sempre estar     
	//abaixo do Loop do For, pois devem substituir as informacoes
	//que foram armazenadas no Loop acima 
	//-----------------------------------------------------------/                        

	FAltRegAnt( "T72", "2" )
	
	//----------------------------------------------------------
	//Apos deixar o registro corrente como inativo eu seto a 
	//opera��o de inclus�o para o novo registro
	//-----------------------------------------------------------/
	oModel:DeActivate()
	oModel:SetOperation( 3 )
	oModel:Activate()
	
	//----------------------------------------------------------
	//Neste momento eu realizo a grava��o de um novo registro id�ntico
	//ao original, apenas com as altera��es nos campos modificados
	//pelo usu�rio no cadastro
	//-----------------------------------------------------------/			
	       		
	For nI := 1 to Len( aGrava )
		oModel:LoadValue( "MODEL_T72", aGrava[ nI, 1 ], aGrava[ nI, 2 ] )
	Next nI        
	                          	
	//Busco a nova versao do registro
	cVersao := xFunGetVer()
	
	//---------------------------------------------------------
	//ATENCAO -> A alteracao destes campos deve sempre estar     
	//abaixo do Loop do For, pois devem substituir as informacoes
	//que foram armazenadas no Loop acima                        
	//-----------------------------------------------------------/
	oModel:LoadValue( "MODEL_T72", "T72_VERSAO", cVersao )
	oModel:LoadValue( "MODEL_T72", "T72_VERANT", cVerAnt )
	oModel:LoadValue( "MODEL_T72", "T72_PROTPN", cProtocolo )
	
	
	//---------------------------------------------------------
	//Tratamento para que caso o Evento Anterior fosse de exclus�o
	//seta-se o novo evento como uma "nova inclus�o", caso contr�rio o
	//evento passar a ser uma altera��o
	//-----------------------------------------------------------/
	oModel:LoadValue( "MODEL_T72", "T72_EVENTO", "E" )
	oModel:LoadValue( "MODEL_T72", "T72_ATIVO" , "1" )
		
	
	FwFormCommit( oModel )
	TAFAltStat( 'T72',"6" )
	
End Transaction

Return ( .T. )