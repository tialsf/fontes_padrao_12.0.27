#Include 'Protheus.ch'
#INCLUDE "TOPCONN.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFEXTEMP.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} xMnuExtmp
Funcao responsavel por retornar o MENU dos eventos extempor�neos

@param 	cMenu   - Nome da Rotina

@return	aRotina - Array com as opcoes de MENU

@author Denis R. de Oliveira / Alceu Pereira
@since 22/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function xMnuExtmp( cMenu, cAliasX )

Local aRotina		as array
Local aRotExcl	as array
Local cRecno		as char

Default cMenu		:= ""
Default cAliasX 	:= ""


	aRotina	:= {}
	aRotExcl	:= {}
	cRecno 	:= cValToChar( (cAliasX)->(Recno()) )


	ADD OPTION aRotina Title 'Visualizar' 			Action "VIEWDEF." + cMenu									OPERATION 2 ACCESS 0
	ADD OPTION aRotina Title 'Inclus�o Extp.' 		Action "EvtExtmp('A','" + cMenu + "','" + cRecno + "')" 	OPERATION 3 ACCESS 0
	ADD OPTION aRotina Title 'Retifica��o Extp.' 	Action "EvtExtmp('R','" + cMenu + "','" + cRecno + "')" 	OPERATION 4 ACCESS 0
	ADD OPTION aRotina Title 'Excluir Extp.' 		Action	""													OPERATION 5 ACCESS 0
	ADD OPTION aRotina Title "Ajuste de Recibo"		Action 'xFunAltRec() 	'									OPERATION 3 ACCESS 0


	If FindFunction( "TafXmlExtp" )
		ADD OPTION aRotina Title 'Exportar XML'	Action "TafXmlExtp()" OPERATION 9 ACCESS 0		
	EndIf

	//Grupo de op��es para exclus�o
	Aadd(aRotExcl,{"Excluir Registro"					, "EvtExtmp('E','" + cMenu + "','" + cRecno + "','1')"	, 0, 3, 0, Nil, Nil, Nil} ) 
	Aadd(aRotExcl,{"Desfazer Exclus�o"					, "EvtExtmp('E','" + cMenu + "','" + cRecno + "','2')"	, 0, 5, 0, Nil, Nil, Nil} ) 
	Aadd(aRotExcl,{"Visualizar Registro de Exclus�o"	, "EvtExtmp('E','" + cMenu + "','" + cRecno + "','3')"  , 0, 2, 0, Nil, Nil, Nil} )   
	
	
	aRotina[4][2] := aRotExcl

Return( aRotina )

//-----------------------------------------------------------------------------
/*/{Protheus.doc} EvtExtmp
Funcao que possibilita a chamada do evento extemporaneo no menu das rotinas 

@Return 

@Author Denis R. Oliveira / Alceu Pereira
@Since 22/02/2018
@Version 1.0
/*/
//-----------------------------------------------------------------------------
Function EvtExtmp( cTpExtmp, cMenu, cRecno, cTpExcl )

Local nExView	as numeric

Default cTpExtmp	:= ""
Default cMenu		:= ""
Default cRecno	:= ""
Default cTpExcl	:= ""

nExView	:=	-1

Eval( {|| nExView := GoExtemp( cTpExtmp, cMenu, cRecno, cTpExcl ), Nil })

Return

//-----------------------------------------------------------------------------
/*/{Protheus.doc} GoSetExtemp
Funcao que possibilita o evento extemporaneo para os eventos do eSocial 

@Return nExView

@Author Denis R. Oliveira / Alceu Pereira
@Since 22/02/2018
@Version 1.0
/*/
//-----------------------------------------------------------------------------
Static Function GoExtemp( cTpExtmp, cMenu, cRecno, cTpExcl ) 

Local cTitulo		as char
Local cAlsHist	as char
Local cNomEvto	as char
Local cStatEve 	as char
                
Local nOpera		as numeric
Local nExView		as numeric
Local nRecno		as numeric

Local oModel		as object

Private ALTERA
Private INCLUI
Private lGoExtemp	as logical

Default cTpExtmp	:= ""
Default cMenu 	:= ""
Default cRecno	:= ""
Default cTpExcl	:= ""

cTitulo	:= ""
cAlsHist 	:= TAFRotinas( cMenu, 1, .F., 2 )[3]
cNomEvto	:= TAFRotinas( cMenu, 1, .F., 2 )[4]
                
nOpera		:= 0  //Opera��o a ser realizada 
nExView	:= -1
nRecno		:= 0

oModel		:= Nil

lGoExtemp	:= .T.

//Recebo o recno 
nRecno	:= (cAlsHist)->(Recno())

//Recebo o Model
oModel	:= FWLoadModel(cMenu)

//-- Inclus�o de Extempor�neo
If cTpExtmp == "A" 

	//Posiciono no registro 
	(cAlsHist)->( DbGoTo(Val(cRecno)) )
	
	//Recebo o status do registro
	cStatEve	:= (cAlsHist)->&((cAlsHist) + "_STATUS")  

	If cStatEve $ "2|6"        

		Aviso(STR0001,STR0002,{STR0003})//("e-Social","Esta opera��o n�o � permitida para eventos aguardando retorno do Governo.",{"Ok"})

	ElseIf cStatEve == "4" 
		
		nOpera := MODEL_OPERATION_INSERT
		INCLUI := .T.
		ALTERA := .F.
		
		cTitulo	:= STR0004 + cNomEvto //"Inclus�o do Evento "
		
		oModel:SetOperation(4)
		oModel:Activate()
	
		//Efetuo a inclus�o do registro
		FWMsgRun(,{||nExView:=FWExecView(cTitulo,cMenu,nOpera,,{||.T.},,,,,,, oModel )},,"Executando Rotina do evento " + cNomEvto )
	Else
		Aviso(STR0001,STR0005,{STR0003}) //"e-Social","N�o � permitido incluir extempor�neo para eventos n�o trasmitidos com sucesso ao RET.",{"Ok"})
	EndIf

//-- Retifica��o ou Exclus�o de Extempor�neo	
Else

	//Posiciono no registro 
	(cAlsHist)->( DbGoTo(nRecno) )

	//Recebo o status do registro
	cStatEve	:= (cAlsHist)->&((cAlsHist) + "_STATUS")  

	If cTpExtmp == "1" .AND. cStatEve $ "2|6"        
		
		Aviso(STR0001,STR0002,{STR0003})//("e-Social","Esta opera��o n�o � permitida para eventos aguardando retorno do Governo.",{"Ok"})
	
	Else

		If cTpExtmp == "R"
		
			If cNomEvto == "S-2190" .AND. cStatEve == "4"
				Aviso(STR0001,STR0016,{STR0003}) //("e-Social","N�o � poss�vel retificar um evento S-2190 (Admiss�o de Trabalhador - Registro Preliminar) j� transmitido.",{"Ok"})
			Else
				nOpera := MODEL_OPERATION_UPDATE
				INCLUI := .F.
				ALTERA := .T.
				
				cTitulo	:= STR0007 + cNomEvto //"Altera��o do Evento "
			
				//Efetuo a altera��o no registro
				FWMsgRun(,{||nExView:=FWExecView(cTitulo,cMenu,nOpera,,{||.T.})},, STR0008 + cNomEvto ) //"Executando Rotina do evento "
			EndIf
		
		ElseIf cTpExtmp == "E"
		
			INCLUI := .F.
			ALTERA := .F.
		
			cTitulo	:= STR0009 + cNomEvto //"Exclus�o do Evento "
			
			//Efetuo a exclus�o no registro
			xTafVExc( cAlsHist, nRecno, Val(cTpExcl) )
	
		EndIf
		
	EndIf

EndIf

Return( nExView )

//-------------------------------------------------------------------
/*/{Protheus.doc} TafGrvExt
Funcao para Ser chamada genericamente antes de executar um commit de um modelo

@param	oModel  - Objeto do Model
		cModel  - Nome do Model
		cAliasX - Alias principal do Model

@return Nil

@Author Roberto Souza
@Author Denis R. Oliveira / Alceu Pereira
@Since 02/03/2018
@version 1.0

/*/
//-------------------------------------------------------------------
Function TafGrvExt( oModel, cModel, cAliasX )

Local cEvtModel	as char
Local cEvento		as char
Local cStatus		as char
Local cAtivo		as char

Local lEvtExtmp	as logical
Local dData 		as date

Default oModel	:= ""
Default cModel	:= ""
Default cAliasX 	:= ""

If TafColumnPos( cAliasX + "_DINSIS" ) .AND. TAFColumnPos( cAliasX + "_STASEC" )	

	cEvtModel	:=	oModel:GetModel( "MODEL_" + cAliasX ):GetValue( (cAliasX) + "_EVENTO" )
		
	cEvento	:= (cAliasX)->&((cAliasX) + "_EVENTO" )
	cStatus	:= (cAliasX)->&((cAliasX) + "_STATUS" )
	cAtivo		:= (cAliasX)->&((cAliasX) + "_ATIVO" )
	
	lEvtExtmp	:= (cAliasX)->&((cAliasX) + "_STASEC" ) == "E"
	dData 		:= dDatabase
	
	oModel:LoadValue( cModel, cAliasX + "_DINSIS", dData )
	oModel:LoadValue( cModel, cAliasX + "_STASEC", "E" )
	
	If Empty(cEvtModel) .OR. !lEvtExtmp
		If !lEvtExtmp .AND. cAtivo == "1"
			oModel:LoadValue( "MODEL_" + cAliasX, cAliasX + "_EVENTO", "I" )
			oModel:LoadValue( cModel, cAliasX + "_PROTPN", "" ) 
		ElseIf cStatus == "4"
			oModel:LoadValue( "MODEL_" + cAliasX, cAliasX + "_EVENTO", "R" )
		EndIf
	EndIf
	
	oModel:LoadValue( cModel, cAliasX + "_ATIVO" , "2" )

EndIf	

Return( Nil )

//-------------------------------------------------------------------
/*/{Protheus.doc} xTafExtmp

Funcao para verificar se a funcionalidade de eventos extemporaneos esta habilitada

@Param:  None
@Return: lRet			
				.T. Para validacao OK
				.F. Para validacao NAO OK

@author Roberto Souza
@since 04/01/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function xTafExtmp()
	
	Local lRet := .F.
		
	lRet := TafColumnPos( "T1V_STASEC" ) .And. TafColumnPos( "T1U_STASEC" ) .And. TafColumnPos( "C9V_STASEC" ) .And. TafColumnPos( "CM6_STASEC" )

Return( lRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} xNewHisAlt

Funcao para criar a tela de historico de alteracoes do registro.
O registro a ser filtrado eh o mesmo selecionado pelo usuario na Grid
Similar a xFunHisAlt, com as altera��es de novas colunas, legendas e itens para atender os eventos extemporaneos.

@Param:
cAlias  -  Alias da Tabela Principal ( Tabela onde a Grid eh baseada
                                       para montar as informacoes    )
cRotina - Rotina onde a ViewDef se encontra
cTitulo - Titulo da Janela
lCadTrab- Informa se trata-se de Eventos do Trabalhador

@Return:

@author Roberto Souza
@since 04/01/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function xNewHisAlt( cAlias, cRotina, aHeaderT, cTitulo, lCadTrab, oBrw, cEvento,cLayout,cFunXML  )
Local aAreaAlias := (cAlias)->(GetArea())
Local cFilter    := cAlias + "_FILIAL + " + cAlias + "_ID"
Local nRecCAli	 := (cAlias)->(RECNO())
Local aCoors     := FwGetDialogSize( oMainWnd )
Local aFields    := {}
Local aArea	     := T9U->(getArea())
Local oPanel     := Nil
Local oBrowse    := Nil
Local lLoop      := .F.
Local cFiltBrw   := ""
Local cFil       := ""
Local cId	     := ""
Local nRecno     := 0
Local oDlgPrinc  := Nil
Local lLGPDperm := .T. 
Local bXml	 	:= {||TafXmlret(cFunXML,cLayout,cAlias)}
Local bXmlTrab	:= {||TAF421XmlVld(1)}

Private lMenuDif := .T.
Private cAliasHist := cAlias //Alias do browse de historico para utilizacao nas extemporaneas

Default cLayout := ""
Default oBrw 	:= Nil
Default cTitulo := STR0015 + (cAlias)->&( cAlias + "_ID" )


// Bloqueia ou audita de acordo com a fun��o que est� chamando e acesso do usu�rio
// LGPD

lLGPDperm := TafHistLGPD()											 		
If lLGPDperm 
	
	nRecno := (cAlias)->(Recno())
	aWindow := {095,000}
	aColumn := {092,008}

	oDlgPrinc := tDialog():New(aCoors[1] + 100, aCoors[2] + 100,aCoors[3] - 100, aCoors[4] - 100,cTitulo,,,,,CLR_BLACK,CLR_WHITE,,,.T.)

	oAreaHist := FWLayer():New()
	oAreaHist:Init( oDlgPrinc, .F., .T. )
	
	oAreaHist:Init(oDlgPrinc,.F., .F. )

	//Mapeamento da area
	oAreaHist:AddLine("L01",100,.T.)

	//���������Ŀ
	//�Colunas  �
	//�����������
	oAreaHist:AddCollumn("L01C01",aColumn[01],.F.,"L01") //dados
	oAreaHist:AddCollumn("L01C02",aColumn[02],.F.,"L01") //botoes

	//���������Ŀ
	//�Paineis  �
	//�����������
	oAreaHist:AddWindow("L01C01","TEXT","Informa��es",aWindow[01],.F.,.F.,/*bAction*/,"L01",/*bGotFocus*/)
	oPanel	:= oAreaHist:GetWinPanel("L01C01","TEXT","L01")

	oAreaHist:AddWindow("L01C02","L01C02P01","Fun��es",aWindow[01],.F.,.F.,/*bAction*/,"L01",/*bGotFocus*/)
	oAreaBut := oAreaHist:GetWinPanel("L01C02","L01C02P01","L01")


	oButt1 := tButton():New(000,000,"&Sair",oAreaBut,{|| oDlgPrinc:End() }	,oAreaBut:nClientWidth/2,15,,/*oFont*/,,.T.,,,,/*bWhen*/)
	oButt1 := tButton():New(025,000,"&Gerar XML",oAreaBut,IIF(FunName()=="TAFA421",bXmlTrab,bXml),oAreaBut:nClientWidth/2,15,,/*oFont*/,,.T.,,,,/*bWhen*/)

	oBrowse:= FWmBrowse():New()
	oBrowse:SetOwner( oPanel )			
	oBrowse:SetDescription( cTitulo )
	
	If FWIsInCallStack("TAFA421") .And. FWIsInCallStack("XFUNNEWHIS") // Prote��o para error log.
		If Type("cAliasTrb") <> "U" .And. (!FWIsInCallStack("FopenPnTrab") .Or. FWIsInCallStack("XFUNNEWHIS") )// Quando for chamado pelo hist�rico de altera��es
			oBrowse:SetAlias( cAlias )
			dbSelectArea(cAlias)
		EndIf
	Else
		//TRATAMENTO INCLUIDO PARA QUE O BROWSE PRINCIPAL N�O FA�A USO DO ALIAS,
		//CORRIGINDO ASSIM O PROBLEMA DO SETFILTERDEFAULT.
		(cAlias)->(dbCloseArea())	
		dbSelectArea(cAlias)
		(cAlias)->(dbSetOrder(1))
		(cAlias)->(dbGoto(nRecCAli))

		oBrowse:SetAlias( cAlias )
	EndIf	

	//Tratamento para o cadastro do Trabalhador
	If lCadTrab
		If !xTafExtmp()
			oBrowse:AddLegend( "EVENTO == 'I' " 																	, "GREEN"					, "Inclus�o de Cadastro do Trabalhador" ) 		//"Inclu�s�o de Cadastro do Trabalhador"
			oBrowse:AddLegend( "EVENTO == 'I' .AND. !(NOMEVE $('S2200|S2300')) "								, "ORANGE"					, "Inclus�o de Evento de Altera��o" ) 				//"Inclu�s�o de Evento de Altera��o"
			oBrowse:AddLegend( "EVENTO == 'A' "									 								, "YELLOW"					, "Retifica��o de Informa��es do Trabalhador " ) //"Retifica��o de Informa��es do Trabalhador "
			oBrowse:AddLegend( "EVENTO == 'E' "									 								, "RED"      				, "Exclus�o de Evento (Altera��o/Retifica��o)" ) //"Exclus�o de Evento (Altera��o/Retifica��o)"
		Else
			oBrowse:AddLegend( "EVENTO == 'I' .AND. STASEC <> 'E' .AND. (NOMEVE $('S2200|S2300'))"			, "GREEN"					, "Inclus�o de Cadastro do Trabalhador" ) 		//"Inclu�s�o de Cadastro do Trabalhador"
			oBrowse:AddLegend( "EVENTO == 'I' .AND. STASEC <> 'E' .AND. !(NOMEVE $('S2200|S2300')) "		, "ORANGE"					, "Inclus�o de Evento de Altera��o" ) 				//"Inclu�s�o de Evento de Altera��o"
			oBrowse:AddLegend( "EVENTO == 'A' .AND. STASEC <> 'E' "								 				, "YELLOW"					, "Retifica��o de Informa��es do Trabalhador " ) //"Retifica��o de Informa��es do Trabalhador "
			oBrowse:AddLegend( "EVENTO == 'E' .AND. STASEC <> 'E' "								 				, "RED"      				, "Exclus�o de Evento ( Altera��o / Retifica��o )" ) //"Exclus�o de Evento (Altera��o/Retifica��o)"
			oBrowse:AddLegend( "EVENTO == 'I' .AND. STASEC == 'E' "									 			, "BLUE"	      			, "Inclus�o ( Extempor�neo ) " ) 					//"Exclus�o de Evento (Altera��o/Retifica��o)"
			oBrowse:AddLegend( "EVENTO == 'A' .AND. STASEC == 'E' "									 			, "GRAY"      			, "Retifica��o ( Extempor�neo )" ) 				//"Exclus�o de Evento (Altera��o/Retifica��o)"	
			oBrowse:AddLegend( "EVENTO == 'E' .AND. STASEC == 'E' " 											, "VIOLET"      			, "Exclus�o ( Extempor�neo )" ) 					//"Exclus�o( Extempor�neo )"	
		EndIf
		oBrowse:SetColumns(aHeaderT)

	// Historicos Reinf
	ElseIf 	cAlias $ "V1O"	
		cFiltBrw := ""
		If TafColumnPos( cAlias + "_STASEC" )
			cFiltBrw := " .AND. " + cAlias + "_STASEC <> 'E' "
		EndIf
		oBrowse:AddLegend( cAlias + "_EVENTO == 'I' " + cFiltBrw 			, "GREEN"	, "Inclus�o" ) 
		oBrowse:AddLegend( cAlias + "_EVENTO == 'A' " + cFiltBrw			, "YELLOW"	, "Altera��o" )

		cFilter := cAlias + "_FILIAL + " + cAlias + "_ID"
		aFields := xFunGetSX3( cAlias )
		oBrowse:SetFields( aFields )
		oBrowse:SetFilterDefault( cAlias + "_ATIVO == '2' .and. " + cFilter + "=='" + ( cAlias )->&( cFilter ) + "'" )
	Else	
		If xTafExtmp() .And. TafColumnPos( cAlias + "_STASEC" )
			oBrowse:AddLegend( cAlias + "_EVENTO == 'I' .AND. " + cAlias + "_STASEC == 'E' " 			                            	, "BLUE"	   		, "Inclus�o ( Extempor�neo ) " ) 				 	//"Inclus�o( Extempor�neo )"
			oBrowse:AddLegend( "(" + cAlias + "_EVENTO == 'A' .OR. " + cAlias + "_EVENTO == 'R' ) .AND. " + cAlias + "_STASEC == 'E' "	, "GRAY"      	, "Retifica��o / Altera��o ( Extempor�neo )" ) 	//"Retifica��o / Altera��o ( Extempor�neo )"
			oBrowse:AddLegend( cAlias + "_EVENTO == 'E' .AND. " + cAlias + "_STASEC == 'E' " 												, "VIOLET"      	, "Exclus�o ( Extempor�neo )" ) 					//"Exclus�o( Extempor�neo )"		
		Endif
		
		cFiltBrw := ""
		If TafColumnPos( cAlias + "_STASEC" )
			cFiltBrw := " .AND. " + cAlias + "_STASEC <> 'E' "
		EndIf
		If TafColumnPos( cAlias + "_XMLREC" )
			oBrowse:AddLegend( cAlias + "_XMLREC == 'COMP' " + cFiltBrw		, "BROWN" 	, "Afastamento Completo (In�cio e T�rmino)" )
		EndIf	
		oBrowse:AddLegend( cAlias + "_EVENTO == 'I' " + cFiltBrw 			, "GREEN"	, "Inclus�o de Cadastro do Evento" ) 			 	//"Inclus�o de Cadastro do Evento"
		oBrowse:AddLegend( cAlias + "_EVENTO == 'A' " + cFiltBrw			, "YELLOW"	, "Retifica��o das Informa��es do Evento" ) 	  	//"Retifica��o das Informa��es do Evento"
		oBrowse:AddLegend( cAlias + "_EVENTO == 'E' " + cFiltBrw			, "RED"  	, "Exclus�o de Evento (Altera��o/Retifica��o)" )	//"Exclus�o de Evento (Altera��o/Retifica��o)"
		oBrowse:AddLegend( cAlias + "_EVENTO == 'R' " + cFiltBrw			, "WHITE" 	, "Afastamento Retificado" )
		oBrowse:AddLegend( cAlias + "_EVENTO == 'F' " + cFiltBrw			, "BLACK" 	, "T�rmino do Afastamento" )
		
		cFilter := cAlias + "_FILIAL + " + cAlias + "_ID"
		cFil := cAlias + "_FILIAL"
		cId		:= cAlias + "_ID"
		aFields := xFunGetSX3( cAlias )
		oBrowse:SetFields( aFields )
		oBrowse:SetFilterDefault(cAlias + "_ATIVO == '2' .AND. " +cAlias + "_FILIAL == '" + (cAlias)->&( cFil ) + "' .AND. " + cAlias + "_ID == '" + (cAlias)->&( cId ) + "'" )
	Endif

	oBrowse:SetMenuDef( cRotina )
	oBrowse:SetProfileID( '1' )
	oBrowse:DisableDetails()

	oBrowse:Activate()

	oDlgPrinc:Activate(,,,.T.,/*valid*/,,/*On Init*/)
	RestArea(aArea)
	RestArea(aAreaAlias)
	oBrowse:Deactivate()
	oBrowse:Destroy()
	oBrowse:= NIL
 	If oBrw != Nil
		oBrw:SetAlias(cAlias)//inclus�o para suprir a quest�o do dbclosearea incluido para corre��o do filtro.
	 	oBrw:SetFilterDefault(TAFBrwSetFilter(cAlias,cRotina,cEvento)) 
		If nRecno > 0 
			oBrw:GoTo(nRecno, .T.)
		EndIf
	EndIf 
EndIf 

Return( lLoop )

//-------------------------------------------------------------------
/*/{Protheus.doc} TafHistLGPD
Verifica se contem dados senss�veis e tamb�m verifica se o usuario tem acesso aos dados, se sim o log
� auditado. se n�o a tela(Ex: Historico) � bloqueada.
@author  TOTVS
@since   14/01/2020
@version 1.0
/*/
//-------------------------------------------------------------------

Static Function TafHistLGPD()
	Local aArea		:= GetArea()
	Local aHistAlt 	:= {}
	Local lRet		:= .T.
	Local lAccUsr	:= .T.
	Local nPosTaf   := 0

	aHistAlt := TAFRotinas()

	If Len(aHistAlt) > 0
		nPosTaf := aScan(aHistAlt,{|x| x[1] == FunName()})
		If  nPosTaf > 0 .And. Len(aHistAlt[nPosTaf]) > 16 .And. aHistAlt[nPosTaf][17]  // verifica se contem dados senss�veis
			If ( lRet	:= IIf(FindFunction("PROTDATA"),ProtData(),.T.)  )
				IIf (FindFunction('FwPDLogUser'),FwPDLogUser('XFUNHISALT'),.T.)
			EndIf
		EndIf
	EndIf

	RestArea(aArea)

Return lRet

//---------------------------------------------------------------------
/*/{Protheus.doc} TafXmlExtp
Fun��o que exporta xml a partir da tela de historico, no registro posicionado. 

@Author Roberto Souza
@Since	28/01/2018
@Version 1.0

@param -> Nil

@return ->Nil
/*/
//---------------------------------------------------------------------
Function TafXmlExtp()

Local cIdEven		:= "" 
Local cStatsEve 	:= ""
Local cAliasExp	:= ""

Local cEvento		:= ""
Local cLayout		:= ""
Local cPath		:= ""

Local cXmlExp		:= ""
Local cFuncExp  	:= ""

Local nRecnoExp	:= 0
Local aEvts2    	:= {}

Local lExtemp		:= .F.

//Verifico se o evento � Extempor�neo
If Len(cAliasHist) > 3
	lExtemp := (cAliasHist)->STASEC == "E"
ElseIf TafColumnPos( cAliasHist + "_STASEC" )
	lExtemp := (cAliasHist)->&((cAliasHist) + "_STASEC") == "E"
EndIf

If !lExtemp

	Aviso(STR0001,STR0006,{STR0003}) // ("e-Social","� permitido realizar esta opera��o somente para eventos extempor�neos.",{"Ok"})	
		
Else

	If Type( cAliasHist ) <> "C"
		
		If FunName() == "TAFA421"
			cIdEven	:= (cAliasHist)->ID
			cStatsEve	:= (cAliasHist)->STATUS  
			cAliasExp	:= (cAliasHist)->ALIASTAB
			nRecnoExp	:= (cAliasHist)->RECNO
		Else
			cIdEven	:= (cAliasHist)->&((cAliasHist) + "_ID" )
			cStatsEve	:= (cAliasHist)->&((cAliasHist) + "_STATUS" )  
			cAliasExp	:= cAliasHist
			nRecnoExp	:= (cAliasHist)->(Recno())
		EndIf
	
		aEvts2    	:= TAFRotinas(cAliasExp,3,.F.,2)
		cFuncExp	:= aEvts2[8]
		cEvento	:= aEvts2[4]
	
		If !Empty( cFuncExp )
			bFunExp := &('{|| cXmlExp := '+ cFuncExp +'( cAliasExp, nRecnoExp,,.T. )}')
			Eval( bFunExp )
	
			If !Empty(cXmlExp)
				cLayout	:= Substr(cEvento,3)
				cPath		:= cGetFile( "Diret�rio"�+�"|*.*" ,�"Procurar" ,�0, ,�.T. ,�GETF_LOCALHARD�+�GETF_RETDIRECTORY ,�.T.�)
				
				If !Empty(cPath)
					cMsgRet := xTafGerXml( cXmlExp, cLayout, cPath, .F., .F., 0 )
					Aviso(STR0010,cMsgRet,{STR0003},2) //"Exportar Xml" # OK
				EndIf
			Else
				Aviso(STR0010, STR0011 + cEvento + STR0012 ,{STR0003},2) //"Exportar Xml"#"Exporta��o do evento"#"n�o foi executada."#OK 		
			EndIf
		Else
			Aviso(STR0010, STR0011 + cEvento + STR0013 ,{STR0003},2) //"Exportar Xml","Exporta��o do evento " + cEvento + " n�o est� habilitada." ,{"Ok"}
		EndIf
	Else
		Aviso(STR0010, STR0014 ,{STR0003},2) //"Exportar Xml","Esta rotina deve ser executada somente da visualiza��o do hist�rico de eventos." ,{"Ok"}
	EndIf
	
EndIf

Return




