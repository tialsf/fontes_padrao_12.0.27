#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "APWIZARD.CH"
#INCLUDE "FILEIO.CH"
#INCLUDE "TopConn.CH"

Static lFindClass := FindFunction("TAFFindClass") .And. TAFFindClass( "FWCSSTools" ) // Por causa de atualiza��o de Lib, verifica se existe a fun��o FindClass e com a fun��o verifica se existe a classe FWCSSTools

//------------------------------------------------------------------
/*/{Protheus.doc} TAFFILESOC

Fonte gen�rico para filtros personalizados do eSocial

@author Eduardo Sukeda
@since 12/02/2019
@version 1.0
/*/
//------------------------------------------------------------------


//--------------------------------------------------------------------
/*/{Protheus.doc} FilCpfNome
Fun��o responsavel por filtrar os eventos por CPF

@param oBrowse  , Objeto  , Objeto do Browse onde ser� aplicado o filtro.
@param cAlias   , Caracter, Alias do browse onde ser� executado o filtro.
@param cEvento  , Caracter, Evento relacionado ao browse.
@param nTpData  , Numerico, Indica o formato do campo data: 1 = AAAA/MM, 2 = DD/MM/AAAA; 3 = MM/AAAA
@param cCpoData1, Caracter, Indica o campo correspondente � data/per�odo que ser� utilizado no filtro por per�odo.
@param cCpoData2, Caracter, Indica o campo correspondente � data/per�odo final (se houver) que ser� utilizado no filtro por per�odo.

@author Eduardo Sukeda
@since 12/02/2019
@version 1
/*/
//--------------------------------------------------------------------
Function FilCpfNome(oBrowse, cAlias, cEvento, nTpData, cCpoData1, cCpoData2 )

Local cPerApu		:= Space(6)
Local dPerIni		:= SToD( "  /  /    " )
Local dPerFim		:= SToD( "  /  /    " )
Local cCpf			:= Space(11)
Local cNome			:= Space(70)
Local aItems		:= {}

Local oDlg			:=	Nil

Local nRadio		:= 1
Local nHeightBox	:= 0
Local nWidthBox		:= 0
Local nTop			:= 0
Local nHeight		:= 290
Local nWidth		:= 590
Local nPosIni		:= 0
Local cDtIni        := ""
Local cDtFim        := ""

Local bVldCpf       := {||TafVldCpf(cCpf)}
Local bVldNom       := {||IIF(!Empty(cNome) .And. !Empty(cCpf),Alert("Por gentileza preencha somente o CPF ou o Nome para realizar o filtro."),IIF(!Empty(cNome) .And. Len(AllTrim(cNome)) <= 2,;
						  Alert("Quantidade m�nima de caracteres para a busca � de 3, por gentileza refine melhor seu filtro."),))}
Local lHtml			:= Iif(lFindClass,FWCSSTools():GetInterfaceCSSType() == 5,.F.)

Default nTpData     := 1
Default cCpoData1   := cAlias + "_PERAPU"
Default cCpoData2   := ""

oBrowse:DeleteFilter("CPFFilter")

oDlg := MsDialog():New( 0, 0, nHeight, nWidth, "Filtro CPF/Nome",,,,,,,,, .T. )

nHeightBox := ( nHeight - 60 ) / 2
nWidthBox := ( nWidth - 20 ) / 2

@10,10 To nHeightBox,nWidthBox of oDlg Pixel

nTop := 20
TSay():New( nTop, 20, { || '<font size="' + IIF(lHtml,'2','3') + '" color="#000000"><b>Informe o CPF, Per�odo ou Nome do funcion�rio para realizar o filtro:</b></font><br/>' }, oDlg,,,,,, .T.,,, 250, 020,,,,,, .T. )
nTop += 30
TGet():New( nTop, 20  ,{ |x| If( PCount() == 0,  cCpf, cCpf := x ) }      , oDlg, 65 , 10, "@R 999.999.999-99",bVldCpf,,,,,, .T.,,,,,,,,,"C9VE",,,,,.T.,,, '<font size="' + IIF(lHtml,'2','3') + '" color="#0c9abe"><b>CPF</b></font><br/>'    , 1 )

If cAlias == 'CM6' .Or. nTpData == 2
	TGet():New( nTop, 100,{|x| If( PCount() == 0,  cNome, cNome := x ) }    , oDlg, 160, 10,                    ,bVldNom,,,,,, .T.,,,,,,,,,      ,,,,,   ,,, '<font size="' + IIF(lHtml,'2','3') + '" color="#0c9abe"><b>Nome</b></font><br/>', 1 )
	
	If cAlias == "CM6"
		cDtIni := 'Data In�cio'
		cDtFim := 'Data T�rmino'
	Else
		cDtIni := 'Data De'
		cDtFim := 'Data At�'
	EndIf
	
	TGet():New( 80, 20  ,{ |x| If( PCount() == 0,  dPerIni, dPerIni := x ) }, oDlg, 65 , 10, "@D"        ,,,,,,, .T.,,,,,,,,,,,,,,.T.,,, '<font size="' + IIF(lHtml,'2','3') + '" color="#0c9abe"><b>' + cDtIni + '</b></font><br/>', 1 )
	TGet():New( 80, 100 ,{ |x| If( PCount() == 0,  dPerFim, dPerFim := x ) }, oDlg, 65 , 10, "@D"        ,,,,,,, .T.,,,,,,,,,,,,,,.T.,,, '<font size="' + IIF(lHtml,'2','3') + '" color="#0c9abe"><b>' + cDtFim + '</b></font><br/>', 1 )
	
	If cAlias == "CM6"
		aItems := {'Pesquisar Intervalo','Pesquisar Data Exata'}
		oRadio := TRadMenu():New (80,180,aItems,,oDlg,,,,,,,,100,12,,,,.T.)
		oRadio:bSetGet := {|u|Iif (PCount()==0,nRadio,nRadio:=u)}
	EndIf
Else
	TGet():New( nTop, 120 ,{ |x| If( PCount() == 0,  cPerApu, cPerApu := x ) }, oDlg, 65 , 10, "@R 99/9999",,,,,,, .T.,,,,,,,,,,,,,,   ,,, '<font size="' + IIF(lHtml,'2','3') + '" color="#0c9abe"><b>Per�odo</b></font><br/>', 1 )
	TGet():New( 80,   20  ,{ |x| If( PCount() == 0,  cNome, cNome := x ) }    , oDlg, 200, 10,                    ,bVldNom,,,,,, .T.,,,,,,,,,      ,,,,,   ,,, '<font size="' + IIF(lHtml,'2','3') + '" color="#0c9abe"><b>Nome</b></font><br/>', 1 )
EndIf

nTop += 20

nPosIni := ( ( nWidth - 20 ) / 2 ) - 64
SButton():New( nHeightBox + 10, nPosIni, 1, { ||IIF(TafBtnFil(oBrowse, cCpf, cPerApu, cNome, cAlias, dPerIni, dPerFim, cEvento, nRadio, nTpData, cCpoData1, cCpoData2 ),oDlg:End(),) }, oDlg )
nPosIni += 32
SButton():New( nHeightBox + 10, nPosIni, 2, { || oDlg:End() }, oDlg )

oDlg:Activate( ,,,.T. )

Return()

//--------------------------------------------------------------------
/*/{Protheus.doc} TafFilter
Fun��o respons�vel por filtrar os eventos por CPF

@author Eduardo Sukeda
@since 12/02/2019
@version 1
/*/
//--------------------------------------------------------------------
Function TafFilter(oBrowse, cCpf, cPerApu, cNome, cAlias, dPerIni, dPerFim, cEvento, nRadio, nTpData, cCpoData1, cCpoData2 )

Local cNomeFil	:= ""
Local cFiltro 	:= ""
Local cIdFilter	:= "CPFFilter"
Local aFunc	    := {} 
Local cQtdReg   := "" 
Local aEvento   := {}
Local cCpoTrab  := ""
Local nX        := 0
Local lRet      := .T.
Local cFilSql   := ""
Local nPosFil   := 0

Local cPerIni   := ""
Local cPerFim   := ""

Local cTpEvt	:= ""
Default cEvento := ""

// Trecho implementado para garantir que n�o haja impacto no filtro pro TAFA250, TAFA261 e TAFA407, pois no momento eles ainda n�o informam o evento para a fun��o de filtro.
If ValType(cEvento) == "U" .Or. Empty(cEvento)
	Do Case
		Case cAlias == "C91"
			cEvento := "S-1200"
		Case cAlias == "T3P"
			cEvento := "S-1210"
	EndCase
EndIf

aEvento  := TAFRotinas( cEvento, 4, .F., 2 )
cCpoTrab := aEvento[11]
cTpEvt   := aEvento[12]

aFunc	 := FilIdFunc(cCpf,cAlias,cCpoTrab,cTpEvt)

If Empty(cCpoData1)
	cCpoData1   := cAlias + "_PERAPU"
EndIf

If nTpData == 1
	cPerApu  := Substr(cPerApu, 3)    + Substr(cPerApu, 1, 2)
EndIf

cPerIni  := DtoS(dPerIni)
cPerFim  := DtoS(dPerFim)
cNome    := AllTrim(Upper( cNome )) 

If !Empty(cNome)
	cQtdReg  := CountNm(cNome,cAlias,cCpoTrab,cPerApu,cPerIni,cPerFim,nTpData,cCpoData1,cCpoData2,nRadio,cTpEvt)
EndIf

If !Empty(cNome) .AND. cQtdReg >= 1
    cNomeFil := TafFilNm(cNome, cAlias, cCpoTrab, cPerIni, cPerFim, nRadio,cTpEvt)
EndIf

If !Empty(aFunc) .And. AllTrim(aFunc[1][1]) <> ""
    If !Empty(cCpf)
       
        cFiltro += "( "
        
        // Filtro por CPF
		If !Empty(cCpf) .And. cAlias $ "C91|T3P" // Apenas esses eventos possuem a possibilidade de grava��o direta de CPF, por conta do cen�rio de m�ltiplos v�nculos.
		    cFiltro += "(" + cAlias + "_CPF == '" + cCpf + "') .OR. "
		EndIf
        
        For nX := 1 To Len( aFunc )
            If nX >= 2
                cFiltro += " .OR. "
            EndIf
            If cTpEvt == "T" 
				If cAlias == 'T2M'							
					cFiltro	+= " T2M_CPFTRB == '" + aFunc[nX][3] + "' "
				ELSeIf cAlias == 'T2G'							
					cFiltro	+= " T2G_CPFTRA == '" + aFunc[nX][3] + "' "
				Else
					cFiltro += "(" + cAlias + "_FILIAL == '"+ aFunc[nX][1] + "' .AND. " + cAlias + "_CPF == '" + aFunc[nX][3] + "')"
				EndIf
            Else
            	cFiltro += "(" + cAlias + "_FILIAL == '"+ aFunc[nX][1] + "' .AND. " + cCpoTrab + " == '"   + aFunc[nX][2] + "')"
            EndIf
        Next nX
        cFiltro +=")"
    EndIf
EndIf

If !Empty(cPerApu) .OR. !Empty(cPerIni) .OR. !Empty(cPerFim)
    If !Empty(cCpf) .And. AllTrim(aFunc[1][1]) <> "" .And. Empty(cNomeFil)
        cFiltro += " .AND. "
    ElseIf !Empty(cCpf) .And. AllTrim(aFunc[1][1]) <> "" .And. !Empty(cNomeFil)
        cFiltro += " .AND. "
    ElseIf Empty(cCpf) .And. AllTrim(aFunc[1][1]) == "" .And. !Empty(cNomeFil)
        cFiltro += ""
    ElseIf !Empty(cCpf) .And. AllTrim(aFunc[1][1]) <> "" .And. Empty(cNomeFil)
        cFiltro += " .OR. "
    ElseIf !Empty(cCpf) .And. AllTrim(aFunc[1][1]) == "" .And. !Empty(cNomeFil)
        cFiltro += ""
    ElseIf !Empty(cCpf) .And. AllTrim(aFunc[1][1]) == "" .And. Empty(cNomeFil)
        cFiltro += " .AND. "
    ElseIf Empty(cCpf) .And. AllTrim(aFunc[1][1]) <> "" .And. Empty(cNomeFil)   
        cFiltro += " .AND. "
    ElseIf Empty(cCpf) .And. AllTrim(aFunc[1][1]) <> "" .And. !Empty(cNomeFil)   
        cFiltro += ""
    EndIf
    
	If !Empty(cPerApu)
	
		If Empty(cCpoData1)
			cCpoData1   := cAlias + "_PERAPU"
		EndIf
		
    	cFiltro += "(" + cCpoData1 + " == '" + cPerApu + "')"
    EndIf
    
    If cAlias $ 'CM6'
    	If (!Empty(cNome) .AND. cQtdReg >= 1) .OR. Empty(cNome)
    	
	    	If nRadio == 1	
			    If !Empty(cPerIni) .AND. Empty(cPerFim)
			    	cFiltro += "(" + cAlias + "_DTAFAS >= '" + cPerIni + "') .AND. (" + cAlias + "_DTAFAS <> '')"
			    ElseIf Empty(cPerIni) .AND. !Empty(cPerFim)
			    	cFiltro += "(" + cAlias + "_DTFAFA <= '" + cPerFim + "') .AND. (" + cAlias + "_DTFAFA <> '')"
			    ElseIf !Empty(cPerIni) .AND. !Empty(cPerFim)
			    	cFiltro += "(" + cAlias + "_DTAFAS >= '" + cPerIni + "' .AND. " + cAlias + "_DTAFAS <= '" + cPerFim + "') .AND. (" + cAlias + "_DTFAFA >= '" + cPerIni + "'.AND. " + cAlias + "_DTFAFA <= '" + cPerFim + "')"
			    EndIf
		    ElseIf nRadio == 2
		    	If !Empty(cPerIni) .AND. Empty(cPerFim)
			    	cFiltro += "(" + cAlias + "_DTAFAS == '" + cPerIni + "')"
			   	ElseIf Empty(cPerIni) .AND. !Empty(cPerFim)
		    		cFiltro += "(" + cAlias + "_DTFAFA == '" + cPerFim + "')"
		    	ElseIf !Empty(cPerIni) .AND. !Empty(cPerFim)
		    		cFiltro += "(" + cAlias + "_DTAFAS == '" + cPerIni + "') .AND. (" + cAlias + "_DTFAFA == '" + cPerFim + "')"
		    	EndIf
		    EndIf
		    
		EndIf
	
	ElseIf nTpData == 2
	
		If !Empty(cPerIni) .AND. Empty(cPerFim)
	    	cFiltro += "(" + cCpoData1 + " >= '" + cPerIni + "')"
	    ElseIf Empty(cPerIni) .AND. !Empty(cPerFim)
	    	cFiltro += "(" + cCpoData1 + " <= '" + cPerFim + "')"
	    ElseIf !Empty(cPerIni) .AND. !Empty(cPerFim)
	    	cFiltro += "(" + cCpoData1 + " >= '" + cPerIni + "' .AND. " + cCpoData1 + " <= '" + cPerFim + "')"
	    EndIf
	  
	EndIf
    
EndIf

If !Empty(cNomeFil) .And. Empty(cCpf)
	If !Empty(cFiltro)
		cFiltro += " .AND. "
	EndIf
	cFiltro += (cNomeFil)
EndIf

If  Empty(cCpf) .And. !Empty(cNome) .And. cQtdReg >= 30
    MsgAlert("A sua busca retornou uma quantidade muito grande de registros, por gentileza refine seu filtro.", "Muitos registros")
	lRet := .F.
ElseIf Empty(cFiltro)
	MsgAlert("O filtro utilizado para realizar a busca por trabalhador n�o retornou nenhum resultado. Por gentileza, refa�a utilizando novos par�metros.","Busca sem retorno")
	lRet := .F.
ElseIf !Empty(cFiltro)
	oBrowse:AddFilter("Filtro por CPF",cFiltro,,.T.,,,,cIdFilter)

	nPosFil := aScan(oBrowse:oFWFilter:aFilter,{|x| x[9] == cIdFilter })
	cFilSql := cFiltro
	cFilSql := StrTran( Upper(cFilSql), ".OR." , "OR"  )
	cFilSql := StrTran( Upper(cFilSql), ".AND.", "AND" )
	cFilSql := StrTran( Upper(cFilSql), "=="   , "="   )

	//Essa posi��o do array � preenchida para que ao utilizar a gera��o de XML em lote, o sistema consiga utilizar o filtro aplicado ao Browse para filtrar os XMLs a serem gerados.
	oBrowse:oFWFilter:aFilter[nPosFil,3] := cFilSql 

    FWMsgRun(, {|| oBrowse:ExecuteFilter( .T. ) }, "Filtro CPF/Nome", "Aplicando Filtro...")
EndIf

Return lRet

//--------------------------------------------------------------------
/*/{Protheus.doc} TafGetCPF
Fun��o respons�vel por posicionar o CPF do funcion�rio a partir do ID do mesmo

@author Eduardo Sukeda
@since 13/02/2019
@version 1
/*/
//--------------------------------------------------------------------
Function TafGetCPF(cFil, cIdTrab, cCpf, cAlias)

Local aArea  := GetArea()
Local cRet   := ""

Default cCPF := ""

If Empty(cCpf)
	cRet := Posicione("C9V",2,cFil+cIdTrab+"1","C9V_CPF")
Else
	cRet := cCpf
EndIf

RestArea(aArea)

Return cRet

//--------------------------------------------------------------------
/*/{Protheus.doc} TafGetNIS
Fun��o respons�vel por posicionar o NIS do funcion�rio a partir do ID do mesmo

@author Eduardo Sukeda
@since 13/02/2019
@version 1
/*/
//--------------------------------------------------------------------
Function TafGetNIS(cFil, cIdTrab, cAlias, cCPFTrab)

Local aArea  	:= GetArea()
Local cNisC9V   := ""
Local cRet      := ""

Default cIdTrab		:= ""
Default cCPFTrab	:= ""


//Busca pelo CPF
If Empty(cCPFTrab)
	cCPFTrab := C9V->C9V_CPF
EndIf
// Caso ainda n�o possua Id do trabalhador, busca essa informa��o
// pelo CPF ordernando por ativo
If Empty(cIdTrab) .And. !Empty(cCPFTrab)
	cIdTrab := Posicione("C9V",3,cFil + cCPFTrab,"C9V_ID")	//C9V_FILIAL+C9V_CPF+C9V_ATIVO
EndIf
	
If !Empty(cIdTrab)
	cNisC9V := Posicione("C9V",2,cFil+cIdTrab+"1","C9V_NIS")
EndIf

cRet := TAF250Nis( cFil, cIdTrab, cNisC9V )

RestArea(aArea)

Return cRet

//--------------------------------------------------------------------
/*/{Protheus.doc} TafGetNome
Fun��o respons�vel por posicionar o NIS do funcion�rio a partir do ID do mesmo

@author Eduardo Sukeda
@since 13/02/2019
@version 1
/*/
//--------------------------------------------------------------------
Function TafGetNome(cFil,cIdTrab, cNome, cAlias,cCPFTrab)

Local aArea  := GetArea()
Local cRet 	 := ""

Default cCPFTrab	:= ""
Default cIdTrab		:= ""
Default cNome		:= ""

If Empty(cNome)
	If Empty(cIdTrab) .And. !Empty(cCPFTrab)
		cIdTrab := Posicione("C9V", 3, cFil + cCPFTrab + "1", "C9V_ID")
	EndIf
	
	cRet := TAFNmTrab(cFil,cIdTrab)
Else
	cRet := cNome
EndIf

RestArea(aArea)

Return cRet

//--------------------------------------------------------------------
/*/{Protheus.doc} TafGetMatr
Fun��o respons�vel por posicionar a(as) Matricula(as) do(s) funcion�rio(s) 
a partir do ID do trabalhador/Id da C91 e Vers�o da C91

@author Eduardo Sukeda
@since 13/02/2019
@version 1
/*/
//--------------------------------------------------------------------
Function TafGetMatr( cFil, cIdTrab, cIdTab, cVerTab, cCpf, cAlias, cCPFTrab )

Local aArea  	:= GetArea()
Local cRet      := ""
Local cMatric   := ""
Local cAux      := ""
Local cSelect	:= ""
Local cFrom		:= ""
Local cWhere	:= ""
Local cAliasQry := GetNextAlias()
Local nX        := 1
Local nTotReg	:= 0

Default cFil    := cFilAnt
Default cIdTrab := ""
Default cIdTab  := ""
Default cVerTab := ""
Default cCpf    := ""
Default cAlias  := ""
Default cCPFTrab:= ""

If cAlias == "C91"

    If !Empty(cIdTrab)
        If C9V->C9V_NOMEVE == "S2200"
            cRet := Posicione("C9V",2,cFil+cIdTrab+"1","C9V_MATRIC")
        ElseIf C9V->C9V_NOMEVE == "S2300"
            cRet := "TRABALHADOR SEM V�NCULO"        
        ElseIf C9V->C9V_NOMEVE == "TAUTO"
            cRet := "TRABALHADOR AUT�NOMO"
        EndIf
    Else

    cSelect := "DISTINCT T14.T14_IDEDMD, T14.T14_CODCAT, C9L.C9L_DTRABA "
    cFrom   := RetSqlName( "T14" ) + " T14 "
    cFrom   += "LEFT JOIN "
    cFrom   += "    " + RetSqlName( "C9K" ) + " C9K  "
    cFrom   += "    ON  T14.T14_FILIAL = C9K.C9K_FILIAL "
    cFrom   += "    AND T14.T14_ID = C9K.C9K_ID  "
    cFrom   += "    AND T14.T14_VERSAO = C9K.C9K_VERSAO  "
    cFrom   += "    AND T14.T14_IDEDMD = C9K.C9K_RECIBO  "
    cFrom   += "    AND C9K.D_E_L_E_T_ = ' ' "
    cFrom   += "LEFT JOIN "
    cFrom   += "    " + RetSqlName( "C9L" ) + " C9L  "
    cFrom   += "    ON C9K.C9K_FILIAL = C9L.C9L_FILIAL "
    cFrom   += "    AND C9K.C9K_ID = C9L.C9L_ID "
    cFrom   += "    AND C9K.C9K_VERSAO = C9L.C9L_VERSAO "
    cFrom   += "    AND C9K.C9K_ESTABE = C9L.C9L_ESTABE "
    cFrom   += "    AND C9K.C9K_LOTACA = C9L.C9L_LOTACA "
    cFrom   += "    AND C9K.C9K_CODLOT = C9L.C9L_CODLOT "
    cFrom   += "    AND C9K.C9K_RECIBO = C9L.C9L_RECIBO "
    cFrom   += "    AND C9L.D_E_L_E_T_ = ' ' "
    cWhere  := "C9L.C9L_ID = '" + cIdTab + "' "
    cWhere  += "AND C9L.C9L_VERSAO = '" + cVerTab + "' "
    cWhere  += "AND T14.D_E_L_E_T_ = ' ' "

    cSelect := "%" + cSelect + "%"
	cFrom   := "%" + cFrom + "%"
	cWhere  := "%" + cWhere + "%"

	BeginSql Alias cAliasQry
		SELECT
			%Exp:cSelect%
		FROM
			%Exp:cFrom%
		WHERE
			%Exp:cWhere%
	EndSql

        While ( cAliasQry )->(!Eof())

            If !(AllTrim(( cAliasQry )->T14_CODCAT) $ ('000001|000002|000003|000004|000005|000006|000049'))

                If nX == 1
                    cAux := "TRABALHADOR SEM V�NCULO " 
                Else
                    cAux := "| TRABALHADOR SEM V�NCULO"
                EndIf
            Else

                If nX == 1
                    cAux := AllTrim(( cAliasQry )->C9L_DTRABA) + " " 
                Else
                    cAux := "| " + AllTrim(( cAliasQry )->C9L_DTRABA)
                EndIf
            EndIf

            cMatric := AllTrim(cMatric) + Space(1) + cAux
            ( cAliasQry )->(DbSkip())
            nX++
        EndDo
        
        ( cAliasQry )->( DbCloseArea() )
        
        cRet := cMatric

    EndIf

ElseIf cAlias == "T3P"

    If !Empty(cIdTrab)
        If C9V->C9V_NOMEVE == "S2200"
            cRet := Posicione("C9V",2,cFil+cIdTrab+"1","C9V_MATRIC")
        ElseIf C9V->C9V_NOMEVE == "S2300"
            cRet := "TRABALHADOR SEM V�NCULO"        
        ElseIf C9V->C9V_NOMEVE == "TAUTO"
            cRet := "TRABALHADOR AUT�NOMO"
        EndIf
    Else

    cSelect := "DISTINCT T14.T14_IDEDMD, T14.T14_CODCAT, C9L.C9L_DTRABA "
    cFrom   := RetSqlName( "T3P" ) + " T3P "
    cFrom   += " LEFT JOIN "
    cFrom   += "     " + RetSqlName( "C91" ) + " C91  "
    cFrom   += "     ON T3P.T3P_FILIAL = C91.C91_FILIAL "
    cFrom   += "     AND T3P.T3P_BENEFI = C91.C91_TRABAL  "
    cFrom   += "     AND T3P.T3P_INDAPU = C91.C91_INDAPU  "
    cFrom   += "     AND T3P.T3P_PERAPU = C91.C91_PERAPU  "
    cFrom   += "     AND T3P.T3P_CPF = C91.C91_CPF  "
    cFrom   += "     AND T3P.T3P_ATIVO = C91.C91_ATIVO  "
    cFrom   += "     AND C91.D_E_L_E_T_ = ' ' "
    cFrom   += "     LEFT JOIN "
    cFrom   += "         " + RetSqlName( "T14" ) + " T14 "
    cFrom   += "         ON C91.C91_FILIAL = T14.T14_FILIAL "
    cFrom   += "         AND C91.C91_ID = T14.T14_ID "
    cFrom   += "         AND C91.C91_VERSAO = T14.T14_VERSAO "
    cFrom   += "         AND T14.D_E_L_E_T_ = ' ' "
    cFrom   += "     LEFT JOIN "
    cFrom   += "         " + RetSqlName( "C9K" ) + " C9K "
    cFrom   += "         ON  T14.T14_FILIAL = C9K.C9K_FILIAL "
    cFrom   += "         AND T14.T14_ID = C9K.C9K_ID "
    cFrom   += "         AND T14.T14_VERSAO = C9K.C9K_VERSAO "
    cFrom   += "         AND T14.T14_IDEDMD = C9K.C9K_RECIBO "
    cFrom   += "         AND C9K.D_E_L_E_T_ = ' ' "
    cFrom   += "     LEFT JOIN "
    cFrom   += "         " + RetSqlName( "C9L" ) + " C9L "
    cFrom   += "         ON C9K.C9K_FILIAL = C9L.C9L_FILIAL "
    cFrom   += "         AND C9K.C9K_ID = C9L.C9L_ID "
    cFrom   += "         AND C9K.C9K_VERSAO = C9L.C9L_VERSAO "
    cFrom   += "         AND C9K.C9K_ESTABE = C9L.C9L_ESTABE "
    cFrom   += "         AND C9K.C9K_LOTACA = C9L.C9L_LOTACA "
    cFrom   += "         AND C9K.C9K_CODLOT = C9L.C9L_CODLOT "
    cFrom   += "         AND C9K.C9K_RECIBO = C9L.C9L_RECIBO "
    cFrom   += "        AND C9L.D_E_L_E_T_ = ' ' "
    cWhere  := "T3P.T3P_CPF = '" + cCpf + "' "
    cWhere  += "AND T3P.D_E_L_E_T_ = ' ' "

    cSelect := "%" + cSelect + "%"
	cFrom   := "%" + cFrom + "%"
	cWhere  := "%" + cWhere + "%"

	BeginSql Alias cAliasQry
		SELECT
			%Exp:cSelect%
		FROM
			%Exp:cFrom%
		WHERE
			%Exp:cWhere%
	EndSql

    While ( cAliasQry )->(!Eof())

		If !Empty(AllTrim(( cAliasQry )->T14_CODCAT)) .And. !(AllTrim(( cAliasQry )->T14_CODCAT) $ ('000001|000002|000003|000004|000005|000006|000049'))

			If nX == 1
				cAux := "TRABALHADOR SEM V�NCULO " 
			Else
				cAux := "| TRABALHADOR SEM V�NCULO"
			EndIf
		Else

			If !Empty(AllTrim(( cAliasQry )->C9L_DTRABA)) .And. nX == 1
				cAux := AllTrim(( cAliasQry )->C9L_DTRABA) + " " 
			Else
				If !Empty(AllTrim(( cAliasQry )->C9L_DTRABA))
					cAux := "| " + AllTrim(( cAliasQry )->C9L_DTRABA)
				EndIf
			EndIf
		EndIf

        cMatric := AllTrim(cMatric) + Space(1) + cAux
        ( cAliasQry )->(DbSkip())
        nX++
    EndDo

    ( cAliasQry )->( DbCloseArea() )

    cRet := cMatric

    EndIf
ElseIf cAlias $ "T2M|T2G|V2P"
	BeginSql alias cAliasQry
		SELECT 	 DISTINCT C9V_MATRIC
				,C9V_NOMEVE
		FROM %Table:C9V% C9V
		WHERE	C9V_CPF = %Exp:cCPFTrab%
			AND C9V.%NotDel%
	EndSql

	cRet := ""

	While (cAliasQry)->(!EOF())
		cRet += IIF(!Empty(cRet),' | ','')

		If !Empty( (cAliasQry)->C9V_MATRIC )
			cRet += AllTrim( (cAliasQry)->C9V_MATRIC )
		
		ElseIf (cAliasQry)->C9V_NOMEVE == 'S2300'
			cRet += "TRABALHADOR SEM V�NCULO" 
		ElseIf (cAliasQry)->C9V_NOMEVE == "TAUTO"
            cRet += "TRABALHADOR AUT�NOMO"
		EndIf

	(cAliasQry)->(DbSkip())
	EndDo

	(cAliasQry)->(DbCloseArea())
Else

    If !Empty(cIdTrab)
        If C9V->C9V_NOMEVE == "S2200"
            cRet := Posicione("C9V",2,cFil+cIdTrab+"1","C9V_MATRIC")
        ElseIf C9V->C9V_NOMEVE == "S2300"
            cRet := "TRABALHADOR SEM V�NCULO"        
        ElseIf C9V->C9V_NOMEVE == "TAUTO"
            cRet := "TRABALHADOR AUT�NOMO"
        EndIf
    EndIf

EndIf

RestArea(aArea)

Return cRet

//--------------------------------------------------------------------
/*/{Protheus.doc} TafFilNm
Fun��o respons�vel por retornar todos os id's com o nome inserido na dialog

@author Eduardo Sukeda
@since 06/03/2019
@version 1
/*/
//--------------------------------------------------------------------
Function TafFilNm(cNome, cAlias, cCpoTrab, cPerIni, cPerFim, nRadio,cTpEvt)

Local cRet      := ""
Local cSelect	:= ""
Local cFrom		:= ""
Local cWhere	:= ""
Local cAliasQry := GetNextAlias()
Local cCpoNome  := ""

Local nTotReg   := 0
Local nX        := 1

Default cTpEvt  := ""
If cAlias == "T3P"
	cCpoNome := "T3P_NOMER"
ElseIf cAlias $ "C91"
	cCpoNome := cAlias + "_NOME"
EndIf

cSelect := "DISTINCT C9V.C9V_FILIAL, C9V.C9V_ID, C9V.C9V_CPF  "
cFrom   := RetSqlName( "C9V" ) + " C9V "
cFrom   += " JOIN "
cFrom   += "    " + RetSqlName( cAlias ) + " " + cAlias + "  "
If cTpEvt == "T"
	If cAlias == "T2M"
		cFrom   += "    ON  C9V.C9V_CPF     = " + cAlias + "." + cAlias + "_CPFTRB "
	ElseIf cAlias == "T2G"
		cFrom   += "    ON  C9V.C9V_CPF     = " + cAlias + "." + cAlias + "_CPFTRA "
	Else
		cFrom   += "    ON  C9V.C9V_CPF     = " + cAlias + "." + cAlias + "_CPF "
	EndIf
Else
	cFrom   += "    ON  C9V.C9V_ID     = " + cAlias + "." + cCpoTrab
EndIf
cFrom   += "    AND C9V.C9V_FILIAL = " + cAlias + "." + cAlias + "_FILIAL  "
cWhere  := "C9V.C9V_NOME LIKE '%" + AllTrim(Upper( cNome )) + "%' "
cWhere  += " AND C9V.C9V_ATIVO  = '1'"
cWhere  += " AND C9V.D_E_L_E_T_ = ' ' "
If cAlias $ ('CM6')
	If nRadio == 1	
	    If !Empty(cPerIni) .AND. Empty(cPerFim)
	    	cWhere += "AND ((CM6.CM6_DTAFAS >= '" + cPerIni + "') AND (CM6.CM6_DTAFAS <> ''))"
	    ElseIf Empty(cPerIni) .AND. !Empty(cPerFim)
	    	cWhere += "AND ((CM6.CM6_DTFAFA <= '" + cPerFim + "') AND (CM6.CM6_DTFAFA <> ''))"
	    ElseIf !Empty(cPerIni) .AND. !Empty(cPerFim)
	    	cWhere += "AND ((CM6.CM6_DTAFAS >= '" + cPerIni + "' AND CM6.CM6_DTAFAS <= '" + cPerFim + "') AND (CM6.CM6_DTFAFA >= '" + cPerIni + "' AND CM6.CM6_DTFAFA <= '" + cPerFim + "'))"
	    EndIf
    ElseIf nRadio == 2
    	If !Empty(cPerIni) .AND. Empty(cPerFim)
	    	cWhere += "AND (CM6.CM6_DTAFAS = '" + cPerIni + "')"
	   	ElseIf Empty(cPerIni) .AND. !Empty(cPerFim)
    		cWhere += "AND (CM6.CM6_DTFAFA = '" + cPerFim + "')"
    	ElseIf !Empty(cPerIni) .AND. !Empty(cPerFim)
    		cWhere += "AND ((CM6.CM6_DTAFAS = '" + cPerIni + "') AND (CM6.CM6_DTFAFA = '" + cPerFim + "'))"
    	EndIf
    EndIf
EndIf

cSelect := "%" + cSelect + "%"
cFrom   := "%" + cFrom + "%"
cWhere  := "%" + cWhere + "%"

BeginSql Alias cAliasQry
	SELECT
		%Exp:cSelect%
	FROM
		%Exp:cFrom%
	WHERE
		%Exp:cWhere%
EndSql

(cAliasQry)->( DbEVal({|| nTotReg++},,{|| !Eof()}) )
(cAliasQry)->( DbGoTop() )

If nTotReg >= 1 .And. ( cAliasQry )->(!Eof())
	cRet += "("
	
    While ( cAliasQry )->(!Eof())

        cRet += "(" + cAlias + "_FILIAL == '" + AllTrim(( cAliasQry )->C9V_FILIAL) + "' .AND. " 
        
        If cTpEvt == "T"
			If cAlias == "T2M"
				cRet += cAlias + "_CPFTRB == '" + AllTrim(( cAliasQry )->C9V_CPF) +"')
			ELseIf cAlias == "T2G"
				cRet += cAlias + "_CPFTRA == '" + AllTrim(( cAliasQry )->C9V_CPF) +"')
			Else
        		cRet += cAlias + "_CPF == '" + AllTrim(( cAliasQry )->C9V_CPF) +"')
			EndIf
        Else
        	cRet += cCpoTrab + " == '" + AllTrim(( cAliasQry )->C9V_ID) +"')
        EndIf

        If nTotReg > nX
            cRet += " .OR. "
        EndIf

        ( cAliasQry )->(DbSkip())

        nX++

    EndDo
    cRet += ")"

    If nTotReg == nX
        cRet += " .OR. "
    EndIf

EndIf

( cAliasQry )->( DbCloseArea() )

If cAlias $ "C91|T3P"
	If nTotReg >= 1
		cRet += ".OR. ('" + cNome + "' $ " + cCpoNome + ")"
	Else
		cRet += " ('" + cNome + "' $ " + cCpoNome + ")"
	EndIf
EndIf


Return cRet

//--------------------------------------------------------------------
/*/{Protheus.doc} CountNm
Fun��o respons�vel por retornar o valor de pesquisa inserida

@author Eduardo Sukeda
@since 08/03/2019
@version 1
/*/
//--------------------------------------------------------------------
Function CountNm(cNome, cAlias, cCpoTrab, cPerApu, cPerIni, cPerFim, nTpData, cCpoData1, cCpoData2, nRadio, cTpEvt)

Local cQtd        := ""
Local cSelect	  := ""
Local cFrom		  := ""
Local cWhere	  := ""
Local cCpoNome    := ""
Local cAliasQry   := GetNextAlias()
Local nX          := 1
Local lAlias      := .F.

Default cCpoData2 := ""

If (cAlias $ "C91|T3P")
	cSelect := "QTD1 + QTD2 QTD "
Else
	cSelect := "QTD1 QTD  "
EndIf

cFrom   := " (SELECT COUNT (C9V.C9V_ID) AS QTD1 "
cFrom   += " FROM " + RetSqlName( "C9V" ) + " C9V "

cFrom   += " JOIN " + RetSqlName( cAlias ) + " " 

If cTpEvt == "T"
	If cAlias == "T2M"
		cFrom   += cAlias + " ON C9V.C9V_CPF = " + cAlias + "." + cAlias + "_CPFTRB "
	ELseIf cAlias == "T2G"
		cFrom   += cAlias + " ON C9V.C9V_CPF = " + cAlias + "." + cAlias + "_CPFTRA "
	Else
		cFrom   += cAlias + " ON C9V.C9V_CPF = " + cAlias + "." + cAlias + "_CPF "
	EndIf
Else
	cFrom   += cAlias + " ON C9V.C9V_ID = " + cAlias + "." + cCpoTrab + " "
	cFrom   += " AND C9V.C9V_FILIAL = " + cAlias + "." + cAlias + "_FILIAL " 
EndIf
cFrom   += " WHERE C9V.C9V_NOME LIKE '%" + AllTrim(Upper( cNome )) + "%' "
cFrom   += " AND C9V.C9V_ATIVO = '1'  "
cFrom   += " AND C9V.D_E_L_E_T_ = ' '  "
cFrom   += " AND " + cAlias + ".D_E_L_E_T_ = ' ' "
cFrom   += " AND " + cAlias + "." + cAlias + "_ATIVO = '1'"

If nTpData <> 2
	If !Empty(cPerApu)
		cFrom   += " AND " + cAlias + "." + cCpoData1 + " = '" + cPerApu + "'"
	EndIf
Else
	
	If cAlias == 'CM6'

		If nRadio == 1	
		    If !Empty(cPerIni) .AND. Empty(cPerFim)
		    	cFrom += " AND ((CM6.CM6_DTAFAS >= '" + cPerIni + "') AND (CM6.CM6_DTAFAS <> ''))"
		    ElseIf Empty(cPerIni) .AND. !Empty(cPerFim)
		    	cFrom += " AND ((CM6.CM6_DTFAFA <= '" + cPerFim + "') AND (CM6.CM6_DTFAFA <> ''))"
		    ElseIf !Empty(cPerIni) .AND. !Empty(cPerFim)
		    	cFrom += " AND ((CM6.CM6_DTAFAS >= '" + cPerIni + "' AND CM6.CM6_DTAFAS <= '" + cPerFim + "') AND (CM6.CM6_DTFAFA >= '" + cPerIni + "' AND CM6.CM6_DTFAFA <= '" + cPerFim + "'))"
		    EndIf
	    ElseIf nRadio == 2
	    	If !Empty(cPerIni) .AND. Empty(cPerFim)
		    	cFrom += " AND (CM6.CM6_DTAFAS = '" + cPerIni + "')"
		   	ElseIf Empty(cPerIni) .AND. !Empty(cPerFim)
	    		cFrom += " AND (CM6.CM6_DTFAFA = '" + cPerFim + "')"
	    	ElseIf !Empty(cPerIni) .AND. !Empty(cPerFim)
	    		cFrom += " AND ((CM6.CM6_DTAFAS = '" + cPerIni + "') AND (CM6.CM6_DTFAFA = '" + cPerFim + "'))"
	    	EndIf
	    EndIf
	
	Else
	
	    If !Empty(cPerIni)
	    	cFrom   += " AND " + cAlias + "." + cCpoData1 + " >= '" + cPerIni + "'"
	    EndIf
	    
	    If !Empty(cPerFim)
	    	cFrom   += " AND " + cAlias + "." + IIF(!Empty(cCpoData2),cCpoData2,cCpoData1) + " <= '" + cPerFim + "'"
	    EndIf
	
	EndIf
    
EndIf

cFrom   += ") QTD1 "

If (cAlias $ "C91|T3P")
	
	If cAlias == "T3P"
		cCpoNome := "T3P_NOMER"
	ElseIf cAlias == "C91"
		cCpoNome := cAlias + "_NOME"
	EndIf
	
	cFrom   += " ,(SELECT COUNT (" + cAlias + "." + cAlias + "_ID) AS QTD2 "
	cFrom   += " FROM " + RetSqlName( cAlias ) + " " + cAlias + " "
	cFrom   += " WHERE " 
	If !Empty(cCpoNome) .And. !Empty(cNome)
		cFrom   += cAlias + "." + cCpoNome + " LIKE '%" + AllTrim(Upper( cNome )) + "%' AND "
	EndIf
	cFrom   += cAlias + "." + cAlias + "_ATIVO = '1' "
	cFrom   += " AND " + cAlias + ".D_E_L_E_T_ = ' ' "
	
	If !Empty(cPerApu)
		cFrom   += " AND " + cAlias + "." + cCpoData1 + " = '" + cPerApu + "'"
	EndIf
	cFrom   += " ) QTD2 "

EndIf

cSelect := "%" + cSelect + "%"
cFrom   := "%" + cFrom + "%"

BeginSql Alias cAliasQry
	SELECT
		%Exp:cSelect%
	FROM
		%Exp:cFrom%
EndSql

cQtd := (( cAliasQry )->QTD)

( cAliasQry )->( DbCloseArea() )

Return cQtd

//-------------------------------------------------------------------
/*/{Protheus.doc} FilIdFunc
Posiciona no ID do funcion�rio
@author  Eduardo Sukeda
@since   14/03/2019
@version 1
/*/
//-------------------------------------------------------------------
Function FilIdFunc(cCpf, cAlias, cCpoTrab,cTpEvt)

Local aRet      := {}
Local cSelect	:= ""
Local cFrom		:= ""
Local cWhere	:= ""
Local cAliasQry := GetNextAlias()

Local nTotReg	:= 0

Local lAlias    := .F.

Default cTpEvt := ""

cSelect := "DISTINCT C9V.C9V_FILIAL, C9V.C9V_ID, C9V.C9V_CPF "

cFrom   := RetSqlName( "C9V" ) + " C9V "
cFrom   += "  INNER JOIN "
cFrom   += RetSqlName( cAlias ) + " " + cAlias + " "
If cTpEvt == "T"
	If cAlias == 'T2M'
		cFrom   += " ON  C9V.C9V_CPF    = " + cAlias + "." + cAlias + "_CPFTRB "
	ElseIf cAlias == 'T2G'
		cFrom   += " ON  C9V.C9V_CPF    = " + cAlias + "." + cAlias + "_CPFTRA "
	Else
		cFrom   += " ON  C9V.C9V_CPF    = " + cAlias + "." + cAlias + "_CPF "
	EndIf
Else
	cFrom   += " ON  C9V.C9V_ID     = " + cAlias + "." + cCpoTrab + " "
EndIf
cFrom   += "     AND C9V.C9V_FILIAL = " + cAlias + "." + cAlias + "_FILIAL "
cFrom   += "     AND C9V.C9V_ATIVO  = '1'" 
cFrom   += "     AND C9V.D_E_L_E_T_ = '' "

cWhere  := " C9V.C9V_CPF = '" + cCpf + "' "
cWhere  += " AND C9V.D_E_L_E_T_ = ' ' "

cSelect := "%" + cSelect + "%"
cFrom   := "%" + cFrom + "%"
cWhere  := "%" + cWhere + "%"

BeginSql Alias cAliasQry
	SELECT
		%Exp:cSelect%
	FROM
		%Exp:cFrom%
	WHERE
		%Exp:cWhere%
EndSql

If (cAliasQry)->(!Eof())
	(cAliasQry)->( DbEVal({|| nTotReg++},,{|| !Eof()}) )
	(cAliasQry)->( DbGoTop() )
    If nTotReg > 1
        While (cAliasQry)->(!Eof())
            If lAlias := .T.
                aAdd(aRet,{(cAliasQry)->(C9V_FILIAL),(cAliasQry)->(C9V_ID),(cAliasQry)->C9V_CPF})
            EndIf
            ( cAliasQry )->(DbSkip())
        EndDo
    EndIf
EndIf

If lAlias == .F.
    aAdd(aRet,{(cAliasQry)->(C9V_FILIAL),(cAliasQry)->(C9V_ID),(cAliasQry)->C9V_CPF})
EndIf

( cAliasQry )->( DbCloseArea() )

Return aRet

//--------------------------------------------------------------------
/*/{Protheus.doc} TafVldCpf
Fun��o responsavel por filtrar os eventos por CPF

@author Eduardo Sukeda
@since 25/02/2019
@version 1
/*/
//--------------------------------------------------------------------
Static Function TafVldCpf(cCpf)

Local lRet := .T.

If !Empty(cCpf)
    If !CGC(cCpf,,.F.)
        Alert("Por gentileza digite um CPF v�lido para realizar o filtro.")
        lRet := .F.
    EndIf
EndIf

Return lRet

//--------------------------------------------------------------------
/*/{Protheus.doc} TafBtnFil
Fun��o responsavel por filtrar os eventos por CPF

@author Eduardo Sukeda
@since 25/02/2019
@version 1
/*/
//--------------------------------------------------------------------
Static Function TafBtnFil(oBrowse, cCpf, cPerApu, cNome, cAlias, dPerIni, dPerFim, cEvento, nRadio, nTpData, cCpoData1, cCpoData2 )

Local lRet := .T.

If !Empty(cNome) .And. !Empty(cCpf)
    Alert("Por gentileza preencha somente o CPF ou o Nome para realizar o filtro.")
    lRet := .F.
ElseIf !Empty(cNome) .And. Len(AllTrim(cNome)) <= 2
    Alert("Quantidade m�nima de caracteres para a busca � de 3, por gentileza refine melhor seu filtro.")
    lRet := .F.
ElseIf !Empty(cCpf)
    TafVldCpf(cCpf)
EndIf

If lRet
    lRet := TafFilter(oBrowse, cCpf, cPerApu, cNome, cAlias, dPerIni, dPerFim, cEvento, nRadio, nTpData, cCpoData1, cCpoData2 )
EndIf

Return lRet

//--------------------------------------------------------------------
/*/{Protheus.doc} TafNewBrowse
Fun��o responsavel por criar o formato mais novo do Browse, com os bot�es do filtro por CPF/nome/per�odo.

@param cEvento    , Caracter, Evento relacionado ao browse.
@param cCpoData1  , Caracter, Indica o campo correspondente � data/per�odo que ser� utilizado no filtro por per�odo.
@param cCpoData2  , Caracter, Indica o campo correspondente � data/per�odo final (se houver) que ser� utilizado no filtro por per�odo.
@param nTpData    , Numerico, Indica o formato do campo data: 1 = AAAA/MM, 2 = DD/MM/AAAA; 3 = MM/AAAA.
@param cTitulo    , Caracter, Titulo a ser apresentado pelo browse.
@param aOnlyFields, Array   , Rela��o de campos que ser�o exibidos no browse.
@param nIndLeg    , Numerico, Seta o indice utilizado pela funcao TafLegend().
@param nOrder     , Numerico, Seta o indice utilizado pelo m�todo do browse SetIniWindow().
@param aLegend    , Array   , Rela��o de dados para montagem da legenda do browse, caso as legendas sejam apresentadas num padr�o diferente do TafLegend().
@param cFilterDef , Caracter, Informa o filtro desejado para execu��o no objeto do browse. 

@author Leandro Dourado
@since 17/04/2019
@version 1
/*/
//--------------------------------------------------------------------
Function TafNewBrowse( cEvento, cCpoData1, cCpoData2, nTpData, cTitulo, aOnlyFields, nIndLeg, nOrder, aLegend, cFilterDef )
Local aEvento  		:= TAFRotinas( cEvento, 4, .F., 2 )
Local cAlias   		:= ""
Local cFonte  		:= ""
Local cFuncVld      := ""
Local cFuncXML      := ""
Local cTagEvt       := ""
Local cLayout		:= ""
Local bFiltro 		:= Nil
Local bExcReg  		:= Nil
Local bDesExc 		:= Nil
Local bVisExc 		:= Nil
Local bGerXml 		:= Nil
Local bVldReg  		:= Nil
Local bAjuRec  		:= Nil
Local bHistAlt 		:= Nil
Local bXmlLote		:= Nil
Local bClose		:= Nil

Local oDialog		:= Nil
Local oLayer		:= Nil
Local oPanel01		:= Nil
Local oPanel02		:= Nil
Local oBtFil		:= Nil
Local nTop			:= 0
Local nHeight		:= 0
Local nWidth		:= 0
Local lFreeze		:= .T.
Local aSize			:= FWGetDialogSize()
Local nX            := 0



Default cCpoData1 	:= ""
Default cCpoData2 	:= ""
Default nTpData     := 1
Default cTitulo     := ""
Default aOnlyFields := {}
Default nIndLeg     := 2
Default nOrder      := 1
Default aLegend     := {}
Default cFilterDef	:= ""


If Len(aEvento) > 0 .And. FindFunction("TAFSetFilter")
	
	cFonte   := aEvento[1]
	cFuncVld := aEvento[2]
	cAlias   := aEvento[3]
	cFuncXML := aEvento[8]
	cTagEvt  := aEvento[9]
	cLayout  :=  StrTran(aEvento[4],"S-","",,)

	bFiltro  := {|| FilCpfNome(oBrw, cAlias, cEvento, nTpData, cCpoData1, cCpoData2  ) }
	bExcReg  := {|| xTafVExc(cAlias,(cAlias)->(Recno()),1), oBrw:Refresh(.T.) }
	bDesExc  := {|| xTafVExc(cAlias,(cAlias)->(Recno()),2 , IIF(Type ("oBrw") == "U", Nil, oBrw))}
	bVisExc  := {|| xTafVExc(cAlias,(cAlias)->(Recno()),3) }
	bGerXml  := {|| IIF(FindFunction("TAFXMLRET"),TAFXMLRET(cFuncXML,cLayout,cAlias),&(cFuncXML+ "()"))}
	bVldReg  := {|| &(cFuncVld+ "()") }
	bAjuRec  := {|| xFunAltRec( cAlias )}
	bHistAlt := {|| xNewHisAlt( cAlias, cFonte, , , , IIF(Type ("oBrw") == "U", Nil, oBrw), cEvento,cLayout, cFuncXML)}
	bXmlLote := {|| TAFXmlLote( cAlias, cEvento , cTagEvt , cFuncXML, ,oBrw )}
	bClose   := {|| oDialog:End() }
	bXmlErp  := {|| IIF(IIf(FindFunction("PROTDATA"),PROTDATA(),.T.),XmlErpxTaf( cAlias, cFuncXML, cEvento, &((cAlias)+"->"+(cAlias + "_FILIAL"))),.F.) }

	/*----------------------------
	Constru��o do Painel Principal
	----------------------------*/
	
	oDialog := MsDialog():New( aSize[1], aSize[2], aSize[3], aSize[4], cTitulo,,,,,,,,, .T.,,,, .F. ) 
	
	oLayer := FWLayer():New()
	
	oLayer:Init( oDialog, .F. )
	
	oLayer:AddLine( "LINE01", 100 )
	
	oLayer:AddCollumn( "BOX01",88,, "LINE01" )
	oLayer:AddCollumn( "BOX02",12,, "LINE01" )
	
	oLayer:AddWindow( "BOX01", "PANEL01", cTitulo, 100, .F.,,, "LINE01" )
	oLayer:AddWindow( "BOX02", "PANEL02", "Outras A��es", 100, .F.,,, "LINE01" )
	
	oPanel01 := oLayer:GetWinPanel( "BOX01", "PANEL01", "LINE01" )
	oPanel02 := oLayer:GetWinPanel( "BOX02", "PANEL02", "LINE01" )
	
	/*----------------------------------------------------------------
	Constru��o do Painel 01 - Browse do Cadastro de Folha de Pagamento
	----------------------------------------------------------------*/
	
	If TafAtualizado()
	
		oBrw:SetDescription(cTitulo)
		oBrw:SetOwner( oPanel01 )
		oBrw:SetAlias(cAlias)
		oBrw:SetIniWindow(DbSetOrder(nOrder))
	
		If Empty(cFilterDef)
			oBrw:SetFilterDefault(TAFBrwSetFilter(cAlias,cFonte,cEvento))
		Else 
			oBrw:SetFilterDefault(cFilterDef)
		EndIf	
		
		aOnlyFields := GetBrwFields( cAlias, aOnlyFields )

		If Len(aOnlyFields) > 0
			oBrw:SetOnlyFields( aOnlyFields )
		EndIf
		
		If Len(aLegend) > 0
			For nX := 1 To Len(aLegend)
				oBrw:AddLegend( aLegend[nX,1], aLegend[nX,2], aLegend[nX,3] )
			Next nX
		Else
			TafLegend(nIndLeg,cAlias,@oBrw)
		EndIf
	
	EndIf
	
	/*------------------------------------
	Constru��o do Painel 02 - Outras A��es
	------------------------------------*/
	
	nWidth := ( oPanel02:nClientWidth / 2 ) - 3
	nHeight := Int( ( oPanel02:nClientHeight / 2 ) / 10 ) - 5
	
	nTop := 5
	oBtFil := TButton():New( 005, 002, "Filtro CPF/Nome", oPanel02, bFiltro, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )
	oBtFil:SetCSS(SetCssButton("11","#FFFFFF","#1DA2C3","#1DA2C3"))

	nTop += nHeight + 5
	TButton():New( nTop, 002, "Xml ERP x TAF", oPanel02, bXmlErp, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )
	
	nTop += nHeight + 5
	TButton():New( nTop, 002, "Excluir Registro", oPanel02, bExcReg, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )
	
	nTop += nHeight + 5
	TButton():New( nTop, 002, "Desfazer Exclus�o", oPanel02, bDesExc, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )
	
	nTop += nHeight + 5
	TButton():New( nTop, 002, "Visualizar Reg.Excl.", oPanel02, bVisExc, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )
	
	nTop += nHeight + 5
	TButton():New( nTop, 002, "Gerar Xml e-Social", oPanel02, bGerXml, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

	nTop += nHeight + 5	
	TButton():New( nTop, 002, "Validar Registro", oPanel02, bVldReg, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )	
	
	nTop += nHeight + 5
	TButton():New( nTop, 002, "Ajuste de Recibo", oPanel02, bAjuRec, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )
	
	nTop += nHeight + 5
	TButton():New( nTop, 002, "Exibir Hist.Alt.", oPanel02, bHistAlt, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )
	
	nTop += nHeight + 5
	TButton():New( nTop, 002, "Gerar XML em Lote", oPanel02, bXmlLote, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )
	
	/*-------------------
	Ativa��o da Interface
	-------------------*/
	oBrw:Activate()
	oDialog:Activate()

EndIf


Return



//--------------------------------------------------------------------
/*/{Protheus.doc} SetCssButton

Cria objeto TButton utilizando CSS

@author Eduardo Sukeda
@since 22/03/2019
@version 1.0

@param cTamFonte - Tamanho da Fonte
@param cFontColor - Cor da Fonte
@param cBackColor - Cor de Fundo do Bot�o
@param cBorderColor - Cor da Borda

@return cCss
/*/
//--------------------------------------------------------------------
Static Function SetCssButton(cTamFonte,cFontColor,cBackColor,cBorderColor)

Local cCSS := ""

cCSS := "QPushButton{ background-color: " + cBackColor + "; "
cCSS += "border: none; "
cCSS += "font: bold; "
cCSS += "color: " + cFontColor + ";" 
cCSS += "padding: 2px 5px;" 
cCSS += "text-align: center; "
cCSS += "text-decoration: none; "
cCSS += "display: inline-block; "
cCSS += "font-size: " + cTamFonte + "px; "
cCSS += "border: 1px solid " + cBorderColor + "; "
cCSS += "border-radius: 3px "
cCSS += "}"

Return cCSS

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFNmTrab

Retorna o nome do funcion�rio

@author Totvs
@since 02/08/2019
@version 1.0
/*/
//---------------------------------------------------------------------
Function TAFNmTrab(cFil,cIdTrab)
Local aArea	:= GetArea()
Local cNome := ""
Local cCPF	:= ""
Local cRet	:= ""


If !Empty(cIdTrab) 
	cNomEve := Posicione("C9V", 2, cFil + cIdTrab + "1", "C9V_NOMEVE")

	If cNomEve == 'S2200'
		cNome := Posicione("T1U", 2, cFil + cIdTrab + "1", "T1U_NOME")
		cCPF  := Posicione("T1U", 2, cFil + cIdTrab + "1", "T1U_CPF")
		cRet  := cCPF + " - " + cNome
	Else
		cNome := Posicione("T1V", 2, cFil + cIdTrab + "1", "T1V_NOME")
		cCPF  := Posicione("T1V", 2, cFil + cIdTrab + "1", "T1V_CPF")
		cRet  := cCPF + " - " + cNome
	EndIf

	If Empty(cNome) .OR. Empty(cCPF)
		cNome := Posicione("C9V", 2, cFil + cIdTrab + "1", "C9V_NOME")
		cCPF  := Posicione("C9V", 2, cFil + cIdTrab + "1", "C9V_CPF")
		cRet  := cCPF + " - " + cNome
	EndIf
EndIf

RestArea(aArea)
Return cRet

//--------------------------------------------------------------------
/*/{Protheus.doc} GetBrwFields

Busca campos marcados para exibi��o no browse. 
Inicialmente cada rotina passava os campos a serem exibidos, por�m foi aberta issue por conta da impossibilidade de editar os campos a serem exibidos no browse.
Portanto, foi criada essa fun��o.

@author Leandro Dourado
@since 07/08/2019
@version 1.0

@param cAlias      , Caracter, Alias a ser exibido no Browse.
@param aOnlyFields , Array   , Rela��o dos campos j� indicados para serem exibidos no Browse. 

@return aRet       , Array   , Rela��o dos campos a serem exibidos no Browse.
/*/
//--------------------------------------------------------------------
Static Function GetBrwFields( cAlias, aOnlyFields )
Local aRet          := {}
Local aArea         := SX3->(GetArea())

Default cAlias      := ""
Default aOnlyFields := {}

aRet := aClone(aOnlyFields)

If !Empty(cAlias)
	DbSelectArea("SX3")
	SX3->(DbSetOrder(1))

	If SX3->(DbSeek(cAlias))
		While SX3->(!Eof()) .And. SX3->X3_ARQUIVO == cAlias
			If SX3->X3_BROWSE == "S" .And. aScan(aOnlyFields,SX3->X3_CAMPO) == 0
				aAdd(aRet,SX3->X3_CAMPO)
			EndIf
			SX3->(DbSkip())
		EndDo
	EndIf
EndIf

RestArea( aArea )

Return aRet