#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFXFUNDIC.CH"

Static __aAliasInDic
Static __aIdIndex  
Static __lLay0205	:= TafLayESoc("02_05_00")
Static cRetNTrab 	:= ""

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFXFUNDIC
Fonte com fun�oes genericas do TAF relacionadas a dicionario de dados

@author Denis R. de Oliveira
@since 13/08/2015
@version 1.0

@Return ( Nil )

/*/       
                                                                                                                              
//---------------------------------------------------------------------
/*/{Protheus.doc} TAFAlsInDic

Indica se um determinado alias est� presente no dicion�rio de dados

@Author	Anderson Costa
@Since		22/07/2015
@Version	1.0

@return lRet  - .T. -> Validacao OK
				  .F. -> Nao Valido

/*/
//---------------------------------------------------------------------
Function TAFAlsInDic(cAlias,lHelp)
Local aArea     := {}
Local aAreaSX2  := {}
Local aAreaSX3  := {}
Local lRet		:= .F.
Local nAt

Default __aAliasInDic := {}

nAt := Ascan( __aAliasInDic, {|x| x[1]==cAlias})

If ( nAt == 0 )
	aArea		:= GetArea()
	aAreaSX2	:= SX2->( GetArea() )
	aAreaSX3	:= SX3->( GetArea() )
	
	DEFAULT lHelp	:= .F.
	
	SX2->( DbSetOrder( 1 ) )
	SX3->( DbSetOrder( 1 ) )
	
	lRet := ( SX2->( dbSeek( cAlias ) ) .And. SX3->( dbSeek( cAlias ) ) )

	Aadd(__aAliasInDic, {cAlias,lRet})
	
	SX3->( RestArea( aAreaSX3 ) )
	SX2->( RestArea( aAreaSX2 ) )
	RestArea( aArea )
Else 
	lRet := __aAliasInDic[nAt][2]
EndIf

If !lRet .And. lHelp
	Help( "", 1, "ALIASINDIC",,cAlias )
EndIf


Return( lRet )


//---------------------------------------------------------------------
/*/{Protheus.doc} TafColumnPos
Verifica se existe o campo no dicion�rio de dados levando em considera��o a vers�o corrente do release
do cliente.


@Author	Rodrigo Aguilar
@Since		01/12/2015
@Version	1.0

@return lFindCmp ( Indica se o campo existe ou n�o no dicion�rio de dados )

/*/
//---------------------------------------------------------------------
Function TafColumnPos( cCampo )

Local cReleaseAtu := Substr( alltrim( GetRpoRelease() ), 1, 2 )
Local cAliasAtu   := Substr( alltrim( cCampo ), 1, 3 ) 
Local lFindCmp    := .F.

//De acordo com a vers�o corrente do release do cliente utilizo a fun�ao correta para saber se o campo
//existe ou n�o no dicion�rio de dados
if (TAFAlsInDic(cAliasAtu))
	dbSelectArea( cAliasAtu )
	if  cReleaseAtu <> '12'
		lFindCmp := ( FieldPos( cCampo ) > 0 )  
	else
		lFindCmp := ( ColumnPos( cCampo ) > 0 )
	endif
endIf

Return ( lFindCmp )



//---------------------------------------------------------------------
/*/{Protheus.doc} TafIndexInDic
Verifica se existe o indice no dicion�rio de dados e no banco de dados

@param	cAlias		-> Alias da tabela
@param	uIdIndex	-> N�mero ou Id caracter do indice

@Author	Felipe Rossi Moreira
@Since		12/01/2018
@Version	1.0

@return lRet (indica se existe o indice e pode ser usado)

/*/
//---------------------------------------------------------------------

Function TafIndexInDic(cAlias, uIdIndex, lHelp)
Local lRet := .F.
Local cIndex := ""
Local cAliasSQLName := ""

Default lHelp := .F.

if TAFAlsInDic(cAlias, lHelp)
	//Verifica o Id do indice e converte para formato caracter caso num�rico
	if ValType(uIdIndex) == 'C'
		cIndex := uIdIndex
	elseif ValType(uIdIndex) == 'N'
		if uIdIndex > 9
			cIndex := Chr(65+uIdIndex-10)
		else
			cIndex := AllTrim(Str(uIdIndex))
		endif
	endif

	//Nome da tabela no banco para valida��o da exist�ncia do indice no banco de dados
	cAliasSQLName := RetSQLName(cAlias)

	//lRet := !Empty(cIndex) .and. !Empty(Posicione("SIX",1,cAlias+cIndex,"CHAVE")) .and. TcCanOpen(cAliasSQLName,cAliasSQLName+cIndex)

	If TcCanOpen(cAliasSQLName,cAliasSQLName+cIndex)
		lRet := !Empty(cIndex) .and. !Empty(Posicione("SIX",1,cAlias+cIndex,"CHAVE"))
	Else
		DbSelectArea(cAlias)
		lRet := !Empty(cIndex) .and. !Empty(Posicione("SIX",1,cAlias+cIndex,"CHAVE")) .and. TcCanOpen(cAliasSQLName,cAliasSQLName+cIndex)
	EndIf

	If !lRet .And. lHelp
		MsgInfo( STR0069+CRLF+STR0070+cAlias+CRLF+STR0071+cIndex, STR0072 ) //"O seguinte indice n�o est� dispon�vel na dicion�rio de dados:" ## "Tabela: " ## "Indice: " ## "Ambiente Desatualizado!"
	EndIf
endif

Return(lRet)




//---------------------------------------------------------------------
/*/{Protheus.doc} TAFGetIdIndex

Fun��o que retorna a ordem do indice do ID conforme alias solicitado

@param	cAliasTAF	-> Alias do TAF
@param	cFieldId	-> Campo considerado ID

@Author		Luccas Curcio
@Since		06/04/2016
@Version	1.0

@return nIdIndex - Ordem do Indice do ID da tabe�a

/*/
//---------------------------------------------------------------------
function TAFGetIdIndex( cAliasTAF , cFieldId )

local	nIdIndex	:=	0
local	nPosIndex	:=	0
local	aAreaSIX	:=	{}
local	nTamFldId	:=	0
local	nTamFldCod	:=	0 //Cria��o da vari�vel que verifica o tamanho que o campo _CODIGO 
Local	cFieldCodi	:=	"_CODIGO" //Cria��o da vari�vel que verifica o nomedo campo que o campo que ser� o �ndice 

default	__aIdIndex	:=	{}
default	cFieldId	:=	"_ID"


nTamFldId	:=	iif( cFieldId  == "_CHVNF" , 6 , 3 )
nTamFldCod	:= 	iif( cFieldCodi == "_CODIGO" , 7 , 3 )//Defini��o de tamanho da var�vel que receber� o �ndice  

If cAliasTAF $ 'C0A|C8A'
	//Caso o Alias seja C8A / Fpas, modifico o campo pois n�o existe _CODIGO
	If cAliasTAF == "C8A"
		cFieldCodi := "_CDFPAS"
		nTamFldCod := 7
	ElseIf cAliasTAF == "C0A"
		cFieldId := "_CODIGO"
		nTamFldId := 7
	EndIf
EndIf

//Verifico se ja tenho em cache o indice deste alias
if ( nPosIndex := aScan( __aIdIndex , { |x| x[1] == cAliasTAF } ) ) > 0
	nIdIndex	:=	__aIdIndex[ nPosIndex , 2 ]

else
	
	aAreaSIX	:=	SIX->( getArea() )
	
	//Caso o alias nao tenha sido utilizado anteriormente, posiciono no primeiro indice do Alias no dicionario SIX
	if SIX->( msSeek( cAliasTAF ) )
	
		//Procuro apenas nos indices do proprio alias
		while SIX->( !eof() ) .and. allTrim( SIX->INDICE ) == ( cAliasTAF )
			
			//Se encontrar algum indice que o segundo campo da chave seja o c�digo posso sair do la�o e utilizar este indice (DSERTAF2-777/DSERTAF2-776 
			//Todas os indices onde o segundo campo � CODIGO come�am com: "XXX_FILIAL+XXX" e depois "_CODIGO" ( XXX_FILIAL+XXX_CODIGO ). Por isso procuro da posicao 11 em diante 
			If cAliasTAF $ ('C3Z|C1A|C6U|C8Z|C8A|CHY|C1U|T71|CUF|CMM') .And. !IsInCallStack("FAtuTabTAF")  
				if substr( SIX->CHAVE , 15 , nTamFldCod ) == cFieldCodi 
					nIdIndex	:=	val( SIX->ORDEM ) 
					//Guardo este alias e chave no array est�tico para agilizar futuras pesquisas do mesmo alias 
					aAdd( __aIdIndex , { cAliasTAF , nIdIndex } ) 
					exit	 
				Endif 					
			Else			  
				//Se encontrar algum indice que o segundo campo da chave seja ID posso sair do la�o e utilizar este indice
				//Todas os indices onde o segundo campo � ID come�am com: "XXX_FILIAL+XXX" e depois "_ID" ( XXX_FILIAL+XXX_ID ). Por isso procuro da posicao 15 em diante
				if substr( SIX->CHAVE , 15 , nTamFldId ) == cFieldId
					nIdIndex	:=	val( SIX->ORDEM )
					//Guardo este alias e chave no array est�tico para agilizar futuras pesquisas do mesmo alias
					aAdd( __aIdIndex , { cAliasTAF , nIdIndex } )
					exit
				endif
			Endif 
			SIX->( dbSkip() )
		enddo
	endif
	
	restArea( aAreaSIX )
	
endif

return nIdIndex

//---------------------------------------------------------------------
/*/{Protheus.doc} TafVldAmb
Fun��o tem como objetivo realizar a valida��o do dicion�rio de dados, verificando se o TAF
esta em uma vers�o compat�vel com a execu��o da integra��o Online.

Para a vers�o 12 essa fun��o n�o tem sentido pois sempre que utilizada a integra��o online
o dicion�rio do cliente estar� atualizado devido a execu��o do upddistr, por�m com a nova estrutura��o 
dos fontes o m�dulo SIGAFIS ir� possuir fontes �nicos (MATXFIS, por exemplo), assim devemos manter 
a fun��o para que na vers�o 12 sempre retorne .T., indicando que o dicion�rio est� atualizado.

@Author     Rodrigo Aguilar
@Since       10/10/2016
@Version    1.0

@param cEscopo   - Compatibilidade com a vers�o 11
       cIdentity - Compatibilidade com a vers�o 11 

@return .T. 

/*/
//---------------------------------------------------------------------
Function TafVldAmb( cEscopo , cIdentity )

Return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} TafAmbInvMsg

Retorna mensagem padr�o informando que o ambiente TAF est� desatualizado.

@Return	cMensagem

@Author	Anderson Costa
@Since		01/12/2015
@Version	1.0
/*/
//---------------------------------------------------------------------
Function TafAmbInvMsg()

Local cEnter		
Local cMensagem	
Local cVersion	

cEnter		:=	Chr( 13 ) + Chr( 10 )
cMensagem	:=	""
cVersion	:=	SubStr( GetRpoRelease(), 1, 2 )

cMensagem := STR0001 + cEnter + cEnter //"Inconsist�ncia:"
cMensagem += STR0002 + cEnter + cEnter //"O ambiente do TAF est� com o dicion�rio de dados incompat�vel com a vers�o dos fontes existentes no reposit�rio de dados, este problema ocorre devido a n�o execu��o dos compatibilizadores do produto."

If cVersion == "11"
	cMensagem += STR0003 + cEnter + cEnter //"Ser� necess�rio executar o UPDDISTR e em seguida o UPDTAF com o �ltimo arquivo diferencial ( SDFBRA ) dispon�vel no portal do cliente."
ElseIf cVersion == "12"
	cMensagem += STR0007 + cEnter + cEnter //"Ser� necess�rio executar o UPDDISTR com o �ltimo arquivo diferencial ( SDFBRA ) dispon�vel no portal do cliente."
EndIf

cMensagem += STR0004 + cEnter //"Siga as instru��es do link abaixo para realizar a atualiza��o:"

If cVersion == "11"
	cMensagem += STR0005 + cEnter + cEnter //"http://tdn.totvs.com.br/pages/releaseview.action?pageId=187534210"
ElseIf cVersion == "12"
	cMensagem += STR0008 + cEnter + cEnter //"http://tdn.totvs.com.br/pages/releaseview.action?pageId=198935223"
EndIf

cMensagem += STR0006 + cEnter //"Ap�s seguir os passos acima o acesso ao TAF ser� liberado!"

Return( cMensagem )

//---------------------------------------------------------------------
/*/{Protheus.doc} TafLock
Fun��o criada para realizar a grava��o das tabelas do TAF, ou seja, em termos pr�ticos
ela substitui o RECLOCK(), realizando o controle de concorr�ncia de grava��o na tabela
que sera manutenida.

Conceito:

Grava��o sem lSimpleLock: O pr�prio Reclock() trava o usu�rio na tela dizendo que o registro a ser
alterado est� preso com outro usu�rio, a cada 5 segundos tenta novamente realizar a opera��o e assim
fica at� conseguir efetivar a opera��o.

Grava��o com lSimpleLock: S�o realizadas 5 tentativas de reservar o registro na tabela com simplelock(), tratar
o retorno como da fun��o para saber se o registro est� reservado ou n�o

@Author  Rodrigo Aguilar
@Since   11/11/2016
@Version 1.0

@param cAlias       - Alias a ser gravado/alterado
        lInclui      - .T. - Inclui/ .F. Altera/Exclui 
        lSimpleLock  - Indica se deve realizar o simplelock antes do reclock

@return .T. 

/*/
//---------------------------------------------------------------------
Function TafLock( cAlias, lInclui, lSimpleLock ) 

local lLock  := .F.
local nLock := 0

//---------------------------------------------------
//Realizo a tentativa de reservar o registro 5 vezes
//---------------------------------------------------
for nLock := 1 to 5

	//-----------------------------------------------------------------------------------------------
	//Quando o processamento n�o possuir tela verifico com simplelock se conseguirei reservar o Alias
	//a ser reservado e se a opera��o � de Altera��o/Exclus�o
	//-----------------------------------------------------------------------------------------------
	if lSimpleLock .and. !lInclui
		if ( lLock := ( cAlias )->( SimpleLock() ) ) 	  
			RecLock( cAlias , lInclui )
		endIf
	
	//--------------------------------------------------------
	//Para grava��o em tela apenas realizo a opera��o desejada
	//--------------------------------------------------------
	else		
		RecLock( cAlias , lInclui )
		lLock	:=	.T.
		
	endIf
	
	//------------------------------------------------------------------------------------------
	//Se nao conseguir fazer LOCK, aguarda um 2,5 segundos e tento de novo, esse tempo vai sendo
	//exponencial a medida que for tentando.
	//------------------------------------------------------------------------------------------
	if !lLock
		Conout( 'TAF - Aguardando libera��o do registro ( concorrencia ) ' +  alltrim( str( nLock ) ) )									
		Sleep( 2500 * nLock )	 								
	else
		exit
	endIf
next


Return lLock

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFObrVldEnv

Fun��o que valida o ambiente para gera��o da obriga��o fiscal

@param	aFields -> Campos que devem ser validados
@param	aTables -> Tabelas que devem ser validadas

@author Luccas Curcio
@since 21/11/2016
@version 1.0
/*/
//---------------------------------------------------------------------
function TAFObrVldEnv( aFields, aTables)

local	nX			
local	nOpc		
local	cCmpNoEx	
local	cTblNoEx	
local	cMsg		
local	lRet		

default	aFields	:=	{}
default	aTables	:=	{}

nX		:=	0
nOpc	:=	0
cCmpNoEx:=	""
cTblNoEx:=	""
lRet	:=	.T.
//"O sistema identificou a aus�ncia de componentes neste ambiente que influenciam no resultado final do processo.
//Para mais informa��es sobre a atualiza��o de ambiente acesse: 
//http://tdn.totvs.com/pages/viewpage.action?pageId=198935223."
cMsg := STR0009 + STR0008 + CRLF + CRLF

if !empty( aTables )

	for nX := 1 to len( aTables )
		
		//Verifico se a tabela existe no ambiente. Caso n�o exista adiciono na string que far� o alerta no final da fun��o.
		if !( tafAlsInDic( aTables[ nX ] ) )
			cTblNoEx += aTables[ nX ] + "(" + allTrim( FWX2Nome( aTables[ nX ] ) ) + "), "
		endif
	
	next nX
	
	if !empty( cTblNoEx )

		//Retiro ", " do final da string
		cTblNoEx := subStr( cTblNoEx , 1 , len( cTblNoEx ) - 2 )
	endif
	
	cMsg += STR0010 + CRLF + CRLF + cTblNoEx + CRLF + CRLF //"Tabelas:"

endif

if !empty( aFields )

	for nX := 1 to len( aFields )
		
		//Verifico se o campo existe no ambiente. Caso n�o exista adiciono na string que far� o alerta no final da fun��o.
		if !( tafColumnPos( aFields[ nX ] ) )
			cCmpNoEx += aFields[ nX ] + "(" + allTrim( FWX2Nome( subStr( aFields[ nX ] , 1 , 3 ) ) ) + "), "
		endif
	
	next nX
	
	if !empty( cCmpNoEx )
		//Retiro ", " do final da string
		cCmpNoEx := subStr( cCmpNoEx , 1 , len( cCmpNoEx ) - 2 )
	endif
	
	cMsg += STR0011 + CRLF + CRLF + cCmpNoEx + CRLF + CRLF //"Campos:"

endif

if !empty( cCmpNoEx ) .or. !empty( cTblNoEx )
	nOpc := tafAviso( STR0012 , cMsg  , { STR0013 , STR0014 } , 3 ) //"Ambiente desatualizado" ## "Continuar" ## "Encerrar"
endif

if nOpc == 2
	lRet :=	.F.
endif

return lRet

//---------------------------------------------------------------------
/*/{Protheus.doc} CboTpAdmiss()

Fun��o que retorna as op��es do CBO do evento S-2200

@author Ricardo Lovrenovic
@since 07/11/2017
@version 1.0
/*/
//---------------------------------------------------------------------
Function CboTpAdmiss()
Local cString 

cString := "1="+STR0015+";" //"Admiss�o"
cString += "2="+STR0016+";" //"Transfer�ncia de empresa do mesmo grupo econ�mico"
cString += "3="+STR0017+";" //"Transfer�ncia de empresa consorciada ou de cons�rcio"
cString += "4="+STR0018+";" //"Transfer�ncia por motivo de sucess�o, incorpora��o, cis�o ou fus�o"
cString += "5="+STR0019+";" //"Transfer�ncia do empregado dom�stico para outro representante da mesma unidade familiar"
cString += "6="+STR0083+";" //"Mudan�a de CPF"

Return(cString)

//---------------------------------------------------------------------
/*/{Protheus.doc} CboTpRegJ

Fun��o que retorna as op��es do CBO do evento S-2200 e S-2206
Campos: CUP_TPREGJ e T1V_TPREGJ
Tag: tpRegJor
Defini��o: Identifica o regime de jornada do empregado

@author Ricardo Lovrenovic / Felipe Rossi Moreira
@since 07/11/2017 / 21/11/2017
@version 1.0
/*/
//---------------------------------------------------------------------
Function CboTpRegJ()
Local cString 

cString := "1="+STR0020+";" //"Submetidos a Hor�rio de Trabalho (Cap. II da CLT)"
cString += "2="+STR0021+";" //"Atividade Externa especificada no Inciso I do Art. 62 da CLT"
cString += "3="+STR0022+";" //"Fun��es especificadas no Inciso II do Art. 62 da CLT"
cString += "4="+STR0023 //"Teletrabalho, previsto no Inciso III do Art. 62 da CLT"

Return(cString)

//---------------------------------------------------------------------
/*/{Protheus.doc} CbotpPgto

Fun��o que retorna as op��es do CBO do evento S-1210
Campos: T3Q_TPPGTO
Tag: tpPgto
Defini��o: Informar o tipo de pagamento

@author Felipe Rossi Moreira
@since 30/11/2017
@version 1.0
/*/
//---------------------------------------------------------------------
Function CbotpPgto()
Local cString 

cString := "1="+STR0024+";" //"Pagamento de remunera��o, conforme apurado em {dmDev} do S-1200"
cString += "2="+STR0025+";" //"Pagamento de verbas rescis�rias conforme apurado em {dmDev} do S-2299"
cString += "3="+STR0026+";" //"Pagamento de verbas rescis�rias conforme apurado em {dmDev} do S-2399"
cString += "5="+STR0027+";" //"Pagamento de remunera��o conforme apurado em {dmDev} do S-1202"
cString += "6="+STR0028+";" //"Pagamento de Benef�cios Previdenci�rios, conforme apurado em {dmDev} do S-1207"
cString += "7="+STR0029+";" //"Recibo de f�rias"
cString += "9="+STR0030 //Pagamento relativo a compet�ncias anteriores ao in�cio de obrigatoriedade dos eventos peri�dicos para o contribuinte

Return(cString)

//---------------------------------------------------------------------
/*/{Protheus.doc} CboIndCum

Fun��o que retorna as op��es do CBO do evento S-2299
Campos: CMD_INDCUM
Tag: indCumprParc
Defini��o: Indicador de cumprimento de aviso pr�vio:

@author Felipe Rossi Moreira
@since 19/12/2017
@version 1.0
/*/
//---------------------------------------------------------------------
Function CboIndCum()
Local cString 

cString := "0="+STR0031+";" //Cumprimento total
cString += "1="+STR0032+";" //Cumprimento parcial em raz�o de obten��o de novo emprego pelo empregado
cString += "2="+STR0033+";" //Cumprimento parcial por iniciativa do empregador
cString += "3="+STR0034+";" //Outras hip�teses de cumprimento parcial do aviso pr�vio
cString += "4="+STR0035		//Aviso pr�vio indenizado ou n�o exig�vel

Return(cString)

//---------------------------------------------------------------------
/*/{Protheus.doc} CboCodAjus

Fun��o que retorna as op��es de combo do c�digo de ajuste 
Campos: T9T_CODAJU
Tag: CodAjuste
Defini��o: C�digo de Ajuste da contribui��o apurada no per�odo

@author anieli.rodrigues
@since 29/01/2017
@version 1.0
/*/
//---------------------------------------------------------------------
Function CboCodAjus()
	
	Local cString 
	
	cString := "01="+STR0058+";" //Ajuste da CPRB: Ado��o do Regime de Caixa
	cString += "02="+STR0059+";" //Ajuste da CPRB: Diferimento de Valores a recolher no per�odo
	cString += "03="+STR0060+";" //Adi��o de valores Diferidos em Per�odo(s) Anteriores(es)
	cString += "04="+STR0061+";" //Exporta��es diretas
	cString += "05="+STR0062+";" //Transporte internacional de cargas
	cString += "06="+STR0063+";" //Vendas canceladas e os descontos incondicionais concedidos
	cString += "07="+STR0064+";" //IPI, se inclu�do na receita bruta
	cString += "08="+STR0065+";" //ICMS, quando cobrado pelo vendedor dos bens ou prestador dos servi�os na condi��o de substituto tribut�rio
	cString += "09="+STR0066+";" //Receita bruta reconhecida pela constru��o, recupera��o, reforma, amplia��o ou melhoramento da infraestrutura, cuja contrapartida seja ativo intang�vel representativo de direito de explora��o, no caso de contratos de concess�o de servi�os p�blicos
	cString += "10="+STR0067+";" //O valor do aporte de recursos realizado nos termos do art 6 �3 inciso III da Lei 11.079/2004
	cString += "11="+STR0068 //Demais ajustes oriundos da Legisla��o Tribut�ria, estorno ou outras situa��es.

Return(cString)


//---------------------------------------------------------------------
/*/{Protheus.doc} CboIndAqu

Fun��o que retorna as op��es de combo do indicativo de aquisi��o 
Campos: CMT_INDAQU
Tag: indAquis
Defini��o: Indicativo de aquisi��o de produto

@author ricardo.prandi	
@since 08/08/2018
@version 1.0
/*/
//---------------------------------------------------------------------
Function CboIndAqu()
	
	Local cString 
	
	cString := "1="+STR0073+";" //Aquisi��o produtor rural PF
	cString += "2="+STR0074+";" //Aquisi��o produtor rural PF por entidade PAA
	cString += "3="+STR0075+";" //Aquisi��o produtor rural PJ por entidade PAA
	cString += "4="+STR0076+";" //Aquisi��o produtor rural PF - Produ��o Isenta (Lei 13.606/2018)
	cString += "5="+STR0077+";" //Aquisi��o produtor rural PF por entidade PAA - Produ��o Isenta (Lei 13.606/2018)
	cString += "6="+STR0078     //Aquisi��o produtor rural PJ por Entidade PAA - Produ��o Isenta (Lei 13.606/2018)

Return(cString)


//---------------------------------------------------------------------
/*/{Protheus.doc} CboOrdExa

Fun��o que retorna as op��es de combo da Ordem do Exame
Campos: C9W_ORDEXA
Tag: ordExame
Defini��o: Ordem do Exame

@author Karyna.martins
@since 10/01/2019
@version 1.0
/*/
//---------------------------------------------------------------------
Function CboOrdExa()
	
	Local cString 
	
	If __lLay0205
		cString := "1="+STR0082+";" //Inicial		
	Else
		cString := "1="+STR0080+";" //Referencial
	EndIf
	cString += "2="+STR0081+";" //Sequencial
	
	
Return(cString)

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFPic

Ajusta a picture vari�vel dos campos de per�odo dos totalizadores 5003 e 5013, alterandno entre o formato MM-AAAA e AAAA.

@author Leandro.dourado
@since 12/02/2019
@version 1.0
/*/
//---------------------------------------------------------------------
Function TAFPic( cCampo )
Local cRet   := ""
Local cValor := ""

If cCampo $ 'V2P_PERAPU|V2Z_PERAPU'
	cValor := StrTran(AllTrim(FWFLDGET(cCampo)),"-","")
	
	If Empty(cValor)
		cValor := AllTrim(&(SubStr(cCampo,1,3) + "->" + cCampo))
	EndIf
	
	cRet := IIF(Len(cValor) == 6, '@R 99-9999', '@R 9999')
EndIf

Return cRet

//----------------------------------------------------------------------------------------------
/*/{Protheus.doc} C9VHFil
Consulta Especifica de trabalhadores S-2200,S-2205,S-2300 e TAUTO.

@author Henrique Cesar
@since 06/08/2019
@version 1.0

/*/       
//----------------------------------------------------------------------------------------------
Function C9VHFil(cEvtTrab)

	Local oListBox		:= Nil
	Local oArea			:= Nil
	Local oList			:= Nil
	Local oButt1 		:= Nil
	Local oButt2 		:= Nil
	Local oButt3 		:= Nil
	Local aCols  		:= {}
	Local aColSizes 	:= { 60, 50, 20 }
	Local aCoord		:= {}
	Local aWindow		:= {}
	Local aHeader   	:= { "ID", "Nome", "CPF" }
	Local cTitulo   	:= "" 
	Local cQuery 		:= ""
	Local cAliasquery 	:= GetNextAlias()
	Local cFiltro		:= Space(50)
	Local Nx 			:= 0
	Local lLGPDperm := IIF(FindFunction("PROTDATA"),ProtData(),.T.)


	Default cEvtTrab := ""

	If Empty(cEvtTrab)

		cTitulo   := "Consulta Trabalhador Com/Sem Vinculo - S2200/S2205/S2300/Autonomo" 

		cQuery := " SELECT C9V_FILIAL Filial, C9V_ID Id, C9V_NOME Nome, C9V_CPF CPF, C9V_NOMEVE AS EVENTO "
		cQuery += " FROM "+RetSQLName("C9V")+" C9V "
		cQuery += " WHERE C9V.D_E_L_E_T_ = ' ' AND C9V_FILIAL = '" + xFilial("C9V") + "' AND C9V.C9V_ATIVO = '1' AND C9V.R_E_C_N_O_ NOT IN ("
		cQuery += " SELECT DISTINCT C9V.R_E_C_N_O_"
		cQuery += " FROM "+RetSQLName("C9V")+" C9V "
		cQuery += " INNER JOIN "+RetSQLName("T1U")+" T1U ON (T1U.T1U_FILIAL = C9V.C9V_FILIAL AND T1U.T1U_ID = C9V.C9V_ID) "
		cQuery += " WHERE T1U.D_E_L_E_T_ = ' ') "
		cQuery += " UNION "
		cQuery += " SELECT T1U_FILIAL Filial, T1U_ID Id, T1U_NOME Nome, T1U_CPF CPF, 'S2205' as EVENTO "
		cQuery += " FROM "+RetSQLName("T1U")+" T1U "
		cQuery += " WHERE T1U.D_E_L_E_T_ = ' ' AND T1U_FILIAL = '" + xFilial("T1U") + "' AND T1U.T1U_ATIVO = '1' AND T1U.R_E_C_N_O_ IN ( "
		cQuery += " SELECT DISTINCT T1U.R_E_C_N_O_ "
		cQuery += " FROM "+RetSQLName("T1U")+" T1U "
		cQuery += " INNER JOIN "+RetSQLName("C9V")+" C9V ON (C9V.C9V_FILIAL = T1U.T1U_FILIAL AND C9V.C9V_ID = T1U.T1U_ID) "
		cQuery += " WHERE C9V.D_E_L_E_T_ = ' ') "

	ElseIf cEvtTrab == "TAUTO"

		cTitulo   := "Consulta Trabalhador Autonomo" 
	
		cQuery := " SELECT C9V_FILIAL FILIAL, C9V_ID ID, C9V_NOME NOME, C9V_CPF CPF, C9V_NOMEVE AS EVENTO "
		cQuery += " FROM "+RetSQLName("C9V")+" C9V "
		cQuery += " WHERE C9V.D_E_L_E_T_ = ' ' AND C9V.C9V_FILIAL = '" + xFilial("C9V") + "' AND C9V.C9V_ATIVO = '1' AND C9V.C9V_NOMEVE = 'TAUTO' "

	EndIf
	
	cQuery := ChangeQuery(cQuery)

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasquery,.T.,.T.)

	While (cAliasquery)->(!Eof())
		aAdd(aCols,{(cAliasquery)->(Id), (cAliasquery)->(Nome), (cAliasquery)->(CPF), (cAliasquery)->(EVENTO) })
		(cAliasquery)->(dbSkip())
	End
	(cAliasquery)->(dbCloseArea())

	If Len(aCols) < 1
		aAdd(aCols,{" "," "," "," "})
	EndIf

	aCoord 	:= {000,000,400,800}
	aWindow := {020,073}

	oArea := FWLayer():New()
	oFather := tDialog():New(aCoord[1],aCoord[2],aCoord[3],aCoord[4],cTitulo,,,,,CLR_BLACK,CLR_WHITE,,,.T.)
	oArea:Init(oFather,.F., .F. )

	oArea:AddLine("L01",100,.T.)

	oArea:AddCollumn("L01C01",99,.F.,"L01")
	oArea:AddWindow("L01C01","TEXT","A��es",aWindow[01],.F.,.F.,/*bAction*/,"L01",/*bGotFocus*/)
	oText	:= oArea:GetWinPanel("L01C01","TEXT","L01")

	TSay():New(005,002,{||'Pesquisa Nome/CPF:'},oText,,,,,,.T.,,,200,20)
	TGet():New(003,057,{|u| if( PCount() > 0, cFiltro := u, cFiltro ) },oText,130,009,"@!",,,,,,,.T.,,,,.T.,,,.F.,,"","cFiltro",,,,.T.,.T.)
	oButt3 := tButton():New(003,190,"Pesquisar",oText,{||CheckFilF(oListBox,cFiltro)}, 45,11,,,.F.,.T.,.F.,,.F.,,,.F. )

	oArea:AddWindow("L01C01","LIST","Trabalhador",aWindow[02],.F.,.F.,/*bAction*/,"L01",/*bGotFocus*/)
	oList	:= oArea:GetWinPanel("L01C01","LIST","L01")

	oButt2 := tButton():New(003,239,"&Visualizar",oText,{||PosicTrab(aCols[oListBox:nAt,1],"1",aCols[oListBox:nAt,4])},45,11,,,.F.,.T.,.F.,,.F.,,,.F. )
	oButt1 := tButton():New(003,290,"&OK",oText,{||PosicTrab(aCols[oListBox:nAt,1]), oFather:End()},45,11,,,.F.,.T.,.F.,,.F.,,,.F. )
	oButt3 := tButton():New(003,340,"&Sair",oText,{|| oFather:End()},45,11,,,.F.,.T.,.F.,,.F.,,,.F. )

	oFather:lEscClose := .T.


	nTamCol := Len(aCols[01])
	bLine 	:= "{|| {"
	For Nx := 1 To nTamCol
		bLine += "aCols[oListBox:nAt]["+StrZero(Nx,3)+"]"
		If Nx < nTamCol
			bLine += ","
		EndIf
	Next
	bLine += "} }"

	oListBox := TCBrowse():New(0,0,386,130,,aHeader,,oList,'Fonte')
	oListBox:SetArray( aCols )
	oListBox:bLine := &bLine

	If !lLGPDperm
		oListBox:aObfuscatedCols :={.F.,.T.,.T.}
	EndIf

	If !Empty( aColSizes )
		oListBox:aColSizes := aColSizes
	EndIf
	oListBox:SetFocus()	

	oFather:Activate(,,,.T.,/*valid*/,,/*On Init*/)


Return(.T.)

//----------------------------------------------------------------------------------------------
/*/{Protheus.doc} PosNTrab
Fun��o para retornar o registro na consulta especifica SXB.
do trabalhador.

@author Henrique Cesar
@since 08/08/2019
@version 1.0

/*/       
//----------------------------------------------------------------------------------------------
//
Function PosNTrab()

Return (cRetNTrab)


//----------------------------------------------------------------------------------------------
/*/{Protheus.doc} PosicTrab
Fun��o responsavel por realizar o posicionamento no registro selecionado na consulta do trabalhador.

@author Henrique Cesar
@since 06/08/2019
@version 1.0

/*/       
//----------------------------------------------------------------------------------------------

Static Function PosicTrab(cIdTrab,cTipo,cEvent)

	Local aButtons := {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,Nil},{.T.,Nil},{.T.,Nil},{.T.,"Salvar"},{.T.,"Cancelar"},{.T.,Nil},{.T.,Nil},{.T.,Nil},{.T.,Nil},{.T.,Nil},{.T.,Nil}}
	Default cIdTrab:= ""
	Default cTipo  := ""		

	DbSelectArea("C9V")

	C9V->( DbSetOrder( 1 ) )

	If C9V->( DBSeek(xFilial("C9V")+ cIdTrab))
		cRetNTrab := C9V->C9V_ID
	EndIf

	If cTipo == "1"
		If cEvent == "S2205"
			DbSelectArea("T1U")

			T1U->( DbSetOrder( 1 ) )

			T1U->( DBSeek(xFilial("T1U")+ cRetNTrab))
			
			FWExecView('Cadastro do Trabalhador','TAFA275', MODEL_OPERATION_VIEW, , { || .T. }, , ,aButtons )
		Else
			FWExecView('Cadastro do Trabalhador','TAFA278', MODEL_OPERATION_VIEW, , { || .T. }, , ,aButtons )
		EndIf	
	EndIf

Return()

//----------------------------------------------------------------------------------------------
/*/{Protheus.doc} CheckFilF
Fun��o responsavel por realizar a pesquisa por Nome e/ou CPF do trabalhador.

@author Henrique Cesar
@since 06/08/2019
@version 1.0

/*/       
//----------------------------------------------------------------------------------------------

Static Function CheckFilF(oListBox,cFiltro)

	Local nPos  	 := 0
	Local lRet  	 := .F.
	Local ni		 := 1
	Local lPosPesq	 := .F.
	Local lCPF 		 := .F.
	Default oListBox := Nil
	Default cFiltro	 := ""

	cFiltro := AllTrim(cFiltro)

	// Faz um scan no objeto para encontrar a posi��o e posicionar no browser
	If Valtype(cFiltro) = "C" .And. !Empty(cFiltro)
		nPos := aScan( oListBox:aArray, {|x| x[2] == cFiltro } )
		If nPos == 0
			nPos := aScan( oListBox:aArray, {|x| x[3] == cFiltro } )
			lCPF := .T.
		EndIf

		If nPos > 0
			oListBox:GoPosition(nPos)
			oListBox:Refresh()
			lRet  := .T.
		EndIf

		// Pesquisa parcial
		If !lRet .and. lCPF
			For ni := 1 to Len(oListBox:aArray)
				lPosPesq := cFiltro $ oListBox:aArray[ni][2]
				If lPosPesq
					Exit
				EndIf
			Next ni
			If lPosPesq
				oListBox:GoPosition(ni)
				oListBox:Refresh()
			EndIf
		EndIf
	EndIf

	If nPos == 0 .And. !lPosPesq
		MsgAlert("N�o foi poss�vel encontrar o trabalhador " + cFiltro + " na pesquisa.")
	EndIf

Return
