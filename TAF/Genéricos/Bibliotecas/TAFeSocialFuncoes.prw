#Include 'Protheus.ch'
#INCLUDE "TOPCONN.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFESOCIALFUNCOES.CH" 

#DEFINE CRLF Chr(13)+Chr(10)

Static __lTAFConOut
Static __lTAFinCloud 
Static __aTblsEsocial
Static __aTblsFiscal
Static __aTblsReinf
Static __aTblsECF
Static __aTblsAutoCont
Static lTAFCodRub	:= FindFunction("TAFCodRub")
//-------------------------------------------------------------------
/*/{Protheus.doc} FeSocCallV
Realiza a chamada da View de um determinado evento
de acordo com os par�metros.
@author evandro.oliveira
@since 25/02/2016 
@version 1.0
@param cEvento, character, (C�digo do Evento e-Social)
@param nRecNo, num�rico, (RecNo do registro a ser exibido)
@param nOpc, num�rico, (Op��o do Cadastro 2-Visualizar,4-Alterar etc...)
@param nScan, num�rico, (Indice para busca no TafRotinas)
@param cFilPos, caracter, (Indica se deve posicionar na filial antes de executar o Painel do trabalhador, visto que internamente essa rotina faz filtro por filial )
@return ${Nil}
//teste
/*/
//-------------------------------------------------------------------
Function FeSocCallV(cEvento,nRecNo,nOpc,nScan,cFilPos, nOwner)

Local aEvento := {}

Private oBrowse, oPanelBrw, oTree
Private nRecnoFunc
Private lPainel, lFirstOpe, lExistAlt	

Default nScan := 4
Default cFilPos := ''
Default nOwner	:= 2

//Se for enviada a filial eu troco antes de executar o painel do trabalhador
If !empty( cFilPos )
	cFilposAnt := cFilAnt 
	cFilAnt := cFilPos
EndIf

If FPerAcess(/*cAliasTrb*/, cEvento, ""/*cMsg*/, 0/*nMark*/, ""/*cRotina*/, .T./*lJob*/, .F./*lMarkAll*/, cFilAnt/*cFilAccCFG*/)
	If cEvento == 'TAFAPNFUNC'
		DbSelectArea('C9V')
		C9V->(DbGoTo(nRecNo))     
		If !C9V->(Eof())
			FWExecView('' ,cEvento, nOpc,, {|| .T. } )
		Else
			Help(NIL, NIL, STR0051, NIL, STR0052, 1, 0, NIL, NIL, NIL, NIL, NIL, {STR0053})
		EndIf 
	Else
		aEvento := TAFRotinas(cEvento ,nScan,.F.,nOwner)
		DbSelectArea(aEvento[3])     
		(aEvento[3])->(DbGoTo(nRecNo))
		
		If !"C9V"$aEvento[3]
			FWExecView('' ,aEvento[1], nOpc,, {|| .T. } ) 
		Else
			AltCadTrab()
		EndIf	
		
	EndIf
Else
	//"Acesso negado!"- "O usu�rio corrente n�o possui privil�gios de acesso a rotina relacionada na filial do registro." - "Verifique as configura��es DOS privil�gios de acesso no configurador."
	Help(NIL, NIL, STR0054, NIL, STR0055, 1, 0, NIL, NIL, NIL, NIL, NIL, {STR0056})
EndIf

If !empty( cFilPos )
	cFilAnt := cFilposAnt
EndIf 
	
Return Nil 
//-------------------------------------------------------------------
/*/{Protheus.doc}
Verifica se o Servidor WS est� no Ar
@author evandro.oliveira
@since 06/04/2016
@version 1.0
@param cUrl, character, (Url do Servidor TSS)
@param cMsg, character, (Mensagem em caso de erro (refer�ncia))
@return ${lRet}, ${Indica de houve conex�o}
/*/
//-------------------------------------------------------------------
Function TafWVerUrl(cUrl,cMsg)

Local lRet := .T.
Local cCheckURL := ""

Default cUrl := TafGetUrlTSS()
Default cMsg := ""

If Empty(AllTrim(cUrl))
	cMsg := "O par�metro MV_TAFSURL n�o est� preenchido"
	xTAFMsgJob(cMsg)
	lRet := .F. 
EndIf

If lRet
	If  !("TSSWSSOCIAL.APW" $ Upper(cUrl)) 
		cCheckURL := cUrl
		cUrl += "/TSSWSSOCIAL.apw"
	Else
		cCheckURL := Substr(cUrl,1,Rat("/",cUrl)-1)
	EndIf
	
	If !TAFCTSpd(cCheckURL)
		cMsg := "Erro na conex�o com o TSS. Verificar se o servi�o est� iniciado e se o par�metro MV_TAFSURL est� correto" 
		lRet := .F.
	EndIf 
	
Endif

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} XVldAfastAf
Funcao utilizada para efetuar a valida��o de um funcion�rio,
verificando de j� assiste um afastamento para o mesmo.

@return lRet - Retorno da valida��o efetuada.

@author Vitor Siqueira / Denis R. de Oliveira
@since 25/11/2015	/ 09/11/2017

@version 1.0
   
/*/
//-------------------------------------------------------------------
Function xVldAfastAf( cFunc, dDataDig, nTipo )
	
Local lRet			:= .T.
Local aAreaCM6		:=	{}
Local aArea 		:= GetArea()
Local dDtVincIni	:= STOD("")

Default cFunc	:= ""

C9V->( dBSetOrder(2) )
If C9V->( MSSeek(xFilial("C9V") + cFunc + "1" ) )
	If C9V->C9V_STATUS == "4"

		CUP->( dbSetOrder( 1 ) )
		If CUP->( MsSeek( xFilial( "CUP" ) + C9V->C9V_ID + C9V->C9V_VERSAO ) )
					
			//------------------------------------------------------------
			//Verifico se a data de afastamento do S-2200 esta preenchida
			//------------------------------------------------------------
			If !Empty( CUP->CUP_DTINIA )
				dDtVincIni := CUP->CUP_DTINIA
			EndIf
						
		ElseIf ALLTRIM(C9V->C9V_NOMEVE) == "S2300" //TSV

			CUU->( dbSetOrder( 1 ) )
			If CUU->( MsSeek( xFilial( "CUU" ) + C9V->C9V_ID ) )
				If !Empty( CUU->CUU_DTINIA )
					dDtVincIni := CUU->CUU_DTINIA
				EndIf
			EndIf

		EndIf

		lRet := TAFAfasCM6( cFunc, dDataDig, dDtVincIni, nTipo )
	
	EndIf
EndIf

/*
C9V->(DBSetOrder(2)) //C9V_FILIAL, C9V_ID, C9V_ATIVO, R_E_C_N_O_, D_E_L_E_T_
C9V->(MSSeek(xFilial("C9V") + cFunc + "1" ) )
	

//Posiciono no vinculo referente ao trabalhador
CUP->(DBSetOrder(1))
CUP->( MsSeek( xFilial("CUP") + C9V->(C9V_ID + C9V_VERSAO) ) )

If C9V->C9V_STATUS == "4"

	aAreaCM6 :=	CM6->( GetArea() )
	//Verifico se existe afastamento para esse trabalhador
	CM6->( DbSetOrder( 9 ) )
	If CM6->( MsSeek( xFilial("CM6") + cFunc + "1" ) )
		
		If Empty(CUP->CUP_DTINIA) //Caso o funcion�rio n�o se encontre afastado (S-2200), continuo a valida��o
			If Empty(CM6->CM6_DTFAFA)
				Help( ,,STR0027,, xValStrEr("000748"), 1, 0 ) //"Aten��o" # "Para incluir um novo registro para este trabalhador � necessario que todos os outros registros ativos estejam com a data de termino preenchida."
				lRet := .F.
			ElseIf	!CM6->CM6_STATUS == "4" 	
				Help( ,,STR0027,, xValStrEr("001065"), 1, 0 ) //"Aten��o" # "Existe um afastamento vigente no TAF e pendente de envio ao Governo, at� que o envio seja realizado n�o � poss�vel a inclus�o de um novo afastamento para esse trabalhador."
				lRet := .F.
			EndIf
		EndIf
	
	EndIf
	RestArea(aAreaCM6)
Else
	
	Help( ,,STR0027,, xValStrEr("001066"), 1, 0 ) //"Aten��o" # "� necess�rio que o trabalhador esteja com status igual a '4'(Transmitido ao RET), antes que seja inclu�do um evento de afastamento."
	lRet := .F.
	
EndIF
*/

RestArea( aArea )
	
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} xTafAlt
Cria uma interface para a altera��o do evento de acordo com o tipo de
opera��o 

@Param  cAlias	- Alias do Browse
		 nReg 	- Registro posicionado no Browse
		 nOpc	- Opera��o de Manuten��o 

@Return Nil 

@Author Paulo V.B. Santana
@Since 27/01/2016
@Version 1.0
/*/
//------------------------------------------------------------------
Function xTafAlt(cAlias, nReg, nOpc)

Local cTitulo 	:= ""
Local cNmFun  	:= Iif(FindFunction("TafSeekRot"), TafSeekRot(cAlias), FunName())
Local aTafRot 	:= {}
Local cOper		:= ""
Local lVldOp	:= .T.
Local cEvento	:= ""
Private cOperEvnt	:= cValtoChar(nOpc)

Default	nOpc	:= 0

aTafRot 	:= TAFRotinas(cNmFun,1,.F.,2)
cAlias		:= aTafRot[3]
cEvento	:= aTafRot[4]

// --> S� executa as valida��es de status, se o registro controlar esse tipo de status
If TafColumnPos( (cAlias + "_STATUS" ) )

	If (cAlias)->&( cAlias + "_STATUS" ) ==  "2" 
		lVldOp := .F.
		msgAlert(xValStrEr("000727"))
	ElseIf (cAlias)->&( cAlias + "_STATUS" ) == "6"
		lVldOp := .F.
		msgAlert(xValStrEr("000728"))
	Elseif (cAlias)->&( cAlias + "_STATUS" ) == "7"	
		lVldOp := .F.					
		msgAlert(xValStrEr("000772"))
	Elseif (cAlias)->&( cAlias + "_STATUS" ) == "4"	 .And. (cAlias)->&( cAlias + "_EVENTO" ) == "E"	.And. (cAlias)->&( cAlias + "_ATIVO" ) == "2"	
		lVldOp := .F.					
		msgAlert(xValStrEr("001117"))		
	Endif

	If lVldOp
		If nOpc == 1 .AND.(cAlias)->&( cAlias + "_STATUS" ) == '4'
			
			If IsBlind()
				cOper := 1
			Else
				cOper := Aviso( STR0001,STR0005, { "OK", "Cancelar" },3 )   // "Esta opera��o permite retificar o �ltimo evento transmitido, referente a este trabalhador. Para retificar outro evento � necess�rio excluir as informa��es enviadas ap�s a transmiss�o do evento � ser retificado.")
			EndIf

			If cOper <> 1
				lVldOp  := .F.
			Else
				cTitulo := STR0001 // "Retifi��o do Evento"
			Endif	
		ElseIf nOpc == 2 
			If (cAlias)->&( cAlias + "_EVENTO" ) <> "A" .And. Empty( (cAlias)->&( cAlias + "_STATUS" ) ) 
				msgAlert(STR0006) //"N�o � poss�vel cadastrar um evento de 'Altera��o', enquanto houver outro evento pendente de transmiss�o."
				lVldOp := .F.
			ElseIf (cAlias)->&( cAlias + "_EVENTO" ) == "F"
				msgAlert(Iif(cAlias == "CM8",STR0010,STR0007))//"N�o � poss�vel cadastrar um evento de 'Altera��o', enquanto houver um evento de 'Finaliza��o' ativo."
				lVldOp := .F.
			Else
				cTitulo := STR0002 // "Altera��o do Evento "
			Endif
		ElseIf nOpc == 3
			
			If cAlias == "CM6"
				If TafColumnPos( cAlias + "_XMLREC" )
					If (cAlias)->&( cAlias + "_XMLREC" ) == "COMP"
						Help( ,, STR0027,, STR0050, 1, 0 )//"N�o � poss�vel realizar um T�rmino de Afastamento para um registro completo."
						lVldOp := .F.
					EndIf
				EndIf
			EndIF
		
			If !cAlias == "CM6"
				If (cAlias)->&( cAlias + "_EVENTO" ) <> "F" .AND. Empty( (cAlias)->&( cAlias + "_STATUS" ) )
					msgAlert(Iif(cAlias == "CM8",STR0009,STR0008))//"N�o � poss�vel cadastrar um evento de 'Finaliza��o', enquanto houver outro evento pendente de transmiss�o."
					lVldOp := .F.
				Else
					cTitulo := IIF(cNmFun == "TAFA263",STR0003,STR0004) //"Cancelamento do Aviso Pr�vio" # "Finaliza��o do Evento"
				Endif
			EndIf
		Endif
	Endif

EndIf 

If lVldOp
	If !IsBlind()
		FWExecView(cTitulo,cNmFun,4)
	EndIf
Endif

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} TafCloseTb
Cria uma interface para a altera��o do evento de acordo com o tipo de
opera��o 

@Param  cOrigem - Identifica a origem da chamada (I=integracao,C=Cadastro(tela))
		 oModel - Model do regitro Pai
		 cAlias - Alias do registro Pai
		 idModelo - Id do ModelocDt
		cDtOldFim - Data Fim do registro Anterior (somente eventos de tabelas)
		cDtNewReg - Valor do campo Data Inicial do registro Novo depois do input no campo (somente eventos de tabelas)
		cDtOldReg - Valor do campo Data Inicial do registro Novo antes do input no campo (somente eventos de tabelas)
		cDtFinR - Data para grava��o do registro anterior (somente quando cOrigem for igual a I), passar por refer�ncia
@Return Nil 

@Author Evandro dos Santos Oliveira
@Since 13/05/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TafCloseTb(cOrigem,oModel,cAlias,idModelo,cDtOldFim,cDtNewReg,cDtOldReg,cDtFinR)

	Local cMes := ""
	Local cAno := ""	
	Local cDtIni := ""
	Local cDtIniAnt := ""
	
	Default oModel := Nil 
	Default cDtOldFim := ""
	Default cDtNewReg := ""
	Default cDtOldReg := ""
	Default cDtFinR := ""
	
	
	If Empty(cDtOldFim)
		If cOrigem = 'I'
			cDtOldReg := StrZero(Month(cDtOldReg),2)+StrZero(Year(cDtOldReg),4)
		EndIf
		cDtIniAnt := Substr(cDtOldReg,3,4) + Substr(cDtOldReg,1,2)
		If Len(cDtNewReg) > 5 
			cDtIni := Substr(cDtNewReg,3,4) + Substr(cDtNewReg,1,2)
		EndIf
		If cDtIni == cDtIniAnt
			If cOrigem = 'I'
				cDtFinR := cDtNewReg	
			Else
				oModel:LoadValue(idModelo,cAlias+"_DTFIN",cDtNewReg)
			EndIf
		ElseIf cDtIni > cDtIniAnt
			cMes := Substr(cDtNewReg,1,2)
			cAno := Substr(cDtNewReg,3,4)
			cMes := StrZero((Val(cMes)-1),2)
			//Quando m�s for 00 ser� igual a 12 e diminui um ano
			If(cMes == '00')
				cMes := '12'
				cAno := StrZero((Val(cAno)-1),4)
			EndIf 
			If cOrigem = 'I'
				cDtFinR := cMes+cAno
			Else
				oModel:LoadValue(idModelo,cAlias+"_DTFIN",cMes+cAno)
			EndIf
		EndIf
	EndIf

Return Nil	
//-------------------------------------------------------------------
/*/{Protheus.doc} TafCacheModel
Grava e Retorna dados do Modelo de acordo com os par�metros
@type function
@author Evandro
@since 17/06/2016
@version 1.0
@param lForm, ${boolean}, (Define se o modelo � um formul�rio(.T.), no caso de ser uma grid utilizar .F.)
@param oModel, objeto, (Model do Fonte)
@param cModelTab, character, (Id do Modelo)
@param cAliasTab, character, (Alias do (Pai) do Modelo)
@param cExclCmp, character, (campos a serem desconsiderados(separar por pipe))
@return ${aCampos}, ${Campos com seus respectivos valores}
/*/
//-------------------------------------------------------------------
Function TafCacheModel(lForm,oModel,cModelTab,cAliasTab,cExclCmp)

	Local oModelTab 	:= Nil 
	Local aCampos  	:= {}
	Local aAuxCmps 	:= {}
	Local cCmpsExcl 	:= ""
	Local nX,nY		:= 0
	Default cExclCmp 	:= ""
	Default lForm		:= .F.

	cCmpsExcl := cAliasTab + "_FILIAL|"
	//cCmpsExcl += cAliasTab + "_ID|" //O campo Id foi retirado da lista pois estava gerando novo Id para novas vers�es do mesmo registro
	cCmpsExcl += cAliasTab + "_VERSAO|"
	cCmpsExcl += cAliasTab + "_VERANT|"
	cCmpsExcl += cAliasTab + "_PROTUL|"
	cCmpsExcl += cAliasTab + "_PROTPN|"
	cCmpsExcl += cAliasTab + "_ATIVO|"
	cCmpsExcl += cAliasTab + "_EVENTO|"
	cCmpsExcl += cExclCmp

	oModelTab := oModel:GetModel(cModelTab)
	If lForm
		//aEval(oModelTab:aDataModel[1][1],{|x|aAdd(aAuxCmps,{x[1],x[2]})}) 
		For nX := 1 To Len(oModelTab:aDataModel[1])
			If !(oModelTab:aDataModel[1][nX][1] $ cCmpsExcl)
				aAdd(aAuxCmps,{oModelTab:aDataModel[1][nX][1],oModelTab:aDataModel[1][nX][2]})
			EndIf
		Next nX
		aAdd(aCampos,aAuxCmps)
	Else
		For nX := 1 To Len(oModelTab:aCols) 
			For nY := 1 To (Len(oModelTab:aCols[1])-1)
				If !(oModelTab:aHeader[nY][2] $ cCmpsExcl)
					aAdd(aAuxCmps,{oModelTab:aHeader[nY][2],oModelTab:aCols[nX][nY]})
				EndIf
			Next nY
			aAdd(aCampos,aAuxCmps)
			aAuxCmps := {}
		Next nX
	EndIf

Return aCampos
//-------------------------------------------------------------------

/*/{Protheus.doc} TafSetModel
Atribui os valores do array aGrava no modelo passado pelo oModel e cIdModel.
@author Evandro
@since 20/06/2016
@version 1.0
@param lForm, ${boolean}, (Define se o modelo � um formul�rio(.T.), no caso de ser uma grid utilizar .F.)
@param oModel, objeto, (Model do Fonte)
@param cIdModel, character, (Id Do Model)
@param lDelRegs, boolean,(Indica se deve apagar os registros do Model antes da Inclus�o)
@param aGrava, array, (Dados do Modelo)
@return ${boolean},, ${Informa que o processamento foi executado com sucesso}
/*/
//-------------------------------------------------------------------
Function TafSetModel(lForm,oModel,cIdModel,lDelRegs,aGrava)
	
	Local nJ,nI := 0
	Local lReturn := .F.
	Default lForm := .F.

	If lDelRegs
		For nJ := 1 to oModel:GetModel(cIdModel):Length()
			oModel:GetModel(cIdModel):GoLine(nJ)
			oModel:GetModel(cIdModel):DeleteLine()
		Next nJ
	EndIf
	
	For nI := 1 To Len(aGrava)
	
		If !lForm
			oModel:GetModel(cIdModel):lValid:= .T.
			oModel:GetModel(cIdModel):AddLine()
		EndIf

		For nJ := 1 To Len(aGrava[nI])
			oModel:LoadValue(cIdModel,aGrava[nI][nJ][1],aGrava[nI][nJ][2])
		Next nJ
	Next nI
	
	//Verifico se o la�o foi executado at� o fim
	lReturn := nI > Len(aGrava)

Return lReturn
//-------------------------------------------------------------------
/*/{Protheus.doc} TafGetAGrv
Retorna valor do campo contido no Array aGrava
@author Evandro
@since 20/06/2016
@version 1.0
@param aGrava, array, (Array Multidimensional com os dados do modelo sendo a primeira posic�o o campo e a segunda o valor)
@param	cCampo, string, (Campo para busca no Array)
@return ${several types}, ${O Array de campos contem varios tipos de dados, por isso n�o � possivel especificar o retorno}
/*/
//-------------------------------------------------------------------
Function TafGetAGrv(aGrava,cCampo)

	Local nPos := 0
	Local xReturn := Nil
	
	nPos := aScan(aGrava,{|x|x[1] == cCampo})
	If nPos > 0
		xReturn := aGrava[nPos][2]
	EndIf
	
Return xReturn 
//-------------------------------------------------------------------
/*/{Protheus.doc} TafFormCommit

Executa o commit do processamento atrav�s do FWFormCommit e deve capturar poss�vel 
erro de chave duplicada na integra��o.

@param	oModel - Model que sera realizado o commit

@author Vitor Siqueira 

@since 20/01/2017

@version 1.0

/*/
//-------------------------------------------------------------------
Function TafFormCommit( oModel, lTypeRet )

Local lErroInt		:=	.F.
Local cDescError	:= ""
Local cRetError		:= ""
Local oError		:=	ErrorBlock( { |oError| TafTratFWForm( oError, @lErroInt, @cDescError ) } )

Default lTypeRet 	:= .F.

Begin Sequence
	FWFormCommit( oModel )
End Sequence

If lErroInt
	If !Empty( cDescError )
		cRetError := GetAlsDup(cDescError)
	EndIf
	DisarmTransaction()
EndIf

oError := Nil

//Tratamento paleativo. Para que, n�o haja a necessidade de se alterar todos os fontes de uma s� vez :(
If lTypeRet
	Return( { lErroInt, cRetError, cDescError } )
Else
	Return lErroInt
EndIf

//-------------------------------------------------------------------
/*/{Protheus.doc} TafTratFWForm

Efetua tratamento de error log da chave duplicada

@param	oError - Objeto carrega erro

@author Vitor Siqueira 

@since 20/01/2017

@version 1.0

/*/
//-------------------------------------------------------------------
Function TafTratFWForm( oError, lErroInt, cDescError )

Local cErr  := oError:Description
Local cErro := ""

cDescError	 := cErr
lErroInt 	 := .T.
cErro :=  cErr + chr(10) + chr(13) 
ConOut(cErro)

Return( NIL )

//----------------------------------------------------------------------------
/*/{Protheus.doc} getTafDStatus 
Retorna a descri��o do Status do registro no TAF.

@param cStatus 	- C�digo do Status

@return cDescr		- Descri��o do Status

@author Evandro dos Santos O. Teixeira
@since 02/01/2017
@version 1.0

/*/
//--------------------------------------------------------------------------- 
Function getTafDStatus(cStatus)

	Local cDescr := ""

	If Empty(AllTrim(cStatus))
		cDescr := STR0011 //"Registro Integrado"
	ElseIf AllTrim(cStatus) == "0"
		cDescr := STR0012 //"Registro Valido."
	ElseIf AllTrim(cStatus) == "1"
		cDescr := STR0013 //"Registro Invalido."
	ElseIf AllTrim(cStatus) == "2"
		cDescr := STR0014 //"Registro Transmitido."
	ElseIf AllTrim(cStatus) == "3"
		cDescr := STR0015 //"Registro Transmitido com inconsist�ncia(s)."
	ElseIf AllTrim(cStatus) == "4"
		cDescr := STR0016 //"Registro Transmitido V�lido."
	ElseIf AllTrim(cStatus) == "6"
		cDescr := STR0017 //"Registro Com Solicita��o de Exclus�o."
	ElseIf AllTrim(cStatus) == "7"
		cDescr := STR0018 //"Registro Com Solicita��o de Exclus�o e Transmitido."
	EndIf
	
Return (cDescr) 
//-------------------------------------------------------------------
/*/{Protheus.doc} xTafTagGroup
Verifica se existe foi criada pelo menos 1 tag Filha para a cria��o da
tag Pai. 
*Utilizado quando um registro tem ocorr�ncia 0-1

@param cTagPai    	- Nome da Tag Pai
@param aTagFilhas	- array com as informa��es da Tag Filha
					  [n][1] - Nome da Tag Filha
					  [n][2] - Valor da Tag
					  [n][3] - Picture
					  [n][4] - Informa se a tag deve ser gerada somente
					  quando preenchida (ocorr�ncia 0-1)
					  .T. - gera somente quando preenchida
					  .F. - gera independente se estiver preenchiada ou n�o
					  [n][5] - Indica se deve ser formatado os campos n�merico conforme o Esocial
					  .T. - Troca virgula por ponto na casa de decimais
					  .F. - Mantem padr�o do protheus.	  
					  [n][6] - For�a a gera��o de tags num�ricas com valor 0 (zero) 
					  .T. - gera tag numerica zerada
					  .F. - considera nulo o valor de tag numerica zerada
					  
@param cXml    	- String de gera��o do XML - (Passar como refer�ncia)
@param aGrpFilhos	- Array com Strings de grupos filhos
					  [n][1] - Nome da Tag Filha
					  [n][2] - Trecho do XML da tag Filha
					  Obs: Enviar String passada por par�metro dentro desta
					  mesma fun��o quando o Grupo Filho foi gerado. Obrigatoriamente
					  os grupos filhos devem ser gerados antes do Pai para possibilitar
					  a verifica��o de exist�ncia dos mesmos.
					  [n][3] - Ocorr�ncia do Grupo (num�rico)

@param lGrpObrigat	- Flag que indica se o grupo de tags eh obrigatorio e dever� ser
					  gerado independente se uma das tags tiver conte�do.
					  
@return cRet - Retorno com a categoria encontrada.

@author Evandro dos Santos O. Teixeira
@since 27/04/2017
@version 1.0
@exemple 

xTafTagGroup("procAdmJudRat"	,{{"tpProc"		,cTpProc					,,.F.};
									, {"nrProc"		,nProcesso					,,.F.};
									, {"codSusp"	,Alltrim(C92->C92_CODSUF)	,,.T.}};
									, @cXmlProcRat)	
			  
xTafTagGroup("procAdmJudFap"	,{{"tpProc"		,cTpProc			,,.F.};
									, {"nrProc"		,nProcesso			,,.F.};
									, {"codSusp"	,C1E->C1E_VLRSUB	,,.T.}};
									, @cXmlProcFat)
														
xTafTagGroup("aliqGirat"			,{{"aliqRat"		,C92->C92_ALQRAT	,								,.F.}};
									, {"fap"			,C92->C92_FAP		,PesqPict("C92","C92_FAP")		,.T.};
									, {"aliqRatAjust"	,C92->C92_AJURAT	,PesqPict("C92","C92_AJURAT")	,.T.};
									, @cXml,{{"procAdmJudRat",cXmlProcRat,0},{"procAdmJudFap",cXmlProcFat,0}})	
   
/*/
//-------------------------------------------------------------------
Function xTafTagGroup( cTagPai , aTagFilhas , cXml , aGrpFilhos , lGrpObrigat, lGrpComp )

	Local nX 			:= 0
	Local cAuxXml 		:= ""
	Local lExistVal 	:= .F.
	Local lExistGrp 	:= .F.

	Default cTagPai 	:= ""
	Default cXml 		:= ""
	Default aTagFilhas 	:= {}
	Default aGrpFilhos 	:= {}
	Default lGrpObrigat := .F.
	Default lGrpComp 	:= .F.
	
	For nX := 1 To Len(aTagFilhas)
		If !Empty(aTagFilhas[nX][2])
			lExistVal := .T.
			Exit
		EndIf
	Next nX

	For nX := 1 To Len(aGrpFilhos)
		If !Empty(aGrpFilhos[nX][2])
			lExistGrp := .T.
			Exit
		EndIf
	Next nX
	
	If lExistVal .or. lGrpObrigat .or. (lGrpComp .and. lExistGrp)

		For nX := 1 To Len(aTagFilhas)	
			cAuxXml += xTafTag(	 aTagFilhas[nX][1];
									,aTagFilhas[nX][2];
									,IIf(Len(aTagFilhas[nX]) > 2,aTagFilhas[nX][3],Nil);
									,IIf(Len(aTagFilhas[nX]) > 3,aTagFilhas[nX][4],Nil);
									,IIf(Len(aTagFilhas[nX]) > 4,aTagFilhas[nX][5],Nil);
									,IIf(Len(aTagFilhas[nX]) > 5,aTagFilhas[nX][6],Nil))
		Next nX
		
		cXml += "<"+cTagPai+">"
		cXml += cAuxXml
		If lExistGrp
			For nX := 1 To Len(aGrpFilhos)
				If Empty(aGrpFilhos[nX][2])
					cXml += IIf(aGrpFilhos[nX][3] == 1,"<"+aGrpFilhos[nX][1]+"></"+aGrpFilhos[nX][1]+">","") //Se o Grupo for obrigatorio eu gero a tag em branco
				Else
					cXml += aGrpFilhos[nX][2]
				EndIf
			Next nX
		EndIf
		cXml += "</"+cTagPai+">"

	EndIf
	
	aSize(aTagFilhas,0)
	aSize(aGrpFilhos,0)
	
Return (cXml)

//----------------------------------------------------------------------------
/*/{Protheus.doc} TAFGetDTpEvento 
Retorna a descri��o do Tipode Evento do registro no TAF.

Return (cXml)
@return cDescr		- Descri��o do Tipo de Evento

@author Luccas Curcio
@since 25/05/2017
@version 1.0

/*/
//--------------------------------------------------------------------------- 
function TAFGetDTpEvento( cTpEvento )

	local cDescr := ""

	if empty( allTrim( cTpEvento ) )
		cDescr := STR0019 //'Desconhecido'
	elseif allTrim( cTpEvento ) == "I"
		cDescr := STR0020 //'Inclus�o'
	elseif allTrim( cTpEvento ) == "A"
		cDescr := STR0021 //'Altera��o'
	elseif allTrim( cTpEvento ) == "E"
		cDescr := STR0022 //'Exclus�o'
	elseif allTrim( cTpEvento ) == "R"
		cDescr := STR0023 //'Retifica��o'
	elseif allTrim( cTpEvento ) == "F"
		cDescr := STR0024 //'Finaliza��o'
	elseif allTrim( cTpEvento ) == "C"
		cDescr := STR0025 //'Cancelamento'
	endif
	
return ( cDescr )

//----------------------------------------------------------------------------
/*/{Protheus.doc} TAFGetLastRcn 
Retorna �ltimo recno de chave enviada como par�metro

@return nMaxRec		- Ultimo recno desejado

@author Luccas Curcio
@since 25/05/2017
@version 1.0

/*/
//--------------------------------------------------------------------------- 
function TAFGetLastRcn( cTicket , cKey , lValido , nStartRecNo )

Local cQry       := ""
Local cWhere     := ""
Local cTafTicket := ""
Local cTafKey    := ""
Local cJoin      := ""
Local nMaxRec    := 0

Default cTicket  := ""
Default cKey 	 := ""

cTafTicket	:= AllTrim(cTicket)
cTafKey 	:= AllTrim(cKey)

If !Empty(cTafTicket) .And.  !Empty(cTafKey)

	cWhere := " ST2.TAFTICKET =  '" + cTafTicket +  "'"
	cWhere += " AND ST2.TAFKEY = '" + cTafKey + "'"
	cWhere += " AND ST2.D_E_L_E_T_ <> '*'"
ElseIf !Empty(cTafTicket)

	cWhere := " ST2.TAFTICKET =  '" + cTafTicket +  "'"
	cWhere += " AND ST2.D_E_L_E_T_ <> '*'"
Else 

	cWhere := " ST2.TAFKEY = '" + cTafKey + "'"
	cWhere += " AND ST2.D_E_L_E_T_ <> '*'"
EndIf

If lValido
	cWhere += " AND ST2.TAFSTATUS = '3' AND XERP.TAFSTATUS = '1' "
EndIf

cWhere += " AND ST2.R_E_C_N_O_ >= " + AllTrim(Str(nStartRecNo))

cJoin := " LEFT JOIN TAFXERP XERP ON  ST2.TAFKEY = XERP.TAFKEY "
cJoin += " AND ST2.TAFTICKET = XERP.TAFTICKET "
cJoin += " AND XERP.D_E_L_E_T_ <> '*' "

cQry := " SELECT MAX(ST2.R_E_C_N_O_) RECNO"
cQry += " FROM TAFST2 ST2 "
cQry += cJoin
cQry += " WHERE "
cQry += cWhere

cQry := ChangeQuery(cQry)
dbUseArea( .T. , "TOPCONN" , TcGenQry(,,cQry) ,"MAXST2")

nMaxRec := MAXST2->RECNO

MAXST2->(dbCloseArea())

return ( nMaxRec )

//----------------------------------------------------------------------------
/*/{Protheus.doc} TAFxSelMod 
Fun��o que retorna o modelo de montagem de uma mensagem para retorno do WS

@return cMod		- Modelo de montagem da mensagem

@author Luccas Curcio
@since 25/05/2017
@version 1.0

/*/
//--------------------------------------------------------------------------- 
function TAFxSelMod( cTicket , cKey )

	Local cMod := ""
	
	Default cTicket 	:= ""
	Default cKey 		:= ""

	If !Empty(AllTrim(cTicket)) .And. !Empty(AllTrim(cKey)) 
		cMod := 'both'
	ElseIf !Empty(AllTrim(cTicket)) .And. Empty(AllTrim(cKey))
		cMod := 'ticket'
	Else
		cMod := 'key'
	EndIf
	
return cMod

//----------------------------------------------------------------------------
/*/{Protheus.doc} TAFSeek 
Seek perform�tico para funcionalidades do TAF

@author Luccas Curcio
@since 25/05/2017
@version 1.0

/*/
//--------------------------------------------------------------------------- 
function TAFSeek( cAliasTXERP , cAliasTaf , nRecno )

	//Evito ficar abrindo a �rea desnecessariamente
	If cAliasTaf != cAliasTXERP
		cAliasTaf := cAliasTXERP
		dbSelectArea(cAliasTaf)
		dbSetOrder(1)
	EndIf
	(cAliasTaf)->(dbGoTo(nRecno))

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} xVldCPFDep
Verifica se j� existe um CPF de Dependente na base.

@Param  cVlrOri  -> Valor do campo
@Param  nTpTrc   -> Tipo de Troca

@Return .T.

@Author Daniel Otero Schmidt
@Since 30/06/2017
@Version 1.0
/*/
//-------------------------------------------------------------------

Function xVldCPFDep(cAlias, cCPFDep, cEvento, cCnpjOp, cPerApu )

Local cAliasQry	:= GetNextAlias()
Local cQuery		:= ""
Local cRet			:= .F.
Local cAlias2		:= cAlias + "2"

Default cEvento	    := "" 
Default cCPFDep 	:= ""
Default cCnpjOp     := ""
Default cPerApu     := ""
	
If cEvento $ "2299|1202|1200|2399"
	If !Empty( cCPFDep )
		cQuery := "SELECT " 
		cQuery += "COUNT(" + cAlias + "." + cAlias + "_CPFDEP) CONT" 

		cQuery += "FROM "
		cQuery += RetSqlName( cAlias ) + " " + cAlias 

        If cEvento $ "1200" 

            cQuery += " INNER JOIN " + RetSqlName("C91") + " C91 "
            cQuery += "   ON C91.C91_FILIAL = T6Z.T6Z_FILIAL "
            cQuery += "  AND C91.C91_ID = T6Z.T6Z_ID "
            cQuery += "  AND C91.C91_VERSAO = T6Z.T6Z_VERSAO "
            cQuery += "  AND C91.D_E_L_E_T_ = ' ' "

        EndIf

		cQuery += "WHERE " 
		cQuery += cAlias + "." + cAlias	+ "_FILIAL='" + xFilial( cAlias )	+ "' AND "
		cQuery += cAlias + "." + cAlias	+ "_CPFDEP='" + cCPFDep				+ "' AND "
		cQuery += cAlias + "." + cAlias	+ "_CNPJOP='" + cCnpjOp				+ "' AND "

        If cEvento $ "1200"

            cQuery += "C91.C91_PERAPU = '" + cPerApu + "' AND " 
        
        EndIF
		
		If cEvento == "2399"
			cQuery += cAlias + "." + cAlias + "_VERSAO IN "
			cQuery += "(SELECT MAX(" + cAlias2 + "." + cAlias + "_VERSAO) VERSAO FROM "
			cQuery += RetSqlName( cAlias ) + " " + cAlias2 
			cQuery += " WHERE " + cAlias2 + "." + cAlias + "_FILIAL = " + cAlias +"." + cAlias + "_FILIAL AND " 
			cQuery += cAlias2 + "." + cAlias + "_ID =" + cAlias + "." + cAlias + "_ID AND " + cAlias2 + ".D_E_L_E_T_=' ' ) AND "
		EndIf 
		cQuery += cAlias + "." + "D_E_L_E_T_=' ' "
		cQuery := ChangeQuery( cQuery )
	
		dbUseArea( .T. , "TOPCONN" , TcGenQry( , , cQuery ) , cAliasQry ) 
				
		If (cAliasQry)->CONT > 1		
			cRet := .T.
		Endif			
	Endif
EndIf	
	
Return(cRet)


Function TAFGrvLog(cLog,cArquivo)

Local nHandle	:= 0
Local cPath := "\taflog\"


Default cLog := ""
Default cArquivo := "taf.log"

If !File(cPath + cArquivo)
	nHandle := FCREATE(cPath + cArquivo, FC_NORMAL) 
	
	if nHandle > 0
		ConOut("Criado arquivo  " + cArquivo)
	else
		ConOut("Erro ao criar arquivo  " + cArquivo)
	EndIf
	
	FCLOSE(nHandle)
EndIf

nHandle := FOpen(cPath + cArquivo, FO_READWRITE + FO_SHARED)
if nHandle > 0
	FSEEK(nHandle, 0, FS_END)
	FWrite(nHandle,cLog) 
	FClose(nHandle)
endIf	

Return Nil 

//-------------------------------------------------------------------
/*/{Protheus.doc} TafNormTelES
Realiza a Normaliza��o do DDD e Telefone de uma String retornando
somente numeros.

@Param  cTelFull  -> String com o Telefone Completo

@Return aTel -> [1]DDD,[2]Telefone

@Author Evandro dos Santos Oliveira
@Since 06/10/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TafNormTelES(cTelFull)

	Local cDDD := ""
	Local cDDI := ""
	Local cTel := ""
	Local nTamDDD := 0
	Local nTamDDI := 0
	Local aTel := Array(2)

	cTelFull := AllTrim(cTelFull)
	cTelFull := StrTran(cTelFull," ","")
	cTelFull := StrTran(cTelFull,"-","")
	cTelFull := StrTran(cTelFull,"(","")
	cTelFull := StrTran(cTelFull,")","")

	If Substr(cTelFull,1,1) == "0"
		nTamDDD := 3
	ElseIf Len(cTelFull) == 13
		nTamDDI := 2
		nTamDDD := 2
	Else
		nTamDDD := 2
	EndIf

	If Len(cTelFull) == 13 .And. TafColumnPos("C9V_DDIFPR")
		aTel := Array(3)
		cDDI := Substr(cTelFull,1,nTamDDI)	
		cDDD := Substr(cTelFull,3,nTamDDD)
		cTel := Substr(cTelFull,nTamDDI+nTamDDD+1)
	Else
		cDDD := Substr(cTelFull,1,nTamDDD)
		cTel := Substr(cTelFull,nTamDDD+1)
	EndIf

	aTel[1] := cDDD
	aTel[2] := cTel

	If Len(cTelFull) == 13 .And. TafColumnPos("C9V_DDIFPR")
		aTel[3] := cDDI
	EndIf

Return(aTel)

//-------------------------------------------------------------------
/*/{Protheus.doc} TafGetDtTab


@param cDataIni -> Data Inicial
@param cDataFim -> Data Final


@Return cXml -> Retorno do Xml

@Author Evandro dos Santos Oliveira
@Since 20/10/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TafGetDtTab(cDataIni,cDataFim)

	Local cXml := ""

	If !Empty(cDataIni)
		cDataIni := Substr(cDataIni,3,4) +"-"+ Substr(cDataIni,1,2)
	EndIf
	If !Empty(cDataFim)
		cDataFim := Substr(cDataFim,3,4) +"-"+ Substr(cDataFim,1,2)
	EndIf

	cXml +=	xTafTag("iniValid",cDataIni)
	cXml +=	xTafTag("fimValid",cDataFim,,.T.)

Return (cXml)

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFTabVig

Verifica se o registro referente ao evento de tabela do eSocial
est� vigente no per�odo de refer�ncia indicado.

@Param	cAlias		-	Alias referente a tabela do evento
		cID			-	ID do registro para verifica��o de vig�ncia
		cPeriodo	-	Per�odo de refer�ncia da pesquisa ( AAAAMM )
		lPosiciona	-	Indica se deve posicionar no registro pesquisado
						Obs: Para utilizar este par�metro, deve-se realizar
						DBSelectArea e DBSetOrder previamente

@Return	lRet		-	Indica se o registro est� vigente

@Author		Felipe C. Seolin
@Since		03/11/2017
@Version	1.0
/*/
//-------------------------------------------------------------------
Function TAFTabVig( cAlias, cID, cPeriodo, lPosiciona )

Local cAliasQry	:=	GetNextAlias()
Local cSelect	:=	""
Local cFrom		:=	""
Local cWhere	:=	""
Local cPrefixo	:=	cAlias + "." + cAlias
Local lRet		:=	.F.

Default lPosiciona	:=	.F.

cPeriodo := SubStr( cPeriodo, 5, 2 ) + SubStr( cPeriodo, 1, 4 )

cSelect	:= " " + cAlias + ".R_E_C_N_O_ RECNO "
cFrom	:= RetSqlName( cAlias ) + " " + cAlias + " "
cWhere	:= " " + cPrefixo + "_FILIAL = '" + xFilial( cAlias ) + "' "
cWhere	+= "AND " + cPrefixo + "_ID = '" + cID + "' "
cWhere	+= "AND ( SUBSTRING( " + cPrefixo + "_DTINI, 3, 4 ) < '" + SubStr( cPeriodo, 3 , 4 ) + "' OR ( SUBSTRING( " + cPrefixo + "_DTINI, 3, 4 ) = '" + SubStr( cPeriodo, 3 , 4 ) + "' AND SUBSTRING( " + cPrefixo + "_DTINI, 1, 2 ) <= '" + SubStr( cPeriodo, 1 , 2 ) + "' ) ) 
cWhere	+= "AND ( ( SUBSTRING( " + cPrefixo + "_DTFIN, 3, 4 ) > '" + SubStr( cPeriodo, 3 , 4 ) + "' OR ( SUBSTRING( " + cPrefixo + "_DTFIN, 3, 4 ) = '" + SubStr( cPeriodo, 3 , 4 ) + "' AND SUBSTRING( " + cPrefixo + "_DTFIN, 1, 2 ) > '" + SubStr( cPeriodo, 1 , 2 ) + "' ) ) OR " + cPrefixo + "_DTFIN = '' ) 
cWhere	+= "AND " + cPrefixo + "_ATIVO = '1' "
cWhere	+= "AND " + cAlias + ".D_E_L_E_T_ = '' "

cSelect	:= "%" + cSelect + "%"
cFrom	:= "%" + cFrom   + "%"
cWhere	:= "%" + cWhere  + "%"

BeginSql Alias cAliasQry

	SELECT
		%Exp:cSelect%
	FROM
		%Exp:cFrom%
	WHERE
		%Exp:cWhere%

EndSql

If ( cAliasQry )->( !Eof() )
	lRet := .T.

	If lPosiciona
		( cAlias )->( DBGoTo( ( cAliasQry )->RECNO ) )
	EndIf
EndIf

( cAliasQry )->( DBCloseArea() )

Return( lRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} TafClearModel
Limpa modelo da memoria.

@param oModel - Modelo


@Return Nil 

@Author Evandro dos Santos Oliveira
@Since 25/11/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TafClearModel(oModel)
	If !IsInCallStack("TafPrepInt")
		FwFormCancel( oModel )
		oModel:Destroy()
		FreeObj( oModel )
	EndIf
Return Nil 
//-------------------------------------------------------------------
/*/{Protheus.doc} TAFVAltEsocial
Funcao que verifica se altera��o do evento � permitida e dispara mensagem na tela 
@param cAlias, character, (Alias do registro posicionado)
@Return .T.

@Author Vitor Henrique Ferreira
@Since 25/08/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TAFVAltEsocial(cAlias)
Local cNmFun  	:= FunName()
Local cTitulo  	:= STR0017

Default cAlias := ''

	/*---------------------------------------------------------
	Se o registro ja foi transmitido
	---------------------------------------------------------*/
	If (cAlias)->&(cAlias + '_STATUS') == ( "4" )
		MsgAlert(xValStrEr("000749")) //"N�o � poss�vel realizar altera��es em registros j� transmitidos." 

	ElseIf	(cAlias)->&(cAlias + '_STATUS') == ( "2" ) 
		MsgAlert(xValStrEr("000727")) //"Registro n�o pode ser alterado, pois se encontra em processo da transmiss�o."

	ElseIf (cAlias)->&(cAlias + '_STATUS') == ( "6" ) 
		MsgAlert(xValStrEr("000728")) //"Registro n�o pode ser alterado. Aguardando processo de transmiss�o do evento de Exclus�o S-3000"
		
	ElseIf (cAlias)->&(cAlias + '_STATUS') == ( "7" )
		MsgAlert(xValStrEr("000772")) //"Registro n�o pode ser alterado, pois o evento de exclus�o j� se encontra na base do RET" 
		
	Else
		FWExecView(cTitulo,cNmFun,MODEL_OPERATION_UPDATE)
	Endif
Return
//-------------------------------------------------------------------
/*/{Protheus.doc} TAFVExcEsocial
Funcao que verifica se exclus�o do evento � permitida e dispara mensagem na tela 
@param cAlias, character, (Alias do registro posicionado)
@Return .T.

@Author Vitor Henrique Ferreira
@Since 07/12/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TAFVExcEsocial(cAlias)
Local cNmFun  	:= FunName()
Local cTitulo  	:= STR0017

Default cAlias := ''

If (cAlias)->(!Eof())
	/*---------------------------------------------------------
	Se o registro ja foi transmitido
	---------------------------------------------------------*/
	If (cAlias)->&(cAlias + '_STATUS') == ( "4" )
		MsgAlert(xValStrEr("001099")) //"N�o � poss�vel realizar a exclus�o de um registros j� transmitidos." 
	
	ElseIf	(cAlias)->&(cAlias + '_STATUS') == ( "2" ) 
		MsgAlert(xValStrEr("001100")) //"Registro n�o pode ser exclu�do, pois se encontra em processo da transmiss�o."
	
	ElseIf (cAlias)->&(cAlias + '_STATUS') == ( "6" ) 
		MsgAlert(xValStrEr("001101")) //"Registro n�o pode ser exclu�do. Aguardando processo de transmiss�o do evento de Exclus�o S-3000"
		
	ElseIf (cAlias)->&(cAlias + '_STATUS') == ( "7" )
		MsgAlert(xValStrEr("001102")) //"Registro n�o pode ser exclu�do, pois o evento de exclus�o j� se encontra na base do RET" 
		
	Else
		FWExecView(cTitulo,cNmFun,MODEL_OPERATION_DELETE)
	Endif
Else
	Help(" ",1,"NVAZIO")
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} VldEvTab
Funcao que valida a inclus�o e altera��o de eventos de tabelas do e-social

nChmd ->	1 - Dicion�rio
			2 - Integra��o	
			
nOper ->	3 - Inclus�o	
		 	4 - Altera��o
		 	5 - Exclus�o

nEscopo	->	Escopo de agrupamento do Evento no TAFRotinas

@Return .T.

@Author Denis R. de Oliveira
@Since 19/12/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Function VldEvTab( cTabEvt, nIndice, cCodigo, cPerIni, cPerFin, nChmd, nOper, aIncons, cPerIniXML, nEscopo, cEsocial, lNewValid )

Local cLayout      := TAFRotinas( cTabEvt, 3, .F., nEscopo )[4]
Local cChave       := ""
Local cId          := ""
Local cStatus      := ""
Local cProtPn      := ""
Local cDataIni     := ""
Local nI           := 1
Local nTam         := Len( xFilial( cTabEvt ) + cCodigo )
Local aCpoInd      := {}
Local aAreaSX3     := {}
Local aAreaAnt     := {}
Local aAreaTab     := ( cTabEvt )->( GetArea() )
Local lRet         := .T.
Local lNt1519      := FindFunction("TafNT1519") .And. TafNT1519() //Indica se est� em vigor a NT 15/2019

Default cTabEvt    := ""
Default nIndice    := 0
Default cCodigo    := ""
Default cPerIni    := ""
Default cPerFin    := ""
Default nChmd      := 1
Default nOper      := 1
Default aIncons    := {}
Default cPerIniXML := ""
Default nEscopo    := 2
Default lNewValid  := .F.

If nChmd == 1 .and. ALTERA
	cStatus 	:= (cTabEvt)->&(cTabEvt + '_STATUS')
	cProtPn		:= (cTabEvt)->&(cTabEvt + '_PROTPN')
	cDataIni	:= (cTabEvt)->&(cTabEvt + '_DTINI' )
EndIf	

If nIndice == 13 .AND. cTabEvt == 'C1G' 

	cCodigo := Padr(cEsocial,TamSX3("C1G_ESOCIA")[1]) + cCodigo + Padr(cPerIni,TamSX3("C1G_DTINI")[1]) + Padr(cPerFin,TamSX3("C1G_DTFIN")[1]) + '1'
	nTam         := Len( xFilial( cTabEvt ) + cCodigo )

elseIf nIndice == 14 .AND. cTabEvt == 'C1G' 

	cCodigo :=  Padr(cEsocial,TamSX3("C1G_ESOCIA")[1]) + cCodigo + Padr(cPerIni,TamSX3("C1G_DTINI")[1]) //+ '1'
	nTam         := Len( xFilial( cTabEvt ) + cCodigo )

EndIf

//Incluso tratamento para controlar Data de Inicio e Data de Fim no Evento S-1050
If nIndice == 2 .AND. cTabEvt == 'C90'

	cCodigo := cCodigo + Padr(cPerIni,TamSX3("C90_DTINI")[1]) + Padr(cPerFin,TamSX3("C90_DTFIN")[1]) + '1'
	nTam := Len( xFilial( cTabEvt ) + cCodigo )

EndIf

//Verifica a exist�ncia da tabela e indice
if TafIndexInDic(cTabEvt, nIndice, .T.)

	//Posiciono na tabela do evento
	(cTabEvt)->(DBSetOrder(nIndice))

	If (cTabEvt)->( MsSeek( xFilial(cTabEvt) + cCodigo )) 		

		//Controla a opera��o (1-Inclus�o|2-Altera��o|3-Exclus�o)
		If nChmd == 1
			
			If INCLUI 
				nOper	:= 3
			ElseIf ALTERA 
				nOper	:= 4
				cId		:= M->&(cTabEvt + '_ID')
			Else
				nOper 	:= 5
			EndIf
		
		Else
			
			If nOper == 4
				aAreaAnt	:= (cTabEvt)->(GetArea())
				
				(cTabEvt)->(DBSetOrder(nIndice))
				If lTAFCodRub .And.  cTabEvt == "C8R" 
					cRubTab := ""
					If Len(cCodigo) > TamSx3("C8R_CODRUB")[1]
						cRubTab := SubStr(cCodigo,(TamSx3("C8R_CODRUB")[1])+1,TamSx3("C8R_IDTBRU")[1])
					EndIf
				
					IF nChmd != 2
						C8R->(dbGoTo(TAFCodRub(SubStr(cCodigo,1,TamSx3("C8R_CODRUB")[1]), /*DTINI*/SubStr(cPerIniXML,3,4)+SubStr(cPerIniXML,1,2),;
						/*DTFIN*/"", /*ATIVO*/"1",/*TABELA*/cRubTab)))
					Else
						(cTabEvt)->( MsSeek( xFilial(cTabEvt) + cCodigo + cPerIniXML + "1"))
					EndIf
					cId			:= (cTabEvt)->&(cTabEvt + '_ID')
					cStatus 	:= (cTabEvt)->&(cTabEvt + '_STATUS')
					cProtPn	:= (cTabEvt)->&(cTabEvt + '_PROTPN')
					cDataIni	:= (cTabEvt)->&(cTabEvt + '_DTINI')
				ElseIf (cTabEvt)->( MsSeek( xFilial(cTabEvt) + cCodigo + cPerIniXML + "1"))
					cId			:= (cTabEvt)->&(cTabEvt + '_ID')
					cStatus 	:= (cTabEvt)->&(cTabEvt + '_STATUS')
					cProtPn	:= (cTabEvt)->&(cTabEvt + '_PROTPN')
					cDataIni	:= (cTabEvt)->&(cTabEvt + '_DTINI')
				EndIf
				RestArea( aAreaAnt )	
			EndIf
		
		EndIf	
			
		//Pego o t�tulo dos campos da chave, exceto _DTINI e _DTFIN
		aAreaSX3	:=	SX3->( GetArea() )
			
		dbSelectArea('SX3')
		SX3->( dbSetOrder(2) )
		
		aCpoInd	:= StrTokArr( (cTabEvt)->(IndexKey()), "+" )
		For nI := 1 To Len(aCpoInd)
			If !Alltrim(Substr(aCpoInd[nI],5)) $ "FILIAL|ID|VERSAO|DTINI|DTFIN|ATIVO|"
				SX3->( dbSeek( aCpoInd[nI] ) )
				cChave := cChave + Alltrim(X3Titulo()) + Iif(Len(aCpoInd)>1, " e ", "")
			EndIf
		Next
		
		RestArea( aAreaSX3 )
		
		//Valida��o das regras dos eventos de tabela
		If nOper <> 5 

			While !(cTabEvt)->(Eof()) .AND. xFilial(cTabEvt) + cCodigo == Substr((cTabEvt)->&(IndexKey()),1,nTam)
				If cTabEvt == "C1G" .And. lNt1519 .And. C1G->C1G_STATUS == "4" .And. C1G->C1G_LOGOPE == "A" // Caso o processo administrativo/judicial tenha sido incluido de forma automatica por outro evento

					RecLock(cTabEvt,.F.)
					C1G->C1G_ATIVO := "2"
					C1G->(MsUnlock())

				ElseIf (cTabEvt)->&(cTabEvt + '_ATIVO') == "1"

					If cTabEvt == "C1E" .And. (cTabEvt)->&(cTabEvt + '_STATUS') $ " |0|1|3" //Desconsidera a regra para C1E/S-1000 enquanto n�o for transmitido ou rejeitado pelo RET.
						Exit
					EndIf

					If nOper == 3 .AND. (cTabEvt)->&(cTabEvt + '_STATUS') == "2" 
					
						If nChmd == 1 
							Help( ,,"VLDDATA01",, STR0028 + cLayout + STR0029 + cChave + STR0030 , 1, 0  )//"N�o � poss�vel a inclus�o do registro. Existe um evento " + cLayout + "na base, com o(a) mesmo(a) " + cChave + " transmitido e aguardando retorno do Governo."
							lRet	:= .F.	
						Else
							Aadd( aIncons, STR0028 + cLayout + STR0029 + cChave + STR0030) //"N�o � poss�vel a inclus�o do registro. Existe um evento " + cLayout + "na base, com o(a) mesmo(a) " + cChave + " transmitido e aguardando retorno do Governo."
						EndIf
					
					ElseIf nOper == 3 .AND. (cTabEvt)->&(cTabEvt + '_STATUS') == "3" 
					
						If nChmd == 1 	
							Help( ,,"VLDDATA02",, STR0028 + cLayout + STR0029 + cChave + STR0031 , 1, 0  ) //"N�o � poss�vel a inclus�o do registro. Existe um evento " + cLayout + "na base, com o(a) mesmo(a) " + cChave + " transmitido e n�o autorizado ( retornado com erro )."
							lRet	:= .F.	
						Else
							Aadd( aIncons, STR0028 + cLayout + STR0029 + cChave + STR0031) //"N�o � poss�vel a inclus�o do registro. Existe um evento " + cLayout + "na base, com o(a) mesmo(a) " + cChave + " transmitido e n�o autorizado ( retornado com erro )." 
						EndIf
					
					ElseIf nOper == 3 .AND. (cTabEvt)->&(cTabEvt + '_STATUS') <> "4"  
					
						If nChmd == 1 
							Help( ,,"VLDDATA03",, STR0028 + cLayout + STR0029 + cChave + STR0032 , 1, 0  )//"N�o � poss�vel a inclus�o do registro. Existe um evento " + cLayout + " na base, com o(a) mesmo(a) " + cChave + " n�o transmitido com sucesso ao RET." 
							lRet	:= .F.			 
						Else
							Aadd( aIncons, STR0028 + cLayout + STR0029 + cChave + STR0032) //"N�o � poss�vel a inclus�o do registro. Existe um evento " + cLayout + " na base, com o(a) mesmo(a) " + cChave + " n�o transmitido com sucesso ao RET." 
						EndIf
				
					ElseIf ( nOper == 3 .OR. nOper == 4 ) .AND. cID <> (cTabEvt)->&(cTabEvt + '_ID') .AND. xPerToData(cPerIni) == xPerToData((cTabEvt)->&(cTabEvt + '_DTINI'))  
					
						If nChmd == 1 
							Help( ,,"VLDDATA04",, STR0034 + cLayout + STR0029 + cChave + STR0033 , 1, 0 ) //"N�o � poss�vel a inclus�o/altera��o do registro. Existe um evento " + cLayout + " na base, com o(a) mesmo(a) " + cChave + " com a mesma data de in�cio de validade {iniValid}."
							lRet	:= .F.
						Else
							Aadd( aIncons, STR0034 + cLayout + STR0029 + cChave + STR0033) //"N�o � poss�vel a inclus�o/altera��o do registro. Existe um evento " + cLayout + " na base, com o(a) mesmo(a) " + cChave + " com a mesma data de in�cio de validade {iniValid}."
						EndIf
			
					ElseIf ( nOper == 3 .OR. nOper == 4 ) .AND. cID <> (cTabEvt)->&(cTabEvt + '_ID') .AND. xPerToData(cPerIni) <= xPerToData((cTabEvt)->&(cTabEvt + '_DTFIN')) .AND. xPerToData(cPerIni) >= xPerToData((cTabEvt)->&(cTabEvt + '_DTINI'))
						
						If nChmd == 1
							Help( ,,"VLDDATA05",, STR0034 + cLayout + STR0029 + cChave + STR0041 , 1, 0 ) //"N�o � poss�vel a inclus�o/altera��o do registro. Existe um evento " + cLayout + " na base, com o(a) mesmo(a) " + cChave + " com a data final igual ou superior a data de in�cio a desse registro"
							lRet	:= .F.
						Else
							Aadd( aIncons, STR0034 + cLayout + STR0029 + cChave + STR0035) //"N�o � poss�vel a inclus�o/altera��o do registro. Existe um evento " + cLayout + " na base, com o(a) mesmo(a) " + cChave + " com a data final igual ou superior a data de in�cio do evento integrado."
						EndIf
					
					ElseIf nOper == 3 .AND. !Empty(cPerFin) .AND. xPerToData(cPerFin) <= xPerToData((cTabEvt)->&(cTabEvt + '_DTINI')) 
				
						If nChmd == 1 
							Help( ,,"VLDDATA06",, STR0028 + cLayout + STR0029 + cChave + STR0040 , 1, 0  ) //"N�o � poss�vel a inclus�o do registro. Existe um evento " + cLayout + "na base, com o(a) mesmo(a) " + cChave + " com a data in�cio igual ou superior a data final a desse registro"
							lRet	:= .F.
						Else
							Aadd( aIncons, STR0028 + cLayout + STR0029 + cChave + STR0036) //"N�o � poss�vel a inclus�o do registro. Existe um evento " + cLayout + "na base, com o(a) mesmo(a) " + cChave + " com a data in�cio igual ou superior a data final do evento integrado."
						EndIf
					
					ElseIf nOper == 4 .AND. cID <> (cTabEvt)->&(cTabEvt + '_ID') .AND. !Empty(cPerFin) .and. xPerToData(cPerIni) < xPerToData((cTabEvt)->&(cTabEvt + '_DTINI')) .AND. xPerToData(cPerFin) >= xPerToData((cTabEvt)->&(cTabEvt + '_DTINI')) 
				
						If nChmd == 1 
							Help( ,,"VLDDATA07",, STR0037 + cLayout + STR0029 + cChave + STR0038 , 1, 0  ) //"N�o � poss�vel a altera��o do registro. Existe um evento " + cLayout + " na base, com o(a) mesmo(a) " + cChave + " com a data in�cio igual ou inferior a data final a desse registro.
							lRet	:= .F.
						Else
							Aadd( aIncons, STR0037 + cLayout + STR0029 + cChave + STR0039) //"N�o � poss�vel a altera��o do registro. Existe um evento " + cLayout + " na base, com o(a) mesmo(a) " + cChave + " com a data in�cio igual ou inferior a data final do evento integrado."
						EndIf
						
					ElseIf nOper == 3 .AND. !Empty(cPerFin) .and. xPerToData(cPerIni) < xPerToData((cTabEvt)->&(cTabEvt + '_DTINI')) .AND. xPerToData(cPerFin) >= xPerToData((cTabEvt)->&(cTabEvt + '_DTINI')) 
				
						If nChmd == 1 
							Help( ,,"VLDDATA07",, STR0028 + cLayout + STR0029 + cChave + STR0044 , 1, 0  ) //"N�o � poss�vel a inclus�o do registro. Existe um evento " + cLayout + " na base, com o(a) mesmo(a) " + cChave + " com a data in�cio igual ou superior que a data in�cio desse registro."
							lRet	:= .F.
						Else
							Aadd( aIncons, STR0028 + cLayout + STR0029 + cChave + STR0045 ) //"N�o � poss�vel a inclus�o do registro. Existe um evento " + cLayout + " na base, com o(a) mesmo(a) " + cChave + " com a data in�cio igual ou superior que a data in�cio do evento integrado."
						EndIf
							
					EndIf
				
				EndIf	
				
				(cTabEvt)->(DbSkip())
			EndDo
				
		EndIf

	EndIf

Endif

RestArea( aAreaTab )

//Regra do bloqueio de altera��o de data de in�cio dos eventos de tabela
If !lNewValid
	If nOper == 4 .AND. (cStatus == "4" .OR. !Empty(cProtPn)) .AND. xPerToData(cPerIni) <> xPerToData(cDataIni)

		If nChmd == 1
			Help( ,,"VLDDATA08",, STR0042 + cLayout + STR0043 , 1, 0 ) // "N�o � poss�vel a altera��o da data de in�cio de validade {iniValid} do evento " # ". Para uma nova data de in�cio de validade, favor inclua um novo registro."
			lRet	:= .F.
		Else
			Aadd( aIncons, STR0042 + cLayout + STR0043 )
		EndIf	

	EndIf
EndIf

Return Iif ( nChmd == 1, lRet, aIncons )

//-------------------------------------------------------------------
/*/{Protheus.doc} TafAtDtVld
Funcao que verifica se houve altera��o na dataFim do evento atual com o evento anterior (alterado, ativo = 2)  

@param cAlias	- Alias que a query er� trabalhar
@param cId		- Id do registro posicionado  
@param dNewDtIni,dNewDtFin - dataIni e dataFim do registro posicionado
@Return True/False

@Author Henrique Pereira
@Since 20/12/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TafAtDtVld(cAlias, cId, dNewDtIni, dNewDtFin, cVerAnt, lVldDtIni)

Local lRet 		:= .F.
Local cAliasQry	:= GetNextAlias()
Local cSelect	:= ""
Local cFrom		:= ""
Local cWhere	:= ""
Local cOrderBy	:= ""

Default cAlias	:= ""
Default cId		:= ""
Default cVerAnt	:= ""
Default lVldDtIni	:= .F.

If !Empty(cAlias) .AND. !Empty(cId) 
	
	cSelect	:= cAlias + "_DTINI, " + cAlias + "_DTFIN"
	cFrom		:= RetSqlName( cAlias ) + " " + cAlias
	cWhere 	:= cAlias + "." + cAlias + "_FILIAL = '" + xFilial( cAlias ) + "' "
	cWhere		+= " AND "+ cAlias + "." + cAlias + "_ID  = '" + cId + "'
	If !Empty(cVerAnt)
		cWhere		+= " AND "+ cAlias + "." + cAlias + "_VERSAO  = '" + cVerAnt + "'
	EndIf
	cWhere		+= " AND "+ cAlias + "." + cAlias + "_ATIVO  = '2'
	cWhere 	+= " AND "+ cAlias + ".D_E_L_E_T_ = ' ' "
	cOrderBy	+= cAlias + ".R_E_C_N_O_ "
	
	cSelect	:= "%" + cSelect 	+ "%"
	cFrom		:= "%" + cFrom   	+ "%"
	cWhere		:= "%" + cWhere  	+ "%"
	cOrderBy	:= "%" + cOrderBy	+ "%"
	
	BeginSql Alias cAliasQry
	
		SELECT
			%Exp:cSelect%
		FROM
			%Exp:cFrom%
		WHERE
			%Exp:cWhere%
		ORDER BY 
			%Exp:cOrderBy%
		DESC
	EndSql
	
	If (cAliasQry)->(!EOF())	
		If lVldDtIni
			If	(Alltrim(dNewDtIni) <> Alltrim((cAliasQry)->&(cAlias + "_DTINI"))) .Or. ;
				(Alltrim(dNewDtFin) <> Alltrim((cAliasQry)->&(cAlias + "_DTFIN"))) 	
					lRet := .T.
			EndIf			
		Else				
			If	Alltrim(dNewDtFin) <> Alltrim((cAliasQry)->&(cAlias + "_DTFIN")) 	
					lRet := .T.
			EndIf		
		EndIf
	EndIf
	
	(cAliasQry)->(DbCloseArea())
EndIf
	
Return lRet 

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFGFilMatriz
Verifica qual � a Filial Matriz da Empresa/Grupo de Empresa

@return aFilMatriz [1] - Codigo da Empresa
				   [2] - Filial da Empresa
				   [3] - CNPJ 
				   [4] - IE
				   [5] - Status da Filial no Complemento de Empresa

@Author Evandro dos Santos Oliveira Teixeira
@Since 01/03/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TAFGFilMatriz()

	Local cCnpj			:= Substr(SM0->M0_CGC,1,8) // Cnpj base da filial posicionada
    Local aFilMatriz 	:= {}
	Local aAreaC1E		:= C1E->(getArea())
	Local aFilials		:= {}
    Local nX 			:= 0
    Local nTamFil 		:= 0
    Local nSizeFil 		:= 0
	Local lForceSM0 	:= !IsInCallStack("TAFPROC4")
	Local aSM0			:= FWLoadSM0(lForceSM0)
    
	dbSelectArea("C1E")
    C1E->(dbSetOrder(3))

    nSizeFil := FWSizeFilial()

	For nX := 1 To Len(aSM0)
		If cCnpj == Substr(aSM0[nX][18],1,8)
			aAdd(aFilials;
				,{AllTrim(aSM0[nX][01]); 			//Codigo Grupo de Empresa
				,AllTrim(aSM0[nX][02]);  			//Codigo Filial	
				,AllTrim(aSM0[nX][18]);  			//CNPJ
				,retIESM0(AllTrim(aSM0[nX][22]))};	//IE		
				)
		EndIf 
	Next nX

    For nX := 1 To Len(aFilials)
         If MsSeek(xFilial("C1E")+PadR(aFilials[nX][2],nSizeFil)+"1")
            If C1E->C1E_MATRIZ 

                aFilMatriz := {  aFilials[nX][1];
								,aFilials[nX][2];
								,aFilials[nX][3];
								,aFilials[nX][4];
								,C1E->C1E_STATUS;
								,C1E->C1E_ID}

               nX := nX + Len(aFilials) // S� pode ter 1 Filial Marcada como Matriz
			   							// por isso posso sair do la�o quando ha a ocorr�ncia.
            EndIf
         Endif
    Next nX

    RestArea(aAreaC1E)
	
Return (aFilMatriz)


//-------------------------------------------------------------------
/*/{Protheus.doc} retIESM0
Normaliza a Inscri��o Estadual Retornada pela fun��o FWLoadSM0

@param cIEFWSM0 - Posi��o 22 de retorno da Fun��o FWLoadSM0
@return cIE 	- Numero da Inscri��o Estadual

@Author Evandro dos Santos Oliveira Teixeira
@Since 01/03/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function retIESM0(cIEFWSM0)

	Local cIE := ""
	Local aAux := {}

	aAux := StrTokArr(cIEFWSM0,"_")

	If Len(aAux) > 1
		cIE := aAux[2]
	EndIf 

Return cIE


//-------------------------------------------------------------------
/*/{Protheus.doc} TAFTransFil
Verifica se deve ser permitido a transmiss�o de eventos pelas filiais,
o default � somente pela filial Matriz.

Permite a manipula��o dos status v�lidos para a transmissao o default
� somente o 0 - Transmitido.

@return aFilMatriz [1] - Codigo da Empresa
				   [2] - Filial da Empresa
				   [3] - CNPJ 
				   [4] - IE

@Author Evandro dos Santos Oliveira Teixeira
@Since 01/03/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TAFTransFil(lJob,cStatus)

	Local lTransFil := SuperGetMv('MV_TAFTFIL',.F.,.F.) //Permite que seja realizado a tranmiss�o pela filial
	Local nX 		:= 0
	Local aParamTrs	:= {}
	Local cStsParam	:= ""
	Local lVirgula	:= .F. 

	Default cStatus := ""  
	Default lJob	:= .F.

	If lJob

		lTransFil := .T. //Quando executado via schedule a transmiss�o � sempre realiza pela filial
						//Na fun��o TAFRIdEnt foi inserido um tratamento para que o cnpj utilizado
						//na recupera��o da entidade seja da matriz.

		cStsParam := SuperGetMv('MV_TAFSTRS',.F.,"'0'") 
		
		aParamTrs := StrTokArr(cStsParam,",")
		cStatus := ""

		For nX := 1 To Len(aParamTrs)

			Iif( lVirgula, cStatus += ",", )
			cStatus += aParamTrs[nX]
			lVirgula := .T. 	

		Next nX

	EndIf 

Return lTransFil 

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFMonSts
Retorna Status para monitoramento via schedule.

@Author Leonardo Kichitaro
@Since 10/10/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TAFMonSts(lJob)

	Local nX 		
	Local aParamTrs	
	Local cStsParam	
	Local cStatus	
	Local lVirgula	

	Default lJob	:= .F.

	nX 			:= 0
	aParamTrs 	:= {}
	cStsParam 	:= ""
	cStatus		:= ""
	lVirgula	:= .F. 

	If lJob

		cStsParam := SuperGetMv('MV_TAFSTMN',.F.,"'2'") //Par�metro com status de monitoramento via Schedule
		
		aParamTrs := StrTokArr(cStsParam,",")
		cStatus := ""

		For nX := 1 To Len(aParamTrs)

			Iif( lVirgula, cStatus += ",", )
			cStatus += aParamTrs[nX]
			lVirgula := .T. 	

		Next nX

	EndIf 

Return cStatus

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFSetFilter
N�o Apagar, esta fun��o est� sendo utilizada apenas para verifica��o
da existencia da rotina TAFBrwSetFilter
/*/
//-------------------------------------------------------------------
Function TAFSetFilter(cAlias,cRotina,cNomeEve)
Return TAFBrwSetFilter(cAlias,cRotina,cNomeEve)  

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFBrwSetFilter
Define o Filtro do objeto FwMBrowse

@param cAlias - Alias do Browse
@param cRotina - Nome da Rotina

@Author Evandro dos Santos Oliveira Teixeira
@Since 01/03/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TAFBrwSetFilter(cAlias,cRotina,cNomeEve)    

	Local cFiltro := ""
	Default cNomeEve := "" 


	If cRotina == "TAFA421"

		cFiltro := "(C9V_ATIVO == '1'"
		cFiltro += " .And. C9V_NOMEVE <> 'S2399'"
		cFiltro += " .And. C9V_NOMEVE <> 'TAUTO')"

	ElseIf cNomeEve $ "S-1200|S-1202"

		cNomeEve := StrTran(cNomeEve,"-","") 

		cFiltro := cAlias + "_ATIVO == '1'"
		cFiltro += " .And. " + cAlias + "_NOMEVE == '" + cNomeEve +  "'"

	Else

		cFiltro := cAlias + "_ATIVO == '1'"

	EndIf 
	
	cFiltro += " .Or. (" + cAlias + "_ATIVO == '2'" 
	
	If cRotina == "TAFA421" 
		cFiltro += " .And. C9V_NOMEVE <> 'S2399'"
		cFiltro += " .And. C9V_NOMEVE <> 'TAUTO'
	EndIf  

	If cAlias == 'C9V'	.And. TAFColumnPos( "C9V_IDTRAN" ) 
		cFiltro += " .And. (" + cAlias + "_EVENTO = 'E' .Or. (" + cAlias + "_IDTRAN <> '' .And. !('|' $ " + cAlias + "_IDTRAN))))"  
	Else
		cFiltro += " .And. " + cAlias + "_EVENTO = 'E')" 
	EndIf


Return cFiltro

//-------------------------------------------------------------------
/*/{Protheus.doc} TafAjustRecibo
Altera nome e Descri��o do campo _PROTUL

@param oStruct - Estrutura que contem o campo _PROTUL
@param cAlias - Alias do Cadastro

@Author Evandro dos Santos Oliveira Teixeira
@Since 20/03/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TafAjustRecibo(oStruct,cAlias)
	
	oStruct:SetProperty( cAlias+'_PROTUL', MVC_VIEW_TITULO ,STR0046) //"N� Recibo"
	oStruct:SetProperty( cAlias+'_PROTUL', MVC_VIEW_DESCR  ,STR0047) //"N�mero do Recibo retornado pelo RET."

Return Nil 

//-------------------------------------------------------------------
/*/{Protheus.doc} TafNmFolder
Altera Descri��o e Titulo das pastas MVC

@param cToken - Identifica��o do Titulo
@param nTipo - Tipo do Titulo (1=Titulo do Folder, 0=Titulo da Pasta)

@Author Evandro dos Santos Oliveira Teixeira
@Since 20/03/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TafNmFolder(cToken,nTipo)

	Local cTitulo := ""
	Default nTipo := 0

	If cToken == 'recibo'
		If nTipo == 0
			cTitulo := STR0048 //"N�mero do Recibo"
		Else 
			cTitulo := STR0049 //"Recibo da �ltima Transmiss�o"  
		EndIf 
	EndIf

Return cTitulo



//-------------------------------------------------------------------
/*/{Protheus.doc} TafLayESoc
Retorna se a vers�o do layout do eSocial � a vers�o necess�ria do par�metro

@param cVersao - Identifica��o do Titulo
@param nTipo - Tipo do Titulo (1=Titulo do Folder, 0=Titulo da Pasta)

@Author Evandro dos Santos Oliveira Teixeira
@Since 20/03/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TafLayESoc(cVerCompara)
Return (SuperGetMv('MV_TAFVLES',.F.,"02_04_02") >= cVerCompara)


//-------------------------------------------------------------------
/*/{Protheus.doc} TafDicInDb
Verifica se o Ambiente permite o uso de tabela temporaria no Banco

@Author Evandro dos Santos Oliveira Teixeira
@Since 11/04/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TafDicInDb()

	Local lTempTable := .F.

	If Val(GetVersao(.F.)) > 11 .And. AllTrim(GetRpoRelease()) >= "12.1.005"
		lTempTable := .T.
	EndIf

Return(lTempTable)

//-------------------------------------------------------------------
/*/{Protheus.doc} TafDelTempTable
Apaga tabela tempor�rio criada no Banco

@param cRealName - Nome da tabela no Banco de dados
@param cErroSQL - Mensagem de Erro (refer�ncia)

@Author Evandro dos Santos Oliveira Teixeira
@Since 11/04/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
FuncTion TafDelTempTable(cRealName,cErroSQL)

	Local cQuery := ""
	Local lRet := .T. 

	cQuery := "DELETE FROM " + cRealName
	If TCSQLExec (cQuery) < 0
		cErroSQL := TCSQLError()
		lRet := .F. 
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFConOut
Printa mensagens no console Informando a Data e Hora

@param cMensagem - Mensagem a ser impressa

@Author Evandro dos Santos Oliveira Teixeira
@Since 17/04/2018
@Version 1.0
/*/
//-------------------------------------------------------------------
Function TAFConOut(cMensagem,nTipoMsg)

	Local cDataBase := ""
	Local cTime := ""
	Local cTipoMsg := ""
	Local cValue := ""

	If __lTAFConOut == Nil
		__lTAFConOut := ( GetPvProfString("General","TafConOut","0",getAdv97()) == "1" )
	EndIf

	If __lTAFConOut

		Default nTipoMsg := 1 

		If nTipoMsg == 2 
			cTipoMsg := "TAFWARN"
		Else
			cTipoMsg := "TAFINFO"
		EndIf

		cDataBase := DTOC(Date())
		cTime := Time()
		
		ConOut("[" + cTipoMsg + "][" + ProcName(1) + "|"+ cValToChar(ProcLine(1)) + "] " + cMensagem + " Date: " + cDataBase + " Time: " + cTime)

	EndIf

Return Nil 

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFAfasCM6
Retorna se pode incluir afastamento na data informada
@author  Victor A. Barbosa
@since   14/05/2018
@version 1
/*/
//-------------------------------------------------------------------
Static Function TAFAfasCM6( cIDFunc, dData, dDataIniAf, nTipo )

Local aArea			:= GetArea()
Local cNextAlias	:= GetNextAlias()
Local lRet			:= .T.
Local dDataAfas		:= Nil
Local dDataFim		:= Nil

// In�cio Afastamento
If nTipo == 1
	
	If dData < dDataIniAf
		Help( ,,"Aten��o",,"Data informada � menor que a data inicial informada no evento S-2200", 1, 0 )
		lRet := .F.
	Else
		
		If Select( cNextAlias ) > 0
			(cNextAlias)->( DbCloseArea() )
		EndIf

		BeginSQL Alias cNextAlias
			SELECT CM6_DTAFAS, CM6_DTFAFA, CM6_EVENTO, CM6_STATUS FROM %table:CM6% CM6
			WHERE CM6.CM6_FILIAL = %xFilial:CM6%
			AND	CM6.CM6_FUNC = %exp:cIDFunc%
			AND CM6.%notdel%
			ORDER BY CM6_DTAFAS
		EndSQL

		(cNextAlias)->( dbGoTop() )

		While (cNextAlias)->( !Eof() )
				
			dDataAfas	:= STOD( (cNextAlias)->CM6_DTAFAS )
			dDataFim	:= STOD( (cNextAlias)->CM6_DTFAFA )

			If dData <= dDataAfas
				lRet := .T.
				Exit
			ElseIf dData >= dDataAfas .And. dData <= dDataFim
				Help( ,,"Aten��o",,"J� existe um afastamento no per�odo informado.", 1, 0 )
				lRet := .F.
				Exit
			EndIf 

			(cNextAlias)->( DbSkip() )
			
		EndDo

		(cNextAlias)->( DbCloseArea() )

	EndIf

//T�rmino Afastamento
Else 
	
	// Se a data de in�cio estiver vazia, � sinal que est� informando o encerramento que foi iniciado no s-2200
	If !Empty( DTOS( dData ) )
		If Empty( M->CM6_DTAFAS )
			If dData < dDataIniAf
				Help( ,,"Aten��o",,"Data final informada � menor que a data inicial informada no S-2200", 1, 0 )
				lRet := .F.
			ElseIf TAFMaxAfast( cIDFunc, DTOS( dData ) )
				Help( ,,"Aten��o",,"Existe afastamento com data de in�cio menor que a data de t�rmino informada.", 1, 0 )
				lRet := .F.
			EndIf
		ElseIf !Empty( M->CM6_DTAFAS ) .And. dData < M->CM6_DTAFAS
			Help( ,,"Aten��o",,"Data final informada � menor que a data inicial.", 1, 0 )
			lRet := .F.
		EndIf
	EndIf
	
EndIf

RestArea( aArea )

Return( lRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFMaxAfast
Retorna se existe algum afastamento com data de in�cio inferior aquela data de t�rmino.
@author  Victor A. Barbosa
@since   16/05/2018
@version 1
/*/
//-------------------------------------------------------------------
Static Function TAFMaxAfast( cIDFunc, cParamData )

Local aArea 		:= GetArea()
Local cAliasAfast	:= "AliasAfast"
Local lRet			:= .F.

If Select( cAliasAfast ) > 0
	(cAliasAfast)->( DbCloseArea() )
EndIf

BeginSQL Alias cAliasAfast
	SELECT R_E_C_N_O_ FROM %table:CM6% CM6
	WHERE CM6.CM6_FILIAL = %xFilial:CM6%
	AND	CM6.CM6_FUNC 	= %exp:cIDFunc%
	AND CM6.CM6_DTAFAS < %exp:cParamData%
	AND CM6_DTFAFA	= ''
	AND CM6_VERANT 	= ''
	AND CM6.%notdel%
	ORDER BY CM6_DTAFAS
EndSQL

(cAliasAfast)->( DbGoTop() )

If (cAliasAfast)->( !Eof() )
	lRet := .T.
EndIf

(cAliasAfast)->( DbCloseArea() )

RestArea( aArea )

Return( lRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} TafRetStack
Fun��o para troubleshooting. Retorna a pilha de execu��o.
@author  Evandro dos Santos Oliveira
@since   20/05/2018
@version 1
/*/
//-------------------------------------------------------------------
Function TafRetStack()

	Local nProc := 1

	While !Empty(AllTrim(ProcName(nProc)))
		
		ConOut("Stack " + cValToChar(nProc))
		ConOut("Fun��o: " + ProcName(nProc))
		ConOut("Linha: " + cValToChar(ProcLine(nProc-1)))
		ConOut("#")

		nProc++
	EndDo 

	ConOut("")
	ConOut("Empresa: " + SM0->M0_CODIGO + " Filial: " + SM0->M0_CODFIL + " CGC: " + SM0->M0_CGC)
	ConOut("--FIM")
	ConOut("")
	
Return Nil 

//-------------------------------------------------------------------
/*/{Protheus.doc} isTAFInCloud
Indica se o ambiente est� sendo executado em ambiente Cloud.
(Amazon+Docker+Kubernets)

@author  Evandro dos Santos Oliveira
@since   25/05/2018
@version 1
/*/
//-------------------------------------------------------------------
Function isTAFInCloud()

	If __lTAFinCloud == Nil
		__lTAFinCloud := (GetPvProfString(GetEnvServer(),"TafInCloud","0",getAdv97()) == "1")
	EndIf

Return __lTAFinCloud

//-------------------------------------------------------------------
/*/{Protheus.doc} TafGetUrlTSS

@author  Evandro dos Santos Oliveira
@since   13/06/2018
@version 1
/*/
//-------------------------------------------------------------------
Function TafGetUrlTSS(cCodEmp)

	Local cUrl := ""
	Default cCodEmp := FWCodEmp()

	cUrl := PadR(SuperGetMv("MV_TAFSURL",.F.,"",cCodEmp),250)

Return cUrl 

//-------------------------------------------------------------------
/*/{Protheus.doc} TafTableTag

@author  Eduardo Sukeda
@since   28/01/2019
@version 1

CRQ - horario
C9J - infoMV
T3H - procJudTrab
T3I - dmDev
T3J - ideDmDev
CMK - detVerbas
T15 - detOper
T16 - detPlano

/*/
//-------------------------------------------------------------------
Function TafTableTag(cAlias, cErrorLog)

Local cTag 		:= ""
Local cTabTag 	:= ""
Local cMsg 		:= "Verifica��o de chave duplicada, Erro de chave duplicada, favor verificar as informa��es que est�o sendo integradas. "

Default cErrorLog := ""

If !Empty(cAlias)

	If TAFAlsInDic(cAlias)
	
		If cAlias == "CRQ"	
			cTag := "horario"
		ElseIf cAlias $ "C9M|C9R|T6E|T6K"
			cTag := "itensRemun"
		ElseIf cAlias $ "T05|T5S|CMK"
			cTag := "detVerbas"
		ElseIf cAlias $ "C9J"
			cTag := "infoMV"
		ElseIf cAlias $ "T3H|CRN"
			cTag := "procJudTrab"	
		ElseIf cAlias $ "T3I"
			cTag := "verbasResc"
		ElseIf cAlias $ "T3J|T14"
			cTag := "dmDev"
		ElseIf cAlias $ "T15|T6Y"
			cTag := "detOper"
		ElseIf cAlias == "T6W"
			cTag := "remunOutrEmpr"
		ElseIf cAlias == "C9N"
			cTag := "ideADC"
		ElseIf cAlias == "C9O"
			cTag := "idePeriodo"
		ElseIf cAlias == "C9P"
			cTag := "ideEstabLot"
		ElseIf cAlias $ "LE2|T6Q"
			cTag := "retPgtoTot"
		ElseIf cAlias $ "LE4|T6R"
			cTag := "infoPgtoParc"
		ElseIf cAlias $ "T5V"
			cTag := "detPgtoAnt"
		ElseIf cAlias $ "T5Y"
			cTag := "detRubrFer"
		ElseIf cAlias $ "T5Z|LE3"
			cTag := "penAlim"
		EndIf

		cTabTag := cMsg + "Tabela: " + cAlias + " - " +  "Grupo da Tag: " + cTag
	Else
		cTabTag := "Tabela inexistente no dicion�rio de dados."
	EndIf

Else
	cTabTag := "Houve um erro que impediu a integra��o deste registro. Entre em contato com o suporte TOTVS e informe o erro abaixo: " + CRLF
	cTabTag += cErrorLog
EndIf

Return(cTabTag)

/*/{Protheus.doc} GetAlsDup
Rotina para encontrar o Alias da tabela no qual 
ocorreu o error.log em virtude de chave duplicada
@type  Static Function
@author santos.diego
@since 29-01-2019
@version 1.0
@param param, param_type, param_descr
@return return, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function GetAlsDup( cDescError )

Local cRet := ""
Local nPos := At( "_UNQ", cDescError)

If nPos > 0
	cRet := Upper(SubStr( cDescError, (nPos - 6), 3 ) )
Else
	nPos := At( "_unq", cDescError)
	If nPos > 0
		cRet := Upper(SubStr( cDescError, (nPos - 6) , 3 ) )
	EndIf
EndIf

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFLayTables
Retorna os eventos com suas respectivas tabelas de acordo com a
estrura do Model.

@param - nEscopo - Escopo da Obriga��o conforme defini�ao no 
TAFRotinas:
1-Fiscal
2-eSocial
3-ECF
4-Autocontidas
5-Reinf 
@param cLayoutTAF - Nome do Layout

@Return - Array com Eventos na posicao 1 e Tabelas na posicao 2.
Se o parametro Evento nao for informado a estrura do retorno 
sera um array multidimensional por que serao retornardo todos 
os eventos


@author  Evandro dos Santos Oliveira
@since   22/06/2018
@version 1
/*/
//-------------------------------------------------------------------
Function TAFLayTables(nEscopo,cLayoutTAF) 

    Local aEventos := {} 
    Local cModel := ""
    Local cLayout := ""
    Local cTable := ""
	Local cTblPai := ""
    Local oModel := Nil 
    Local nX := 0
    Local nY := 0
	Local nPosEvento := 0
    Local aTables := {}
    Local aTableModel := {}
	Local lLoadModels := .F. 

    Default nEscopo := 0 //Nao Habilitar o Todos
	Default cLayoutTAF := ""

	//Foi realizado a separacao por escopo para for�ar o analista
	//utilzar somente o que precisa por que o carregamento de modelo � demorado.
	//� feito cache por demanda de acordo com o Escopo passado.

	If  (__aTblsFiscal   == Nil .And. nEscopo == 1) .Or.;
	    (__aTblsEsocial  == Nil .And. nEscopo == 2) .Or.;
		(__aTblsECF      == Nil .And. nEscopo == 3) .Or.;
		(__aTblsAutoCont == Nil .And. nEscopo == 4) .Or.;
		(__aTblsReinf	 == Nil .And. nEscopo == 5)
	  
		lLoadModels := .T.
		aEventos := TAFRotinas(,,.T.,nEscopo)

	EndIf 

	If lLoadModels	
 
		For nY := 1 To Len(aEventos)

			cModel := aEventos[nY][1]
			cLayout := AllTrim(aEventos[nY][4])
			cTblPai := AllTrim(aEventos[nY][3])

			If !Empty(cLayout) .And. TAFAlsInDic(cTblPai)

				aTables := Array(2)
				aTables[1] := cLayout
				aTables[2] := {}

				oModel := FWLoadModel(cModel)

				For nX := 1 To Len(oModel:aAllSubModels)
				
					If Len(oModel:aAllSubModels[nX]:oFormModelStruct:aTable) > 0
					
						cTable := oModel:aAllSubModels[nX]:oFormModelStruct:aTable[1]
						aAdd(aTables[2],cTable)
						
					EndIf 
					
				Next nX 
				aAdd(aTableModel,aClone(aTables))
				aSize(aTables,0)

			EndIf
		Next nY

		aSize(aEventos,0)

		If nEscopo == 1 
			__aTblsFiscal := aClone(aTableModel)
		ElseIf nEscopo == 2
			__aTblsEsocial := aClone(aTableModel)
		ElseIf nEscopo == 3
			__aTblsECF := aClone(aTableModel)
		ElseIf nEscopo == 4
			__aTblsAutoCont := aClone(aTableModel)
		ElseIf nEscopo == 5
			__aTblsReinf := aClone(aTableModel)
		EndIf 

		FreeObj(oModel)
		oModel := Nil 
		aSize(aTableModel,0)
		DelClassIntF()
	
	EndIf 

Return getTablesLayout(nEscopo,cLayoutTAF)

//-------------------------------------------------------------------
/*/{Protheus.doc} getTablesLayout
Retorna array com eventos + tabelas de acordo com os parametros.

@author  Evandro dos Santos Oliveira
@since   23/06/2018
@version 1
/*/
//-------------------------------------------------------------------
Static Function getTablesLayout(nEscopo,cLayoutTAF)

	Local aLayoutTable := {}
	Local nPosEvento := 0

	Default nEscopo := 0
	Default cLayoutTAF := ""

	If nEscopo == 1 

		If Empty(cLayoutTAF)
			aLayoutTable := aClone(__aTblsFiscal)
		Else 
			nPosEvento := aScan(__aTblsFiscal,{|x|x[1] == cLayoutTAF})
			aLayoutTable :=  aClone(__aTblsFiscal[nPosEvento])
		EndIf 
	ElseIf nEscopo == 2

		If Empty(cLayoutTAF)
			aLayoutTable := aClone(__aTblsEsocial)
		Else
			nPosEvento := aScan(__aTblsEsocial,{|x|x[1] == cLayoutTAF})
			aLayoutTable :=  aClone(__aTblsEsocial[nPosEvento])
		EndIf 
	ElseIf nEscopo == 3

		If Empty(cLayoutTAF)
			aLayoutTable := aClone(__aTblsECF)
		Else
			nPosEvento := aScan(__aTblsECF,{|x|x[1] == cLayoutTAF})
			aLayoutTable :=  aClone(__aTblsECF[nPosEvento])
		EndIf
	ElseIf nEscopo == 4

		If Empty(cLayoutTAF)
			aLayoutTable := aClone(__aTblsAutoCont)
		Else 
			nPosEvento := aScan(__aTblsAutoCont,{|x|x[1] == cLayoutTAF})
			aLayoutTable :=  aClone(__aTblsAutoCont[nPosEvento])
		EndIf 
	ElseIf nEscopo == 5

		If Empty(cLayoutTAF)
			aLayoutTable := aClone(__aTblsReinf)
		Else 
			nPosEvento := aScan(__aTblsReinf,{|x|x[1] == cLayoutTAF})
			aLayoutTable :=  aClone(__aTblsReinf[nPosEvento])	
		EndIf 
	EndIf 

Return aLayoutTable


//-------------------------------------------------------------------
/*/{Protheus.doc} TAFSetEpoch
Altera o SET_EPOCH de campos do tipo data. 
Ou seja, informa qual o ano o sistema assumir� por padr�o quando o usu�rio digitar apenas 2 d�gitos.

@author  Leandro Dourado
@since   26/08/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function TAFSetEpoch(nEpochNew)

Default nEpochNew := 0

If nEpochNew == 0
	nEpochNew := Year(Date()) - 90 
EndIf

SET(_SET_EPOCH,nEpochNew)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFFindClass
Verifica a exist�ncia de uma classe contida no RPO. 
Por conta de problemas na lib, a fun��o FindClass est� travando o sistema e funciona apenas para a vers�o 12.1.17. Por conta disso, foi feito um desvio para as demais vers�es.

@author  Leandro Dourado
@since   15/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function TAFFindClass( cClass )
Local lRet     := .F.
Local cRelease := GetRpoRelease()

Default cClass := "FWCSSTools"

If cRelease $ "12.1.017|12.1.023|12.1.025"
	lRet := .T.
Else
	lRet := FindFunction("FindClass") .And. FindClass(cClass)
EndIf

Return lRet