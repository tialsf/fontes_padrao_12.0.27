#include "PROTHEUS.CH"
#INCLUDE "POSCSS.CH"
#INCLUDE "TAFA567.CH"

/*/{Protheus.doc} TAFA567
    Fun��o de compara��o de dados posicionados com dados comparativos
@type function
@version 
@author edvf8
@since 01/07/2020
@param cCPFPos, character, CPF do trabalhador posicionado
@param cNomePos, character, Nome do trabalhador posicionado
@param cEventoPos, character, Evento do trabalhador posicionado
@param dDtAltPos, date, Data de Altera��o do registro do trabalhador posicionado
/*/
Function TAFA567(cCPFPos,cNomePos,cEventoPos,dDtAltPos )
    
//------------------------------------------
//VARI�VEIS PARA DIMENSIONAMENTO DE 
//ACORDO COM O TAMANHO DA TELA E FORMATA��O
//------------------------------------------
Local aRes		    := GetScreenRes()	// Recupera Resolu��o atual
Local nWidth	    := aRes[1]		// Largura 
Local nHeight       := aRes[2]		// Altura 
Local nTmGrd        := 0            // Controle para montagem de grid de posicionamento de objetos 
Local nTmGrdCpo     := 0            // Controle dos tamanhos do campo
Local oTFont        := TFont():New('Arial Black',,-14,.T.)
//----------------------------------------------------------------------
//VARI�VEIS PARA CAMPOS LIBERADOS PARA EDI��O E PR�-CARREGAMENTO DE TELA
//----------------------------------------------------------------------
Local cFiltCPF      := Space(TamSx3("C9V_NOME")[1]) //STRING DE PROCURA
Local cIndCMB       := Space(1)
Local aFields       := {STR0001,STR0002,STR0003}//COLUNAS DO GRID # 'CAMPO','EVENTO POSICIONADO','EVENTO COMPARADO'
Local aMotivos      := {STR0004,STR0005,STR0006}//OP��ES DO COMBO DE FILTRO # '1=Campo','2=Evento Posicionado','3=Evento Comparado'

//========================================
//COMPONENTES DE TELA
//========================================
Local oModal 		:= NIl			//Dialog Modal
Local oPanel    	:= NIl			//Painel que receber� a area �til do Modal
Local oPanelSub 	:= NIL          //Necess�rio a cria��o para inclus�o do tercerio TGROUP.
Local oBrowse   	:= NIL          //TWBROWSE Carregar� os dados ao clicar no processamento

//--------------------------------------------------
//�REA DE CAMPOS E TITULOS DO REGISTRO POSICIONADO
//--------------------------------------------------
Local oGDPos    	:= NIL          //Agrupador de campos de registro posicionado
Local oSCPFPos  	:= NIL	        //Label de CPF
Local oGCPFPos  	:= NIL          //Campo de CPF
Local oSTrbPos  	:= NIL	        //Label Nome do trabalhador
Local oGTrbPos  	:= NIL	        //Campo Nome do Trabalhador
Local oSEvtPos  	:= NIL	        //Label Evento
Local oGEvtPos  	:= NIL	        //Campo Evento 
Local oSDtAltPos	:= NIL          //Label Data de Altera��o 
Local oGDtAltPos	:= NIL          //Campo Data de Altera��o 

//--------------------------------------
//�REA DE CAMPOS E TITULOS DO COMPARADOR
//--------------------------------------
Local oGComp    	:= NIL          //Agrupador de campos de registro comparador
Local oSCPFCmp  	:= NIL	        //Label de CPF
Local oCPFCmp   	:= NIL          //Campo de CPF
Local oSTrbCmp  	:= NIL	        //Label Nome do trabalhador
Local oGTrbCmp  	:= NIL	        //Campo Nome do Trabalhador
Local oSEvtCmp  	:= NIL	        //Label Evento
Local oGEvtCmp  	:= NIL	        //Campo Evento 
Local oSDtAlt   	:= NIL          //Label Data de Altera��o
Local oGDtAlt   	:= NIL          //Campo Data de Altera��o

//--------------------------------------
//�REA DE CAMPOS E TITULOS DE FILTRO
//--------------------------------------
Local oTGFilt		:= NIL          //Agrupador de Campos de Filtro 
Local oSCMBCOL		:= NIL          //Label do combobox
Local oSCMBFILT		:= NIL          //ComboBox com op��es de filtro
Local oSTermFilt	:= NIL          //Label do campo de busca
Local oGFilt		:= NIL          //Campo de busca

//-------------------------
//BOT�ES DE TELA
//-------------------------
Local oBtnFil		:= NIL
Local oBtnProc		:= NIL
Local oBtnClose		:= NIL

//--------------------------------------------------------
//BLOCOS DE C�DIGO QUE REPRESENTAM AS A��ES EM TELA(BOT�ES)
//--------------------------------------------------------
Local bExecute		:= {|| TAFProcCmp(cCpfPos,cEventoPos,dDtAltPos,@oBrowse) }
Local bClose    	:= {||oModal:Deactivate()}   
Local bExecFil		:= {||oBrowse:GoPosition( fPosReg(cFiltCPF,Val(cIndCMB),aBrowse) )}
Local bLDblClick	:= {||fViewDLG(aBrowse[oBrowse:nAt],aFields)}

Private cFilialCom  := ""
Private nRecnoCom   := 0
Private cCpfCom   	:= Space(TamSx3("C9V_CPF")[1])
Private cNomeCom  	:= Space(TamSx3("C9V_NOME")[1])
Private cEventoCom	:= Space(TamSx3("C9V_NOMEVE")[1])
Private dDtAltCom 	:= Space(TamSx3("C9V_DINSIS")[1])
Private cFilCs		:= ''
Private cFilialpos	:= ''
Private cIdpos		:= ''
Private cId			:= ''
Private cCpf 		:= ''
Private cNome 		:= ''
Private cEvento 	:= ''
Private dData 		:= SToD(' / / ')
Private nRecnoPos 	:= 0
Private aBrowse		:= {{"","",""}} //Vari�vel onde ir� conter os dados do grid

//-----------------------------------------------------------
//BLOCOS DE C�DIGO QUE REPRESENTAM AS A��ES NO PROCESSAMENTO
//-----------------------------------------------------------
Private cWhereFilD := ""
Private	cWhereFilP := ""
Private	cWhereCPFD := ""
Private	cWhereDtAD := ""
Private	cWhereCPFP := ""
Private	cWhereDtAP := ""
Private	cWhereRecD := ""
Private	cWhereRecP := ""
Private cWhereDelD := ""

//---------------------------------------------------------------
//PROTE��O DE DADOS PARA N�O IMPACTAR NO CARREGAMENTO DA TELA 
//---------------------------------------------------------------
Default cCpfPos     := Space(TamSx3("C9V_CPF")[1])
Default cNomePos    := Space(TamSx3("C9V_NOME")[1])
Default cEventoPos  := Space(TamSx3("C9V_NOMEVE")[1])
Default dDtAltPos   := Space(TamSx3("C9V_DINSIS")[1])

//--------------------------------------------------------
//CARREGANDO DADOS NOS CAMPOS DO REGISTRO POSICIONADO,
//SELE��O BASEADA NO ALIAS TEMPOR�RIO DA TELA DE HIST�RICO
//--------------------------------------------------------
If ValType("ID") != Nil .And. !Empty(ID)    .And. Empty(cCpfPos)

    cCpfPos     := CPF
    cNomePos    := NOME
    cEventoPos  := NOMEVE
    cFilialpos	:= FILIAL
    cIdpos		:= ID
    dDtAltPos   := fGetDtAlt(RECNO,cEventoPos)
    nRecnoPos	:= RECNO

EndIf

cCpf 		:= cCpfPos
cNome		:= cNomePos
cEvento		:= cEventoPos
cId			:= cIdpos
cFilCs		:= cFilialpos
dData		:= dDtAltPos

//-----------------
//MONTAGEM DA TELA
//-----------------
oModal  := FWDialogModal():New()
oModal:SetBackground(.F.)
oModal:SetTitle(STR0007)//"Interface Comparadora"
oModal:lFontBoldTitle:= .T.
oModal:enableFormBar(.F.)
//-------------------------
//DIMENSIONAMENTO DA DIALOG
//--------------------------
nWidth      := (nWidth/2)  * 0.5
nHeight     := (nHeight/2) * 0.6
oModal:SetFreeArea(nWidth,nHeight)
oModal:createDialog()
oTFont:Bold := .T.
//------------------------------------------------------------------
//MONTAGEM DE GRID PARA DIMENSIONAMENTO E LAYOUT DOS OBJETOS EM TELA
//------------------------------------------------------------------
nTmGrd		:= nWidth/4 //POSICIONAMENTO DOS OBJETOS DENTRO DA DIALOG
nTmGrdCpo	:= nTmGrd-(nTmGrd*0.1) //TAMANHO DO CAMPO

//------------------------------------------
//PEGANDO A �REA �TIL PARA OS COMPONENTES.
//------------------------------------------
oPanel 		:= TPanel():New(0,0,'',oModal:getPanelMain(),,.T.,.T.,,,nWidth,nHeight,.T.,.T.)

//-----------------------------------------------
//AGRUPAMENTO DE OBJETOS DE REGISTRO POSICIONADO
//-----------------------------------------------
oGDPos		:= tGroup():New(1, 1 ,45 ,nWidth-1		,STR0008  		,oPanel,CLR_RED,CLR_WHITE,.T.)	  //"Registro posicionado"
   
    oSCPFPos	:= TSay():New(10,5			    ,{|| STR0009 }			                        ,oGDPos,,,,,,.T.)//"CPF"
    oGCPFPos	:= TGet():New(20,5			    ,{|u| if(PCount()>0,cCpfPos:=u,cCpfPos)}	    ,oGDPos,nTmGrdCpo,10,'@!',,,,,,,.T.,,,,,,,.T.,,,"cCpfPos")
    oSTrbPos	:= TSay():New(10,nTmGrd-1	    ,{|| STR0010 }			                        ,oGDPos,,,,,,.T.) //"Nome" 
    oGTrbPos	:= TGet():New(20,nTmGrd-1	    ,{|u| if(PCount()>0,cNomePos:=u,cNomePos)}	    ,oGDPos,nTmGrdCpo,10,'@!',,,,,,,.T.,,,,,,,.T.,,,"cNomePos")
    oSEvtPos	:= TSay():New(10,(nTmGrd-1)*2   ,{|| STR0011 }	                                ,oGDPos,,,,,,.T.) //"Evento"
    oGEvtPos	:= TGet():New(20,(nTmGrd-1)*2   ,{|u| if(PCount()>0,cEventoPos:=u,cEventoPos)}	,oGDPos,nTmGrdCpo,10,'@!',,,,,,,.T.,,,,,,,.T.,,,"cEventoPos")		
    oSDtAltPos  := TSay():New(10,(nTmGrd-1)*3   ,{|| STR0012 }	                    ,oGDPos,,,,,,.T.) //"Data de Altera��o"
    oGDtAltPos  := TGet():New(20,(nTmGrd-1)*3   ,{|u| if(PCount()>0,dDtAltPos:=u,dDtAltPos)}	,oGDPos,nTmGrdCpo,10,'@!',,,,,,,.T.,,,,,,,.T.,,,"dDtAltPos")		
oGDPos:SetFont(oTFont)
//-----------------------------------------------
//AGRUPAMENTO DE OBJETOS DE REGISTRO COMPARADO
//-----------------------------------------------
oGComp		:= tGroup():New(50,1	,90,nWidth-1		,STR0013  		,oPanel,CLR_BLUE,CLR_WHITE,.T.)//"Registro Comparado"

    oSCPFCmp	:= TSay():New(60,5			    ,{|| STR0009 }			                        ,oGComp,,,,,,.T.)  //"CPF"
    oCPFCmp	    := TGet():New(70,5			    ,{|u| if(PCount()>0,cCpfCom:=u,cCpfCom)}	    ,oGComp,nTmGrdCpo -15,10,'@!',,,,,,,.T.,,,,,,,,,/*"TRBCOM"*/,"cCpfCom",,,,,.F.)
    oBtnLupa    := TBtnBmp2():New( 140,(nTmGrd *2)*0.81,25,25,'BMPVISUAL' ,,,,{|| fEvtComp ()  },oGComp,,,.T. ) 
    oSTrbCmp	:= TSay():New(60,nTmGrd-1	    ,{|| STR0010 }			                        ,oGComp,,,,,,.T.)//"Nome"
    oGTrbCmp	:= TGet():New(70,nTmGrd-1	    ,{|u| if(PCount()>0,cNomeCom:=u,cNomeCom)}	    ,oGComp,nTmGrdCpo,10,'@!',,,,,,,.T.,,,,,,,.T.,,,"cNomeCom")
    oSEvtCmp	:= TSay():New(60,(nTmGrd-1)*2   ,{|| STR0011 }	                                ,oGComp,,,,,,.T.)//"Evento"
    oGEvtCmp	:= TGet():New(70,(nTmGrd-1)*2   ,{|u| if(PCount()>0,cEventoCom:=u,cEventoCom)}	,oGComp,nTmGrdCpo,10,'@!',,,,,,,.T.,,,,,,,.T.,,,"cEventoCom")		
    oSDtAlt	    := TSay():New(60,(nTmGrd-1)*3   ,{|| STR0012 }	                    ,oGComp,,,,,,.T.)//"Data de Altera��o"
    oGDtAlt	    := TGet():New(70,(nTmGrd-1)*3   ,{|u| if(PCount()>0,dDtAltCom:=u,dDtAltCom)}	,oGComp,nTmGrdCpo,10,'@!',,,,,,,.T.,,,,,,,.T.,,,"dDtAlt")		
oGComp:SetFont(oTFont)
//----------------------------------------------------------------------
//MONTAGEM DE PAINEL PARA ARMAZENAR O AGRUPADOR DE ELEMENTOS DE FILTRO
//----------------------------------------------------------------------
oPanelSub 	:= TPanel():New(90,0,'',oPanel,,.T.,.T.,,,(nTmGrd-1)*3,40,.T.,.T.)

//----------------------------------------------- 
//AGRUPAMENTO DE OBJETOS DE FILTRO
//-----------------------------------------------
oTGFilt		    := TGROUP():New(00,1	,35,((nTmGrd-1)*3)-5	,STR0014,oPanelSub,,,.T.)//"Op��es de Filtro"
oTGFilt:SetFont(oTFont)    
    oSCMBCOL	:= TSay():New(10,05	    ,{|| STR0015 }			                    ,oTGFilt,,,,,,.T.)//"Coluna de Pesq."
    oSCMBFILT   :=TComboBox():New(20,05,{|u|if(PCount()>0,cIndCMB:=u,cIndCMB)}, aMotivos,nTmGrdCpo, 13,oTGFilt,,,,,,.T.,,,,,,,,,'cIndCMB')
    
    oSTermFilt	:= TSay():New(10,nTmGrd-1	 ,{|| STR0016 }			                    ,oTGFilt,,,,,,.T.)//"Termo de Busca:"
    oGFilt	    := TGet():New(20,nTmGrd-1   ,{|u| if(PCount()>0,cFiltCPF:=u,cFiltCPF)}	,oTGFilt,nTmGrdCpo,10,'@!',,,,,,,.T.,,,,,,,,,,"cFiltCPF")
    
    oBtnFil		:= tButton():New(20,(nTmGrd-1)*2,STR0017,oTGFilt,bExecFil,nTmGrdCpo,12,,,,.T.)//"&Posicionar"
    oBtnFil:SetCSS( POSCSS (GetClassName(oBtnFil), CSS_BTN_ATIVO ))
    
    oBtnProc	:= tButton():New(95,(nTmGrd-1)*3,STR0018,oPanel,bExecute,nTmGrdCpo,15,,,,.T.)//"&Processar"
    oBtnProc:SetCSS( POSCSS (GetClassName(oBtnProc),  CSS_BTN_FOCAL )) 
    oBtnClose	:= tButton():New(110,(nTmGrd-1)*3,STR0019,oPanel,bClose,nTmGrdCpo,15,,,,.T.)//"&Fechar"
    oBtnClose:SetCSS( POSCSS (GetClassName(oBtnClose),CSS_BTN_NORMAL )) 

    
 oBrowse := TWBrowse():New( 130 , 00, nWidth-1,nHeight-130,,aFields,{nTmGrdCpo,nTmGrdCpo,nTmGrdCpo},;                              
                            oPanel,,,,,{||},,,,,,,.F.,,.T.,,.F.,,, )    

oBrowse:SetArray(aBrowse) 
oBrowse:bLine       := {||{aBrowse[oBrowse:nAt,01],aBrowse[oBrowse:nAt,02],aBrowse[oBrowse:nAt,03]} }  
oBrowse:bLDblClick  := bLDblClick
oBrowse:DrawSelect()
 
oModal:Activate()

Return Nil

/*/{Protheus.doc} fPosReg
Fun��o de busca\posicionamento de Browse
@type function
@version 
@author eduardo.vicente
@since 05/07/2020
@param cBusca, character, Termo de Busca
@param nInd, numeric, Indice\coluna de Pesquisa
@param aBrw, array, dados de browse
@return nLin,numeric, Linha de posicionamento
/*/
Static Function fPosReg(cBusca,nInd,aBrw)

Local nLin  := 1

Default nInd    := 1
Default cBusca  := ""
Default aBrw    := {}
//----------------------------------
//VALIDA��O DE PAR�METROS DE BUSCA
//----------------------------------
If Empty(cBusca) .Or. Empty(nInd)
    MsgInfo( STR0020 ) //"� necess�rio escolher a coluna e digitar o termo de busca."
Else
    //-----------------------------------
    //POSICIONAMENTO NO REGISTRO\LINHA
    //-----------------------------------
    nLin:= ASCAN(aBrw, { |x| ALLTRIM(UPPER(cBusca)) $ UPPER(x[nInd]) }) 
    
    If nLin < 1
        MsgInfo( STR0021 )//"Registro n�o encontrado."
    EndIf
EndIf
Return nLin

/*/{Protheus.doc} fViewDLG
Visualiza��o da linha posicionada
@type function
@version 
@author edvf8
@since 06/07/2020
@param aLinPos, array, Linha posicionada
@param aFields, array, Colunas do grid
@return return_type, return_description
/*/
Static Function fViewDLG(aLinPos,aFields)

Local lRet      := .T.
Local cTextPos  := ""
Local oDlg := Nil
Local oMemoP := Nil
Local oMemoC := Nil
Local oFontTit := Nil
Local oFontMem := Nil
Local oSay  := Nil

Default aLinPos := {}

cTextPos := aLinPos[2]
cTextComp := aLinPos[3]

DEFINE FONT oFontTit NAME "Mono AS" SIZE 005,012 BOLD
DEFINE FONT oFontMem NAME "Mono AS" SIZE 005,012 

DEFINE MSDIALOG oDlg TITLE STR0043 + aLinPos[1] From 003,000 TO 370,417 PIXEL //"Diferen�a campo: "

@ 005,005 SAY oSay PROMPT aFields[2] SIZE 200,20 COLORS CLR_RED,CLR_WHITE FONT oFontTit OF oDlg PIXEL
@ 010,005 GET oMemoP VAR cTextPos MEMO SIZE 200,070 OF oDlg PIXEL
@ 090,005 SAY oSay PROMPT aFields[3] SIZE 200,20 COLORS CLR_BLUE,CLR_WHITE FONT oFontTit OF oDlg PIXEL
@ 096,005 GET oMemoC VAR cTextComp MEMO SIZE 200,070 OF oDlg PIXEL

oMemoP:bRClicked := {||AllwaysTrue()}
oMemoP:oFont := oFontMem
oMemoC:oFont := oFontMem

DEFINE SBUTTON  FROM 169,175 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL

ACTIVATE MSDIALOG oDlg CENTER

Return lRet

/*/{Protheus.doc} fEvtComp
Consulta Especifica 

@author Silas.Gomes
@since 16/07/2020
@version 1.0
/*/       
Function fEvtComp()

	Local oListBox		:= Nil
	Local oArea			:= Nil
	Local oList			:= Nil
	Local oButt1 		:= Nil
	Local oButt2 		:= Nil
	Local oButt3 		:= Nil
	Local aCols  		:= {}
	Local aColSizes 	:= { 60, 50, 20 }
	Local aCoord		:= {}
	Local aWindow		:= {}
	Local aHeader   	:= { STR0032, STR0033 , STR0034 , STR0035 , STR0036 , STR0037 }//"ID" # "CPF" # "Nome" # "Evento" # "Data Altera��o" # "Status de Transmiss�o"
	Local cTitulo   	:= "" 
	Local cQuery 		:= ""
	Local cAliasquery 	:= GetNextAlias()
	Local cFiltro		:= STR0038 + Space(GetSx3Cache('C9V_NOME'    ,   'X3_TAMANHO'))//"CPF, Nome ou Evento..."
	Local Nx 			:= 0
	Local lLGPDperm 	:= IIF(FindFunction("PROTDATA"),ProtData(),.T.)
	Local lSemReg       := .F.

	Private cCpfPesq	:= Alltrim(ReadVar())
	Private cNomePesq	:= ""
	Private cEventPesq	:= ""
	Private dDtAltPesq	:= ""
	
	cTitulo   := STR0039 // "Consulta Espec�fica de Trabalhador"

	If !Empty(cEvento)

		cQuery	:= fQryTrb(cEvento,cCpf,nRecnoPos)

	EndIf
	
	cQuery := ChangeQuery(cQuery)

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasquery,.T.,.T.)

	While (cAliasquery)->(!Eof())
		aAdd(aCols,{(cAliasquery)->(Id), (cAliasquery)->(CPF), (cAliasquery)->(Nome), (cAliasquery)->(EVENTO), SToD((cAliasquery)->(Alteracao)), fRtnSts((cAliasquery)->(STATUS)), (cAliasquery)->(FILIAL), (cAliasquery)->(RECNO) })
		(cAliasquery)->(dbSkip())
	End
	(cAliasquery)->(dbCloseArea())

	If Len(aCols) < 1
		aAdd(aCols,{" "," "," "," "," "," "})
		lSemReg := .T.
	EndIf

	aCoord 	:= {000,000,400,800}
	aWindow := {020,073}

	oArea := FWLayer():New()
	oFather := tDialog():New(aCoord[1],aCoord[2],aCoord[3],aCoord[4],cTitulo,,,,,CLR_BLACK,CLR_WHITE,,,.T.)
	oArea:Init(oFather,.F., .F. )

	oArea:AddLine("L01",100,.T.)

	oArea:AddCollumn("L01C01",99,.F.,"L01")
	oArea:AddWindow("L01C01","TEXT","A��es",aWindow[01],.F.,.F.,/*bAction*/,"L01",/*bGotFocus*/)
	oText	:= oArea:GetWinPanel("L01C01","TEXT","L01")

	TSay():New(005,01,{||STR0040},oText,,,,,,.T.,,,200,20)//'Pesquisa:'
	TGet():New(003,028,{|u| if( PCount() > 0, cFiltro := u, cFiltro ) },oText,130,009,"@!",,,,,,,.T.,,,,.T.,,,.F.,,"","cFiltro",,,,.T.,.T.,,,,,,,,)
	oButt3 := tButton():New(003,160,STR0041,oText,{||fPesqTrb(oListBox,cFiltro)}, 45,11,,,.F.,.T.,.F.,,.F.,,,.F. )//"Pesquisar"

	oArea:AddWindow("L01C01","LIST",STR0042,aWindow[02],.F.,.F.,/*bAction*/,"L01",/*bGotFocus*/)//"Trabalhador"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
	oList	:= oArea:GetWinPanel("L01C01","LIST","L01")

	oButt1 := tButton():New(003,290,"&Confirmar",oText,{||IIF(!lSemReg,fPosicTrb(oListBox:nAt, aCols),oFather:End()),oFather:End()},45,11,,,.F.,.T.,.F.,,.F.,,,.F. )
	oButt2 := tButton():New(003,340,"&Sair",oText,{|| oFather:End()},45,11,,,.F.,.T.,.F.,,.F.,,,.F. )

	oFather:lEscClose := .T.

	nTamCol := Len(aCols[01])
	bLine 	:= "{|| {"
	For Nx := 1 To nTamCol
		bLine += "aCols[oListBox:nAt]["+StrZero(Nx,3)+"]"
		If Nx < nTamCol
			bLine += ","
		EndIf
	Next
	bLine += "} }"

	oListBox := TCBrowse():New(0,0,386,130,,aHeader,,oList,'Fonte')
	oListBox:SetArray( aCols )
	oListBox:bLine := &bLine

	If !lLGPDperm
		oListBox:aObfuscatedCols :={.F.,.T.,.T.}
	EndIf

	If !Empty( aColSizes )
		oListBox:aColSizes := aColSizes
	EndIf
	oListBox:SetFocus()	

	oFather:Activate(,,,.T.,/*valid*/,,/*On Init*/)


Return(.T.)

/*/{Protheus.doc} fQryTrb
Retorna a Query dos Eventos S-2200, S-2205, S-2206,
S-2300 e S-2306.
@type function
@version 
@author Silas.Gomes
@since 21/07/2020
@param cEvento, character, Evento posicionado
@param cCpf, character, cpf do trabalhador
@param nRecnoPos, numeric, recno do posicionado
@return cQuery, character, query de retorno
/*/
 Static Function fQryTrb(cEvento,cCpf,nRecnoPos)

	Local cQuery		:= ""
	Local cMatric		:= ""
    Local cEvtT1u       := ""
    Local cEvtT1v       := ""
    Local cEvtT0F       := ""

    Local lCLT          := .T.

	Default cEvento		:= ""
	Default cCpf		:= ""
	Default nRecnoPos	:= 0

	If cEvento == "S2205"
		DBSelectArea("C9V")
		C9V->(DBSetOrder(2))
		C9V->(DBSeek(cFilCs + cId + "1"))
		cMatric	:= C9V->C9V_MATRIC
	EndIf

	If (cEvento = "S2205" .And. !Empty(cMatric)) .Or. cEvento $ 'S2200/S2206'
		cEvtT1u := 'S2205'
		cEvtT1v	:= 'S2206'
        lCLT    := .T.
	ElseIf Empty(cMatric) .or. cEvento $ 'S2300/S2205/S2306'
		cEvtT1u := 'S2205'
		cEvtT0F	:= 'S2306'
        lCLT    := .F.
	EndIf		

	cQuery := " SELECT C9V_FILIAL AS Filial, C9V_ID AS Id, C9V_CPF AS CPF, C9V_Nome AS Nome, C9V_NOMEVE AS EVENTO, C9V_DTALT AS Alteracao, C9V_STATUS AS STATUS, C9V.R_E_C_N_O_ AS RECNO "
	cQuery += " FROM "+RetSQLName("C9V")+" C9V "
	cQuery += " WHERE C9V.D_E_L_E_T_ = ' ' AND C9V_FILIAL = '" + xFilial("C9V",cFilCs) + "' AND C9V.C9V_CPF = '"+cCpf+"' "
	If cEvento $ 'S2200#S2300'
    	cQuery+= " AND C9V.R_E_C_N_O_ != "+ALLTRIM(Str(nRecnoPos))+" "
		cQuery+= " AND C9V_NOMEVE = '" + cEvento  +"' "
	EndIf
	If cEvento == "S2205"
		If Empty(cMatric)
			cQuery+= " AND C9V_NOMEVE = 'S2300' "
		Else
			cQuery+= " AND C9V_NOMEVE = 'S2200' "
		EndIf
	EndIf

    If !(cEvento $ "S2206/S2306")

	    cQuery += " UNION ALL "
	    cQuery += " SELECT T1U_FILIAL AS Filial, T1U_ID AS Id, T1U_CPF AS CPF, T1U_NOME AS Nome, '"+cEvtT1u+"' AS EVENTO, T1U_DTALT AS ALTERACAO, T1U_STATUS AS STATUS, T1U.R_E_C_N_O_ AS RECNO "
	    cQuery += " FROM "+RetSQLName("T1U")+" T1U "
	    cQuery += " WHERE T1U.D_E_L_E_T_ = ' ' AND T1U_FILIAL = '" + xFilial("T1U",cFilCs)  + "' AND T1U.T1U_CPF = '"+cCpf+"' AND T1U.R_E_C_N_O_ IN ( "
	    cQuery += " SELECT DISTINCT T1U.R_E_C_N_O_ "
	    cQuery += " FROM "+RetSQLName("T1U")+" T1U "
	    cQuery += " INNER JOIN "+RetSQLName("C9V")+" C9V ON (C9V.C9V_FILIAL = T1U.T1U_FILIAL AND C9V.C9V_ID = T1U.T1U_ID
		If cEvento $ 'S2200#S2300'
			cQuery += " AND C9V_NOMEVE = '" + cEvento  +"'"
		EndIf
		cQuery += " ) "
	    cQuery += " WHERE C9V.D_E_L_E_T_ = ' ' "
    
	    If cEvento == 'S2205'
		    cQuery += " AND T1U.R_E_C_N_O_ != "+ALLTRIM(Str(nRecnoPos))+" "
	    EndIf
	    cQuery += " )"
        
    EndIf
    
    If lClt .And. cEvento != "S2205"

        cQuery  += " UNION ALL "
        cQuery  += " SELECT T1V_FILIAL AS Filial, T1V_ID AS Id, T1V_CPF AS CPF, T1V_NOME AS Nome, '"+cEvtT1v+"' AS EVENTO, T1V_DTALT AS ALTERACAO, T1V_STATUS AS STATUS, T1V.R_E_C_N_O_ AS RECNO "
        cQuery  += " FROM "+RetSQLName("T1V")+" T1V "
        cQuery  += " WHERE T1V.D_E_L_E_T_ = ' ' AND T1V_FILIAL = '" + xFilial("T1V",cFilCs)  + "' AND T1V.T1V_CPF = '"+cCpf+"' AND T1V.R_E_C_N_O_ IN ( "
        cQuery  += " SELECT DISTINCT T1V.R_E_C_N_O_ "
        cQuery  += " FROM "+RetSQLName("T1V")+" T1V "
        cQuery  += " INNER JOIN "+RetSQLName("C9V")+" C9V ON (C9V.C9V_FILIAL = T1V.T1V_FILIAL AND C9V.C9V_ID = T1V.T1V_ID) "
        cQuery  += " WHERE C9V.D_E_L_E_T_ = ' ' "
		
        If cEvento $ 'S2206'
            cQuery  += " AND T1V.R_E_C_N_O_ != "+ALLTRIM(Str(nRecnoPos))+" "
        EndIf
        cQuery  += " ) "
    EndIf

    If !lClt .AND. cEvento != "S2205"
        cQuery  += " UNION ALL "
        cQuery	+= "SELECT T0F_FILIAL AS Filial, T0F_ID AS Id, T0F_CPF AS CPF, T0F_NOME AS Nome, '"+cEvtT0F+"' AS EVENTO, T0F_DTALT AS ALTERACAO, T0F_STATUS AS STATUS, T0F.R_E_C_N_O_ AS RECNO "
        cQuery 	+= "FROM " + RetSqlName('T0F') + " T0F "
        cQuery 	+= "WHERE T0F.T0F_FILIAL = '" + xFilial("T0F",cFilCs)  + "' AND T0F.D_E_L_E_T_ = ' ' AND T0F.T0F_CPF = '"+cCpf+" '
        If cEvento $ 'S2306'
            cQuery  += " AND T0F.R_E_C_N_O_ != "+ALLTRIM(Str(nRecnoPos))+" "
        EndIf

    EndIf
	

Return cQuery

/*/{Protheus.doc} fPesqTrb
Fun��o responsavel por realizar a pesquisa por Nome e/ou CPF do trabalhador.
@type function
@version 
@author Silas.Gomes
@since 16/07/2020
@param oListBox, object, objeto listbox
@param cFiltro, character, informa��o de consulta
@return return_type, return_description
/*/
Static Function fPesqTrb(oListBox,cFiltro)

	Local nPos  	 := 0
	Local lRet  	 := .F.
	Local lPosPesq	 := .F.
	Local lCPF 		 := .F.
	Default oListBox := Nil
	Default cFiltro	 := ""

	cFiltro := AllTrim(cFiltro)

	// Faz um scan no objeto para encontrar a posi��o e posicionar no browser
	If Valtype(cFiltro) = "C" .And. !Empty(cFiltro)
		nPos := aScan( oListBox:aArray, {|x| cFiltro $ AllTrim(x[2])  } )
		lCPF := .T.
		If nPos == 0
			nPos := aScan( oListBox:aArray, {|x| cFiltro $ AllTrim(x[3]) } )			
		EndIf
		If nPos == 0
			nPos := aScan( oListBox:aArray, {|x| cFiltro $ AllTrim(x[4]) } )			
		EndIf

		If nPos > 0
			oListBox:GoPosition(nPos)
			oListBox:Refresh()
			lRet  := .T.
		EndIf

	EndIf

	If nPos == 0 .And. !lPosPesq
		MsgAlert(STR0022 + cFiltro + STR0023)      // N�o foi poss�vel encontrar o trabalhador na pesquisa
	EndIf

Return


/*/{Protheus.doc} fPosicTrb
Fun��o responsavel por realizar o posicionamento no registro selecionado na consulta do trabalhador.
@type function
@author Silas.Gomes
@since 21/07/2020
@param nPos, numeric, posi��o da linha 
@param aCols, array, coluna do grid
@return return_type, return_description
/*/
Static Function fPosicTrb(nPos,aCols)

	Default nPos		:= ""
	Default aCols  		:= ""

	cCpfCom 	:= aCols [nPos] [2]
	cNomeCom  	:= aCols [nPos] [3]
	cEventoCom	:= aCols [nPos] [4]
	dDtAltCom  	:= aCols [nPos] [5]

	cFilialCom  := aCols [nPos] [7]
	nRecnoCom   := aCols [nPos] [8]
	
Return .T.


/*/{Protheus.doc} fRtnSts
Retorna a descri��o do status de trasmiss�o.
@type function
@version 
@author Silas.Gomes
@since 21/07/2020
@param cSts, character, c�digo do status
@return cStatus, character, Descri��o de status
/*/
Static Function fRtnSts(cSts)

Local cStatus   := ""
Default cSts    := ""

    DO CASE

        CASE Empty(cSts)
            cStatus := STR0024          //"AGUARDANDO PROCESSAMENTO" 

        CASE cSts == "0"
            cStatus := STR0025          //"V�LIDO" 

        CASE cSts == "1"
            cStatus := STR0026          //"INV�LIDO" 

        CASE cSts == "2"
            cStatus := STR0027          //"TRANSMITIDO (AGUARDANDO RETORNO)"
            
        CASE cSts == "3"
            cStatus := STR0028          //"TRANSMITIDO INV�LIDO"

        CASE cSts == "4"
            cStatus := STR0029          //"TRANSMITIDO V�LIDO"
        
        CASE cSts == "6"
            cStatus := STR0030          //"PENDENTE DE EXCLUS�O"

            OTHERWISE
            cStatus := STR0031          //"EXCLUS�O EFETIVADA"

    ENDCASE

Return cStatus

/*/{Protheus.doc} fGetDtAlt
Fun��o encapsulada para retornar a data de altera��o
@type function
@version 
@author eduardo.vicente
@since 23/07/2020
@param nRecPos, numeric, Recno do evento posicionado
@param cEvtPos, character, Evento posicionado
@return dDtAltAtu, string, Data da altera��o
/*/
Static Function fGetDtAlt(nRecPos,cEvtPos)

Local dDtAltAtu	:= STOD(" / / ")
Local aArea		:= GetArea()

Default nRecPos	:= 0
Default cEvtPos := ""

If !Empty(cEvtPos)

	Do Case
		CASE cEvtPos $ "S2200/S2300"

			dbSelectArea("C9V")
			C9V->(dbSetOrder(1))
			C9V->(DBGOTO(nRecPos))
			dDtAltAtu := C9V->C9V_DTALT

		CASE cEvtPos $ "S2205"

			dbSelectArea("T1U")
			T1U->(dbSetOrder(1))
			T1U->(DBGOTO(nRecPos))
			dDtAltAtu := T1U->T1U_DTALT

		CASE cEvtPos  $ "S2206"

			dbSelectArea("T1V")
			T1V->(dbSetOrder(1))
			T1V->(DBGOTO(nRecPos))
			dDtAltAtu := T1V->T1V_DTALT

		CASE cEvtPos  $ "S2306"

			dbSelectArea("T0F")
			T0F->(dbSetOrder(1))
			T0F->(DBGOTO(nRecPos))
			dDtAltAtu := T0F->T0F_DTALT

	EndCase

EndIf

RestArea(aArea)

Return dDtAltAtu

/*/{Protheus.doc} TAFProcCmp
Realiza o processamento de compara��o dos registros
@type function
@version 1.0 
@author  Bruno de Oliveira
@since   27/07/2020
@param   cCpfPos   , caracter, CPF do registro posicionado
@param   cEventoPos, caracter, Evento do registro posicionado
@param   dDtAltPos , caracter, Data de altera��o do registro posicionado
@param   oBrowse   , objeto  , Browse principal 
@return  lRet      , logico  , retorno da fun��o
/*/
Static Function TAFProcCmp(cCpfPos,cEventoPos,dDtAltPos,oBrowse)

Local lRet        := .T.
Local cQuery      := ""
Local cEvtTran    := "S2200|S2205|S2206|S2300|S2306"
Local cAliasComp  := GetNextAlias()
Local nZ          := 0
Local nX          := 0
Local nLinha1     := 1
Local aDadosLin1  := {}
Local aResult     := {}
Local cDescricao  := ""
Local cRetPosicd  := ""
Local cRetCompad  := ""
Local cTabela     := ""
Local cRecTab     := ""
Local cResultMem  := ""
Local cTipo       := ""
Local cPosicCmp   := "" 
Local cCompaCmp   := ""

Private aCampos   := {}
Private cMemos    := ""
Private cRecPrinc := ""

If !Empty(cCpfPos) .AND. !Empty(cEventoPos) .AND. !Empty(cCpfCom) .AND. !Empty(cEventoCom)

	If len(aBrowse) > 0
		aSize(aBrowse,1)
		aBrowse[1] := {"","",""}
	EndIf

	cQuery := TAFBscDPar(cEventoPos,cEvtTran)

	If !Empty(cQuery)
		cQuery += cWhereFilD + "'" + cFilialpos + "'"  
		cQuery += cWhereFilP + "'" + cFilialCom + "'" 

		If cEventoPos $ cEvtTran .OR. cEventoCom $ cEvtTran //Apenas para eventos do trabalhador
			cQuery += 	cWhereCPFD + "'" + cCpfPos + "'" 
			cQuery += 	cWhereDtAD + "'" + DTOS(dDtAltPos) + "'" 
			cQuery += 	cWhereCPFP + "'" + cCpfCom + "'" 
			cQuery += 	cWhereDtAP + "'" + DTOS(dDtAltCom) + "'" 
		EndIf

		cQuery += cWhereRecD + "'" + Alltrim(STR(nRecnoPos)) + "'" 
		cQuery += cWhereRecP + "'" + Alltrim(STR(nRecnoCom)) + "'" 

		cQuery += cWhereDelD 
	EndIf

	cQuery := ChangeQuery(cQuery)

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasComp,.T.,.T.)

	While (cAliasComp)->(!EOF()) 

		If cEventoPos <> cEventoCom

			For nZ := 1 to Len(aCampos)

				For nX := 1 to Len(aCampos[nZ][2])

					If !((aCampos[nZ][2][nX]) $ cMemos)

						cTipo := ""
						cTipo := AllTrim( Posicione( "SX3", 2, aCampos[nZ][2][nX], "X3_TIPO" ) )

						If cTipo == "N"
							cPosicCmp := (cAliasComp)->&(aCampos[nZ][2][nX])
							cCompaCmp := (cAliasComp)->&(aCampos[nZ+1][2][nX])
						Else
							cPosicCmp := AllTrim((cAliasComp)->&(aCampos[nZ][2][nX]))
							cCompaCmp := Alltrim((cAliasComp)->&(aCampos[nZ+1][2][nX]))
						EndIf

						If cPosicCmp <> cCompaCmp

							aResult := TafConCmps(aCampos[nZ][2][nX],aCampos[nZ+1][2][nX],cPosicCmp,cCompaCmp)
							
							cDescricao := aResult[1]
							cRetPosicd := aResult[2]
							cRetCompad := aResult[3]

							aAdd(aBrowse,{cDescricao,cRetPosicd,cRetCompad})

						EndIf
					Else
						//tratamento compara��o campos memos
						aResult := TafTratMem(aCampos[nZ][2][nX],aCampos[nZ+1][2][nX],(cAliasComp))

						If Len(aResult) > 0
							cDescricao := aResult[1]
							cRetPosicd := aResult[2]
							cRetCompad := aResult[3]

							aAdd(aBrowse,{cDescricao,cRetPosicd,cRetCompad})
						EndIf

					EndIf

					cPosicCmp  := "" 
					cCompaCmp  := ""
					cDescricao := ""
					cRetPosicd := ""
					cRetCompad := ""

				Next nX
				nZ := nZ + 1
			Next nZ

		Else

			For nZ := 1 to Len(aCampos)

				For nX := 1 to Len(aCampos[nZ][2])

					If nLinha1 == 1
						If nX == 1
							aAdd(aDadosLin1,{nZ,{AllTrim((cAliasComp)->&(aCampos[nZ][2][nX]))}})	
						Else
							If !((aCampos[nZ][2][nX]) $ cMemos)
							
								cTipo := ""
								cTipo := AllTrim( Posicione( "SX3", 2, aCampos[nZ][2][nX], "X3_TIPO" ) )

								If cTipo == "N"
									cPosicCmp := (cAliasComp)->&(aCampos[nZ][2][nX])
								Else
									cPosicCmp := AllTrim((cAliasComp)->&(aCampos[nZ][2][nX]))
								EndIf

								aAdd(aDadosLin1[nZ][2],cPosicCmp)
							Else
								cTabela := SubStr(aCampos[nZ][2][nX],1,3)
								cRecTab := (cAliasComp)->&(cTabela+'REC')

								If !Empty(cRecTab)
									DbSelectArea(cTabela)
									(cTabela)->(DbGoto(cRecTab))
									cResultMem := (cTabela)->&(aCampos[nZ][2][nX])
								EndIf

								aAdd(aDadosLin1[nZ][2],IIF(!Empty(cResultMem),cResultMem,""))
							EndIf
						EndIf
						
						cTabela    := ""
						cRecTab    := ""
						cResultMem := ""

					Else

						If !((aCampos[nZ][2][nX]) $ cMemos)

							cTipo := ""
							cTipo := AllTrim( Posicione( "SX3", 2, aCampos[nZ][2][nX], "X3_TIPO" ) )

							If cTipo == "N"
								cPosicCmp := (cAliasComp)->&(aCampos[nZ][2][nX])
								cCompaCmp := aDadosLin1[nZ][2][nX]
							Else
								cPosicCmp := AllTrim((cAliasComp)->&(aCampos[nZ][2][nX]))
								cCompaCmp := aDadosLin1[nZ][2][nX]
							EndIf

							If cPosicCmp <> cCompaCmp

								aResult := TafConCmps(aCampos[nZ][2][nX],aCampos[nZ][2][nX],AllTrim((cAliasComp)->&(aCampos[nZ][2][nX])),aDadosLin1[nZ][2][nX])
								
								cDescricao := aResult[1]
								cRetPosicd := aResult[2]
								cRetCompad := aResult[3]

								If Alltrim(STR(nRecnoPos)) == Alltrim(STR((cAliasComp)->&(cRecPrinc)))
									aAdd(aBrowse,{cDescricao,cRetPosicd,cRetCompad })
								Else
									aAdd(aBrowse,{cDescricao,cRetCompad,cRetPosicd })
								EndIf

							EndIf
						Else
							//Tratamentos compara��o campos memos
							aResult := TafTratMem(aCampos[nZ][2][nX],"",(cAliasComp))

							If Len(aResult) > 0
								cDescricao := aResult[1]
								cRetPosicd := aResult[2]
								cRetCompad := aResult[3]
							EndIf

							If cRetPosicd <> aDadosLin1[nZ][2][nX]
								If Alltrim(STR(nRecnoPos)) == Alltrim(STR((cAliasComp)->&(cRecPrinc)))
									aAdd(aBrowse,{cDescricao,cRetPosicd,aDadosLin1[nZ][2][nX]})
								Else
									aAdd(aBrowse,{cDescricao,aDadosLin1[nZ][2][nX],cRetPosicd})
								EndIf
							EndIf
							
						EndIf
					EndIf

					cPosicCmp  := "" 
					cCompaCmp  := ""
					cDescricao := ""
					cRetPosicd := ""
					cRetCompad := ""

				Next nX

			Next nZ

		EndIf

		nLinha1++
		(cAliasComp)->(DbSkip()) 
	End

Else
	MsgAlert(STR0044) //"N�o ser� realizado processamento. Necess�rio informar o registro a ser comparado!")
EndIf

If Len(abrowse) > 1
	aDel(abrowse,1)
	aSize(abrowse,len(abrowse)-1)
EndIf

oBrowse:Refresh()

Return lRet

/*/{Protheus.doc} TAFBscDPar
Busca tabela De\Para
@type function
@version 1.0
@author  Bruno de Oliveira
@since   27/07/2020
@param   cEventoPos, caracter, Evento Posicionado
@param   cEvtTran  , caracter, Eventos relacionados ao trabalhador 
@return  cQuery    , caracter, consulta da tabela De\Para
/*/
Static Function TAFBscDPar(cEventoPos,cEvtTran)

Local cQuery     := ""
Local cCodDePar  := ""
Local cSelectP   := ""
Local cTabLeftD  := ""
Local cTabLeftP  := ""
Local cItemTab   := ""
Local cChavNeg   := ""
Local cCamposQry := ""
Local cSelectCmp := ""
Local cLeftJ     := ""
Local aStructCmp := {}
Local nX         := 0
Local nY         := 0
Local cRecnTabs  := ""
Local aEstrut1   := {}
Local aEstrut2   := {}

DbSelectArea("V5P")
DbSelectArea("V5Q")

cCodDePar := Posicione("V5O",3,XFilial("V5O")+SubStr(cEventoPos,2)+SubStr(cEventoCom,2),"V5O_ID")

If !Empty(cCodDePar)

	V5P->(DbSetOrder(1))
	V5P->(DbSeek(xFilial("V5P")+cCodDePar))
	While V5P->(!EOF()) .AND. V5P->(V5P_FILIAL+V5P_ID) == xFilial("V5P")+cCodDePar

		If Empty(cSelectP)
			
			cSelectP  := V5P->V5P_TABDE
			cTabLeftD := V5P->V5P_TABDE
			cTabLeftP := V5P->V5P_TABPAR

			cRecnTabs += cSelectP + 'TAB.R_E_C_N_O_ ' + cSelectP + 'REC,'

			cRecPrinc := cSelectP + 'REC'

			cWhereFilD := cTabLeftD + "_FILIAL = "
			cWhereFilP := " AND " + cTabLeftP + "_FILIAL = "
			If cEventoPos $ cEvtTran .OR. cEventoCom $ cEvtTran
				cWhereCPFD := " AND " + cTabLeftD + "_CPF = "
				cWhereDtAD := " AND " + cTabLeftD + "_DTALT = "
				cWhereCPFP := " AND " + cTabLeftP + "_CPF = "
				cWhereDtAP := " AND " + cTabLeftP + "_DTALT = "
			EndIf
			cWhereRecD := " AND (" + cSelectP + "TAB.R_E_C_N_O_ = "
			
			If cEventoPos <> cEventoCom
				cWhereRecP := " AND " 
			Else
				cWhereRecP := " OR "
			EndIf
			cWhereRecP += cTabLeftP + "TAB.R_E_C_N_O_ = "

			cWhereDelD := ") AND " + cSelectP + "TAB.D_E_L_E_T_ = ' ' "
		Else
			cTabLeftD := V5P->V5P_TABDE
			cTabLeftP := V5P->V5P_TABPAR	
		EndIf

		aEstrut1 := {}
		aEstrut2 := {}

		If !Empty(cTabLeftD)
			aEstrut1 := (cTabLeftD)->(DbStruct())
		EndIf
		aEstrut2 := (cTabLeftP)->(DbStruct())

		aAdd(aStructCmp,aEstrut1)
		aAdd(aStructCmp,aEstrut2)

		cItemTab := V5P->V5P_ITEM
		cChavNeg := Alltrim(V5P->V5P_CHVNEG)

		V5Q->(DbSetOrder(1))
		V5Q->(DbSeek(xFilial("V5Q")+cCodDePar+cItemTab))
		While V5Q->(!EOF()) .AND. V5Q->(V5Q_FILIAL+V5Q_ID+V5Q_ITEMTB) == xFilial("V5Q")+cCodDePar+cItemTab
			If !Empty(V5Q->V5Q_CAMPOS)
				cCamposQry := Alltrim(V5Q->V5Q_CAMPOS)
				aAdd(aCampos,{cTabLeftD+"x"+cTabLeftP,StrTokArr(cCamposQry,",")})
			EndIf
			
			If !Empty(cCamposQry)
				cSelectCmp += cCamposQry + ","
				cCamposQry := ""
				If cEventoPos == cEventoCom
					Exit
				EndIf
			EndIf

			V5Q->(DbSkip())
		End
		
		If !Empty(cTabLeftP) .AND. !Empty(cChavNeg)
			cLeftJ += ' LEFT JOIN ' + RetSqlName(cTabLeftP) + ' ' + cTabLeftP + 'TAB ON ' + cChavNeg
			cRecnTabs += cTabLeftP + 'TAB.R_E_C_N_O_ ' + cTabLeftP + 'REC,' 
		EndIf

		V5P->(DbSkip())
	End

	cQuery += "SELECT " + cSelectCmp
	cQuery += SubStr(cRecnTabs,1,Len(cRecnTabs)-1)
	cQuery += " FROM " + RetSqlName(cSelectP) + ' ' + cSelectP + 'TAB'
	cQuery += cLeftJ
	cQuery += " WHERE "

	For nX := 1 to Len(aStructCmp)

		For nY := 1 to Len(aStructCmp[nX])

			If aStructCmp[nX][nY][2] == "M"
				cMemos += aStructCmp[nX][nY][1] + "|"
			EndIf

		Next nY

	Next nX

EndIf

Return cQuery

/*/{Protheus.doc} TafConCmps
Realiza valida��o do tipo de Campo e faz a convers�o para mascara de campo.
@type function
@version 1.0
@author  Bruno de Oliveira
@since   05/08/2020
@param   cCampoPos , caracter, Campo do registro posicionado
@param   cCampoCprd, caracter, Campo do registro comparado
@param   cValPosic , caracter, Valor do registro posicionado
@param   cValCompar, caracter, Valor do registro posicionado
@return  Array, {retorno da descri��o, conteudo registo posicionado, conteudo registro comparado}
/*/
Static Function TafConCmps(cCampoPos,cCampoCprd,cValPosic,cValCompar)

Local cPosiciond := ""
Local cComparado := ""
Local cDscCombP  := ""
Local cDscCombC  := ""
Local cTituloCmp := ""
Local cTipo      := ""

cTipo := AllTrim( Posicione( "SX3", 2, cCampoPos, "X3_TIPO" ) )

If cTipo == "C"
	If TamSX3(cCampoPos)[1] == 1
		cDscCombP := cValPosic + "-" + X3COMBO(cCampoPos,cValPosic)
		cDscCombC := cValCompar + "-" +X3COMBO(cCampoCprd,cValCompar)
	EndIf
	cPosiciond := IIF(Empty(cDscCombP),cValPosic,cDscCombP)
	cComparado := IIF(Empty(cDscCombC),cValCompar,cDscCombC)
ElseIf cTipo == "D"
	cPosiciond := STOD(cValPosic)
	cComparado := STOD(cValCompar)
ElseIf cTipo == "N"
	cPosiciond := cValPosic
	cComparado := cValCompar
EndIf

cTituloCmp := TafTitCamp(cCampoPos)

Return {cTituloCmp,cPosiciond,cComparado}

/*/{Protheus.doc} TafTratMem
Tratamento da compara��o dos campos memos.
@type function
@version 1.0
@author  Bruno de Oliveira
@since   05/08/2020
@param   cCMemoPos , caracter, Campo Memo posicionado
@param   cCMemoCmp , caracter, Campo Memo comparado
@param   cAliasComp, caracter, Alias da query
@return  aRet      , array   , {retorno da descri��o, conteudo registo posicionado, conteudo registro comparado}
/*/
Static Function TafTratMem(cCMemoPos,cCMemoCmp,cAliasComp)

Local cDescricao := ""
Local cTabelaPos := ""
Local cRecTabPos := ""
Local cTabelaCmp := ""
Local cRecTabCmp := ""
Local cPosiciond := ""
Local cComparado := ""
Local aRet       := {}

cTabelaPos := SubStr(cCMemoPos,1,3)
cRecTabPos := (cAliasComp)->&(cTabelaPos+'REC')

If !Empty(cCMemoCmp)
	cTabelaCmp := SubStr(cCMemoCmp,1,3)
	cRecTabCmp := (cAliasComp)->&(cTabelaCmp+'REC')
EndIf

If !Empty(cRecTabPos)
	DbSelectArea(cTabelaPos)
	(cTabelaPos)->(DbGoto(cRecTabPos))
	cPosiciond := (cTabelaPos)->&(cCMemoPos)

	cDescricao := TafTitCamp(cCMemoPos)
EndIf

If !Empty(cRecTabCmp)
	DbSelectArea(cTabelaCmp)
	(cTabelaCmp)->(DbGoto(cRecTabCmp))
	cComparado := (cTabelaCmp)->&(cCMemoCmp)
EndIf

If cPosiciond <> cComparado
	aRet := {cDescricao,cPosiciond,cComparado}
EndIf

Return aRet

/*/{Protheus.doc} TafTitCamp
Tratamento da compara��o dos campos memos.
@type function
@version 1.0 
@author  Bruno de Oliveira
@since   06/08/2020
@param   cCampoPos, caracter, Campo posicionado
@return  cRet     , caracter, titulo do campo
/*/
Static Function TafTitCamp(cCampoPos)

Local cRet := ""

cRet := AllTrim( Posicione( "SX3", 2, cCampoPos, "X3_TITULO" ) )

Return cRet