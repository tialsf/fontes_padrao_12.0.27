#Include "Protheus.ch"

Static cBarra        := Iif(isSrvUnix(), "\", "/")

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFConvSxs
Convers�o dos Sxs de dbf para cTree.

@author Victor A. Barbosa
@since 20/09/2019
/*/
//-------------------------------------------------------------------
Main Function TAFConvSxs(cExtension)

Local cStartPath    := GetSrvProfString("StartPath", "\system\")
Local cDirDestiny   := cBarra + "tafsxsproc" + cBarra // Diret�rio onde ser� convertido os SXS
Local cDirBackup    := cBarra + "tafsxsbkp" + cBarra // Diret�rio de backup dos SXS
Local cFileStatus   := ""
Local lConvOk       := .T.
Local oError        := ErrorBlock({ |e| ConvDisarm(e) })
Local oParam        := Nil

Default cExtension  := ""

// Private, pois � utilizado em diversos fontes do programa (evitar verbosidade).
Private cDbExtension := ""

If !Empty(cExtension)
    oParam := JsonObject():New()
    oParam:FromJSON(cExtension)

    cDbExtension := Upper(oParam["localdbextension"])

    FreeObj(oParam)
Else
    cDbExtension := ".DBF"
EndIf

// Utilizadas por RecLock
Public __TTSInUse   := .F.
Public __cLogSiga   := ""

// Ativando a visualizacao registos deletados
SET DELETED OFF
SET SOFTSEEK ON
SET DATE BRITISH
SET CENTURY ON
SET EPOCH TO 1950

BEGIN SEQUENCE

    If File(cStartPath + "TAFConvSXSOk.json")
        fErase(cStartPath + "TAFConvSXSOk.json")
    EndIf

    If File(cStartPath + "TAFConvSXSError.json")
        fErase(cStartPath + "TAFConvSXSError.json")
    EndIf


    //Efetua a c�pia dos DBFs para um diret�rio paralelo.
    CloneDBF(cStartPath, cDirDestiny, cDirBackup)

    // Efetua a troca do RDD de ads para cTree.
    lConvOk := ChangeRDD(cDirDestiny)

    // Deleta os �ndices ADS
    DeleteIndADS(cStartPath)

    // Copia os novos arquivos para a system/profile
    CopyConvFiles(cStartPath, cDirDestiny)

    If lConvOk
        cFileStatus := "TAFConvSXSOk.json"

        // Rebuild em thread secund�ria, pois a fun��o Final() sem interface gr�fica gera uma "exception"
        StartJob("FWRebuildIndex", GetEnvServer(), .T., .F.)
    Else
        cFileStatus := "TAFConvSXSError.json"
    EndIf

    // Gera um arquivo, indicando a conclus�o do processo.
    MemoWrite(cStartPath + cFileStatus, ' ')

END SEQUENCE

ErrorBlock(oError)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} CloneDBF
Efetua o clone dos Sxs para outro diret�rio

@author Victor A. Barbosa
@since 20/09/2019
/*/
//-------------------------------------------------------------------
Static Function CloneDBF(cStartPath, cDirDestiny, cDirBackup)

Local nX        := 0
Local aFilesDBF := Directory(cStartPath + "*" + cDbExtension)

CreateDir(cDirDestiny)
CreateDir(cDirBackup)

For nX := 1 To Len(aFilesDBF)
    CopyDBF(cStartPath, aFilesDBF[nX][1], cDirDestiny, cDirBackup)
Next nX

// C�pia dos arquivos com extens�o diferente de .dbf (sigamat, profile, etc.)
CopyOtherFiles(cStartPath, cDirDestiny, cDirBackup)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} CopyDBF
Efetua a c�pia do DBF para os diret�rios de processamento/backup

@author Victor A. Barbosa
@since 23/09/2019
/*/
//-------------------------------------------------------------------
Static Function CopyDBF(cStartPath, cDBF, cDirDestiny, cDirBackup)

Local cAlias    := GetNameFile(cDBF)
Local cFPTFile  := cAlias + ".fpt"
Local cDBFFile  := cAlias + cDbExtension

// Necess�rio alterar o nome do arquivo de processamento, pois o ambiente pode estar com a extens�o .dtc e driver ads.
Local cProcDB   :=  cAlias + "_proc" + cDbExtension
Local cProcFPT  := cAlias + "_proc.fpt"
    
__CopyFile(cStartPath + cDBFFile, cDirDestiny + cProcDB)
__CopyFile(cStartPath + cDBFFile, cDirBackup + cDBFFile)

// Verifica se existe o arquivo .fpt (memo)
If File(cStartPath + cFPTFile)
    __CopyFile(cStartPath + cFPTFile, cDirDestiny + cProcFPT)
    __CopyFile(cStartPath + cFPTFile, cDirBackup + cFPTFile)
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} CopyOtherFiles
Efetua a c�pia dos arquivos com extens�o diferente de .dbf

@author Victor A. Barbosa
@since 23/09/2019
/*/
//-------------------------------------------------------------------
Static Function CopyOtherFiles(cStartPath, cDirDestiny, cDirBackup)

Local cDirProf  := cBarra + "profile" + cBarra
Local cSigaMat  := "sigamat.emp"
Local cSigaBMD  := "sigaadv.bmd"
Local cProfile  := "profile.usr"
Local cProfFPT  := "profile.fpt"

If File(cStartPath + cSigaMat)
    __CopyFile(cStartPath + cSigaMat, cDirDestiny + StrTran(cSigaMat, "emp", cDbExtension))
    __CopyFile(cStartPath + cSigaMat, cDirBackup + cSigaMat)
EndIf

If File(cStartPath + cSigaBMD)
    __CopyFile(cStartPath + cSigaBMD, cDirDestiny + StrTran(cSigaBMD, "bmd", cDbExtension))
    __CopyFile(cStartPath + cSigaBMD, cDirBackup + cSigaBMD)
EndIf

If File(cDirProf + cProfile)
    __CopyFile(cDirProf + cProfile, cDirDestiny + StrTran(cProfile, "usr", cDbExtension))
    __CopyFile(cDirProf + cProfFPT, cDirDestiny + cProfFPT)

    __CopyFile(cDirProf + cProfile, cDirBackup + cProfile)
    __CopyFile(cDirProf + cProfFPT, cDirBackup + cProfFPT)
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ChangeRDD
Efetua a convers�o da RDD dos dicion�rios de dados

@author Victor A. Barbosa
@since 23/09/2019
/*/
//-------------------------------------------------------------------
Static Function ChangeRDD(cDirChange)

Local aChangeDBF    := Directory(cDirChange + "*" + cDbExtension)
Local aStru         := {}
Local aSM0          := {}
Local aTabConv      := {}
Local nX            := 0
Local cAlias        := ""
Local cNewTable     := ""
Local cDBFFile      := ""
Local cFileDelete   := ""
Local lShared       := .F.
Local lRead         := .F.
Local lRet          := .T.
Local oDBFFile      := Nil

For nX := 1 To Len(aChangeDBF)
    cDBFFile    := Upper(aChangeDBF[nX][1])
    oDBFFile  := TAFDBFFile():New(cDirChange + cDBFFile)
    
    If oDBFFile:Open()
        If Right(cDBFFile, 4) == cDbExtension
            cFileDelete := GetNameFile(cDBFFile)
            cAlias      := GetNameFile(cDBFFile, .T.)
            cNewTable   := cAlias + ".dtc"
            aStru       := oDBFFile:GetStruct()

            If Select(cAlias) > 0
                (cAlias)->(dbCloseArea())
            EndIf

            If cAlias == "SIGAMAT"
                aSM0 := ReadSM0Data(aStru, oDBFFile)
            EndIf

            DBCreate( cDirChange + cNewTable, aStru, "CTREECDX" )
            DbUseArea(.T., "CTREECDX", cDirChange + cNewTable, cAlias, lShared, lRead)

            // Alimenta o novo arquivo .dtc
            InsertDTC(cAlias, aStru, oDBFFile)

            (cAlias)->(dbCloseArea())

            // --> Exclui o .dbf ap�s a convers�o
            DeleteDBF(cDirChange, cFileDelete)

            If ("SIX" $ cAlias) .Or. ("SX" $ cAlias) .Or. ("XX" $ cAlias)
                aAdd(aTabConv, cAlias)
            EndIf

            ConOut("Tabela: " + cAlias + " convertida" )
        EndIf
    Else
        lRet := .F.
        ConOut("Erro na abertura da Tabela: " + cAlias )
    EndIf
    
    FreeObj(oDBFFile)
    aStru   := {}

Next nX

For nX := 1 To Len(aSM0)
    If aScan( aTabConv, {|x| SubStr(x, 4, 2)  == aSM0[nX] } ) <= 0
        ConOut("Dicionario da Empresa " + aSM0[nX] + " nao foi criado." )
        lRet := .F.
        Exit
    EndIf
Next nX

Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} CopyConvFiles
Copia os arquivos cTree para o startpath

@author Victor A. Barbosa
@since 23/09/2019
/*/
//-------------------------------------------------------------------
Static Function CopyConvFiles(cStartPath, cDirDestiny)

Local aDTCStartPath := {}
Local nX            := 0
Local cDirProf      := cBarra + "profile" + cBarra

If File(cDirDestiny + "profile.dtc")
    // exclus�o dos arquivos ads
    fErase(cDirProf + "profile.usr")
    fErase(cDirProf + "profile.cdx")

    fRename(cDirDestiny + "profile.dtc", cDirProf + "profile.usr")
EndIf

If File(cDirDestiny + "sigamat.dtc")
    // exclus�o dos arquivos ads
    fErase(cStartPath + "sigamat.emp")
    fErase(cStartPath + "sigamat.ind")

    fRename(cDirDestiny + "sigamat.dtc", cStartPath + "sigamat.emp")
EndIf

If File(cDirDestiny + "sigaadv.dtc")
    // exclus�o dos arquivos ads
    fErase(cStartPath + "sigaadv.bmd")

    fRename(cDirDestiny + "sigaadv.dtc", cStartPath + "sigaadv.bmd")
EndIf

aDTCStartPath := Directory(cDirDestiny + "*.dtc")

For nX := 1 To Len(aDTCStartPath)
    
    // exclus�o dos arquivos ads
    If File(cStartPath + GetNameFile(aDTCStartPath[nX][1]) + cDbExtension )
        fErase(cStartPath + GetNameFile(aDTCStartPath[nX][1]) + cDbExtension )
    EndIf

    If File(cStartPath + GetNameFile(aDTCStartPath[nX][1]) + ".cdx" )
        fErase(cStartPath + GetNameFile(aDTCStartPath[nX][1]) + ".cdx" )
    EndIf

    If File(cStartPath + GetNameFile(aDTCStartPath[nX][1]) + ".fpt" )
        fErase(cStartPath + GetNameFile(aDTCStartPath[nX][1]) + ".fpt" )
    EndIf

    fRename(cDirDestiny + aDTCStartPath[nX][1], cStartPath + aDTCStartPath[nX][1])
    
Next nX

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} CreateDir
Efetua a cria��o de um diret�rio dentro do rootpath

@author Victor A. Barbosa
@since 23/09/2019
/*/
//-------------------------------------------------------------------
Static Function CreateDir(cNewDir)

If !ExistDir(cNewDir)
    MakeDir(cNewDir)
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} DeleteDBF
Exclus�o dos arquivos .dbf e fpt tempor�rios.

@author Victor A. Barbosa
@since 23/09/2019
/*/
//-------------------------------------------------------------------
Static Function DeleteDBF(cDir, cFile)

Local cFileDBF := cFile + cDbExtension
Local cFPTFile := cFile + ".FPT"

If File(cDir+cFileDBF)
    fErase(cDir+cFileDBF)
EndIf

If File(cDir+cFPTFile)
    fErase(cDir+cFPTFile)
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} DeleteIndADS
Exclus�o dos �ndices ADS

@author Victor A. Barbosa
@since 24/09/2019
/*/
//-------------------------------------------------------------------
Static Function DeleteIndADS(cStartPath)

Local cDirProf  := cBarra + "profile" + cBarra
Local aCDXStartPath := Directory(cStartPath + "*.cdx")
Local aCDXProfile   := Directory(cBarra + "profile" + cBarra + "*.cdx")
Local nX            := 0

For nX := 1 To Len(aCDXStartPath)
    fErase(cStartPath + aCDXStartPath[nX][1])
Next nX

For nX := 1 To Len(aCDXProfile)
    fErase(cDirProf + aCDXProfile[nX][1])
Next nX

fErase(cStartPath + "sigamat.ind")
fErase(cStartPath + "sigaadv.bmi")
fErase(cStartPath + "mpupdlog.log")

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} InsertDTC
Insere os registros do DBF no arquivo .dtc

@author Victor A. Barbosa
@since 02/10/2019
/*/
//-------------------------------------------------------------------
Static Function InsertDTC(cAlias, aStru, oDBFFile)

Local nX        := 0
Local nTamStru  := Len(aStru)

oDBFFile:GoTop()

While !oDBFFile:Eof()

    RecLock(cAlias, .T.)
            
    For nX := 1 To nTamStru 
        &(aStru[nX][1]) := oDBFFile:FieldGet(nX)
    Next nX

    (cAlias)->(MsUnlock())
    
    // Marca o registro como exclu�do
    If oDBFFile:Deleted()
        RecLock(cAlias, .F.)
        (cAlias)->(DbDelete())
        (cAlias)->(MsUnlock())
    EndIf

    oDBFFile:Skip()
EndDo

// Fecha a tabela 
oDBFFile:Close()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ConvDisarm
"Disarma" a convers�o

@author Victor A. Barbosa
@since 02/10/2019
/*/
//-------------------------------------------------------------------
Static Function ConvDisarm(oErr)

Default oErr    := Nil

ConOut("Erro na conversao.")
ConOut("O processo de conversao sera revertido")

If oErr <> NIL
    ConOut("Description Fatal error: " + AllTrim(oErr:Description) )
    ConOut("ErrorEnv Fatal error: " + AllTrim(oErr:ErrorEnv) )
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} GetNameFile
Extrai o nome ou alias do arquivo de uma string 
**Baseado em SduFile**

@author Victor A. Barbosa
@since 23/09/2019
/*/
//-------------------------------------------------------------------
Static Function GetNameFile(cFile, lDelCoringa)

Local cTempFile := ''
Local nI        := 0
Local nT        := 0
Local cR        := 0

Default lDelCoringa := .F.

cFile := Upper(Alltrim(cFile))

// Agora remove extensao
If Rat(".",cFile) <> 0
	cFile := Substr(cFile,1,Rat(".",cFile)-1)
EndIf

// Agora gera um nome de alias valido
cTempFile := NoAcento(AnsiToOem(cFile))
nT := len(cTempFile)
cFile := ""

For nI := 1 to nT
	cR := substr(cTempFile,nI,1)
	If ( cR >= "A" .and. cR <= "Z" ) 
		cFile += cR
	ElseIf ( cR >= "0" .and. cR <= "9" ) 
		If nI == 1
			cFile += "_"+cR
		Else
			cFile += cR
		Endif
    ElseIf cR == "_"
        cFile += cR
    Endif
Next

// Remove o caracter coringa
If lDelCoringa
    cFile := StrTran(cFile, "_PROC", "")
EndIf

Return(cFile)

//-------------------------------------------------------------------
/*/{Protheus.doc} ReadSM0Data
Extrai as empresas do SIGAMAT

@author Victor A. Barbosa
@since 15/10/2019
/*/
//-------------------------------------------------------------------
Static Function ReadSM0Data(aStru, oDBFFile)

Local nPosM0Cod := aScan(aStru, {|x| AllTrim(x[1]) == "M0_CODIGO"})
Local aSM0      := {}
Local cEmpFile  := ""

oDBFFile:GoTop()

While !oDBFFile:Eof()
    If !oDBFFile:Deleted()
        cEmpFile  := oDBFFile:FieldGet(nPosM0Cod)

        If aScan(aSM0, {|x| x == cEmpFile}) <= 0
            aAdd( aSM0, cEmpFile )
        EndIf
    EndIf
    oDBFFile:Skip()
EndDo

Return(aSM0)