#include 'Protheus.ch'

/*
	--* CLASSE: TAFISAMFILE *--
	--* Criada originalmente por J�lio Wittwer *--
	-----------------------------------------------------------------------------
	Utilizada para realizar a abertura de arquivos ads em bin�rio x64
	-----------------------------------------------------------------------------
	--* N�O UTILIZAR ESSA CLASSE!!! *--
	-----------------------------------------------------------------------------
*/

/* ======================================================================================
Classe       ZISAMFILE
Autor        Julio Wittwer
Data         01/2019
Descri��o    A Classe ZISAMFILE serve de base para implementa��o de tabeas ISAM 
             atrav�s de heran�a. Atualmente � herdada pelas classes ZDBFFILE e ZMEMFILE\
             
Ela serve para unificar os m�todos comuns de processamento e l�gica de acesso a 
registros em tabela ISAM 
             
====================================================================================== */

CLASS TAFISAMFILE FROM LONGNAMECLASS

  DATA cError			    // Descri��o do �ltimo erro 
  DATA lVerbose             // Modo Verbose (echo em console ligado)
  DATA bFilter              // Codeblock de filtro 
  DATA nIndexOrd            // Ordem de indice atual 
  DATA aIndexes             // Array com objetos de indice 
  DATA oCurrentIndex        // Objeto do indice atual 
  DATA nRecno				// N�mero do registro (RECNO) atualmnete posicionado 
  DATA nLastRec				// Ultimo registro do arquivo - Total de registros
  DATA aStruct		   		// Array com a estrutura do DBF 
  DATA nFldCount			// Quantidade de campos do arquivo 
  DATA lBOF					// Flag de inicio de arquivo 
  DATA lEOF					// Flag de final de arquivo 
  DATA lOpened              // Indica se o arquivo est� aberto 
  DATA lCanWrite            // Arquivo aberto para gravacao 
  DATA aGetRecord			// Array com todas as colunas do registro atual 
  DATA aPutRecord           // Array com campos para update 

  
  DATA oFileDef             // Defini��o extendida do arquivo 

  METHOD New()              // *** O Construtor nao pode ser chamado diretamente ***
  METHOD GoTo(nRec)		    // Posiciona em um registro informado. 
  METHOD GoTop()			// Posiciona no RECNO 1 da tabela 
  METHOD GoBottom()   	    // Posiciona em LASTREC da tabela 
  METHOD Skip()             // Navega��o de registros ISAM 
  METHOD SetFilter()        // Permite setar um filtro para os dados 
  METHOD ClearFilter()      // Limpa o filtro 
  METHOD BOF()				// Retorna .T. caso tenha se tentado navegar antes do primeiro registro 
  METHOD EOF()				// Retorna .T, caso o final de arquivo tenha sido atingido 
  METHOD Lastrec()			// Retorna o total de registros / numero do ultimo registro da tabela 
  METHOD RecCount()			// Retorna o total de registros / numero do ultimo registro da tabela 
  METHOD GetStruct()		// Retorna CLONE da estrutura de dados da tabela 
  METHOD FCount()           // Retorna o numero de campo / colunas da tabela
  METHOD FieldName( nPos )	// Recupera o nome da coluna informada 
  METHOD FieldPos( cField ) // Retorna a posicao de um campo na estrutura da tabela ( ID da Coluna )
  METHOD FieldType( nPos )	// Recupera o tipo da coluna informada 

  METHOD SetOrder()         // Seta um indice / ordem ativa 
  METHOD IndexOrd()         // Retorna a ordem ativa
  METHOD IndexKey()         // Retorna a expressao de indice ativa 
  METHOD IndexValue()       // Retorna o valor da chave de indice do registro atual 
  METHOD Seek(cKeyExpr)     // Realiza uma busca usando o indice ativo 
  METHOD CreateIndex()      // Cria um Indice ( em memoria ) para a tabela 
  METHOD ClearIndex()       // Fecha todos os indices
  METHOD Search()           // Busca um registro que atenda os criterios informados

  METHOD GetErrorStr()		// Retorna apenas a descri��o do �ltimo erro ocorrido

  METHOD SetVerbose()       // Liga ou desliga o modo "verbose" da classe
  METHOD IsVerbose()        // Consulta ao modo verbose
  
  METHOD SetFileDef()       // Guarda o objeto da defini��o do arquivo 


  // ========================= Metodos de uso interno da classe

  METHOD _ResetError()		// Limpa a ultima ocorrencia de erro 
  METHOD _SetError()        // Seta uma nova ocorrencia de erro 
  METHOD _InitVars() 		// Inicializa propriedades  

  METHOD _CheckFilter()     // Verifica se o registro atual est� contemplado no filtro 
  METHOD _SkipNext()		// Le o proximo registro da tabela 
  METHOD _SkipPrev()        // Le o registro anterior da tabela 
  METHOD _ClearRecord()     // Limpa o conteudo do registro em memoria 
  METHOD _BuildFieldBlock(cFieldExpr) // Cria codeblock com expressao de campos 
 
ENDCLASS


// ----------------------------------------
METHOD New(oParent) CLASS TAFISAMFILE
Return

// ----------------------------------------
// Retorna .T. caso a ultima movimenta��o de registro 
// tentou ir antes do primeiro registro 
METHOD BOF() CLASS TAFISAMFILE 
Return ::lBOF

// ----------------------------------------\
// Retorna .T. caso a tabela esteja em EOF
METHOD EOF() CLASS TAFISAMFILE
Return ::lEOF

// ----------------------------------------------------------
// Posiciona diretamente em um regsitro 

METHOD GoTo(nRec)  CLASS TAFISAMFILE

// Verifica se o registro � v�lido 
// Se n�o for, vai para EOF

If nRec > ::nLastRec .or. nRec < 1
	::lEOF := .T.
	::_ClearRecord()
	Return
Endif

// ----------------------------------------
// Atualiza o numero do registro atual 
::nRecno := nRec

If ::nIndexOrd > 0 
	// Eu tenho indice ativo, sincroniza a posicao do indice 
	// com a posicao do registro atual 
	::oCurrentIndex:SetResync()
Endif

// Traz o registro atual para a mem�ria
::_ReadRecord()

Return

// ----------------------------------------------------------
// Movimenta a tabela para o primeiro registro 
// Release 20190105 : Contempla uso de indice

METHOD GoTop() CLASS TAFISAMFILE 

IF ::nLastRec == 0 
	// Nao h� registros 
	::lBOF := .T. 
	::lEOF := .T. 
	::nRecno   := 0
	::_ClearRecord()
	Return
Endif

If ::nIndexOrd > 0 
	// Se tem indice ativo, pergunta pro indice
	// quanl � o primeiro registro da ordem 
	::nRecno := ::oCurrentIndex:GetFirstRec()
Else
	// Ordem fisica 
	// Atualiza para o primeiro registtro 
	::nRecno     := 1
Endif

// Traz o registro atual para a mem�ria
::_ReadRecord()

If ( !::_CheckFilter() )
	// Nao passou na verificacao do filtro
	// busca o proximo registro que atenda
	::_SkipNext()
Endif

Return

// ----------------------------------------------------------
// Movimenta a tabela para o �ltimo registro

METHOD GoBottom() CLASS TAFISAMFILE 

IF ::nLastRec == 0 
	// Nao h� registros 
	::lBOF := .T. 
	::lEOF := .T. 
	::nRecno   := 0
	::_ClearRecord()
	Return
Endif

If ::nIndexOrd > 0 
	// Se tem indice ativo, pergunta pro indice
	// quanl � o primeiro registro da ordem 
	::nRecno := ::oCurrentIndex:GetLastRec()
Else
	// Ordem fisica 
	// Atualiza o RECNO para o ultimo registro 
	::nRecno     := ::nLastRec
Endif

// Traz o registro atual para a mem�ria
::_ReadRecord()

If ( !::_CheckFilter() )
	// Nao passou na verificacao do filtro
	// busca nos registros anteriores o primeiro que atende
	::_SkipPrev()
Endif

Return

// ----------------------------------------------------------
// Avan�a ou retrocede o ponteiro de registro 
// No caso de DBSkip(0), apenas faz refresh do registro atual   
// Default = 1 ( Pr�ximo Registro ) 

METHOD Skip( nQtd ) CLASS TAFISAMFILE
Local lForward := .T. 

If nQtd  == NIL
	nQtd := 1
ElseIF nQtd < 0 
	lForward := .F. 
Endif

// Quantidade de registros para mover o ponteiro
// Se for negativa, remove o sinal 
nQtd := abs(nQtd)

While nQtd > 0 
	If lForward
		IF ::_SkipNext()
			nQtd--
		Else
			// Bateu EOF()
			::_ClearRecord()
			Return
		Endif
	Else
		IF ::_SkipPrev()
			nQtd--
		Else
			// Bateu BOF()
			Return
		Endif
	Endif
Enddo

// Traz o registro atual para a mem�ria
::_ReadRecord()

Return


// ----------------------------------------------------------
// Permite setar um filtro para a navega��o de dados 
// Todos os campos devem estar em letras maiusculas 

METHOD SetFilter( cFilter ) CLASS TAFISAMFILE
Local cFilterBlk

// retorna string com codebloc para expressao de campos 
cFilterBlk := ::_BuildFieldBlock(cFilter)

// Monta efetivamente o codeblock 
::bFilter := &(cFilterBlk)

Return .T. 

// ----------------------------------------------------------
// Limpa a expressao de filtro atual 

METHOD ClearFilter() CLASS TAFISAMFILE
::bFilter := NIL
Return


// ----------------------------------------------------------
// Retorna o numero do ultimo registro da tabela 

METHOD Lastrec() CLASS TAFISAMFILE
Return ::nLastRec

// ----------------------------------------------------------
// Colocado apenas por compatibilidade 
// 

METHOD Reccount() CLASS TAFISAMFILE
Return ::nLastRec

// ----------------------------------------------------------
// Retorna um clone do Array da estrutura da tabela 

METHOD GetStruct() CLASS TAFISAMFILE
Return aClone( ::aStruct )

// ----------------------------------------------------------
// Retorna o numero de campo / colunas da tabela
METHOD FCount()  CLASS TAFISAMFILE
Return ::nFldCount

// ----------------------------------------------------------
// Recupera o nome de um campo da tabela 
// a partir da posicao do campo na estrutura

METHOD FieldName(nPos) CLASS TAFISAMFILE
If nPos > 0 .and. nPos <= ::nFldCount 
	Return ::aStruct[nPos][1]
Endif
Return NIL

// ----------------------------------------------------------
// Recupera o numero do campo na estrutura da tabela 
// a partir do nome do campo 

METHOD FieldPos( cField ) CLASS TAFISAMFILE
Return ASCAN( ::aStruct , {|x| x[1] = cField })

// ----------------------------------------------------------
// Recupera o tipo do campo na estrutura da tabela 
// a partir da posicao do campo na estrutura

METHOD FieldType(nPos) CLASS TAFISAMFILE
If nPos > 0 .and. nPos <= ::nFldCount 
	Return ::aStruct[nPos][2]
Endif
Return NIL

// ----------------------------------------
// Permite trocar a ordedm atual usando 
// um indice aberto 

METHOD SetOrder(nOrd) CLASS TAFISAMFILE

If nOrd < 0 .OR.  nOrd > len( ::aIndexes )
	UserException("DbSetOrder - Invalid Order "+cValToChar(nOrd))
Endif
::nIndexOrd := nOrd
If ::nIndexOrd > 0 
	::oCurrentIndex := ::aIndexes[::nIndexOrd]
	::oCurrentIndex:SetResync()
Else
	::oCurrentIndex := NIL
Endif
Return

// ----------------------------------------
// Retorna o numero da ordem do indce ativo 

METHOD IndexOrd() CLASS TAFISAMFILE
Return ::nIndexOrd

// ----------------------------------------
// Retorna a expressao da chave de indice atual 
// Caso nao haja indice ativo, retorna ""

METHOD IndexKey() CLASS TAFISAMFILE
IF ::nIndexOrd > 0 
	Return ::oCurrentIndex:GetIndexExpr()
Endif
Return ""

// ----------------------------------------
// Retorna o numero da ordem do indce ativo 
METHOD IndexValue() CLASS TAFISAMFILE
IF ::nIndexOrd > 0 
	Return ::oCurrentIndex:GetIndexValue()
Endif
Return NIL


// ----------------------------------------
// Retorna o numero da ordem do indce ativo 
METHOD Seek(cKeyExpr) CLASS TAFISAMFILE
Local nRecFound := 0

IF ::nIndexOrd <= 0
	UserException("DBSeek Failed - No active Index")
Endif

nRecFound := ::oCurrentIndex:IndexSeek(cKeyExpr)

If nRecFound > 0
	// NAo precisa resincronizar o indice
	// Eu j� fiz a busca pelo indice
	::nRecno := nRecFound
	::_ReadRecord()
	Return .T.
Endif

// Nao achou nada, vai para EOF 
::lEOF := .T.
::_ClearRecord()

Return .F.
	
  
// ----------------------------------------
// *** METODO DE USO INTERNO ***
// Cria uma instancia de um indice em memoria 
// Acrescenta na lista de indices 
// Torna o indice ativo e posiciona no primeiro 
// registro da nova ordem 

METHOD CreateIndex(cIndexExpr) CLASS TAFISAMFILE
Local oMemIndex
Local nLastIndex

// Cria o objeto do indice passando a instancia
// do arquivo DBF atual 
oMemIndex := ZMEMINDEX():New(self)

// Cria o indice com a expressao informada
oMemIndex:CreateIndex(cIndexExpr) 

// Acrescenta o indice criado na tabela 
aadd(::aIndexes,oMemIndex)

// E torna este indice atual 
nLastIndex := len(::aIndexes)
::SetOrder( nLastIndex ) 

// Posiciona no primeiro registro da nova ordem 
::GoTop()

Return

// ----------------------------------------
// Fecha todos os indices

METHOD ClearIndex()  CLASS TAFISAMFILE
Local nI

For nI := 1 to len(::aIndexes)
	::oCurrentIndex := ::aIndexes[nI]
	::oCurrentIndex:Close()
	FreeObj(::oCurrentIndex)
Next

Return

// ----------------------------------------------------------
// Recebe a definicao extendida da tabela 
// Com isso eu j� tenho a estrutura 

METHOD SetFileDef(oDef)  CLASS TAFISAMFILE

IF ::lOpened
	UserException("SetFileDef Failed - Table already opened")
	Return .F.
Endif

// Recebe a defini��o do arquivo 
::oFileDef := oDef

Return .T.

// ----------------------------------------
// *** METODO DE USO INTERNO ***
// Verifica se o registro atual est� contemplado pelo filtro 
// Release 20190106 -- Contempla filtro de registros deletados

METHOD _CheckFilter() CLASS TAFISAMFILE

If ::lSetDeleted .AND. ::lDeleted
	// Filtro de deletados est� ligado 
	// e este registro est� deletado .. ignora
	Return .F. 
Endif

If ::bFilter != NIL 
	// Existe uma expressao de filtro 
	// Roda a express�o para saber se este registro 
	// deve estar  "Visivel" 
	Return Eval(::bFilter , self )	
Endif

Return .T. 

// ----------------------------------------
// *** METODO DE USO INTERNO ***
// Le e posiciona no proximo registro, considerando filtro 

METHOD _SkipNext() CLASS TAFISAMFILE
Local nNextRecno

While (!::lEOF)

	If ::nIndexOrd > 0 
		// Se tem indice ativo, pergunta pro indice
		// qual � o pr�ximo registro
		nNextRecno := ::oCurrentIndex:GetNextRec()
	Else
		// Estou na ordem fisica
		// Parte do registro atual , soma 1 
		nNextRecno := ::Recno() + 1 
	Endif
	
	// Retornou ZERO ou 
	// Passou do final de arquivo, esquece
	If nNextRecno == 0 .OR. nNextRecno > ::nLastRec
		::lEOF := .T.
		::_ClearRecord()
		Return .F. 
	Endif

	// ----------------------------------------
	// Atualiza o numero do registro atual 
	::nRecno := nNextRecno

	// Traz o registro atual para a mem�ria
	::_ReadRecord()

	// Passou na checagem de filtro ? Tudo certo 
	// Senao , continua lendo ate achar um registro valido 
	If ::_CheckFilter()
		Return .T. 
	Endif

Enddo

Return .F. 

// ----------------------------------------
// *** METODO DE USO INTERNO ***
// Le e posiciona no registro anmterior, considerando filtro 

METHOD _SkipPrev() CLASS TAFISAMFILE
Local nPrevRecno

While (!::lBOF)

	If ::nIndexOrd > 0 
		// Se tem indice ativo, pergunta pro indice
		// qual � o registro anterior
		nPrevRecno := ::oCurrentIndex:GetPrevRec()
	Else
		// Estou na ordem fisica
		// Parte do registro atual , subtrai 1
		nPrevRecno := ::Recno() - 1 
    Endif
    
	// Tentou ler antes do primeiro registro 
	// Bateu em BOF()
	If nPrevRecno < 1 
		::lBOF := .T.
		Return .F. 
	Endif

	// ----------------------------------------
	// Atualiza o numero do registro atual 
	::nRecno := nPrevRecno

	// Traz o registro atual para a mem�ria
	::_ReadRecord()

	// Passou na checagem de filtro ? Tudo certo 
	// Senao , continua lendo ate achar um registro valido 
	If ::_CheckFilter()
		Return .T. 
	Endif

Enddo

// Chegou no topo. 
// Se tem filtro, e o registro nao entra no filtro, localiza 
// o primeir registro v�lido 
If ( !::_CheckFilter() )
	::GoTop()
	::lBOF := .T. 
Endif

Return .F. 

// ----------------------------------------------------------
// Retorna apenas a descri��o do ultimo erro 

METHOD GetErrorStr() CLASS TAFISAMFILE 
Return ::cError

// ----------------------------------------------------------
// *** METODO DE USO INTERNO ***
// Limpa o registro do ultimo erro 

METHOD _ResetError() CLASS TAFISAMFILE 
::cError := ''
Return

// ----------------------------------------------------------
// *** METODO DE USO INTERNO ***
// Seta uma nova ocorrencia de erro

METHOD _SetError(cErrorMsg) CLASS TAFISAMFILE 
::cError := cErrorMsg
Return


// ----------------------------------------------------------
// Permite setar o modo "verbose" da classe

METHOD SetVerbose( lSet ) CLASS TAFISAMFILE 
::lVerbose := lSet
Return

// ----------------------------------------------------------
// Retorna  .T. se o modo verbose est� ligado 

METHOD IsVerbose() CLASS TAFISAMFILE 
Return ::lVerbose

// ----------------------------------------------------------
// Inicializa as propriedades da classe base

METHOD _InitVars() CLASS TAFISAMFILE 

::lOpened       := .F. 
::lCanWrite     := .F. 
::cError        := ''
::lVerbose      := .T. 
::bFilter       := NIL
::lBof          := .F. 
::lEof          := .F. 
::nIndexOrd     := 0
::aIndexes      := {}
::oCurrentIndex := NIL
::nLastRec      := 0
::aStruct       := {}
::nFldCount     := 0
::aGetRecord    := {}
::aPutRecord    := {}

Return


// ----------------------------------------------------------
// *** METODO DE USO INTERNO ***
// Limpa os campos do registro atual de leitura
// ( Inicializa todos com os valores DEFAULT ) 
// Limpa campos de grava��o / update 
// ( seta NIL nos elementos ) 

METHOD _ClearRecord() CLASS TAFISAMFILE
Local nI , cTipo , nTam

// Inicializa com o valor default os campos da estrutura 
For nI := 1 to ::nFldCount
	cTipo := ::aStruct[nI][2]
	nTam  := ::aStruct[nI][3]
	If cTipo == 'C'
		::aGetRecord[nI] := space(nTam)
	ElseIf cTipo == 'N'
		::aGetRecord[nI] := 0
	ElseIf cTipo == 'D'
		::aGetRecord[nI] := ctod('')
	ElseIf cTipo == 'L'
		::aGetRecord[nI] := .F.
	ElseIf cTipo == 'M'
		::aGetRecord[nI] := 0
	Endif
Next

// Zera tamb�m registro de grana��o
::aPutRecord := Array(::nFldCount)

Return

// ----------------------------------------------------------
// Cria uma string para criar codeblock dinamico 
// baseado em expressao usando camposa da tabela
// Os campos devem estar em letras mai�sculas. Cada campo ser� 
// trocado por o:FieldGet(nPos), o codeblock deve ser usado 
// com Eval() passando como argumento o objeto da tabela 

METHOD _BuildFieldBlock(cFieldExpr) CLASS TAFISAMFILE
Local aCampos := {}
Local cBlockStr
Local nI, nPos

// Cria lista de campos
aEval( ::aStruct , {|x| aadd(aCampos , x[1]) } )

// Ordena pelos maiores campos primeiro
aSort( aCampos ,,, {|x,y| alltrim(len(x)) > alltrim(len(y)) } )

// Copia a expressao 
cBlockStr := cFieldExpr

// Troca os campos por o:Fieldget(nCpo)
// Exemplo : CAMPO1 + CAMPO2 ser� trocado para o:FieldGet(1) + o:FieldGet(2)

For nI := 1 to len(aCampos)
	cCampo := alltrim(aCampos[nI])
	nPos   := ::Fieldpos(cCampo)
	cBlockStr  := StrTran( cBlockStr , cCampo,"o:FieldGet(" +cValToChar(nPos)+ ")")
Next

// Monta a string com o codeblock para indice
cBlockStr := "{|o| "+cBlockStr+"}"

Return cBlockStr

// ----------------------------------------------------------
// Busca um registro que atenda os criterios informados
// aRecord recebe os dados a procurar no formato [1] Campo [2][ Conteudo 
// aFound retorna o registro encontrado por referencia ( todos os campos ) 
// no mesmo formato do aRecord, acrescido do RECNO 
// Por padrao a busca � feita por substring 
// Caso seja especificada busca EXATA, os conteudos dos campos 
// informados devem ter correspondencia exata com a base de dados

METHOD Search(aRecord,aFound,lExact)  CLASS TAFISAMFILE 
Local nCnt := len(aRecord)
Local nI
Local aFldPos := {}
Local nFound := 0

IF lExact = NIL
	lExact := .F.
Endif

aSize(aFound,0)

// Sempre posiciona no topo 

If nCnt <= 0 
	
	// Sem condi��es especificadas, pega 
	// o primeiro registro 
	::GoTop()

Else

	// Mapeia campos informados com a posi��o 
	// do campo no arquivo 	
	For nI := 1 to nCnt
		aadd( aFldPos , ::fieldpos(aRecord[nI][1]) )
	Next
    
	// Come�a a busca sempre no inicio do arquivo 
	::GoTop()

	// FAz busca sequencial	
	While !::Eof()
		nFound := 0 
		For nI := 1 to nCnt 
			IF lExact
				// Busca exata
				IF ::FieldGet(aFldPos[nI]) == aRecord[nI][2]
					nFound++
				Endif
			Else
			    // Busca por substring ( ou "like %content%" ) 
				If alltrim(aRecord[nI][2]) $ ::FieldGet(aFldPos[nI])  
					nFound++
				Endif
			Endif
		Next
		If nFound == nCnt
			EXIT
		Endif
		::Skip()
	Enddo
	
Endif

If !::Eof()  
	// Nao estou em EOF = achei um registro 
	For nI := 1 to ::nFldCount
		aadd(aFound , {  ::FieldName(nI) , ::FieldGet(nI)  })
	Next
	// Acrescenta o RECNO no campo
	aadd(aFound,{"RECNO",::Recno()})
	Return .T.
Endif

::_SetError( "Nenhum registro foi encontrado baseado nos dados informados" )

Return .F. 


