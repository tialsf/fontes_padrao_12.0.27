
#ifndef TAFLibNBin_CH

  #define TAFLibNBin_CH

  #xtranslate Bin4toN( <cBin4> ) => STATICCALL( TAFLibNBin , Bin4toN , <cBin4> ) 
  #xtranslate NtoBin4( <nNum> )  => STATICCALL( TAFLibNBin , NtoBin4 , <nNum>  ) 

  #xtranslate Bin2toN( <cBin2> ) => STATICCALL( TAFLibNBin , Bin2toN , <cBin2> ) 
  #xtranslate NtoBin2( <nNum> )  => STATICCALL( TAFLibNBin , NtoBin2 , <nNum>  ) 

  #xtranslate NTOBIT8( <nNum> )  => STATICCALL( TAFLibNBin , NTOBIT8 , <nNum>  ) 
  #xtranslate NTOBIT16( <nNum> )  => STATICCALL( TAFLibNBin , NTOBIT16 , <nNum>  ) 

  #xtranslate NTOBITS( <nNum> )  => STATICCALL( TAFLibNBin , NTOBITS , <nNum>  ) 
  #xtranslate BITSTON( <nNum> )  => STATICCALL( TAFLibNBin , BITSTON , <nNum>  ) 

  #xtranslate Bit4ToHex( <cBit4> ) => STATICCALL( TAFLibNBin , Bit4ToHex , <cBit4> ) 

  #xtranslate BIT8TON( <cBit8> , <nNum> ) => STATICCALL( TAFLibNBin , BIT8TON , <cBit8> , @<nNum> ) 

#endif
