#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "WSTAF001.CH"

#DEFINE SOURCEBRANCH 		1
#DEFINE PERIOD				2
#DEFINE STATUS				3	
#DEFINE	EVENTS 				4
#DEFINE EVENTTYPE			5
#DEFINE PAGE				6
#DEFINE PAGESIZE			7
#DEFINE EMPPROC				8
#DEFINE FILPROC  			9  

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} WSTAF001
WS para retorno das inform��es referente ao estado dos eventos da reinf no TAF

Retorna o estado dos eventos no "legado", ou seja, a vis�o de apura��o, o que est� apurado ou n�o 
Retorna o estado dos eventos no "espelho", ou seja, a vis�o do monitor de transmiss�o, se o determinado
evento est� transmido, bem como o retorno do governo para o evento.

@author Henrique Fabiano Pateno Pereira
@since 29/03/20198
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

WSRESTFUL WSTAF001 DESCRIPTION STR0001

	WSDATA sourceBranch			AS STRING
	WSDATA period 				AS STRING
	WSDATA events 				AS STRING 
	WSDATA status	 			AS STRING 
	WSDATA eventType 			AS INTEGER
	WSDATA page					AS INTEGER	OPTIONAL
	WSDATA pageSize 			AS INTEGER	OPTIONAL 

	WSMETHOD GET 		DESCRIPTION STR0002 PRODUCES APPLICATION_JSON 

END WSRESTFUL 

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Metodo GET
M�todo respons�vel pela consulta ao programa nativo da reinf e montagem da mensagem de resposta para camada THF

@author Henrique Fabiano Pateno Pereira
@since 29/03/20198
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

WSMETHOD GET QUERYPARAM sourceBranch, period, status, events, eventType, page, pageSize WSREST  WSTAF001
   
local aRet			as array 
local aQryParam		as array
local aRotinas		as array
local aStatPer		as array
local aFiliais 		as array
local cResponse		as character
local cTag			as character
local cFuncIPC		as character
local cCodErr		as character
local cDescription	as character
local cFilProc 		as character
local cEmpProc 		as character
local cDisplInvc	as character
local cDisplName	as character
local cDisplStat	as character
local nStatus		as numeric
local nApiX			as numeric
local nApiY			as numeric
local nApiZ			as numeric
local nTotal		as numeric
local nStart		as numeric
local nQtdRegRet	as numeric
local nQtdRegTot	as numeric
local nLenMonit		as numeric
local nLenEvtRe		as numeric
local lGeraTotal	as logical
local lTotalizer	as logical
local lBildMinit	as logical
local oEvento 		as object

//-----------------------------------------------
// inicializa��o vari�veis do tipo array
//-----------------------------------------------
aRet			:=	{}
aQryParam		:=	{}
aRotinas		:= 	{}
aStatPer		:=  {}
aFiliais 		:= 	{}

//-----------------------------------------------
// inicializa��o variaveis do tipo string
//-----------------------------------------------
cResponse		:=	""
cTag			:=	""
cFuncIPC		:= 	""
cCodErr			:=	""
cDescription	:=	""
cFilProc 		:= 	""
cEmpProc 		:= 	""
cDisplInvc		:=	""
cDisplName		:=	""
cDisplStat		:=	""

//-----------------------------------------------
// inicializa��o variaveis numericas
//-----------------------------------------------
nApiX			:=	0
nApiY			:=	0
nApiZ			:=	0
nTotal			:=	0
nStart			:=	1 
nQtdRegRet		:=	0 // quantidae m�xima de retorn
nQtdRegTot		:=	0
nLenMonit		:=	0
nLenEvtRe		:=	0
nStatus			:=	6

//-----------------------------------------------
// inicializa��o variaveis booleanas
//-----------------------------------------------
lGeraTotal	:=	.f.
lTotalizer	:=	.f.
lBildMinit	:=	.t.

//-----------------------------------------------
// inicializa��o vari�veis do tipo object
//-----------------------------------------------
oEvento 	:= 	JsonObject():New()

self:SetContentType("application/json") 

//-------------------------------------------------------------------
// Valida a existencia da filial no TAF, tabela C1E.	
// abre o ambiente caso n�o esteja aberto	
//-------------------------------------------------------------------
 if WSPrepFil(self:sourceBranch,@cFuncIPC ,@cCodErr, @cDescription, @cFilProc, @cEmpProc )
	cFilProc := cFilAnt
	cEmpProc :=	cEmpAnt
	aFiliais := wsLoadFil()

	aRotinas		:= 	TafRotinas(,,.T.,5)  

	aQryParam	:= TafAdQryPar(self, cFilProc, cEmpProc)
	aStatPer	:= TafGetPerS(aQryParam[PERIOD], aFiliais)  
 
	//-------------------------------------------------------------------
	// Executa a consulta ao reinf tafxreinf.prw		
	//-------------------------------------------------------------------
	aRet := TafGetReinf(aQryParam, aRotinas, aFiliais) 
	
	//-------------------------------------------------------------------
	// Alimenta as variaveis com o controle de pagina��o (HasNext)
	//-------------------------------------------------------------------
	TafGetPage(@nStart, @nQtdRegRet, @nQtdRegRet, @nQtdRegTot, aQryParam, aRet )
	
	oEvento["eventsReinf"] := {} 
	oEvento["eventsReinfTotalizers"] := {} 

	//---------------------------------------------------------------------------
	// O for abaixo � executado com base nas vari�veis de controle de pagina��o
	// alimentadas em TafGetPage()
	//---------------------------------------------------------------------------
	for nApiX := nStart to nQtdRegRet
		cTagEvents 	:= "eventsReinf"	
		lTotalizer	:=	.f.
		lBildMinit	:=	.t.
		
		//-------------------------------------------------------------------
		// � um totalizador!
		//-------------------------------------------------------------------
		if aRet[nApiX][1] == "T" 
			cTagEvents 	:= "eventsReinfTotalizers"
			lTotalizer	:=	.t.
		endif

		aAdd( oEvento[cTagEvents],JsonObject():New()) 
		nLenEvtRe	:= len(oEvento[cTagEvents]) 

		//-------------------------------------------------------------------
		// N�o � um totalizador!
		//-------------------------------------------------------------------
		if !(aRet[nApiX][1] == "T") 
			// retorna o nome do tipo de evento para os cards do THF			
			oEvento[cTagEvents][nLenEvtRe]["metrics"] 	:=  Ws001Name(aRet[nApiX][2])

		endif
		
		//--------------------------------------------------------------------------------
		// inicio nApiY	:=	2 o indice 1 � apenas para saber se � um totalizador ou n�o	
		//--------------------------------------------------------------------------------
		for nApiY	:=	2	to	len(aRet[nApiX])		

			cTag	:=	TafRetTag(nApiY, aRet[nApiX][2])
			
			//-------------------------------------------------------------------------------------------------------
			// N�o gero todas as tags de TafRetTag() quando for totalizadores, apenas o nome do evento e a descri��o
			//-------------------------------------------------------------------------------------------------------
			if !empty(cTag) .and. (!lTotalizer .or. lTotalizer .and. (nApiY == 2 .or. nApiY == 3 ) )
				oEvento[cTagEvents][nLenEvtRe][cTag] := aRet[nApiX][nApiY] 
			endif 
 
			if lBildMinit 
				oEvento[cTagEvents][nLenEvtRe]["monitoring"] := {}
				lBildMinit	:=	.f.
			endif

			if valtype(aRet[nApiX][nApiY]) == "A" .and. len(aRet[nApiX][nApiY]) > 0 
				lBildMinit	:=	.t.
				lGeraTotal := .f.				
				
				for nApiZ := 1 to  len(aRet[nApiX][nApiY] )
					lGeraTotal := .t.	
					statusCode := iif(empty(aRet[nApiX][nApiY][nApiZ][2]) .or. aRet[nApiX][nApiY][nApiZ][2] == '1' ,0, val(aRet[nApiX][nApiY][nApiZ][2]))

					aAdd( oEvento[cTagEvents][nLenEvtRe]["monitoring"],JsonObject():New())
					nLenMonit	:=	len(oEvento[cTagEvents][nLenEvtRe]["monitoring"])		 	 
					nTotal	+=	aRet[nApiX][nApiY][nApiZ][1]
					oEvento[cTagEvents][nLenEvtRe]["monitoring"][nLenMonit]["quantity"] 	:= aRet[nApiX][nApiY][nApiZ][1]                                                       
					oEvento[cTagEvents][nLenEvtRe]["monitoring"][nLenMonit]["statusCode"] 	:= statusCode
					if statusCode < nStatus
						nStatus := statusCode
					endIf
				next

				if lGeraTotal
					oEvento[cTagEvents][nLenEvtRe]["totalMonitoring"] := nTotal
				endif

				oEvento[cTagEvents][nLenEvtRe]["statusMonitoring"] := nStatus

				nTotal	:= 0
				nStatus := 6
			endif
		next	
	next

	//------------------------------------------------------------------------------
	// Retorna o status atual do per�odo e o protocolo de fechamento caso exista.
	//------------------------------------------------------------------------------
	If len(aStatPer) > 0
		oEvento['statusPeriod'] := aStatPer[1,1]
		oEvento['protocol'] 	:= aStatPer[1,2]
	EndIf

	//-------------------------------------------------------------------
	// Valida a exitencia de mais paginas
	//-------------------------------------------------------------------
	if nQtdRegRet < nQtdRegTot
		oEvento['hasNext'] := .T.
	else
		oEvento['hasNext'] := .F.
	endif

	cResponse:= FWJsonSerialize( oEvento, .T., .T.,,.F. ) 
	self:SetResponse(cResponse ) 
	FreeObj(oEvento)

endif

lRet := .T.

return lRet 

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} fun��o est�tica TafGetReinf
Executa a consulta ao programa nativo da Reinf e retora o aRet com os eventos encontrados bem como seus status
de apura��o e transmiss�o, de acordo com os par�metros passados na chamada do WS

@author Henrique Fabiano Pateno Pereira
@since 29/03/20198
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

static function TafGetReinf(aQryParam, aRotinas, aFiliais)
local aRet			as array
local aRetAux		as array
local aFilToReinf	as array
local nEventFor		as numeric
local nPosEvento	as numeric
local nQtdApur		as numeric
local nQtdNApur		as numeric
local nTotFor       as numeric
local cDescriEv	 	as numeric
local nX			as numeric
local nVisao		as numeric
local nTotDocOk		as numeric
local nTotDocNo		as numeric
local cEventType	as character
local cStatPCons	as character
local cStatMonit	as character
local cStat			as character
local cEventAnt		as character
local lExstMonit	as logical
local lApur			as logical
local lGetStatus	as logical

//-----------------------------------------------
// inicializa��o variaveis do tipo array
//-----------------------------------------------
aRet			:=	{}
aRetAux			:= 	{}
aFilToReinf		:= 	{}

//-----------------------------------------------
// inicializa��o variaveis do tipo numeric
//-----------------------------------------------
nEventFor		:=	0
nPosEvento		:=	0
nQtdApur		:=	0
nQtdNApur		:=	0
nTotFor         :=  0
nX				:=	0
nVisao			:=	0
nTotDocOk		:= 	0 
nTotDocNo		:= 	0

//-----------------------------------------------
// inicializa��o variaveis do tpo caracter
//-----------------------------------------------
cStatPCons		:=	'' // status a ser considerado referente ao legado Pendente Apura��o/ Apurados
cStatMonit		:=	'' // status a ser considerado referente as transmiss�es
cStat			:=	'5'
cEventType		:=	''
cEventAnt		:=	''
cDescriEv	 	:=	0

//-----------------------------------------------
// inicializa��o variaveis do tipo l�gica
//-----------------------------------------------
lExstMonit	:=	.t. // nocaso de ter sido escolhido status referente a transmiss�o, valido a gera��o ou n�o do evento
lApur		:=	.f.
lGetStatus	:=	.f.

For nX:=1 To Len(aFiliais)
	aAdd(aFilToReinf,aFiliais[nX])
Next nX

	//----------------------------------------------------------------------------
	// Com base em self:status retorna o tipo de filtro que deve ser considerado 
	//----------------------------------------------------------------------------
	TAFVisFilt(@nVisao, @cStatMonit, @cStatPCons, aQryParam)

	for nEventFor := 1 to len(aRotinas) 
		cEvento		:= 	aRotinas[nEventFor][4]
		cEventType	:=	TafConvTypeEv(aRotinas[nEventFor][12])
		nTotDocOk	:=	0
		nTotDocNo	:=	0
		
		//-------------------------------------------------------------------------------------------------------------------------
		// Descarta os eventos que n�o foram passados em sel:events ou processa todos eventos se self:events for TODOS == '1'
		//--------------------------------------------------------------------------------------------------------------------------
		if 	(cEvento $ aQryParam[EVENTS] .or. "TODOS" $ aQryParam[EVENTS]) .and. (cEventType $ aQryParam[EVENTTYPE] .or. '|0|' $ aQryParam[EVENTTYPE] )
			 
			cDescriEv	:=	EncodeUTF8(TafRetDescr(cEvento)) 
				
			cStat := '5'
			
			if "R-2050" $ cEvento .or. ; 
			   "R-2010"	$ cEvento .or. ;
			   "R-2020"	$ cEvento
				lApur	:= .t.
				lGetStatus	:=	.t.  
			endif
			//-------------------------------------------------------------
			// n�o executo TafRStatEv para totalizadores cEventType == 4
			//-------------------------------------------------------------
			if cEventType <> '4'  
				cStat = TafRStatEv(aQryParam[PERIOD],,,cEvento,aFilToReinf, nVisao, @nQtdApur, @nQtdNApur, lApur, lGetStatus,/*cCNPJC1H*/ ,@nTotDocOk,@nTotDocNo, .T., @nTotFor) 
			endif
			lApur	:=	.f. 
			//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
			// cStat <> '5' .and. cStat == cStatPCons = Apenas se existe o determinado evento na base e cStat <> '5' e est� condizente com o filtr escolhido cStat == cStatPCons
			// cStat == '5' .and.	cEventType == 4	Totalizadores n�o existe na base ent�o considero cStat == 5 desde que seja um totalizador cEventType == 4	
			//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
			if (cStat <> '5' .and. cEventType <> '4') .or. (cStat == '5' .and.   cEventType == '4') 

				aRetAux	:=	TafEvRStat(aFilToReinf,aQryParam[PERIOD], cEvento,nil,cStatMonit, "R-1000|R-5011|R-5001", .T. )  
				lExstMonit	:=	.t. 
				
				if (cStat <> '5' .and. cEventType <> '4' .and. !(cStat $ cStatPCons .or. '|99|'$ cStatPCons)) .and. nVisao == 2
					lExstMonit	:=	.f. 
				endif

				//-----------------------------------------------------------------------------------------------------------------------------------------------------------
				// se for evento de tabela, ao menos um dos status escolhidos deve ser de transmiss�o ou todos, caso contr�rio n�o escrituro totalizadores cEventType == 4	
				//-----------------------------------------------------------------------------------------------------------------------------------------------------------
				if cEventType == '4' .and. !('|1|' $ aQryParam[STATUS])
					lExstMonit	:=	.f. 
				endif 
				if  '9' == aQryParam[STATUS]
					if (cStat <> '3') .or. ((aScan(aRetAux,{|x|x[2] $ ' |2|3|6'})) > 0)
						lExstMonit	:=	.f. 
					endif
				endif
				//--------------------------------------------------------------------------------------------------------
				// Se o status escolhido � referente a transmiss�o, s� gero o evento se houver retorno de TafEvrStat()
				// filtro referente a transmiss�o  
				//--------------------------------------------------------------------------------------------------------
				if 	!('2'$aQryParam[STATUS]) .and. !('3'$aQryParam[STATUS]) .or.  cEventType == '4' 
					if len(aRetAux) == 0 .and. cStat <> '1'  
						lExstMonit	:=	.f.   
					endif
				endif
				if lExstMonit .and. cEventAnt <> cEvento 
					cEventAnt := cEvento
					aadd(aRet,{aRotinas[nEventFor][12],cEvento,cDescriEv,val(cEventType)})
					nPosEvento	:=	len(aRet)
					if cEvento $ 'R-2010|R-2020'
						aadd(aRet[nPosEvento],nTotFor) 	// Total fornecedor
					else
						aadd(aRet[nPosEvento],nQtdApur+nQtdNApur) 	// Total fornecedor
					EndIf
					aadd(aRet[nPosEvento],nTotDocOk+nTotDocNo) 	// Total documento
					aadd(aRet[nPosEvento],nQtdApur			)	// Qtd apurado 
					aadd(aRet[nPosEvento],nQtdNApur			)  	// Qtd N�o apurado
					aadd(aRet[nPosEvento],aRetAux			) 	// Array status transmiss�o
				endif		
			endif
		endif 
	next

return aRet

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} fun��o est�tica TafRetTag
Identifica a TAG que deve ser escriturada na a montagem da mensagem de retorno

@author Henrique Fabiano Pateno Pereira
@since 29/03/20198
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

static function TafRetTag(nIndice, cEvento) 
local cTag	as character

default nIndice		:=	0
default cEvento	:=	''
//-----------------------------------------------
// Inicializa��o variaveis do tipo caracter
//-----------------------------------------------
cTag	:= ''

	Do Case
		Case nIndice == 2
			cTag	:=	"event"
		Case nIndice == 3
			cTag	:=	"descriptionEvent" 
		Case nIndice == 4
			cTag	:=	"typeEvent"
		Case nIndice == 5
			cTag	:=	"total"
		Case nIndice == 6 .and. (cEvento $ "R-2010|R-2020|R-2030|R-2040|R-2050") 
			cTag	:=	"totalInvoice" 
		Case nIndice == 7
			cTag	:=	"totalValidation"
		Case nIndice == 8
			cTag	:=	"totalNotValidation"
	endCase

return cTag

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} fun��o st�tica TafConvTypeEv
Converte de caracter para num�rico o tipo do evento que est� sendo processado 
Tipos:
C-Carga Inicial  ( Tabelas R-1000, R-1070) retorno retorno => 2
M-Mensal (Peri�dicos R-2010, R-2020, R-2030, R-2040, R2050, R-2060) retorno => 3
E-Eventual (N�o Peri�dicos R-3010, R-9000) retorno => retorno 4
T-Totalizador (Tabelas R-5001, R-5011) retorno=> 5

@author Henrique Fabiano Pateno Pereira
@since 29/03/20198
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

static function TafConvTypeEv(cTypeEvent)
local 	cEventType	as character

default	cTypeEvent	:=	""

//-----------------------------------------------
// Inicializa��o variaveis do tipo STRING
//-----------------------------------------------
cEventType	:=	''

	do case 
		case cTypeEvent == "C"
				cEventType	:=	'1' // Tabelas R-1000, R-1070
		case cTypeEvent == "M"
				cEventType	:=	'2' // Peri�dicos R-2010, R-2020, R-2030, R-2040, R2050, R-2060
		case cTypeEvent == "E"
				cEventType	:=	'3' // N�o Peri�dicos R-3010, R-9000
		case cTypeEvent == "T"
				cEventType	:=	'4' // Tabelas R-5001, R-5011
	endCase			

return cEventType

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} fun��o st�tica TAFVisFilt
Alimenta as vari�veis de controle do tipo de consulta que deve ser executada a Reinf de acordo
com o filtro recebido na requisi��o

==================================================================
||STATUS											||	NUMERO	||
||================================================================											
||Todos												||		1	||
||Pendentes apura��o								||		2	||
||Apurados											||		3	||
||Pendentes transmiss�o								||		4	||
||Retorno com erros (Transmitidos Inconsistente)	||		5	||
||Transmitidos (Transmitidos Consistente)			||		6	||
||Aguardando Retorno (Transmitido Aguardando)		||		7	||
||Reajustados										||		8	||
||Sem pendencia										||		9	||
==================================================================

@author Henrique Fabiano Pateno Pereira
@since 29/03/20198
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

static function TAFVisFilt(nVisao, cStatMonit, cStatPCons, aQryParam)

default nVisao 		:= 	0
default cStatMonit 	:= 	''
default cStatPCons	:=	'|'	 
default aQryParam	:=	{}

		if '|1|' == aQryParam[STATUS] // Todos 
			nVisao		:= 2
			cStatPCons	+=	'|99|' 
		endif
		if '|2|' $ aQryParam[STATUS] // Pendente apura��o
			nVisao		:= 2
			cStatPCons	+=	'|1|2|'// n�o apurado|apurado parcialmente
		endif
		if  '|3|' $ aQryParam[STATUS] // Apurados
			nVisao		:= 2
			cStatPCons	+=	'|3|'
		endif
		if '|4|' $ aQryParam[STATUS] // Pendentes Transmiss�o 
			nVisao		:= 3
			cStatPCons	+=	'|3|'
			if at(cStatMonit,"'0'") == 0
				cStatMonit	+=	"'0',"
			endif
		endif
		if '|5|' $ aQryParam[STATUS] // Retorno com erros (Transmitidos Inconsistente)
			nVisao		:= 3
			cStatPCons	+=	'|3|'
			if at(cStatMonit,"'3'") == 0
				cStatMonit	+=	"'3',"
			endif
		endif
		if '|6|' $ aQryParam[STATUS] // Transmitidos (Transmitidos Consistente/ com erro, transmitido aguardando)
			nVisao		:= 3
			cStatPCons	+=	'|3|'
			if at(cStatMonit,	"'4','3','2','6','7',") == 0
				cStatMonit	+=	"'4','3','2','6','7',"
			endif
		endif
		if '|7|' $ aQryParam[STATUS] // Aguardando Retorno (Transmitido Aguardando)
			nVisao		:= 3
			cStatPCons	+=	'|3|'
			if at(cStatMonit,"'2'") == 0
				cStatMonit	+=	"'2',"
			endif
		endif
			/*case aQryParam[STATUS] == 8 // Reajustados <at� o momento ainda n�o temos essa inteligencia na Reinf, quando for implementado esse ser� o c�digo
					nVisao		:=	1
					cStatPCons	:=	'3'
					cStatMonit	:=	'2'*/
		if '|9|' $ aQryParam[STATUS] // Sem pendencia
			nVisao		:= 2
			cStatPCons	+=	'3|'
			if at(cStatMonit,"'4'") == 0
				cStatMonit	+=	"'4',"
			endif
		endif 
	cStatMonit := substr(cStatMonit,1,len(cStatMonit)-1) 
return
//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} fun��o st�tica TafAdQryPar
Alimenta o array aQryParam para controle das consultas a reinf com base nos par�metros da requisi��o

@author Henrique Fabiano Pateno Pereira
@since 29/03/20198
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------
static function TafAdQryPar(self, cFilProc, cEmpProc)
local aQryParam	as array
local cPeriod 	as character

//-----------------------------------------------
// inicializa��o variaveis do tipo array
//-----------------------------------------------
aQryParam	:=	{} 

//-----------------------------------------------
// inicializa��o variaveis do tipo caracter
//-----------------------------------------------
cPeriod 	:=  if(empty(self:period),strzero(month(dDatabase),2)+cValToChar(Year(dDataBase)),self:period)

	aAdd(aQryParam,	self:sourceBranch			 ) 
	aAdd(aQryParam,	cPeriod						 )
	aAdd(aQryParam,	TafConvAtS(self, "STATUS") 	 )
	aAdd(aQryParam,	TafConvAtS(self, "EVENTS") 	 )
	aAdd(aQryParam,	TafConvAtS(self, "EVENTTYPE"))
	aAdd(aQryParam,	self:page					 )
	aAdd(aQryParam,	self:pageSize				 ) 
	aAdd(aQryParam,	cEmpProc					 ) 
	aAdd(aQryParam,	cFilProc					 )  

return aQryParam

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} fun��o st�tica TafGetReinf
Alimenta as vari�veis de controle de pagi��o com base nos par�metros page e pageSize passados na requisi��o

@author Henrique Fabiano Pateno Pereira
@since 29/03/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

static function TafGetPage(nStart, nQtdRegRet, nQtdRegRet, nQtdRegTot, aQryParam, aRet)

	if (aQryParam[PAGESIZE] <> nil .and. aQryParam[PAGESIZE] > 0) .and. (aQryParam[PAGE] <> nil .and. aQryParam[PAGE] > 0)
		nQtdRegRet	:=	(aQryParam[PAGESIZE] * aQryParam[PAGE])		
		if aQryParam[PAGE] > 1
			nStart := ( ( aQryParam[PAGE] - 1 ) * aQryParam[PAGESIZE] ) + 1
		endif
	else
		nQtdRegRet	:=	len(aRet)
		nStart		:=	1 
	endif
 
	if nQtdRegRet > len(aRet)
		nQtdRegRet	:=	len(aRet) 
	endif
	nQtdRegTot	:=	len(aRet)
return

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} fun��o st�tica TafGetReinf
Converte os parametros enviados como array para string
 
@author Henrique Fabiano Pateno Pereira
@since 29/03/20198
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

static function TafConvAtS(self, cIdConv)
local cRet			as character
local nx 			as numeric

default	cIdConv		:= ''
//-----------------------------------------------
// inicializa��o vari�veis do tipo string
//-----------------------------------------------
cRet 		:= ''

//----------------------------------------------- 
// inicializa��o vari�veis do tipo numericas
//-----------------------------------------------
nx := 0

	for nx := 1 to len(self:aQueryString)

		if self:aQueryString[nx][1] == cIdConv
			if empty(cRet)
				cRet 		:= '|'
			endif
			cRet += UPPER(cValToChar(self:aQueryString[nx][2] )) + '|' 
		endif
	next 
	
	if empty(cRet)
		do case 
			case cIdConv $ 'STATUS'
				cRet	:= '|1|'
			case cIdConv $ 'EVENTTYPE'
				cRet	:= '|0|'
			case cIdConv == 'EVENTS'
				cRet	:= 'TODOS'
		endCase
	elseif ('|0|' $ cRet .and. len(cRet)>3) 
		cRet := STRTRAN(cRet,'|0','')
	elseif ('|TODOS|'$cRet .and. len(cRet)>7) 
		cRet := STRTRAN(cRet,'|TODOS','') 
	endif
return cRet  

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} fun��o st�tica Ws001Name
Retorna o nome de tela dos eventos para os cards da primeiratela do Reinf - THF
 
@author Henrique Fabiano Pateno Pereira
@since 08/05/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

static function Ws001Name(cEvent)
local cDisplName	:=	''

default cEvent		:=	''

	do case
		case cEvent == "R-1000" 
			cDisplName :=  EncodeUTF8("contribuintes")
		case cEvent == "R-1070"
			cDisplName :=  EncodeUTF8("processos")	
		case cEvent == "R-2010"
			cDisplName :=  EncodeUTF8("fornecedores")
		case cEvent == "R-2020"
			cDisplName :=  EncodeUTF8("clientes")	
		case cEvent == "R-2030"
			cDisplName :=  EncodeUTF8("Filiais")
		case cEvent == "R-2040" 
			cDisplName :=  EncodeUTF8("clientes")
		case cEvent == "R-2050"
			cDisplName :=  EncodeUTF8("clientes")
		case cEvent == "R-2060"
			cDisplName :=  EncodeUTF8("CPRB")	
		case cEvent == "R-3010"
			cDisplName :=  EncodeUTF8("boletins")
		endCase 
return(cDisplName) 

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} fun��o st�tica TafGetPerS
Retorna um array com o status do per�odo informado e o n�mero do protocolo
 
@author Bruno Cremaschi
@since 12/06/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

static function TafGetPerS(cPeriodo)

local cAliasV1O := GetNextAlias()
local cStatus	:= ""
local aRet 		:= {}


BeginSql ALIAS cAliasV1O	
	SELECT 
		V1O1.V1O_NOMEVE,
		V1O1.V1O_PROTUL
	FROM 
		%TABLE:V1O% V1O1 
	WHERE 
		V1O1.V1O_PERAPU = %exp:cPeriodo% AND
		V1O1.V1O_ATIVO = '1' AND
		V1O1.%NOTDEL% AND 
		V1O1.V1O_FILIAL= %xFilial:V1O% AND 
		V1O1.V1O_EVENTO = 'A' AND 
		V1O1.V1O_DATA || V1O1.V1O_HORA = (
			SELECT 
				MAX(V1O2.V1O_DATA || V1O2.V1O_HORA)
			FROM %TABLE:V1O% V1O2 
			WHERE 
				V1O2.V1O_PERAPU = %exp:cPeriodo% AND 
				V1O2.V1O_ATIVO = '1' AND
				V1O2.%NOTDEL% AND 
				V1O2.V1O_EVENTO = 'A' AND 
				V1O2.V1O_FILIAL= %xFilial:V1O%)				
EndSql

If !(cAliasV1O)->(EOF())
	If !Empty((cAliasV1O)->V1O_NOMEVE)
		If "2098" $ (cAliasV1O)->V1O_NOMEVE
			cStatus := "open"
		ElseIf "2099" $ (cAliasV1O)->V1O_NOMEVE
			DBSelectArea( "V1A" )
			V1A->( DbSetOrder(2) )
			If V1A->( DbSeek(xFilial("V1A") + cPeriodo + "1") )
				If V1A->V1A_STATUS == "2"
					cStatus := "waitingReopening"
				Else
					cStatus := "open"
				EndIf
			Else	
				cStatus := "closed"
			EndIf
		EndIf
	Else
		DBSelectArea( "V0B" )
		V0B->( DbSetOrder(2) )
		If V0B->( DbSeek( xFilial("V0B") + cPeriodo + "1") )
			If V0B->V0B_STATUS == "2"
				cStatus := "waitingClosing"
			Elseif V0B->V0B_STATUS == "4"
				cStatus := "closed"
			Else
				cStatus := "open"
			EndIf
		Else
			cStatus := "open"
		EndIf
	EndIf

	Aadd(aRet, {cStatus, (cAliasV1O)->V1O_PROTUL} )
Else
	DBSelectArea( "V0B" )
	V0B->( DbSetOrder(2) )
	If V0B->( DbSeek( xFilial("V0B") + cPeriodo + "1") )
		If V0B->V0B_STATUS == "2"
			cStatus := "waitingClosing"
		Elseif V0B->V0B_STATUS == "4"
			cStatus := "closed"
		Else
			cStatus := "open"
		EndIf
	Else
		cStatus := "open"
	EndIf
	Aadd(aRet, {cStatus, ""} )
		
EndIf


(cAliasV1O)->(DbCloseArea())
V0B->(DbCloseArea())
V1A->(DbCloseArea())

Return(aRet)