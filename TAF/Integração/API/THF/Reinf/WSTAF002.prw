#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"

#DEFINE SINTETICO 1,1
#DEFINE DELETSINT 1,2
#DEFINE DELETANAL 2,2

#DEFINE CONTRIB             1
#DEFINE SOURCEBRANCH 		1
#DEFINE PERIOD				2
#DEFINE STATUS				3
#DEFINE	EVENTS 				4
#DEFINE EVENTTYPE			5
#DEFINE PAGE				6
#DEFINE PAGESIZE			7
#DEFINE EMPPROC				8
#DEFINE FILPROC  			9

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} WSTAF002
WS para retorno das inform��es referente ao detalhe das movimenta��es da base

@author Henrique Fabiano Pateno Pereira
@since 29/03/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------
WSRESTFUL WSTAF002 DESCRIPTION "API Reinf - Info Base para Apura��o"

    WSDATA sourceBranch			AS STRING
    WSDATA period 				AS STRING
    WSDATA event 				AS STRING
    WSDATA branchId             AS STRING
    WSDATA id                   AS STRING
    WSDATA item                 AS STRING
    WSDATA page					AS INTEGER	OPTIONAL
    WSDATA pageSize 			AS INTEGER	OPTIONAL

    WSMETHOD POST eventDetail DESCRIPTION "Documentos pendentes de apura��o por eventos" WSSYNTAX "/eventDetail"  PATH 'eventDetail' PRODUCES APPLICATION_JSON
    WSMETHOD GET invoiceDetail DESCRIPTION "Detalhe dos documentos pendentes de apura��o por eventos" WSSYNTAX "/invoiceDetail"  PATH 'invoiceDetail' PRODUCES APPLICATION_JSON
    WSMETHOD GET taxDetail DESCRIPTION "Detalhe dos tributos por documento" WSSYNTAX "/taxDetail" PATH 'taxDetail' PRODUCES APPLICATION_JSON
    WSMETHOD GET errorMessageApurReinf DESCRIPTION "Detalhe da mensagem de erro retornado durante a apura��o" WSSYNTAX "/errorMessageApurReinf"  PATH 'errorMessageApurReinf' PRODUCES APPLICATION_JSON
    WSMETHOD POST apurReinf DESCRIPTION "Apura��o da API" PRODUCES APPLICATION_JSON

END WSRESTFUL

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Metodo GET
M�todo respons�vel pela consulta ao programa nativo da reinf e montagem da mensagem de resposta para camada THF

@author Henrique Fabiano Pateno Pereira
@since 29/03/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------
WSMETHOD POST eventDetail PATHPARAM sourceBranch, period, event, page, pageSize WSREST  WSTAF002

local aApurac       as array
local aFiliais      as array
local cFilProc 		as character
local cEmpProc 		as character
local cDescription  as character
local cCodErr       as character
local cFuncIPC      as character
local cResponse     as character
local cNrInsc       as character
local cBranch       as character
local cBody         as character
local nX            as numeric
local nTSintetic    as numeric
local nPage         as numeric
local nPageSize     as numeric
local oEstruct      as object
local oBody         as object

aApurac         :=  {}
aFiliais        :=  {}
cFilProc 		:= 	""
cEmpProc 		:= 	""
cResponse       :=  ""
cDescription    :=  ""
cCodErr         :=  ""
cFuncIPC        :=  ""
cNrInsc         :=  ""
cBranch         :=  ""
cBody           :=  self:GetContent()
nX              :=  0
nTSintetic      :=  0
nPage           :=  0
nPageSize       :=  0

//-----------------------------------------------
// inicializa��o vari�veis do tipo object
//-----------------------------------------------
oEstruct 	:= 	JsonObject():New()
oBody       :=  JsonObject():New()

self:SetContentType("application/json")

//-------------------------------------------------------------------
// Valida a existencia da filial no TAF, tabela C1E.		
//-------------------------------------------------------------------
if WSPrepFil(self:sourceBranch,@cFuncIPC ,@cCodErr, @cDescription, @cFilProc, @cEmpProc )

    cEmpProc := cEmpAnt
    cFilProc := cFilAnt

    aFiliais := wsLoadFil()

    oBody:fromJSON( cBody )

    //-------------------------------------------------------------------
    // Consulta a apura��o da reinf conforme evento
    //-------------------------------------------------------------------
    aApurac := TafGetEvt(self:period,self:event,"", aFiliais, 1)

    nX := 0

    oEstruct["eventDetail"] := {}

    if self:event $ "R-1000" .and. !empty(aApurac)

        While (aApurac[1])->(!eof())
            cNrInsc := Posicione("SM0",1,SM0->M0_CODIGO + (aApurac[CONTRIB])->C1E_FILTAF,"M0_CGC")
            cBranch := AllTrim(EncodeUTF8(SM0->M0_NOME))

            aadd(oEstruct["eventDetail"],JsonObject():New())
            nTSintetic := len(oEstruct["eventDetail"])

            //Nome da Filial
            oEstruct["eventDetail"][nTSintetic]["branch"]                       := cBranch

            //Tipo de inscri��o [1] CNPJ [2] CPF
            oEstruct["eventDetail"][nTSintetic]["typeOfInscription"]            := IIf(len(cNrInsc) == 14, "1 - CNPJ", "2 - CPF")

            //CNPJ/CPF contribuinte
            oEstruct["eventDetail"][nTSintetic]["taxNumber"]                    := subStr(cNrInsc, 1, 8)

            //Classifica��o tribut�ria
            oEstruct["eventDetail"][nTSintetic]["taxClassification"]            := (aApurac[CONTRIB])->C1E_CLAFIS

            //Informativo de obrigatoriedade do contribuinte
            oEstruct["eventDetail"][nTSintetic]["isMandatoryBookkeeping"]       := EncodeUTF8(Iif((aApurac[CONTRIB])->C1E_INDESC == "0", "0 - Empresa n�o obrigada a ECD", "1 - Empresa obrigada a ECD"))

            //Indicativo de desonera��o da folha de pagamento
            oEstruct["eventDetail"][nTSintetic]["isPayrollExemption"]           := EncodeUTF8(Iif((aApurac[CONTRIB])->C1E_INDDES == "0", "0 - N�o Aplic�vel", "1 - Empresa enquadrada nos artigos 7� a 9� da Lei 12.546/2011"))

            //Indicativo da exist�ncia de acordo internacional para isen��o de multa
            oEstruct["eventDetail"][nTSintetic]["hasFineExemptionAgreement"]    := Iif((aApurac[CONTRIB])->C1E_ISEMUL == "0", "0 - Sem acordo", "1 - Com acordo")

            //Data de inicio da validade do contribuinte
            oEstruct["eventDetail"][nTSintetic]["beginingDate"]                 := (aApurac[CONTRIB])->C1E_INIPER

            //Data de inicio da validade do contribuinte
            oEstruct["eventDetail"][nTSintetic]["finishingdate"]                := (aApurac[CONTRIB])->C1E_FINPER

            //Contato
            oEstruct["eventDetail"][nTSintetic]["contact"]                      := EncodeUTF8(Iif(TAFColumnPos("C1E_RNOMEC"), (aApurac[CONTRIB])->C1E_RNOMEC, (aApurac[CONTRIB])->C1E_NOMCNT))

            //CPF do contato
            oEstruct["eventDetail"][nTSintetic]["contactTaxNumber"]             := Iif(TAFColumnPos("C1E_RCPFC"), (aApurac[CONTRIB])->C1E_RCPFC, (aApurac[CONTRIB])->C1E_CPFCNT)

            //Status
            oEstruct["eventDetail"][nTSintetic]["status"]                       := Iif(TafRStatEv(self:period,,,self:event,aFiliais,2,,,,,cNrInsc ) $ ' |0|1|', 'notValidated', 'validated')

            //Id de apura��o
            oEstruct["eventDetail"][nTSintetic]["key"]                          := subStr(cNrInsc, 1, 8)

            //Propriedade errors que habilita o icone no frontend
            oEstruct["eventDetail"][nTSintetic]["errors"]                       := 'errors'

            If !Empty(cBody) .and. !cBody == '{}'
                // Chave de busca do erro da apura��o
                oEstruct["eventDetail"][nTSintetic]["keyValidationErrors"]  := KeyError(oEstruct["eventDetail"][nTSintetic], oBody)
            EndIf

            (aApurac[1])->(dbSkip())
        EndDo

    elseIf self:event $ "R-1070" .and. !empty(aApurac)
        WS0021070(aApurac, oEstruct, oBody)

    elseIf self:event $ "R-2010|R-2020" .and. aApurac[4]
        WsPagination(self:page, self:pageSize, @nPage, @nPageSize) //pagination
        (aApurac[SINTETICO])->(DbSetOrder(3))
        if nPage > 1
            (aApurac[SINTETICO])->(DbGoTo(nPage))
        else
            (aApurac[SINTETICO])->(DbGoTop())
        endif

        //-------------------------------------------------------------------
        // monta a mensagem JSON de retorno referente a R-2010 e R-2020
        //-------------------------------------------------------------------
        WS002APRCP(aApurac, oEstruct, self:period, aFiliais, self:event, nPageSize, oBody)

        aApurac[DELETSINT]:Delete()
        aApurac[DELETANAL]:Delete()

    elseIf self:event $ "R-2030|R-2040" .and. !empty(aApurac)
        WS002PRECAD(aApurac, oEstruct, self:event, oBody)

    elseIf self:event $ "R-2050" .and. !empty(aApurac)
        WS0022050(aApurac, oEstruct, oBody)

    elseIf self:event $ "R-2060" .and. aApurac[6]
        WS0022060(aApurac, oEstruct, self:period, oBody)

    elseIf self:event $ "R-3010" .and. !empty(aApurac)
        WS0023010(aApurac, oEstruct, oBody)
    endIf

    cResponse:= FWJsonSerialize( oEstruct, .T., .T.,,.F. )
    self:SetResponse(cResponse )

    FreeObj(oEstruct)

endif

lRet := .T.

return lRet

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Metodo GET
M�todo respons�vel pela consulta ao programa nativo da reinf e montagem da mensagem de resposta para camada THF

@author Henrique Fabiano Pateno Pereira
@since 29/03/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

WSMETHOD GET invoiceDetail QUERYPARAM sourceBranch, period, event, id, branchId, page, pageSize WSREST  WSTAF002
local aApurac       as array
local aFiliais      as array
local aFil          as array
local cFilProc 		as character
local cEmpProc 		as character
local cDescription  as character
local cCodErr       as character
local cFuncIPC      as character
local cResponse     as character
local cId           as character
local cCNPJAtu      as character
local nX            as numeric
local nPage         as numeric
local nPageSize     as numeric

aApurac         :=  {}
aFiliais        :=  {}
aFil            :=  {}
cFilProc 		:= 	""
cEmpProc 		:= 	""
cResponse       :=  ""
cDescription    :=  ""
cCodErr         :=  ""
cFuncIPC        :=  ""
cId             :=  ""
cCNPJAtu        :=  ""
nX              :=  0
nPage           :=  0
nPageSize       :=  0

//-----------------------------------------------
// inicializa��o vari�veis do tipo object
//-----------------------------------------------
oEstruct 	:= 	JsonObject():New()

self:SetContentType("application/json")

//-------------------------------------------------------------------
// Valida a existencia da filial no TAF, tabela C1E.		
//-------------------------------------------------------------------
if WSPrepFil(self:sourceBranch,@cFuncIPC ,@cCodErr, @cDescription, @cFilProc, @cEmpProc )

    cEmpProc := cEmpAnt
    cFilProc := cFilAnt

    aFiliais    := wsLoadFil()
    if !self:event $ "R-1070|R-2060"
        cCNPJAtu    := aFiliais[aScan(aFiliais, {|x| AllTrim(x[3]) == AllTrim(self:branchId) })][6]
        aFil        := RetFilDet(aFiliais, cCNPJAtu, self:event)

        cId         := WsRetId(self:aQueryString)
        //-------------------------------------------------------------------
        // Consulta a apura��o da reinf conforme evento
        //-------------------------------------------------------------------
        aApurac := TafGetEvt(self:period,self:event, cId, aFil, 2)
    else
        If self:event $ "R-2060"
            cId         := WsRetId(self:aQueryString)
        EndIf
        aApurac := TafGetEvt(self:period,self:event, "", aFiliais, 2, "", "", self:id)
    EndIf

    nX := 0

    oEstruct["invoices"] := {}
    If self:event $ "R-1070"
        WS002D1070(aApurac, oEstruct)

    Elseif self:event $ "R-2010|R-2020" .and. !empty(aApurac)
        WsPagination(self:page, self:pageSize, @nPage, @nPageSize) //pagination
        (aApurac[SINTETICO])->(DbSetOrder(4))
        if nPage > 1
            (aApurac[SINTETICO])->(DbGoTo(nPage))
        else
            (aApurac[SINTETICO])->(DbGoTop())
        endif
        //-------------------------------------------------------------------
        // monta a mensagem JSON de retorno referente a R-2010 e R-2020
        //-------------------------------------------------------------------
        Det002APRCP(oEstruct, aApurac, cId)

        aApurac[DELETSINT]:Delete()
        aApurac[DELETANAL]:Delete()

    ElseIf self:event $ "R-2030|R-2040" .and. !empty(aApurac)
        WS002DPRECAD(aApurac, oEstruct, self:event)

    ElseIf self:event $ "R-2050" .and. !empty(aApurac)
        WS002D2050(aApurac, oEstruct)

    ElseIf self:event $ "R-2060" .and. aApurac[6]
        WS002D2060(aApurac, oEstruct, cId)

    ElseIf self:event $ "R-3010" .and. !empty(aApurac)
        WS002D3010(aApurac, oEstruct)
    endif

    cResponse:= FWJsonSerialize( oEstruct, .T., .T.,,.F. )
    self:SetResponse(cResponse )

    FreeObj(oEstruct)

endif

lRet := .T.

return lRet

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Metodo GET
M�todo respons�vel pela consulta dos detalhes dos tributos por documento.

@author Bruno Cremaschi
@since 19/11/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

WSMETHOD GET taxDetail QUERYPARAM sourceBranch, period, event, id, item, branchId, page, pageSize WSREST  WSTAF002
local aApurac       as array
local aFiliais      as array
local aFil          as array
local cFilProc 		as character
local cEmpProc 		as character
local cDescription  as character
local cCodErr       as character
local cFuncIPC      as character
local cResponse     as character
local cCnpjC1H      as character
local cCNPJAtu      as character
local nX            as numeric
local nPage         as numeric
local nPageSize     as numeric

aApurac         :=  {}
aFiliais        :=  {}
aFil            :=  {}
cFilProc 		:= 	""
cEmpProc 		:= 	""
cResponse       :=  ""
cDescription    :=  ""
cCodErr         :=  ""
cFuncIPC        :=  ""
cCnpjC1H        :=  ""
cCNPJAtu        :=  ""
nX              :=  0
nPage           :=  0
nPageSize       :=  0

//-----------------------------------------------
// inicializa��o vari�veis do tipo object
//-----------------------------------------------
oEstruct 	:= 	JsonObject():New()

self:SetContentType("application/json")

//-------------------------------------------------------------------
// Valida a existencia da filial no TAF, tabela C1E.
//-------------------------------------------------------------------
if WSPrepFil(self:sourceBranch,@cFuncIPC ,@cCodErr, @cDescription, @cFilProc, @cEmpProc )

    cEmpProc := cEmpAnt
    cFilProc := cFilAnt

    aFiliais    := wsLoadFil()
    cCNPJAtu    := aFiliais[aScan(aFiliais, {|x| AllTrim(x[3]) == AllTrim(self:branchId) })][6]
    aFil        := RetFilDet(aFiliais, cCNPJAtu, self:event)

    cId         := WsRetId(self:aQueryString)
    //-------------------------------------------------------------------
    // Consulta a apura��o da reinf conforme evento
    //-------------------------------------------------------------------
    aApurac := TafGetEvt(self:period,self:event, cId, aFil, 3, self:id, self:item, "")

    nX := 0

    oEstruct["tax"] := {}

    if self:event $ "R-2030|R-2040" .and. !empty(aApurac)
        WS002SPRECAD(aApurac, oEstruct)

    elseIf self:event $ "R-2050" .and. !empty(aApurac)
        WS002S2050(aApurac, oEstruct)
    endif

    cResponse:= FWJsonSerialize( oEstruct, .T., .T.,,.F. )
    self:SetResponse(cResponse )

    FreeObj(oEstruct)

endif

lRet := .T.

return lRet

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TafGetEvt()
Executa a consulta dos registros na base que devem ser considerados para apura��o

@type  Static Function
@author henrique.pereira
@since 09/04/2019
@param cPerApu periodo para apura��o
@param cEvent evento requisitante da estrutura
@return aAliasQry, array, alias e objeto retornados por Apur1020
/*/
//---------------------------------------------------------------------------------------------------------------

static function TafGetEvt(cPerApu, cEvent, cId, aFiliais, nLevel, cChvNF, cItemNF, cNumProc)

    local cAliasQry as character
    local nX        as numeric
    local aRet      as array
    local aFil      as array
    local aAliasQry as array


    cAliasQry   := ""
    aFil        := {}
    aRet        := {}
    aAliasQry   := {}
    aInfEUF 	:= TamEUF(Upper(AllTrim(SM0->M0_LEIAUTE)))

    dbSelectArea("C1H")

    do case
    case cEvent $ "R-1000"
        aFil := ValidFils( aFiliais )
        cAliasQry := TAFR1000COP( cEvent, substr(cPerApu,3,4)+substr(cPerApu,1,2) , cToD(' / / '), cToD(' / / '), '', aFil, nil, .F., .T.  )
        aAdd(aAliasQry, cAliasQry)

    case cEvent $ "R-1070"
        For nX:=1 To Len(aFiliais)
            aAdd(aFil,{ aFiliais[nX][9] , aFiliais[nX][3] })
        Next nX

        aAliasQry := WSQry1070(aFil, nLevel, cNumProc)

    case cEvent $ "R-2010|R-2020"
        For nX:=1 To Len(aFiliais)
            aAdd(aFil,{ aFiliais[nX][9] , aFiliais[nX][3] })
        Next nX

        aAliasQry := Apur1020(substr(cPerApu,3,4)+substr(cPerApu,1,2), cEvent, aFil, aInfEUF, .t., cId)

    case cEvent $ "R-2030|R-2040"
        For nX:=1 To Len(aFiliais)
            aAdd(aFil,{ aFiliais[nX][9] , aFiliais[nX][3] })
        Next nX

        aAliasQry := WSQryPreCa(substr(cPerApu,3,4)+substr(cPerApu,1,2), aFil, nLevel, cChvNF, cEvent )

    case cEvent $ "R-2050"
        For nX:=1 To Len(aFiliais)
            aAdd(aFil, {    aFiliais[nx][2], ;
                PadR(aFiliais[nx][3], FwSizeFilial(), " "), ;
                aFiliais[nx][4], ;
                aFiliais[nx][5], ;
                aFiliais[nx][6], ;
                aFiliais[nx][7], ;
                TAFGFilMatriz()[6] })
        Next nX

        aAliasQry := WSQry2050(substr(cPerApu,3,4)+substr(cPerApu,1,2), aFil, nLevel, cChvNF, cItemNF)

    case cEvent $ "R-2060"
        For nX:=1 To Len(aFiliais)
            aAdd(aFil,{ aFiliais[nX][9] , aFiliais[nX][3] })
        Next nX

        aAliasQry := Qury2060(substr(cPerApu,3,4)+substr(cPerApu,1,2), .T., .F. , aFil, aInfEUF, {}, cNumProc)

    case cEvent $ "R-3010"
        For nX:=1 To Len(aFiliais)
            aAdd(aFil,{ aFiliais[nX][9] , aFiliais[nX][3] })
        Next nX

        aAliasQry := WSQry3010(substr(cPerApu,3,4)+substr(cPerApu,1,2), aFil, nLevel, cId )
    
    endCase

return aAliasQry


//-------------------------------------------------------------------
/*/{Protheus.doc} TamEUF()
Tamanho da Estrutura SM0 para a empresa, unidade neg�cio e filial

@author totvs
/*/ 
//-------------------------------------------------------------------
Static Function TamEUF(cLayout)

    Local aTam 	As Array
    Local nAte 	As Numeric
    Local nlA 	As Numeric
    Default cLayout := Upper(AllTrim(SM0->M0_LEIAUTE))

    aTam := {0,0,0}
    nAte := Len(cLayout)
    nlA	 := 0

    For nlA := 1 to nAte
        if Upper(substring(cLayout,nlA,1)) == "E"
            ++aTam[1]
        elseif Upper(substring(cLayout,nlA,1)) == "U"
            ++aTam[2]
        elseif Upper(substring(cLayout,nlA ,1)) == "F"
            ++aTam[3]
        endif
    Next nlA

Return aTam

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} metodo POST apurReinf
invoca a apura��o do determinado evento

@author Henrique Fabiano Pateno Pereira
@since 23/04/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------
    WSMETHOD POST apurReinf PATHPARAM sourceBranch, event, period WSREST WSTAF002
    Local aEvents       as array
    Local aFil          as array
    Local aRegRec       as array
    Local aFiliais      as array
    Local aLoadFil      as array
    Local aRetErro      as array
    Local aIdLog        as array
    Local aRegKey       as array
    Local nX            as numeric
    Local nY            as numeric
    Local nConTss       as numeric
    Local cFuncIPC      as character
    Local cCodErr       as character
    Local cDescription  as character
    Local cFilProc      as character
    Local cEmpProc      as character
    Local cIdApReinf    as character
    Local cIdLog        as character
    Local cAliasTmp     as character
    Local cRetProc      as character
    Local cMsgRet       as character
    Local cMsgErro      as character
    Local cBody         as character
    Local cIdTrans      as character
    Local cNumProc      as character
    Local oJsonResp	    as object
    Local oJson         as object
    Local oRegKey       as object
    Local lSucesso      as logical
    Local lValid        as logical
    Local lApi          as logical

//----------------------------------
// Inicializa��o vari�veis array
//----------------------------------
    aEvents     := {}
    aFil        := {}
    aRegRec     := {}
    aFiliais    := {}
    aLoadFil    := {}
    aRetErro    := {}
    aIdLog      := {}
    aRegKey     := {}

//----------------------------------
// Inicializa��o vari�veis string
//----------------------------------
    cFuncIPC        := ''
    cCodErr         := ''
    cDescription    := ''
    cMsgErro        := ''
    cFilProc        := ''
    cEmpProc        := ''
    cIdApReinf      := ''
    cIdLog          := ''
    cAliasTmp       := ''
    cRetProc        := ''
    cMsgRet         := ''
    cBody           := ''
    cIdTrans        := ''
    cNumProc        := ''

//----------------------------------
// Inicializa��o vari�veis booleana
//----------------------------------
    lSucesso        := .F.
    lValid          := Left(GetNewPar( "MV_TAFRVLD", "N" ),1) == "S"
    cBody           := self:GetContent()
    lApi            := .T.

//----------------------------------
// Inicializa��o vari�veis objeto
//----------------------------------
    oJsonResp		:= JsonObject():New()
    oJson           := JsonObject():New()

    nX      := 1
    nY      := 1
    nConTss := 0

    if WSPrepFil(self:sourceBranch,@cFuncIPC ,@cCodErr, @cDescription, @cFilProc, @cEmpProc )
        cEmpProc := cEmpAnt
        cFilProc := cFilAnt

        oJson:fromJSON( cBody )

        aLoadFil := WsLoadFil()

        //--------------------------------------------------------------------
        // Trata a vari�vel cIdTrans concatenando os CNPJs para filtro dos eventos 2010 e 2020 para apura��o
        //--------------------------------------------------------------------
        if self:event == "R-1000"
            aFil := {{TAFGFilMatriz()[6],cFilProc,cFilProc,cEmpProc+cFilProc, "", "",.t.,.t.,.t.}}

            AADD( aIdLog, { cIdApReinf, Alltrim(oJson["customerProviders"][nX]["id"]) } )

            TAFAPR1000( self:event ,self:period,cTOD('01/03/2019') ,cTOD('31/03/2019'), cIdApReinf, aFil, , lValid , .f., @lSucesso, @cMsgRet )

            if !empty(cMsgRet)
                oJsonResp['message'] := EncodeUTF8(cMsgRet)
            endif
        elseif self:event == "R-1070"
            For nX := 1 to Len(oJson["customerProviders"])
                cNumProc := ''
                cNumProc := oJson["customerProviders"][nX]["id"]

                For nY := 1 to Len(aLoadFil)
                    aAdd(aFil,  {   aLoadFil[nY][2],;
                        aLoadFil[nY][3],;
                        aLoadFil[nY][3],;
                        aLoadFil[nY][4],;
                        ""             ,;
                        ""             ,;
                        .t.            ,;
                        .t.            ,;
                        .t.            })
                Next nY
                cIdApReinf := TafXLogIni( cIdApReinf, self:event, self:period )
                AADD( aIdLog, { cIdApReinf, Alltrim(oJson["customerProviders"][nX]["id"]) } )
                TAFAPR1070( self:event, self:period, cTOD('01/03/2019'), cTOD('31/03/2019'), cIdApReinf, aFil, nil, lValid , cNumProc, @lSucesso )
                cIdApReinf := TafXLogFim( cIdApReinf, self:event, self:period )
            Next nX
        elseif self:event $ "R-2010|R-2020"
            For nX:=1 To Len(oJson["customerProviders"])
                If nPos := aScan(aFiliais, {|x| x[1] == oJson["customerProviders"][nX]["branchId"]})
                    aAdd(aFiliais[nPos][2],oJson["customerProviders"][nX]["id"])
                Else
                    aAdd(aFiliais,{oJson["customerProviders"][nX]["branchId"], { oJson["customerProviders"][nX]["id"]}})
                Endif

            Next

            For nX:=1 To Len(aFiliais)
                For nY:=1 To len(aFiliais[nX][2])
                    cIdTrans := ""
                    If nY == len(aFiliais[nX][2])
                        cIdTrans += "'" + aFiliais[nX][2][nY] + "'"
                    Else
                        cIdTrans += "'" + aFiliais[nX][2][nY] + "',"
                    Endif
                    aFil := RetFil(aLoadFil, aFiliais[nX], self:event)
                    cIdApReinf := TafXLogIni( cIdApReinf, self:event, self:period )
                    AADD( aIdLog, { cIdApReinf, oJson["customerProviders"][nX]["branchId"], Alltrim(oJson["customerProviders"][nX]["id"]) } )
                    TAFAPRCP(self:event, self:period, cTOD('01/03/2019'), cTOD('31/03/2019'), cIdApReinf, aFil, nil, lValid, @lSucesso, cIdTrans)
                    TafXLogFim( cIdApReinf, self:event, self:period )
                Next nY
            Next nX
        elseIf self:event $ "R-2030|R-2040"
            For nX:=1 To Len(oJson["customerProviders"])
                If nPos := aScan(aFiliais, {|x| x[1] == oJson["customerProviders"][nX]["branchId"]})
                    aAdd(aFiliais[nPos][2],oJson["customerProviders"][nX]["id"])
                Else
                    aAdd(aFiliais,{oJson["customerProviders"][nX]["branchId"], { oJson["customerProviders"][nX]["id"]}})
                Endif
            Next

            For nX:=1 To Len(aFiliais)
                For nY:=1 To len(aFiliais[nX][2])
                    cIdTrans := ""
                    If nY == len(aFiliais[nX][2])
                        cIdTrans += "'" + aFiliais[nX][2][nY] + "'"
                    Else
                        cIdTrans += "'" + aFiliais[nX][2][nY] + "',"
                    Endif
                    aFil := RetFil(aLoadFil, aFiliais[nX], self:event)

                    cIdApReinf := TafXLogIni( cIdApReinf, self:event, self:period )
                    AADD( aIdLog, { cIdApReinf, oJson["customerProviders"][nX]["branchId"], Alltrim(oJson["customerProviders"][nX]["id"]) } )
                    TAFApRecAD(self:event, self:period, cTOD('01/03/2019'), cTOD('31/03/2019'), cIdApReinf, aFil, nil, lValid, @lSucesso, cIdTrans )
                    cIdApReinf := TafXLogFim( cIdApReinf, self:event, self:period )
                Next nY
            Next nX
        elseIf self:event $ "R-2050"
            For nX:=1 To Len(oJson["customerProviders"])
                If nPos := aScan(aFiliais, {|x| x[1] == oJson["customerProviders"][nX]["branchId"]})
                    aAdd(aFiliais[nPos][2],oJson["customerProviders"][nX]["id"])
                Else
                    aAdd(aFiliais,{oJson["customerProviders"][nX]["branchId"], { oJson["customerProviders"][nX]["id"]}})
                Endif
            Next

            For nX:=1 To Len(aFiliais)
                cIdTrans := ""
                For nY:=1 To len(aFiliais[nX][2])
                    If nY == len(aFiliais[nX][2])
                        cIdTrans += "'" + aFiliais[nX][2][nY] + "'"
                    Else
                        cIdTrans += "'" + aFiliais[nX][2][nY] + "',"
                    Endif
                Next nY

                aFil := RetFil(aLoadFil, aFiliais[nX], self:event)

                cIdApReinf := TafXLogIni( cIdApReinf, self:event, self:period )
                AADD( aIdLog, { cIdApReinf, oJson["customerProviders"][nX]["branchId"], Alltrim(oJson["customerProviders"][nX]["id"]) } )
                TAFAPR2050( "", self:period, cTOD('01/03/2019'), cTOD('31/03/2019'), cIdApReinf, aFil, nil,lValid, @lSucesso)
                cIdApReinf := TafXLogFim( cIdApReinf, self:event, self:period )
            Next nX
        elseIf self:event $ "R-2060"

            For nX:=1 To Len(oJson["customerProviders"])
                If nPos := aScan(aFiliais, {|x| x[1] == oJson["customerProviders"][nX]["branchId"]})
                    aAdd(aFiliais[nPos][2],oJson["customerProviders"][nX]["id"])
                Else
                    aAdd(aFiliais,{oJson["customerProviders"][nX]["branchId"], { oJson["customerProviders"][nX]["id"]}})
                Endif
            Next

            For nX:=1 To Len(aLoadFil)
                aAdd(aFil, {    aLoadFil[nx][2], ;
                    PadR(aLoadFil[nx][3], FwSizeFilial(), " "), ;
                    aLoadFil[nx][4], ;
                    aLoadFil[nx][5], ;
                    aLoadFil[nx][6], ;
                    aLoadFil[nx][7], ;
                    TAFGFilMatriz()[6] })
            Next nX


            For nX:=1 To Len(oJson["customerProviders"])
                cIdTrans := ""

                cIdTrans += "'" + Alltrim(oJson["customerProviders"][nX]["id"]) + "'"

                cIdApReinf := TafXLogIni( cIdLog, self:event, self:period )

                AADD( aIdLog, { cIdApReinf, oJson["customerProviders"][nX]["branchId"], Alltrim(oJson["customerProviders"][nX]["id"]) } )

                TAFAPR2060(self:event , self:period, cTOD('01/03/2019'), cTOD('31/03/2019'), cIdApReinf, aFil, nil, lValid, cIdTrans, @lSucesso)
                TafXLogFim( cIdApReinf, self:event, self:period )
            Next

        elseif self:event == "R-3010"
            For nX:=1 To Len(oJson["customerProviders"])
                If nPos := aScan(aFiliais, {|x| x[1] == oJson["customerProviders"][nX]["branchId"]})
                    aAdd(aFiliais[nPos][2],oJson["customerProviders"][nX]["id"])
                Else
                    aAdd(aFiliais,{oJson["customerProviders"][nX]["branchId"], { oJson["customerProviders"][nX]["id"]}})
                Endif
            Next

            For nX:=1 To Len(aFiliais)
				For nY:=1 To len(aFiliais[nX][2])
					cIdTrans := ""
					cIdTrans += "'" + aFiliais[nX][2][nY] + "'"

					aFil := RetFil(aLoadFil, aFiliais[nX], self:event)

					cIdApReinf := TafXLogIni( cIdApReinf, self:event, self:period )
					AADD( aIdLog, { cIdApReinf, oJson["customerProviders"][nX]["branchId"], Alltrim(oJson["customerProviders"][nX]["id"]) } )
					TAFAPR3010(self:event, self:period, cTOD('01/03/2019'), cTOD('31/03/2019'), cIdApReinf, aFil, nil, @lSucesso, cIdTrans, lApi )
					cIdApReinf := TafXLogFim( cIdApReinf, self:event, self:period )

				Next nY
            Next nX

        elseif self:event $ "R-2099|R-2098"

            aFil := {{TAFGFilMatriz()[6],cFilProc,cFilProc,cEmpProc+cFilProc, "", "",.t.,.t.,.t.}}

            if self:event == "R-2099"
                //cTOD('01/03/2019') ,cTOD('31/03/2019')  s�o passados chumbados. A fun��o TAFAPRCP possui os parametros na assinatura, mas n�o s�o usados para nada.
                cRetProc := TAFAPR2099(self:event ,self:period,cTOD('01/03/2019') ,cTOD('31/03/2019') , cIdApReinf, aFil, nil, .T. )
                if !empty(cRetProc)
                    oJsonResp['message']  := EncodeUTF8(cRetProc)
                endif
            elseif self:event == "R-2098"
                //cTOD('01/03/2019') ,cTOD('31/03/2019')  s�o passados chumbados. A fun��o TAFAPRCP possui os parametros na assinatura, mas n�o s�o usados para nada.
                cRetProc := TAFAPR2098(self:event ,self:period,cTOD('01/03/2019') ,cTOD('31/03/2019') , cIdApReinf, aFil, nil, .T. )
                if !empty(cRetProc)
                    oJsonResp['message']  := EncodeUTF8(cRetProc)
                endif
            endif

            aEvents     := TAFRotinas( self:event ,4,.F.,5)
            //--------------------------------------------------------------------
            // Executa a query dos registros que devem ser transmitidos
            //--------------------------------------------------------------------

            cAliasTmp := WS004Event( aEvents, self:Period)


            varinfo('aEvents', aEvents)


            //--------------------------------------------------------------------
            // Com base na query acima, alimenta o arrai aRegRec para a fun��o TAFProc9Tss execitar a transmiss�o
            //--------------------------------------------------------------------
            aRegRec  := WsTafRecno(cAliasTmp)
            aRetorno := TAFProc9Tss(.T., aEvents, Nil, Nil, Nil, Nil, Nil, @cMsgRet, Nil, Nil, Nil, Nil,Nil, aRegRec,, lApi,@aRetErro)
            nConTss  := AT("N�o foi possivel conectar com o servidor TSS", cMsgRet)

            if len(aRetorno ) == 0 .or. nConTss > 0
                lSucesso    := .f.
                if !empty(cMsgRet)
                    oJsonResp['message'] := EncodeUTF8(cMsgRet)
                endif
            else
                oJsonResp['message'] := EncodeUTF8("Evento transmitido com sucesso.")
                lSucesso    := .t.
            endif
        endif
    endif

    oJsonResp['success']        := lSucesso

    if !self:event $ 'R-2098|R-2099'

        For nX := 1 To Len( aIdLog )

            oRegKey := JsonObject():new()

            if self:event $ 'R-1000|R-1070'
                oRegKey['error']    := aIdLog[nX][1]
                oRegKey['id']       := aIdLog[nX][2]
            else
                oRegKey['error']    := aIdLog[nX][1]
                oRegKey['branchId'] := aIdLog[nX][2]
                oRegKey['id']       := aIdLog[nX][3]
            endif

            AADD(aRegKey, oRegKey)
            FreeObj( oRegKey )
        Next nX

    endIf

    if lSucesso .And. !self:event $ 'R-2098|R-2099'
        oJsonResp['message']        := EncodeUTF8(ErrorMsg(cFilProc, self:event, aIdLog[1][1]))

        if !Empty(oJsonResp['message'])
            oJsonResp['registryKey']    := aRegKey
        endif
    elseif !self:event $ 'R-2098|R-2099'
        cMsgErro := iif(Len(aIdLog) == 1, EncodeUTF8(ErrorMsg(cFilProc, self:event, aIdLog[1][1])), EncodeUTF8('Houve erros na apura��o dos itens selecionados!'))
        oJsonResp['message']        := cMsgErro
        oJsonResp['registryKey']    := aRegKey
    endif

    //-------------------------------------------------------------------
    // Serializa objeto Json
    //-------------------------------------------------------------------
    cJsonResp := FwJsonSerialize( oJsonResp )

    //-------------------------------------------------------------------
    // Seta resposta
    //-------------------------------------------------------------------
    Self:SetResponse( cJsonResp )

    //-------------------------------------------------------------------
    // Elimina objeto da memoria
    //-------------------------------------------------------------------
    FreeObj( oJsonResp )

return .t.

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} RetFil
Fun��o para retornar a filial do registro selecionado para apura��o

@author Bruno Cremaschi
@since 29/10/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------
Static Function RetFil(aLoadFil, aFilial, cEvent)

    local cCNPJAtu  as character
    local nX        as numeric
    local aRet      as array
    local lDifCNPJ  as logical

    cCNPJAtu    := ""
    nX          := 0
    aRet        := {}
    lDifCNPJ    := .F.

    If !cEvent $ "R-2050"
        For nX := 1 to Len(aLoadFil)
            If aLoadFil[nX][6] == FWSM0Util():GetSM0Data( cEmpAnt , aFilial[1] , { "M0_CGC" } )[1][2]
                aAdd(aRet, {    TAFGFilMatriz()[6], ;
                    aLoadFil[nx][3], ;
                    aLoadFil[nx][3], ;
                    aLoadFil[nx][4], ;
                    "", ;
                    "", ;
                    .T., ;
                    .T., ;
                    .T., })
            EndIf
        next nX
    Else
        cCNPJAtu := aLoadFil[aScan(aLoadFil, {|x| AllTrim(x[3]) == AllTrim(aFilial[1]) })][6]

        For nX := 1 to Len(aLoadFil)
            If AllTrim(aLoadFil[nx][6]) <> cCNPJAtu
                lDifCNPJ := .T.
            EndIf
        Next nX

        If lDifCNPJ
            For nX := 1 to Len(aLoadFil)
                If aLoadFil[nX][6] == FWSM0Util():GetSM0Data( cEmpAnt , aFilial[1] , { "M0_CGC" } )[1][2]
                    aAdd(aRet, {    aLoadFil[nx][2], ;
                        PadR(aLoadFil[nx][3], FwSizeFilial(), " "), ;
                        aLoadFil[nx][4], ;
                        aLoadFil[nx][5], ;
                        aLoadFil[nx][6], ;
                        aLoadFil[nx][7], ;
                        TAFGFilMatriz()[6] })
                EndIf
            Next nX
        Else
            For nX := 1 to Len(aLoadFil)
                aAdd(aRet, {    aLoadFil[nx][2], ;
                    PadR(aLoadFil[nx][3], FwSizeFilial(), " "), ;
                    aLoadFil[nx][4], ;
                    aLoadFil[nx][5], ;
                    aLoadFil[nx][6], ;
                    aLoadFil[nx][7], ;
                    TAFGFilMatriz()[6] })
            Next nX
        EndIf
    EndIf

Return aRet

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} RetFilDet
Fun��o para retornar a filial do registro posicionado, para apresenta��o dos documentos

@author Bruno Cremaschi
@since 30/10/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------
Static Function RetFilDet(aFiliais, cCNPJFil, cEvent)

    local nX        as numeric
    local aRet      as array
    local lDifCNPJ  as logical

    nX          := 0
    aRet        := {}
    lDifCNPJ    := .F.

    If cEvent <> "R-2050"
        For nX := 1 to Len(aFiliais)
            If AllTrim(aFiliais[nx][6]) == AllTrim(cCNPJFil)
                aAdd(aRet, aFiliais[nx])
            EndIf
        Next nX
    Else
        For nX := 1 to Len(aFiliais)
            If AllTrim(aFiliais[nx][6]) <> cCNPJFil
                lDifCNPJ := .T.
            EndIf
        Next nX

        If lDifCNPJ
            For nX := 1 to Len(aFiliais)
                If AllTrim(aFiliais[nx][6]) == AllTrim(cCNPJFil)
                    aAdd(aRet, aFiliais[nx])
                EndIf
            Next nX
        Else
            aRet := aFiliais
        EndIf
    EndIf

Return aRet

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Metodo GET
M�todo respons�vel pela consulta detalhada do erro ocorrido durante a apura��o do evento

@author Leticia Campos
@since 27/07/2020
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------
    WSMETHOD GET errorMessageApurReinf QUERYPARAM sourceBranch, event, D WSREST WSTAF002

    local cEmpProc 		as character
    local cFilProc 		as character
    local cJsonResp     as character
    local cFuncIPC      as character
    local cCodErr       as character
    local cDescription  as character
    local oJsonResp 	as object

    cEmpProc            := ''
    cFilProc            := ''
    cJsonResp           := ''
    cFuncIPC            := ''
    cCodErr             := ''
    cDescription        := ''
    oJsonResp 	        := 	JsonObject():New()

    ::SetContentType("application/json")

    if WSPrepFil(::sourceBranch, @cFuncIPC ,@cCodErr, @cDescription, @cFilProc, @cEmpProc )

        cEmpProc := cEmpAnt
        cFilProc := cFilAnt

        oJsonResp['message'] := EncodeUTF8(ErrorMsg(cFilProc, ::event, ::id))

        // Serializa objeto Json
        cJsonResp := FwJsonSerialize( oJsonResp )

        // Seta resposta
        ::SetResponse( cJsonResp )

        // Elimina objeto da memoria
        FreeObj( oJsonResp )

    endif

return .T.

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ErrorMsg
Fun��o que retorna a mensagem de erro ocorrida na apura��o do evento

@author Leticia Campos
@since 27/07/2020
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------
Static Function ErrorMsg(cBranch, cEvent, cIdLog)

    Local cAlias        := GetNextAlias()
    Local cMessage      := ''

    Default cIdLog      := ''
    Default cBranch     := ''
    Default cEvent      := ''

    BeginSql Alias cAlias
    SELECT
        V0K_TIPO TIPO, V0K_INFO INFO 
    FROM 
        %table:V0K%
    WHERE 
        V0K_FILIAL = %Exp:cBranch% 
        AND V0K_EVENTO = %Exp:cEvent% 
        AND V0K_TIPO NOT IN ('INICIO', 'FIM', 'MSG')
        AND V0K_PROCID = %Exp:cIdLog%
    EndSql

    cMessage := (cAlias)->INFO

Return ( cMessage )

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} KeyError
Fun��o respons�vel por retornar o procid da tabela de log que cont�m o motivo do erro da apura��o

@author Leticia Campos
@since 30/07/2020
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

Static Function KeyError(oEstruct, oBody)
    local cKeyError as character
    local nX        as numeric

    cKeyError       := ""
    nX              := 1

    For nX := 1 to Len(oBody["registryKey"])
        if alltrim(oBody["registryKey"][nX]["id"]) == alltrim(oEstruct["key"])
            cKeyError := oBody["registryKey"][nX]["error"]
        endif
    Next nX

return ( cKeyError )
