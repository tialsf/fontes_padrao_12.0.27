#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"

#DEFINE CHILD1 1,1
#DEFINE CHILD2 1,2

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} WSTAF004
WS para retorno das informa��es referente ao detalhe das movimenta��es da base

@author Henrique Fabiano Pateno Pereira
@since 29/03/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

WSRESTFUL WSTAF004 DESCRIPTION "API Reinf - Monitor de eventos"

    WSDATA sourceBranch			AS STRING
    WSDATA period 				AS STRING
    WSDATA event 				AS STRING
    WSDATA page					AS INTEGER	OPTIONAL
    WSDATA pageSize 			AS INTEGER	OPTIONAL
    WSDATA key                  AS STRING
    WSDATA item                 AS STRING
    WSDATA id                   AS STRING
    WSDATA customerProviders    AS STRING

    WSMETHOD GET eventDetail DESCRIPTION "Detalhe do evento vis�o de transmiss�o" WSSYNTAX "/eventDetail"  PATH 'eventDetail' PRODUCES APPLICATION_JSON
    WSMETHOD GET invoiceDetail DESCRIPTION "Detalhe dos eventos a transmistir " WSSYNTAX "/invoiceDetail"  PATH 'invoiceDetail' PRODUCES APPLICATION_JSON
    WSMETHOD GET taxDetail DESCRIPTION "Detalhe dos recursos recebidos" WSSYNTAX "/taxDetail" PATH 'taxDetail' PRODUCES APPLICATION_JSON
    WSMETHOD POST transmitionReinf DESCRIPTION "Transmiss�o" PRODUCES APPLICATION_JSON

END WSRESTFUL

WSMETHOD GET eventDetail QUERYPARAM sourceBranch, period, event, page, pageSize WSREST  WSTAF004
local cFuncIPC      as character
local cCodErr       as character
local cDescription  as character
local cFilProc      as character
local cEmpProc      as character
local cAliasTmp     as character
local cFilName      as character
local cPeriodo      as character
local cBranch       as character
local cAlsfilho     as character
local cTpProc       as character
local nTpTab        as numeric
local nTAnalitc     as numeric
local nTAnalVal     as numeric
local aFindFil      as array
local lOper         as logical
local lObra         as logical
local oDetMonit     as object
local oTmpTab       as object

cFuncIPC        := ''
cCodErr         := ''
cDescription    := ''
cFilProc        := ''
cEmpProc        := ''
cBranch         := ''
cAliasTmp       := ''
cFilName        := ''
cEvento         := self:event
cPeriodo        := self:period
cAlsfilho       := ''
cTpProc         := ''
aFindFil        := {}
lOper           := .t.
lObra           := .f.
nTpTab          := GetEvenTyp(cEvento)
nTAnalitc       := 0
nTAnalVal       := 0
oTmpTab         := nil

//-----------------------------------------------
// inicializa��o vari�veis do tipo object
//-----------------------------------------------
oDetMonit 	:= 	JsonObject():New()

//-------------------------------------------------------------------
// Valida a exist�ncia da filial no TAF, tabela C1E.
//-------------------------------------------------------------------
if WSPrepFil(self:sourceBranch,@cFuncIPC ,@cCodErr, @cDescription, @cFilProc, @cEmpProc )

    aEvents     := TAFRotinas( cEvento ,4,.F.,5)
    cAlsChild   := AlsChld(cEvento)
    aAlsAuxCh   := AlsAuxCh(cEvento)

    If cEvento == "R-1070"
        cAlsfilho := "C07"
    ElseIf cEvento == "R-2020"
        cAlsfilho := "C1H"
    ElseIf cEvento == "R-2030"
        cAlsfilho := "V1G"
    ElseIf cEvento == "R-2040"
        cAlsfilho := "V1J"
    Else
        cAlsfilho := ""
    EndIf

    cAliasTmp   := WS004Event( aEvents, cPeriodo, cAlsfilho)

    oDetMonit["eventDetail"] := {}

    While !(cAliasTmp)->(eof())
        aadd(oDetMonit["eventDetail"],JsonObject():New())
        nTAnalitc := len(oDetMonit["eventDetail"])
        If (cEvento $ "R-1000")

            cBranch :=  Posicione("SM0",1,SM0->M0_CODIGO + cFilAnt,"M0_FILIAL")

            // Filial
            oDetMonit["eventDetail"][nTAnalitc]["branch"]                               := EncodeUTF8(cBranch)

            //Tipo de inscri��o [1] CNPJ [2] CPF
            oDetMonit["eventDetail"][nTAnalitc]["typeOfInscription"]                    := IIf(len(AllTrim((cAliasTmp)->CNPJ)) == 14, "1 - CNPJ", "2 - CPF")

            //CNPJ/CPF contribuinte
            oDetMonit["eventDetail"][nTAnalitc]["taxNumber"]                            := subStr((cAliasTmp)->CNPJ, 1, 8)

            //Classifica��o tribut�ria
            oDetMonit["eventDetail"][nTAnalitc]["taxClassification"]                    := (cAliasTmp)->CLAFIS

            //Informativo de obrigatoriedade do contribuinte
            oDetMonit["eventDetail"][nTAnalitc]["isMandatoryBookkeeping"]               := EncodeUTF8(Iif((cAliasTmp)->INDESC == "0", "0 - Empresa n�o obrigada a ECD", "1 - Empresa obrigada a ECD"))

            //Indicativo de desonera��o da folha de pagamento
            oDetMonit["eventDetail"][nTAnalitc]["isPayrollExemption"]                   := EncodeUTF8(Iif((cAliasTmp)->INDDES == "0", "0 - N�o Aplic�vel", "1 - Empresa enquadrada nos artigos 7� a 9� da Lei 12.546/2011"))

            //Indicativo da exist�ncia de acordo internacional para isen��o de multa
            oDetMonit["eventDetail"][nTAnalitc]["hasFineExemptionAgreement"]            := Iif((cAliasTmp)->ISEMUL == "0", "0 - Sem acordo", "1 - Com acordo")

            //Data de inicio da validade do contribuinte
            oDetMonit["eventDetail"][nTAnalitc]["beginingDate"]                         := (cAliasTmp)->INIPER

            //Data de inicio da validade do contribuinte
            oDetMonit["eventDetail"][nTAnalitc]["finishingdate"]                        := (cAliasTmp)->FINPER

            //Contato
            oDetMonit["eventDetail"][nTAnalitc]["contact"]                              := (cAliasTmp)->CONTATO

            //CPF do contato
            oDetMonit["eventDetail"][nTAnalitc]["contactTaxNumber"]                     := (cAliasTmp)->CPFCTT

        ElseIf (cEvento $ "R-1070")
            If (cAliasTmp)->TPPROC == "1"
                cTpProc := "Administrativo"
            ElseIf (cAliasTmp)->TPPROC == "2"
                cTpProc := "Judicial"
            ElseIf (cAliasTmp)->TPPROC == "3"
                cTpProc := "N�mero do benef�cio (NB) do INSS"
            ElseIf (cAliasTmp)->TPPROC == "4"
                cTpProc := "Processo FAP"
            EndIf

            // Tipo de processo
            oDetMonit["eventDetail"][nTAnalitc]["proccesType"]          := alltrim(EncodeUTF8(cTpProc))

            // RN�mero do processo
            oDetMonit["eventDetail"][nTAnalitc]["proccesNumber"]        := (cAliasTmp)->NUMPROC

            // UF Vara
            oDetMonit["eventDetail"][nTAnalitc]["courtFederatedUnit"]   := alltrim(EncodeUTF8((cAliasTmp)->DESCUFVARA))

            // Munic�pio
            oDetMonit["eventDetail"][nTAnalitc]["cityCode"]             := (cAliasTmp)->CODMUN + " - " + alltrim(EncodeUTF8((cAliasTmp)->DESCMUN))

            //C�digo munic�pio
            oDetMonit["eventDetail"][nTAnalitc]["courtId"]              := (cAliasTmp)->IDMUN

            // Data ini
            oDetMonit["eventDetail"][nTAnalitc]["beginingDate"]   	    := (cAliasTmp)->DTINI

            // Data fim
            oDetMonit["eventDetail"][nTAnalitc]["finishingDate"]   	    := (cAliasTmp)->DTFIM

            // Status
            oDetMonit["eventDetail"][nTAnalitc]["status"]   	        := Iif((cAliasTmp)->STATUS $ ' ', 'notValidated', 'validated')

            //Id de apura��o
            oDetMonit["eventDetail"][nTAnalitc]["key"]                  := (cAliasTmp)->NUMPROC

        ElseIf (cEvento $ "R-2010|R-2020")
            lObra   :=  !empty((cAliasTmp)->DOBRA)

            cBranch :=  Posicione("SM0",1,SM0->M0_CODIGO + (cAliasTmp)->FILIAL,"M0_FILIAL")

            // Filial
            oDetMonit["eventDetail"][nTAnalitc]["branch"]                               := EncodeUTF8(cBranch)

            // Filial
            oDetMonit["eventDetail"][nTAnalitc]["branchTaxNumber"]                      := AllTrim((cAliasTmp)->NRINSC)

            //Cnpj
            oDetMonit["eventDetail"][nTAnalitc]["taxNumber"]                            := alltrim(iif( lObra, AllTrim(SubStr((cAliasTmp)->CNO, 2 )), AllTrim((cAliasTmp)->CNPJ)))

            //Descri��o participante C1H
            oDetMonit["eventDetail"][nTAnalitc]["company"]                              := alltrim(iif( lObra,(cAliasTmp)->DOBRA, (cAliasTmp)->DPARTI))

            // Quantidade de notas/faturas
            oDetMonit["eventDetail"][nTAnalitc]["totalInvoice"]                         := Ws004Docs((cAliasTmp)->ID, cAlsChild, aAlsAuxCh, .t., cEvento)

            // ID do registro
            oDetMonit["eventDetail"][nTAnalitc]["key"]                                   := (cAliasTmp)->ID

            // Valor bruto
            oDetMonit["eventDetail"][nTAnalitc]["totalGrossValue"]                      := (cAliasTmp)->BRUTO

            // valor total da base de c�lculo
            oDetMonit["eventDetail"][nTAnalitc]["totalTaxBase"]                         := (cAliasTmp)->VLRBASE

            // valor total
            oDetMonit["eventDetail"][nTAnalitc]["totalTaxes"]                           := (cAliasTmp)->VALOR

        ElseIf (cEvento $ "R-2030|R-2040")

            // filial
            oDetMonit["eventDetail"][nTAnalitc]["branchId"]                                     := (cAliasTmp)->FILIAL

            // n�mero de inscri��o do estabelecimento
            oDetMonit["eventDetail"][nTAnalitc]["taxNumber"]                                    := (cAliasTmp)->NRINSC

            // valor bruto dos recursos recebidos
            oDetMonit["eventDetail"][nTAnalitc]["totalGrossValue"]                              := (cAliasTmp)->VLREPA

            // valor total da reten��o
            oDetMonit["eventDetail"][nTAnalitc]["totalReceivedWithholdAmount"]                  := (cAliasTmp)->VLRET

            // valor total da reten��o previd�nci�ria com exigibilidade suspensa
            oDetMonit["eventDetail"][nTAnalitc]["totalValueOfRetentionWithSuspendedLiability"]  := (cAliasTmp)->VLNRET

            // ID do registro
            oDetMonit["eventDetail"][nTAnalitc]["key"]                                          := (cAliasTmp)->ID

        ElseIf (cEvento $ "R-2050")
            aFindFil := WsFindFil((cAliasTmp)->NRINSC)

            if (len(aFindFil) > 0)
                cFilName := aFindFil[17]
            EndIf

            //ID do registro
            oDetMonit["eventDetail"][nTAnalitc]["key"]                                   := (cAliasTmp)->ID

            //Raz�o social
            oDetMonit["eventDetail"][nTAnalitc]["company"]                              := EncodeUTF8(AllTrim(cFilName))

            //N�mero da inscri��o
            oDetMonit["eventDetail"][nTAnalitc]["taxNumber"]                            := (cAliasTmp)->NRINSC

            //Valor da receita bruta
            oDetMonit["eventDetail"][nTAnalitc]["totalGrossValue"]                      := (cAliasTmp)->BRUTO

            //Valor da contribui��o previdenci�ria
            oDetMonit["eventDetail"][nTAnalitc]["sociaSecurityContributionValue"]       := (cAliasTmp)->VLRCONTR

            //Valor da contribui��o previdenci�ria GILRAT
            oDetMonit["eventDetail"][nTAnalitc]["sociaSecurityContributionValueGilrat"] := (cAliasTmp)->GILRAT

            //Valor da contribui��o previdenci�ria SENAR
            oDetMonit["eventDetail"][nTAnalitc]["sociaSecurityContributionValueSenar"]  := (cAliasTmp)->SENAR

        ElseIf (cEvento $ "R-2060")

            //Filial
            oDetMonit["eventDetail"][nTAnalitc]["branch"]                                   := alltrim((cAliasTmp)->FILIAL)

            //Tipo de inscri��o do estabelecimento
            oDetMonit["eventDetail"][nTAnalitc]["typeOfInscription"]                        := (cAliasTmp)->CTPINSC + Iif((cAliasTmp)->CTPINSC == '1', " - CNPJ", " - CNO")

            //N�mero de inscri��o do estabelecimento
            oDetMonit["eventDetail"][nTAnalitc]["companyTaxNumber"]                         := alltrim((cAliasTmp)->CNRINSC)

            // Quantidade de registro de atividade
            oDetMonit["eventDetail"][nTAnalitc]["totalInvoice"]                             := Ws004Docs((cAliasTmp)->ID, cAlsChild, aAlsAuxCh, .t., cEvento)

            //Valor da receita bruta total
            oDetMonit["eventDetail"][nTAnalitc]["totalGrossValue"]                          := (cAliasTmp)->VLRECBTT

            //Valor total da contribui��o previdenci�ria sobre a receita bruta
            oDetMonit["eventDetail"][nTAnalitc]['sociaSecurityContributionValue']           := (cAliasTmp)->VLCPAPUT

            //Valor total da contribui��o previdenci�ria com exigibilidade suspensa
            oDetMonit["eventDetail"][nTAnalitc]['sociaSecurityContributionValueSuspended']  := (cAliasTmp)->VLCSUSPT

            //Id de transmiss�o
            oDetMonit["eventDetail"][nTAnalitc]["key"]                                      := (cAliasTmp)->ID

        EndIf

        // Transmitidos? Status diferente de branco, zero e 1
        oDetMonit["eventDetail"][nTAnalitc]["status"]                                   := iif((cAliasTmp)->STATUS $ ' |0|1|', "notTransmitted","transmitted")

        (cAliasTmp)->(dbskip())
    Enddo

    cResponse:= FWJsonSerialize( oDetMonit, .T., .T.,,.F. )

    self:SetResponse(cResponse )

    FreeObj(oDetMonit)

endif

lRet := .T.

return lRet

//---------------------------------------------------------------------
/*/{Protheus.doc} WS004Event

Executa a consulta aos eventos pass�veis de transmiss�o 

@Author		henrique.pereira
@Since		25/04/2019
@Version	1.0
/*/
//---------------------------------------------------------------------
Function WS004Event( aEvents, cPerIni, cAlsfilho, cIdTrans)
    local cSelect   as character
    local cFrom     as character
    local cwhere    as character
    local cAlias    as character
    local cLayout   as character
    local cNrInsc   as character
    local aSM0      as array
    local nMatriz   as numeric

    cSelect		:=	""
    cFrom       :=  ""
    cwhere      :=  ""
    cAlias      :=  getnextalias()
    cLayout     :=  ""
    cNrInsc     :=  ""
    aSM0		:=  {}
    nMatriz     :=  0

    default     cPerIni     :=  ''
    default     cIdTrans    := ''
    default     lNotProtoc  := .t. // filtra APENAS os n�o protocolados

    cAliasLay := aEvents[3] //Alias do Evento
    cLayout   := aEvents[4] //Layout

    if AllTrim( cLayout ) == "R-1000"
        aSM0 := WsLoadFil()
        nMatriz := aScan(aSM0,{|x| x[8] })
        cNrInsc := aSM0[nMatriz][6]
    endif

    cSelect += "' " + cLayout   + "' EVENTO "
    cSelect += " ," + cAliasLay + "." + cAliasLay + "_FILIAL FILIAL "
    cSelect += " ," + cAliasLay + "." + cAliasLay + "_ID ID "
    cSelect += " ," + cAliasLay + "." + cAliasLay + "_STATUS STATUS "

    If AllTrim( cLayout ) == "R-2010"
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_CNPJPR  CNPJ "
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_DPARTI  DPARTI "
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_TPNUOB  CNO "   // Inscri��o da Obra
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_DOBRA   DOBRA " // descri��o da Obra
    EndIf

    If AllTrim( cLayout ) == "R-2020"
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_CNPJPR  CNPJ "
        cSelect += " ,  C1H.C1H_NOME AS  DPARTI "
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_TPNUOB  CNO "   // Inscri��o da Obra
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_DOBRA   DOBRA " // descri��o da Obra
    EndIf

    If AllTrim( cLayout ) $ "R-2010|R-2020"
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_NRINSC  NRINSC "
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_VLRPRI  VALOR "
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_VLRBRU  BRUTO "
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_VLRBRE  VLRBASE "
        cSelect += " ," + cAliasLay + "." + cAliasLay + "_PERAPU PERAPU "
    EndIf

    If AllTrim( cLayout ) $ "R-2030|R-2040"
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_NRINSC NRINSC"   // n�mero de inscri��o do estabelecimento
        cSelect += " , SUM(" + cAlsfilho + "." + cAlsfilho + "_VLREPA) VLREPA"   // valor total recebido
        cSelect += " , SUM(" + cAlsfilho + "." + cAlsfilho + "_VLRET)  VLRET"    // valor total da reten��o
        cSelect += " , SUM(" + cAlsfilho + "." + cAlsfilho + "_VLNRET) VLNRET"   // valor total da reten��o com exibilidade suspensa
    endif

    If AllTrim( cLayout ) $ "R-2060"
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_FILIAL FILIAL "  // filial
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_TPINSC CTPINSC"   // tipo de inscri��o
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_NRINSC CNRINSC"   // n�mero de inscri��o
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_VLRBRT VLRECBTT"  // valor da receita bruta
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_VLCPAP VLCPAPUT"  // valor da CPBR
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_VLSCPR VLCSUSPT"  // valor da CPBR com exibilidade suspensa
    endif

    If AllTrim( cLayout ) $ "R-2050"
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_TPINSC  TPINSC "    // tipo de inscri��o
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_NRINSC  NRINSC "    // n�mero da inscri��o
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_VRECBT  BRUTO "     // valor da receita bruta
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_VCPAPU  VLRCONTR "  // valor da contribui��o previdenci�ria
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_VRAAPU  GILRAT "    // valor da contribui��o previdenci�ria GILRAT
        cSelect += " ,  " + cAliasLay + "." + cAliasLay + "_VSEAPU  SENAR "     // valor da contribui��o previdenci�ria SENAR
    EndIf

    If AllTrim( cLayout ) $ "R-1070"
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_TPPROC TPPROC "      // tipo de processo
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_NUMPRO NUMPROC "     // n�mero do processo
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_DSUFVA DESCUFVARA "  // uf da vara
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_IDMUNI IDMUN "       // id do municipio
        cSelect += ", " + cAlsfilho + "." + cAlsfilho + "_CODIGO CODMUN "      // c�digo do municipio
        cSelect += ", " + cAlsfilho + "." + cAlsfilho + "_DESCRI DESCMUN "     // descri��o do municipio
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_IDVARA IDVARA "      // id da vara
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_DTINI  DTINI "       // data de inicio
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_DTFIN  DTFIM "       // data de fim
    EndIf

    If AllTrim( cLayout ) $ "R-1000"
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_NRINSC  CNPJ "
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_CLAFIS  CLAFIS "
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_INDECD  INDESC "
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_IDCPRB  INDDES "
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_INDACR  ISEMUL "
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_DTINI   INIPER "
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_DTFIN   FINPER "
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_NOMCTT  CONTATO "
        cSelect += ", " + cAliasLay + "." + cAliasLay + "_CPFCTT  CPFCTT "
    EndIf

    cSelect += " ," + cAliasLay + ".R_E_C_N_O_ RECNO "
    // FROM
    cFrom += RetSqlName( cAliasLay ) + " " + cAliasLay + " "

    if AllTrim( cLayout ) $ "R-2020"
        cFrom += " LEFT JOIN " +  RetSqlName( cAlsfilho  ) + " " + cAlsfilho  + " ON "
        cFrom += cAlsfilho + "." + cAlsfilho + "_CNPJ = " + cAliasLay + "." + cAliasLay + "_NRINST "
        cFrom += " AND " + cAlsfilho + "." + cAlsfilho + "_FILIAL = '"   + xFilial(cAlsfilho) + "'"
        cFrom += " AND " + cAlsfilho + ".D_E_L_E_T_ = ' ' "
    elseif AllTrim( cLayout ) $ "R-1070"
        cFrom += " LEFT JOIN " +  RetSqlName( cAlsfilho  ) + " " + cAlsfilho  + " ON "
        cFrom += cAlsfilho + "." + cAlsfilho + "_ID = " + cAliasLay + "." + cAliasLay + "_IDMUNI "
        cFrom += " AND " + cAlsfilho + ".D_E_L_E_T_ = ' ' "
    elseif AllTrim( cLayout ) $ "R-2030|R-2040"
        cFrom += " LEFT JOIN " + RetSqlName( cAlsfilho ) + " " + cAlsfilho + " ON "
        cFrom += cAlsfilho + "." + cAlsfilho + "_ID =" + cAliasLay + "." + cAliasLay + "_ID "
        cFrom += " AND " + cAlsfilho + ".D_E_L_E_T_ = ' ' "
    endif

    cWhere += cAliasLay + "." + cAliasLay + "_FILIAL = '" + xFilial(cAliasLay) + "'"
    cWhere += "  AND " + cAliasLay + ".D_E_L_E_T_ = ' ' "
    cWhere += "  AND " + cAliasLay + "." + cAliasLay + "_ATIVO = '1' "

    If AllTrim( cLayout ) $ "R-2010|R-2020|R-2030|R-2040|R-2050|R-2060"
        cWhere += " AND " + cAliasLay + "." + cAliasLay + "_PERAPU = '" + STRTRAN(cPerIni,"/","") + "'"
        if !empty(cIdTrans)
            cWhere += " AND " + cAliasLay + "." + cAliasLay + "_ID IN (" + cIdTrans +")"
        endif
        if AllTrim( cLayout ) $ "R-2030|R-2040"
            cWhere += " GROUP BY " + cAliasLay + "." + cAliasLay + "_FILIAL "
            cWhere += ", " + cAliasLay + "." + cAliasLay + "_STATUS "
            cWhere += ", " + cAliasLay + "." + cAliasLay + "_NRINSC "
            cWhere += ", " + cAliasLay + "." + cAliasLay + "_ID "
            cWhere += ", " + cAliasLay + ".R_E_C_N_O_ "
        endif
    elseIf AllTrim( cLayout ) == "R-1070" .And. !empty(cIdTrans)
        cWhere += " AND " + cAliasLay + "." + cAliasLay + "_NUMPRO IN (" + cIdTrans +")"
    EndIf

    if AllTrim( cLayout ) $ "R-1000" .and. !empty(cNrInsc)
        cWhere += " AND " + cAliasLay + "." + cAliasLay + "_NRINSC = '" + cNrInsc + " '"
    endIf

    cSelect :=  "%" +   cSelect +   "%"
    cFrom   :=  "%" +   cFrom   +   "%"
    cWhere  :=  "%" +   cWhere  +   "%"

    BeginSql Alias cAlias
        SELECT DISTINCT
        %Exp:cSelect% 
        FROM 
        %Exp:cFrom%
        WHERE
        %Exp:cWhere%
    EndSql

Return(cAlias)

//---------------------------------------------------------------------
/*/{Protheus.doc} Ws004Docs

Devolve a quantidade de documentos dos eventos apurados do tipo: pendentes de transmiss�o
                                    Ou
Se lTotal = .f., retorna o detalhamento dos documentos a serem transmitidos

@Author		henrique.pereira
@Since		25/04/2019
@Version	1.0
/*/
//---------------------------------------------------------------------
static function Ws004Docs(cID, cAlsChild, aAlsAuxCh, lTotal, cEvento)

    local cSelect   as character
    local cFrom     as character
    local cWhere    as character
    local cAlsBegin as character
    local nTotal    as numeric

    default nTotal  := 0
    default lTotal  := .f.
    default cEvento := ''

    cSelect     :=  ''
    cFrom       :=  ''
    cWhere      :=  ''
    cAlsBegin   :=  getnextalias()
    nTotal      :=  0

    If lTotal
        cSelect :=  "COUNT(*) AS TOTAL"

        cFrom   :=  RetSqlName(cAlsChild) + " " + cAlsChild
    ElseIf cEvento $ "R-1070"
        cSelect += aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_CODSUS AS CODSUSP,"
        cSelect += aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_DTDEC AS DECISIONDATE,"
        cSelect += aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_INDDEP AS DEPINDICATOR,"
        cSelect += aAlsAuxCh[CHILD2] + "." + aAlsAuxCh[CHILD2] + "_DESCRI AS INDICSUSP"

        cFrom   :=  RetSqlName(cAlsChild) + " " + cAlsChild

        cFrom  += " INNER JOIN " + RetSqlName(aAlsAuxCh[CHILD1]) + " " + aAlsAuxCh[CHILD1] + " ON "
        cFrom  += aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_FILIAL = " + cAlsChild +'.'+cAlsChild + "_FILIAL"
        cFrom  += " AND " + aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_ID = " + cAlsChild +'.'+cAlsChild + "_ID"
        cFrom  += " AND " + aAlsAuxCh[CHILD1] + ".D_E_L_E_T_ = ' '"

        cFrom  += " INNER JOIN " + RetSqlName(aAlsAuxCh[CHILD2]) + " " + aAlsAuxCh[CHILD2] + " ON "
        cFrom  += aAlsAuxCh[CHILD2] + "." + aAlsAuxCh[CHILD2] + "_FILIAL = '" + xFilial(aAlsAuxCh[CHILD2]) + "'"
        cFrom  += " AND " + aAlsAuxCh[CHILD2] + "." + aAlsAuxCh[CHILD2] + "_ID = " + cAlsChild +'.'+cAlsChild + "_ID"
        cFrom  += " AND " + aAlsAuxCh[CHILD2] + ".D_E_L_E_T_ = ' '"
    ElseIf cEvento $ "R-2010|R-2020"
        cSelect := cAlsChild +'.'+cAlsChild + "_FILIAL  AS FILIAL, "
        cSelect += cAlsChild +'.'+cAlsChild + "_NUMFAT  AS NUMFAT, "
        cSelect += cAlsChild +'.'+cAlsChild + "_SERIE   AS SERIE, "
        cSelect += cAlsChild +'.'+cAlsChild + "_NUMDOC  AS NUMDOC, "
        cSelect += cAlsChild +'.'+cAlsChild + "_DTEMIS  AS DATAEMISSAO, "

        if cEvento == 'R-2010'
            cSelect += cAlsChild +'.'+cAlsChild + "_VLBRUT  AS VALORBRUTO, "

        elseif cEvento == 'R-2020
            cSelect += cAlsChild +'.'+cAlsChild + "_VLRBRU  AS VALORBRUTO, "
        endif

        cSelect += aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_CODSER AS CODSER,"
        cSelect += aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_DTPSER AS DESCSER,"
        cSelect += aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_VLRBAS AS VLRBAS,"
        cSelect += aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_VLRRET AS VLRRET,"
        cSelect += cAlsChild + '.' + cAlsChild + "_OBSERV  AS OBSERVACAO "

        cFrom   :=  RetSqlName(cAlsChild) + " " + cAlsChild

        cFrom  += " INNER JOIN " + RetSqlName(aAlsAuxCh[CHILD1]) + " " + aAlsAuxCh[CHILD1] + " ON "
        cFrom  += aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_FILIAL = '" + xFilial(aAlsAuxCh[CHILD1]) + "'"
        cFrom  += " AND " + aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_ID = '" + cID + "'"
        cFrom  += " AND " + aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_VERSAO = " + cAlsChild + "." + cAlsChild + "_VERSAO"
        cFrom  += " AND " + aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_NUMDOC = " + cAlsChild + "." + cAlsChild + "_NUMDOC"
        cFrom  += " AND " + aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_SERIE = " + cAlsChild + "." + cAlsChild + "_SERIE"
        cFrom  += " AND " + aAlsAuxCh[CHILD1] + "." + aAlsAuxCh[CHILD1] + "_NUMFAT = " + cAlsChild + "." + cAlsChild + "_NUMFAT"
        cFrom  += " AND " + aAlsAuxCh[CHILD1] + "." + "D_E_L_E_T_ = ' '"

    ElseIf cEvento $ "R-2030|R-2040"
        cSelect := cAlsChild +'.'+cAlsChild + "_FILIAL  AS FILIAL, "   //filial

        if cEvento == "R-2030"
            cSelect += cAlsChild +'.'+cAlsChild + "_CNPJOR  AS CNPJOR, "   //cnpj de origem do recurso
        else
            cSelect += cAlsChild +'.'+cAlsChild + "_CNPJAD  AS CNPJAD, "   //cnpj da associa��o desportiva
        endif

        cSelect += cAlsChild +'.'+cAlsChild + "_VLREPA  AS VLREPA, "   //valor repasse
        cSelect += cAlsChild +'.'+cAlsChild + "_VLRET   AS VLBRUT, "   //valor bruto
        cSelect += cAlsChild +'.'+cAlsChild + "_VLNRET  AS VLRECP, "  //valor da reten��o
        cSelect += cAlsChild +'.'+cAlsChild + "_ID  AS ID   "  //id

        cFrom   := RetSqlName(cAlsChild) + " " + cAlsChild
    ElseIf cEvento $ "R-2050"
        cSelect := cAlsChild +'.'+cAlsChild + "_FILIAL  AS FILIAL, "
        cSelect := cAlsChild +'.'+cAlsChild + "_IDCOM AS INDCOM, "
        cSelect += cAlsChild +'.'+cAlsChild + "_VRECBR  AS RECBRU "

        cFrom   :=  RetSqlName(cAlsChild) + " " + cAlsChild
    ElseIf cEvento $ "R-2060"
        cSelect := cAlsChild +'.'+cAlsChild + "_FILIAL  AS FILIAL,  "   //filial
        cSelect := cAlsChild +'.'+cAlsChild + "_CDATIV  AS CODATIV, "   //c�digo da atividade
        cSelect += cAlsChild +'.'+cAlsChild + "_DESATV  AS CDSATIV, "   //descri��o da atividade
        cSelect += cAlsChild +'.'+cAlsChild + "_VLBTAT  AS VLBATIV, "   //valor da atividade
        cSelect += cAlsChild +'.'+cAlsChild + "_VLBCPR  AS VBCCPRB, "   //valor base da cpbr
        cSelect += cAlsChild +'.'+cAlsChild + "_VLARBT  AS VLRAJU,  "   //valor do ajuste
        cSelect += cAlsChild +'.'+cAlsChild + "_VLERBT  AS VLREXC,  "   //valor da exclus�o do ajuste
        cSelect += cAlsChild +'.'+cAlsChild + "_ALQCON  AS ALQATIV, "   //aliquota da atividade
        cSelect += cAlsChild +'.'+cAlsChild + "_CPRBAP  AS VLRCPRBAP"   //valor da exclus�o do ajuste"

        cFrom   :=  RetSqlName(cAlsChild) + " " + cAlsChild
    EndIf

    cWhere  :=  cAlsChild + ".D_E_L_E_T_ = ' ' "
    cWhere  +=  " AND " + cAlsChild + '.'+ cAlsChild + "_FILIAL = '" + xFilial(cAlsChild) + "'"
    If !cEvento == 'R-1070'
        cWhere  +=  " AND " + cAlsChild + '.'+ cAlsChild + "_ID = '" + cID +"'"
    Else
        cWhere  +=  " AND " + cAlsChild + '.'+ cAlsChild + "_NUMPRO = '" + cID +"'"
    EndIf
    cWhere  +=  " AND " + cAlsChild + ".D_E_L_E_T_ = ' '"

    cSelect :=  "%" + cSelect + "%"
    cFrom   :=  "%" + cFrom   + "%"
    cWhere  :=  "%" + cWhere  + "%"

    beginsql Alias cAlsBegin
        SELECT
            %exp:cSelect%  
        FROM
            %exp:cFrom%
        WHERE
            %exp:cWhere% 
    endsql

    (cAlsBegin)->(DbGoTop())

    if !(cAlsBegin)->(eof()) .and. lTotal
        nTotal  :=  (cAlsBegin)->TOTAL
    endif

    if lTotal
        (cAlsBegin)->(DbCloseArea())
    endif

return (iif(lTotal, nTotal, cAlsBegin))

//---------------------------------------------------------------------
/*/{Protheus.doc} AlsChld
Retorna o alias filho referente aos documentos existentes na tabelas espelho

@Author		henrique.pereira
@Since		25/04/2019
@Version	1.0
/*/
//---------------------------------------------------------------------
Static Function AlsChld(cEvento)
    local cAlsRet   as character

    cAlsRet     :=  ''

    do case
    case cEvento == 'R-1070'
        cAlsRet  := 'T9V'
    case cEvento == 'R-2010'
        cAlsRet  := 'T96'
    case cEvento == 'R-2020'
        cAlsRet  := 'CRO'
    case cEvento == 'R-2030'
        cAlsRet  := 'V1G'
    case cEvento == 'R-2040'
        cAlsRet  := 'V1J'
    case cEvento == 'R-2050'
        cAlsRet  := 'V1E'
    case cEvento == 'R-2060'
        cAlsRet  := 'V0T'
    endcase

return(cAlsRet)

//---------------------------------------------------------------------
/*/{Protheus.doc} AlsAuxCh
Retorna o alias auxiliar referente aos documentos existentes na tabelas espelho

@Author		Leticia Campos da Silva
@Since		30/10/2019
@Version	1.0
/*/
//---------------------------------------------------------------------

Static Function AlsAuxCh(cEvento)
    local aAlsRet   as array

    default cEvento := ''

    aAlsRet     := {}

    do case
    case cEvento == 'R-1070'
        aAdd(aAlsRet, {'T5L', 'C8S'})
    case cEvento == 'R-2010'
        aAdd(aAlsRet, {'T97'})
    case cEvento == 'R-2020'
        aAdd(aAlsRet, {'T9Y'})
    endcase

return(aAlsRet)

//---------------------------------------------------------------------
/*/{Protheus.doc} TafIndCom
Retorna o indicativo de comercializa��o correspondente

@Author		Leticia Campos da Silva
@Since		31/10/2019
@Version	1.0
/*/
//---------------------------------------------------------------------
Static Function TafIndCom(cIndCom)
    local cRet as character

    do case
    case cIndCom == '1'
        cRet := '1 - Comercializa��o da Produ��o por Prod. Rural PJ/Agroind�stria, exceto para entidades executoras do PAA.'
    case cIndCom == '7'
        cRet := '7 - Comercializa��o da Produ��o com Isen��o de Contribui��o Previdenci�ria, de acordo com a Lei n� 13.606/2018.'
    case cIndCom == '8'
        cRet := '8 - Comercializa��o da Produ��o para Entidade do Programa de Aquisi��o de Alimentos - PAA.'
    case cIndCom == '9'
        cRet := '9 - Comercializa��o direta da Produ��o no Mercado Externo.'
    endcase

return(cRet)

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} metodo POST apurReinf
invoca a apura��o do determinado evento

@author Henrique Fabiano Pateno Pereira
@since 23/04/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

    WSMETHOD POST transmitionReinf PATHPARAM sourceBranch, event, period, customerProviders WSREST WSTAF004
    Local cFuncIPC      as character
    Local cCodErr       as character
    Local cDescription  as character
    Local cFilProc      as character
    Local cEmpProc      as character
    Local cIdLog        as character
    Local cCnpjC1H      as character
    Local cIdEnt        as character
    Local cBody         as character
    Local cIdTrans      as character
    Local cAlsfilho     as character
    Local cMsgRet       as character
    Local aFil          as array
    Local aRetorno      as array
    Local aRetErro      as array
    Local oJsonResp	    as object
    Local oJson         as object
    Local lSucesso      as logical
    Local lApi          as logical

//----------------------------------
// Inicializa��o vari�veis string
//----------------------------------
    cFuncIPC        := ''
    cCodErr         := ''
    cDescription    := ''
    cFilProc        := ''
    cEmpProc        := ''
    cIdLog          := ''
    cCnpjC1H        := ''
    cIdEnt          := ''
    cBody           := ''
    cIdTrans        := ''
    cAlsfilho       := ''
    cMsgRet         := ''
    aFil            := {}
    aRetorno        := {}
    aRetErro        := {}
    lSucesso        := .T.
    lApi            := .T.
    oJsonResp		:= JsonObject():New()
    oJson           := JsonObject():New()

    cBody   :=  self:GetContent()

    if WSPrepFil(self:sourceBranch,@cFuncIPC ,@cCodErr, @cDescription, @cFilProc, @cEmpProc ) .and. !empty(cBody) .and. !empty(self:event)


        aEvents := TAFRotinas( self:event, 4, .F., 5)

        if len(aEvents) > 0
            cIdEnt     := TAFGFilMatriz()[6]
            oJson:fromJSON( cBody )
            //--------------------------------------------------------------------
            // Trata a vari�vel cIdTrans concatenando os CNPJs para filtro dos eventos 1070, 2010, 2020, 2030, 2050 e 2060 para transnmiss�o
            //--------------------------------------------------------------------
            if (self:event $ "R-1070|R-2010|R-2020|R-2030|R-2040|R-2050|R-2060")
                WsProviders(oJson["transmissionItems"],@cIdTrans)
            endif

            //--------------------------------------------------------------------
            // Executa a query dos registros que devem ser transmitidos
            //--------------------------------------------------------------------
            If self:event == "R-1070"
                cAlsfilho := "C07"
            ElseIf self:event == "R-2020"
                cAlsfilho := "C1H"
            ElseIf self:event == "R-2030"
                cAlsfilho := "V1G"
            ElseIf self:event == "R-2040"
                cAlsfilho := "V1J"
            Else
                cAlsfilho := ""
            EndIf

            cAliasTmp := WS004Event( aEvents, self:period, cAlsfilho, cIdTrans)

            //--------------------------------------------------------------------
            // Com base na query acima, alimenta o arrai aRegRec para a fun��o TAFProc9Tss execitar a transmiss�o
            //--------------------------------------------------------------------
            aRegRec := WsTafRecno(cAliasTmp)

            if self:event $ "R-1000|R-1070|R-2010|R-2020|R-2030|R-2040|R-2050|R-2060|R-2099|R-2098"
                if len(aRegRec) > 0
                    aRetorno := TAFProc9Tss(.T., aEvents, Nil, Nil, Nil, Nil, Nil, @cMsgRet, Nil, Nil, Nil, Nil,Nil, aRegRec,, lApi,@aRetErro)
                    lSucesso :=  aRetorno[1][1]
                    if !lSucesso
                        oJsonResp['type']     := RetType(aRetErro)
                        if EMPTY(EncodeUTF8(aRetorno[1][2]))
                            oJsonResp['message'] := EncodeUTF8(cMsgRet)
                        else
                            oJsonResp['message']  := EncodeUTF8(aRetorno[1][2])
                        EndIf

                    endif
                else
                    cCodErr := EncodeUTF8('Registros n�o encontrados para os par�metros enviados')
                    lSucesso := .F.
                endif
            endif
        else
            lSucesso    :=  .F.
            oJsonResp['status'] := lSucesso
            if !lSucesso
                oJsonResp['message']  := EncodeUTF8("Evento: " + self:event + " n�o encontrado")
            endif
        endif
    elseif empty(cBody) .or. !empty(cCodErr) .or. empty(self:event)
        lSucesso    :=  .F.
        oJsonResp['status'] := lSucesso
        if !lSucesso
            oJsonResp['message']  := EncodeUTF8(cCodErr)
            if empty(self:event)
                oJsonResp['message']  += EncodeUTF8("Par�metro EVENT n�o informado")
            endif
        endif

    endif

    oJsonResp['status'] := lSucesso

//------------------------------------------------------------------- 
// Serializa objeto Json 
//-------------------------------------------------------------------
    cJsonResp := FwJsonSerialize( oJsonResp )

//-------------------------------------------------------------------
// Seta resposta
//-------------------------------------------------------------------
    Self:SetResponse( cJsonResp )

//-------------------------------------------------------------------
// Elimina objeto da memoria
//-------------------------------------------------------------------
    FreeObj( oJsonResp )

return .t.

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} metodo GET invoiceDetail
Retorna o detalhe dos documentos existentes para transmiss�o dos eventos R-2010, R-2020 e R-2050

@author Henrique Fabiano Pateno Pereira/Leticia Campos da Silva
@since 23/04/2019
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------
    WSMETHOD GET invoiceDetail QUERYPARAM sourceBranch, id, event WSREST  WSTAF004
    local cFuncIPC      as character
    local cCodErr       as character
    local cDescription  as character
    local cFilProc      as character
    local cEmpProc      as character
    local cAlias        as character
    local cAlsChild     as character
    local cCnpj         as character
    local nTAnalitc     as numeric
    local nTAnalVal     as numeric
    local aAlsAuxCh     as array
    local lFatura       as logical
    local oDetMonit     as object

//----------------------------------
// Inicializa��o vari�veis string
//----------------------------------
    cFuncIPC        := ''
    cCodErr         := ''
    cDescription    := ''
    cFilProc        := ''
    cEmpProc        := ''
    cAlias          := ''
    cAlsChild       := ''
    cCnpj           := ''

//----------------------------------
// Inicializa��o vari�veis numerica
//----------------------------------
    nTAnalitc   :=  0
    nTAnalVal   :=  0

//----------------------------------
// Inicializa��o vari�veis array
//----------------------------------
    aAlsAuxCh   := {}

//-----------------------------------------------
// inicializa��o vari�veis do tipo booleana
//-----------------------------------------------
    lFatura         := .f.

//-----------------------------------------------
// inicializa��o vari�veis do tipo object
//-----------------------------------------------
    oDetMonit 	:= JsonObject():New()

    cAlsChild   := AlsChld(self:event)

    aAlsAuxCh   := AlsAuxCh(self:event)

    if WSPrepFil(self:sourceBranch,@cFuncIPC ,@cCodErr, @cDescription, @cFilProc, @cEmpProc )

        cAlias := Ws004Docs(self:id, cAlsChild, aAlsAuxCh, .f., self:event)

        (cAlias)->(DbGoTop())

        oDetMonit["invoices"] := {}

        while (cAlias)->(!EOF())

            aadd(oDetMonit["invoices"],JsonObject():New())
            nTAnalitc := len(oDetMonit["invoices"])

            If self:event $ "R-1070"
                //C�digo da suspens�o
                oDetMonit["invoices"][nTAnalitc]["suspensionCode"]          :=  (cAlias)->CODSUSP

                //Indicativo da suspens�o
                oDetMonit["invoices"][nTAnalitc]["suspensionIndicator"]     :=  alltrim(EncodeUTF8((cAlias)->INDICSUSP))

                //Data da decis�o
                oDetMonit["invoices"][nTAnalitc]["decisionDate"]            :=  SToD((cAlias)->DECISIONDATE)

                //Indicativo de dep�sito
                oDetMonit["invoices"][nTAnalitc]["depositIndicator"]        :=  (cAlias)->DEPINDICATOR
            ElseIf self:event $ "R-2010|R-2020"

                lFatura :=  !empty((cAlias)->NUMFAT)

                // tipo nota ou fatura
                oDetMonit["invoices"][nTAnalitc]["type"]                                    :=  iif(lFatura, 'FAT', 'NFS')

                // Numero da nota ou fatura
                oDetMonit["invoices"][nTAnalitc]["invoice"]                                 :=  iif(lFatura, (cAlias)->NUMFAT, (cAlias)->NUMDOC)

                // Serie da nota ou fatura
                oDetMonit["invoices"][nTAnalitc]["invoiceSeries"]                           :=  iif(lFatura, 0, (cAlias)->SERIE)

                // C�d. servi�o
                oDetMonit["invoices"][nTAnalitc]["serviceCode"]                             :=  (cAlias)->CODSER + "-" + (cAlias)->DESCSER

                // Data de emiss�o da nota ou fatura
                oDetMonit["invoices"][nTAnalitc]["issueDate"]                               := STOD((cAlias)->DATAEMISSAO)

                // valor bruto
                oDetMonit["invoices"][nTAnalitc]["grossValue"]                              := (cAlias)->VALORBRUTO

                // Base de C�lculo
                oDetMonit["invoices"][nTAnalitc]["taxBase"]                                 :=  (cAlias)->VLRBAS

                // Tributo
                oDetMonit["invoices"][nTAnalitc]["tax"]                                     :=  (cAlias)->VLRRET

                // Observa��o
                oDetMonit["invoices"][nTAnalitc]["note"]                                    := alltrim((cAlias)->OBSERVACAO)

            ElseIf self:event $ "R-2050"

                // indicativo de comercializa��o
                oDetMonit["invoices"][nTAnalitc]["typeOfTrading"]                           := EncodeUTF8(TafIndCom((cAlias)->INDCOM))

                // valor bruto
                oDetMonit["invoices"][nTAnalitc]["grossValue"]                              := (cAlias)->RECBRU

            ElseIf self:event $ "R-2030|R-2040"

                cCnpj := Iif(self:event == "R-2030", (cAlias)->CNPJOR, (cAlias)->CNPJAD)

                // cnpj origem/repasse do recurso
                oDetMonit["invoices"][nTAnalitc]["branchId"]                                                    := (cAlias)->FILIAL

                // cnpj origem/repasse do recurso
                oDetMonit["invoices"][nTAnalitc]["sourceTaxNumber"]                                             := cCnpj

                // valor bruto
                oDetMonit["invoices"][nTAnalitc]["grossValue"]                                                  := (cAlias)->VLREPA

                // valor retido
                oDetMonit["invoices"][nTAnalitc]["receivedAmount"]                                              := (cAlias)->VLBRUT

                // valor total da reten��o
                oDetMonit["invoices"][nTAnalitc]["totalValueOfRetentionWithSuspendedLiability"]                 := (cAlias)->VLRECP

                // item
                oDetMonit["invoices"][nTAnalitc]["item"]                                                        := cCnpj

                // id
                oDetMonit["invoices"][nTAnalitc]["invoiceKey"]                                                  := (cAlias)->ID

            ElseIf self:event $ "R-2060"

                //C�digo de Atividade
                oDetMonit["invoices"][nTAnalitc]['activityCode']                   := (cAlias)->CODATIV + " - " + EncodeUTF8( Alltrim( ( cAlias)->CDSATIV ) )

                //Valor bruto da Atividade
                oDetMonit["invoices"][nTAnalitc]['grossValue']                     := (cAlias)->VLBATIV

                //Valor base
                oDetMonit["invoices"][nTAnalitc]['taxBase']                         := (cAlias)->VBCCPRB

                //Valor adicional de ajuste
                oDetMonit["invoices"][nTAnalitc]['additionalValueOfAdjustment']    := (cAlias)->VLRAJU

                //Valor de exclus�o do ajuste
                oDetMonit["invoices"][nTAnalitc]['exclusionValueOfAdjustment']     := (cAlias)->VLREXC

                //Al�quota
                oDetMonit["invoices"][nTAnalitc]['aliquot']                        := (cAlias)->ALQATIV

                //Valor de contribui��o
                oDetMonit["invoices"][nTAnalitc]['contributionValue']              := (cAlias)->VLRCPRBAP

            EndIf

            (cAlias)->(DbSkip())
        enddo

    endif

    Self:SetResponse( oDetMonit:toJSON())

    //-------------------------------------------------------------------
    // Elimina objeto da memoria
    //-------------------------------------------------------------------
    FreeObj( oDetMonit )
return .t.

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Metodo GET
M�todo respons�vel pela consulta dos detalhes dos recursos recebidos por documento.

@author Leticia Campos
@since 28/01/2020
@version 1.0
/*/
//---------------------------------------------------------------------------------------------------------------

    WSMETHOD GET taxDetail QUERYPARAM sourceBranch, period, event, id, item, page, pageSize WSREST  WSTAF004
    local cFuncIPC      as character
    local cCodErr       as character
    local cDescr        as character
    local cDescription  as character
    local cFilProc      as character
    local cEmpProc      as character
    local cAlias        as character
    local cTable        as character
    local cAlsChild     as character
    local nTAnalitc     as numeric
    local nTAnalVal     as numeric
    local aAlsAuxCh     as array
    local lFatura       as logical
    local oDetMonit     as object

//----------------------------------
// Inicializa��o vari�veis string
//----------------------------------
    cFuncIPC        := ''
    cCodErr         := ''
    cDescription    := ''
    cFilProc        := ''
    cEmpProc        := ''
    cAlias          := ''
    cAlsChild       := ''
    cDescr          := ''
    cTable          := ''

//----------------------------------
// Inicializa��o vari�veis numerica
//----------------------------------
    nTAnalitc   :=  0
    nTAnalVal   :=  0

//----------------------------------
// Inicializa��o vari�veis array
//----------------------------------
    aAlsAuxCh   := {}

//-----------------------------------------------
// inicializa��o vari�veis do tipo booleana
//-----------------------------------------------
    lFatura         := .f.

//-----------------------------------------------
// inicializa��o vari�veis do tipo object
//-----------------------------------------------
    oDetMonit 	:= JsonObject():New()

    if WSPrepFil(self:sourceBranch,@cFuncIPC ,@cCodErr, @cDescription, @cFilProc, @cEmpProc )

        cTable := Iif(self:event == 'R-2030', 'V1H', 'V1K')

        cAlias := Ws004Det(self:id, cTable, self:event, self:item)

        (cAlias)->(DbGoTop())

        oDetMonit["tax"] := {}

        while (cAlias)->(!EOF())

            aadd(oDetMonit["tax"], JsonObject():New())
            nTAnalitc := len(oDetMonit["tax"])

            If self:event $ "R-2030|R-2040"

                If (cAlias)->TPREPA == "1"
                    cDescr := "1 - Patroc�nio"
                ElseIf (cAlias)->TPREPA == "2"
                    cDescr := "2 - Licenciamento de marcas e s�mbolos"
                ElseIf (cAlias)->TPREPA == "3"
                    cDescr := "3 - Publicidade"
                ElseIf (cAlias)->TPREPA == "4"
                    cDescr := "4 - Propaganda"
                ElseIf (cAlias)->TPREPA == "5"
                    cDescr := "5 - Transmiss�o de espet�culos"
                EndIf

                // tipo do repasse e descri��o do recurso
                oDetMonit["tax"][nTAnalitc]["typeOfTransfer"]                          := alltrim(EncodeUTF8(cDescr))

                // valor bruto
                oDetMonit["tax"][nTAnalitc]["grossValue"]                              := (cAlias)->VLBRUT

                // valor da reten��o
                oDetMonit["tax"][nTAnalitc]["receivedAmount"]                          := (cAlias)->VLRECP

            EndIf

            (cAlias)->(DbSkip())
        enddo

    endif

    Self:SetResponse( oDetMonit:toJSON())

    //-------------------------------------------------------------------
    // Elimina objeto da memoria
    //-------------------------------------------------------------------
    FreeObj( oDetMonit )

return .T.

//---------------------------------------------------------------------
/*/{Protheus.doc} Ws004Det

Retorna o detalhamento dos recursos a serem transmitidos

@Author		leticia.campos
@Since		28/01/2020
@Version	1.0
/*/
//---------------------------------------------------------------------
static function Ws004Det(cID, cAlsChild, cEvento, cCNPJ)

    local cSelect   as character
    local cFrom     as character
    local cWhere    as character
    local cAlsBegin as character

    default cID         := ''
    default cAlsChild   := ''
    default cEvento     := ''
    default cCNPJ       := ''

    cSelect     :=  ''
    cFrom       :=  ''
    cWhere      :=  ''
    cAlsBegin   :=  getnextalias()

    If cEvento $ "R-2030|R-2040"
        cSelect := cAlsChild +'.'+cAlsChild + "_FILIAL  AS FILIAL, "   //filial

        if cEvento == "R-2030"
            cSelect += cAlsChild +'.'+cAlsChild + "_CNPJOR  AS CNPJOR, "   //cnpj de origem do recurso
        else
            cSelect += cAlsChild +'.'+cAlsChild + "_CNPJAD  AS CNPJAD, "   //cnpj de origem do recurso
        endif

        cSelect += cAlsChild +'.'+cAlsChild + "_TPREPA  AS TPREPA, "   //tipo do repasse
        cSelect += cAlsChild +'.'+cAlsChild + "_DESCRE  AS DESCRE, "   //descri��o
        cSelect += cAlsChild +'.'+cAlsChild + "_VLBRUT  AS VLBRUT,   "  //valor bruto
        cSelect += cAlsChild +'.'+cAlsChild + "_VLRECP  AS VLRECP   "  //valor bruto

        cFrom   := RetSqlName(cAlsChild) + " " + cAlsChild

        cWhere  :=  cAlsChild + ".D_E_L_E_T_ = ' ' "
        cWhere  +=  " AND " + cAlsChild + '.' + cAlsChild + "_FILIAL = '" + xFilial(cAlsChild) + "'"

        if cEvento == "R-2030"
            cWhere  +=  " AND " + cAlsChild + '.' + cAlsChild + "_CNPJOR = '" + cCNPJ + "'"
        else
            cWhere  +=  " AND " + cAlsChild + '.' + cAlsChild + "_CNPJAD = '" + cCNPJ + "'"
        endif

        cWhere  +=  " AND " + cAlsChild + '.' + cAlsChild + "_ID = '" + cID +"'"
    EndIf

    cSelect :=  "%" + cSelect + "%"
    cFrom   :=  "%" + cFrom   + "%"
    cWhere  :=  "%" + cWhere  + "%"

    beginsql Alias cAlsBegin
        SELECT
            %exp:cSelect%  
        FROM
            %exp:cFrom%
        WHERE
            %exp:cWhere% 
    endsql

    (cAlsBegin)->(DbGoTop())

return  ( cAlsBegin )

/*/{Protheus.doc} RetType
    Retorna o tipo de erro que foi apresentado no TAFProc9.
    @type  Function
    @author Jos� Mauro
    @since 14/07/2020
    @version version
    @param Recebe o array que vem do TAFProc9.
    @return Retorna o tipo de erro.
        1 - Erro de schema
        2 - Erro para se conectar no TSS
        3 - Erro de predecess�o.
    /*/
Static Function RetType(aRetorno)
    Local nType as numeric
    Local nX    as numeric

    nX    := 0
    nType := 0

    For nX := 1 to Len(aRetorno)
        if aRetorno[nX]
            nType :=  nX
        EndIf
    Next nX

Return nType