#Include "totvs.ch"
#Include "topconn.ch"
#Include "restful.ch"

#DEFINE TOTAL       1
#DEFINE PENDENT     2
#DEFINE WAITING     3
#DEFINE REJECTED    4
#DEFINE AUTHORIZED  5
#DEFINE EXCLUDED    6

Static __aSocRot    := Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} WSTAF008
API para obter os dados sint�ticos dos cards do Dashboard do THF

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
WSRESTFUL WSTAF008 DESCRIPTION "Dados dos Cards Sint�ticos Dashboard" FORMAT APPLICATION_JSON

WSMETHOD POST  V1 DESCRIPTION "Retorna os dados para os Cards Sint�ticos" PATH "/v1/"  TTALK "v1" WSSYNTAX "/WSTAF008" PRODUCES APPLICATION_JSON

END WSRESTFUL

//-------------------------------------------------------------------
/*/{Protheus.doc} POST
M�todo para retornar os dados de acordo com os par�metros informados
no Body.

Est� sendo utilizado o m�todo POST, pois o guia de API TOTVS n�o permite
que seja passados par�metros no Body da Request no m�todo GET

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
WSMETHOD POST V1 WSSERVICE WSTAF008

Local lRet          := .T.
Local cResultConv   := ""
Local cRequest      := self:GetContent()
Local oRequest      := JsonObject():New()
Local oResponse     := Nil

cResultConv := oRequest:FromJson(cRequest)

If cResultConv == Nil
    oResponse   := WS008Resp(oRequest)
    self:setResponse( oResponse:ToJson() )
Else
    lRet := .F.
    SetRestFault(400, "Invalid Json Object", .T., 400, cResultConv)
EndIf

Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS008Resp
Utilizado para montar o objeto com a response

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS008Resp(oRequest)
    
Local aBranches     := {}
Local aEvents       := {}
Local aSynthetic    := {}
Local oResponse     := JsonObject():New()
Local cPeriod       := ""

If oRequest["branches"] <> NIL
    aBranches := aClone(oRequest["branches"])
EndIf

If oRequest["period"] <> NIL
    cPeriod := oRequest["period"]
EndIf

If oRequest["events"] <> NIL
    aEvents := aClone(oRequest["events"])
EndIf

// Faz as query por evento
aSynthetic := WS008Work(cPeriod, aBranches, aEvents)

oResponse["total"]      := aSynthetic[TOTAL]
oResponse["pendent"]    := aSynthetic[PENDENT]
oResponse["waiting"]    := aSynthetic[WAITING]
oResponse["rejected"]   := aSynthetic[REJECTED]
oResponse["authorized"] := aSynthetic[AUTHORIZED]
oResponse["excluded"]   := aSynthetic[EXCLUDED]

Return(oResponse)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS008Work
Retorna o array contendo os valores de acordo com o status

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS008Work(cPeriod, aBranches, aEvents)

Local cAliasEvent   := ""
Local cEvent        := ""
Local cInFiliais    := ""
Local cTypeEvent    := ""
Local nX            := 0
Local nPosEvent     := 0
Local aValues       := {}
Local aTotal        := WS008InitArr()

// Inicializa as vari�veis statics
InitStatic()

For nX := 1 To Len(aEvents)
    
    cEvent      := aEvents[nX] 
    nPosEvent   := aScan(__aSocRot, {|x| x[4] == cEvent })

    If nPosEvent > 0
        
        //Limpa a cada volta do array
        cInFiliais  := ""
        
        cAliasEvent := __aSocRot[nPosEvent][3]
        cTypeEvent  := __aSocRot[nPosEvent][12]
        
        // Monta a cl�usula IN com as filiais informadas
        aEval( aBranches, { |x| cInFiliais += xFilial(cAliasEvent, x) + ";" })
        cInFiliais  := Left(cInFiliais, Len(cInFiliais) -1 )
        cInFiliais  := FormatIn(cInFiliais, ";")
        
        // Incrementa o array
        aValues := WS008Values(cAliasEvent, cPeriod, cInFiliais, cTypeEvent)

        aTotal[TOTAL]       += aValues[TOTAL]         
        aTotal[PENDENT]     += aValues[PENDENT]
        aTotal[WAITING]     += aValues[WAITING]       
        aTotal[REJECTED]    += aValues[REJECTED]      
        aTotal[AUTHORIZED]  += aValues[AUTHORIZED]    
        aTotal[EXCLUDED]    += aValues[EXCLUDED]      

    EndIf

Next nX

Return(aTotal)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS008Values
Monta a string da query por evento

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS008Values(cAliasEvent, cPeriod, cInFiliais, cTypeEvent)

Local cWorkTable    := GetNextAlias()
Local cQuery        := ""
Local cFieldStatus  := cAliasEvent + "_STATUS "
Local aRet          := WS008InitArr()

If Select(cWorkTable) > 0
    (cWorkTable)->( dbCloseArea() )
EndIf

cQuery := "SELECT COUNT(" + cAliasEvent + "_STATUS) QTD, "
cQuery += cFieldStatus
cQuery += " FROM " + RetSQLName(cAliasEvent)
cQuery += " WHERE " + cAliasEvent + "_FILIAL IN " + cInFiliais
cQuery += " AND " + cAliasEvent + "_ATIVO = '1' "

If cTypeEvent == "M" .And. !Empty(cPeriod)
    cQuery += " AND " + cAliasEvent + "_PERAPU = '" + cPeriod + "' "
EndIf

cQuery += " AND D_E_L_E_T_ = ' ' "
cQuery += " GROUP BY " + cAliasEvent + "_STATUS "

TCQuery cQuery New Alias (cWorkTable)

(cWorkTable)->( dbGoTop() )

While (cWorkTable)->( !Eof() )

    Do Case
        Case &("(cWorkTable)->" + cFieldStatus) $ " |0|1"
            aRet[PENDENT] := (cWorkTable)->QTD
        Case &("(cWorkTable)->" + cFieldStatus) == "2"
            aRet[WAITING] := (cWorkTable)->QTD
        Case &("(cWorkTable)->" + cFieldStatus) == "3"
            aRet[REJECTED] := (cWorkTable)->QTD
        Case &("(cWorkTable)->" + cFieldStatus) == "4"
            aRet[AUTHORIZED] := (cWorkTable)->QTD
        Case &("(cWorkTable)->" + cFieldStatus) $ "6|7"
            aRet[EXCLUDED] := (cWorkTable)->QTD
    EndCase

    aRet[TOTAL] += (cWorkTable)->QTD

    (cWorkTable)->( dbSkip() )

EndDo

(cWorkTable)->( dbCloseArea() )

Return(aRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS008InitArr
Inicializa��o de arrays.

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS008InitArr()

Local aNewArr := Array(6)

//Inicializa as posi��es do array
aNewArr[TOTAL]       := 0
aNewArr[PENDENT]     := 0
aNewArr[WAITING]     := 0
aNewArr[REJECTED]    := 0
aNewArr[AUTHORIZED]  := 0
aNewArr[EXCLUDED]    := 0

Return(aNewArr)

//-------------------------------------------------------------------
/*/{Protheus.doc} InitStatic
Inicializa as vari�veis statics

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
Static Function InitStatic()
    
If __aSocRot == Nil
    __aSocRot := TAFRotinas(,,.T.,2)
EndIf

Return