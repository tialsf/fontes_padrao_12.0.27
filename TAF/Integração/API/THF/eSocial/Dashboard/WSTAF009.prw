#Include "totvs.ch"
#Include "topconn.ch"
#Include "restful.ch"

Static __aSocRot    := Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} WSTAF009
API para obter os dados sint�ticos dos cards do Dashboard do THF

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
WSRESTFUL WSTAF009 DESCRIPTION "Dados sint�ticos dos Cards de Eventos do Dashboards" FORMAT APPLICATION_JSON

WSMETHOD POST  V1 DESCRIPTION "Retorna os dados para os Cards de Eventos" PATH "/v1/"  TTALK "v1" WSSYNTAX "/WSTAF009" PRODUCES APPLICATION_JSON

END WSRESTFUL

//-------------------------------------------------------------------
/*/{Protheus.doc} POST
M�todo para retornar os dados de acordo com os par�metros informados
no Body.

Est� sendo utilizado o m�todo POST, pois o guia de API TOTVS n�o permite
que seja passados par�metros no Body da Request no m�todo GET

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
WSMETHOD POST V1 WSSERVICE WSTAF009

Local lRet          := .T.
Local cResultConv   := ""
Local cRequest      := self:GetContent()
Local oRequest      := JsonObject():New()
Local oResponse     := Nil

InitStatic()

cResultConv := oRequest:FromJson(cRequest)

If cResultConv == Nil
    oResponse   := WS009Resp(oRequest)
    self:setResponse( oResponse:ToJson() )
Else
    lRet := .F.
    SetRestFault(400, "Invalid Json Object", .T., 400, cResultConv)
EndIf

Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS009Resp
Utilizado para montar o objeto com a response

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS009Resp(oRequest)

Local aBranches     := {}
Local aEvents       := {}
Local oResponse     := JsonObject():New()
Local cPeriod       := ""
Local cEvent        := ""
Local cTableEvent   := ""
Local cTypeEvent    := ""
Local nPosEvent     := 0
Local nX            := 0

If oRequest["branches"] <> NIL
    aBranches := aClone(oRequest["branches"])
EndIf

If oRequest["period"] <> NIL
    cPeriod := oRequest["period"]
EndIf

If oRequest["events"] <> NIL
    aEvents := aClone(oRequest["events"])
EndIf

oResponse["events"] := {}

For nX := 1 To Len(aEvents)

    //Limpa a cada volta do array
    cInFiliais  := ""

    cEvent      := aEvents[nX] 
    nPosEvent   := aScan(__aSocRot, {|x| x[4] == cEvent .And. x[12] <> "T" })
    cTableEvent := __aSocRot[nPosEvent][3]
    cTypeEvent  := __aSocRot[nPosEvent][12]
  
    // Monta a cl�usula IN com as filiais informadas
    aEval( aBranches, { |x| cInFiliais += xFilial(cTableEvent, x) + ";" })
    cInFiliais  := Left(cInFiliais, Len(cInFiliais) -1 )
    cInFiliais := FormatIn(cInFiliais, ";")

    aAdd(oResponse["events"], JsonObject():New())

    WS009Event(@oResponse["events"], nX, cPeriod, cInFiliais, cTableEvent, cEvent, cTypeEvent)

Next nX

Return(oResponse)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS009Event
Faz a query agrupando um determinado evento por status e retorna por
refer�ncia. 

@author Victor A. Barbosa
@since 15/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS009Event(oResponse, nNewPos, cPeriod, cInFiliais, cTableEvent, cEvent, cTypeEvent)

Local cWorkAlias    := GetNextAlias()
Local cQuery        := ""
Local cFieldStatus  := cTableEvent + "_STATUS "
Local nTotEvent     := 0

If Select(cWorkAlias) > 0
    (cWorkAlias)->( dbCloseArea() )
EndIf

cQuery := "SELECT COUNT(" + cTableEvent + "_STATUS) QTD, "
cQuery += cFieldStatus
cQuery += " FROM " + RetSQLName(cTableEvent)
cQuery += " WHERE " + cTableEvent + "_FILIAL IN " + cInFiliais
cQuery += " AND " + cTableEvent + "_ATIVO = '1' "

If cTypeEvent == "M" .And. !Empty(cPeriod)
    cQuery += " AND " + cTableEvent + "_PERAPU = '" + cPeriod + "' "
EndIf

cQuery += " AND D_E_L_E_T_ = ' ' "
cQuery += " GROUP BY " + cTableEvent + "_STATUS "

TCQuery cQuery New Alias (cWorkAlias)

oResponse[nNewPos]["event"]         := Upper(AllTrim(cEvent))
oResponse[nNewPos]["description"]   := AllTrim(EncodeUTF8(Posicione( "C8E", 2, xFilial( "C8E" ) + cEvent, "C8E_DESPRT" )))

// Inicializa zerado, para devolver zerado caso n�o existe o status
oResponse[nNewPos]["pendent"]   := 0
oResponse[nNewPos]["waiting"]   := 0
oResponse[nNewPos]["rejected"]  := 0
oResponse[nNewPos]["success"]   := 0
oResponse[nNewPos]["deleted"]   := 0
oResponse[nNewPos]["total"]     := 0

If (cWorkAlias)->( !Eof() )

    While (cWorkAlias)->( !Eof() )

        nTotEvent += (cWorkAlias)->QTD

        Do Case
            Case &("(cWorkAlias)->" + cFieldStatus) $ " |0|1"
                oResponse[nNewPos]["pendent"] := (cWorkAlias)->QTD
            Case &("(cWorkAlias)->" + cFieldStatus) == "2"
                oResponse[nNewPos]["waiting"] := (cWorkAlias)->QTD
            Case &("(cWorkAlias)->" + cFieldStatus) == "3"
                oResponse[nNewPos]["rejected"] := (cWorkAlias)->QTD
            Case &("(cWorkAlias)->" + cFieldStatus) == "4"
                oResponse[nNewPos]["success"] := (cWorkAlias)->QTD
            Case &("(cWorkAlias)->" + cFieldStatus) $ "6|7"
                oResponse[nNewPos]["deleted"] := (cWorkAlias)->QTD
        EndCase

        (cWorkAlias)->( dbSkip() )

    EndDo

    oResponse[nNewPos]["total"] := nTotEvent

EndIf

(cWorkAlias)->( dbCloseArea() )

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} InitStatic
Inicializa as vari�veis st�tics

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
Static Function InitStatic()
    
If __aSocRot == Nil
    __aSocRot := TAFRotinas(,,.T.,2)
EndIf

Return