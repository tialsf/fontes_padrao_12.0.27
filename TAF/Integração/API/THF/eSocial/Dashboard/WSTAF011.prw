#Include "totvs.ch"
#Include "topconn.ch"
#Include "restful.ch"

Static __aSocRot    := Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} WSTAF011
API para obter os eventos/descritivos do eSocial.

@author Victor A. Barbosa
@since 15/08/2019
/*/
//-------------------------------------------------------------------
WSRESTFUL WSTAF011 DESCRIPTION "Eventos do eSocial" FORMAT APPLICATION_JSON

WSMETHOD GET  V1 DESCRIPTION "Retorna os dados dos eventos do eSocial" PATH "/v1/"  TTALK "v1" WSSYNTAX "/WSTAF011" PRODUCES APPLICATION_JSON

END WSRESTFUL

//-------------------------------------------------------------------
/*/{Protheus.doc} GET
M�todo para retornar os eventos do eSocial

@author Victor A. Barbosa
@since 15/08/2019
/*/
//-------------------------------------------------------------------
WSMETHOD GET V1 WSSERVICE WSTAF011

Local lRet          := .T.
Local oResponse     := JsonObject():New()

oResponse   := WS011Resp()
self:setResponse( oResponse:ToJson() )

Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS011Resp
Monta o objeto de resposta, contendo os eventos do eSocial

@author Victor A. Barbosa
@since 16/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS011Resp()

Local cWorkTable    := GetNextAlias()
Local cNowValid     := Dtos(Date())
Local nQuantity     := 0
Local oResponse     := JsonObject():New()

InitStatic()

If Select(cWorkTable) > 0
    (cWorkTable)->( dbCloseArea() )
EndIf

BeginSQL Alias cWorkTable
    SELECT C8E_CODIGO, C8E_DESPRT FROM %table:C8E% C8E
    WHERE C8E_FILIAL = %xFilial:C8E%
    AND (C8E_VALIDA = ' ' OR C8E_VALIDA >= %exp:cNowValid% )
    AND C8E.%notdel%
EndSQL

(cWorkTable)->( dbGoTop() )

oResponse["events"] := {}

While (cWorkTable)->( !Eof() )

    nQuantity++
    aAdd( oResponse["events"], JsonObject():New() )

    oResponse["events"][nQuantity]["event"]       := EncodeUTF8( AllTrim( (cWorkTable)->C8E_CODIGO ) )
    oResponse["events"][nQuantity]["description"] := EncodeUTF8( AllTrim( (cWorkTable)->C8E_DESPRT ) )
    oResponse["events"][nQuantity]["type"]        := WS011Type( (cWorkTable)->C8E_CODIGO )

    (cWorkTable)->( dbSkip() )
EndDo

(cWorkTable)->( dbCloseArea() )

Return(oResponse)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS011Type
Retorna o tipo do evento, podendo ser:
 - Tabelas;
 - N�o Peri�dicos;
 - Peri�dicos;
 - Totalizadores;

@author Victor A. Barbosa
@since 16/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS011Type(cEvent)

Local nPosEvent     := 0
Local cTypeTafRot   := ""
Local cTypeEvent    := ""

nPosEvent   := aScan(__aSocRot, {|x| x[4] == cEvent })

If nPosEvent > 0

    cTypeTafRot := __aSocRot[nPosEvent][12]

    Do Case
        Case cTypeTafRot == "C"
            cTypeEvent := "tables"
        Case cTypeTafRot == "M"
            cTypeEvent := "periodics"
        Case cTypeTafRot == "E"
            cTypeEvent := "notPeriodics"
        Case cTypeTafRot == "T"
            cTypeEvent := "totalizers"
    EndCase

EndIf

Return(cTypeEvent)

//-------------------------------------------------------------------
/*/{Protheus.doc} InitStatic
Inicializa as vari�veis st�tics

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
Static Function InitStatic()
    
If __aSocRot == Nil
    __aSocRot := TAFRotinas(,,.T.,2)
EndIf

Return