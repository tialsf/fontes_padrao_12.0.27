#Include "totvs.ch"
#Include "topconn.ch"
#Include "restful.ch"

Static __aSocRot    := Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} WSTAF014
API para obter os eventos totalizadores para os cards do Dashboard do portal

@author Victor A. Barbosa
@since 23/08/2019
/*/
//-------------------------------------------------------------------
WSRESTFUL WSTAF014 DESCRIPTION "Eventos totalizadores do Dashboards" FORMAT APPLICATION_JSON

WSMETHOD POST  V1 DESCRIPTION "Retorna os eventos totalizadores para o dashboard" PATH "/v1/"  TTALK "v1" WSSYNTAX "/WSTAF014" PRODUCES APPLICATION_JSON

END WSRESTFUL

//-------------------------------------------------------------------
/*/{Protheus.doc} POST
M�todo para retornar os dados de acordo com os par�metros informados
no Body.

Est� sendo utilizado o m�todo POST, pois o guia de API TOTVS n�o permite
que seja passados par�metros no Body da Request no m�todo GET

@author Victor A. Barbosa
@since 23/08/2019
/*/
//-------------------------------------------------------------------
WSMETHOD POST V1 WSSERVICE WSTAF014

Local lRet          := .T.
Local cResultConv   := ""
Local cRequest      := self:GetContent()
Local oRequest      := JsonObject():New()
Local oResponse     := Nil

InitStatic()

cResultConv := oRequest:FromJson(cRequest)

If cResultConv == Nil
    oResponse   := WS014Resp(oRequest)
    self:setResponse( oResponse:ToJson() )
Else
    lRet := .F.
    SetRestFault(400, "Invalid Json Object", .T., 400, cResultConv)
EndIf

Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS014Resp
Utilizado para montar o objeto com a response

@author Victor A. Barbosa
@since 23/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS014Resp(oRequest)

Local aBranches     := {}
Local aEvents       := {}
Local oResponse     := JsonObject():New()
Local cPeriod       := ""
Local cEvent        := ""
Local cTableEvent   := ""
Local nPosEvent     := 0
Local nX            := 0

If oRequest["branches"] <> NIL
    aBranches := aClone(oRequest["branches"])
EndIf

If oRequest["period"] <> NIL
    cPeriod := oRequest["period"]
EndIf

If oRequest["totalizers"] <> NIL
    aEvents := aClone(oRequest["totalizers"])
EndIf

oResponse["events"] := {}

For nX := 1 To Len(aEvents)

    //Limpa a cada volta do array
    cInFiliais  := ""

    cEvent      := aEvents[nX] 
    nPosEvent   := aScan(__aSocRot, {|x| x[4] == cEvent .And. x[12] == "T" })
    cTableEvent := __aSocRot[nPosEvent][3]
  
    // Monta a cl�usula IN com as filiais informadas
    aEval( aBranches, { |x| cInFiliais += xFilial(cTableEvent, x) + ";" })
    cInFiliais  := Left(cInFiliais, Len(cInFiliais) -1 )
    cInFiliais := FormatIn(cInFiliais, ";")

    aAdd(oResponse["events"], JsonObject():New())

    WS014Event(@oResponse["events"], nX, cPeriod, cInFiliais, cTableEvent, cEvent)

Next nX

Return(oResponse)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS014Event
Faz a query agrupando um determinado evento por status e retorna por
refer�ncia. 

@author Victor A. Barbosa
@since 23/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS014Event(oResponse, nNewPos, cPeriod, cInFiliais, cTableEvent, cEvent)

Local cWorkAlias    := GetNextAlias()
Local cQuery        := ""

If Select(cWorkAlias) > 0
    (cWorkAlias)->( dbCloseArea() )
EndIf

cQuery := "SELECT COUNT( R_E_C_N_O_ ) QTD "
cQuery += " FROM " + RetSQLName(cTableEvent)
cQuery += " WHERE " + cTableEvent + "_FILIAL IN " + cInFiliais

If !Empty(cPeriod)
    cQuery += " AND " + cTableEvent + "_PERAPU = '" + cPeriod + "' "
EndIf

cQuery += " AND D_E_L_E_T_ = ' ' "

TCQuery cQuery New Alias (cWorkAlias)

oResponse[nNewPos]["event"]         := Upper(AllTrim(cEvent))
oResponse[nNewPos]["description"]   := AllTrim(EncodeUTF8(Posicione( "C8E", 2, xFilial( "C8E" ) + cEvent, "C8E_DESPRT" )))
oResponse[nNewPos]["total"]         := 0

If (cWorkAlias)->( !Eof() )
    oResponse[nNewPos]["total"] := (cWorkAlias)->QTD
EndIf

(cWorkAlias)->( dbCloseArea() )

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} InitStatic
Inicializa as vari�veis st�tics

@author Victor A. Barbosa
@since 23/08/2019
/*/
//-------------------------------------------------------------------
Static Function InitStatic()
    
If __aSocRot == Nil
    __aSocRot := TAFRotinas(,,.T.,2)
EndIf

Return