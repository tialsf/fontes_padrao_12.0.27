#Include "totvs.ch"
#Include "topconn.ch"
#Include "restful.ch"

Static __aSocRot    := Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} WSTAF012
API para obter os dados sint�ticos de um evento espec�fico do eSocial.

@author Victor A. Barbosa
@since 16/08/2019
/*/
//-------------------------------------------------------------------
WSRESTFUL WSTAF012 DESCRIPTION "Eventos do eSocial" FORMAT APPLICATION_JSON

WSMETHOD POST V1 DESCRIPTION "Retorna os dados dos eventos do eSocial para os card sint�ticos" PATH "/v1/"  TTALK "v1" WSSYNTAX "/WSTAF012" PRODUCES APPLICATION_JSON

END WSRESTFUL

//-------------------------------------------------------------------
/*/{Protheus.doc} POST
M�todo para retornar os eventos do eSocial

Est� sendo utilizado o m�todo POST, pois o guia de API TOTVS n�o permite
que seja passados par�metros no Body da Request no m�todo GET

@author Victor A. Barbosa
@since 16/08/2019
/*/
//-------------------------------------------------------------------
WSMETHOD POST V1 WSSERVICE WSTAF012

Local lRet          := .T.
Local cResultConv   := ""
Local cRequest      := self:GetContent()
Local oRequest      := JsonObject():New()
Local oResponse     := Nil

cResultConv := oRequest:FromJson(cRequest)

If cResultConv == Nil
    oResponse   := WS012Resp(oRequest)
    self:setResponse( oResponse:ToJson() )
Else
    lRet := .F.
    SetRestFault(400, "Invalid Json Object", .T., 400, cResultConv)
EndIf

Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS012Resp
Utilizado para montar o objeto com a response

@author Victor A. Barbosa
@since 16/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS012Resp(oRequest)
    
Local aBranches     := {}
Local oResponse     := Nil
Local cPeriod       := ""
Local cEvent        := ""
Local cStatus       := ""

InitStatic()

If oRequest["branches"] <> NIL
    aBranches := aClone(oRequest["branches"])
EndIf

If oRequest["period"] <> NIL
    cPeriod := oRequest["period"]
EndIf

If oRequest["event"] <> NIL
    cEvent := oRequest["event"]
EndIf

If oRequest["status"] <> NIL
    cStatus := oRequest["status"]
EndIf

oResponse := WS012Event(aBranches, cPeriod, cEvent, cStatus)

Return(oResponse)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS012Event
Realiza a consulta dos registros de um determinado evento de acordo 
com o status

@author Victor A. Barbosa
@since 16/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS012Event(aBranches, cPeriod, cEvent, cStatus)

Local nPosEvent     := 0
Local cAliasEvent   := ""
Local cInFiliais    := ""
Local oResponse     := JsonObject():New()

nPosEvent   := aScan(__aSocRot, {|x| x[4] == cEvent })

If nPosEvent > 0
    
    cAliasEvent := __aSocRot[nPosEvent][3]
    
    // Monta a cl�usula IN com as filiais informadas
    aEval( aBranches, { |x| cInFiliais += xFilial(cAliasEvent, x) + ";" })
    cInFiliais  := Left(cInFiliais, Len(cInFiliais) -1 )
    cInFiliais  := FormatIn(cInFiliais, ";")
    
    WS012Values(@oResponse, cAliasEvent, cPeriod, cInFiliais, cStatus)
    
EndIf

Return(oResponse)

//-------------------------------------------------------------------
/*/{Protheus.doc} WS012Values
Realiza a query, agrupando os registros por status

@author Victor A. Barbosa
@since 16/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS012Values(oResponse, cAliasEvent, cPeriod, cInFiliais, cStatus)

Local cQuery        := ""
Local cWorkTable    := GetNextAlias()
Local cFieldStatus  := cAliasEvent + "_STATUS "

If Select(cWorkTable) > 0
    (cWorkTable)->( dbCloseArea() )
EndIf

cQuery := "SELECT COUNT(" + cAliasEvent + "_STATUS) QTD, "
cQuery += cFieldStatus
cQuery += " FROM " + RetSQLName(cAliasEvent)
cQuery += " WHERE " + cAliasEvent + "_FILIAL IN " + cInFiliais
cQuery += " AND D_E_L_E_T_ = ' ' "
cQuery += " GROUP BY " + cAliasEvent + "_STATUS "

TCQuery cQuery New Alias (cWorkTable)

(cWorkTable)->( dbGoTop() )

If (cWorkTable)->( !Eof() )

    // Inicializa zerado, para devolver zerado caso n�o exista o status
    oResponse["pending"]       := 0
    oResponse["waiting"]       := 0
    oResponse["rejected"]      := 0
    oResponse["authorized"]    := 0
    oResponse["excluded"]      := 0
    oResponse["total"]         := 0

    While (cWorkTable)->( !Eof() )

        Do Case
            Case &("(cWorkTable)->" + cFieldStatus) $ "0|1"
                oResponse["pending"] := (cWorkTable)->QTD
            Case &("(cWorkTable)->" + cFieldStatus) == "2"
                oResponse["waiting"] := (cWorkTable)->QTD
            Case &("(cWorkTable)->" + cFieldStatus) == "3"
                oResponse["rejected"] := (cWorkTable)->QTD
            Case &("(cWorkTable)->" + cFieldStatus) == "4"
                oResponse["authorized"] := (cWorkTable)->QTD
            Case &("(cWorkTable)->" + cFieldStatus) $ "6|7"
                oResponse["excluded"] := (cWorkTable)->QTD
        EndCase

        oResponse["total"] += (cWorkTable)->QTD

        (cWorkTable)->( dbSkip() )

    EndDo

EndIf

(cWorkTable)->( dbCloseArea() )

Return(oResponse)

//-------------------------------------------------------------------
/*/{Protheus.doc} InitStatic
Inicializa as vari�veis st�tics

@author Victor A. Barbosa
@since 16/08/2019
/*/
//-------------------------------------------------------------------
Static Function InitStatic()
    
If __aSocRot == Nil
    __aSocRot := TAFRotinas(,,.T.,2)
EndIf

Return