#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "FWAdapterEAI.ch"

#DEFINE PAGE_DEF		1
#DEFINE PAGESIZE_DEF	15
#DEFINE ERRORCODE_DEF	400

Static __aSM0	:=	Nil


/*/{Protheus.doc} WSRESTFUL monitorEsocial
Servi�o de retorno de filiais de acordo com os par�metros informados
@author  Totvs
@since   05/11/2019
@version 12.1.25
/*/
WSRESTFUL EsocialBranches DESCRIPTION oEmToAnsi( "Servi�o do Monitor de Transmiss�o do eSocial (TAFFULL) " ) FORMAT APPLICATION_JSON
	WSDATA companyId	AS STRING
	WSDATA page			AS INTEGER OPTIONAL
	WSDATA pageSize		AS INTEGER OPTIONAL
	
	WSMETHOD GET DESCRIPTION oEmToAnsi( "M�todo para consultar as filiais para a transmiss�o dos eventos do E-Social" ) WSSYNTAX "api/rh/esocial/v1/EsocialBranches/?{companyId}&{page}&{pageSize}" PATH "api/rh/esocial/v1/EsocialBranches/" TTALK "v1" PRODUCES APPLICATION_JSON
END WSRESTFUL


/*/{Protheus.doc} WSMETHOD GET EsocialBranches
Retorna as filiais de acordo com os par�metros informados
@author  Totvs
@since   05/11/2019
@version 12.1.25
/*/
WSMETHOD GET QUERYPARAM companyId, page, pageSize WSRESTFUL EsocialBranches
	Local oResponse		:=	Nil
	Local cEmpRequest	:=	""
	Local cFilRequest	:=	""
	Local nPage			:=	1
	Local nPageSize		:=	1
	Local aCompany		:=	{}
	Local lAmbiente		:=	.T.
	Local lRet			:=	.T.
	Local cCompId		:= ""
	
	DEFAULT Self:companyId 		:= {}
	DEFAULT Self:page 			:= PAGE_DEF
	DEFAULT Self:pageSize 		:= PAGESIZE_DEF
		
	cCompId    := Self:companyId
	nPage 	   := Self:page
	nPageSize  := Self:pageSize
	
	If Empty( cCompId )
		lRet := .F.
		SetRestFault( ERRORCODE_DEF, EncodeUTF8( "Grupo, Empresa e Filial logada n�o foram informados no par�metro 'companyId'." ) )
	Else
		aCompany := StrTokArr( cCompId, "|" )
	
		If Len( aCompany ) < 2
			lRet := .F.
			SetRestFault( ERRORCODE_DEF, EncodeUTF8( "Grupo, Empresa e Filial logada n�o foram informados no par�metro 'companyId'." ) ) 
		Else
			cEmpRequest := aCompany[1]
			cFilRequest := aCompany[2]
	
			If Type( "cEmpAnt" ) == "U" .or. Type( "cFilAnt" ) == "U"
				RPCClearEnv()
				RPCSetType( 3 )
				RPCSetEnv( cEmpRequest, cFilRequest,,, "TAF" )
			
			ElseIf cEmpAnt <> cEmpRequest
				If FWFilExist( cEmpRequest, cFilRequest )
					RPCClearEnv()
					RPCSetType( 3 )
					RPCSetEnv( cEmpRequest, cFilRequest,,, "TAF" )
				Else
					lAmbiente := .F.
				EndIf
			
			ElseIf cFilAnt <> cFilRequest
				cFilAnt := cFilRequest
			EndIf
	
			If lAmbiente .and. FWFilExist( cEmpRequest, cFilRequest )
				fLoadSM0()
	            fGetBranch( @oResponse, cEmpRequest, cFilRequest, nPage, nPageSize )
	            self:SetResponse( oResponse:ToJson() )
			Else
				lRet := .F.
				SetRestFault( ERRORCODE_DEF, EncodeUTF8( "Falha na prepara��o do ambiente para a Empresa '" ) + cEmpRequest + EncodeUTF8( "' e Filial '" ) + cFilRequest + "'." )
			EndIf
		EndIf
	EndIf
	
	oResponse := Nil
	FreeObj( oResponse )
	DelClassIntF()

Return( lRet )


/*/{Protheus.doc} function fGetBranch
Retorna as filiais de acordo com os par�metros informados
@author  Totvs
@since   05/11/2019
@version 12.1.25
/*/
Static Function fGetBranch( oResponse, cCompanyId, cBranchId, nPage, nPageSize )
	Local nX        	:=  1
	Local nRegIni		:=	1
	Local nRegFim		:=	1
	Local nPosBranch	:=  0
	Local aESocBranches	:= {}
	Local cRaizCNPJ		:= ""

	nRegIni := ( ( nPage - 1 ) * nPageSize ) + 1
	nRegFim := nPage * nPageSize
	
	oResponse          := JsonObject():New()
	oResponse["items"] := {}


	//Utilizado para obter o CNPJ da filial recebida por par�metro
	nPosBranch := aScan( __aSM0, { |x| AllTrim( x[1] ) == AllTrim( cCompanyId ) .and. AllTrim( x[2] ) == AllTrim( cBranchId ) } )

	If nPosBranch > 0

		cRaizCNPJ := SubStr( __aSM0[nPosBranch][18], 1, 8 )

		//Monta a cl�usula IN com as filiais do grupo de empresas logado
		aEval( __aSM0, { |x| Iif( x[1] == cCompanyId .and. SubStr( x[18], 1, 8 ) == cRaizCNPJ, aAdd( aESocBranches, { x[1], x[2], x[7] } ) , Nil ) } )

		For nX := nRegIni To nRegFim
			If nX <= Len(aESocBranches)
				If cCompanyId == ALLTRIM(aESocBranches[nX][1])
					aAdd( oResponse["items"], JsonObject():New() )
					oResponse["items"][Len(oResponse["items"])]["branchCode"]	        := Rtrim(aESocBranches[nX][2])
					oResponse["items"][Len(oResponse["items"])]["branchDescription"]	:= EncodeUTF8( Rtrim( StrTran( Upper( aESocBranches[nX][3] ), "FILIAL", "" ) ) )
				EndIf
			EndIf
		Next nX

		oResponse["hasNext"] := Iif( Len(aESocBranches) > nRegFim, .T., .F.)

	EndIf

Return


/*/{Protheus.doc} function fLoadSM0
Realiza o carregamento da tabela SM0
@author  Totvs
@since   05/11/2019
@version 12.1.25
/*/
Static Function fLoadSM0()
	If __aSM0 == Nil
		__aSM0 := FWLoadSM0()
	EndIf
Return()