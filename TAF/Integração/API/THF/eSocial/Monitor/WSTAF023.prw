#INCLUDE "TOTVS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "RESTFUL.CH"

#DEFINE CODE_DEF	200

Static __aSocRot    := Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} WSRESTFUL EsocialMonitorHomeCards
API para obter os dados sint�ticos dos cards do Monitor do esocial

@author Leticia Campos
@since 06/05/2020
/*/
//-------------------------------------------------------------------
WSRESTFUL EsocialMonitorHomeCards DESCRIPTION "Dados sint�ticos dos cards de eventos do Monitor eSocial" FORMAT APPLICATION_JSON

    WSDATA companyId    AS STRING
    WSDATA branches	    As ARRAY OF STRING
    WSDATA events	    As ARRAY OF STRING
    WSDATA period	    AS STRING OPTIONAL
    WSDATA periodFrom	AS STRING OPTIONAL
    WSDATA periodTo	    AS STRING OPTIONAL


    WSMETHOD POST;
        DESCRIPTION "Retorna os dados para os cards de eventos do Monitor eSocial";
        WSSYNTAX "api/rh/esocial/v1/EsocialMonitorHomeCards/?{companyId}&{branches}&{period}&{events}";
        PATH "api/rh/esocial/v1/EsocialMonitorHomeCards";
        TTALK "v1";
        PRODUCES APPLICATION_JSON

END WSRESTFUL

//-------------------------------------------------------------------
/*/{Protheus.doc} POST

@description M�todo para retornar os dados de acordo com os par�metros informados
no Body. Est� sendo utilizado o m�todo POST, pois o guia de API TOTVS n�o permite
que seja passados par�metros no Body da Request no m�todo GET
@author Leticia Campos
@since 06/05/2020
/*/
//-------------------------------------------------------------------
WSMETHOD POST WSSERVICE EsocialMonitorHomeCards

    Local cRequest      := self:GetContent()
    Local cErrors       := ""
    Local cEmpRequest   := ""
    Local cFilRequest   := ""
    Local lRet          := .T.
    Local aCompany      := {}
    Local oRequest      := Nil
    Local oResponse     := Nil

    If Empty( cRequest )
        lRet    := .F.
        SetRestFault( CODE_DEF, EncodeUTF8( "Requisi��o n�o possui par�metros no corpo da mensagem." ) )
    Else
        oResponse     := JsonObject():New()
        oRequest      := JsonObject():New()
        oRequest:FromJson( cRequest )

        cErrors := checkRequest( oRequest )

        If Empty(cErrors)

            aCompany := StrTokArr( oRequest["companyId"], "|" )

            cEmpRequest := aCompany[1]
            cFilRequest := aCompany[2]

            If PrepEnv( cEmpRequest, cFilRequest )

                InitStatic()

                oResponse   := WS023Resp(oRequest)

                ::SetContentType("application/json")
                ::SetStatus(200)
                ::SetResponse( oResponse:ToJson() )
            Else
                lRet    := .F.
                SetRestFault( CODE_DEF, EncodeUTF8( "Falha na prepara��o do ambiente para a Empresa '" + cEmpRequest + "' e Filial '" + cFilRequest + "'." ) )
            EndIf
        Else
            lRet    := .F.
            SetRestFault( CODE_DEF, EncodeUTF8( cErrors ) )
        EndIf

    EndIf

Return ( lRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} WS023Resp
Utilizado para montar o objeto com a response

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
Static Function WS023Resp(oRequest)

    Local aBranches     := {}
    Local aEvents       := {}
    Local oResponse     := JsonObject():New()
    Local cPeriod       := ""
    Local cEvent        := ""
    Local cTableEvent   := ""
    Local cTypeEvent    := ""
    Local cPeriodFrom   := ""
    Local cPeriodTo     := ""
    Local cCampoTable   := ""
    Local nPosEvent     := 0
    Local nX            := 0

    aEvents     := aClone(oRequest["events"])
    aBranches   := aClone(oRequest["branches"])

    If oRequest["period"] <> NIL
        cPeriod := oRequest["period"]
    EndIf

    oResponse["items"] := {}

    For nX := 1 To Len( aEvents )
        ASORT(aEvents)

        //Limpa a cada volta do array
        cInFiliais  := ""
        cEvent      := aEvents[nX]
        nPosEvent   := aScan(__aSocRot, {|x| x[4] == cEvent })
        cTableEvent := __aSocRot[nPosEvent][3]
        cTypeEvent  := __aSocRot[nPosEvent][12]
        cCampoTable := __aSocRot[nPosEvent][6]

        cPeriodFrom := oRequest["periodFrom"]
        cPeriodTo   := oRequest["periodTo"]

        // Monta a cl�usula IN com as filiais informadas
        aEval( aBranches, { |x| cInFiliais += xFilial(cTableEvent, x) + ";" })
        cInFiliais  := Left(cInFiliais, Len(cInFiliais) -1 )
        cInFiliais := FormatIn(cInFiliais, ";")

        aAdd(oResponse["items"], JsonObject():New())

        if Empty(cCampoTable)
            cCampoTable := toFrom( cEvent )
        endIf

        WS023Event(@oResponse["items"], nX, cPeriod, cInFiliais, cTableEvent, cEvent, cTypeEvent,cCampoTable,cPeriodFrom,cPeriodTo)

    Next nX

Return( oResponse )

//-------------------------------------------------------------------
/*/{Protheus.doc} WS023Event

@description Faz a query agrupando um determinado evento por status 
e retorna por refer�ncia. 
@author Leticia Campos
@since 07/05/2020
/*/
//-------------------------------------------------------------------
Static Function WS023Event(oResponse, nNewPos, cPeriod, cInFiliais, cTableEvent, cEvent, cTypeEvent,cCampoTable,cPeriodFrom,cPeriodTo)

    Local cQuery        := ""
    Local cWorkAlias    := GetNextAlias()
    Local cFieldStatus  := cTableEvent + "_STATUS "
    Local cEventDesc    := Posicione( "C8E", 2, xFilial( "C8E" ) + cEvent, "C8E_DESPRT" )
    Local nZ		    := 1
    Local nTotEvent     := 0
    Local nPosStatus    := 1
    Local aStatus       := Array(5)
    Local aTypeStatus   := {}
    Local oStatus 		:= JsonObject():new()

    oResponse[nNewPos]["eventCode"] 		:= cEvent
    oResponse[nNewPos]["eventDescription"]	:= EncodeUTF8(AllTrim(cEventDesc))

    If cTypeEvent == "T"

        If Select(cWorkAlias) > 0
            (cWorkAlias)->( dbCloseArea() )
        EndIf

        cQuery := "SELECT COUNT(" + cTableEvent + "_PERAPU) QTD "
        cQuery += " FROM " + RetSQLName(cTableEvent)
        cQuery += " WHERE " + cTableEvent + "_FILIAL IN " + cInFiliais
        cQuery += " AND " + cTableEvent + "_ATIVO = '1' "

        If !Empty(cPeriod)
            cQuery += " AND " + cTableEvent + "_PERAPU = '" + cPeriod + "' "
        EndIf

        cQuery += " AND D_E_L_E_T_ = ' ' "

        TCQuery cQuery New Alias (cWorkAlias)

        oResponse[nNewPos]["total"]  := (cWorkAlias)->QTD

        (cWorkAlias)->( dbCloseArea() )

    Else

        If Select(cWorkAlias) > 0
            (cWorkAlias)->( dbCloseArea() )
        EndIf


        cQuery := "SELECT COUNT(" + cTableEvent + "_STATUS) QTD, "
        cQuery += cFieldStatus + " STATUS "
        cQuery += " FROM " + RetSQLName(cTableEvent)

        if cEvent == "S-2200" 
			cQuery += " C9V"
            cQuery += " INNER JOIN " + RetSqlName("CUP") + " CUP ON C9V_FILIAL = CUP_FILIAL "
            cQuery += " AND C9V_ID = CUP_ID  "
            cQuery += " AND C9V_VERSAO = CUP_VERSAO "
            cQuery += " AND CUP.D_E_L_E_T_ = ' ' "
        endIf

        cQuery += " WHERE " + cTableEvent + "_FILIAL IN " + cInFiliais
        cQuery += " AND " + cTableEvent + "_ATIVO = '1' "

        If cEvent == "S-1000"
            cQuery += " AND " + cTableEvent + "_MATRIZ = 'T' "
        EndIf

        If cEvent == "S-2200" .Or. cEvent == "S-2300"
            cQuery += " AND " + cTableEvent + "_NOMEVE = '"+ StrTran(cEvent,"-","") +"' "
        EndIf

        If cTypeEvent == "M" .And. !Empty(cPeriod)
            cQuery += " AND " + cTableEvent + "_PERAPU = '" + cPeriod + "' "
        EndIf

        If cTypeEvent == "E" .And. !Empty(cPeriodFrom) .And. !Empty(cPeriodTo) .AND. !Empty(cCampoTable)

            cQuery += " AND " + cCampoTable + " BETWEEN " + cPeriodFrom +' AND ' + cPeriodTo
        EndIf

        cQuery +=  IIF(cEvent=="S-2200", " AND C9V.D_E_L_E_T_ = ' '", " AND D_E_L_E_T_ = ' ' ")
        cQuery += " GROUP BY " + cTableEvent + "_STATUS "


        TCQuery cQuery New Alias (cWorkAlias)

        If (cWorkAlias)->( !Eof() )

            While (cWorkAlias)->( !Eof() )

                oStatus     := JsonObject():new()
                nTotEvent   += (cWorkAlias)->QTD

                // Retorna a descri��o do status encontrado com a quantidade e tipo
                Do Case
                Case (cWorkAlias)->STATUS $ " |0|1"
                    oStatus["title"] 	:= "Pendente de Envio"
                    nPosStatus          := 1

                Case (cWorkAlias)->STATUS == "2"
                    oStatus["title"] 	:= "Aguardando Governo"
                    nPosStatus          := 2

                Case (cWorkAlias)->STATUS == "3"
                    oStatus["title"] 	:= "Rejeitado"
                    nPosStatus          := 3

                Case (cWorkAlias)->STATUS == "4"
                    oStatus["title"] 	:= "Sucesso"
                    nPosStatus          := 4

                Case (cWorkAlias)->STATUS $ "6|7"
                    oStatus["title"] 	:= "Excluido"
                    nPosStatus          := 5
                EndCase

                IIf( (cWorkAlias)->STATUS $ " |0", "1", IIf((cWorkAlias)->STATUS $ "6|7", "5", (cWorkAlias)->STATUS ) )
                oStatus["type"]     := IIf( (cWorkAlias)->STATUS $ " |0", "1", IIf((cWorkAlias)->STATUS $ "6|7", "5", (cWorkAlias)->STATUS ) )
                oStatus["value"]    := (cWorkAlias)->QTD
                oStatus["warning"]  := IIf( (cWorkAlias)->STATUS $ " |0|1|3", .T., .F. )

                aStatus[nPosStatus] := oStatus

                AADD( aTypeStatus, oStatus["type"] )
                FreeObj( oStatus )

                (cWorkAlias)->( dbSkip() )

            EndDo

            // Adiciona os status vazios
            For nZ := 1 To 5
                If ASCAN( aTypeStatus, ALLTRIM( STR( nZ ) ) ) == 0
                    oStatus := JsonObject():new()

                    oStatus["title"]  := RETNAME(nZ)

                    oStatus["type"]     := cValToChar(nZ)
                    oStatus["value"]    := 0 	 // Quantidade com Status analisado
                    oStatus["warning"]  := .F.
                    aStatus[nZ]         := oStatus

                    FreeObj( oStatus )

                EndIf
            Next nZ

            // Evento(s) n�o encontrado(s)
        Else
            For nZ := 1 To 5
                oStatus := JsonObject():new()

                oStatus["title"]  := RETNAME(nZ)

                oStatus["value"]    := 0 // Quantidade do status analisado
                oStatus["type"]     := cValToChar(nZ)
                oStatus["warning"]  := .F.
                aStatus[nZ]         := oStatus

                FreeObj( oStatus )
            Next nZ

        EndIf

        oResponse[nNewPos]["status"]    := aStatus
        oResponse[nNewPos]["total"]     := nTotEvent

        (cWorkAlias)->( dbCloseArea() )

    EndIf


Return

//-------------------------------------------------------------------
/*/{Protheus.doc} InitStatic
Inicializa as vari�veis st�tics

@author Victor A. Barbosa
@since 13/08/2019
/*/
//-------------------------------------------------------------------
Static Function InitStatic()

    If __aSocRot == Nil
        __aSocRot := TAFRotinas(,,.T.,2)
    EndIf

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} checkRequest
Valida��o dos par�metros recebidos.

@author Leticia Campos
@since 13/05/2020
/*/
//-------------------------------------------------------------------
Static Function checkRequest( oRequest )

    Local cMessage  := ""

    If Empty(oRequest["companyId"])
        cMessage    += " Empresa|Filial n�o informado no par�metro 'companyId'. "
    EndIf

    If Empty(oRequest["branches"])
        cMessage    += " Filial n�o informado no par�metro 'branches'. "
    EndIf

    If Empty(oRequest["events"])
        cMessage    += " Evento n�o informado no par�metro 'events'. "
    EndIf

Return( cMessage )

Static Function RETNAME(nX)

    Local aRet as array
    aRet :={}

    aadd(aRet,"Pendente de Envio")
    aadd(aRet,"Aguardando Governo")
    aadd(aRet,"Rejeitado")
    aadd(aRet,"Sucesso")
    aadd(aRet,"Excluido")

return aRet[nX]
//-------------------------------------------------------------------
/*/{Protheus.doc} toFrom
Retorna o campo de data dos eventos n�o periodicos, para ser usado
na query quando ele n�o existe no TAFROTINAS.PRW.

@author Katielly Feitosa
@since 18/06/2020
/*/
//-------------------------------------------------------------------
Function toFrom( cEvent )

    Local cRet
    Default cEvent = ""

    If cEvent == "S-2200"
        cRet := "CUP_DTADMI"

    ElseIf cEvent == "S-2221"
        cRet := "V3B_DTEXAM"

    ElseIf cEvent == "S-2300"
        cRet := "C9V_DTINIV" 
    EndIf

return cRet