#INCLUDE "PROTHEUS.CH"
#include 'fwlibversion.ch'

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFA552
@type			function
@description	Programa de inicializa��o do Portal THF/Portinari do TAF (ESOCIAL).
@author			Flavio Lopes Rasta
@since			07/08/2019
@version		1.0
/*/
//---------------------------------------------------------------------
Function TAFA552A()

	If (GetBuild() >= "7.00.170117A-20190628")

		If (Empty( GetNewPar( "MV_BACKEND", "" ) ) .OR. Empty( GetNewPar( "MV_GCTPURL", "" ) ) .OR. !TafVldRP(.F.))


			If FWIsAdmin( __cUserID )
				If TAFFUTPAR()
					If TAFAlsInDic( "V3J" ) .and. TAFAlsInDic( "V45" )
						FWMsgRun( , { || EraseData() }, "Aguarde", "Aplicando limpeza das tabelas de requisi��es" )
					EndIf

					FWCallApp( "TAFA552" )
				EndIf
			Else
				MsgAlert("As configura��es para o funcionamento do TAF do Futuro n�o foram realizadas. Contate o administrador do sistema.")
			EndIf

		Else
			If TAFAlsInDic( "V3J" ) .and. TAFAlsInDic( "V45" )
				FWMsgRun( , { || EraseData() }, "Aguarde", "Aplicando limpeza das tabelas de requisi��es" )
			EndIf

			FWCallApp( "TAFA552" )
		EndIf
	Else
		MsgAlert("Para utilizar as funcionalidades do TAF do Futuro voc� deve atualizar o seu sistema para uma build 64 bits (Lobo Guar�).")
	EndIf

Return( .T. )

//---------------------------------------------------------------------
/*/{Protheus.doc} JsToAdvpl
@type			function
@description	Bloco de c�digo que receber� as chamadas JavaScript.
@author			Robson Santos
@since			20/09/2019
@version		1.0
/*/
//---------------------------------------------------------------------
Static Function JsToAdvpl( oWebChannel, cType, cContent )

	Local cJsonCompany	:=	""
	Local cJsonContext	:=	""
	Local cJsonTafFull	:=	""
	Local cContext		:=  ""
	Local cSourceBranch	:=	""
	Local cIsTafFull	:=	"true"
	Local cJsonTafFeat	:=  ""



	If FWIsInCallStack("TAFA552A")
		cContext := "esocial"
	ElseIf FWIsInCallStack("TAFA552B")
		cContext := "reinf"
	ElseIf FWIsInCallStack("TAFA552C")
		cContext := "gpe"
	EndIf

	Do Case

	Case cType == "preLoad"

		DBSelectArea( "C1E" )
		C1E->( DBSetOrder( 3 ) )
		If C1E->( MsSeek( xFilial( "C1E" ) + PadR( FWCodFil(), TamSX3( "C1E_FILTAF" )[1] ) + "1" ), .T. )
			cSourceBranch := C1E->C1E_CODFIL
		EndIf

		cJsonCompany	:=	'{ "company_code" : "' + FWGrpCompany() + '", "branch_code":"' + FWCodFil() + '", "source_branch":"' + cSourceBranch + '" }'
		cJsonContext	:=	'{ "context" : "'+ cContext +'" }'
		cJsonTafFull    :=  '{ "tafFull" : "'+cIsTafFull+'" }'
		cJsonCodUser    :=  '{ "codUser" : "'+RetCodUsr()+'" }'

		cJsonTafFeat	:=  GetTafFeatures()

		oWebChannel:AdvPLToJS( "setContext"	  , cJsonContext  )
		oWebChannel:AdvPLToJS( "setCompany"   , cJsonCompany  )
		oWebChannel:AdvPLToJS( "setlIsTafFull", cJsonTafFull  )
		oWebChannel:AdvPLToJS( "setCodUser"   , cJsonCodUser  )
		oWebChannel:AdvplToJS( "setFeatures"  , cJsonTafFeat  )

	Case cType == "form"

		shellExecute(cContent, "https://forms.gle/p55sbu1fzowp1Cop6", "", "", 1 )

	EndCase

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} GetTafFeatures
@type			function
@description	Fun��o respons�vel por verificar todas as features que necessitam de 
				atualiza��o de bin�rio, lib, etc e retorna um json informando se a feature encontra-se dispon�vel ou n�o.
@author			Diego Santos
@since			14/07/2020
@version		1.0
/*/
//---------------------------------------------------------------------
Static Function GetTafFeatures

	Local cBuildSmart	:= GetBuild(.T.)
	Local cBuildAppSrv	:= GetBuild(.F.)
	Local cLibVersion	:= FwLibVersion()
	Local cRet			:= ''
	Local aFeatures		:= {}
	Local nX

//Feature downloadXLS
	If cBuildSmart >= '7.00.191205P-20200504' .And. cBuildAppSrv >= '7.00.191205P-20200629' .And. cLibVersion >= '20200615' .and. FINDFUNCTION( 'FwDLExList' )
		aAdd( aFeatures, { "downloadXLS",  .T., Encode64("Teste") } )
	Else
		aAdd( aFeatures, { "downloadXLS",  .F., Encode64("Funcionalidade dispon�vel a partir do bin�rio AppServer: 7.00.191205P-20200629, Smartclient: 7.00.191205P-20200504, Lib superior � 15/06/2020 e pacote acumulado do TAF igual ou superior � 09/2020.") } )
	EndIf

// '{
// 	"feature1": { "access": true, "message": "Teste" }, 
// 	"feature2": { "access": false, "message": "Teste2" }
// 	"feature3": { "access": false, "message": "Teste3" }
// }'

	cRet += ' { '
	For nX := 1 To Len(aFeatures)

		cRet += '"' + aFeatures[nX][1] + '": { "access": ' + Iif(aFeatures[nX][2], 'true', 'false') + ', "message": "' + aFeatures[nX][3] + '" } '

		If Len(aFeatures) > 1 .And. nX <> Len(aFeatures)
			cRet += ','
		EndIf

	Next nX
	cRet += ' } '

Return cRet

//---------------------------------------------------------------------
/*/{Protheus.doc} EraseData
@type			function
@description	Exclui os dados vol�teis dos relat�rios.
@author			Robson Santos
@since			20/09/2019
@version		1.0
/*/
//---------------------------------------------------------------------
Static Function EraseData

	Local cQuery	:=	""
	Local cDate		:=	DToS( dDataBase )

	cQuery := "DELETE FROM " + RetSqlName( "V45" ) + " "
	cQuery += "WHERE V45_ID IN ( SELECT V3J_ID FROM " + RetSqlName( "V3J" ) + " WHERE V3J_DTREQ < '" + cDate + "' )

	TCSQLExec( cQuery )

	cQuery := "DELETE FROM " + RetSqlName( "V3J" ) + " "
	cQuery += "WHERE V3J_DTREQ < '" + cDate + "' "

	TCSQLExec( cQuery )

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFA552B
@type			function
@description	Programa de inicializa��o do Portal THF/Portinari do TAF (REINF).
@author			Robson Santos
@since			26/11/2019
@version		1.0
/*/
//---------------------------------------------------------------------
Function TAFA552B()

	If (GetBuild() >= "7.00.170117A-20190628")
		If (Empty( GetNewPar( "MV_BACKEND", "" ) ) .OR. Empty( GetNewPar( "MV_GCTPURL", "" ) ) .OR. !TafVldRP(.F.))
			If FWIsAdmin( __cUserID )
				If TAFFUTPAR()
					FWCallApp( "TAFA552" )
				EndIf
			Else
				MsgAlert("As configura��es para o funcionamento do TAF do Futuro n�o foram realizadas. Contate o administrador do sistema.")
			EndIf
		Else
			FWCallApp( "TAFA552" )
		EndIf
	Else
		MsgAlert("Para utilizar as funcionalidades do TAF do Futuro voc� deve atualizar o seu sistema para uma build 64 bits (Lobo Guar�).")
	EndIf

Return( .T. )


//---------------------------------------------------------------------
/*/{Protheus.doc} TAFA552B
@type			function
@description	Programa de inicializa��o do Portal THF/Portinari do TAF (GPE).
@author			Robson Santos
@since			26/11/2019
@version		1.0
/*/
//---------------------------------------------------------------------
Function TAFA552C()

	If (GetBuild() >= "7.00.170117A-20190628")
		If (Empty( GetNewPar( "MV_BACKEND", "" ) ) .OR. Empty( GetNewPar( "MV_GCTPURL", "" ) ) .OR. !TafVldRP(.F.))
			If FWIsAdmin( __cUserID )
				If TAFFUTPAR()
					FWCallApp( "TAFA552" )
				EndIf
			Else
				MsgAlert("As configura��es para o funcionamento do TAF do Futuro n�o foram realizadas. Contate o administrador do sistema.")
			EndIf
		Else
			FWCallApp( "TAFA552" )
		EndIf
	Else
		MsgAlert("Para utilizar as funcionalidades do TAF do Futuro voc� deve atualizar o seu sistema para uma build 64 bits (Lobo Guar�).")
	EndIf

Return( .T. )
