#INCLUDE "PROTHEUS.CH" 
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TOPCONN.CH"

#DEFINE TAMMAXXML 075000  //Tamanho Maximo do XML
#DEFINE TAMMSIGN  004000  //Tamanho m�dio da assinatura 

static oModel497 as Object
static oModel501 as Object


//----------------------------------------------------------------------------
/*/{Protheus.doc} TAFProc10
Chama rotina responsavel por verificar os registros que devem ser 
Consultados
      
@return Nil 

@author Evandro dos Santos Oliveira
@since 07/11/2013 - Alterado 18/05/2015
@version 1.0
@obs - Rotina separada do fonte TAFAINTEG e realizado tratamentos especificos
		para a utiliza��o do Job5 realizando a chamada individualmente e utilizando
		o schedDef para a execu��o no schedule.
/*/
//----------------------------------------------------------------------------  
Function TAFProc10( aAlias , aEvt  )

Local lJob as logical
Local lEnd as logical

Default aAlias := {} 
Default aEvt   := {}

lJob := .F.
lEnd := .F.

lJob := IsBlind()
If TAFAtualizado(!lJob)
	ConOut('Rotina de Monitoramento de eventos REINF - Empresa: ' + cEmpAnt + ' Filial: ' + cFilAnt)

	If lJob
		TAFProc10TSS(lJob,aEvt,/*3*/,/*4*/,/*5*/,@lEnd,/*7*/,/*8*/,/*9*/,/*10*/,/*11*/,/*12*/,aAlias,/*14*/)
	Else
		Processa( {||TAFProc10TSS(lJob,,,,,@lEnd)}, "Aguarde...", "Executando rotina de Transmiss�o",  )
	EndIf

	If lEnd .And. !lJob
		MsgInfo("Processo finalizado.")
	EndIf
EndIf
	
Return Nil 

//---------------------------------------------------------------------------
/*/{Protheus.doc} TAFProc10Tss 
Processo responsavel por verificar os registros que devem ser consultados no
TSS.

Altera��o: Evandro dos Santos
Data: 05/04/2016 
Descri��o: - Alterado a forma de gera��o dos registros, para a rotina possibilitar
a gera��o de XMLs em disco, foram incluidos uma s�rie de par�metros que permitem 
a gera��o de layouts especificos e filtros por status, recno e Id.
- Alterado a Origem do array dos layouts, antes os mesmos eram baseados no array 
aTafSocial deste fonte, agora os layouts considerados s�o os especificados no
fonte TAFROTINAS.

@param	lJob - Flag para Identifica��o da chamada de Fun��o por Job
@param 	aEvtsESoc 	- Array com os Eventos a serem considerados, quando vazio s�o considerados 
		todos os eventos contidos no TAFROTINAS. 
		Obs: Quando informados os eventos devem seguir a mesma estrutura dos eventos e-Social
		contidos no TAFROTINAS.
@param 	cStatus - Status dos eventos que devem ser transmitidos, quando vazio  o sistema usa o 0 
        para tranmiss�o e o 2 para consulta; o par�metro pode conter mais de 1 status par isso
        passar os status separados por virgula ex: "1,3,4"	
@param aIdTrab - Array com o Id dos trabalhadores (para filtro dos eventos que tem rela��o com o 
	    trabalhador)
@param cRecNos - Filtra os registro pelo RecNo do Evento, pode ser utilizado um range de recnos
		ex;"1,5,40,60"
@param lEnd - Verifica o fim do processamento(vari�vel referenciada) 
@param cMsgRet - Mensagem de retorno do WS (refer�ncia)
@param aFiliais - Array de Filiais
@param dDataIni	-> Data Inicial dos eventos
@param dDataFim	-> Data Fim dos dos eventos    
@param lEvtInicial -> Informa se o par�metro de evento inicial foi marcado. 	
@param lCommit -> Indica se ser� comitado na tabela

@return Nil 

@author Evandro dos Santos Oliveira
@since 07/11/2013
@version 1.0
/*/
//---------------------------------------------------------------------------    
Function TAFProc10Tss(lJob, aEvtsReinf, cStatus, aIdTrab, cRecNos, lEnd, cMsgRet,aFiliais,dDataIni,dDataFim,lEvtInicial,lCommit,aRecREINF,cIdEnt)
//Function TAFProc9Tss(lJob , aEvtsReinf, cStatus, cPathXml, aIdTrab, cRecNos, lNoErro, cMsgRet,lForce,aFiliais,dDataIni,dDataFim,lEvtInicial,aRecREINF)

Local nTopSlct 		as numeric
Local cQry			as char
Local cMaxRegs 		as char
Local lMultSts 		as logical
Local nX,nY	 		as numeric
Local nQtdRegs		as numeric
Local cFunction		as char
Local cAliasRegs	as char
Local cXml			as char
Local cMsg			as char
Local cAliasTb  	as char
Local cLayout		as char
Local cIdTrab		as char
Local lEvtTrb		as logical
Local cId			as char
Local cTabOpen		as char
Local cRegNode		as char
Local nByteXML		as numeric
Local oXml			as object
Local cMsgProc   	as char
Local aRetorno		as array
Local cAmbte		as char
Local cBancoDB 		as char
Local cUrl			as char
Local cCheckURL		as char
Local cLog			as char
Local cTimeProc		as char
Local cAliasEve		as char
Local cIdThread		as char
Local lAllEventos	as logical
Local nQtdPorLote	as numeric
Local aXmlsLote		as array
Local nItem			as numeric
Local aXmls			as array 

Default cStatus 	:= ""
Default aEvtsReinf	:= {}
Default aIdTrab 	:= {}
Default aRecREINF	:= {}
Default cRecNos		:= ""
Default cMsgRet 	:= ""
Default lEnd		:= .T.
Default dDataIni	:= dDataBase
Default dDataFim	:= dDataBase
Default lCommit		:= .T.

nTopSlct 		:= 999999
cQry			:= ""
cMaxRegs 		:= ""
lMultSts 		:= .F.
nX		 		:= 0
nY		 		:= 0
nQtdRegs		:= 0
cFunction		:= ""
cAliasRegs		:= GetNextAlias()
cXml			:= ""
cMsg			:= ""
cAliasTb  		:= ""
cLayout			:= ""
cIdTrab			:= ""
lEvtTrb			:= .F.
cId				:= ""
cTabOpen		:= ""
cRegNode		:= ""
cAliasEve		:= ""
nByteXML		:= 0
oXml			:= Nil 
cMsgProc   		:= ""
aRetorno		:= {}
cAmbte			:= SuperGetMv('MV_TAFAMBR',.F.,"2")
cBancoDB 		:= Upper(AllTrim(TcGetDB()))
cUrl			:= GetMv("MV_TAFSURL")
cTimeProc		:= Time() 
cIdThread	 	:= StrZero(ThreadID(), 10 )
nQtdPorLote		:= 50
aXmls			:= {}
aXmlsLote		:= {}
nItem 			:= 0

If Empty(AllTrim(cUrl))
	If lJob
		ConOut("O par�metro MV_TAFSURL n�o est� preenchido")
	Else
		cMsgRet := "O par�metro MV_TAFSURL n�o est� preenchido"
	EndIf
Else

	If lJob
		cLog := "* Inicio Consulta TAFProc5 TheadId: " + cIdThread + " - Data de Inicio: " + DTOC(dDataBase) + " - " + cTimeProc
		ConOut(cLog)
	EndIf

	If !("TSSWSREINF.APW" $ Upper(cUrl)) 
		cCheckURL := cUrl
		cUrl += "/TSSWSREINF.apw"
	Else
		cCheckURL := Substr(cUrl,1,Rat("/",cUrl)-1)
	EndIf	
	
	If TAFCTSpd(cCheckURL)
	
		cStatus 	:= IIf(Empty(cStatus),'2',cStatus)
		cAliasEve	:= AllTrim( aEvtsReinf[3] )
		lAllEventos	:= Empty(aRecREINF) //Quando n�o vem eventos selecionados devo considerar todos por que n�o houve marca��o no browse

		If Len(aRecREINF) > 0
			cAliasEve := AllTrim( aEvtsReinf[3] )

			For nX:= 1 to Len(aRecREINF)
				( cAliasEve )->(dbGoTo( aRecREINF[nX] ) )

				cId := AllTrim ( STRTRAN( aEvtsReinf[4] , "-" , "" ) ) + &(AllTrim(cAliasEve)+"->"+AllTrim(cAliasEve)+"_ID") + &(AllTrim(cAliasEve)+"->"+AllTrim(cAliasEve)+"_VERSAO")
				aAdd(aXmls,{"",cId, aRecREINF[nX], AllTrim( aEvtsReinf[4] ) , cAliasEve})
				nItem++

				If nItem == nQtdPorLote
					aAdd(aXmlsLote,aClone(aXmls))
					aSize(aXmls,0)
					nItem := 0
				EndIf
			Next

			//Se houver, adiciono o residuo no array de lote
			If Len(aXmls) > 0
				aAdd(aXmlsLote,aClone(aXmls))
				aSize(aXmls,0)
			EndIf
		EndIf

		If Len(aXmlsLote) > 0
		  	aRetorno := TAFConRg(aXmlsLote,cAmbte,,cUrl,lJob,cIdEnt)
		EndIf

/*
		// Tratamento para funcionalidade via Job/Schedule 
		If !lJob                        
		//	ProcRegua(Len(aEvtsESoc))	
		EndIf 
		
		cMsgProc := "Verificando itens transmitidos para o RET. "

		If lJob
			cQry := TAFQryXMLeSocial(cBancoDB,nTopSlct,,cStatus,aEvtsESoc,aIdTrab,cRecNos, cMsgProc,,aFiliais,,lJob,,lCommit)
		Else
			cQry := TAFQryMonTSS(cBancoDB,nTopSlct,,cStatus,aEvtsESoc,aIdTrab,cRecNos, cMsgProc,,aFiliais,lAllEventos,dDataIni,dDataFim,lEvtInicial)
		EndIf
		
		cQry := ChangeQuery(cQry) 

		TcQuery cQry New Alias (cAliasRegs)
		Count To nQtdRegs
		
		If nQtdRegs > 0
			(cAliasRegs)->(dbGoTop())	
			While (cAliasRegs)->(!Eof())
			
				If AliasInDic((cAliasRegs)->ALIASEVT)
				
					cFunction := (cAliasRegs)->FUNCXML
					
					If !((cAliasRegs)->ALIASEVT $ cTabOpen)
						dbSelectArea((cAliasRegs)->ALIASEVT)
						cTabOpen += "|" + (cAliasRegs)->ALIASEVT 
					EndIf
					
					((cAliasRegs)->ALIASEVT)->(dbGoTo((cAliasRegs)->RECTAB))
					cId := STRTRAN((cAliasRegs)->LAYOUT,"-","") + aLLtRIM((cAliasRegs)->ID) + AllTrim((cAliasRegs)->VERSAO)
					aAdd(aXmls,{"",cId,(cAliasRegs)->RECTAB,(cAliasRegs)->LAYOUT,(cAliasRegs)->ALIASEVT})
					nItem++

					If nItem == nQtdPorLote
						aAdd(aXmlsLote,aClone(aXmls))
						aSize(aXmls,0)
						nItem := 0
					EndIf

				EndIf
				(cAliasRegs)->(dbSkip())
			EndDo

			//Se houver, adiciono o residuo no array de lote
			If Len(aXmls) > 0
				aAdd(aXmlsLote,aClone(aXmls))
				aSize(aXmls,0)
			EndIf

		Else
			cMsgProc := "0 documento(s)  consultado(s)."
			ConOut(cMsgProc)
		EndIf
		
		If Len(aXmlsLote) > 0
		  	aRetorno := TAFConRg(aXmlsLote,cAmbte,,cUrl,lJob)
		EndIf
		
		If lJob
			cTimeProc := Time()
			cLog := "* Fim Consulta TAFProc5 TheadId: " + cIdThread + " - Data de Inicio: " + DTOC(dDataBase) + " - " + Time() + " - Tempo de processamento: " + ElapTime(cTimeProc,Time())  + " - Quantidade de Registros: " + AllTrim(Str(nQtdRegs))
			conOut(cLog)
		EndIf
*/
	Else
		If lJob
			ConOut("N�o foi possivel conectar com o servidor TSS")
		Else
			cMsgRet := "N�o foi possivel conectar com o servidor TSS"
		EndIf
	EndIf
EndIf

Return (aRetorno)

//---------------------------------------------------------------------------
/*/{Protheus.doc} TAFConRg  
Realiza consulta dos registros transmitidos.

@param	aXmlsLote  	- Array com os dados do Xml    
		cAmbiente	- Ambiente de Transmiss�o/Consulta 		  
					  [x][1] - Xml do Evento
					  [x][2] - Id(chave para transmiss�o)
					  [x][3] - RecNo do Evento na sua respectiva tabela
					  [x][4] - Layout que correspondente ao evento
					  [x][5] - Alias correspondente ao Evento
		lGrvRet		- Determina se deve ocorrer a grava��o dos status    
		cUrl		- Url - Url do servidor TSS para o Ambiente e-Social
		lJob		- Identifica se a rotina est� sendo executada por Job ou tela

@author Evandro dos Santos Oliveira
@since 19/11/2013
@version 1.0
/*/
//---------------------------------------------------------------------------    
Static Function TAFConRg(aXmlsLote,cAmbiente,lGrvRet,cUrl,lJob,cIdEnt,cEvento)

Local oReinf 		as object
Local lRet			as logical
Local cStatus   	as char
Local cUserTk		as char
Local cMsgRet		as char
Local oHashXML		as object
Local cIdAux		as char
Local aRetorno  	as array
Local aAreaC9V		as array
Local cAuxSts		as char
Local aCampos		as array
Local cExclCmp		as char
Local cAliasTb		as char
Local aInfRegs		as array
Local cTabOpen 		as char
Local cLayOut		as char
Local cRecAnt		as char 
Local cRecibo		as char
Local cFilErp		as char
Local cPerApur		as char
Local nQtdPorLote	as numeric
Local nNumLote	    as numeric
Local nQtdLotes		as numeric 
Local nY			as numeric 
Local nItemLote		as numeric
Local nLote 		as numeric 
Local aLoteRetorno  as array
Local dDateProc		as Date
Local xRetXML		

Default cAmbiente	 := ""
Default aXmlsLote    := {}
Default lGrvRet 	 := .T.  

Private aRetXML  as array

oReinf	 	 := Nil
lRet		 := .F.
cStatus   	 := ""
cIdEnt		 := TAFRIdEnt(,,,,,.T.)
cUserTk		 := ""
cMsgRet		 := ""
oHashXML	 := Nil 
cIdAux		 := ""
aRetorno  	 := {}
aAreaC9V	 := {}
cAuxSts		 := ""
aCampos		 := {}
cExclCmp	 := ""
cAliasTb	 := ""
aInfRegs	 := {}
cTabOpen 	 := ""
cLayOut		 := ""
cRecAnt		 := ""
cRecibo		 := ""
cFilErp 	 := ""
cPerApur     := ""
dDateProc    := CtoD("  /  /    ")
nY			 := 0
nItemLote	 := 0
nNumLote     := 0
nTotEventos  := 0
nLote 		 := 0
nQtdPorLote	 := 50
aLoteRetorno := {}
aRetXML		 := {}
xRetXML		 := Nil

cUserTk := "TOTVS" 

nQtdLotes := Len(aXmlsLote)

If lJob
	ConOut("Quantidade de Lotes a serem enviados: " + AllTrim(Str(nQtdLotes)))
Else
	ProcRegua(Len(aXmlsLote))
EndIf

dbSelectArea("T0X")
T0X->(dbSetOrder(3))

dbSelectArea("C1E")
C1E->(dbSetOrder(3))
C1E->(MsSeek(xFilial("C1E")+cFilAnt+"1"))
cFilErp := AllTrim(C1E->C1E_CODFIL) 

oModel497 := FWLoadModel("TAFA497")	
oModel501 := FWLoadModel("TAFA501")

For nLote := 1 To nQtdLotes

	oReinf 											:= WSTSSWSREINF():New() 
	oReinf:oWSREINFCONSULTA:oWSCABEC				:= WsClassNew("TSSWSREINF_REINFCABECCONSULTA")
	oReinf:_Url 									:= cUrl
	oReinf:oWSREINFCONSULTA:oWSCABEC:cENTIDADE		:= cIdEnt
	oReinf:oWSREINFCONSULTA:oWSCABEC:cUSERTOKEN		:= cUserTk
	oReinf:oWSREINFCONSULTA:oWSCABEC:cAMBIENTE		:= cAmbiente
//	oReinf:oWSREINFCONSULTA:oWSCABEC:lHISTPROC		:= .T.
	oReinf:oWSREINFCONSULTA:oWSCABEC:lRETORNAXML	:= .T.
	
	oReinf:oWSREINFCONSULTA:oWSEVENTOS	:= WsClassNew("TSSWSREINF_ARRAYOFREINFID")
	oReinf:oWSREINFCONSULTA:oWSEVENTOS:oWSREINFID :={} 

	xTAFMsgJob("Processando Lote: " + AllTrim(Str(nLote)) + "/" +  AllTrim(Str(nQtdLotes))) 

	For nItemLote := 1 To Len(aXmlsLote[nLote])

		aAdd(oReinf:oWSREINFCONSULTA:oWSEVENTOS:oWSREINFID,WsClassNew("TSSWSREINF_REINFID"))
		Atail(oReinf:oWSREINFCONSULTA:oWSEVENTOS:oWSREINFID):CID := aXmlsLote[nLote][nItemLote][2] 

	Next nItemLote 
																															
	lRet := oReinf:CONSULTAREVENTOS() 
	If ValType(lRet) == "L"   

		If lRet

			oHashXML	:=	AToHM(aXmlsLote[nLote], 2, 3 )
			
			aLoteRetorno := oReinf:oWSCONSULTAREVENTOSRESULT:oWSREINFRETCONSULTA
			
			If (lGrvRet)	
				For nY := 1 To Len(aLoteRetorno)
	
					cIdAux := AllTrim(aLoteRetorno[nY]:CID) 
					HMGet( oHashXML , cIdAux ,@xRetXML )	
					
					If ValType(xRetXML[1][3]) == "N"
					
						cAliasTb := xRetXML[1][5]
						If !(cAliasTb $ cTabOpen)
							cTabOpen += "|" + cAliasTb
						EndIf
						
						dbSelectArea(cAliasTb)
						(cAliasTb)->(dbGoTo(xRetXML[1][3]))
						
						BEGIN TRANSACTION
						
						cLayOut := xRetXML[1][4]  
						//dbGoTo(xRetXML[1][3])
//						If aLoteRetorno[nY]:LSUCESSO
							
							// |Status de Retorno dos Documentos	 
							// |
							//1 - Recebido
							//2 - Assinado
							//3 - Erro de schema
							//4 - Aguardando transmiss�o
							//5 - Rejei��o
							//6 - Autorizado
					
							cStatus :=  aLoteRetorno[nY]:CSTATUS 
							
							//Retorno do N�mero do Recibo de Transmiss�o do TSS.
							cRecibo		:= AllTrim(aLoteRetorno[nY]:CRECIBO)
							dDateProc	:= aLoteRetorno[nY]:DDTPROC

							If cStatus == "6"

								limpaRegT0X(cIdAux)

							ElseIf cStatus == "5"
							
								limpaRegT0X(cIdAux)
							EndIf
							
							cAuxSts := TAFStsXTSS(cStatus) 
	
							//Gravo o status do registro de retorno
							If !Empty(cAuxSts) //.And. (cAliasTb)->&(cAliasTb+"_STATUS") <> "4"
								RecLock((cAliasTb),.F.)
								(cAliasTb)->&(cAliasTb+"_STATUS") := cAuxSts 
								
								//Gravo o numero do protocolo de transmiss�o do TSS.
								If !Empty(cRecibo)
									(cAliasTb)->&(cAliasTb+"_PROTUL") := cRecibo 
								EndIf
								
								(cAliasTb)->(MsUnlock())	

								//Armazeno o n�mero do protocolo anterior
								cRecAnt := (cAliasTb)->&(cAliasTb+"_PROTPN")

								If TAFColumnPos( cAliasTb+"_PERAPU" )
									cPerApur := (cAliasTb)->&(cAliasTb+"_PERAPU")
								EndIf
								
								//Retorno para tratar tipo
								aRetXML := aLoteRetorno[nY]

								If cLayOut $ "R-2010|R-2020|R-2030|R-2040|R-2050|R-2060|R-3010|R-2099|R-2098|R-4099|R-4098" .And. cStatus == "6" .And. Type("aRetXML:CXMLRETEVEN") <> "U"
									GeraEvtTot(aRetXML:CXMLRETEVEN, cLayOut, cAliasTb, (cAliasTb)->&(cAliasTb+"_FILIAL"), cRecibo, cRecAnt, aRetXML:CVERSAO)
									IF cLayOut $ "R-2099|R-2098" 
										Taf503Grv(cLayOut, cPerApur, dDateProc, Time(), cRecibo,"20")
									Elseif  cLayOut $ "R-4099|R-4098" 
										Taf503Grv(cLayOut, cPerApur, dDateProc, Time(), cRecibo,"40")
									EndIf	
								EndIf
								If cLayOut $ "R-9000" .And. cStatus == "6" .And. Type("aRetXML:CXMLRETEVEN") <> "U"
									cEvento := Posicione( "T9B", 1, xFilial( "T9B" ) + T9D->T9D_IDTPEV, "T9B_CODIGO" )
									cProtEvt:= T9D->T9D_NRRECI
									cProtExc:= T9D->T9D_PROTUL
									cMsgRet += TafSetEvExc( cEvento , cProtEvt , cStatus , cProtExc )[02] 
								EndIf

							EndIf
 /*
						Else
							cMsgRet := "Layout " + cLayOut + " - Id " + cIdAux  + " Problemas no Arquivo: " 
							cMsgRet += aLoteRetorno[nY]:CDETSTATUS
							RecLock((cAliasTb),.F.)
							(cAliasTb)->&(cAliasTb+"_STATUS") := '3' 
							(cAliasTb)->(MsUnlock())	
							Conout(cMsgRet)
						EndIf
*/						
							END TRANSACTION
					Else
						cMsgRet := "Id " + cIdAux +" n�o encontrado no lote de envio. "
					EndIf
				Next nY
			EndIf
		Else
			cMsgRet := IIf(Empty(GetWscError(3)),GetWscError(1),GetWscError(3)) //SOAPFAULT
		EndIf
	Else
		cMsgRet := "Retorno do WS n�o � do tipo l�gico."
		cMsgRet += CRLF + IIf(Empty(GetWscError(3)),GetWscError(1),GetWscError(3))
	EndIf

	oSocial  := Nil 
	oHashXML := Nil 
	aAdd(aRetorno,aClone(aLoteRetorno))
	aSize(aLoteRetorno,0)
	
Next nLote

oModel497:Destroy()
oModel497 := Nil

oModel501:Destroy()
oModel501 := Nil

aSize(aXmlsLote,0)

If !Empty(cMsgRet)
	ConOut(cMsgRet)
EndIf

Return aRetorno 

//---------------------------------------------------------------------
/*/{Protheus.doc} TafSetEvExc

Atualiza registros excluidos de acordo com o retorno do S-3000
Obs. O registro S-3000 deve estar posicionado.

@Param  cStatus - Status de retorno do TSS
@return x - [1] - Status da grava��o (logico)
		    [2] - Descri��o efetividade da grava��o

@Author		Evandro dos Santos O. Teixeira
@Since		13/08/2017
@Version	1.0
/*/
//---------------------------------------------------------------------
Static Function TafSetEvExc( cEvtExcluido , cProtEvt , cStatus , cProtExc )

	Local cAliasExclu  	as char
	Local nOrdRecibo   	as numeric 
	Local aEvtExc	 	as array
	Local lGravaOk		as logical
	Local cMsgRet		as char
	Local Nx 			as numeric
	Local aArea			as array

	Default	cEvtExcluido	:= ""
	Default	cProtEvt		:= ""
	Default	cStatus			:= ""
	Default	cProtExc		:= ""

	Nx 		 	:= 1	
	lGravaOk 	:= .T.
	cMsgRet		:= ""
	cMsgErro 	:= ""
	aEvtExc		:= TAFRotinas(cEvtExcluido,4,.F.,5)
	aArea 		:= GetArea()

	If Len( aEvtExc ) > 1
		cAliasExclu  := aEvtExc[3]
		nOrdRecibo	 := aEvtExc[13]
		
		cQryExc := "SELECT R_E_C_N_O_, "+cAliasExclu+"_ID FROM "+RetSqlName(cAliasExclu)+ " WHERE D_E_L_E_T_ = ' ' AND ("+cAliasExclu+"_PROTPN ='"+cProtEvt+"' OR "+cAliasExclu+"_PROTUL ='"+cProtEvt+"' )"
		aRecnos := TafQryarr( cQryExc )

		DbSelectArea(cAliasExclu)
		For Nx := 1 To Len( aRecnos )
			(cAliasExclu)->( DbGoTo( aRecnos[Nx][01] ) )
			If (cAliasExclu)->&(cAliasExclu+"_EVENTO") == "E" .And. (cAliasExclu)->&(cAliasExclu+"_STATUS") == "6"
				RecLock( cAliasExclu, .F. )
				(cAliasExclu)->&(cAliasExclu+"_STATUS") := TAFStsEXTSS(cStatus)
				(cAliasExclu)->&(cAliasExclu+"_PROTUL") := cProtExc
				(cAliasExclu)->(MsUnlock())
				cMsgRet := "Atualiza��o do Status de exclus�o do evento " + cEvtExcluido + " realizado com sucesso."
			Else
				lGravaOk := .F.
				cMsgRet := "Evento n�o encontrado para atualiza��o do Status de exclus�o."
			EndIf
		Next
	Else
		lGravaOk := .F.
		cMsgRet := "Evento n�o encontrado para atualiza��o do Status de exclus�o."
	EndIf
	RestArea( aArea ) 

Return( { lGravaOk, cMsgRet} )

//-------------------------------------------------------------------
/*/{Protheus.doc} GeraEvtTot
Gera��o dos eventos totalizadores (5001|5011) no retorno do 2010/2020/2030
@Return  Mensagem com status 
@author Victor Andrade
@since  17/05/2016
@version 1.0

/*///----------------------------------------------------------------
Static Function GeraEvtTot( cXmlTot as character, cLayout as character, cAliasTb as character, cFilErp as character, cRecibo as character, cRecAnt as character, cVsReinf as character, lJob as logical )

	Local nX	  		as numeric
	Local cErrorXML 	as character
	Local cWarningXML 	as character
	Local cMsgRetorno	as character
	
	Default cXmlTot  := ""
	Default cLayout  := ""
	Default cFilErp	 := ""
	Default cAliasTb := "C1E"
	Default cRecAnt	 := ""
	Default lJob	 := .F.

	Private oXmlTot 	as object

	nX := 0
	oXmlTot := Nil
	cErrorXML := ""
	cWarningXML := ""
	cMsgRetorno := ""
	
	// --> Faz o "parse" do XML para pegar somente o bloco do eSocial, pois a tag possui o retorno do governo completo
	oXmlTot := XmlParser( cXmlTot,"", @cErrorXML, @cWarningXML )

	If Empty(cErrorXML) .And. oXmlTot <> Nil
		If cLayOut $ "R-2099|R-4099" .And. TAFColumnPos( "V0F_VLRSUS" )
			cMsgRetorno := grvTotFecha(oXmlTot,cFilErp,cLayout,cRecibo, cRecAnt, cVsReinf, lJob) + CRLF
		Else
			If Type("oXmlTot:_REINF:_EVTTOTAL") <> "U" .And. !(cLayOut $ "R-2098|R-4098")
				cMsgRetorno := gravaTotalizador(oXmlTot:_REINF:_EVTTOTAL,cFilErp,cLayout,cRecibo, cRecAnt, cVsReinf) + CRLF
			EndIf
		EndIf
	Else
		cMsgRetorno := cErrorXML
	EndIf

Return (cMsgRetorno)

//-------------------------------------------------------------------
/*/{Protheus.doc} gravaTotalizador
Grava evento totalizador utilizado a API de integra��o TafPrepIntrava evento totalizador utilizado a API de integra��o TafPrepInt
@Return  aRetInt - Retorno da grava��o do totalizador
			[1] - logico - determin	a se a mensagem � de uma integra��o bem sucedida
			[2] - caracter - status do registro (codigo utilizado na TAFXERP)
				1 - Incluido
				2 - Alterado
				3 - Excluido
				4 - Aguardando na Fila
				8 - Filhos Duplicado
				9 - Erro
			[3] - Codigo do Erro
			[4] - Descri��o da Mensagem de Integra��o
@author Evandro dos Santos Oliveira	
@since  19/08/2017
@version 1.0
/*///----------------------------------------------------------------
Static Function gravaTotalizador(oXmlTot,cFilErp,cLayout,cRecibo, cRecAnt, cVsReinf)

	Local cXMLConv		as char
	Local cEvento		as char 
	Local cEvtTot		as char
	Local cErro			as char
	Local cId			as char
	Local cPerApur		as char
	Local cProTPN		as char
	Local cSeq			as char 
	Local cVerAnt		as char
	Local dDtProcess	as date
	Local aRetInt		as array
	Local lNewTot		as logical
	Local lVSup13		as logical
	Local nInfoTotal	as numeric
	Local nPosIni		as numeric
	Local nRegOcorrs	as numeric
	//Local oModel 		as object
	Local oModelV0X		as object
	Local oModelV0Y 	as object
	Local oModelV0Z 	as object
	Local lIdeEstab     as Logical // tags de identifica��o do estabelecimento -> REINF v 1.4.0 ( tpInsc e nrInsc )

	Private aInfo       as array
	Private oXmlInfoTot	as object

	Default cRecAnt := ""

	aInfo		:= {}
	cXMLConv	:= ""
	cEvento		:= "I"
	cEvtTot		:= ""
	cErro		:= ""
	cId			:= ""
	cPerApur	:= ""
	cProTPN		:= ""
	cSeq		:= "001"
	cVerAnt		:= ""
	dDtProcess	:= CTOD("  /  /    ")
	aRetInt		:= {}
	lNewTot		:= TAFColumnPos("V0W_CRTOM")
	nPosIni		:= 0
	nRegOcorrs	:= 0 
	//oModel 		:= FWLoadModel("TAFA501")
	oModelV0X 	:= oModel501:GetModel("MODEL_V0X")
	oModelV0Y 	:= oModel501:GetModel("MODEL_V0Y")
	oModelV0Z	:= oModel501:GetModel("MODEL_V0Z")
	lVSup13     := IIf( "1_03" $ AllTrim(cVsReinf), .F., .T. )
	lIdeEstab   := IIf( TafColumnPos( "V0W_TPINSE" ) .and. TafColumnPos( "V0W_NRINSE" ), .T., .F. )
	oXmlInfoTot := oXmlTot

	cPerApur	:= SubStr(oXmlInfoTot:_ideEvento:_perApur:TEXT,6,2)+SubStr(oXmlInfoTot:_ideEvento:_perApur:TEXT,1,4)
	dDtProcess	:= StoD(SubStr(oXmlInfoTot:_infoRecEv:_dhProcess:TEXT,1,4)+SubStr(oXmlInfoTot:_infoRecEv:_dhProcess:TEXT,6,2)+SubStr(oXmlInfoTot:_infoRecEv:_dhProcess:TEXT,9,2))

	V0W->(DbSetOrder(5))
	If !Empty(cRecAnt) .And. V0W->(DbSeek(xFilial("V0W") + cRecAnt))
		FAltRegAnt('V0W', '2', .F.)
		cVerAnt := V0W->V0W_VERSAO
		cProTPN	:= cRecAnt
		cId 	:= V0W->V0W_ID
		cEvento := 'A'
	EndIf 

 	oModel501:SetOperation(MODEL_OPERATION_INSERT)
	oModel501:Activate()

	oModel501:GetModel( 'MODEL_V0W' )
	oModel501:LoadValue('MODEL_V0W', "V0W_FILIAL"	, xFilial("V0W"))

	If !Empty(cId)
		oModel501:LoadValue('MODEL_V0W', "V0W_ID"	, cId)
	EndIf 

	oModel501:LoadValue('MODEL_V0W', "V0W_VERSAO"	, xFunGetVer()) 
	oModel501:LoadValue('MODEL_V0W', "V0W_PERAPU"	, cPerApur )
	oModel501:LoadValue('MODEL_V0W', "V0W_CODRET"	, oXmlInfoTot:_ideRecRetorno:_ideStatus:_cdRetorno:TEXT)
	oModel501:LoadValue('MODEL_V0W', "V0W_DSCRET"	, oXmlInfoTot:_ideRecRetorno:_ideStatus:_descRetorno:TEXT)
	oModel501:LoadValue('MODEL_V0W', "V0W_DTPROC"	, dDtProcess)
	oModel501:LoadValue('MODEL_V0W', "V0W_HRPROC"	, SubStr(Time(),1,2)+SubStr(Time(),4,2))
	oModel501:LoadValue('MODEL_V0W', "V0W_TPEVEN"	, oXmlInfoTot:_infoRecEv:_tpEv:TEXT)
	oModel501:LoadValue('MODEL_V0W', "V0W_IDEVEN"	, oXmlInfoTot:_infoRecEv:_idEv:TEXT)
	oModel501:LoadValue('MODEL_V0W', "V0W_HASH"	, oXmlInfoTot:_infoRecEv:_hash:TEXT)
	oModel501:LoadValue('MODEL_V0W', "V0W_NRRECB"	, oXmlInfoTot:_infoTotal:_nrRecArqBase:TEXT)
	oModel501:LoadValue('MODEL_V0W', "V0W_ATIVO"	, "1")
	oModel501:LoadValue('MODEL_V0W', "V0W_EVENTO"	, cEvento)
	oModel501:LoadValue('MODEL_V0W', "V0W_PROTUL"	, oXmlInfoTot:_infoTotal:_nrRecArqBase:TEXT)
	oModel501:LoadValue('MODEL_V0W', "V0W_PROTPN"	, cProTPN)
	oModel501:LoadValue('MODEL_V0W', "V0W_VERANT"	, cVerAnt)

	If TAFColumnPos("V0W_LEIAUT")
		oModel501:LoadValue('MODEL_V0W', "V0W_LEIAUT"	, AllTrim(cVsReinf))
	EndIf

	If lVSup13 //Mantem o devido funcionamento na versao 1.4.0
		R1405001DT( cLayout, lNewTot, lIdeEstab, oModel501, oModelV0X, oModelV0Y, oModelV0Z )

	Else //Mantem o devido funcionamento ate a versao 1.3.2
		If cLayout == "R-2010"
			oModel501:LoadValue('MODEL_V0W', "V0W_CNPJ10"	, oXmlInfoTot:_infoTotal:_RTom:_cnpjPrestador:TEXT)
			oModel501:LoadValue('MODEL_V0W', "V0W_VLTTBR"	, Val(StrTran(oXmlInfoTot:_infoTotal:_RTom:_vlrTotalBaseRet:TEXT,",",".")))

			If ValType(oXmlInfoTot:_infoTotal:_RTom:_infoCrTom) == "A"
				aInfo := oXmlInfoTot:_infoTotal:_RTom:_infoCrTom
			Else 
				aInfo := aClone({oXmlInfoTot:_infoTotal:_RTom:_infoCrTom})
			EndIf 

			For nInfoTotal := 1 to Len(aInfo) 
				If lNewTot
					oModel501:LoadValue('MODEL_V0W', "V0W_CRTOM", aInfo[nInfoTotal]:_CRTOM:TEXT)
				EndIf 
				If aInfo[nInfoTotal]:_CRTOM:TEXT == "116201"
					If Type("aInfo["+cValToChar(nInfoTotal)+"]:_vlrCRTom") <> "U"
						oModel501:LoadValue('MODEL_V0W', "V0W_VLTTRP", Val(StrTran(aInfo[nInfoTotal]:_vlrCRTom:TEXT,",",".")))
					EndIf
					If Type("aInfo["+cValToChar(nInfoTotal)+"]:_vlrCRTomSusp") <> "U"
						oModel501:LoadValue('MODEL_V0W', "V0W_VLTTNP", Val(StrTran(aInfo[nInfoTotal]:_vlrCRTomSusp:TEXT,",",".")))
					EndIf 
				Else
					If Type("aInfo["+cValToChar(nInfoTotal)+"]:_vlrCRTom") <> "U"
						oModel501:LoadValue('MODEL_V0W', "V0W_VLTTRA", Val(StrTran(aInfo[nInfoTotal]:_vlrCRTom:TEXT,",",".")))
					EndIf
					If Type("aInfo["+cValToChar(nInfoTotal)+"]:_vlrCRTomSusp") <> "U"
						oModel501:LoadValue('MODEL_V0W', "V0W_VLTTNA", Val(StrTran(aInfo[nInfoTotal]:_vlrCRTomSusp:TEXT,",",".")))
					EndIf 
				EndIf 
			Next nInfoTotal
		ElseIf cLayout == "R-2020"
			oModel501:LoadValue('MODEL_V0W', "V0W_TPINST"	, oXmlInfoTot:_infoTotal:_RPrest:_tpInscTomador:TEXT)
			oModel501:LoadValue('MODEL_V0W', "V0W_NRINST"	, oXmlInfoTot:_infoTotal:_RPrest:_nrInscTomador:TEXT)
			oModel501:LoadValue('MODEL_V0W', "V0W_VLPTBR"	, Val(StrTran(oXmlInfoTot:_infoTotal:_RPrest:_vlrTotalBaseRet:TEXT,",",".")) )
			oModel501:LoadValue('MODEL_V0W', "V0W_VLPTRP"	, Val(StrTran(oXmlInfoTot:_infoTotal:_RPrest:_vlrTotalRetPrinc:TEXT,",",".")) )
			If Type("oXmlInfoTot:_infoTotal:_RPrest:_vlrTotalRetAdic") <> "U"
				oModel501:LoadValue('MODEL_V0W', "V0W_VLPTRA"	, Val(StrTran(oXmlInfoTot:_infoTotal:_RPrest:_vlrTotalRetAdic:TEXT,",",".")) )
			EndIf 
			If Type("oXmlInfoTot:_infoTotal:_RPrest:_vlrTotalNRetPrinc") <> "U"
				oModel501:LoadValue('MODEL_V0W', "V0W_VLPTNP"	, Val(StrTran(oXmlInfoTot:_infoTotal:_RPrest:_vlrTotalNRetPrinc:TEXT,",",".")) )
			EndIf 
			If Type("oXmlInfoTot:_infoTotal:_RPrest:_vlrTotalNRetAdic") <> "U"
				oModel501:LoadValue('MODEL_V0W', "V0W_VLPTNA"	, Val(StrTran(oXmlInfoTot:_infoTotal:_RPrest:_vlrTotalNRetAdic:TEXT,",",".")) )
			EndIf 
		ElseIf cLayout == "R-2040"
			If ValType(oXmlInfoTot:_infoTotal:_RRecRepAD) == "A"
				aInfo := oXmlInfoTot:_infoTotal:_RRecRepAD
			Else 
				aInfo := {oXmlInfoTot:_infoTotal:_RRecRepAD}
			EndIf 
			For nInfoTotal := 1 to Len(aInfo)
				If nInfoTotal > 1 
					oModelV0Y:AddLine()
				EndIf  
				If lNewTot
					oModel501:LoadValue('MODEL_V0Y', "V0Y_CRRECR", aInfo[nInfoTotal]:_CRRecRepAD:TEXT)
				EndIf 
				oModel501:LoadValue('MODEL_V0Y', "V0Y_CNPJAD"	, aInfo[nInfoTotal]:_cnpjAssocDesp:TEXT)
				oModel501:LoadValue('MODEL_V0Y', "V0Y_VLTREP"	, Val(StrTran(aInfo[nInfoTotal]:_vlrTotalRep:TEXT,",",".")))
				oModel501:LoadValue('MODEL_V0Y', "V0Y_VLTRET"	, Val(StrTran(aInfo[nInfoTotal]:_vlrCrRecRepAD:TEXT,",",".")))
				If Type("aInfo["+cValToChar(nInfoTotal)+"]:_vlrCrRecRepADSusp") <> "U"
					oModel501:LoadValue('MODEL_V0Y', "V0Y_VLTNRT"	, Val(StrTran(aInfo[nInfoTotal]:_vlrCrRecRepADSusp:TEXT,",",".")))
				EndIf
			Next nInfoTotal
		ElseIf cLayout == "R-2050"
			If lNewTot
				If ValType(oXmlInfoTot:_infoTotal:_RComl) == "A"
					aInfo := oXmlInfoTot:_infoTotal:_RComl
				Else 
					aInfo := {oXmlInfoTot:_infoTotal:_RComl}
				EndIf 
				For nInfoTotal := 1 to Len(aInfo)
					If nInfoTotal > 1 
						oModelV0X:AddLine()
						cSeq := Soma1(cSeq)
					EndIf   
					oModel501:LoadValue('MODEL_V0X', "V0X_SEQUEN"	, cSeq)
					oModel501:LoadValue('MODEL_V0X', "V0X_CRCOML"	, aInfo[nInfoTotal]:_CRComl:TEXT)
					oModel501:LoadValue('MODEL_V0X', "V0X_VLCOML"	, Val(StrTran(aInfo[nInfoTotal]:_vlrCRComl:TEXT,",",".")))
					If Type("aInfo["+cValToChar(nInfoTotal)+"]:_vlrCRComlSusp") <> "U"
						oModel501:LoadValue('MODEL_V0X', "V0X_VLSUSP"	, Val(StrTran(aInfo[nInfoTotal]:_vlrCRComlSusp:TEXT,",",".")))
					EndIf
				Next nInfoTotal
			EndIf 
		ElseIf cLayout == "R-2060"
			If ValType(oXmlInfoTot:_infoTotal:_RCPRB) == "A"
				aInfo := oXmlInfoTot:_infoTotal:_RCPRB
			Else 
				aInfo := {oXmlInfoTot:_infoTotal:_RCPRB}
			EndIf 

			For nInfoTotal := 1 to Len(aInfo)
				If nInfoTotal > 1 
					oModelV0Z:AddLine()
				EndIf  
				oModel501:LoadValue('MODEL_V0Z', "V0Z_CODREC"	, aInfo[nInfoTotal]:_CRCPRB:TEXT)
				oModel501:LoadValue('MODEL_V0Z', "V0Z_VLCPAT"	, Val(StrTran(aInfo[nInfoTotal]:_vlrCRCPRB:TEXT,",",".")))
				If Type("aInfo["+cValToChar(nInfoTotal)+"]:_vlrCRComlSusp") <> "U"
					oModel501:LoadValue('MODEL_V0Z', "V0Z_VLCSUS"	, Val(StrTran(aInfo[nInfoTotal]:_vlrCRCPRBSusp:TEXT,",",".")))
				EndIf
			Next nInfoTotal

		ElseIf cLayout == "R-3010"
			If lNewTot
				oModel501:LoadValue('MODEL_V0W', "V0W_CRESPE", oXmlInfoTot:_infoTotal:_RRecEspetDesp:_CRRecEspetDesp:TEXT)
			EndIf
			oModel501:LoadValue('MODEL_V0W', "V0W_VLRCTT"	, Val(StrTran(oXmlInfoTot:_infoTotal:_RRecEspetDesp:_vlrReceitaTotal:TEXT,",",".")) )
			oModel501:LoadValue('MODEL_V0W', "V0W_VLCPTT"	, Val(StrTran(oXmlInfoTot:_infoTotal:_RRecEspetDesp:_vlrCRRecEspetDesp:TEXT,",",".")) )
			If Type("oXmlInfoTot:_infoTotal:_RRecEspetDesp:_vlrCRRecEspetDespSusp") <> "U"
				oModel501:LoadValue('MODEL_V0W', "V0W_VLCPST"	, Val(StrTran(oXmlInfoTot:_infoTotal:_RRecEspetDesp:_vlrCRRecEspetDespSusp:TEXT,",",".")) )
			EndIf
		EndIf
	Endif

	If oModel501:VldData()
		FwFormCommit( oModel501 )
	Else
		cErro := TafRetEMsg( oModel501 )
	EndIf

	oModel501:DeActivate()
//	oModel501:Destroy()
//	oModel501 := Nil

Return (cErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} grvTotFecha
Grava evento totalizador utilizado a API de integra��o TafPrepInt
@Return  aRetInt - Retorno da grava��o do totalizador
			[1] - logico - determina se a mensagem � de uma integra��o bem sucedida
			[2] - caracter - status do registro (codigo utilizado na TAFXERP)
				1 - Incluido
				2 - Alterado
				3 - Excluido
				4 - Aguardando na Fila
				8 - Filhos Duplicado
				9 - Erro
			[3] - Codigo do Erro
			[4] - Descri��o da Mensagem de Integra��o
@author Evandro dos Santos Oliveira	
@since  19/08/2017
@version 1.0
/*///----------------------------------------------------------------
Static Function grvTotFecha(oXmlTot,cFilErp,cLayout,cRecibo, cRecAnt, cVsReinf, lJob)

	Local aInfo			as array
	//Local oModel 		as object
	Local nLine			as numeric
	Local nLinCrTom		as numeric
	Local nPosIni		as numeric
	Local cXMLConv		as char
	Local cEvtTot		as char
	Local cErro			as char
	Local cEvento		as char 
	Local cId			as char
	Local cProTPN		as char 
	Local cVerAnt		as char
	Local cNrRecibo		as char
	Local dDtProcess	as date
	Local aRetInt		as Array
	Local cVsReinf      as Char
	Local lVSup13       as logical

	Private oXml		as object 
	Private oXmlInfoTot	as object

	Default cRecAnt		:= ""
	Default lJob		:= .F.

	aInfo		:= {}
	oXml		:= oXmlTot
	cXMLConv	:= ""
	cEvtTot		:= ""
	cErro		:= ""
	cEvento		:= "I"
	cId			:= ""
	cNrRecibo	:= ""
	cProTPN		:= ""
	cVerAnt		:= ""
	dDtProcess	:= CtoD("  /  /    ")
	aRetInt		:= {}
	nPosIni		:= 0
	nLine		:= 0
	nLinCrTom	:= 0 

	//oModel		:= FWLoadModel("TAFA497")

	cPerApur	:= SubStr(oXmlTot:_REINF:_EVTTOTALCONTRIB:_IDEEVENTO:_PERAPUR:TEXT,6,2)+SubStr(oXmlTot:_REINF:_EVTTOTALCONTRIB:_IDEEVENTO:_PERAPUR:TEXT,1,4)

	dDtProcess	:= StoD(SubStr(oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFORECEV:_DHPROCESS:TEXT,1,4)+SubStr(oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFORECEV:_DHPROCESS:TEXT,6,2)+SubStr(oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFORECEV:_DHPROCESS:TEXT,9,2))
	cHrProcess	:= SubStr(oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFORECEV:_DHPROCESS:TEXT,12,2)+SubStr(oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFORECEV:_DHPROCESS:TEXT,15,2)
	lVSup13     := IIf( "1_03" $ AllTrim(cVsReinf), .F., .T. )

	If Type ("oXml:_REINF:_EVTTOTALCONTRIB:_INFOTOTALCONTRIB:_NRRECARQBASE") <> "U"
		cNrRecibo	:= oXml:_REINF:_EVTTOTALCONTRIB:_INFOTOTALCONTRIB:_NRRECARQBASE:TEXT
	EndIf

	V0C->(DbSetOrder(2))
	If !Empty(cRecAnt) .And. V0C->(DbSeek(xFilial("V0C") + cPerApur))
		FAltRegAnt('V0C', '2', .F.)
		cVerAnt := V0C->V0C_VERSAO
		cProTPN	:= cRecAnt
		cId 	:= V0C->V0C_ID
		cEvento := 'A'
	Endif 

	if lJob .And. ( isInCallStack("R2099_004") .Or. Upper(Alltrim(FunName())) == "RPC" ) .And. type("oModel497") == "U"
		oModel497 := FWLoadModel("TAFA497")
	endif

	oModel497:SetOperation(MODEL_OPERATION_INSERT)
	oModel497:Activate()

	oModel497:GetModel( 'MODEL_V0C' )
	oModel497:LoadValue('MODEL_V0C', "V0C_FILIAL"	, xFilial("V0C")																)

	If !Empty(cId)
		oModel497:LoadValue('MODEL_V0C', "V0C_ID"	, cId)
	EndIf 

	oModel497:LoadValue('MODEL_V0C', "V0C_VERSAO"	, xFunGetVer()) 
	oModel497:LoadValue('MODEL_V0C', "V0C_PERAPU"	, cPerApur																		)
	oModel497:LoadValue('MODEL_V0C', "V0C_CODRET"	, oXmlTot:_REINF:_EVTTOTALCONTRIB:_IDERECRETORNO:_IDESTATUS:_CDRETORNO:TEXT		)
	oModel497:LoadValue('MODEL_V0C', "V0C_DESCRE"	, oXmlTot:_REINF:_EVTTOTALCONTRIB:_IDERECRETORNO:_IDESTATUS:_DESCRETORNO:TEXT	)
	oModel497:LoadValue('MODEL_V0C', "V0C_PROTEN"	, oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFORECEV:_NRPROTENTR:TEXT					)
	oModel497:LoadValue('MODEL_V0C', "V0C_DTPROC"	, dDtProcess																	)
	oModel497:LoadValue('MODEL_V0C', "V0C_HRPROC"	, cHrProcess																	)
	oModel497:LoadValue('MODEL_V0C', "V0C_TPEVT"	, oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFORECEV:_TPEV:TEXT							)
	oModel497:LoadValue('MODEL_V0C', "V0C_IDEVT"	, oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFORECEV:_IDEV:TEXT							)
	oModel497:LoadValue('MODEL_V0C', "V0C_HASH"	, oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFORECEV:_HASH:TEXT							)
	oModel497:LoadValue('MODEL_V0C', "V0C_ATIVO"	, "1"																			)
	oModel497:LoadValue('MODEL_V0C', "V0C_EVENTO"	, cEvento)
	oModel497:LoadValue('MODEL_V0C', "V0C_PROTUL"	, AllTrim(cRecibo)																)
	oModel497:LoadValue('MODEL_V0C', "V0C_PROTPN"	, cProTPN)
	oModel497:LoadValue('MODEL_V0C', "V0C_VERANT"	, cVerAnt)

	If TAFColumnPos("V0C_LEIAUT")
		oModel497:LoadValue('MODEL_V0C', "V0C_LEIAUT"	, AllTrim(cVsReinf))
	EndIf

	If Type("oXML:_REINF:_EVTTOTALCONTRIB:_INFOTOTALCONTRIB") <> "U"
		oXmlInfoTot := oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFOTOTALCONTRIB
		If ValType(oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFOTOTALCONTRIB) == "A"
			aInfo := oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFOTOTALCONTRIB
		Else 
			aInfo := {oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFOTOTALCONTRIB}
		EndIf 

		For nLine := 1 To Len(aInfo)
			If nLine > 1
				oModel497:GetModel('MODEL_V0E'):AddLine()
			EndIf 
			oModel497:GetModel( 'MODEL_V0E' )
			oModel497:LoadValue('MODEL_V0E', "V0E_NRREC"	, cNrRecibo																		)
			oModel497:LoadValue('MODEL_V0E', "V0E_INDEXI"	, oXmlTot:_REINF:_EVTTOTALCONTRIB:_INFOTOTALCONTRIB:_INDEXISTINFO:TEXT			)
		Next nLine
	EndIf 

	If Type("oXmlInfoTot:_RTOM") <> "U"
		If ValType(oXmlInfoTot:_RTOM) == "A"
			For nLine := 1 To Len(oXmlInfoTot:_RTOM)
				If nLine > 1
					oModel497:GetModel( "MODEL_V0F" ):lValid:= .T.
					oModel497:GetModel( "MODEL_V0F" ):AddLine()
				EndIf
				oModel497:GetModel( 'MODEL_V0F' )
				oModel497:LoadValue('MODEL_V0F', "V0F_CNPJPR"	, oXmlInfoTot:_RTOM[nLine]:_CNPJPRESTADOR:TEXT									)
				
				if lVSup13 .and. TAFColumnPos( "V0F_CNO" )
					If Type( "oXmlInfoTot:_RTOM[" + cValToChar(nLine) + "]:_CNO" ) <> "U"
						oModel497:LoadValue( "MODEL_V0F", "V0F_CNO", oXmlInfoTot:_RTOM[nLine]:_CNO:TEXT )
					Endif
				EndIf

				oModel497:LoadValue('MODEL_V0F', "V0F_VLRBRE"	, Val(StrTran(oXmlInfoTot:_RTOM[nLine]:_VLRTOTALBASERET:TEXT,",","."))			)
								
				If Type("oXmlInfoTot:_RTOM["+cValToChar(nLine)+"]:_INFOCRTOM") <> "U"
					If ValType(oXmlInfoTot:_RTOM[nLine]:_INFOCRTOM) == "A"
						For nLinCrTom := 1 to Len(oXmlInfoTot:_RTOM[nLine]:_INFOCRTOM)
							If nLinCrTom > 1 
								oModel497:GetModel( "MODEL_V0F" ):lValid:= .T.
								oModel497:GetModel( "MODEL_V0F" ):AddLine()
								
								oModel497:GetModel( 'MODEL_V0F' )
								oModel497:LoadValue('MODEL_V0F', "V0F_CNPJPR"	, oXmlInfoTot:_RTOM[nLine]:_CNPJPRESTADOR:TEXT									)
								oModel497:LoadValue('MODEL_V0F', "V0F_VLRBRE"	, Val(StrTran(oXmlInfoTot:_RTOM[nLine]:_VLRTOTALBASERET:TEXT,",",".")))
							EndIf 		
							
							If Type("oXmlInfoTot:_RTOM["+cValToChar(nLine)+"]:_INFOCRTOM["+cValToChar(nLinCrTom)+"]:_CRTOM") <> "U"	
								oModel497:LoadValue('MODEL_V0F', "V0F_CRTOM"	, oXmlInfoTot:_RTOM[nLine]:_INFOCRTOM[nLinCrTom]:_CRTOM:TEXT)
							EndIf 	
							
							If Type("oXmlInfoTot:_RTOM["+cValToChar(nLine)+"]:_INFOCRTOM["+cValToChar(nLinCrTom)+"]:_VLRCRTOM") <> "U"
								oModel497:LoadValue('MODEL_V0F', "V0F_VLRTOM"	, Val(StrTran(oXmlInfoTot:_RTOM[nLine]:_INFOCRTOM[nLinCrTom]:_VLRCRTOM:TEXT,",","."))	)
							EndIf 
							
							If Type("oXmlInfoTot:_RTOM["+cValToChar(nLine)+"]:_INFOCRTOM["+cValToChar(nLinCrTom)+"]:_VLRCRTOMSUSP") <> "U"
								oModel497:LoadValue('MODEL_V0F', "V0F_VLRSUS"	, Val(StrTran(oXmlInfoTot:_RTOM[nLine]:_INFOCRTOM[nLinCrTom]:_VLRCRTOMSUSP:TEXT,",",".")))
							EndIf
						Next 
					Else 				
						If Type("oXmlInfoTot:_RTOM["+cValToChar(nLine)+"]:_INFOCRTOM:_CRTOM") <> "U"
							oModel497:LoadValue('MODEL_V0F', "V0F_CRTOM"	, oXmlInfoTot:_RTOM[nLine]:_INFOCRTOM:_CRTOM:TEXT							)
						EndIf				
						If Type("oXmlInfoTot:_RTOM["+cValToChar(nLine)+"]:_INFOCRTOM:_VLRCRTOM") <> "U"
							oModel497:LoadValue('MODEL_V0F', "V0F_VLRTOM"	, Val(StrTran(oXmlInfoTot:_RTOM[nLine]:_INFOCRTOM:_VLRCRTOM:TEXT,",","."))	)
						EndIf 
						If Type("oXmlInfoTot:_RTOM["+cValToChar(nLine)+"]:_INFOCRTOM:_VLRCRTOMSUSP") <> "U"
							oModel497:LoadValue('MODEL_V0F', "V0F_VLRSUS"	, Val(StrTran(oXmlInfoTot:_RTOM[nLine]:_INFOCRTOM:_VLRCRTOMSUSP:TEXT,",",".")))
						EndIf
					EndIf 
				EndIf 
			Next
		Else
			oModel497:GetModel( 'MODEL_V0F' )															
			oModel497:LoadValue('MODEL_V0F', "V0F_CNPJPR"	, oXmlInfoTot:_RTOM:_CNPJPRESTADOR:TEXT											)

			if lVSup13 .and. TAFColumnPos( "V0F_CNO" )
				If Type( "oXmlInfoTot:_RTOM:_CNO" ) <> "U"
					oModel497:LoadValue( "MODEL_V0F", "V0F_CNO", oXmlInfoTot:_RTOM:_CNO:TEXT )
				Endif
			EndIf

			oModel497:LoadValue('MODEL_V0F', "V0F_VLRBRE"	, Val(StrTran(oXmlInfoTot:_RTOM:_VLRTOTALBASERET:TEXT,",","."))					)
			
			If Type("oXmlInfoTot:_RTOM:_INFOCRTOM") <> "U"
				If ValType(oXmlInfoTot:_RTOM:_INFOCRTOM) == "A"
					For nLinCrTom := 1 to Len(oXmlInfoTot:_RTOM:_INFOCRTOM)
						If nLinCrTom > 1 
							oModel497:GetModel( "MODEL_V0F" ):lValid:= .T.
							oModel497:GetModel( "MODEL_V0F" ):AddLine()
								
							oModel497:GetModel( 'MODEL_V0F' )
							oModel497:LoadValue('MODEL_V0F', "V0F_CNPJPR"	, oXmlInfoTot:_RTOM:_CNPJPRESTADOR:TEXT									)
							oModel497:LoadValue('MODEL_V0F', "V0F_VLRBRE"	, Val(StrTran(oXmlInfoTot:_RTOM:_VLRTOTALBASERET:TEXT,",",".")))
						EndIf 		
							
						If Type("oXmlInfoTot:_RTOM:_INFOCRTOM["+cValToChar(nLinCrTom)+"]:_CRTOM") <> "U"	
							oModel497:LoadValue('MODEL_V0F', "V0F_CRTOM"	, oXmlInfoTot:_RTOM:_INFOCRTOM[nLinCrTom]:_CRTOM:TEXT)
						EndIf 	
							
						If Type("oXmlInfoTot:_RTOM:_INFOCRTOM["+cValToChar(nLinCrTom)+"]:_VLRCRTOM") <> "U"
							oModel497:LoadValue('MODEL_V0F', "V0F_VLRTOM"	, Val(StrTran(oXmlInfoTot:_RTOM:_INFOCRTOM[nLinCrTom]:_VLRCRTOM:TEXT,",","."))	)
						EndIf 
							
						If Type("oXmlInfoTot:_RTOM:_INFOCRTOM["+cValToChar(nLinCrTom)+"]:_VLRCRTOMSUSP") <> "U"
							oModel497:LoadValue('MODEL_V0F', "V0F_VLRSUS"	, Val(StrTran(oXmlInfoTot:_RTOM:_INFOCRTOM[nLinCrTom]:_VLRCRTOMSUSP:TEXT,",",".")))
						EndIf
					Next 
				Else 
					If Type("oXmlInfoTot:_RTOM:_INFOCRTOM:_CRTOM") <> "U"
						oModel497:LoadValue('MODEL_V0F', "V0F_CRTOM"	, oXmlInfoTot:_RTOM:_INFOCRTOM:_CRTOM:TEXT									)
					EndIf 			
					If Type("oXmlInfoTot:_RTOM:_INFOCRTOM:_VLRCRTOM") <> "U"
						oModel497:LoadValue('MODEL_V0F', "V0F_VLRTOM"	, Val(StrTran(oXmlInfoTot:_RTOM:_INFOCRTOM:_VLRCRTOM:TEXT,",","."))			)
					EndIf 
					If Type("oXmlInfoTot:_RTOM:_INFOCRTOM:_VLRCRTOMSUSP") <> "U"
						oModel497:LoadValue('MODEL_V0F', "V0F_VLRSUS"	, Val(StrTran(oXmlInfoTot:_RTOM:_INFOCRTOM:_VLRCRTOMSUSP:TEXT,",","."))		)
					EndIf
				EndIf  
			EndIf 
		EndIf
	EndIf

	If Type("oXmlInfoTot:_RPREST") <> "U"
		If ValType(oXmlInfoTot:_RPREST) == "A"
			For nLine := 1 To Len(oXmlInfoTot:_RPREST)
				If nLine > 1
					oModel497:GetModel( "MODEL_V0G" ):lValid:= .T.
					oModel497:GetModel( "MODEL_V0G" ):AddLine()
				EndIf
				oModel497:GetModel( 'MODEL_V0G' )
				oModel497:LoadValue('MODEL_V0G', "V0G_TPINST"	, oXmlInfoTot:_RPREST[nLine]:_TPINSCTOMADOR:TEXT							)
				oModel497:LoadValue('MODEL_V0G', "V0G_NRINST"	, oXmlInfoTot:_RPREST[nLine]:_NRINSCTOMADOR:TEXT							)
				oModel497:LoadValue('MODEL_V0G', "V0G_VLRBRE"	, Val(StrTran(oXmlInfoTot:_RPREST[nLine]:_VLRTOTALBASERET:TEXT,",","."))	)
				oModel497:LoadValue('MODEL_V0G', "V0G_VLRPRI"	, Val(StrTran(oXmlInfoTot:_RPREST[nLine]:_VLRTOTALRETPRINC:TEXT,",","."))	)
				If Type("oXmlInfoTot:_RPREST["+cValToChar(nLine)+"]:_VLRTOTALRETADIC") <> "U"
					oModel497:LoadValue('MODEL_V0G', "V0G_VLRADI"	, Val(StrTran(oXmlInfoTot:_RPREST[nLine]:_VLRTOTALRETADIC:TEXT,",",".")))
				EndIf 
				If Type("oXmlInfoTot:_RPREST["+cValToChar(nLine)+"]:_VLRTOTALNRETPRINC") <> "U"
					oModel497:LoadValue('MODEL_V0G', "V0G_VLRNPR"	, Val(StrTran(oXmlInfoTot:_RPREST[nLine]:_VLRTOTALNRETPRINC:TEXT,",",".")))
				EndIf 
				If Type("oXmlInfoTot:_RPREST["+cValToChar(nLine)+"]:_VLRTOTALNRETADIC") <> "U"
					oModel497:LoadValue('MODEL_V0G', "V0G_VLRNAD"	, Val(StrTran(oXmlInfoTot:_RPREST[nLine]:_VLRTOTALNRETADIC:TEXT,",",".")))
				EndIf
			Next
		Else
			oModel497:GetModel( 'MODEL_V0G' )														
			oModel497:LoadValue('MODEL_V0G', "V0G_TPINST"	, oXmlInfoTot:_RPREST:_TPINSCTOMADOR:TEXT									)
			oModel497:LoadValue('MODEL_V0G', "V0G_NRINST"	, oXmlInfoTot:_RPREST:_NRINSCTOMADOR:TEXT									)
			oModel497:LoadValue('MODEL_V0G', "V0G_VLRBRE"	, Val(StrTran(oXmlInfoTot:_RPREST:_VLRTOTALBASERET:TEXT,",","."))			)
			oModel497:LoadValue('MODEL_V0G', "V0G_VLRPRI"	, Val(StrTran(oXmlInfoTot:_RPREST:_VLRTOTALRETPRINC:TEXT,",","."))			)
			If Type("oXmlInfoTot:_RPREST:_VLRTOTALRETADIC") <> "U"
				oModel497:LoadValue('MODEL_V0G', "V0G_VLRADI"	, Val(StrTran(oXmlInfoTot:_RPREST:_VLRTOTALRETADIC:TEXT,",","."))		)
			EndIf
			If Type("oXmlInfoTot:_RPREST:_VLRTOTALNRETPRINC") <> "U"
				oModel497:LoadValue('MODEL_V0G', "V0G_VLRNPR"	, Val(StrTran(oXmlInfoTot:_RPREST:_VLRTOTALNRETPRINC:TEXT,",","."))		)
			EndIf 
			If Type("oXmlInfoTot:_RPREST:_VLRTOTALNRETADIC") <> "U"
				oModel497:LoadValue('MODEL_V0G', "V0G_VLRNAD"	, Val(StrTran(oXmlInfoTot:_RPREST:_VLRTOTALNRETADIC:TEXT,",","."))		)
			EndIf
		EndIf
	EndIf

	If Type("oXmlInfoTot:_RRECREPAD") <> "U"
		If ValType(oXmlInfoTot:_RRECREPAD) == "A"
			For nLine := 1 To Len(oXmlInfoTot:_RRECREPAD)
				If nLine > 1
					oModel497:GetModel( "MODEL_V0H" ):lValid:= .T.
					oModel497:GetModel( "MODEL_V0H" ):AddLine()
				EndIf
				oModel497:GetModel( 'MODEL_V0H' )
				oModel497:LoadValue('MODEL_V0H', "V0H_CNPJAD"	, oXmlInfoTot:_RRECREPAD[nLine]:_CNPJASSOCDESP:TEXT								)
				oModel497:LoadValue('MODEL_V0H', "V0H_VLRREP"	, Val(StrTran(oXmlInfoTot:_RRECREPAD[nLine]:_VLRTOTALREP:TEXT,",","."))			)
				If TAFColumnPos("V0H_CRRECR")
					oModel497:LoadValue('MODEL_V0H', "V0H_CRRECR", oXmlInfoTot:_RRECREPAD[nLine]:_CRRecRepAD:TEXT								)								
				EndIf
				
				oModel497:LoadValue('MODEL_V0H', "V0H_VLRRET"	, Val(StrTran(oXmlInfoTot:_RRECREPAD[nLine]:_VLRCRRECREPAD:TEXT,",","."))		)
				
				If Type("oXmlInfoTot:_RRECREPAD["+cValToChar(nLine)+"]:_VLRCRRECREPADSUSP") <> "U"
					oModel497:LoadValue('MODEL_V0H', "V0H_VLRNRE"	, Val(StrTran(oXmlInfoTot:_RRECREPAD[nLine]:_VLRCRRECREPADSUSP:TEXT,",",".")))
				EndIf 
			Next
		Else
			oModel497:GetModel( 'MODEL_V0H' )
			oModel497:LoadValue('MODEL_V0H', "V0H_FILIAL"	, xFilial("V0H")																)
			
			// Campos abaixo foram retirados a partir da vers�o 1.4
			If !lVSup13
				oModel497:LoadValue('MODEL_V0H', "V0H_CNPJAD"	, oXmlInfoTot:_RRECREPAD:_CNPJASSOCDESP:TEXT									)
				oModel497:LoadValue('MODEL_V0H', "V0H_VLRREP"	, Val(StrTran(oXmlInfoTot:_RRECREPAD:_VLRTOTALREP:TEXT,",","."))				)
			Endif

			If TAFColumnPos("V0H_CRRECR")
				oModel497:LoadValue('MODEL_V0H', "V0H_CRRECR", oXmlInfoTot:_RRECREPAD:_CRRecRepAD:TEXT										)					
			EndIf

			oModel497:LoadValue('MODEL_V0H', "V0H_VLRRET"	, Val(StrTran(oXmlInfoTot:_RRECREPAD:_VLRCRRECREPAD:TEXT,",","."))				)
			
			If Type("oXmlInfoTot:_RRECREPAD:_VLRCRRECREPADSUSP") <> "U"
				oModel497:LoadValue('MODEL_V0H', "V0H_VLRNRE"	, Val(StrTran(oXmlInfoTot:_RRECREPAD:_VLRCRRECREPADSUSP:TEXT,",","."))		)
			EndIf
		EndIf
	EndIf

	If Type("oXmlInfoTot:_RCOML") <> "U"
		If ValType(oXmlInfoTot:_RCOML) == "A"
			For nLine := 1 To Len(oXmlInfoTot:_RCOML)
				If nLine > 1
					oModel497:GetModel( "MODEL_V0I" ):lValid:= .T.
					oModel497:GetModel( "MODEL_V0I" ):AddLine()
				EndIf
				oModel497:GetModel( 'MODEL_V0I' )
				oModel497:LoadValue('MODEL_V0I', "V0I_FILIAL"	, xFilial("V0I")														)
				oModel497:LoadValue('MODEL_V0I', "V0I_SEQUEN"	, StrZero(nLine,3)														)
				oModel497:LoadValue('MODEL_V0I', "V0I_CRCOML"	, oXmlInfoTot:_RCOML[nLine]:_CRCOML:TEXT								)
				oModel497:LoadValue('MODEL_V0I', "V0I_VRCOML"	, Val(StrTran(oXmlInfoTot:_RCOML[nLine]:_VLRCRCOML:TEXT,",","."))		)
				If Type("oXmlInfoTot:_RCOML["+cValToChar(nLine)+"]:_VLRCRCOMLSUSP") <> "U"
					oModel497:LoadValue('MODEL_V0I', "V0I_VRCOMS"	, Val(StrTran(oXmlInfoTot:_RCOML[nLine]:_VLRCRCOMLSUSP:TEXT,",",".")))
				EndIf 
			Next
		Else
			oModel497:GetModel( 'MODEL_V0I' )
			oModel497:LoadValue('MODEL_V0I', "V0I_FILIAL"	, xFilial("V0I")															)
			oModel497:LoadValue('MODEL_V0I', "V0I_SEQUEN"	, StrZero(nLine,3)															)
			oModel497:LoadValue('MODEL_V0I', "V0I_CRCOML"	, oXmlInfoTot:_RCOML:_CRCOML:TEXT											)
			oModel497:LoadValue('MODEL_V0I', "V0I_VRCOML"	, Val(StrTran(oXmlInfoTot:_RCOML:_VLRCRCOML:TEXT,",","."))					)
			If Type("oXmlInfoTot:_RCOML:_VLRCRCOMLSUSP") <> "U"
				oModel497:LoadValue('MODEL_V0I', "V0I_VRCOMS"	, Val(StrTran(oXmlInfoTot:_RCOML:_VLRCRCOMLSUSP:TEXT,",","."))			)
			EndIf
		EndIf
	EndIf

	If Type("oXmlInfoTot:_RCPRB") <> "U"
		If ValType(oXmlInfoTot:_RCPRB) == "A"
			For nLine := 1 To Len(oXmlInfoTot:_RCPRB)
				If nLine > 1
					oModel497:GetModel( "MODEL_V0J" ):lValid:= .T.
					oModel497:GetModel( "MODEL_V0J" ):AddLine()
				EndIf
				oModel497:GetModel( 'MODEL_V0J' )
				oModel497:LoadValue('MODEL_V0J', "V0J_FILIAL"	, xFilial("V0J")														)
				oModel497:LoadValue('MODEL_V0J', "V0J_CODREC"	, oXmlInfoTot:_RCPRB[nLine]:_CRCPRB:TEXT								)
				oModel497:LoadValue('MODEL_V0J', "V0J_VLRCPT"	, Val(StrTran(oXmlInfoTot:_RCPRB[nLine]:_VLRCRCPRB:TEXT,",","."))		)
				If Type("oXmlInfoTot:_RCPRB["+cValToChar(nLine)+"]:_VLRCRCPRBSUSP") <> "U"
					oModel497:LoadValue('MODEL_V0J', "V0J_VLRCPS"	, Val(StrTran(oXmlInfoTot:_RCPRB[nLine]:_VLRCRCPRBSUSP:TEXT,",",".")))
				EndIf
			Next
		Else
			oModel497:GetModel( 'MODEL_V0J' )
			oModel497:LoadValue('MODEL_V0J', "V0J_FILIAL"	, xFilial("V0J")														)
			oModel497:LoadValue('MODEL_V0J', "V0J_CODREC"	, oXmlInfoTot:_RCPRB:_CRCPRB:TEXT										)
			oModel497:LoadValue('MODEL_V0J', "V0J_VLRCPT"	, Val(StrTran(oXmlInfoTot:_RCPRB:_VLRCRCPRB:TEXT,",","."))				)
			If Type("oXmlInfoTot:_RCPRB:_VLRCRCPRBSUSP") <> "U"
				oModel497:LoadValue('MODEL_V0J', "V0J_VLRCPS"	, Val(StrTran(oXmlInfoTot:_RCPRB:_VLRCRCPRBSUSP:TEXT,",","."))		)
			EndIf 
		EndIf
	EndIf

	If oModel497:VldData() 
		FwFormCommit( oModel497 )
	Else
		cErro := TafRetEMsg( oModel497 )
	EndIf

	oModel497:DeActivate()
//	oModel497:Destroy()
//	oModel497 := Nil

Return (cErro)

//-------------------------------------------------------------------
/*/{Protheus.doc} limpaRegT0X
Limpa inconsist�nca da tabela T0X.

@Return cIdAux - Chave da inconsist�ncia a ser excluida 

@author Evandro dos Santos Oliveira	
@since  15/01/2018
@version 1.0

/*///----------------------------------------------------------------
Static Function limpaRegT0X(cIdAux)

	If TcSqlExec("DELETE FROM " + RetSqlName("T0X") + " WHERE T0X_IDCHVE = '" + cIdAux + "' AND (T0X_USER = '" + cUserName + "' OR T0X_USER = '__Schedule')") < 0
		If lJob
			ConOut("Erro na limpeza das inconsit�ncias: " + TCSQLError())
		Else
			MsgStop(TCSQLError(),"Erro na limpeza das inconsit�ncias")
		EndIf
	EndIf

Return 


//-------------------------------------------------------------------
/*/{Protheus.doc} SchedDef
Informacoes de definicao dos parametros do schedule
@Return  Array com as informacoes de definicao dos parametros do schedule
		 Array[x,1] -> Caracter, Tipo: "P" - para Processo, "R" - para Relatorios
		 Array[x,2] -> Caracter, Nome do Pergunte
		 Array[x,3] -> Caracter, Alias(para Relatorio)
		 Array[x,4] -> Array, Ordem(para Relatorio)
		 Array[x,5] -> Caracter, Titulo(para Relatorio)

@author Evandro dos Santos Oliveira	
@since  17/05/2016
@version 1.0

/*///----------------------------------------------------------------
Static Function SchedDef()
Local aParam as array

aParam  := {}

aParam := { "P",;			//Tipo R para relatorio P para processo
            "TAFESXTSS",;	//Pergunte do relatorio, caso nao use passar ParamDef
            ,"SM0";			//Alias
            ,;			//Array de ordens
            }				//Titulo

Return ( aParam )

//-------------------------------------------------------------------
/*/{Protheus.doc} R145001Det
Reinf 1.4.0 Grava Detalhe Do Totalizador 5001
@Return
@author Denis Souza
@since  04/10/2018
@version 1.0
/*///----------------------------------------------------------------

Static Function R1405001DT( cLay, lTom, lEstab, oMdl, oMdlV0X,  oMdlV0Y, oMdlV0Z )

Local cSeq	  as char
Local cTpInsc as char
Local nInfTT  as numeric

nInfTT 	:= 0
cSeq	:= "001"
cTpInsc := " "

If lEstab
	If Type( "oXmlInfoTot:_infoTotal:_ideEstab" ) <> "U"
		If Type( "oXmlInfoTot:_infoTotal:_ideEstab:_tpInsc" ) <> "U"
			cTpInsc := oXmlInfoTot:_infoTotal:_ideEstab:_tpInsc:TEXT
			if Valtype( cTpInsc ) == "C" .And. ( cTpInsc == "1" .Or. cTpInsc == "4" )
				oMdl:LoadValue( "MODEL_V0W", "V0W_TPINSE", cTpInsc )
			elseif Valtype( cTpInsc ) == "C" .And. cTpInsc == "0"
				oMdl:LoadValue( "MODEL_V0W", "V0W_TPINSE", " " )
			endif
		endif
		If Type( "oXmlInfoTot:_infoTotal:_ideEstab:_nrInsc" ) <> "U"
			oMdl:LoadValue( "MODEL_V0W", "V0W_NRINSE", oXmlInfoTot:_infoTotal:_ideEstab:_nrInsc:TEXT )
		endif
	EndIf
EndIf

If cLay == "R-2010"
	oMdl:LoadValue( 'MODEL_V0W', "V0W_CNPJ10"	, oXmlInfoTot:_infoTotal:_ideEstab:_RTom:_cnpjPrestador:TEXT )
	If TafColumnPos( "V0W_CNOEST" )
		If Type( "oXmlInfoTot:_infoTotal:_ideEstab:_RTom:_CNO" ) <> "U"
			oMdl:LoadValue( "MODEL_V0W", "V0W_CNOEST", oXmlInfoTot:_infoTotal:_ideEstab:_RTom:_CNO:TEXT )
		EndIf
	EndIf
	oMdl:LoadValue( 'MODEL_V0W', "V0W_VLTTBR"	, Val(StrTran(oXmlInfoTot:_infoTotal:_ideEstab:_RTom:_vlrTotalBaseRet:TEXT,",",".")) )
	If ValType( oXmlInfoTot:_infoTotal:_ideEstab:_RTom:_infoCrTom ) == "A"
		aInfo := oXmlInfoTot:_infoTotal:_ideEstab:_RTom:_infoCrTom
	Else
		aInfo := aClone( { oXmlInfoTot:_infoTotal:_ideEstab:_RTom:_infoCrTom } )
	EndIf
	For nInfTT := 1 to Len(aInfo) 
		If lTom
			oMdl:LoadValue('MODEL_V0W', "V0W_CRTOM", aInfo[nInfTT]:_CRTOM:TEXT)
		EndIf 
		If aInfo[nInfTT]:_CRTOM:TEXT == "116201"
			If Type("aInfo["+cValToChar(nInfTT)+"]:_vlrCRTom") <> "U"
				oMdl:LoadValue('MODEL_V0W', "V0W_VLTTRP", Val(StrTran(aInfo[nInfTT]:_vlrCRTom:TEXT,",",".")))
			EndIf
			If Type("aInfo["+cValToChar(nInfTT)+"]:_vlrCRTomSusp") <> "U"
				oMdl:LoadValue('MODEL_V0W', "V0W_VLTTNP", Val(StrTran(aInfo[nInfTT]:_vlrCRTomSusp:TEXT,",",".")))
			EndIf 
		Else
			If Type("aInfo["+cValToChar(nInfTT)+"]:_vlrCRTom") <> "U"
				oMdl:LoadValue('MODEL_V0W', "V0W_VLTTRA", Val(StrTran(aInfo[nInfTT]:_vlrCRTom:TEXT,",",".")))
			EndIf
			If Type("aInfo["+cValToChar(nInfTT)+"]:_vlrCRTomSusp") <> "U"
				oMdl:LoadValue('MODEL_V0W', "V0W_VLTTNA", Val(StrTran(aInfo[nInfTT]:_vlrCRTomSusp:TEXT,",",".")))
			EndIf 
		EndIf 
	Next nInfTT

ElseIf cLay == "R-2020"
	oMdl:LoadValue('MODEL_V0W', "V0W_TPINST"	, oXmlInfoTot:_infoTotal:_ideEstab:_RPrest:_tpInscTomador:TEXT )
	oMdl:LoadValue('MODEL_V0W', "V0W_NRINST"	, oXmlInfoTot:_infoTotal:_ideEstab:_RPrest:_nrInscTomador:TEXT )
	oMdl:LoadValue('MODEL_V0W', "V0W_VLPTBR"	, Val( StrTran(oXmlInfoTot:_infoTotal:_ideEstab:_RPrest:_vlrTotalBaseRet:TEXT,",",".")) )
	oMdl:LoadValue('MODEL_V0W', "V0W_VLPTRP"	, Val( StrTran(oXmlInfoTot:_infoTotal:_ideEstab:_RPrest:_vlrTotalRetPrinc:TEXT,",",".")) )
	If Type("oXmlInfoTot:_infoTotal:_ideEstab:_RPrest:_vlrTotalRetAdic") <> "U"
		oMdl:LoadValue('MODEL_V0W', "V0W_VLPTRA"	, Val(StrTran(oXmlInfoTot:_infoTotal:_ideEstab:_RPrest:_vlrTotalRetAdic:TEXT,",",".")) )
	EndIf
	If Type("oXmlInfoTot:_infoTotal:_ideEstab:_RPrest:_vlrTotalNRetPrinc") <> "U"
		oMdl:LoadValue('MODEL_V0W', "V0W_VLPTNP"	, Val(StrTran(oXmlInfoTot:_infoTotal:_ideEstab:_RPrest:_vlrTotalNRetPrinc:TEXT,",",".")) )
	EndIf
	If Type("oXmlInfoTot:_infoTotal:_ideEstab:_RPrest:_vlrTotalNRetAdic") <> "U"
		oMdl:LoadValue('MODEL_V0W', "V0W_VLPTNA"	, Val(StrTran(oXmlInfoTot:_infoTotal:_ideEstab:_RPrest:_vlrTotalNRetAdic:TEXT,",",".")) )
	EndIf

ElseIf cLay == "R-2040"
	If ValType( oXmlInfoTot:_infoTotal:_ideEstab:_RRecRepAD ) == "A"
		aInfo := oXmlInfoTot:_infoTotal:_ideEstab:_RRecRepAD
	Else
		aInfo := { oXmlInfoTot:_infoTotal:_ideEstab:_RRecRepAD }
	EndIf
	For nInfTT := 1 to Len(aInfo)
		If nInfTT > 1
			oMdlV0Y:AddLine()
		EndIf
		oMdl:LoadValue('MODEL_V0Y', "V0Y_CNPJAD"	, aInfo[nInfTT]:_cnpjAssocDesp:TEXT)
		oMdl:LoadValue('MODEL_V0Y', "V0Y_VLTREP"	, Val(StrTran(aInfo[nInfTT]:_vlrTotalRep:TEXT,",",".")))
		If lTom
			oMdl:LoadValue('MODEL_V0Y', "V0Y_CRRECR", aInfo[nInfTT]:_CRRecRepAD:TEXT)
		EndIf			
		oMdl:LoadValue('MODEL_V0Y', "V0Y_VLTRET"	, Val(StrTran(aInfo[nInfTT]:_vlrCrRecRepAD:TEXT,",",".")))									
		If Type("aInfo["+cValToChar(nInfTT)+"]:_vlrCrRecRepADSusp") <> "U"
			oMdl:LoadValue('MODEL_V0Y', "V0Y_VLTNRT"	, Val(StrTran(aInfo[nInfTT]:_vlrCrRecRepADSusp:TEXT,",",".")))
		EndIf
	Next nInfTT

ElseIf cLay == "R-2050"
	If lTom
		If ValType(oXmlInfoTot:_infoTotal:_ideEstab:_RComl) == "A"
			aInfo := oXmlInfoTot:_infoTotal:_ideEstab:_RComl
		Else
			aInfo := { oXmlInfoTot:_infoTotal:_ideEstab:_RComl }
		EndIf
		For nInfTT := 1 to Len(aInfo)
			If nInfTT > 1 
				oMdlV0X:AddLine()
				cSeq := Soma1(cSeq)
			EndIf   
			oMdl:LoadValue('MODEL_V0X', "V0X_SEQUEN"	, cSeq)
			oMdl:LoadValue('MODEL_V0X', "V0X_CRCOML"	, aInfo[nInfTT]:_CRComl:TEXT)
			oMdl:LoadValue('MODEL_V0X', "V0X_VLCOML"	, Val(StrTran(aInfo[nInfTT]:_vlrCRComl:TEXT,",",".")))
			If Type("aInfo["+cValToChar(nInfTT)+"]:_vlrCRComlSusp") <> "U"
				oMdl:LoadValue('MODEL_V0X', "V0X_VLSUSP"	, Val(StrTran(aInfo[nInfTT]:_vlrCRComlSusp:TEXT,",",".")))
			EndIf
		Next nInfTT
	EndIf

ElseIf cLay == "R-2060"
	If ValType(oXmlInfoTot:_infoTotal:_ideEstab:_RCPRB) == "A"
		aInfo := oXmlInfoTot:_infoTotal:_ideEstab:_RCPRB
	Else
		aInfo := { oXmlInfoTot:_infoTotal:_ideEstab:_RCPRB }
	EndIf

	For nInfTT := 1 to Len(aInfo)
		If nInfTT > 1 
			oMdlV0Z:AddLine()
		EndIf  
		oMdl:LoadValue('MODEL_V0Z', "V0Z_CODREC"	, aInfo[nInfTT]:_CRCPRB:TEXT)
		oMdl:LoadValue('MODEL_V0Z', "V0Z_VLCPAT"	, Val(StrTran(aInfo[nInfTT]:_vlrCRCPRB:TEXT,",",".")))
		If Type("aInfo["+cValToChar(nInfTT)+"]:_vlrCRComlSusp") <> "U"
			oMdl:LoadValue('MODEL_V0Z', "V0Z_VLCSUS"	, Val(StrTran(aInfo[nInfTT]:_vlrCRCPRBSusp:TEXT,",",".")))
		EndIf
	Next nInfTT

ElseIf cLay == "R-3010"
	If lTom
		oMdl:LoadValue('MODEL_V0W', "V0W_CRESPE", oXmlInfoTot:_infoTotal:_ideEstab:_RRecEspetDesp:_CRRecEspetDesp:TEXT)
	EndIf
	oMdl:LoadValue('MODEL_V0W', "V0W_VLRCTT"	, Val(StrTran(oXmlInfoTot:_infoTotal:_ideEstab:_RRecEspetDesp:_vlrReceitaTotal:TEXT,",",".")) )
	oMdl:LoadValue('MODEL_V0W', "V0W_VLCPTT"	, Val(StrTran(oXmlInfoTot:_infoTotal:_ideEstab:_RRecEspetDesp:_vlrCRRecEspetDesp:TEXT,",",".")) )
	If Type("oXmlInfoTot:_infoTotal:_RRecEspetDesp:_vlrCRRecEspetDespSusp") <> "U"
		oMdl:LoadValue('MODEL_V0W', "V0W_VLCPST"	, Val(StrTran(oXmlInfoTot:_infoTotal:_ideEstab:_RRecEspetDesp:_vlrCRRecEspetDespSusp:TEXT,",",".")) )
	EndIf
EndIf

Return Nil