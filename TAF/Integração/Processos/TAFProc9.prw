#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TAFPROC4.CH"

#DEFINE TAMMAXXML 0750000  //Tamanho Maximo do XML
#DEFINE TAMMSIGN  0040000  //Tamanho m�dio da assinatura

//---------------------------------------------------------------------------
/*/{Protheus.doc} TAFProc9 
Chama rotina responsavel por verificar os registros que devem ser 
transmitidos.

@return Nil 

@author Leonardo Kichitaro
@since 09/03/2018
@version 1.0
@obs - Rotina separada do fonte TAFAINTEG e realizado tratamentos especificos
		para a utiliza��o do Job4 realizando a chamada individualmente e utilizando
		o schedDef para a execu��o no schedule.
/*/
//-----------------------------------------------------------------------------   
Function TAFProc9( aAlias , aEvt )

	Local lJob := IsBlind()
	Local lEnd := .F.
	Local aErrosJob := {}

	Default aAlias := {}
	Default aEvt   := {}

	If TAFAtualizado(!lJob)
		ConOut('Rotina de Transmiss�o de eventos REINF - Empresa: ' + cEmpAnt + ' Filial: ' + cFilAnt)

		If lJob
			aErrosJob := TAFProc9TSS(lJob, aEvt ,/*3*/,/*4*/,/*5*/,/*6*/,@lEnd,/*8*/,/*9*/,/*10*/,/*11*/,/*12*/,/*13*/, aAlias ,/*15*/ )
			TAFMErrT0X(aErrosJob,lJob)
		Else
			Processa( {||TAFProc9TSS(lJob,,,,,,@lEnd)}, "Aguarde...", "Executando rotina de Transmiss�o",  )
		EndIf

		If lEnd .And. !lJob
			MsgInfo("Processo finalizado.")
		EndIf
	EndIf

Return Nil

//---------------------------------------------------------------------------
/*/{Protheus.doc} TAFProc9Tss 
Processo responsavel por verificar os registros que devem ser transmitidos no
TSS.

Altera��o: Evandro dos Santos
Data: 05/04/2016 
Descri��o: - Alterado a forma de gera��o dos registros, para a rotina possibilitar
a gera��o de XMLs em disco, foram incluidos uma s�rie de par�metros que permitem 
a gera��o de layouts especificos e filtros por status, recno e Id.
- Alterado a Origem do array dos layouts, antes os mesmos eram baseados no array 
aTafSocial deste fonte, agora os layouts considerados s�o os especificados no
fonte TAFROTINAS.

@param	lJob - Flag para Identifica��o da chamada de Fun��o por Job
@param 	aEvtsESoc 	- Array com os Eventos a serem considerados, quando vazio s�o considerados 
		todos os eventos contidos no TAFROTINAS. 
		Obs: Quando informados os eventos devem seguir a mesma estrutura dos eventos e-Social
		contidos no TAFROTINAS.
@param 	cStatus - Status dos eventos que devem ser transmitidos, quando vazio  o sistema usa o 0 
        para tranmiss�o e o 2 para consulta; o par�metro pode conter mais de 1 status par isso
        passar os status separados por virgula ex: "1,3,4"
@param cPathXml - Path para a gera��o dos XMls, quando esse par�metro � informado e o cEvOrCon �
        vazio o sistema gera os XMLs em disco.	
@param aIdTrab - Array com o Id dos trabalhadores (para filtro dos eventos que tem rela��o com o 
	    trabalhador)
@param cRecNos - Filtra os registro pelo RecNo do Evento, pode ser utilizado um range de recnos
		ex;"1,5,40,60"
@param lNoErro - Determina se houve erros no processamento (vari�vel referenciada) 
@param cMsgRet - Mensagem de retorno do WS (refer�ncia)
@param lForce - For�a a geracao do XML nao respeitando o cadastro de predecess�o 
@param aFiliais - Array de Filiais     	
@param dDataIni	-> Data Inicial dos eventos
@param dDataFim	-> Data Fim dos dos eventos
@param lEvtInicial -> Informa se o par�metro de evento inicial foi marcado.

@return Nil 

@author Evandro dos Santos O. Teixeira
@since 07/11/2013
@version 1.0
/*/
//---------------------------------------------------------------------------    
Function TAFProc9Tss(lJob , aEvtsReinf, cStatus, cPathXml, aIdTrab, cRecNos, lNoErro, cMsgRet,lForce,aFiliais,dDataIni,dDataFim,lEvtInicial,aRecREINF, lConexTSS, lApi,aRetErro)

	Local cQry		  := ""
	Local cMaxRegs 	  := ""
	Local cFunction	  := ""
	Local cXml		  := ""
	Local cMsg		  := ""
	Local cAliasTb    := ""
	Local cLayout	  := ""
	Local cIdTrab	  := ""
	Local cId		  := ""
	Local cTabOpen	  := ""
	Local cRegNode	  := ""
	Local cMsgProc    := ""
	Local cSelect     := ""
	Local cAliasRegs  := GetNextAlias()
	Local cAliasTrb   := GetNextAlias()
	Local cAliasEve   := {}
	Local cBancoDB 	  := Upper(AllTrim(TcGetDB()))
	Local cHoraIni	  := Time()
	Local cTempoTr	  := ""
	Local cMsgAux	  := ""
	Local cTimeProc	  := Time()
	Local cLog		  := ""
	Local cIdThread   := StrZero(ThreadID(), 10 )
	Local cDel		  := ""
	Local cAlsEvt	  := ""
	Local nTopSlct 	  := GetNewPar("MV_TAFQPRC",5000)
	Local nSeq		  := 0
	Local nX          := 0
	Local nY	 	  := 0
	Local nQtdRegs	  := 0
	Local nByteXML	  := 0
	Local nRegsOk	  := 0
	Local lEvtTrb	  := .F.
	Local lMultSts 	  := .F.
	Local lAllEventos := .F.
	Local aXmls		  := {}
	Local aRetorno	  := {}
	Local aRetAux	  := {}
	Local aEvenGrp	  := {}
	Local aArea		  := {}
	Local aAuxRet     := {}
	Local aRetEvts    := {}
	Local aHoraIni	  := {}
	Local oXml		  := Nil
	Local lHaErros	  := .F.
	Local lErroSch	  := .F.
	Local lErroSrv	  := .F.
	Local lErroPred	  := .F.
	Local cFilBkp	  := ""
	Local cAmbES      := SuperGetMv('MV_TAFAMBR',.F.,"2")

	Default cStatus 	:= ""
	Default aEvtsReinf	:= {}
	Default aIdTrab 	:= {}
	Default aFiliais	:= {}
	Default aRecREINF	:= {}
	Default cRecNos		:= ""
	Default cPathXml	:= ""
	Default cMsgRet 	:= ""
	Default	lForce		:= .F.
	Default dDataIni	:= dDataBase
	Default dDataFim	:= dDataBase
	Default lJob		:= .F.
	Default lApi		:= .F.

	cStatus 	 := IIf(Empty(cStatus),'0',cStatus)

	lAllEventos  := Empty(aRecREINF) //Se aEvtsESoc for vazio devo considerar todos os eventos na query de transmiss�o

// Tratamento para funcionalidade via Job/Schedule 
	If lJob
		cLog := "* Inicio Transmiss�o TAFProc6 TheadId: " + cIdThread + " - Data de Inicio: " + DTOC(dDataBase) + " - " + cTimeProc
		ConOut(cLog)
	Else
		ProcRegua(Len(aRecREINF))
	EndIf

	cMsgProc := "Selecionando registros para a gera��o dos XMLs. " //"Selecionando registros para a gera��o dos XMLs. "

	if TAFAlsInDic( 'T0X' )
		dbSelectArea("T0X")
		T0X ->(DbSetOrder(3))
	endif

	If Len(aRecREINF) > 0
		cAliasEve	:= AllTrim( aEvtsReinf[3] )
		nQtdRegs	:= Len(aRecREINF)

		For nX:= 1 to Len(aRecREINF)
			( cAliasEve )->(dbGoTo( aRecREINF[nX] ) )
			cId			:= AllTrim ( STRTRAN( aEvtsReinf[4] , "-" , "" ) ) + &(AllTrim(cAliasEve)+"->"+AllTrim(cAliasEve)+"_ID") + &(AllTrim(cAliasEve)+"->"+AllTrim(cAliasEve)+"_VERSAO")
			cFunction	:= AllTrim( aEvtsReinf[8] )
			cKeyId		:= &(AllTrim(cAliasEve)+"->"+AllTrim(cAliasEve)+"_FILIAL") + &(AllTrim(cAliasEve)+"->"+AllTrim(cAliasEve)+"_ID") + &(AllTrim(cAliasEve)+"->"+AllTrim(cAliasEve)+"_VERSAO")

			If cFunction == 'TAF496Xml'
				cXml := &cFunction.( cAliasEve , aRecREINF[nX] , , .T. , lApi)
			elseif cFunction == 'TAF494Xml'
				cXml := &cFunction.( cAliasEve , aRecREINF[nX] , , .T. , , lApi)
			else
				cXml := &cFunction.( cAliasEve , aRecREINF[nX] , , .T.)
			EndIF

			nSeq++
			aAdd( aXmls , { EncodeUTF8( cXml ) , cId , aRecREINF[nX] , AllTrim( aEvtsReinf[4] ) , cAliasEve } )

			nByteXML += Len( cXML ) + TAMMSIGN

		/*+-----------------------------------------------------------------------------------------------------+
			| Quando alcan�ar o limite, fa�o o envio do que j� tenho  e zero o Array de XMLs                      |
			| S� � permitido o envio de 50 registros por lote (Manual de Orienta��o do Desenvolvedor e-Social 1.4)|
			| A Variavel nSeq � utilizada para controle do lote e para o sequenciamento do ID do evento.          |
			+-----------------------------------------------------------------------------------------------------+*/ 
			If nByteXML >= TAMMAXXML .Or. nSeq == 50
			aAuxRet := TAFEvXml(aXmls,cAmbES,@nRegsOk,lJob,cIdThread,@lErroSch,@lErroSrv)
			aAdd(aRetEvts,aClone(aAuxRet))
			aXmls		:= {}
			nByteXML	:= 0
			nSeq		:= 0
			lConexTSS	:= Iif(lErroSrv,.F.,.T.)
			//Quando ocorre um Erro no Servidor aborto a opera��o.
				If lErroSrv
				Exit
				EndIf
			EndIf
		Next

		If Len(aXmls) > 0
		aAuxRet		:= TAFEvXml(aXmls,cAmbES,@nRegsOk,lJob,cIdThread,@lErroSch,@lErroSrv)
		lConexTSS	:= Iif(lErroSrv,.F.,.T.)
		aAdd(aRetEvts,aClone(aAuxRet))
		EndIf

		aHoraIni := StrTokArr(cHoraIni,":")
		cTempoTr := DecTime( Time() , Val(aHoraIni[1]) , Val(aHoraIni[2]) , Val(aHoraIni[3]) ) 
	
		If nQtdRegs > 1
			cMsgAux := "os eventos foram vinculados" //"os eventos foram vinculados"  
		Else
			cMsgAux := "o evento foi vinculado" //"o evento foi vinculado"
		EndIf
	
		If !lErroSch .And. !lErroSrv .And. !lErroPred
			cMsgRet := "Voc� concluiu com sucesso a transmiss�o para o TSS. Verifique se " + cMsgAux + " ao ambiente REINF utilizando a rotina de detalhamento." + CRLF + CRLF //"Voc� concluiu com sucesso a transmiss�o para o TSS. Verifique se "#" ao ambiente e-Social (RET) utilizando a rotina de detalhamento." 	
		ElseIf lErroSch
			cMsgRet := "Ocorreu(ram) erro(s) de schema(s) em 1 ou mais registros. Verifique as inconssist�ncias utilizando a rotina de detalhamento." + CRLF + CRLF
		ElseIf lErroSrv
			cMsgRet := "N�o foi possivel efetuar o envio do(s) lote(s) para o servidor TSS. " + "Descri��o do Erro: " + aAuxRet[2] + CRLF + CRLF
		Else
			cMsgRet += CRLF 
		EndIf
	
		If !lErroSrv
			cMsgRet += AllTrim(Str(nRegsOK)) + "/" + AllTrim(Str(nQtdRegs)) + " evento(s) transmido(s) em " + cTempoTr + "." //" evento(s) transmido(s) em "
		Else
			cMsgRet += "Tempo de Processamento: " + cTempoTr
		EndIf
	Else
		cMsgRet := "N�o h� eventos pendentes de transmiss�o a serem transmitidos considerando as op��es selecionadas. " // "N�o h� eventos pendentes de transmiss�o a serem transmitidos considerando as op��es selecionadas. "
	EndIf

	If lJob
		cTimeProc := Time()
		ConOut(cMsgRet) 
		cLog := "* Fim Transmiss�o TAFProc6 TheadId: " + cIdThread + " - Data de Inicio: " + DTOC(dDataBase) + " - " + Time() + " - Tempo de processamento: " + ElapTime(cTimeProc,Time())  + " - Quantidade de Registros: " + AllTrim(Str(nQtdRegs))
		conOut(cLog)
	EndIf

	//Preciso desses valores no array retornado para mostrar o tipo de erro no Portinari.
	if lApi
		AADD(aRetErro,lErroSch) // Erro de schema
		AADD(aRetErro,lErroSrv) // Erro para se conectar no TSS
		AADD(aRetErro,lErroPred)// Erro de predecess�o.
	EndIf

Return (aRetEvts)

//---------------------------------------------------------------------------
/*/{Protheus.doc} TAFEvXml 
Realiza a Transmiss�o dos documentos.

@param aXmls  	- Array com os dados do Xml    
		  [x][1] - Xml do Evento
		  [x][2] - Id(chave do evento no TSS)
		  [x][3] - RecNo do Evento na sua respectiva tabela
		  [x][4] - Layout que correspondente ao evento
		  [x][5] - Alias correspondente ao Evento 
@param cAmbES	 - Ambiente de Transmiss�o/Consulta 		  
@param nRegsOk   - Numero de registros Integrados com sucesso   
@param cIdThread - Id da Thread que est� executando o processamento (Job)
@param lErroSch  - Informa se houve erro de erro de Schema
@param lErroSrv  - Informa se houve erro no envio para o servidor TSS

		 
@return lRegs [x][1] - Determina se o lote foi processado com sucesso (logico)
			  [x][2] - Descri��o do erro  (Caso houver)
			  [x][3] - Status dos eventos
			  	 [3][x][1] - Determina se o evento foi transmitido com sucesso (logico)
			  	 [3][x][2] - Layout do evento
			  	 [3][x][3] - Id (chave do evento no TSS)
			  	 [3][x][4] - Descri��o do Resultado
	     lJob - Informa se o processo est� sendo executado via JOB.
			  	 
@author Evandro dos Santos Oliveira
@since 07/11/2013
@version 1.0
/*/
//---------------------------------------------------------------------------   
Static Function TAFEvXml(aXmls,cAmbES,nRegsOk,lJob,cIdThread,lErroSch,lErroSrv)

	Local oReinf 		:= Nil
	Local cUrl			:= GetMv("MV_TAFSURL")
	Local cVerSchema	:= SuperGetMv('MV_TAFVLRE',.F.,"1_03_00")
	Local cCheckURL		:= ""
	Local cMsgRetEnv	:= ""
	Local nY 			:= 1
	Local lRetWS		:= .F.
	Local aRetEvts		:= {}
	Local lOk			:= .T.
	Local oHashXML		:= Nil
	Local xRetXML		:= Nil
	Local cDescRet		:= ""
	Local cDescErro		:= ""
	Local cTabOpen		:= ""
	Local cAliasTb		:= ""

	Default cAmbES		:= "2"
	Default aXmls  		:= {}
	Default nRegsOk 	:= 0
	Default cIdThread 	:= ""

	cIdEnt  := TAFRIdEnt(,,,,,.T.)
	cUserTk := "TOTVS"

	If Empty(AllTrim(cUrl))
		cDescErro := "O par�metro MV_TAFSURL n�o est� preenchido"
		lOk := .F.
	Else

		If !("TSSWSREINF.APW" $ Upper(cUrl))
			cCheckURL := cUrl
			cUrl += "/TSSWSREINF.apw"
		Else
			cCheckURL := Substr(cUrl,1,Rat("/",cUrl)-1)
		EndIf

		If TAFCTSpd(cCheckURL)

			oReinf 	   											:= WSTSSWSREINF():New()
			oReinf:oWSREINFENVIO:oWSCABEC						:= WsClassNew("TSSWSREINF_REINFCABEC")
			oReinf:_Url 										:= cUrl
			oReinf:oWSREINFENVIO:oWSCABEC:cUSERTOKEN 			:= cUserTk
			oReinf:oWSREINFENVIO:oWSCABEC:cENTIDADE    			:= cIdEnt
			oReinf:oWSREINFENVIO:oWSCABEC:cAMBIENTE   			:= cAmbES
			oReinf:oWSREINFENVIO:oWSCABEC:cVERSAO				:= cVerSchema

			oReinf:oWSREINFENVIO:oWSEVENTOS						:= WsClassNew("TSSWSREINF_ARRAYOFREINFENVIOEVENTO")
			oReinf:oWSREINFENVIO:oWSEVENTOS:oWSREINFENVIOEVENTO	:= {}

			For nY := 1 To Len(aXmls)
				xTAFMsgJob("Iniciando Transmissao - Layout " + aXmls[nY][4] + " - " + "Id" + aXmls[nY][2]) //"Iniciando Transmissao - Layout "#"Id"
				aAdd(oReinf:oWSREINFENVIO:oWSEVENTOS:oWSREINFENVIOEVENTO,WsClassNew("TSSWSREINF_REINFENVIOEVENTO"))
				Atail(oReinf:oWSREINFENVIO:oWSEVENTOS:oWSREINFENVIOEVENTO):CCODIGO	:= aXmls[nY][4]
				Atail(oReinf:oWSREINFENVIO:oWSEVENTOS:oWSREINFENVIOEVENTO):CID		:= aXmls[nY][2]
				Atail(oReinf:oWSREINFENVIO:oWSEVENTOS:oWSREINFENVIOEVENTO):CXML		:= aXmls[nY][1] //Encode64(aXmls[nY][1])
			Next nY

			lRetWS := oReinf:ENVIAREVENTOS()
			If ValType(lRetWS) == "L"

				If lRetWS

					oHashXML	:=	AToHM(aXmls, 2, 3 )
					aXmls := {}

					If ValType(oReinf:oWSENVIAREVENTOSRESULT:oWSREINFRETENVIO) <> "U"
						For nY := 1 To Len(oReinf:oWSENVIAREVENTOSRESULT:oWSREINFRETENVIO)

							cIdAux := AllTrim(oReinf:oWSENVIAREVENTOSRESULT:oWSREINFRETENVIO[nY]:CID)
							HMGet( oHashXML , cIdAux ,@xRetXML )
							If !Empty(xRetXML[1][3])
								cAliasTb := xRetXML[1][5]
								If !(cAliasTb $ cTabOpen)
									cTabOpen += "|" + cAliasTb
									dbSelectArea(cAliasTb)
								EndIf
								(cAliasTb)->(dbGoTo(xRetXML[1][3]))
								RecLock((cAliasTb),.F.)

								If oReinf:oWSENVIAREVENTOSRESULT:oWSREINFRETENVIO[nY]:lSucesso
									aAdd(aRetEvts,{.T.,xRetXML[1][4],cIdAux,STR0015,""}) //"Transmitido com Sucesso."
									(cAliasTb)->&(cAliasTb+"_STATUS") := '2'
									nRegsOK++
								Else
									If AllTrim(oReinf:oWSENVIAREVENTOSRESULT:oWSREINFRETENVIO[nY]:CCODIGO) $ "203"
										cMsgRetEnv := TafValSche(xRetXML[1][1], xRetXML[1][4], cIdAux, cUrl, cUserTk, cIdEnt, cAmbES, cVerSchema)
									Else
										cMsgRetEnv := oReinf:oWSENVIAREVENTOSRESULT:oWSREINFRETENVIO[nY]:CDESCRICAO
									EndIf

									//evento com inconsist�ncia.
									aAdd(aRetEvts,{.F.,xRetXML[1][4],cIdAux,cMsgRetEnv,"S"})
									If lJob
										cLog := "* Retorno Com Erro : " + cIdThread + " - Hora: " + DTOC(dDataBase) + " - " + Time() + CRLF
										cLog += oReinf:oWSENVIAREVENTOSRESULT:oWSREINFRETENVIO[nY]:CDESCRICAO
										ConOut(cLog)
									Endif
									(cAliasTb)->&(cAliasTb+"_STATUS") := '3'
									lErroSch := .T.
									lOk := .F.
								EndIf
								(cAliasTb)->(MsUnlock())
							Else
								aAdd(aRetEvts,{.F.,xRetXML[1][4],cIdAux,STR0016,"A"}) //"N�o encontrado no lote de envio"
							EndIf

						Next nY
					Else
						cDescErro := "Tipo de dado Indefinido no retorno do WS." //"Tipo de dado Indefinido no retorno do WS."
						lOk := .F.
						lErroSrv := .T.
					EndIf
				Else
					cDescErro := "Servidor TSS n�o conseguiu processar a requisi��o."
					cDescErro += CRLF + IIf(Empty(GetWscError(3)),GetWscError(1),GetWscError(3))
					lOk := .F.
					lErroSrv := .T.
				EndIf
			Else
				cDescErro := "Retorno do WS n�o � do Tipo L�gico." //"Retorno do WS n�o � do Tipo L�gico."
				cDescErro += CRLF + IIf(Empty(GetWscError(3)),GetWscError(1),GetWscError(3))
				lOk := .F.
				lErroSrv := .T.
			EndIf
		Else
			cDescErro := "N�o foi possivel conectar com o servidor TSS"	  //"N�o foi possivel conectar com o servidor TSS"
			lOk := .F.
			lErroSrv := .T.
		EndIf
	EndIf

	ConOut(cDescErro)

Return {lOk,cDescErro,aRetEvts}

//-------------------------------------------------------------------
/*/{Protheus.doc} TafValSche
Informacoes de definicao dos parametros do schedule
@Return  Array com as informacoes de definicao dos parametros do schedule
		 Array[x,1] -> Caracter, Tipo: "P" - para Processo, "R" - para Relatorios
		 Array[x,2] -> Caracter, Nome do Pergunte
		 Array[x,3] -> Caracter, Alias(para Relatorio)
		 Array[x,4] -> Array, Ordem(para Relatorio)
		 Array[x,5] -> Caracter, Titulo(para Relatorio)

@author Evandro dos Santos Oliveira	
@since  17/05/2016
@version 1.0

/*///----------------------------------------------------------------
Function TafValSche( cXml, cCodReinf, cIdReinf, cUrl, cUserTk, cIdEnt, cAmbES, cVerSchema )

	Local oReinfSche	:= Nil
	Local cMsgRetEnv	:= ""

	oReinfSche 	   							:= WSTSSWSREINF():New()
	oReinfSche:_Url							:= cUrl
	oReinfSche:oWSREINFSCHEMA:cAMBIENTE		:= cAmbES
	oReinfSche:oWSREINFSCHEMA:cCODIGO		:= cCodReinf
	oReinfSche:oWSREINFSCHEMA:cENTIDADE		:= cIdEnt
	oReinfSche:oWSREINFSCHEMA:cID			:= cIdReinf
	oReinfSche:oWSREINFSCHEMA:cUSERTOKEN	:= cUserTk
	oReinfSche:oWSREINFSCHEMA:cVERSAO		:= cVerSchema
	oReinfSche:oWSREINFSCHEMA:cXML			:= cXml

	lRetWS := oReinfSche:VALIDARSCHEMA()
	If ValType(lRetWS) == "L"
		If lRetWS
			If ValType(oReinfSche:oWSVALIDARSCHEMARESULT) <> "U"
				If !oReinfSche:oWSVALIDARSCHEMARESULT:lSTATUS
					cMsgRetEnv := oReinfSche:oWSVALIDARSCHEMARESULT:cERRO
				EndIf
			EndIf
		EndIf
	EndIf

Return ( cMsgRetEnv )

//-------------------------------------------------------------------
/*/{Protheus.doc} SchedDef
Informacoes de definicao dos parametros do schedule
@Return  Array com as informacoes de definicao dos parametros do schedule
		 Array[x,1] -> Caracter, Tipo: "P" - para Processo, "R" - para Relatorios
		 Array[x,2] -> Caracter, Nome do Pergunte
		 Array[x,3] -> Caracter, Alias(para Relatorio)
		 Array[x,4] -> Array, Ordem(para Relatorio)
		 Array[x,5] -> Caracter, Titulo(para Relatorio)

@author Evandro dos Santos Oliveira	
@since  17/05/2016
@version 1.0

/*///----------------------------------------------------------------
Static Function SchedDef()
	Local aParam  := {}

	aParam  := { "P",;			//Tipo R para relatorio P para processo
	"TAFESXTSS",;	//Pergunte do relatorio, caso nao use passar ParamDef
	,"SM0";			//Alias
	,;			//Array de ordens
	}				//Titulo

Return ( aParam )
//-------------------------------------------------------------------
/*/{Protheus.doc} TAFCheckPred
Function para validar a predecess�o de cada evento
@param	cEvento - Nome do evento(Ex: S-1010)  
		cAlias	- Alias do evento  
		cIdChave- Id da chave do registro pai 
		nRecno	- Recno do registro	
		cIdUnic - Id Unico Utilizado para identifica��o do registro
		lJob	- Identificador de execu��o autom�tica	
		cKeyId  - Chave com a Filial da query de trabalho utilizada para identifica��o do registro
@return  aEvt   - Array com o eventos que precisam ser transmitidos 

@author Vitor Siqueira
@since  26/07/2016
@version 1.0

/*///----------------------------------------------------------------
Static Function TAFCheckPred(cEvento, cAlias, cIdChave, nRecno,cIdUnic,lJob, cKeyId )
	Local cIdEvt      := ""
	Local cRecChv     := ""
	Local cAliasEvt   := ""
	Local cEvt	      := ""
	Local cMsgPred	  := ""
	Local nInd        := 0
	Local nY	      := 0
	Local nZ		  := 0
	Local nCnt		  := 1
	Local aEvt	      := {}
	Local aTafRotn    := {}
	Local aQtdEvt	  := {}
	Local lPred		  := .T. // T-representa que as predec. j� foram enviadas para o ret
	Local lHabilitado := .T.
	Local aAreaC9V	  := {}
	Local lNewCheck	  := AllTrim( cFilAnt ) <> AllTrim(SM0->M0_CODFIL)
	Default cEvento	  := ""
	Default	cAlias	  := ""
	Default cIdChave  := ""
	Default nRecno	  := 0
	Default cKeyId    := ""

	aAreaC9V := C9V->(GetArea())

	cIdEvt 	 := Posicione("C8E",2,xFilial("C8E") + cEvento ,"C8E_ID" )

	(cAlias)->( DbGoTo( nRecno ) )

	DbSelectArea("LEJ")
	LEJ->(dbSetOrder(2))
	If LEJ->( MSSeek( xFilial("LEJ") + cIdEvt) )
		If LEJ->LEJ_HABILI == "0"
			lHabilitado = .F.
		EndIf
	EndIf

// Verifica��o para S-1000
	If Upper(cEvento) <> "S-1000" .AND. lHabilitado
		C1E ->( DbSetOrder( 3 ) )
		If C1E ->( MsSeek ( xFilial("C1E")+ cFilAnt+"1"))
			If C1E->C1E_STATUS  <> '4'
				// Prote��o para evitar os erros de filiais fora de sincronia
				If lNewCheck
					cQryMatriz	:= "SELECT Count(*) FROM "+RetSqlname("C1E") +" C1E "
					cQryMatriz	+= "WHERE C1E_STATUS ='4' AND  "
					cQryMatriz	+= "C1E_MATRIZ ='T' AND  "
					cQryMatriz	+= "LEFT(C1E_CODFIL,2) ='"+SM0->M0_CODIGO+"' AND  "
					cQryMatriz	+= "D_E_L_E_T_ ='' "

					aMatriz 	:= TafQryArr( cQryMatriz )

					If !Empty( aMatriz ) .And. aMatriz[01][01] <> 1
						aAdd(aEvt,'S-1000')
					EndIf
				Else
					aAdd(aEvt,'S-1000')
				EndIf
			EndIf
		EndIf
	EndIf


	If Upper(cEvento) == "S-1005" .AND. lHabilitado

		//S-1070
		If !Empty((cAlias)->C92_PROCAP)
			If Posicione("C1G",8,xFilial("C1G")+(cAlias)->C92_PROCAP+"1","C1G_STATUS")    <> '4'
				aAdd(aEvt,'S-1070')
			EndIf
		EndIf

		//S-1070
		If !Empty((cAlias)->C92_PROCPD)
			If Posicione("C1G",8,xFilial("C1G")+(cAlias)->C92_PROCPD+"1","C1G_STATUS")    <> '4'
				aAdd(aEvt,'S-1070')
			EndIf
		EndIf

		//S-1070
		If !Empty((cAlias)->C92_PROFAP)
			If Posicione("C1G",8,xFilial("C1G")+(cAlias)->C92_PROFAP+"1","C1G_STATUS")    <> '4'
				aAdd(aEvt,'S-1070')
			EndIf
		EndIf

		//S-1070
		If !Empty((cAlias)->C92_PRORAT)
			If Posicione("C1G",8,xFilial("C1G")+(cAlias)->C92_PRORAT+"1","C1G_STATUS")    <> '4'
				aAdd(aEvt,'S-1070')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-1010" .AND. lHabilitado

		//S-1070
		If !Empty((cAlias)->C8R_PROCCP)
			If Posicione("C1G",8,xFilial("C1G")+(cAlias)->C8R_PROCCP+"1","C1G_STATUS")    <> '4'
				aAdd(aEvt,'S-1070')
			EndIf
		EndIf

		//S-1070
		If !Empty((cAlias)->C8R_PROCCS)
			If Posicione("C1G",8,xFilial("C1G")+(cAlias)->C8R_PROCCS+"1","C1G_STATUS")    <> '4'
				aAdd(aEvt,'S-1070')
			EndIf
		EndIf

		//S-1070
		If !Empty((cAlias)->C8R_PROCFG)
			If Posicione("C1G",8,xFilial("C1G")+(cAlias)->C8R_PROCFG+"1","C1G_STATUS")    <> '4'
				aAdd(aEvt,'S-1070')
			EndIf
		EndIf

		//S-1070
		If !Empty((cAlias)->C8R_PROCIR)
			If Posicione("C1G",8,xFilial("C1G")+(cAlias)->C8R_PROCIR+"1","C1G_STATUS")    <> '4'
				aAdd(aEvt,'S-1070')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-1020" .AND. lHabilitado


		DbSelectArea("T03")
		T03->( DbSetOrder( 1 ) )
		If T03->( MsSeek ( xFilial("T03")+(cAlias)->(C99_ID+C99_VERSAO)))

			While !T03->(Eof()) .And. AllTrim((cAlias)->(C99_ID+C99_VERSAO)) == AllTrim(T03->(T03_ID+T03_VERSAO))

				//S-1070
				If !Empty(T03->T03_IDPROC)
					If Posicione("C1G",8,xFilial("C1G")+T03->T03_IDPROC+"1","C1G_STATUS") <> '4'
						aAdd(aEvt,'S-1070')
					EndIf
				EndIf

				T03->(DbSkip())
			EndDo
		EndIf

	ElseIf Upper(cEvento) == "S-1060" .AND. lHabilitado

		If (cAlias)->T04_LOCAMB == "1"
			//S-1005
			If !Empty((cAlias)->T04_NRINSC)
				If Posicione("C92",6,xFilial("C92") + "1" + Alltrim((cAlias)->T04_NRINSC) + "1","C92_STATUS") <> "4"
					aAdd(aEvt,'S-1005')
				Endif
			Endif
		Else
			//S-1020
			If !Empty((cAlias)->T04_NRINSC)
				If Posicione("C99",7,xFilial("C99") + "1" + Alltrim((cAlias)->T04_NRINSC) + "1","C99_STATUS") <> "4"
					aAdd(aEvt,'S-1020')
				Endif
			Endif
		EndIf

	ElseIf Upper(cEvento) == "S-1200" .AND. lHabilitado

		//S-1005
		DbSelectArea("T6W")
		T6W->( DbSetOrder( 1 ) )
		If T6W->( MsSeek ( xFilial("T6W")+(cAlias)->(C91_ID+C91_VERSAO)))

			While !T6W->(Eof()) .And. AllTrim((cAlias)->(C91_ID+C91_VERSAO)) == AllTrim(T6W->(T6W_ID+T6W_VERSAO))
				If !Empty(T6W->T6W_NRINSC)
					If Posicione("C92",6,xFilial("C92") + "1" + T6W->T6W_NRINSC + "1","C92_STATUS") <> "4"
						aAdd(aEvt,'S-1005')
					EndIf
				EndIf

				T6W->(DbSkip())
			EndDo
		EndIf

		//S-1070
		DbSelectArea("CRN")
		CRN->( DbSetOrder( 1 ) )
		If CRN->( MsSeek ( xFilial("CRN")+(cAlias)->(C91_ID+C91_VERSAO)))

			While CRN->(!Eof()) .And. AllTrim((cAlias)->(C91_ID+C91_VERSAO)) == AllTrim(CRN->(CRN_ID+CRN_VERSAO))
				If !Empty(CRN->CRN_IDPROC)
					If Posicione("C1G",8,xFilial("C1G") + CRN->CRN_IDPROC+ "1","C1G_STATUS") <> "4"
						aAdd(aEvt,'S-1070')
					EndIf
				EndIf

				CRN->(DbSkip())
			EndDo
		EndIf

		DbSelectArea("T14")
		DbselectArea("C9K")
		DbselectArea("C9L")
		DbselectArea("C9M")
		DbselectArea("T6Y")
		DbselectArea("T6Z")
		DbselectArea("C9Q")
		DbselectArea("C9P")
		DbselectArea("C9O")
		DbselectArea("C9N")
		DbselectArea("C9R")
		T14->( DbSetOrder( 1 ) )
		If T14->( MsSeek ( xFilial("T14")+(cAlias)->(C91_ID+C91_VERSAO)))
			While !T14->(Eof()) .And. AllTrim((cAlias)->(C91_ID+C91_VERSAO)) == AllTrim(T14->(T14_ID+T14_VERSAO))

				C9K->( DbSetOrder( 1 ) )
				If C9K->( MsSeek ( xFilial("C9K")+T14->(T14_ID+T14_VERSAO+T14_IDEDMD)))
					While !C9K->(Eof()) .And. AllTrim(C9K->(C9K_ID+C9K_VERSAO+C9K_RECIBO)) == AllTrim(T14->(T14_ID+T14_VERSAO+T14_IDEDMD))

						//S-1005
						If !Empty(C9K->C9K_ESTABE)
							If Posicione("C92",5,xFilial("C92") + C9K->C9K_ESTABE +"1","C92_STATUS") <> "4"
								aAdd(aEvt,'S-1005')
							EndIf
						EndIf

						//S-1020
						If !Empty(C9K->C9K_LOTACA)
							If Posicione("C99",4,xFilial("C99") + C9K->C9K_LOTACA +"1","C99_STATUS") <> "4"
								aAdd(aEvt,'S-1020')
							EndIf
						EndIf

						C9L->( DbSetOrder( 1 ) )
						If C9L->( MsSeek ( xFilial("C9L")+C9K->(C9K_ID+C9K_VERSAO+C9K_RECIBO+C9K_ESTABE+C9K_LOTACA)))
							While !C9L->(Eof()) .And. AllTrim(C9K->(C9K_ID+C9K_VERSAO+C9K_RECIBO+C9K_ESTABE+C9K_LOTACA)) == AllTrim(C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA))

								//S-2200
								If !Empty(C9L->C9L_TRABAL)
									If Posicione("C9V",2,xFilial("C9V") + C9L->C9L_TRABAL +"1","C9V_STATUS") <> "4"
										aAdd(aEvt,'S-2200')
									EndIf
								EndIf

								C9M->( DbSetOrder( 1 ) )
								If C9M->( MsSeek ( xFilial("C9M")+C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)))
									While !C9M->(Eof()) .And. AllTrim(C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)) == AllTrim(C9M->(C9M_ID+C9M_VERSAO+C9M_RECIBO+C9M_ESTABE+C9M_LOTACA+C9M_TRABAL))

										//S-1010
										If !Empty(C9M->C9M_CODRUB)
											If Posicione("C8R",5,xFilial("C8R") + C9M->C9M_CODRUB +"1","C8R_STATUS") <> "4"
												If Posicione( "C8R",1,cKeyId+"1","C8R_STATUS")  <> "4"
													aAdd(aEvt,'S-1010')
												EndIf
											EndIf
										EndIf

										C9M->(DbSkip())
									EndDo
								EndIf

								T6Y->( DbSetOrder( 1 ) )
								If T6Y->( MsSeek ( xFilial("T6Y")+C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)))

									While !T6Y->(Eof()) .And. AllTrim(C9L->(C9L_ID+C9L_VERSAO+C9L_RECIBO+C9L_ESTABE+C9L_LOTACA+C9L_TRABAL)) == AllTrim(T6Y->(T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_ESTABE+T6Y_LOTACA+T6Y_TRABAL))
										T6Z->( DbSetOrder( 1 ) )
										If T6Z->( MsSeek ( xFilial("T6Z")+T6Y->(T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_ESTABE+T6Y_LOTACA+T6Y_TRABAL+T6Y_CNPJOP)))
											While !T6Z->(Eof()) .And. AllTrim(T6Y->(T6Y_ID+T6Y_VERSAO+T6Y_RECIBO+T6Y_ESTABE+T6Y_LOTACA+T6Y_TRABAL+T6Y_CNPJOP)) == AllTrim(T6Z->(T6Z_ID+T6Z_VERSAO+T6Z_RECIBO+T6Z_ESTABE+T6Z_LOTACA+T6Z_TRABAL+T6Z_CNPJOP))

												//S-2200
												If !Empty(T6Z->T6Z_CPFDEP)
													If Posicione("C9V",3,xFilial("C9V") + T6Z->T6Z_CPFDEP +"1","C9V_STATUS") <> "4"
														aAdd(aEvt,'S-2200')
													EndIf
												EndIf
												T6Z->(DbSkip())
											EndDo
										EndIf

										T6Y->(DbSkip())
									EndDo
								EndIf

								C9L->(DbSkip())
							EndDo
						EndIf

						C9K->(DbSkip())
					EndDo
				EndIf

				C9N->( DbSetOrder( 1 ) )
				If C9N->( MsSeek ( xFilial("C9N")+T14->(T14_ID+T14_VERSAO+T14_IDEDMD)))
					While !C9N->(Eof()) .And. AllTrim(T14->(T14_ID+T14_VERSAO+T14_IDEDMD)) == AllTrim(C9N->(C9N_ID+C9N_VERSAO+C9N_RECIBO))

						C9O->( DbSetOrder( 1 ) )
						If C9O->( MsSeek ( xFilial("C9O")+C9N->(C9N_ID+C9N_VERSAO+C9N_RECIBO+DTOS(C9N_DTACOR)+C9N_TPACOR+C9N_COMPAC)))
							While !C9O->(Eof()) .And.  AllTrim(C9N->(C9N_ID+C9N_VERSAO+C9N_RECIBO+DTOS(C9N_DTACOR)+C9N_TPACOR+C9N_COMPAC)) == AllTrim(C9O->(C9O_ID+C9O_VERSAO+C9O_RECIBO+DTOS(C9O_DTACOR)+C9O_TPACOR+C9O_COMPAC))

								C9P->( DbSetOrder( 1 ) )
								If C9P->( MsSeek ( xFilial("C9P")+C9O->(C9O_ID+C9O_VERSAO+C9O_RECIBO+DTOS(C9O_DTACOR)+C9O_TPACOR+C9O_COMPAC+C9O_PERREF)))
									While !C9P->(Eof()) .And. AllTrim(C9O->(C9O_ID+C9O_VERSAO+C9O_RECIBO+DTOS(C9O_DTACOR)+C9O_TPACOR+C9O_COMPAC+C9O_PERREF)) == AllTrim(C9P->(C9P_ID+C9P_VERSAO+C9P_RECIBO+DTOS(C9P_DTACOR)+C9P_TPACOR+C9P_COMPAC+C9P_PERREF))

										//S-1005
										If !Empty(C9P->C9P_ESTABE)
											If Posicione("C92",5,xFilial("C92") + C9P->C9P_ESTABE +"1","C92_STATUS") <> "4"
												aAdd(aEvt,'S-1005')
											EndIf
										EndIf

										C9Q->( DbSetOrder( 1 ) )
										If C9Q->( MsSeek ( xFilial("C9Q")+C9P->(C9P_ID+C9P_VERSAO+C9P_RECIBO+DTOS(C9P_DTACOR)+C9P_TPACOR+C9P_COMPAC+C9P_PERREF+C9P_ESTABE+C9P_LOTACA)))
											While !C9Q->(Eof()) .And. AllTrim(C9P->(C9P_ID+C9P_VERSAO+C9P_RECIBO+DTOS(C9P_DTACOR)+C9P_TPACOR+C9P_COMPAC+C9P_PERREF+C9P_ESTABE+C9P_LOTACA)) == AllTrim(C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+C9Q_COMPAC+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA))

												//S-2200
												If !Empty(C9Q->C9Q_TRABAL)
													If Posicione("C9V",2,xFilial("C9V") + C9Q->C9Q_TRABAL +"1","C9V_STATUS") <> "4"
														aAdd(aEvt,'S-2200')
													EndIf
												EndIf

												C9R->( DbSetOrder( 1 ) )
												If C9R->( MsSeek ( xFilial("C9R")+C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+C9Q_COMPAC+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_TRABAL)))
													While !C9R->(Eof()) .And. AllTrim(C9Q->(C9Q_ID+C9Q_VERSAO+C9Q_RECIBO+DTOS(C9Q_DTACOR)+C9Q_TPACOR+C9Q_COMPAC+C9Q_PERREF+C9Q_ESTABE+C9Q_LOTACA+C9Q_TRABAL)) == AllTrim(C9R->(C9R_ID+C9R_VERSAO+C9R_RECIBO+DTOS(C9R_DTACOR)+C9R_TPACOR+C9R_COMPAC+C9R_PERREF+C9R_ESTABE+C9R_LOTACA+C9R_TRABAL))

														//S-1010
														If !Empty(C9R->C9R_CODRUB)
															If Posicione("C8R",3,xFilial("C8R") + C9R->C9R_CODRUB +"1","C8R_STATUS") <> "4"
																If Posicione( "C8R",1,cKeyId+"1","C8R_STATUS")  <> "4"
																	aAdd(aEvt,'S-1010')
																EndIf
															EndIf
														EndIf
														C9R->(DbSkip())
													EndDo
												EndIf
												C9Q->(DbSkip())
											EndDo
										EndIf
										C9P->(DbSkip())
									EndDo
								EndIf
								C9O->(DbSkip())
							EndDo
						EndIf
						C9N->(Dbskip())
					EndDo
				EndIf

				T14->(DbSkip())
			EndDo
		EndIf


	ElseIf Upper(cEvento) == "S-1210" .AND. lHabilitado

		//S-2200
		If !Empty((cAlias)->T3P_BENEFI)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->T3P_BENEFI +"1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200 ou S-2300')
			EndIf
		EndIf

		DbselectArea("T3Q")
		DbselectArea("T3R")
		DbselectArea("LE2")
		T3Q->( DbSetOrder( 1 ) )
		If T3Q->( MsSeek( xFilial( "T3Q" ) + (cAlias)->T3P_ID + (cAlias)->T3P_VERSAO) )
			While T3Q->(!Eof()) .And. T3Q->T3Q_ID+T3Q->T3Q_VERSAO == (cAlias)->T3P_ID+(cAlias)->T3P_VERSAO

				T3R->( DbSetOrder( 1 ) )
				If T3R->( MsSeek( xFilial( "T3R" )+T3Q->T3Q_ID+T3Q->T3Q_VERSAO+DTOS(T3Q->T3Q_DTPGTO)+T3Q->T3Q_TPPGTO) )
					While T3R->(!Eof()) .And. T3R->T3R_ID + T3R->T3R_VERSAO + DTOS(T3R->T3R_DTPGTO) + T3R->T3R_TPPGTO == T3Q->T3Q_ID + T3Q->T3Q_VERSAO + DTOS(T3Q->T3Q_DTPGTO) + T3Q->T3Q_TPPGTO

						//S-1200 ou 1202
						If !Empty(T3R->T3R_IDEDMD)
							cId1200 := Posicione("T14",2,xFilial("T14") + T3R->T3R_IDEDMD,"T14_ID")
							If !Empty(cId1200)
								If Posicione("C91",3,xFilial("C91") + cId1200 +"1","C91_STATUS") <> "4"
									aAdd(aEvt,'S-1200 ou S-1202')
								EndIf
							EndIf

						EndIf

						LE2->( DbSetOrder( 1 ) )
						If LE2->(MsSeek( xFilial( "LE2" )+T3R->(T3R_ID+T3R_VERSAO+DTOS(T3R_DTPGTO)+T3R_TPPGTO+T3R_PERREF+T3R_IDEDMD+T3R_INDPGT)))
							While LE2->(!Eof()) .And. LE2->(LE2_ID+LE2_VERSAO+DTOS(LE2_DTPGTO)+LE2_TPPGTO+LE2_PERREF+LE2_IDEDMD+LE2_INDPGT) == T3R->(T3R_ID+T3R_VERSAO+DTOS(T3R_DTPGTO)+T3R_TPPGTO+T3R_PERREF+T3R_IDEDMD+T3R_INDPGT)

								//S-1010
								If !Empty(LE2->LE2_IDRUBR)
									If Posicione("C8R",5,xFilial("C8R") + LE2->LE2_IDRUBR +"1","C8R_STATUS") <> "4"
										aAdd(aEvt,'S-1010')
									EndIf
								EndIf
								LE2->(DbSkip())
							EndDo
						EndIf
						T3R->(DbSkip())
					EndDo
				EndIf
				T3Q->(DbSkip())
			EndDo
		EndIf

	ElseIf Upper(cEvento) == "S-1250" .AND. lHabilitado

		//S-1005
		If !Empty((cAlias)->CMR_IDESTA)
			If Posicione("C92",5,xFilial("C92") + (cAlias)->CMR_IDESTA + "1","C92_STATUS") <> "4"
				aAdd(aEvt,'S-1005')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-1260" .AND. lHabilitado

		//S-1070
		DbselectArea("T1N")
		DbselectArea("T1P")
		T1N->(DBSetOrder(1))
		If T1N->(MsSeek(xFilial("T1N")+(cAlias)->(T1M_ID+T1M_VERSAO+T1M_IDESTA)))
			While ("T1N")->(!Eof()) .And. T1N->T1N_ID+T1N->T1N_VERSAO+T1N->T1N_IDESTA == (cAlias)->(T1M_ID+T1M_VERSAO+T1M_IDESTA)

				T1P->(DBSetOrder(1))
				If T1P->(MsSeek(xFilial("T1P")+T1N->T1N_ID+T1N->T1N_VERSAO+T1N->T1N_NRINSC+T1N->T1N_INDCOM))
					While ("T1P")->(!Eof()) .And. T1P->T1P_ID+T1P->T1P_VERSAO+T1P->T1P_NRINSC+T1P->T1P_INDCOM == T1N->T1N_ID+T1N->T1N_VERSAO+T1N->T1N_NRINSC+T1N->T1N_INDCOM

						If !Empty(T1P->T1P_IDPROC)
							If Posicione("C1G",8,xFilial("C1G") + T1P->T1P_IDPROC+ "1","C1G_STATUS") <> "4"
								aAdd(aEvt,'S-1070')
							EndIf
						EndIf

						T1P->(dbSkip())
					EndDo
				EndIf
				T1N->(dbSkip())
			EndDo
		EndIf

	ElseIf Upper(cEvento) == "S-1270" .AND. lHabilitado

		DbselectArea("LE1")
		DbselectArea("T1X")
		DbselectArea("T1Y")
		T1X->( DbSetOrder( 1 ) )
		If T1X->( MsSeek ( xFilial("T1X")+(cAlias)->(T2A_ID+T2A_VERSAO)))
			While !T1X->(Eof()) .And. AllTrim(T1X->(T1X_ID+T1X_VERSAO)) == AllTrim((cAlias)->(T2A_ID+T2A_VERSAO))

				//S-1005
				If !Empty(T1X->T1X_CNPJSI)
					If Posicione("C92",6,xFilial("C92") + "1" + T1X->T1X_CNPJSI + "1","C92_STATUS") <> "4"
						aAdd(aEvt,'S-1005')
					EndIf
				EndIf

				T1Y->(dbSetOrder(1))
				If T1Y->( msSeek(xFilial("T1Y") + T1X->( T1X_ID + T1X_VERSAO + T1X_CNPJSI ) ) )
					While T1Y->( !Eof() ) .And. Alltrim(T1Y->( T1Y_FILIAL+T1Y_ID+T1Y_VERSAO+T1Y_CNPJSI)) == Alltrim(T1X->( T1X_ID + T1X_VERSAO + T1X_CNPJSI ))

						//S-1005
						If !Empty(T1Y->T1Y_ESTABE)
							If Posicione("C92",5,xFilial("C92") + T1Y->T1Y_ESTABE+ "1","C92_STATUS") <> "4"
								aAdd(aEvt,'S-1005')
							EndIf
						EndIf

						T1Y->(DbSkip())
					Enddo
				Endif


				LE1->(dbSetOrder(1))
				If LE1->( msSeek(xFilial("LE1") + T1X->( T1X_ID + T1X_VERSAO + T1X_CNPJSI ) ) )
					While LE1->( !Eof() ) .And. Alltrim(LE1->( LE1_ID + LE1_VERSAO + LE1_CNPJSI )) == Alltrim(T1X->( T1X_ID + T1X_VERSAO + T1X_CNPJSI ))

						//S-1070
						If !Empty(LE1->LE1_IDPROC)
							If Posicione("C1G",8,xFilial("C1G") + LE1->LE1_IDPROC+ "1","C1G_STATUS") <> "4"
								aAdd(aEvt,'S-1070')
							EndIf
						EndIf

						LE1->(DbSkip())
					Enddo
				Endif

				T1X->(DbSkip())
			EndDo
		Endif

	ElseIf Upper(cEvento) == "S-1280" .AND. lHabilitado

		DbselectArea("T3X")
		T3X->(dbSetOrder(1))
		If T3X->( MsSeek(xFilial("T3X") + (cAlias)->( T3V_ID + T3V_VERSAO) ) )
			While T3X->( !Eof() ) .And. Alltrim((cAlias)->( T3V_ID + T3V_VERSAO)) == Alltrim(T3X->( T3X_ID + T3X_VERSAO))

				//S-1080
				If !Empty(T3X->T3X_IDCNPJ)
					If Posicione("C8W",6,xFilial("C8W") + T3X->T3X_IDCNPJ+ "1","C8W_STATUS") <> "4"
						aAdd(aEvt,'S-1080')
					EndIf
				EndIf

				T3X->(DbSkip())
			Enddo
		Endif

	ElseIf Upper(cEvento) == "S-2200" .AND. lHabilitado

		DbSelectArea("CUP")
		CUP->(dbSetOrder(4))
		If CUP->( MsSeek(xFilial("CUP") + (cAlias)->( C9V_ID + C9V_VERSAO+ C9V_NOMEVE) ) )

			//S-1005
			If !Empty(CUP->CUP_TPINSC) .AND. !Empty(CUP->CUP_NRINSC)
				If Posicione("C92",6,xFilial("C92") + CUP->CUP_TPINSC + CUP->CUP_NRINSC + "1","C92_STATUS") <> "4"
					aAdd(aEvt,'S-1005')
				EndIf
			EndIf

			//S-1030
			If !Empty(CUP->CUP_CODCGO)
				If Posicione("C8V",4,xFilial("C8V") + CUP->CUP_CODCGO+ "1","C8V_STATUS") <> "4"
					aAdd(aEvt,'S-1030')
				EndIf
			EndIf

			//S-1040
			If !Empty(CUP->CUP_CODFUN)
				If Posicione("C8X",3,xFilial("C8X") + CUP->CUP_CODFUN+ "1","C8X_STATUS") <> "4"
					aAdd(aEvt,'S-1040')
				EndIf
			EndIf

			//S-1050
			DbselectArea("CRQ")
			CRQ->(dbSetOrder(1))
			If CRQ->( MsSeek(xFilial("CRQ") + CUP->( CUP_ID + CUP_VERSAO) ) )
				While CRQ->( !Eof() ) .And. Alltrim(CUP->( CUP_ID + CUP_VERSAO)) == Alltrim(CRQ->( CRQ_ID + CRQ_VERSAO))

					If !Empty(CRQ->CRQ_CODHOR)
						If Posicione("C90",3,xFilial("C90") + CRQ->CRQ_CODHOR+ "1","C90_STATUS") <> "4"
							aAdd(aEvt,'S-1050')
						EndIf
					EndIf

					CRQ->(DbSkip())
				Enddo
			Endif

			//S-1070
			If !Empty(CUP->CUP_ALVJUD)
				If Posicione("C1G",8,xFilial("C1G") + CUP->CUP_ALVJUD+ "1","C1G_STATUS") <> "4"
					aAdd(aEvt,'S-1070')
				EndIf
			EndIf

			//S-2190
			If !Empty(CUP->CUP_RECEVT)
				If Posicione("T3A",4,xFilial("T3A") + CUP->CUP_RECEVT+ "1","T3A_STATUS") <> "4"
					aAdd(aEvt,'S-2190')
				EndIf
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2205" .AND. lHabilitado

		//S-2200, S-2205 ou S-2300
		If !Empty((cAlias)->T1U_CPF)
			If Posicione("C9V",3,xFilial("C9V") + (cAlias)->T1U_CPF + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200, S-2205 ou S-2300')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2206" .AND. lHabilitado

		//S-1005
		If !Empty((cAlias)->T1V_TPINSC)
			If Posicione("C92",6,xFilial("C92") + (cAlias)->T1V_TPINSC + (cAlias)->T1V_NRINSC + "1","C92_STATUS") <> "4"
				aAdd(aEvt,'S-1005')
			EndIf
		EndIf

		//S-1030
		If !Empty((cAlias)->T1V_CODCGO)
			If Posicione("C8V",4,xFilial("C8V") + (cAlias)->T1V_CODCGO+ "1","C8V_STATUS") <> "4"
				aAdd(aEvt,'S-1030')
			EndIf
		EndIf

		//S-1040
		If !Empty((cAlias)->T1V_CODFUN)
			If Posicione("C8X",3,xFilial("C8X") + (cAlias)->T1V_CODFUN+ "1","C8X_STATUS") <> "4"
				aAdd(aEvt,'S-1040')
			EndIf
		EndIf

		//S-1050
		DbselectArea("T3U")
		T3U->(dbSetOrder(1))
		If T3U->( MsSeek(xFilial("T3U") + (cAlias)->( T1V_ID + T1V_VERSAO) ) )
			While T3U->( !Eof() ) .And. Alltrim((cAlias)->( T1V_ID + T1V_VERSAO)) == Alltrim(T3U->( T3U_ID + T3U_VERSAO))

				If !Empty(T3U->T3U_CODHOR)
					If Posicione("C90",3,xFilial("C90") + T3U->T3U_CODHOR+ "1","C90_STATUS") <> "4"
						aAdd(aEvt,'S-1050')
					EndIf
				EndIf

				T3U->(DbSkip())
			Enddo
		Endif

		//S-1070
		If !Empty((cAlias)->T1V_ALVJUD)
			If Posicione("C1G",8,xFilial("C1G") + (cAlias)->T1V_ALVJUD+ "1","C1G_STATUS") <> "4"
				aAdd(aEvt,'S-1070')
			EndIf
		EndIf

		//S-2200, S-2205 ou S-2300
		If !Empty((cAlias)->T1V_CPF)
			If Posicione("C9V",3,xFilial("C9V") + (cAlias)->T1V_CPF + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200, S-2205 ou S-2300')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2210" .AND. lHabilitado

		//S-1005
		If !Empty((cAlias)->CM0_CNPJLO)
			If Posicione("C92",6,xFilial("C92") + "1" + (cAlias)->CM0_CNPJLO + "1","C92_STATUS") <> "4"
				aAdd(aEvt,'S-1005')
			EndIf
		EndIf

		//S-2200 ou S-2300
		If !Empty((cAlias)->CM0_TRABAL)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->CM0_TRABAL + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200 ou S-2300')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2220" .AND. lHabilitado


		//S-2200 ou S-2300
		If !Empty((cAlias)->C8B_FUNC)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->C8B_FUNC + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200 ou S-2300')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2230" .AND. lHabilitado

		//S-1005
		If !Empty((cAlias)->CM6_CNPJCE)
			If Posicione("C92",6,xFilial("C92") + "1" + (cAlias)->CM6_CNPJCE + "1","C92_STATUS") <> "4"
				aAdd(aEvt,'S-1005')
			EndIf
		EndIf

		//S-1005
		If !Empty((cAlias)->CM6_CNPJSD)
			If Posicione("C92",6,xFilial("C92") + "1" + (cAlias)->CM6_CNPJSD + "1","C92_STATUS") <> "4"
				aAdd(aEvt,'S-1005')
			EndIf
		EndIf

		//S-2200
		If !Empty((cAlias)->CM6_FUNC)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->CM6_FUNC + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2240" .AND. lHabilitado

		//S-2200
		If !Empty((cAlias)->CM9_FUNC)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->CM9_FUNC + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2241" .AND. lHabilitado

		//S-1060
		DbselectArea("T3C")
		T3C->(dbSetOrder(1))
		If T3C->( MsSeek(xFilial("T3C") + (cAlias)->( T3B_ID + T3B_VERSAO) ) )
			While T3C->( !Eof() ) .And. Alltrim((cAlias)->( T3B_ID + T3B_VERSAO)) == Alltrim(T3C->( T3C_ID + T3C_VERSAO))

				If !Empty(T3U->T3U_CODHOR)
					If Posicione("T04",3,xFilial("T04") + T3U->T3U_CODHOR+ "1","T04_STATUS") <> "4"
						aAdd(aEvt,'S-1060')
					EndIf
				EndIf

				T3C->(DbSkip())
			Enddo
		Endif

		//S-2200
		If !Empty((cAlias)->T3B_IDTRAB)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->T3B_IDTRAB + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200')
			EndIf
		EndIf


	ElseIf Upper(cEvento) == "S-2250" .AND. lHabilitado

		//S-2200
		If !Empty((cAlias)->CM8_TRABAL)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->CM8_TRABAL + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2260" .AND. lHabilitado

		//S-2200
		If !Empty((cAlias)->T87_TRABAL)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->T87_TRABAL + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200')
			EndIf
		EndIf

		//S-1050
		If !Empty((cAlias)->T87_CODHOR)
			If Posicione("C90",3,xFilial("C90") + (cAlias)->T87_CODHOR+ "1","C90_STATUS") <> "4"
				aAdd(aEvt,'S-1050')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2298" .AND. lHabilitado

		//S-1070
		If !Empty((cAlias)->CMF_NRPROC)
			If Posicione("C1G",8,xFilial("C1G") + (cAlias)->CMF_NRPROC+ "1","C1G_STATUS") <> "4"
				aAdd(aEvt,'S-1070')
			EndIf
		EndIf

		//S-2200
		If !Empty((cAlias)->CMF_FUNC)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->CMF_FUNC + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2299" .AND. lHabilitado

		DBSelectArea( "T06" )
		DBSelectArea( "T3G" )
		DBSelectArea( "T05" )
		DBSelectArea( "T15" )
		DBSelectArea( "T16" )
		DBSelectArea( "T88" )
		DBSelectArea( "C9J" )
		DBSelectArea( "T3H" )

		T3H->(dbSetOrder(1))
		If T3H->( MSSeek( xFilial("T3H") + (cAlias)->(CMD_ID + CMD_VERSAO) ) )
			While T3H->(!Eof()) .and. Alltrim(T3H->(T3H_ID + T3H_VERSAO)) == Alltrim((cAlias)->((CMD_ID + CMD_VERSAO)))

				//S-1070
				If !Empty(T3H->T3H_IDPROC)
					If Posicione("C1G",8,xFilial("C1G") + T3H->T3H_IDPROC+ "1","C1G_STATUS") <> "4"
						aAdd(aEvt,'S-1070')
					EndIf
				EndIf

				T3H->(DbSkip())
			Enddo
		Endif

		C9J->(dbSetOrder(1))
		If C9J->( MSSeek( xFilial("C9J") + 	(cAlias)->(CMD_ID + CMD_VERSAO) ) )
			While C9J->(!Eof()) .and. Alltrim(C9J->(C9J_ID + C9J_VERSAO)) == Alltrim((cAlias)->(CMD_ID + CMD_VERSAO))

				//S-1005
				If !Empty(C9J->C9J_TPINSC) .AND. !Empty(C9J->C9J_NRINSC)
					If Posicione("C92",6,xFilial("C92") + C9J->C9J_TPINSC + C9J->C9J_NRINSC + "1","C92_STATUS") <> "4"
						aAdd(aEvt,'S-1005')
					EndIf
				EndIf

				C9J->(DbSkip())
			Enddo
		Endif


		//S-2200
		If !Empty((cAlias)->CMD_FUNC)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->CMD_FUNC + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200')
			EndIf
		EndIf

		T06->( DBSetOrder( 1 ) )
		T3G->( DBSetOrder( 1 ) )
		T05->( DBSetOrder( 1 ) )
		T15->( DBSetOrder( 1 ) )
		T16->( DBSetOrder( 1 ) )
		T88->( DBSetOrder( 1 ) )

		If T06->( MSSeek( xFilial("T06") + 	(cAlias)->(CMD_ID + CMD_VERSAO) ) )
			While T06->(!Eof()) .and. Alltrim(T06->(T06_ID + T06_VERSAO)) == Alltrim((cAlias)->(CMD_ID + CMD_VERSAO))

				If T3G->( MSSeek( xFilial("T3G") + T06->(T06_ID+T06_VERSAO+T06_IDEDMD) ) )
					While T3G->(!Eof()) .and. Alltrim(T06->(T06_ID + T06_VERSAO+T06_IDEDMD)) == Alltrim(T3G->(T3G_ID + T3G_VERSAO + T3G_RECPAG))

						//S-1005
						If !Empty(T3G->T3G_ESTABE)
							If Posicione("C92",5,xFilial("C92") + T3G->T3G_ESTABE +"1","C92_STATUS") <> "4"
								aAdd(aEvt,'S-1005')
							EndIf
						EndIf

						//S-1020
						If !Empty(T3G->T3G_LOTTRB)
							If Posicione("C99",4,xFilial("C99") + T3G->T3G_LOTTRB +"1","C99_STATUS") <> "4"
								aAdd(aEvt,'S-1020')
							EndIf
						EndIf

						If T05->( MSSeek( xFilial("T05") + T3G->(T3G_ID+T3G_VERSAO+T3G_LOTTRB+T3G_RECPAG) ) )
							While T05->(!Eof()) .and. Alltrim(T05->(T05_ID +T05_VERSAO+T05_LOTTRB+T05_RECPAG)) == Alltrim(T3G->(T3G_ID + T3G_VERSAO + T3G_LOTTRB + T3G_RECPAG))

								//S-1010
								If !Empty(T05->T05_CODRUB)
									If Posicione("C8R",3,xFilial("C8R") + T05->T05_CODRUB +"1","C8R_STATUS") <> "4"
										aAdd(aEvt,'S-1010')
									EndIf
								EndIf

								T05->(DbSkip())
							Enddo
						Endif


						If T15->( MSSeek( xFilial("T15") + T3G->(T3G_ID+T3G_VERSAO+T3G_RECPAG+T3G_ESTABE+T3G_LOTTRB) ) )
							While T15->(!Eof()) .and. Alltrim(T15->(T15_ID + T15_VERSAO+T15_RECIBO+T15_ESTABE+T15_LOTACA)) == Alltrim(T3G->(T3G_ID+T3G_VERSAO+T3G_RECPAG+T3G_ESTABE+T3G_LOTTRB))

								If T16->( MSSeek( xFilial("T16") + T15->(T15_ID+T15_VERSAO+T15_RECIBO+T15_ESTABE+T15_LOTACA+T15_CNPJOP) ) )
									While T16->(!Eof()) .and. Alltrim(T16->(T16_ID + T16_VERSAO+T16_RECIBO+T16_ESTABE+T16_LOTACA+T16_CNPJOP)) == Alltrim(T15->(T15_ID+T15_VERSAO+T15_RECIBO+T15_ESTABE+T15_LOTACA+T15_CNPJOP))

										//S-2200
										If !Empty(T16->T16_CPFDEP)
											If Posicione("C9V",3,xFilial("C9V") + T16->T16_CPFDEP + "1","C9V_STATUS") <> "4"
												aAdd(aEvt,'S-2200')
											EndIf
										EndIf
										T16->(DbSkip())
									Enddo
								Endif

								T15->(DbSkip())
							Enddo
						Endif
						T3G->(DbSkip())
					Enddo
				Endif

				If T88->( MsSeek( xFilial( "T88" ) + T06->( T06_ID + T06_VERSAO + T06_IDEDMD ) ) )
					//S-2260
					If !Empty( T88->T88_IDCODC )
						If Posicione( "T87", 4, xFilial( "T87" ) + T88->T88_IDCODC + "1", "T87_STATUS" ) <> "4"
							aAdd( aEvt, "S-2260" )
						EndIf
					EndIf
				EndIf

				T06->(DbSkip())
			EndDo
		EndIf

	ElseIf Upper(cEvento) == "S-2300" .AND. lHabilitado

		DbSelectArea("CUU")
		CUU->(dbSetOrder(1))
		If CUU->( MsSeek(xFilial("CUU") + (cAlias)->( C9V_ID + C9V_VERSAO+ C9V_NOMEVE) ) )

			//S-1030
			If !Empty(CUU->CUU_CARCI)
				If Posicione("C8V",4,xFilial("C8V") + CUU->CUU_CARCI+ "1","C8V_STATUS") <> "4"
					aAdd(aEvt,'S-1030')
				EndIf
			EndIf

			//S-1040
			If !Empty(CUU->CUU_FUNCI)
				If Posicione("C8X",3,xFilial("C8X") + CUU->CUU_FUNCI+ "1","C8X_STATUS") <> "4"
					aAdd(aEvt,'S-1040')
				EndIf
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2305" .AND. lHabilitado

		//S-1030
		If !Empty((cAlias)->T0F_CARCI)
			If Posicione("C8V",4,xFilial("C8V") + (cAlias)->T0F_CARCI+ "1","C8V_STATUS") <> "4"
				aAdd(aEvt,'S-1030')
			EndIf
		EndIf

		//S-1040
		If !Empty((cAlias)->T0F_FUNCI)
			If Posicione("C8X",3,xFilial("C8X") + (cAlias)->T0F_FUNCI+ "1","C8X_STATUS") <> "4"
				aAdd(aEvt,'S-1040')
			EndIf
		EndIf

		//S-2300
		If !Empty((cAlias)->T0F_CPF)
			If Posicione("C9V",4,xFilial("C9V") + (cAlias)->T0F_CPF +"S2300" +"1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2300')
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-2399" .AND. lHabilitado

		DbSelectArea("C9J")
		DbSelectArea("T3H")
		DbSelectArea("T3I")
		DbSelectArea("T3J")
		DbSelectArea("CMK")
		DbSelectArea("T15")
		DbSelectArea("T16")


		//S-2200 ou S-2300
		If TAFAlsInDic("T92")
			If !Empty((cAlias)->T92_TRABAL)
				If Posicione("C9V",2,xFilial("C9V") + (cAlias)->T92_TRABAL + "1","C9V_STATUS") <> "4"
					aAdd(aEvt,'S-2200 ou S-2300')
				EndIf
			EndIf

			T3H->(dbSetOrder(1))
			If T3H->( MSSeek( xFilial("T3H") + (cAlias)->(T92_ID + T92_VERSAO) ) )
				While T3H->(!Eof()) .and. Alltrim(T3H->(T3H_ID + T3H_VERSAO)) == Alltrim((cAlias)->((T92_ID + T92_VERSAO)))

					//S-1070
					If !Empty((cAlias)->T3H_IDPROC)
						If Posicione("C1G",8,xFilial("C1G") + (cAlias)->T3H_IDPROC+ "1","C1G_STATUS") <> "4"
							aAdd(aEvt,'S-1070')
						EndIf
					EndIf

					T3H->(DbSkip())
				Enddo
			Endif

			T3I->(dbSetOrder(1))
			If T3I->( MSSeek( xFilial("T3I") + 	(cAlias)->(T92_ID + T92_VERSAO) ) )
				While T3I->(!Eof()) .and. Alltrim(T3I->(T3I_ID + T3I_VERSAO)) == Alltrim((cAlias)->(T92_ID + T92_VERSAO))

					T3J->(dbSetOrder(1))
					If T3J->( MSSeek( xFilial("T3J") + T3I->(T3I_ID+T3I_VERSAO+T3I_IDEDMD) ) )
						While T3J->(!Eof()) .and. Alltrim(T3I->(T3I_ID + T3I_VERSAO+T3I_IDEDMD)) == Alltrim(T3J->(T3J_ID + T3J_VERSAO + T3J_IDEDMD))

							//S-1005
							If !Empty(T3J->T3J_ESTABE)
								If Posicione("C92",5,xFilial("C92") + T3J->T3J_ESTABE +"1","C92_STATUS") <> "4"
									aAdd(aEvt,'S-1005')
								EndIf
							EndIf

							//S-1020
							If !Empty(T3J->T3J_LOTTRB)
								If Posicione("C99",4,xFilial("C99") + T3J->T3J_LOTTRB +"1","C99_STATUS") <> "4"
									aAdd(aEvt,'S-1020')
								EndIf
							EndIf

							CMK->(dbSetOrder(1))
							If CMK->( MSSeek( xFilial("CMK") + T3J->(T3J_ID+T3J_VERSAO+T3J_IDEDMD+T3J_LOTTRB) ) )
								While CMK->(!Eof()) .and. Alltrim(T3J->(T3J_ID + T3J_VERSAO+T3J_IDEDMD+T3J_LOTTRB)) == Alltrim(CMK->(CMK_ID + CMK_VERSAO + CMK_IDEDMD +CMK_LOTTRB))

									//S-1010
									If !Empty(CMK->CMK_CODRUB)
										If Posicione("C8R",3,xFilial("C8R") + CMK->CMK_CODRUB +"1","C8R_STATUS") <> "4"
											aAdd(aEvt,'S-1010')
										EndIf
									EndIf

									CMK->(DbSkip())
								Enddo
							Endif

							T15->(dbSetOrder(1))
							If T15->( MSSeek( xFilial("T15") + T3J->(T3J_ID+T3J_VERSAO+T3J_IDRPGT+T3J_ESTABE+T3J_LOTTRB) ) )
								While T15->(!Eof()) .and. Alltrim(T15->(T15_ID + T15_VERSAO+T15_RECIBO+T15_ESTABE+T15_LOTACA)) == Alltrim(T3J->(T3J_ID+T3J_VERSAO+T3J_IDRPGT+T3J_ESTABE+T3J_LOTTRB))

									T16->(dbSetOrder(1))
									If T16->( MSSeek( xFilial("T16") + T15->(T15_ID+T15_VERSAO+T15_RECIBO+T15_ESTABE+T15_LOTACA+T15_CNPJOP) ) )
										While T16->(!Eof()) .and. Alltrim(T15->(T15_ID+T15_VERSAO+T15_RECIBO+T15_ESTABE+T15_LOTACA+T15_CNPJOP)) == Alltrim(T16->(T16_ID + T16_VERSAO+T16_RECIBO+T16_ESTABE+T16_LOTACA+T16_LOTACA))

											//S-2200
											If !Empty(T16->T16_CPFDEP)
												If Posicione("C9V",3,xFilial("C9V") + T16->T16_CPFDEP + "1","C9V_STATUS") <> "4"
													aAdd(aEvt,'S-2200')
												EndIf
											EndIf
											T16->(DbSkip())
										Enddo
									Endif

									T15->(DbSkip())
								Enddo
							Endif
							T3J->(DbSkip())
						Enddo
					Endif
					T3I->(DbSkip())
				Enddo
			Endif
		EndIf

	ElseIf Upper(cEvento) == "S-3000" .AND. lHabilitado

		cEvento  := Posicione("C8E",1,xFilial("C8E") + (cAlias)->CMJ_TPEVEN,"C8E_CODIGO")
		aTafRotn := TAFRotinas( cEvento ,4,.F.,2)
		cRecChv  := (cAlias)->CMJ_NRRECI

		If !Empty (aTafRotn)
			cAliasEvt  := aTafRotn[3]
			nInd	   := aTafRotn[13]

			DbselectArea(cAliasEvt)
			(cAliasEvt)->( DbSetOrder( nInd ) )
			If (cAliasEvt)->( MsSeek( xFilial( cAliasEvt ) + cRecChv + '1' ) )

				//Todos os eventos que puderem ser exclu�dos de S-1000 at� S-4000
				If (cAliasEvt)->&(cAliasEvt+"_STATUS") <> "6"
					aAdd(aEvt,cEvento)
				EndIf
			EndIf
		EndIf

	ElseIf Upper(cEvento) == "S-4000" .AND. lHabilitado

		//S-2200 ou S-2300
		If !Empty((cAlias)->T1R_IDTRAB)
			If Posicione("C9V",2,xFilial("C9V") + (cAlias)->T1R_IDTRAB + "1","C9V_STATUS") <> "4"
				aAdd(aEvt,'S-2200 ou S-2300')
			EndIf
		EndIf

	EndIf


	If !Empty(aEvt)

		//Ordena o array de eventos
		ASort(aEvt)

		//Conta e gera a msg a ser gravada na tabela de predecess�es p/ cada evento
		For nY := 1 To Len(aEvt)
			If Ascan(aQtdEvt,{|aVal| aVal[1] == aEvt[nY]}) == 0 // compara para ver se evento ja foi adicionado a array
				aAdd(aQtdEvt, {aEvt[nY], 0})
				For nZ := 1 To Len(aEvt)
					If aEvt[nY] == aEvt[nZ]
						aQtdEvt[nCnt][2]++
					EndIf
				Next nZ
				cMsgPred += aQtdEvt[nCnt][1] +  " | "
				nCnt++
			EndIf
		Next nY


		//Grava o erro de predecess�o na tabela de controle de predecess�o
		TAFGrvPred(cIdEvt, cIdChave, cAlias, SubStr(cMsgPred, 1,Len(cMsgPred)-2),cIdUnic,lJob)

		lPred := .F.
	EndIf

	RestArea(aAreaC9V)

Return (lPred)
//-------------------------------------------------------------------
/*/{Protheus.doc} TAFGRVPRED
Function que grava e apaga informa��o da tabela de controle de predecess�o
@param	cIdEvt    - Id do evento  na C8E
		cIdChave  - Id do registro 
		cAlias	  - Alias do pai
		cMsgPred  - Mensagem a ser gravada
		cIdUnic   - Id Unico utilizado na identifica��o do registro
@author Vitor Siqueira
@since  26/07/2016
@version 1.0

/*///----------------------------------------------------------------
Static Function TAFGrvPred(cIdEvt, cIdChave, cAlias, cMsgPred,cIdUnic,lJob)

	Local cEvento	 := ""
	Local lRec 		 := .T.
	Default cIdEvt	 := ""
	Default cIdChave := ""
	Default cAlias	 := ""
	Default cMsgPred := ""
	Default lJob	 := .F.

	lRec := !TafSeekT0X(cIdUnic)
	cEvento	 := Posicione("C8E",1,xFilial("C8E") + cIdEvt ,"C8E_CODIGO" )

	//Gera uma inconsistencia de predecess�o
	If RecLock( "T0X", lRec)

		If lRec
			T0X->T0X_FILIAL   := xFilial("T0X")

			If GetSx3Cache("T0X_ID"	,"X3_TAMANHO") == 6
				T0X->T0X_ID := GetSx8Num("T0X","T0X_ID")
			Else
				T0X->T0X_ID := TafGeraID("TAF")
			EndIf
		EndIf

		T0X->T0X_IDEVEN   := cIdEvt
		T0X->T0X_IDCHVE   := cIdUnic
		T0X->T0X_PREDEC   := cMsgPred
		T0X->T0X_TPERRO   := "P"
		If lJob
			T0X->T0X_USER := "__Schedule"
		Else
			T0X->T0X_USER := cUserName
		Endif
		T0X->( MsUnlock() )
	EndIf

	//Altera o status do registro para invalido.
	DbSelectArea(cAlias)
	If cAlias == "C9V"

		(cAlias) ->( DbSetOrder( 16 ) )
		If (cAlias) ->( MsSeek ( xFilial(cAlias) + cIdChave + Strtran(cEvento,"-","") + "1"))
			If RecLock( cAlias, .F. )
				(cAlias) ->&(cAlias+"_STATUS")   := "1"
			EndIf
		EndIf
	Else
		(cAlias) ->( DbSetOrder( 1 ) )
		If (cAlias) ->( MsSeek ( xFilial(cAlias)+cIdChave))
			If RecLock( cAlias, .F. )
				(cAlias) ->&(cAlias+"_STATUS")   := "1"
			EndIf
		EndIf
	EndIf

	//N�o � uma boa pratica fechar Alias de dicion�rio. Estava dando colateral na transmiss�o.
	//(cAlias)->(dbCloseArea())
	//("T0X")->(dbCloseArea())

Return ()

//--------------------------------------------------------------------
/*/{Protheus.doc} TafLimpRei

Fun��o utilizada para transmiss�o do evento R-1000 modificado, com o intuito de remover contribuinte da base de dados do Reinf 

@Author	anieli.rodrigues
@Since	24/04/2018

@Version 1.0
/*/
//---------------------------------------------------------------------
Function TafLimpRei()

	Local aAreaT9U		:= T9U->(GetArea())
	Local aAuxRet		:= {}
	Local aRetEvts		:= {}
	Local aXML			:= {}
	Local cAliasRegs	:= GetNextAlias()
	Local cId			:= ""
	Local cQry			:= ""
	Local cXml 			:= ""
	Local lErroSch		:= .F.
	Local lErroSrv		:= .F.
	Local lJob			:= .F.
	Local nQtdRegs		:= 0
	Local nRegsOk		:= 0
	Local oModel494		:= Nil

	If MsgYesNo(STR0029, STR0022) //"Ao selecionar esta op��o, todos os eventos enviados ao ambiente de Produ��o Restrita, inclusive o evento R-1000, ser�o removidos da base de dados do governo. Deseja continuar?" "ATEN��O"

		cQry := "SELECT T9U.R_E_C_N_O_ RECTAB "
		cQry += "FROM " + RetSqlName("C1E") + " C1E "
		cQry += "JOIN " + RetSqlName("T9U") + " T9U "
		cQry += "ON C1E.C1E_ID = T9U.T9U_ID AND "
		cQry += "C1E.C1E_VERSAO = T9U.T9U_VERORI "
		cQry += "WHERE C1E.C1E_FILIAL = '"+xFilial("C1E")+"' "
		cQry += " AND C1E.C1E_FILTAF = '"+cFilAnt+"' "
		cQry += " AND C1E.C1E_ATIVO  = '1' "
		cQry += " AND C1E.C1E_MATRIZ = 'T' "
		cQry += " AND C1E.D_E_L_E_T_ <> '*' "
		cQry += " AND T9U.D_E_L_E_T_ <> '*' "
		cQry += " AND T9U.T9U_ATIVO  = '1' "

		cQry := ChangeQuery(cQry)

		TcQuery cQry New Alias (cAliasRegs)

		Count To nQtdRegs

		If nQtdRegs > 0

			(cAliasRegs)->(dbGoTop())
			T9U->(DbGoTo((cAliasRegs)->RECTAB))

			oModel494 := FWLoadModel("TAFA494")
			oModel494:SetOperation(4)
			oModel494:Activate()
			oModel494:LoadValue( 'MODEL_T9U', 'T9U_VERSAO', xFunGetVer() )
			oModel494:LoadValue( 'MODEL_T9U', 'T9U_EVENTO', "I" )
			FWFormCommit( oModel494 )
			oModel494:DeActivate()

			dbSelectArea("T9U")
			RecLock("T9U", .F.)
			T9U->T9U_STATUS := " "
			T9U->T9U_PROTUL := " "
			T9U->T9U_PROTPN := " "
			MsUnLock()

			cId := "R1000" + T9U->T9U_ID + T9U->T9U_VERSAO

			cXml := TAF494Xml("T9U", T9U->(Recno()), ,.T., .T.)

			aAdd(aXml, {EncodeUTF8(cXml), cId, (cAliasRegs)->RECTAB, "R-1000", "T9U"})

			aAuxRet := TAFEvXml(aXml, "2", @nRegsOk, lJob, , @lErroSch, @lErroSrv,,cVerSchema)
			aAdd(aRetEvts,aClone(aAuxRet))

			If !lErroSch .And. !lErroSrv
				cMsgRet := STR0030 //"Transmiss�o efetuada com sucesso. Verifique o retorno da transmiss�o atrav�s da op��o 'Monitorar Transmiss�es', com o registro R-1000 posicionado e selecionando a op��o 'Tabelas'. Se a situa��o do evento for 'Evento Rejeitado', consulte sua inconsist�ncia. Se a tag 'descRetorno' apresentar o conteudo 'Sucesso', execute a rotina 'Exclus�o por Per�odo Fiscal' para remover os dados da base do TAF."
			ElseIf lErroSch
				cMsgRet := STR0024 //"Ocorreu um erro de schema. Verifique as inconsist�ncias utilizando atrav�s da op��o 'Detalhamento' selecionando o evento S-1000."
			ElseIf lErroSrv
				cMsgRet := STR0025 + aAuxRet[2]
			EndIf

			TAFAviso( STR0031 , cMsgRet , {"OK"}, 3, STR0027) //"Remo��o do contribuinte" "Transmiss�o do evento"
		Else
			TAFAviso( STR0031 , STR0028 , {"OK"}, 3, STR0027) //"Remo��o do contribuinte" "N�o h� nenhum registro v�lido para remo��o" "Transmiss�o do evento"
		EndIf

		DbCloseArea(cAliasRegs)

	EndIf

	If Len(aRetEvts) > 0
		TAFMErrT0X(aRetEvts)
	EndIf

	RestArea(aAreaT9U)

Return