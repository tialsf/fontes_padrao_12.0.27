#INCLUDE "TOTVS.CH" 
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "RESTFUL.CH"

/*/{Protheus.doc} TSIPARTIC
	(Classe que para consula e retorno da mensagem Json )
    @type Class
	@author Henrique Pereira
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/

Class TSIPARTIC
Data cQRY       as String ReadOnly
Data cFinalQuery as String ReadOnly
Data aFilC1H     as String 
Data oStatement  as Object ReadOnly 
Data oJObjTSI    as Object 

Method New() Constructor
Method PrepQuery()
Method LoadQuery() 
Method JSon()
Method FilC1H() 

Method GetQry()
Method GetJsn()

EndClass

/*/{Protheus.doc} New
	(M�todo contrutor da classe TSIPARTIC )
    Fluxo New:
    1� Monta-se a query com LoadQuery()
    2� Instaciar o preparedStatement com PrepQuery() e alimenta a propriedade
    cFinalQuery com a query final j� com os par�metros
	@type Class
	@author Henrique Pereira
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/

Method New(cSourceBr) Class TSIPARTIC 
    self:FilC1H(cSourceBr)
    Self:LoadQuery()  
    Self:PrepQuery()   
    Self:JSon()  
Return Nil

/*/{Protheus.doc} PrepQuery
	(M�todo respons�vel por Instaciar o preparedStatement com PrepQuery() e alimenta a propriedade
    cFinalQuery com a query final j� com os par�metros )
    @type Class
	@author Henrique Pereira    
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/ 

Method PrepQuery() Class TSIPARTIC
    self:oStatement := FWPreparedStatement():New()  
    self:oStatement:SetQuery(self:cQRY) 
   
    // Clientes SA1
    self:oStatement:setIn(1,self:aFilC1H) //C1h   
    self:oStatement:SetString(2,xfilial('SA1'))
     
    //union Fornecedores SA2 
    self:oStatement:setIn(3,self:aFilC1H) //C1h  
    self:oStatement:SetString(4,xfilial('SA2'))
     
     //union Transportadora SA4 
    self:oStatement:setIn(5,self:aFilC1H) //C1h 
    self:oStatement:SetString(6,xfilial('SA4'))

    self:cFinalQuery := self:oStatement:GetFixQuery()

Return Nil

 /*/{Protheus.doc} PrepQuery
	(M�todo respons�vel por montar a query para o preparedstatemen, por hora ainda com '?'
    nos par�metros vari�veis
	@author Henrique Pereira
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/

Method LoadQuery() Class TSIPARTIC
Local cDbType   := Upper(Alltrim(TCGetDB()))
Local cConcat   := ""
Local cQuery    := 'SELECT DISTINCT '

// VerIfica o tipo de concatena��o para o banco
If "MSSQL" $ cDbType
	cConcat := "+"
Else
	cConcat := "||"
EndIf

cQuery += " 'C' " + cConcat + " SA1.A1_COD " + cConcat + " SA1.A1_LOJA COD_PART, "
cQuery += " LTRIM(SA1.A1_NOME) NOME," 
cQuery += " SA1.A1_CODPAIS COD_PAIS,"
cQuery += " SA1.A1_CGC CGC,"
cQuery += " SA1.A1_INSCR IE,"
cQuery += " CASE WHEN SA1.A1_EST='EX' THEN '99999' ELSE SA1.A1_COD_MUN END COD_MUN, "
cQuery += " LTRIM(SA1.A1_SUFRAMA)  SUFRAMA,"
cQuery += " LTRIM(SA1.A1_END) " + cConcat + " SA1.A1_EST ENDERECO,"
cQuery += " LTRIM(SA1.A1_COMPLEM) COMPL,"
cQuery += " LTRIM(SA1.A1_BAIRRO) BAIRRO,"
cQuery += " SA1.A1_EST UF,"
cQuery += " LTRIM(SA1.A1_CEP) CEP,"
cQuery += " LTRIM(SA1.A1_DDD) DDD,"
cQuery += " LTRIM(SA1.A1_TEL) FONE,"		
cQuery += " CASE WHEN SA1.A1_FAX <> ' ' THEN SA1.A1_DDD ELSE ''END DDD2,"
cQuery += " LTRIM(SA1.A1_FAX) FAX,"
cQuery += " LTRIM(SA1.A1_EMAIL) EMAIL,"
cQuery += " LTRIM(SA1.A1_DTCAD) INCLUSAO,"
cQuery += " CASE WHEN SA1.A1_TIPO <> 'X' AND SA1.A1_PESSOA = 'F' THEN '1' WHEN SA1.A1_TIPO <> 'X' AND SA1.A1_PESSOA = 'J' THEN '2' WHEN SA1.A1_TIPO = 'X' THEN '2' ELSE '' END TP_PESSOA, "
cQuery += " '' RAMO_ATV, "
cQuery += " '0' INDCPRB, "
cQuery += " AI0.AI0_INDPAA EXECPAA,"
cQuery += " '' IND_ASSOC_DESPORT,"
cQuery += " SA1.A1_CONTRIB CONTRIBUINTE,"
cQuery += " '' ISENCAO_IMUNIDADE,"
cQuery += " '' ESTADO_EXT,"
cQuery += " '' TELEFONE_EXT,"
cQuery += " '' ESTADO_EXT,"
cQuery += " '' NIF," 
cQuery += " '' FORMA_TRIBUTACAO,"

If cDbType $ "MSSQL/MSSQL7"
    cQuery += " convert(varchar(23), SA1.S_T_A_M_P_ , 21 ) STAMP, "
Elseif cDbType $ "ORACLE"
    cQuery += "  cast( to_char(SA1.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) STAMP, "
Elseif cDbType $ "POSTGRES"
    cQuery += "  cast( to_char(SA1.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) STAMP, "
Endif

cQuery += " 'SA1' TABELA,"
cQuery += "  SA1.R_E_C_N_O_ RECNO "
cQuery += "  FROM "+RetSqlName("SA1")+ " SA1 "
cQuery += "  LEFT JOIN "+RetSqlName("C1H")+ " C1H "
cQuery += "  ON  C1H.C1H_FILIAL IN ( ? )"
cQuery += "  AND C1H.C1H_CODPAR = 'C' " + cConcat + " SA1.A1_COD " + cConcat + " SA1.A1_LOJA "
cQuery += "  LEFT JOIN "+RetSqlName("AI0")+ " AI0 ON 
cQuery += "  AI0.AI0_FILIAL = SA1.A1_FILIAL AND "
cQuery += "  AI0.AI0_CODCLI = SA1.A1_COD AND "
cQuery += "  AI0.AI0_LOJA = SA1.A1_LOJA  AND "
cQuery += "  AI0.D_E_L_E_T_ = ' ' "
cQuery += "  WHERE SA1.A1_FILIAL = ? " 
cQuery += "  AND SA1.S_T_A_M_P_ IS NOT NULL " 
If cDbType $ "MSSQL/MSSQL7"
    cQuery += " AND ( (convert(varchar(23), SA1.S_T_A_M_P_, 21) > C1H.C1H_STAMP) OR C1H.C1H_STAMP IS NULL ) "
Elseif cDbType $ "ORACLE"
    cQuery += " AND ( (cast(to_char(SA1.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23)) > C1H.C1H_STAMP) OR C1H.C1H_STAMP IS NULL ) "
Elseif cDbType $ "POSTGRES"
    cQuery += " AND ( (cast(to_char(SA1.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23)) > C1H.C1H_STAMP) OR C1H.C1H_STAMP IS NULL ) "
Endif
cQuery += "  AND SA1.D_E_L_E_T_ = ' ' "

cQuery += " UNION "

cQuery += " SELECT DISTINCT "
cQuery += " 'F' " + cConcat + " SA2.A2_COD " + cConcat + " SA2.A2_LOJA COD_PART, "
cQuery += " LTRIM(SA2.A2_NOME) NOME, "
cQuery += " SA2.A2_CODPAIS COD_PAIS, "
cQuery += " SA2.A2_CGC CGC, "
cQuery += " SA2.A2_INSCR IE, " 
cQuery += " CASE WHEN SA2.A2_EST='EX' THEN '99999' ELSE SA2.A2_COD_MUN END COD_MUN, "
cQuery += " '' SUFRAMA, "
cQuery += " LTRIM(SA2.A2_END) " + cConcat + " SA2.A2_EST ENDERECO, " 
cQuery += " LTRIM(SA2.A2_COMPLEM) COMPL, "
cQuery += " LTRIM(SA2.A2_BAIRRO) BAIRRO, "
cQuery += " SA2.A2_EST UF, "
cQuery += " LTRIM(SA2.A2_CEP) CEP, "
cQuery += " LTRIM(SA2.A2_DDD) DDD, "
cQuery += " LTRIM(SA2.A2_TEL) FONE,	"	
cQuery += " CASE WHEN SA2.A2_FAX <> ' 'THEN SA2.A2_DDD ELSE ''END DDD2, "
cQuery += " LTRIM(SA2.A2_FAX) FAX, "
cQuery += " LTRIM(SA2.A2_EMAIL) EMAIL, "
cQuery += " '' INCLUSAO, "
cQuery += " CASE WHEN SA2.A2_TIPO <> 'X' AND SA2.A2_TIPO = 'F' THEN '1' WHEN SA2.A2_TIPO <> 'X' AND SA2.A2_TIPO = 'J' THEN '2' WHEN SA2.A2_TIPO = 'X' THEN '2' ELSE '' END TP_PESSOA, "
cQuery += " CASE WHEN SA2.A2_TIPORUR <> ' ' THEN '4' ELSE '' END RAMO_ATV, "
cQuery += " CASE WHEN SA2.A2_CPRB = '2' THEN '0' ELSE SA2.A2_CPRB END INDCPRB, "
cQuery += " '' EXECPAA, "
cQuery += " SA2.A2_DESPORT IND_ASSOC_DESPORT, "
cQuery += " SA2.A2_CONTRIB CONTRIBUINTE, "
cQuery += " '' ISENCAO_IMUNIDADE, "
cQuery += " LTRIM(SA2.A2_ESTEX) ESTADO_EXT, "
cQuery += " LTRIM(SA2.A2_TELRE) TELEFONE_EXT, "
cQuery += " CASE WHEN SA2.A2_NIFEX <> ' ' THEN '1' WHEN SA2.A2_NIFEX = ' ' AND SA2.A2_MOTNIF = '1' THEN '2' WHEN SA2.A2_NIFEX = ' ' AND SA2.A2_MOTNIF = '2' THEN '3' ELSE SA2.A2_MOTNIF END ESTADO_EXT, "
cQuery += " LTRIM(SA2.A2_NIFEX) NIF, "
cQuery += " LTRIM(SA2.A2_TRBEX) FORMA_TRIBUTACAO, "

If cDbType $ "MSSQL/MSSQL7"
    cQuery += " convert(varchar(23), SA2.S_T_A_M_P_ , 21 ) STAMP, "
Elseif cDbType $ "ORACLE"
    cQuery += "  cast( to_char(SA2.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) STAMP, "
Elseif cDbType $ "POSTGRES"
    cQuery += "  cast( to_char(SA2.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) STAMP, "
Endif

cQuery += " 'SA2' TABELA, "
cQuery += " SA2.R_E_C_N_O_ RECNO " 
cQuery += " FROM "+RetSqlName("SA2")+ " SA2 "
cQuery += " LEFT JOIN "+RetSqlName("C1H")+ " C1H "
cQuery += " ON  C1H.C1H_FILIAL IN ( ? )"
cQuery += " AND C1H.C1H_CODPAR = 'F' " + cConcat + " SA2.A2_COD " + cConcat + " SA2.A2_LOJA "
cQuery += " AND C1H.D_E_L_E_T_ = ' ' "
cQuery += " WHERE SA2.A2_FILIAL = ? "
If cDbType $ "MSSQL/MSSQL7"
    cQuery += " AND ( (convert(varchar(23), SA2.S_T_A_M_P_, 21) > C1H.C1H_STAMP) OR C1H.C1H_STAMP IS NULL ) "
Elseif cDbType $ "ORACLE"
    cQuery += " AND ( (cast(to_char(SA2.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23)) > C1H.C1H_STAMP) OR C1H.C1H_STAMP IS NULL ) "
Elseif cDbType $ "POSTGRES"
    cQuery += " AND ( (cast(to_char(SA2.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23)) > C1H.C1H_STAMP) OR C1H.C1H_STAMP IS NULL ) "
Endif
cQuery += " AND SA2.S_T_A_M_P_ IS NOT NULL "
cQuery += " AND SA2.D_E_L_E_T_ = ' ' "

cQuery += " UNION "

cQuery += " SELECT DISTINCT "
cQuery += " 'T' " + cConcat + " SA4.A4_COD COD_PART, "
cQuery += " LTRIM(SA4.A4_NOME) NOME, "
cQuery += " SA4.A4_CODPAIS COD_PAIS, "
cQuery += " SA4.A4_CGC CGC, "
cQuery += " LTRIM(SA4.A4_INSEST) IE, "//-- SPEDConType(SPEDVldIE(SA2->A1_INSCR))
cQuery += " CASE WHEN SA4.A4_EST='EX' THEN '99999' ELSE SA4.A4_COD_MUN END COD_MUN, "
cQuery += " LTRIM(SA4.A4_SUFRAMA) SUFRAMA, "
cQuery += " LTRIM(SA4.A4_END) " + cConcat + " SA4.A4_EST ENDERECO, "// --FisGetEnd(SA2->A1_End,SA2->A1_EST) campos 11-End e 12-NUM
cQuery += " LTRIM(SA4.A4_COMPLEM) COMPL, "
cQuery += " LTRIM(SA4.A4_BAIRRO) BAIRRO, "
cQuery += " LTRIM(SA4.A4_EST) UF, "
cQuery += " LTRIM(SA4.A4_CEP) CEP, " 
cQuery += " LTRIM(SA4.A4_DDD) DDD2, "
cQuery += " LTRIM(SA4.A4_TEL) FONE, "	 	
cQuery += " '' DDD2, "
cQuery += " '' FAX, "
cQuery += " LTRIM(SA4.A4_EMAIL) EMAIL, "
cQuery += " '' INCLUSAO, "
cQuery += " LTRIM(SA4.A4_CGC)  TP_PESSOA, "
cQuery += " '' RAMO_ATV, "
cQuery += " '0' INDCPRB, " //--IIf(!Empty(SA2->A2_CPRB) .And. SA2->A2_CPRB <> '2','1','0')
cQuery += " '' EXECPAA, " //-- iif(AI0->AI0_INDPAA == '1', 1, 0)
cQuery += " '' IND_ASSOC_DESPORT, "
cQuery += " '' CONTRIBUINTE, "
cQuery += " '' ISENCAO_IMUNIDADE, "
cQuery += " '' ESTADO_EXT, " 
cQuery += " '' TELEFONE_EXT, "
cQuery += " '' ESTADO_EXT, " 
cQuery += " '' NIF, "
cQuery += " '' FORMA_TRIBUTACAO, "

If cDbType $ "MSSQL/MSSQL7"
    cQuery += " convert(varchar(23), SA4.S_T_A_M_P_ , 21 ) STAMP, "
Elseif cDbType $ "ORACLE"
    cQuery += "  cast( to_char(SA4.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) STAMP, "
Elseif cDbType $ "POSTGRES"
    cQuery += "  cast( to_char(SA4.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) STAMP, "
Endif

cQuery += " 'SA4' TABELA, "
cQuery += " SA4.R_E_C_N_O_ RECNO "		
cQuery += " FROM "+RetSqlName("SA4")+ " SA4 "
cQuery += " LEFT JOIN "+RetSqlName("C1H")+ " C1H "
cQuery += " ON  C1H.C1H_FILIAL IN ( ? )"
cQuery += " AND C1H.C1H_CODPAR = 'T' " + cConcat + " SA4.A4_COD "
cQuery += " AND C1H.D_E_L_E_T_ = ' ' " 
cQuery += " WHERE SA4.A4_FILIAL = ? " 

If cDbType $ "MSSQL/MSSQL7"
    cQuery += " AND ( (convert(varchar(23), SA4.S_T_A_M_P_, 21) > C1H.C1H_STAMP) OR C1H.C1H_STAMP IS NULL ) "
Elseif cDbType $ "ORACLE"
    cQuery += " AND ( (cast(to_char(SA4.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23)) > C1H.C1H_STAMP) OR C1H.C1H_STAMP IS NULL ) "
Elseif cDbType $ "POSTGRES"
    cQuery += " AND ( (cast(to_char(SA4.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23)) > C1H.C1H_STAMP) OR C1H.C1H_STAMP IS NULL ) "
Endif

cQuery += " AND SA4.S_T_A_M_P_ IS NOT NULL "
cQuery += " AND SA4.D_E_L_E_T_ = ' ' "

self:cQRY := cQuery
cQuery := ''

Return

 /*/{Protheus.doc} PrepQuery
	(M�todo respons�vel por retornar a propriedade self:cFinalQuery
	@author Henrique Pereira
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/

Method GetQry() Class TSIPARTIC 
return self:cFinalQuery

 /*/{Protheus.doc} JSon(
	(M�todo respons�vel montar o objeto Json e alimenta a propriedade self:oJObjTSI
	@author Henrique Pereira
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/

Method JSon() Class TSIPARTIC
Local cAlias    := getNextAlias()
Local nCountAux := 0
Local nLen      := 0
Local oJObjRet  := nil

oJObjRet := JsonObject():New()

DbSelectArea('C1H')
C1H->(DbSetOrder(1))
dbUseArea(.T., "TOPCONN", TCGenQry(, , self:GetQry()), cAlias, .F., .T.)
(cAlias)->(DbGoTop())

oJObjRet['participants'] := {}

While  (cAlias)->(!EOF())
    if nCountAux < 50
        aAdd( oJObjRet['participants'],JsonObject():New())
        nLen := Len(oJObjRet['participants'])

        oJObjRet['participants'][nLen]["code"]                 := alltrim((cAlias)->COD_PART)  //01
        oJObjRet['participants'][nLen]["name"]                 := alltrim((cAlias)->NOME)      //02
        oJObjRet['participants'][nLen]["countryCode"]          := alltrim((cAlias)->COD_PAIS)  //03
        if len(alltrim((cAlias)->CGC))>11
            oJObjRet['participants'][nLen]["registrationCNPJ"] := alltrim((cAlias)->CGC)       //04
        else
            oJObjRet['participants'][nLen]["registrationCPF"]  := alltrim((cAlias)->CGC)       //05
        endif
        oJObjRet['participants'][nLen]["stateRegistration"]    := alltrim((cAlias)->IE)        //06
        oJObjRet['participants'][nLen]["codeCity"]             := alltrim((cAlias)->COD_MUN)   //07
        oJObjRet['participants'][nLen]["SUFRAMA"]              := alltrim((cAlias)->SUFRAMA)   //08

        //oJObjRet['participants'][nLen]["TypeStreet"]         := alltrim((cAlias)->TPLOGR     //09
        aFisGetEnd := FisGetEnd((cAlias)->ENDERECO)                                            //10
        oJObjRet['participants'][nLen]["adress"]               := alltrim(aFisGetEnd[1])       //11
        oJObjRet['participants'][nLen]["numberAdress"]         := alltrim( IIf(!Empty(aFisGetEnd[2]),aFisGetEnd[3],"SN") ) //12
        oJObjRet['participants'][nLen]["complement" ]          := alltrim((cAlias)->COMPL)     //13
        //oJObjRet['participants'][nLen]["TypeNeighborhood"]   := alltrim((cAlias)->TPBAIR)    //14
        oJObjRet['participants'][nLen]["neighborhood"]         := alltrim((cAlias)->BAIRRO)    //15
        oJObjRet['participants'][nLen]["unitFederative"]       := alltrim((cAlias)->UF)        //16
        oJObjRet['participants'][nLen]["CEP"]                  := alltrim((cAlias)->CEP)       //17

        oJObjRet['participants'][nLen]["DDD"]                  := alltrim((cAlias)->DDD )      //18
        oJObjRet['participants'][nLen]["phoneNumber"]          := alltrim((cAlias)->FONE)      //19
        oJObjRet['participants'][nLen]["faxDDD"]               := alltrim((cAlias)->DDD2)      //20
        oJObjRet['participants'][nLen]["fax"]                  := alltrim((cAlias)->FAX)       //21
        oJObjRet['participants'][nLen]["email"]                := alltrim((cAlias)->EMAIL)     //22

        oJObjRet['participants'][nLen]["inclusionDate"]        := (cAlias)->INCLUSAO           //23
        oJObjRet['participants'][nLen]["kindOfPerson"]         := alltrim((cAlias)->TP_PESSOA) //24
        oJObjRet['participants'][nLen]["activity"]             := alltrim((cAlias)->RAMO_ATV)  //25
        oJObjRet['participants'][nLen]["CPRB"]                 := (cAlias)->INDCPRB            //41
        oJObjRet['participants'][nLen]["PAA"]                  := (cAlias)->EXECPAA            //43
        oJObjRet['participants'][nLen]["sportsAssociationIndicator"] := (cAlias)->IND_ASSOC_DESPORT //44
        oJObjRet['participants'][nLen]["CTISSCode"]            := (cAlias)->CONTRIBUINTE       //45
        oJObjRet['participants'][nLen]["stamp"]                := (cAlias)->STAMP
        (cAlias)->(DBSKIP())
        nCountAux++
    else
        exit
    endif
EndDo

self:oJObjTSI   := oJObjRet
FWFreeObj(oJObjRet)
C1H->(DbCloseArea())
cAlias := ''

Return

 /*/{Protheus.doc} GetJsn 
	(M�todo respons�vel retornar a propriedade self:oJObjTSI
	@author Henrique Pereira      
	@since 08/06/2020 
	@return Nil, nulo, n�o tem retorno.     
/*/
Method GetJsn() Class TSIPARTIC     
Return self:oJObjTSI
    
 /*/{Protheus.doc} FilC1H
	(M�todo respons�vel por montar o conte�do da filial da C1H
	@author Henrique Pereira
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/
Method FilC1H(cSourceBr) Class TSIPARTIC        
    self:aFilC1H := TafTSIFil(cSourceBr, 'C1H')       
Return