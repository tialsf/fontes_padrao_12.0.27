#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "RESTFUL.CH"

/*/{Protheus.doc} TSIITEM
	(Classe que cont�m preparedstatament do T005 )
    @type Class
	@author Henrique Pereira 
    @author Carlos Eduardo
	@since 10/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/ 
 
Class TSIITEM

    Data TSITQRY     as String ReadOnly
    Data cFinalQuery as String ReadOnly
    Data oStatement  as Object ReadOnly
    Data aFilC1L     as Array ReadOnly
    Data oJObjTSI    as Object

    Method New() Constructor
    Method PrepQuery()
    Method LoadQuery()
    Method JSon()
    Method FilC1L()

    Method GetQry()
    Method GetJsn()

EndClass

/*/{Protheus.doc} New
	(M�todo contrutor da classe TSIITEM )
    Fluxo New:
    1� Monta-se a query com LoadQuery()
    2� Instanciar o preparedStatement com PrepQuery() e alimenta a propriedade
    cFinalQuery com a query final j� com os par�metros
	@type Class
	@author Henrique Pereira 
    @author Carlos Eduardo
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/
 
Method New(cSourceBr) Class TSIITEM
    Self:FilC1L(cSourceBr)
    Self:LoadQuery()
    Self:PrepQuery()
    Self:JSon()
Return Nil

/*/{Protheus.doc} PrepQuery
	(M�todo respons�vel por Instanciar o preparedStatement com PrepQuery() e alimenta a propriedade
    cFinalQuery com a query final j� com os par�metros )
    @type Class
	@author Henrique Pereira
    @author Carlos Eduardo
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno. 
/*/

Method PrepQuery() Class TSIITEM

    self:oStatement := FWPreparedStatement():New()
    self:oStatement:SetQuery(self:TSITQRY)

    self:oStatement:SetString(1,xFilial('SB5')) 
    self:oStatement:SetIn(2,self:aFilC1L)
    self:oStatement:SetString(3,xFilial('CDN'))
    self:oStatement:SetString(4,xFilial('CDN'))

    self:cFinalQuery := self:oStatement:GetFixQuery()

Return Nil

 /*/{Protheus.doc} LoadQuery
	(M�todo respons�vel por montar a query para o preparedstatemen, por hora ainda com '?'
    nos par�metros vari�veis
	@author Henrique Pereira 
    @author Carlos Eduardo
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/

Method LoadQuery() Class TSIITEM

Local cQuery := ''
Local aBlocoK   := {}
Local cB1Sped   := GetNewPar('MV_DTINCB1','')
Local nIcmPad   := GetNewPar('MV_ICMPAD','')
Local cDbType   := Upper(Alltrim(TCGetDB()))

//Guardo todos os parametros referente ao bloco K no array.
aadd(aBlocok,GetNewPar('MV_BLKTP00','')) // Mercadoria para revenda
aadd(aBlocok,GetNewPar('MV_BLKTP01','')) // Materia Prima
aadd(aBlocok,GetNewPar('MV_BLKTP02','')) // Embalagem
aadd(aBlocok,GetNewPar('MV_BLKTP03','')) // Produto em processo
aadd(aBlocok,GetNewPar('MV_BLKTP04','')) // Produto acabado
aadd(aBlocok,GetNewPar('MV_BLKTP06','')) // Produto Intermediario
aadd(aBlocok,GetNewPar('MV_BLKTP10','')) // OUtros Insumos

if !empty(cB1Sped) 
	cB1Sped := StrTran(cB1Sped, '"','')
	cB1Sped := StrTran(cB1Sped, "'",'')
    cB1Sped := 'B1.'+alltrim(cB1Sped)
else
    cB1Sped := dtos(dDataBase)
endif

//Gerando registro T007
cQuery += " 	SELECT "

If cDbType $ "MSSQL/MSSQL7"
    cQuery += " convert(varchar(23), B1.S_T_A_M_P_ , 21 ) B1_STAMP, "
    cQuery += " convert(varchar(23), B5.S_T_A_M_P_ , 21 ) B5_STAMP, "
    cQuery += " convert(varchar(23), F2Q.S_T_A_M_P_ , 21 ) F2Q_STAMP, "   
    cQuery += " convert(varchar(23), CDNPROD.S_T_A_M_P_ , 21 ) CDNPROD_STAMP, "
    cQuery += " convert(varchar(23), CDNISS.S_T_A_M_P_  , 21 ) CDNISS_STAMP, "
Elseif cDbType $ "ORACLE"
    cQuery += " cast( to_char(B1.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) B1_STAMP, "
    cQuery += " cast( to_char(B5.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) B5_STAMP, "
    cQuery += " cast( to_char(F2Q.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) F2Q_STAMP, "
    cQuery += " cast( to_char(CDNPROD.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) CDNPROD_STAMP, "
    cQuery += " cast( to_char(CDNISS.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) CDNISS_STAMP, "
Elseif cDbType $ "POSTGRES"
    cQuery += "  cast( to_char(B1.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) B1_STAMP, "
    cQuery += "  cast( to_char(B5.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) B5_STAMP, "
    cQuery += "  cast( to_char(F2Q.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) F2Q_STAMP, "
    cQuery += "  cast( to_char(CDNPROD.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) CDNPROD_STAMP, "
    cQuery += "  cast( to_char(CDNISS.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) CDNISS_STAMP, "
Endif

cQuery += " 		'T007' REGISTRO, " // 01 - Registro
cQuery += " 		B1.B1_COD COD_ITEM, " // 02 - COD_ITEM
cQuery += " 		B1.B1_DESC  DESCR_ITEM, " // 03 - DESCR_ITEM
cQuery += " 		B1.B1_CODBAR COD_BARRA, " // 04 - COD_BARRA
cQuery += " 		B1.B1_UM UNID_INV, " // 05 - UNID_INV
cQuery += " 		case " 
cQuery += " 			when B1.B1_CODISS != '' then '09' "
cQuery += " 			when B1.B1_TIPO = 'AI' then '08' "
cQuery += " 			when B1.B1_TIPO = 'EM' OR B1.B1_TIPO = '"+aBlocok[03]+"' then '02' "
cQuery += " 			when B1.B1_TIPO = 'MC' then '07' "
cQuery += " 			when B1.B1_TIPO = 'ME' OR B1.B1_TIPO = '"+aBlocok[01]+"' then '00' "
cQuery += " 			when B1.B1_TIPO = 'MP' OR B1.B1_TIPO = '"+aBlocok[02]+"' then '01' "
cQuery += " 			when B1.B1_TIPO = 'OI' OR B1.B1_TIPO = '"+aBlocok[07]+"' then '10' "
cQuery += " 			when B1.B1_TIPO = 'PA' OR B1.B1_TIPO = '"+aBlocok[05]+"' then '04' "
cQuery += " 			when B1.B1_TIPO = 'PI' OR B1.B1_TIPO = '"+aBlocok[06]+"' then '06' "
cQuery += " 			when B1.B1_TIPO = 'PP' OR B1.B1_TIPO = '"+aBlocok[04]+"' then '03' "
cQuery += " 			when B1.B1_TIPO = 'SP' OR B1.B1_TIPO = '"+aBlocok[05]+"' then '05' "
cQuery += " 		else '99' end TIPO_ITEM, " //Verificar se o conte�do dever� vir do sped // 06 - TIPO_ITEM
cQuery += " 		B1.B1_POSIPI COD_NCM, " // 07 - COD_NCM
cQuery += " 		B1.B1_EX_NCM EX_IPI, " // 08 - EX_IPI

cQuery += " 		case when B1.B1_CODISS = '' then "
if cDbType $ "ORACLE/POSTGRES"
    cQuery += " SUBSTR(B1.B1_POSIPI,1,2) "
else //cDbType $ "MSSQL/MSSQL7"
    cQuery += " SUBSTRING(B1.B1_POSIPI,1,2) "
EndIf
cQuery += " else '00' end COD_GEN, " // 09 - COD_GEN

cQuery += " 		case
cQuery += " 			when CDNPROD.CDN_CODLST IS NOT NULL then CDNPROD.CDN_CODLST
cQuery += " 			when CDNISS.CDN_CODLST IS NOT NULL then CDNISS.CDN_CODLST
cQuery += " 		else '' end COD_LST, "														//-- 10 - COD_LST -> Na posi��o 10 vai o c�digo de servi�o federal.
cQuery += " 		B5.B5_TABINC COD_TAB, "																//-- 12 - COD_TAB
cQuery += " 		B5.B5_CODGRU COD_GRU, "																//-- 13 - COD_GRU
cQuery += " 		B1.B1_ORIGEM ORIGEM, "																//-- 14 - ORIGEM
cQuery +=           cB1Sped + " DT_INCLUSAO, "                                                 //-- 15 - DT_INCLUSAO
cQuery += "         case when B1.B1_PICM > 0 then B1.B1_PICM else " + str(nIcmPad,5,2) + " end ALIQ_ICMS,  " //-- 16 - ALIQ_ICMS
cQuery += " 		B1.B1_IPI ALIQ_IPI, "                                                               //-- 18 - ALIQ_IPI -- Val2Str((cAliasQry)->B1_IPI,5,2))
cQuery += " 		B1.B1_CEST CEST, "															    //-- 28 - CEST
cQuery += " 		case "
// Caso existe a tabela e o campo, priorizo o tipo de servi�o da reinf que esta no cadastro do produto.
if TAFAlsInDic('F2Q') .and. TafColumnPos('F2Q_TPSERV')
    cQuery += " 		when F2Q.F2Q_TPSERV IS NOT NULL AND F2Q.F2Q_TPSERV != '' then F2Q.F2Q_TPSERV " 
endif
cQuery += " 			when CDNPROD.CDN_CODLST IS NOT NULL then CDNPROD.CDN_TPSERV "
cQuery += " 			when CDNISS.CDN_CODLST IS NOT NULL then CDNISS.CDN_TPSERV "
cQuery += " 		else '' end TIP_SERV, " //29 - TIP_SERV	 
cQuery += " 		B1.B1_TIPO  , B1.B1_CODISS, B1.B1_PICM  , B1.B1_SEGUM ,	B1.B1_CONV " //-- Campos auxiliares
cQuery += " 	FROM " + RetSqlName('SB1') + " B1 "
cQuery += " 		LEFT JOIN " + RetSqlName('SB5') + " B5 ON B5.B5_FILIAL = ? AND B5.B5_COD = B1.B1_COD AND B5.D_E_L_E_T_ = ' ' "
cQuery += " 		LEFT JOIN " + RetSqlName('C1L') + " C1L ON C1L.C1L_FILIAL IN(?) AND C1L.C1L_CODIGO = B1.B1_COD AND C1L.D_E_L_E_T_ = ' ' "
cQuery += " 		FULL OUTER JOIN " + RetSqlName('CDN') + " CDNPROD ON CDNPROD.CDN_FILIAL = ? AND CDNPROD.CDN_CODISS = B1.B1_CODISS AND CDNPROD.CDN_PROD = B1.B1_COD AND CDNPROD.D_E_L_E_T_ = ' ' "
cQuery += " 		FULL OUTER JOIN " + RetSqlName('CDN') + " CDNISS ON CDNISS.CDN_FILIAL = ? AND CDNISS.CDN_CODISS = B1.B1_CODISS AND CDNISS.CDN_PROD = '' AND CDNISS.D_E_L_E_T_ = ' ' "
if TAFAlsInDic('F2Q') .and. TafColumnPos('F2Q_TPSERV')
    cQuery += "         LEFT JOIN " + RetSqlName('F2Q') + " F2Q ON F2Q.F2Q_FILIAL = '" + xFilial('F2Q') + "' AND F2Q.F2Q_PRODUT = B1.B1_COD AND F2Q.D_E_L_E_T_ = ' ' "
endif
cQuery += " 	WHERE B1.D_E_L_E_T_ = ' ' "
cQuery += " 	    AND B1.B1_FILIAL = '" + xFilial('SB1') + "' "

If cDbType $ "MSSQL/MSSQL7"
    cQuery += " AND ((B1.S_T_A_M_P_  IS NOT NULL AND ( (convert(varchar(23), B1.S_T_A_M_P_ , 21 ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR "
    cQuery += "     (B5.S_T_A_M_P_  IS NOT NULL AND ( (convert(varchar(23), B5.S_T_A_M_P_ , 21 ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR "
    cQuery += "     (F2Q.S_T_A_M_P_ IS NOT NULL AND ( (convert(varchar(23), F2Q.S_T_A_M_P_ , 21 ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR "
    cQuery += "     (CDNISS.S_T_A_M_P_ IS NOT NULL AND ( (convert(varchar(23), CDNISS.S_T_A_M_P_ , 21 ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR "
    cQuery += "     (CDNPROD.S_T_A_M_P_ IS NOT NULL AND ( ( convert(varchar(23), CDNPROD.S_T_A_M_P_ , 21 ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) ) "
Elseif cDbType $ "ORACLE"
    cQuery += " AND ((B1.S_T_A_M_P_  IS NOT NULL AND ( ( cast(to_char(B1.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR "
    cQuery += "     (B5.S_T_A_M_P_  IS NOT NULL AND ( ( cast(to_char(B5.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR  "		    
    cQuery += "     (F2Q.S_T_A_M_P_ IS NOT NULL AND ( ( cast(to_char(F2Q.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR  "		    
    cQuery += "     (CDNISS.S_T_A_M_P_ IS NOT NULL AND ( ( cast(to_char(CDNISS.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR "
    cQuery += "     (CDNPROD.S_T_A_M_P_ IS NOT NULL AND ( ( cast(to_char(CDNPROD.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23) ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) ) "
Elseif cDbType $ "POSTGRES"
    cQuery += " AND ((B1.S_T_A_M_P_  IS NOT NULL AND ( ( cast(to_char(B1.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR "
    cQuery += "     (B5.S_T_A_M_P_  IS NOT NULL AND ( ( cast(to_char(B5.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR "
    cQuery += "     (F2Q.S_T_A_M_P_ IS NOT NULL AND ( ( cast(to_char(F2Q.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR "
    cQuery += "     (CDNISS.S_T_A_M_P_ IS NOT NULL AND ( ( cast(to_char(CDNISS.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) OR "
    cQuery += "     (CDNPROD.S_T_A_M_P_ IS NOT NULL AND ( ( cast(to_char(CDNPROD.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23) ) > C1L.C1L_STAMP) OR C1L.C1L_STAMP IS NULL )) ) "
Endif

self:TSITQRY := cQuery
cQuery := ''

Return

 /*/{Protheus.doc} PrepQuery
	(M�todo respons�vel por retornar a propriedade self:cFinalQuery
	@author Henrique Pereira
    @author Carlos Eduardo
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/

Method GetQry() Class TSIITEM
return self:cFinalQuery

 /*/{Protheus.doc} JSon
	(M�todo respons�vel montar o objeto Json e alimenta a propriedade self:oJObjTSI
	@author Henrique Pereira
    @author Carlos Eduardo 
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/

Method JSon() Class TSIITEM

Local nPos      := 0
Local oJObjRet  := JsonObject():New()
Local cAlias    := getNextAlias() 
 
 oJObjRet['item'] := {}
 dbUseArea(.T., "TOPCONN", TCGenQry(, , self:GetQry()), cAlias, .F., .T.)
while (cAlias)->(!eof())
    
    //Gravo o maior stamp entro todos que compoe o registro.aSort( { (cAlias)->
    cStamp := aSort( {(cAlias)->B1_STAMP, (cAlias)->B5_STAMP,(cAlias)->F2Q_STAMP, (cAlias)->CDNPROD_STAMP, (cAlias)->CDNISS_STAMP } ,,, { |x, y| x > y } )[1] 
    
    aAdd( oJObjRet['item'],JsonObject():New())  
    nPos := Len(oJObjRet['item']) 
    oJObjRet["item"][nPos]["itemId"       ] := (cAlias)->COD_ITEM
    oJObjRet["item"][nPos]["description"  ] := (cAlias)->DESCR_ITEM
    oJObjRet["item"][nPos]["barcode"      ] := (cAlias)->COD_BARRA
    oJObjRet["item"][nPos]["unit"         ] := (cAlias)->UNID_INV
    oJObjRet["item"][nPos]["itemType"     ] := alltrim((cAlias)->TIPO_ITEM)
    oJObjRet["item"][nPos]["idNcm"        ] := alltrim((cAlias)->COD_NCM) + iif(!empty((cAlias)->EX_IPI),(cAlias)->EX_IPI,'')
    oJObjRet["item"][nPos]["genderId"     ] := alltrim((cAlias)->COD_GEN)
    oJObjRet["item"][nPos]["serviceId"    ] := alltrim((cAlias)->COD_LST)
    oJObjRet["item"][nPos]["tableBeverage"] := (cAlias)->COD_TAB
    oJObjRet["item"][nPos]["groupBeverage"] := (cAlias)->COD_GRU
    oJObjRet["item"][nPos]["originId"     ] := (cAlias)->ORIGEM
    oJObjRet["item"][nPos]["icmsRate"     ] := (cAlias)->ALIQ_ICMS 
    oJObjRet["item"][nPos]["ipiRate"      ] := (cAlias)->ALIQ_IPI
    oJObjRet["item"][nPos]["inclusionDate"] := stod((cAlias)->DT_INCLUSAO)
    oJObjRet["item"][nPos]["cestId"       ] := (cAlias)->CEST
    oJObjRet["item"][nPos]["serviceTypeId"] := iif( !empty((cAlias)->TIP_SERV), '1' + StrZero(Val((cAlias)->TIP_SERV),08), '' )
    oJObjRet["item"][nPos]["stamp"        ] := cStamp
    
    (cAlias)->(DbSkip())
enddo

self:oJObjTSI := oJObjRet
FWFreeObj(oJObjRet)
cAlias := ''

Return

 /*/{Protheus.doc} GetJsn
	(M�todo respons�vel retornar a propriedade self:oJObjTSI
	@author Henrique Pereira
    @author Carlos Eduardo
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.  
/*/
Method GetJsn () Class TSIITEM

Return self:oJObjTSI

 /*/{Protheus.doc} FilC1L
	(M�todo respons�vel por montar o conte�do da filial da C1H
	@author Henrique Pereira
    @author Carlos Eduardo
	@since 08/06/2020
	@return Nil, nulo, n�o tem retorno.
/*/
Method FilC1L(cSourceBr) Class TSIITEM        
     self:aFilC1L := TafTSIFil(cSourceBr, 'C1L')      
Return