#include 'totvs.ch'
#include 'topconn.ch'
#include 'restful.ch'
#include 'fwmvcdef.ch'

/*/-------------------------------------------------------------
{Protheus.doc}TSIPROREF
(Classe principal para integra��o de processos referenciados)
@author Carlos Eduardo
@since 17/08/2020
@param param_name, param_type, param_descr 
-------------------------------------------------------------/*/
Class TSIPROREF

Data oJObjTSI    as Object

Method New() Constructor
Method LoadQuery()
Method JSon()
Method GetJsn()

EndClass

/*---------------------------------------------------------------------------------------------------------------------------
{Protheus.doc} New
(M�todo contrutor da classe TSIPROREF )
Fluxo New:
1� Monta-se a query com LoadQuery()
2� Instanciar o preparedStatement com PrepQuery() e alimenta a propriedade cFinalQuery com a query final j� com os par�metros
@author Carlos Eduardo
@since 17/08/2020
@return Nil, nulo, n�o tem retorno.
----------------------------------------------------------------------------------------------------------------------------*/
Method New(cSourceBr) Class TSIPROREF

Self:LoadQuery(cSourceBr)

Return Nil

 /*-----------------------------------------------------------------------------------
{Protheus.doc} LoadQuery
(M�todo respons�vel por montar a query para o preparedstatemen, por hora ainda com '?'
nos par�metros vari�veis, e atribui valores a tabela temporaria.
@author Carlos Eduardo
@since 17/08/2020
@return Nil, nulo, n�o tem retorno.
-------------------------------------------------------------------------------------*/

Method LoadQuery(cSourceBr) Class TSIPROREF

Local cDbType := Upper(Alltrim(TCGetDB()))
Local lRet    := .t.
Local cQuery  := ''
Local aFilTaf := TafTSIFil(cSourceBr,'CCF')
Local cFilTaf := ''
Local nLenFil := len(aFilTaf)
Local cAlias  := GetNextAlias()
Local nI      := 0
Local cConvCpo:= ''

//Monta a string com as filiais para somar � query
for nI := 1 to nLenFil
    cFilTaf += "'" + aFilTaf[nI] + "'"
    if nI < nLenFil; cFilTaf += ','; endif
next

//Converte o conte�do do campo conforme o banco de dados usado.
If cDbType $ 'MSSQL/MSSQL7'
    cConvCpo := " convert(varchar(23), CCF.S_T_A_M_P_, 21) "
Elseif cDbType $ 'ORACLE'
    cConvCpo := " cast(to_char(CCF.S_T_A_M_P_,'DD.MM.YYYY HH24:MI:SS.FF') AS VARCHAR2(23)) "
Elseif cDbType $ 'POSTGRES'
    cConvCpo := " cast(to_char(CCF.S_T_A_M_P_,'YYYY-MM-DD HH24:MI:SS.MS') AS VARCHAR(23)) "
Endif

//Monto a query que servir� como base para integra��o.
cQuery := " SELECT "
cQuery += " 	CCF.CCF_NUMERO , "   //-- NUM_PROC
cQuery += " 	CCF.CCF_TIPO,	 "   //-- IND_PROC
cQuery += " 	CCF.CCF_IDVARA,	 "   //-- ID_VARA
cQuery += " 	CCF.CCF_TPCOMP,	 "   //-- TP_PROC
cQuery += " 	CCF.CCF_INDAUT,	 "   //-- INDAUTORIA
cQuery += " 	CCF.CCF_UF,	     "   //-- UFVARA
cQuery += " 	CCF.CCF_CODMUN,	 "   //-- CODMUNIC
cQuery += " 	CCF.CCF_DTINI,	 "   //-- INIVALID
cQuery += " 	CCF.CCF_DTFIN,	 "   //-- FINVALID
cQuery += " 	CCF.CCF_INDSUS,	 "   //-- COD_SUSP
cQuery += " 	CCF.CCF_SUSEXI,	 "   //-- IND_SUSP
cQuery += "     case when CCF.CCF_DTADM = ' ' then CCF.CCF_DTADM else CCF.CCF_DTSENT end DTDECISAO, "   //-- DT_DECISAO
cQuery += "     case when CCF.CCF_MONINT = '1' then 'S' else 'N' end DEPOSITO "   //-- IND_DEPOSITO
cQuery += " FROM " + RetSqlName('CCF')  + ' CCF '
cQuery += " 	LEFT JOIN " + RetSqlName('C1G') + " C1G ON C1G.C1G_FILIAL IN (" + cFilTaf + ") AND C1G.C1G_NUMPRO = CCF.CCF_NUMERO AND C1G.D_E_L_E_T_ = ' ' "
cQuery += " WHERE CCF.D_E_L_E_T_ = ' ' "
cQuery += " 	AND CCF.CCF_FILIAL = '" + xFilial('CCF')+ "' "
cQuery += "	    AND ( 
cQuery +=  		  " ( CCF.S_T_A_M_P_ IS NOT NULL AND ( ( " + cConvCpo + " > C1G.C1G_STAMP ) OR C1G.C1G_STAMP IS NULL ) ) "
cQuery +=  		  " OR CCF.S_T_A_M_P_ IS NULL  AND C1G.C1G_STAMP IS NULL ) "
cQuery := ChangeQuery(cQuery) 
DbUseArea(.t., 'topconn', TCGenQry(, , cQuery), cAlias, .f., .t. )

//Crio o JSon
Self:JSon(cAlias)
(cAlias)->(DbCloseArea())

Return lRet

/*/----------------------------------------------------------------------------
{Protheus.doc}JSon
(M�todo respons�vel montar o objeto Json e alimenta a propriedade self:oJObjTSI)
@author Carlos Eduardo
@since 17/08/2020
@param cTmpTbl, string, Nome da tabela tempor�ria 
-----------------------------------------------------------------------------/*/
Method JSon(cTmpTbl) Class TSIPROREF
Local nPos  := 0
Local nPos2 := 0
Local cProcesso := ''
Local oJson := JsonObject():New()

oJson['referencedProcesses'] := {}

while (cTmpTbl)->(!eof())
    aAdd( oJson['referencedProcesses'],JsonObject():New())                                 
    nPos := Len(oJson['referencedProcesses']) 
    oJson['referencedProcesses'][nPos]['processNumber'         ] := (cTmpTbl)->CCF_NUMERO 
    oJson['referencedProcesses'][nPos]['processOriginIndicator'] := (cTmpTbl)->CCF_TIPO	
    oJson['referencedProcesses'][nPos]['courtIdentification'   ] := (cTmpTbl)->CCF_IDVARA	
    oJson['referencedProcesses'][nPos]['processType'           ] := (cTmpTbl)->CCF_TPCOMP	
    oJson['referencedProcesses'][nPos]['lawsuitAuthorship'     ] := (cTmpTbl)->CCF_INDAUT	
    oJson['referencedProcesses'][nPos]['state'                 ] := (cTmpTbl)->CCF_UF	    
    oJson['referencedProcesses'][nPos]['cityCode'              ] := (cTmpTbl)->CCF_CODMUN	
    oJson['referencedProcesses'][nPos]['proceduralStart'       ] := (cTmpTbl)->CCF_DTINI	
    oJson['referencedProcesses'][nPos]['proceduralEnd'         ] := (cTmpTbl)->CCF_DTFIN	
    oJson['referencedProcesses'][nPos]['suspension'            ] := {}

    cProcesso := (cTmpTbl)->CCF_NUMERO 
    
    while (cTmpTbl)->(!eof()) .and. cProcesso = (cTmpTbl)->CCF_NUMERO 
        aadd( oJson['referencedProcesses'][nPos]['suspension'],JsonObject():New() )
        nPos2 := Len(oJson['referencedProcesses'][nPos]['suspension']) 
        oJson['referencedProcesses'][nPos]['suspension'][nPos2]['suspensionIndicativeCode'] := (cTmpTbl)->CCF_INDSUS	
        oJson['referencedProcesses'][nPos]['suspension'][nPos2]['suspensionIndicativeInfo'] := (cTmpTbl)->CCF_SUSEXI
        oJson['referencedProcesses'][nPos]['suspension'][nPos2]['decisionDate'            ] := alltrim((cTmpTbl)->DTDECISAO)
        oJson['referencedProcesses'][nPos]['suspension'][nPos2]['fullAmount'              ] := alltrim((cTmpTbl)->DEPOSITO)
        (cTmpTbl)->(DbSkip())
    enddo   
enddo

self:oJObjTSI := oJson
FWFreeObj(oJson)

return 

/*-------------------------------------------------------
{Protheus.doc} GetJsn
(M�todo respons�vel retornar a propriedade self:oJObjTSI
@author Carlos Eduardo
@since 18/08/2020
@return Nil, nulo, n�o tem retorno.
--------------------------------------------------------*/
Method GetJsn() Class TSIPROREF
Return self:oJObjTSI
