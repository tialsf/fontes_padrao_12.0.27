#include "protheus.ch"
#include "fwmvcdef.ch"
#include "TAFA564.ch"

/*-------------------------------------------------------------------
{Protheus.doc} TAFA564()
(Rotina para visualiza��o do log de erro gerado pelo TSI)
@author Carlos Eduardo
@since 30/06/2020
@return Nil, nulo, n�o tem retorno.
//-----------------------------------------------------------------*/
Function TAFA564() 
Local oBrowse := FWMBrowse():New()
	
oBrowse:SetDescription(STR0001)
oBrowse:SetAlias('V5R')
oBrowse:SetMenuDef('TAFA564')
oBrowse:Activate()

Return

/*-------------------------------------------------------------------
{Protheus.doc} MenuDef()
@author Carlos Eduardo
@since 30/06/2020
@return Nil, nulo, n�o tem retorno.
//-----------------------------------------------------------------*/
Static Function MenuDef()
Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'VIEWDEF.TAFA564' OPERATION 2 ACCESS 0

Return aRotina

/*-------------------------------------------------------------------
{Protheus.doc} ModelDef()
@author Carlos Eduardo
@since 30/06/2020
@return Nil, nulo, n�o tem retorno.
//-----------------------------------------------------------------*/
Static Function ModelDef()
Local oStruV5R := FWFormStruct(1,'V5R')
Local oModel   := MPFormModel():New( "TAFA564")

oModel:AddFields('MODEL_V5R', /*cOwner*/, oStruV5R)  

Return oModel

/*-------------------------------------------------------------------
{Protheus.doc} ViewDef()
@author Carlos Eduardo
@since 30/06/2020 
@return Nil, nulo, n�o tem retorno.
//-----------------------------------------------------------------*/
Static Function ViewDef()
Local oModel        := FwLoadModel('TAFA564')
Local oStrV5R       := FWFormStruct(2, 'V5R')
Local oView         := FWFormView():New() 
Local bBlocoMagic   :=  nil

if TafColumnPos('V5R_URL')
    oStrV5R:RemoveField('V5R_URL')            
endif

oView:SetModel(oModel)
oView:AddField('VIEW_V5R' ,oStrV5R,'MODEL_V5R')  
oView:EnableTitleView( 'VIEW_V5R', STR0001 ) 
oView:CreateHorizontalBox( 'FIELDS_V5R', 100 ) 
oView:SetOwnerView( 'VIEW_V5R', 'FIELDS_V5R' )
if TafColumnPos('V5R_URL')
    bBlocoMagic :=  {|oModel| T565URL(oModel)}
    oView:addUserButton("Help URL", "MAGIC_BMP", bBlocoMagic, "Coment�rios do Bot�o", , , )
endif
	
Return oView

/*-------------------------------------------------------------------
{Protheus.doc} ViewDef()
( Cria um Hash com os codigos de erro padr�o )
@author Carlos Eduardo
@since 30/06/2020
@return Nil, nulo, n�o tem retorno.
//-----------------------------------------------------------------*/
Static Function ErrPad()
Local lRet := .t.


return lRet

/*-------------------------------------------------------------------
{Protheus.doc} ViewDef()
(Abre a pargina de documenta��o saldo no bot�o V5R_URL )
@author Henrique Pereira
@since 23/07/2020
@return Nil, nulo, n�o tem retorno.
//-----------------------------------------------------------------*/
static function T565URL()
Local cLink := fwfldget('V5R_URL')
 
if !empty(cLink)
    ShellExecute("open",cLink,"","",1)
else
    MsgInfo(STR0003)//"N�o h� documenta��o on-line para este problema"
endif
 
return .t.