// JOB

Main Function TST()  

    RPCSETTYPE(3) 
    RPCSETENV( 'T1','D MG 01' )   
  
    TAFA566()
 
REturn 

/*/{Protheus.doc} TAFA566
Fun��o respons�vel pela chamada da classe de extra��o dos produtos do ERP ( json )
e enviar os dados para serem persistidos pelo MVC de produtos do TAF ( TAFA057 )
@author Henrique Fabiano Pateno Pereira
@author Wesley Pinheiro
@since 01/07/2020
@version 1.0 
*/ 
function TAFA566( )

    Local oBj    := Nil
    Local oBjAux := Nil 

    oBj :=  TSICC( ):new( 'T1D MG 01' )
    oBjAux := oBj:GetJsn( )
    freeobj( oBj ) 
 
    TAFA565( HashCC( ), oBjAux:GetJsonObject(oBjAux:GetNames()[1]))

return 