#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA568()

Integra��o Cliente \ Fornecedor \ Transportadora 
para os participantes do TAF ( C1H )

@author Denis Souza
@since 08/07/2020
@version 1.0
@return

/*/ 
//-------------------------------------------------------------------
Function TAFA568()

//Trecho comentado para testar integra��o da SA1/SA2 (ERP) para C1H
//RPCSETTYPE(3)
//RPCSETENV( "10", "010101" )
//oObjPartic := TSIPARTIC():new(cEmpAnt+cFilAnt)
//cPropN1   := oObjPartic:GetJsn():GetNames()[1] //Retorna um array com todas as propriedades dispon�veis no primeiro n�vel do JsonObject.
//aJSPartic := oObjPartic:GetJsn():GetJsonObject(cPropN1)  //Retorna um objeto do tipo JsonObject com o conte�do da propriedade informada.
//freeobj( oObjPartic ) 
//TAFA565( HashTSIPARTIC( ), aJSPartic )
//Return .T.

Return HashTSIPARTIC( )

//-------------------------------------------------------------------
/*/{Protheus.doc} HashTSIPARTIC()

Fun��o respons�vel por montar o HashMap que ser� utilizado para:
1o Armazenar informa��es necess�rias para a grava��o do json em MVC
2o Realizar o De-Para entre propriedades json e campos do TAF
    Esse "De-Para" ser� utilizado para:
    - Persistir o json recebido no MVC ( permitindo assim validar a estrutura json em tempo de persit�ncia no MVC ) e 
    - Caso ocorra erro de commit no MVC, permitir� gravar tabela de log V5R com mensagem amigav�is, pois saberemos qual campo do TAF corresponde a propriedade json com erro.
3o Macro execu��o de pesquisa F3 de campos que precisam realizar "De-Para" para o Id do TAF

@author Denis Souza
@since 08/07/2020
@version 1.0
@return

/*/ 
//-------------------------------------------------------------------
Static Function HashTSIPARTIC( )

    Local oHash := HMNew( )

    // Infos gen�ricas
    HMSet( oHash, "m_o_d_e_l_"   , "MODEL_C1H"   )
    HMSet( oHash, "s_o_u_r_c_e_" , "TAFA053"     )
    HMSet( oHash, "a_r_e_a_"     , "C1H"         )
    HMSet( oHash, "o_r_d_e_r_"   , 1             )
    HMSet( oHash, "k_e_y_"       , "aObjJson[nI]['code']" )
    HMSet( oHash, "s_e_e_k_"     , "C1H->( DbSeek( xfilial( 'C1H') + cKey ) )" )
    HMSet( oHash, "t_a_g_i_d_"   , "code"        )

    // Propriedades e Campos
    SetHashKey( oHash, "code"                      , "C1H_CODPAR"      ) //02 COD_PART
    SetHashKey( oHash, "name"                      , "C1H_NOME"        ) //03 NOME
    SetHashKey( oHash, "countryCode"               , "C1H_CODPAI#F3#"  ) //04 COD_PAIS   XFUNCh2ID( xA, 'C08' , 1 , ,.T.)
    SetHashKey( oHash, "registrationCNPJ"          , "C1H_CNPJ"        ) //05 CNPJ
    SetHashKey( oHash, "registrationCPF"           , "C1H_CPF"         ) //06 CPF
    SetHashKey( oHash, "stateRegistration"         , "C1H_IE"          ) //07 IE
    SetHashKey( oHash, "codeCity"                  , "C1H_CODMUN#F3#"  ) //08 COD_MUN    XFUNSpcMIL( 'C07' , xA , xB )
    SetHashKey( oHash, "SUFRAMA"                   , "C1H_SUFRAM"      ) //09 SUFRAMA
    //SetHashKey( oHash, "TypeStreet"              , "C1H_TPLOGR#F3#"  ) //10 TP_LOGR    XFUNCh2ID( xA, 'C06', 1, ,.T.)
    SetHashKey( oHash, "adress"                    , "C1H_END"         ) //11 END
    SetHashKey( oHash, "numberAdress"              , "C1H_NUM"         ) //12 NUM
    SetHashKey( oHash, "complement"                , "C1H_COMPL"       ) //13 COMPL
    //SetHashKey( oHash, "TypeNeighborhood"        , "C1H_TPBAIR#F3#"  ) //14 TP_BAIRRO  XFUNCh2ID( xA, 'C86' , 2 , ,.T.)
    SetHashKey( oHash, "neighborhood"              , "C1H_BAIRRO"      ) //15 BAIRRO
    SetHashKey( oHash, "unitFederative"            , "C1H_UF#F3#"      ) //16 UF         XFUNVldCmp("C09",3).And.XFUNNWFunc('A053LogAlt')
    SetHashKey( oHash, "CEP"                       , "C1H_CEP"         ) //17 CEP
    SetHashKey( oHash, "DDD"                       , "C1H_DDD"         ) //18 DDD FONE
    SetHashKey( oHash, "phoneNumber"               , "C1H_FONE"        ) //19 FONE
    SetHashKey( oHash, "faxDDD"                    , "C1H_DDDFAX"      ) //20 DDD FAX
    SetHashKey( oHash, "fax"                       , "C1H_FAX"         ) //21 FAX
    SetHashKey( oHash, "email"                     , "C1H_EMAIL"       ) //22 EMAIL    
    SetHashKey( oHash, "inclusionDate"             , "C1H_DTINCL#DT#"  ) //23 DT_INCLUSAO
    SetHashKey( oHash, "kindOfPerson"              , "C1H_PPES"        ) //24 TP_PESSOA
    SetHashKey( oHash, "activity"                  , "C1H_RAMO"        ) //25 RAMO_ATV
    SetHashKey( oHash, "CPRB"                      , "C1H_CPRB"        ) //41 INDCPRB
    SetHashKey( oHash, "PAA"                       , "C1H_PAA"         ) //43 EXECPAA
    SetHashKey( oHash, "sportsAssociationIndicator", "C1H_INDDES"      ) //44 IND_ASSOC_DESPORT
    SetHashKey( oHash, "CTISSCode"                 , "C1H_CONTRI"      ) //45 CONTRIBUINTE
    SetHashKey( oHash, "stamp"                     , "C1H_STAMP"       )

    // #F3#
    HMSet( oHash, "#F3#C1H_CODPAI", "GetTafId( 'C08', cChave, 1 )" )
    HMSet( oHash, "#F3#C1H_CODMUN", "GetTafId2( 'C09', aObjJson[nI]['unitFederative'], 1, 'C07', cChave, 1 ) " )
    //HMSet( oHash, "C1H_TPLOGR"  , "GetTafId( 'C06', cChave, 1 )" )
    //HMSet( oHash, "C1H_TPBAIR"  , "GetTafId( 'C86', cChave, 1 )" )
    HMSet( oHash, "#F3#C1H_UF"    , "GetTafId( 'C09', cChave, 1 )" )

Return oHash

//-------------------------------------------------------------------
/*/{Protheus.doc} getParti()

Fun��o respons�vel por executar a consulta ao cadastro de Participante

@author Henrique Fabiano Pateno Pereira / Denis Souza
@since 01/07/2020
@version 1.0
@return

/*/ 
//-------------------------------------------------------------------
Function GetParti( cPartiDe, cPartiAte, nPage, nPageSize, cOrder )

    Local cQry   := GetPartQry(cPartiDe, cPartiAte, nPage, nPageSize, cOrder )
    Local cAlias := GetNextAlias()
    dbUseArea(.T., "TOPCONN", TCGenQry(, , cQry), cAlias, .F., .T.)

Return cAlias

//-------------------------------------------------------------------
/*/{Protheus.doc} GetPartQry()

Fun��o respons�vel por montar a consulta ao cadastro de Participante

@author Henrique Fabiano Pateno Pereira / Denis Souza
@since 01/07/2020
@version 1.0
@return

/*/ 
//-------------------------------------------------------------------
Static Function GetPartQry( cPartiDe, cPartiAte, nPage, nPageSize, cOrder )

Local cQry := ''

if nPage == 0
    nPage := 1
endif

if nPageSize == 0
    nPageSize := 10
endif

if Empty(cPartiDe)
    cPartiDe := ' '
endif

if Empty(cPartiAte)
    cPartiAte := 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'
endif

cQry += " SELECT "
cQry += "  C1H.C1H_CODPAR "
cQry += "  ,C1H.C1H_NOME "
cQry += "  ,C1H.C1H_CODPAI "
cQry += "  ,C1H.C1H_CNPJ "
cQry += "  ,C1H.C1H_CPF "
cQry += "  ,C1H.C1H_IE "
cQry += "  ,C1H.C1H_CODMUN "
cQry += "  ,C1H.C1H_SUFRAM "
cQry += "  ,C1H.C1H_TPLOGR "
cQry += "  ,C1H.C1H_END "
cQry += "  ,C1H.C1H_NUM "
cQry += "  ,C1H.C1H_COMPL "
cQry += "  ,C1H.C1H_TPBAIR "
cQry += "  ,C1H.C1H_BAIRRO "
cQry += "  ,C1H.C1H_UF "
cQry += "  ,C1H.C1H_CEP "
cQry += "  ,C1H.C1H_DDD "
cQry += "  ,C1H.C1H_FONE "
cQry += "  ,C1H.C1H_DDDFAX "
cQry += "  ,C1H.C1H_FAX "
cQry += "  ,C1H.C1H_EMAIL "
cQry += "  ,C1H.C1H_DTINCL "
cQry += "  ,C1H.C1H_PPES "
cQry += "  ,C1H.C1H_RAMO "
cQry += "  ,C1H.C1H_CPRB "
cQry += "  ,C1H.C1H_PAA "
cQry += "  ,C1H.C1H_INDDES "
cQry += "  ,C1H.C1H_CONTRI "
cQry += "  ,C1H.C1H_STAMP "
cQry  += " FROM " + RetSqlName("C1H") + " C1H "
cQry  += " WHERE C1H.C1H_FILIAL = '" + xFilial("C1H") + "' "
cQry  += " AND C1H.C1H_CODPAR BETWEEN '" + cPartiDe + "' AND '" + cPartiAte + "' "
cQry  += " AND C1H.D_E_L_E_T_ = ' ' "
cQry  += " ORDER BY " + cOrder //#todo dinamizar ordem com de para TAG x CAMPO

cQry  += " OFFSET ( " + cValToChar(nPage-1) + " * " + cValToChar(nPageSize) + " ) ROWS "
cQry  += " FETCH NEXT " + cValToChar(nPageSize+1) + " ROWS ONLY "

Return cQry

//-------------------------------------------------------------------
/*/{Protheus.doc} TSTC1H()

Execucao rotina por menu

@author Denis Souza
@since 01/07/2020
@version 1.0
@return

/*/ 
//-------------------------------------------------------------------
Main Function TSTC1H()
    RPCSETTYPE(3)
    RPCSETENV( "10", "010101" )
    TAFA568()
Return