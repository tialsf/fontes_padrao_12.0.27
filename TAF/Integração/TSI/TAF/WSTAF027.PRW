#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} TSIPARTICIPANTE
WS para persisitencia do cadastro de Participantes do TAF

@author Henrique Fabiano Pateno Pereira / Denis Souza
@since 08/07/2020
@version 1.0 
*/

WSRESTFUL TSIPARTICIPANTE DESCRIPTION "Api de persist�ncia ao Participante"

    WSDATA sourceBranch  As String
    WSDATA page			 As Integer	Optional
	WSDATA pageSize 	 As Integer	Optional
    WSDATA order         As String  Optional
    WSDATA codDe         As String  Optional
    WSDATA codAte        As String  Optional

	WSMETHOD GET    PARTICIPANTS DESCRIPTION "M�todo de consulta aos cadastros " PRODUCES APPLICATION_JSON 
    WSMETHOD POST   PARTICIPANTS DESCRIPTION "M�todo de inclus�o " PRODUCES APPLICATION_JSON 
    WSMETHOD PUT    PARTICIPANTS DESCRIPTION "M�todo de altera��o " PRODUCES APPLICATION_JSON
    WSMETHOD DELETE PARTICIPANTS DESCRIPTION "M�todo de dele��o " PRODUCES APPLICATION_JSON 

END WSRESTFUL

/*
Metodo respons�vel por persistir os dados de Participantes ao TAF
@author Henrique Fabiano Pateno Pereira / Denis Souza
@since 08/07/2020
@version 1.0
*/

WSMETHOD POST PARTICIPANTS WSREST TSIPARTICIPANTE

Local cJsonResp := ""
Local oBjJson   := JsonObject():New() // Requisi��o
Local oJsonResp := JsonObject():New() // retorno
Local nGetNames := 0
Local nError    := 0
Local aError    := {}
Local aNames    := {}

if VldExecute( self:sourceBranch, @oJsonResp )
    cBody := self:GetContent( )
    oBjJson:fromJSON( cBody )

    if len(oBjJson:GetNames())>0
        for nGetNames := 1 to len(oBjJson:GetNames())
            aNames := oBjJson:GetJsonObject( oBjJson:GetNames()[nGetNames] )
            if oBjJson:GetNames()[nGetNames] == 'participants'
                TAFA565( TAFA568( ), aNames, nil, @aError )
            endif
        next nGetNames

        if len(aError) == 0
            oJsonResp['success'] := .t.
        endif
    else
        oJsonResp['success'] := .f.
        oJsonResp['errorcode'] := '400'
        oJsonResp['message'] := EncodeUTF8( "Requisi��o mal-formada -  a requisi��o n�o est� de acordo com o formato esperado. Verifique o JSON (body) que est� sendo enviado" )
    endif

    for nError := 1 to len(aError)
        if nError == 1
            oJsonResp['success'] := .f.
            oJsonResp['error'] := {}
        endif
        aAdd( oJsonResp['error'],JsonObject():New())
        oJsonResp["error"][nError]["errorDescription"] := aError[nError][1]
        oJsonResp['error'][nError]["code"] := '803'
        oJsonResp["error"][nError]["messager"] := EncodeUTF8(aError[nError][2])
    next nError
endif

//-------------------------------------------------------------------
// Serializa objeto Json
//-------------------------------------------------------------------
cJsonResp := FwJsonSerialize( oJsonResp )
Self:SetResponse( cJsonResp )

return .T.

/*
Metodo respons�vel por persistir opera��es de altera��o do cadastro de Participantes ao TAF
@author Henrique Fabiano Pateno Pereira / Denis Souza
@since 08/07/2020
@version 1.0 
*/ 

WSMETHOD PUT PARTICIPANTS WSREST TSIPARTICIPANTE

Local cJsonResp := ""
Local oBjJson   := JsonObject():New()
Local oJsonResp	:= JsonObject():New()
Local aError    := {}
Local nError    := 0
Local nGetNames := 0
Local aNames    := {}

if VldExecute( self:sourceBranch, @oJsonResp )
    cBody := self:GetContent( )
    oBjJson:fromJSON( cBody )

    if len(oBjJson:GetNames())>0
        for nGetNames := 1 to len(oBjJson:GetNames())
            oJsonResp['success'] := .t.
            aNames := oBjJson:GetJsonObject( oBjJson:GetNames()[nGetNames] )
            if oBjJson:GetNames()[nGetNames] == 'participants'
                TAFA565( TAFA568( ), aNames, MODEL_OPERATION_UPDATE, @aError )
            endif
        next nGetNames

        if len(aError) == 0
            oJsonResp['success'] := .t.
        endif
    else
        oJsonResp['success'] := .f.
        oJsonResp['errorcode'] := '400'
        oJsonResp['message'] := EncodeUTF8( "Requisi��o mal-formada -  a requisi��o n�o est� de acordo com o formato esperado. Verifique o JSON (body) que est� sendo enviado" )
    endif

    for nError := 1 to len(aError)
        if nError == 1
            oJsonResp['error'] := {}
        endif
        aAdd( oJsonResp['error'],JsonObject():New())
        oJsonResp["error"][nError]["errorDescription"] := aError[nError][1]
        oJsonResp['error'][nError]["code"] := '404'
        oJsonResp["error"][nError]["messager"] := EncodeUTF8(aError[nError][2])
    next nError
endif

//-------------------------------------------------------------------
// Serializa objeto Json
//-------------------------------------------------------------------
cJsonResp := FwJsonSerialize( oJsonResp )
Self:SetResponse( cJsonResp )

Return .t.

/*
Metodo respons�vel por persistir opera��es de dele��o do cadastro de Participantes ao TAF
@author Henrique Fabiano Pateno Pereira / Denis Souza
@since 08/07/2020
@version 1.0 
*/

WSMETHOD DELETE PARTICIPANTS WSREST TSIPARTICIPANTE

Local cJsonResp := ""
Local oBjJson   := JsonObject():New()
Local oJsonResp := JsonObject():New()
Local aError    := {}
Local nError    := 0
Local nGetNames := 0
Local aNames    := {}

if VldExecute( self:sourceBranch, @oJsonResp )
    cBody := self:GetContent( )
    oBjJson:fromJSON( cBody )

    if len(oBjJson:GetNames())>0
        for nGetNames := 1 to len(oBjJson:GetNames())
            oJsonResp['success'] := .t.
            aNames := oBjJson:GetJsonObject( oBjJson:GetNames()[nGetNames] )
            if  oBjJson:GetNames()[nGetNames] == 'participants'
                TAFA565( TAFA568( ), aNames, MODEL_OPERATION_DELETE, aError )
            endif
        next nGetNames
    else
        oJsonResp['success'] := .f.
        oJsonResp['errorcode'] := '400'
        oJsonResp['message'] := EncodeUTF8( "Requisi��o mal-formada -  a requisi��o n�o est� de acordo com o formato esperado. Verifique o JSON (body) que est� sendo enviado" )
    endif

    for nError := 1 to len(aError)
        if nError == 1
            oJsonResp['error'] := {}
        endif
        aAdd( oJsonResp['error'],JsonObject():New())
        oJsonResp["error"][nError]["errorDescription"] := aError[nError][1]
        oJsonResp['error'][nError]["code"] := '404'
        oJsonResp["error"][nError]["messager"] := EncodeUTF8(aError[nError][2])
    next nError
endif

//-------------------------------------------------------------------
// Serializa objeto Json
//-------------------------------------------------------------------
cJsonResp := FwJsonSerialize( oJsonResp )
Self:SetResponse( cJsonResp )

Return .t.

/*
Metodo respons�vel por consultar o cadastro de Participantes ao TAF
@author Henrique Fabiano Pateno Pereira / Denis Souza
@since 08/07/2020
@version 1.0 
*/

WSMETHOD GET PARTICIPANTS WSREST TSIPARTICIPANTE

Local cJsonResp     as character
Local cCodDe        as character
Local cCodAte       as character
Local cAlias        as character
Local cTagChave     as character
Local cTabSearch    as character
Local cCmpSearch    as character
Local cOrder        as character
Local nIndSearch    as numeric
Local nPage         as numeric 
Local nPageSize     as numeric 
Local nStruct       as numeric 
Local nPos          as numeric
Local nPageAux      as numeric
Local lHasNext      as logical
Local aStruct       as array
Local oHash         as object
Local oJsonResp     as object

cJsonResp   := ''
cCodDe      := ''
cCodAte     := ''
cAlias      := ''
cTagChave   := 'participants'
cTagJson    := ''
cTabSearch  := ''
cCmpSearch  := ''
nIndSearch  := 0
nPage       := 0
nPageSize   := 10
nStruct     := 0
nPos        := 0
nPageAux    := 0
lHasNext    := .f.
aStruct     := {}
oJsonResp	:= JsonObject():New()
oHash       := TAFA568( )

if VldExecute( self:sourceBranch, @oJsonResp )

    If valtype( self:page ) <> 'U' .and. self:page > 0
        nPage := self:page
        if valtype( self:pageSize ) <> 'U' .and. self:pageSize > 0
            nPageSize := self:pageSize
        endif
    endif

    If valtype( self:codDe ) <> 'U' .and. !Empty(self:codDe)
        cCodDe := self:codDe
    else
        cCodDe := ' '
    endif

    If valtype( self:codAte ) <> 'U' .and. !Empty(self:codAte)
        cCodAte := self:codAte
    else
        cCodAte := 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'
    endif

    If valtype( self:Order ) <> 'U' .and. !Empty( self:Order )
        cOrder := self:Order
    else
        cOrder := '1'
    endif

    cAlias := GetParti(cCodDe, cCodAte, nPage, nPageSize, cOrder )

    aStruct := C1H->(DBStruct())
    while (cAlias)->(!eof())
        nPageAux++
        if nPageAux == 1
            oJsonResp[cTagChave] := {}
        endif

        if nPageAux <= nPageSize

            aAdd( oJsonResp[cTagChave],JsonObject():New())

            for nStruct := 1 to len(aStruct)
                if hmget( oHash, "#F3#" + alltrim(aStruct[nStruct][1]),"") //Busca de Campo F3
                    if hmget( oHash, alltrim(aStruct[nStruct][1]), @cTagJson ) //Obtem Tag do Campo
                        nPos := Len(oJsonResp[cTagChave])
                        cTabSearch := cCmpSearch  := ''
                        nIndSearch := 0
                        ChkPartiF3( alltrim(aStruct[nStruct][1]), @cTabSearch, @nIndSearch, @cCmpSearch )
                        if nIndSearch > 0 //indice
                            (cTabSearch)->( DbSetOrder( nIndSearch ) )
                            if (cTabSearch)->( DBSeek( xFilial(cTabSearch) + &('(cAlias)->'+aStruct[nStruct][1]) ) )
                                oJsonResp[cTagChave][nPos][cTagJson] := Alltrim(&('(cTabSearch)->'+cCmpSearch))
                            endif
                        else //Se nao localizar o codigo recupera Id TAF na Tag
                            oJsonResp[cTagChave][nPos][cTagJson] := Alltrim(&('(cAlias)->'+aStruct[nStruct][1]))
                        endif
                    endif
                //Busca Demais Campos
                elseif  hmget( oHash, alltrim(aStruct[nStruct][1]), @cTagJson )
                    nPos := Len(oJsonResp[cTagChave])
                    oJsonResp[cTagChave][nPos][cTagJson] := Alltrim(&('(cAlias)->'+aStruct[nStruct][1]))
                //Busca de Campo Date
                elseif hmget( oHash, alltrim(aStruct[nStruct][1]) + "#DT#", @cTagJson )
                    nPos := Len(oJsonResp[cTagChave])
                    oJsonResp[cTagChave][nPos][cTagJson] := Alltrim(DTOC(STOD( &('(cAlias)->'+aStruct[nStruct][1]))))
                endif
            Next nStruct
        else
            lHasNext := .t.
            exit
        endif
        (cAlias)->(DbSkip())
    enddo

    if nPageAux == 0
        oJsonResp["code"] := "404"
        oJsonResp["message"]:= EncodeUTF8("Itens n�o Encontrados")
        oJsonResp["detailedMessage"]:= EncodeUTF8("N�o encontrado Itens para os par�metros enviados")
        oJsonResp["helpUrl"]:= EncodeUTF8("https://api.totvs.com.br/")
    endif
endif

oJsonResp["hasnext"] := lHasNext
//-------------------------------------------------------------------
// Serializa objeto Json
//-------------------------------------------------------------------
cJsonResp := FwJsonSerialize( oJsonResp )
Self:SetResponse( cJsonResp )
freeobj(oJsonResp)
cCodDe := cCodAte := cJsonResp := ' '

Return .t.

/*
Fun��o respons�vel por retorna indice do ID do TAF para buscar o campo 
correspondente a chave enviada no post
@author Denis Souza
@since 08/07/2020
@version 1.0 
*/

Static Function ChkPartiF3( cCmp, cTabSearch, nIndSearchc, CmpSearch )

    if cCmp == "C1H_CODMUN"
        cTabSearch  := "C07"
        nIndSearchc := 3
        CmpSearch   := "C07_CODIGO"
    elseif cCmp == 'C1H_CODPAI'
        cTabSearch  := "C08"
        nIndSearchc := 3
        CmpSearch   := "C08_CODIGO"
    elseif cCmp == 'C1H_UF'
        cTabSearch  := "C09"
        nIndSearchc := 3
        CmpSearch   := "C09_UF"
    endif

Return Nil