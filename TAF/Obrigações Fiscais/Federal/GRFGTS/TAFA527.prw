#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFA527
@type			function
@description	Fun��o principal do processo de Guia de Recolhimento do FGTS.
@author			Felipe C. Seolin
@since			11/01/2019
/*/
//---------------------------------------------------------------------
Function TAFA527()

Local cMessage	:=	""

PanelBanner( .F. )

If TAFAlsInDic( "V3A" )
	PanelGRFGTS()
Else
	cMessage := "Ambiente desatualizado com a vers�o do programa existente no reposit�rio de dados."
	cMessage += Chr( 13 ) + Chr( 10 )
	cMessage += Chr( 13 ) + Chr( 10 )
	cMessage += "Execute a atualiza��o do dicion�rio da GRFGTS por meio do compatibilizador UPDDISTR."

	Aviso( "Dicion�rio Incompat�vel", cMessage, { "Encerrar" }, 2 )
EndIf

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} PanelBanner
@type			function
@description	Painel referente ao Tutorial da funcionalidade.
@author			Felipe C. Seolin
@since			16/01/2019
@param			lForceShow	-	Indica se o Painel obrigatoriamente deve ser exibido
/*/
//---------------------------------------------------------------------
Static Function PanelBanner( lForceShow )

Local oProfile		:=	Nil
Local oDialog		:=	Nil
Local oTIBrowser	:=	Nil
Local oCheck		:=	Nil
Local aSize			:=	MsAdvSize()
Local cRetProf		:=	""
Local nHeightBox	:=	0
Local nWidthBox		:=	0
Local nHeight		:=	aSize[6]
Local nWidth		:=	aSize[5]
Local nPosIni		:=	0
Local lCheck		:=	.F.
Local lRet			:=	.F.
Local bClose		:=	{ || }

If lForceShow
	bClose := { || oDialog:End() }
Else
	bClose := { || ( SetProfile( oProfile, lCheck ), oDialog:End() ) }

    oProfile := FWProfile():New()
    oProfile:SetUser( RetCodUsr() )
    oProfile:SetProgram( "TAFA527" )
    oProfile:SetTask( "GRFGTS" )
    oProfile:Load()

    cRetProf := oProfile:GetStringProfile()

    If Empty( cRetProf ) .or. DateDiffDay( Date(), SToD( cRetProf ) ) >= 7
	    lRet := .T.
    EndIf
EndIf

If lRet .or. lForceShow
	oDialog := MsDialog():New( 0, 0, nHeight, nWidth, "Documenta��o GRFGTS",,,,,,,,, .T. )

	nHeightBox := ( nHeight - 60 ) / 2
	nWidthBox := ( nWidth - 20 ) / 2

	oTIBrowser := TIBrowser():New( 0, 0, nWidthBox, nHeightBox, "http://tdn.totvs.com/plugins/servlet/remotepageview?pageId=286737675", oDialog )

    If !lForceShow
	    oCheck := TCheckBox():New( nHeightBox + 10, 10, "N�o exibir pelos pr�ximos 7 dias.", { || lCheck }, oDialog, 500, 010,, { || lCheck := !lCheck },,,,,, .T. )
    EndIf

    nPosIni := ( ( nWidth - 20 ) / 2 ) - 52
	TButton():New( nHeightBox + 10, nPosIni, "Fechar", oDialog, bClose, 50, 15,,,, .T. )

	oDialog:Activate( ,,,.T. )
EndIf

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} SetProfile
@type			function
@description	Atualiza o arquivo de profile da GRFGTS.
@author			Felipe C. Seolin
@since			29/01/2019
@param			oProfile	-	Objeto referente ao arquivo de profile
@param			lCheck		-	Indica se a op��o para n�o exibir nos pr�ximos 7 dias foi marcada
/*/
//---------------------------------------------------------------------
Static Function SetProfile( oProfile, lCheck )

Local cSetting	:=	""

If lCheck
    cSetting := DToS( Date() )
EndIf

oProfile:SetStringProfile( cSetting )
oProfile:Save()

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} PanelGRFGTS
@type			function
@description	Painel com as requisi��es e a��es disponibilizadas.
@author			Felipe C. Seolin
@since			16/01/2019
/*/
//---------------------------------------------------------------------
Static Function PanelGRFGTS()

Local oDialog		:=	Nil
Local oLayer		:=	Nil
Local oMrkBrowse	:=	Nil
Local oPanel01		:=	Nil
Local oPanel02		:=	Nil
Local oPanel03		:=	Nil
Local oPanel04		:=	Nil
Local oSay1			:=	Nil
Local oSay2			:=	Nil
Local oSay3			:=	Nil
Local oSay4			:=	Nil
Local oMeter		:=	Nil
Local oTimer		:=	Nil
Local oImage		:=	Nil
Local cTime			:=	Time()
Local cAmpulheta	:=	""
Local nTop			:=	0
Local nHeight		:=	0
Local nWidth		:=	0
Local nCont			:=	60
Local nTimer		:=	nCont + 1
Local nMeter		:=	0
Local aSize			:=	FWGetDialogSize()
Local lFreeze		:=	.T.
Local lDeplete		:=	.F.
Local lPlay			:=	.T.
Local bInclui		:=	{ || TAFFGTSWiz() }
Local bTransmit		:=	{ || }
Local bConsult		:=	{ || }
Local bInconsist	:=	{ || }
Local bLog			:=	{ || }
Local bArquivo		:=	{ || }
Local bReports		:=	{ || }
Local bCronom		:=	{ || ( SetTimer( @nCont ), UpdPanel03( nCont, @nTimer, @oSay4, @cAmpulheta, @oMeter, @nMeter, @oSay2, @cTime, @lDeplete, @lFreeze, .T., @oMrkBrowse ) ) }
Local bTutorial		:=	{ || PanelBanner( .T. ) }
Local bClose		:=	{ || oDialog:End() }

/*----------------------------------------------------------

Constru��o do Painel Principal

----------------------------------------------------------*/
oDialog := MsDialog():New( aSize[1], aSize[2], aSize[3], aSize[4], "GRFGTS",,,, nOr( WS_VISIBLE, WS_POPUP ),,,,, .T.,,,, .F. )

oLayer := FWLayer():New()

oLayer:Init( oDialog, .F. )

oLayer:AddLine( "LINE01", 83 )
oLayer:AddLine( "LINE02", 18 )

oLayer:AddCollumn( "BOX01",88,, "LINE01" )
oLayer:AddCollumn( "BOX02",12,, "LINE01" )
oLayer:AddCollumn( "BOX03",88,, "LINE02" )
oLayer:AddCollumn( "BOX04",12,, "LINE02" )

oLayer:AddWindow( "BOX01", "PANEL01", "Hist�rico de Requisi��es", 100, .F.,,, "LINE01" )
oLayer:AddWindow( "BOX02", "PANEL02", "A��es", 100, .F.,,, "LINE01" )
oLayer:AddWindow( "BOX03", "PANEL03", "Cron�metro", 100, .F.,,, "LINE02" )
oLayer:AddWindow( "BOX04", "PANEL04", "Play/Pause", 100, .F.,,, "LINE02" )

oPanel01 := oLayer:GetWinPanel( "BOX01", "PANEL01", "LINE01" )
oPanel02 := oLayer:GetWinPanel( "BOX02", "PANEL02", "LINE01" )
oPanel03 := oLayer:GetWinPanel( "BOX03", "PANEL03", "LINE02" )
oPanel04 := oLayer:GetWinPanel( "BOX04", "PANEL04", "LINE02" )

/*----------------------------------------------------------

Constru��o do Painel 01 - Browse de Hist�rico de Requisi��es

----------------------------------------------------------*/
oMrkBrowse := FWMarkBrowse():New()
oMrkBrowse:SetOwner( oPanel01 )
oMrkBrowse:SetAlias( "V3A" )
//oMrkBrowse:SetFieldMark( "V3A_MARK" )
oMrkBrowse:SetUseFilter()
oMrkBrowse:AddFilter( "N�o Transmitido", "V3A_STATUS == '0'", .F., .F. )
oMrkBrowse:AddFilter( "Inv�lido", "V3A_STATUS == '1'", .F., .F. )
oMrkBrowse:AddFilter( "Transmitido", "V3A_STATUS == '2'", .F., .F. )
oMrkBrowse:SetMenuDef( "" )
oMrkBrowse:SetIgnoreARotina( .T. )
oMrkBrowse:SetChange( { || Iif( lPlay .and. oMeter <> Nil, UpdPanel03( nCont, @nTimer, @oSay4, @cAmpulheta, @oMeter, @nMeter, @oSay2, @cTime, @lDeplete, @lFreeze, .T., @oMrkBrowse ), Nil ) } )
oMrkBrowse:Activate()

/*----------------------------------------------------------

Constru��o do Painel 02 - A��es

----------------------------------------------------------*/
nWidth := ( oPanel02:nClientWidth / 2 ) - 3
nHeight := Int( ( oPanel02:nClientHeight / 2 ) / 10 ) - 5

nTop := 5
TButton():New( 005, 002, "Nova Opera��o", oPanel02, bInclui, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

nTop += nHeight + 5
TButton():New( nTop, 002, "Transmitir", oPanel02, bTransmit, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

nTop += nHeight + 5
TButton():New( nTop, 002, "Consultar", oPanel02, bConsult, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

nTop += nHeight + 5
TButton():New( nTop, 002, "Exibir Inconsist�ncias", oPanel02, bInconsist, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

nTop += nHeight + 5
TButton():New( nTop, 002, "Log de Inconsist�ncias", oPanel02, bLog, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

nTop += nHeight + 5
TButton():New( nTop, 002, "Abrir Arquivo", oPanel02, bArquivo, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

nTop += nHeight + 5
TButton():New( nTop, 002, "Relat�rios", oPanel02, bReports, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

nTop += nHeight + 5
TButton():New( nTop, 002, "Cron�metro", oPanel02, bCronom, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

nTop += nHeight + 5
TButton():New( nTop, 002, "Tutorial", oPanel02, bTutorial, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

nTop += nHeight + 5
TButton():New( nTop, 002, "Fechar", oPanel02, bClose, nWidth, nHeight,,,, .T.,,,, { || lFreeze } )

/*----------------------------------------------------------

Constru��o do Painel 03 - Atualiza��o

----------------------------------------------------------*/
cAmpulheta := ConvToMMSS( nCont, @nTimer, @lDeplete )

nTop := ( oPanel03:nClientHeight / 2 ) / 4
oSay1 := TSay():New( nTop, ( oPanel03:nRight / 2 ) - 220, { || '<font size="3" color="#0c9abe"><b>�ltima atualiza��o realizada em</b></font><br/>' }, oPanel03,,,,,, .T.,,, 095, 010,,,,,, .T. )
oSay3 := TSay():New( nTop, ( oPanel03:nRight / 2 ) - 105, { || '<font size="3" color="#0c9abe"><b>Pr�xima atualiza��o em</b></font><br/>' }, oPanel03,,,,,, .T.,,, 095, 010,,,,,, .T. )

nTop += 7
oSay2 := TSay():New( nTop, ( oPanel03:nRight / 2 ) - 200, { || '<font size="6" color="#0c9abe"><b>' + cTime + '</b></font><br/>' }, oPanel03, "99:99:99",,,,, .T.,,, 050, 013,,,,,, .T. )
oSay4 := TSay():New( nTop, ( oPanel03:nRight / 2 ) - 105, { || '<font size="6" color="#0c9abe"><b>' + cAmpulheta + '</b></font><br/>' }, oPanel03, "99:99",,,,, .T.,,, 035, 012,,,,,, .T. )

nTop += 1
oMeter := TMeter():New( nTop, ( oPanel03:nRight / 2 ) - 65, { |x| Iif( PCount() > 0, nMeter := x, nMeter ) }, nCont, oPanel03, 060, 012,, .T. )

oTimer := TTimer():New( 1000, { || Iif( lPlay, UpdPanel03( nCont, @nTimer, @oSay4, @cAmpulheta, @oMeter, @nMeter, @oSay2, @cTime, @lDeplete, @lFreeze, .F., @oMrkBrowse ), Nil ) }, oDialog )
oTimer:Activate()

/*----------------------------------------------------------

Constru��o do Painel 01 - Play/Pause

----------------------------------------------------------*/
oImage := TBitmap():New( oPanel04:nClientHeight / 5.46, oPanel04:nClientWidth / 5.3, 058, 050, "bottom.png",, .T., oPanel04, { || lPlay := !lPlay },,,,,,,, .T. )
oImage:Align := CONTROL_ALIGN_CENTER

/*----------------------------------------------------------

Ativa��o e Desativa��o da Interface

----------------------------------------------------------*/
oDialog:Activate()

oMrkBrowse:DeActivate()

TAFEncArr( aSize )

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} SetTimer
@type			function
@description	Interface para informar o tempo desejado para a ampulheta.
@author			Felipe C. Seolin
@since			28/01/2019
@param			nCont	-	Tempo atual da ampulheta
/*/
//---------------------------------------------------------------------
Static Function SetTimer( nCont )

Local oDlg			:=	Nil
Local nAux			:=	nCont
Local nHeightBox	:=	0
Local nWidthBox		:=	0
Local nTop			:=	0
Local nHeight		:=	220
Local nWidth		:=	520
Local nPosIni		:=	0

oDlg := MsDialog():New( 0, 0, nHeight, nWidth, "Parametriza��o do Cron�metro",,,,,,,,, .T. )

nHeightBox := ( nHeight - 60 ) / 2
nWidthBox := ( nWidth - 20 ) / 2

@10,10 to nHeightBox,nWidthBox of oDlg Pixel

nTop := 20
TSay():New( nTop, 20, { || '<font size="3" color="#0c9abe"><b>Informe o valor, em segundos, a ser atribu�do para o contador do cron�metro</b></font><br/>' }, oDlg,,,,,, .T.,,, 200, 020,,,,,, .T. )
nTop += 30
TGet():New( nTop, 20, { |x| If( PCount() == 0, nAux, nAux := x ) }, oDlg, 65, 10, "999", { || ValidTimer( nAux ) },,,,,, .T.,,,,,,,,,, "nAux",,,,,,, '<font size="3" color="#0c9abe"><b>Cron�metro</b></font><br/>', 1 )
nTop += 20

nPosIni := ( ( nWidth - 20 ) / 2 ) - 64
SButton():New( nHeightBox + 10, nPosIni, 1, { || Iif( ValidTimer( nAux ), ( nCont := nAux, oDlg:End() ), Nil ) }, oDlg )
nPosIni += 32
SButton():New( nHeightBox + 10, nPosIni, 2, { || oDlg:End() }, oDlg )

oDlg:Activate( ,,,.T. )

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} ValidTimer
@type			function
@description	Valida��o da entrada de dados para o tempo desejado para a ampulheta.
@author			Felipe C. Seolin
@since			28/01/2019
@param			nCont	-	Tempo informado para atualiza��o da ampulheta
@return			lRet	-	Indica se todas as condi��es foram respeitadas
/*/
//---------------------------------------------------------------------
Static Function ValidTimer( nCont )

Local lRet	:=	.T.

If nCont < 30
	MsgInfo( "Valor m�nimo a ser informado em 'Cron�metro' deve ser superior a 29 segundos." )
	lRet := .F.
EndIf

Return( lRet )

//---------------------------------------------------------------------
/*/{Protheus.doc} ConvToMMSS
@type			function
@description	Convers�o da de segundos da ampulheta para o formato MM:SS.
@author			Felipe C. Seolin
@since			16/01/2019
@param			nCont		-	Tempo atual da ampulheta
@param			nTimer		-	Tempo relacionado ao objeto de Timer
@param			lDeplete	-	Indica se o tempo da ampulheta esgotou
@param			lReset		-	Indica se deve for�ar a reinicializa��o da ampulheta
@return			cRet		-	Tempo atual da ampulheta no formato MM:SS
/*/
//---------------------------------------------------------------------
Static Function ConvToMMSS( nCont, nTimer, lDeplete, lReset )

Local cRet		:=	""
Local nMinutes	:=	0
Local nSeconds	:=	0

If nTimer == 0 .or. lReset
	nTimer := nCont
	lDeplete := .T.
Else
	nTimer --
	lDeplete := .F.
EndIf

nMinutes := Int( nTimer / 60 )
nSeconds := Mod( nTimer, 60 )

cRet := StrZero( nMinutes, 2 ) + ":" + StrZero( nSeconds, 2 )

Return( cRet )

//---------------------------------------------------------------------
/*/{Protheus.doc} UpdPanel03
@type			function
@description	Constru��o do Painel de Atualiza��o.
@author			Felipe C. Seolin
@since			16/01/2019
@param			nCont		-	Tempo atual da ampulheta
@param			nTimer		-	Tempo relacionado ao objeto de Timer
@param			oSay4		-	Objeto que exibe a ampulheta
@param			cAmpulheta	-	Valor a ser exibido na ampulheta
@param			oMeter		-	Objeto da barra de progresso
@param			nMeter		-	Valor do progresso atual da barra de progresso
@param			oSay2		-	Objeto que exibe a �ltima atualiza��o no formato HH:MM:SS
@param			cTime		-	Valor a ser exibido na na �ltima atualiza��o
@param			lDeplete	-	Indica se o tempo da ampulheta esgotou
@param			lFreeze		-	Indica se os objetos da interface devem ficar congelados durante a atualiza��o da Browse
@param			lReset		-	Indica se deve for�ar a reinicializa��o da ampulheta
@param			oMrkBrowse	-	Objeto da Browse
/*/
//---------------------------------------------------------------------
Static Function UpdPanel03( nCont, nTimer, oSay4, cAmpulheta, oMeter, nMeter, oSay2, cTime, lDeplete, lFreeze, lReset, oMrkBrowse )

If lReset
    cAmpulheta := ConvToMMSS( nCont, @nTimer, @lDeplete, lReset )
    nMeter := 0
    oMeter:SetTotal( nCont )
Else
    cAmpulheta := ConvToMMSS( nCont, @nTimer, @lDeplete, lReset )

    If lDeplete
	    nMeter := 0
	    cTime := Time()
	    lFreeze := .F.
	    FWMsgRun( , { || oMrkBrowse:Refresh( .T. ) }, "Atualizando", "Aguarde..." )
	    lFreeze := .T.
    Else
	    nMeter ++
    EndIf
EndIf

oMeter:Set( nMeter )

oSay2:Refresh()
oSay4:Refresh()

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
@type			function
@description	Constru��o do modelo MVC.
@author			Veronica Toledo
@since			01/02/2019
/*/
//----------------------------------------------------------------------
Static Function ModelDef()

Local oModel		:=	MPFormModel():New( "TAFA527" )
Local oStructV3A	:=	FWFormStruct( 1, "V3A" )

oModel:AddFields( "MODEL_V3A",, oStructV3A )
oModel:GetModel( "MODEL_V3A" ):SetPrimaryKey( { "V3A_FILIAL", "V3A_ID" } )

Return( oModel )