#Include "Protheus.ch"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "APWIZARD.CH"
#Include "FWBROWSE.ch"

#DEFINE  DIMENSAO_TELA   MsAdvSize(.F.)
#DEFINE  PROPORCAO_TELA  Val("0." + Str(DIMENSAO_TELA[6]))
#DEFINE  LARGURA_PAINEL  DIMENSAO_TELA[6] * 0.78
#DEFINE  ALTURA_PAINEL   DIMENSAO_TELA[5] * 0.78
#DEFINE  LARGURA_TEXTOS  IIF( DIMENSAO_TELA[6] > 650, DIMENSAO_TELA[6] * 0.75, DIMENSAO_TELA[6] * 0.60 ) //DIMENSAO_TELA[6] * PROPORCAO_TELA

#DEFINE  COLUNA_IDTRAB   1
#DEFINE  COLUNA_CPFTRAB  2
#DEFINE  COLUNA_NOMTRAB  3
#DEFINE  COLUNA_MATRIC   4
#DEFINE  COLUNA_CATTRAB  5
#DEFINE  COLUNA_TRABDEL  6

#DEFINE  COLUNA_IDINSCR  1
#DEFINE  COLUNA_NRINSCR  2
#DEFINE  COLUNA_IDLOTAC  3
#DEFINE  COLUNA_CODLOTA  4
#DEFINE  COLUNA_ESTDEL   5

//Static aGuia     := {}
Static lGoFirst  := .F. // Indica se, ao selecionar o bot�o voltar, o Wizard retornar� ao painel principal ao inv�s do painel anterior

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFFGTSWiz
Monta o Wizard da GRFGTS com os passos.

@author  Leandro F. Dourado
@since   11/01/2019
@version 1
/*/
//-------------------------------------------------------------------
Function TAFFGTSWiz()
Local oFont         := Nil
Local oWizard	    := Nil
Local oRadio 		:= Nil

Local aItens 		:= {}
Local oGroup        := Nil
Local oFontGrp      := TFont():New( "Times New Roman",, -14 ,, .T.  )
//Local oFontSay      := TFont():New( "Times New Roman",, -12 ,, .F.  )

Local oNewPag       := Nil

Local oPnl1	 		:= Nil
Local oSayPnl1      := Nil
Local oRadio        := Nil
Local nRadioPnl1	:= 1

Local oPnl2			:= Nil
Local oSayPnl2      := Nil
Local oRadioPnl2    := Nil
//Local nRadioPnl2	:= 1

Local oPnl3	 		:= Nil
Local oPnl4	 		:= Nil

Local oGroup        := Nil

Local nColuna       := 0
Local nAltLinha     := 0
Local nLargLinha    := 0

Local nLinLabel     := 0
Local nLinGet       := 0
Local aCoord 		:= {DIMENSAO_TELA[7],0 , DIMENSAO_TELA[6] * 0.75, DIMENSAO_TELA[5] * 0.75 }	//Coordenadas da Janela 

Private nRadioPnl2  := 1

oWizard := ApWizard():New( "", "Selecione entre as op��es disponibilizadas pelo Governo, entre a solicita��o/emiss�o das guias mensais e rescis�rias da GRFGTS.",;
						   "", "", { || SelNextOper( nRadioPnl1, oWizard ) }, { || .T.}, .T.,,,.F., aCoord  )

// Painel Principal do Wizard
CREATE PANEL oWizard;
	HEADER  "";
	MESSAGE "";
	BACK { || .F. };
	NEXT { || SelNextOper( nRadioPnl1, oWizard ) };
	FINISH { || .T. };
	PANEL

	oPnl1		:=�	TPanel():New( 0, 0, , oWizard:oMPanel[ 2 ],, .F., .F.,,, LARGURA_PAINEL, ALTURA_PAINEL, .T., .F. )
	
	TSay():New( 5,  15, {||  TAFFGTSTxt("TITULO","Selecione entre as op��es disponibilizadas pelo Governo, entre a solicita��o/emiss�o das guias mensais e rescis�rias da GRFGTS.")   }, oPnl1,,/*oFontSay*/,,,,.T.,,,LARGURA_TEXTOS,50,,,,,,.T.)
	
	aItens 		:= {"Solicitar Guia Mensal", "Emitir Guia Mensal", "Emitir/Atualizar Guia Rescis�ria"}
	
	oRadio := TRadMenu():New( 30, 15, aItens,, oPnl1,,,,,,,, 150, 12,,,, .T.)
	oRadio:setCSS( TAFFGTSTxt("RADIO") )
	oRadio:bSetGet := { |u| Iif( PCount() == 0, nRadioPnl1, nRadioPnl1 := u ) }
	
	oGroup       := TGroup():New(80,10,170,LARGURA_TEXTOS+20,'Instru��es da opera��o',oPnl1,CLR_GRAY,,.T.)
	oGroup:oFont := oFontGrp
	
	oSayPnl1 := TSay():New(95,  15, {||  TAFFGTSTxt("TEXTO","A guia mensal por compet�ncia fica dispon�vel para emiss�o pelo governo ap�s o envio do evento de fechamento da folha (S-1299). Caso deseje emitir a guia antes do fechamento, ser� necess�rio utilizar essa opera��o.")   }, oGroup,,/*oFontSay*/,,,,.T.,,,LARGURA_TEXTOS,150,,,,,,.T.)
	oRadio:bChange := { || Pnl01Radio(nRadioPnl1, oSayPnl1) }

// Painel 2 - Sele��o de tipo de guia para solicita��o
CREATE PANEL oWizard;
	HEADER  "";
	MESSAGE "";
	BACK { || .T. };			 
	NEXT { || SolicTpGuia( nRadioPnl2, oWizard ) };
	FINISH { || .T. };
	PANEL


// Painel 3 - Painel final de todas as opera��es, sendo montado de forma din�mica
CREATE PANEL oWizard;
	HEADER  ""; 
	MESSAGE "";
	BACK   { || SelPrevPanel( oWizard ) };			 
	NEXT   { || .T. };
	FINISH { || .T. };
	PANEL

	// Pula a tela inicial padr�o do Wizard	
	oWizard:nPanel+=1
	oWizard:SetPanel(oWizard:nPanel)
									
Activate WIZARD oWizard Centered


lGoFirst := .F.	

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} Pnl01Radio
Altera o texto com as instru��es de cada op��o, de acordo com a op��o selecionada.

@author  Victor A. Barbosa
@since   04/10/2018
@version 1
/*/
//-------------------------------------------------------------------
Static Function Pnl01Radio(nRadio, oSayInstr)

Do Case
	Case nRadio == 1
		oSayInstr:SetText(TAFFGTSTxt("TEXTO","A guia mensal por compet�ncia fica dispon�vel para emiss�o pelo governo ap�s o envio do evento de fechamento da folha (S-1299). Caso deseje emitir a guia antes do fechamento, ser� necess�rio utilizar essa opera��o."))
	Case nRadio == 2
		oSayInstr:SetText(TAFFGTSTxt("TEXTO","Utilize essa opera��o para emiss�o da guia mensal por compet�ncia (caso o evento de fechamento da folha tenha sido enviado) ou por n�mero de guia. O n�mero de guia � gerado a partir de uma solicita��o de guia."))
	Case nRadio == 3
		oSayInstr:SetText(TAFFGTSTxt("TEXTO","Utilize essa opera��o para a emiss�o ou atualiza��o de guias rescis�rias."))
EndCase

oSayInstr:CtrlRefresh()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} Pnl02Radio
Altera o texto com as instru��es de cada op��o, de acordo com a op��o selecionada.

@author  Leandro F. Dourado
@since   16/01/2019
@version 1
/*/
//-------------------------------------------------------------------
Static Function Pnl02Radio(nRadio, oSayInstr)

Do Case
	Case nRadio == 1
		oSayInstr:SetText(TAFFGTSTxt("TEXTO","Guia para recolhimento de uma �nica compet�ncia. Contempla todos os trabalhadores que tiveram a remunera��o informada para o per�odo at� o momento da gera��o."))
	Case nRadio == 2
		oSayInstr:SetText(TAFFGTSTxt("TEXTO","Solicita��o de guia para recolhimento de FGTS para trabalhadores espec�ficos." + CRLF +; 
    										 "A solicita��o pode ser feita para um determinado trabalhador em uma compet�ncia espec�fica ou para todas as compet�ncias em aberto." + CRLF +; 
    										 "A solicita��o tamb�m pode ser feita para diversos trabalhadores, por�m em uma �nica compet�ncia."))
	Case nRadio == 3
		oSayInstr:SetText(TAFFGTSTxt("TEXTO","Permite gera��o de guia por estabelecimento. Tamb�m podem ser informadas as lota��es tribut�rias e os trabalhadores de cada estabelecimento informado."))
	Case nRadio == 4
		oSayInstr:SetText(TAFFGTSTxt("TEXTO","Permite gera��o de guia por lota��o tribut�ria. Tamb�m podem ser informados os trabalhadores de cada lota��o tribut�ria."))
EndCase

oSayInstr:CtrlRefresh()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SelNextOper
Indica a opera��o selecionada para a troca correta de paineis

@author  Leandro F. Dourado
@since   04/10/2018
@version 1
/*/
//-------------------------------------------------------------------
Static Function SelNextOper( nRadioPnl1, oWizard )
Local aItens     := {}
Local oRadioPnl2 := Nil
Local oSayPnl2   := Nil
//Local nRadioPnl2 := 1

Do Case
	Case nRadioPnl1 == 1
	
		oWizard:SetPanel( 2 ) 
		PnlSelTpGuia( oWizard )
		
	Case nRadioPnl1 == 2
	
		oWizard:SetPanel( 3 )
		PnlEmitGuia( oWizard )
		
	Case nRadioPnl1 == 3
	
EndCase

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} PnlSelTpGuia
Cria o painel para a sele��o do tipo de guia que se deseja solicitar.

@author  Leandro F. Dourado
@since   16/01/2019
@version 12.1.23
/*/
//-------------------------------------------------------------------
Static Function PnlSelTpGuia( oWizard )

Local aItens     := {}
Local oRadio     := Nil
Local oSay       := Nil
Local oPanel     := Nil
Local oGroup     := Nil
Local oFontGrp   := TFont():New( "Times New Roman",, -14 ,, .T.  )
//Local oFontSay   := TFont():New( "Times New Roman",, -12 ,, .F.  )
//Local nRadioPnl2 := 0

oPanel := TPanel():New( 0, 0, , oWizard:oMPanel[ 3 ],, .F., .F.,,, LARGURA_PAINEL, ALTURA_PAINEL, .T., .F. )

TSay():New( 5,  15, {||  TAFFGTSTxt("TITULO","Selecione o tipo de guia desejada para ser solicitada ao Governo.")   }, oPanel,,/*oFontSay*/,,,,.T.,,,LARGURA_TEXTOS,50,,,,,,.T.)

aItens 		:= {"Guia Consolidada por Compet�ncia", "Guia por Trabalhador", "Guia por Estabelecimento/Lota��o Tribut�ria"}

oRadio := TRadMenu():New(30, 15, aItens,, oPanel,,,,,,,, LARGURA_TEXTOS, 12,,,, .T.)
oRadio:setCSS( TAFFGTSTxt("RADIO") )
oRadio:bSetGet := { |u| Iif( PCount() == 0, nRadioPnl2, nRadioPnl2 := u ) }
//oRadio:oFont := oFontSay

oGroup       := TGroup():New(80,10,170,LARGURA_TEXTOS+20,'Instru��es da opera��o',oPanel,CLR_GRAY,,.T.)
oGroup:oFont := oFontGrp

oSay := TSay():New(95,  15, {||  TAFFGTSTxt("TEXTO","Guia para recolhimento de uma �nica compet�ncia. Contempla todos os trabalhadores que tiveram a remunera��o informada para o per�odo at� o momento da gera��o.")   }, oGroup,,/*oFontSay*/,,,,.T.,,,LARGURA_TEXTOS,150,,,,,,.T.)
oRadio:bChange := { || Pnl02Radio( nRadioPnl2, oSay ) }

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SolicTpGuia
Monta a tela de solicita��o de guia, de acordo com a op��o selecionada no painel 2.

@author  Leandro F. Dourado
@since   04/10/2018
@version 1
/*/
//-------------------------------------------------------------------
Static Function SolicTpGuia( nRadio, oWizard )

Local nLinLabel     := 25
Local nLinGet       := 37
Local nColuna       := 15
Local nAltLinha	    := 10	
Local nLargLinha    := 60

Local oPanel        := Nil
Local oPanel2       := Nil

Local cCompetencia  := Space(7)
Local cDtRec        := Space(9)

Local oBrwTrab      := Nil
Local oBrwEstab     := Nil
Local oColumn       := Nil

Local aTrab         := {}

//aGuia := {} // Reinicia variavel static

oWizard:SetPanel( 3 )

oPanel := TPanel():New( 0, 0, , oWizard:oMPanel[ 4 ],, .F., .F.,,, LARGURA_PAINEL, ALTURA_PAINEL, .T., .F. )


TSay():New(nLinLabel, nColuna, {||  TAFFGTSTxt( "TXTGET", "Compet�ncia" )   }, oPanel,,,,,,.T.,,,nLargLinha,nAltLinha,,,,,,.T.)
TGet():New(nLinGet, nColuna, {|u| if( PCount()>0, cCompetencia := u, cCompetencia )}, oPanel, nLargLinha, nAltLinha, "99/9999",, 0,,,,,.T.,,,,,,,.F., .F.,, cCompetencia )

nColuna   += 90

TSay():New(nLinLabel, nColuna, {||  TAFFGTSTxt( "TXTGET", "Dt. Recolhimento")   }, oPanel,,,,,,.T.,,,nLargLinha,nAltLinha,,,,,,.T.)
TGet():New(nLinGet  , nColuna, {|u| if( PCount()>0, cDtRec := u, cDtRec )}, oPanel, nLargLinha, nAltLinha, "99/99/9999",, 0,,,,,.T.,,,,,,,.F., .F.,, cDtRec )

nColuna   := 15

Do Case
	
	Case nRadio == 1
	
		TSay():New( 5,  nColuna, {||  TAFFGTSTxt("TITULO","Solicita��o de Guia por Compet�ncia")   }, oPanel,,,,,,.T.,,,LARGURA_TEXTOS,20,,,,,,.T.)
		

	Case nRadio == 2
	
		TSay():New( 5,  nColuna, {||  TAFFGTSTxt("TITULO","Solicita��o de Guia por Trabalhador")   }, oPanel,,,,,,.T.,,,LARGURA_TEXTOS,20,,,,,,.T.)
		
		oPanel2 := TPanel():New( 65, nColuna, , oPanel,, .F., .F.,,, LARGURA_PAINEL, 100, .T., .F. )
		
		oBrwTrab := CriaBrwTrab( oPanel2 )
		
	Case nRadio == 3
	
		TSay():New( 5,  nColuna, {||  TAFFGTSTxt("TITULO","Solicita��o de Guia por Lota��o Tribut�ria")   }, oPanel,,,,,,.T.,,,LARGURA_TEXTOS,20,,,,,,.T.)
		
		oPanel3   := TPanel():New( 125, nColuna, , oPanel,, .F., .F.,,, LARGURA_PAINEL, 100, .T., .F. )
		oBrwTrab  := CriaBrwTrab( oPanel3, @aTrab )
		
		oPanel2   := TPanel():New( 65, nColuna, , oPanel,, .F., .F.,,, LARGURA_PAINEL, 60, .T., .F. )
		oBrwEstab := CriaBrwEstab( oPanel2, @aTrab )
		
		oBrwEstab:AddBrwRelation( oBrwTrab )
	
EndCase

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} CriaBrwTrab
Monta a grid para input das informa��es dos trabalhadores.

@author  Leandro F. Dourado
@since   28/01/2019
@version 12.1.23
/*/
//-------------------------------------------------------------------
Static Function CriaBrwTrab( oPanel, aTrab )
Local oBrwTrab      := Nil
Local oColumn       := Nil
Local bCommitTrab   := {|| CommitTrab(@aTrab,oBrwTrab)}

Local aNewLine      := {Space(TamSX3("C9V_ID")[1]),;
						Space(TamSX3("C9V_CPF")[1]),;
						Space(TamSX3("C9V_NOME")[1]),;
						Space(TamSX3("C9V_MATRIC")[1]),;
						Space(TamSX3("CUP_CODCAT")[1]),;
						.F.}
Local bAddLine      := {|| AddLine(aTrab, oBrwTrab, aClone(aNewLine))}
Local bDelete       := {|| FDelLine(aTrab, oBrwTrab, aClone(aNewLine))}

Default aTrab       := {}

Aadd(aTrab, aClone(aNewLine))
		
DEFINE FWBROWSE oBrwTrab DATA ARRAY EDITCELL bCommitTrab ARRAY aTrab NO SEEK NO CONFIG NO REPORT Of oPanel

ADD COLUMN oColumn DATA { || aTrab[oBrwTrab:At(),1] } 	Title "ID Trab."  TYPE "C" SIZE 06 PICTURE PesqPict("C9V","C9V_ID")      EDIT READVAR "CIDTRAB"    Of oBrwTrab
oColumn:xF3 := "C9VD"

ADD COLUMN oColumn DATA { || aTrab[oBrwTrab:At(),2] } 	Title "CPF"       TYPE "C" SIZE 11 PICTURE PesqPict("C9V","C9V_CPF")     /*EDIT READVAR "CCPF"*/       Of oBrwTrab
ADD COLUMN oColumn DATA { || aTrab[oBrwTrab:At(),3] } 	Title "Nome"      TYPE "C" SIZE 30 PICTURE PesqPict("C9V","C9V_NOME")    /*EDIT READVAR "CNOME"*/      Of oBrwTrab
ADD COLUMN oColumn DATA { || aTrab[oBrwTrab:At(),4] } 	Title "Matr�cula" TYPE "C" SIZE 30 PICTURE PesqPict("C9V","C9V_MATRIC")  /*EDIT READVAR "CMATRICULA"*/ Of oBrwTrab
ADD COLUMN oColumn DATA { || aTrab[oBrwTrab:At(),5] } 	Title "Categoria" TYPE "C" SIZE 06 PICTURE PesqPict("CUP","CUP_CODCAT")  /*EDIT READVAR "CCATEG"*/     Of oBrwTrab

oBrwTrab:SetInsert( .T. )
oBrwTrab:SetAddline(bAddLine)

oBrwTrab:SetDelete( .T., bDelete )
oBrwTrab:SetDelOk( {||.T.} )

oBrwTrab:SetLineHeight( 20 )

oBrwTrab:SetChange({|| ChangeLine( @aTrab, oBrwTrab ) } )
//oBrwTrab:SetLineOk({|| LineOk( aTrab, oBrwTrab ) })

ACTIVATE FWBrowse oBrwTrab

//FreeObj( oBrwTrab )

Return oBrwTrab

//-------------------------------------------------------------------
/*/{Protheus.doc} CommitTrab
Valida o ID do trabalhador e gatilha as demais colunas da Grid.

@author  Leandro F. Dourado
@since   22/01/2019
@version 12.1.23
/*/
//-------------------------------------------------------------------
Static Function CommitTrab( aTrab, oBrwTrab )
Local aArea    := GetArea()
Local cVar     := ReadVar()
Local cChvSeek := ""
Local lRet     := .T.
Local nLinePos := oBrwTrab:At()
Local nColPos  := oBrwTrab:ColPos()
Local cIdCateg := ""

If cVar == "CIDTRAB"
	If !Vazio()
		If aScan(aTrab,{|x| x[1] == &cVar }) > 0
			lRet := .F.
			Help("",1,"IDTRAB",,"Trabalhador j� informado nessa solicita��o!",1,0)
		EndIf
		
		If lRet
			cChvSeek := Padr(&cVar, TamSX3( "C9V_ID" )[1] ) + "1"
			lRet := ExistCpo( "C9V" , cChvSeek , 2 )
		EndIf
	
		If lRet 
			DbSelectArea("C9V")
			C9V->(DbSetOrder(2))
			
			C9V->(DbSeek(xFilial("C9V") + cChvSeek))
			
			aTrab[nLinePos, COLUNA_IDTRAB]  := C9V->C9V_ID
			aTrab[nLinePos, COLUNA_CPFTRAB] := C9V->C9V_CPF
			aTrab[nLinePos, COLUNA_NOMTRAB] := C9V->C9V_NOME
			aTrab[nLinePos, COLUNA_MATRIC]  := C9V->C9V_MATRIC
			
			If C9V->C9V_NOMEVE == "S2300"
				cIdCateg := Posicione("CUU",1,xFilial("CUU") + C9V->C9V_ID + C9V->C9V_VERSAO + C9V->C9V_NOMEVE, "CUU_CATAV")
			Else
				cIdCateg := Posicione("CUP",4,xFilial("CUP") + C9V->C9V_ID + C9V->C9V_VERSAO + C9V->C9V_NOMEVE, "CUP_CODCAT")
			EndIf
			
			If !Empty(cIdCateg)
				aTrab[nLinePos, COLUNA_CATTRAB] := Posicione( "C87", 1, xFilial("C87") + cIdCateg, "C87_CODIGO" )
			EndIf
		EndIf
	EndIf
EndIf

RestArea( aArea )

Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} CriaBrwLot
Monta o Painel para solicita��o de guia por lota��o tribut�ria.

@author  Leandro F. Dourado
@since   28/01/2019
@version 12.1.23
/*/
//-------------------------------------------------------------------
Static Function CriaBrwEstab( oPanel, aTrab )
Local oBrwEstab     := Nil
Local oColumn       := Nil

Local aGuia         := {}

Local aEstab        := {}
Local aNewLine      := {Space(TamSX3("C92_ID")[1])    ,;
						Space(TamSX3("C92_NRINSC")[1]),;
						Space(TamSX3("C99_ID")[1])    ,;
						Space(TamSX3("C99_CODIGO")[1]),;
						.F.}

Local bCommit       := {|| CommitEstab(@aEstab,oBrwEstab,aTrab)}
Local bAddLine      := {|| AddLine(aEstab, oBrwEstab, aClone(aNewLine))}
Local bDelete       := {|| FDelLine(aEstab, oBrwEstab, aClone(aNewLine))}

Aadd( aEstab, aClone(aNewLine) )
		
DEFINE FWBROWSE oBrwEstab DATA ARRAY EDITCELL bCommit ARRAY aEstab NO SEEK NO CONFIG NO REPORT Of oPanel

ADD COLUMN oColumn DATA { || aEstab[oBrwEstab:At(),1] } 	Title "ID Estab."     TYPE "C" SIZE 06 PICTURE PesqPict("C92","C92_ID")      EDIT READVAR "CIDESTAB" Of oBrwEstab
oColumn:xF3 := "C92"
ADD COLUMN oColumn DATA { || aEstab[oBrwEstab:At(),2] } 	Title "Nr. Inscri��o" TYPE "C" SIZE 11 PICTURE PesqPict("C92","C92_NRINSC")                          Of oBrwEstab

ADD COLUMN oColumn DATA { || aEstab[oBrwEstab:At(),3] } 	Title "ID Lota��o"    TYPE "C" SIZE 30 PICTURE PesqPict("C99","C99_ID")      EDIT READVAR "CIDLOTAC" Of oBrwEstab
oColumn:xF3 := "C99"
ADD COLUMN oColumn DATA { || aEstab[oBrwEstab:At(),4] } 	Title "Cod. Lota��o"  TYPE "C" SIZE 30 PICTURE PesqPict("C99","C99_CODIGO")                          Of oBrwEstab

oBrwEstab:SetInsert( .T. )
oBrwEstab:SetAddline( bAddLine )

oBrwEstab:SetDelete( .T., bDelete )
oBrwEstab:SetDelOk( {||.T.} )

oBrwEstab:SetLineHeight( 20 )

oBrwEstab:SetChange({|| ChangeLine( @aEstab, @oBrwEstab, .T., @aGuia, @aTrab ) } )
//oBrwEstab:SetLineOk({|| LineOk( aEstab, oBrwEstab ) })

ACTIVATE FWBrowse oBrwEstab

Return oBrwEstab

//-------------------------------------------------------------------
/*/{Protheus.doc} CommitEstab
Valida o ID do trabalhador e gatilha as demais colunas da Grid.

@author  Leandro F. Dourado
@since   22/01/2019
@version 12.1.23
/*/
//-------------------------------------------------------------------
Static Function CommitEstab( aEstab, oBrwEstab, aTrab )
Local aArea    := GetArea()
Local cVar     := ReadVar()
Local cChvSeek := ""
Local lRet     := .T.
Local nLinePos := oBrwEstab:At()
Local nColPos  := oBrwEstab:ColPos()
Local cIdCateg := ""

If cVar == "CIDESTAB"
	If !Vazio()
		cChvSeek := Padr(&cVar, TamSX3( "C92_ID" )[1] )
		lRet     := ExistCpo( "C92" , cChvSeek , 1 )
	
		If lRet 
			aEstab[nLinePos, COLUNA_IDINSCR]  := &cVar
			aEstab[nLinePos, COLUNA_NRINSCR]  := Posicione("C92",1,xFilial("C92") + cChvSeek,"C92_NRINSC")
		EndIf
	EndIf
EndIf

If cVar == "CIDLOTAC"
	If !Vazio()
		cChvSeek := Padr( &cVar, TamSX3( "C99_ID" )[1] )
		lRet     := ExistCpo( "C99", cChvSeek, 1 )
	
		If lRet 
			aEstab[nLinePos, COLUNA_IDLOTAC]  := &cVar
			aEstab[nLinePos, COLUNA_CODLOTA]  := Posicione("C99",1,xFilial("C99") + cChvSeek,"C99_CODIGO")
		EndIf
	EndIf
EndIf

RestArea( aArea )

Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} ChangeLine
Configura��o da a��o de troca de linhas.

@author  Leandro F. Dourado
@since   22/01/2019
@version 12.1.23
/*/
//-------------------------------------------------------------------
Static Function ChangeLine( aItens, oBrowse, lEstab, aGuia, aTrab )
Local lRet      := .T.
Local nLenBrw   := oBrowse:LogicLen()
Local nMvType   := oBrowse:nMoveType
Local nAt       := oBrowse:At()
Local oData     := oBrowse:Data()
Local lDelLine  := .F.

Local aNewLinTrab := {Space(TamSX3("C9V_ID")[1]),;
					  Space(TamSX3("C9V_CPF")[1]),;
					  Space(TamSX3("C9V_NOME")[1]),;
					  Space(TamSX3("C9V_MATRIC")[1]),;
					  Space(TamSX3("CUP_CODCAT")[1]),;
					  .F.}

Default lEstab := .F.
Default aGuia  := {}
Default aTrab  := {}

If     nMvType == GRID_MOVEUP
	
	If lEstab
		If Len(aItens) > 1 .And. Empty(aItens[nAt+1,COLUNA_IDINSCR]) .And. Empty(aItens[nAt+1,COLUNA_IDLOTAC])
			lDelLine := .T.
		EndIf
		
		If Len(aGuia) > 0
			
			If !lDelLine
				If Len(aGuia) < nAt+1
					aAdd(aGuia, { aItens[nAt+1], aTrab })
				Else
					aGuia[nAt+1,1] := aItens[nAt+1]
					aGuia[nAt+1,2] := aTrab
				EndIf
			EndIf
			
			// Fa�o a restaura��o das grids filhas do grid de estabelecimento/lota��o
			aTrab := aClone(aGuia[nAt,2])
			oBrowse:aBrwRelation[1]:Data():aArray := aClone(aTrab)
			oBrowse:aBrwRelation[1]:Refresh(.T.)
			
		EndIf
		
	Else
		If Len(aItens) > 1 .And. Empty(aItens[nAt+1,COLUNA_IDTRAB]) 
			lDelLine := .T.
		EndIf
	EndIf
	
	// Caso a grid seja movimentada para a linha de cima em uma linha em branco, essa linha ser� removida.
	If lDelLine
		aDel(aItens,nAt+1)
		aSize(aItens,nAt)
		oBrowse:Data():aArray := aClone(aItens)
		oBrowse:Refresh(.F.)
	EndIf
	
Elseif nMvType == GRID_MOVEDOWN
	
	If lEstab
	
		If Len(aGuia) < nAt-1
			aAdd( aGuia, { aItens[nAt-1], aClone(aTrab) } )
		Else
			aGuia[nAt-1,1] := aItens[nAt-1]
			aGuia[nAt-1,2] := aClone(aTrab)
		EndIf
		
		If Len(aGuia) == nAt
			aTrab := aClone(aGuia[nAt,2])
		Else
			aTrab := {}
			aAdd( aTrab, aClone(aNewLinTrab) )
		EndIf
		
		oBrowse:aBrwRelation[1]:Data():aArray := aClone(aTrab)
		oBrowse:aBrwRelation[1]:Refresh(.T.)
		
	Else
	
		If Empty(aItens[nAt-1,COLUNA_IDTRAB])
			lRet := .F.
			aDel(aItens,nAt)
			aSize(aItens,nAt-1)
			oBrowse:Data():aArray := aClone(aItens)
			oBrowse:Refresh(.F.)
		EndIf
	
	EndIf

EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} AddLine
Bloco executado na adi��o de linhas

@author  Leandro F. Dourado
@since   22/01/2019
@version 12.1.23
/*/
//-------------------------------------------------------------------
Static Function AddLine( aItens, oBrowse, aNewLine )

Aadd(aItens, aNewLine)
oBrowse:GoColumn(1)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} DelLine
Bloco executado na adi��o de linhas

@author  Leandro F. Dourado
@since   22/01/2019
@version 12.1.23
/*/
//-------------------------------------------------------------------
Static Function FDelLine( aItens, oBrowse,aNewLine )

aItens[oBrowse:At()] := aClone(aNewLine)
oBrowse:Data():aArray[oBrowse:At()] := aClone(aItens[oBrowse:At()])
oBrowse:Refresh(.F.)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} PnlEmitGuia
Cria o painel para a sele��o do tipo de guia que se deseja solicitar.

@author  Leandro F. Dourado
@since   16/01/2019
@version 12.1.23
/*/
//-------------------------------------------------------------------
Static Function PnlEmitGuia( oWizard )
Local oPanel        := Nil
//Local oFontSay      := TFont():New( "Times New Roman",, -12 ,, .F.  )

Local cCompetencia  := Space(7)
Local cGuia         := Space(20)
Local cTpGuia       := Space(1)
Local cFormato      := Space(1)
Local cOrdenacao    := Space(1)

Local nLinLabel     := 10
Local nLinGet       := 20
Local nColuna       := 15
Local nAltLinha	    := 10	
Local nLargLinha    := 60
Local nX            := 1

Local aInfo         := { 0, 0, LARGURA_TEXTOS, ALTURA_PAINEL, 30, 10 , 10, 15 }
Local aObjects      := {}
Local aPosObj       := {}

lGoFirst    := .T.

oPanel		:=�	TPanel():New( 0, 0, , oWizard:oMPanel[ 4 ],, .F., .F.,,, LARGURA_PAINEL, ALTURA_PAINEL, .T., .F. )

For nX := 1 To 5 // quantidade total de Gets, sendo que todos possuem o mesmo tamanho
	AADD(aObjects,{nLargLinha,nAltLinha,.F.,.F.,.F.})
Next nX

aPosObj := MsObjSize( aInfo, aObjects, .F., .T.)

TSay():New(5, aPosObj[1,2], {||  TAFFGTSTxt("TITULO","Emiss�o de Guia Regular")   }, oPanel,,/*oFontSay*/,,,,.T.,,,LARGURA_TEXTOS,20,,,,,,.T.)

TSay():New(aPosObj[1,1]+15, aPosObj[1,2], {||  TAFFGTSTxt( "TXTGET", "Compet�ncia")   }, oPanel,,/*oFontSay*/,,,,.T.,,,nLargLinha,nAltLinha,,,,,,.T.)
TGet():New(aPosObj[1,1]+27, aPosObj[1,2], {|u| if( PCount()>0, cCompetencia := u, cCompetencia )}, oPanel, nLargLinha, nAltLinha, "99/9999",, 0,,,,,.T.,,,,,,,.F., .F.,, cCompetencia )

nColuna   += 90

TSay():New(aPosObj[2,1]+15, aPosObj[2,2], {||  TAFFGTSTxt( "TXTGET", "N�mero da Guia")   }, oPanel,,/*oFontSay*/,,,,.T.,,,nLargLinha,nAltLinha,,,,,,.T.)
TGet():New(aPosObj[2,1]+27, aPosObj[2,2], {|u| if( PCount()>0, cGuia := u, cGuia )}, oPanel, nLargLinha, nAltLinha, "@!",, 0,,,,,.T.,,,,,,,.F., .F.,, cGuia )

nColuna   += 90

TSay():New(aPosObj[3,1]+15, aPosObj[3,2], {||  TAFFGTSTxt( "TXTGET", "Tipo de Guia")   }, oPanel,,/*oFontSay*/,,,,.T.,,,nLargLinha,nAltLinha,,,,,,.T.)
TCombobox():New(aPosObj[3,1]+27,aPosObj[3,2],{|u| if( PCount()>0, cTpGuia := u, cTpGuia )},{"1=Guia","2=Detalhe da Guia","3=Rel. Trabalhadores"},nLargLinha,nAltLinha,oPanel,,,,,,.T.,,,,)

nColuna   += 90

TSay():New(aPosObj[4,1]+15, aPosObj[4,2], {||  TAFFGTSTxt( "TXTGET", "Formato")   }, oPanel,,/*oFontSay*/,,,,.T.,,,nLargLinha,nAltLinha,,,,,,.T.)
TCombobox():New(aPosObj[4,1]+27,aPosObj[4,2],{|u| if( PCount()>0, cFormato := u, cFormato )},{"1=PDF","2=CSV","3=XML"},nLargLinha,nAltLinha,oPanel,,,,,,.T.,,,,)

nColuna   += 90

TSay():New(aPosObj[5,1]+15, aPosObj[5,2], {||  TAFFGTSTxt( "TXTGET", "Ordena��o")   }, oPanel,,/*oFontSay*/,,,,.T.,,,nLargLinha,nAltLinha,,,,,,.T.)
TCombobox():New(aPosObj[5,1]+27,aPosObj[5,2],{|u| if( PCount()>0, cOrdenacao := u, cOrdenacao )},{"","1=Lista","2=Estabelecimento","3=Lota��o"},nLargLinha,nAltLinha,oPanel,,,,,,.T.,,,,)

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} SelPrevPanel
Ao selecionar o bot�o voltar no �ltimo painel do Wizard, essa fun��o ir� indicar qual o painel que dever� retornar

@author  Leandro F. Dourado
@since   04/10/2018
@version 1
/*/
//-------------------------------------------------------------------
Static Function SelPrevPanel( oWizard )

If lGoFirst

	oWizard:SetPanel( oWizard:nPanel - 1 )
	lGoFirst := .F.
	
EndIf

Return .T.

