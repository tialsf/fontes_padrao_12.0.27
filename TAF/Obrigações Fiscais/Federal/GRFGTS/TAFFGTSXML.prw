#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFFGTSXML
@type			function
@description	Fun��o principal do processo de Gera��o do XML para
                Recolhimento do FGTS.
@author			Veronica Toledo
@since			01/02/2019
@Param          nGuia   Op��o da guia de recolhimento
                aParam  Par�metros utilizados na gera��o da guia de
                        recolhimento
/*/
//---------------------------------------------------------------------
Function TAFFGTSXML(nGuia,aParam)
Local cXml      := ''
Local cTpGuia   := ''
Local cComp     := ''
Local cOpera    := ''
Local cCPF      := ''
Local cTpFormat := ''

Default nGuia   := 0
Default aParam  := {}

// Gera XML da guia solicitada
Do Case
    Case nGuia == 1 // Solicitar Guia Regular FGTS Folha de Pagamento
        cXml := TAFXmlGRCC(aParam,@cComp,@cTpGuia,@cOpera)

    Case nGuia == 2 // Emitir Guia Regular
        cXml := TAFXMLGREG(aParam, @cComp,,,@cTpguia, @cTpFormat)
EndCase

    TAFGRGrv( nGuia, cComp, cOpera, cTpGuia, cCPF, cXml, cTpFormat )

Return

/*---------------------------------------------------------------------
{Protheus.doc} TAFGRGrv()

@type			Static Function
@description	Grava��o do Xml da GRFGTS
@author			Veronica Toledo/Eduardo Sukeda
@since			01/02/2019
---------------------------------------------------------------------*/
Static Function TAFGRGrv( nOpc, cComp, cOpera, cTpGuia, cCPF, cXml, cTpFormat )

Local oModel    := FWLoadModel('TAFA527')
Local cStatus       := ''

Default nOpc        := 0
Default cComp       := ''
Default cOpera      := ''
Default cTpGuia     := ''
Default cCPF        := ''
Default cXml        := ''
Default cTpFormat   := '1'
Default cCPF        := ''

oModel:SetOperation(MODEL_OPERATION_INSERT)
oModel:Activate()

//oModel:LoadValue( "MODEL_V3A", "V3A_FILIAL"	    , xFilial('V3A')      )
oModel:LoadValue( "MODEL_V3A", "V3A_ID"	            , TafGeraID('TAF')    )
//oModel:LoadValue( "MODEL_V3A", "V3A_COMPET"	    , StrTran(cComp,'/')  )
//oModel:LoadValue( "MODEL_V3A", "V3A_OPERA"	    , cOpera              )
//oModel:LoadValue( "MODEL_V3A", "V3A_TPGUIA"	    , cTpGuia             )
//oModel:LoadValue( "MODEL_V3A", "V3A_CPF"	        , cCPF                )
//oModel:LoadValue( "MODEL_V3A", "V3A_STATUS"	    , cStatus             )
//oModel:LoadValue( "MODEL_V3A", "V3A_XMLENV"	    , cXml                )
//oModel:LoadValue( "MODEL_V3A", "V3A_DTREQ"	    , Date()              )

If oModel:VldData()
    oModel:CommitData()
EndIf

Return

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFGRCab
@type			function
@description	Fun��o respons�vel pela gera��o do cabe�alho do xml 
@author			Veronica Toledo
@since			01/02/2019
/*/
//---------------------------------------------------------------------
Static Function TAFGRCab(cXml, cNomeServ)

Local cXmlCab       := ''
Local cCGC          := AllTrim(SM0->M0_CGC)

Local cPathURLES    := SuperGetMv('MV_TAFFGES',.F.,"http://schemas.xmlsoap.org/soap/envelope/")
Local cServico      := SuperGetMv('MV_TAFFGSE',.F.,"http://www.siies.caixa.gov.br/servicos")

Local cRepreLeg     := 'true'

Default cXml        := ''
Default cNomeServ   := ''

cXmlCab +=  '<soapenv:Envelope xmlns:soapenv="' + AllTrim(cPathURLES) + '" '
cXmlCab +=                  'xmlns:ser="' + AllTrim(cServico) + '">'
cXmlCab +=      '<soapenv:Header>'
cXmlCab +=          '<requestGuiaHeader>'
cXmlCab +=              '<RL>' + cRepreLeg + '</RL>'
cXmlCab +=              '<AS>' + cCGC + '</AS>'
cXmlCab +=              '<SR>' + cNomeServ + '</SR>'
cXmlCab +=              '<SIS>FUG</SIS>'
cXmlCab +=          '</requestGuiaHeader>'
cXmlCab +=      '</soapenv:Header>'

cXmlCab += cXml

cXmlCab += '</soapenv:Envelope>'


Return cXmlCab

//---------------------------------------------------------------------
/*/{Protheus.doc} TAFXmlGRCC
@type			function
@description	Fun��o respons�vel pela gera��o do xml para solicita��o
                da guia de regular consolidada por compet�ncia
@author			Veronica Toledo
@since			01/02/2019
@Param          nGuia   Op��o da guia de recolhimento
                aParam  Par�metros utilizados na gera��o da guia de
                        recolhimento
/*/
//---------------------------------------------------------------------
Function TAFXmlGRCC(aParam,cComp,cTpGuia,cOpera)
Local cXml          := ''
Local cDtRec        := ''
Local cVersao       := SuperGetMv('MV_TAFFGVE',.F.,"v2.0.0")
Local cNomeServ     := 'Solicitar Guia Regular FGTS Folha de Pagamento'

Default aParam      := {}
Default cComp       := ''
Default cTpGuia     := ''
Default cOpera      := ''


If Len(aParam) > 1

    cTpGuia         := '1' //1=Emiss�o de Guia;2=Detalhe da Guia;3=Rela��o de Trabalhadores
    cComp           := aParam[1]
    cDtRec          := aParam[2]

    cXml += '<soapenv:Body>'
    cXml +=     '<ser:arquivo>'
    cXml +=         '<solicitaGuiaRegular xmlns="http://caixa.gov.br/request/solicitaGuiaRegularXmlRequest"'
    cXml +=                              'xmlns:xsi="thhp://www.w3.org/2001/XMLSchema-instance"'
    cXml +=                              'xsi:schemaLocation="http://caixa.gov.br/request/solicitaGuiaRegularXmlRequest SolicitacaoGuiaRegular_' + cVersao + '.xsd">'
    cXml +=             '<guiaRegular>'
    cXml +=                 '<competencia>' + cComp + '</competencia>'

    If !Empty(StrTran(cDtRec,'/',''))
        cXml +=             '<dataRecolhimento>' + cDtRec + '</dataRecolhimento>'
    EndIf

    cXml +=             '</guiaRegular>'
    cXml +=         '</solicitaGuiaRegular>
    cXml +=     '</ser:arquivo>'
    cXml += '</soapenv:Body>'
    
    
    cXml := TAFGRCab(cXml,cNomeServ)
EndIf

Return cXml

/*---------------------------------------------------------------------
{Protheus.doc} TAFXMLGREG

@type			Static Function
@description	Gera��o do Xml de Emiss�o de Guia Regular
@author			Eduardo Sukeda
@since			01/02/2019
---------------------------------------------------------------------*/
Static Function TAFXMLGREG(aParam)

Local cXml          := ""
Local cTpGuia       := "1"
Local cNrInscr      := AllTrim(SM0->M0_CGC)
Local cTpInscr      := IIf(Len(cNrInscr) == 14,"1","2")
Local cComp         := ""
Local cNrGuia       := ""
Local cTpFormat     := ""
Local cOrdena       := ""
Local cNomeServ     := 'Emitir Guia Regular FGTS Folha de Pagamento'

If Len(aParam) > 1

    StrTran(cComp, "/")  := aParam[1]
    cNrGuia              := aParam[2]
    cTpFormat            := aParam[3]
    cOrdena              := aParam[4]

    cXml += "<soapenv:Body>"
    cXml +=     "<ser:arquivo>"
    cXml +=         "<guiaRegular>"
    cXml +=             "<tipoInscricao>" + cTpInscr + "</tipoInscricao>"
    cXml +=             "<inscricao>" + cNrInscr + "</inscricao>"

    If !Empty(StrTran(cComp, "/"))
        cXml +=         "<competencia>" + cComp + "</competencia>"
    ElseIf !Empty(cNrGuia)
        cXml +=         "<numeroGuia>" + cNrGuia + "</numeroGuia>"
    EndIf

    cXml +=             "<tipoDocumento>"
    cXml +=                 "<tipo>" + cTpGuia + "</tipo>"
    cXml +=                 "<formato>" + cTpFormat + "</formato>"
    cXml +=                 "<ordenacao>" + cOrdena + "</ordenacao>"
    cXml +=             "</tipoDocumento>"
    cXml +=         "</guiaRegular>"
    cXml +=     "</ser:arquivo>"
    cXml += "</soapenv:Body>"

    cXml := TAFGRCab( cXml,cNomeServ )

EndIf

Return ( cXml )