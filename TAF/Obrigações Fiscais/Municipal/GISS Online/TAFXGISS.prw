#Include 'Protheus.ch'
#Include "ApWizard.ch"
#Include "TAFXGISS.ch"

#Define cObrig "GISS Online"

//--------------------------------------------------------------------------
/*/{Protheus.doc} TAFXGISS

Esta rotina tem como objetivo a geracao do Arquivo GISS Online

@Author francisco.nunes
@Since 01/02/2017
@Version 1.0
/*/
//---------------------------------------------------------------------------
Function TAFXGISS()

Local cNomWiz    as char
Local lEnd       as logical
Local cFunction	 as char
Local nOpc       as numeric

Private oProcess as object
Private dInicial as date
Private dFinal   as date
Private lCancel  as logical
Private lBrwServ as logical

	cNomWiz		:= cObrig + FWGETCODFILIAL
	lEnd		:= .F.	
	lCancel		:= .F.
	oProcess	:= Nil	
   	cFunction 	:= ProcName()
	nOpc      	:= 2 //View

	//Protect Data / Log de acesso / Central de Obrigacoes
	Iif(FindFunction('FwPDLogUser'),FwPDLogUser(cFunction, nOpc), )

	//Cria objeto de controle do processamento
	oProcess := TAFProgress():New( { |lEnd| ProcGISSON( @lEnd, @oProcess, cNomWiz ) }, STR0001 )
	oProcess:Activate()

	If !lCancel
		If (MSGYESNO( STR0037, STR0006)) //Deseja gerar o relat�rio de confer�ncia?
			TGISSRelat()
		EndIf
	EndIf

	//Limpando a mem�ria
	DelClassIntf()

Return()

//--------------------------------------------------------------------------
/*/{Protheus.doc} ProcGISSON

Inicia o processamento para geracao da Giss Online


@Param lEnd      -> Verifica se a operacao foi abortada pelo usuario
	   oProcess  -> Objeto da barra de progresso da emissao da GISS Online
	   cNomWiz   -> Nome da Wizard criada para a GISS Online


@Return ( Nil )

@Author francisco.nunes
@Since 01/02/2017
@Version 1.0
/*/
//---------------------------------------------------------------------------


Static Function ProcGISSON( lEnd as logical, oProcess as object, cNomWiz as char)

Local dIni        as date
Local dFim        as date
Local cErrorGISS  as char
Local cErrorTrd   as char
Local cYearGiss	  as char
Local nX          as numeric
Local nPos        as numeric
Local nProgress1  as numeric
Local nCont       as numeric
Local aWizard     as array
Local lProc       as logical

Private lBrwNat  as logical
Private lBrwIte  as logical
Private lBrwPar  as logical
	
	cErrorGISS  := ""
	cErrorTrd   := ""
	cYearGiss	:= ""
	lBrwNat     := .F.
	lBrwIte     := .F.
	lBrwPar     := .F.
	
	nX          := 0
	nPos        := 0
	nProgress1  := 0 
	
	aWizard     := {}
	
	lProc       := .T.
	nCont       := 1
	
	//Carrega informa��es na wizard
	If !xFunLoadProf( cNomWiz , @aWizard )
		Return( Nil )
	EndIf

	cYearGiss := Iif(ValType(aWizard[1][4])=="N",LTRIM(STR(aWizard[1][4])),AllTrim(aWizard[1][4]))

	dInicial 	:= CTOD("01/"+Substr(aWizard[1][3],1,2)+"/"+cYearGiss)
	dFinal 	:= LastDay(dInicial)
	
	If TAFConsServ(dInicial, dFinal)
		If (MSGYESNO( STR0061, STR0006 )) //Para o per�odo selecionado existem documentos fiscais com c�digo de servi�o n�o informado. Esses documentos n�o ser�o considerados. Deseja inform�-los?     Aten��o!
			If !TAFCadAdic(dIni, dFim)
			 	lProc := MSGYESNO( STR0062, STR0006 ) //"Deseja gerar a obriga��o sem finalizar o cadastro de c�digos de servi�o?"	   Aten��o!	 	 
			EndIf
		EndIF	
	EndIf
				
	If lProc
	
		Conout( "Tempo de Inicio " + Time() )
	
		//Alimentando a vari�vel de controle da barra de status do processamento
		nProgress1 := 2
		oProcess:Set1Progress( nProgress1 )
	
		//Iniciando o Processamento
		oProcess:Inc1Progress( STR0002 ) //"Preparando o Ambiente..."
		oProcess:Inc1Progress( STR0003 ) //"Executando o Processamento..."		
		
		//************************************************************************
		//Gera��o GISS Online - Aqui v�o a chamada das fun��es da gera��o dos registros
		TAFCabGISS()          // Imprimir Cabe�alho
		TAFGISSNF(aWizard)    // Processamento dos Documentos Fiscais					
		//************************************************************************
		//********* FIM da chamada das fun��es da gera��o dos registros **********
		//************************************************************************		
	Else
		oProcess:Inc1Progress( STR0004 )	//"Processamento cancelado"
		oProcess:Inc2Progress( STR0005 )	//"Clique em Finalizar"
		oProcess:nCancel = 1
	EndIf
	
	//Tratamento para quando o processamento tem problemas
	If oProcess:nCancel == 1 .or. !Empty( cErrorGISS ) .or. !Empty( cErrorTrd )
	
		//Cancelado o processamento
		If oProcess:nCancel == 1
	
			Aviso( STR0006, STR0007, { STR0008 } ) 	//"Aten��o" "A gera��o do arquivo foi cancelada com sucesso!" "Sair"
			lCancel := .T.
	
		//Erro na inicializa��o das threads
		ElseIf !Empty( cErrorTrd )
	
			Aviso( STR0006, cErrorTrd, { STR0008 } ) //"Aten��o"    "Sair"
	
		//Erro na execu��o dos Blocos
		Else
	
			cErrorGISS := STR0009	                      //"Ocorreu um erro fatal durante a gera��o do(s) Registro(s) da GISS Online"
			cErrorGISS += STR0010 + Chr( 10 ) + Chr( 10 )	 //"Favor efetuar o reprecessamento da GISS Online, caso o erro persista entre em contato com o administrador de sistemas / suporte TOTVS"			
	
			Aviso( STR0006, cErrorGISS, { STR0008 } )
	
		EndIf
	
	Else
	
		//Atualizando a barra de processamento
		oProcess:Inc1Progress( STR0011 )	//"Informa��es processadas"		
		oProcess:Inc2Progress( STR0012 )	//"Consolidando as informa��es e gerando arquivo..."		
	
		If GerArqCons( aWizard )
			Conout( "Tempo Final " + Time() )
			
			//Atualizando a barra de processamento
			oProcess:Inc2Progress( STR0013 ) //"Arquivo gerado com sucesso."			
			msginfo(STR0013) 					 //"Arquivo gerado com sucesso."
		Else
			oProcess:Inc2Progress( STR0014 ) 	//"Falha na gera��o do arquivo."			
		EndIf	
	EndIf

Return()

//-------------------------------------------------------------------
/*/{Protheus.doc} getObrigParam

@Author francisco.nunes
@Since 01/02/2016
@Version 1.0

/*/
//-------------------------------------------------------------------
Static Function getObrigParam()

Local	cNomWiz	as char
Local 	cNomeAnt 	as char
Local	cTitObj1	as char
Local	aTxtApre	as array
Local	aPaineis	as array	
Local	aItens1	as array
Local	aRet		as array
	
	cNomWiz	:= cObrig + FWGETCODFILIAL
	cNomeAnt 	:= ""
	aTxtApre	:= {}
	aPaineis	:= {}	
	aItens1	:= {}	
	cTitObj1	:= ""
	aRet		:= {}
	
	aAdd (aTxtApre, STR0015)	//"Processando Empresa."	
	aAdd (aTxtApre, "")
	aAdd (aTxtApre, STR0016)	//"Preencha corretamente as informa��es solicitadas."	
	aAdd (aTxtApre, STR0017)	//"Informa��es necess�rias para a gera��o do meio-magn�tico GISS Online."	

	//============= Painel 0 ==============

	aAdd (aPaineis, {})
	nPos := Len (aPaineis)
	aAdd (aPaineis[nPos], STR0016)	//"Preencha corretamente as informa��es solicitadas."	
	aAdd (aPaineis[nPos], STR0017)	//"Informa��es necess�rias para a gera��o do meio-magn�tico GISS Online."
	aAdd (aPaineis[nPos], {})
																
	//LINHA 1---------------------------------------------------------------------------------------------//
	cTitObj1 := STR0018 //"Diret�rio do Arquivo Destino"	                 
	cTitObj2 := STR0019 //"Nome do Arquivo Destino"

	aAdd( aPaineis[nPos,3], { 1, cTitObj1,,,,,,} )
	aAdd (aPaineis[nPos,3], { 1, cTitObj2,,,,,,} )

	cTitObj1 := Replicate( "X", 50 )
	cTitObj2 := Replicate( "X", 20 )

	aAdd( aPaineis[nPos,3], { 2,, cTitObj1, 1,,,, 50,,,, { "xValWizCmp", 3 , { "", "" } } } ) //Valida campo: "Diret�rio do Arquivo Destino" no Bot�o Avan�ar
	aAdd( aPaineis[nPos,3], { 2,, cTitObj2, 1,,,, 20,,,, { "xValWizCmp", 4 , { "", "" } } } ) //Valida campo: "Nome do Arquivo Destino" no Bot�o Avan�ar

	aAdd (aPaineis[nPos][3], {0,"",,,,,,})					
	aAdd (aPaineis[nPos][3], {0,"",,,,,,}) //Pula Linha
   
   //LINHA 2---------------------------------------------------------------------------------------------//

	cTitObj1	:=	STR0020   //"M�s Refer�ncia"
	cTitObj2	:=	STR0021   //"Ano Refer�ncia"

	aAdd( aPaineis[nPos,3], { 1, cTitObj1,,,,,,} )
	aAdd( aPaineis[nPos,3], { 1, cTitObj2,,,,,,} )

	aAdd (aItens1, STR0022)     //"01 - Janeiro"
	aAdd (aItens1, STR0023)     //"02 - Fevereiro"
	aAdd (aItens1, STR0024)     //"03 - Mar�o"
	aAdd (aItens1, STR0025)     //"04 - Abril"
	aAdd (aItens1, STR0026)     //"05 - Maio"
	aAdd (aItens1, STR0027)     //"06 - Junho"
	aAdd (aItens1, STR0028)     //"07 - Julho"
	aAdd (aItens1, STR0029)     //"08 - Agosto"
	aAdd (aItens1, STR0030)     //"09 - Setembro"
	aAdd (aItens1, STR0031)     //"10 - Outubro"
	aAdd (aItens1, STR0032)     //"11 - Novembro"
	aAdd (aItens1, STR0033)     //"12 - Dezembro"

	cTitObj2 :=	"@E 9999"

	aAdd (aPaineis[nPos,3], {3,,,,,aItens1,,,,,}) 
	aAdd( aPaineis[nPos,3], {2,,cTitObj2,2,0,,,4})

	aAdd (aPaineis[nPos][3], {0,"",,,,,,})					
	aAdd (aPaineis[nPos][3], {0,"",,,,,,}) //Pula Linha
   
   //--------------------------------------------------------------------------------------------------//

	aAdd(aRet, aTxtApre)
	aAdd(aRet, aPaineis)
	aAdd(aRet, cNomWiz)
	aAdd(aRet, cNomeAnt)
	aAdd(aRet, Nil )
	aAdd(aRet, Nil )
	aAdd(aRet, { || TAFXGISS() } )

Return (aRet)

//---------------------------------------------------------------------
/*/{Protheus.doc} GerTxtGISS

Geracao do Arquivo TXT da GISS Online.
Gera o arquivo de cada registros.

@Param cStrTxt -> Alias da tabela de informacoes geradas pelo GISS Online
        lCons   -> Gera o arquivo consolidado ou apenas o TXT de um registro

@Return ( Nil )

@Author francisco.nunes
@Since 01/02/2016
@Version 1.0
/*/
//---------------------------------------------------------------------
Function GerTxtGISS( nHandle as numeric, cTXTSys as char, cReg as char)

Local cDirName	as char
Local cFileDest	as char
Local lRetDir		as logical
Local lRet			as logical

	cDirName	:=	TAFGetPath( "2" , "GISS Online" )
	cFileDest	:=	""
	lRetDir	:= .T.
	lRet		:= .T.
	
	//Verifica se o diretorio de gravacao dos arquivos existe no RoothPath e cria se necessario
	if !File( cDirName )
	
		lRetDir := FWMakeDir( cDirName )
	
		if !lRetDir
	
			cDirName	:=	""
	
			Help( ,,"CRIADIR",, STR0036 + cValToChar( FError() ) , 1, 0 )   //"N�o foi poss�vel criar o diret�rio \Obrigacoes_TAF\GISS Online. Erro:"
	
			lRet	:=	.F.
	
		endIf
	
	endIf
	
	if lRet
	
		//Tratamento para Linux onde a barra � invertida
		If GetRemoteType() == 2
			If !Empty( cDirName ) .and. ( SubStr( cDirName, Len( cDirName ), 1 ) <> "/" )
				cDirName += "/"
			EndIf
		Else
			If !Empty( cDirName ) .and. ( SubStr( cDirName, Len( cDirName ), 1 ) <> "\" )
				cDirName += "\"
			EndIf
		EndIf
	
		//Monto nome do arquivo que ser� gerado
		cFileDest := AllTrim( cDirName ) + cReg
	
		If Upper( Right( AllTrim( cFileDest ), 4 ) ) <> ".TXT"
			cFileDest := cFileDest + ".TXT"
		EndIf
	
		lRet := SaveTxt( nHandle, cTxtSys, cFileDest )
	
	endif

Return( lRet )
//---------------------------------------------------------------------
/*/{Protheus.doc} GerArqCons

Geracao do Arquivo .DAT da GISS Online. Gera o arquivo dos registros e arquivo
consolidado

@Return ( Nil )

@Author francisco.nunes
@Since 01/02/2016
@Version 1.0
/*/
//---------------------------------------------------------------------
Static Function GerArqCons( aWizard as array )

Local cFileDest  	as char
Local cPathTxt	as char
Local cStrTxtFIM	as char
Local cTxtSys		as char
Local nx			as numeric
Local nHandle		as numeric
Local aFiles		as array

	cFileDest	:=	Alltrim( aWizard[1][1] ) 		  //diretorio onde vai ser gerado o arquivo consolidado
	cPathTxt	:=	TAFGetPath( "2" , "GISS Online" )//diretorio onde foram gerados os arquivos txt temporarios
	nx			:=	0
	cTxtSys	:=	CriaTrab( , .F. ) + ".DAT"
	nHandle	:=	MsFCreate( cTxtSys )
	aFiles		:=	{}
	cStrTxtFIM	:= ""

	cNomeArq	:= aWizard[1,2]

	//Tratamento para Linux onde a barra � invertida
	If GetRemoteType() == 2
		If !Empty( cPathTxt ) .and. ( SubStr( cPathTxt, Len( cPathTxt ), 1 ) <> "/" )
			cPathTxt += "/"
		EndIf
		//Verifica o se Diret�rio foi digitado sem a barra final e incrementa a barra + nome do arquivo
		If !Empty( cFileDest ) .and. ( SubStr( cFileDest, Len( cFileDest ), 1 ) <> "/" )
			cFileDest += "/"
			cFileDest += Alltrim(cNomeArq) //Incrementa o nome do arquivo de gera��o
		elseIf !Empty( cFileDest ) .and. ( SubStr( cFileDest, Len( cFileDest ), 1 ) = "/" )
			cFileDest += Alltrim(cNomeArq) //Incrementa o nome do arquivo de gera��o
		EndIf
	Else
		If !Empty( cPathTxt ) .and. ( SubStr( cPathTxt, Len( cPathTxt ), 1 ) <> "\" )
			cPathTxt += "\"
		EndIf
		//Verifica o se Diret�rio foi digitado sem a barra final e incrementa a barra + nome do arquivo
		If !Empty( cFileDest ) .and. ( SubStr( cFileDest, Len( cFileDest ), 1 ) <> "\" )
			cFileDest += "\"
			cFileDest += Alltrim(cNomeArq) //Incrementa o nome do arquivo de gera��o
		elseIf !Empty( cFileDest ) .and. ( SubStr( cFileDest, Len( cFileDest ), 1 ) = "\" )
			cFileDest += Alltrim(cNomeArq) //Incrementa o nome do arquivo de gera��o
		EndIf
	EndIf

	aFiles := GISSFilesTxt(cPathTxt)
	for nx := 1 to Len( aFiles )

		//Verifica se o arquivo foi encontrado no diretorio
		if File( aFiles[nx][1] )

			FT_FUSE( aFiles[nx][1] )	//ABRIR
			FT_FGOTOP()				//POSICIONO NO TOPO

			while !FT_FEOF()
	   			cBuffer := FT_FREADLN()
	 			cStrTxtFIM += cBuffer + CRLF
				FT_FSKIP()
			endDo
			FT_FUSE()
			FERASE( aFiles[nx][1] )			
		endif
	next

	If Upper( Right( AllTrim( cFileDest ), 4 ) ) <> ".DAT"
		cFileDest := cFileDest + ".dat"
	EndIf

	WrtStrTxt( nHandle, cStrTxtFIM )

	lRet := SaveTxt( nHandle, cTxtSys, cFileDest )

Return( lRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} GISSFilesTxt

GISSFilesTxt() - Arquivos por bloco da GISS Online

@Author francisco.nunes
@Since 20/12/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
static function GISSFilesTxt( cPathTxt as char )

Local aRet	as array

	aRet	:=	{}
	
	AADD(aRet,{cPathTxt + "CABECALHO.TXT"}) // Cabe�alho da GISS Online	
	AADD(aRet,{cPathTxt + "MOV_NF.TXT"})    // Registro de Documentos Fiscais	
	
Return( aRet )

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFCabGISS

TAFCabGISS() - Imprime o Cabe�alho da GISS Online
 
@Author Francisco Kennedy Nunes Pinheiro
@Since 01/02/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function TAFCabGISS( )
	
	Local cTxtSys := CriaTrab( , .F. ) + ".TXT"
	Local nHandle := MsFCreate( cTxtSys )
	
	Local cStrTxt	:= ""
		
	cStrTxt += "CD_INDICADOR||"               // Indicador de Registro
	cStrTxt += "NR_LAYOUT||"                  // Indicador do Tipo do Layout
	cStrTxt += "DT_EMISSAO_NF||"              // Data da Presta��o do Servi�o
	cStrTxt += "NR_DOC_NF_INICIAL||"          // N�mero da Nota Fiscal Inicial
	cStrTxt += "NR_DOC_NF_SERIE||"            // S�rie da Nota Fiscal
	cStrTxt += "NR_DOC_NF_FINAL||"				// N�mero da Nota Fiscal Final
	cStrTxt += "TP_DOC_NF||"                  // Tipo da Nota Fiscal
	cStrTxt += "VL_DOC_NF||"                  // Valor da Nota Fiscal
	cStrTxt += "VL_BASE_CALCULO||"		       // Base de C�lculo
	cStrTxt += "CD_ATIVIDADE||"               // Atividade ou Servi�o prestado
	cStrTxt += "CD_PREST_TOM_ESTABELECIDO||"  // Prestador/Tomador estabelecido no munic�pio
	cStrTxt += "CD_LOCAL_PRESTACAO||"         // Local de presta��o do servi�o
	cStrTxt += "NM_RAZAO_SOCIAL||"            // Raz�o social do Prestador/Tomador
	cStrTxt += "NR_CNPJ_CPF||"	              // CNPJ/CPF
	cStrTxt += "CD_TIPO_CADASTRO||"           // Tipo de cadastro (Pessoa F�sica ou Pessoa Jur�dica)
	cStrTxt += "NR_INSCRICAO_MUNICIPAL||"     // Inscri��o municipal
	cStrTxt += "NM_INSCRICAO_MUNICIPAL_DV||"  // D�gito da inscri��o municipal
	cStrTxt += "NR_INSCRICAO_ESTADUAL||"      // Inscri��o estadual
	cStrTxt += "NM_TIPO_LOGRADOURO||"         // Tipo do logradouro
	cStrTxt += "NM_TITULO_LOGRADOURO||"       // T�tulo do logradouro
	cStrTxt += "NM_LOGRADOURO||"              // Logradouro
	cStrTxt += "NM_COMPL_LOGRADOURO||"        // Complemento do logradouro
	cStrTxt += "NR_LOGRADOURO||"              // N�mero do logradouro
	cStrTxt += "CD_CEP||"                     // CEP referente ao logradouro
	cStrTxt += "NM_BAIRRO||"                  // Bairro referente ao logradouro
	cStrTxt += "CD_ESTADO||"                  // Estado (UF) referente ao logradouro
	cStrTxt += "NM_CIDADE||"                  // Cidade referente ao logradouro
	cStrTxt += "CD_PAIS||"                    // Sigla do pa�s
	cStrTxt += "NM_OBSERVACAO||"              // Informa��es gerais sobre a empresa
	cStrTxt += "CD_PLANO_CONTA||"             // C�digo do item do plano de contas
	cStrTxt += "CD_ALVARA||"						// C�digo da obra
	cStrTxt += "IC_ORIGEM_DADOS||"            // Origem dos Dados (Valor Fixo 'R')
	cStrTxt += "IC_ENQUADRAMENTO||"           // Tabela de Enquadramento
	cStrTxt += "CD_PLANO_CONTA_PAI||"         // C�digo da conta mestre no primeiro grau de contas
	cStrTxt += "IC_RECOLHE_IMPOSTO||"         // Recolhe Imposto
	cStrTxt += "VL_ALIQUOTA||"                // Valor al�quota
	cStrTxt += "FL_ISENTO||"                  // Isen��o de Inscri��o Estadual
	cStrTxt += "FL_SIMPLES"         	       // Prestador optante pelo simples nacional

	Begin Sequence
 		WrtStrTxt( nHandle, cStrTxt )
 		GerTxtGISS( nHandle, cTxtSys, "CABECALHO" )
 		Recover
	End Sequence
              
Return ()


//-------------------------------------------------------------------
/*/{Protheus.doc} TGISSRelat

TGISSRelat() - Gera relat�rio de confer�ncia da GISS ONLINE para os registros
que foram impressos no arquivo da obriga��o

@Author Rafael V�ltz
@Since 04/01/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function TGISSRelat()
	Local oReport
	Private cTipoServico as char

	If FindFunction("TRepInUse") .And. TRepInUse()
		//-- Interface de impressao
		oReport := ReportDef()		
		oReport:PrintDialog()
	EndIf	
	

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ReportDef

ReportDef() - Gera relat�rio de confer�ncia da GISS ONLINE para os registros
que foram impressos no arquivo da obriga��o

@Author Rafael V�ltz
@Since 04/01/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function ReportDef	

	Local oReport     	as object
	Local oSectionNF   	as object
	Local oSectionIT   	as object
	Private cAliasRelNF  	as char	
	Private cAliasRec       as char
	
	oReport 	:= TReport():New("GISS ONLINE",UPPER(STR0052),"",{|oReport| ReportPrint(oReport)},STR0053)		//Confer�ncia dos documentos gerados na GISS ONLINE
	cAliasRelNF	:= ""
	cAliasRec   := ""
	cAliasTot 	:= ""
	
	oSectionNF := TRSection():New(oReport,STR0054,{"(cAliasRelNF)"},{"NUM_DOC"})  //Documentos Fiscais de Servi�o
	oSectionNF:SetHeaderPage(.F.)	//Define que imprime cabe�alho das c�lulas no topo da p�gina.		
	
	oCell := TRCell():New(oSectionNF,"(cAliasRelNF)->DC_NUMDOC",	"(cAliasRelNF)",UPPER(STR0043),	/*Picture*/,  15,	/*lPixel*/,{|| (cAliasRelNF)->DC_NUMDOC }) 																//NOTA FISCAL
	oCell := TRCell():New(oSectionNF,"(cAliasRelNF)->DC_SERIE",		"(cAliasRelNF)",UPPER(STR0044),	/*Picture*/,  3,	/*lPixel*/,{|| (cAliasRelNF)->DC_SERIE })																//S�RIE
	oCell := TRCell():New(oSectionNF,"(cAliasRelNF)->DC_DTDOC",		"(cAliasRelNF)",UPPER(OEMTOANSI(STR0045)),	/*Picture*/,  10,	/*lPixel*/,{|| SToD((cAliasRelNF)->DC_DTDOC)}) 												//DATA EMISSAO				
	oCell := TRCell():New(oSectionNF,"(cAliasRelNF)->PT_NOME",		"(cAliasRelNF)",UPPER(STR0047),	/*Picture*/,  60,	/*lPixel*/,{|| Alltrim((cAliasRelNF)->PT_CODIGO) 	+ ' - ' +  Alltrim((cAliasRelNF)->PT_NOME)}) 		//PARTICIPANTE
	oCell := TRCell():New(oSectionNF,"(cAliasRelNF)->PT_MUNPIO",	"(cAliasRelNF)",UPPER(STR0048),	/*Picture*/,  30,	/*lPixel*/,{|| Alltrim((cAliasRelNF)->PT_MUNPIO)})														//MUNIC�PIO PARTICIPANTE
	oCell := TRCell():New(oSectionNF,"(cAliasRelNF)->DC_LOCPRE",	"(cAliasRelNF)",UPPER(STR0049),	/*Picture*/,  30,	/*lPixel*/,{|| Alltrim( POSICIONE("C07", 3,  xFilial( "C07" ) + (cAliasRelNF)->DC_LOCPRE, "C07_DESCRI"))  })		//MUNIC�PIO PRESTA��O
	oCell := TRCell():New(oSectionNF,"(cAliasRelNF)->DC_SRVMUN",	"(cAliasRelNF)",UPPER(STR0057),	/*Picture*/,  12,	/*lPixel*/,{||  Alltrim( Iif(Empty((cAliasRelNF)->DC_NFSRVMUN),(cAliasRelNF)->DC_SRVMUN,(cAliasRelNF)->DC_NFSRVMUN) ) })												//C�digo do servi�o
	oCell := TRCell():New(oSectionNF,"(cAliasRelNF)->DC_CHVNF",		"(cAliasRelNF)",UPPER(STR0050),	/*Picture*/,  60,	/*lPixel*/,{|| Alltrim(TAFDescServ((cAliasRelNF)->DC_CHVNF))})											//Descri��o do Servi�o
	oCell := TRCell():New(oSectionNF,"(cAliasRelNF)->DC_VLDOC",		"(cAliasRelNF)",UPPER(STR0051),	"@E 999,999,999.99",  20,	/*lPixel*/,{|| (cAliasRelNF)->DC_VLDOC })														//Valor
	
	oReport:HideParamPage()   		//Define se ser� permitida a altera��o dos par�metros do relat�rio.
	oReport:SetLandScape()  		//Define orienta��o de p�gina do relat�rio como paisagem
	oReport:ParamReadOnly()			//Par�metros n�o poder�o ser alterados pelo usu�rio
	oReport:DisableOrientation()	//Desabilita a sele��o da orienta��o (Retrato/Paisagem)
	oReport:HideFooter()			//Define que n�o ser� impresso o rodap� padr�o da p�gina
	
	oBreak:= TRBreak():New(oSectionNF, "(cAliasRelNF)->DC_INDOPE" , {|| "TOTAL SERVI�O " + (cTipoServico)}  , .F., 'NOMEBRK', .T.)	
	oSum  := TRFunction():New(oSectionNF:Cell('(cAliasRelNF)->DC_VLDOC'),"TOTALSERV", 'SUM',oBreak,"","@E 999,999,999.99",,.F.,.F.,.F., oSectionNF)		
	
	
	oSectionRec := TRSection():New(oReport,"Recibos de Servi�os Tomados",{"(cAliasRec)"},{"LEM_NUMERO"})  //Documentos Fiscais de Servi�o
	oSectionRec:SetHeaderPage(.F.)	//Define que imprime cabe�alho das c�lulas no topo da p�gina.
	
	oCell := TRCell():New(oSectionRec,"(cAliasRec)->LEM_NUMERO",	"(cAliasRec)",UPPER("N�mero do Documento"),	/*Picture*/,  15,	/*lPixel*/,{|| (cAliasRec)->LEM_NUMERO }) 																//NOTA FISCAL	
	oCell := TRCell():New(oSectionRec,"(cAliasRec)->LEM_DTEMIS",		"(cAliasRec)",UPPER(OEMTOANSI(STR0045)),	/*Picture*/,  10,	/*lPixel*/,{|| SToD((cAliasRec)->LEM_DTEMIS)}) 												//DATA EMISSAO				
	oCell := TRCell():New(oSectionRec,"(cAliasRec)->C1H_NOME",		"(cAliasRec)",UPPER(STR0047),	/*Picture*/,  60,	/*lPixel*/,{|| Alltrim((cAliasRec)->C1H_CODPAR) 	+ ' - ' +  Alltrim((cAliasRec)->C1H_NOME)}) 		//PARTICIPANTE
	oCell := TRCell():New(oSectionRec,"(cAliasRec)->C07_DESCRI",	"(cAliasRec)",UPPER(STR0048),	/*Picture*/,  30,	/*lPixel*/,{|| Alltrim((cAliasRec)->C07_DESCRI)})														//MUNIC�PIO PARTICIPANTE
	oCell := TRCell():New(oSectionRec,"(cAliasRec)->LEM_CODLOC",	"(cAliasRec)",UPPER(STR0049),	/*Picture*/,  30,	/*lPixel*/,{|| Alltrim( POSICIONE("C07", 3,  xFilial( "C07" ) + (cAliasRec)->LEM_CODLOC, "C07_DESCRI"))  })		//MUNIC�PIO PRESTA��O
	oCell := TRCell():New(oSectionRec,"(cAliasRec)->LEM_SRVMUN",	"(cAliasRec)",UPPER(STR0057),	/*Picture*/,  12,	/*lPixel*/,{||  Alltrim( (cAliasRec)->LEM_SRVMUN) })												//C�digo do servi�o	
	oCell := TRCell():New(oSectionRec,"(cAliasRec)->LEM_VLBRUT",		"(cAliasRec)",UPPER(STR0051),	"@E 999,999,999.99",  20,	/*lPixel*/,{|| (cAliasRec)->LEM_VLBRUT })
	
	oBreak:= TRBreak():New(oSectionRec, "(cAliasRec)->LEM_NUMERO" , {|| "TOTAL SERVI�O RECIBO/FATURA "}  , .F., 'NOMEBRK', .T.)	
	oSum  := TRFunction():New(oSectionRec:Cell('(cAliasRec)->LEM_VLBRUT'),"TOTALSERV", 'SUM',oBreak,"","@E 999,999,999.99",,.F.,.F.,.F., oSectionRec)			
	
	oReport:SetCustomText({|| TCabecGISS( oReport )})		
	
	                                    
Return oReport

//-------------------------------------------------------------------
/*/{Protheus.doc} ReportPrint

ReportPrint() - Query de consulta para extra��o do relat�rio

@Param
 oReport   - Objeto do relat�rio
 
@Author Rafael V�ltz
@Since 04/01/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function ReportPrint(oReport as object)
	Local oSectionNF 	as object
	Local oTFont    	as object	
	Local cNF 			as char
	
	cAliasRelNF := TAFSQLServ(DTOS(dInicial), DTOS(dFinal))	 
	
	oSectionNF  := oReport:Section(1)	
	cNF := ""
		
	While !(cAliasRelNF)->(Eof()) .And. !oReport:Cancel()		
		
		If Empty(cNF)
			oSectionNF:Init()
			cNF := (cAliasRelNF)->DC_NUMDOC
		EndIf
		 
		oSectionNF:PrintLine()
		cTipoServico := iif((cAliasRelNF)->DC_INDOPE == '0',STR0055,STR0056 )

		(cAliasRelNF)->(dbSkip())
	EndDo
			
	oSectionNF:Finish()
	
	oSectionNF:SetHeaderPage(.F.)
	oSectionNF:SetHeaderBreak(.F.)
	oSectionNF:SetHeaderSection(.F.)
	
	(cAliasRelNF)->(DbCloseArea())
		
	/* Servi�os Tomados sem documentos fiscal */	
	If TAFAlsInDic( "LEM",.F. )
	
		oSectionRec := oReport:Section(2)	
	
		cAliasRec := TAFRecibosServ(DTOS(dInicial), DTOS(dFinal))						
		cNF := ""
		
		While !(cAliasRec)->(Eof()) .And. !oReport:Cancel()		
			
			If Empty(cNF)
				oSectionRec:Init()
				cNF := (cAliasRec)->LEM_NUMERO
			EndIf
			 
			oSectionRec:PrintLine()
			(cAliasRec)->(dbSkip())
		EndDo
		
		oSectionRec:Finish()
		(cAliasRec)->(DbCloseArea())
		
	EndIf	
	
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} TCabecGISS

TCabecGISS() - Montagem do cabe�alho do relat�rio

@Param
 oReport   - Objeto do relat�rio
 
@Author Rafael V�ltz
@Since 04/01/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function TCabecGISS( oReport as object )

Local aArea     as array
Local aCabec    as array
Local cChar     as char

	aArea     := GetArea()
	aCabec    := {}
	cChar     := chr(160)  // caracter dummy para alinhamento do cabe�alho
	
	If SM0->(Eof())                                
	    SM0->( MsSeek( cEmpAnt + cFilAnt , .T. ))
	Endif
	
	aCabec := {    "__LOGOEMP__" , cChar + "            " ;
	          + "            " + cChar + RptFolha + TRANSFORM(oReport:Page(),'9999');
	          , cChar + "            " ;
	          + "            " + cChar ;
	          , "SIGATAF /" + 'GISS ONLINE' + " /v." + cVersao ; 
	          + "            " + cChar + UPPER( oReport:CTITLE ) ; 
	          + "            " + cChar;
	          , "            " + cChar + RptEmiss + " " + Dtoc(dDataBase);
	          ,(STR0038 + "...........: " +Trim(SM0->M0_NOME) + " / " + STR0039+": " + Trim(SM0->M0_FILIAL));  //"Empresa:"  "Filial:"
	          + "            " + cChar + STR0042+":  " + " " + time();
	          ,(STR0040+"....: " + DTOC(dInicial) + " "+STR0041 + " "+ DTOC(dFinal)); //Per�odo Gerado:	   �       
	          , cChar + "            " }
	
	RestArea( aArea )
              
Return aCabec
//-------------------------------------------------------------------
/*/{Protheus.doc} TAFDescServ

TAFDescServ() - Busca a descri��o do servi�o

@Param
 cChaveNF   - Chave da Nota
 
@Author Rafael V�ltz
@Since 06/02/2017
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function TAFDescServ(cChaveNF as char)

Local cDescServ  as char
Local cAliasQry  as char

	cAliasQry  := GetNextAlias()
	cDescServ  := ""
	
	BeginSql Alias cAliasQry
		SELECT C1L.C1L_DESCRI		       
	           		
		FROM %table:C20% C20
			INNER JOIN %table:C30% C30 ON C30.C30_FILIAL = C20.C20_FILIAL AND C30.C30_CHVNF = C20.C20_CHVNF  AND C30.%NotDel%
			INNER JOIN %table:C1L% C1L ON C1L.C1L_FILIAL = C30.C30_FILIAL AND C1L.C1L_ID 	= C30.C30_CODITE AND C1L.%NotDel%
			
		WHERE C20.C20_FILIAL = %xFilial:C20%
		   AND C20.C20_CHVNF = %Exp:Alltrim(cChaveNF) %
		   AND C20.%NotDel%
		
	EndSQL
	
	
	DbSelectArea(cAliasQry)
	(cAliasQry)->(DbGoTop())
	
	While (cAliasQry)->(!EOF())
		If Empty(cDescServ)
			cDescServ := Alltrim((cAliasQry)->C1L_DESCRI)
		Else
			cDescServ += ". " + Alltrim((cAliasQry)->C1L_DESCRI)
		EndIf
		
		(cAliasQry)->(DbSkip())
	EndDo
	
	(cAliasQry)->(DbCloseArea())

Return cDescServ


//-------------------------------------------------------------------
/*/{Protheus.doc} TAFCadAdic

Cadastro adicional para manuten��o dos c�digos de servi�o

@Param
 dIni    - Data inicial informado na wizard
 dFim    - Data final informado na wizard
 

@Author Rafael V�ltz
@Since 28/12/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function TAFCadAdic(dIni as date, dFim as date)

Local nOpc         as numeric
Local aArea	       as array
Local aAlter       as array
Local lOk          as logical

Private aColsGrid  as array
Private aHeaGrid   as array
Private oBrowGISS   as object
Private oDlg1      as object
Private noBrw      as numeric
	
	nOpc		:= GD_UPDATE	
	aArea		:= GetArea()	
	
	If lBrwServ
		aAlter 		:= {}	
		aColsGrid	:= {}
		aHeaGrid	:= {}
		oBrowGISS	:= nil
		oDlg1		:= nil
		noBrw 		:= 0
		lOk		 	:= .T.
		
		TAFAGISSBrw("C1L")
		TAFAGISSCols("C1L",dInicial, dFinal)
	
		aAdd(aAlter,aHeaGrid[3,2]) //C�digo do servi�o municipal
	
		oDlg1    := MSDialog():New( 091,232,502,900,STR0058,,,.F.,,,,,,.T.,,,.T. )   //str0058 C�digo de Servi�o/Atividade Municipal por Item"
		
		DbSelectArea("C1L")
		oBrowGISS := MsNewGetDados():New(024, 016, 216, 368, nOpc, "AllwaysTrue", "AllwaysTrue", "", aAlter, 000, len(aColsGrid), "AllwaysTrue", "", "AllwaysTrue", oDlg1, aHeaGrid, aColsGrid)
		
		oBrowGISS:obrowse:align:= CONTROL_ALIGN_ALLCLIENT
		
		oDlg1:bInit 		:= EnchoiceBar(oDlg1,{||lOk:=GravaGrid("C1N"), oDlg1:End()},{|| lOk := .F., oDlg1:End()})
		oDlg1:lCentered		:= .T.
		oDlg1:Activate()
	EndIf
		
	RestArea(aArea)

Return lOk

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFAGISSBrw

TAFAGISSBrw() - Monta aHeaGrid da MsNewGetDados
@Param 
 cAlias  - Alias principal que ser� alterado 

@Author Rafael V�ltz
@Since 28/12/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function TAFAGISSBrw(cAlias as char)

	DbSelectArea("SX3")
	DbSetOrder(1)	
	
	DbSeek("C1L")	  	
  	While !Eof() .and. SX3->X3_ARQUIVO == "C1L"
  		
		If Alltrim(SX3->X3_CAMPO) $ "C1L_CODIGO"
	 		noBrw++
			aAdd( aHeaGrid, { AlLTrim( X3Titulo() ),; // 01 - Titulo
			SX3->X3_CAMPO,;
	       	SX3->X3_PICTURE,;
	       	20,;
	       	SX3->X3_DECIMAL,;
	       	SX3->X3_VALID,;
	       	SX3->X3_USADO,;
	       	SX3->X3_TIPO,;
	       	SX3->X3_F3	,;				// 09 - F3
			SX3->X3_CONTEXT ,;       	// 10 - Contexto
			SX3->X3_CBOX	,; 	  	  	// 11 - ComboBox
		    "", } ) 	   	    		// 12 - Relacao
		Endif
		
		If Alltrim(SX3->X3_CAMPO) $ "C1L_DESCRI"
	 		noBrw++
			aAdd( aHeaGrid, { AlLTrim( X3Titulo() ),; // 01 - Titulo
			SX3->X3_CAMPO,;
	       	SX3->X3_PICTURE,;
	       	40,;
	       	SX3->X3_DECIMAL,;
	       	SX3->X3_VALID,;
	       	SX3->X3_USADO,;
	       	SX3->X3_TIPO,;
	       	SX3->X3_F3	,;					// 09 - F3
			SX3->X3_CONTEXT ,;       		// 10 - Contexto
			SX3->X3_CBOX		,; 	  	  	// 11 - ComboBox
		    "", } ) 		  				// 12 - Relacao
		Endif
		
		If Alltrim(SX3->X3_CAMPO) $ "C1L_SRVMUN"
	 		noBrw++
			aAdd( aHeaGrid, { AlLTrim( X3Titulo() ),; // 01 - Titulo
			SX3->X3_CAMPO,;
	       	SX3->X3_PICTURE,;
	       	SX3->X3_TAMANHO,;
	       	20,;
	       	SX3->X3_VALID,;			       	
	       	SX3->X3_USADO,;
	       	SX3->X3_TIPO,;
	       	SX3->X3_F3	,;					// 09 - F3
			SX3->X3_CONTEXT ,;       		// 10 - Contexto
			SX3->X3_CBOX		,; 	  	  	// 11 - ComboBox
		    "", } ) 		  				// 12 - Relacao
		Endif
		  		
		DbSkip()
	EndDo	

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFConsServ()

TAFConsServ() - Consulta no cadastro de Produto para os movimentos do per�odo,
				para verificar se existe c�digo de servi�o n�o preenchido. 
@Param
 dIni    - Data inicial informado na wizard
 dFim    - Data final informado na wizard
 
 @Return
 lExist  - Indica se existe algum cadastro com ANP em branco. 

@Author Rafael V�ltz
@Since 28/12/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function TAFConsServ(dIni as date, dFim as date)
 
 Local cAliasQry	as char 
 
 cAliasQry 		:= GetNextAlias() 
 
 BeginSQL Alias cAliasQry
		SELECT COUNT(*) COUNT
	 	  FROM %table:C20% C20	
	INNER JOIN %table:C0U% C0U ON C0U.C0U_FILIAL = %xFilial:C0U%   AND C0U.C0U_ID   = C20.C20_TPDOC  AND C0U.C0U_CODIGO = %Exp:'06'% AND C0U.%NotDel% //TIPO DE NOTA (SERVI�O) (C0U)
	INNER JOIN %table:C30% C30 ON C30.C30_FILIAL = C20.C20_FILIAL AND C30.C30_CHVNF = C20.C20_CHVNF  AND C30.%NotDel%	
	INNER JOIN %table:C1L% C1L ON C1L.C1L_FILIAL = C30.C30_FILIAL AND C1L.C1L_ID    = C30.C30_CODITE AND C1L.%NotDel%
	 	 WHERE C20.C20_FILIAL = %xFilial:C20%	 	    
	 	   AND C20.C20_DTDOC BETWEEN %Exp:DTOS(dIni)% AND %Exp:DTOS(dFim)%	 	   
	 	   AND (C1L.C1L_SRVMUN IS NULL OR C1L.C1L_SRVMUN = ' ') 	   
	 	   AND C20.%NotDel%
 EndSQL
 
 IIF ((cAliasQry)->COUNT > 0, lBrwServ := .T., lBrwServ := .F.)  

 (cAliasQry)->(DbCloseArea())
   
Return lBrwServ

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFAGISSCols()

TAFA456Cols() - Monta aColsGrid da MsNewGetDados

@Param 
 cAlias  - Alias principal que ser� alterado
 dIni    - Data inicial informado na wizard
 dFim    - Data final informado na wizard 

@Author Rafael V�ltz
@Since 28/12/2016
@Version 1.0
/*/
//-------------------------------------------------------------------

Static Function TAFAGISSCols(cAlias as char, dIni as date, dFim as date)
Local nX         as numeric
Local nY         as numeric
Local cAliasQry  as char

 nX        := 0
 nY        := 0
 
 cAliasQry := GetNextAlias()
 	
	BeginSQL Alias cAliasQry
		 	SELECT DISTINCT 
		 	       C1L_CODIGO,
			       C1L_DESCRI,
			       C1L_SRVMUN 
		 	  FROM %table:C20% C20
		INNER JOIN %table:C0U% C0U ON C0U.C0U_FILIAL = %xFilial:C0U%   AND C0U.C0U_ID   = C20.C20_TPDOC  AND C0U.C0U_CODIGO = %Exp:'06'% AND C0U.%NotDel% //TIPO DE NOTA (SERVI�O) (C0U)
		INNER JOIN %table:C30% C30 ON C30.C30_FILIAL = C20.C20_FILIAL AND C30.C30_CHVNF = C20.C20_CHVNF  AND C30.%NotDel%
		INNER JOIN %table:C1L% C1L ON C1L.C1L_FILIAL = C30.C30_FILIAL AND C1L.C1L_ID    = C30.C30_CODITE AND C1L.%NotDel%				
		 	 WHERE C20.C20_FILIAL = %xFilial:C20%			 	 
		 	   AND C20.C20_DTDOC BETWEEN %Exp:DTOS(dIni)% AND %Exp:DTOS(dFim)%		 	   	 	   
		 	   AND (C1L.C1L_SRVMUN IS NULL OR C1L.C1L_SRVMUN = ' ')			 	   		
		 	   AND C20.%NotDel%		 	  
		 	  ORDER BY C1L_CODIGO
	EndSql
	 
	 While (cAliasQry)->(!Eof())
	 	AADD(aColsGrid,Array(noBrw+1))		
		aColsGrid[Len(aColsGrid)][1]  := (cAliasQry)->C1L_CODIGO
		aColsGrid[Len(aColsGrid)][2]  := substr((cAliasQry)->C1L_DESCRI,   1, 50)
		
		//C�digo do Servi�o
		aColsGrid[Len(aColsGrid)][3]  := space(20)
		aColsGrid[Len(aColsGrid),noBrw+1]:=.F.
	
		(cAliasQry)->(DbSkip())
	EndDo
	
	(cAliasQry)->(DbCloseArea()) 
 
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} GravaGrid

Atualiza c�digo do servi�o municipal na tabela informada por par�metro

@Param 
cAlias - Tabela onde ser� realizada a atualiza��o

@Return 

@Author Rafael V�ltz
@Since 28/12/2016
@Version 1.0
/*/
//-------------------------------------------------------------------
Static Function GravaGrid(cAlias as char)
Local nX   		as numeric
Local lAlterou	as logical

	lAlterou := .F.
	
	Begin Transaction
		DbSelectArea("C1L")				
		
		C1L->(DbSetOrder(1))				
		
		For nX := 1 To Len(oBrowGISS:aCols)				
			If(!oBrowGISS:aCols[nX][4])						
				
				If (C1L->(DbSeek(xFilial("C1L") + oBrowGISS:aCols[nX][1])))
					While C1L->(!Eof()) .AND. (C1L->C1L_FILIAL + C1L->C1L_CODIGO) == (xFilial("C1L") + oBrowGISS:aCols[nX][1])
						
						//C�digo Servi�o Municipal
						If !Empty(oBrowGISS:aCols[nX][3])
							RecLock("C1L",.F.) //altera��o
							C1L->C1L_SRVMUN := Alltrim(oBrowGISS:aCols[nX][3])
							MsUnLock()
							lAlterou := .T.									
						EndIf
						C1L->(DbSkip())								
					EndDo
				EndIf		
			EndIf
		Next
		
		C1L->(DbCloseArea())
				
	End Transaction	
	
	If lAlterou
		msginfo( STR0059  ) 	//"Aten��o" 	"Informa��es atualizadas com sucesso."   	"Sair"
	Else
		msginfo( STR0060 ) 	//"Aten��o"		"Nenhum registro foi alterado"     			"Sair"
	EndIf

Return	.T.
