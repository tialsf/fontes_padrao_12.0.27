#INCLUDE "PROTHEUS.CH"
#INCLUDE "TAFSPNFE.CH"                                     
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "TOTVS.CH"  
#INCLUDE "RPTDEF.CH"  
#INCLUDE "PARMTYPE.CH"    
#INCLUDE "FILEIO.CH"    
#INCLUDE "APWEBSRV.CH"                                 

#DEFINE TAMMAXXML 400000 //- Tamanho maximo do XML em  bytes       

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFSPNFE
Rotina de remessa da Nota fiscal eletronica para o Totvs 
Service SPED - Ambiente TAF
*Fun��o Original: SpedNFeRe2 (Eduardo Rieira - 21.06.2007)

@author Evandro dos Santos Oliveira
@since 02/04/2014
@version 1.0	

@param cSerie   - Serie da Nota Fiscal 
@param cNotaIni - Nota Inicial
@param cNotaFim - Nota Final

@return Nil 
/*/
//-------------------------------------------------------------------
Function TAFSPNFE(cSerie,cNotaIni,cNotaFim)
    
	Local aArea       := GetArea()
	Local aParam      := {Space(Len(C20->C20_SERIE)),Space(Len(C20->C20_NUMDOC)),Space(Len(C20->C20_NUMDOC))}
	Local aPerg  	  := {}
	Local cLoadPar    := SM0->M0_CODIGO+SM0->M0_CODFIL+"TAFNFEREM"  
	Local cIdEnt 	  := ""       
	Local lOk 		  := .T.     
	Local cModel      := "55"  
	Local aXML		  := {}  
	Local aTexto	  := {}
	Local nX 		  := 0 
	Local cMonSEF 	  := "" 
	Local cAmb 	  	  := ""
	Local cModalid    := ""
	Local cSugest	  := "" 
	Local cRet		  := ""
	Local cVersao	  := "" 
	Local cPictDoc	  := ""
	Local cPictSer    := "" 
	Local cUrl		  := ""

	Static cCodDen    := "110,301,205,302,303,304,305,306"
	 	
	If FindFunction("TafGetUrlTSS")
		cUrl := PadR(TafGetUrlTSS(),250)
	Else
		cUrl := PadR(GetNewPar("MV_TAFSURL","http://"),250)
	EndIf 
	cURL := AllTrim(cURL)

	If cSerie == Nil
		MV_PAR01 := aParam[01] := PadR(ParamLoad(cLoadPar,aPerg,1,aParam[01]),Len(C20->C20_SERIE))
		MV_PAR02 := aParam[02] := PadR(ParamLoad(cLoadPar,aPerg,2,aParam[02]),Len(C20->C20_NUMDOC))
		MV_PAR03 := aParam[03] := PadR(ParamLoad(cLoadPar,aPerg,3,aParam[03]),Len(C20->C20_NUMDOC))
	Else
		MV_PAR01 := aParam[01] := cSerie
		MV_PAR02 := aParam[02] := cNotaIni
		MV_PAR03 := aParam[03] := cNotaFim
	EndIf
	

	/*+-------------------------------------------------+
	  | Limito o tamanho m�ximo do Documento e da S�rie |
	  | para evitar estouro do campo na Query			|	
	  +-------------------------------------------------+*/  
	cPictDoc := Replicate("9",9)
	cPictSer := Replicate("9",3)
	
	aAdd(aPerg,{1,STR0001,aParam[01],cPictSer,".T.","",".T.",30,.F.}) 	//"Serie da Nota Fiscal"
	aAdd(aPerg,{1,STR0002,aParam[02],cPictDoc,".T.","",".T.",50,.T.})	//"Nota fiscal inicial"
	aAdd(aPerg,{1,STR0003,aParam[03],cPictDoc,".T.","",".T.",50,.T.})	//"Nota fiscal final" 
		
	If TAFCTSpd() 	
		cIdEnt := AllTrim(TAFRIdEnt())  		
		If !Empty(cIdEnt)
		
			/*+-----------------------------------------------------+
			  | Obtem o ambiente de execucao do Totvs Services SPED |
			  +-----------------------------------------------------+*/  
			oWS := WsSpedCfgNFe():New()
			oWS:cUSERTOKEN := "TOTVS"
			oWS:cID_ENT    := cIdEnt
			oWS:nAmbiente  := 0	
			oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
			lOk  := oWS:CFGAMBIENTE()
			cAmb := oWS:cCfgAmbienteResult
			If lOk
				lOk := oWs:CfgTSSVersao()
			EndIf
			
			/*+---------------------------------------------------+
			  | Obtem a modalidade de comunicacao com a SEFAZ TSS |
			  +---------------------------------------------------+*/  
			If lOk .And. oWs:cCfgTssVersaoResult >= "1.35"
				oWS:cUSERTOKEN := "TOTVS"
				oWS:cID_ENT    := cIdEnt			
				oWS:nAMBIENTE  := 0
				oWS:nMODALIDADE:= 0
				oWS:cVERSAONFE := ""      
				oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
				oWS:CFGPARAMSPED()		
			EndIf   
		  		
			/*+-------------------------------------------------------+
			  | Obtem a modalidade de execucao do Totvs Services SPED |
			  +-------------------------------------------------------+*/  			
			If lOk
				oWS:cUSERTOKEN 	:= "TOTVS"
				oWS:cID_ENT    	:= cIdEnt
				oWS:nModalidade	:= 0
				oWS:_URL       	:= AllTrim(cURL)+"/SPEDCFGNFe.apw"
				oWS:cModelo   	:= cModel
				lOk 			:= oWS:CFGModalidade()
				cModalid    	:= oWS:cCfgModalidadeResult
			EndIf   
			
			/*+----------------------------------------------------------+
			  | Obtem a versao de trabalho da NFe do Totvs Services SPED |
			  +----------------------------------------------------------+*/  			
			If lOk
				oWS:cUSERTOKEN := "TOTVS"
				oWS:cID_ENT    := cIdEnt
				oWS:cVersao    := "0.00"
				oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
				lOk := oWS:CFGVersao()
				cVersao        := oWS:cCfgVersaoResult
			EndIf
			
			/*+-----------------------------+
			  | Verifica o status na SEFAZ  |
			  +-----------------------------+*/  	
			If lOk
				oWS:= WSNFeSBRA():New()
				oWS:cUSERTOKEN := "TOTVS"
				oWS:cID_ENT    := cIdEnt
				oWS:_URL       := AllTrim(cURL)+"/NFeSBRA.apw"
				lOk := oWS:MONITORSEFAZMODELO()
				If lOk
					aXML := oWS:oWsMonitorSefazModeloResult:OWSMONITORSTATUSSEFAZMODELO
					For nX := 1 To Len(aXML)
		
						If aXML[nX]:cModelo == "55"
							cMonSEF += "- NFe"+CRLF
							cMonSEF += STR0004+cVersao+CRLF	//"Versao do layout: "
							If !Empty(aXML[nX]:cSugestao)
								cSugest += STR0005+"(NFe)"+": "+aXML[nX]:cSugestao+CRLF //"Sugest�o"
							EndIf
						EndIf
			
						cMonSEF += Space(6)+STR0006+": "+aXML[nX]:cVersaoMensagem+CRLF //"Vers�o da mensagem"
						cMonSEF += Space(6)+STR0007+": "+aXML[nX]:cStatusCodigo+"-"+aXML[nX]:cStatusMensagem+CRLF //"C�digo do Status"
		                cMonSEF += Space(6)+STR0008+": "+aXML[nX]:cUFOrigem //"UF Origem"
		                If !Empty(aXML[nX]:cUFResposta)
			                cMonSEF += "("+aXML[nX]:cUFResposta+")"+CRLF //"UF Resposta"
			   			Else
			   				cMonSEF += CRLF
			   			EndIf
		                If aXML[nX]:nTempoMedioSEF <> Nil
							cMonSEF += Space(6)+STR0010+": "+Str(aXML[nX]:nTempoMedioSEF,6)+CRLF //"Tempo de espera"
						EndIf
						If !Empty(aXML[nX]:cMotivo)
							cMonSEF += Space(6)+STR0011+": "+aXML[nX]:cMotivo+CRLF //"Motivo"
						EndIf
						If !Empty(aXML[nX]:cObservacao)
							cMonSEF += Space(6)+STR0012+": "+aXML[nX]:cObservacao+CRLF //"Observa��o"
						EndIf
					Next nX
				EndIf
			EndIf		

			If (lOk == .T. .Or. lOk == Nil)

				aadd(aTexto,{})
				aTexto[1] := STR0013+" " //"Esta rotina tem como objetivo auxilia-lo na transmiss�o da Nota Fiscal eletr�nica para o servi�o Totvs Services SPED. "
				aTexto[1] += STR0014+CRLF+CRLF //"Neste momento o Totvs Services SPED, est� operando com a seguinte configura��o: "
				aTexto[1] += STR0015+cAmb+CRLF //"Ambiente: "
				aTexto[1] += STR0016+cModalid+CRLF	//"Modalidade de emiss�o: "
				If !Empty(cSugest)
					aTexto[1] += CRLF
					aTexto[1] += cSugest
					aTexto[1] += CRLF
				EndIf			
				aTexto[1] += cMonSEF
				
				aadd(aTexto,{})
					
				oWizard := APWizard():New(	 STR0017; //"Assistente de transmiss�o da nota fiscal eletr�nica"
			  								,STR0018; //"Aten��o"
			  								,STR0019; //"Siga atentamente os passos para a configura��o da nota fiscal eletr�nica."   
									   		,aTexto[1]; 
			  								,{|| .T.};
											,{|| .T.})	
			
				oWizard:NewPanel( 	STR0017; //"Assistente de transmiss�o da nota fiscal eletr�nica"
									,""; 
									,{|| .T.}; 
									,{|| ParamSave(cLoadPar,aPerg,"1"),Processa({|lEnd| cRet := TAFSpedTrf(aArea[1],aParam[1],aParam[2],aParam[3],cIdEnt,cAmb,cModalid,cVersao,@lEnd)}),aTexto[02]:= cRet,.T. };  //++ procurar a fun��o paramsave       
									,{|| .F.}; 
									,.T.; 
							 		,{||.T.})				 	    
				 			 	
			    ParamBox(aPerg,"SPED - NFe",@aParam,,,,,,oWizard:oMPanel[2],cLoadPar,.T.,.T.) 
			    
			    oWizard:NewPanel( 	 STR0017; //"Assistente de transmiss�o da nota fiscal eletr�nica" 
									,""; 
									,{|| .T.}; 
									,{|| .T.};  
									,{|| .T.}; 
									,.T.; 
						 			,{||.T.})				 	    
			
				@ 010,010 GET  aTexto[2] MEMO SIZE 270, 115 READONLY PIXEL OF oWizard:oMPanel[3]
			
			
				oWizard:Activate(    .T. ;
								 ,{||.T.};
								 ,{||.T.};
								 ,{||.T.})
			EndIf 
		EndIf
	Else 
		Aviso("TAF-NFE",STR0020,{STR0009},3) //"Execute o m�dulo de configura��o do servi�o, antes de utilizar esta op��o!!!"###"Ok"
	EndIf

Return Nil 

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFSpedTrf
Rotina de remessa da Nota fiscal eletronica para o Totvs 
Service SPED - Ambiente TAF
*Fun��o Original: SpedNFeTrf(Eduardo Rieira - 21.06.2007)

@author Evandro dos Santos Oliveira
@since 02/04/2014
@version 1.0	

@param cAlias   - Alias da tabela da Mbrowse //++ N�o usado 
@param cSerie 	- Serie
@param cNotaIni - Nota inicial
@param cNotaFim - Nota final
@param cIDEnt  	- Id da entidade empresarial
@param cAmbient - Ambiente de emissao da NFe
@param cModalid - Modalidade de emissao da NFe
@param cVersao  - Versa da NFe
@param lEnd	 	- Controle de encerramento

@return cRet	- Mensagem de de Finaliza��o do Wizard 
/*/
//-------------------------------------------------------------------
Static Function TAFSpedTrf(cAlias,cSerie,cNotaIni,cNotaFim,cIDEnt,cAmbient,cModalid,cVersao,lEnd)

Local aArea			:= GetArea()
Local aNotas		:= {}
Local aNotasOri		:= {}
Local aRetNotas		:= {}
Local aXml			:= {}
Local aMotCont		:= {}
Local aInfnota		:= {}
Local aValNFe		:= {}
Local cRet			:= ""
Local cWhere		:= ""
Local cErro			:= ""
Local cHoraIni		:= Time()
Local dDataIni		:= Date()
Local lQuery		:= .F.
Local lRet			:= .T.
Local nX			:= 0
Local nY			:= 0
Local nNFes			:= 0
Local nXmlTam		:= 0  
Local nXmlTam2		:= 0
Local nPosAux		:= 0 
Local nTemp			:= 0
Local oWs   
Local cMensagem  	:= ""
Local cCodRNfe   	:= "" 
Local cMsgRNfe   	:= "" 
Local cIdTfNfe 	 	:= ""
Local cUrl  		:= ""

If FindFunction("TafGetUrlTSS")
	cUrl := PadR(TafGetUrlTSS(),250)
Else
	cUrl := PadR(GetNewPar("MV_TAFSURL","http://"),250)
EndIf 
cURL := AllTrim(cURL)

/*+-----------------------------------------------------+
  | Restaura a integridade da rotina caso exista filtro |
  +-----------------------------------------------------+*/  
dbSelectArea(cAlias)
dbClearFilter()
RetIndex(cAlias)

aadd(aInfnota,cSerie)
aadd(aInfnota,cNotaIni)
aadd(aInfnota,cNotaFim)

cAmbient    := SubStr(cAmbient,1,1)
cModalid    := SubStr(cModalid,1,1)

/*+--------------------------------------+
  | Obtem as informacoes de contigencia  |
  +--------------------------------------+*/  
If cModalid $ "2,3,5,7"
	oWs2 := WsSpedCfgNFe():New()	
	oWS2:cUSERTOKEN := "TOTVS"
	oWS2:cID_ENT    := cIdEnt
	oWS2:dDATEINIC  := SToD("  /  /    ")
	oWS2:dDATEFIM   := SToD("  /  /    ")
	oWS2:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
	oWS2:GetDateCont()
	If Type("oWS2:oWSGETDATECONTRESULT")=="O" .And. Type("oWS2:oWSGETDATECONTRESULT:OWSGETCONT")=="A" .And. Len(oWS2:oWSGETDATECONTRESULT:OWSGETCONT)>0
		aAdd(aMotCont,AllTrim(oWS2:oWSGETDATECONTRESULT:OWSGETCONT[Len(oWS2:oWSGETDATECONTRESULT:OWSGETCONT)]:CXMOTIVO))
		aAdd(aMotCont,oWS2:oWSGETDATECONTRESULT:OWSGETCONT[Len(oWS2:oWSGETDATECONTRESULT:OWSGETCONT)]:DDATEINIC)
		aAdd(aMotCont,AllTrim(oWS2:oWSGETDATECONTRESULT:OWSGETCONT[Len(oWS2:oWSGETDATECONTRESULT:OWSGETCONT)]:CTIMEINIC))
	EndIf
EndIf

cAliasNf := GetNextAlias() 
nNotaIni :=  Val(cNotaIni)
nNotaFim :=  Val(cNotaFim)  
cWhere := "% ( (C20.C20_INDEMI='0' And C20.C20_DTCANC = ' ')) %"

BeginSql Alias cAliasNf

SELECT C20.C20_FILIAL 	AS FILIAL
	  ,C20.C20_DTDOC    AS EMISSAO
	  ,C20.C20_NUMDOC   AS DOCUMENTO
	  ,C20.C20_SERIE    AS SERIE
	  ,C20.C20_INDEMI   AS TP_EMISSAO
	  ,C1H.C1H_CODPAR   AS PARTICIP
	  ,C20.C20_CHVNF	AS CHAVENF 
	  ,C0U.C0U_CODIGO   AS TP_DOC   
	  ,C20.C20_INDOPE   AS INDOPE
	  ,C20.C20_CHVELE	AS CHVELE
	  ,C20.C20_CHVREF 	AS CHVREF
	  ,C20.R_E_C_N_O_   AS REC_C20
FROM %table:C20% C20  
LEFT JOIN %table:C1H% C1H ON C1H.C1H_ID = C20.C20_CODPAR AND C1H.C1H_FILIAL = %xFilial:C1H% AND C1H.%NotDel%
LEFT JOIN %table:C0U% C0U ON C0U.C0U_ID = C20.C20_TPDOC  AND C0U.C0U_FILIAL = %xFilial:C0U% AND C0U.%NotDel%
WHERE C20.C20_FILIAL = %xFilial:C20%  AND C20.%NotDel% 
AND C20.C20_SERIE = %Exp:cSerie%  
AND %Exp:cWhere% 
AND CAST(C20.C20_NUMDOC AS REAL) BETWEEN %Exp:nNotaIni% AND %Exp:nNotaFim% 

EndSql 

While (cAliasNf)->(!Eof())   
	IncProc("(1/2) "+STR0021+(cAliasNf)->DOCUMENTO) //"Preparando nota: " 
	If Empty(AllTrim((cAliasNf)->CHVELE)) .And. Empty(AllTrim((cAliasNf)->CHVREF))
		aAdd(aNotas,{})
		nX := Len(aNotas)	
		aAdd(aNotas[nX],(cAliasNf)->INDOPE) 
		aAdd(aNotas[nX],(cAliasNf)->CHAVENF)  
		aAdd(aNotas[nX],IIf(Empty((cAliasNf)->SERIE),"000",AllTrim((cAliasNf)->SERIE)))
		aAdd(aNotas[nX],StrZero(Val((cAliasNf)->DOCUMENTO),9)) 
		aAdd(aNotas[nX],(cAliasNf)->TP_DOC) 
		aAdd(aNotas[nX],(cAliasNf)->REC_C20)
		aAdd(aNotas[nX],aMotCont)	
	Else
		cErro+=STR0075 + AllTrim((cAliasNf)->SERIE) + StrZero(Val((cAliasNf)->DOCUMENTO),9) + STR0076
	EndIf
  	(cAliasNf)->(DbSkip())
EndDo   

oWs:= WsNFeSBra():New()
oWs:cUserToken := "TOTVS"
oWs:cID_ENT    := cIdEnt
oWS:_URL       := AllTrim(cURL)+"/NFeSBRA.apw"
oWs:oWsNFe:oWSNOTAS :=  NFeSBRA_ARRAYOFNFeS():New()
For nX := 1 To Len(aNotas)               

	IncProc("(2/2) "+STR0022+aNotas[nX][3]) //"Transmitindo XML da nota: "    
	aXml	:= ExecBlock("TAFNfesf",.F.,.F.,{aNotas[nX][1],aNotas[nX][2],aNotas[nX][5]})
	nXmlTam2 := Len(aXML[2]) 
	  	
   	If !Empty(aXML[2]) .And. nXmlTam2 <= TAMMAXXML
		If nXmlTam + Len(aXML[2]) <= TAMMAXXML
			nY++
			nNFes++
			nXmlTam += Len(aXML[2])
			aAdd(oWs:oWsNFe:oWSNOTAS:oWSNFeS,NFeSBRA_NFeS():New())
			
			aAdd(aRetNotas,aNotas[nX])
			
			oWs:oWsNFe:oWSNOTAS:oWsNFes[nY]:cID := aNotas[nX][3]+aNotas[nX][4]
			oWs:oWsNFe:oWSNOTAS:oWsNFes[nY]:cXML:= aXML[2]
		Else
		   	lRet := XMLRemessa(@oWs,@cErro,@aRetNotas,@nY,@nXmlTam,cIdEnt,cURL,aXML[2],@cIdTfNfe)
			If !lRet 
				Exit 
			EndIf
			nX -- //- Diminui o contador para que seja pego a nota corrente 
			Loop 
		EndIF 
	ElseIf !Empty(aXML[2]) .And. nXmlTam2 > TAMMAXXML
		If !lAuto
			Aviso("TAF-NFE",STR0023+CRLF+CRLF+STR0025+aNotas[nX][4]+" / "+aNotas[nX][3],{STR0009},3)
		Else
			Conout("SPED",STR0024+CRLF+CRLF+STR0025+aNotas[nX][4]+" / "+aNotas[nX][3],{STR0009},3)
		EndIf
		nXmlTam2 := 0
	EndIf
	
	If ((nY >=50 .Or. nX == Len(aNotas) .Or. nXmlTam>=TAMMAXXML) .And. nNFes > 0)
  		lRet := XMLRemessa(@oWs,@cErro,@aRetNotas,@nY,@nXmlTam,cIdEnt,cURL,aXML[2],@cIdTfNfe)
	EndIf	
	
Next nX

If lRet
	cRet := STR0026+CRLF //"Voc� conclu�u com sucesso a transmiss�o do Protheus para o Totvs Services SPED."
	cRet += STR0027+CRLF+CRLF //"Verifique se as notas foram autorizadas na SEFAZ, utilizando a rotina 'Monitor'. Antes de imprimir a DANFe."
	cRet += STR0028+AllTrim(Str(nNFes,18))+STR0029+TAFInt2Hor(TAFSubHora(dDataIni,cHoraIni,Date(),Time()))+CRLF+CRLF //"Foram transmitidas "###" nota(s) em "  //++ Verificar se as fun��es sao padr�es
	cRet += cErro	
Else
	cRet := STR0030+CRLF+CRLF //"Houve erro durante a transmiss�o para o Totvs Services SPED."
	cRet += cErro
EndIf

	oWs:= WsNFeSBra():New()
	oWs:cUserToken := "TOTVS"
	oWs:cID_ENT    := cIdEnt
	oWS:_URL       := AllTrim(cURL)+"/NFeSBRA.apw"
	oWs:oWsNFe:oWSNOTAS :=  NFeSBRA_ARRAYOFNFeS():New()
	nTotNF := 0
	nXmlSize := 0
	aRetNotas := {}

Return(cRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} XMLRemessa
Rotina de remessa da Nota fiscal eletronica para o Totvs 
Service SPED - Ambiente TAF
*Fun��o Original: XMLRemessa(Gustavo. G. Rueda - 24.03.2010)

@author Evandro dos Santos Oliveira
@since 02/04/2014
@version 1.0	                                                                

@param oWs	     - Objeto da NFe 
@param cErro     - String para atribui��o de poss�veis erros(@ref)
@param aRetNotas - Array com Informa��es da NFe
@param nTotNf    - Total de Notas para transmiss�o
@param nXmlSize  - Tamanho do XML
@param cIdEnt    - Id da Entidade (TSS)
@param cURL      - Url do Totvs Sped
@param cXml      - XML da Nota
@param cIdTfNfe  - Chave da Nfe (TAF)

@return cRet	- Mensagem de de Finaliza��o do Wizard 
/*/
//-------------------------------------------------------------------
Static Function XMLRemessa(oWs,cErro,aRetNotas,nTotNf,nXmlSize,cIdEnt,cURL,cXml,cIdTfNfe)
Local nY
Local lRetorno := .T.  
Local cStatus  := ""    
Local cCodSeq  := ""  
Local cAuxChv  := ""
Local nAuxCod  := 0
Local cRetSch  := ""    
Local cRetXML  := ""
Local lRec	   := .T.
Local cCodSt   := ""  
Local lErro    := .F.
Local nRecCU0  := 0

If nXmlSize>0 .And. oWs:Remessa()
	If Len(oWs:oWsRemessaResult:oWSID:cString) <> nTotNF
		cErro += STR0033+CRLF+CRLF //"As notas abaixo foram recusadas, verifique a rotina 'Monitor' para saber os motivos." 
	EndIf          
	
	For nY := 1 To Len(aRetNotas)
		If Len(oWs:oWsRemessaResult:oWSID:cString) <> nY
			If aScan(oWs:oWsRemessaResult:oWSID:cString,aRetNotas[nY][3]+aRetNotas[nY][4])==0
				cErro += "NFe: " + aRetNotas[nY][3]  + aRetNotas[nY][4] + CRLF    
				cStatus := "1" 
			Else
				cStatus := "2" 
			EndIf
		EndIf    
	
		dbSelectArea("CU0")
		dbSetOrder(6)
		If !(CU0->(MsSeek(xFilial("CU0")+aRetNotas[nY][3]+aRetNotas[nY][4]+"100")))		
			If CU0->(MsSeek(xFilial("CU0")+aRetNotas[nY][3]+aRetNotas[nY][4]))
				While CU0->(!Eof()) .And. CU0->CU0_SERIE+CU0->CU0_NUMDOC == aRetNotas[nY][3]+aRetNotas[nY][4]
					If CU0->CU0_STATUS == "2" .And. Empty(AllTrim(CU0->CU0_RECIBO)) 
				  		cErro += STR0041 + " NFe:" + aRetNotas[nY][3]  + aRetNotas[nY][4] + CRLF   
				  		lErro := .T.
				  		lRetorno := .T. // Retorno True para n�o aparece a mensagem de verificar no monitor
				 	ElseIf CU0->CU0_STATUS == "1" .And. Empty(AllTrim(CU0->CU0_RECIBO))
				 		lRec := .F.
				 		nRecCU0 := CU0->(RECNO())
				 		CU0->(dbGoBottom())
				  	EndIf
			  		CU0->(dbSkip())
			  	EndDo 
	  		EndIf
	  		If !lErro 	
		   		TAFValSch(cIdEnt,(aRetNotas[nY][3]+aRetNotas[nY][4]),2,@cRetSch,@cStatus,@cRetXML,@cCodSt)	
		   		If !lRec  
		   			dbSelectArea("CU0")
		   		 	CU0->(dbGoto(nRecCU0))
		   		EndIf		
				RecLock("CU0",lRec)
				CU0->CU0_FILIAL := xFilial("CU0") 
				CU0->CU0_DTOCOR := dDataBase 
				CU0->CU0_CODERR := cCodSt
				CU0->CU0_DCODER := cRetSch 
				CU0->CU0_XML    := cRetXML 
				CU0->CU0_STATUS := cStatus     
				CU0->CU0_TPMOV	:= aRetNotas[nY][1] + IIf(AllTrim(aRetNotas[nY][1]) == "0",STR0080,STR0081) //" - Entrada"###" - Saida "
				CU0->CU0_CHVNF  := aRetNotas[nY][2]
				CU0->CU0_SERIE  := aRetNotas[nY][3]	 
				CU0->CU0_NUMDOC := aRetNotas[nY][4]
				CU0->CU0_RECNO  := aRetNotas[nY][6]		
				If lRec
					CU0->CU0_ID	    := GetSx8Num("CU0","CU0_ID") 
				EndIf
				CU0->(MsUnlock())
				If lRec
			   		ConfirmSX8()
				EndIf
				cIdTfNfe := CU0->CU0_ID 
			EndIf
		Else
			cErro += STR0079 +  CRLF
			lRetorno := .F. 
		EndIf

		lRec  := .F.
		lErro := .F.		  		 
	Next nY    
	
	oWs:= WsNFeSBra():New()
	oWs:cUserToken := "TOTVS"
	oWs:cID_ENT    := cIdEnt
	oWS:_URL       := AllTrim(cURL)+"/NFeSBRA.apw"
	oWs:oWsNFe:oWSNOTAS :=  NFeSBRA_ARRAYOFNFeS():New()
	
Else
	cErro += GetWscError(3) + CRLF
	cErro += STR0034 + CRLF //"Erro indeterminado"
	lRetorno := .F.
EndIf
Return lRetorno       


//-----------------------------------------------------------------------
/*/{Protheus.doc} TAFValSch 
Verifica se ocorreu erro de schema e grava informa��oes na CU0

@author Evandro dos Santos Oliveira 
@since  04/04/2014
@version 1.0 

@param	cIdEnt  - Codigo da entidade
		cIdNFe  - Id da NFe que ser� feito a valida��o de schema 
		nTipo   - Indica se a Nota � Normal ou Cancelada
		cRet    - Mensagem de Retorno
		cStatus - Status de transmissao da NFe 
		
@return	.T.
/*/
//-----------------------------------------------------------------------
Static Function TAFValSch(cIdEnt,cIdNFe,nTipo,cRet,cStatus,cXML,cCodSt)

Local cURL     := ""
Local cMsg     := ""

Local oWS

DEFAULT nTipo  := 1

If FindFunction("TafGetUrlTSS")
	cURL := PadR(TafGetUrlTSS(),250)
Else
	cURL := PadR(GetNewPar("MV_TAFSURL","http://"),250)
EndIf 
cURL := AllTrim(cURL)

oWS:= WSNFeSBRA():New()
oWS:cUSERTOKEN        := "TOTVS"
oWS:cID_ENT           := cIdEnt
oWS:oWSNFEID          := NFESBRA_NFES2():New()
oWS:oWSNFEID:oWSNotas := NFESBRA_ARRAYOFNFESID2():New()
aadd(oWS:oWSNFEID:oWSNotas:oWSNFESID2,NFESBRA_NFESID2():New())
Atail(oWS:oWSNFEID:oWSNotas:oWSNFESID2):cID := cIdNfe
oWS:nDIASPARAEXCLUSAO := 0
oWS:_URL          := AllTrim(cURL)+"/NFeSBRA.apw"			

If oWS:RETORNANOTAS()
	If Len(oWs:oWsRetornaNotasResult:OWSNOTAS:oWSNFES3) > 0
		If nTipo == 1
			Do Case
				Case oWs:oWsRetornaNotasResult:OWSNOTAS:oWSNFES3[1]:oWSNFECANCELADA <> Nil
					cRet := oWs:oWsRetornaNotasResult:OWSNOTAS:oWSNFES3[1]:oWSNFECANCELADA:cXML
				OtherWise
					cRet := oWs:oWsRetornaNotasResult:OWSNOTAS:oWSNFES3[1]:oWSNFE:cXML
			EndCase
		Else
			cMsg := AllTrim(oWs:oWsRetornaNotasResult:OWSNOTAS:oWSNFES3[1]:oWSNFE:cXML)
			
			If !Empty(cMsg)		
				oWS:= WSNFeSBRA():New()
				oWS:cUSERTOKEN     := "TOTVS"
				oWS:cID_ENT        := cIdEnt
				oWs:oWsNFe:oWSNOTAS:=  NFeSBRA_ARRAYOFNFeS():New()
				aadd(oWs:oWsNFe:oWSNOTAS:oWSNFeS,NFeSBRA_NFeS():New())
				oWs:oWsNFe:oWSNOTAS:oWsNFes[1]:cID := cIdNfe
				oWs:oWsNFe:oWSNOTAS:oWsNFes[1]:cXML:= EncodeUtf8(cMsg)
				oWS:_URL          := AllTrim(cURL)+"/NFeSBRA.apw"
			
				If Valtype(cRet) <> "U"	
					If oWS:Schema()
				  		If Empty(oWS:oWSSCHEMARESULT:oWSNFES4[1]:cMENSAGEM)
				  			cRet 	:= STR0038 //"Aguardando Retorno do Sefaz"
				  			cStatus := "2"
				  			cXML  	:= oWS:oWSSCHEMARESULT:oWSNFES4[1]:cXML 
				  			cCodSt  := "000"
				  		Else 
				 			cRet 	:= STR0039 //"Falha no Schema"
				  	   		cStatus := "1"
				  	   		cXML  	:= oWS:oWSSCHEMARESULT:oWSNFES4[1]:cMENSAGEM 
				  	   		cCodSt  := "212"
				  		EndIf
				 	Else
				 		cRet 	:= STR0040 //"Falha na instancia��o do Schema"
				 		cStatus := "1"  
				 		cXML 	:= IIf(Empty(GetWscError(3)),GetWscError(1),GetWscError(3))
				 		cCodSt  := "999"
				 	EndIf      
			  	Else			
				 	If oWS:Schema()
						If Empty(oWS:oWSSCHEMARESULT:oWSNFES4[1]:cMENSAGEM)
				  			cRet 	:= STR0041 //"Aguardando Retorno do Sefaz"
				  			cStatus := "2"
				  			cXML  	:= oWS:oWSSCHEMARESULT:oWSNFES4[1]:cXML 
				  			cCodSt  := "000"
						Else 									
						    If Empty(oWS:oWSSCHEMARESULT:oWSNFES4[1]:cMENSAGEM)
					  			cRet 	:= STR0041 //"Aguardando Retorno do Sefaz"
					  			cStatus := "2"
					  			cXML  	:= oWS:oWSSCHEMARESULT:oWSNFES4[1]:cXML 
					  			cCodSt  := "000"
							Else 
					 			cRet 	:= STR0039 //"Falha no Schema"
					  	   		cStatus := "1"
					  	   		cXML  	:= oWS:oWSSCHEMARESULT:oWSNFES4[1]:cMENSAGEM
					  	   		cCodSt  := "212" 
							EndIf															
						EndIf
					Else
				 		cRet 	:= STR0040 //"Falha na instancia��o do Schema"
				 		cStatus := "1"  
				 		cXML 	:= IIf(Empty(GetWscError(3)),GetWscError(1),GetWscError(3))
				 		cCodSt  := "999"
					EndIf 
				EndIf
			EndIf
		EndIf
	EndIf
Else
	cRet 	:= STR0042 //"Falha no m�todo RETORNANOTAS" 
	cStatus := "1" 
	cXML	:= IIf(Empty(GetWscError(3)),GetWscError(1),GetWscError(3))
	cCodSt  := "999"
EndIf  

Return .T.  

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFDanfe
Rotina de chamada do WS de impressao da DANFE 
*Fun��o Original: SpedDanfe(Eduardo Riera - 27.06.2007)

@author Evandro dos Santos Oliveira
@since 08/04/2014
@version 1.0	

@return Nil
/*/
//-------------------------------------------------------------------
Function TAFDanfe()

Local cIdEnt := TAFRIdEnt()
Local aIndArq   := {}
Local oDanfe
Local nHRes  := 0
Local nVRes  := 0
Local nDevice
Local cFilePrint := "DANFE_"+cIdEnt+Dtos(MSDate())+StrTran(Time(),":","")
Local oSetup
Local aDevice  := {}
Local cSession     := GetPrinterSession()
Local nRet := 0


AADD(aDevice,"DISCO") // 1
AADD(aDevice,"SPOOL") // 2
AADD(aDevice,"EMAIL") // 3
AADD(aDevice,"EXCEL") // 4
AADD(aDevice,"HTML" ) // 5
AADD(aDevice,"PDF"  ) // 6

                                                                        
nLocal       	:= If(fwGetProfString(cSession,"LOCAL","SERVER",.T.)=="SERVER",1,2 )
nOrientation 	:= If(fwGetProfString(cSession,"ORIENTATION","PORTRAIT",.T.)=="PORTRAIT",1,2)
cDevice     	:= If(Empty(fwGetProfString(cSession,"PRINTTYPE","SPOOL",.T.)),"PDF",fwGetProfString(cSession,"PRINTTYPE","SPOOL",.T.))
nPrintType      := aScan(aDevice,{|x| x == cDevice })

If TAFCTSpd()

	lAdjustToLegacy := .F. // Inibe legado de resolu��o com a TMSPrinter
	oDanfe := FWMSPrinter():New(cFilePrint, IMP_PDF, lAdjustToLegacy, /*cPathInServer*/, .T.)
	
	// ----------------------------------------------
	// Cria e exibe tela de Setup Customizavel
	// OBS: Utilizar include "FWPrintSetup.ch"
	// ----------------------------------------------
	//nFlags := PD_ISTOTVSPRINTER+ PD_DISABLEORIENTATION + PD_DISABLEPAPERSIZE + PD_DISABLEPREVIEW + PD_DISABLEMARGIN
	nFlags := PD_ISTOTVSPRINTER + PD_DISABLEPAPERSIZE + PD_DISABLEPREVIEW + PD_DISABLEMARGIN
	If ( !oDanfe:lInJob )
		oSetup := FWPrintSetup():New(nFlags, "DANFE")
		// ----------------------------------------------
		// Define saida
		// ----------------------------------------------
		oSetup:SetPropert(PD_PRINTTYPE   , nPrintType)
		oSetup:SetPropert(PD_ORIENTATION , nOrientation)
		oSetup:SetPropert(PD_DESTINATION , nLocal)
		oSetup:SetPropert(PD_MARGIN      , {60,60,60,60})
		oSetup:SetPropert(PD_PAPERSIZE   , 2)

	EndIf
	
	// ----------------------------------------------
	// Pressionado bot�o OK na tela de Setup
	// ----------------------------------------------
	If oSetup:Activate() == PD_OK // PD_OK =1
		//�������������������������������������������Ŀ
		//�Salva os Parametros no Profile             �
		//���������������������������������������������

        fwWriteProfString( cSession, "LOCAL"      , If(oSetup:GetProperty(PD_DESTINATION)==1 ,"SERVER"    ,"CLIENT"    ), .T. )
        fwWriteProfString( cSession, "PRINTTYPE"  , If(oSetup:GetProperty(PD_PRINTTYPE)==2   ,"SPOOL"     ,"PDF"       ), .T. )
        fwWriteProfString( cSession, "ORIENTATION", If(oSetup:GetProperty(PD_ORIENTATION)==1 ,"PORTRAIT"  ,"LANDSCAPE" ), .T. )
		
		// Configura o objeto de impress�o com o que foi configurado na interface.
        oDanfe:setCopies( val( oSetup:cQtdCopia ) )
		
		If oSetup:GetProperty(PD_ORIENTATION) == 1
		//�������������������������������������������Ŀ
			//�Danfe Retrato DANFEII.PRW                  �
			//���������������������������������������������	
			If Findfunction("u_PrtDfTAF")
				u_PrtDfTAF(cIdEnt,,,oDanfe, oSetup, cFilePrint)	
			Else 
				oDanfe := Nil
				oSetup := Nil
				Return() 
			EndIf		
		Else
			//�������������������������������������������Ŀ
			//�Danfe Paisagem DANFEIII.PRW                �
			//��������������������������������������������� 
			If Findfunction("u_PrtDanf3")
				u_PrtDanf3(cIdEnt,,,oDanfe, oSetup)
			Else  
				oDanfe := Nil
				oSetup := Nil
				Return() 
			EndIf
		EndIf
		Pergunte("TAFDANFE",.F.)			
	Else
		MsgInfo(STR0037)
		Pergunte("TAFDANFE",.F.)
   		Return
	Endif
EndIf
oDanfe := Nil
oSetup := Nil
Return()             

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFNFeStat()
Analisa o Status do Servico da NFe  
*Fun��o Original: SpedNFeStatus(Eduardo Riera - 18.06.2007)

@author Evandro dos Santos Oliveira
@since 08/04/2014
@version 1.0	

@return Nil
/*/
//-------------------------------------------------------------------
Function TAFNFeStat()

Local oWS
Local oDlg
Local oGet
Local cIdEnt     := ""
Local cURL       := ""
Local cStatus    := ""
Local cAuditoria := ""
Local aSize      := {}
Local aXML       := {}
Local nX         := 0

If FindFunction("TafGetUrlTSS")
	cURL := PadR(TafGetUrlTSS(),250)
Else
	cURL := PadR(GetNewPar("MV_TAFSURL","http://"),250)
EndIf 
cURL := AllTrim(cURL)

If TAFCTSpd()   
	/*+------------------------------+
	  |  Obtem o codigo da entidade  | 
	  +------------------------------+*/ 
	cIdEnt := TAFRIdEnt()
	If !Empty(cIdEnt)
		/*+------------------------------+
		  |      Instancia a classe		 | 
		  +------------------------------+*/ 
		If !Empty(cIdEnt)
			oWS:= WSNFeSBRA():New()
			oWS:cUSERTOKEN := "TOTVS"
			oWS:cID_ENT    := cIdEnt
			oWS:_URL       := AllTrim(cURL)+"/NFeSBRA.apw"
			If oWS:MONITORSEFAZMODELO()
				aSize := MsAdvSize()
				aXML := oWS:oWsMonitorSefazModeloResult:OWSMONITORSTATUSSEFAZMODELO
				For nX := 1 To Len(aXML)
					cStatus += "- NFe"+CRLF
					cStatus += Space(6)+STR0006+": "+aXML[nX]:cVersaoMensagem+CRLF //"Vers�o da mensagem"
					cStatus += Space(6)+STR0007+": "+aXML[nX]:cStatusCodigo+"-"+aXML[nX]:cStatusMensagem+CRLF //"C�digo do Status"
	                cStatus += Space(6)+STR0008+": "+aXML[nX]:cUFOrigem+CRLF //"UF Origem"
	                If !Empty(aXML[nX]:cUFResposta)
		                cStatus += Space(6)+STR0038+": "+aXML[nX]:cUFResposta+CRLF //"UF Resposta"
		   			EndIf
	                If aXML[nX]:nTempoMedioSEF <> Nil
						cStatus += Space(6)+STR0010+": "+Str(aXML[nX]:nTempoMedioSEF,6)+CRLF+CRLF //"Tempo de espera"
					EndIf
					If !Empty(aXML[nX]:cMotivo)
						cStatus += Space(6)+STR0011+": "+aXML[nX]:cMotivo+CRLF+CRLF //"Motivo"
					EndIf
					If !Empty(aXML[nX]:cObservacao)
						cStatus += Space(6)+STR0012+": "+aXML[nX]:cObservacao+CRLF+CRLF //"Observa��o"
					EndIf
					If !Empty(aXML[nX]:cSugestao)
						cStatus += Space(6)+STR0005+": "+aXML[nX]:cSugestao+CRLF+CRLF //"Sugest�o"
					EndIf
					If !Empty(aXML[nX]:cLogAuditoria)
						cAuditoria += aXML[nX]:cLogAuditoria
					EndIf
				Next nX
										
				DEFINE MSDIALOG oDlg TITLE "SPED - NFe" From 017,000 to (aSize[6]/3),(aSize[5]/2) OF oMainWnd PIXEL
				@ 005,005 GET oGet VAR cStatus OF oDlg SIZE (aSize[3]/2)-005,(aSize[4]/3)-032 PIXEL MULTILINE
				oGet:lReadOnly := .T.
				@ (aSize[4]/3)-022,(aSize[3]/2)-040 BUTTON oBtn1 PROMPT STR0009   		ACTION oDlg:End() OF oDlg PIXEL SIZE 035,011 //"OK"
				@ (aSize[4]/3)-022,(aSize[3]/2)-080 BUTTON oBtn2 PROMPT STR0039   		ACTION (Aviso(STR0039,cAuditoria,{STR0009},3)) OF oDlg PIXEL SIZE 035,011 WHEN !Empty(cAuditoria)//"Detalhes"
				ACTIVATE MSDIALOG oDlg	CENTERED
			Else
				Aviso("TAF-NFE",IIf(Empty(GetWscError(3)),GetWscError(1),GetWscError(3)),{STR0009},3)
			EndIf	
		EndIf
	Else
		Aviso("TAF-NFE",STR0040,{STR0009},3)	//"Execute o m�dulo de configura��o do servi�o, antes de utilizar esta op��o!!!"
	EndIf
Else
	Aviso("TAF-NFE",STR0040,{STR0009},3) //"Execute o m�dulo de configura��o do servi�o, antes de utilizar esta op��o!!!"
EndIf

Return Nil

//---------------------------------------------------------------------------
/*/{Protheus.doc}  TAFCfAmb
Configura Ambiente do e-Socil e NFe 

@author Evandro dos Santos Oliveira
@since 24/03/2014
@version 1.0	

@param nAmb	 	- Numero do Ambiente (1-Producao,2-Homologacao)
@param nServico	- Numero do Servico  (1-E-Social,2-NFe)

@return Nil
/*/
//---------------------------------------------------------------------------- 
Function TAFCfAmb(nAmb,nServico)  

	Local oWs
	Local aPerg  	 := {}
	Local aParam 	 := {"","","",0}
	Local aCombo1	 := {}
	Local aCombo2	 := {}
	Local cCombo1	 := ""
	Local cCombo2	 := ""
	Local cIdEnt  	 := ""
	Local cURL   	 := ""
	Local cVerNfe	 := PadR(GetNewPar("MV_TAFVNFE","2.00"),4)
	Local cParNSePar := SM0->M0_CODIGO+SM0->M0_CODFIL+"TAFCfAmb"    
	Local lRet	 	 := .T.
	Default nServico := 2
	Default nAmb	 := 2 

	If FindFunction("TafGetUrlTSS")
		cURL := PadR(TafGetUrlTSS(),250)
	Else
		cURL := PadR(GetNewPar("MV_TAFSURL","http://"),250)
	EndIf 
	cURL := AllTrim(cURL)

	If nServico == 1  
		/*+----------------------------------------------------+
		  | O Ambiente do e-social � controlado por par�metro  |
		  +----------------------------------------------------+*/  
		If  !PutMV("TAFAMB",AllTrim(Str(nAmb)))
			RecLock("SX6",.T.)
			SX6->X6_FIL     := xFilial( "SX6" )
			SX6->X6_VAR     := "MV_TAFAMB"
			SX6->X6_TIPO    := "C"
			SX6->X6_DESCRIC := STR0078
			MsUnLock()
			PutMV("MV_TAFAMB",AllTrim(Str(nAmb)))
		Else
			PutMV("MV_TAFAMB",AllTrim(Str(nAmb)))	
		EndIf
	ElseIf nServico == 2
	
		If TAFCTSpd()
	   		cIdEnt := TAFRIdEnt()  
					
			oWS := WsSpedCfgNFe():New()
			oWS:cUSERTOKEN 	:= "TOTVS"
			oWS:cID_ENT    	:= cIdEnt
			oWS:nAmbiente  	:= nAmb	
			oWS:_URL       	:= AllTrim(cURL)+"/SPEDCFGNFe.apw"
			lOk  			:= oWS:CFGAMBIENTE()
			cAmb 			:= oWS:cCfgAmbienteResult

			oWS2 := WsSpedCfgNFe():New()    
			oWS2:cUSERTOKEN := "TOTVS"
			oWS2:cID_ENT    := cIdEnt
			oWS2:cVersao    := cVerNfe
			oWS2:cModelo	:= "55"
			oWS2:CFGVersao()
		Else 
			Aviso("TAF-NFE",STR0040,{STR0009},3) //"Execute o m�dulo de configura��o do servi�o, antes de utilizar esta op��o!!!"
			lRet := .F.
		EndIf
	EndIf
	
Return (lRet)    

//---------------------------------------------------------------------------
/*/{Protheus.doc}  TAFCancNFe
Rotina de Cancelamento de NFe no SEFAZ.

@author Evandro dos Santos Oliveira
@since 24/03/2014
@version 1.0	

@param Nil

@return Nil
/*/
//---------------------------------------------------------------------------- 
Function TAFCancNFe() 

	Local cXjust  	  := "" 
	Local cModelo 	  := "55"
	Local cUFCob  	  := "" 
	Local cModal	  := "" 
	Local cSerie	  := ""
	Local cNota 	  := ""    
	Local lOk		  := .F.      
	Local cXml		  := ""         
	Local cChave	  := ""
	Local cURL		  := ""  
	Local cRecCU0	  := ""    

	If FindFunction("TafGetUrlTSS")
		cUrl := PadR(TafGetUrlTSS(),250)
	Else
		cUrl := PadR(GetNewPar("MV_TAFSURL","http://"),250)
	EndIf 
	cURL := AllTrim(cURL)
	
	cSerie := SubStr(AllTrim(C20->C20_SERIE),1,3)
	cNota  := StrZero(Val(C20->C20_NUMDOC),9)      
	
	dbSelectArea("CU0")
	dbSetOrder(6)
	If CU0->(MsSeek(xFilial("CU0")+cSerie+cNota+"000"))
		cAviso := STR0077 + " NFe: " + cNota + "-" + cSerie  
	Else                               
		If MsgYesNo(STR0074 + cSerie + cNota) == .T. 
			If Empty(C20->C20_DTCANC) .And. !Empty(C20->C20_PROTOC) 
				If TAFCTSpd()
			   		cIdEnt := TAFRIdEnt()
			 
			 		oWS := WsSpedCfgNFe():New()
					oWS:cUSERTOKEN := "TOTVS"
					oWS:cID_ENT    := cIdEnt
					oWS:nAmbiente  := 0	
					oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
					lOk := oWS:CFGAMBIENTE()
					
					If lOK
						oWS:cUSERTOKEN   := "TOTVS"
						oWS:cID_ENT      := cIdEnt
						oWS:nModalidade  := 0
						oWS:_URL         := AllTrim(cURL)+"/SPEDCFGNFe.apw"
						oWS:cModelo   	 := cModelo
						oWS:CFGModalidade()
						cModal	    	 := oWS:cCfgModalidadeResult 
					EndIf
					
					cModal := Substr(cModal,1,1)		
					cUFCob := Posicione("C09",1,xFilial("C09")+SM0->M0_ESTCOB,"C09_CODIGO")
					dbSelectArea("CU0")
					dbSetOrder(4)
					If MsSeek(xFilial("CU0")+C20->C20_CHVNF)
						cRecCU0 := CU0->(RECNO())
						If !(AllTrim(CU0->CU0_CODERR) $ (cCodDen+'101,102,220'))   
							  
							/*+----------------------------------------------------+
							  | Recupero o XML para pegar o ID da Nota garantindo  |
							  | a integridade do mesmo							   |
							  +----------------------------------------------------+*/
							oWs2:= WsNFeSBra():New()
							oWs2:cUserToken    		:= "TOTVS"
							oWs2:cID_ENT       		:= cIdEnt
							oWs2:_URL       		:= AllTrim(cURL)+"/NFeSBRA.apw" 
							oWs2:oWSNFEID          	:= NFESBRA_NFES2():New()
							oWs2:oWSNFEID:oWSNotas 	:= NFESBRA_ARRAYOFNFESID2():New()
							aAdd(oWS2:oWSNFEID:oWSNotas:oWSNFESID2,NFESBRA_NFESID2():New())
							Atail(oWS2:oWSNFEID:oWSNotas:oWSNFESID2):cID := Substr(AllTrim(C20->C20_SERIE),1,3)+StrZero(Val(C20->C20_NUMDOC),9)
							oWs2:nDIASPARAEXCLUSAO 	:= 0
							oWs2:_URL          	  	:= AllTrim(cURL)+"/NFeSBRA.apw"	   
					
							If oWs2:RETORNANOTAS()	
							 	If Len(oWs2:oWsRetornaNotasResult:OWSNOTAS:oWSNFES3) > 0
							   		cXml := oWs2:oWsRetornaNotasResult:OWSNOTAS:oWSNFES3[1]:oWSNFE:cXML
						   		EndIf  
						   	EndIf 	
						   	cChave := 'Id="' + StrTran(TAFIdNfe(cXml,"Id"),"NFe","") + '"'  
						   	
							oWs:= WsNFeSBra():New()
							oWs:cUserToken := "TOTVS"
							oWs:cID_ENT    := cIdEnt
							oWS:_URL       := AllTrim(cURL)+"/NFeSBRA.apw"
							oWs:oWsNFe:oWsNotas :=  NFESBRA_ARRAYOFNFES():New()	
							aAdd(oWs:oWsNFe:oWsNotas:oWSNFES,NFESBRA_NFES():New())   
				   			aTail(oWs:oWsNFe:oWsNotas:oWSNFES):cID := Substr(AllTrim(C20->C20_SERIE),1,3)+StrZero(Val(C20->C20_NUMDOC),9) 
				   			aTail(oWs:oWsNFe:oWsNotas:oWSNFES):cXML:= cChave
					
							If Empty(AllTrim(CU0->CU0_CODERR))
								cAviso := STR0044 + cNota + "-" + cSerie
							Else
								cAviso := STR0045 + cNota + "-" + cSerie
							EndIf
				
					  		If AllTrim(AllTrim(CU0->CU0_CODERR)) $ "100"
								If  !(Empty(AllTrim(CU0->CU0_CODERR)))
									While .T.
										If Aviso(cAviso, @cXjust, {STR0046, STR0047}, 3,STR0048,,, .T.) == 1 //"Confirmar"###"Cancelar"###Cancelamento de Nfe como Evento
											If TAFLimTxt(cXjust,250)
												aTail(oWs:oWsNFe:oWsNotas:oWSNFES):cXML += '<xJust>xJust="'+cXjust+'"<\xJust>'
												Exit
											Else
												Loop
											EndIf
										Else
											Exit
										Endif
									Enddo
								Else
									If Aviso(cAviso, @cXjust, {STR0046, STR0047}, 4,,,, .T.) == 1 .And. !Empty(cXjust) //"Confirmar"###"Cancelar"
										aTail(oWs:oWsNFe:oWsNotas:oWSNFES):cXML += '<xJust>xJust="'+cXjust+'"<\xJust>'
									Endif
								Endif	
							EndIf
			
							If oWs:CancelaNotas()
								oWs:= WsNFeSBra():New()
								oWs:cUserToken := "TOTVS"
								oWs:cID_ENT    := cIdEnt
								oWS:_URL       := AllTrim(cURL)+"/NFeSBRA.apw"
								oWs:oWsNFe:oWsNotas :=  NFESBRA_ARRAYOFNFES():New()
								nY := 0  
								   
								/*+-----------------------------------------------------+
								  | Crio um novo registro para controlar o cancelamento |
								  +-----------------------------------------------------+*/
								Begin Transaction     
								If RecLock("CU0",.T.)
									CU0->CU0_FILIAL := xFilial("CU0")
									CU0->CU0_ID     := GetSx8Num("CU0","CU0_ID")
									CU0->CU0_TPMOV  := + IIf(AllTrim(C20->C20_INDOPE) == "0",STR0080,STR0081) //" - Entrada"###" - Saida "
									CU0->CU0_SERIE  := cSerie
									CU0->CU0_NUMDOC := cNota
									CU0->CU0_CODSEQ := "000001"
									CU0->CU0_DTOCOR := dDataBase
									CU0->CU0_CODERR := "000"
									CU0->CU0_DCODER := STR0077
									CU0->CU0_XML	:= cXml      
									CU0->CU0_STATUS := "2"
									CU0->CU0_CHVNF	:= C20->C20_CHVNF
									CU0->(MsUnLock())
									ConfirmSX8() 
								EndIf
								End Transaction 
							Else
								MsgStop(STR0049) //"Erro ao chamar o m�todo CancelaNotas"
							EndIf    
						Else
							Aviso("TAF-NFE",STR0050 + C20->C20_CHVNF  + STR0051,{STR0009},3) //"Documento "###" est� cancelado ou denegada"###"Ok"
						EndIf
					Else
						Aviso("TAF-NFE",STR0050 + C20->C20_CHVNF  + STR0052,{STR0009},3)
					EndIf
				Else
			 		Aviso("TAF-NFE",STR0040,{STR0009},3) //"Execute o m�dulo de configura��o do servi�o, antes de utilizar esta op��o!!!"
			   	EndIf
			Else
				If !Empty(C20->C20_DTCANC) 
			   		Aviso("TAF-NFE",STR0072,{STR0009},3)  //"Nota j� Cancelada"
			 	Else
		   			Aviso("TAF-NFE",STR0073,{STR0009},3)  //"Nota n�o Transmida ou Danfe n�o gerada"
			 	EndIf
			EndIf
		EndIf  
	EndIf
  	
Return Nil 

//-------------------------------------------------------------------
/*/{Protheus.doc} TAFRIdEnt
Obtem o codigo da entidade apos enviar o post para o Totvs Service

@author Evandro dos Santos Oliveira
@since 21/11/2013
@version 1.0

@param lMatriz - Determina se deve posicionar na Filial Matriz
@param lExibeMsg - Determina se a rotina deve exibir uma caixa de alerta
				 caso ocorro erro na rotina
@param cMsgErro - Mesagem de Erro (caso houver)
@param lValidErpOk - Indica se n�o houve erro de valida��o do lado do ERP (.t. - nao ha erro) (referencia)
@param lTSSOk - Indica se houve erro do lado do TSS. (referencia) (.t. - n�o ha erros)

@return  cIdEnt - C�digo da Entidade
/*/
//-------------------------------------------------------------------
Function TAFRIdEnt(lMatriz,lExibeMsg,cMsgErro,lValidErpOk,lTSSOk,lEsocial)
Return(GetIdEnt(lMatriz,.T.,lExibeMsg,@cMsgErro,@lValidErpOk,@lTSSOk,lEsocial))

Static Function GetIdEnt(lMatriz,lNewId,lExibeMsg,cMsgErro,lValidErpOk,lTSSOk,lEsocial)

Local aArea  := SM0->(GetArea())
Local cIdEnt := ""
Local cURL   := ""
Local oWs
Local lUsaGesEmp := IIF(FindFunction("FWFilialName") .And. FindFunction("FWSizeFilial") .And. FWSizeFilial() > 2,.T.,.F.)
Local aInfoMatriz := {} 
Local lJob := IsBlind() 
Local cProdRural := ""
Local aProdRural := {}

Default lMatriz := .F.
Default lNewId 	:= .F.
Default lExibeMsg := .T.
Default cMsgErro 	:= ""
Default lValidErpOk := .F.
Default lTSSOk := .F. 
Default lEsocial    := .F.

If FindFunction("VProdRural") 
	 aProdRural := VProdRural()
	 cProdRural := aProdRural[2]
EndIf

If FindFunction("TafGetUrlTSS")
	cURL := PadR(TafGetUrlTSS(),250)
Else
	cURL := PadR(GetNewPar("MV_TAFSURL","http://"),250)
EndIf
cURL := AllTrim(cURL) 

If lMatriz
	aInfoMatriz := TAFGFilMatriz()
	If Len(aInfoMatriz) > 0 
		If !SM0->(MsSeek(aInfoMatriz[1]+aInfoMatriz[2]))
			If lJob
				If Findfunction("TafConOut")
					TafConOut("� necess�rio selecionar 1 Filial como Matriz no Complemento de Empresas.")
				Else
					ConOut("� necess�rio selecionar 1 Filial como Matriz no Complemento de Empresas.")
				EndIf
			Else
				If lExibeMsg
					Aviso("TOTVS SERVICE SOA","� necess�rio selecionar 1 Filial como Matriz no Complemento de Empresas.",{"OK"},3)
				Else
					cMsgErro := "� necess�rio selecionar 1 Filial como Matriz no Complemento de Empresas."
				EndIf 
			EndIf
		Else
			lValidErpOk := .T. 
		EndIf
	Else
		If lJob
			If Findfunction("TafConOut")
				TafConOut("N�o h� Filial Marcada como Matriz no Cadastro de Complemento de Empresas ou o CNPJ Base da Filial n�o � igual ao da Matriz.")
			Else
				ConOut("N�o h� Filial Marcada como Matriz no Cadastro de Complemento de Empresas ou o CNPJ Base da Filial n�o � igual ao da Matriz.")
			EndIf 
		Else
			If lExibeMsg
				Aviso("TOTVS SERVICE SOA","N�o h� Filial Marcada como Matriz no Cadastro de Complemento de Empresas ou o CNPJ Base da Filial n�o � igual ao da Matriz.",{"OK"},3)
			Else
				cMsgErro := "N�o h� Filial Marcada como Matriz no Cadastro de Complemento de Empresas ou o CNPJ Base da Filial n�o � igual ao da Matriz."
			EndIf 

		EndIf
	EndIf
Else
	lValidErpOk := .T. 
EndIf

If lValidErpOk
	//������������������������������������������������������������������������Ŀ
	//�Obtem o codigo da entidade                                              �
	//��������������������������������������������������������������������������
	If lNewId
		// Cria��o das propriedades com classe embarcada nesse fonte
		oWS := WSTAFSPEDADM():New()
	Else
		oWS := WSSPEDADM():New()	
	EndIf	
	oWS:cUSERTOKEN := "TOTVS"
		
	/*
	Realiza tratamento para cria��o de Entidade no TSS com CPF e manter 
	Entidade com CNPJ para envio de NFE na mesma filial.
	*/
	If !Empty(cProdRural) .And. lEsocial	
		oWS:OWSEMPRESA:cCPF����:=�cProdRural
	Else
	oWS:oWSEMPRESA:cCNPJ       := IIF(SM0->M0_TPINSC==2 .Or. Empty(SM0->M0_TPINSC),SM0->M0_CGC,"")	
		oWS:OWSEMPRESA:cCPF����:=�IIF(SM0->M0_TPINSC==3,AllTrim(SM0->M0_CGC),"")
	oWS:oWSEMPRESA:cIE         := SM0->M0_INSC
	EndIf
	oWS:oWSEMPRESA:cIM         := SM0->M0_INSCM		
	oWS:oWSEMPRESA:cNOME       := SM0->M0_NOMECOM
	oWS:oWSEMPRESA:cFANTASIA   := IIF(lUsaGesEmp,FWFilialName(),Alltrim(SM0->M0_NOME))
	oWS:oWSEMPRESA:cENDERECO   := FisGetEnd(SM0->M0_ENDENT)[1]
	oWS:oWSEMPRESA:cNUM        := FisGetEnd(SM0->M0_ENDENT)[3]
	oWS:oWSEMPRESA:cCOMPL      := FisGetEnd(SM0->M0_ENDENT)[4]
	oWS:oWSEMPRESA:cUF         := SM0->M0_ESTENT
	oWS:oWSEMPRESA:cCEP        := SM0->M0_CEPENT
	oWS:oWSEMPRESA:cCOD_MUN    := SM0->M0_CODMUN
	oWS:oWSEMPRESA:cCOD_PAIS   := "1058"
	oWS:oWSEMPRESA:cBAIRRO     := SM0->M0_BAIRENT
	oWS:oWSEMPRESA:cMUN        := SM0->M0_CIDENT
	oWS:oWSEMPRESA:cCEP_CP     := Nil
	oWS:oWSEMPRESA:cCP         := Nil
	oWS:oWSEMPRESA:cDDD        := Str(FisGetTel(SM0->M0_TEL)[2],3)
	oWS:oWSEMPRESA:cFONE       := AllTrim(Str(FisGetTel(SM0->M0_TEL)[3],15))
	oWS:oWSEMPRESA:cFAX        := AllTrim(Str(FisGetTel(SM0->M0_FAX)[3],15))
	oWS:oWSEMPRESA:cEMAIL      := UsrRetMail(RetCodUsr())  
	oWS:oWSEMPRESA:cNIRE       := SM0->M0_NIRE
	oWS:oWSEMPRESA:dDTRE       := SM0->M0_DTRE
	oWS:oWSEMPRESA:cNIT        := IIF(SM0->M0_TPINSC==1,SM0->M0_CGC,"")
	oWS:oWSEMPRESA:cINDSITESP  := ""
	oWS:oWSEMPRESA:cID_MATRIZ  := ""

	If lUsaGesEmp 
		oWS:oWSEMPRESA:CIDEMPRESA:= FwGrpCompany()+FwCodFil()
	EndIf

	If lNewId
		oWS:oWSOUTRASINSCRICOES:oWSInscricao := TAFADM_ARRAYOFSPED_GENERICSTRUCT():New()
	Else
		oWS:oWSOUTRASINSCRICOES:oWSInscricao := SPEDADM_ARRAYOFSPED_GENERICSTRUCT():New()
	EndIf
	oWS:_URL := AllTrim(cURL)+"/SPEDADM.apw"

	// Tratamento para inclusao do campo de emissor/responsavel
	If TAFColumnPos( "C1E_CNPJTR" )
		// Verifica a fun��o do client do SPEDADM(sped_wscadm.prw) 
		// e protege para a nova propriedade n�o gerar error log.
		If lNewId .Or. (FindFunction("U_TAFSPEDADM") .And. U_TAFSPEDADM() >= "20180227")
			oWS:oWSEMPRESA:cUPDINSCRTR	:= "S"
		Else
			// Se for chamado do wizardo, apresenta mensagem
			If IsInCallStack("TAFAWizd")
				If lJob
					If Findfunction("TafConOut")
						TafConOut("A vers�o dos programas de uso do TSS est�o desatualizadas.")
					Else
						ConOut("A vers�o dos programas de uso do TSS est�o desatualizadas.")
					EndIf 
				Else
					Aviso("Configura��o TSS","A vers�o dos programas de uso do TSS est�o desatualizadas.",{STR0009},2)
				EndIf			
			EndIf	
		EndIf

		oWS:oWSEMPRESA:cINSCRTRA   	:= GetInscTra()
	EndIf

	If oWs:ADMEMPRESAS()
		cIdEnt  := oWs:cADMEMPRESASRESULT
		lTSSOk := .T. 
	Else
		// Retirada a recursividade desnecess�ria.
		// cIdEnt := GetIdEnt( lMatriz, .F. )	
		//Tratamento para mensagens mais amig�veis
		cMsgErro := IIf(Empty(GetWscError(3)),GetWscError(1),GetWscError(3))
		If "WSCERR044" $ cMsgErro
			cMsgErro := "Falha ao tentar se conectar ao TSS."+CRLF+CRLF
			cMsgErro += "Configura��es usadas:"+CRLF
			cMsgErro += "Url Totvs Service SOA: "+ AllTrim(cURL) +CRLF+CRLF
			cMsgErro += "Verifique as configura��es do servidor e se o mesmo est� ativo."
		EndIf

		If lExibeMsg .And. !lJob
			Aviso("Conex�o TSS - Totvs Service SOA",cMsgErro,{STR0009},3)
		Else
			If Findfunction("TafConOut")
				TafConOut(cMsgErro)
			Else
				ConOut(cMsgErro)
			EndIf 
		EndIf 	
	EndIf
	
	FreeObj(oWS)
	oWS := Nil 
	

EndIf 

RestArea(aArea)


Return(cIdEnt) 

//---------------------------------------------------------------------------
/*/{Protheus.doc} GetInscTra
Funcao que coleta informa��o do cadastro de complemento de empresas para informar o CNPJ do transmissor quando for diferente do empregador

@author Roberto Souza
@since 20/12/2017
@version 1.0	

@return cResult - CNPJ do Transmissor, se houver 
/*/
//---------------------------------------------------------------------------- 
Static Function GetInscTra()
	Local cInfo := ""
	Local aArea := GetArea()
	
	DbSelectArea("C1E")
	DbSetOrder(3)

	If C1E->( DbSeek( xFilial( "C1E" ) + PadR( SM0->M0_CODFIL , TamSX3( 'C1E_FILTAF' )[1] ) + '1' ) )
		cInfo := C1E->C1E_CNPJTR //"CNPJ" do transmissor
	EndIf
	
	RestArea( aArea )
Return( cInfo )

//---------------------------------------------------------------------------
/*/{Protheus.doc} TAFCDate
Funcao de conversao de data para string em varios formatos 
*Fun��o Original: FsDateConv (Eduardo Rieira - 20.10.2006)

@author Evandro dos Santos Oliveira
@since 24/03/2014
@version 1.0	

@param xData - Data a ser convertida
@param cMasc - Formato onde: DD = Dia; MM = Mes; YYYY ou YY = Ano   

@return cResult - Data Convertida 
/*/
//---------------------------------------------------------------------------- 
Function TAFCDate(xData,cMasc)

Local cDia    := ""
Local cMes    := ""
Local cAno    := ""
Local cData   := ""
Local cResult := ""
Local cAux    := ""

DEFAULT cMasc := "DDMMYYYY"    

cData := IIf (ValType(xData) == "D",DtoS(xData),xData)   

cDia := SubStr(cData,7,2)
cMes := SubStr(cData,5,2)
cAno := SubStr(cData,1,4)

While !Empty(cMasc)
	cAux := SubStr(cMasc,1,2)
	Do Case
		Case cAux == "DD"
			cResult += cDia
		Case cAux == "MM"
			cResult += cMes
		Case cAux == "YY"
			If SubStr(cMasc,1,4) == "YYYY"
				cResult += cAno
				cMasc := SubStr(cMasc,3)
			Else
				cResult += SubStr(cAno,3)
			EndIf			
	EndCase
	cMasc := SubStr(cMasc,3)
EndDo
Return(cResult)  

//---------------------------------------------------------------------------- 
/*/{Protheus.doc}  TAFInverte
Gera Chave de Acesso (cNf)
*Fun��o Original:  Inverte (Autor: N�o Informado)

@author Evandro dos Santos Oliveira
@since 24/03/2014
@param uCpo	 - Campo a ser Convertido

@return cRet - Campo Convertido
/*/
//---------------------------------------------------------------------------- 
Function TAFInverte(uCpo)

Local cCpo	:= uCpo
Local cRet	:= ""
Local cByte	:= ""
Local nAsc	:= 0
Local nI		:= 0
Local aChar	:= {}
Local nDiv	:= 0

Aadd(aChar,	{"0", "9"})
Aadd(aChar,	{"1", "8"})
Aadd(aChar,	{"2", "7"})
Aadd(aChar,	{"3", "6"})
Aadd(aChar,	{"4", "5"})
Aadd(aChar,	{"5", "4"})
Aadd(aChar,	{"6", "3"})
Aadd(aChar,	{"7", "2"})
Aadd(aChar,	{"8", "1"})
Aadd(aChar,	{"9", "0"})

For nI:= 1 to Len(cCpo)
   cByte := Upper(Subs(cCpo,nI,1))
   If (Asc(cByte) >= 48 .And. Asc(cByte) <= 57) .Or. ;	// 0 a 9
   		(Asc(cByte) >= 65 .And. Asc(cByte) <= 90) .Or. ;	// A a Z
   		Empty(cByte)	// " "
	   nAsc	:= Ascan(aChar,{|x| x[1] == cByte})
   	If nAsc > 0
   		cRet := cRet + aChar[nAsc,2]	// Funcao Inverte e chamada pelo rdmake de conversao
	   EndIf
	Else
		// Caracteres <> letras e numeros: mantem o caracter
		cRet := cRet + cByte
	EndIf
Next          

Return(cRet) 

//-----------------------------------------------------------------------
/*/{Protheus.doc} TAFLimTxt
*Fun��o Original: SpedCCeTexto - (Sergio S. Fuzinaka - 04.08.2011) 
Fun��o que valida o tamanho minimo do texto

@author Evandro dos Santos Oliveira
@since 24.04.2014
@version 1.00

@param	cTexto  - Texto a ser tratado
@param  nLimTam - Limite de Tamanho

/*/
//-----------------------------------------------------------------------
Function TAFLimTxt(cTexto,nLimTam)

Local lRet := .T.

Default nLimTam := 1000

// Tratamento do Texto para correcao da carta
cTexto := Alltrim(cTexto)
cTexto := StrTran(cTexto,Chr(10),"")
cTexto := StrTran(cTexto,Chr(13),"")

If Len(cTexto) > nLimTam
	cTexto := Left(cTexto,nLimTam)
Endif

If Len(cTexto) < 15
	MsgAlert(STR0043+cValToChar(nLimTam)+"  ","TAF") //"Texto com m�nimo de 15 caracteres e m�ximo de "        
	lRet := .F.
Endif

Return( lRet )

//-----------------------------------------------------------------------
/*/{Protheus.doc} TAFNFePar
*Fun��o Original: SpedNFePar - (Eduardo Riera - 18.06.2007) 
Fun��o que valida o tamanho minimo do texto

@author Evandro dos Santos Oliveira
@since 22.05.2014
@version 1.00

/*/
//-----------------------------------------------------------------------
Function TAFNFePar()

Local oWs
Local aPerg  := {}
Local aParam := {"","","",0}
Local aCombo1:= {}
Local aCombo2:= {}
Local aCombo3:= {}
Local aCombo4:= {}
Local aCombo5:= {}
Local cCombo1:= ""
Local cCombo2:= ""
Local cCombo3:= ""
Local cCombo4:= ""
Local cCombo5:= ""
Local cIdEnt := ""
Local cURL   := ""
Local nTempo := 0
Local cParNfePar := SM0->M0_CODIGO+SM0->M0_CODFIL+"SPEDNFEPAR"
Local cAvisoCont := Space(1000)
Local cModel	 := "55"
Local nOpcCont   := 0
Local lAdminTaf  := GetNewPar("MV_TAFADM",.T.)

aAdd(aCombo1,STR0055) //"1-Produ��o"
aAdd(aCombo1,STR0056) //"2-Homologa��o"
aAdd(aCombo2,STR0057) //"1-Normal"
aAdd(aCombo2,STR0058) //"2-Conting�ncia off-line"
aAdd(aCombo2,STR0059) //"3-Conting�ncia SCAN"
aAdd(aCombo2,STR0060) //"4-Ambiente Nacional"
aAdd(aCombo2,STR0061) //"5-Contig�ncia DPEC"
aAdd(aCombo2,STR0062) //"6-Sefaz Virtual"
aAdd(aCombo2,STR0063) //"7-Conting�ncia FS-DA"

aAdd(aCombo3,"2.00")
aAdd(aCombo4,"1.04")
aAdd(aCombo5,"1.01") 

If FindFunction("TafGetUrlTSS")
	cURL := PadR(TafGetUrlTSS(),250)
Else
	cURL := PadR(GetNewPar("MV_TAFSURL","http://"),250)
EndIf 
cURL := AllTrim(cURL)

If (IIf(lAdminTaf,PswAdmin( /*cUser*/, /*cPsw*/,RetCodUsr()) == 0,.T.))
	If TAFCTSpd() 
		If !Empty(cURL)
			//������������������������������������������������������������������������Ŀ
			//�Obtem o codigo da entidade                                              �
			//��������������������������������������������������������������������������
			
			cIdEnt := TAFRIdEnt()
			
			//������������������������������������������������������������������������Ŀ
			//�Obtem o ambiente                                                        �
			//��������������������������������������������������������������������������
			oWS := WsSpedCfgNFe():New()
			oWS:cUSERTOKEN := "TOTVS"
			oWS:cID_ENT    := cIdEnt
			oWS:nAmbiente  := 0
			oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
			oWS:CFGAMBIENTE()
			cCombo1 := oWS:cCfgAmbienteResult
			
			aAdd(aPerg,{2,STR0015,cCombo1,aCombo1,120,".T.",.T.,".T."}) //"Ambiente"
			//������������������������������������������������������������������������Ŀ
			//�Obtem a modalidade                                                      �
			//��������������������������������������������������������������������������
			oWS:cUSERTOKEN := "TOTVS"
			oWS:cID_ENT    := cIdEnt
			oWS:nModalidade:= 0
			oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
			oWS:cModelo   := cModel
			oWS:CFGModalidade()
			cCombo2 := PADR(oWS:cCfgModalidadeResult,25)
			
			aAdd(aPerg,{2,STR0064,cCombo2,aCombo2,120,".T.",.T.,".T."}) //"Modalidade"
			//������������������������������������������������������������������������Ŀ
			//�Obtem a versao da NFe                                                   �
			//��������������������������������������������������������������������������
			oWS:cUSERTOKEN := "TOTVS"
			oWS:cID_ENT    := cIdEnt
			oWS:cVersao    := "0.00"
			oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
			oWS:CFGVersao()
			cCombo3 := oWS:cCfgVersaoResult
			aAdd(aPerg,{2,STR0065,cCombo3,aCombo3,120,".T.",.T.,".T."}) //"Vers�o"
	
			//������������������������������������������������������������������������Ŀ
			//�Obtem a versao do DPEC                                                  �
			//��������������������������������������������������������������������������
			oWS:cUSERTOKEN := "TOTVS"
			oWS:cID_ENT    := cIdEnt
			oWS:cVersao    := "0.00"
			oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
			oWS:CfgVersaoDpec()
			cCombo5 := oWS:cCfgVersaoDpecResult
			aAdd(aPerg,{2,STR0066,cCombo5,aCombo5,120,".T.",.T.,".T."}) //"Vers�o DPEC"
			//������������������������������������������������������������������������Ŀ
			//�Obtem o tempo de espera                                                 �
			//��������������������������������������������������������������������������
			oWS:cUSERTOKEN  := "TOTVS"
			oWS:cID_ENT     := cIdEnt
			oWS:nTempoEspera:= 0
			oWS:_URL        := AllTrim(cURL)+"/SPEDCFGNFe.apw"
			oWS:CFGTempoEspera()
			nTempo := oWS:nCfgTempoEsperaResult
			
			aAdd(aPerg,{1,STR0067,nTempo,"99",".T.","",".T.",30,.F.})		//"Tempo de espera"
			
			aParam := {SubStr(cCombo1,1,1),SubStr(cCombo2,1,1),cCombo3,cCombo4,cCombo5,nTempo}
			If ParamBox(aPerg,"TAF SPED - NFe",aParam,,,,,,,cParNfePar,.T.,.F.)
				lContingencia	:=	Val(aParam[2])==2 .Or. Val(aParam[2])==3 .Or. Val(aParam[2])==5 .Or. Val(aParam[2])==7
				
				If lContingencia .And. Val(cCombo2)<>Val(aParam[2])
					While Empty(cAvisoCont) .And. nOpcCont<=1
						nOpcCont	:=	Aviso("TAF-NFE" + STR0068,@cAvisoCont,{STR0046,STR0047},3,,,,.T.)
					End
					If nOpcCont==2
						Aviso("TAF-NFE" + STR0068,STR0069+AllTrim(aParam[2])+STR0070,{STR0009},3)
					EndIf
				EndIf
				oWS := WsSpedCfgNFe():New()
				oWS:cUSERTOKEN := "TOTVS"
				oWS:cID_ENT    := cIdEnt
				oWS:nAmbiente  := Val(aParam[1])
				oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
				oWS:CFGAMBIENTE()
				
				If !lContingencia .Or. (lContingencia .And. nOpcCont==1 .And. !Empty(cAvisoCont))
					oWS:cUSERTOKEN := "TOTVS"
					oWS:cID_ENT    := cIdEnt
					oWS:nModalidade:= Val(aParam[2])
					oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
					oWS:CfgModalidGrv()
				EndIf
				
				oWS:cUSERTOKEN := "TOTVS"
				oWS:cID_ENT    := cIdEnt
				oWS:cVersao    := aParam[3]
				oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
				oWS:CFGVersao()
							
				oWS:cUSERTOKEN := "TOTVS"
				oWS:cID_ENT    := cIdEnt
				oWS:cVersao    := aParam[5]
				oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
				oWS:CFGVersaoDpec()
				oWS:cUSERTOKEN  := "TOTVS"
				oWS:cID_ENT     := cIdEnt
				oWS:nTempoEspera:= aParam[5]
				oWS:_URL        := AllTrim(cURL)+"/SPEDCFGNFe.apw"
				oWS:CFGTempoEspera()
				If !lContingencia  .Or. (lContingencia .And. nOpcCont==1 .And. !Empty(cAvisoCont))
					//-- Ct-e ativo.
					If !Empty(cAvisoCont)
						oWS:cUSERTOKEN := "TOTVS"
						oWS:cID_ENT    := cIdEnt
						oWS:nModalidade:= Val(aParam[2])
						oWS:cMotivo    := cAvisoCont
						oWS:_URL       := AllTrim(cURL)+"/SPEDCFGNFe.apw"
						oWS:CfgModalidGrv()
					EndIf
				EndIf
			EndIf
		Else
			Aviso("TAF-NFE",STR0071,{STR0009},3)//"Par�metro MV_TAFSURL n�o configurado"
		EndIf
	Else
		Aviso("TAF-NFE",STR0020,{STR0009},3) //"Execute o m�dulo de configura��o do servi�o, antes de utilizar esta op��o!!!"
	EndIf
Else
	Help( "", 1, "SEMPERM" )
EndIf

Return Nil   

//---------------------------------------------------------------------------
/*/{Protheus.doc}  TAFIdNfe
Retorna a Chave Eletronica da NFe

@author Evandro dos Santos Oliveira
@since 23/05/2014
@version 1.0	

@param cXml   - Xml na NFe
@param cAttId - Id do atributo a ser recuperado

@return cUri  - Chave Eletronica
/*/
//---------------------------------------------------------------------------- 
Function TAFIdNfe(cXML,cAttId)  

	Local nAt  := 0
	Local cURI := ""
	Local nSoma:= Len(cAttId)+2
	
	nAt := At(cAttId+'=',cXml)
	cURI:= SubStr(cXml,nAt+nSoma)
	nAt := At('"',cURI)
	If nAt == 0
		nAt := At("'",cURI)
	EndIf
	cURI:= SubStr(cURI,1,nAt-1) 
	
Return(cUri)  

//*********************************************************************************** 
//*********************************************************************************** 
//*******                 ********       ***********            ********************* 
//*******                 *******         **********            ********************* 
//*************     ************     *     *********     **************************** 
//*************     ***********     ***     ********     **************************** 
//*************     **********               *******         ************************ 
//*************     *********     *******     ******     **************************** 
//*************     ********     *********     *****     **************************** 
//*************     *******     ***********     ****     **************************** 
//*********************************************************************************** 
//*********************************************************************************** 
/* ===============================================================================
WSDL Location    http://localhost:8080/SPEDADM.apw?WSDL
Gerado em        02/27/18 11:30:26
Observa��es      C�digo-Fonte gerado por ADVPL WSDL Client 1.120703
                 Altera��es neste arquivo podem causar funcionamento incorreto
                 e ser�o perdidas caso o c�digo-fonte seja gerado novamente.
=============================================================================== */

/* -------------------------------------------------------------------------------
WSDL Service WSTAFSPEDADM
------------------------------------------------------------------------------- */

WSCLIENT WSTAFSPEDADM

	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD RESET
	WSMETHOD CLONE
	WSMETHOD ADDENTFILE
	WSMETHOD ADMEMISS
	WSMETHOD ADMEMPRESAS
	WSMETHOD CONSULTEMISS
	WSMETHOD ENTIDADEATIVA
	WSMETHOD ENTIDADECLEAR
	WSMETHOD GETADMEMPRESAS
	WSMETHOD GETADMEMPRESASID
	WSMETHOD GETINFOPEDNFE
	WSMETHOD GETPASSENT

	WSDATA   _URL                      AS String
	WSDATA   _HEADOUT                  AS Array of String
	WSDATA   _COOKIES                  AS Array of String
	WSDATA   cUSERTOKEN                AS string
	WSDATA   cKEYPASS                  AS string
	WSDATA   cCNPJ                     AS string
	WSDATA   cNOME                     AS string
	WSDATA   cADDENTFILERESULT         AS string
	WSDATA   cID_ENT                   AS string
	WSDATA   cCPF                      AS string
	WSDATA   cIE                       AS string
	WSDATA   cUF                       AS string
	WSDATA   nOPC                      AS integer
	WSDATA   oWSARQUIVO                AS TAFADM_ARQUIVOAUT
	WSDATA   cPARTNER                  AS string
	WSDATA   oWSADMEMISSRESULT         AS TAFADM_AUTEMP_EMISSAO
	WSDATA   oWSEMPRESA                AS TAFADM_SPED_ENTIDADE
	WSDATA   oWSOUTRASINSCRICOES       AS TAFADM_SPED_ENTIDADEREFERENCIAL
	WSDATA   cADMEMPRESASRESULT        AS string
	WSDATA   cPEDIDO                   AS string
	WSDATA   cIDINICIAL                AS string
	WSDATA   cIDFINAL                  AS string
	WSDATA   oWSCONSULTEMISSRESULT     AS TAFADM_ARRAYOFCONTROLENFEEMISS
	WSDATA   cENTIDADEATIVARESULT      AS string
	WSDATA   cENTIDADECLEARRESULT      AS string
	WSDATA   cIDEMPRESA                AS string
	WSDATA   oWSGETADMEMPRESASRESULT   AS TAFADM_ARRAYOFSPED_ENTIDADE
	WSDATA   cGETADMEMPRESASIDRESULT   AS string
	WSDATA   oWSGETINFOPEDNFERESULT    AS TAFADM_PEDIDOSNFE
	WSDATA   cNEWPASS                  AS string
	WSDATA   cGETPASSENTRESULT         AS string

	// Estruturas mantidas por compatibilidade - N�O USAR
	WSDATA   oWSARQUIVOAUT             AS TAFADM_ARQUIVOAUT
	WSDATA   oWSSPED_ENTIDADE          AS TAFADM_SPED_ENTIDADE
	WSDATA   oWSSPED_ENTIDADEREFERENCIAL AS TAFADM_SPED_ENTIDADEREFERENCIAL

ENDWSCLIENT

WSMETHOD NEW WSCLIENT WSTAFSPEDADM
::Init()
If !FindFunction("XMLCHILDEX")
	UserException("O C�digo-Fonte Client atual requer os execut�veis do Protheus Build [7.00.131227A-20171213 NG] ou superior. Atualize o Protheus ou gere o C�digo-Fonte novamente utilizando o Build atual.")
EndIf
Return Self

WSMETHOD INIT WSCLIENT WSTAFSPEDADM
	::oWSARQUIVO         := TAFADM_ARQUIVOAUT():New()
	::oWSADMEMISSRESULT  := TAFADM_AUTEMP_EMISSAO():New()
	::oWSEMPRESA         := TAFADM_SPED_ENTIDADE():New()
	::oWSOUTRASINSCRICOES := TAFADM_SPED_ENTIDADEREFERENCIAL():New()
	::oWSCONSULTEMISSRESULT := TAFADM_ARRAYOFCONTROLENFEEMISS():New()
	::oWSGETADMEMPRESASRESULT := TAFADM_ARRAYOFSPED_ENTIDADE():New()
	::oWSGETINFOPEDNFERESULT := TAFADM_PEDIDOSNFE():New()

	// Estruturas mantidas por compatibilidade - N�O USAR
	::oWSARQUIVOAUT      := ::oWSARQUIVO
	::oWSSPED_ENTIDADE   := ::oWSEMPRESA
	::oWSSPED_ENTIDADEREFERENCIAL := ::oWSOUTRASINSCRICOES
Return

WSMETHOD RESET WSCLIENT WSTAFSPEDADM
	::cUSERTOKEN         := NIL 
	::cKEYPASS           := NIL 
	::cCNPJ              := NIL 
	::cNOME              := NIL 
	::cADDENTFILERESULT  := NIL 
	::cID_ENT            := NIL 
	::cCPF               := NIL 
	::cIE                := NIL 
	::cUF                := NIL 
	::nOPC               := NIL 
	::oWSARQUIVO         := NIL 
	::cPARTNER           := NIL 
	::oWSADMEMISSRESULT  := NIL 
	::oWSEMPRESA         := NIL 
	::oWSOUTRASINSCRICOES := NIL 
	::cADMEMPRESASRESULT := NIL 
	::cPEDIDO            := NIL 
	::cIDINICIAL         := NIL 
	::cIDFINAL           := NIL 
	::oWSCONSULTEMISSRESULT := NIL 
	::cENTIDADEATIVARESULT := NIL 
	::cENTIDADECLEARRESULT := NIL 
	::cIDEMPRESA         := NIL 
	::oWSGETADMEMPRESASRESULT := NIL 
	::cGETADMEMPRESASIDRESULT := NIL 
	::oWSGETINFOPEDNFERESULT := NIL 
	::cNEWPASS           := NIL 
	::cGETPASSENTRESULT  := NIL 

	// Estruturas mantidas por compatibilidade - N�O USAR
	::oWSARQUIVOAUT      := NIL
	::oWSSPED_ENTIDADE   := NIL
	::oWSSPED_ENTIDADEREFERENCIAL := NIL
	::Init()
Return

WSMETHOD CLONE WSCLIENT WSTAFSPEDADM

	Local oClone := WSTAFSPEDADM():New()
	oClone:_URL          := ::_URL 
	oClone:cUSERTOKEN    := ::cUSERTOKEN
	oClone:cKEYPASS      := ::cKEYPASS
	oClone:cCNPJ         := ::cCNPJ
	oClone:cNOME         := ::cNOME
	oClone:cADDENTFILERESULT := ::cADDENTFILERESULT
	oClone:cID_ENT       := ::cID_ENT
	oClone:cCPF          := ::cCPF
	oClone:cIE           := ::cIE
	oClone:cUF           := ::cUF
	oClone:nOPC          := ::nOPC
	oClone:oWSARQUIVO    :=  IIF(::oWSARQUIVO = NIL , NIL ,::oWSARQUIVO:Clone() )
	oClone:cPARTNER      := ::cPARTNER
	oClone:oWSADMEMISSRESULT :=  IIF(::oWSADMEMISSRESULT = NIL , NIL ,::oWSADMEMISSRESULT:Clone() )
	oClone:oWSEMPRESA    :=  IIF(::oWSEMPRESA = NIL , NIL ,::oWSEMPRESA:Clone() )
	oClone:oWSOUTRASINSCRICOES :=  IIF(::oWSOUTRASINSCRICOES = NIL , NIL ,::oWSOUTRASINSCRICOES:Clone() )
	oClone:cADMEMPRESASRESULT := ::cADMEMPRESASRESULT
	oClone:cPEDIDO       := ::cPEDIDO
	oClone:cIDINICIAL    := ::cIDINICIAL
	oClone:cIDFINAL      := ::cIDFINAL
	oClone:oWSCONSULTEMISSRESULT :=  IIF(::oWSCONSULTEMISSRESULT = NIL , NIL ,::oWSCONSULTEMISSRESULT:Clone() )
	oClone:cENTIDADEATIVARESULT := ::cENTIDADEATIVARESULT
	oClone:cENTIDADECLEARRESULT := ::cENTIDADECLEARRESULT
	oClone:cIDEMPRESA    := ::cIDEMPRESA
	oClone:oWSGETADMEMPRESASRESULT :=  IIF(::oWSGETADMEMPRESASRESULT = NIL , NIL ,::oWSGETADMEMPRESASRESULT:Clone() )
	oClone:cGETADMEMPRESASIDRESULT := ::cGETADMEMPRESASIDRESULT
	oClone:oWSGETINFOPEDNFERESULT :=  IIF(::oWSGETINFOPEDNFERESULT = NIL , NIL ,::oWSGETINFOPEDNFERESULT:Clone() )
	oClone:cNEWPASS      := ::cNEWPASS
	oClone:cGETPASSENTRESULT := ::cGETPASSENTRESULT

	// Estruturas mantidas por compatibilidade - N�O USAR
	oClone:oWSARQUIVOAUT := oClone:oWSARQUIVO
	oClone:oWSSPED_ENTIDADE := oClone:oWSEMPRESA
	oClone:oWSSPED_ENTIDADEREFERENCIAL := oClone:oWSOUTRASINSCRICOES
Return oClone

// WSDL Method ADDENTFILE of Service WSTAFSPEDADM

WSMETHOD ADDENTFILE WSSEND cUSERTOKEN,cKEYPASS,cCNPJ,cNOME WSRECEIVE cADDENTFILERESULT WSCLIENT WSTAFSPEDADM
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<ADDENTFILE xmlns="http://webservices.totvs.com.br/spedadm.apw">'
cSoap += WSSoapValue("USERTOKEN", ::cUSERTOKEN, cUSERTOKEN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("KEYPASS", ::cKEYPASS, cKEYPASS , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CNPJ", ::cCNPJ, cCNPJ , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("NOME", ::cNOME, cNOME , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</ADDENTFILE>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://webservices.totvs.com.br/spedadm.apw/ADDENTFILE",; 
	"DOCUMENT","http://webservices.totvs.com.br/spedadm.apw",,"1.031217",; 
	"http://localhost:8080/SPEDADM.apw")

::Init()
::cADDENTFILERESULT  :=  WSAdvValue( oXmlRet,"_ADDENTFILERESPONSE:_ADDENTFILERESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method ADMEMISS of Service WSTAFSPEDADM

WSMETHOD ADMEMISS WSSEND cUSERTOKEN,cID_ENT,cCNPJ,cCPF,cIE,cUF,nOPC,oWSARQUIVO,cPARTNER WSRECEIVE oWSADMEMISSRESULT WSCLIENT WSTAFSPEDADM
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<ADMEMISS xmlns="http://webservices.totvs.com.br/spedadm.apw">'
cSoap += WSSoapValue("USERTOKEN", ::cUSERTOKEN, cUSERTOKEN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("ID_ENT", ::cID_ENT, cID_ENT , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CNPJ", ::cCNPJ, cCNPJ , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CPF", ::cCPF, cCPF , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("IE", ::cIE, cIE , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("UF", ::cUF, cUF , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("OPC", ::nOPC, nOPC , "integer", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("ARQUIVO", ::oWSARQUIVO, oWSARQUIVO , "ARQUIVOAUT", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("PARTNER", ::cPARTNER, cPARTNER , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</ADMEMISS>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://webservices.totvs.com.br/spedadm.apw/ADMEMISS",; 
	"DOCUMENT","http://webservices.totvs.com.br/spedadm.apw",,"1.031217",; 
	"http://localhost:8080/SPEDADM.apw")

::Init()
::oWSADMEMISSRESULT:SoapRecv( WSAdvValue( oXmlRet,"_ADMEMISSRESPONSE:_ADMEMISSRESULT","AUTEMP_EMISSAO",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method ADMEMPRESAS of Service WSTAFSPEDADM

WSMETHOD ADMEMPRESAS WSSEND cUSERTOKEN,oWSEMPRESA,oWSOUTRASINSCRICOES WSRECEIVE cADMEMPRESASRESULT WSCLIENT WSTAFSPEDADM
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<ADMEMPRESAS xmlns="http://webservices.totvs.com.br/spedadm.apw">'
cSoap += WSSoapValue("USERTOKEN", ::cUSERTOKEN, cUSERTOKEN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("EMPRESA", ::oWSEMPRESA, oWSEMPRESA , "SPED_ENTIDADE", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("OUTRASINSCRICOES", ::oWSOUTRASINSCRICOES, oWSOUTRASINSCRICOES , "SPED_ENTIDADEREFERENCIAL", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</ADMEMPRESAS>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://webservices.totvs.com.br/spedadm.apw/ADMEMPRESAS",; 
	"DOCUMENT","http://webservices.totvs.com.br/spedadm.apw",,"1.031217",; 
	"http://localhost:8080/SPEDADM.apw")

::Init()
::cADMEMPRESASRESULT :=  WSAdvValue( oXmlRet,"_ADMEMPRESASRESPONSE:_ADMEMPRESASRESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method CONSULTEMISS of Service WSTAFSPEDADM

WSMETHOD CONSULTEMISS WSSEND cUSERTOKEN,cID_ENT,cPEDIDO,cIDINICIAL,cIDFINAL WSRECEIVE oWSCONSULTEMISSRESULT WSCLIENT WSTAFSPEDADM
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<CONSULTEMISS xmlns="http://webservices.totvs.com.br/spedadm.apw">'
cSoap += WSSoapValue("USERTOKEN", ::cUSERTOKEN, cUSERTOKEN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("ID_ENT", ::cID_ENT, cID_ENT , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("PEDIDO", ::cPEDIDO, cPEDIDO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("IDINICIAL", ::cIDINICIAL, cIDINICIAL , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("IDFINAL", ::cIDFINAL, cIDFINAL , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</CONSULTEMISS>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://webservices.totvs.com.br/spedadm.apw/CONSULTEMISS",; 
	"DOCUMENT","http://webservices.totvs.com.br/spedadm.apw",,"1.031217",; 
	"http://localhost:8080/SPEDADM.apw")

::Init()
::oWSCONSULTEMISSRESULT:SoapRecv( WSAdvValue( oXmlRet,"_CONSULTEMISSRESPONSE:_CONSULTEMISSRESULT","ARRAYOFCONTROLENFEEMISS",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method ENTIDADEATIVA of Service WSTAFSPEDADM

WSMETHOD ENTIDADEATIVA WSSEND cUSERTOKEN,cID_ENT,nOPC WSRECEIVE cENTIDADEATIVARESULT WSCLIENT WSTAFSPEDADM
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<ENTIDADEATIVA xmlns="http://webservices.totvs.com.br/spedadm.apw">'
cSoap += WSSoapValue("USERTOKEN", ::cUSERTOKEN, cUSERTOKEN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("ID_ENT", ::cID_ENT, cID_ENT , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("OPC", ::nOPC, nOPC , "integer", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</ENTIDADEATIVA>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://webservices.totvs.com.br/spedadm.apw/ENTIDADEATIVA",; 
	"DOCUMENT","http://webservices.totvs.com.br/spedadm.apw",,"1.031217",; 
	"http://localhost:8080/SPEDADM.apw")

::Init()
::cENTIDADEATIVARESULT :=  WSAdvValue( oXmlRet,"_ENTIDADEATIVARESPONSE:_ENTIDADEATIVARESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method ENTIDADECLEAR of Service WSTAFSPEDADM

WSMETHOD ENTIDADECLEAR WSSEND cUSERTOKEN,cID_ENT WSRECEIVE cENTIDADECLEARRESULT WSCLIENT WSTAFSPEDADM
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<ENTIDADECLEAR xmlns="http://webservices.totvs.com.br/spedadm.apw">'
cSoap += WSSoapValue("USERTOKEN", ::cUSERTOKEN, cUSERTOKEN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("ID_ENT", ::cID_ENT, cID_ENT , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</ENTIDADECLEAR>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://webservices.totvs.com.br/spedadm.apw/ENTIDADECLEAR",; 
	"DOCUMENT","http://webservices.totvs.com.br/spedadm.apw",,"1.031217",; 
	"http://localhost:8080/SPEDADM.apw")

::Init()
::cENTIDADECLEARRESULT :=  WSAdvValue( oXmlRet,"_ENTIDADECLEARRESPONSE:_ENTIDADECLEARRESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method GETADMEMPRESAS of Service WSTAFSPEDADM

WSMETHOD GETADMEMPRESAS WSSEND cUSERTOKEN,cCNPJ,cCPF,cIE,cUF,cIDEMPRESA WSRECEIVE oWSGETADMEMPRESASRESULT WSCLIENT WSTAFSPEDADM
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<GETADMEMPRESAS xmlns="http://webservices.totvs.com.br/spedadm.apw">'
cSoap += WSSoapValue("USERTOKEN", ::cUSERTOKEN, cUSERTOKEN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CNPJ", ::cCNPJ, cCNPJ , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CPF", ::cCPF, cCPF , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("IE", ::cIE, cIE , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("UF", ::cUF, cUF , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("IDEMPRESA", ::cIDEMPRESA, cIDEMPRESA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</GETADMEMPRESAS>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://webservices.totvs.com.br/spedadm.apw/GETADMEMPRESAS",; 
	"DOCUMENT","http://webservices.totvs.com.br/spedadm.apw",,"1.031217",; 
	"http://localhost:8080/SPEDADM.apw")

::Init()
::oWSGETADMEMPRESASRESULT:SoapRecv( WSAdvValue( oXmlRet,"_GETADMEMPRESASRESPONSE:_GETADMEMPRESASRESULT","ARRAYOFSPED_ENTIDADE",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method GETADMEMPRESASID of Service WSTAFSPEDADM

WSMETHOD GETADMEMPRESASID WSSEND cUSERTOKEN,cCNPJ,cCPF,cIE,cUF,cIDEMPRESA WSRECEIVE cGETADMEMPRESASIDRESULT WSCLIENT WSTAFSPEDADM
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<GETADMEMPRESASID xmlns="http://webservices.totvs.com.br/spedadm.apw">'
cSoap += WSSoapValue("USERTOKEN", ::cUSERTOKEN, cUSERTOKEN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CNPJ", ::cCNPJ, cCNPJ , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CPF", ::cCPF, cCPF , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("IE", ::cIE, cIE , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("UF", ::cUF, cUF , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("IDEMPRESA", ::cIDEMPRESA, cIDEMPRESA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</GETADMEMPRESASID>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://webservices.totvs.com.br/spedadm.apw/GETADMEMPRESASID",; 
	"DOCUMENT","http://webservices.totvs.com.br/spedadm.apw",,"1.031217",; 
	"http://localhost:8080/SPEDADM.apw")

::Init()
::cGETADMEMPRESASIDRESULT :=  WSAdvValue( oXmlRet,"_GETADMEMPRESASIDRESPONSE:_GETADMEMPRESASIDRESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method GETINFOPEDNFE of Service WSTAFSPEDADM

WSMETHOD GETINFOPEDNFE WSSEND cUSERTOKEN,cID_ENT,cKEYPASS WSRECEIVE oWSGETINFOPEDNFERESULT WSCLIENT WSTAFSPEDADM
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<GETINFOPEDNFE xmlns="http://webservices.totvs.com.br/spedadm.apw">'
cSoap += WSSoapValue("USERTOKEN", ::cUSERTOKEN, cUSERTOKEN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("ID_ENT", ::cID_ENT, cID_ENT , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("KEYPASS", ::cKEYPASS, cKEYPASS , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</GETINFOPEDNFE>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://webservices.totvs.com.br/spedadm.apw/GETINFOPEDNFE",; 
	"DOCUMENT","http://webservices.totvs.com.br/spedadm.apw",,"1.031217",; 
	"http://localhost:8080/SPEDADM.apw")

::Init()
::oWSGETINFOPEDNFERESULT:SoapRecv( WSAdvValue( oXmlRet,"_GETINFOPEDNFERESPONSE:_GETINFOPEDNFERESULT","PEDIDOSNFE",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method GETPASSENT of Service WSTAFSPEDADM

WSMETHOD GETPASSENT WSSEND cUSERTOKEN,cID_ENT,cNEWPASS,nOPC WSRECEIVE cGETPASSENTRESULT WSCLIENT WSTAFSPEDADM
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<GETPASSENT xmlns="http://webservices.totvs.com.br/spedadm.apw">'
cSoap += WSSoapValue("USERTOKEN", ::cUSERTOKEN, cUSERTOKEN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("ID_ENT", ::cID_ENT, cID_ENT , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("NEWPASS", ::cNEWPASS, cNEWPASS , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("OPC", ::nOPC, nOPC , "integer", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</GETPASSENT>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://webservices.totvs.com.br/spedadm.apw/GETPASSENT",; 
	"DOCUMENT","http://webservices.totvs.com.br/spedadm.apw",,"1.031217",; 
	"http://localhost:8080/SPEDADM.apw")

::Init()
::cGETPASSENTRESULT  :=  WSAdvValue( oXmlRet,"_GETPASSENTRESPONSE:_GETPASSENTRESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.


// WSDL Data Structure ARQUIVOAUT

WSSTRUCT TAFADM_ARQUIVOAUT
	WSDATA   cCNPJ                     AS string OPTIONAL
	WSDATA   cCPF                      AS string OPTIONAL
	WSDATA   dDATAPED                  AS date
	WSDATA   cIE                       AS string OPTIONAL
	WSDATA   cPEDIDO                   AS string
	WSDATA   nQTDPED                   AS integer
	WSDATA   cUF                       AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_ARQUIVOAUT
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_ARQUIVOAUT
Return

WSMETHOD CLONE WSCLIENT TAFADM_ARQUIVOAUT
	Local oClone := TAFADM_ARQUIVOAUT():NEW()
	oClone:cCNPJ                := ::cCNPJ
	oClone:cCPF                 := ::cCPF
	oClone:dDATAPED             := ::dDATAPED
	oClone:cIE                  := ::cIE
	oClone:cPEDIDO              := ::cPEDIDO
	oClone:nQTDPED              := ::nQTDPED
	oClone:cUF                  := ::cUF
Return oClone

WSMETHOD SOAPSEND WSCLIENT TAFADM_ARQUIVOAUT
	Local cSoap := ""
	cSoap += WSSoapValue("CNPJ", ::cCNPJ, ::cCNPJ , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CPF", ::cCPF, ::cCPF , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DATAPED", ::dDATAPED, ::dDATAPED , "date", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("IE", ::cIE, ::cIE , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("PEDIDO", ::cPEDIDO, ::cPEDIDO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("QTDPED", ::nQTDPED, ::nQTDPED , "integer", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("UF", ::cUF, ::cUF , "string", .T. , .F., 0 , NIL, .F.,.F.) 
Return cSoap

// WSDL Data Structure AUTEMP_EMISSAO

WSSTRUCT TAFADM_AUTEMP_EMISSAO
	WSDATA   cCNPJ                     AS string OPTIONAL
	WSDATA   cCPF                      AS string OPTIONAL
	WSDATA   cID_ENT                   AS string OPTIONAL
	WSDATA   cIE                       AS string OPTIONAL
	WSDATA   nQTDE                     AS integer OPTIONAL
	WSDATA   nSALDO                    AS integer OPTIONAL
	WSDATA   cUF                       AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_AUTEMP_EMISSAO
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_AUTEMP_EMISSAO
Return

WSMETHOD CLONE WSCLIENT TAFADM_AUTEMP_EMISSAO
	Local oClone := TAFADM_AUTEMP_EMISSAO():NEW()
	oClone:cCNPJ                := ::cCNPJ
	oClone:cCPF                 := ::cCPF
	oClone:cID_ENT              := ::cID_ENT
	oClone:cIE                  := ::cIE
	oClone:nQTDE                := ::nQTDE
	oClone:nSALDO               := ::nSALDO
	oClone:cUF                  := ::cUF
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT TAFADM_AUTEMP_EMISSAO
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCNPJ              :=  WSAdvValue( oResponse,"_CNPJ","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cCPF               :=  WSAdvValue( oResponse,"_CPF","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cID_ENT            :=  WSAdvValue( oResponse,"_ID_ENT","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cIE                :=  WSAdvValue( oResponse,"_IE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::nQTDE              :=  WSAdvValue( oResponse,"_QTDE","integer",NIL,NIL,NIL,"N",NIL,NIL) 
	::nSALDO             :=  WSAdvValue( oResponse,"_SALDO","integer",NIL,NIL,NIL,"N",NIL,NIL) 
	::cUF                :=  WSAdvValue( oResponse,"_UF","string",NIL,"Property cUF as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure SPED_ENTIDADEREFERENCIAL

WSSTRUCT TAFADM_SPED_ENTIDADEREFERENCIAL
	WSDATA   oWSINSCRICAO              AS TAFADM_ARRAYOFSPED_GENERICSTRUCT OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_SPED_ENTIDADEREFERENCIAL
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_SPED_ENTIDADEREFERENCIAL
Return

WSMETHOD CLONE WSCLIENT TAFADM_SPED_ENTIDADEREFERENCIAL
	Local oClone := TAFADM_SPED_ENTIDADEREFERENCIAL():NEW()
	oClone:oWSINSCRICAO         := IIF(::oWSINSCRICAO = NIL , NIL , ::oWSINSCRICAO:Clone() )
Return oClone

WSMETHOD SOAPSEND WSCLIENT TAFADM_SPED_ENTIDADEREFERENCIAL
	Local cSoap := ""
	cSoap += WSSoapValue("INSCRICAO", ::oWSINSCRICAO, ::oWSINSCRICAO , "ARRAYOFSPED_GENERICSTRUCT", .F. , .F., 0 , NIL, .F.,.F.) 
Return cSoap

// WSDL Data Structure ARRAYOFCONTROLENFEEMISS

WSSTRUCT TAFADM_ARRAYOFCONTROLENFEEMISS
	WSDATA   oWSCONTROLENFEEMISS       AS TAFADM_CONTROLENFEEMISS OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_ARRAYOFCONTROLENFEEMISS
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_ARRAYOFCONTROLENFEEMISS
	::oWSCONTROLENFEEMISS  := {} // Array Of  TAFADM_CONTROLENFEEMISS():New()
Return

WSMETHOD CLONE WSCLIENT TAFADM_ARRAYOFCONTROLENFEEMISS
	Local oClone := TAFADM_ARRAYOFCONTROLENFEEMISS():NEW()
	oClone:oWSCONTROLENFEEMISS := NIL
	If ::oWSCONTROLENFEEMISS <> NIL 
		oClone:oWSCONTROLENFEEMISS := {}
		aEval( ::oWSCONTROLENFEEMISS , { |x| aadd( oClone:oWSCONTROLENFEEMISS , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT TAFADM_ARRAYOFCONTROLENFEEMISS
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_CONTROLENFEEMISS","CONTROLENFEEMISS",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSCONTROLENFEEMISS , TAFADM_CONTROLENFEEMISS():New() )
			::oWSCONTROLENFEEMISS[len(::oWSCONTROLENFEEMISS)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure ARRAYOFSPED_ENTIDADE

WSSTRUCT TAFADM_ARRAYOFSPED_ENTIDADE
	WSDATA   oWSSPED_ENTIDADE          AS TAFADM_SPED_ENTIDADE OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_ARRAYOFSPED_ENTIDADE
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_ARRAYOFSPED_ENTIDADE
	::oWSSPED_ENTIDADE     := {} // Array Of  TAFADM_SPED_ENTIDADE():New()
Return

WSMETHOD CLONE WSCLIENT TAFADM_ARRAYOFSPED_ENTIDADE
	Local oClone := TAFADM_ARRAYOFSPED_ENTIDADE():NEW()
	oClone:oWSSPED_ENTIDADE := NIL
	If ::oWSSPED_ENTIDADE <> NIL 
		oClone:oWSSPED_ENTIDADE := {}
		aEval( ::oWSSPED_ENTIDADE , { |x| aadd( oClone:oWSSPED_ENTIDADE , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT TAFADM_ARRAYOFSPED_ENTIDADE
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_SPED_ENTIDADE","SPED_ENTIDADE",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSSPED_ENTIDADE , TAFADM_SPED_ENTIDADE():New() )
			::oWSSPED_ENTIDADE[len(::oWSSPED_ENTIDADE)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure PEDIDOSNFE

WSSTRUCT TAFADM_PEDIDOSNFE
	WSDATA   oWSPEDIDOS                AS TAFADM_ARRAYOFENTPEDNFE OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_PEDIDOSNFE
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_PEDIDOSNFE
Return

WSMETHOD CLONE WSCLIENT TAFADM_PEDIDOSNFE
	Local oClone := TAFADM_PEDIDOSNFE():NEW()
	oClone:oWSPEDIDOS           := IIF(::oWSPEDIDOS = NIL , NIL , ::oWSPEDIDOS:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT TAFADM_PEDIDOSNFE
	Local oNode1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNode1 :=  WSAdvValue( oResponse,"_PEDIDOS","ARRAYOFENTPEDNFE",NIL,NIL,NIL,"O",NIL,NIL) 
	If oNode1 != NIL
		::oWSPEDIDOS := TAFADM_ARRAYOFENTPEDNFE():New()
		::oWSPEDIDOS:SoapRecv(oNode1)
	EndIf
Return

// WSDL Data Structure ARRAYOFSPED_GENERICSTRUCT

WSSTRUCT TAFADM_ARRAYOFSPED_GENERICSTRUCT
	WSDATA   oWSSPED_GENERICSTRUCT     AS TAFADM_SPED_GENERICSTRUCT OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_ARRAYOFSPED_GENERICSTRUCT
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_ARRAYOFSPED_GENERICSTRUCT
	::oWSSPED_GENERICSTRUCT := {} // Array Of  TAFADM_SPED_GENERICSTRUCT():New()
Return

WSMETHOD CLONE WSCLIENT TAFADM_ARRAYOFSPED_GENERICSTRUCT
	Local oClone := TAFADM_ARRAYOFSPED_GENERICSTRUCT():NEW()
	oClone:oWSSPED_GENERICSTRUCT := NIL
	If ::oWSSPED_GENERICSTRUCT <> NIL 
		oClone:oWSSPED_GENERICSTRUCT := {}
		aEval( ::oWSSPED_GENERICSTRUCT , { |x| aadd( oClone:oWSSPED_GENERICSTRUCT , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPSEND WSCLIENT TAFADM_ARRAYOFSPED_GENERICSTRUCT
	Local cSoap := ""
	aEval( ::oWSSPED_GENERICSTRUCT , {|x| cSoap := cSoap  +  WSSoapValue("SPED_GENERICSTRUCT", x , x , "SPED_GENERICSTRUCT", .F. , .F., 0 , NIL, .F.,.F.)  } ) 
Return cSoap

// WSDL Data Structure CONTROLENFEEMISS

WSSTRUCT TAFADM_CONTROLENFEEMISS
	WSDATA   cCNPJDEST                 AS string
	WSDATA   dDATE_NFE                 AS date
	WSDATA   nERROS                    AS integer
	WSDATA   cNFE_ID                   AS string
	WSDATA   cPED_DPEC                 AS string
	WSDATA   cPED_NFE                  AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_CONTROLENFEEMISS
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_CONTROLENFEEMISS
Return

WSMETHOD CLONE WSCLIENT TAFADM_CONTROLENFEEMISS
	Local oClone := TAFADM_CONTROLENFEEMISS():NEW()
	oClone:cCNPJDEST            := ::cCNPJDEST
	oClone:dDATE_NFE            := ::dDATE_NFE
	oClone:nERROS               := ::nERROS
	oClone:cNFE_ID              := ::cNFE_ID
	oClone:cPED_DPEC            := ::cPED_DPEC
	oClone:cPED_NFE             := ::cPED_NFE
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT TAFADM_CONTROLENFEEMISS
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCNPJDEST          :=  WSAdvValue( oResponse,"_CNPJDEST","string",NIL,"Property cCNPJDEST as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::dDATE_NFE          :=  WSAdvValue( oResponse,"_DATE_NFE","date",NIL,"Property dDATE_NFE as s:date on SOAP Response not found.",NIL,"D",NIL,NIL) 
	::nERROS             :=  WSAdvValue( oResponse,"_ERROS","integer",NIL,"Property nERROS as s:integer on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::cNFE_ID            :=  WSAdvValue( oResponse,"_NFE_ID","string",NIL,"Property cNFE_ID as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPED_DPEC          :=  WSAdvValue( oResponse,"_PED_DPEC","string",NIL,"Property cPED_DPEC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPED_NFE           :=  WSAdvValue( oResponse,"_PED_NFE","string",NIL,"Property cPED_NFE as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure SPED_ENTIDADE

WSSTRUCT TAFADM_SPED_ENTIDADE
	WSDATA   cBAIRRO                   AS string OPTIONAL
	WSDATA   cCEP                      AS string
	WSDATA   cCEP_CP                   AS string OPTIONAL
	WSDATA   cCNPJ                     AS string OPTIONAL
	WSDATA   cCOD_MUN                  AS string
	WSDATA   cCOD_PAIS                 AS string
	WSDATA   cCOMPL                    AS string OPTIONAL
	WSDATA   cCP                       AS string OPTIONAL
	WSDATA   cCPF                      AS string OPTIONAL
	WSDATA   cDDD                      AS string OPTIONAL
	WSDATA   dDTRE                     AS date OPTIONAL
	WSDATA   cEMAIL                    AS string OPTIONAL
	WSDATA   cENDERECO                 AS string
	WSDATA   cFANTASIA                 AS string OPTIONAL
	WSDATA   cFAX                      AS string OPTIONAL
	WSDATA   cFONE                     AS string OPTIONAL
	WSDATA   cID_MATRIZ                AS string OPTIONAL
	WSDATA   cIDEMPRESA                AS string OPTIONAL
	WSDATA   cIE                       AS string OPTIONAL
	WSDATA   cIM                       AS string OPTIONAL
	WSDATA   cINDSITESP                AS string OPTIONAL
	WSDATA   cINSCRTRA                 AS string OPTIONAL
	WSDATA   cMUN                      AS string OPTIONAL
	WSDATA   cNIRE                     AS string
	WSDATA   cNIT                      AS string OPTIONAL
	WSDATA   cNOME                     AS string
	WSDATA   cNUM                      AS string OPTIONAL
	WSDATA   cSUFRAMA                  AS string OPTIONAL
	WSDATA   cUF                       AS string
	WSDATA   cUPDINSCRTR               AS string OPTIONAL
	WSDATA   cINDTAF	               AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_SPED_ENTIDADE
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_SPED_ENTIDADE
Return

WSMETHOD CLONE WSCLIENT TAFADM_SPED_ENTIDADE
	Local oClone := TAFADM_SPED_ENTIDADE():NEW()
	oClone:cBAIRRO              := ::cBAIRRO
	oClone:cCEP                 := ::cCEP
	oClone:cCEP_CP              := ::cCEP_CP
	oClone:cCNPJ                := ::cCNPJ
	oClone:cCOD_MUN             := ::cCOD_MUN
	oClone:cCOD_PAIS            := ::cCOD_PAIS
	oClone:cCOMPL               := ::cCOMPL
	oClone:cCP                  := ::cCP
	oClone:cCPF                 := ::cCPF
	oClone:cDDD                 := ::cDDD
	oClone:dDTRE                := ::dDTRE
	oClone:cEMAIL               := ::cEMAIL
	oClone:cENDERECO            := ::cENDERECO
	oClone:cFANTASIA            := ::cFANTASIA
	oClone:cFAX                 := ::cFAX
	oClone:cFONE                := ::cFONE
	oClone:cID_MATRIZ           := ::cID_MATRIZ
	oClone:cIDEMPRESA           := ::cIDEMPRESA
	oClone:cIE                  := ::cIE
	oClone:cIM                  := ::cIM
	oClone:cINDSITESP           := ::cINDSITESP
	oClone:cINSCRTRA            := ::cINSCRTRA
	oClone:cMUN                 := ::cMUN
	oClone:cNIRE                := ::cNIRE
	oClone:cNIT                 := ::cNIT
	oClone:cNOME                := ::cNOME
	oClone:cNUM                 := ::cNUM
	oClone:cSUFRAMA             := ::cSUFRAMA
	oClone:cUF                  := ::cUF
	oClone:cUPDINSCRTR          := ::cUPDINSCRTR
Return oClone

WSMETHOD SOAPSEND WSCLIENT TAFADM_SPED_ENTIDADE
	Local cSoap := ""
	cSoap += WSSoapValue("BAIRRO", ::cBAIRRO, ::cBAIRRO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CEP", ::cCEP, ::cCEP , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CEP_CP", ::cCEP_CP, ::cCEP_CP , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CNPJ", ::cCNPJ, ::cCNPJ , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("COD_MUN", ::cCOD_MUN, ::cCOD_MUN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("COD_PAIS", ::cCOD_PAIS, ::cCOD_PAIS , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("COMPL", ::cCOMPL, ::cCOMPL , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CP", ::cCP, ::cCP , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CPF", ::cCPF, ::cCPF , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DDD", ::cDDD, ::cDDD , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DTRE", ::dDTRE, ::dDTRE , "date", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("EMAIL", ::cEMAIL, ::cEMAIL , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("ENDERECO", ::cENDERECO, ::cENDERECO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("FANTASIA", ::cFANTASIA, ::cFANTASIA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("FAX", ::cFAX, ::cFAX , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("FONE", ::cFONE, ::cFONE , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("ID_MATRIZ", ::cID_MATRIZ, ::cID_MATRIZ , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("IDEMPRESA", ::cIDEMPRESA, ::cIDEMPRESA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("IE", ::cIE, ::cIE , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("IM", ::cIM, ::cIM , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("INDSITESP", ::cINDSITESP, ::cINDSITESP , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("INSCRTRA", ::cINSCRTRA, ::cINSCRTRA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("MUN", ::cMUN, ::cMUN , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NIRE", ::cNIRE, ::cNIRE , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NIT", ::cNIT, ::cNIT , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NOME", ::cNOME, ::cNOME , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NUM", ::cNUM, ::cNUM , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("SUFRAMA", ::cSUFRAMA, ::cSUFRAMA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("UF", ::cUF, ::cUF , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("UPDINSCRTR", ::cUPDINSCRTR, ::cUPDINSCRTR , "string", .F. , .F., 0 , NIL, .F.,.F.) 
Return cSoap

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT TAFADM_SPED_ENTIDADE
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cBAIRRO            :=  WSAdvValue( oResponse,"_BAIRRO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cCEP               :=  WSAdvValue( oResponse,"_CEP","string",NIL,"Property cCEP as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cCEP_CP            :=  WSAdvValue( oResponse,"_CEP_CP","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cCNPJ              :=  WSAdvValue( oResponse,"_CNPJ","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cCOD_MUN           :=  WSAdvValue( oResponse,"_COD_MUN","string",NIL,"Property cCOD_MUN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cCOD_PAIS          :=  WSAdvValue( oResponse,"_COD_PAIS","string",NIL,"Property cCOD_PAIS as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cCOMPL             :=  WSAdvValue( oResponse,"_COMPL","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cCP                :=  WSAdvValue( oResponse,"_CP","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cCPF               :=  WSAdvValue( oResponse,"_CPF","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cDDD               :=  WSAdvValue( oResponse,"_DDD","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::dDTRE              :=  WSAdvValue( oResponse,"_DTRE","date",NIL,NIL,NIL,"D",NIL,NIL) 
	::cEMAIL             :=  WSAdvValue( oResponse,"_EMAIL","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cENDERECO          :=  WSAdvValue( oResponse,"_ENDERECO","string",NIL,"Property cENDERECO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cFANTASIA          :=  WSAdvValue( oResponse,"_FANTASIA","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cFAX               :=  WSAdvValue( oResponse,"_FAX","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cFONE              :=  WSAdvValue( oResponse,"_FONE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cID_MATRIZ         :=  WSAdvValue( oResponse,"_ID_MATRIZ","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cIDEMPRESA         :=  WSAdvValue( oResponse,"_IDEMPRESA","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cIE                :=  WSAdvValue( oResponse,"_IE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cIM                :=  WSAdvValue( oResponse,"_IM","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cINDSITESP         :=  WSAdvValue( oResponse,"_INDSITESP","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cINSCRTRA          :=  WSAdvValue( oResponse,"_INSCRTRA","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cMUN               :=  WSAdvValue( oResponse,"_MUN","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cNIRE              :=  WSAdvValue( oResponse,"_NIRE","string",NIL,"Property cNIRE as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cNIT               :=  WSAdvValue( oResponse,"_NIT","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cNOME              :=  WSAdvValue( oResponse,"_NOME","string",NIL,"Property cNOME as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cNUM               :=  WSAdvValue( oResponse,"_NUM","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cSUFRAMA           :=  WSAdvValue( oResponse,"_SUFRAMA","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cUF                :=  WSAdvValue( oResponse,"_UF","string",NIL,"Property cUF as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cUPDINSCRTR        :=  WSAdvValue( oResponse,"_UPDINSCRTR","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure ARRAYOFENTPEDNFE

WSSTRUCT TAFADM_ARRAYOFENTPEDNFE
	WSDATA   oWSENTPEDNFE              AS TAFADM_ENTPEDNFE OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_ARRAYOFENTPEDNFE
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_ARRAYOFENTPEDNFE
	::oWSENTPEDNFE         := {} // Array Of  TAFADM_ENTPEDNFE():New()
Return

WSMETHOD CLONE WSCLIENT TAFADM_ARRAYOFENTPEDNFE
	Local oClone := TAFADM_ARRAYOFENTPEDNFE():NEW()
	oClone:oWSENTPEDNFE := NIL
	If ::oWSENTPEDNFE <> NIL 
		oClone:oWSENTPEDNFE := {}
		aEval( ::oWSENTPEDNFE , { |x| aadd( oClone:oWSENTPEDNFE , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT TAFADM_ARRAYOFENTPEDNFE
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_ENTPEDNFE","ENTPEDNFE",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSENTPEDNFE , TAFADM_ENTPEDNFE():New() )
			::oWSENTPEDNFE[len(::oWSENTPEDNFE)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure SPED_GENERICSTRUCT

WSSTRUCT TAFADM_SPED_GENERICSTRUCT
	WSDATA   cCODE                     AS string
	WSDATA   cDESCRIPTION              AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_SPED_GENERICSTRUCT
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_SPED_GENERICSTRUCT
Return

WSMETHOD CLONE WSCLIENT TAFADM_SPED_GENERICSTRUCT
	Local oClone := TAFADM_SPED_GENERICSTRUCT():NEW()
	oClone:cCODE                := ::cCODE
	oClone:cDESCRIPTION         := ::cDESCRIPTION
Return oClone

WSMETHOD SOAPSEND WSCLIENT TAFADM_SPED_GENERICSTRUCT
	Local cSoap := ""
	cSoap += WSSoapValue("CODE", ::cCODE, ::cCODE , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DESCRIPTION", ::cDESCRIPTION, ::cDESCRIPTION , "string", .T. , .F., 0 , NIL, .F.,.F.) 
Return cSoap

// WSDL Data Structure ENTPEDNFE

WSSTRUCT TAFADM_ENTPEDNFE
	WSDATA   dDATAPED                  AS date
	WSDATA   cNFE_ID                   AS string
	WSDATA   cPEDIDO                   AS string
	WSDATA   nQTDPED                   AS integer
	WSDATA   nSALDO                    AS integer
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT TAFADM_ENTPEDNFE
	::Init()
Return Self

WSMETHOD INIT WSCLIENT TAFADM_ENTPEDNFE
Return

WSMETHOD CLONE WSCLIENT TAFADM_ENTPEDNFE
	Local oClone := TAFADM_ENTPEDNFE():NEW()
	oClone:dDATAPED             := ::dDATAPED
	oClone:cNFE_ID              := ::cNFE_ID
	oClone:cPEDIDO              := ::cPEDIDO
	oClone:nQTDPED              := ::nQTDPED
	oClone:nSALDO               := ::nSALDO
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT TAFADM_ENTPEDNFE
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::dDATAPED           :=  WSAdvValue( oResponse,"_DATAPED","date",NIL,"Property dDATAPED as s:date on SOAP Response not found.",NIL,"D",NIL,NIL) 
	::cNFE_ID            :=  WSAdvValue( oResponse,"_NFE_ID","string",NIL,"Property cNFE_ID as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPEDIDO            :=  WSAdvValue( oResponse,"_PEDIDO","string",NIL,"Property cPEDIDO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::nQTDPED            :=  WSAdvValue( oResponse,"_QTDPED","integer",NIL,"Property nQTDPED as s:integer on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::nSALDO             :=  WSAdvValue( oResponse,"_SALDO","integer",NIL,"Property nSALDO as s:integer on SOAP Response not found.",NIL,"N",NIL,NIL) 
Return

