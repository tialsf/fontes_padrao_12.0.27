#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

#DEFINE ANALITICO_MATRICULA				1
#DEFINE ANALITICO_CATEGORIA				2
#DEFINE ANALITICO_TIPO_ESTABELECIMENTO	3
#DEFINE ANALITICO_ESTABELECIMENTO		4
#DEFINE ANALITICO_LOTACAO				5
#DEFINE ANALITICO_NATUREZA				6
#DEFINE ANALITICO_TIPO_RUBRICA			7
#DEFINE ANALITICO_INCIDENCIA_CP			8
#DEFINE ANALITICO_INCIDENCIA_IRRF		9
#DEFINE ANALITICO_INCIDENCIA_FGTS		10
#DEFINE ANALITICO_DECIMO_TERCEIRO		11
#DEFINE ANALITICO_TIPO_VALOR			12
#DEFINE ANALITICO_VALOR					13
#DEFINE ANALITICO_MOTIVO_DESLIGAMENTO	14


//---------------------------------------------------------------------
/*/{Protheus.doc} TAFSocialReport
@type			class
@description	Classe com fun��es utilizadas nos Relat�rios de Confer�ncias do eSocial ( INSS/IRRF/FGTS ).
@author			Victor A. Barbosa
@since			15/05/2019
@version		1.0
/*/
//---------------------------------------------------------------------
Class TAFSocialReport From LongClassName

	Data oVOReport
	Data aStructV3N
	Data nTamFilial

	Method New() Constructor
	Method Upsert()
	Method Delete()
	Method GetRubrica()

EndClass

//---------------------------------------------------------------------
/*/{Protheus.doc} New
@type			method
@description	Retorna a inst�ncia do objeto.
@author			Victor A. Barbosa
@since			15/05/2019
@version		1.0
@return			self - Objeto para utiliza��o nos relat�rios totalizadores
/*/
//---------------------------------------------------------------------
Method New() Class TAFSocialReport

self:oVOReport	:=	TAFVOReport():New()
self:aStructV3N	:=	V3N->( DBStruct() )
self:nTamFilial	:=	self:aStructV3N[aScan( self:aStructV3N, { |x| x[1] == "V3N_FILIAL" } ) ][3]

Return( self )

//---------------------------------------------------------------------
/*/{Protheus.doc} Upsert
@type			method
@description	Insere/atualiza um registro na tabela V3N.
@author			Victor A. Barbosa
@since			15/05/2019
@version		1.0
@param			cEvento	-	C�digo do Evento: S-1200|S-2299|S-2399|S-5001|S-5002|S-5003
@param			cOrigem	-	Origem da Grava��o: 1=Folha|2=TAF|3=Governo-INSS|4=Governo-IRRF|5=Governo-FGTS
@param			cFilEvt	-	Filial do Evento
@param			oData	-	Objeto com os dados a ser inserido/atualizado
@param			lDelete	-	Indica se deve ser executado apenas a exclus�o dos registros
/*/
//---------------------------------------------------------------------
Method Upsert( cEvento, cOrigem, cFilEvt, oData, lDelete ) Class TAFSocialReport

Local cIndApu		:=	""
Local cPerApu		:=	""
Local cCPF			:=	""
Local cNome			:=	""
Local cRecibo		:=	""
Local nAnalitico	:=	0
Local aAnalitico	:=	{}

Default lDelete	:=	.F.

cFilEvt	:=	PadR( cFilEvt, self:nTamFilial )
cIndApu	:=	oData:GetIndApu()
cPerApu	:=	oData:GetPeriodo()
cCPF	:=	oData:GetCPF()
cNome	:=	oData:GetNome()
cRecibo	:=	oData:GetRecibo()

//Se existir dados para o FILIAL + INDAPU + PERAPU + CPF + EVENTO + ORIGEM ent�o exclui, pois qualquer uma das informa��es do n�vel abaixo, pode ser alterada/exlu�da
self:Delete( cFilEvt + cIndApu + cPerApu + cCPF + cEvento + cOrigem, .F. )

If !lDelete
	aAnalitico := oData:GetAnalitico()

	For nAnalitico := 1 to Len( aAnalitico )
		RecLock( "V3N", .T. )

		V3N->V3N_FILIAL	:=	cFilEvt
		V3N->V3N_ID		:=	TAFGeraID( "TAF" )
		V3N->V3N_INDAPU	:=	cIndApu
		V3N->V3N_PERAPU	:=	cPerApu
		V3N->V3N_CPF	:=	cCPF
		V3N->V3N_NOME	:=	cNome
		V3N->V3N_MATRIC	:=	aAnalitico[nAnalitico][ANALITICO_MATRICULA]
		V3N->V3N_CATEG	:=	aAnalitico[nAnalitico][ANALITICO_CATEGORIA]
		V3N->V3N_TPINSC	:=	aAnalitico[nAnalitico][ANALITICO_TIPO_ESTABELECIMENTO]
		V3N->V3N_NRINSC	:=	aAnalitico[nAnalitico][ANALITICO_ESTABELECIMENTO]
		V3N->V3N_CODLOT	:=	aAnalitico[nAnalitico][ANALITICO_LOTACAO]
		V3N->V3N_EVENTO	:=	cEvento
		V3N->V3N_ORIGEM	:=	cOrigem
		V3N->V3N_RECIBO	:=	cRecibo
		V3N->V3N_NATRUB	:=	aAnalitico[nAnalitico][ANALITICO_NATUREZA]
		V3N->V3N_TPRUBR	:=	aAnalitico[nAnalitico][ANALITICO_TIPO_RUBRICA]
		V3N->V3N_ITCP	:=	aAnalitico[nAnalitico][ANALITICO_INCIDENCIA_CP]
		V3N->V3N_ITIRRF	:=	aAnalitico[nAnalitico][ANALITICO_INCIDENCIA_IRRF]
		V3N->V3N_ITFGTS	:=	aAnalitico[nAnalitico][ANALITICO_INCIDENCIA_FGTS]
		V3N->V3N_INDDEC	:=	aAnalitico[nAnalitico][ANALITICO_DECIMO_TERCEIRO]
		V3N->V3N_TPVLR	:=	aAnalitico[nAnalitico][ANALITICO_TIPO_VALOR]
		V3N->V3N_VALOR	:=	aAnalitico[nAnalitico][ANALITICO_VALOR]
		
		If TafColumnPos("V3N_MOTDES") .And. Len(aAnalitico[nAnalitico]) >= 14
			V3N->V3N_MOTDES := aAnalitico[nAnalitico][ANALITICO_MOTIVO_DESLIGAMENTO]
		EndIf

		V3N->( MsUnlock() )
	Next nAnalitico
EndIf

oData:Clear()

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} Delete
@type			method
@description	Deleta logicamente um ou mais registros na tabela V3N, respeitando a chave recebida por par�metro.
@author			Victor A. Barbosa
@since			16/05/2019
@version		1.0
@param			cChave	-	Chave do(s) registro(s) a ser deletado(s)
@param			lAll	-	Indica se deve excluir todas as ocorr�ncias, sem considerar a origem
/*/
//---------------------------------------------------------------------
Method Delete( cChave, lAll ) Class TAFSocialReport

Local cV3NAlias	:=	""
local cBancoDB	:= Upper(AllTrim(TcGetDB()))
Local cWhere	:= ''
Default lAll	:=	.F.

If !lAll
	V3N->( DBSetOrder( 2 ) ) //V3N_FILIAL+V3N_INDAPU+V3N_PERAPU+V3N_CPF+V3N_EVENTO+V3N_ORIGEM
	If V3N->( MsSeek( cChave ) )
		While V3N->( !Eof() ) .and. V3N->( V3N_FILIAL + V3N_INDAPU + V3N_PERAPU + V3N_CPF + V3N_EVENTO + V3N_ORIGEM ) == cChave
			If RecLock( "V3N", .F. )
				V3N->( DBDelete() )
				V3N->( MsUnlock() )
			EndIf

			V3N->( DBSkip() )
		EndDo
	EndIf
Else
	cV3NAlias := "ALIASV3N"

	If cBancoDB $ "POSTGRES"
		cWhere	:= "% CONCAT(V3N.V3N_FILIAL , V3N.V3N_INDAPU , V3N.V3N_PERAPU , V3N.V3N_CPF , V3N.V3N_EVENTO) = '" + cChave + "' %"
	ElseIf cBancoDB $ "ORACLE|DB2|INFORMIX|OPENEDGE"
		cWhere	:= "% V3N.V3N_FILIAL || V3N.V3N_INDAPU || V3N.V3N_PERAPU || V3N.V3N_CPF || V3N.V3N_EVENTO = '" + cChave + "' %"
	Else
		cWhere	:= "% V3N.V3N_FILIAL + V3N.V3N_INDAPU + V3N.V3N_PERAPU + V3N.V3N_CPF + V3N.V3N_EVENTO = '" + cChave + "' %"
	EndIf
	
	BeginSQL Alias cV3NAlias
		SELECT V3N.R_E_C_N_O_ AS RECNOV3N
		FROM %table:V3N% V3N
		WHERE %exp:cWhere%
		  AND V3N.%notdel%
	EndSQL

	( cV3NAlias )->( DBGoTop() )

	While ( cV3NAlias )->( !Eof() )
		V3N->( DBGoTo( ( cV3NAlias )->RECNOV3N ) )

		If RecLock( "V3N", .F. )
			V3N->( DBDelete() )
			V3N->( MsUnlock() )
		EndIf

		( cV3NAlias )->( DBSkip() )
	EndDo

	( cV3NAlias )->( DBCloseArea() )
EndIf

Return()

//---------------------------------------------------------------------
/*/{Protheus.doc} GetRubrica
@type			method
@description	Retorna um array com os dados da Rubrica.
@author			Victor A. Barbosa
@since			16/05/2019
@version		1.0
@param			cCodRubr	-	C�digo da Rubrica
@param			cIDTabRubr	-	Identificador da Tabela de Rubrica
@param			cPerApu		-	Per�odo de refer�ncia
@param			lMultVinc	-	Indica se o evento de origem possui M�ltiplos V�nculos
@return			aRubr		-	Array com as informa��es da Rubrica
/*/
//---------------------------------------------------------------------
Method GetRubrica( cCodRubr, cIDTabRubr, cPerApu, lMultVinc ) Class TAFSocialReport

Local cNatureza	:=	""
Local cTipo		:=	""
Local cIncCP	:=	""
Local cIncIRRF	:=	""
Local cIncFGTS	:=	""
Local cCNPJRaiz	:=	SubStr( SM0->M0_CGC, 1, 8 )
Local nRecnoC8R	:=	0
Local aInfoSM0	:=	FWLoadSM0()
Local aFilMV	:=	{}
Local aRubr		:=	Array( 5 )
Local aArea		:=	GetArea()
Local lCont		:=	.F.

Default cIDTabRubr	:=	""
Default cPerApu		:=	""
Default lMultVinc	:=	.F.

If lMultVinc
	aEval( aInfoSM0, { |x| Iif( SubStr( x[18], 1, 8 ) == cCNPJRaiz .and. x[1] == cEmpAnt, aAdd( aFilMV, x[2] ), Nil ) } )
	nRecnoC8R := StaticCall( TAFR120, MVQueryC8R, cCodRubr, cIDTabRubr, cPerApu, aFilMV )

	If nRecnoC8R > 0
		lCont := .T.
		C8R->( DBGoTo( nRecnoC8R ) )
	EndIf
Else
	C8R->( DBSetOrder( 5 ) ) //C8R_FILIAL+C8R_ID+C8R_ATIVO
	If C8R->( MsSeek( xFilial( "C8R" ) + cCodRubr + "1" ) )
		lCont := .T.
	EndIf
EndIf

If lCont
	If C89->( MsSeek( xFilial( "C89" ) + C8R->C8R_NATRUB ) )
		cNatureza := AllTrim( C89->C89_CODIGO )
	EndIf

	cTipo := AllTrim( C8R->C8R_INDTRB )

	If C8T->( MsSeek( xFilial( "C8T" ) + C8R->C8R_CINTPS ) )
		cIncCP := AllTrim( C8T->C8T_CODIGO )
	EndIf

	If C8U->( MsSeek( xFilial( "C8U" ) + C8R->C8R_CINTIR ) )
		cIncIRRF := AllTrim( C8U->C8U_CODIGO )
	EndIf

	cIncFGTS := AllTrim( C8R->C8R_CINTFG )
EndIf

aRubr[1] := cNatureza
aRubr[2] := cTipo
aRubr[3] := cIncCP
aRubr[4] := cIncIRRF
aRubr[5] := cIncFGTS

RestArea( aArea )

Return( aRubr )