#INCLUDE "PROTHEUS.CH" 
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "TAFA227.CH" 
//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA227
Cadastro MVC de Situa��o Geradora do Acidente de Trabalho

@author Anderson Costa
@since 09/08/2013
@version 1.0

/*/ 
//-------------------------------------------------------------------
Function TAFA227()
Local   oBrw        :=  FWmBrowse():New()

oBrw:SetDescription(STR0001)    //"Cadastro de Situa��o Geradora do Acidente de Trabalho"
oBrw:SetAlias( 'C8L')
oBrw:SetMenuDef( 'TAFA227' )
oBrw:Activate()

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Anderson Costa
@since 09/08/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Return XFUNMnuTAF( "TAFA227" )
//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao generica MVC do model

@return oModel - Objeto do Modelo MVC

@author Anderson Costa
@since 09/08/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
Local oStruC8L  :=  FWFormStruct( 1, 'C8L' )
Local oModel    :=  MPFormModel():New( 'TAFA227' )

oModel:AddFields('MODEL_C8L', /*cOwner*/, oStruC8L)
oModel:GetModel('MODEL_C8L'):SetPrimaryKey({'C8L_FILIAL', 'C8L_ID'})

Return oModel
//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Anderson Costa
@since 09/08/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local   oModel      :=  FWLoadModel( 'TAFA227' )
Local   oStruC8L    :=  FWFormStruct( 2, 'C8L' )
Local   oView       :=  FWFormView():New()

oView:SetModel( oModel )
oView:AddField( 'VIEW_C8L', oStruC8L, 'MODEL_C8L' )

oView:EnableTitleView( 'VIEW_C8L', STR0001 )    //"Cadastro de Situa��o Geradora do Acidente de Trabalho"
oView:CreateHorizontalBox( 'FIELDSC8L', 100 )
oView:SetOwnerView( 'VIEW_C8L', 'FIELDSC8L' )

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} FAtuCont

Rotina para carga e atualiza��o da tabela autocontida.

@Param		nVerEmp	-	Vers�o corrente na empresa
			nVerAtu	-	Vers�o atual ( passado como refer�ncia )

@Return	aRet		-	Array com estrutura de campos e conte�do da tabela

@Author	Felipe de Carvalho Seolin
@Since		24/11/2015
@Version	1.0
/*/
//-------------------------------------------------------------------
Static Function FAtuCont( nVerEmp, nVerAtu )

Local aHeader	:=	{}
Local aBody	:=	{}
Local aRet		:=	{}

nVerAtu := 1026.72

If nVerEmp < nVerAtu
	aAdd( aHeader, "C8L_FILIAL" )
	aAdd( aHeader, "C8L_ID" )
	aAdd( aHeader, "C8L_CODIGO" )
	aAdd( aHeader, "C8L_DESCRI" )
	aAdd( aHeader, "C8L_VALIDA" )

	aAdd( aBody, { "", "000001", "200004300", "IMPACTO DE PESSOA CONTRA OBJETO PARADO", "" } )
	aAdd( aBody, { "", "000002", "200004600", "IMPACTO DE PESSOA CONTRA OBJETO EM MOVIMENTO", "" } )
	aAdd( aBody, { "", "000003", "200008300", "IMPACTO SOFRIDO POR PESSOA DE OBJETO QUE CAI", "" } )
	aAdd( aBody, { "", "000004", "200008600", "IMPACTO SOFRIDO POR PESSOA DE OBJETO PROJETADO", "" } )
	aAdd( aBody, { "", "000005", "200008900", "IMPACTO SOFRIDO POR PESSOA, NIC", "" } )
	aAdd( aBody, { "", "000006", "200012200", "QUEDA DE PESSOA COM DIFERENCA DE NIVEL DE ANDAIME, PASSAGEM, PLATAFORMA, ETC", "" } )
	aAdd( aBody, { "", "000007", "200012300", "QUEDA DE PESSOA COM DIFERENCA DE NIVEL DE ESCADA MOVEL OU FIXADA CUJOS DEGRAUS", "" } )
	aAdd( aBody, { "", "000008", "200012400", "QUEDA DE PESSOA COM DIFERENCA DE NIVEL DE MATERIAL EMPILHADO", "" } )
	aAdd( aBody, { "", "000009", "200012500", "QUEDA DE PESSOA COM DIFERENCA DE NIVEL DE VEICULO", "" } )
	aAdd( aBody, { "", "000010", "200012600", "QUEDA DE PESSOA COM DIFERENCA DE NIVEL EM ESCADA PERMANENTE CUJOS DEGRAUS", "" } )
	aAdd( aBody, { "", "000011", "200012700", "QUEDA DE PESSOA COM DIFERENCA DE NIVEL EM POCO, ESCAVACAO, ABERTURA NO PISO, ETC.", "" } )
	aAdd( aBody, { "", "000012", "200012900", "QUEDA DE PESSOA COM DIFERENCA DE NIVEL, NIC", "" } )
	aAdd( aBody, { "", "000013", "200016300", "QUEDA DE PESSOA EM MESMO NIVEL EM PASSAGEM OU SUPERFICIE DE SUSTENTACAO", "" } )
	aAdd( aBody, { "", "000014", "200016600", "QUEDA DE PESSOA EM MESMO NIVEL SOBRE OU CONTRA ALGUMA COISA", "" } )
	aAdd( aBody, { "", "000015", "200016900", "QUEDA DE PESSOA EM MESMO NIVEL, NIC", "" } )
	aAdd( aBody, { "", "000016", "200020100", "APRISIONAMENTO EM, SOBRE OU ENTRE OBJETOS EM MOVIMENTO CONVERGENTE", "" } )
	aAdd( aBody, { "", "000017", "200020300", "APRISIONAMENTO EM, SOBRE OU ENTRE OBJETO PARADO E OUTRO EM MOVIMENTO", "" } )
	aAdd( aBody, { "", "000018", "200020500", "APRISIONAMENTO EM, SOBRE OU ENTRE DOIS OU MAIS OBJETOS EM MOVIMENTO", "" } )
	aAdd( aBody, { "", "000019", "200020700", "APRISIONAMENTO EM, SOBRE OU ENTRE DESABAMENTO OU DESMORONAMENTO", "" } )
	aAdd( aBody, { "", "000020", "200020900", "APRISIONAMENTO EM, SOB OU ENTRE, NIC", "" } )
	aAdd( aBody, { "", "000021", "200024300", "ATRITO OU ABRASAO POR ENCOSTAR, PISAR, AJOELHAR OU SENTAR EM OBJETO", "" } )
	aAdd( aBody, { "", "000022", "200024400", "ATRITO OU ABRASAO POR MANUSEAR OBJETO", "" } )
	aAdd( aBody, { "", "000023", "200024500", "ATRITO OU ABRASAO POR OBJETO EM VIBRACAO", "" } )
	aAdd( aBody, { "", "000024", "200024600", "ATRITO OU ABRASAO POR CORPO ESTRANHO NO OLHO", "" } )
	aAdd( aBody, { "", "000025", "200024700", "ATRITO OU ABRASAO POR COMPRESSAO REPETITIVA", "" } )
	aAdd( aBody, { "", "000026", "200024900", "ATRITO OU ABRASAO, NIC", "" } )
	aAdd( aBody, { "", "000027", "200028300", "REACAO DO CORPO A MOVIMENTO INVOLUNTARIO", "" } )
	aAdd( aBody, { "", "000028", "200028600", "REACAO DO CORPO A MOVIMENTO VOLUNTARIO", "" } )
	aAdd( aBody, { "", "000029", "200032200", "ESFORCO EXCESSIVO AO ERGUER OBJETO", "" } )
	aAdd( aBody, { "", "000030", "200032400", "ESFORCO EXCESSIVO AO EMPURRAR OU PUXAR OBJETO", "" } )
	aAdd( aBody, { "", "000031", "200032600", "ESFORCO EXCESSIVO AO MANEJAR, SACUDIR OU ARREMESSAR OBJETO", "" } )
	aAdd( aBody, { "", "000032", "200032900", "ESFORCO EXCESSIVO, NIC", "" } )
	aAdd( aBody, { "", "000033", "200036000", "ELETRICA, EXPOSICAO A ENERGIA", "" } )
	aAdd( aBody, { "", "000034", "200040300", "TEMPERATURA MUITO ALTA, CONTATO COM OBJETO OU SUBSTANCIA A", "" } )
	aAdd( aBody, { "", "000035", "200040600", "TEMPERATURA MUITO BAIXA, CONTATO COM OBJETO OU SUBSTANCIA A", "" } )
	aAdd( aBody, { "", "000036", "200044300", "TEMPERATURA AMBIENTE ELEVADA, EXPOSICAO A", "" } )
	aAdd( aBody, { "", "000037", "200044600", "TEMPERATURA AMBIENTE BAIXA, EXPOSICAO A", "" } )
	aAdd( aBody, { "", "000038", "200048200", "LNALACAO DE SUBSTANCIA CAUSTICA, TOXICA OU NOCIVA", "" } )
	aAdd( aBody, { "", "000039", "200048400", "INGESTAO DE SUBSTANCIA CAUSTICA.", "" } )
	aAdd( aBody, { "", "000040", "200048600", "ABSORCAO DE SUBSTANCIA CAUSTICA", "" } )
	aAdd( aBody, { "", "000041", "200048900", "INALACAO, INGESTAO OU ABSORCAO, NIC", "" } )
	aAdd( aBody, { "", "000042", "200052000", "IMERSAO", "" } )
	aAdd( aBody, { "", "000043", "200056000", "RADIACAO NAO IONIZANTE, EXPOSICAO A", "" } )
	aAdd( aBody, { "", "000044", "200060000", "RADIACAO IONIZANTE, EXPOSICAO A", "" } )
	aAdd( aBody, { "", "000045", "200064000", "RUIDO, EXPOSICAO A", "" } )
	aAdd( aBody, { "", "000046", "200068000", "VIBRACAO, EXPOSICAO A", "" } )
	aAdd( aBody, { "", "000047", "200072000", "PRESSAO AMBIENTE, EXPOSICAO A", "" } )
	aAdd( aBody, { "", "000048", "200072300", "EXPOSICAO A PRESSAO AMBIENTE ELEVADA", "" } )
	aAdd( aBody, { "", "000049", "200072600", "EXPOSICAO A PRESSAO AMBIENTE BAIXA", "" } )
	aAdd( aBody, { "", "000050", "200076200", "POLUICAO DA AGUA, ACAO DA", "" } )
	aAdd( aBody, { "", "000051", "200076400", "POLUICAO DO AR, ACAO DA", "" } )
	aAdd( aBody, { "", "000052", "200076600", "POLUICAO DO SOLO, ACAO DA", "" } )
	aAdd( aBody, { "", "000053", "200076900", "POLUICAO, NIC, EXPOSICAO A", "" } )
	aAdd( aBody, { "", "000054", "200080200", "ATAQUE DE SER VIVO POR MORDEDURA, PICADA, CHIFRADA, COICE, ETC.", "" } )
	aAdd( aBody, { "", "000055", "200080400", "ATAQUE DE SER VIVO COM PECONHA", "" } )
	aAdd( aBody, { "", "000056", "200080600", "ATAQUE DE SER VIVO COM TRANSMISSAO DE DOENCA", "" } )
	aAdd( aBody, { "", "000057", "200080900", "ATAQUE DE SER VIVO, NIC", "" } )
	aAdd( aBody, { "", "000058", "209000000", "TIPO, NIC", "" } )
	aAdd( aBody, { "", "000059", "209500000", "TIPO INEXISTENTE", "" } )

    aAdd( aBody, { "", "000060", "200080901", "CONTATO COM PESSOAS DOENTES OU MATERIAL INFECTO CONTAGIANTE AGENTES BIOLOGICOS", "" } )
						
	aAdd( aRet, { aHeader, aBody } )
EndIf

Return( aRet )