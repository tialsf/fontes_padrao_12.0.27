#INCLUDE "PROTHEUS.CH" 
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "TAFA234.CH" 
//-------------------------------------------------------------------
/*/{Protheus.doc} TAFA234
C�digo de incid�ncia tribut�ria da rubrica para o IRRF

@author Anderson Costa
@since 14/08/2013
@version 1.0

/*/ 
//-------------------------------------------------------------------
Function TAFA234()
Local   oBrw        :=  FWmBrowse():New()

oBrw:SetDescription(STR0001)    //"C�digo de incid�ncia tribut�ria da rubrica para o IRRF"
oBrw:SetAlias( 'C8U')
oBrw:SetMenuDef( 'TAFA234' )
C8U->(dbSetOrder(2))
oBrw:Activate()

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Funcao generica MVC com as opcoes de menu

@author Anderson Costa
@since 14/08/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Return XFUNMnuTAF( "TAFA234" )
//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Funcao gen�rica MVC do model

@return oModel - Objeto do Modelo MVC

@author Anderson Costa
@since 14/08/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
Local oStruC8U  :=  FWFormStruct( 1, 'C8U' )
Local oModel    :=  MPFormModel():New( 'TAFA234' )

oModel:AddFields('MODEL_C8U', /*cOwner*/, oStruC8U)
oModel:GetModel('MODEL_C8U'):SetPrimaryKey({'C8U_FILIAL', 'C8U_ID'})

Return oModel
//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Funcao generica MVC do View

@return oView - Objeto da View MVC

@author Anderson Costa
@since 14/08/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local   oModel      :=  FWLoadModel( 'TAFA234' )
Local   oStruC8U    :=  FWFormStruct( 2, 'C8U' )
Local   oView       :=  FWFormView():New()

oView:SetModel( oModel )
oView:AddField( 'VIEW_C8U', oStruC8U, 'MODEL_C8U' )

oView:EnableTitleView( 'VIEW_C8U', STR0001 )    //"C�digo de incid�ncia tribut�ria da rubrica para o IRRF"
oView:CreateHorizontalBox( 'FIELDSC8U', 100 )
oView:SetOwnerView( 'VIEW_C8U', 'FIELDSC8U' )

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} FAtuCont

Rotina para carga e atualiza��o da tabela autocontida.

@Param		nVerEmp	-	Vers�o corrente na empresa
			nVerAtu	-	Vers�o atual ( passado como refer�ncia )

@Return	aRet		-	Array com estrutura de campos e conte�do da tabela

@Author	Felipe de Carvalho Seolin
@Since		24/11/2015
@Version	1.0
/*/
//-------------------------------------------------------------------
Static Function FAtuCont( nVerEmp, nVerAtu )

Local aHeader	as array
Local aBody		as array
Local aRet		as array

Default nVerEmp	:= 0
Default nVerAtu	:= 0

aHeader	:=	{}
aBody		:=	{}
aRet		:=	{}

nVerAtu	:= 1031.02

If nVerEmp < nVerAtu .And. TafAtualizado(.F.)
	aAdd( aHeader, "C8U_FILIAL" )
	aAdd( aHeader, "C8U_ID" )
	aAdd( aHeader, "C8U_CODIGO" )
	aAdd( aHeader, "C8U_DESCRI" )
	aAdd( aHeader, "C8U_VALIDA" )

	aAdd( aBody, { "", "000001", "00", "RENDIMENTO NAO TRIBUTAVEL", "" } )
	aAdd( aBody, { "", "000002", "11", "BASE DE CALCULO DO IRRF - REMUNERA�AO MENSAL", "" } )
	aAdd( aBody, { "", "000003", "12", "BASE DE CALCULO DO IRRF - 13. SALARIO", "" } )
	aAdd( aBody, { "", "000004", "13", "BASE DE CALCULO DO IRRF - FERIAS", "" } )
	aAdd( aBody, { "", "000005", "14", "BASE DE CALCULO DO IRRF - PLR", "" } )
	aAdd( aBody, { "", "000006", "15", "BASE DE CALCULO DO IRRF - RENDIMENTOS RECEBIDOS ACUMULADAMENTE - RRA", "" } )
	aAdd( aBody, { "", "000007", "31", "RETENCOES DO IRRF - REMUNERA�AO MENSAL", "" } )
	aAdd( aBody, { "", "000008", "32", "RETENCOES DO IRRF - 13. SALARIO", "" } )
	aAdd( aBody, { "", "000009", "33", "RETENCOES DO IRRF - FERIAS", "" } )
	aAdd( aBody, { "", "000010", "34", "RETENCOES DO IRRF - PLR", "" } )
	aAdd( aBody, { "", "000011", "35", "RETENCOES DO IRRF - RRA", "" } )
	aAdd( aBody, { "", "000012", "41", "DEDUCOES DA BASE DE CALCULO DO IRRF - PREVIDENCIA SOCIAL OFICIAL - PSO - REMUNERA�AO MENSAL", "" } )
	aAdd( aBody, { "", "000013", "42", "DEDUCOES DA BASE DE CALCULO DO IRRF - PSO - 13. SALARIO", "" } )
	aAdd( aBody, { "", "000014", "43", "DEDUCOES DA BASE DE CALCULO DO IRRF - PSO - FERIAS", "" } )
	aAdd( aBody, { "", "000015", "44", "DEDUCOES DA BASE DE CALCULO DO IRRF - PSO - RRA", "" } )
	aAdd( aBody, { "", "000016", "46", "DEDUCOES DA BASE DE CALCULO DO IRRF - PREVIDENCIA PRIVADA - SALARIO MENSAL", "" } )
	aAdd( aBody, { "", "000017", "47", "DEDUCOES DA BASE DE CALCULO DO IRRF - PREVIDENCIA PRIVADA - 13. SALARIO", "" } )
	aAdd( aBody, { "", "000018", "51", "DEDUCOES DA BASE DE CALCULO DO IRRF - PENSAO ALIMENTICIA - REMUNERA�AO MENSAL", "" } )
	aAdd( aBody, { "", "000019", "52", "DEDUCOES DA BASE DE CALCULO DO IRRF - PENSAO ALIMENTICIA - 13. SALARIO", "" } )
	aAdd( aBody, { "", "000020", "53", "DEDUCOES DA BASE DE CALCULO DO IRRF - PENSAO ALIMENTICIA - FERIAS", "" } )
	aAdd( aBody, { "", "000021", "54", "DEDUCOES DA BASE DE CALCULO DO IRRF - PENSAO ALIMENTICIA - PLR", "" } )
	aAdd( aBody, { "", "000022", "56", "DEDUCOES DA BASE DE CALCULO DO IRRF - DEPENDENTE - REMUNERA�AO MENSAL", "20170901" } )
	aAdd( aBody, { "", "000023", "57", "DEDUCOES DA BASE DE CALCULO DO IRRF - DEPENDENTE - 13. SALARIO", "20170119" } )
	aAdd( aBody, { "", "000024", "58", "DEDUCOES DA BASE DE CALCULO DO IRRF - DEPENDENTE - FERIAS", "20170119" } )
	aAdd( aBody, { "", "000025", "61", "DEDUCOES DA BASE DE CALCULO DO IRRF - FUNDO DE APOSENTADORIA PROGRAMADA INDIVIDUAL - FAPI - REMUNERA�AO MENSAL", "" } )
	aAdd( aBody, { "", "000026", "62", "DEDUCOES DA BASE DE CALCULO DO IRRF - FUNDO DE APOSENTADORIA PROGRAMADA INDIVIDUAL - FAPI - 13. SALARIO", "" } )
	aAdd( aBody, { "", "000027", "63", "DEDUCOES DA BASE DE CALCULO DO IRRF - FUNDA�AO DE PREVIDENCIA COMPLEMENTAR DO SERVIDOR - FUNPRESP - REMUNERA�AO MENSAL", "" } )
	aAdd( aBody, { "", "000028", "64", "DEDUCOES DA BASE DE CALCULO DO IRRF - FUNDA�AO DE PREVIDENCIA COMPLEMENTAR DO SERVIDOR - FUNPRESP - 13. SALARIO", "" } )
	aAdd( aBody, { "", "000029", "71", "ISENCOES DO IRRF - PARCELA ISENTA 65 ANOS - 13. SALARIO", "" } )
	aAdd( aBody, { "", "000030", "72", "ISENCOES DO IRRF - DIARIAS", "" } )
	aAdd( aBody, { "", "000031", "73", "ISENCOES DO IRRF - AJUDA DE CUSTO", "" } )
	aAdd( aBody, { "", "000032", "74", "ISENCOES DO IRRF - INDENIZA�AO E RESCISAO DE CONTRATO, INCLUSIVE A TITULO DE PDV E ACIDENTES DE TRABALHO", "" } )
	aAdd( aBody, { "", "000033", "75", "ISENCOES DO IRRF - ABONO PECUNIARIO", "" } )
	aAdd( aBody, { "", "000034", "76", "ISENCOES DO IRRF - PENSAO, APOSENTADORIA OU REFORMA POR MOLESTIA GRAVE OU ACIDENTE EM SERVI�O - REMUNERA�AO MENSAL", "" } )
	aAdd( aBody, { "", "000035", "77", "ISENCOES DO IRRF - PENSAO, APOSENTADORIA OU REFORMA POR MOLESTIA GRAVE OU ACIDENTE EM SERVI�O - 13. SALARIO", "" } )
	aAdd( aBody, { "", "000036", "78", "ISENCOES DO IRRF - VALORES PAGOS A TITULAR OU SOCIO DE MICROEMPRESA OU EMPRESA DE PEQUENO PORTE, EXCETO PRO-LABORE E ALUGUEIS", "" } )
	aAdd( aBody, { "", "000037", "79", "ISENCOES DO IRRF - OUTRAS ISENCOES (O NOME DA RUBRICA DEVE SER CLARO PARA IDENTIFICA�AO DA NATUREZA DOS VALORES)", "" } )
	aAdd( aBody, { "", "000038", "81", "DEMANDAS JUDICIAIS - DEPOSITO JUDICIAL", "" } )
	aAdd( aBody, { "", "000039", "82", "DEMANDAS JUDICIAIS - COMPENSA�AO JUDICIAL DO ANO CALENDARIO", "" } )
	aAdd( aBody, { "", "000040", "83", "DEMANDAS JUDICIAIS - COMPENSA�AO JUDICIAL DE ANOS ANTERIORES", "" } )
	aAdd( aBody, { "", "000041", "91", "INCIDENCIA SUSPENSA EM DECORRENCIA DE DECISAO JUDICIAL, RELATIVAS A BASE DE CALCULO DO IRRF SOBRE REMUNERA�AO MENSAL", "" } )
	aAdd( aBody, { "", "000042", "92", "INCIDENCIA SUSPENSA EM DECORRENCIA DE DECISAO JUDICIAL, RELATIVAS A BASE DE CALCULO DO IRRF SOBRE 13. SALARIO", "" } )
	aAdd( aBody, { "", "000043", "93", "INCIDENCIA SUSPENSA EM DECORRENCIA DE DECISAO JUDICIAL, RELATIVAS A BASE DE CALCULO DO IRRF SOBRE FERIAS", "" } )
	aAdd( aBody, { "", "000044", "94", "INCIDENCIA SUSPENSA EM DECORRENCIA DE DECISAO JUDICIAL, RELATIVAS A BASE DE CALCULO DO IRRF SOBRE PLR", "" } )
	aAdd( aBody, { "", "000045", "95", "INCIDENCIA SUSPENSA EM DECORRENCIA DE DECISAO JUDICIAL, RELATIVAS A BASE DE CALCULO DO IRRF SOBRE RRA", "" } )

	//Altera��es Layout 2.2.3
	aAdd( aBody, { "", "000046", "01", "REDIMENTO NAO TRIBUTAVEL EM FUNCAO DE ACORDOS INTERNACIONAIS DE BITRIBUTACAO", "" } )
	aAdd( aBody, { "", "000047", "18", "BASE DE CALCULO DO IRRF - REMUNERACAO RECEBIDA POR RESIDENTE FISCAL NO EXTERIOR", "20170901" } )
	aAdd( aBody, { "", "000048", "55", "DEDUCOES DA BASE DE CALCULO DO IRRF - PENSAO ALIMENTICIA - RRA", "" } )
	aAdd( aBody, { "", "000049", "70", "ISENCOES DO IRRF - PARCELA ISENTA 65 ANOS - REMUNERACAO MENSAL", "" } )
	
	//Altera��es Layout 2.3
	aAdd( aBody, { "", "000050", "09", "OUTRAS VERBAS N�O CONSIDERADAS COMO BASE DE C�LCULO OU RENDIMENTO", "" } )

	aAdd( aRet, { aHeader, aBody } )
EndIf

Return( aRet )