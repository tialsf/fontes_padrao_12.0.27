#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TAFA530.CH"


/*/{Protheus.doc} TAFA530
Tabela autocontida criada para evento do e-Social S-5011
@author Victor A. Barbosa
@since 24/01/2018
@version 1.0
@type function
/*/
Function TAFA530()

Local oBrw := FwMBrowse():New()

oBrw:SetDescription( STR0001 ) //"Indicativos de Comercializa��o"
oBrw:SetAlias( "V3F" )
oBrw:SetMenuDef( "TAFA530" )
V3F->( DBSetOrder( 1 ) )
oBrw:Activate()

Return 


/*/{Protheus.doc} MenuDef
Defini��o do menu da rotina
@author Victor A. Barbosa
@since 24/01/2018
@version 1.0
@type function
/*/
Static Function MenuDef()
Return xFunMnuTAF( "TAFA530",,,, .T. )


/*/{Protheus.doc} MenuDef
Modelo da rotina
@author Victor A. Barbosa
@since 24/01/2018
@version 1.0
@type function
/*/
Static Function ModelDef()

Local oStruV3F := FwFormStruct( 1, "V3F" )
Local oModel   := MpFormModel():New( "TAFA530" )

oModel:AddFields( "MODEL_V3F", /*cOwner*/, oStruV3F )
oModel:GetModel ( "MODEL_V3F" ):SetPrimaryKey( { "V3F_FILIAL", "V3F_ID" } )

Return( oModel )

/*/{Protheus.doc} MenuDef
View da rotina
@author Victor A. Barbosa
@since 24/01/2018
@version 1.0
@type function
/*/
Static Function ViewDef()

Local oModel   := FwLoadModel( "TAFA530" )
Local oStruV3F := FwFormStruct( 2, "V3F" )
Local oView    := FwFormView():New()

oView:SetModel( oModel )
oView:AddField( "VIEW_V3F", oStruV3F, "MODEL_V3F" )
oView:EnableTitleView( "VIEW_V3F", STR0001 ) //"Indicativos de Comercializa��o"
oView:CreateHorizontalBox( "FIELDSV3F", 100 )
oView:SetOwnerView( "VIEW_V3F", "FIELDSV3F" )

Return( oView )


/*/{Protheus.doc} MenuDef
Fun��o que carrega os dados da autocontida de acordo com a vers�o do cliente
@author Victor A. Barbosa
@since 24/01/2018
@version 1.0
@type function
/*/
Static Function FAtuCont( nVerEmp, nVerAtu )

Local aHeader	:=	{}
Local aBody		:=	{}
Local aRet		:=	{}

nVerAtu := 1026.61

If nVerEmp < nVerAtu
	aAdd( aHeader, "V3F_FILIAL" )
	aAdd( aHeader, "V3F_ID" )
	aAdd( aHeader, "V3F_CODIGO" )
	aAdd( aHeader, "V3F_DESCRI" )
	aAdd( aHeader, "V3F_VALIDA" )

	aAdd( aBody, { "", "000001", "1 ","1 - dose di�ria de ru�do"                        		                    , "" } )            //NT 11/2019
    aAdd( aBody, { "", "000002", "2 ","2 - decibel linear (dB (linear))"		                                    , "" } )
    aAdd( aBody, { "", "000003", "3 ","3 - decibel (C) (dB(C))"		                                                , "" } ) 
    aAdd( aBody, { "", "000004", "4 ","4 - decibel (A) (dB(A))"		                                                , "" } ) 
    aAdd( aBody, { "", "000005", "5 ","5 - quilocaloria por hora (kcal/h)"		                                    , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000006", "6 ","6 - gray (Gy)"		                                                        , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000007", "7 ","7 - sievert (Sv)"		                                                    , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000008", "8 ","8 - quilograma-for�a por cent�metro quadrado (kgf/cm2)"		                , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000009", "9 ","9 - metro por segundo ao quadrado (m/s2)"		                            , "" } ) 
    aAdd( aBody, { "", "000010", "10","10 - metro por segundo elevado a 1,75 (m/s1,75)"		                        , "" } ) 
    aAdd( aBody, { "", "000011", "11","11 - parte de vapor ou g�s por milh�o de partes de ar contaminado (ppm)"	    , "" } ) 
    aAdd( aBody, { "", "000012", "12","12 - miligrama por metro c�bico de ar (mg/m3)"		                        , "" } ) 
    aAdd( aBody, { "", "000013", "13","13 - fibra por cent�metro c�bico (f/cm3)"		                            , "" } ) 
    aAdd( aBody, { "", "000014", "14","14 - grau Celsius (�C)"		                                                , "" } ) 
    aAdd( aBody, { "", "000015", "15","15 - metro por segundo (m/s)"		                                        , "" } )
    aAdd( aBody, { "", "000016", "16","16 - porcentual" 		                                                    , "" } ) 
    aAdd( aBody, { "", "000017", "17","17 - lux (lx)"		                                                        , "" } ) 
    aAdd( aBody, { "", "000018", "18","18 - unidade formadora de col�nias por metro c�bico (ufc/m3)"		        , "" } ) 
    aAdd( aBody, { "", "000019", "19","19 - dose di�ria"		                                                    , "" } ) 
    aAdd( aBody, { "", "000020", "20","20 - dose mensal"		                                                    , "" } ) 
    aAdd( aBody, { "", "000021", "21","21 - dose trimestral"		                                                , "" } ) 
    aAdd( aBody, { "", "000022", "22","22 - dose anual"		                                                        , "" } ) 
    aAdd( aBody, { "", "000023", "23","23 - hertz (Hz)"		                                                        , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000024", "24","24 - gigahertz (GHz)"		                                                , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000025", "25","25 - quilohertz (kHz)"	                                                    , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000026", "26","26 - watt por metro quadrado (W/m2)"		                                    , "" } ) 
    aAdd( aBody, { "", "000027", "27","27 - amp�re por metro (A/m)"		                                            , "" } ) 
    aAdd( aBody, { "", "000028", "28","28 - militesla(mT)"		                                                    , "" } ) 
    aAdd( aBody, { "", "000029", "29","29 - microtesla (�T)"		                                                , "" } ) 
    aAdd( aBody, { "", "000030", "30","30 - miliamp�re (mA)"		                                                , "" } ) 
    aAdd( aBody, { "", "000031", "31","31 - quilovolt por metro (kV/m)"		                                        , "" } ) 
    aAdd( aBody, { "", "000032", "32","32 - volt por metro (V/m)"		                                            , "" } ) 
    aAdd( aBody, { "", "000033", "33","33 - minuto (min)"		                                                    , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000034", "34","34 - hora (h)"		                                                        , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000035", "35","35 - joule por metro quadrado (J/m2)"		                                , "" } ) 
    aAdd( aBody, { "", "000036", "36","36 - milijoule por cent�metro quadrado (mJ/cm2)"		                        , "" } ) 
    aAdd( aBody, { "", "000037", "37","37 - milisievert (mSv)"                                  		            , "" } )            //NT 11/2019
    aAdd( aBody, { "", "000038", "38","38 - atmosfera (atm)"		                                                , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000039", "39","39 - milh�o de part�culas por dec�metro c�bico (mppdc)"		                , "" } )
    aAdd( aBody, { "", "000040", "40","40 - nan�metro (nm)"		                                                    , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000041", "41","41 - miliwatt (mW)"		                                                    , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000042", "42","42 - watt (W)"		                                                        , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000043", "43","43 - umidade relativa do ar (UR (%))"		                                , "" } ) 
    aAdd( aBody, { "", "000044", "44","44 - decibel (A) (dB(A)) NEN Q=3"		                                    , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000045", "45","45 - milisievert (mSv) - dose equivalente anual - cristalino"		        , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000046", "46","46 - milisievert (mSv) - dose equivalente anual - pele"		                , "20190331" } )    //NT 11/2019
    aAdd( aBody, { "", "000047", "47","47 - milisievert (mSv) - dose equivalente anual - m�os e p�s."		        , "20190331" } )    //NT 11/2019
	
    aAdd( aRet, { aHeader, aBody } )

EndIf

Return( aRet )
