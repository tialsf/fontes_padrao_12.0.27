#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "RFID.ch"

Static __aMapFld := MapFld()
Static __oFilAPI := JsonObject():New()
Static __lDic    := Nil

/*/{Protheus.doc} rfid
API de fun��es do RFID
@type  API
@author marcelo.neumann
@since 11/06/2020
@version P12.1.30
/*/
WSRESTFUL rfid DESCRIPTION STR0001 FORMAT APPLICATION_JSON //"API de fun��es do RFID"

	WSDATA page        AS INTEGER OPTIONAL
	WSDATA pageSize    AS INTEGER OPTIONAL
	WSDATA tag         AS STRING  OPTIONAL
	WSDATA product     AS STRING  OPTIONAL
	WSDATA initialDate AS STRING  OPTIONAL
	WSDATA finishDate  AS STRING  OPTIONAL
	WSDATA epc         AS STRING  OPTIONAL

	WSMETHOD GET login;
	   DESCRIPTION STR0002; //"Valida��o do login"
	   WSSYNTAX    "api/rfid/v1/core/login";
	   PATH        "/api/rfid/v1/core/login";
	   TTALK       "v1"

	WSMETHOD GET rastreability;
	   DESCRIPTION STR0003; //"Retorna a rastreabilidade das etiquetas"
	   WSSYNTAX    "api/rfid/v1/core/rastreability";
	   PATH        "/api/rfid/v1/core/rastreability";
	   TTALK       "v1"

	WSMETHOD POST epc;
	   DESCRIPTION STR0017; //"Cria uma nova EPC"
	   WSSYNTAX    "api/rfid/v1/core/epc";
	   PATH        "/api/rfid/v1/core/epc";
	   TTALK       "v1"

	WSMETHOD PUT epc;
	   DESCRIPTION STR0018; //"Altera a quantidade de uma EPC"
	   WSSYNTAX    "api/rfid/v1/core/epc/{epc}";
	   PATH        "/api/rfid/v1/core/epc/{epc}";
	   TTALK       "v1"

	WSMETHOD GET epc;
	   DESCRIPTION STR0072; //"Consulta uma EPC"
	   WSSYNTAX    "api/rfid/v1/core/epc/{epc}";
	   PATH        "/api/rfid/v1/core/epc/{epc}";
	   TTALK       "v1"

	WSMETHOD POST listepc;
	   DESCRIPTION STR0073; //"Consulta lista de EPCs"
	   WSSYNTAX    "api/rfid/v1/core/listepc";
	   PATH        "/api/rfid/v1/core/listepc";
	   TTALK       "v1"

ENDWSRESTFUL

/*/{Protheus.doc} GET login
M�todo de valida��o do login (login � efetuado pela camada do framework)
URL: localhost:5050/rest/api/rfid/v1/core/login

@author marcelo.neumann
@since 11/06/2020
@version P12.1.30
@return .T., L�gico, Retorno do m�todo GET
/*/
WSMETHOD GET login WSSERVICE rfid
	Local lRet := .T.

	Self:SetContentType("application/json")

	lRet := vldFilial(Self:GetHeader("filial"))
	If lRet
 		Self:SetResponse('{"status":"ok"}')
	EndIf

Return lRet

/*/{Protheus.doc} GET rastreability
M�todo que recupera a rastreabilidade das etiquetas

URL: localhost:5050/rest/api/rfid/v1/core/rastreability
@author marcelo.neumann
@since 21/07/2020
@version P12
@return Nil
/*/
WSMETHOD GET rastreability WSRECEIVE page, pageSize, tag, product, initialDate, finishDate WSSERVICE rfid
	Local aDados   := {}
	Local cAlias   := ""
	Local cQuery   := ""
	Local lRet     := .T.
	Local lDataPad := .F.
	Local nIndex   := 0
	Local nPos     := 0
	Local nStart   := 0
	Local nTotal   := Len(__aMapFld)
	Local oRetJson := JsonObject():New()

	DEFAULT Self:page     := 1
	DEFAULT Self:pageSize := 20

	Self:SetContentType("application/json")

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet

		If Empty(Self:initialDate) .And. Empty(Self:finishDate)
			lDataPad := .T.
			Self:finishDate  := ConvDate( Date() )
			Self:initialDate := ConvDate( Date()-7 )
		EndIf

		cQuery := "SELECT "
		For nIndex := 1 To nTotal
			If nIndex > 1
				cQuery += ","
			EndIf
			cQuery += __aMapFld[nIndex][1] + "." + __aMapFld[nIndex][2]
		Next nIndex
		cQuery +=  " FROM " + RetSqlName("HWN") + " HWN"
		cQuery += " INNER JOIN " + RetSqlName("HWW") + " HWW"
		cQuery +=    " ON HWW.HWW_FILIAL = '" + xFilial("HWW") + "'"
		cQuery +=   " AND HWW.HWW_EPC    = HWN.HWN_EPC"
		cQuery +=   " AND HWW.D_E_L_E_T_ = ' '"
		cQuery += " WHERE HWN.HWN_FILIAL = '" + xFilial("HWN") + "'"
		cQuery +=   " AND HWN.D_E_L_E_T_ = ' '"
		If !Empty(Self:tag)
			cQuery += " AND HWW.HWW_EPC = '" + Self:tag + "'"
		EndIf
		If !Empty(Self:product)
			cQuery += " AND HWN.HWN_PROD = '" + Self:product + "'"
		EndIf
		If !Empty(Self:initialDate)
			cQuery += " AND HWW.HWW_DATA >= '" + StrTran(Self:initialDate, '-', "") + "'"
		EndIf
		If !Empty(Self:finishDate)
			cQuery += " AND HWW.HWW_DATA <= '" + StrTran(Self:finishDate, '-', "") + "'"
		EndIf
		cQuery += " ORDER BY HWW_DATA, HWW_HORA, HWW_EPC"

		cAlias := GetNextAlias()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

		For nIndex := 1 To nTotal
			If __aMapFld[nIndex][5] $ "|D|L|N|"
				TcSetField(cAlias, __aMapFld[nIndex][2],;
				                   __aMapFld[nIndex][5],;
				                   __aMapFld[nIndex][6],;
				                   __aMapFld[nIndex][7])
			EndIf
		Next nIndex

		If Self:page > 1
			nStart := ( (Self:page-1) * Self:pageSize )
			If nStart > 0
				(cAlias)->(DbSkip(nStart))
			EndIf
		EndIf

		While (cAlias)->(!Eof())
			aAdd(aDados, JsonObject():New())
			nPos := Len(aDados)

			For nIndex := 1 To nTotal
				If __aMapFld[nIndex][5] == "D"
					aDados[nPos][__aMapFld[nIndex][3]] := convDate(&("(cAlias)->" + __aMapFld[nIndex][2]))
				ElseIf __aMapFld[nIndex][2] == "HWW_ACAO"
					aDados[nPos][__aMapFld[nIndex][3]] := getDesCBox(cAlias, "HWW_ACAO")
				ElseIf __aMapFld[nIndex][2] == "HWW_ROTINA"
					aDados[nPos][__aMapFld[nIndex][3]] := getRotina(&("(cAlias)->" + __aMapFld[nIndex][2]))
				Else
					aDados[nPos][__aMapFld[nIndex][3]] := &("(cAlias)->" + __aMapFld[nIndex][2])
				EndIf
			Next nTotal

			(cAlias)->(dbSkip())

			//Verifica tamanho da p�gina
			If nPos >= Self:pageSize
				Exit
			EndIf
		End

		oRetJson["hasNext"] := (cAlias)->(!Eof())

		(cAlias)->(dbCloseArea())

		oRetJson["items"] := aDados

		//Adiciona os filtros
		oRetJson["filters"] := Array(4)
		oRetJson["filters"][1] := JsonObject():New()
		oRetJson["filters"][1]["label"      ] := STR0004 //"Etiqueta / EPC"
		oRetJson["filters"][1]["property"   ] := "tag"
		oRetJson["filters"][1]["type"       ] := "text"
		oRetJson["filters"][1]["placeholder"] := "Ex: 3074257BF7194E4000001A85"
		oRetJson["filters"][1]["maxLength"  ] := GetSX3Cache("HWW_EPC", "X3_TAMANHO")
		oRetJson["filters"][2] := JsonObject():New()
		oRetJson["filters"][2]["label"      ] := STR0005 //"Produto"
		oRetJson["filters"][2]["property"   ] := "product"
		oRetJson["filters"][2]["type"       ] := "text"
		oRetJson["filters"][2]["maxLength"  ] := GetSX3Cache("HWN_PROD", "X3_TAMANHO")
		oRetJson["filters"][3] := JsonObject():New()
		oRetJson["filters"][3]["label"      ] := STR0006 //"Data Inicial"
		oRetJson["filters"][3]["property"   ] := "initialDate"
		oRetJson["filters"][3]["type"       ] := "date"
		If lDataPad
			oRetJson["filters"][3]["value"] := self:initialDate
		EndIf
		oRetJson["filters"][4] := JsonObject():New()
		oRetJson["filters"][4]["label"   ] := STR0007 //"Data Final"
		oRetJson["filters"][4]["property"] := "finishDate"
		oRetJson["filters"][4]["type"    ] := "date"
		If lDataPad
			oRetJson["filters"][4]["value"] := Self:finishDate
		EndIf

		//Adiciona as propriedades de exibi��o das informa��es
		oRetJson["columns"] := Array(nTotal)
		For nIndex := 1 To nTotal
			oRetJson["columns"][nIndex] := JsonObject():New()
			oRetJson["columns"][nIndex]["property"] := __aMapFld[nIndex][3]
			oRetJson["columns"][nIndex]["label"   ] := __aMapFld[nIndex][4]

			If __aMapFld[nIndex][2] == "HWW_DATA"
				oRetJson["columns"][nIndex]["type"] := "date"
			EndIf
		Next nIndex

		Self:SetResponse(EncodeUtf8(oRetJson:ToJson()))
	EndIf
	FreeObj(oRetJson)
	aSize(aDados, 0)

Return lRet

/*/{Protheus.doc} POST epc
M�todo de cria��o de nova EPC
URL: localhost:5050/rest/api/rfid/v1/core/epc

@author lucas.franca
@since 27/07/2020
@version P12.1.31
@return lRet, L�gico, Retorno do m�todo POST
/*/
WSMETHOD POST epc WSSERVICE rfid
	Local cAlias     := ""
	Local cBody      := ""
	Local cError     := ""
	Local cEPC       := ""
	Local cFunName   := ""
	Local cProduto   := ""
	Local cLote      := ""
	Local cSubLote   := ""
	Local cNumSerie  := ""
	Local cMessage   := ""
	Local cQuery     := ""
	Local lRet       := .T.
	Local lLote      := .F.
	Local lSubLote   := .F.
	Local nPos       := 0
	Local nTamPrd    := GetSX3Cache("B1_COD"    , "X3_TAMANHO")
	Local nTamLote   := GetSX3Cache("B8_LOTECTL", "X3_TAMANHO")
	Local nTamSbLote := GetSX3Cache("B8_NUMLOTE", "X3_TAMANHO")
	Local nTamSerie  := GetSX3Cache("BF_NUMSERI", "X3_TAMANHO")
	Local oBody      := JsonObject():New()

	Self:SetContentType("application/json")

	cBody := Self:GetContent()

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		cError := oBody:FromJson(cBody)
		If !Empty(cError)
			criaMsgErr(STR0019, cError) //"Erro ao interpretar os dados recebidos."
			lRet := .F.
		EndIf

		//Faz a valida��o dos dados recebidos.
		If lRet .And. Empty(oBody["quantity"])
			lRet := .F.
			If oBody["quantity"] == Nil
				criaMsgErr(STR0020, STR0021) //"Quantidade n�o informada." || "Informe a quantidade da EPC (quantity)."
			Else
				criaMsgErr(STR0022, STR0021) //"Quantidade n�o pode ser 0." || "Informe a quantidade da EPC (quantity)."
			EndIf
		EndIf

		If lRet .And. oBody["quantity"] < 0
			lRet := .F.
			criaMsgErr(STR0023, STR0024) //"Quantidade n�o pode ser negativa." || "Informe a quantidade da EPC (quantity) com valores positivos."
		EndIf

		If lRet .And. Empty(oBody["keys"])
			lRet := .F.
			criaMsgErr(STR0025, STR0026) //"Atributo 'keys' n�o recebido." || "Atributo 'keys' n�o foi recebido com os dados necess�rios para cria��o da EPC."
		EndIf

		If lRet .And. (ValType(oBody["keys"]) != "A" .Or. Len(oBody["keys"]) < 1 )
			lRet := .F.
			criaMsgErr(STR0027, STR0028) //"Atributo 'keys' n�o recebido no formato correto." || "Atributo 'keys' foi recebido, mas n�o est� no formato especificado. � esperado uma lista com as informa��es para cria��o da EPC."
		EndIf

		If lRet
			//Recupera os dados do produto
			nPos := aScan(oBody["keys"], {|x| x["code"] == "product"})
			If nPos > 0
				cProduto := PadR(oBody["keys"][nPos]["value"], nTamPrd)
			EndIf
			nPos := aScan(oBody["keys"], {|x| x["code"] == "lotCode"})
			If nPos > 0
				cLote := PadR(oBody["keys"][nPos]["value"], nTamLote)
			EndIf
			nPos := aScan(oBody["keys"], {|x| x["code"] == "subLotCode"})
			If nPos > 0
				cSubLote := PadR(oBody["keys"][nPos]["value"], nTamSbLote)
			EndIf
			nPos := aScan(oBody["keys"], {|x| x["code"] == "serialNumber"})
			If nPos > 0
				cNumSerie := PadR(oBody["keys"][nPos]["value"], nTamSerie)
			EndIf

			//Valida os dados do produto
			If Empty(cProduto)
				lRet := .F.
				criaMsgErr(STR0029, STR0030) //"C�digo do produto n�o informado." || "N�o foi recebido o c�digo do produto (product) na lista de informa��es para cria��o da EPC."
			ElseIf !UsaRFID(cProduto)
				lRet := .F.
				criaMsgErr(STR0031, STR0032 + AllTrim(cProduto) + STR0033) //"Produto n�o possui controle de RFID." || "O produto '"xxx"' n�o possui controle de RFID parametrizado. Somente produtos com controle de RFID podem ser utilizados para gerar EPCs."
			EndIf

			//Valida o lote/sub-lote
			If lRet
				lLote    := Rastro(cProduto)
				lSubLote := Rastro(cProduto, "S")

				If lLote .And. Empty(cLote)
					lRet := .F.
					criaMsgErr(STR0034, STR0035) //"Lote n�o foi informado." || "O n�mero do lote � obrigat�rio para produtos que possuem controle de lote."
				ElseIf !lLote .And. !Empty(cLote)
					lRet := .F.
					criaMsgErr(STR0036, STR0037) //"Produto sem controle de lote." || "O n�mero do lote n�o pode ser informado para produtos que n�o possuem controle de lote."
				ElseIf lSubLote .And. Empty(cSubLote)
					lRet := .F.
					criaMsgErr(STR0038, STR0039) //"Sub-lote n�o foi informado." || "O sub-lote � obrigat�rio para produtos que possuem controle de sub-lote."
				ElseIf !lSubLote .And. !Empty(cSubLote)
					lRet := .F.
					criaMsgErr(STR0040, STR0041) //"Produto sem controle de sub-lote." || "O sub-lote n�o pode ser informado para produtos que n�o possuem controle de sub-lote."
				EndIf

				If lRet .And. lLote
					cAlias := GetNextAlias()
					cQuery := " SELECT COUNT(*) TOTAL "
					cQuery +=   " FROM " + RetSqlname("SB8") + " SB8 "
					cQuery +=  " WHERE SB8.B8_FILIAL  = '" + xFilial("SB8") + "' "
					cQuery +=    " AND SB8.B8_PRODUTO = '" + cProduto + "' "
					cQuery +=    " AND SB8.B8_LOTECTL = '" + cLote + "' "
					If lSubLote
						cQuery += " AND SB8.B8_NUMLOTE = '" + cSubLote + "' "
					EndIf
					cQuery +=    " AND SB8.D_E_L_E_T_ = ' ' "

					dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)
					If (cAlias)->(TOTAL) == 0
						lRet := .F.
						If lSubLote
							cMessage := STR0042 + AllTrim(cLote) + STR0043 + AllTrim(cSubLote) + STR0044 // "O lote '" XXX"' e sub-lote '"YYY"' n�o existem. Informe um lote e sub-lote existente."
						Else
							cMessage := STR0042 + AllTrim(cLote) + STR0045 // "O lote '"ZZZ"' n�o existe. Informe um lote existente."
						EndIf
						criaMsgErr(STR0046, cMessage) //"Lote inv�lido."
					EndIf
					(cAlias)->(dbCloseArea())
				EndIf
			EndIf

			//Valida n�mero de s�rie
			If lRet .And. !Empty(cNumSerie)
				If Localiza(cProduto)
					cAlias := GetNextAlias()
					cQuery := " SELECT COUNT(*) TOTAL "
					cQuery +=   " FROM " + RetSqlName("SBF") + " SBF "
					cQuery +=  " WHERE SBF.BF_FILIAL  = '" + xFilial("SBF") + "' "
					cQuery +=    " AND SBF.BF_PRODUTO = '" + cProduto + "' "
					cQuery +=    " AND SBF.BF_NUMSERI = '" + cNumSerie + "' "
					If lLote
						cQuery += " AND SBF.BF_LOTECTL = '" + cLote + "' "
					EndIf
					If lSubLote
						cQuery += " AND SBF.BF_NUMLOTE = '" + cSubLote + "' "
					EndIf
					cQuery +=    " AND SBF.D_E_L_E_T_ = ' ' "

					dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)
					If (cAlias)->(TOTAL) == 0
						If lLote
							cMessage := STR0047 + AllTrim(cNumSerie) + STR0048 + AllTrim(cLote) + "'" //"O n�mero de s�rie '"XXX"' n�o existe para o lote '"ZZZ"'"
							If lSubLote
								cMessage += STR0049 + AllTrim(cSubLote) + "'." //" e sub-lote '"
							EndIf
							cMessage += "."
						Else
							cMessage := STR0047 + AllTrim(cNumSerie) + STR0050 //"O n�mero de s�rie '" XXX "' n�o existe."
						EndIf
						lRet := .F.
						criaMsgErr(STR0051, cMessage) //"N�mero de s�rie inv�lido."
					EndIf
					(cAlias)->(dbCloseArea())
				Else
					lRet := .F.
					criaMsgErr(STR0052, STR0053) //"Produto sem controle de endere�amento." || "O n�mero de s�rie n�o pode ser informado para produtos que n�o possuem controle de endere�amento."
				EndIf
			EndIf
		EndIf

		//Dados v�lidos, faz a cria��o da EPC.
		If lRet
			If SVK->VK_COD != cProduto .Or. SVK->VK_FILIAL != xFilial("SVK")
				SVK->(dbSetOrder(1))
				SVK->(dbSeek(xFilial("SVK")+cProduto))
			EndIf
			//Altera o FUNNAME para registrar na tabela da rastreabilidade (HWW)
			cFunName := FunName()
			SetFunName("rfid")
			cEPC := RFIDNewEPC(cProduto, cLote, cSubLote, cNumSerie, oBody["quantity"])
			//Retorna o FUNNAME original.
			SetFunName(cFunName)
 			Self:SetResponse('{"code":"'+cEPC+'"}')
		EndIf
	EndIf

	FreeObj(oBody)
Return lRet


/*/{Protheus.doc} PUT epc
M�todo de altera��o de quantidade da EPC
URL: localhost:5050/rest/api/rfid/v1/core/epc

@author lucas.franca
@since 28/07/2020
@version P12.1.31
@return lRet, L�gico, Retorno do m�todo PUT
/*/
WSMETHOD PUT epc PATHPARAM epc WSSERVICE rfid
	Local cBody    := ""
	Local cError   := ""
	Local cRetorno := ""
	Local lRet     := .T.
	Local oBody    := JsonObject():New()

	Self:SetContentType("application/json")

	cBody := Self:GetContent()

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		cError := oBody:FromJson(cBody)
		If !Empty(cError)
			criaMsgErr(STR0019, cError) //"Erro ao interpretar os dados recebidos."
			lRet := .F.
		EndIf
	EndIf

	//Faz a valida��o dos dados recebidos.
	If lRet .And. Empty(oBody["quantity"])
		lRet := .F.
		If oBody["quantity"] == Nil
			criaMsgErr(STR0020, STR0021) //"Quantidade n�o informada." || "Informe a quantidade da EPC (quantity)."
		Else
			criaMsgErr(STR0022, STR0021) //"Quantidade n�o pode ser 0." ||"Informe a quantidade da EPC (quantity)."
		EndIf
	EndIf

	If lRet .And. oBody["quantity"] < 0
		lRet := .F.
		criaMsgErr(STR0023, STR0024) //"Quantidade n�o pode ser negativa." || "Informe a quantidade da EPC (quantity) com valores positivos."
	EndIf

	If lRet .And. Empty(Self:epc)
		criaMsgErr(STR0054, STR0055) //"C�digo da EPC n�o informado." || "O c�digo da EPC n�o foi informado, e este � obrigat�rio para realizar a modifica��o da EPC."
		lRet := .F.
	EndIf

	If lRet
		Self:epc := PadR(Self:epc, GetSX3Cache("HWN_EPC","X3_TAMANHO"))
		HWN->(dbSetOrder(1))
		If ! HWN->(dbSeek(xFilial("HWN")+Self:epc))
			lRet := .F.
			criaMsgErr(STR0056, STR0057) //"C�digo da EPC inv�lido." || "O c�digo da EPC n�o foi encontrado no sistema."
		ElseIf HWN->HWN_STATUS == '0'
			lRet := .F.
			criaMsgErr(STR0056, STR0060) //"C�digo da EPC inv�lido." || "A EPC est� desativada. N�o � permitido modificar a quantidade de EPC que n�o est� ativa."
		Else
			BEGIN TRANSACTION
				//Altera a quantidade na HWN
				RecLock("HWN", .F.)
					HWN->HWN_QUANT := oBody["quantity"]
				MsUnLock()
				//Gera o registro de rastreabilidade
				If !RFIDGrvRas({{Self:epc, oBody["quantity"]}}, "3", "RFID")
					DisarmTransaction()
					lRet := .F.
					criaMsgErr(STR0058, tcSQLError()) //"Erro ao gravar a rastreabilidade da modifica��o de quantidade da etiqueta."
				EndIf
			END TRANSACTION

			cRetorno := getEpc(Self:epc)
			Self:SetResponse(EncodeUtf8(cRetorno))
		EndIf
	EndIf

	FreeObj(oBody)

Return lRet

/*/{Protheus.doc} GET epc
M�todo que recupera os dados de uma EPC

URL: localhost:5050/rest/api/rfid/v1/core/epc/{epc}
@author marcelo.neumann
@since 05/08/2020
@version P12
@return lRet, L�gico, Retorno do m�todo GET
/*/
WSMETHOD GET epc WSRECEIVE epc WSSERVICE rfid
	Local cError   := ""
	Local cRetorno := ""
	Local lAtivo   := .F.
	Local lExiste  := .F.
	Local lRet     := .T.
	Local oRetorno := JsonObject():New()

	Self:SetContentType("application/json")

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		cRetorno := getEpc(Self:epc, @lExiste, @lAtivo)
		If !lExiste
			criaMsgErr(STR0056, STR0057) //"C�digo da EPC inv�lido." || "O c�digo da EPC n�o foi encontrado no sistema."
			lRet := .F.
		ElseIf !lAtivo
			criaMsgErr(STR0056, STR0061) //"C�digo da EPC inv�lida." || "A EPC informada n�o est� ativa."
			lRet := .F.
		Else
			cError := oRetorno:FromJson(cRetorno)
			If Empty(cError)
				oRetorno:FromJson(cRetorno)
				If RFIDGrvRas({{oRetorno["code"], oRetorno["quantity"]}}, "2", "RFID")
					Self:SetResponse(EncodeUtf8(cRetorno))
				Else
					criaMsgErr(STR0075, tcSQLError()) //"Erro ao gravar a rastreabilidade da EPC."
					lRet := .F.
				EndIf
			Else
				criaMsgErr(STR0074, cError) //"Erro ao preparar os dados de retorno."
				lRet := .F.
			EndIf
		EndIf
	EndIf

	FreeObj(oRetorno)

Return lRet

/*/{Protheus.doc} POST listepc
M�todo que recupera uma lista de EPC

URL: localhost:5050/rest/api/rfid/v1/core/listepc
@author marcelo.neumann
@since 05/08/2020
@version P12
@return lRet, L�gico, Retorno do m�todo POST
/*/
WSMETHOD POST listepc WSSERVICE rfid
	Local aDataEPC  := {}
	Local aDataHWW  := {}
	Local lRet      := .T.
	Local nIndex    := 0
	Local nPosItems := 0
	Local nTamEpc   := GetSX3Cache("HWP_EPC", "X3_TAMANHO")
	Local nTotBody  := 0
	Local nTotEpc   := 0
	Local oBody     := JsonObject():New()
	Local oItems    := JsonObject():New()
	Local oRetJson  := JsonObject():New()
	Local oTags     := JsonObject():New()

	Self:SetContentType("application/json")

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		cBody  := Self:GetContent()
		cError := oBody:FromJson(cBody)
		If !Empty(cError)
			criaMsgErr(STR0019, cError) //"Erro ao interpretar os dados recebidos."
			lRet := .F.
		EndIf

		//Faz a valida��o dos dados recebidos
		If lRet
			If oBody["tags"] == Nil
				criaMsgErr(STR0062, STR0065) //"Lista de EPC n�o recebida." || "N�o foi recebido a lista de EPC no formato correto ('tags')."
				lRet := .F.
			ElseIf ValType(oBody["tags"]) <> "A"
				criaMsgErr(STR0063, STR0065) //"Lista de EPC incorreta." || "N�o foi recebido a lista de EPC no formato correto ('tags')."
				lRet := .F.
			ElseIf Len(oBody["tags"]) == 0
				criaMsgErr(STR0064, STR0066) //"Lista de EPC vazia." || "N�o existe nenhuma EPC para buscar ('tags')."
				lRet := .F.
			EndIf
		EndIf

		//Cria o JSON de retorno
		IF lRet
			oRetJson["columns"]                := Array(6)
			oRetJson["columns"][1]             := JsonObject():New()
			oRetJson["columns"][1]["property"] := "product"
			oRetJson["columns"][1]["label"   ] := FWX3Titulo("HWN_PROD")
			oRetJson["columns"][2]             := JsonObject():New()
			oRetJson["columns"][2]["property"] := "description"
			oRetJson["columns"][2]["label"   ] := FWX3Titulo("B1_DESC")
			oRetJson["columns"][3]             := JsonObject():New()
			oRetJson["columns"][3]["property"] := "lotCode"
			oRetJson["columns"][3]["label"   ] := FWX3Titulo("HWN_LOTE")
			oRetJson["columns"][4]             := JsonObject():New()
			oRetJson["columns"][4]["property"] := "subLotCode"
			oRetJson["columns"][4]["label"   ] := FWX3Titulo("HWN_SUBLOT")
			oRetJson["columns"][5]             := JsonObject():New()
			oRetJson["columns"][5]["property"] := "serialNumber"
			oRetJson["columns"][5]["label"   ] := FWX3Titulo("HWN_NUMSER")
			oRetJson["columns"][6]             := JsonObject():New()
			oRetJson["columns"][6]["property"] := "quantity"
			oRetJson["columns"][6]["label"   ] := FWX3Titulo("HWN_QUANT")
			oRetJson["columns"][6]["type"    ] := "label"

			oRetJson["items"] := {}
			oRetJson["tags" ] := {}

			//Percorre pelas EPCs informadas
			nTotBody := Len(oBody["tags"])
			For nIndex := 1 To nTotBody
				cEpc := PadR(oBody["tags"][nIndex], nTamEpc)

				//Verifica se a EPC est� duplicada no body
				If oTags[cEpc] == Nil
					oTags[cEpc] := .T.
					aDataEPC  := RFIDGetEpc(cEpc)
					cProduto  := aDataEPC[RFIDGetCnE("ARR_DATAEPC_PRODUTO" )]
					cLote     := aDataEPC[RFIDGetCnE("ARR_DATAEPC_LOTE"    )]
					cSubLote  := aDataEPC[RFIDGetCnE("ARR_DATAEPC_SUBLOTE" )]
					cNumSerie := aDataEPC[RFIDGetCnE("ARR_DATAEPC_NUMSERIE")]

					nTotEpc++
					aAdd(oRetJson["tags"], JsonObject():New())

					oRetJson["tags"][nTotEpc]["code"]    := cEpc
					oRetJson["tags"][nTotEpc]["isValid"] := IIf(aDataEPC[RFIDGetCnE("ARR_DATAEPC_ATIVO" )], "true", "false")

					If aDataEPC[RFIDGetCnE("ARR_DATAEPC_ATIVO")]
						//Posiciona na tabela de produto para buscar a descri��o
						If SB1->B1_COD != cProduto .Or. SB1->B1_FILIAL != xFilial("SB1")
							SB1->(dbSetOrder(1))
							SB1->(dbSeek(xFilial("SB1")+cProduto))
						EndIf

						oRetJson["tags"][nTotEpc]["description"] := RTrim(cProduto) + " - " + RTrim(SB1->B1_DESC)
						If !Empty(cLote)
							oRetJson["tags"][nTotEpc]["description"] += " - " + STR0067 + ": " + RTrim(cLote) //"LOTE"
							If !Empty(cSubLote)
								oRetJson["tags"][nTotEpc]["description"] += " - " + STR0068 + ": " + RTrim(cSubLote) //"SUB-LOTE"
							EndIf
						EndIf
						If !Empty(cNumSerie)
							oRetJson["tags"][nTotEpc]["description"] += " - " + STR0069 + ": " + RTrim(cNumSerie) //"S�RIE"
						EndIf

						//Verifica se a EPC � de um mesmo produto (lote, sublote e s�ria)
						nPosItems := oItems[cProduto + cLote + cSubLote + cNumSerie]
						If nPosItems == Nil
							aAdd(oRetJson["items"], JsonObject():New())
							nPosItems := Len(oRetJson["items"])
							oItems[cProduto + cLote + cSubLote + cNumSerie] := nPosItems

							oRetJson["items"][nPosItems]["product"     ] := cProduto
							oRetJson["items"][nPosItems]["description" ] := RTrim(SB1->B1_DESC)
							oRetJson["items"][nPosItems]["lotCode"     ] := cLote
							oRetJson["items"][nPosItems]["subLotCode"  ] := cSubLote
							oRetJson["items"][nPosItems]["serialNumber"] := cNumSerie
							oRetJson["items"][nPosItems]["read"        ] := aDataEPC[RFIDGetCnE("ARR_DATAEPC_QUANT")]
						Else
							oRetJson["items"][nPosItems]["read"        ] += aDataEPC[RFIDGetCnE("ARR_DATAEPC_QUANT")]
						EndIf

						aAdd(aDataHWW, {cEpc, aDataEPC[RFIDGetCnE("ARR_DATAEPC_QUANT")]})
					Else
						If aDataEPC[RFIDGetCnE("ARR_DATAEPC_EXISTE")]
							oRetJson["tags"][nTotEpc]["description"] := STR0070 //"EPC est� desativada."
						Else
							oRetJson["tags"][nTotEpc]["description"] := STR0071 //"EPC n�o registrada no sistema."
						EndIf
					EndIf

					aSize(aDataEPC, 0)
				EndIf
			Next nIndex

			//Grava a leitura das EPCs na tabela de rastreabilidade
			If RFIDGrvRas(aDataHWW, "2", "RFID")
				Self:SetResponse(EncodeUtf8(oRetJson:ToJson()))
			Else
				criaMsgErr(STR0075, tcSQLError()) //"Erro ao gravar a rastreabilidade da EPC."
				lRet := .F.
			EndIf
			aSize(aDataHWW, 0)
		EndIf
	EndIf

	FreeObj(oBody)
	FreeObj(oItems)
	FreeObj(oRetJson)
	FreeObj(oTags)

Return lRet

/*/{Protheus.doc} MapFld
Monta o mapeamento (de/para) dos campos das tabelas GET x JSON
@type Static Function
@author marcelo.neumann
@since 21/07/2020
@version P12
@return aMap, Array, Array com o mapeamento da tabela HWW
/*/
Static Function MapFld()
	Local aMap := {}

	aAdd(aMap, {"HWW", "HWW_EPC"   , "tag"         , FWX3Titulo("HWW_EPC"   ), "C", GetSX3Cache("HWW_EPC"   ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWN", "HWN_PROD"  , "product"     , FWX3Titulo("HWN_PROD"  ), "C", GetSX3Cache("HWN_PROD"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWN", "HWN_LOTE"  , "lotCode"     , FWX3Titulo("HWN_LOTE"  ), "C", GetSX3Cache("HWN_LOTE"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWN", "HWN_SUBLOT", "subLotCode"  , FWX3Titulo("HWN_SUBLOT"), "C", GetSX3Cache("HWN_SUBLOT","X3_TAMANHO"), 0})
	aAdd(aMap, {"HWN", "HWN_NUMSER", "serialNumber", FWX3Titulo("HWN_NUMSER"), "C", GetSX3Cache("HWN_NUMSER","X3_TAMANHO"), 0})
	aAdd(aMap, {"HWW", "HWW_DATA"  , "date"        , FWX3Titulo("HWW_DATA"  ), "D", GetSX3Cache("HWW_DATA"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWW", "HWW_HORA"  , "time"        , FWX3Titulo("HWW_HORA"  ), "C", GetSX3Cache("HWW_HORA"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWW", "HWW_USER"  , "user"        , FWX3Titulo("HWW_USER"  ), "C", GetSX3Cache("HWW_USER"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWW", "HWW_QTD"   , "quantity"    , FWX3Titulo("HWW_QTD"   ), "N", GetSX3Cache("HWW_QTD"   ,"X3_TAMANHO"), GetSX3Cache("HWW_QTD", "X3_DECIMAL")})
	aAdd(aMap, {"HWW", "HWW_ROTINA", "routine"     , FWX3Titulo("HWW_ROTINA"), "C", GetSX3Cache("HWW_ROTINA","X3_TAMANHO"), 0})
	aAdd(aMap, {"HWW", "HWW_ACAO"  , "action"      , FWX3Titulo("HWW_ACAO"  ), "C", GetSX3Cache("HWW_ACAO"  ,"X3_TAMANHO"), 0})
Return aMap

/*/{Protheus.doc} vldFilial
Verifica se a filial recebida na requisi��o � uma filial v�lida
@type Static Function
@author marcelo.neumann
@since 22/07/2020
@version P12
@param cFilAPI, Character, Filial recebida no header da requisi��o HTTP
@return lRet, Logic, Indica se a filial recebida � v�lida
/*/
Static Function vldFilial(cFilAPI)
	Local cMessage := ""
	Local lRet     := .T.

	If Empty(cFilAPI)
		cMessage := STR0008 //"Filial n�o foi informada."
		lRet     := .F.
	EndIf

	If lRet
		If __oFilAPI[cFilAPI] == Nil
			__oFilAPI[cFilAPI]    := Array(3)
			__oFilAPI[cFilAPI][1] := PadR(cFilAPI, FWSizeFilial(cEmpAnt))

			If !FWFilExist(cEmpAnt, __oFilAPI[cFilAPI][1])
				cMessage := STR0009 + " '" + __oFilAPI[cFilAPI][1] + "' " + STR0010 // "Filial 'XX' n�o existe."
				lRet     := .F.
			EndIf

			__oFilAPI[cFilAPI][2] := lRet
			__oFilAPI[cFilAPI][3] := cMessage
			cFilAPI               := __oFilAPI[cFilAPI][1]
		Else
			cMessage := __oFilAPI[cFilAPI][3]
			lRet     := __oFilAPI[cFilAPI][2]
			cFilAPI  := __oFilAPI[cFilAPI][1]
		EndIf
	EndIf

	//Filial est� v�lida. Verifica se � necess�rio mudar a filial logada
	If lRet .And. cFilAPI != cFilAnt
		cFilAnt := cFilAPI
	EndIf

	//Filial n�o est� v�lida. Cria mensagem de erro para retorno
	If !lRet
		criaMsgErr(STR0011, cMessage) //"Filial inv�lida."
	EndIf

Return lRet

/*/{Protheus.doc} validDic
Valida se o dicion�rio de dados possui as tabelas necess�rias para utiliza��o desta API
@type Static Function
@author marcelo.neumann
@since 21/07/2020
@version P12
@return lRet, Logic, Indica se o dicion�rio de dados est� correto.
/*/
Static Function validDic()
	Local lRet := .T.

	If __lDic == Nil
		__lDic := FWAliasInDic("HWN", .F.) .And. FWAliasInDic("HWW", .F.)
	EndIf
	lRet := __lDic

	If !lRet
		SetRestFault(400, EncodeUtf8(STR0012), .T., 400, EncodeUtf8(STR0013)) //"Dicion�rio de dados desatualizado." - "As tabelas necess�rias para execu��o deste processo n�o existem no dicion�rio de dados."
	EndIf

Return lRet

/*/{Protheus.doc} convDate
Faz a convers�o de uma data em string
@type Static Function
@author marcelo.neumann
@since 21/07/2020
@version P12
@param dData, Date, Data para converter
@return cData, Character, Data no formato STRING AAAA-MM-DD
/*/
Static Function convDate(dData)
	Local cData := ""
	cData := StrZero(Year(dData),4) + "-" + StrZero(Month(dData),2) + "-" + StrZero(Day(dData),2)
Return cData

/*/{Protheus.doc} getDesCBox
Retorna a descri��o do campo ComboBox
@type Static Function
@author marcelo.neumann
@since 22/07/2020
@version P12.1.30
@param 01 cAlias, Character, Alias do campo
@param 02 cCampo, Character, Nnome do campo a ser retornada a descri��o do combobox
@return   cDesc , Character, Descri��o da op��o no combobox
/*/
Static Function getDesCBox(cAlias, cCampo)
	Local aOpcoes := RetSX3Box(GetSX3Cache(cCampo, "X3_CBOX"),,,1)
	Local cDesc   := ""
	Local nOrigem := Val((cAlias)->&(cCampo))

	If nOrigem > 0
		cDesc := aOpcoes[nOrigem][3]
	EndIf

Return cDesc

/*/{Protheus.doc} criaMsgErr
Cria o retorno da mensagem de erro
@type  Static Function
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@param cMensagem, Character, Mensagem de erro
@param cDetalhe , Character, Mensagem detalhada de erro
@return Nil
/*/
Static Function criaMsgErr(cMensagem, cDetalhe)

	SetRestFault(400, EncodeUtf8(cMensagem), .T., 400, EncodeUtf8(cDetalhe))

Return Nil

/*/{Protheus.doc} getRotina
Converte o nome da rotina da tabela HWW para exibi��o em tela.

@type  Static Function
@author lucas.franca
@since 24/07/2020
@version P12.1.31
@param cRotina, Character, Rotina da tabela HWW
@return cRotina, Character, Rotina com a nomenclatura correta.
/*/
Static Function getRotina(cRotina)
	cRotina := AllTrim(Upper(cRotina))
	Do Case
		Case cRotina == "RFIDINVENTORY"
			cRotina := STR0014 //"Invent�rio RFID"
		Case cRotina == "RFIDSEPARATIONORDER"
			cRotina := STR0015 //"Confer�ncia de separa��o RFID"
		Case cRotina == "RFID"
			cRotina := STR0016 //"Portal RFID"
	EndCase
Return cRotina

/*/{Protheus.doc} getEpc
Retorna os dados de uma EPC

@type  Static Function
@author lucas.franca
@since 28/07/2020
@version P12.1.31
@param 01 cEpc   , Character, C�digo da EPC para pesquisa
@param 02 lExiste, Logical  , Retorna se a EPC existe (refer�ncia)
@param 03 lAtivo , Logical  , Retorna se a EPC est� ativa (refer�ncia)
@return cRetorno , Character, JSON com os dados da EPC
/*/
Static Function getEpc(cEpc, lExiste, lAtivo)
	Local aDataEPC := {}
	Local cRetorno := ""
	Local nPos     := 0
	Local nSaldo   := 0
	Local oEpc     := JsonObject():New()

	cEpc     := PadR(cEpc, GetSX3Cache("HWN_EPC","X3_TAMANHO"))
	aDataEPC := RFIDGetEpc(cEpc)
	lExiste  := aDataEPC[RFIDGetCnE("ARR_DATAEPC_EXISTE")]
	lAtivo   := aDataEPC[RFIDGetCnE("ARR_DATAEPC_ATIVO")]

	If lExiste
		If SB1->B1_COD != aDataEPC[RFIDGetCnE("ARR_DATAEPC_PRODUTO")] .Or. SB1->B1_FILIAL != xFilial("SB1")
			SB1->(dbSetOrder(1))
			SB1->(dbSeek(xFilial("SB1")+aDataEPC[RFIDGetCnE("ARR_DATAEPC_PRODUTO")]))
		EndIf

		oEpc["code"    ] := cEpc
		oEpc["quantity"] := aDataEPC[RFIDGetCnE("ARR_DATAEPC_QUANT")]
		oEpc["keys"    ] := {}
		oEpc["extras"  ] := ""

		nPos++
		aAdd(oEpc["keys"], JsonObject():New())
		oEpc["keys"][nPos]["code" ] := "product"
		oEpc["keys"][nPos]["label"] := FWX3Titulo("HWN_PROD")
		oEpc["keys"][nPos]["value"] := aDataEPC[RFIDGetCnE("ARR_DATAEPC_PRODUTO")]

		nPos++
		aAdd(oEpc["keys"], JsonObject():New())
		oEpc["keys"][nPos]["code" ] := "description"
		oEpc["keys"][nPos]["label"] := FWX3Titulo("B1_DESC")
		oEpc["keys"][nPos]["value"] := SB1->B1_DESC

		If !Empty(aDataEPC[RFIDGetCnE("ARR_DATAEPC_LOTE")])
			nPos++
			aAdd(oEpc["keys"], JsonObject():New())
			oEpc["keys"][nPos]["code" ] := "lotCode"
			oEpc["keys"][nPos]["label"] := FWX3Titulo("HWN_LOTE")
			oEpc["keys"][nPos]["value"] := aDataEPC[RFIDGetCnE("ARR_DATAEPC_LOTE")]
		EndIf

		If !Empty(aDataEPC[RFIDGetCnE("ARR_DATAEPC_SUBLOTE")])
			nPos++
			aAdd(oEpc["keys"], JsonObject():New())
			oEpc["keys"][nPos]["code" ] := "subLotCode"
			oEpc["keys"][nPos]["label"] := FWX3Titulo("HWN_SUBLOT")
			oEpc["keys"][nPos]["value"] := aDataEPC[RFIDGetCnE("ARR_DATAEPC_SUBLOTE")]
		EndIf

		If !Empty(aDataEPC[RFIDGetCnE("ARR_DATAEPC_NUMSERIE")])
			nPos++
			aAdd(oEpc["keys"], JsonObject():New())
			oEpc["keys"][nPos]["code" ] := "serialNumber"
			oEpc["keys"][nPos]["label"] := FWX3Titulo("HWN_NUMSER")
			oEpc["keys"][nPos]["value"] := aDataEPC[RFIDGetCnE("ARR_DATAEPC_NUMSERIE")]
		EndIf

		SB2->(dbSetOrder(1))
		If SB2->(dbSeek(xFilial("SB2")+aDataEPC[RFIDGetCnE("ARR_DATAEPC_PRODUTO")]))
			While SB2->(!Eof()) .And. SB2->(B2_FILIAL+B2_COD) == xFilial("SB2")+aDataEPC[RFIDGetCnE("ARR_DATAEPC_PRODUTO")]
				nSaldo := SaldoSB2(,,,,,"SB2",,,,,)
				If nSaldo <> 0
					If !Empty(oEpc["extras"])
						oEpc["extras"] += "\n"
					EndIf
					oEpc["extras"] += STR0059 + AllTrim(SB2->B2_LOCAL) + ": " + cValToChar(nSaldo) //"Saldo armaz�m " XX: 10
				EndIf
				SB2->(dbSkip())
			End
		EndIf

		cRetorno := oEpc:ToJson()
	EndIf

	aSize(aDataEPC, 0)
	FreeObj(oEpc)

Return cRetorno