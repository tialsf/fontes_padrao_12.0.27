#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "RFIDInventory.ch"

Static __oFilAPI := JsonObject():New()
Static __aMapHWO := mapHWO()
Static __aMapHWQ := mapHWQ()
Static __lDic    := Nil

#DEFINE ARR_EPC_FILIAL  1
#DEFINE ARR_EPC_ID      2
#DEFINE ARR_EPC_COD_EPC 3
#DEFINE ARR_EPC_QUANT   4
#DEFINE ARR_EPC_STATUS  5
#DEFINE ARR_EPC_PRODUTO 6
#DEFINE ARR_EPC_LOTE    7
#DEFINE ARR_EPC_SUBLOTE 8
#DEFINE ARR_EPC_SERIE   9
#DEFINE ARR_EPC_SIZE    9

#DEFINE ARR_HWQ_FILIAL  1
#DEFINE ARR_HWQ_ID      2
#DEFINE ARR_HWQ_PROD    3
#DEFINE ARR_HWQ_LOTE    4
#DEFINE ARR_HWQ_SUBLOTE 5
#DEFINE ARR_HWQ_NUMSERI 6
#DEFINE ARR_HWQ_QTLIDA  7
#DEFINE ARR_HWQ_QTERP   8
#DEFINE ARR_HWQ_RECNO   9 //RECNO deve ser sempre a �ltima posi��o
#DEFINE ARR_HWQ_SIZE    9

/*/{Protheus.doc} rfidinventory
API para realiza��o de um invent�rio pelo RFID

@type  API
@author lucas.franca
@since 29/05/2020
@version P12.1.30
/*/
WSRESTFUL rfidinventory DESCRIPTION STR0001 FORMAT APPLICATION_JSON //"API de Invent�rio do RFID"

	WSDATA id       AS STRING  OPTIONAL
	WSDATA page     AS INTEGER OPTIONAL
	WSDATA pageSize AS INTEGER OPTIONAL
	WSDATA search   AS STRING  OPTIONAL


	WSMETHOD POST init;
	   DESCRIPTION STR0002; //"Inicia o processo de invent�rio"
	   WSSYNTAX    "api/pcp/v1/rfidinventory/init";
	   PATH        "/api/pcp/v1/rfidinventory/init";
	   TTALK       "v1"

	WSMETHOD GET inventory;
	   DESCRIPTION STR0003; //"Busca os invent�rios existentes"
	   WSSYNTAX    "api/pcp/v1/rfidinventory";
	   PATH        "/api/pcp/v1/rfidinventory";
	   TTALK       "v1"

	WSMETHOD POST read;
	   DESCRIPTION STR0004; //"Insere/Atualiza os dados de leitura do RFID"
	   WSSYNTAX    "api/pcp/v1/rfidinventory/{id}";
	   PATH        "/api/pcp/v1/rfidinventory/{id}";
	   TTALK       "v1"

	WSMETHOD GET read;
	   DESCRIPTION STR0005; //"Retorna os dados de leitura do RFID"
	   WSSYNTAX    "api/pcp/v1/rfidinventory/{id}";
	   PATH        "/api/pcp/v1/rfidinventory/{id}";
	   TTALK       "v1"

	WSMETHOD POST confirm;
	   DESCRIPTION STR0006; //"Confirma os dados de leitura do RFID"
	   WSSYNTAX    "api/pcp/v1/rfidinventory/confirm/{id}";
	   PATH        "/api/pcp/v1/rfidinventory/confirm/{id}";
	   TTALK       "v1"

	WSMETHOD POST cancel;
	   DESCRIPTION STR0007; //"Cancela os dados de leitura do RFID"
	   WSSYNTAX    "api/pcp/v1/rfidinventory/cancel/{id}";
	   PATH        "/api/pcp/v1/rfidinventory/cancel/{id}";
	   TTALK       "v1"

ENDWSRESTFUL

/*/{Protheus.doc} POST init
M�todo que inicia o processo de invent�rio no RFID

C�digo da filial recebido no header da requisi��o com o nome "filial".

URL: localhost:5050/rest/api/pcp/v1/rfidinventory/init
JSON recebido:
{
   "keys": [
       {
           "code": "HWO_LOCAL",
           "label": "Armaz�m",
           "value": "01"
       },
       {
           "code": "HWO_ENDER",
           "label": "Endere�o",
           "value": "C1P2"
       }
    ]
}

@author lucas.franca
@since 29/05/2020
@version P12.1.30
@return lRet, Logic, Indica que a requisi��o foi processada com sucesso ou n�o.
/*/
WSMETHOD POST init WSSERVICE rfidinventory
	Local cAlias   := ""
	Local cBody    := ""
	Local cError   := ""
	Local cIdHWO   := ""
	Local cQuery   := ""
	Local lRet     := .T.
	Local nPosArm  := 0
	Local nPosEnd  := 0
	Local oBody    := JsonObject():New()
	Local oRetJson := JsonObject():New()

	Self:SetContentType("application/json")

	cBody := Self:GetContent()

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		cError := oBody:FromJson(cBody)
		If !Empty(cError)
			criaMsgErr(STR0008, cError) //"Erro ao interpretar os dados recebidos."
			lRet := .F.
		EndIf
	EndIf

	If lRet .And. (oBody["keys"] == Nil .Or. ValType(oBody["keys"]) != "A")
		criaMsgErr(STR0008, STR0009) //"Erro ao interpretar os dados recebidos." | "Atributo 'keys' n�o foi recebido corretamente."
		lRet := .F.
	EndIf

	//Valida as informa��es recebidas em "keys"
	If lRet
		nPosArm := aScan(oBody["keys"], {|x| x["code"] == "HWO_LOCAL" })
		nPosEnd := aScan(oBody["keys"], {|x| x["code"] == "HWO_ENDER" })
		If nPosArm == 0
			criaMsgErr(STR0010, STR0011) //"C�digo do armaz�m para realizar o invent�rio n�o foi informado." | "N�o foi identificado nos dados recebidos a informa��o do Armaz�m para realizar o invent�rio (HWO_LOCAL)."
			lRet := .F.
		ElseIf Empty(oBody["keys"][nPosArm]["value"])
			criaMsgErr(STR0010, STR0012) //"C�digo do armaz�m para realizar o invent�rio n�o foi informado." | "Identificado a informa��o do armaz�m (HWO_LOCAL) nos dados recebidos, mas o armaz�m n�o tem o seu valor informado."
			lRet := .F.
		Else
			oBody["keys"][nPosArm]["value"] := PadR(oBody["keys"][nPosArm]["value"], GetSX3Cache("NNR_CODIGO", "X3_TAMANHO"))
			NNR->(dbSetOrder(1))
			If !NNR->(dbSeek(xFilial("NNR")+oBody["keys"][nPosArm]["value"]))
				criaMsgErr(STR0013, STR0014 + " '" + oBody["keys"][nPosArm]["value"] + "' " + STR0015) //"Armaz�m inv�lido." | "O armaz�m 'XX' n�o est� cadastrado."
				lRet := .F.
			EndIf
		EndIf

		If lRet .And. nPosEnd > 0 .And. !Empty(oBody["keys"][nPosEnd]["value"])
			oBody["keys"][nPosEnd]["value"] := PadR(oBody["keys"][nPosEnd]["value"], GetSX3Cache("BE_LOCALIZ", "X3_TAMANHO"))
			SBE->(dbSetOrder(1))
			If !SBE->(dbSeek(xFilial("SBE")+oBody["keys"][nPosArm]["value"]+oBody["keys"][nPosEnd]["value"]))
				criaMsgErr(STR0016, STR0017 + " '" + oBody["keys"][nPosEnd]["value"] + "' " + STR0018 + " '" + oBody["keys"][nPosArm]["value"] + "'.") //"Endere�o inv�lido." | "O endere�o 'XX' n�o est� cadastrado para o armaz�m 'XX'."
				lRet := .F.
			EndIf
		EndIf
	EndIf

	If lRet
		cQuery := " SELECT HWO.HWO_ID "
		cQuery +=   " FROM " + RetSqlName("HWO") + " HWO "
		cQuery +=  " WHERE HWO.HWO_FILIAL = '" + xFilial("HWO") + "' "
		cQuery +=    " AND HWO.HWO_LOCAL  = '" + oBody["keys"][nPosArm]["value"] + "' "
		If nPosEnd > 0 .And. !Empty(oBody["keys"][nPosEnd]["value"])
			cQuery += " AND HWO.HWO_ENDER  = '" + oBody["keys"][nPosEnd]["value"] + "' "
		Else
			cQuery += " AND HWO.HWO_ENDER  = ' ' "
		EndIf
		cQuery +=    " AND HWO.HWO_STATUS = '1' "
		cQuery +=    " AND HWO.D_E_L_E_T_ = ' ' "

		cAlias := GetNextAlias()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)
		If (cAlias)->(!Eof())
			criaMsgErr(STR0060, STR0061 + AllTrim((cAlias)->HWO_ID) + STR0062) //"J� existe um invent�rio ativo para este armaz�m/endere�o." || "O invent�rio com ID 'xxx' est� ativo. Finalize este invent�rio antes de iniciar um novo invent�rio para o mesmo armaz�m e endere�o."
			lRet := .F.
		EndIf
		(cAlias)->(dbCloseArea())
	EndIf

	//Verifica se existem produtos com controle de RFID para os par�metros informados.
	If lRet
		If nPosEnd > 0 .And. !Empty(oBody["keys"][nPosEnd]["value"])
			cQuery := " SELECT COUNT(*) TOTPRD "
			cQuery +=   " FROM " + RetSqlName("SBF") + " SBF "
			cQuery +=  " INNER JOIN " + RetSqlName("SVK") + " SVK "
			cQuery +=     " ON SVK.VK_FILIAL = '" + xFilial("SVK") + "' "
			cQuery +=    " AND SVK.VK_COD    = SBF.BF_PRODUTO "
			cQuery +=    " AND SVK.VK_RFID   = '1' "
			cQuery +=    " AND SVK.D_E_L_E_T_ = ' ' "
			cQuery +=  " WHERE SBF.BF_FILIAL  = '" + xFilial("SBF") + "' "
			cQuery +=    " AND SBF.BF_LOCAL   = '" + oBody["keys"][nPosArm]["value"] + "' "
			cQuery +=    " AND SBF.BF_LOCALIZ = '" + oBody["keys"][nPosEnd]["value"] + "' "
			cQuery +=    " AND SBF.D_E_L_E_T_ = ' ' "
		Else
			cQuery := " SELECT COUNT(*) TOTPRD "
			cQuery +=   " FROM " + RetSqlName("SB2") + " SB2 "
			cQuery +=  " INNER JOIN " + RetSqlName("SVK") + " SVK "
			cQuery +=     " ON SVK.VK_FILIAL = '" + xFilial("SVK") + "' "
			cQuery +=    " AND SVK.VK_COD    = SB2.B2_COD "
			cQuery +=    " AND SVK.VK_RFID   = '1' "
			cQuery +=    " AND SVK.D_E_L_E_T_ = ' ' "
			cQuery +=  " WHERE SB2.B2_FILIAL  = '" + xFilial("SB2") + "' "
			cQuery +=    " AND SB2.B2_LOCAL   = '" + oBody["keys"][nPosArm]["value"] + "' "
			cQuery +=    " AND SB2.D_E_L_E_T_ = ' ' "
		EndIf

		cAlias := GetNextAlias()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

		If (cAlias)->(TOTPRD) < 1
			criaMsgErr(STR0019, STR0020) //"Armaz�m/Endere�o sem controle de RFID." | "Os produtos contidos neste Armaz�m/Endere�o n�o possuem controle de RFID."
			lRet := .F.
		EndIf

		(cAlias)->(dbCloseArea())
	EndIf

	//Dados v�lidos. Inclui o registro na tabela HWO.
	If lRet
		cIdHWO := GetSxeNum("HWO","HWO_ID")
		RecLock("HWO",.T.)
			HWO->HWO_FILIAL := xFilial("HWO")
			HWO->HWO_ID     := cIdHWO
			HWO->HWO_USER   := cUserName
			HWO->HWO_DATA   := Date()
			HWO->HWO_HORA   := Time()
			HWO->HWO_LOCAL  := oBody["keys"][nPosArm]["value"]
			HWO->HWO_ENDER  := Iif(nPosEnd > 0, oBody["keys"][nPosEnd]["value"], "")
			HWO->HWO_STATUS := "1"
			HWO->HWO_DOCUM  := ""
		HWO->(MsUnLock())
		ConfirmSX8()

		oRetJson["id"] := cIdHWO
		Self:SetResponse(oRetJson:ToJson())
	EndIf

	FreeObj(oBody)
	FreeObj(oRetJson)

Return lRet

/*/{Protheus.doc} GET inventory
M�todo que recupera os invent�rios realizados

URL: localhost:5050/rest/api/pcp/v1/rfidinventory?page=1&pageSize=5&search=2

@author lucas.franca
@since 29/05/2020
@version P12.1.30
@return Nil
/*/
WSMETHOD GET inventory WSRECEIVE page, pageSize, search WSSERVICE rfidinventory
	Local aDados   := {}
	Local cAlias   := ""
	Local cQuery   := ""
	Local lRet     := .T.
	Local nIndex   := 0
	Local nPos     := 0
	Local nStart   := 0
	Local nTotal   := Len(__aMapHWO)
	Local oRetJson := JsonObject():New()

	DEFAULT Self:page     := 1
	DEFAULT Self:pageSize := 20

	Self:SetContentType("application/json")

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		cQuery := " SELECT "
		For nIndex := 1 To nTotal
			If nIndex > 1
				cQuery += ","
			EndIf
			cQuery += " HWO." + __aMapHWO[nIndex][1]
		Next nIndex
		cQuery +=  " FROM " + RetSqlName("HWO") + " HWO "
		cQuery += " WHERE HWO.HWO_FILIAL = '" + xFilial("HWO") + "' "
		cQuery +=   " AND HWO.D_E_L_E_T_ = ' ' "
		If !Empty(Self:search)
			cQuery += " AND HWO.HWO_ID LIKE '%" + Self:search + "%' "
		EndIf
		cQuery += " ORDER BY HWO.HWO_DATA DESC, HWO.HWO_HORA DESC "

		cAlias := GetNextAlias()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

		For nIndex := 1 To nTotal
			If __aMapHWO[nIndex][4] $ "|D|L|N|"
				TcSetField(cAlias, __aMapHWO[nIndex][1],;
				                   __aMapHWO[nIndex][4],;
				                   __aMapHWO[nIndex][5],;
				                   __aMapHWO[nIndex][6])
			EndIf
		Next nIndex

		If Self:page > 1
			nStart := ( (Self:page-1) * Self:pageSize )
			If nStart > 0
				(cAlias)->(DbSkip(nStart))
			EndIf
		EndIf

		While (cAlias)->(!Eof())
			aAdd(aDados, JsonObject():New())
			nPos := Len(aDados)

			For nIndex := 1 To nTotal
				If __aMapHWO[nIndex][4] == "D"
					aDados[nPos][__aMapHWO[nIndex][2]] := convDate(&("(cAlias)->" + __aMapHWO[nIndex][1]))
				Else
					aDados[nPos][__aMapHWO[nIndex][2]] := &("(cAlias)->" + __aMapHWO[nIndex][1])
				EndIf
			Next nTotal

			(cAlias)->(dbSkip())

			//Verifica tamanho da p�gina
			If nPos >= Self:pageSize
				Exit
			EndIf
		End

		oRetJson["hasNext"] := (cAlias)->(!Eof())

		(cAlias)->(dbCloseArea())

		oRetJson["items"] := aDados

		//Adiciona as propriedades de exibi��o das informa��es
		oRetJson["columns"] := Array(nTotal)
		For nIndex := 1 To nTotal
			oRetJson["columns"][nIndex] := JsonObject():New()
			oRetJson["columns"][nIndex]["property"] := __aMapHWO[nIndex][2]
			oRetJson["columns"][nIndex]["label"   ] := __aMapHWO[nIndex][3]
			Do Case
				Case __aMapHWO[nIndex][1] == "HWO_DATA"
					oRetJson["columns"][nIndex]["type"] := "date"
				Case __aMapHWO[nIndex][1] == "HWO_STATUS"
					oRetJson["columns"][nIndex]["type"] := "label"
					oRetJson["columns"][nIndex]["labels"] := Array(3)

					oRetJson["columns"][nIndex]["labels"][1] := JsonObject():New()
					oRetJson["columns"][nIndex]["labels"][1]["color"] := "color-10"
					oRetJson["columns"][nIndex]["labels"][1]["value"] := "1"
					oRetJson["columns"][nIndex]["labels"][1]["label"] := STR0050 //"Ativo"

					oRetJson["columns"][nIndex]["labels"][2] := JsonObject():New()
					oRetJson["columns"][nIndex]["labels"][2]["color"] := "color-03"
					oRetJson["columns"][nIndex]["labels"][2]["value"] := "2"
					oRetJson["columns"][nIndex]["labels"][2]["label"] := STR0051 //"Finalizado"

					oRetJson["columns"][nIndex]["labels"][3] := JsonObject():New()
					oRetJson["columns"][nIndex]["labels"][3]["color"] := "color-07"
					oRetJson["columns"][nIndex]["labels"][3]["value"] := "3"
					oRetJson["columns"][nIndex]["labels"][3]["label"] := STR0052 //"Cancelado"
			EndCase
		Next nIndex

		Self:SetResponse(EncodeUtf8(oRetJson:ToJson()))
	EndIf
	FreeObj(oRetJson)
	aSize(aDados, 0)

Return lRet

/*/{Protheus.doc} POST read
M�todo para processar as leituras efetuadas pelo RFID

URL: localhost:5050/rest/api/pcp/v1/rfidinventory/0000000028
JSON recebido:
{
    "tags":[
        "000000000110000000000001",
        "000000000120000000000004"
    ]
}

@author lucas.franca
@since 29/05/2020
@version P12.1.30
@return Nil
/*/
WSMETHOD POST read PATHPARAM id WSSERVICE rfidinventory
	Local aEpcProc   := {}
	Local aDadosHWQ  := {}
	Local aDadosInc  := {}
	Local aDadosAlt  := {}
	Local aDadosRas  := {}
	Local cBody      := ""
	Local cError     := ""
	Local cCols      := ""
	Local lRet       := .T.
	Local nIndex     := 0
	Local nTotal     := 0
	Local oBody      := JsonObject():New()

	Self:SetContentType("application/json")

	cBody := Self:GetContent()

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		cError := oBody:FromJson(cBody)
		If !Empty(cError)
			criaMsgErr(STR0008, cError) //"Erro ao interpretar os dados recebidos."
			lRet := .F.
		EndIf
	EndIf

	If lRet
		If Empty(Self:id)
			criaMsgErr(STR0021, STR0022) //"Identificador de invent�rio n�o foi informado." | "Identificador do invent�rio inv�lido, pois est� em branco ou n�o foi recebido."
			lRet := .F.
		Else
			Self:id := PadR(Self:id, GetSX3Cache("HWO_ID","X3_TAMANHO"))
			HWO->(dbSetOrder(1))
			If !HWO->(dbSeek(xFilial("HWO")+Self:id))
				criaMsgErr(STR0023, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0025) //"Identificador de invent�rio inv�lido." | "Identificador do invent�rio 'XX' n�o existe."
				lRet := .F.
			ElseIf HWO->HWO_STATUS <> '1'
				criaMsgErr(STR0023, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0026) //"Identificador de invent�rio inv�lido." | "Identificador do invent�rio 'XX' j� foi finalizado ou cancelado."
				lRet := .F.
			EndIf
		EndIf

		If lRet .And. oBody["tags"] == Nil
			criaMsgErr(STR0027, STR0028) //"Lista de EPC n�o recebida." | "N�o foi recebido a lista de EPC no formato correto ('tags')."
			lRet := .F.
		ElseIf ValType(oBody["tags"]) <> "A"
			criaMsgErr(STR0029, STR0030) //"Lista de EPC incorreta." | "N�o foi recebido a lista de EPC no formato correto ('tags')."
			lRet := .F.
		ElseIf Len(oBody["tags"]) == 0
			criaMsgErr(STR0031, STR0032) //"Lista de EPC vazia." | "N�o existe nenhuma EPC para processar ('tags')."
			lRet := .F.
		EndIf
	EndIf

	If lRet
		//Verifica quais s�o as EPCs que devem ser consideradas
		aEpcProc := getEPCProc(Self:id, oBody["tags"])

		//Gera os dados para a tabela HWQ
		aDadosHWQ := carregaHWQ(Self:id, aEpcProc)

		//Remove do array aEpcProc as informa��es auxiliares que n�o ser�o inclu�das na tabela HWP
		aEval(aEpcProc, {|x| aSize(x, 5)})

		//Separa o array aDadosHWQ em dois arrays.
		//aDadosInc -> Dados da HWQ que ser�o inclu�dos na tabela
		//aDadosAlt -> Dados da HWQ que ser�o modificados na tabela
		nTotal := Len(aDadosHWQ)
		For nIndex := 1 To nTotal
			If aDadosHWQ[nIndex][ARR_HWQ_RECNO] == 0
				//N�o possui RECNO, far� inclus�o
				//o aSize � para remover a coluna de RECNO, que n�o ser� utilizada na inclus�o.
				aAdd(aDadosInc, aSize(aClone(aDadosHWQ[nIndex]), ARR_HWQ_SIZE-1) )
			Else
				//Possui RECNO, far� atualiza��o.
				aAdd(aDadosAlt, {aDadosHWQ[nIndex][ARR_HWQ_RECNO], aDadosHWQ[nIndex][ARR_HWQ_QTLIDA]})
			EndIf
		Next nIndex

		//Faz a inclus�o/modifica��o das tabelas HWP e HWQ
		BEGIN TRANSACTION
			nTotal := Len(aEpcProc)
			If nTotal > 0
				cCols := "HWP_FILIAL,HWP_ID,HWP_EPC,HWP_QUANT,HWP_STATUS"
				If TCDBInsert(RetSqlName("HWP"), cCols, aEpcProc) < 0
					DisarmTransaction()
					lRet := .F.
					criaMsgErr(STR0033, tcSQLError()) //"Erro ao atualizar os dados das etiquetas lidas."
				Else
					For nIndex := 1 To nTotal
						aAdd(aDadosRas, {aEpcProc[nIndex][ARR_EPC_COD_EPC], aEpcProc[nIndex][ARR_EPC_QUANT]})
					Next nIndex

					//Grava a rastreabilidade da etiqueta
					If !RFIDGrvRas(aDadosRas, "2", "rfidinventory")
						DisarmTransaction()
						lRet := .F.
						criaMsgErr(STR0063, tcSQLError()) //"Erro ao gravar a rastreabilidade das etiquetas lidas."
					EndIf
					aSize(aDadosRas,0)
				EndIf
			EndIf

			If lRet .And. Len(aDadosInc) > 0
				cCols := "HWQ_FILIAL,HWQ_ID,HWQ_PROD,HWQ_LOTE,HWQ_SUBLOT,HWQ_NUMSER,HWQ_QTLIDA,HWQ_QTERP"
				If TCDBInsert(RetSqlName("HWQ"), cCols, aDadosInc) < 0
					DisarmTransaction()
					lRet := .F.
					criaMsgErr(STR0033, tcSQLError()) //"Erro ao atualizar os dados das etiquetas lidas."
				EndIf
			EndIf

			nTotal := Len(aDadosAlt)
			If lRet .And. nTotal > 0
				For nIndex := 1 To nTotal
					HWQ->(dbGoTo(aDadosAlt[nIndex][1]))
					RecLock("HWQ",.F.)
						HWQ->HWQ_QTLIDA := aDadosAlt[nIndex][2]
					MsUnLock()
				Next nIndex
			EndIf
		END TRANSACTION
	EndIf

	If lRet
		cJsonRet := getInvent(Self:id)
		Self:SetResponse(EncodeUtf8(cJsonRet))
	EndIf

	FreeObj(oBody)
	aSize(aEpcProc , 0)
	aSize(aDadosHWQ, 0)
	aSize(aDadosInc, 0)
	aSize(aDadosAlt, 0)
Return lRet

/*/{Protheus.doc} GET read
M�todo para retornar as leituras efetuadas pelo RFID

URL: localhost:5050/rest/api/pcp/v1/rfidinventory/0000000023

@author lucas.franca
@since 29/05/2020
@version P12.1.30
@return Nil
/*/
WSMETHOD GET read PATHPARAM id WSSERVICE rfidinventory
	Local cJson  := ""
	Local lRet   := .T.
	Local nTamID := GetSX3Cache("HWO_ID", "X3_TAMANHO")

	Self:SetContentType("application/json")

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		Self:id := PadR(Self:id, nTamID)
		cJson := getInvent(Self:id)
		If !Empty(cJson)
			Self:SetResponse(EncodeUtf8(cJson))
		Else
			lRet := .F.
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} POST confirm
M�todo para finalizar o processo de invent�rio do RFID

URL: localhost:5050/rest/api/pcp/v1/rfidinventory/confirm/0000000020

@author lucas.franca
@since 29/05/2020
@version P12.1.30
@return Nil
/*/
WSMETHOD POST confirm PATHPARAM id WSSERVICE rfidinventory
	Local cDocum := ""
	Local cErro  := ""
	Local lRet   := .T.
	Local nTamID := GetSX3Cache("HWO_ID", "X3_TAMANHO")
	Local oJson  := JsonObject():New()

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		Self:id := PadR(Self:id, nTamID)
		HWO->(dbSetOrder(1))
		If !HWO->(dbSeek(xFilial("HWO")+Self:id))
			criaMsgErr(STR0023, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0025) //"Identificador de invent�rio inv�lido." | "Identificador do invent�rio 'XX' n�o existe."
			lRet := .F.
		ElseIf HWO->HWO_STATUS <> '1'
			criaMsgErr(STR0023, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0026) //"Identificador de invent�rio inv�lido." | "Identificador do invent�rio 'XX' j� foi finalizado ou cancelado."
			lRet := .F.
		EndIf
	EndIf

	If lRet
		cDocum := GeraNumDoc()
		Begin Transaction
			cErro  := GravaSB7(@cDocum)
			If Empty(cErro)
				RecLock("HWO", .F.)
					HWO->HWO_STATUS := "2"
					HWO->HWO_DOCUM  := cDocum
				MsUnLock()
				oJson["message"] := STR0034 //"Invent�rio confirmado com sucesso."
			Else
				DisarmTransaction()
				criaMsgErr(STR0055, cErro) //"Erro ao gravar o Invent�rio no ERP."
				lRet := .F.
			EndIf
		End Transaction
		If lRet
			Self:SetResponse(EncodeUtf8(oJson:ToJson()))
		EndIf
	EndIf

	FreeObj(oJson)
Return lRet

/*/{Protheus.doc} POST cancel
M�todo para cancelar o processo de invent�rio do RFID

URL: localhost:5050/rest/api/pcp/v1/rfidinventory/cancel/0000000021

@author lucas.franca
@since 29/05/2020
@version P12.1.30
@return Nil
/*/
WSMETHOD POST cancel PATHPARAM id WSSERVICE rfidinventory
	Local lRet   := .T.
	Local nTamID := GetSX3Cache("HWO_ID", "X3_TAMANHO")
	Local oJson  := JsonObject():New()

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		Self:id := PadR(Self:id, nTamID)
		HWO->(dbSetOrder(1))
		If !HWO->(dbSeek(xFilial("HWO")+Self:id))
			criaMsgErr(STR0023, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0025) //"Identificador de invent�rio inv�lido." | "Identificador do invent�rio 'XX' n�o existe."
			lRet := .F.
		ElseIf HWO->HWO_STATUS <> '1'
			criaMsgErr(STR0023, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0026) //"Identificador de invent�rio inv�lido." | "Identificador do invent�rio 'XX' j� foi finalizado ou cancelado."
			lRet := .F.
		EndIf
	EndIf

	If lRet
		RecLock("HWO", .F.)
			HWO->HWO_STATUS := "3"
		MsUnLock()
		oJson["message"] := STR0035 //"Invent�rio cancelado com sucesso."
		Self:SetResponse(EncodeUtf8(oJson:ToJson()))
	EndIf

	FreeObj(oJson)
Return lRet

/*/{Protheus.doc} vldFilial
Verifica se a filial recebida na requisi��o � uma filial v�lida.

@type  Static Function
@author lucas.franca
@since 29/05/2020
@version P12.1.30
@param cFilAPI, Character, Filial recebida no header da requisi��o HTTP.
@return lRet, Logic, Indica se a filial recebida � v�lida.
/*/
Static Function vldFilial(cFilAPI)
	Local cMessage := ""
	Local lRet     := .T.

	If Empty(cFilAPI)
		cMessage := STR0036 //"Filial n�o foi informada."
		lRet     := .F.
	EndIf

	If lRet
		If __oFilAPI[cFilAPI] == Nil
			__oFilAPI[cFilAPI] := Array(3)
			__oFilAPI[cFilAPI][1] := PadR(cFilAPI, FWSizeFilial(cEmpAnt))

			If !FWFilExist(cEmpAnt, __oFilAPI[cFilAPI][1])
				cMessage := STR0037 + " '" + __oFilAPI[cFilAPI][1] + "' " + STR0025// "Filial 'XX' n�o existe."
				lRet     := .F.
			EndIf

			__oFilAPI[cFilAPI][2] := lRet
			__oFilAPI[cFilAPI][3] := cMessage
			cFilAPI               := __oFilAPI[cFilAPI][1]
		Else
			cMessage := __oFilAPI[cFilAPI][3]
			lRet     := __oFilAPI[cFilAPI][2]
			cFilAPI  := __oFilAPI[cFilAPI][1]
		EndIf
	EndIf

	//Filial est� v�lida. Verifica se � necess�rio mudar a filial logada.
	If lRet .And. cFilAPI != cFilAnt
		cFilAnt := cFilAPI
	EndIf

	//Filial n�o est� v�lida. Cria mensagem de erro para retorno
	If !lRet
		criaMsgErr(STR0038, cMessage) //"Filial inv�lida."
	EndIf

Return lRet

/*/{Protheus.doc} criaMsgErr
Cria o retorno da mensagem de erro

@type  Static Function
@author lucas.franca
@since 29/05/2020
@version P12.1.30
@param cMensagem, Character, Mensagem de erro
@param cDetalhe , Character, Mensagem detalhada de erro
@return Nil
/*/
Static Function criaMsgErr(cMensagem, cDetalhe)
	SetRestFault(400, EncodeUtf8(cMensagem), .T., 400, EncodeUtf8(cDetalhe))
Return Nil

/*/{Protheus.doc} mapHWO
Monta o mapeamento (de/para) dos campos da HWO x JSON

@type  Static Function
@author lucas.franca
@since 29/05/2020
@version P12.1.30
@return aMap, Array, Array com o mapeamento da tabela HWO
/*/
Static Function mapHWO()
	Local aMap := {}

	aAdd(aMap, {"HWO_FILIAL", "branchId" , FWX3Titulo("HWO_FILIAL"), "C", FWSizeFilial()                        , 0})
	aAdd(aMap, {"HWO_ID"    , "id"       , FWX3Titulo("HWO_ID"    ), "C", GetSX3Cache("HWO_ID"    ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWO_USER"  , "user"     , FWX3Titulo("HWO_USER"  ), "C", GetSX3Cache("HWO_USER"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWO_DATA"  , "date"     , FWX3Titulo("HWO_DATA"  ), "D", GetSX3Cache("HWO_DATA"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWO_HORA"  , "time"     , FWX3Titulo("HWO_HORA"  ), "C", GetSX3Cache("HWO_HORA"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWO_LOCAL" , "warehouse", FWX3Titulo("HWO_LOCAL" ), "C", GetSX3Cache("HWO_LOCAL" ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWO_ENDER" , "location" , FWX3Titulo("HWO_ENDER" ), "C", GetSX3Cache("HWO_ENDER" ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWO_STATUS", "status"   , FWX3Titulo("HWO_STATUS"), "C", GetSX3Cache("HWO_STATUS","X3_TAMANHO"), 0})
	aAdd(aMap, {"HWO_DOCUM" , "document" , FWX3Titulo("HWO_DOCUM" ), "C", GetSX3Cache("HWO_DOCUM" ,"X3_TAMANHO"), 0})
Return aMap

/*/{Protheus.doc} mapHWQ
Monta o mapeamento (de/para) dos campos da HWQ x JSON

@type  Static Function
@author lucas.franca
@since 29/05/2020
@version P12.1.30
@return aMap, Array, Array com o mapeamento da tabela HWO
/*/
Static Function mapHWQ()
	Local aMap := {}

	aAdd(aMap, {"HWQ_PROD"  , "product"       , FWX3Titulo("HWQ_PROD"  ), "C", GetSX3Cache("HWQ_PROD"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWQ_LOTE"  , "lotCode"       , FWX3Titulo("HWQ_LOTE"  ), "C", GetSX3Cache("HWQ_LOTE"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWQ_SUBLOT", "subLotCode"    , FWX3Titulo("HWQ_SUBLOT"), "C", GetSX3Cache("HWQ_SUBLOT","X3_TAMANHO"), 0})
	aAdd(aMap, {"HWQ_NUMSER", "serialNumber"  , FWX3Titulo("HWQ_NUMSER"), "C", GetSX3Cache("HWQ_NUMSER","X3_TAMANHO"), 0})
	aAdd(aMap, {"HWQ_QTLIDA", "read"          , FWX3Titulo("HWQ_QTLIDA"), "N", GetSX3Cache("HWQ_QTLIDA","X3_TAMANHO"), GetSX3Cache("HWQ_QTLIDA","X3_DECIMAL")})
	aAdd(aMap, {"HWQ_QTERP" , "expected"      , FWX3Titulo("HWQ_QTERP" ), "N", GetSX3Cache("HWQ_QTERP" ,"X3_TAMANHO"), GetSX3Cache("HWQ_QTERP" ,"X3_DECIMAL")})
	aAdd(aMap, {"_HWQ_DIF"  , "hasDifference" , STR0039 /*"Status"*/    , "L", 1                                     , 0})
Return aMap

/*/{Protheus.doc} convDate
Faz a convers�o de uma data em string

@type  Static Function
@author lucas.franca
@since 01/06/2020
@version P12.1.30
@param dData, Date, Data para converter
@return cData, Character, Data no formato STRING AAAA-MM-DD
/*/
Static Function convDate(dData)
	Local cData := ""
	cData := StrZero(Year(dData),4) + "-" + StrZero(Month(dData),2) + "-" + StrZero(Day(dData),2)
Return cData

/*/{Protheus.doc} getEPCProc
Retorna as informa��es das EPCs que ser�o processadas

@type  Static Function
@author lucas.franca
@since 02/06/2020
@version P12.1.30
@param cId  , Character, ID de processamento
@param aTags, Array    , Array com as EPCs recebidas na requisi��o
@return aEPC, Array, Array com os dados das EPCs para processar
/*/
Static Function getEPCProc(cId, aTags)
	Local aEPC      := {}
	Local aDadosEPC := {}
	Local cFilHWP   := xFilial("HWP")
	Local cEpc      := ""
	Local lTodas    := .F.
	Local nPos      := 0
	Local nIndex    := 0
	Local nTotal    := Len(aTags)
	Local nTamEpc   := GetSX3Cache("HWP_EPC", "X3_TAMANHO")
	Local oTagsProc := JsonObject():New()

	HWP->(dbSetOrder(1))
	lTodas := !HWP->(dbSeek(cFilHWP+cId))

	//Busca as informa��es das EPC's que devem ser consideradas
	For nIndex := 1 To nTotal
		cEpc := PadR(aTags[nIndex], nTamEpc)
		//Verifica se a tag est� duplicada
		If oTagsProc[cEpc] == Nil
			oTagsProc[cEpc] := .T.
			If lTodas .Or. !HWP->(dbSeek(cFilHWP+cId+cEpc))
				aDadosEPC := RFIDGetEpc(cEpc)

				aAdd(aEPC, Array(ARR_EPC_SIZE))
				nPos := Len(aEPC)

				aEPC[nPos][ARR_EPC_FILIAL ] := cFilHWP
				aEPC[nPos][ARR_EPC_ID     ] := cId
				aEPC[nPos][ARR_EPC_COD_EPC] := cEpc
				aEPC[nPos][ARR_EPC_QUANT  ] := aDadosEPC[RFIDGetCnE("ARR_DATAEPC_QUANT")]
				aEPC[nPos][ARR_EPC_STATUS ] := Iif(aDadosEPC[RFIDGetCnE("ARR_DATAEPC_EXISTE")] .And. aDadosEPC[RFIDGetCnE("ARR_DATAEPC_ATIVO")], "1", "0")
				aEPC[nPos][ARR_EPC_PRODUTO] := aDadosEPC[RFIDGetCnE("ARR_DATAEPC_PRODUTO")]
				aEPC[nPos][ARR_EPC_LOTE   ] := aDadosEPC[RFIDGetCnE("ARR_DATAEPC_LOTE")]
				aEPC[nPos][ARR_EPC_SUBLOTE] := aDadosEPC[RFIDGetCnE("ARR_DATAEPC_SUBLOTE")]
				aEPC[nPos][ARR_EPC_SERIE  ] := aDadosEPC[RFIDGetCnE("ARR_DATAEPC_NUMSERIE")]
				aSize(aDadosEPC, 0)
			EndIf
		EndIf
	Next nIndex

	FreeObj(oTagsProc)
Return aEPC

/*/{Protheus.doc} carregaHWQ
Carrega o array com os dados para atualiza��o da tabela HWQ

@type  Static Function
@author lucas.franca
@since 02/06/2020
@version P12.1.30
@param cId , Character, Identificador do invent�rio
@param aEpc, Array    , Array com as EPCs que ser�o processadas
@return aDados, Array , Array com os dados da HWQ para atualiza��o
/*/
Static Function carregaHWQ(cId, aEpc)
	Local aDados     := {}
	Local aSalQtd    := {}
	Local cAlias     := ""
	Local cFilHWQ    := xFilial("HWQ")
	Local cQuery     := ""
	Local cChave     := ""
	Local cProduto   := ""
	Local cLote      := ""
	Local cSubLote   := ""
	Local cSerie     := ""
	Local lIncAll    := .F.
	Local lWmsNew    := SuperGetMV("MV_WMSNEW",.F.,.F.)
	Local nPos       := 0
	Local nTamPrd    := GetSX3Cache("HWQ_PROD"  , "X3_TAMANHO")
	Local nTamLote   := GetSX3Cache("HWQ_LOTE"  , "X3_TAMANHO")
	Local nTamSubLot := GetSX3Cache("HWQ_SUBLOT", "X3_TAMANHO")
	Local nTamSerie  := GetSX3Cache("HWQ_NUMSER", "X3_TAMANHO")
	Local nTotal     := 0
	Local nIndex     := 0
	Local oIndHWQ    := JsonObject():New()
	Local oSaldoWMS  := Nil

	HWQ->(dbSetOrder(1))
	If !HWQ->(dbSeek(cFilHWQ+cId))
		lIncAll := .T.
		//Se ainda n�o existem dados na HWQ, faz a carga dos produtos que devem ser inclu�dos com as suas respectivas quantidades do ERP.

		//Busca da SB2 produtos sem controle de lote nem endere�o, e que possuem controle de RFID
		If Empty(HWO->HWO_ENDER)
			cQuery := " SELECT SB2.B2_COD  AS PRODUTO, "
			cQuery +=        " ' '         AS LOTE, "
			cQuery +=        " ' '         AS SUBLOTE, "
			cQuery +=        " ' '         AS SERIE "
			cQuery +=  " FROM " + RetSqlName("SB2") + " SB2 "
			cQuery += " INNER JOIN " + RetSqlName("SB1") + " SB1 "
			cQuery +=    " ON SB1.B1_FILIAL  = '" + xFilial("SB1") + "' "
			cQuery +=   " AND SB1.B1_COD     = SB2.B2_COD "
			cQuery +=   " AND SB1.D_E_L_E_T_ = ' ' "
			cQuery +=   " AND SB1.B1_LOCALIZ <> 'S' "
			cQuery +=   " AND SB1.B1_RASTRO  NOT IN ('L','S') "
			cQuery += " INNER JOIN " + RetSqlName("SVK") + " SVK "
			cQuery +=    " ON SVK.VK_FILIAL  = '" + xFilial("SVK") + "' "
			cQuery +=   " AND SVK.VK_COD     = SB1.B1_COD "
			cQuery +=   " AND SVK.D_E_L_E_T_ = ' ' "
			cQuery +=   " AND SVK.VK_RFID    = '1' "
			cQuery += " WHERE SB2.B2_FILIAL  = '" + xFilial("SB2") + "' "
			cQuery +=   " AND SB2.B2_LOCAL   = '" + HWO->HWO_LOCAL + "' "
			cQuery +=   " AND SB2.D_E_L_E_T_ = ' ' "

			//Busca da SB8 produtos sem controle de endere�o, e com controle de lote/sub-lote e RFID
			cQuery += " UNION "
			cQuery += " SELECT SB8.B8_PRODUTO AS PRODUTO, "
			cQuery +=        " SB8.B8_LOTECTL AS LOTE, "
			cQuery +=        " SB8.B8_NUMLOTE AS SUBLOTE, "
			cQuery +=        " ' '            AS SERIE "
			cQuery +=  " FROM " + RetSqlName("SB8") + " SB8 "
			cQuery += " INNER JOIN " + RetSqlName("SB1") + " SB1 "
			cQuery +=    " ON SB1.B1_FILIAL  = '" + xFilial("SB1") + "' "
			cQuery +=   " AND SB1.B1_COD     = SB8.B8_PRODUTO "
			cQuery +=   " AND SB1.D_E_L_E_T_ = ' ' "
			cQuery +=   " AND SB1.B1_LOCALIZ <> 'S' "
			cQuery +=   " AND SB1.B1_RASTRO  IN ('L','S') "
			cQuery += " INNER JOIN " + RetSqlName("SVK") + " SVK "
			cQuery +=    " ON SVK.VK_FILIAL  = '" + xFilial("SVK") + "' "
			cQuery +=   " AND SVK.VK_COD     = SB1.B1_COD "
			cQuery +=   " AND SVK.D_E_L_E_T_ = ' ' "
			cQuery +=   " AND SVK.VK_RFID    = '1' "
			cQuery += " WHERE SB8.B8_FILIAL  = '" + xFilial("SB8") + "' "
			cQuery +=   " AND SB8.B8_LOCAL   = '" + HWO->HWO_LOCAL + "' "
			cQuery +=   " AND SB8.D_E_L_E_T_ = ' ' "
			cQuery += " UNION "
		EndIf
		//Busca da SBF produtos com controle de endere�o e RFID
		cQuery += " SELECT SBF.BF_PRODUTO AS PRODUTO, "
		cQuery +=        " SBF.BF_LOTECTL AS LOTE, "
		cQuery +=        " SBF.BF_NUMLOTE AS SUBLOTE, "
		cQuery +=        " SBF.BF_NUMSERI AS SERIE "
		cQuery +=  " FROM " + RetSqlName("SBF") + " SBF "
		cQuery += " INNER JOIN " + RetSqlName("SB1") + " SB1 "
		cQuery +=    " ON SB1.B1_FILIAL  = '" + xFilial("SB1") + "' "
		cQuery +=   " AND SB1.B1_COD     = SBF.BF_PRODUTO "
		cQuery +=   " AND SB1.D_E_L_E_T_ = ' ' "
		cQuery +=   " AND SB1.B1_LOCALIZ = 'S' "
		cQuery += " INNER JOIN " + RetSqlName("SVK") + " SVK "
		cQuery +=    " ON SVK.VK_FILIAL  = '" + xFilial("SVK") + "' "
		cQuery +=   " AND SVK.VK_COD     = SB1.B1_COD "
		cQuery +=   " AND SVK.D_E_L_E_T_ = ' ' "
		cQuery +=   " AND SVK.VK_RFID    = '1' "
		cQuery += " WHERE SBF.BF_FILIAL  = '" + xFilial("SBF") + "' "
		cQuery +=   " AND SBF.BF_LOCAL   = '" + HWO->HWO_LOCAL + "' "
		cQuery +=   " AND SBF.BF_LOCALIZ = '" + HWO->HWO_ENDER + "' "
		cQuery +=   " AND SBF.D_E_L_E_T_ = ' ' "

		cQuery := ChangeQuery(cQuery)

		cAlias := GetNextAlias()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

		While (cAlias)->(!Eof())

			cProduto := PadR((cAlias)->PRODUTO, nTamPrd   )
			cLote    := PadR((cAlias)->LOTE   , nTamLote  )
			cSubLote := PadR((cAlias)->SUBLOTE, nTamSubLot)
			cSerie   := PadR((cAlias)->SERIE  , nTamSerie )

			If (Localiza(cProduto,.T.) .And. !Empty(HWO->HWO_ENDER+cSerie)) .Or. (Rastro(cProduto) .And. !Empty(cLote+cSubLote))
				If IntDl(cProduto) .And. lWmsNew
					oSaldoWMS 	:= WMSDTCEstoqueEndereco():New()
					aSalQtd := oSaldoWMS:SldPrdData(cProduto,HWO->HWO_LOCAL,HWO->HWO_DATA+1,cLote,cSubLote,HWO->HWO_ENDER,cSerie)
				Else
					aSalQtd := CalcEstL(cProduto,HWO->HWO_LOCAL,HWO->HWO_DATA+1,cLote,cSubLote,HWO->HWO_ENDER,cSerie)
				EndIf
			Else
				aSalQtd := CalcEst(cProduto,HWO->HWO_LOCAL,HWO->HWO_DATA+1)
			EndIf
			If aSalQtd[1] <> 0
				addArrayHWQ(cFilHWQ,;
				            cId,;
				            PadR((cAlias)->PRODUTO, nTamPrd   ),;
				            PadR((cAlias)->LOTE   , nTamLote  ),;
				            PadR((cAlias)->SUBLOTE, nTamSubLot),;
				            PadR((cAlias)->SERIE  , nTamSerie ),;
				            0,;
				            aSalQtd[1],;
				            0,;
				            @aDados,;
				            @oIndHWQ)
			EndIf

			aSize(aSalQtd, 0)
			(cAlias)->(dbSkip())
		End
		(cAlias)->(dbCloseArea())
	EndIf

	//Ordena os dados do array para diminuir a quantidade de dbseek feito na tabela HWQ.
	aSort(aEpc,,,{|x,y| x[ARR_EPC_PRODUTO]+x[ARR_EPC_LOTE]+x[ARR_EPC_SUBLOTE]+x[ARR_EPC_SERIE] < ;
	                    y[ARR_EPC_PRODUTO]+y[ARR_EPC_LOTE]+y[ARR_EPC_SUBLOTE]+y[ARR_EPC_SERIE]})

	//Recupera a quantidade das EPC's e soma no array aDados
	nTotal := Len(aEpc)
	For nIndex := 1 To nTotal
		If aEpc[nIndex][ARR_EPC_STATUS] <> "1"
			Loop
		EndIf
		nPos := oIndHWQ[aEpc[nIndex][ARR_EPC_PRODUTO]+;
		                aEpc[nIndex][ARR_EPC_LOTE   ]+;
		                aEpc[nIndex][ARR_EPC_SUBLOTE]+;
		                aEpc[nIndex][ARR_EPC_SERIE  ]]
		If nPos == Nil
			If lIncAll
				addArrayHWQ(cFilHWQ,;
				            cId,;
				            aEpc[nIndex][ARR_EPC_PRODUTO],;
				            aEpc[nIndex][ARR_EPC_LOTE   ],;
				            aEpc[nIndex][ARR_EPC_SUBLOTE],;
				            aEpc[nIndex][ARR_EPC_SERIE  ],;
				            aEpc[nIndex][ARR_EPC_QUANT  ],;
				            0,;
				            0,;
				            @aDados,;
				            @oIndHWQ)
			Else
				cChave := cFilHWQ+cId+aEpc[nIndex][ARR_EPC_PRODUTO]+aEpc[nIndex][ARR_EPC_LOTE]+aEpc[nIndex][ARR_EPC_SUBLOTE]+aEpc[nIndex][ARR_EPC_SERIE]
				If HWQ->(HWQ_FILIAL+HWQ_ID+HWQ_PROD+HWQ_LOTE+HWQ_SUBLOT+HWQ_NUMSER) == cChave .Or. HWQ->(dbSeek(cChave))
					addArrayHWQ(HWQ->HWQ_FILIAL,;
					            HWQ->HWQ_ID,;
					            HWQ->HWQ_PROD,;
					            HWQ->HWQ_LOTE,;
					            HWQ->HWQ_SUBLOT,;
					            HWQ->HWQ_NUMSER,;
					            HWQ->HWQ_QTLIDA + aEpc[nIndex][ARR_EPC_QUANT],;
					            HWQ->HWQ_QTERP,;
					            HWQ->(Recno()),;
					            @aDados,;
					            @oIndHWQ)
				Else
					addArrayHWQ(cFilHWQ,;
					            cId,;
					            aEpc[nIndex][ARR_EPC_PRODUTO],;
					            aEpc[nIndex][ARR_EPC_LOTE   ],;
					            aEpc[nIndex][ARR_EPC_SUBLOTE],;
					            aEpc[nIndex][ARR_EPC_SERIE  ],;
					            aEpc[nIndex][ARR_EPC_QUANT  ],;
					            0,;
					            0,;
					            @aDados,;
					            @oIndHWQ)
				EndIf
			EndIf
		Else
			aDados[nPos][ARR_HWQ_QTLIDA] += aEpc[nIndex][ARR_EPC_QUANT]
		EndIf
	Next nIndex

	FreeObj(oIndHWQ)
Return aDados

/*/{Protheus.doc} addArrayHWQ
Fun��o para centralizar a adi��o de itens no array de dados da tabela HWQ

@type  Static Function
@author lucas.franca
@since 02/06/2020
@version P12.1.30
@param cFilHWQ  , Character , C�digo da filial
@param cId      , Character , ID do processamento
@param cProduto , Character , C�digo do produto
@param cLote    , Character , N�mero do lote
@param cSubLote , Character , N�mero do sub-lote
@param cSerie   , Character , N�mero de s�rie
@param nQtLida  , Numeric   , Quantidade lida do produto
@param nQtErp   , Numeric   , Quantidade real no ERP
@param nRecno   , Numeric   , RECNO do registro da HWQ
@param aDadosHWQ, Array     , Array com os dados j� inseridos da HWQ. Passar por refer�ncia
@param oIndHWQ  , JsonObject, JSON com os �ndices do array aDadosHWQ. Passar por refer�ncia

@return Nil
/*/
Static Function addArrayHWQ(cFilHWQ, cId, cProduto, cLote, cSubLote, cSerie, nQtLida, nQtErp, nRecno, aDadosHWQ, oIndHWQ)
	Local nPos := 0

	aAdd(aDadosHWQ, Array(ARR_HWQ_SIZE))
	nPos := Len(aDadosHWQ)

	aDadosHWQ[nPos][ARR_HWQ_FILIAL ] := cFilHWQ
	aDadosHWQ[nPos][ARR_HWQ_ID     ] := cId
	aDadosHWQ[nPos][ARR_HWQ_PROD   ] := cProduto
	aDadosHWQ[nPos][ARR_HWQ_LOTE   ] := cLote
	aDadosHWQ[nPos][ARR_HWQ_SUBLOTE] := cSubLote
	aDadosHWQ[nPos][ARR_HWQ_NUMSERI] := cSerie
	aDadosHWQ[nPos][ARR_HWQ_QTLIDA ] := nQtLida
	aDadosHWQ[nPos][ARR_HWQ_QTERP  ] := nQtErp
	aDadosHWQ[nPos][ARR_HWQ_RECNO  ] := nRecno

	oIndHWQ[cProduto+cLote+cSubLote+cSerie] := nPos
Return Nil

/*/{Protheus.doc} getInvent
Busca os dados de um invent�rio

@type  Static Function
@author lucas.franca
@since 02/06/2020
@version P12.1.30
@param cId, Character, Identificador do processamento
@return cJson, Character, JSON com os dados.
/*/
Static Function getInvent(cId)
	Local cJson := ""
	Local oJson := JsonObject():New()

	HWO->(dbSetOrder(1))
	If !HWO->(dbSeek(xFilial("HWO")+cId))
		criaMsgErr(STR0040, STR0041 + " '" + cId + "' " + STR0042) // "Identificador do invent�rio n�o existe." | "O identificador 'XX' n�o foi encontrado na base de dados."
		Return Nil
	EndIf

	oJson["branchId"  ] := HWO->HWO_FILIAL
	oJson["id"        ] := HWO->HWO_ID
	oJson["isFinished"] := Iif(HWO->HWO_STATUS<>"1", .T., .F.)
	oJson["headers"   ] := inventHead()
	oJson["columns"   ] := inventCols()
	oJson["items"     ] := inventItms()
	oJson["tags"      ] := inventTags()

	cJson := oJson:ToJson()

	FreeObj(oJson)
Return cJson

/*/{Protheus.doc} inventHead
Monta os dados do cabe�alho do invent�rio

@type  Static Function
@author lucas.franca
@since 02/06/2020
@version P12.1.30
@return aHeaderInv, Array, Array com os dados de cabe�alho do invent�rio
/*/
Static Function inventHead()
	Local aHeaderInv := {}
	Local nPos       := 0
	Local nPosMap    := 0

	aAdd(aHeaderInv, JsonObject():New())
	nPos    := Len(aHeaderInv)
	nPosMap := aScan(__aMapHWO, {|x| x[1] == "HWO_FILIAL"})
	aHeaderInv[nPos]["label"   ] := __aMapHWO[nPosMap][3]
	aHeaderInv[nPos]["value"   ] := HWO->HWO_FILIAL
	aHeaderInv[nPos]["subtitle"] := FWFilName(cEmpAnt, cFilAnt)

	aAdd(aHeaderInv, JsonObject():New())
	nPos    := Len(aHeaderInv)
	nPosMap := aScan(__aMapHWO, {|x| x[1] == "HWO_LOCAL"})
	aHeaderInv[nPos]["label"   ] := __aMapHWO[nPosMap][3]
	aHeaderInv[nPos]["value"   ] := HWO->HWO_LOCAL
	aHeaderInv[nPos]["subtitle"] := Posicione("NNR", 1, xFilial("NNR")+HWO->HWO_LOCAL, "NNR_DESCRI")

	If !Empty(HWO->HWO_ENDER)
		aAdd(aHeaderInv, JsonObject():New())
		nPos    := Len(aHeaderInv)
		nPosMap := aScan(__aMapHWO, {|x| x[1] == "HWO_ENDER"})
		aHeaderInv[nPos]["label"   ] := __aMapHWO[nPosMap][3]
		aHeaderInv[nPos]["value"   ] := HWO->HWO_ENDER
		aHeaderInv[nPos]["subtitle"] := Posicione("SBE", 1, xFilial("SBE")+HWO->HWO_LOCAL+HWO->HWO_ENDER, "BE_DESCRIC")
	EndIf

	If !Empty(HWO->HWO_DOCUM)
		aAdd(aHeaderInv, JsonObject():New())
		nPos    := Len(aHeaderInv)
		nPosMap := aScan(__aMapHWO, {|x| x[1] == "HWO_DOCUM"})
		aHeaderInv[nPos]["label"   ] := __aMapHWO[nPosMap][3]
		aHeaderInv[nPos]["value"   ] := HWO->HWO_DOCUM
	EndIf
Return aHeaderInv

/*/{Protheus.doc} inventCols
Retorna os dados de colunas para exibi��o do invent�rio.

@type  Static Function
@author lucas.franca
@since 02/06/2020
@version P12.1.30
@return aColsInv, Array, Array com as colunas para exibi��o
/*/
Static Function inventCols()
	Local aColsInv := {}
	Local nIndex := 0
	Local nTotal := Len(__aMapHWQ)

	For nIndex := 1 To nTotal
		aAdd(aColsInv, JsonObject():New())
		aColsInv[nIndex]["property"] := __aMapHWQ[nIndex][2]
		aColsInv[nIndex]["label"   ] := __aMapHWQ[nIndex][3]

		If __aMapHWQ[nIndex][1] == "_HWQ_DIF"
			aColsInv[nIndex]["type"  ] := "label"
			aColsInv[nIndex]["labels"] := Array(2)

			aColsInv[nIndex]["labels"][1] := JsonObject():New()
			aColsInv[nIndex]["labels"][1]["color"] := "color-08"
			aColsInv[nIndex]["labels"][1]["value"] := .T.
			aColsInv[nIndex]["labels"][1]["label"] := STR0043 //"Diferen�a"

			aColsInv[nIndex]["labels"][2] := JsonObject():New()
			aColsInv[nIndex]["labels"][2]["color"] := "color-10"
			aColsInv[nIndex]["labels"][2]["value"] := .F.
			aColsInv[nIndex]["labels"][2]["label"] := STR0044 //"OK"
		EndIf
	Next nIndex

Return aColsInv

/*/{Protheus.doc} inventItms
Monta os dados da tabela HWQ para retorno

@type  Static Function
@author lucas.franca
@since 02/06/2020
@version P12.1.30
@return aItems, Array, Array com os dados para retorno
/*/
Static Function inventItms()
	Local aItems := {}
	Local cChave := xFilial("HWQ")+HWO->HWO_ID
	Local nIndex := 0
	Local nPos   := 0
	Local nTotal := Len(__aMapHWQ)

	HWQ->(dbSetOrder(1))
	If HWQ->(dbSeek(cChave))
		While HWQ->(!Eof()) .And. HWQ->(HWQ_FILIAL+HWQ_ID) == cChave
			nPos++
			aAdd(aItems, JsonObject():New())

			For nIndex := 1 To nTotal
				If __aMapHWQ[nIndex][1] == "_HWQ_DIF"
					aItems[nPos][__aMapHWQ[nIndex][2]] := HWQ->HWQ_QTERP <> HWQ->HWQ_QTLIDA
				Else
					aItems[nPos][__aMapHWQ[nIndex][2]] := &("HWQ->"+__aMapHWQ[nIndex][1])
				EndIf
			Next nIndex

			HWQ->(dbSkip())
		End
	EndIf
Return aItems

/*/{Protheus.doc} inventTags
Monta os dados das tags utilizadas na leitura

@type  Static Function
@author lucas.franca
@since 02/06/2020
@version P12.1.30
@return aTags, Array, Array com os dados para retorno
/*/
Static Function inventTags()
	Local aTags := {}
	Local cAlias := GetNextAlias()
	Local cQuery := ""
	Local nPos   := 0

	cQuery := " SELECT HWP.HWP_EPC, "
	cQuery +=        " HWP.HWP_STATUS, "
	cQuery +=        " HWN.HWN_PROD, "
	cQuery +=        " HWN.HWN_LOTE, "
	cQuery +=        " HWN.HWN_SUBLOT, "
	cQuery +=        " HWN.HWN_NUMSER, "
	cQuery +=        " SB1.B1_DESC "
	cQuery +=   " FROM " + RetSqlName("HWP") + " HWP "
	cQuery +=   " LEFT OUTER JOIN " + RetSqlName("HWN") + " HWN "
	cQuery +=     " ON HWN.HWN_FILIAL = '" + xFilial("HWN") + "' "
	cQuery +=    " AND HWN.HWN_EPC    = HWP.HWP_EPC "
	cQuery +=    " AND HWN.D_E_L_E_T_ = ' ' "
	cQuery +=   " LEFT OUTER JOIN " + RetSqlName("SB1") + " SB1 "
	cQuery +=     " ON SB1.B1_FILIAL  = '" + xFilial("SB1") + "' "
	cQuery +=    " AND SB1.B1_COD     = HWN.HWN_PROD "
	cQuery +=    " AND SB1.D_E_L_E_T_ = ' ' "
	cQuery +=  " WHERE HWP.HWP_FILIAL = '" + xFilial("HWP") + "' "
	cQuery +=    " AND HWP.HWP_ID     = '" + HWO->HWO_ID + "' "
	cQuery +=    " AND HWP.D_E_L_E_T_ = ' ' "
	cQuery +=  " ORDER BY HWP.HWP_EPC "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

	While (cAlias)->(!Eof())
		nPos++
		aAdd(aTags, JsonObject():New())

		aTags[nPos]["code"       ] := (cAlias)->HWP_EPC
		aTags[nPos]["isValid"    ] := (cAlias)->HWP_STATUS=="1"

		If aTags[nPos]["isValid"]
			aTags[nPos]["description"] := RTrim((cAlias)->(HWN_PROD)) + " - " + RTrim((cAlias)->(B1_DESC))
			If !Empty((cAlias)->(HWN_LOTE))
				aTags[nPos]["description"] += " - " + STR0045 + ": " + RTrim((cAlias)->(HWN_LOTE)) //LOTE
				If !Empty((cAlias)->(HWN_SUBLOT))
					aTags[nPos]["description"] += " - " + STR0046 + ": " + RTrim((cAlias)->(HWN_SUBLOT)) //SUB-LOTE
				EndIf
			EndIf
			If !Empty((cAlias)->(HWN_NUMSER))
				aTags[nPos]["description"] += " - " + STR0047 + ": " + RTrim((cAlias)->(HWN_NUMSER)) //S�RIE
			EndIf
		Else
			If Empty((cAlias)->(HWN_PROD))
				aTags[nPos]["description"] := STR0048 //"EPC n�o registrada no sistema."
			Else
				aTags[nPos]["description"] := STR0049 //"EPC est� desativada."
			EndIf
		EndIf

		(cAlias)->(dbSkip())
	End
	(cAlias)->(dbCloseArea())
Return aTags

/*/{Protheus.doc} validDic
Valida se o dicion�rio de dados possui as tabelas necess�rias para utiliza��o desta API

@type  Static Function
@author lucas.franca
@since 08/06/2020
@version P12.1.30
@return lRet, Logic, Indica se o dicion�rio de dados est� correto.
/*/
Static Function validDic()
	Local lRet := .T.

	If __lDic == Nil
		__lDic := FWAliasInDic("HWO", .F.) .And. FWAliasInDic("HWP", .F.) .And. FWAliasInDic("HWQ", .F.)
	EndIf
	lRet := __lDic

	If !lRet
		criaMsgErr(STR0053, STR0054) //"Dicion�rio de dados desatualizado." | "As tabelas necess�rias para execu��o deste processo n�o existem no dicion�rio de dados."
	EndIf

Return lRet

/*/{Protheus.doc} GravaSB7
Grava os dados na tabela de invent�rios SB7
@type Static Function
@author marcelo.neumann
@since 11/06/2020
@version P12.1.30
@param cDocum, Character, N�mero do documento (utilizado por refer�ncia)
@return cErro, Character, Mensagem de erro (em caso de sucesso, retorna vazio "")
/*/
Static Function GravaSB7(cDocum)

	Local aDados     := {}
	Local aErroAuto  := {}
	Local cChave     := xFilial("HWQ") + HWO->HWO_ID
	Local cErro      := ""
	Local lGerou     := .F.

	Private lMsErroAuto    := .F.
	Private lAutoErrNoFile := .T.

	HWQ->(dbSetOrder(1))
	If HWQ->(dbSeek(cChave))
		aDados := {{"B7_FILIAL" , xFilial("SB7") , Nil},;
		           {"B7_COD"    , HWQ->HWQ_PROD  , Nil},;
		           {"B7_LOCAL"  , HWO->HWO_LOCAL , Nil},;
		           {"B7_DOC"    , cDocum         , Nil},;
		           {"B7_QUANT"  , HWQ->HWQ_QTLIDA, Nil},;
		           {"B7_LOTECTL", HWQ->HWQ_LOTE  , Nil},;
		           {"B7_NUMLOTE", HWQ->HWQ_SUBLOT, Nil},;
		           {"B7_NUMSERI", HWQ->HWQ_NUMSER, Nil},;
		           {"B7_STATUS" , "1"            , Nil},;
		           {"B7_ORIGEM" , "RFID"         , Nil},;
		           {"B7_CONTAGE", "1"            , Nil},;
		           {"B7_DATA"   , HWO->HWO_DATA  , Nil} }

		If !Empty(HWO->HWO_ENDER)
			aAdd(aDados, {"B7_LOCALIZ", HWO->HWO_ENDER, Nil})
		EndIf

		While HWQ->(!Eof()) .And. HWQ->HWQ_FILIAL+HWQ->HWQ_ID == cChave

			aDados[2][2] := HWQ->HWQ_PROD
			aDados[5][2] := HWQ->HWQ_QTLIDA
			aDados[6][2] := HWQ->HWQ_LOTE
			aDados[7][2] := HWQ->HWQ_SUBLOT
			aDados[8][2] := HWQ->HWQ_NUMSER

			MSExecAuto({|x,y,z| mata270(x,y,z)}, aDados, .T., 3)

			If lMsErroAuto
				aErroAuto := GetAutoGRLog()

				cErro := STR0056 + " '" + AllTrim(HWQ->HWQ_PROD) + "'" //"Erro ao gerar registro de invent�rio para o produto
				If !Empty(HWQ->HWQ_LOTE)
					cErro += ", " + STR0057 + " '" + AllTrim(HWQ->HWQ_LOTE) + "'" //Lote
				EndIf
				If !Empty(HWQ->HWQ_SUBLOT)
					cErro += ", " + STR0058 + " '" + AllTrim(HWQ->HWQ_SUBLOT) + "'" //Sub-Lote
				EndIf
				If !Empty(HWQ->HWQ_NUMSER)
					cErro += ", " + STR0059 + " '" + AllTrim(HWQ->HWQ_NUMSER) + "'" //N�mero de s�rie
				EndIf
				cErro     += ". " + aErroAuto[1]
				Exit
			EndIf

			lGerou := .T.

			HWQ->(dbSkip())
		End
	EndIf

	If !lGerou
		cDocum := ""
	EndIf

	aSize(aDados   , 0)
	aSize(aErroAuto, 0)

Return cErro

/*/{Protheus.doc} GeraNumDoc
Gera o n�mero do documento do invent�rio (B7_DOC)
@type Static Function
@author marcelo.neumann
@since 11/06/2020
@version P12.1.30
@return cDocumento, Character, N�mero do documento a serem relacionados os itens
/*/
Static Function GeraNumDoc()

	Local cDocumento := ""
	Local cIndDoc    := "A"
	Local cPrefixo   := "RF"
	Local nTamDoc    := GetSX3Cache("B7_DOC","X3_TAMANHO") - 2
	Local nTamInd    := 1

	SB7->(dbSetOrder(3))

	//N�mero do documento deve iniciar com "RF", concatenando com as �ltimas posi��es do ID (HWO)
	cDocumento := cPrefixo + Right(HWO->HWO_ID, nTamDoc)

	//Verifica se o n�mero j� foi utilizado em outro registro
	While SB7->(dbSeek(xFilial("SB7") + cDocumento))
		//Se j� existir a combina��o, acrescenta um indicador (Alfab�tico) antes do ID
		cDocumento := cPrefixo + cIndDoc + Right(HWO->HWO_ID, nTamDoc-nTamInd)

		If SubStr(cIndDoc, nTamInd, 1) == "Z"
			If cIndDoc == Padr("Z", nTamInd,"Z")
				nTamInd++
				cIndDoc := Padr("A", nTamInd, "A")
			Else
				cIndDoc := Soma1(cIndDoc)
				cIndDoc := StrTran(cIndDoc,"0","A")
			EndIf
		Else
			cIndDoc := Soma1(cIndDoc)
		EndIf
	End

Return cDocumento
