#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "RFIDSeparationOrder.ch"

Static __aMapHWT := mapHWT()
Static __aMapHWV := mapHWV()
Static __lDic    := Nil
Static __oFilAPI := JsonObject():New()
Static __oProds  := JsonObject():New()

#DEFINE ARR_EPC_FILIAL  1
#DEFINE ARR_EPC_ID      2
#DEFINE ARR_EPC_COD_EPC 3
#DEFINE ARR_EPC_QUANT   4
#DEFINE ARR_EPC_STATUS  5
#DEFINE ARR_EPC_PRODUTO 6
#DEFINE ARR_EPC_LOTE    7
#DEFINE ARR_EPC_SUBLOTE 8
#DEFINE ARR_EPC_SERIE   9
#DEFINE ARR_EPC_SIZE    9

#DEFINE ARR_HWV_FILIAL  1
#DEFINE ARR_HWV_ID      2
#DEFINE ARR_HWV_PROD    3
#DEFINE ARR_HWV_LOTE    4
#DEFINE ARR_HWV_SUBLOTE 5
#DEFINE ARR_HWV_NUMSERI 6
#DEFINE ARR_HWV_QTLIDA  7
#DEFINE ARR_HWV_QTOS    8
#DEFINE ARR_HWV_RECNO   9 //RECNO deve ser sempre a �ltima posi��o
#DEFINE ARR_HWV_SIZE    9

#DEFINE ARR_SB1_DESC    1
#DEFINE ARR_SB1_RASTRO  2

/*/{Protheus.doc} rfidseparationorder
API para realiza��o de uma Confer�ncia (Ordem de Separa��o) pelo RFID
@type API
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
/*/
WSRESTFUL rfidseparationorder DESCRIPTION STR0001 FORMAT APPLICATION_JSON //"API de Ordem de Separa��o do RFID"

	WSDATA id       AS STRING  OPTIONAL
	WSDATA page     AS INTEGER OPTIONAL
	WSDATA pageSize AS INTEGER OPTIONAL
	WSDATA search   AS STRING  OPTIONAL

	WSMETHOD POST init;
	   DESCRIPTION STR0002; //"Inicia o processo de confer�ncia"
	   WSSYNTAX    "api/pcp/v1/rfidseparationorder/init";
	   PATH        "/api/pcp/v1/rfidseparationorder/init";
	   TTALK       "v1"

	WSMETHOD GET separationorder;
	   DESCRIPTION STR0003; //"Busca as confer�ncias existentes"
	   WSSYNTAX    "api/pcp/v1/rfidseparationorder";
	   PATH        "/api/pcp/v1/rfidseparationorder";
	   TTALK       "v1"

	WSMETHOD POST read;
	   DESCRIPTION STR0004; //"Insere/Atualiza os dados de leitura do RFID"
	   WSSYNTAX    "api/pcp/v1/rfidseparationorder/{id}";
	   PATH        "/api/pcp/v1/rfidseparationorder/{id}";
	   TTALK       "v1"

	WSMETHOD GET read;
	   DESCRIPTION STR0005; //"Retorna os dados de leitura do RFID"
	   WSSYNTAX    "api/pcp/v1/rfidseparationorder/{id}";
	   PATH        "/api/pcp/v1/rfidseparationorder/{id}";
	   TTALK       "v1"

	WSMETHOD POST confirm;
	   DESCRIPTION STR0006; //"Confirma a leitura do RFID"
	   WSSYNTAX    "api/pcp/v1/rfidseparationorder/confirm/{id}";
	   PATH        "/api/pcp/v1/rfidseparationorder/confirm/{id}";
	   TTALK       "v1"

	WSMETHOD POST cancel;
	   DESCRIPTION STR0007; //"Cancela a leitura do RFID"
	   WSSYNTAX    "api/pcp/v1/rfidseparationorder/cancel/{id}";
	   PATH        "/api/pcp/v1/rfidseparationorder/cancel/{id}";
	   TTALK       "v1"

ENDWSRESTFUL

/*/{Protheus.doc} POST init
M�todo que inicia o processo de confer�ncia no RFID
C�digo da filial recebido no header da requisi��o com o nome "filial"

URL: localhost:5050/rest/api/pcp/v1/rfidseparationorder/init
JSON recebido:
{
    "keys": [
        {
            "code": "HWT_ORDSEP",
            "label": "N�mero da ordem de separa��o",
            "value": "010015"
        }
     ]
}
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@return lRet, Logic, Indica que a requisi��o foi processada com sucesso ou n�o.
/*/
WSMETHOD POST init WSSERVICE rfidseparationorder
	Local cAlias     := ""
	Local cBody      := ""
	Local cError     := ""
	Local cIdHWT     := ""
	Local cQuery     := ""
	Local lRet       := .T.
	Local nPosOrdSep := 0
	Local oBody      := JsonObject():New()
	Local oRetJson   := JsonObject():New()

	Self:SetContentType("application/json")

	cBody := Self:GetContent()

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		cError := oBody:FromJson(cBody)
		If !Empty(cError)
			criaMsgErr(STR0008, cError) //"Erro ao interpretar os dados recebidos."
			lRet := .F.
		EndIf
	EndIf

	If lRet .And. (oBody["keys"] == Nil .Or. ValType(oBody["keys"]) != "A")
		criaMsgErr(STR0008, STR0009) //"Erro ao interpretar os dados recebidos." | "Atributo 'keys' n�o foi recebido corretamente."
		lRet := .F.
	EndIf

	//Valida as informa��es recebidas em "keys"
	If lRet
		nPosOrdSep := aScan(oBody["keys"], {|x| x["code"] == "HWT_ORDSEP"})

		If nPosOrdSep == 0
			criaMsgErr(STR0010, STR0011) //"N�mero da Ordem de Separa��o n�o foi informado." | "N�o foi identificado nos dados recebidos o N�mero da Ordem de Separa��o (HWT_ORDSEP)."
			lRet := .F.
		ElseIf Empty(oBody["keys"][nPosOrdSep]["value"])
			criaMsgErr(STR0010, STR0012) //"N�mero da Ordem de Separa��o n�o foi informado." | "Identificada a informa��o da Ordem de Separa��o (HWT_ORDSEP) nos dados recebidos, mas n�o foi informado o n�mero da mesma."
			lRet := .F.
		Else
			oBody["keys"][nPosOrdSep]["value"] := PadR(oBody["keys"][nPosOrdSep]["value"], GetSX3Cache("HWT_ORDSEP", "X3_TAMANHO"))
			CB7->(dbSetOrder(1))
			If !CB7->(dbSeek(xFilial("CB7")+oBody["keys"][nPosOrdSep]["value"]))
				criaMsgErr(STR0013, STR0014 + " '" + oBody["keys"][nPosOrdSep]["value"] + "' " + STR0015) //"Ordem de Separa��o inv�lida." | "A Ordem de Separa��o 'XX' n�o est� cadastrada."
				lRet := .F.
			EndIf
		EndIf
	EndIf

	If lRet
		cQuery := " SELECT HWT.HWT_ID "
		cQuery +=   " FROM " + RetSqlName("HWT") + " HWT "
		cQuery +=  " WHERE HWT.HWT_FILIAL = '" + xFilial("HWT") + "' "
		cQuery +=    " AND HWT.HWT_ORDSEP = '" + oBody["keys"][nPosOrdSep]["value"] + "' "
		cQuery +=    " AND HWT.HWT_STATUS = '1' "
		cQuery +=    " AND HWT.D_E_L_E_T_ = ' ' "

		cAlias := GetNextAlias()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

		If (cAlias)->(!Eof())
			criaMsgErr(STR0054, STR0055 + AllTrim((cAlias)->(HWT_ID)) + STR0056) //"J� existe uma confer�ncia ativa para esta ordem de separa��o." || "A confer�ncia com ID 'xxx' est� ativa. Finalize esta confer�ncia antes de iniciar uma nova confer�ncia para a mesma ordem de separa��o."
			lRet := .F.
		EndIf
		(cAlias)->(dbCloseArea())

	EndIf

	//Verifica se existem produtos com controle de RFID para os par�metros informados
	If lRet
		cQuery := "SELECT COUNT(*) TOTPRD"
		cQuery +=  " FROM " + RetSqlName("CB8") + " CB8"
		cQuery += " INNER JOIN " + RetSqlName("SVK") + " SVK"
		cQuery +=    " ON SVK.VK_FILIAL  = '" + xFilial("SVK") + "'"
		cQuery +=   " AND SVK.VK_COD     = CB8.CB8_PROD"
		cQuery +=   " AND SVK.VK_RFID    = '1'"
		cQuery +=   " AND SVK.D_E_L_E_T_ = ' '"
		cQuery += " WHERE CB8.CB8_FILIAL = '" + xFilial("CB8") + "'"
		cQuery +=   " AND CB8.CB8_ORDSEP = '" + oBody["keys"][nPosOrdSep]["value"] + "'"
		cQuery +=   " AND CB8.D_E_L_E_T_ = ' '"

		cAlias := GetNextAlias()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

		If (cAlias)->(TOTPRD) < 1
			criaMsgErr(STR0016, STR0017) //"Ordem de Separa��o sem controle de RFID." | "Os produtos contidos nesta Ordem de Separa��o n�o possuem controle de RFID."
			lRet := .F.
		EndIf

		(cAlias)->(dbCloseArea())
	EndIf

	//Dados v�lidos. Inclui o registro na tabela HWT
	If lRet
		cIdHWT := GetSxeNum("HWT","HWT_ID")
		RecLock("HWT",.T.)
			HWT->HWT_FILIAL := xFilial("HWT")
			HWT->HWT_ID     := cIdHWT
			HWT->HWT_USER   := cUserName
			HWT->HWT_DATA   := Date()
			HWT->HWT_HORA   := Time()
			HWT->HWT_ORDSEP := oBody["keys"][nPosOrdSep]["value"]
			HWT->HWT_STATUS := "1"
		HWT->(MsUnLock())
		ConfirmSX8()

		oRetJson["id"] := cIdHWT
		Self:SetResponse(oRetJson:ToJson())
	EndIf

	FreeObj(oBody)
	FreeObj(oRetJson)

Return lRet

/*/{Protheus.doc} GET separationorder
M�todo que recupera as confer�ncias de ordem de separa��o existentes

URL: localhost:5050/rest/api/pcp/v1/rfidseparationorder?page=1&pageSize=5&search=2
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@return Nil
/*/
WSMETHOD GET separationorder WSRECEIVE page, pageSize, search WSSERVICE rfidseparationorder
	Local aDados   := {}
	Local cAlias   := ""
	Local cQuery   := ""
	Local lRet     := .T.
	Local nIndex   := 0
	Local nPos     := 0
	Local nStart   := 0
	Local nTotal   := Len(__aMapHWT)
	Local oRetJson := JsonObject():New()

	DEFAULT Self:page     := 1
	DEFAULT Self:pageSize := 20

	Self:SetContentType("application/json")

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		cQuery := " SELECT "
		For nIndex := 1 To nTotal
			If nIndex > 1
				cQuery += ","
			EndIf
			cQuery += " HWT." + __aMapHWT[nIndex][1]
		Next nIndex
		cQuery +=  " FROM " + RetSqlName("HWT") + " HWT "
		cQuery += " WHERE HWT.HWT_FILIAL = '" + xFilial("HWT") + "' "
		cQuery +=   " AND HWT.D_E_L_E_T_ = ' ' "
		If !Empty(Self:search)
			cQuery += " AND HWT.HWT_ORDSEP LIKE '%" + Self:search + "%' "
		EndIf
		cQuery += " ORDER BY HWT.HWT_DATA DESC, HWT.HWT_HORA DESC "

		cAlias := GetNextAlias()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

		For nIndex := 1 To nTotal
			If __aMapHWT[nIndex][4] $ "|D|L|N|"
				TcSetField(cAlias, __aMapHWT[nIndex][1],;
				                   __aMapHWT[nIndex][4],;
				                   __aMapHWT[nIndex][5],;
				                   __aMapHWT[nIndex][6])
			EndIf
		Next nIndex

		If Self:page > 1
			nStart := ( (Self:page-1) * Self:pageSize )
			If nStart > 0
				(cAlias)->(DbSkip(nStart))
			EndIf
		EndIf

		While (cAlias)->(!Eof())
			aAdd(aDados, JsonObject():New())
			nPos := Len(aDados)

			For nIndex := 1 To nTotal
				If __aMapHWT[nIndex][4] == "D"
					aDados[nPos][__aMapHWT[nIndex][2]] := convDate(&("(cAlias)->" + __aMapHWT[nIndex][1]))
				Else
					aDados[nPos][__aMapHWT[nIndex][2]] := &("(cAlias)->" + __aMapHWT[nIndex][1])
				EndIf
			Next nTotal

			(cAlias)->(dbSkip())

			//Verifica tamanho da p�gina
			If nPos >= Self:pageSize
				Exit
			EndIf
		End

		oRetJson["hasNext"] := (cAlias)->(!Eof())

		(cAlias)->(dbCloseArea())

		oRetJson["items"] := aDados

		//Adiciona as propriedades de exibi��o das informa��es
		oRetJson["columns"] := Array(nTotal)
		For nIndex := 1 To nTotal
			oRetJson["columns"][nIndex] := JsonObject():New()
			oRetJson["columns"][nIndex]["property"] := __aMapHWT[nIndex][2]
			oRetJson["columns"][nIndex]["label"   ] := __aMapHWT[nIndex][3]
			Do Case
				Case __aMapHWT[nIndex][1] == "HWT_DATA"
					oRetJson["columns"][nIndex]["type"] := "date"
				Case __aMapHWT[nIndex][1] == "HWT_STATUS"
					oRetJson["columns"][nIndex]["type"] := "label"
					oRetJson["columns"][nIndex]["labels"] := Array(3)

					oRetJson["columns"][nIndex]["labels"][1] := JsonObject():New()
					oRetJson["columns"][nIndex]["labels"][1]["color"] := "color-10"
					oRetJson["columns"][nIndex]["labels"][1]["value"] := "1"
					oRetJson["columns"][nIndex]["labels"][1]["label"] := STR0018 //"Ativo"

					oRetJson["columns"][nIndex]["labels"][2] := JsonObject():New()
					oRetJson["columns"][nIndex]["labels"][2]["color"] := "color-03"
					oRetJson["columns"][nIndex]["labels"][2]["value"] := "2"
					oRetJson["columns"][nIndex]["labels"][2]["label"] := STR0019 //"Finalizado"

					oRetJson["columns"][nIndex]["labels"][3] := JsonObject():New()
					oRetJson["columns"][nIndex]["labels"][3]["color"] := "color-07"
					oRetJson["columns"][nIndex]["labels"][3]["value"] := "3"
					oRetJson["columns"][nIndex]["labels"][3]["label"] := STR0020 //"Cancelado"
			EndCase
		Next nIndex

		Self:SetResponse(EncodeUtf8(oRetJson:ToJson()))
	EndIf
	FreeObj(oRetJson)
	aSize(aDados, 0)

Return lRet

/*/{Protheus.doc} POST read
M�todo para processar as leituras efetuadas pelo RFID

URL: localhost:5050/rest/api/pcp/v1/rfidseparationorder/0000000028
JSON recebido:
{
    "tags":[
        "000000000110000000000001",
        "000000000120000000000004"
    ]
}
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@return Nil
/*/
WSMETHOD POST read PATHPARAM id WSSERVICE rfidseparationorder
	Local aEpcProc   := {}
	Local aDadosHWV  := {}
	Local aDadosInc  := {}
	Local aDadosAlt  := {}
	Local aDadosRas  := {}
	Local cBody      := ""
	Local cError     := ""
	Local cCols      := ""
	Local lRet       := .T.
	Local nIndex     := 0
	Local nTotal     := 0
	Local oBody      := JsonObject():New()

	Self:SetContentType("application/json")

	cBody := Self:GetContent()

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		cError := oBody:FromJson(cBody)
		If !Empty(cError)
			criaMsgErr(STR0008, cError) //"Erro ao interpretar os dados recebidos."
			lRet := .F.
		EndIf
	EndIf

	If lRet
		If Empty(Self:id)
			criaMsgErr(STR0021, STR0022) //"Identificador da confer�ncia n�o foi informado." | "Identificador da confer�ncia inv�lido, pois est� em branco ou n�o foi recebido."
			lRet := .F.
		Else
			Self:id := PadR(Self:id, GetSX3Cache("HWT_ID","X3_TAMANHO"))
			HWT->(dbSetOrder(1))
			If !HWT->(dbSeek(xFilial("HWT")+Self:id))
				criaMsgErr(STR0023, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0025) //"Identificador da confer�ncia inv�lido." | "Identificador da confer�ncia 'XX' n�o existe."
				lRet := .F.
			ElseIf HWT->HWT_STATUS <> '1'
				criaMsgErr(STR0023, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0026) //"Identificador da confer�ncia inv�lido." | "Identificador da confer�ncia 'XX' j� foi finalizado ou cancelado."
				lRet := .F.
			EndIf
		EndIf

		If lRet .And. oBody["tags"] == Nil
			criaMsgErr(STR0027, STR0028) //"Lista de EPC n�o recebida." | "N�o foi recebido a lista de EPC no formato correto ('tags')."
			lRet := .F.
		ElseIf ValType(oBody["tags"]) <> "A"
			criaMsgErr(STR0029, STR0028) //"Lista de EPC incorreta." | "N�o foi recebido a lista de EPC no formato correto ('tags')."
			lRet := .F.
		ElseIf Len(oBody["tags"]) == 0
			criaMsgErr(STR0030, STR0031) //"Lista de EPC vazia." | "N�o existe nenhuma EPC para processar ('tags')."
			lRet := .F.
		EndIf
	EndIf

	If lRet
		CB7->(dbSetOrder(1))
		CB7->(dbSeek(xFilial("CB7")+HWT->HWT_ORDSEP))

		//Verifica quais s�o as EPCs que devem ser consideradas
		aEpcProc := getEPCProc(Self:id, oBody["tags"])

		//Gera os dados para a tabela HWV
		aDadosHWV := carregaHWV(Self:id, aEpcProc)

		//Remove do array aEpcProc as informa��es auxiliares que n�o ser�o inclu�das na tabela HWU
		aEval(aEpcProc, {|x| aSize(x, 5)})

		//Separa o array aDadosHWV em dois arrays.
		//aDadosInc -> Dados da HWV que ser�o inclu�dos na tabela
		//aDadosAlt -> Dados da HWV que ser�o modificados na tabela
		nTotal := Len(aDadosHWV)
		For nIndex := 1 To nTotal
			//N�o possui RECNO, far� inclus�o
			If aDadosHWV[nIndex][ARR_HWV_RECNO] == 0
				//Inclui somente se tiver Quantidade Lida ou ERP
				If aDadosHWV[nIndex][ARR_HWV_QTLIDA] <> 0 .Or. aDadosHWV[nIndex][ARR_HWV_QTOS] <> 0
					//o aSize � para remover a coluna de RECNO, que n�o ser� utilizada na inclus�o
					aAdd(aDadosInc, aSize(aClone(aDadosHWV[nIndex]), ARR_HWV_SIZE-1) )
				EndIf
			Else
				//Possui RECNO, far� atualiza��o
				aAdd(aDadosAlt, {aDadosHWV[nIndex][ARR_HWV_RECNO], aDadosHWV[nIndex][ARR_HWV_QTLIDA], aDadosHWV[nIndex][ARR_HWV_QTOS]})
			EndIf
		Next nIndex

		//Faz a inclus�o/modifica��o das tabelas HWU e HWV
		BEGIN TRANSACTION
			nTotal := Len(aEpcProc)
			If nTotal > 0
				cCols := "HWU_FILIAL,HWU_ID,HWU_EPC,HWU_QUANT,HWU_STATUS"
				If TCDBInsert(RetSqlName("HWU"), cCols, aEpcProc) < 0
					DisarmTransaction()
					lRet := .F.
					criaMsgErr(STR0032, tcSQLError()) //"Erro ao atualizar os dados das etiquetas lidas."
				Else
					For nIndex := 1 To nTotal
						aAdd(aDadosRas, {aEpcProc[nIndex][ARR_EPC_COD_EPC], aEpcProc[nIndex][ARR_EPC_QUANT]})
					Next nIndex

					//Grava a rastreabilidade da etiqueta
					If !RFIDGrvRas(aDadosRas, "2", "rfidseparationorder")
						DisarmTransaction()
						lRet := .F.
						criaMsgErr(STR0057, tcSQLError()) //"Erro ao gravar a rastreabilidade das etiquetas lidas."
					EndIf
					aSize(aDadosRas,0)
				EndIf
			EndIf

			If lRet .And. Len(aDadosInc) > 0
				cCols := "HWV_FILIAL,HWV_ID,HWV_PROD,HWV_LOTE,HWV_SUBLOT,HWV_NUMSER,HWV_QTLIDA,HWV_QTOS"
				If TCDBInsert(RetSqlName("HWV"), cCols, aDadosInc) < 0
					DisarmTransaction()
					lRet := .F.
					criaMsgErr(STR0032, tcSQLError()) //"Erro ao atualizar os dados das etiquetas lidas."
				EndIf
			EndIf

			nTotal := Len(aDadosAlt)
			If lRet .And. nTotal > 0
				For nIndex := 1 To nTotal
					HWV->(dbGoTo(aDadosAlt[nIndex][1]))
					RecLock("HWV",.F.)
						If aDadosAlt[nIndex][2] == 0 .And. aDadosAlt[nIndex][3] == 0
							dbDelete()
						Else
							HWV->HWV_QTLIDA := aDadosAlt[nIndex][2]
							HWV->HWV_QTOS   := aDadosAlt[nIndex][3]
						EndIf
					MsUnLock()
				Next nIndex
			EndIf
		END TRANSACTION
	EndIf

	If lRet
		cJsonRet := getConfer(Self:id)
		Self:SetResponse(EncodeUtf8(cJsonRet))
	EndIf

	FreeObj(oBody)
	aSize(aEpcProc , 0)
	aSize(aDadosHWV, 0)
	aSize(aDadosInc, 0)
	aSize(aDadosAlt, 0)
Return lRet

/*/{Protheus.doc} GET read
M�todo para retornar as leituras efetuadas pelo RFID

URL: localhost:5050/rest/api/pcp/v1/rfidseparationorder/0000000023
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@return Nil
/*/
WSMETHOD GET read PATHPARAM id WSSERVICE rfidseparationorder
	Local cJson  := ""
	Local lRet   := .T.
	Local nTamID := GetSX3Cache("HWT_ID", "X3_TAMANHO")

	Self:SetContentType("application/json")

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		Self:id := PadR(Self:id, nTamID)
		cJson   := getConfer(Self:id)
		If !Empty(cJson)
			Self:SetResponse(EncodeUtf8(cJson))
		Else
			lRet := .F.
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} POST confirm
M�todo para finalizar o processo de confer�ncia do RFID

URL: localhost:5050/rest/api/pcp/v1/rfidseparationorder/confirm/0000000020
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@return Nil
/*/
WSMETHOD POST confirm PATHPARAM id WSSERVICE rfidseparationorder
	Local lRet   := .T.
	Local nTamID := GetSX3Cache("HWT_ID", "X3_TAMANHO")
	Local oJson  := JsonObject():New()

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		Self:id := PadR(Self:id, nTamID)
		HWT->(dbSetOrder(1))
		If !HWT->(dbSeek(xFilial("HWT")+Self:id))
			criaMsgErr(STR0033, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0025) //"Identificador de confer�ncia inv�lido." | "Identificador da confer�ncia 'XX' n�o existe."
			lRet := .F.
		ElseIf HWT->HWT_STATUS <> '1'
			criaMsgErr(STR0033, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0026) //"Identificador de confer�ncia inv�lido." | "Identificador da confer�ncia 'XX' j� foi finalizado ou cancelado."
			lRet := .F.
		EndIf
	EndIf

	If lRet
		RecLock("HWT", .F.)
			HWT->HWT_STATUS := "2"
		MsUnLock()
		oJson["message"] := STR0034 //"Confer�ncia confirmada com sucesso."
		Self:SetResponse(EncodeUtf8(oJson:ToJson()))
	EndIf

	FreeObj(oJson)
Return lRet

/*/{Protheus.doc} POST cancel
M�todo para cancelar a confer�ncia do RFID

URL: localhost:5050/rest/api/pcp/v1/rfidseparationorder/cancel/0000000021
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@return Nil
/*/
WSMETHOD POST cancel PATHPARAM id WSSERVICE rfidseparationorder
	Local lRet   := .T.
	Local nTamID := GetSX3Cache("HWT_ID", "X3_TAMANHO")
	Local oJson  := JsonObject():New()

	lRet := vldFilial(Self:GetHeader("filial")) .And. validDic()
	If lRet
		Self:id := PadR(Self:id, nTamID)
		HWT->(dbSetOrder(1))
		If !HWT->(dbSeek(xFilial("HWT")+Self:id))
			criaMsgErr(STR0033, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0025) //"Identificador de confer�ncia inv�lido." | "Identificador da confer�ncia 'XX' n�o existe."
			lRet := .F.
		ElseIf HWT->HWT_STATUS <> '1'
			criaMsgErr(STR0033, STR0024 + " '" + AllTrim(Self:id) + "' " + STR0026) //"Identificador de confer�ncia inv�lido." | "Identificador da confer�ncia 'XX' j� foi finalizado ou cancelado."
			lRet := .F.
		EndIf
	EndIf

	If lRet
		RecLock("HWT", .F.)
			HWT->HWT_STATUS := "3"
		MsUnLock()
		oJson["message"] := STR0051 //"Confer�ncia cancelada com sucesso."
		Self:SetResponse(EncodeUtf8(oJson:ToJson()))
	EndIf

	FreeObj(oJson)
Return lRet

/*/{Protheus.doc} vldFilial
Verifica se a filial recebida na requisi��o � uma filial v�lida
@type Static Function
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@param cFilAPI, Character, Filial recebida no header da requisi��o HTTP
@return lRet, Logic, Indica se a filial recebida � v�lida
/*/
Static Function vldFilial(cFilAPI)
	Local cMessage := ""
	Local lRet     := .T.

	If Empty(cFilAPI)
		cMessage := STR0035 //"Filial n�o foi informada."
		lRet     := .F.
	EndIf

	If lRet
		If __oFilAPI[cFilAPI] == Nil
			__oFilAPI[cFilAPI]    := Array(3)
			__oFilAPI[cFilAPI][1] := PadR(cFilAPI, FWSizeFilial(cEmpAnt))

			If !FWFilExist(cEmpAnt, __oFilAPI[cFilAPI][1])
				cMessage := STR0036 + " '" + __oFilAPI[cFilAPI][1] + "' " + STR0025 // "Filial 'XX' n�o existe."
				lRet     := .F.
			EndIf

			__oFilAPI[cFilAPI][2] := lRet
			__oFilAPI[cFilAPI][3] := cMessage
			cFilAPI               := __oFilAPI[cFilAPI][1]
		Else
			cMessage := __oFilAPI[cFilAPI][3]
			lRet     := __oFilAPI[cFilAPI][2]
			cFilAPI  := __oFilAPI[cFilAPI][1]
		EndIf
	EndIf

	//Filial est� v�lida. Verifica se � necess�rio mudar a filial logada
	If lRet .And. cFilAPI != cFilAnt
		cFilAnt := cFilAPI
	EndIf

	//Filial n�o est� v�lida. Cria mensagem de erro para retorno
	If !lRet
		criaMsgErr(STR0037, cMessage) //"Filial inv�lida."
	EndIf

Return lRet

/*/{Protheus.doc} criaMsgErr
Cria o retorno da mensagem de erro
@type  Static Function
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@param cMensagem, Character, Mensagem de erro
@param cDetalhe , Character, Mensagem detalhada de erro
@return Nil
/*/
Static Function criaMsgErr(cMensagem, cDetalhe)

	SetRestFault(400, EncodeUtf8(cMensagem), .T., 400, EncodeUtf8(cDetalhe))

Return Nil

/*/{Protheus.doc} mapHWT
Monta o mapeamento (de/para) dos campos da HWT x JSON
@type Static Function
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@return aMap, Array, Array com o mapeamento da tabela HWT
/*/
Static Function mapHWT()
	Local aMap := {}

	aAdd(aMap, {"HWT_FILIAL", "branchId"       , FWX3Titulo("HWT_FILIAL"), "C", FWSizeFilial()                        , 0})
	aAdd(aMap, {"HWT_ID"    , "id"             , FWX3Titulo("HWT_ID"    ), "C", GetSX3Cache("HWT_ID"    ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWT_USER"  , "user"           , FWX3Titulo("HWT_USER"  ), "C", GetSX3Cache("HWT_USER"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWT_DATA"  , "date"           , FWX3Titulo("HWT_DATA"  ), "D", GetSX3Cache("HWT_DATA"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWT_HORA"  , "time"           , FWX3Titulo("HWT_HORA"  ), "C", GetSX3Cache("HWT_HORA"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWT_ORDSEP", "separationOrder", FWX3Titulo("HWT_ORDSEP"), "C", GetSX3Cache("HWT_ORDSEP","X3_TAMANHO"), 0})
	aAdd(aMap, {"HWT_STATUS", "status"         , FWX3Titulo("HWT_STATUS"), "C", GetSX3Cache("HWT_STATUS","X3_TAMANHO"), 0})
Return aMap

/*/{Protheus.doc} mapHWV
Monta o mapeamento (de/para) dos campos da HWV x JSON

@type  Static Function
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@return aMap, Array, Array com o mapeamento da tabela HWT
/*/
Static Function mapHWV()
	Local aMap := {}

	aAdd(aMap, {"HWV_PROD"  , "product"      , FWX3Titulo("HWV_PROD"  ), "C", GetSX3Cache("HWV_PROD"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWV_LOTE"  , "lotCode"      , FWX3Titulo("HWV_LOTE"  ), "C", GetSX3Cache("HWV_LOTE"  ,"X3_TAMANHO"), 0})
	aAdd(aMap, {"HWV_SUBLOT", "subLotCode"   , FWX3Titulo("HWV_SUBLOT"), "C", GetSX3Cache("HWV_SUBLOT","X3_TAMANHO"), 0})
	aAdd(aMap, {"HWV_NUMSER", "serialNumber" , FWX3Titulo("HWV_NUMSER"), "C", GetSX3Cache("HWV_NUMSER","X3_TAMANHO"), 0})
	aAdd(aMap, {"HWV_QTLIDA", "read"         , FWX3Titulo("HWV_QTLIDA"), "N", GetSX3Cache("HWV_QTLIDA","X3_TAMANHO"), GetSX3Cache("HWV_QTLIDA","X3_DECIMAL")})
	aAdd(aMap, {"HWV_QTOS"  , "expected"     , FWX3Titulo("HWV_QTOS"  ), "N", GetSX3Cache("HWV_QTOS"  ,"X3_TAMANHO"), GetSX3Cache("HWV_QTOS"  ,"X3_DECIMAL")})
	aAdd(aMap, {"_HWV_DIF"  , "hasDifference", STR0038 /*"Status"*/    , "L", 1                                     , 0})
Return aMap

/*/{Protheus.doc} convDate
Faz a convers�o de uma data em string

@type  Static Function
@author marcelo.neumann
@since 01/06/2020
@version P12.1.30
@param dData, Date, Data para converter
@return cData, Character, Data no formato STRING AAAA-MM-DD
/*/
Static Function convDate(dData)
	Local cData := ""
	cData := StrZero(Year(dData),4) + "-" + StrZero(Month(dData),2) + "-" + StrZero(Day(dData),2)
Return cData

/*/{Protheus.doc} getEPCProc
Retorna as informa��es das EPCs que ser�o processadas
@type Static Function
@author marcelo.neumann
@since 02/06/2020
@version P12.1.30
@param 01 cId  , Character, ID de processamento
@param 02 aTags, Array    , Array com as EPCs recebidas na requisi��o
@return   aEPC , Array    , Array com os dados das EPCs para processar
/*/
Static Function getEPCProc(cId, aTags)
	Local aDadosEPC := {}
	Local aEPC      := {}
	Local cEpc      := ""
	Local cFilHWU   := xFilial("HWU")
	Local lTodas    := .F.
	Local nIndex    := 0
	Local nPos      := 0
	Local nTamEpc   := GetSX3Cache("HWU_EPC", "X3_TAMANHO")
	Local nTotal    := Len(aTags)
	Local oTagsProc := JsonObject():New()

	HWU->(dbSetOrder(1))
	lTodas := !HWU->(dbSeek(cFilHWU+cId))

	//Busca as informa��es das EPC's que devem ser consideradas
	For nIndex := 1 To nTotal
		cEpc := PadR(aTags[nIndex], nTamEpc)
		//Verifica se a tag est� duplicada
		If oTagsProc[cEpc] == Nil
			oTagsProc[cEpc] := .T.

			If lTodas .Or. !HWU->(dbSeek(cFilHWU+cId+cEpc))
				aDadosEPC := RFIDGetEpc(cEpc)

				aAdd(aEPC, Array(ARR_EPC_SIZE))
				nPos := Len(aEPC)

				aEPC[nPos][ARR_EPC_FILIAL ] := cFilHWU
				aEPC[nPos][ARR_EPC_ID     ] := cId
				aEPC[nPos][ARR_EPC_COD_EPC] := cEpc
				aEPC[nPos][ARR_EPC_QUANT  ] := aDadosEPC[RFIDGetCnE("ARR_DATAEPC_QUANT")]
				aEPC[nPos][ARR_EPC_STATUS ] := Iif(aDadosEPC[RFIDGetCnE("ARR_DATAEPC_EXISTE")] .And. aDadosEPC[RFIDGetCnE("ARR_DATAEPC_ATIVO")], "1", "0")
				aEPC[nPos][ARR_EPC_PRODUTO] := aDadosEPC[RFIDGetCnE("ARR_DATAEPC_PRODUTO")]
				aEPC[nPos][ARR_EPC_LOTE   ] := aDadosEPC[RFIDGetCnE("ARR_DATAEPC_LOTE")]
				aEPC[nPos][ARR_EPC_SUBLOTE] := aDadosEPC[RFIDGetCnE("ARR_DATAEPC_SUBLOTE")]
				aEPC[nPos][ARR_EPC_SERIE  ] := aDadosEPC[RFIDGetCnE("ARR_DATAEPC_NUMSERIE")]
				aSize(aDadosEPC, 0)
			EndIf
		EndIf
	Next nIndex

	FreeObj(oTagsProc)
Return aEPC

/*/{Protheus.doc} carregaHWV
Carrega o array com os dados para atualiza��o da tabela HWV
@type  Static Function
@author marcelo.neumann
@since 02/06/2020
@version P12.1.30
@param 01 cId , Character, Identificador da confer�ncia
@param 02 aEpc, Array    , Array com as EPCs que ser�o processadas
@return aDados, Array    , Array com os dados da HWV para atualiza��o
/*/
Static Function carregaHWV(cId, aEpc)
	Local aDados     := {}
	Local cAlias     := ""
	Local cFilHWV    := xFilial("HWV")
	Local cQuery     := ""
	Local cChave     := ""
	Local cChaveB1   := ""
	Local lIncAll    := .F.
	Local nPos       := 0
	Local nPosDim    := 0
	Local nTamPrd    := GetSX3Cache("HWV_PROD"  , "X3_TAMANHO")
	Local nTamLote   := GetSX3Cache("HWV_LOTE"  , "X3_TAMANHO")
	Local nTamSubLot := GetSX3Cache("HWV_SUBLOT", "X3_TAMANHO")
	Local nTamSerie  := GetSX3Cache("HWV_NUMSER", "X3_TAMANHO")
	Local nTotal     := 0
	Local nIndex     := 0
	Local oIndHWV    := JsonObject():New()

	HWV->(dbSetOrder(1))
	If HWV->(dbSeek(cFilHWV+cId))
		//Se foi gerada a partir de uma OP
		If CB7->CB7_ORIGEM == '3'
			cQuery := "SELECT HWV.HWV_PROD   AS PRODUTO,"         + ;
							" HWV.HWV_NUMSER AS SERIE,"           + ;
							" HWV.HWV_QTLIDA AS QTLIDA,"          + ;
							" HWV.HWV_QTOS   AS QTOS,"            + ;
							" HWV.R_E_C_N_O_ AS RECNO"            + ;
					   " FROM " + RetSqlName("HWV") + " HWV"      + ;
					  " WHERE HWV.HWV_FILIAL = '" + cFilHWV + "'" + ;
						" AND HWV.HWV_ID = '" + cId + "'"         + ;
						" AND HWV.HWV_LOTE = ' '"                 + ;
						" AND HWV.HWV_SUBLOT = ' '"               + ;
						" AND HWV.D_E_L_E_T_ = ' '"               + ;
						" AND (HWV.HWV_QTOS - HWV.HWV_QTLIDA) > 0"
			cQuery := ChangeQuery(cQuery)

			cAlias := GetNextAlias()
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

			While (cAlias)->(!Eof())
				addArrayHWV(cFilHWV                         , ;
							cId                             , ;
							PadR((cAlias)->PRODUTO, nTamPrd), ;
							Space(nTamLote)                 , ;
							Space(nTamSubLot)               , ;
							PadR((cAlias)->SERIE, nTamSerie), ;
							(cAlias)->QTLIDA                , ;
							(cAlias)->QTOS                  , ;
							(cAlias)->RECNO                 , ;
							@aDados                         , ;
							@oIndHWV)
				(cAlias)->(dbSkip())
			End
			(cAlias)->(dbCloseArea())
		EndIf
	Else
		lIncAll := .T.

		//Se foi gerada a partir de uma OP
		If CB7->CB7_ORIGEM == '3'
			cQuery := "SELECT CB8.CB8_ORDSEP AS ORDSEP,"                  + ;
							" CB8.CB8_PROD   AS PRODUTO,"                 + ;
							" CB8.CB8_LOTECT AS LOTE,"                    + ;
							" CB8.CB8_NUMLOT AS SUBLOTE,"                 + ;
							" CB8.CB8_NUMSER AS SERIE,"                   + ;
							" Sum(CB8.CB8_QTDORI) AS QTERP"               + ;
					   " FROM " + RetSqlName("CB8") + " CB8"              + ;
					  " INNER JOIN " + RetSqlName("SVK") + " SVK"         + ;
					     " ON SVK.VK_FILIAL  = '" + xFilial("SVK") + "'"  + ;
					    " AND SVK.VK_COD     = CB8.CB8_PROD"              + ;
					    " AND SVK.VK_RFID    = '1'"                       + ;
					    " AND SVK.D_E_L_E_T_ = ' '"                       + ;
					  " WHERE CB8.CB8_FILIAL = '" + xFilial("CB8") + "'"  + ;
						" AND CB8.CB8_ORDSEP = '" + HWT->HWT_ORDSEP + "'" + ;
						" AND CB8.D_E_L_E_T_ = ' '"                       + ;
					  " GROUP BY CB8.CB8_ORDSEP,"                         + ;
							   " CB8.CB8_PROD,"                           + ;
							   " CB8.CB8_LOTECT,"                         + ;
							   " CB8.CB8_NUMLOT,"                         + ;
							   " CB8.CB8_NUMSER"
		Else
			cQuery := "SELECT CB8.CB8_ORDSEP AS ORDSEP,"                             + ;
							" CB8.CB8_PROD   AS PRODUTO,"                            + ;
							" CB8.CB8_LOTECT AS LOTE,"                               + ;
							" CB8.CB8_NUMLOT AS SUBLOTE,"                            + ;
							" CASE "                                                 + ;
							   " WHEN CB8.CB8_NUMSER = ' ' THEN"                     + ;
							      " SDB.DB_NUMSERI"                                  + ;
							   " ELSE"                                               + ;
							      " CB8.CB8_NUMSER"                                  + ;
							" END AS SERIE,"                                         + ;
							" Sum(COALESCE(SDB.DB_QUANT,CB8.CB8_QTDORI)) AS QTERP"   + ;
					   " FROM " + RetSqlName("CB8") + " CB8"                         + ;
					   " LEFT JOIN " + RetSqlName("SDB") + " SDB"                    + ;
                         " ON SDB.DB_FILIAL  = '" + xFilial("SDB") + "'"             + ;
						" AND SDB.DB_DOC     = CB8.CB8_NOTA"                         + ;
						" AND SDB.DB_SERIE   = CB8.CB8_SERIE"                        + ;
						" AND SDB.DB_PRODUTO = CB8.CB8_PROD"                         + ;
						" AND SDB.DB_LOCAL   = CB8.CB8_LOCAL"                        + ;
						" AND SDB.DB_LOTECTL = CB8.CB8_LOTECT"                       + ;
						" AND SDB.DB_NUMLOTE = CB8.CB8_NUMLOT"                       + ;
						" AND SDB.DB_ESTORNO <> 'S'"                                 + ;
						" AND SDB.D_E_L_E_T_ = ' '"                                  + ;
						" AND CB8.CB8_NUMSER = ' '"                                  + ;
						" AND NOT EXISTS (SELECT 1"                                  + ;
										  " FROM " + RetSqlName("CB8") + " CB8AUX"   + ;
										 " WHERE CB8AUX.CB8_FILIAL = CB8.CB8_FILIAL" + ;
										   " AND CB8AUX.CB8_ORDSEP = CB8.CB8_ORDSEP" + ;
										   " AND CB8AUX.CB8_PROD   = CB8.CB8_PROD"   + ;
										   " AND CB8AUX.CB8_LOCAL  = CB8.CB8_LOCAL"  + ;
										   " AND CB8AUX.CB8_LCALIZ = CB8.CB8_LCALIZ" + ;
										   " AND CB8AUX.CB8_LOTECT = CB8.CB8_LOTECT" + ;
										   " AND CB8AUX.CB8_NUMLOT = CB8.CB8_NUMLOT" + ;
										   " AND CB8AUX.CB8_NUMSER = SDB.DB_NUMSERI" + ;
										   " AND CB8AUX.D_E_L_E_T_ = ' ')"           + ;
					  " INNER JOIN " + RetSqlName("SVK") + " SVK"                    + ;
					     " ON SVK.VK_FILIAL  = '" + xFilial("SVK") + "'"             + ;
					    " AND SVK.VK_COD     = CB8.CB8_PROD"                         + ;
					    " AND SVK.VK_RFID    = '1'"                                  + ;
					    " AND SVK.D_E_L_E_T_ = ' '"                                  + ;
					  " WHERE CB8.CB8_FILIAL = '" + xFilial("CB8")  + "'"            + ;
						" AND CB8.CB8_ORDSEP = '" + HWT->HWT_ORDSEP + "'"            + ;
						" AND CB8.D_E_L_E_T_ = ' '"                                  + ;
					  " GROUP BY CB8.CB8_ORDSEP,"                                    + ;
							   " CB8.CB8_PROD,"                                      + ;
							   " CB8.CB8_LOTECT,"                                    + ;
							   " CB8.CB8_NUMLOT,"                                    + ;
							   " CASE"                                               + ;
							      " WHEN CB8.CB8_NUMSER = ' ' THEN"                  + ;
								     " SDB.DB_NUMSERI"                               + ;
								  " ELSE"                                            + ;
								     " CB8.CB8_NUMSER"                               + ;
								" END"
		EndIf
		cQuery := ChangeQuery(cQuery)

		cAlias := GetNextAlias()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

		While (cAlias)->(!Eof())
			If (cAlias)->QTERP <> 0
				addArrayHWV(cFilHWV                            ,;
				            cId                                ,;
				            PadR((cAlias)->PRODUTO, nTamPrd)   ,;
				            PadR((cAlias)->LOTE   , nTamLote)  ,;
				            PadR((cAlias)->SUBLOTE, nTamSubLot),;
				            PadR((cAlias)->SERIE  , nTamSerie) ,;
				            0                                  ,;
				            (cAlias)->QTERP                    ,;
				            0                                  ,;
				            @aDados                            ,;
				            @oIndHWV)
			EndIf
			(cAlias)->(dbSkip())
		End
		(cAlias)->(dbCloseArea())
	EndIf

	//Ordena os dados do array para diminuir a quantidade de dbseek feito na tabela HWV.
	aSort(aEpc,,,{|x,y| x[ARR_EPC_PRODUTO]+x[ARR_EPC_LOTE]+x[ARR_EPC_SUBLOTE]+x[ARR_EPC_SERIE] < ;
	                    y[ARR_EPC_PRODUTO]+y[ARR_EPC_LOTE]+y[ARR_EPC_SUBLOTE]+y[ARR_EPC_SERIE]})

	//Recupera a quantidade das EPC's e soma no array aDados
	nTotal := Len(aEpc)
	For nIndex := 1 To nTotal
		If aEpc[nIndex][ARR_EPC_STATUS] <> "1"
			Loop
		EndIf

		//Se foi gerada a partir de uma OP
		If CB7->CB7_ORIGEM == '3'
			cChaveB1 := xFilial("SB1") + aEpc[nIndex][ARR_EPC_PRODUTO]
			LoadProd(cChaveB1)
			If __oProds[cChaveB1][ARR_SB1_RASTRO] == "L" .Or. __oProds[cChaveB1][ARR_SB1_RASTRO] == "S"
				nPosDim := oIndHWV[aEpc[nIndex][ARR_EPC_PRODUTO] +;
								   Space(nTamLote)               +;
								   Space(nTamSubLot)             +;
								   aEpc[nIndex][ARR_EPC_SERIE]]
				If nPosDim == Nil
					nPosDim := 0
				EndIf
			EndIf
		EndIf

		nPos := oIndHWV[aEpc[nIndex][ARR_EPC_PRODUTO]+;
		                aEpc[nIndex][ARR_EPC_LOTE   ]+;
		                aEpc[nIndex][ARR_EPC_SUBLOTE]+;
		                aEpc[nIndex][ARR_EPC_SERIE  ]]
		If nPos == Nil
			If lIncAll
				nPos := addArrayHWV(cFilHWV                      ,;
									cId                          ,;
									aEpc[nIndex][ARR_EPC_PRODUTO],;
									aEpc[nIndex][ARR_EPC_LOTE   ],;
									aEpc[nIndex][ARR_EPC_SUBLOTE],;
									aEpc[nIndex][ARR_EPC_SERIE  ],;
									aEpc[nIndex][ARR_EPC_QUANT  ],;
									0                            ,;
									0                            ,;
									@aDados                      ,;
									@oIndHWV)
			Else
				cChave := cFilHWV+cId+aEpc[nIndex][ARR_EPC_PRODUTO]+aEpc[nIndex][ARR_EPC_LOTE]+aEpc[nIndex][ARR_EPC_SUBLOTE]+aEpc[nIndex][ARR_EPC_SERIE]
				If HWV->(HWV_FILIAL+HWV_ID+HWV_PROD+HWV_LOTE+HWV_SUBLOT+HWV_NUMSER) == cChave .Or. HWV->(dbSeek(cChave))
					nPos := addArrayHWV(HWV->HWV_FILIAL                              ,;
										HWV->HWV_ID                                  ,;
										HWV->HWV_PROD                                ,;
										HWV->HWV_LOTE                                ,;
										HWV->HWV_SUBLOT                              ,;
										HWV->HWV_NUMSER                              ,;
										HWV->HWV_QTLIDA + aEpc[nIndex][ARR_EPC_QUANT],;
										HWV->HWV_QTOS                                ,;
										HWV->(Recno())                               ,;
										@aDados                                      ,;
										@oIndHWV)
				Else
					nPos := addArrayHWV(cFilHWV                      ,;
										cId                          ,;
										aEpc[nIndex][ARR_EPC_PRODUTO],;
										aEpc[nIndex][ARR_EPC_LOTE   ],;
										aEpc[nIndex][ARR_EPC_SUBLOTE],;
										aEpc[nIndex][ARR_EPC_SERIE  ],;
										aEpc[nIndex][ARR_EPC_QUANT  ],;
										0                            ,;
										0                            ,;
										@aDados                      ,;
										@oIndHWV)
				EndIf
			EndIf
		Else
			aDados[nPos][ARR_HWV_QTLIDA] += aEpc[nIndex][ARR_EPC_QUANT]
		EndIf

		//Caso espec�fico para diminuir a quantidade do ERP
		If nPosDim <> 0
			If aDados[nPosDim][ARR_HWV_QTOS] > 0
				aDados[nPos   ][ARR_HWV_QTOS] += aEpc[nIndex][ARR_EPC_QUANT]
				aDados[nPosDim][ARR_HWV_QTOS] -= aEpc[nIndex][ARR_EPC_QUANT]
			EndIf
			nPosDim := 0
		EndIf
	Next nIndex

	FreeObj(oIndHWV)
Return aDados

/*/{Protheus.doc} addArrayHWV
Fun��o para centralizar a adi��o de itens no array de dados da tabela HWV

@type  Static Function
@author marcelo.neumann
@since 02/06/2020
@version P12.1.30
@param cFilHWV  , Character , C�digo da filial
@param cId      , Character , ID do processamento
@param cProduto , Character , C�digo do produto
@param cLote    , Character , N�mero do lote
@param cSubLote , Character , N�mero do sub-lote
@param cSerie   , Character , N�mero de s�rie
@param nQtLida  , Numeric   , Quantidade lida do produto
@param nQtErp   , Numeric   , Quantidade real no ERP
@param nRecno   , Numeric   , RECNO do registro da HWV
@param aDadosHWV, Array     , Array com os dados j� inseridos da HWV. Passar por refer�ncia
@param oIndHWV  , JsonObject, JSON com os �ndices do array aDadosHWV. Passar por refer�ncia

@return nPos    , Numeric   , Posi��o do registro inclu�do
/*/
Static Function addArrayHWV(cFilHWV, cId, cProduto, cLote, cSubLote, cSerie, nQtLida, nQtErp, nRecno, aDadosHWV, oIndHWV)
	Local nPos := 0

	aAdd(aDadosHWV, Array(ARR_HWV_SIZE))
	nPos := Len(aDadosHWV)

	aDadosHWV[nPos][ARR_HWV_FILIAL ] := cFilHWV
	aDadosHWV[nPos][ARR_HWV_ID     ] := cId
	aDadosHWV[nPos][ARR_HWV_PROD   ] := cProduto
	aDadosHWV[nPos][ARR_HWV_LOTE   ] := cLote
	aDadosHWV[nPos][ARR_HWV_SUBLOTE] := cSubLote
	aDadosHWV[nPos][ARR_HWV_NUMSERI] := cSerie
	aDadosHWV[nPos][ARR_HWV_QTLIDA ] := nQtLida
	aDadosHWV[nPos][ARR_HWV_QTOS   ] := nQtErp
	aDadosHWV[nPos][ARR_HWV_RECNO  ] := nRecno

	oIndHWV[cProduto+cLote+cSubLote+cSerie] := nPos
Return nPos

/*/{Protheus.doc} getConfer
Busca os dados de uma confer�ncia de ordem de separa��o
@type Static Function
@author marcelo.neumann
@since 02/06/2020
@version P12.1.30
@param cId, Character, Identificador do processamento
@return cJson, Character, JSON com os dados.
/*/
Static Function getConfer(cId)
	Local cJson := ""
	Local oJson := JsonObject():New()

	HWT->(dbSetOrder(1))
	If !HWT->(dbSeek(xFilial("HWT")+cId))
		criaMsgErr(STR0039, STR0040 + " '" + cId + "' " + STR0041) // "Identificador da confer�ncia n�o existe." | "O identificador 'XX' n�o foi encontrado na base de dados."
		Return Nil
	EndIf

	oJson["branchId"  ] := HWT->HWT_FILIAL
	oJson["id"        ] := HWT->HWT_ID
	oJson["isFinished"] := Iif(HWT->HWT_STATUS <> "1", .T., .F.)
	oJson["headers"   ] := ConferHead()
	oJson["columns"   ] := ConferCols()
	oJson["items"     ] := ConferItms()
	oJson["tags"      ] := ConferTags()

	cJson := oJson:ToJson()

	FreeObj(oJson)
Return cJson

/*/{Protheus.doc} ConferHead
Monta os dados do cabe�alho da Confer�ncia
@type Static Function
@author marcelo.neumann
@since 02/06/2020
@version P12.1.30
@return aHeader, Array, Array com os dados de cabe�alho
/*/
Static Function ConferHead()
	Local aHeader := {}
	Local lExiste := .T.
	Local nPos    := 0
	Local nPosMap := 0

	aAdd(aHeader, JsonObject():New())
	nPos    := Len(aHeader)
	nPosMap := aScan(__aMapHWT, {|x| x[1] == "HWT_FILIAL"})
	aHeader[nPos]["label"   ] := __aMapHWT[nPosMap][3]
	aHeader[nPos]["value"   ] := HWT->HWT_FILIAL
	aHeader[nPos]["subtitle"] := FWFilName(cEmpAnt, cFilAnt)

	aAdd(aHeader, JsonObject():New())
	nPos    := Len(aHeader)
	nPosMap := aScan(__aMapHWT, {|x| x[1] == "HWT_ORDSEP"})
	aHeader[nPos]["label"   ] := __aMapHWT[nPosMap][3]
	aHeader[nPos]["value"   ] := HWT->HWT_ORDSEP
	aHeader[nPos]["subtitle"] := ""

	//Verifica se j� est� posicionado
	If CB7->CB7_ORDSEP <> HWT->HWT_ORDSEP .Or. CB7->CB7_FILIAL <> xFilial("CB7")
		CB7->(dbSetOrder(1))
		lExiste := CB7->(dbSeek(xFilial("CB7") + HWT->HWT_ORDSEP))
	EndIf

	If lExiste
		If CB7->CB7_ORIGEM <> '3'
			aAdd(aHeader, JsonObject():New())
			nPos := Len(aHeader)
			aHeader[nPos]["label"   ] := FWX3Titulo("CB7_CLIENT")
			aHeader[nPos]["value"   ] := CB7->CB7_CLIENT + "-" + CB7->CB7_LOJA
			aHeader[nPos]["subtitle"] := Posicione("SA1", 1, xFilial("SA1")+CB7->CB7_CLIENT+CB7->CB7_LOJA, "A1_NOME")
		EndIf

		If !Empty(CB7->CB7_LOCAL)
			aAdd(aHeader, JsonObject():New())
			nPos := Len(aHeader)
			aHeader[nPos]["label"   ] := FWX3Titulo("CB7_LOCAL")
			aHeader[nPos]["value"   ] := CB7->CB7_LOCAL
			aHeader[nPos]["subtitle"] := Posicione("NNR", 1, xFilial("NNR")+CB7->CB7_LOCAL, "NNR_DESCRI")
		EndIf

		If !Empty(CB7->CB7_OP)
			aAdd(aHeader, JsonObject():New())
			nPos := Len(aHeader)
			aHeader[nPos]["label"   ] := FWX3Titulo("CB7_OP")
			aHeader[nPos]["value"   ] := CB7->CB7_OP
			aHeader[nPos]["subtitle"] := ""
		EndIf

		aAdd(aHeader, JsonObject():New())
		nPos := Len(aHeader)
		aHeader[nPos]["label"   ] := FWX3Titulo("CB7_ORIGEM")
		aHeader[nPos]["value"   ] := CB7->CB7_ORIGEM
		aHeader[nPos]["subtitle"] := getDesOrig("CB7","CB7_ORIGEM")

		If "09*" $ CB7->CB7_TIPEXP
			aAdd(aHeader, JsonObject():New())
			nPos := Len(aHeader)
			aHeader[nPos]["label"   ] := STR0052 //"Tipo"
			aHeader[nPos]["value"   ] := STR0053 //"Pr�-separa��o"
			aHeader[nPos]["subtitle"] := ""
		EndIf
	EndIf

Return aHeader

/*/{Protheus.doc} ConferCols
Retorna os dados de colunas para exibi��o da confer�ncia
@type Static Function
@author marcelo.neumann
@since 02/06/2020
@version P12.1.30
@return aColsCon, Array, Array com as colunas para exibi��o
/*/
Static Function ConferCols()
	Local aColsCon := {}
	Local nIndex   := 0
	Local nTotal   := Len(__aMapHWV)

	For nIndex := 1 To nTotal
		aAdd(aColsCon, JsonObject():New())
		aColsCon[nIndex]["property"] := __aMapHWV[nIndex][2]
		aColsCon[nIndex]["label"   ] := __aMapHWV[nIndex][3]

		If __aMapHWV[nIndex][1] == "_HWV_DIF"
			aColsCon[nIndex]["type"  ] := "label"
			aColsCon[nIndex]["labels"] := Array(2)

			aColsCon[nIndex]["labels"][1] := JsonObject():New()
			aColsCon[nIndex]["labels"][1]["color"] := "color-08"
			aColsCon[nIndex]["labels"][1]["value"] := .T.
			aColsCon[nIndex]["labels"][1]["label"] := STR0042 //"Diferen�a"

			aColsCon[nIndex]["labels"][2] := JsonObject():New()
			aColsCon[nIndex]["labels"][2]["color"] := "color-10"
			aColsCon[nIndex]["labels"][2]["value"] := .F.
			aColsCon[nIndex]["labels"][2]["label"] := STR0043 //"OK"
		EndIf
	Next nIndex

Return aColsCon

/*/{Protheus.doc} ConferItms
Monta os dados da tabela HWV para retorno
@type Static Function
@author marcelo.neumann
@since 02/06/2020
@version P12.1.30
@return aItems, Array, Array com os dados para retorno
/*/
Static Function ConferItms()
	Local aItems := {}
	Local cChave := xFilial("HWV")+HWT->HWT_ID
	Local nIndex := 0
	Local nPos   := 0
	Local nTotal := Len(__aMapHWV)

	HWV->(dbSetOrder(1))
	If HWV->(dbSeek(cChave))
		While HWV->(!Eof()) .And. HWV->(HWV_FILIAL+HWV_ID) == cChave
			nPos++
			aAdd(aItems, JsonObject():New())

			For nIndex := 1 To nTotal
				If __aMapHWV[nIndex][1] == "_HWV_DIF"
					aItems[nPos][__aMapHWV[nIndex][2]] := HWV->HWV_QTOS <> HWV->HWV_QTLIDA
				Else
					aItems[nPos][__aMapHWV[nIndex][2]] := &("HWV->"+__aMapHWV[nIndex][1])
				EndIf
			Next nIndex

			HWV->(dbSkip())
		End
	EndIf
Return aItems

/*/{Protheus.doc} ConferTags
Monta os dados das tags utilizadas na leitura
@type Static Function
@author marcelo.neumann
@since 02/06/2020
@version P12.1.30
@return aTags, Array, Array com os dados para retorno
/*/
Static Function ConferTags()
	Local aTags  := {}
	Local cAlias := GetNextAlias()
	Local cChave := ""
	Local cQuery := ""
	Local nPos   := 0

	cQuery := "SELECT HWU.HWU_EPC,"
	cQuery +=       " HWU.HWU_STATUS,"
	cQuery +=       " HWN.HWN_PROD,"
	cQuery +=       " HWN.HWN_LOTE,"
	cQuery +=       " HWN.HWN_SUBLOT,"
	cQuery +=       " HWN.HWN_NUMSER"
	cQuery +=  " FROM " + RetSqlName("HWU") + " HWU"
	cQuery +=  " LEFT OUTER JOIN " + RetSqlName("HWN") + " HWN"
	cQuery +=    " ON HWN.HWN_FILIAL = '" + xFilial("HWN") + "'"
	cQuery +=   " AND HWN.HWN_EPC    = HWU.HWU_EPC"
	cQuery +=   " AND HWN.D_E_L_E_T_ = ' '"
	cQuery += " WHERE HWU.HWU_FILIAL = '" + xFilial("HWU") + "'"
	cQuery +=   " AND HWU.HWU_ID     = '" + HWT->HWT_ID + "'"
	cQuery +=   " AND HWU.D_E_L_E_T_ = ' '"
	cQuery += " ORDER BY HWU.HWU_EPC"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

	While (cAlias)->(!Eof())
		nPos++
		aAdd(aTags, JsonObject():New())

		aTags[nPos]["code"   ] := (cAlias)->HWU_EPC
		aTags[nPos]["isValid"] := (cAlias)->HWU_STATUS == "1"

		If aTags[nPos]["isValid"]
			cChave := xFilial("SB1") + (cAlias)->(HWN_PROD)
			LoadProd(cChave)
			aTags[nPos]["description"] := RTrim((cAlias)->(HWN_PROD)) + " - " + RTrim(__oProds[cChave][ARR_SB1_DESC])
			If !Empty((cAlias)->(HWN_LOTE))
				aTags[nPos]["description"] += " - " + STR0044 + ": " + RTrim((cAlias)->(HWN_LOTE)) //"LOTE"
				If !Empty((cAlias)->(HWN_SUBLOT))
					aTags[nPos]["description"] += " - " + STR0045 + ": " + RTrim((cAlias)->(HWN_SUBLOT)) //"SUB-LOTE"
				EndIf
			EndIf
			If !Empty((cAlias)->(HWN_NUMSER))
				aTags[nPos]["description"] += " - " + STR0046 + ": " + RTrim((cAlias)->(HWN_NUMSER)) //"S�RIE"
			EndIf
		Else
			If Empty((cAlias)->(HWN_PROD))
				aTags[nPos]["description"] := STR0047 //"EPC n�o registrada no sistema."
			Else
				aTags[nPos]["description"] := STR0048 //"EPC est� desativada."
			EndIf
		EndIf

		(cAlias)->(dbSkip())
	End
	(cAlias)->(dbCloseArea())

Return aTags

/*/{Protheus.doc} validDic
Valida se o dicion�rio de dados possui as tabelas necess�rias para utiliza��o desta API
@type Static Function
@author marcelo.neumann
@since 16/06/2020
@version P12.1.30
@return lRet, Logic, Indica se o dicion�rio de dados est� correto.
/*/
Static Function validDic()
	Local lRet := .T.

	If __lDic == Nil
		__lDic := FWAliasInDic("HWT", .F.) .And. FWAliasInDic("HWU", .F.) .And. FWAliasInDic("HWV", .F.)
	EndIf
	lRet := __lDic

	If !lRet
		criaMsgErr(STR0049, STR0050) //"Dicion�rio de dados desatualizado." | "As tabelas necess�rias para execu��o deste processo n�o existem no dicion�rio de dados."
	EndIf

Return lRet

/*/{Protheus.doc} getDesOrig
Retorna a descri��o do Campo Origem no ComboBox
@type Static Function
@author marcelo.neumann
@since 22/06/2020
@version P12.1.30
@param 01 cAlias, Character, Alias do campo
@param 02 cCampo, Character, Nnome do campo a ser retornada a descri��o do combobox
@return   cDesc , Character, Descri��o da op��o no combobox
/*/
Static Function getDesOrig(cAlias, cCampo)
	Local aOpcoes := RetSX3Box(GetSX3Cache(cCampo, "X3_CBOX"),,,1)
	Local cDesc   := ""
	Local nOrigem := Val((cAlias)->&(cCampo))

	If nOrigem > 0
		cDesc := aOpcoes[nOrigem][3]
	EndIf

Return cDesc

/*/{Protheus.doc} LoadProd
Carrega as informa��es do produto
@type Static Function
@author marcelo.neumann
@since 06/07/2020
@version P12.1.30
@param cChave, Character, Chave do registro na SB1 (Filial + C�digo)
@return Nil
/*/
Static Function LoadProd(cChave)

	Local aArea := {}

	If __oProds[cChave] == Nil
		aArea := SB1->(GetArea())
		SB1->(dbSetOrder(1))
		If SB1->(dbSeek(cChave))
			__oProds[cChave] := {SB1->B1_DESC, SB1->B1_RASTRO}
		Else
			__oProds[cChave] := {" ", " "}
		EndIf
		RestArea(aArea)
	EndIf

Return
