#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "formregistration.ch"

STATIC lDicValido:= Nil

WSRESTFUL FormRegistration DESCRIPTION "Servi�o REST para manipula��o do Cadastro de Formul�rios do apontamento"

WSDATA userCode	  AS String  
WSDATA code       AS String  Optional
WSDATA count      AS INTEGER Optional
WSDATA startIndex AS INTEGER Optional
WSDATA page    	  AS INTEGER Optional
WSDATA codeForm   AS String  
 

WSMETHOD GET Form 				DESCRIPTION "Recupera os formul�rios de apontamento"				WSSYNTAX "/Form/{code}/{startIndex}/{count}/{page}" PATH "/Form"
WSMETHOD GET FormUsers 			DESCRIPTION "Recupera os formul�rios de apontamento do usu�rio"		WSSYNTAX "/FormUsers/{userCode}/{startIndex}/{count}/{page}" PATH "FormUsers"
WSMETHOD GET FormFields			DESCRIPTION "Recupera os campos do formul�rio"						WSSYNTAX "/FormFields/{codeForm}/{userCode}/{startIndex}/{count}/{page}" PATH "FormFields"
WSMETHOD GET FormMachines		DESCRIPTION "Recupera as m�quinas do formul�rio"					WSSYNTAX "/FormMachines/{codeForm}/{userCode}/{startIndex}/{count}/{page}" PATH "FormMachines"
WSMETHOD POST	        		DESCRIPTION "Inclui novos registros de formul�rios de apontamento"	WSSYNTAX "/FormRegistration/Form"
WSMETHOD PUT	        		DESCRIPTION "Altera um formul�rio de apontamento"					WSSYNTAX "/FormRegistration/Form"
WSMETHOD DELETE	        		DESCRIPTION "Exclui um formul�rio de apontamento"					WSSYNTAX "/FormRegistration/Form"

END WSRESTFUL

WSMETHOD GET Form WSRECEIVE code, startIndex, count, page WSSERVICE FormRegistration
Local aSOX  := {}
Local lGet  := .T.
Local nI    := 0
Local oJson


	// define o tipo de retorno do m�todo
	::SetContentType("application/json")


	If !VldDiciona()
		SetRestFault(400, EncodeUTF8(STR0021)) //"Tabela SOX ou SOY ou SOZ n�o cadastrada no sistema!"
		lGet := .F.	
	Else
		// define o tipo de retorno do m�todo
		oJson := JsonObject():New()


		If !Empty(::code)

			// insira aqui o c�digo para pesquisa do parametro recebido
			aSOX := PCPA121Con(::code)
			If Len(aSOX) > 0
				oJson['code']            := aSOX[1,1]
				oJson['appointmentType'] := aSOX[1,2]
				oJson['iconName']        := aSOX[1,3]
				oJson['description']     := EncodeUTF8(aSOX[1,4])
				oJson['stopReport']      := aSOX[1,5]
				oJson['useTimer']        := aSOX[1,6]
				oJson['typeProgress']    := aSOX[1,7]
			Else
				lGet := .F.
				SetRestFault(400, EncodeUTF8(STR0012)) //"Formul�rio n�o encontrado."
			EndIf

			// exemplo de retorno de um objeto JSON
			::SetResponse(oJson:toJson())
		Else
			// as propriedades da classe receber�o os valores enviados por querystring
			// exemplo: http://localhost:8080/sample?startIndex=1&count=10
			DEFAULT ::startIndex := 1, ::count := 20, ::page := 0

			// exemplo de retorno de uma lista de objetos JSON
			aSOX:= PCPA121con(::code, ::startIndex, ::count, ::page)
			
			If Len(aSOX) < 1
				lGet := .F.
				SetRestFault(400, EncodeUTF8(STR0012)) //"Formul�rio n�o encontrado."
			Else	
				::SetResponse('[')
				For nI := 1 To len(aSOX)
					If nI > ::startIndex
						::SetResponse(',')
					EndIf
					oJson['code']            := aSOX[nI,1]
					oJson['appointmentType'] := aSOX[nI,2]
					oJson['iconName']        := aSOX[nI,3]
					oJson['description']     := EncodeUTF8(aSOX[nI,4])
					oJson['stopReport']      := aSOX[nI,5]
					oJson['useTimer']        := aSOX[nI,6]
					oJson['typeProgress']    := aSOX[nI,7]
					::SetResponse(oJson:toJson())
				Next nI
				::SetResponse(']') 
			EndIf
			
		EndIf
	EndIf

Return lGet

// O metodo POST pode receber parametros por querystring, por exemplo:
// WSMETHOD POST WSRECEIVE startIndex, count WSSERVICE sample
WSMETHOD POST  WSSERVICE FormRegistration
	Local lPost := .T.
	Local cBody := " "
	Local aSOX := {}
	Local cCodForm   := " "
	Local cPrgApon   := " "
	Local cImagem    := " "
	Local cDescricao := " "
	Local oJson

	If !VldDiciona()
		SetRestFault(400, EncodeUTF8(STR0021)) //"Tabela SOX ou SOY ou SOZ n�o cadastrada no sistema!"
		lPost := .F.	
	Else
		oJson := JsonObject():New()

		cBody := ::GetContent()

		If oJson:fromJson( cBody ) <> nil

			SetRestFault(400, EncodeUTF8(STR0001)) //"Par�metros do apontamento n�o enviados ou inv�lidos."                                                                                                                                                                                                                                                                                                                                                                                                                                                              
			lPost := .F.
		Else
			If Empty(oJson['code'])
				SetRestFault(400, EncodeUTF8(STR0002)) //"C�digo do Formul�rio de Apontamento n�o informado."                                                                                                                                                                                                                                                                                                                                                                                                                                                                
				lPost := .F.
			ElseIf Empty(oJson['description'])
				SetRestFault(400, EncodeUTF8(STR0003)) //"Descri��o do Formul�rio de Apontamento n�o informado."                                                                                                                                                                                                                                                                                                                                                                                                                                                             
				lPost := .F.
			ElseIf Empty(oJson['iconName'])
				SetRestFault(400, EncodeUTF8(STR0004))  //"�cone do Formul�rio de Apontamento n�o informado."                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
				lPost := .F.
			ElseIf Empty(oJson['appointmentType'])
				SetRestFault(400, EncodeUTF8(STR0005)) //"Programa de Apontamento n�o informado"                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
				lPost := .F.
			ElseIf !PCPA121Vld('OX_FORM', oJson['code'], 3)
				SetRestFault(400, EncodeUTF8(STR0006)) //"Registro duplicado. Inclus�o n�o permitida!"                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
				lPost := .F.
			ElseIf !PCPA121Vld('OX_PRGAPON', oJson['appointmentType'], 3) 
				SetRestFault(400, EncodeUTF8(STR0007)) //"Programa de Apontamento inv�lidos. Op��o permitida s�o: 1 = MATA250, 2 = MATA680, 3 = MATA681 , 4 = SFCA314"                                                                                                                                                                                                                                                                                                                                                                                                       
				lPost := .F.
			ElseIf !PCPA121Tam('OX_FORM', oJson['code'])	
				SetRestFault(400, EncodeUTF8(STR0008)) //"Campo C�digo do Formul�rio est� informado com um conte�do maior que o definido no banco de dados"
				lPost := .F.
			ElseIf !PCPA121Tam('OX_PRGAPON', oJson['appointmentType'])	
				SetRestFault(400, EncodeUTF8(STR0009)) //"Campo Programa de Apontamento est� informado com um conte�do maior que o definido no banco de dados"
				lPost := .F.
			ElseIf !PCPA121Tam('OX_IMAGEM', oJson['iconName'])	
				SetRestFault(400, EncodeUTF8(STR0010)) //"Campo Imagem est� informado com um conte�do maior que o definido no banco de dados"
				lPost := .F.
			ElseIf !PCPA121Tam('OX_DESCR', oJson['description'])	
				SetRestFault(400, EncodeUTF8(STR0011)) //"Campo Descri��o do Formul�rio est� informado com um conte�do maior que o definido no banco de dados"
				lPost := .F.
			ElseIf !Empty(oJson['stopReport'] )
				If !PCPA121Vld('OX_PARADA', oJson['stopReport'], 3)
					SetRestFault(400, EncodeUTF8(STR0022)) //"Campo Apontamento de Parada est� informado com conte�do inv�lido. Valores v�lidos: 1-Sim, 2-N�o"
					lPost := .F.
				EndIf

				If lPost
					If !PCPA121vld('OX_PARADA', oJson['stopReport'], 3, oJson['appointmentType']) 
						SetRestFault(400, EncodeUTF8(STR0023)) //"� permitido informar o campo Apontamento de Parada somente para o tipo de apontamento 4 - SFCA314."
						lPost := .F.
					EndIf
				EndIf
			EndIf
			
			If !Empty(oJson['useTimer'] )
				If !PCPA121Vld('OX_CRONOM', oJson['useTimer'], 3)
					SetRestFault(400, EncodeUTF8(STR0024)) //"Campo Usar Cron�metro est� informado com conte�do inv�lido. Valores v�lidos: 1-Sim, 2-N�o"
					lPost := .F.
				EndIf

				If lPost
					If !PCPA121vld('OX_CRONOM', oJson['useTimer'], 3, oJson['appointmentType']) 
						SetRestFault(400, EncodeUTF8(STR0025)) //"� permitido informar o campo Usar Cron�metro somente para o tipo de apontamento 4 - SFCA314."
						lPost := .F.
					EndIf
				EndIf
			EndIf

			If !Empty(oJson['typeProgress'] )
				If !PCPA121Vld('OX_TPPROG', oJson['typeProgress'], 3)
					SetRestFault(400, EncodeUTF8(STR0026)) //"Campo Tipo de Progresso est� informado com conte�do inv�lido. Valores v�lidos: 1-Sim, 2-N�o"
					lPost := .F.
				EndIf

				If lPost
					If !PCPA121vld('OX_TPPROG', oJson['typeProgress'], 3, oJson['useTimer']) 
						SetRestFault(400, EncodeUTF8(STR0027)) //"� permitido informar o campo Tipo de Progresso somente quando o campo Usar Cron�metro estiver igual a 1 - Sim."
						lPost := .F.
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf

	If lPost
		cCodForm   := oJson['code']
		cPrgApon   := oJson['appointmentType']
		cImagem    := oJson['iconName']
		cDescricao := DecodeUTF8(oJson['description'])
		cParada    := oJson['stopReport'] 
		cCronom    := oJson['useTimer']
		cTpProg    := oJson['typeProgress']

		aAdd(aSOX,{"OX_FORM"     ,cCodForm   ,Nil})
		aAdd(aSOX,{"OX_PRGAPON"  ,cPrgApon   ,Nil})
		aAdd(aSOX,{"OX_IMAGEM"   ,cImagem    ,Nil})
		aAdd(aSOX,{"OX_DESCR"    ,cDescricao ,Nil})
		aAdd(aSOX,{"OX_PARADA"   ,cParada    ,Nil})
		aAdd(aSOX,{"OX_CRONOM"   ,cCronom    ,Nil})
		aAdd(aSOX,{"OX_TPPROG"   ,cTpProg    ,Nil})

		If !PCPA121In(aSOX) //Fun��o que far� a inclus�o da tabela SOX
			lPost := .F.
		EndIf
		::SetResponse(oJson:toJson())
	EndIf
	FreeObj(oJson)
Return lPost

// O metodo PUT pode receber parametros por querystring, por exemplo:
// WSMETHOD PUT WSRECEIVE startIndex, count WSSERVICE sample
WSMETHOD PUT  WSSERVICE FormRegistration
	Local lPut			:= .T.
	Local cBody			:= " "
	Local aSOX			:= {}
	Local cCodForm		:= " "
	Local cPrgApon		:= " "
	Local cImagem		:= " "
	Local cDescricao	:= " "
	Local oJson

	If !VldDiciona()
		SetRestFault(400, EncodeUTF8(STR0021)) //"Tabela SOX ou SOY ou SOZ n�o cadastrada no sistema!"
		lPut := .F.	
	Else
		oJson := JsonObject():New()

		cBody := ::GetContent()

		If oJson:fromJson( cBody ) <> nil
			SetRestFault(400, EncodeUTF8(STR0001)) //"Par�metros do apontamento n�o enviados ou inv�lidos."
			lPut := .F.
		Else
			If Empty(oJson['code'])
				SetRestFault(400, EncodeUTF8(STR0002)) //"C�digo do Formul�rio de Apontamento n�o informado."
				lPut := .F.
			ElseIf Empty(oJson['description'])
				SetRestFault(400, EncodeUTF8(STR0003)) //"Descri��o do Formul�rio de Apontamento n�o informado."                                                                                                                                                                                                                                                                                                                                                                                                                                                             
				lPut := .F.
			ElseIf Empty(oJson['iconName'])
				SetRestFault(400, EncodeUTF8(STR0004))  //"�cone do Formul�rio de Apontamento n�o informado."                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
				lPut := .F.
			ElseIf Empty(oJson['appointmentType'])
				SetRestFault(400, EncodeUTF8(STR0005)) //"Programa de Apontamento n�o informado"                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
				lPut := .F.
			ElseIf Empty(oJson['code'])
				SetRestFault(400, EncodeUTF8(STR0002)) //"C�digo do Formul�rio de Apontamento n�o informado."
				lPut := .F.
			ElseIf !PCPA121vld('OX_FORM', oJson['code'], 4) 
				SetRestFault(400, EncodeUTF8(STR0012)) //"Formul�rio n�o encontrado."
				lPut := .F.
			ElseIf !PCPA121vld('OX_PRGAPON', oJson['appointmentType'], 4, oJson['code']) 
				SetRestFault(400, EncodeUTF8(STR0013)) //"N�o � permitido alterar o Programa de Apontamento."
				lPut := .F.
			ElseIf !PCPA121Tam('OX_FORM', oJson['code'])	
				SetRestFault(400, EncodeUTF8(STR0008)) //"Campo C�digo do Formul�rio est� informado com um valor maior que o definido no banco de dados"
				lPut := .F.
			ElseIf !PCPA121Tam('OX_PRGAPON', oJson['appointmentType'])	
				SetRestFault(400, EncodeUTF8(STR0009)) //"Campo Programa de Apontamento est� informado com um valor maior que o definido no banco de dados"
				lPut := .F.
			ElseIf !PCPA121Tam('OX_IMAGEM', oJson['iconName'])	
				SetRestFault(400, EncodeUTF8(STR0010)) //"Campo Imagem est� informado com um valor maior que o definido no banco de dados"
				lPut := .F.
			ElseIf !PCPA121Tam('OX_DESCR', oJson['description'])	
				SetRestFault(400, EncodeUTF8(STR0011)) //"Campo Descri��o do Formul�rio est� informado com um valor maior que o definido no banco de dados"'	'
				lPut := .F.
			ElseIf !Empty(oJson['stopReport'] )
				If !PCPA121Vld('OX_PARADA', oJson['stopReport'], 3)
					SetRestFault(400, EncodeUTF8(STR0022)) //"Campo Apontamento de Parada est� informado com conte�do inv�lido. Valores v�lidos: 1-Sim, 2-N�o"
					lPut := .F.
				EndIf

				If lPut
					If !PCPA121vld('OX_PARADA', oJson['stopReport'], 3, oJson['appointmentType']) 
						SetRestFault(400, EncodeUTF8(STR0023)) //"� permitido informar o campo Apontamento de Parada somente para o tipo de apontamento 4 - SFCA314."
						lPut := .F.
					EndIf
				EndIf
			EndIf
			
			If !Empty(oJson['useTimer'] )
				If !PCPA121Vld('OX_CRONOM', oJson['useTimer'], 3)
					SetRestFault(400, EncodeUTF8(STR0024)) //"Campo Usar Cron�metro est� informado com conte�do inv�lido. Valores v�lidos: 1-Sim, 2-N�o"
					lPut := .F.
				EndIf

				If lPut
					If !PCPA121vld('OX_CRONOM', oJson['useTimer'], 3, oJson['appointmentType']) 
						SetRestFault(400, EncodeUTF8(STR0025)) //"� permitido informar o campo Usar Cron�metro somente para o tipo de apontamento 4 - SFCA314."
						lPut := .F.
					EndIf
				EndIf
			EndIf

			If !Empty(oJson['typeProgress'] )
				If !PCPA121Vld('OX_TPPROG', oJson['typeProgress'], 3)
					SetRestFault(400, EncodeUTF8(STR0026)) //"Campo Tipo de Progresso est� informado com conte�do inv�lido. Valores v�lidos: 1-Sim, 2-N�o"
					lPut := .F.
				EndIf

				If lPut
					If !PCPA121vld('OX_TPPROG', oJson['typeProgress'], 3, oJson['useTimer']) 
						SetRestFault(400, EncodeUTF8(STR0027)) //"� permitido informar o campo Tipo de Progresso somente quando o campo Usar Cron�metro estiver igual a 1 - Sim."
						lPut := .F.
					EndIf
				EndIf
			EndIf
		EndIf     
	EndIf

	If lPut
		cCodForm   := oJson['code']
		cPrgApon   := oJson['appointmentType']
		cImagem    := oJson['iconName']
		cDescricao := DecodeUTF8(oJson['description'])
		cParada    := oJson['stopReport'] 
		cCronom    := oJson['useTimer']
		cTpProg    := oJson['typeProgress']

		aAdd(aSOX,{"OX_FORM"     ,cCodForm   ,Nil})
		aAdd(aSOX,{"OX_PRGAPON"  ,cPrgApon   ,Nil})
		aAdd(aSOX,{"OX_IMAGEM"   ,cImagem    ,Nil})
		aAdd(aSOX,{"OX_DESCR"     ,cDescricao ,Nil})
		aAdd(aSOX,{"OX_PARADA"   ,cParada     ,Nil})
		aAdd(aSOX,{"OX_CRONOM"   ,cCronom    ,Nil})
		aAdd(aSOX,{"OX_TPPROG"   ,cTpProg    ,Nil})

		If !PCPA121Atu(aSOX) //Fun��o que far� a atualiza��o da tabela SOX
			lPut := .F.
		EndIf

		::SetResponse(oJson:toJson())
	EndIf
	FreeObj(oJson)
Return lPut

// O metodo DELETE pode receber parametros por querystring, por exemplo:
// WSMETHOD DELETE WSRECEIVE startIndex, count WSSERVICE sample
WSMETHOD DELETE  WSSERVICE FormRegistration
Local lDelete := .T.
Local oJson

If !VldDiciona()
	SetRestFault(400, EncodeUTF8(STR0021)) //"Tabela SOX ou SOY ou SOZ n�o cadastrada no sistema!"
	lDelete := .F.	
Else
	oJson := JsonObject():New()

	cBody := ::GetContent()

	If oJson:fromJson( cBody ) <> nil
		SetRestFault(400, EncodeUTF8(STR0001)) //"Par�metros do apontamento n�o enviados ou inv�lidos."
		lDelete := .F.
	Else
		If !PCPA121Vld('OX_FORM', oJson['code'], 5) 
			SetRestFault(400, EncodeUTF8(STR0012)) //"Formul�rio n�o encontrado."
			lDelete := .F.
		EndIf
	EndIf
EndIf

If lDelete
	cCodForm   := oJson['code']

	If !PCPA121Del(cCodForm) //Fun��o que far� a exclus�o da tabela SOX
		lDelete := .F.
	EndIf

	::SetResponse(oJson:toJson())
EndIf
FreeObj(oJson)
Return lDelete


WSMETHOD GET FormUsers WSRECEIVE userCode, startIndex, count, page WSSERVICE FormRegistration

Local aSOZ  := {}
Local aSOY  := {}
Local aHWS  := {}
Local lGet  := .T.
Local nI    := 0
Local n1I    := 0
Local oJson


// define o tipo de retorno do m�todo
::SetContentType("application/json")

If !VldDiciona()
	SetRestFault(400, EncodeUTF8(STR0021)) //"Tabela SOX ou SOY ou SOZ n�o cadastrada no sistema!"
	lGet := .F.
Else

	// define o tipo de retorno do m�todo
	oJson := JsonObject():New()
	oJson["FormFields"] := {}
	Aadd(oJson["FormFields"], JsonObject():New())

	// verifica se recebeu parametro pela URL
	// exemplo: http://localhost:8080/sample/1
	If !Empty(::userCode)

		// as propriedades da classe receber�o os valores enviados por querystring
		// exemplo: http://localhost:8080/sample?startIndex=1&count=10
		DEFAULT ::startIndex := 1, ::count := 20, ::page := 0

		// exemplo de retorno de uma lista de objetos JSON
		aSOZ:= PCPA121usf(::userCode, ::startIndex, ::count, ::page)

		
		If Len(aSOZ) < 1
			lGet := .F.
			SetRestFault(400, EncodeUTF8(STR0020)) //"Formul�rio n�o encontrado."
		Else	
			::SetResponse('[')
			For nI := 1 To len(aSOZ)
				If nI > ::startIndex
					::SetResponse(',')
				EndIf 
				oJson['code']            := aSOZ[nI,1]
				oJson['description']     := EncodeUTF8(aSOZ[nI,2])
				oJson['appointmentType'] := aSOZ[nI,3]
				oJson['iconName']        := LOWER(trim(aSOZ[nI,4]))
				oJson['stopReport']      := aSOZ[nI,5]
				oJson['useTimer']        := aSOZ[nI,6]
				oJson['typeProgress']    := aSOZ[nI,7]

				aSOY:={}
				aSOY:= PCPA121fld(aSOZ[nI,1], ::userCode, ::startIndex, ::count, ::page)

				If Len(aSOY) < 1
					lGet := .F.
					SetRestFault(400, EncodeUTF8(STR0020)) //"N�o h� Formul�rio de apontamento cadastrados para o usu�rio."
				Else	
					oJson['FormFields'] := {}
					
					For n1I := 1 To len(aSOY)
						Aadd(oJson['FormFields'], JsonObject():New())

						oJson['FormFields'][n1I]['code']  	    := aSOY[n1I,1]
						oJson['FormFields'][n1I]['field']		:= trim(aSOY[n1I,2])
						oJson['FormFields'][n1I]['description']	:= trim(EncodeUTF8(aSOY[n1I,3]))
						oJson['FormFields'][n1I]['codebar']		:= aSOY[n1I,4]
						oJson['FormFields'][n1I]['visible']		:= aSOY[n1I,5]
						oJson['FormFields'][n1I]['editable']	:= aSOY[n1I,6]
						oJson['FormFields'][n1I]['default']		:= trim(aSOY[n1I,7])
					Next n1I
				EndIf		

                If AliasInDic("HWS")
                	aHWS:={}
                	aHWS:= PCPA121maq(aSOZ[nI,1], ::userCode, ::startIndex, ::count, ::page)

					oJson['FormMachines'] := {}

					If Len(aHWS) >= 1
						For n1I := 1 To len(aHWS)
							Aadd(oJson['FormMachines'], JsonObject():New())

							oJson['FormMachines'][n1I]['code']  	  := aHWS[n1I,1]
							oJson['FormMachines'][n1I]['machine']     := trim(aHWS[n1I,2])
							oJson['FormMachines'][n1I]['description'] := trim(EncodeUTF8(aHWS[n1I,3]))
						Next n1I
					EndIf		
				EndIf		

				::SetResponse(oJson:toJson())
			Next nI
			::SetResponse(']')
		EndIf
	Else
		SetRestFault(400, EncodeUTF8(STR0019)) //"Usu�rio n�o informado."
		lGet := .F.
	EndIf
EndIf

Return lGet


WSMETHOD GET FormFields WSRECEIVE codeForm, userCode, startIndex, count, page WSSERVICE FormRegistration

Local aSOY  := {}
Local lGet  := .T.
Local nI    := 0
Local oJson

// define o tipo de retorno do m�todo
::SetContentType("application/json")

If !VldDiciona()
	SetRestFault(400, EncodeUTF8(STR0021)) //"Tabela SOX ou SOY ou SOZ n�o cadastrada no sistema!"
	lGet := .F.
Else

	// define o tipo de retorno do m�todo
	oJson := JsonObject():New()

	// verifica se recebeu parametro pela URL
	// exemplo: http://localhost:8080/sample/1

	If !Empty(::codeForm) .And. !Empty(::userCode)

		// as propriedades da classe receber�o os valores enviados por querystring
		// exemplo: http://localhost:8080/sample?startIndex=1&count=10
		DEFAULT ::startIndex := 1, ::count := 20, ::page := 0

		// exemplo de retorno de uma lista de objetos JSON
		aSOY:= PCPA121fld(::codeForm, ::userCode, ::startIndex, ::count, ::page)
		
		If Len(aSOY) < 1
			lGet := .F.
			SetRestFault(400, EncodeUTF8(STR0012)) //"Formul�rio n�o encontrado."
		Else	
			::SetResponse('[')
			For nI := 1 To len(aSOY)
				If nI > ::startIndex
					::SetResponse(',')
				EndIf
				oJson['code']			:= aSOY[nI,1]
				oJson['field']			:= aSOY[nI,2]
				oJson['description']	:= EncodeUTF8(aSOY[nI,3])
				oJson['codebar']		:= aSOY[nI,4]
				oJson['visible']		:= aSOY[nI,5]
				oJson['editable']		:= aSOY[nI,6]
				oJson['default']		:= aSOY[nI,7]
				
				::SetResponse(oJson:toJson())
			Next nI
			::SetResponse(']')
		EndIf
	Else
		if Empty(::codeForm)
			SetRestFault(400, EncodeUTF8(STR0002)) //"C�digo do Formul�rio de Apontamento n�o informado."
			lGet := .F.
		ElseIf Empty(::userCode)
			SetRestFault(400, EncodeUTF8(STR0019)) //"Usu�rio n�o informado."
			lGet := .F.
		EndIf
	EndIf
EndIf

Return lGet


WSMETHOD GET FormMachines WSRECEIVE codeForm, userCode, startIndex, count, page WSSERVICE FormRegistration

Local aHWS  := {}
Local lGet  := .T.
Local nI    := 0
Local oJson

// define o tipo de retorno do m�todo
::SetContentType("application/json")

If !VldDiciona()
	SetRestFault(400, EncodeUTF8(STR0021)) //"Tabela SOX ou SOY ou SOZ n�o cadastrada no sistema!"
	lGet := .F.
Else

	// define o tipo de retorno do m�todo
	oJson := JsonObject():New()

	// verifica se recebeu parametro pela URL
	// exemplo: http://localhost:8080/sample/1

	If !Empty(::codeForm) .And. !Empty(::userCode)

		// as propriedades da classe receber�o os valores enviados por querystring
		// exemplo: http://localhost:8080/sample?startIndex=1&count=10
		DEFAULT ::startIndex := 1, ::count := 20, ::page := 0

		// exemplo de retorno de uma lista de objetos JSON
		aHWS:= PCPA121maq(::codeForm, ::userCode, ::startIndex, ::count, ::page)
		
		If Len(aHWS) < 1
			lGet := .F.
			SetRestFault(400, EncodeUTF8(STR0012)) //"Formul�rio n�o encontrado."
		Else	
			::SetResponse('[')
			For nI := 1 To len(aHWS)
				If nI > ::startIndex
					::SetResponse(',')
				EndIf
				oJson['code']  	     := aHWS[nI,1]
				oJson['machine']     := trim(aHWS[nI,2])
				oJson['description'] := trim(EncodeUTF8(aHWS[nI,3]))
				
				::SetResponse(oJson:toJson())
			Next nI
			::SetResponse(']')
		EndIf
	Else
		if Empty(::codeForm)
			SetRestFault(400, EncodeUTF8(STR0002)) //"C�digo do Formul�rio de Apontamento n�o informado."
			lGet := .F.
		ElseIf Empty(::userCode)
			SetRestFault(400, EncodeUTF8(STR0019)) //"Usu�rio n�o informado."
			lGet := .F.
		EndIf
	EndIf
EndIf

Return lGet


/*/{Protheus.doc} VldDiciona
	Valida se as tabelas SOX, SOY e SOZ est�o no dicion�rio.
	@typVldDiciona

	@author Michelle Ramos
	@since 14/12/2018
	@param Sem par�metro
	@return True or False

	/*/
 Static Function VldDiciona()
	Local lRet := .T.

	if lDicValido == Nil
		If !AliasInDic("SOX") .Or. !AliasInDic("SOY") .Or. !AliasInDic("SOZ")
			lRet := .F.
			lDicValido := .F.
		Else
			lDicValido := .T.
		EndIf
	Else
		lRet := lDicValido
	EndIf

Return lRet
