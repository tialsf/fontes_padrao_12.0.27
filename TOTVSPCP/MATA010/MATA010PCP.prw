#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'
#Include "MATA010PCP.CH"

Static __lUsaRFID := Nil
Static __lHaveKey := .F.

/*/{Protheus.doc} MATA010PCP
Classe de eventos relacionados com o produto x SIGAPCP
@author Carlos Alexandre da Silveira
@since 25/02/2019
@version 1.0
/*/
CLASS MATA010PCP FROM FWModelEvent

	METHOD New() CONSTRUCTOR
	METHOD Activate()
	METHOD ModelPosVld()

ENDCLASS

METHOD New(oModel) CLASS MATA010PCP

	If FindClass("MATA010API")
		oModel:InstallEvent("MATA010API",,MATA010API():New())
	EndIf

	If FindClass("MATA010NET")
		oModel:InstallEvent("MATA010NET",,MATA010NET():New())
	EndIf

Return

/*/{Protheus.doc} Activate
M�todo que � chamado pelo MVC quando ocorrer a ativa��o do Model.
@author Marcelo Neumann
@since 20/05/2020
@version 1.0
@param 01 oModel, Object, Modelo principal
@param 02 lCopy , Logical, Informa se � uma opera��o de C�pia
/*/
METHOD Activate(oModel, lCopy) CLASS MATA010PCP

	Local nNextKey := 0

	If __lUsaRFID == Nil
		If oModel:GetModel("SVKDETAIL") == Nil
			__lUsaRFID := .F.
		Else
			__lUsaRFID := oModel:GetModel("SVKDETAIL"):GetStruct():HasField("VK_RFIDKEY")
		EndIf
	EndIf

	//Prote��o para os campos novos
	If __lUsaRFID
		//Verifica se j� foi informado o RFID Key do produto
		If Empty(oModel:GetValue("SVKDETAIL","VK_RFIDKEY"))
			__lHaveKey := .F.
		Else
			If lCopy
				nNextKey := GetNextKey(!lCopy)
				If nNextKey == -1
					oModel:LoadValue("SVKDETAIL","VK_RFID", "0")
					oModel:LoadValue("SVKDETAIL","VK_RFIDKEY", 0)
				Else
					oModel:LoadValue("SVKDETAIL","VK_RFIDKEY", nNextKey)
					If Empty(oModel:GetValue("SVKDETAIL", "VK_RFIDFAT"))
						oModel:LoadValue("SVKDETAIL","VK_RFIDFAT", 1)
					EndIf
				EndIf
				__lHaveKey := .F.
			Else
				__lHaveKey := .T.
			EndIf
		EndIf
	EndIf

Return

/*/{Protheus.doc} ModelPosVld
P�s valida��o do modelo
@author Carlos Alexandre da Silveira
@since 25/02/2019
@version 1.0
@param 01 oModel  , Object   , Modelo de dados que ser� validado
@param 02 cModelId, Character, ID do modelo de dados que est� sendo validado
@return   lRet    , Logical  , Indica se o modelo foi validado com sucesso
/*/
METHOD ModelPosVld(oModel, cModelId) Class MATA010PCP
	Local aRet    := {}
	Local lRet    := .T.
	Local lVldAlt := .T.
	Local lAltern := .F.
	Local nX      := 0
	Local oMdlSVK := oModel:GetModel("SVKDETAIL")
	Local oMdlSGI := oModel:GetModel("SGIDETAIL")
	Local oMdlSB1 := oModel:GetModel("SB1MASTER")

	lVldAlt := oMdlSGI == Nil

	//Caso o campo horizonte fixo seja maior que zero, o campo tipo de horizonte fixo ser� obrigat�rio.
	If oMdlSVK != Nil .And. Empty(oMdlSVK:GetValue("VK_TPHOFIX"))
		If oMdlSVK:GetValue("VK_HORFIX") > 0
			Help(,,'Help',,STR0002,1,0) //STR0002 - Quando � preenchido o campo Horizonte Fixo, � obrigat�rio o preenchimento do campo Tipo de Horizonte Fixo.
			lRet := .F.
		EndIf
	EndIf

	If lRet .And. oModel:GetOperation() == MODEL_OPERATION_DELETE
		//Verifica se poder� excluir o produto. (Valida��o de alternativos)
		aRet := PCPAltVlDe(oModel:GetModel("SB1MASTER"):GetValue("B1_COD"), lVldAlt)
		If !aRet[1]
			HELP(' ',1,"Help" ,,aRet[2],2,0,,,,,,)
			lRet := .F.
		EndIf
	EndIf

	If __lUsaRFID
		If lRet .And. !A010RFIDVa()
			lRet := .F.
		EndIf

		If lRet .And. oMdlSVK != Nil .And. oModel:GetOperation() <> MODEL_OPERATION_DELETE
			If oMdlSVK:GetValue("VK_RFID") == '1' .And. oMdlSVK:GetValue("VK_RFIDFAT") <= 0
				lRet := .F.
				Help( ,  , "Help", ,  STR0008,; //"Fator quantidade dos par�metros de RFID n�o foi informado."
				     1, 0, , , , , , {STR0009}) //"Informe o fator de quantidade do RFID com um valor maior que 0."
			EndIf
		EndIf
	EndIf

	If lRet .And. oModel:GetOperation() == MODEL_OPERATION_UPDATE
		If oMdlSB1:GetValue("B1_FANTASM") == "S"
			If oMdlSGI  != Nil
				If !oMdlSGI:IsEmpty()
					For nX := 1 To oMdlSGI:Length()
						If !oMdlSGI:IsDeleted(nX)
							lAltern := .T.
							Exit
						EndIf
					Next nX
				EndIf
			Else
				SGI->(dbSetOrder(1))
				If SGI->(dbSeek(xFilial("SGI")+oMdlSB1:GetValue("B1_COD")))
					lAltern := .T.
				EndIf
			EndIf
			If !lAltern
				SGI->(dbSetOrder(2))
				If SGI->(dbSeek(xFilial("SGI")+oMdlSB1:GetValue("B1_COD")))
					lAltern := .T.
				EndIf
			EndIf

			If lAltern
				Help(" ",1,"ALTERFAN")
				lRet := .F.
			EndIf
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} ValidPrdOri
Fun��o para complementar os submodelos do Model.
� necessario obter a opera��o do modelo, para verificar
se o usuario tem acesso a rotina nessa opera��o.

Foi definido para ser executado antes do activate, pois
no momento da execu��o da fun��o ModelDef, n�o existe opera��o
ainda.
@author Renan Roeder
@since 26/09/2018
@version 1.0
/*/
Function ValidPrdOri()
	Local lRet    := .T.
	Local lPrdOri := &(ReadVar())
	Local lPrdRet := M->B1_COD

	If lPrdOri == lPrdRet
		Help(" ",1,"A010PCPRET")
		lRet := .F.
	EndIf

Return lRet

/*/{Protheus.doc} ExisteSFCPcp
Valida Integra��o com SFC - Parametro/Compartilhamento tabela
Fun��o migrada do MATA010 - ExisteSFC
@author Michele Girardi
@since 11/01/2019
@version 1.0
/*/
Function ExisteSFCPcp(cTabela)
	Local lRet		:= .F.
	Local xIntSFC	:= GetMV("MV_INTSFC",.F.,0)
	Local nI        := 0
	Local nParSFC
	Local nExistSfc := .F.
	Local cMode
	Local aEmpresas := {}
	Local aAreaSM0 := SM0->(GetArea())
	Local aArea    := GetArea()
	Local nTamanho  := 0

	//Verificar se a filial corrente possui integra��o
	If ValType(xIntSFC) # "N"
		lRet := xIntSFC
	Else
		lRet := xIntSFC == 1
	EndIf

	//Se a filial corrente possuir integra��o, processar a integra��o.
	if lRet
		return lRet
	EndIf

	//Se a filial corrente n�o possuir integra��o verificar se outra filial possui integra��o

	//Carrega todas as filiais no array
	OpenSm0(,.F.)
	If Select("SM0")<= 0
		Final(STR0001)//"SIGAMAT.EMP com problemas!"
	Else
		SM0->(dbGotop())
		While !SM0->(EOF())
			If !SM0->(Deleted())
				aAdd(aEmpresas, {SM0->M0_CODFIL})
			EndIf
			SM0->(dbSkip())
		End
	EndIf

	DbSelectArea("SX6")
	SX6->(DbSetOrder(1))
	nTamanho := Len(SX6->X6_FIL)

	//Busca na SX6 se o par�metro est� marcado para alguma outra filial
	nExistSfc = .F.
	For nI := 1 to Len(aEmpresas)
		DbSelectArea("SX6")
		SX6->(DbSetOrder(1))
		SX6->(dbSeek(substr(aEmpresas[nI,1],1,nTamanho)+"MV_INTSFC"))

		nParSFC := SX6->X6_conteud

		If VAL(nParSFC) == 1
			nExistSfc = .T.
			exit
		EndIf
	Next nI

	//Se nenhuma filial possuir integra��o n�o processar a integra��o.
	if !nExistSfc
		lRet := .F.
		RestArea(aAreaSM0)
		RestArea(aArea)
		return lRet
	EndIf

	//Se a filial corrente n�o possuir integra��o e outra filial possuir
	//Verificar se a tabela em quest�o � compartilhada
	cMode := FWModeAccess(cTabela)

	//Se for compartilhada, processa a integra��o
	//Se n�o for compartilhada n�o processar a integra��o.
	if cMode == 'E'
		lRet = .F.
	Else
		lRet = .T.
	EndIf

	RestArea(aAreaSM0)
	RestArea(aArea)

Return(lRet)

/*/{Protheus.doc} MTA010IEST
Fun��o para executar a integra��o da estrutura.
Produtos alternativos / Altera��o do campo B1_FANTASM

@type  Function
@author marcelo.neumann
@since 20/08/2019
@version P12
@param cEmp     , Character, C�digo da empresa para conex�o
@param cFil     , Character, C�digo da filial para conex�o
@param cProduto , Character, C�digo do produto a ser integrado
@param lFantasma, Logical  , Altera��o da propriedade B1_FANTASM
@return Nil
/*/
Function MTA010IEST(cEmp, cFil, cProduto, lFantasma)

	ErrorBlock({|oDetErro| MTA010PCPE(oDetErro) })

	Begin Sequence
		RpcSetType(3)
		RpcSetEnv(cEmp, cFil)
		SetFunName("MATA010") //Seta a fun��o inicial para MATA010

		IntegSG1(cProduto,lFantasma)

		//Caso ocorra algum erro
		RECOVER
			GravaErrIn()

	End Sequence

Return Nil

/*/{Protheus.doc} MTA010PCPE
Fun��o para tratativa de erros de execu��o

@type  Function
@author marcelo.neumann
@since 20/08/2019
@version P12
@param oDetErro, Object, Objeto com os detalhes do erro ocorrido
/*/
Function MTA010PCPE(oDetErro)

	LogMsg('MTA010IMRP', 0, 0, 1, '', '', ;
	       Replicate("-",70) + CHR(10) + AllTrim(oDetErro:description) + CHR(10) + AllTrim(oDetErro:ErrorStack) + CHR(10) + Replicate("-",70))
	BREAK
Return

/*/{Protheus.doc} GravaErrIn
Fun��o para setar a falha na integra��o e obrigar rodar a Sincroniza��o de Estruturas

@type  Function
@author marcelo.neumann
@since 22/08/2019
@version P12
@param oDetErro, Object, Objeto com os detalhes do erro ocorrido
/*/
Function GravaErrIn()

	TcSqlExec("UPDATE " + RetSqlName("T4P") + " SET T4P_ALTER = '1' WHERE D_E_L_E_T_ = ' ' AND T4P_API = 'MRPBILLOFMATERIAL'")

Return

/*/{Protheus.doc} IntegraSG1
Fun��o para buscar e integrar as estruturas

@type  Function
@author marcelo.neumann
@since 20/08/2019
@version P12
@param cProduto , Character, C�digo do produto a ser integrado
@param lFantasma, Logical  , Altera��o da propriedade B1_FANTASM
/*/
Static Function IntegSG1(cProduto,lFantasma)

	Local aError    := {}
	Local aSuccess  := {}
	Local cAliasSG1 := GetNextAlias()
	Local oIntegra  := Nil
	Local oTTPend
	Local cQuery    := ""

	cQuery := "SELECT DISTINCT SG1.G1_FILIAL,SG1.G1_COD FROM " + RetSqlName("SG1") + " SG1 "
	cQuery += " WHERE SG1.G1_FILIAL  = '" + xFilial("SG1") + "' "
	cQuery += "   AND SG1.G1_COMP    = '" + cProduto + "' "
	cQuery += "   AND SG1.D_E_L_E_T_ = ' ' "

	If lFantasma
		cQuery += "   AND SG1.G1_FANTASM = ' ' "
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSG1,.T.,.T.)

	If !(cAliasSG1)->(Eof())
		//Cria tempor�rias para a PCPA131INT
		oTTPend  := TTPendMRP()
		oIntegra := JsonObject():New()

		//Carrega o array aDadosInc com os registros a serem integrados
		While (cAliasSG1)->(!Eof())
			oIntegra[(cAliasSG1)->G1_FILIAL+(cAliasSG1)->G1_COD] := 1

			(cAliasSG1)->(dbSkip())
		End

		//Chama a fun��o do PCPA200API para integrar os registros
		PCPA200MRP(Nil, Nil, oTTPend, oIntegra, "INSERT", @aSuccess, @aError)

		//Elimina as tempor�rias usadas na PCPA131INT
		oTTPend:Delete()
		FreeObj(oTTPend)
		FreeObj(oIntegra)
		oIntegra := Nil
	EndIf
	(cAliasSG1)->(dbCloseArea())

Return

/*/{Protheus.doc} A010RFIDGa
Gatilho do campo VK_RFID (dicion�rio)
@type Function
@author marcelo.neumann
@since 20/05/2020
@version P12
@return
/*/
Function A010RFIDGa()

	Local cRFID    := ""
	Local nNextKey := 0
	Local oModel   := FWModelActive()

	If oModel <> Nil
		cRFID    := oModel:GetValue("SVKDETAIL","VK_RFID")
		nNextKey := oModel:GetValue("SVKDETAIL","VK_RFIDKEY")

		If cRFID == "1"
			If Empty(nNextKey)
				nNextKey := GetNextKey(.T.)
				If nNextKey == -1
					oModel:LoadValue("SVKDETAIL","VK_RFID",  "0")
					oModel:LoadValue("SVKDETAIL","VK_RFIDKEY", 0)
				EndIf
			EndIf
		Else
			If !__lHaveKey
				nNextKey := 0
			EndIf
		EndIf
	EndIf

Return nNextKey

/*/{Protheus.doc} A010RFIDFt
Gatilho do campo VK_RFID (dicion�rio) para carga do campo VK_RFIDFAT

@type Function
@author lucas.franca
@since 30/07/2020
@version P12
@return
/*/
Function A010RFIDFt()
	Local cRFID  := ""
	Local nFator := 1
	Local oModel := FWModelActive()

	If oModel <> Nil
		cRFID  := oModel:GetValue("SVKDETAIL","VK_RFID")
		nFator := oModel:GetValue("SVKDETAIL","VK_RFIDFAT")

		If cRFID == "1" .And. Empty(nFator)
			nFator := 1
		EndIf
	EndIf
Return nFator

/*/{Protheus.doc} A010RFIDVa
Fun��o de valida��o do campo VK_RFIDKEY (dicion�rio)
@type Function
@author marcelo.neumann
@since 20/05/2020
@version P12
@return lOk, logical, indica se o RFID Key informado � v�lido
/*/
Function A010RFIDVa()

	Local cProduto := ""
	Local cRFID    := ""
	Local lOk      := .T.
	Local nRFIDKey := 0
	Local oModel   := FWModelActive()

	If oModel <> Nil
		cRFID    := oModel:GetValue("SVKDETAIL", "VK_RFID")
		nRFIDKey := oModel:GetValue("SVKDETAIL", "VK_RFIDKEY")
		cProduto := oModel:GetValue("SB1MASTER", "B1_COD")

		If cRFID == "1"
			If nRFIDKey == 0
				lOk := .F.
				Help( ,  , "Help", ,  STR0007,; //"C�digo RFID n�o pode ser zero."
					 1, 0, , , , , , {STR0006}) //"Informe outro C�digo RFID para esse produto."
			Else
				If KeyExiste(nRFIDKey, cProduto, @cProduto)
					lOk := .F.
					Help( ,  , "Help", ,  STR0004 + cProduto,; //"C�digo RFID j� informado no produto: "
						 1, 0, , , , , , {STR0006})            //"Informe outro C�digo RFID para esse produto."
				EndIf
			EndIf
		EndIf
	EndIf

Return lOk

/*/{Protheus.doc} KeyExiste
Verifica se o RFID Key informado j� est� cadastrado em outro produto
@type Static Function
@author marcelo.neumann
@since 20/05/2020
@version P12
@param 01 nRFIDKey  , Numeric  , c�digo RFID a ser verificado
@param 02 cProdAtu  , Character, produto a ser desconsiderado na busca
@param 02 cProdExist, Character, produto que possui o RFID Key (refer�ncia)
@return   lExiste   , Logical  , indica se o c�digo j� existe para outro produto
/*/
Static Function KeyExiste(nRFIDKey, cProdAtu, cProdExist)

	Local cAliasQry  := GetNextAlias()
	Local lExiste    := .F.
	Local cQuery     := ""
	Default cProdAtu := ""

	cQuery := "%" + RetSqlName("SVK")                        + ;
			  " WHERE VK_FILIAL  = '" + xFilial("SVK") + "'" + ;
				" AND VK_RFIDKEY = "  + cValToChar(nRFIDKey)

	If !Empty(cProdAtu)
		cQuery += " AND VK_COD <> '" + cProdAtu + "'"
	EndIf

	cQuery += " AND D_E_L_E_T_ = ' '%"

	BeginSql Alias cAliasQry
		SELECT VK_COD
		  FROM %Exp:cQuery%
    EndSql

	If !(cAliasQry)->(Eof()) .And. !Empty((cAliasQry)->VK_COD)
		lExiste  := .T.
		cProdExist := Trim((cAliasQry)->VK_COD)
	EndIf
	(cAliasQry)->(dbCloseArea())

Return lExiste

/*/{Protheus.doc} A010RFIDWh
Fun��o WHEN do campo VK_RFIDKEY
@type Function
@author marcelo.neumann
@since 20/05/2020
@version P12
@return lPermite, logical, indica se o campo deve estar habilitado ou n�o
/*/
Function A010RFIDWh()

	Local lPermite := .F.
	Local oModel   := Nil

	//S� permitir� alterar se n�o tiver sido informado ainda
	If !__lHaveKey
		oModel := FWModelActive()
		//Habilita o campo somente quando o VK_RFID estiver como "Sim"
		If oModel <> Nil .And. oModel:GetValue("SVKDETAIL","VK_RFID") == "1"
			lPermite := .T.
		Else
			lPermite := .F.
		EndIF
	EndIf

Return lPermite

/*/{Protheus.doc} GetNextKey
Fun��o para buscar o pr�ximo c�digo RFID
@type Static Function
@author marcelo.neumann
@since 20/05/2020
@version P12
@param lExibeMsg, logical, indica se deve exibir o Help
@return nNextKey, numeric, pr�ximo RFID Key v�lido
/*/
Static Function GetNextKey(lExibeMsg)

	Local cAliasQry := GetNextAlias()
	Local nNextKey  := 0

    BeginSql Alias cAliasQry
		SELECT MAX(SVK.VK_RFIDKEY) MAXKEY
		  FROM %Table:SVK% SVK
		 WHERE SVK.VK_FILIAL  = %xfilial:SVK%
		   AND SVK.%NotDel%
    EndSql

	If !(cAliasQry)->(Eof())
		nNextKey := (cAliasQry)->MAXKEY + 1
	EndIf
	(cAliasQry)->(dbCloseArea())

	If nNextKey > 9999999999999
		nNextKey := 1
		While KeyExiste(nNextKey)
			If nNextKey == 9999999999999
				nNextKey := -1
				If lExibeMsg
					HelpInDark(.F.)
					Help( , , 'Help', , STR0005, 1, 0) //"N�o existe mais numera��o v�lida para o RFID."
					HelpInDark(.T.)
				EndIf
				Exit
			EndIf
			nNextKey++
		End
	EndIf

Return nNextKey
