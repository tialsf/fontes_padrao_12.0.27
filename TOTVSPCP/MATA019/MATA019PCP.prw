#INCLUDE 'TOTVS.ch'
#INCLUDE 'FWMVCDef.ch'
#INCLUDE 'MATA019PCP.ch'

/*---------------------------------------------------------------------------------------------/
{Protheus.doc} MATA019PCP
Eventos relacionadas �s regras de neg�cio do SIGAPCP.

Todas as valida��es de modelo, linha, pr� e pos, tamb�m todas as intera��es com a grava��o
s�o definidas nessa classe.

Documenta��o sobre eventos do MVC: http://tdn.totvs.com/pages/viewpage.action?pageId=269552294

@type classe

@author Ricardo Prandi
@since 27/08/2019
@version P12.1.27
/---------------------------------------------------------------------------------------------*/
CLASS MATA019PCP FROM FWModelEvent

	DATA cIDSBZ

	METHOD New() CONSTRUCTOR

	METHOD GridLinePosVld()
	METHOD GridLinePreVld()

ENDCLASS

/*----------------------------------------/
{Protheus.doc} New
M�todo construtor da classe.

@type metodo

@author Ricardo Prandi
@since 27/08/2019
@version P12.1.27
/-----------------------------------------*/
METHOD New(oModel,cIDSBZ) CLASS MATA019PCP

	Default cIDSBZ := "SBZDETAIL"

	::cIDSBZ := cIDSBZ

	oModel:InstallEvent("MATA019API",,MATA019API():New())
	oModel:InstallEvent("MATA019API",,MATA019NET():New())

Return

/*----------------------------------------------------------------/
{Protheus.doc} GridLinePosVld
Valida��es do MVC quando ocorrer as a��es de pos valida��o da linha
do Grid

@type metodo

@author Ricardo Prandi
@since 27/08/2019
@version P12.1.27
/----------------------------------------------------------------*/
METHOD GridLinePosVld(oSubModel, cID, nLine) CLASS MATA019PCP

	Local lRet    := .T.
	Local nOpc    := oSubModel:GetOperation()
	Local cFilSVE := ""

	If cID == "SVEDETAIL"
		cFilSVE := oSubModel:GetValue("VE_FILIAL")

		If !Empty(cFilSVE)
			cFilAnt := cFilSVE
		EndIf

		If !Empty(oSubModel:GetValue("VE_FILIAL")) .And. !Empty(oSubModel:GetValue("VE_LOCGRP"))
			lRet := ExistCpo("SVN",oSubModel:GetValue("VE_LOCGRP"))
		ElseIf Empty(oSubModel:GetValue("VE_FILIAL"))
			Help( ,, 'Help',, STR0001, 1, 0 ) //O campo filial deve ser preenchido
			lRet := .F.
		ElseIf Empty(oSubModel:GetValue("VE_LOCGRP"))
			Help( ,, 'Help',, STR0002, 1, 0 ) //O campo Armaz�m deve ser preenchido
			lRet := .F.
		Endif

		If lRet .And. !Empty(cFilSVE)
			lRet := ExistChav("SM0",oSubModel:GetValue("VE_FILIAL"))
		EndIf

		If lRet .And. oSubModel:GetValue("VE_HORFIX") <> 0
			If lRet .And. Alltrim(oSubModel:GetValue("VE_TPHOFIX")) == ""
				Help(" ",1,"M010HORFIX")
				lRet := .F.
			EndIf
		EndIf
	EndIf

	If lRet .And. cID == ::cIDSBZ .And. (nOpc == MODEL_OPERATION_INSERT .Or. nOpc == MODEL_OPERATION_UPDATE)
		If oSubModel:HasField("BZ_HORFIX") .And. oSubModel:HasField("BZ_TPHOFIX")
			If oSubModel:GetValue("BZ_HORFIX") <> 0 .And. Alltrim(oSubModel:GetValue("BZ_TPHOFIX")) == ""
				Help(" ",1,"M010HORFIX")
				lRet := .F.
			EndIf
		EndIf
	EndIf

Return lRet

/*----------------------------------------------------------------------------------------------------/
{Protheus.doc} GridLinePreVld
Valida��es do MVC quando ocorrer as a��es de pre valida��o da linha do Grid

@type metodo

@author Ricardo Prandi
@since 27/08/2019
@version P12.1.27

/----------------------------------------------------------------------------------------------------*/
METHOD GridLinePreVld(oSubModel, cID, nLine, cAction, cFieldID, xValue, xCurrentValue) CLASS MATA019PCP

	Local lRet    := .T.
	Local cFilSVE := ""

	If cID == "SVEDETAIL"
		cFilSVE := oSubModel:GetValue("VE_FILIAL")

		If !Empty(cFilSVE)
			cFilAnt := cFilSVE
		EndIf

		If cAction == "SETVALUE" .And. cFieldID == "VE_FILIAL"
			cFilSVE := xValue

			If Empty(cFilSVE)
				Help( ,, 'Help',, STR0001, 1, 0 ) //"O campo filial deve ser preenchido"
				lRet := .F.
			ElseIf !FWFilExist(,cFilSVE)
				Help( ,, 'Help',, STR0003, 1, 0 ) //Filial inexistente
				lRet := .F.
			EndIf

			If lRet
				cFilAnt := cFilSVE
			EndIf
		EndIf
	EndIf

Return lRet

/*------------------------------------------------------------/
{Protheus.doc} APCPModel
(Acrescenta no modelo do MATA019 os campos espec�ficos do PCP)
@type  Function
@author Ricardo Prandi
@since 28/08/2019
@param oModel, Objeto, Modelo do programa
@return L�gico
/-------------------------------------------------------------*/
Function APCPModel(oModel)

	Local oStruSVE := FWFormStruct(1,"SVE",,.F.)

	oStruSVE:AddTrigger("VE_LOCGRP", "VE_LOCGRPD", {|| .T. }, {|| l019Desc()})
	oStruSVE:SetProperty("VE_PRODUTO" , MODEL_FIELD_OBRIGAT,.F.)
	oStruSVE:SetProperty("VE_PRODUTO" , MODEL_FIELD_OBRIGAT,.F.)

	oModel:AddGrid("SVEDETAIL","SB1MASTER",oStruSVE)
	oModel:SetRelation('SVEDETAIL', { { 'VE_PRODUTO', 'B1_COD' } }, SVE->(IndexKey(1)) )

	oModel:GetModel("SVEDETAIL"):SetUniqueLine({"VE_FILIAL","VE_LOCGRP"})
	oModel:GetModel('SVEDETAIL'):SetOptional(.T.)

	oStruSVE:SetProperty( 'VE_LOCGRP' , MODEL_FIELD_OBRIGAT)

Return .T.

/*------------------------------------------------------------/
{Protheus.doc} APCPView
(Acrescenta na View do MATA019 os campos espec�ficos do PCP)
@type  Function
@author Ricardo Prandi
@since 28/08/2019
@return L�gico
/-------------------------------------------------------------*/
Function APCPView()

	Local oStrSVE:= FWFormStruct(2, 'SVE')

	oStrSVE:AddField("VE_FILIAL"			,;	// [01]  C   Nome do Campo
	   				 "00"					,;	// [02]  C   Ordem
					 FWX3Titulo("VE_FILIAL"),;	// [03]  C   Titulo do campo//"C�digo"
					 FWX3Titulo("VE_FILIAL"),;	// [04]  C   Descricao do campo//"C�digo"
					 NIL					,;	// [05]  A   Array com Help
					 "C"					,;	// [06]  C   Tipo do campo
					 ""						,;	// [07]  C   Picture
					 NIL					,;	// [08]  B   Bloco de Picture Var
					 "SM0"					,;	// [09]  C   Consulta F3
					 .T.					,;	// [10]  L   Indica se o campo � alteravel
					 NIL					,;	// [11]  C   Pasta do campo
					 NIL					,;	// [12]  C   Agrupamento do campo
					 NIL					,;	// [13]  A   Lista de valores permitido do campo (Combo)
					 NIL					,;	// [14]  N   Tamanho maximo da maior op��o do combo
					 NIL					,;	// [15]  C   Inicializador de Browse
					 .F.					,;	// [16]  L   Indica se o campo � virtual
					 NIL					,;	// [17]  C   Picture Variavel
					 NIL					)	// [18]  L   Indica pulo de linha ap�s o campo

	oStrSVE:SetProperty("VE_FILIAL" , MODEL_FIELD_OBRIGAT ,.T.)
	oStrSVE:SetProperty("VE_LOCGRP" , MODEL_FIELD_OBRIGAT ,.T.)

	oStrSVE:RemoveField('VE_PRODUTO')

Return oStrSVE

//--------------------------------------------------------------------
/*/{Protheus.doc} l019Desc()
Funcao para preencher o campo virtual de descricao do armazem
@author Douglas Heydt
@since 11/06/2018
@version 1.0
@return lRet
/*/
//--------------------------------------------------------------------
Function l019Desc()

	Local aArea 	:= GetArea()
	Local cFilSVN	:= xFilial("SVN")
	Local cCodSVN  := &(READVAR())
	Local cDescri

	If !Empty(cCodSVN)
		dbSelectArea("SVN")
		dbSetOrder(1)

		If dbSeek( cFilSVN+cCodSVN)
			cDescri := Alltrim(SVN->VN_DESCGRU)
		EndIf
	EndIf

	RestArea(aArea)

Return cDescri

//--------------------------------------------------------------------
/*/{Protheus.doc} A019SFLSVE
Muda filial corrente na mudanca de linha
@author Douglas Heydt
@since 05/06/2018
@version P12.1.17
/*/
//--------------------------------------------------------------------
Function A019SFLSVE(oView)

	Local cFilSVE := oView:GetModel():GetValue("SVEDETAIL", "VE_FILIAL")

	If !Empty(cFilSVE)
		cFilAnt := cFilSVE
	EndIf

Return