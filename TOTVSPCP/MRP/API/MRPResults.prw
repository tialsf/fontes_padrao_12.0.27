#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "mrpresults.CH"

Static _aMapMat := MapFields(1)
Static _aMapRas := MapFields(2)
Static _aMapPar := MapFields(3)
Static _aMapMtP := MapFields(4)
Static _aMapPcd := MapFields(5)
Static _aTamQtd := TamSX3("HWG_QTEMPE")
Static _nTamDoc := TamSX3("HWC_DOCERP")[1]
Static _nTamTipo := TamSX3("HWC_TDCERP")[1]

#DEFINE LISTA_RESULTADOS_SUMARIZADOS "Matriz"
#DEFINE LISTA_RESULTADOS_RASTREADOS  "Rastreio"
#DEFINE LISTA_PARAMETROS_MRP         "Parametros"

#DEFINE LISTA_RESULTADOS_SUMARIZADOS_PRODUTO "Matriz_Produto"
#DEFINE LISTA_PROCESSAMENTOS_MRP "Processed_Mrp"

/*/{Protheus.doc} mrpresults
API de integracao de Resultados do MRP

@type  WSCLASS
@author renan.roeder
@since 31/07/2019
@version P12.1.27
/*/
WSRESTFUL mrpresults DESCRIPTION STR0001 FORMAT APPLICATION_JSON //"Resultados do MRP"
	WSDATA branchId      		AS STRING  OPTIONAL
	WSDATA ticket        		AS STRING  OPTIONAL
	WSDATA product       		AS STRING  OPTIONAL
	WSDATA optionalId    		AS STRING  OPTIONAL
	WSDATA necessityDate 		AS STRING  OPTIONAL
	WSDATA parentDocumentType 	AS STRING  OPTIONAL
	WSDATA parentDocument 		AS STRING  OPTIONAL
	WSDATA sequenceInStructure 	AS STRING  OPTIONAL
	WSDATA substitutionKey 		AS STRING  OPTIONAL
	WSDATA recStatus 			AS STRING  OPTIONAL
	WSDATA breakupSeq 			AS INTEGER OPTIONAL
	WSDATA erpDoc				AS STRING  OPTIONAL
	WSDATA erpDocType			AS STRING  OPTIONAL
	WSDATA parentDocT			AS STRING  OPTIONAL
	WSDATA parentDoc			AS STRING  OPTIONAL
	WSDATA seqInStru			AS STRING  OPTIONAL
	WSDATA compCode				AS STRING  OPTIONAL
	WSDATA recordKey			AS STRING  OPTIONAL
	WSDATA substKey				AS STRING  OPTIONAL
	WSDATA docMrp				AS STRING  OPTIONAL
	WSDATA Fields        		AS STRING  OPTIONAL
	WSDATA Order         		AS STRING  OPTIONAL
	WSDATA Page          		AS INTEGER OPTIONAL
	WSDATA PageSize      		AS INTEGER OPTIONAL

	WSMETHOD GET SUM;
		DESCRIPTION STR0002; //"Retorna os resultados sumarizados de uma execu��o do MRP"
		WSSYNTAX "api/pcp/v1/mrpresults/summarized/{branchId}/{ticket}" ;
		PATH "/api/pcp/v1/mrpresults/summarized/{branchId}/{ticket}" ;
		TTALK "v1"

	WSMETHOD GET OPC;
		DESCRIPTION STR0012; //"Retorna os dados do opcional"
		WSSYNTAX "api/pcp/v1/mrpresults/optional/{branchId}/{ticket}" ;
		PATH "/api/pcp/v1/mrpresults/optional/{branchId}/{ticket}" ;
		TTALK "v1"

	WSMETHOD GET PARAM;
		DESCRIPTION STR0003; //"Retorna os parametros utilizados em uma execu��o do MRP"
		WSSYNTAX "api/pcp/v1/mrpresults/parameters/{branchId}/{ticket}" ;
		PATH "api/pcp/v1/mrpresults/parameters/{branchId}/{ticket}" ;
		TTALK "v1"

	WSMETHOD GET SUMPROD;
		DESCRIPTION STR0004; //"Retorna os resultados sumarizados de um produto em uma execu��o do MRP"
		WSSYNTAX "api/pcp/v1/mrpresults/summarized/{branchId}/{ticket}/{product}" ;
		PATH "/api/pcp/v1/mrpresults/summarized/{branchId}/{ticket}/{product}" ;
		TTALK "v1"

	WSMETHOD GET TRAPDDT;
		DESCRIPTION STR0005; //"Retorna os resultados rastreados de uma execu��o do MRP para um produto e data espec�ficos"
		WSSYNTAX "api/pcp/v1/mrpresults/traced/{branchId}/{ticket}/{product}/{necessityDate}" ;
		PATH "api/pcp/v1/mrpresults/traced/{branchId}/{ticket}/{product}/{necessityDate}" ;
		TTALK "v1"

	WSMETHOD GET SUBSTITUTES;
		DESCRIPTION "Retorna os resultados rastreados referente uma substitui��o espec�fica"; //"Retorna os resultados rastreados de uma execu��o do MRP para um produto e data espec�ficos"
		WSSYNTAX "api/pcp/v1/mrpresults/substitutes/{branchId}/{ticket}/{parentDocumentType}/{parentDocument}/{sequenceInStructure}/{substitutionKey}" ;
		PATH "api/pcp/v1/mrpresults/substitutes/{branchId}/{ticket}/{parentDocumentType}/{parentDocument}/{sequenceInStructure}/{substitutionKey}" ;
		TTALK "v1"

	WSMETHOD GET PROCESS;
		DESCRIPTION STR0006; //"Retorna todos os processamentos do MRP"
		WSSYNTAX "api/pcp/v1/mrpresults/processed/{branchId}" ;
		PATH "api/pcp/v1/mrpresults/processed/{branchId}" ;
		TTALK "v1"

	WSMETHOD GET TICKET;
		DESCRIPTION "Retorna informa��es de um processamento do MRP." ;
		WSSYNTAX "api/pcp/v1/mrpresults/processed/{branchId}/{ticket}" ;
		PATH "api/pcp/v1/mrpresults/processed/{branchId}/{ticket}" ;
		TTALK "v1"

	WSMETHOD GET PROCREGS;
		DESCRIPTION STR0007; //"Retorna registros que devem ser processados"
		WSSYNTAX "api/pcp/v1/mrpresults/process/{ticket}" ;
		PATH "api/pcp/v1/mrpresults/process/{ticket}" ;
		TTALK "v1"

	WSMETHOD POST RASTREIO;
		DESCRIPTION STR0008; //"Faz a atualiza��o do status de um registro de rastreio."
		WSSYNTAX "api/pcp/v1/mrpresults/tracking/{ticket}/{recStatus}/{erpDocument}/{erpDocType}/{erpDoc}/{parentDoc}/{seqInStru}/{compCode}/{recordKey}/{substKey}/{breakupSeq}" ;
		PATH "api/pcp/v1/mrpresults/tracking/{ticket}/{recStatus}/{erpDocument}/{erpDocType}/{erpDoc}/{parentDoc}/{seqInStru}/{compCode}/{recordKey}/{substKey}/{breakupSeq}" ;
		TTALK "v1"

	WSMETHOD GET AGLUTINADO;
		DESCRIPTION STR0009; //"Consulta um documento do MRP na tabela de documentos aglutinados."
		WSSYNTAX "api/pcp/v1/mrpresults/aglutinado/{ticket}/{erpDoc}/{compCode}" ;
		PATH "api/pcp/v1/mrpresults/aglutinado/{ticket}/{erpDoc}/{compCode}" ;
		TTALK "v1"

	WSMETHOD GET ORDSUBS;
		DESCRIPTION STR0010;//"Busca dados do componente original de ordem de substitui��o"
		WSSYNTAX "api/pcp/v1/mrpresults/ordsubs/{ticket}/{recordKey}/{parentDoc}" ;
		PATH "api/pcp/v1/mrpresults/ordsubs/{ticket}/{recordKey}/{parentDoc}" ;
		TTALK "v1"

	WSMETHOD GET PRODORIG;
		DESCRIPTION STR0011; //"Retorna o produto origem de um empenho de alternativo"
		WSSYNTAX "api/pcp/v1/mrpresults/prodorig/{ticket}/{recordKey}/{parentDoc}" ;
		PATH "api/pcp/v1/mrpresults/prodorig/{ticket}/{recordKey}/{parentDoc}" ;
		TTALK "v1"

	WSMETHOD POST CLEAR;
		DESCRIPTION STR0013; //"Efetua limpeza de base de processamento do MRP"
		WSSYNTAX "api/pcp/v1/mrpresults/clear" ;
		PATH "/api/pcp/v1/mrpresults/clear" ;
		TTALK "v1"

	WSMETHOD GET EVENTS;
		DESCRIPTION STR0015; //"Consulta eventos do ticket";
		WSSYNTAX "api/pcp/v1/mrpresults/events/{ticket}" ;
		PATH "api/pcp/v1/mrpresults/events/{ticket}" ;
		TTALK "v1"

ENDWSRESTFUL

/*/{Protheus.doc} GET SUM api/pcp/v1/mrpresults/summarized/{branchId}/{ticket}
Retorna o resultado sumarizado de uma execu��o do MRP

@type  WSMETHOD
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param	branchId, Character, Codigo da filial para fazer a pesquisa
@param	ticket  , Character, Codigo �nico do processo para fazer a pesquisa.
@param	Fields  , Character, Campos que ser�o retornados no GET.
@return lRet    , Logico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET SUM PATHPARAM branchId, ticket QUERYPARAM Fields,Order,Page,PageSize WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetSum(Self:branchId, Self:ticket, Self:Fields, Self:Order, Self:Page, Self:PageSize, LISTA_RESULTADOS_SUMARIZADOS)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet

/*/{Protheus.doc} MrpGetSum
Funcao para disparar as acoes da API de Resultados do MRP, para o metodo GET dos resultados sumarizados.

@type  Function
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param cBranch   , Caracter, Codigo da filial
@param cTicket   , Caracter, Codigo �nico do processo
@param cFields   , Caracter, Campos que devem ser retornados.
@param cOrder    , Caracter, Ordena��o do retorno da consulta.
@param cPage     , Caracter, P�gina de retorno.
@param cPageSize , Caracter, Tamanho da p�gina.
@param cMap      , Mapa dos campos utilizados na consulta.
@return aReturn, Array, Array com as informacoes da requisicao.
                        aReturn[1] - Logico    - Indica se a requisicao foi processada com sucesso ou Nao.
						aReturn[2] - Character - JSON com o resultado da requisicao, ou com a mensagem de erro.
						aReturn[3] - Numeric   - Codigo de erro identificado pelo MRPApi.
/*/
Function MrpGetSum(cBranch, cTicket, cFields, cOrder, cPage, cPageSize, cMap)
	Local aReturn   := {}
	Local aQryParam := {}

	//Adiciona os filtros de filial e produto como QueryParam.
	aAdd(aQryParam, {"BRANCHID", cBranch})
	aAdd(aQryParam, {"TICKET"  , cTicket})

	//Chama a funcao para retornar os dados.
	aReturn := GetResults(.T., aQryParam, cOrder, cPage, cPageSize, cFields, cMap)

Return aReturn

/*/{Protheus.doc} GET OPC api/pcp/v1/mrpresults/optional/{branchId}/{ticket}
Retorna os dados do opcional

@type  WSMETHOD
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param	branchId   , Character, Codigo da filial para fazer a pesquisa
@param	ticket     , Character, Codigo �nico do processo para fazer a pesquisa.
@param	cOptionalID, Character, C�digo do ID do Opcional
@param	Fields  , Character, Campos que ser�o retornados no GET.
@return lRet    , Logico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET OPC PATHPARAM branchId, ticket, cOptionalID QUERYPARAM Fields,Order,Page,PageSize WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetOPC(Self:branchId, Self:ticket, Self:cOptionalID)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet

/*/{Protheus.doc} MrpGetOPC
Funcao para disparar as acoes da API de Resultados do MRP, para o metodo GET do memo e string de opcional.

@type  Function
@author brunno.costa
@since 20/11/2019
@version P12.1.27
@param cBranch    , Caracter, Codigo da filial
@param cTicket    , Caracter, Codigo �nico do processo
@param cProduto   , Caracter, Codigo �nico do processo
@param cOptionalID, Caracter, Codigo �nico do processo
@return aReturn, Array, Array com as informacoes da requisicao.
                        aReturn[1] - Logico    - Indica se a requisicao foi processada com sucesso ou Nao.
						aReturn[2] - Character - JSON com o resultado da requisicao, ou com a mensagem de erro.
						aReturn[3] - Numeric   - Codigo de erro identificado pelo MRPApi.
/*/
Function MrpGetOPC(cBranch, cTicket, cOptionalID)

	Local aReturn   := {}
	Local aJson     := JsonObject():New()
	Local cAliasQry := GetNextAlias()

	BeginSql Alias cAliasQry
		SELECT HWD_ERPOPC,
			   HWD_OPCION,
			   HWD_ERPMOP
		  FROM %Table:HWD%
		 WHERE HWD_TICKET = %Exp:cTicket%
		   AND HWD_KEYMAT = %Exp:cOptionalID%
		   AND HWD_FILIAL = %Exp:cBranch%
		   AND %NotDel%
	EndSql

	If (cAliasQry)->(!Eof())
		aJson["optionalMemo"]     := STR2Array((cAliasQry)->HWD_ERPMOP, .F.)
		aJson["optionalString"]   := (cAliasQry)->HWD_ERPOPC
		aJson["optionalSelected"] := Iif(Empty((cAliasQry)->HWD_OPCION), (cAliasQry)->HWD_ERPOPC, (cAliasQry)->HWD_OPCION)

		aAdd(aReturn, .T.)
		aAdd(aReturn, aJson:toJSON())
		aAdd(aReturn, 200)
	Else
		aJson["optionalMemo"]     := ""
		aJson["optionalString"]   := ""
		aJson["optionalSelected"] := ""

		aAdd(aReturn, .F.)
		aAdd(aReturn, aJson:toJSON())
		aAdd(aReturn, 400)
	End
	(cAliasQry)->(dbCloseArea())

Return aReturn

/*/{Protheus.doc} GET PARAM api/pcp/v1/mrpresults/parameters/{branchId}/{ticket}
Retorna os parametros de uma execu��o do MRP

@type  WSMETHOD
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param	branchId , Character, Codigo da filial para fazer a pesquisa
@param	ticket   , Character, Codigo �nico do processo para fazer a pesquisa.
@param	Fields   , Character, Campos que ser�o retornados no GET.
@param  cOrder   , Caracter, Ordena��o do retorno da consulta.
@param  cPage    , Caracter, P�gina de retorno.
@param  cPageSize, Caracter, Tamanho da p�gina.
@return lRet    , Logico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET PARAM PATHPARAM branchId, ticket QUERYPARAM Fields, Order, Page, PageSize WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetPar(Self:branchId, Self:ticket, Self:Fields, Self:Order, Self:Page, Self:PageSize, LISTA_PARAMETROS_MRP)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet

/*/{Protheus.doc} MrpGetPar
Funcao para disparar as acoes da API de Resultados do MRP, para o metodo GET dos parametros do MRP.

@type  Function
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param cBranch , Caracter, Codigo da filial
@param cTicket , Caracter, Codigo �nico do processo
@param cFields , Caracter, Campos que devem ser retornados.
@param cOrder    , Caracter, Ordena��o do retorno da consulta.
@param cPage     , Caracter, P�gina de retorno.
@param cPageSize , Caracter, Tamanho da p�gina.
@param cMap      , Mapa dos campos utilizados na consulta.
@return aReturn, Array, Array com as informacoes da requisicao.
                        aReturn[1] - Logico    - Indica se a requisicao foi processada com sucesso ou Nao.
						aReturn[2] - Character - JSON com o resultado da requisicao, ou com a mensagem de erro.
						aReturn[3] - Numeric   - Codigo de erro identificado pelo MRPApi.
/*/
Function MrpGetPar(cBranch, cTicket, cFields, cOrder, cPage, cPageSize, cMap)
	Local aReturn   := {}
	Local aQryParam := {}

	//Adiciona os filtros de filial e produto como QueryParam.
	aAdd(aQryParam, {"BRANCHID", cBranch})
	aAdd(aQryParam, {"TICKET"  , cTicket})

	//Chama a funcao para retornar os dados.
	aReturn := GetResults(.T., aQryParam, cOrder, cPage, cPageSize, cFields, cMap)

Return aReturn

/*/{Protheus.doc} GET SUMPROD api/pcp/v1/mrpresults/summarized/{branchId}/{ticket}/{product}
Retorna o resultado sumarizado de um produto em uma execu��o do MRP

@type  WSMETHOD
@author renan.roeder
@since 22/11/2019
@version P12.1.27
@param	branchId, Character, Codigo da filial para fazer a pesquisa
@param	ticket  , Character, Codigo �nico do processo para fazer a pesquisa.
@param	Fields  , Character, Campos que ser�o retornados no GET.
@param cOrder   , Caracter, Ordena��o do retorno da consulta.
@param cPage    , Caracter, P�gina de retorno.
@param cPageSize, Caracter, Tamanho da p�gina.
@return lRet    , Logico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET SUMPROD PATHPARAM branchId, ticket, product, optionalId QUERYPARAM Fields,Order,Page,PageSize WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetSPd(Self:branchId, Self:ticket, Self:product, Self:optionalId, Self:Fields, Self:Order, Self:Page, Self:PageSize, LISTA_RESULTADOS_SUMARIZADOS)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet

/*/{Protheus.doc} MrpGetSPd
Funcao para disparar as acoes da API de Resultados do MRP, para o metodo GET dos resultados sumarizados.

@type  Function
@author renan.roeder
@since 22/11/2019
@version P12.1.27
@param cBranch    , Caracter, Codigo da filial
@param cTicket    , Caracter, Codigo �nico do processo
@param cProduct   , Caracter, C�digo do produto
@param cOptionalId, Caracter, ID do Opcional
@param cFields    , Caracter, Campos que devem ser retornados.
@param cOrder     , Caracter, Ordena��o do retorno da consulta.
@param cPage      , Caracter, P�gina de retorno.
@param cPageSize  , Caracter, Tamanho da p�gina.
@param cMap       , Mapa dos campos utilizados na consulta.
@return aReturn, Array, Array com as informacoes da requisicao.
                        aReturn[1] - Logico    - Indica se a requisicao foi processada com sucesso ou Nao.
						aReturn[2] - Character - JSON com o resultado da requisicao, ou com a mensagem de erro.
						aReturn[3] - Numeric   - Codigo de erro identificado pelo MRPApi.
/*/
Function MrpGetSPd(cBranch, cTicket, cProduct, cOptionalId, cFields, cOrder, cPage, cPageSize, cMap)
	Local aReturn   := {}
	Local aQryParam := {}

	//Adiciona os filtros de filial e produto como QueryParam.
	aAdd(aQryParam, {"BRANCHID"  , cBranch})
	aAdd(aQryParam, {"TICKET"    , cTicket})
	aAdd(aQryParam, {"PRODUCT"   , cProduct})
	aAdd(aQryParam, {"OPTIONALID", cOptionalId})

	//Chama a funcao para retornar os dados.
	aReturn := GetResults(.T., aQryParam, cOrder, cPage, cPageSize, cFields, cMap)

Return aReturn

/*/{Protheus.doc} GET TRACED api/pcp/v1/mrpresults/traced/{branchId}/{ticket}
Retorna o resultado rastreado de uma execu��o do MRP

@type  WSMETHOD
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param	branchId, Character, Codigo da filial para fazer a pesquisa
@param	ticket  , Character, Codigo �nico do processo para fazer a pesquisa.
@param	Fields  , Character, Campos que ser�o retornados no GET.
@return lRet    , Logico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET TRAPDDT PATHPARAM branchId, ticket, product, necessityDate QUERYPARAM Fields,Order,Page,PageSize WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetTPD(Self:branchId, Self:ticket, Self:product, Self:necessityDate, Self:Fields, Self:Order, Self:Page, Self:PageSize, LISTA_RESULTADOS_RASTREADOS)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet

/*/{Protheus.doc} MrpGetTPD
Funcao para disparar as acoes da API de Resultados do MRP, para o metodo GET dos resultados rastreados.

@type  Function
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param cBranch    , Caracter, Codigo da filial
@param cTicket    , Caracter, Codigo �nico do processo
@param cProduct   , Caracter, C�digo doproduto
@param cOptionalID, Caracter, C�digo do opcional do produto
@param cFields    , Caracter, Campos que devem ser retornados.
@return aReturn, Array, Array com as informacoes da requisicao.
                        aReturn[1] - Logico    - Indica se a requisicao foi processada com sucesso ou Nao.
						aReturn[2] - Character - JSON com o resultado da requisicao, ou com a mensagem de erro.
						aReturn[3] - Numeric   - Codigo de erro identificado pelo MRPApi.
/*/
Function MrpGetTPD(cBranch, cTicket, cProduct, cOptionalID, dNecessityDate, cFields, cOrder, cPage, cPageSize, cMap)
	Local aReturn   := {}
	Local aQryParam := {}

	//Adiciona os filtros como QueryParam.
	aAdd(aQryParam, {"BRANCHID"      , cBranch})
	aAdd(aQryParam, {"TICKET"        , cTicket})
	aAdd(aQryParam, {"COMPONENTCODE" , cProduct})
	aAdd(aQryParam, {"OPTIONALID"    , cOptionalID})
	aAdd(aQryParam, {"NECESSITYDATE" , dToS(dNecessityDate)})

	//Chama a funcao para retornar os dados.
	aReturn := GetResults(.T., aQryParam, cOrder, cPage, cPageSize, cFields, cMap)

Return aReturn

/*/{Protheus.doc} GET SUBSTITUTES api/pcp/v1/mrpresults/substitutes/{branchId}/{ticket}/{parentDocumentType}/{parentDocument}/{sequenceInStructure}/{substitutionKey}
Retorna os resultados rastreados referente uma substitui��o espec�fica

@type  WSMETHOD
@author brunno.costa
@since 05/12/2019
@version P12.1.27
@param	branchId           , Character, Codigo da filial para fazer a pesquisa
@param	ticket             , Character, Codigo �nico do processo para fazer a pesquisa.
@param	parentDocumentType , Character, Tipo do documento pai
@param	parentDocument     , Character, Documento pai
@param	sequenceInStructure, Character, Sequ�ncia TRT do produto
@param	substitutionKey    , Character, Chave de substitui��o
@return lRet    , Logico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET SUBSTITUTES PATHPARAM branchId, ticket, parentDocumentType, parentDocument, sequenceInStructure, substitutionKey QUERYPARAM Fields,Order,Page,PageSize WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetSUB(Self:branchId, Self:ticket, Self:parentDocumentType, Self:parentDocument, Self:sequenceInStructure, Self:substitutionKey, Self:Fields, Self:Order, Self:Page, Self:PageSize, LISTA_RESULTADOS_RASTREADOS)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet

/*/{Protheus.doc} MrpGetSUB
Funcao para disparar as acoes da API de Resultados do MRP, para o metodo GET dos resultados rastreados.

@type  Function
@author brunno.costa
@since 05/12/2019
@version P12.1.27
@param cBranch    , Caracter, Codigo da filial
@param cTicket    , Caracter, Codigo �nico do processo
@param cTpDocPai  , Caracter, Tipo do documento pai
@param cDocumento , Caracter, Documento pai
@param cTRT       , Caracter, Sequ�ncia TRT do produto
@param cChave     , Caracter, Chave de substitui��o
@param cFields    , Caracter, Campos que devem ser retornados.
@return aReturn, Array, Array com as informacoes da requisicao.
                        aReturn[1] - Logico   - Indica se a requisicao foi processada com sucesso ou Nao.
						aReturn[2] - Caracter - JSON com o resultado da requisicao, ou com a mensagem de erro.
						aReturn[3] - Numeric  - Codigo de erro identificado pelo MRPApi.
/*/
Function MrpGetSUB(cBranch, cTicket, cTpDocPai, cDocumento, cTRT, cChave, cFields, cOrder, cPage, cPageSize, cMap)
	Local aReturn   := {}
	Local aQryParam := {}

	//Adiciona os filtros como QueryParam.
	aAdd(aQryParam, {"BRANCHID"           , cBranch   })
	aAdd(aQryParam, {"TICKET"             , cTicket   })
	aAdd(aQryParam, {"parentDocumentType" , cTpDocPai })
	aAdd(aQryParam, {"parentDocument"     , cDocumento})
	aAdd(aQryParam, {"sequenceInStructure", cTRT      })
	aAdd(aQryParam, {"substitutionKey"    , cChave    })

	//Chama a funcao para retornar os dados.
	aReturn := GetResults(.T., aQryParam, cOrder, cPage, cPageSize, cFields, cMap)

Return aReturn

/*/{Protheus.doc} GET PROCESS api/pcp/v1/processed
Retorna todos os processamentos do MRP.

@type  WSMETHOD
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param	branchId, Character, Codigo da filial para fazer a pesquisa
@param	ticket  , Character, Codigo �nico do processo para fazer a pesquisa.
@param	Fields  , Character, Campos que ser�o retornados no GET.
@return lRet    , Logico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET PROCESS PATHPARAM branchId QUERYPARAM Fields,Order,Page,PageSize WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetAPcd(Self:branchId, Self:Fields, Self:Order, Self:Page, Self:PageSize, LISTA_PROCESSAMENTOS_MRP)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet

/*/{Protheus.doc} MrpGetAPcd
Funcao para disparar as acoes da API de Resultados do MRP, para o metodo GET dos processamentos do MRP.

@type  Function
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param cBranch  , Caracter, Codigo da filial
@param cFields  , Caracter, Campos que devem ser retornados.
@param cOrder   , Character, Ordenacao desejada do retorno.
@param cPage    , Caracter, P�gina de retorno.
@param cPageSize, Caracter, Tamanho da p�gina.
@param cMap     , Caracter, Mapa dos campos utilizados na consulta.
@param cStatus  , Caracter, indicador do status de campo para filtro dos resultados conforme  HW3_STATUS
@return aReturn, Array, Array com as informacoes da requisicao.
                        aReturn[1] - Logico    - Indica se a requisicao foi processada com sucesso ou Nao.
						aReturn[2] - Character - JSON com o resultado da requisicao, ou com a mensagem de erro.
						aReturn[3] - Numeric   - Codigo de erro identificado pelo MRPApi.
/*/
Function MrpGetAPcd(cBranch, cFields, cOrder, cPage, cPageSize, cMap, cStatus)
	Local aReturn   := {}
	Local aQryParam := {}

	Default cStatus := ""

	aAdd(aQryParam, {"BRANCHID", cBranch})

	//Repassa filtros de status
	If !Empty(cStatus)
		aAdd(aQryParam, {"status", ".IN." + AllTrim(cStatus) })
	EndIf

	//Chama a funcao para retornar os dados.
	aReturn := GetResults(.T., aQryParam, cOrder, cPage, cPageSize, cFields, cMap)

Return aReturn

/*/{Protheus.doc} GET TICKET api/pcp/v1/processed/{branchId}/{ticket}
Retorna informa��es de um processamento do MRP

@type  WSMETHOD
@author lucas.franca
@since 31/01/2020
@version P12.1.27
@param	branchId, Character, Codigo da filial para fazer a pesquisa
@param	ticket  , Character, Codigo �nico do processo para fazer a pesquisa.
@param	Fields  , Character, Campos que ser�o retornados no GET.
@return lRet    , Logico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET TICKET PATHPARAM branchId, ticket QUERYPARAM Fields WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetPrc(Self:branchId, Self:ticket, Self:Fields)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet

/*/{Protheus.doc} MrpGetPrc
Fun��o para retornar informa��es de um processamento do MRP

@type  Function
@author lucas.franca
@since 24/04/2019
@version P12.1.27
@param cBranch, Character, C�digo da filial
@param cTicket, Character, Ticket de processamento
@param cFields, Character, Fields que devem ser retornados pela api
@return aReturn, Array, Array com as informa��es da requisi��o.
                        aReturn[1] - L�gico    - Indica se a requisi��o foi processada com sucesso ou n�o.
						aReturn[2] - Character - JSON com o resultado da requisi��o, ou com a mensagem de erro.
						aReturn[3] - Numeric   - C�digo de erro identificado pelo FwApiManager.
/*/
Function MrpGetPrc(cBranch, cTicket, cFields)
	Local aReturn := {}
	Local aQryParam := {}

	//Adiciona os filtros de filial e produto como QueryParam.
	aAdd(aQryParam, {"BRANCHID", cBranch})
	aAdd(aQryParam, {"TICKET"  , cTicket})

	//Processa o GET
	aReturn := GetResults(.F., aQryParam, Nil, Nil, Nil, cFields, LISTA_PROCESSAMENTOS_MRP)
Return aReturn

/*/{Protheus.doc} MapFields
Gera o array com o MAP dos fields da API

@type  Static Function
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@return aFields, Array, Array contendo o MAP dos fields da API e da tabela
/*/
Static Function MapFields(nType)
	Local aFields := {}

/*
	O array de mapeamento do JSON e composto por:
	aArray[1]
	aArray[1][1] = Nome do elemento do JSON que contem a informacao.
	aArray[1][2] = Nome da coluna da tabela correspondente a informacao.
	aArray[1][3] = Tipo de dado no banco de dados.
	aArray[1][4] = Tamanho do campo.
	aArray[1][5] = Decimais do campo, quando e do tipo numerico.
*/

	If nType == 1
		//Estrutura dos resultados sumarizados
			aFields := { ;
						{"branchId"                , "HWB_FILIAL", "C", FWSizeFilial()                        , 0},;
						{"ticket"                  , "HWB_TICKET", "C", GetSx3Cache("HWB_TICKET","X3_TAMANHO"), 0},;
						{"necessityDate"           , "HWB_DATA"  , "D", GetSx3Cache("HWB_DATA"  ,"X3_TAMANHO"), 0},;
						{"product"                 , "HWB_PRODUT", "C", GetSx3Cache("HWB_PRODUT","X3_TAMANHO"), 0},;
						{"optionalId"              , "HWB_IDOPC", "C" , GetSx3Cache("HWB_IDOPC" ,"X3_TAMANHO"), 0},;
						{"stockBalance"            , "HWB_QTSLES", "N", GetSx3Cache("HWB_QTSLES","X3_TAMANHO"), GetSx3Cache("HWB_QTSLES","X3_DECIMAL")},;
						{"inFlows"                 , "HWB_QTENTR", "N", GetSx3Cache("HWB_QTENTR","X3_TAMANHO"), GetSx3Cache("HWB_QTENTR","X3_DECIMAL")},;
						{"outFlows"                , "HWB_QTSAID", "N", GetSx3Cache("HWB_QTSAID","X3_TAMANHO"), GetSx3Cache("HWB_QTSAID","X3_DECIMAL")},;
						{"structureOutFlows"       , "HWB_QTSEST", "N", GetSx3Cache("HWB_QTSEST","X3_TAMANHO"), GetSx3Cache("HWB_QTSEST","X3_DECIMAL")},;
						{"finalBalance"            , "HWB_QTSALD", "N", GetSx3Cache("HWB_QTSALD","X3_TAMANHO"), GetSx3Cache("HWB_QTSALD","X3_DECIMAL")},;
						{"necessityQuantity"       , "HWB_QTNECE", "N", GetSx3Cache("HWB_QTNECE","X3_TAMANHO"), GetSx3Cache("HWB_QTNECE","X3_DECIMAL")},;
						{"startDate"               , "HWB_DTINIC", "D", GetSx3Cache("HWB_DTINIC","X3_TAMANHO"), 0};
					}
	ElseIf nType == 2
		//Estrutura dos resultados rastreados
			aFields := { ;
						{"branchId"                , "HWC_FILIAL", "C", FWSizeFilial()                        , 0},;
						{"ticket"                  , "HWC_TICKET", "C", GetSx3Cache("HWC_TICKET","X3_TAMANHO"), 0},;
						{"necessityDate"           , "HWC_DATA"  , "D", GetSx3Cache("HWC_DATA"  ,"X3_TAMANHO"), 0},;
						{"parentDocumentType"      , "HWC_TPDCPA", "C", GetSx3Cache("HWC_TPDCPA","X3_TAMANHO"), 0},;
						{"parentDocument"          , "HWC_DOCPAI", "C", GetSx3Cache("HWC_DOCPAI","X3_TAMANHO"), 0},;
						{"childDocument"           , "HWC_DOCFIL", "C", GetSx3Cache("HWC_DOCFIL","X3_TAMANHO"), 0},;
						{"componentCode"           , "HWC_PRODUT", "C", GetSx3Cache("HWC_PRODUT","X3_TAMANHO"), 0},;
						{"optionalId"              , "HWC_IDOPC" , "C", GetSx3Cache("HWC_IDOPC" ,"X3_TAMANHO"), 0},;
						{"sequenceInStructure"     , "HWC_TRT"   , "C", GetSx3Cache("HWC_TRT"   ,"X3_TAMANHO"), 0},;
						{"originalNecessity"       , "HWC_QTNEOR", "N", GetSx3Cache("HWC_QTNEOR","X3_TAMANHO"), GetSx3Cache("HWC_QTNEOR","X3_DECIMAL")},;
						{"stockBalanceQuantity"    , "HWC_QTSLES", "N", GetSx3Cache("HWC_QTSLES","X3_TAMANHO"), GetSx3Cache("HWC_QTSLES","X3_DECIMAL")},;
						{"quantityStockWriteOff"   , "HWC_QTBXES", "N", GetSx3Cache("HWC_QTSUBS","X3_TAMANHO"), GetSx3Cache("HWC_QTBXES","X3_DECIMAL")},;
						{"quantitySubstitution"    , "HWC_QTSUBS", "N", GetSx3Cache("HWC_QTSUBS","X3_TAMANHO"), GetSx3Cache("HWC_QTSUBS","X3_DECIMAL")},;
						{"alocationQuantity"       , "HWC_QTEMPE", "N", GetSx3Cache("HWC_QTEMPE","X3_TAMANHO"), GetSx3Cache("HWC_QTEMPE","X3_DECIMAL")},;
						{"quantityNecessity"       , "HWC_QTNECE", "N", GetSx3Cache("HWC_QTNECE","X3_TAMANHO"), GetSx3Cache("HWC_QTNECE","X3_DECIMAL")},;
						{"structureReview"         , "HWC_REV"   , "C", GetSx3Cache("HWC_REV"   ,"X3_TAMANHO"), 0},;
						{"routing"                 , "HWC_ROTEIR", "C", GetSx3Cache("HWC_ROTEIR","X3_TAMANHO"), 0},;
						{"operation"               , "HWC_OPERAC", "C", GetSx3Cache("HWC_OPERAC","X3_TAMANHO"), 0},;
						{"consumptionLocation"     , "HWC_LOCAL" , "C", GetSx3Cache("HWC_LOCAL" ,"X3_TAMANHO"), 0},;
						{"recordKey"               , "HWC_CHAVE" , "C", GetSx3Cache("HWC_CHAVE" ,"X3_TAMANHO"), 0},;
						{"substitutionKey"         , "HWC_CHVSUB", "C", GetSx3Cache("HWC_CHVSUB","X3_TAMANHO"), 0},;
						{"breakupSequence"         , "HWC_SEQUEN", "N", GetSx3Cache("HWC_SEQUEN","X3_TAMANHO"), 0},;
						{"erpDocument"             , "HWC_DOCERP", "C", GetSx3Cache("HWC_DOCERP","X3_TAMANHO"), 0},;
						{"erpDocumentType"         , "HWC_TDCERP", "C", GetSx3Cache("HWC_TDCERP","X3_TAMANHO"), 0},;
						{"recordStatus"            , "HWC_STATUS", "C", GetSx3Cache("HWC_STATUS","X3_TAMANHO"), 0},;
						{"productionVersion"       , "HWC_VERSAO", "C", GetSx3Cache("HWC_VERSAO","X3_TAMANHO"), 0};
					}

					dbSelectArea("HWC")
					If FieldPos("HWC_ROTFIL") > 0
						aAdd(aFields, {"routingChildDocument"    , "HWC_ROTFIL", "C", GetSx3Cache("HWC_ROTFIL","X3_TAMANHO"), 0})
					EndIf
	ElseIf nType == 3
		//Estrutura dos parametros
		aFields := { ;
					{"branchId"           , "HW1_FILIAL", "C", FWSizeFilial()                        , 0},;
					{"parameter"          , "HW1_PARAM" , "C", GetSx3Cache("HW1_PARAM" ,"X3_TAMANHO"), 0},;
					{"value"              , "HW1_VAL"   , "C", GetSx3Cache("HW1_VAL"   ,"X3_TAMANHO"), 0},;
					{"list"               , "HW1_LISTA" , "M", GetSx3Cache("HW1_LISTA" ,"X3_TAMANHO"), 0},;
					{"ticket"             , "HW1_TICKET", "C", GetSx3Cache("HW1_TICKET","X3_TAMANHO"), 0};
				}
	ElseIf nType == 4
		//Estrutura dos resultados sumarizados
		aFields := { ;
					{"branchId"                , "HWB_FILIAL", "C", FWSizeFilial()                        , 0},;
					{"ticket"                  , "HWB_TICKET", "C", GetSx3Cache("HWB_TICKET","X3_TAMANHO"), 0},;
					{"optionalId"              , "HWB_IDOPC" , "C", GetSx3Cache("HWB_IDOPC" ,"X3_TAMANHO"), 0},;
					{"product"                 , "HWB_PRODUT", "C", GetSx3Cache("HWB_PRODUT","X3_TAMANHO"), 0};
				}
	ElseIf nType == 5
		aFields := { ;
					{"branchId"                , "HW3_FILIAL", "C", FWSizeFilial()                        , 0},;
					{"ticket"                  , "HW3_TICKET", "C", GetSx3Cache("HW3_TICKET","X3_TAMANHO"), 0},;
					{"startDate"               , "HW3_DTINIC", "D", GetSx3Cache("HW3_DTINIC","X3_TAMANHO"), 0},;
					{"startTime"               , "HW3_HRINIC", "C", GetSx3Cache("HW3_HRINIC","X3_TAMANHO"), 0},;
					{"endDate"                 , "HW3_DTFIM" , "D", GetSx3Cache("HW3_DTFIM" ,"X3_TAMANHO"), 0},;
					{"endTime"                 , "HW3_HRFIM" , "C", GetSx3Cache("HW3_HRFIM" ,"X3_TAMANHO"), 0},;
					{"user"                    , "HW3_USER"  , "C", GetSx3Cache("HW3_USER"  ,"X3_TAMANHO"), 0},;
					{"status"                  , "HW3_STATUS", "C", GetSx3Cache("HW3_STATUS","X3_TAMANHO"), 0};
				}
	EndIf

Return aFields

/*/{Protheus.doc} GetResults
Executa o processamento do metodo GET dos resultados do MRP.

@type  Static Function
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param lLista   , Logic    , Indica se devera retornar uma lista de registros (.T.), ou apenas um registro (.F.).
@param aQuery   , Array    , Array com os dados que devem ser filtrados.
                             Exemplo: Array[1]
                                      Array[1][1] = "PRODUCT"
                                      Array[1][2] = "PRODUTO001"
                                      Array[2]
                                      Array[2][1] = "PRODUCTIONORDER"
                                      Array[2][2] = "00000101001"
@param cOrder   , Character, Ordenacao desejada do retorno.
@param nPage    , Numeric  , Pagina dos dados. Se Nao enviado, considera pagina 1.
@param nPageSize, Numeric  , Quantidade de registros retornados por pagina. Se Nao enviado, considera 20 registros por pagina.
@param cFields  , Character, Campos que devem ser retornados. Se Nao enviado, retorna todos os fields que possuem valor.
@return aReturn , Array    , Array com as informacoes da requisicao.
                             aReturn[1] - Logico    - Indica se a requisicao foi processada com sucesso ou Nao.
						     aReturn[2] - Character - JSON com o resultado da requisicao, ou com a mensagem de erro.
						     aReturn[3] - Numeric   - Codigo de erro identificado pelo MRPApi.
/*/
Static Function GetResults(lLista, aQuery, cOrder, nPage, nPageSize, cFields, cMap)
	Local aReturn  := {.T.,"",200}
	Local oMRPApi  := defMRPApi("GET",cOrder) //Instancia da classe MRPApi para o metodo GET

	//Seta os Processos de paginacao, filtros e campos para retorno
	oMRPApi:setFields(cFields)
	oMRPApi:setPage(nPage)
	oMRPApi:setPageSize(nPageSize)
	oMRPApi:setQueryParams(aQuery)
	oMRPApi:setUmRegistro(!lLista)

	//Executa o processamento
	aReturn[1] := oMRPApi:processar(cMap)

	//Retorna o status do processamento
	aReturn[3] := oMRPApi:getStatus()

	If aReturn[1]
		//Se processou com sucesso, recupera o JSON com os dados.
		aReturn[2] := oMRPApi:getRetorno(1)
	Else
		//Ocorreu algum erro no processo. Recupera mensagem de erro.
		aReturn[2] := oMRPApi:getMessage()
	EndIf

	//Libera o objeto MRPApi da memoria.
	oMRPApi:Destroy()
	FreeObj(oMRPApi)
	oMRPApi := Nil
Return aReturn

/*/{Protheus.doc} defMRPApi
Faz a instancia da classe MRPAPI e seta as propriedades basicas.

@type  Static Function
@author renan.roeder
@since 20/11/2019
@version P12.1.27
@param cMethod  , Character, Metodo que sera executado (GET/POST/DELETE)
@param cOrder   , Character, Ordenacao para o GET
@return oMRPApi , Object   , Refer�ncia da classe MRPApi com as definicoes ja executadas.
/*/
Static Function defMRPApi(cMethod, cOrder)
	Local oMRPApi := MRPApi():New(cMethod)

	//Seta o APIMAP dos resultados sumarizados (distinct produto)
	oMRPApi:setAPIMap(LISTA_RESULTADOS_SUMARIZADOS, _aMapMat , "HWB", .T., cOrder)
	//Seta o APIMAP dos resultados rastreados
	oMRPApi:setAPIMap(LISTA_RESULTADOS_RASTREADOS, _aMapRas , "HWC", .F., cOrder)
	//Seta o APIMAP dos parametros
	oMRPApi:setAPIMap(LISTA_PARAMETROS_MRP, _aMapPar , "HW1", .F., cOrder)
    //Seta o APIMAP dos resultados sumarizados
	oMRPApi:setAPIMap(LISTA_RESULTADOS_SUMARIZADOS_PRODUTO, _aMapMtP , "HWB", .T., cOrder)
	//Seta o APIMAP dos processamentos do MRP
	oMRPApi:setAPIMap(LISTA_PROCESSAMENTOS_MRP, _aMapPcd , "HW3", .T., cOrder)

	//Seta os campos utilizados para busca de registros.
	oMRPApi:setKeySearch(LISTA_RESULTADOS_SUMARIZADOS,{"HWB_FILIAL","HWB_TICKET"})
	oMRPApi:setKeySearch(LISTA_RESULTADOS_RASTREADOS,{"HWC_FILIAL","HWC_TICKET"})
	oMRPApi:setKeySearch(LISTA_PARAMETROS_MRP,{"HW1_FILIAL","HW1_TICKET"})
	oMRPApi:setKeySearch(LISTA_RESULTADOS_SUMARIZADOS_PRODUTO,{"HWB_FILIAL","HWB_TICKET"})
	oMRPApi:setKeySearch(LISTA_PROCESSAMENTOS_MRP,{"HW3_FILIAL"})

Return oMRPApi

//dummy function
Function MrpResults()
Return



/*/{Protheus.doc} GET PROCREGS api/pcp/v1/mrpresults/process/{ticket}
Busca os registros que devem ser processados

@type  WSMETHOD
@author douglas.heydt
@since 17/12/2019
@version P12.1.27
@param	ticket  , Character, Codigo �nico do processo para fazer a pesquisa.
/*/
WSMETHOD GET PROCREGS PATHPARAM ticket QUERYPARAM Fields,Order,Page,PageSize WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetHWC( Self:ticket, .F. )
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet

/*/{Protheus.doc} MrpGetHWC
Busca os registros que devem ser processados

@type  Function
@author douglas.heydt
@since 17/12/2019
@version P12.1.27
@param	ticket  , Character, Codigo �nico do processo para fazer a pesquisa.
@param lRetJSON , Logical  , Identifica se o retorno deve ser em JSON ou em STRING
/*/
Function MrpGetHWC(cTicket, lRetJSON)

	Local aResult 	 := {.T.,"",200}
	Local cAliasQry  := GetNextAlias()
	Local cQuery     := ""
	Local nPos       := 0
	Local oDados     := JsonObject():New()

	cQuery := "SELECT HWC.HWC_DATA, "
	cQuery +=       " HWC.HWC_TPDCPA, "
	cQuery +=       " HWC.HWC_DOCPAI, "
	cQuery +=       " HWC.HWC_DOCFIL, "
	cQuery +=       " HWC.HWC_PRODUT, "
	cQuery +=       " HWC.HWC_IDOPC, "
	cQuery +=       " HWC.HWC_TRT, "
	cQuery +=       " HWC.HWC_QTNEOR, "
	cQuery +=       " HWC.HWC_QTSLES, "
	cQuery +=       " HWC.HWC_QTBXES, "
	cQuery +=       " HWC.HWC_QTSUBS, "
	cQuery +=       " HWC.HWC_QTEMPE, "
	cQuery +=       " HWC.HWC_QTNECE, "
	cQuery +=       " HWC.HWC_REV, "
	cQuery +=       " HWC.HWC_ROTEIR, "
	cQuery +=       " HWC.HWC_OPERAC, "
	dbSelectArea("HWC")
	If FieldPos("HWC_ROTFIL") > 0
		cQuery +=       " HWC.HWC_ROTFIL, "
	Else
		cQuery +=       " '' as HWC_ROTFIL, "
	EndIf
	cQuery +=       " HWC.HWC_CHAVE, "
	cQuery +=       " HWC.HWC_CHVSUB, "
	cQuery +=       " HWC.HWC_SEQUEN, "
	cQuery +=       " COALESCE(HWB.HWB_DTINIC, HWC.HWC_DATA) AS HWB_DTINIC, "
	cQuery +=       " HWA.HWA_NIVEL, "

	//Se n�o possui o campo na HWA, marca para gerar sempre solicita��o de compra
	If HWA->(FieldPos("HWA_CONTRA")) > 0
		cQuery +=   " HWA.HWA_CONTRA, "
	Else
		cQuery +=   " '2' AS HWA_CONTRA, "
	EndIf

	cQuery +=       " CASE WHEN HWC.HWC_LOCAL = ' ' "
	cQuery +=            " THEN HWA.HWA_LOCPAD "
	cQuery +=            " ELSE HWC.HWC_LOCAL "
	cQuery +=       " END AS HWC_LOCAL, "
	cQuery +=       " HWC.R_E_C_N_O_ AS RECHWC "
	cQuery +=  " FROM " + RetSqlName("HWA") + " HWA, "  //Tabela de Produto
	cQuery +=             RetSqlName("HWC") + " HWC " //Tabela de rastreio
	cQuery +=  " LEFT OUTER JOIN " + RetSqlName("HWB") + " HWB " //Tabela da matriz
	cQuery +=    " ON HWB.HWB_TICKET = HWC.HWC_TICKET "
	cQuery +=   " AND HWB.HWB_PRODUT = HWC.HWC_PRODUT "
	cQuery +=   " AND HWB.HWB_IDOPC  = HWC.HWC_IDOPC "
	cQuery +=   " AND HWB.HWB_DATA   = HWC.HWC_DATA "
	cQuery +=   " AND HWB.HWB_FILIAL = '" + xFilial("HWB") + "' "
	cQuery +=   " AND HWB.D_E_L_E_T_ = ' ' "
	cQuery += " WHERE HWC.HWC_TICKET = '" + cTicket + "' "
	cQuery +=   " AND HWA.HWA_PROD   = HWC.HWC_PRODUT "
	cQuery +=   " AND HWC.HWC_STATUS <> '1' " //Somente o que ainda n�o foi processado.
	cQuery +=   " AND HWA.HWA_FILIAL = '" + xFilial("HWA") + "' "
	cQuery +=   " AND HWC.HWC_FILIAL = '" + xFilial("HWC") + "' "
	cQuery +=   " AND HWA.D_E_L_E_T_ = ' ' "
	cQuery +=   " AND HWC.D_E_L_E_T_ = ' ' "
	cQuery += " ORDER BY HWA.HWA_NIVEL, HWC.HWC_DATA, HWC.HWC_SEQUEN, HWC.HWC_DOCPAI, HWC.HWC_PRODUT, HWC.HWC_IDOPC "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQry,.F.,.F.)

	//Ajusta o tipo dos campos na query.
	TcSetField(cAliasQry, 'HWC_QTNEOR', 'N', _aTamQtd[1], _aTamQtd[2])
	TcSetField(cAliasQry, 'HWC_QTSLES', 'N', _aTamQtd[1], _aTamQtd[2])
	TcSetField(cAliasQry, 'HWC_QTBXES', 'N', _aTamQtd[1], _aTamQtd[2])
	TcSetField(cAliasQry, 'HWC_QTSUBS', 'N', _aTamQtd[1], _aTamQtd[2])
	TcSetField(cAliasQry, 'HWC_QTEMPE', 'N', _aTamQtd[1], _aTamQtd[2])
	TcSetField(cAliasQry, 'HWC_QTNECE', 'N', _aTamQtd[1], _aTamQtd[2])

	oDados["items"] := {}

	nPos := 0
	While (cAliasQry)->(!Eof())

		aAdd(oDados["items"], JsonObject():New())
		nPos++

		oDados["items"][nPos]['necessityDate'        ] := getDate((cAliasQry)->HWC_DATA)
		oDados["items"][nPos]['parentDocumentType'   ] := (cAliasQry)->HWC_TPDCPA
		oDados["items"][nPos]['parentDocument'       ] := (cAliasQry)->HWC_DOCPAI
		oDados["items"][nPos]['childDocument'        ] := (cAliasQry)->HWC_DOCFIL
		oDados["items"][nPos]['componentCode'        ] := (cAliasQry)->HWC_PRODUT
		oDados["items"][nPos]['optionalId'           ] := (cAliasQry)->HWC_IDOPC
		oDados["items"][nPos]['sequenceInStructure'  ] := (cAliasQry)->HWC_TRT
		oDados["items"][nPos]['originalNecessity'    ] := (cAliasQry)->HWC_QTNEOR
		oDados["items"][nPos]['stockBalanceQuantity' ] := (cAliasQry)->HWC_QTSLES
		oDados["items"][nPos]['quantityStockWriteOff'] := (cAliasQry)->HWC_QTBXES
		oDados["items"][nPos]['quantitySubstitution' ] := (cAliasQry)->HWC_QTSUBS
		oDados["items"][nPos]['alocationQuantity'    ] := (cAliasQry)->HWC_QTEMPE
		oDados["items"][nPos]['quantityNecessity'    ] := (cAliasQry)->HWC_QTNECE
		oDados["items"][nPos]['structureReview'      ] := (cAliasQry)->HWC_REV
		oDados["items"][nPos]['routing'              ] := (cAliasQry)->HWC_ROTEIR
		oDados["items"][nPos]['operation'            ] := (cAliasQry)->HWC_OPERAC
		oDados["items"][nPos]['routingChildDocument' ] := (cAliasQry)->HWC_ROTFIL
		oDados["items"][nPos]['recordKey'            ] := (cAliasQry)->HWC_CHAVE
		oDados["items"][nPos]['substitutionKey'      ] := (cAliasQry)->HWC_CHVSUB
		oDados["items"][nPos]['breakupSequence'      ] := (cAliasQry)->HWC_SEQUEN
		oDados["items"][nPos]['startDate'            ] := getDate((cAliasQry)->HWB_DTINIC )
		oDados["items"][nPos]['level'                ] := (cAliasQry)->HWA_NIVEL
		oDados["items"][nPos]['consumptionLocation'  ] := (cAliasQry)->HWC_LOCAL
		oDados["items"][nPos]['purchaseContract'     ] := (cAliasQry)->HWA_CONTRA

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	aResult[2] := Iif(lRetJSON, oDados, oDados:toJson())

	If nPos > 0
		aResult[1] := .T.
		aResult[3] := 200
	Else
		aResult[1] := .F.
		aResult[3] := 400
	EndIf

	If !lRetJSON
		aSize(oDados["items"],0)
		FreeObj(oDados)
	EndIf

Return aResult

/*/{Protheus.doc} POST RASTREIO api/pcp/v1/mrpresults/tracking/{ticket}/{recStatus}/{erpDoc}/{erpDocType}/{parentDocT}/{parentDoc}/{seqInStru}/{compCode}/{recordKey}/{substKey}/{breakupSeq}

@param ticket     , Character, Codigo �nico do processo
@param recStatus  , Character, Status do registro
@param erpDoc     , Character, Documento gerado pelo ERP
@param erpDocType , Character, Tipo de Documento gerado pelo ERP
@param parentDocT , Character, Tipo do documento pai
@param parentDoc  , Character, Documento pai
@param seqInStru  , Character, Sequ�ncia TRT do produto
@param compCode   , Character, C�digo do componente
@param recordKey  , Character, Chave Registro
@param substKey   , Character, Chave de substitui��o
@param breakupSeq , Character, Sequencia de quebra

@return lRet	, L�gico, Informa se o processo foi executado com sucesso.
@author	Douglas.heydt
@since 17/12/2019
@version 12.1.28
/*/
WSMETHOD POST RASTREIO PATHPARAM ticket, recStatus, erpDoc, erpDocType, parentDocT, parentDoc, seqInStru, compCode, recordKey, substKey, breakupSeq WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpPostRas( Self:ticket, Self:recStatus, Self:erpDoc, Self:erpDocType, Self:parentDocT, Self:parentDoc, Self:seqInStru, Self:compCode, Self:recordKey, Self:substKey, Self:breakupSeq )
	MRPApi():restReturn(Self, aReturn, "POST", @lRet)
	aReturn := Nil

Return lRet


/*/{Protheus.doc} POST MrpPostRas

@param ticket     , Character, Codigo �nico do processo
@param recStatus  , Character, Status do registro
@param erpDoc     , Character, Documento gerado pelo ERP
@param erpDocType , Character, Tipo de Documento gerado pelo ERP
@param parentDocT , Character, Tipo do documento pai
@param parentDoc  , Character, Documento pai
@param seqInStru  , Character, Sequ�ncia TRT do produto
@param compCode   , Character, C�digo do componente
@param recordKey  , Character, Chave Registro
@param substKey   , Character, Chave de substitui��o
@param breakupSeq , Character, Sequencia de quebra

@return aResult	, Array, resultados obtidos pela query
@author	Douglas.heydt
@since 17/12/2019
@version 12.1.28
/*/
Function MrpPostRas( ticket, recStatus, erpDoc, erpDocType, parentDocT, parentDoc, seqInStru, compCode, recordKey, substKey, breakupSeq )

	Local cSql     := ""
	Local aResult  := {}

	If Empty(_nTamTipo)
		_nTamTipo := TamSX3("HWC_TDCERP")[1]
	EndIf

	If Empty(_nTamDoc)
		_nTamDoc  := TamSX3("HWC_DOCERP")[1]
	EndIf

	recStatus  := StrTran(recStatus , "'", "''")
	erpDoc     := StrTran(erpDoc    , "'", "''")
	erpDocType := StrTran(erpDocType, "'", "''")
	parentDocT := StrTran(parentDocT, "'", "''")
	parentDoc  := StrTran(parentDoc , "'", "''")
	seqInStru  := StrTran(seqInStru , "'", "''")
	compCode   := StrTran(compCode  , "'", "''")
	recordKey  := StrTran(recordKey , "'", "''")
	substKey   := StrTran(substKey  , "'", "''")

	cSql := " UPDATE " + RetSqlName("HWC")
	cSql +=    " SET HWC_STATUS = '" + recStatus          			+ "', "
	cSql +=        " HWC_DOCERP = '" + Padr(erpDoc,_nTamDoc)        + "', "
	cSql +=        " HWC_TDCERP = '" + Padr(erpDocType,_nTamTipo)   + "'  "
	cSql +=  " WHERE HWC_FILIAL = '" + xFilial("HWC")       	    + "'  "
	cSql +=    " AND HWC_TICKET = '" + ticket         				+ "'  "
	cSql +=    " AND HWC_TPDCPA = '" + parentDocT   				+ "'  "
	cSql +=    " AND HWC_DOCPAI = '" + parentDoc       				+ "'  "
	cSql +=    " AND HWC_TRT    = '" + seqInStru  					+ "'  "
	cSql +=    " AND HWC_PRODUT = '" + compCode        			    + "'  "
	cSql +=    " AND HWC_CHAVE  = '" + recordKey            		+ "'  "
	cSql +=    " AND HWC_CHVSUB = '" + substKey      			    + "'  "
	cSql +=    " AND HWC_SEQUEN = "  + STR(breakupSeq)

	If TcSqlExec(cSql) < 0
		aAdd(aResult, 400)
		aAdd(aResult, TcSqlError())
	Else
		aAdd(aResult, 200)
		aAdd(aResult, 'Ok')
	EndIf

Return aResult



/*/{Protheus.doc} GET AGLUTINADO api/pcp/v1/mrpresults/aglutinado/{ticket}/{docMrp}/{product}
Consulta um documento do MRP na tabela de documentos aglutinados.
@type  WSMETHOD
@author douglas.heydt
@since 17/12/2019
@version P12.1.27
@param	ticket  , Character, Codigo �nico do processo para fazer a pesquisa.
@param	docMrp  , Character, Documento gerado pelo ERP
@param  product , Character, C�digo do componente
/*/
WSMETHOD GET AGLUTINADO PATHPARAM ticket, docMrp, product  WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetHWG( Self:ticket, Self:docMrp, Self:product )
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet


/*/{Protheus.doc} MrpGetHWG
Consulta um documento do MRP na tabela de documentos aglutinados.
@type  WSMETHOD
@author douglas.heydt
@since 17/12/2019
@version P12.1.27
@param	cTicket  , Character, Codigo �nico do processo para fazer a pesquisa.
@param	cDocMRP  , Character, Documento gerado pelo ERP
@param  cProduto , Character, C�digo do componente
/*/
Function MrpGetHWG(cTicket, cDocMRP, cProduto)

	Local cAliasQry  := GetNextAlias()
	Local cQuery     := ""
	Local aResult 	 := {}
	Local aJson      := {}
	Local nPos       := 0

	cQuery :=	"SELECT HWC.HWC_DOCFIL, "
	cQuery +=	"      HWG.HWG_QTEMPE "
	cQuery +=	"  FROM "+RetSqlName("HWG")+" HWG, "+RetSqlName("HWC")+" HWC "
	cQuery +=	" WHERE HWG.HWG_FILIAL = '"+xFilial("HWG")+"' "
	cQuery +=	"   AND HWG.HWG_TICKET = '"+cTicket+"' "
	cQuery +=	"   AND HWG.HWG_DOCAGL = '"+cDocMRP+"' "
	cQuery +=	"   AND HWG.HWG_PROD   = '"+cProduto+"' "
	cQuery +=	"   AND HWG.D_E_L_E_T_ = ' ' "
	cQuery +=	"   AND HWC.HWC_FILIAL = '"+xFilial("HWC")+"' "
	cQuery +=	"   AND HWC.HWC_TICKET = HWG.HWG_TICKET "
	cQuery +=	"   AND HWC.HWC_DOCPAI = HWG.HWG_DOCORI "
	cQuery +=	"   AND HWC.HWC_DOCFIL <> ' ' "
	cQuery +=	"   AND HWC.HWC_SEQUEN = HWG.HWG_SEQORI "
	cQuery +=	"   AND HWC.HWC_PRODUT = HWG.HWG_PRODOR "
	cQuery +=	"   AND HWC.D_E_L_E_T_ = ' '"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQry,.F.,.F.)

	TcSetField(cAliasQry, 'HWG_QTEMPE', 'N', _aTamQtd[1], _aTamQtd[2])

	aJson := JsonObject():New()
	aJson["items"] := {}

	While (cAliasQry)->(!Eof())

		Aadd(aJson["items"],JsonObject():new())
        nPos := Len(aJson["items"])
       	aJson["items"][nPos]['allocation'] := (cAliasQry)->HWG_QTEMPE
		aJson["items"][nPos]['childDocument'] := (cAliasQry)->HWC_DOCFIL
		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	If Len(aJson["items"]) > 0
		aAdd(aResult, .T.)
		aAdd(aResult, aJson:toJSON())
		aAdd(aResult, 200)
	Else
		aAdd(aResult, .F.)
		aAdd(aResult, aJson:toJSON())
		aAdd(aResult, 400)
	EndIf

	aSize(aJson["items"],0)
	FreeObj(aJson)

Return aResult

/*/{Protheus.doc} GET ORDSUBS api/pcp/v1/mrpresults/ordsubs/{ticket}/{recordKey}/{parentDocument}
Busca documentos na tabela de substitui��o
@type  WSMETHOD
@author douglas.heydt
@since 17/12/2019
@version P12.1.27
@param	cTicket , Character, Codigo �nico do processo para fazer a pesquisa.
@param	cChave  , Character, Chave Registro
@param  cDocPai , Character, Documento pai
/*/
WSMETHOD GET ORDSUBS PATHPARAM  ticket, recordKey, parentDoc WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpOrdSubs(Self:ticket, Self:recordKey, Self:parentDoc )
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet


/*/{Protheus.doc} MrpOrdSubs
Busca documentos na tabela de substitui��o
@type  WSMETHOD
@author douglas.heydt
@since 17/12/2019
@version P12.1.27
@param	cTicket , Character, Codigo �nico do processo para fazer a pesquisa.
@param	cChave  , Character, Chave Registro
@param  cDocPai , Character, Documento pai
/*/
Function MrpOrdSubs(cTicket, cChave, cDocPai)

	Local cAliasQry  := GetNextAlias()
	Local cQuery     := ""
	Local aResult 	 := {}
	Local aJson      := {}
	Local nPos       := 0

	//Busca os dados do componente original que foi trocado
	cQuery := "	SELECT HWC.HWC_DOCFIL, "
	cQuery += "	       HWC.HWC_PRODUT, "
	cQuery += "	       HWC.HWC_IDOPC, "
	cQuery += "	       CASE WHEN HWC.HWC_LOCAL = ' ' "
	cQuery += "	            THEN HWA.HWA_LOCPAD "
	cQuery += "	            ELSE HWC.HWC_LOCAL "
	cQuery += "	       END AS HWC_LOCAL, "
	cQuery += "	       HWC.HWC_TRT, "
	cQuery += "	       HWC.HWC_QTEMPE "
	cQuery += "	 FROM "+RetSqlName("HWC")+" HWC, "+RetSqlName("HWA")+" HWA "
	cQuery += "	 WHERE HWC.HWC_FILIAL = '"+xFilial("HWC")+"' "
	cQuery += "	   AND HWC.HWC_TICKET = '"+cTicket+"' "
	cQuery += "	   AND HWC.HWC_CHAVE  = '"+cChave+"' "
	cQuery += "	   AND HWC.HWC_DOCPAI = '"+cDocPai+"' "
	cQuery += "	   AND HWA.HWA_FILIAL = '"+xFilial("HWA")+"' "
	cQuery += "	   AND HWA.HWA_PROD   = HWC.HWC_PRODUT "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQry,.F.,.F.)

	TcSetField(cAliasQry, 'HWC_QTEMPE', 'N', _aTamQtd[1], _aTamQtd[2])

	aJson := JsonObject():New()
	aJson["items"] := {}

	While (cAliasQry)->(!Eof())

		Aadd(aJson["items"],JsonObject():new())
        nPos := Len(aJson["items"])
       	aJson["items"][nPos]['childDocument'] 		:= (cAliasQry)->HWC_DOCFIL
		aJson["items"][nPos]['componentCode'] 		:= (cAliasQry)->HWC_PRODUT
		aJson["items"][nPos]['optionalId'] 		    := (cAliasQry)->HWC_IDOPC
		aJson["items"][nPos]['consumptionLocation'] := (cAliasQry)->HWC_LOCAL
		aJson["items"][nPos]['sequenceInStructure'] := (cAliasQry)->HWC_TRT
		aJson["items"][nPos]['alocationQuantity'] 	:= (cAliasQry)->HWC_QTEMPE
		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	If Len(aJson["items"]) > 0
		aAdd(aResult, .T.)
		aAdd(aResult, aJson:toJSON())
		aAdd(aResult, 200)
	Else
		aAdd(aResult, .F.)
		aAdd(aResult, aJson:toJSON())
		aAdd(aResult, 400)
	EndIf

	aSize(aJson["items"],0)
	FreeObj(aJson)

Return aResult


/*/{Protheus.doc} GET PRODORIG api/pcp/v1/mrpresults/prodorig/{ticket}/{recordKey}/{parentDocument}
Busca documentos na tabela de substitui��o
@type  WSMETHOD
@author douglas.heydt
@since 17/12/2019
@version P12.1.27
@param	cTicket , Character, Codigo �nico do processo para fazer a pesquisa.
@param	cChave  , Character, Chave Registro
@param  cDocPai , Character, Documento pai
/*/
WSMETHOD GET PRODORIG PATHPARAM  ticket, recordKey, parentDoc WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpPrdOrig(Self:ticket, Self:recordKey, Self:parentDoc  )
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet

/*/{Protheus.doc} POST CLEAR /api/pcp/v1/mrpresults/clear
Efetua limpeza da base de processamento do MRP

@type  WSMETHOD
@author brunno.costa
@since 18/03/2020
@version P12.1.30
@return lRet, Logico, Informa se o processo foi executado com sucesso.
/*/
WSMETHOD POST CLEAR WSSERVICE mrpresults
	Local aReturn  := {}
	Local cBody    := ""
	Local cError   := ""
	Local lRet     := .T.
	Local oBody    := JsonObject():New()

	Self:SetContentType("application/json")
	cBody  := Self:GetContent()
	cError := oBody:FromJson(cBody)

	If cError == Nil
		aReturn := MrpRClear(oBody)
		HTTPSetStatus(aReturn[1])
		Self:SetResponse(EncodeUtf8(aReturn[2]))
	Else
		//Ocorreu erro ao transformar os dados recebidos em objeto JSON.
		lRet    := .F.
		SetRestFault(400, EncodeUtf8(STR0006), .T., , cError ) //"Nao foi poss�vel interpretar os dados recebidos."
	EndIf

	FreeObj(oBody)
	oBody   := Nil
	aReturn := Nil

Return lRet

/*/{Protheus.doc} MrpRClear
Dispara limpeza da base de processamento do MRP

@type  Function
@author brunno.costa
@since 18/03/2020
@version P12.1.30
@param 01 oBody , JsonObject, Objeto JSON com as informacoes recebidas no corpo da requisicao.
@return aReturn, Array, Array contendo o Codigo HTTP que devera ser retornado pela API, e o JSON formatado com todos os dados de retorno.
                        Array[1] -> Numeric. Codigo HTTP de retorno (201 - Created; 207 - MultiStatus; 400 - Bad Request; 200 - OK)
                        Array[2] -> Character. JSON no formato definido pela API para ser retornado.
/*/
Function MrpRClear(oBody)

	Local aTickets := {}
	Local aTabelas := {"HWB", "HWC", "HWD", "HWG", "HW1", "HW3"}
	Local aReturn   := {201, ""}
	Local cSQL     := ""
	Local cTickets := oBody["cTickets"]
	Local cNotIN   := UltProcPen()
	Local nInd     := 0
	Local nIndReg  := 0
	Local nTotal   := Len(aTabelas)
	Local nTotReg  := 0
	Local lResult  := .T.
	Local oJsonAux  := JsonObject():New()

	If AliasInDic("HWM")
		aAdd(aTabelas, "HWM")
		nTotal++
	EndIf

	aTickets := StrTokArr( cTickets, "," )
	nTotReg  := Len(aTickets)
	oJsonAux["items"] := JsonObject():New()
	For nInd := 1 to nTotal
		If AllTrim(cTickets) == "*"
			If Empty(cNotIN)
				cSQL    := "DELETE FROM " + RetSQLName(aTabelas[nInd])
			Else
				cSQL    := "DELETE FROM " + RetSQLName(aTabelas[nInd]) + " WHERE " + aTabelas[nInd] + "_TICKET NOT IN (" + cNotIN + ")"
			EndIf
			lResult := PCPSqlExec(cSQL, @oJsonAux, aTabelas[nInd])

		Else
			cTickets := ""
			For nIndReg := 1 to nTotReg
				If Empty(cTickets)
					cTickets := aTickets[nIndReg]
				Else
					cTickets += "," + aTickets[nIndReg]
				EndIf
				If Mod(nIndReg, 500) == 0
					cSQL     := "DELETE FROM " + RetSQLName(aTabelas[nInd]) + " WHERE " + aTabelas[nInd] + "_TICKET IN (" + cTickets + ")"
					lResult  := PCPSqlExec(cSQL, @oJsonAux, aTabelas[nInd])
					cTickets := ""
				EndIf
			Next

			If !Empty(cTickets)
				cSQL    := "DELETE FROM " + RetSQLName(aTabelas[nInd]) + " WHERE " + aTabelas[nInd] + "_TICKET IN (" + cTickets + ")"
				lResult := PCPSqlExec(cSQL, @oJsonAux, aTabelas[nInd])
			EndIf

		EndIf
	Next
	oJsonAux["lResult"] := lResult

	//Recupera o status do processamento
	If lResult
		aReturn[1] := 201 //Ok
	Else
		aReturn[1] := 201 //Falha
	EndIf

	//Recupera o JSON com os dados do retorno do processo.
	aReturn[2] := oJsonAux:toJson()

	//Limpeza de mem�ria
	FreeObj(oJsonAux["items"])
	oJsonAux["items"] := Nil
	FreeObj(oJsonAux)
	oJsonAux := Nil
	aSize(aTabelas, 0)

Return aReturn

/*/{Protheus.doc} UltProcPen
Retorna o ID do �timo processamento, caso esteja ainda em execu��o
@type  WSMETHOD
@author brunno.costa
@since 22/07/2020
@version P12.1.27
@return cNotIN, caracter, ultimo ticket em execucao, caso esteja ainda em processamento
/*/
Static Function UltProcPen()

	Local aUltProc := MrpGStatus()
	Local cNotIN := ""
	Local oJsonAux

	If !Empty(aUltProc)
		oJsonAux := JsonObject():New()
		oJsonAux:fromJson(aUltProc[2])
		If oJsonAux["status"] != Nil .AND. oJsonAux["status"] $ "1,2" //Reservado, Iniciado
			cNotIN := oJsonAux["ticket"]
		EndIf
		FreeObj(oJsonAux)
		oJsonAux := Nil
		aSize(aUltProc, 0)
	EndIf

Return cNotIN

/*/{Protheus.doc} PCPSqlExec
Executa TcSQLExe
@author brunno.costa
@since 23/03/2020
@version P12.1.30
@param	cSQL    , caracter , script SQL
@param	oJsonAux, objeto   , Ojeto de controle de erros Json
@param	cTabela , caracter , nome da tabela relacionada
@return lResult, l�gico, indica sucesso na execu��o
/*/

Static Function PCPSqlExec(cSQL, oJsonAux, cTabela)
	Local cErro
	Local lResult := .T.

	If TcSqlExec(cSQL) < 0
		cErro   := AllTrim(TcSqlError())
		LogMsg('PCPA144', 0, 0, 1, '', '', "PCPA144 - " + STR0014 + " '" + cTabela + "': " + cErro) //Erro na limpeza da tabela
		oJsonAux["items"][cTabela] := cErro
		lResult := .F.

	ElseIf oJsonAux["items"][cTabela] == Nil
		oJsonAux["items"][cTabela] := "ok"

	EndIf
Return lResult

/*/{Protheus.doc} MrpPrdOrig
Busca documentos na tabela de substitui��o
@type  WSMETHOD
@author douglas.heydt
@since 17/12/2019
@version P12.1.27
@param	cTicket , Character, Codigo �nico do processo para fazer a pesquisa.
@param	cChave  , Character, Chave Registro
@param  cDocPai , Character, Documento pai
/*/
Function MrpPrdOrig(cTicket, cChave, cDocPai )

	Local cAliasQry  := GetNextAlias()
	Local cQuery     := ""
	Local aResult 	 := {}
	Local aJson      := {}
	Local nPos       := 0

	cQuery := "		SELECT HWC.HWC_PRODUT "
	cQuery += "		FROM "+RetSqlName("HWC")+" HWC "
	cQuery += "		WHERE HWC.HWC_FILIAL = '"+xFilial("HWC")+"' "
	cQuery += "		AND HWC.HWC_TICKET = '"+cTicket+"' "
	cQuery += "		AND HWC.HWC_CHAVE  = '"+cChave+"' "
	cQuery += "		AND HWC.HWC_DOCPAI = '"+cDocPai+"' "


	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQry,.F.,.F.)

	aJson := JsonObject():New()
	aJson["items"] := {}

	While (cAliasQry)->(!Eof())
		Aadd(aJson["items"],JsonObject():new())
        nPos := Len(aJson["items"])
       	aJson["items"][nPos]['componentCode'] 		:= (cAliasQry)->HWC_PRODUT
		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	If Len(aJson["items"]) > 0
		aAdd(aResult, .T.)
		aAdd(aResult, aJson:toJSON())
		aAdd(aResult, 200)
	Else
		aAdd(aResult, .F.)
		aAdd(aResult, aJson:toJSON())
		aAdd(aResult, 400)
	EndIf

	aSize(aJson["items"],0)
	FreeObj(aJson)

Return aResult

/*/{Protheus.doc} getDate
Formata uma string de data no formato AAAAMMDD para o formato AAAA-MM-DD

@type  Static Function
@author douglas.heydt
@since 17/12/2019
@version P12.1.27
@param cData, Character, Data no formato AAAAMMDD
@return cData, Character, Data no formato AAAA-MM-DD
/*/
Static Function getDate(dData)
	Local cData := ""
	cData := Left(dData, 4) + "-" + SubStr(dData, 5, 2) + "-" + Right(dData, 2)
Return cData

/*/{Protheus.doc} MrpGetProd
Consulta os produtos conforme a tabela HWB.
@type  Function
@author renan.roeder
@since 24/03/2020
@version P12.1.30
@param	cTicket  , Character, Codigo �nico do processo para fazer a pesquisa.
/*/
Function MrpGetProd(cTicket)

	Local cAliasQry  := GetNextAlias()
	Local cQuery     := ""
	Local aResult 	 := {}
	Local aJson      := {}
	Local nPos       := 0


	cQuery := " SELECT DISTINCT HWB_FILIAL, HWB_PRODUT, HWB_IDOPC, "
	cQuery += "   ( SELECT SUM(H2.HWB_QTNECE) FROM "+RetSqlName("HWB")+" H2 "
	cQuery += "       WHERE H2.HWB_FILIAL = H.HWB_FILIAL "
	cQuery += "         AND H2.HWB_TICKET = H.HWB_TICKET "
	cQuery += "         AND H2.HWB_PRODUT = H.HWB_PRODUT "
	cQuery += "         AND H2.HWB_IDOPC  = H.HWB_IDOPC "
	cQuery += "         AND H2.D_E_L_E_T_ = ' ' ) HWB_QTNECE "
	cQuery += " FROM " + RetSqlName("HWB") + " H "
	cQuery += "   WHERE H.HWB_FILIAL = '" + xFilial("HWB") + "' "
	cQuery += "     AND H.HWB_TICKET = '" + cTicket + "' "
	cQuery += "     AND H.D_E_L_E_T_ = ' ' "
	cQuery += " ORDER BY H.HWB_PRODUT,H.HWB_IDOPC "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQry,.F.,.F.)

	TcSetField(cAliasQry, 'HWB_QTNECE', 'N', _aTamQtd[1], _aTamQtd[2])

	aJson := JsonObject():New()
	aJson["items"] := {}

	While (cAliasQry)->(!Eof())

		Aadd(aJson["items"],JsonObject():new())
        nPos := Len(aJson["items"])
       	aJson["items"][nPos]['product']           := (cAliasQry)->HWB_PRODUT
		aJson["items"][nPos]['optionalId']        := (cAliasQry)->HWB_IDOPC
		aJson["items"][nPos]['necessityQuantity'] := (cAliasQry)->HWB_QTNECE
		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	If Len(aJson["items"]) > 0
		aAdd(aResult, .T.)
		aAdd(aResult, aJson:toJSON())
		aAdd(aResult, 200)
	Else
		aAdd(aResult, .F.)
		aAdd(aResult, aJson:toJSON())
		aAdd(aResult, 400)
	EndIf

	aSize(aJson["items"],0)
	FreeObj(aJson)

Return aResult


/*/{Protheus.doc} GET EVENTS api/pcp/v1/mrpresults/ticket/{ticket}
Consulta um documento do MRP na tabela de documentos aglutinados.
@type  WSMETHOD
@author douglas.heydt
@since 17/12/2019
@version P12.1.27
@param	ticket  , Character, Codigo �nico do processo para fazer a pesquisa.
/*/
WSMETHOD GET EVENTS PATHPARAM ticket WSSERVICE mrpresults
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGetLog( Self:ticket )
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
	aReturn := Nil

Return lRet


/*/{Protheus.doc} MrpGetLog

@type  WSMETHOD
@author douglas.heydt
@since
@version P12.1.27
@param	cTicket  , Character, Codigo �nico do processo para fazer a pesquisa.
/*/
Function MrpGetLog(cTicket)

	Local cAliasQry  := GetNextAlias()
	Local cQuery     := ""
	Local aResult 	 := {}
	Local aJson      := {}
	Local nPos       := 0

	cQuery :=	" SELECT HWM_FILIAL, HWM_TICKET,HWM_PRODUT, HWM_EVENTO, HWM_LOGMRP, HWM_DOC, HWM_ITEM, HWM_ALIAS, HWM_PRDORI "
	cQuery +=	" FROM "+RetSqlName("HWM")
	cQuery +=	" WHERE HWM_FILIAL = '"+xFilial("HWM")+"' "
	cQuery +=	" AND HWM_TICKET = '"+cTicket+"' "
	cQuery +=	" AND D_E_L_E_T_ = ' '"
	cQuery +=	" ORDER BY HWM_PRODUT, HWM_EVENTO"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQry,.F.,.F.)

	aJson := JsonObject():New()
	aJson["items"] := {}

	While (cAliasQry)->(!Eof())

		Aadd(aJson["items"],JsonObject():new())
        nPos := Len(aJson["items"])
       	aJson["items"][nPos]['branchId'  ] := (cAliasQry)->HWM_FILIAL
		aJson["items"][nPos]['ticket'    ] := (cAliasQry)->HWM_TICKET
		aJson["items"][nPos]['product'   ] := (cAliasQry)->HWM_PRODUT
		aJson["items"][nPos]['event'     ] := (cAliasQry)->HWM_EVENTO
		aJson["items"][nPos]['logMrp'    ] := (cAliasQry)->HWM_LOGMRP
		aJson["items"][nPos]['doc'       ] := (cAliasQry)->HWM_DOC
		aJson["items"][nPos]['logItem'   ] := (cAliasQry)->HWM_ITEM
		aJson["items"][nPos]['logAlias'  ] := (cAliasQry)->HWM_ALIAS
		aJson["items"][nPos]['productOri'] := (cAliasQry)->HWM_PRDORI
		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	If Len(aJson["items"]) > 0
		aAdd(aResult, .T.)
		aAdd(aResult, aJson:toJSON())
		aAdd(aResult, 200)
	Else
		aAdd(aResult, .F.)
		aAdd(aResult, aJson:toJSON())
		aAdd(aResult, 400)
	EndIf

	aSize(aJson["items"],0)
	FreeObj(aJson)

Return aResult
