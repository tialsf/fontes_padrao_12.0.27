#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "MRPTRANSPORTINGLANES.CH"

Static _aMapCab   := MapFields(1)
Static _aMapList  := MapFields(2)

#DEFINE LISTA_DE_EXCECOES  "listOfMRPLaneException"

//dummy function
Function MrpTL()
Return

/*/{Protheus.doc} MrpTransportingLanes
API de integra��o de Malhas de Distribui��o do MRP

@type  WSCLASS
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
/*/
WSRESTFUL mrptransportinglanes DESCRIPTION STR0001 FORMAT APPLICATION_JSON //"Malhas de Distribui��o do MRP"
	WSDATA Fields     AS STRING  OPTIONAL
	WSDATA Order      AS STRING  OPTIONAL
	WSDATA Page       AS INTEGER OPTIONAL
	WSDATA PageSize   AS INTEGER OPTIONAL
	WSDATA branchId   AS STRING  OPTIONAL
	WSDATA code    	  AS STRING  OPTIONAL

	WSMETHOD GET ALL;
		DESCRIPTION STR0002; //"Retorna todas as malhas de distribui��o do MRP"
		WSSYNTAX "api/pcp/v1/mrptransportinglanes" ;
		PATH "/api/pcp/v1/mrptransportinglanes" ;
		TTALK "v1"

	WSMETHOD GET TRANSPORTLANE;
		DESCRIPTION STR0003; //"Retorna uma malha de distribui��o espec�fica"
		WSSYNTAX "api/pcp/v1/mrptransportinglanes/{branchId}/{startWarehouseGroup}/{endWarehouseGroup}" ;
		PATH "/api/pcp/v1/mrptransportinglanes/{branchId}/{startWarehouseGroup}/{endWarehouseGroup}" ;
		TTALK "v1"

	WSMETHOD POST TRANSPORTLANE;
		DESCRIPTION STR0004; //"Inclui ou atualiza uma ou mais malhas de distribui��o do MRP"
		WSSYNTAX "api/pcp/v1/mrptransportinglanes" ;
		PATH "/api/pcp/v1/mrptransportinglanes" ;
		TTALK "v1"

	WSMETHOD POST SYNC;
		DESCRIPTION STR0005; //"Sincroniza��o de malhas de distribui��o do MRP"
		WSSYNTAX "api/pcp/v1/mrptransportinglanes/sync" ;
		PATH "/api/pcp/v1/mrptransportinglanes/sync" ;
		TTALK "v1"

	WSMETHOD DELETE TRANSPORTLANE;
		DESCRIPTION STR0006; //"Exclui uma ou mais malhas de distribui��o do MRP"
		WSSYNTAX "api/pcp/v1/mrptransportinglanes" ;
		PATH "/api/pcp/v1/mrptransportinglanes" ;
		TTALK "v1"

ENDWSRESTFUL

/*/{Protheus.doc} GET ALL/api/pcp/v1/mrptransportinglanes
Retorna todas as Malhas de Distribui��o MRP

@type  WSMETHOD
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@param	Order   , caracter, Ordena��o da tabela principal
@param	Page    , num�rico, N�mero da p�gina inicial da consulta
@param	PageSize, num�rico, N�mero de registro por p�ginas
@param	Fields  , caracter, Campos que ser�o retornados no GET.
@return lRet    , L�gico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET ALL QUERYPARAM Order, Page, PageSize, Fields WSSERVICE mrptransportinglanes
	Local aReturn := {}
	Local lRet    := .T.

	Self:SetContentType("application/json")

	aReturn := MrpTLGAll(Self:aQueryString, Self:Order, Self:Page, Self:PageSize, Self:Fields)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
Return lRet

/*/{Protheus.doc} GET TRANSPORTLANE /api/pcp/v1/mrptransportinglanes/{branchId}/{startWarehouseGroup}/{endWarehouseGroup}
Retorna uma malha de distribui��o espec�fica do MRP

@type  WSMETHOD
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@param  branchId, Character, C�digo da filial para fazer a busca
@param  product , Character, C�digo do produto para fazer a busca
@param	Fields  , Character, Campos que ser�o retornados no GET.
@return lRet    , L�gico   , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET TRANSPORTLANE PATHPARAM code QUERYPARAM Fields WSSERVICE mrptransportinglanes
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a fun��o para retornar os dados.
	aReturn := MrpTLGId(Self:code, Self:Fields)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
Return lRet

/*/{Protheus.doc} POST TRANSPORTLANE /api/pcp/v1/mrptransportinglanes
Inclui ou altera uma ou mais Malhas de Distribui��o do MRP

@type  WSMETHOD
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@return lRet    , L�gico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD POST TRANSPORTLANE WSSERVICE mrptransportinglanes
	Local aReturn  := {}
	Local cBody    := ""
	Local cError   := ""
	Local lRet     := .T.
	Local oBody    := JsonObject():New()

	Self:SetContentType("application/json")
	cBody := Self:GetContent()

	cError := oBody:FromJson(cBody)

	If cError == Nil
		aReturn := MrpTLPost(oBody)
		MRPApi():restReturn(Self, aReturn, "POST", @lRet)
	Else
		//Ocorreu erro ao transformar os dados recebidos em objeto JSON.
		lRet    := .F.
		SetRestFault(400, EncodeUtf8(STR0007), .T., , cError ) //"N�o foi poss�vel interpretar os dados recebidos."
	EndIf

	FreeObj(oBody)
	oBody := Nil
Return lRet

/*/{Protheus.doc} POST SYNC /api/pcp/v1/mrptransportinglanes/sync
Sincroniza��o de Malhas de Distribui��o

@type  WSMETHOD
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@return lRet    , L�gico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD POST SYNC WSSERVICE mrptransportinglanes
	Local aReturn  := {}
	Local cBody    := ""
	Local cError   := ""
	Local lRet     := .T.
	Local oBody    := JsonObject():New()

	Self:SetContentType("application/json")
	cBody := Self:GetContent()

	cError := oBody:FromJson(cBody)

	If cError == Nil
		aReturn := MrpTLSync(oBody)
		MRPApi():restReturn(Self, aReturn, "POST", @lRet)
	Else
		//Ocorreu erro ao transformar os dados recebidos em objeto JSON.
		lRet    := .F.
		SetRestFault(400, EncodeUtf8(STR0007), .T., , cError ) //"N�o foi poss�vel interpretar os dados recebidos."
	EndIf

	FreeObj(oBody)
	oBody := Nil
Return lRet

/*/{Protheus.doc} DELETE BOM /api/pcp/v1/mrptransportinglanes
Deleta uma ou mais Malhas de Distribui��o do MRP

@type  WSMETHOD
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@return lRet    , L�gico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD DELETE TRANSPORTLANE WSSERVICE mrptransportinglanes
	Local aReturn  := {}
	Local cBody    := ""
	Local cError   := ""
	Local lRet     := .T.
	Local oBody    := JsonObject():New()

	Self:SetContentType("application/json")
	cBody := Self:GetContent()

	cError := oBody:FromJson(cBody)

	If cError == Nil
		aReturn := MrpTLDel(oBody)
		MRPApi():restReturn(Self, aReturn, "DELETE", @lRet)
	Else
		//Ocorreu erro ao transformar os dados recebidos em objeto JSON.
		lRet    := .F.
		SetRestFault(400, EncodeUtf8(STR0007), .T., , cError ) //"N�o foi poss�vel interpretar os dados recebidos."
	EndIf

	FreeObj(oBody)
	oBody := Nil
Return lRet

/*/{Protheus.doc} MrpTLPost
Fun��o para disparar as a��es da API de Malhas de Distribui��o do MRP, para o m�todo POST (Inclus�o/Altera��o).

@type    Function
@author  douglas.heydt
@since   23/09/2019
@version P12.1.27
@oaram   oBody, JsonObject, Objeto JSON com as informa��es recebidas no corpo da requisi��o.
@return aReturn, Array, Array contendo o c�digo HTTP que dever� ser retornado pela API, e o JSON formatado com todos os dados de retorno.
                        Array[1] -> Numeric. C�digo HTTP de retorno (201 - Created; 207 - MultiStatus; 400 - Bad Request; 200 - OK)
                        Array[2] -> Character. JSON no formato definido pela API para ser retornado.
/*/
Function MrpTLPost(oBody)
	Local aReturn := {201, ""}
	Local oMRPApi := defMRPApi("POST","") //Inst�ncia da classe MRPApi para o m�todo POST

	//Adiciona os par�metros recebidos no corpo da requisi��o (BODY)
	oMRPApi:setBody(oBody)

	//Aciona flag para que a cada atualiza��o, os dados sejam deletados e reinseridos
	oMRPApi:setDelStruct(.T.)

	//Executa o processamento do POST
	oMRPApi:processar("fields")
	//Recupera o status do processamento
	aReturn[1] := oMRPApi:getStatus()
	//Recupera o JSON com os dados do retorno do processo.
	aReturn[2] := oMRPApi:getRetorno(1)

	//Libera o objeto MRPApi da mem�ria.
	oMRPApi:Destroy()
	FreeObj(oMRPApi)
	oMRPApi := Nil
Return aReturn

/*/{Protheus.doc} MrpTLSync
Fun��o para disparar as a��es da API de Malhas de Distribui��o do MRP, para o m�todo SYNC (Sincroniza��o).

@type    Function
@author  douglas.heydt
@since   23/09/2019
@version P12.1.27
@oaram   oBody, JsonObject, Objeto JSON com as informa��es recebidas no corpo da requisi��o.
@return aReturn, Array, Array contendo o c�digo HTTP que dever� ser retornado pela API, e o JSON formatado com todos os dados de retorno.
                        Array[1] -> Numeric. C�digo HTTP de retorno (201 - Created; 207 - MultiStatus; 400 - Bad Request; 200 - OK)
                        Array[2] -> Character. JSON no formato definido pela API para ser retornado.
/*/
Function MrpTLSync(oBody)
	Local aReturn := {201, ""}
	Local oMRPApi := defMRPApi("POST","") //Inst�ncia da classe MRPApi para o m�todo POST

	//Adiciona os par�metros recebidos no corpo da requisi��o (BODY)
	oMRPApi:setBody(oBody)

	//Seta FLAG que indica o processo de sincroniza��o.
	oMRPApi:setSync(.T.)

	//Executa o processamento do POST
	oMRPApi:processar("fields")
	//Recupera o status do processamento
	aReturn[1] := oMRPApi:getStatus()
	//Recupera o JSON com os dados do retorno do processo.
	aReturn[2] := oMRPApi:getRetorno(1)

	//Libera o objeto MRPApi da mem�ria.
	oMRPApi:Destroy()
	FreeObj(oMRPApi)
	oMRPApi := Nil
Return aReturn

/*/{Protheus.doc} MrpTLDel
Fun��o para disparar as a��es da API de Malhas de Distribui��o do MRP, para o m�todo DELETE (Exclus�o) de toda a estrutura.

@type  Function
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@oaram oBody, JsonObject, Objeto JSON com as informa��es recebidas no corpo da requisi��o.
@return aReturn, Array, Array contendo o c�digo HTTP que dever� ser retornado pela API, e o JSON formatado com todos os dados de retorno.
                        Array[1] -> Numeric. C�digo HTTP de retorno (201 - Created; 207 - MultiStatus; 400 - Bad Request; 200 - OK)
                        Array[2] -> Character. JSON no formato definido pela API para ser retornado.
/*/
Function MrpTLDel(oBody)
	Local aReturn  := {201, ""}
	Local oMRPApi  := defMRPApi("DELETE","") //Inst�ncia da classe MRPApi para o m�todo DELETE
	//Adiciona os par�metros recebidos no corpo da requisi��o (BODY)
	oMRPApi:setBody(oBody)
	oMRPApi:setMapDelete("fields")

	//Executa o processamento do POST
	oMRPApi:processar("fields")
	//Recupera o status do processamento
	aReturn[1] := oMRPApi:getStatus()
	//Recupera o JSON com os dados do retorno do processo.
	aReturn[2] := oMRPApi:getRetorno(1)

	//Libera o objeto MRPApi da mem�ria.
	oMRPApi:Destroy()
	FreeObj(oMRPApi)
	oMRPApi := Nil
Return aReturn

/*/{Protheus.doc} MrpTLGAll
Fun��o para disparar as a��es da API de Malhas de Distribui��o do MRP, para o m�todo GET (Consulta) para v�rias Malhas de Distribui��o.

@type  Function
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@param aQuery   , Array    , Array com os dados que devem ser filtrados.
                             Exemplo: Array[1]
                                      Array[1][1] = "PRODUCT"
                                      Array[1][2] = "PRODUTO001"
                                      Array[2]
                                      Array[2][1] = "COMPONENT"
                                      Array[2][2] = "PRODUTO002"
@param cOrder   , Character, Ordena��o desejada do retorno.
@param nPage    , Numeric  , P�gina dos dados. Se n�o enviado, considera p�gina 1.
@param nPageSize, Numeric  , Quantidade de registros retornados por p�gina. Se n�o enviado, considera 20 registros por p�gina.
@param cFields  , Character, Campos que devem ser retornados. Se n�o enviado, retorna todos os fields que possuem valor.
@return aReturn , Array    , Array com as informa��es da requisi��o.
                             aReturn[1] - L�gico    - Indica se a requisi��o foi processada com sucesso ou n�o.
						     aReturn[2] - Character - JSON com o resultado da requisi��o, ou com a mensagem de erro.
						     aReturn[3] - Numeric   - C�digo de erro identificado pelo FwApiManager.
/*/
Function MrpTLGAll(aQuery, cOrder, nPage, nPageSize, cFields)
	Local aReturn := {}

	//Processa o GET
	aReturn := TLGet(.T., aQuery, cOrder, nPage, nPageSize, cFields)
Return aReturn

/*/{Protheus.doc} MrpTLGId
Fun��o para disparar as a��es da API de Malhas de Distribui��o do MRP, para o m�todo GET (Consulta) de uma estrutura espec�fica.

@type  Function
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@param cBranch , Caracter, C�digo da filial
@param cCode, Caracter, C�digo da
@param cFields , Caracter, Campos que devem ser retornados.
@return aReturn, Array, Array com as informa��es da requisi��o.
                        aReturn[1] - L�gico    - Indica se a requisi��o foi processada com sucesso ou n�o.
						aReturn[2] - Character - JSON com o resultado da requisi��o, ou com a mensagem de erro.
						aReturn[3] - Numeric   - C�digo de erro identificado pelo FwApiManager.
/*/
Function MrpTLGId(cCode, cFields)
	Local aReturn   := {}
	Local aQryParam := {}

	//Adiciona os filtros de filial e produto como QueryParam.
	aAdd(aQryParam, {"IDREG" , cCode})

	If !Empty(cFields)
		//Adiciona o FIELDS se for recebido.
		aAdd(aQryParam, {"FIELDS", cFields})
	EndIf

	//Chama a fun��o para retornar os dados.
	aReturn := TLGet(.F., aQryParam, Nil, Nil, Nil, cFields)

Return aReturn

/*/{Protheus.doc} MapFields
Gera o array com o MAP dos fields da API e os fields da tabela HW4 e T4O

@type  Static Function
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@param nType, Numeric, Tipo da estrutura (1=Cabe�alho; 2=Componentes; 3=Alternativos)
@return aFields, Array, Array contendo o MAP dos fields da API e da tabela
/*/
Static Function MapFields(nType)
	Local aFields := {}

/*
	O array de mapeamento do JSON � composto por:
	aArray[1]
	aArray[1][1] = Nome do elemento do JSON que cont�m a informa��o.
	aArray[1][2] = Nome da coluna da tabela correspondente a informa��o.
	aArray[1][3] = Tipo de dado no banco de dados.
	aArray[1][4] = Tamanho do campo.
	aArray[1][5] = Decimais do campo, quando � do tipo Num�rico.
*/

	If nType == 1
		//Estrutura do cabe�alho
		aFields := { ;
		            {"branchId"  			, "HW4_FILIAL"	, "C", FWSizeFilial()                      		, 0},;
		            {"startWarehouseGroup"  , "HW4_ARMORG"  , "C", GetSx3Cache("HW4_ARMORG","X3_TAMANHO")	, 0},;
					{"branchIdDest"   		, "HW4_FILDES" , "C", FWSizeFilial()                      		, 0},;
					{"endWarehouseGroup"  	, "HW4_ARMDES"  , "C", GetSx3Cache("HW4_ARMDES","X3_TAMANHO")	, 0},;
		            {"code"					, "HW4_IDREG"  	, "C", GetSx3Cache("HW4_IDREG","X3_TAMANHO")	, 0};
		           }
	ElseIf nType == 2
		//Estrutura do cabe�alho
		aFields := { ;
		            {"product"  	, "HW5_PROD"	, "C", GetSx3Cache("HW5_PROD" ,"X3_TAMANHO"), 0},;
		            {"startDate"   	, "HW5_DTINI"  	, "D", GetSx3Cache("HW5_DTINI","X3_TAMANHO"), 0},;
					{"endDate"  	, "HW5_DTFIN"  	, "D", GetSx3Cache("HW5_DTFIN","X3_TAMANHO"), 0},;
					{"startQuantity", "HW5_QTDINI"  	, "N", GetSx3Cache("HW5_QTDINI","X3_TAMANHO"), GetSx3Cache("HW5_QTDINI","X3_DECIMAL")},;
		            {"endQuantity"	, "HW5_QTDFIN"  	, "N", GetSx3Cache("HW5_QTDFIN","X3_TAMANHO"), GetSx3Cache("HW5_QTDFIN","X3_DECIMAL")};
		           }
	EndIf
Return aFields

/*/{Protheus.doc} TLGet
Executa o processamento do m�todo GET de acordo com os par�metros recebidos.

@type  Static Function
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@param lLista   , Logic    , Indica se dever� retornar uma lista de registros (.T.), ou apenas um registro (.F.).
@param aQuery   , Array    , Array com os dados que devem ser filtrados.
                             Exemplo: Array[1]
                                      Array[1][1] = "PRODUCT"
                                      Array[1][2] = "PRODUTO001"
                                      Array[2]
                                      Array[2][1] = "COMPONENT"
                                      Array[2][2] = "PRODUTO002"
@param cOrder   , Character, Ordena��o desejada do retorno.
@param nPage    , Numeric  , P�gina dos dados. Se n�o enviado, considera p�gina 1.
@param nPageSize, Numeric  , Quantidade de registros retornados por p�gina. Se n�o enviado, considera 20 registros por p�gina.
@param cFields  , Character, Campos que devem ser retornados. Se n�o enviado, retorna todos os fields que possuem valor.
@return aReturn , Array    , Array com as informa��es da requisi��o.
                             aReturn[1] - L�gico    - Indica se a requisi��o foi processada com sucesso ou n�o.
						     aReturn[2] - Character - JSON com o resultado da requisi��o, ou com a mensagem de erro.
						     aReturn[3] - Numeric   - C�digo de erro identificado pelo FwApiManager.
/*/
Static Function TLGet(lLista, aQuery, cOrder, nPage, nPageSize, cFields)
	Local aReturn := {.T.,"",200}
	Local oMRPApi := defMRPApi("GET",cOrder) //Inst�ncia da classe MRPApi para o m�todo GET

	//Seta os par�metros de pagina��o, filtros e campos para retorno
	oMRPApi:setFields(cFields)
	oMRPApi:setPage(nPage)
	oMRPApi:setPageSize(nPageSize)
	oMRPApi:setQueryParams(aQuery)
	oMRPApi:setUmRegistro(!lLista)

	//Executa o processamento
	aReturn[1] := oMRPApi:processar("fields")
	//Retorna o status do processamento
	aReturn[3] := oMRPApi:getStatus()
	If aReturn[1]
		//Se processou com sucesso, recupera o JSON com os dados.
		aReturn[2] := oMRPApi:getRetorno(1)
	Else
		//Ocorreu algum erro no processo. Recupera mensagem de erro.
		aReturn[2] := oMRPApi:getMessage()
	EndIf

	//Libera o objeto MRPApi da mem�ria.
	oMRPApi:Destroy()
	FreeObj(oMRPApi)
	oMRPApi := Nil
Return aReturn

/*/{Protheus.doc} MRPTLVLD
Fun��o respons�vel por validar as informa��es recebidas.

@type  Function
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@param oMRPApi   , Object   , Refer�ncia da classe MRPApi que est� processando os dados.
@param cMapCode  , Character, C�digo do mapeamento que ser� validado
@param oItem     , Object   , Refer�ncia do objeto JSON com os dados que devem ser validados.
@return lRet     , L�gico   , Identifica se os dados est�o v�lidos.
/*/
Function MRPTLVLD(oMRPApi, cMapCode, oItem)
	Local lRet := .T.

	Do Case
		Case cMapCode == "fields"
			lRet := validaCab(oMRPApi, oItem)
		Case cMapCode == LISTA_DE_EXCECOES
			lRet := vldExce(oMRPApi, oItem)
	EndCase
Return lRet

/*/{Protheus.doc} validaCab
Faz a valida��o do cabe�alho.

@type  Static Function
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@param oMRPApi, Object    , Refer�ncia da classe MRPAPI que est� executando o processo.
@param oItem  , JsonObject, Objeto JSON do item que ser� validado
@return lRet  , Logical   , Indica se o item poder� ser processado.
/*/
Static Function validaCab(oMRPApi, oItem)
	Local lRet      := .T.

	If Empty(oItem["branchId"])
		lRet     := .F.
		oMRPApi:SetError(400, "STR0010" + " 'branchId' " + "STR0014") //"Atributo 'XXX' n�o foi informado."
	EndIf

	If lRet .And. Empty(oItem["startWarehouseGroup"])
		lRet     := .F.
		oMRPApi:SetError(400, "STR0010" + " 'startWarehouseGroup' " + "STR0014") //
	EndIf

	If lRet .And. Empty(oItem["branchIdDest"])
		lRet     := .F.
		oMRPApi:SetError(400, "STR0010" + " 'branchIdDest' " + "STR0014") //
	EndIf

	If lRet .And. Empty(oItem["endWarehouseGroup"])
		lRet     := .F.
		oMRPApi:SetError(400, "STR0010" + " 'endWarehouseGroup' " + "STR0014") //
	EndIf

	If lRet .And. Empty(oItem["code"])
		lRet     := .F.
		oMRPApi:SetError(400, "STR0010" + " 'code' " + "STR0014") //
	EndIf

Return lRet

/*/{Protheus.doc} vldCompons
Executa as valida��es referentes � lista de componentes para um determinado item.

@type  Static Function
@author douglas.heydt
@since 23/09/2019
@version P12.1.27
@param oMRPApi , Object    , Refer�ncia da classe MRPAPI que est� executando o processo.
@param oList, JsonObject, Objeto JSON do item que ser� validado
@return lRet   , L�gico    , Identifica se os dados est�o v�lidos.
/*/
Static Function vldExce(oMRPApi, oList)
	Local lRet   := .T.

	//C�digo �nico do componente
	If lRet .And. Empty(oList["product"])
		lRet := .F.
		oMRPApi:SetError(400, "STR0010" + " 'product' " + "STR0014") //"Atributo 'XXX' n�o foi informado."
	EndIf
	//C�digo do componente preenchido.
	If lRet .And. Empty(oList["startQuantity"])
		lRet := .F.
		oMRPApi:SetError(400, "STR0010" + " 'startQuantity' " + "STR0014") //"Atributo 'XXX' n�o foi informado."
	EndIf

	//Quantidade necess�ria preenchida
	If lRet .And. Empty(oList["endQuantity"])
		lRet := .F.
		oMRPApi:SetError(400, "STR0010" + " 'endQuantity' " + "STR0014") //"Atributo 'XXX' n�o foi informado."
	EndIf

Return lRet



/*/{Protheus.doc} defMRPApi
Faz a inst�ncia da classe MRPAPI e seta as propriedades b�sicas.

@type  Static Function
@author douglas.heydt
@since 06/06/2019
@version P12.1.27
@param cMethod  , Character, M�todo que ser� executado (GET/POST/DELETE)
@param cOrder   , Character, Ordena��o para o GET
@return oMRPApi , Object   , Refer�ncia da classe MRPApi com as defini��es j� executadas.
/*/
Static Function defMRPApi(cMethod, cOrder)
	Local aRelac  := {}
	Local oMRPApi := MRPApi():New(cMethod)

	//Seta o APIMAP do item principal (cabe�alho)
	oMRPApi:setAPIMap("fields"          , _aMapCab , "HW4", .T., cOrder)
	//Seta o APIMAP da lista de exce��es
	oMRPApi:setAPIMap(LISTA_DE_EXCECOES , _aMapList, "HW5", .F., cOrder)

	//Adiciona o relacionamento entre o cabe�alho e a lista de exce��es
	aRelac := {}
	aAdd(aRelac, {"HW4_FILIAL", "HW5_FILIAL"})
	aAdd(aRelac, {"HW4_IDREG"  , "HW5_IDMAL"  })
	oMRPApi:setMapRelation("fields", LISTA_DE_EXCECOES, aRelac, .F.)

	//Seta os campos utilizados para busca de registros.
	oMRPApi:setKeySearch("fields"          ,{"HW4_FILIAL","HW4_IDREG"})
	oMRPApi:setKeySearch(LISTA_DE_EXCECOES ,{"HW5_FILIAL","HW5_IDMAL","HW5_PROD","HW5_DTINI","HW5_DTFIN","HW5_QTDINI","HW5_QTDFIN"})

	If cMethod == "POST"
		//Seta as fun��es de valida��o de cada mapeamento.
		oMRPApi:setValidData("fields"          , "MRPTLVLD")
		oMRPApi:setValidData(LISTA_DE_EXCECOES , "MRPTLVLD")
	EndIf

Return oMRPApi

/*/{Protheus.doc} MrpTLMap
Retorna um array com todos os MapFields utilizados na API

@type    Function
@author  marcelo.neumann
@since   18/10/2019
@version P12.1.27
@return  Array, array com os arrays de MapFields
/*/
Function MrpTLMap()
Return {_aMapCab, _aMapList}