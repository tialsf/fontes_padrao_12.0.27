#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "MRPWarehouseGroup.CH"

Static _aMapCab  := MapFields(1)
Static _aMapList := MapFields(2)

#DEFINE LISTA_DE_ARMAZENS "listOfMRPWarehouse"

//dummy function
Function MrpGrpAmaz()
Return

/*/{Protheus.doc} MRPWarehouseGroup
API de integracao de Grupos de Armaz�m

@type WSCLASS
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
/*/
WSRESTFUL mrpwarehousegroup DESCRIPTION STR0001 FORMAT APPLICATION_JSON //"Grupo de Armaz�m"
	WSDATA Fields         AS STRING  OPTIONAL
	WSDATA Order          AS STRING  OPTIONAL
	WSDATA Page           AS INTEGER OPTIONAL
	WSDATA PageSize       AS INTEGER OPTIONAL
	WSDATA branchId       AS STRING  OPTIONAL
	WSDATA warehouseGroup AS STRING  OPTIONAL

	WSMETHOD GET ALL;
		DESCRIPTION STR0002; //"Retorna todos os Grupos de Armaz�m"
		WSSYNTAX "api/pcp/v1/mrpwarehousegroup" ;
		PATH "/api/pcp/v1/mrpwarehousegroup" ;
		TTALK "v1"

	WSMETHOD GET WAREHOUSEGROUP;
		DESCRIPTION STR0003; //"Retorna um Grupo de Armaz�m especifico"
		WSSYNTAX "api/pcp/v1/mrpwarehousegroup/{branchId}/{warehouseGroup}" ;
		PATH "/api/pcp/v1/mrpwarehousegroup/{branchId}/{warehouseGroup}" ;
		TTALK "v1"

	WSMETHOD POST WAREHOUSEGROUP;
		DESCRIPTION STR0004; //"Inclui ou atualiza um ou mais Grupos de Armaz�m"
		WSSYNTAX "api/pcp/v1/mrpwarehousegroup" ;
		PATH "/api/pcp/v1/mrpwarehousegroup" ;
		TTALK "v1"

	WSMETHOD POST SYNC;
		DESCRIPTION STR0012; //"Sincroniza os Grupos de Armaz�m"
		WSSYNTAX "api/pcp/v1/mrpwarehousegroup/sync" ;
		PATH "/api/pcp/v1/mrpwarehousegroup/sync" ;
		TTALK "v1"

	WSMETHOD DELETE WAREHOUSEGROUP;
		DESCRIPTION STR0005; //"Exclui um ou mais Grupos de Armaz�m"
		WSSYNTAX "api/pcp/v1/mrpwarehousegroup" ;
		PATH "/api/pcp/v1/mrpwarehousegroup" ;
		TTALK "v1"
ENDWSRESTFUL

/*/{Protheus.doc} GET ALL /api/pcp/v1/mrpwarehousegroup
Retorna todos os Grupos de Armaz�m

@type WSMETHOD
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@param Order   , caracter, Ordenacao da tabela principal
@param Page    , numerico, N�mero da pagina inicial da consulta
@param PageSize, numerico, N�mero de registro por paginas
@param Fields  , caracter, Campos que ser�o retornados no GET.
@return lRet   , Logico  , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET ALL QUERYPARAM Order, Page, PageSize, Fields WSSERVICE mrpwarehousegroup
	Local aReturn := {}
	Local lRet    := .T.

	Self:SetContentType("application/json")

	aReturn := MrpGrAGAll(Self:aQueryString, Self:Order, Self:Page, Self:PageSize, Self:Fields)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
Return lRet

/*/{Protheus.doc} GET WAREHOUSEGROUP /api/pcp/v1/mrpwarehousegroup/{branchId}/{warehouseGroup}
Retorna um Grupo de Armaz�m especifico

@type WSMETHOD
@author marcelo.neumann
@since 19/06/2019
@version P12.1.28
@param branchId, Character, Codigo da filial para fazer a pesquisa
@param cGroup  , Character, C�digo do grupo de armaz�m
@param Fields  , Character, Campos que ser�o retornados no GET.
@return lRet   , Logico   , Informa se o processo foi executado com sucesso.
/*/
WSMETHOD GET WAREHOUSEGROUP PATHPARAM branchId, warehouseGroup QUERYPARAM Fields WSSERVICE mrpwarehousegroup
	Local aReturn   := {}
	Local lRet      := .T.

	Self:SetContentType("application/json")

	//Chama a funcao para retornar os dados.
	aReturn := MrpGrAGet(Self:branchId, Self:warehouseGroup, Self:Fields)
	MRPApi():restReturn(Self, aReturn, "GET", @lRet)
Return lRet

/*/{Protheus.doc} POST WAREHOUSEGROUP /api/pcp/v1/mrpwarehousegroup
Inclui ou altera um ou mais Grupos de Armaz�m

@type WSMETHOD
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@return lRet, Logico, Informa se o processo foi executado com sucesso.
/*/
WSMETHOD POST WAREHOUSEGROUP WSSERVICE mrpwarehousegroup
	Local aReturn  := {}
	Local cBody    := ""
	Local cError   := ""
	Local lRet     := .T.
	Local oBody    := JsonObject():New()

	Self:SetContentType("application/json")
	cBody := Self:GetContent()

	cError := oBody:FromJson(cBody)

	If cError == Nil
		aReturn := MrpGrAPost(oBody)
		MRPApi():restReturn(Self, aReturn, "POST", @lRet)
	Else
		//Ocorreu erro ao transformar os dados recebidos em objeto JSON.
		lRet    := .F.
		SetRestFault(400, EncodeUtf8(STR0006), .T., , cError ) //"N�o foi poss�vel interpretar os dados recebidos."
	EndIf

	FreeObj(oBody)
	oBody := Nil
Return lRet

/*/{Protheus.doc} POST SYNC /api/pcp/v1/mrpwarehousegroup/sync
Sincroniza��o de Grupos de Armaz�m (Apaga todos os registros existentes na base, e inclui os recebidos na requisi��o)

@type WSMETHOD
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@return lRet, L�gico, Informa se o processo foi executado com sucesso.
/*/
WSMETHOD POST SYNC WSSERVICE mrpwarehousegroup
	Local aReturn  := {}
	Local cBody    := ""
	Local cError   := ""
	Local lRet     := .T.
	Local oBody    := JsonObject():New()

	Self:SetContentType("application/json")
	cBody := Self:GetContent()

	cError := oBody:FromJson(cBody)

	If cError == Nil
		aReturn := MrpGrASync(oBody)
		MRPApi():restReturn(Self, aReturn, "POST", @lRet)
	Else
		//Ocorreu erro ao transformar os dados recebidos em objeto JSON.
		lRet    := .F.
		SetRestFault(400, EncodeUtf8(STR0006), .T., , cError ) //"N�o foi poss�vel interpretar os dados recebidos."
	EndIf

	FreeObj(oBody)
	oBody := Nil

Return lRet

/*/{Protheus.doc} DELETE WAREHOUSEGROUP /api/pcp/v1/mrpwarehousegroup
Deleta um ou mais Grupos de Armaz�m

@type WSMETHOD
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@return lRet, Logico, Informa se o processo foi executado com sucesso.
/*/
WSMETHOD DELETE WAREHOUSEGROUP WSSERVICE mrpwarehousegroup
	Local aReturn  := {}
	Local cBody    := ""
	Local cError   := ""
	Local lRet     := .T.
	Local oBody    := JsonObject():New()

	Self:SetContentType("application/json")
	cBody := Self:GetContent()

	cError := oBody:FromJson(cBody)

	If cError == Nil
		aReturn := MrpGrADel(oBody)
		MRPApi():restReturn(Self, aReturn, "DELETE", @lRet)
	Else
		//Ocorreu erro ao transformar os dados recebidos em objeto JSON.
		lRet    := .F.
		SetRestFault(400, EncodeUtf8(STR0006), .T., , cError ) //"Nao foi poss�vel interpretar os dados recebidos."
	EndIf

	FreeObj(oBody)
	oBody := Nil
Return lRet

/*/{Protheus.doc} MrpGrAPost
Dispara as acoes da API de Grupos de Armaz�m, para o metodo POST (Inclusao/Alteracao).

@type Function
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@oaram oBody, JsonObject, Objeto JSON com as informacoes recebidas no corpo da requisicao.
@return aReturn, Array, Array contendo o Codigo HTTP que devera ser retornado pela API, e o JSON formatado com todos os dados de retorno.
                        Array[1] -> Numeric. Codigo HTTP de retorno (201 - Created; 207 - MultiStatus; 400 - Bad Request; 200 - OK)
                        Array[2] -> Character. JSON no formato definido pela API para ser retornado.
/*/
Function MrpGrAPost(oBody)
	Local aReturn := {201, ""}
	Local oMRPApi := defMRPApi("POST","") //Instancia da classe MRPApi para o metodo POST

	//Adiciona os parametros recebidos no corpo da requisicao (BODY)
	oMRPApi:setBody(oBody)

	//Seta para deletar os registros antes de fazer o POST
	oMRPApi:setDelStruct(.T.)

	//Executa o processamento do POST
	oMRPApi:processar("fields")

	//Recupera o status do processamento
	aReturn[1] := oMRPApi:getStatus()

	//Recupera o JSON com os dados do retorno do processo.
	aReturn[2] := oMRPApi:getRetorno(1)

	//Libera o objeto MRPApi da memoria.
	oMRPApi:Destroy()
	FreeObj(oMRPApi)
	oMRPApi := Nil
Return aReturn

/*/{Protheus.doc} MrpGrASync
Fun��o para disparar as a��es da API de Grupos de Armaz�m, para o m�todo Sync (Sincroniza��o).

@type Function
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@oaram oBody, JsonObject, Objeto JSON com as informa��es recebidas no corpo da requisi��o.
@return aReturn, Array, Array contendo o c�digo HTTP que dever� ser retornado pela API, e o JSON formatado com todos os dados de retorno.
                        Array[1] -> Numeric. C�digo HTTP de retorno (201 - Created; 207 - MultiStatus; 400 - Bad Request; 200 - OK)
                        Array[2] -> Character. JSON no formato definido pela API para ser retornado.
/*/
Function MrpGrASync(oBody)
	Local aReturn := {201, ""}
	Local oMRPApi := defMRPApi("POST","") //Inst�ncia da classe MRPApi para o m�todo POST

	//Adiciona os par�metros recebidos no corpo da requisi��o (BODY)
	oMRPApi:setBody(oBody)

	//Seta FLAG que indica o processo de sincroniza��o.
	oMRPApi:setSync(.T.)

	//Executa o processamento do POST
	oMRPApi:processar("fields")
	//Recupera o status do processamento
	aReturn[1] := oMRPApi:getStatus()
	//Recupera o JSON com os dados do retorno do processo.
	aReturn[2] := oMRPApi:getRetorno(1)

	//Libera o objeto MRPApi da mem�ria.
	oMRPApi:Destroy()
	FreeObj(oMRPApi)
	oMRPApi := Nil
Return aReturn

/*/{Protheus.doc} MrpGrADel
Dispara as acoes da API de Grupos de Armaz�m, para o metodo DELETE (Exclusao).

@type Function
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@oaram oBody, JsonObject, Objeto JSON com as informacoes recebidas no corpo da requisicao.
@return aReturn, Array, Array contendo o Codigo HTTP que devera ser retornado pela API, e o JSON formatado com todos os dados de retorno.
                        Array[1] -> Numeric. Codigo HTTP de retorno (201 - Created; 207 - MultiStatus; 400 - Bad Request; 200 - OK)
                        Array[2] -> Character. JSON no formato definido pela API para ser retornado.
/*/
Function MrpGrADel(oBody)
	Local aReturn  := {201, ""}
	Local oMRPApi  := defMRPApi("DELETE","") //Instancia da classe MRPApi para o metodo DELETE

	//Adiciona os parametros recebidos no corpo da requisicao (BODY)
	oMRPApi:setBody(oBody)

	oMRPApi:setMapDelete("fields")

	//Executa o processamento do POST
	oMRPApi:processar("fields")

	//Recupera o status do processamento
	aReturn[1] := oMRPApi:getStatus()

	//Recupera o JSON com os dados do retorno do processo.
	aReturn[2] := oMRPApi:getRetorno(1)

	//Libera o objeto MRPApi da memoria.
	oMRPApi:Destroy()
	FreeObj(oMRPApi)
	oMRPApi := Nil
Return aReturn

/*/{Protheus.doc} MrpGrAGAll
Dispara as acoes da API de Grupos de Armaz�m, para o metodo GET (Consulta) para varios Calend�rios.

@type Function
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@param aQuery   , Array    , Array com os dados que devem ser filtrados.
                             Exemplo: Array[1]
                                      Array[1][1] = "PRODUCT"
                                      Array[1][2] = "PRODUTO001"
                                      Array[2]
                                      Array[2][1] = "PRODUCTIONORDER"
                                      Array[2][2] = "00000101001"
@param cOrder   , Character, Ordenacao desejada do retorno.
@param nPage    , Numeric  , Pagina dos dados. Se Nao enviado, considera pagina 1.
@param nPageSize, Numeric  , Quantidade de registros retornados por pagina. Se Nao enviado, considera 20 registros por pagina.
@param cFields  , Character, Campos que devem ser retornados. Se Nao enviado, retorna todos os fields que possuem valor.
@return aReturn , Array    , Array com as informacoes da requisicao.
                             aReturn[1] - Logico    - Indica se a requisicao foi processada com sucesso ou Nao.
						     aReturn[2] - Character - JSON com o resultado da requisicao, ou com a mensagem de erro.
						     aReturn[3] - Numeric   - Codigo de erro identificado pelo MRPApi.
/*/
Function MrpGrAGAll(aQuery, cOrder, nPage, nPageSize, cFields)
	Local aReturn := {}

	//Processa o GET
	aReturn := GrAGet(.T., aQuery, cOrder, nPage, nPageSize, cFields)
Return aReturn

/*/{Protheus.doc} MrpGrAGet
Funcao para disparar as acoes da API de Grupos de Armaz�m, para o metodo GET (Consulta) de um grupo de armza�m especifico.

@type Function
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@param cBranch , Caracter, Codigo da filial
@param cGroup  , Caracter, C�digo do grupo de armaz�m
@param cFields , Caracter, Campos que devem ser retornados.
@return aReturn, Array, Array com as informacoes da requisicao.
                        aReturn[1] - Logico    - Indica se a requisicao foi processada com sucesso ou Nao.
						aReturn[2] - Character - JSON com o resultado da requisicao, ou com a mensagem de erro.
						aReturn[3] - Numeric   - Codigo de erro identificado pelo MRPApi.
/*/
Function MrpGrAGet(cBranch, cGroup, cFields)
	Local aReturn   := {}
	Local aQryParam := {}

	//Adiciona os filtros de filial e produto como QueryParam.
	aAdd(aQryParam, {"BRANCHID"      , cBranch})
	aAdd(aQryParam, {"WAREHOUSEGROUP", cGroup})

	If !Empty(cFields)
		//Adiciona o FIELDS se for recebido.
		aAdd(aQryParam, {"FIELDS", cFields})
	EndIf

	//Chama a funcao para retornar os dados.
	aReturn := GrAGet(.F., aQryParam, Nil, Nil, Nil, cFields)

Return aReturn

/*/{Protheus.doc} MapFields
Gera o array com o MAP dos fields da API e os fields da tabela HW0

@type Function
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@param nType, Numeric, Tipo da estrutura (1=Cabe�alho (Grupo); 2=Lista (Armaz�ns))
@return aFields, Array, Array contendo o MAP dos fields da API e da tabela
/*/
Static Function MapFields(nType)
	Local aFields := {}

/*
	O array de mapeamento do JSON e composto por:
	aArray[1]
	aArray[1][1] = Nome do elemento do JSON que contem a informacao.
	aArray[1][2] = Nome da coluna da tabela correspondente a informacao.
	aArray[1][3] = Tipo de dado no banco de dados.
	aArray[1][4] = Tamanho do campo.
	aArray[1][5] = Decimais do campo, quando e do tipo numerico.
*/
	If nType == 1
		//Estrutura do cabe�alho
		aFields := { ;
					{"branchId"        , "HW6_FILIAL", "C", FWSizeFilial()                        , 0},;
					{"warehouseGroup"  , "HW6_GRPARM", "C", GetSx3Cache("HW6_GRPARM","X3_TAMANHO"), 0},;
					{"typeGroup"       , "HW6_TPGRP" , "C", GetSx3Cache("HW6_TPGRP" ,"X3_TAMANHO"), 0} ;
				}
	Else
		//Estrutura da lista de armaz�ns
		aFields := { ;
					{"warehouse"       , "HW6_LOCAL" , "C", GetSx3Cache("HW6_LOCAL" ,"X3_TAMANHO"), 0},;
					{"warehouseDefault", "HW6_DEFAUL", "C", GetSx3Cache("HW6_DEFAUL","X3_TAMANHO"), 0},;
					{"code"            , "HW6_IDREG" , "C", GetSx3Cache("HW6_IDREG" ,"X3_TAMANHO"), 0} ;
		           }
	EndIf
Return aFields

/*/{Protheus.doc} GrAGet
Executa o processamento do metodo GET de acordo com os parametros recebidos.

@type Function
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@param lLista   , Logic    , Indica se devera retornar uma lista de registros (.T.), ou apenas um registro (.F.).
@param aQuery   , Array    , Array com os dados que devem ser filtrados.
                             Exemplo: Array[1]
                                      Array[1][1] = "warehousegroup"
                                      Array[1][2] = "0001"
@param cOrder   , Character, Ordenacao desejada do retorno.
@param nPage    , Numeric  , Pagina dos dados. Se Nao enviado, considera pagina 1.
@param nPageSize, Numeric  , Quantidade de registros retornados por pagina. Se Nao enviado, considera 20 registros por pagina.
@param cFields  , Character, Campos que devem ser retornados. Se Nao enviado, retorna todos os fields que possuem valor.
@return aReturn , Array    , Array com as informacoes da requisicao.
                             aReturn[1] - Logico    - Indica se a requisicao foi processada com sucesso ou Nao.
						     aReturn[2] - Character - JSON com o resultado da requisicao, ou com a mensagem de erro.
						     aReturn[3] - Numeric   - Codigo de erro identificado pelo MRPApi.
/*/
Static Function GrAGet(lLista, aQuery, cOrder, nPage, nPageSize, cFields)
	Local aReturn := {.T.,"",200}
	Local oMRPApi := defMRPApi("GET",cOrder) //Instancia da classe MRPApi para o metodo GET

	//Seta os parametros de paginacao, filtros e campos para retorno
	oMRPApi:setFields(cFields)
	oMRPApi:setPage(nPage)
	oMRPApi:setPageSize(nPageSize)
	oMRPApi:setQueryParams(aQuery)
	oMRPApi:setUmRegistro(!lLista)

	//Executa o processamento
	aReturn[1] := oMRPApi:processar("fields")
	//Retorna o status do processamento
	aReturn[3] := oMRPApi:getStatus()
	If aReturn[1]
		//Se processou com sucesso, recupera o JSON com os dados.
		aReturn[2] := oMRPApi:getRetorno(1)
	Else
		//Ocorreu algum erro no processo. Recupera mensagem de erro.
		aReturn[2] := oMRPApi:getMessage()
	EndIf

	//Libera o objeto MRPApi da memoria.
	oMRPApi:Destroy()
	FreeObj(oMRPApi)
	oMRPApi := Nil
Return aReturn

/*/{Protheus.doc} MrpGrAVLD
Funcao responsavel por validar as informacoes recebidas.

@type Function
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@param oMRPApi , Object   , Refer�ncia da classe MRPApi que esta processando os dados.
@param cMapCode, Character, Codigo do mapeamento que sera validado
@param oItem   , Object   , Refer�ncia do objeto JSON com os dados que devem ser validados.
@return lRet   , Logico   , Identifica se os dados est�o validos.
/*/
Function MrpGrAVLD(oMRPApi, cMapCode, oItem, oPai)
	Local lRet := .T.

	If cMapCode == "fields"
		If Empty(oItem["branchId"])
			oMRPApi:SetError(400, STR0007 + " 'branchId' " + STR0008) //"Atributo 'XXX' n�o foi informado."
			Return .F.
		EndIf

		If Empty(oItem["warehouseGroup"])
			oMRPApi:SetError(400, STR0007 + " 'warehouseGroup' " + STR0008) //"Atributo 'XXX' n�o foi informado."
			Return .F.
		EndIf

		If Empty(oItem["typeGroup"])
			oMRPApi:SetError(400, STR0007 + " 'typeGroup' " + STR0008) //"Atributo 'XXX' n�o foi informado."
			Return .F.
		Else
			//typeGroup: 1 (Filial) / 2 (Armaz�m)
			If oItem["typeGroup"] == "1"
				If Len(oItem["listOfMRPWarehouse"]) > 1
					oMRPApi:SetError(400, STR0007 + " 'listOfMRPWarehouse' " + STR0009) //"Atributo 'XXX' deve ser enviado somente com um 'code' quando 'typeGroup' for '1'"
					Return .F.
				EndIf
			Else
				If oItem["typeGroup"] <> "2"
					oMRPApi:SetError(400, STR0007 + " 'typeGroup' " + STR0011) //"Atributo 'XXX' deve ser preenchido com '1' para Filial e '2' para Armaz�m."
					Return .F.
				EndIf
			EndIf
		EndIf

	ElseIf cMapCode == LISTA_DE_ARMAZENS
		//C�digo �nico do registro
		If Empty(oItem["code"])
			lRet := .F.
			oMRPApi:SetError(400, STR0007 + " 'code' " + STR0008) //"Atributo 'XXX' n�o foi informado."
		EndIf

		If oPai != Nil
			//typeGroup: 1 (Filial) / 2 (Armaz�m)
			If oPai["typeGroup"] <> "1"
				If Empty(oItem["warehouse"])
					oMRPApi:SetError(400, STR0007 + " 'warehouse' " + STR0008) //"Atributo 'XXX' n�o foi informado."
					Return .F.
				EndIf

				If Empty(oItem["warehouseDefault"])
					oMRPApi:SetError(400, STR0007 + " 'warehouseDefault' " + STR0008) //"Atributo 'XXX' n�o foi informado."
					Return .F.
				Else
					If oItem["warehouseDefault"] <> '1' .And. oItem["warehouseDefault"] <> '2'
						oMRPApi:SetError(400, STR0007 + " 'warehouseDefault' " + STR0010) //"Atributo 'XXX' deve ser preenchido com '1' para Sim e '2' para N�o."
						Return .F.
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} defMRPApi
Faz a instancia da classe MRPAPI e seta as propriedades basicas.

@type Static Function
@author marcelo.neumann
@since 19/09/2019
@version P12.1.28
@param cMethod , Character, Metodo que sera executado (GET/POST/DELETE)
@param cOrder  , Character, Ordenacao para o GET
@return oMRPApi, Object   , Refer�ncia da classe MRPApi com as definicoes ja executadas.
/*/
Static Function defMRPApi(cMethod, cOrder)
	Local oMRPApi := MRPApi():New(cMethod)

	//Seta o APIMAP do item principal (cabe�alho)
	oMRPApi:setAPIMap("fields"         , _aMapCab , "HW6", .T., "branchId,warehouseGroup")
	//Seta o APIMAP da lista de componentes
	oMRPApi:setAPIMap(LISTA_DE_ARMAZENS, _aMapList, "HW6", .F., cOrder)

	//Adiciona o relacionamento entre o cabe�alho e a lista
	aRelac := {}
	aAdd(aRelac, {"HW6_FILIAL", "HW6_FILIAL"})
	aAdd(aRelac, {"HW6_GRPARM", "HW6_GRPARM"})
	oMRPApi:setMapRelation("fields", LISTA_DE_ARMAZENS, aRelac, .T.)

	//Seta os campos utilizados para busca de registros.
	oMRPApi:setKeySearch("fields"         , {"HW6_FILIAL","HW6_GRPARM"})
	oMRPApi:setKeySearch(LISTA_DE_ARMAZENS, {"HW6_FILIAL","HW6_IDREG" })

	If cMethod == "POST"
		//Seta as funcoes de validacao de cada mapeamento.
		oMRPApi:setValidData("fields"         , "MrpGrAVLD")
		oMRPApi:setValidData(LISTA_DE_ARMAZENS, "MrpGrAVLD")
	EndIf
Return oMRPApi

/*/{Protheus.doc} MrpGrAMap
Retorna um array com todos os MapFields utilizados na API

@type    Function
@author  marcelo.neumann
@since   18/10/2019
@version P12.1.27
@return  Array, array com os arrays de MapFields
/*/
Function MrpGrAMap()
Return {_aMapCab, _aMapList}