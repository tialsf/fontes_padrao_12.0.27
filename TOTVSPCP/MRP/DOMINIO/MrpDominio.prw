#INCLUDE 'protheus.ch'
#INCLUDE 'MRPDominio.ch'

#DEFINE ABAIXA_POS_CHAVE              1
#DEFINE ABAIXA_POS_DOCPAI             2
#DEFINE ABAIXA_POS_NECESSIDADE        3
#DEFINE ABAIXA_POS_QTD_ESTOQUE        4
#DEFINE ABAIXA_POS_CONSUMO_ESTOQUE    5
#DEFINE ABAIXA_POS_QTD_SUBSTITUICAO   6
#DEFINE ABAIXA_POS_QUEBRAS_QUANTIDADE 7
#DEFINE ABAIXA_POS_TIPO_PAI           8
#DEFINE ABAIXA_POS_NEC_ORIG           9
#DEFINE ABAIXA_POS_REGRA_ALT         10
#DEFINE ABAIXA_POS_CHAVE_SUBST       11
#DEFINE ABAIXA_SIZE                  11

#DEFINE aRevisoes_POS_REVISAO     1
#DEFINE aRevisoes_POS_ROTEIRO     2
#DEFINE aRevisoes_POS_LOCAL       3
#DEFINE aRevisoes_POS_QTDE_TOTAL  4
#DEFINE aRevisoes_POS_aBaixaPorOP 5
#DEFINE aRevisoes_POS_VersaoProd  6

Static soDominio     := Nil             //Instancia da camada de dominio
Static soAlternativo := Nil
Static slRecria      := .T.

/*/{Protheus.doc} MrpDominio
Regras de Negocio - Processamento Calculo do MRP
@author    brunno.costa
@since     25/04/2019
@version   1
/*/
CLASS MrpDominio FROM LongClassName

	//Declaracao de propriedades da classe
	DATA aPeriodos          AS ARRAY    //Array contendo os periodos de calculos
	DATA oAlternativo       AS OBJECT   //Classe para regras de Alternativos
	DATA oAglutina          AS OBJECT   //Classe para regras de Aglutina��o
	DATA oDados             AS OBJECT   //Instancia da camada de dados
	DATA oFantasma          AS OBJECT   //Classe para processamento de produto fantasma
	DATA oHorizonteFirme    AS OBJECT   //Classe para processamento de horizonte firme
	DATA oLeadTime          AS OBJECT   //Classe para manipulacao de Lead Time
	DATA oLogs              AS OBJECT   //Classe para manipulacao de logs
	DATA oOpcionais         AS OBJECT   //Classe para manipulacao de opcionais
	DATA oParametros        AS OBJECT   //Objeto JSON com todos os parametros do MRP - Consulte MRPAplicacao():parametrosDefault()
	DATA oPeriodos          AS OBJECT   //Classe para regras de periodos
	DATA oRastreio          AS OBJECT   //Classe para regras de rastreabilidade
	DATA oVersaoDaProducao  AS OBJECT   //Classe para regras de vers�o da Produ��o
	DATA oSubProduto        AS OBJECT   //Classe para regras de SubProdutos
	DATA oEventos           AS OBJECT   //Classe para regras de Log de Eventos
	DATA oSeletivos         AS OBJECT   //Classe para regras de Seletivos Multivalorados
	DATA oMOD               AS OBJECT   //Classe para regras de MOD - Mao de Obra de Produto

	METHOD new() CONSTRUCTOR
	METHOD aguardaProcesso(lFimPrd, cNivelAtu, cNivelAnt, nInexistente, nInxstLoop, nReinicios, nDistrib, nIgnorados) //Aguarda processos multi-thread
	METHOD agrupaRevisoes(aBaixaPorOP)                 //Agrupa demandas por revis�o
	METHOD ajustarNecessidadeExplosao(cComponente, nNecPai, nNecComp, lQtdFixa, nFPotencia, nFPotEstru, nFatPerda) //Aplica ajustes na necessidade para explosao da estrutura
	METHOD ajustarArrayQuebrasQtd(aQuant)              //Converte um array de quantidades para a matriz QTD,DOCUMENTO
	METHOD aplicarPotencia(cProduto, nFPotencia, nNecComp, nFPotEstru) //Aplicar fator de potencia
	METHOD aplicarPerda(nNecComp, nFatPerda)           //Aplica fator de perda
	METHOD arredondarExplosao(cCompon, nNecess)        //Faz o arredondamento de acordo com par�metros do produto.
	METHOD checarFimLoop()
	METHOD calcularLotes(cProduto, nNecess, nQtdLotes) //Calcula a quebra de lotes do produto.
	METHOD calcularNivel()                             //Calcula o nivel da estrutura
	METHOD destruir()                                  //Destroi a classe
	METHOD explodeAgrupadoPorRevisao(cChaveProd, cProduto, nPerLead, nPeriodo, nPerMaximo, cIDOpcPai, aBaixaPorOP) //Explode a estrutura do produto agrupado por revis�o do produto
	METHOD explodirEstrutura(cProduto, nNecess, nPerLead, nPeriodo, cFantasma, cIDOpcPai, aBaixaPorOP, cRevisao, cRoteiro, cLocal, cList, oRastrPais) //Explode a estrutura do produto
	METHOD loopNiveis()                                //Loop niveis + produtos para calcular
	METHOD periodoMaxComponentes(cProduto, nPeriodo)   //Atualiza os periodos maximos dos componentes do produto
	METHOD periodoAnterior(cChaveProd, nPeriodo, aPRDPeriodos, lErrorMAT)  //Identifica o periodo anterior do produto existente na matriz
	METHOD periodoPosterior(cChaveProd, nPeriodo, aPRDPeriodos, lErrorMAT) //Identifica o periodo anterior do produto existente na matriz
	METHOD politicaDeEstoque(cChaveProd, nSaldoAtu, lAtual, nPeriodo, lReinicia, aBaixaPorOP, lAlternativo, nSaiPrev) //Aplica regras da politica de estoque do produto
	METHOD registraProcessados()                       //Atualiza os status dos registros que foram processados
	METHOD revisaoProduto(cProduto, cRoteiro)          //Identifica a revis�o e roteiro atual do produto
	METHOD reduzPMPSaldo(nSaldo, aBaixaPorOP)          //Desconta qtd. de PMP do saldo.
	METHOD saldoInicial(cProduto, nPeriodo, lSomaNec)  //Identifica o saldo anterior ao periodo
	METHOD saldoFinal(cProduto, nPeriodo, lSomaNec)    //Identifica o saldo final do produto no periodo atual
	METHOD saldoPosterior(cProduto, nPeriodo)          //Identifica o saldo inicial do produto no periodo posterior ao periodo atual
	METHOD validaLotesVencidos(cProduto, nPeriodo, nSldInicial, aRetLotes, cChaveProd) //Desconta os lotes vencidos do saldo inicial do produto
	METHOD getUsoPotencia(cCompon)
ENDCLASS

/*/{Protheus.doc} new
Metodo construtor
@author    brunno.costa
@since     25/04/2019
@version   1
@param 01 - oParametros, objeto  , Objeto JSON com todos os parametros do MRP - Consulte MRPAplicacao():parametrosDefault()
@param 02 - oLogs      , objeto  , instancia da classe de logs
@param 03 - lRecursiva , numero  , indica se refere-se a execucao recursiva
@param 04 - aPeriodos  , array   , array com as datas dos periodos de processamento
/*/
METHOD new(oParametros, oLogs, lRecursiva, aPeriodos) CLASS MrpDominio

	Local oStatus
	Local nTentativas := 0

	Default lRecursiva := .F.

	::aPeriodos   := aPeriodos

	//Instancia propriedade da classe de parametros
	::oParametros := oParametros

	//Instancia propriedade da classe de Logs
	::oLogs       := oLogs

	//Instancia propriedade da classe de Periodos
	::oPeriodos   := MrpDominio_Periodos():New(Self)

	//Verifica se � uma thread
	If lRecursiva
		//Instancia propriedade da camada de repositorio de dados
		::oDados := MrpDados():New(::oParametros, ::aPeriodos, ::oLogs, lRecursiva)
	Else
		//Instancia objeto da camada de repositorio de dados
		::oDados := MrpDados():New(::oParametros, {}, ::oLogs, lRecursiva .AND. ::oParametros["nOpcCarga"] == 1)

		If ::oParametros["nThreads"] > 0 .And. ::oParametros["nOpcCarga"] == 1
			PCPIPCStart(::oParametros["cSemaforoThreads"], ::oParametros["nThreads"], 0, ::oParametros["cEmpAnt"], ::oParametros["cFilAnt"]) //Inicializa as Threads
		EndIf

		//Faz a carga do Calend�rio MRP antes de montar os Per�odos
		If ::oParametros["nOpcCarga"] == 2
			oStatus := MrpDados_Status():New(::oParametros["ticket"])
			oStatus:preparaAmbiente(::oDados)
			While ::oDados:oCalendario:getFlag("termino_carga", .T.) <> "S" .And. oStatus:getStatus("status") != "4"
				ntentativas++
				Sleep(100)
				If Mod(ntentativas, 100) == 0
					::oDados:oLogs:log(STR0145 + Time(), "C") //"Aguardando carga calendario "
				EndIf
			EndDo

			::oPeriodos:criarArrayPeriodos(::oParametros["dDataIni"], ::oParametros["nTipoPeriodos"], ::oParametros["nPeriodos"])  //Monta Array de Periodos
		Else
			//Inicia o rec�lculo dos n�veis (em uma nova thread)
			::calcularNivel()

			::oDados:oLogs:log(STR0107 + Time(), "C") //"pre-carga;inicio;"
			::oDados:oCargaMemoria:preCarga()
			::oDados:oLogs:log(STR0108 + Time(), "C") //"pre-carga;fim;"
		EndIf

		//Seta o array novamente objeto da camada de repositorio de dados para atualizar o aPeriodos
		::oDados:setAPeriodos(::aPeriodos)
	EndIf

	//Seta instancia do objeto
	::oDados:setoDominio(Self)

	//Instancia propriedade da classe de manipulacao de opcionais
	::oOpcionais := MrpDominio_Opcional():New(::oDados)

	//Instancia propriedade da classe de processamento de produtos fantasmas
	::oFantasma  := MrpDominio_Fantasma():New(Self)

	//Instancia classe para manipulacao de lead time
	::oLeadTime  := MrpDominio_LeadTime():New(Self)

	//Instancia propriedade da classe de processamento de Horizonte Firme
	::oHorizonteFirme := MrpDominio_HorizonteFirme():New(Self)

	//Instancia propriedade da classe de processamento de Rastreabilidade
	::oRastreio := MrpDominio_Rastreio():New(::oDados)

	//Instancia propriedade da classe de processamento de Vers�o da Produ��o
	::oVersaoDaProducao := MrpDominio_VersaoDaProducao():New(Self)

	//Instancia propriedade da classe de processamento de SubProdutos
	::oSubProduto := MrpDominio_Subproduto():New(Self)

	//Instancia propriedade da classe de processamento de Log de Eventos
	::oEventos := MrpDominio_Eventos():New(Self)

	//Instancia propriedade da classe de processamento de Seletivos Multivalorados
	::oSeletivos := MrpDominio_Seletivos():New(Self)

	//Instancia propriedade da classe de processamento de alternativos
	::oAlternativo := MrpDominio_Alternativo():New(Self)

	//Instancia propriedade da classe de processamento de alternativos
	::oAglutina := MrpDominio_Aglutina():New(::oDados)

	//Instancia propriedade da classede processamento de MOD - Produto Mao de Obra
	::oMOD := MrpDominio_MOD():New()

	If !lRecursiva .And. ::oParametros["nOpcCarga"] == 2
		//Carrega dados em memoria
		::oDados:oLogs:log(STR0051 + Time(), "C") //"carga_inicial;inicio;"
		::oDados:oCargaMemoria:carregaRegistros()
		::oDados:oLogs:log(STR0052 + Time(), "C") //"carga_inicial;fim;"
	EndIf

Return Self
/*/{Protheus.doc} loopNiveis
Loop niveis + produtos para calculo das necessidades do MRP
@author    brunno.costa
@since     25/04/2019
@version 1.0
/*/
METHOD loopNiveis() CLASS MrpDominio

	Local aAreaPRD
	Local aCachePRD      := {}
	Local aCposPRD       := {"PRD_COD","PRD_IDOPC","PRD_PPED","PRD_ESTSEG","PRD_NIVEST"}
	Local aRetAux
	Local cChaveProd
	Local cIDOpc          := ""
	Local cProduto        := ""
	Local cNivelAnt
	Local cNivelAtu
	Local lErrorPRD       := .F.
	Local lDelegar        := .T.
	Local lCacheLocal     := .F.
	Local lLogPerc        := .T. //::oLogs:logValido("%") - For�ado para exibir em tela.
	Local lPercent        := lLogPerc
	Local nInexistente    := 0
	Local nInexistLoop    := 0
	Local nEstSeg         := 0
	Local nIndCache       := 0
	Local nLiveLock       := 0
	Local nPontoPed       := 0
	Local nProdutos       := ::oDados:oProdutos:getRowsNum()
	Local nDistribuicoes  := 0 //Quantidade de chamadas ao calculo da necessidade do produto
	Local nIgnorados      := 0 //Quantidade de vezes que avalia produto sem execucao
	Local nMaxaCache
	Local nReinicios      := 0 //Reinicios loopNiveis
	Local nSecIni         := Seconds()
	Local nSecUlt         := nSecIni
	Local nSecAtu
	Local nThreads        := ::oParametros["nThreads"]
	Local oDados          := ::oDados
	Local oProdutos       := oDados:oProdutos
	Local oLiveLock       := oDados:oLiveLock
	Local oStatus         := MrpDados_Status():New(::oParametros["ticket"])
	Local oExisteMAT      := JsonObject():New()

	::oLogs:log(STR0001 + " " + Iif(::oParametros["lPorNivel"], STR0002, STR0003) + " - " + Time(), "L") //"Inicio do loop master de delegacao dos processamentos" + "por nivel" + "multi-nivel"

	//Seta variaveis de controle globais
	oProdutos:setflag("cNivMin"                   , "01", .F., .F.) //Menor nivel ja processado
	oProdutos:setflag("nThreadsOk"                , 0   , .F., .T.) //Processos finalizados nas Threads
	oProdutos:setflag("nProdCalcT"                , 0   , .F., .T.) //Total de produtos calculados
	oProdutos:setflag("nProcessNv"                , 0   , .F., .T.) //Produtos processados por nivel
	oProdutos:setflag("nProdutosT"                , nProdutos , .F., .F.) //Total de produtos
	oProdutos:setflag("lPercentX"                 , .F. , .F., .F.) //Controle de impressao do percentual multi-thread
	oLiveLock:setflag("nJobLiveLock"              , 0   , .F., .F.) //Seta variavel de controle de JOB's em LiveLock
	oProdutos:setflag("calculationPercentage"     , 0   , .F., .F.) //Indica o % de conclusao do calculo
	oProdutos:setflag("memoryLoadPercentage"      , 0   , .F., .F.) //Indica o % de conclus�o na etapa de carga mem�ria
	oStatus:setStatus("documentEventLogPercentage", 0   , .F., .F.) //Indica o % de conclusao da an�lise de documentos do Log de Eventos
	oStatus:setStatus("documentEventLogStatus"    , "1" , .F., .F.) //Indica o status da an�lise de documentos do Log de Eventos

	If "|CS|" $ "|" + ::oParametros["cLogsImpre"] + "|"
		::oLogs:log(STR0069, "CS") //"Periodo/Produto -> Saldo  := Sld.Inicial + Ent.Prevista - Saida Prev. - Saida Estrutura"
	EndIf

	If oStatus:getStatus("status") == "4"
		::oLogs:log(STR0065 + Time(), "L") //"Fim do loop master de delegacao dos processamentos - "
		Return
	EndIf

	//Percorre todos os Produtos - Indice: Nivel + Produto
	lErrorPRD := .T.
	While .T.
		If lErrorPRD	//EOF ou BOF
			lErrorPRD  := .F.
			cNivelAnt  := oProdutos:getflag("cNivMin"              , .F., .F.)
			cChaveProd := oProdutos:getflag("cPriNivel" + cNivelAnt, .F., .F.)
			cNivelAtu  := cNivelAnt
			If lCacheLocal
				nIndCache := 1
				aRetAux   := aCachePRD[nIndCache]

			ElseIf cChaveProd == Nil
				aRetAux   := oDados:retornaCampo("PRD", 2, , aCposPRD, @lErrorPRD, .F., .T. /*lPrimeiro*/, , , , .T. /*lVarios*/)
				If !lErrorPRD
					aAdd(aCachePRD, aRetAux)
				EndIf
				If lErrorPRD
					Exit
				EndIf
			Else
				aRetAux   := oDados:retornaCampo("PRD", 2, Iif(cNivelAnt == "00", "01", cNivelAnt) + cChaveProd, aCposPRD, @lErrorPRD, , , , , , .T. /*lVarios*/)
				If !lErrorPRD
					aAdd(aCachePRD, aRetAux)
				EndIf
			EndIf
			cProduto  := aRetAux[1]
			cIDOpc    := aRetAux[2]
			nPontoPed := aRetAux[3]
			nEstSeg   := aRetAux[4]
		EndIf

		If !::oParametros['lEstoqueSeguranca'] .And. nEstSeg > 0
			oDados:gravaCampo("PRD", 2, , {"PRD_SLDDIS", "PRD_ESTSEG"}, {nEstSeg, -nEstSeg}, .T., .T.,, .T. /*lVarios*/)
			aRetAux[4] := 0
			nEstSeg    := 0
		EndIf

		//Verifica se o produto deve ser calculado
		lDelegar := .F.
		If Self:oSeletivos:consideraProduto(cProduto, oDados)
			If !oExisteMAT[cProduto + cIDOpc] .AND. !oDados:existeMatriz(cProduto, , , cIDOpc)
				oExisteMAT[cProduto + cIDOpc] := .F.
				If (nPontoPed > 0 .Or. nEstSeg > 0);
				   .And. !Self:oMOD:produtoMOD(cProduto, oDados);
				   .And. oDados:possuiPendencia(cProduto, .F., cIDOpc)
					lDelegar   := .T.
				EndIf
			Else
				oExisteMAT[cProduto + cIDOpc] := .T.
				lDelegar                      := !Self:oMOD:produtoMOD(cProduto, oDados);
				                                 .AND. oDados:possuiPendencia(cProduto, .F., cIDOpc)
			EndIf
		Else
			If nEstSeg > 0
				oDados:gravaCampo("PRD", 2, , {"PRD_SLDDIS", "PRD_ESTSEG"}, {nEstSeg, -nEstSeg}, .T., .T.,, .T. /*lVarios*/)
				aRetAux[4] := 0
			EndIf
		EndIf

		If lDelegar
			If lPercent
				If oProdutos:getflag("lPercentX")
					lPercent := .F. //Nao delega novamente, pois ha operacao em andamento
				Else
					oProdutos:setflag("lPercentX", .T., .F., .F.)
				EndIf
			EndIf

			nDistribuicoes++
			If nThreads > 1
				nLiveLock := oLiveLock:getResult(cProduto)
				If (nLiveLock == Nil .OR. nLiveLock == 0)
					PCPIPCGO(::oParametros["cSemaforoThreads"], .F., "MRPCalculo", Nil, cProduto, cIDOpc, Nil, lPercent, nInexistLoop)  //Delega calculo para Threa
				Else
					//Delega thread para execucoes exclusivas de produtos em live-lock em single-thread
					If oLiveLock:getflag("nJobLiveLock") == 0
						oLiveLock:setFlag("nJobLiveLock", 1)
						//PCPIPCStart(::oParametros["cSemaforoThreads"], 1, 0, ::oParametros["cEmpAnt"], ::oParametros["cFilAnt"]) //Inicializa nova Thread
						PCPIPCGO(::oParametros["cSemaforoThreads"], .F., "PCPCalcLL")
					EndIf
				EndIf
			Else
				aAreaPRD   := oDados:retornaArea("PRD")
				MRPCalculo(Self, cProduto, cIDOpc, Nil, lPercent, nInexistLoop)  //Realiza o calculo single Thread
				oDados:setaArea(aAreaPRD)
			EndIf

			If lPercent
				lPercent := .F.
				nSecUlt  := Seconds()
			EndIf

		Else
			nInexistente++
			nInexistLoop++
			nIgnorados++

			::oLogs:log(STR0083 + cProduto + Iif(!Empty(cIDOpc), cIDOpc, ""), "29") //"Produto sem matriz ou sem pendencia: "
		EndIf

		//Identifica o proximo produto e Nivel
		cNivelAnt := cNivelAtu

		If lCacheLocal
			nIndCache++
			If nIndCache <= nMaxaCache
				aRetAux := aCachePRD[nIndCache]
			Else
				lErrorPRD := .T.
			EndIf
		Else
			aRetAux   := oDados:retornaCampo("PRD", 2, Nil, aCposPRD, @lErrorPRD, , , .T. /*lProximo*/, , , .T. /*lVarios*/)
			If !lErrorPRD
				aAdd(aCachePRD, aRetAux)
			EndIf
		EndIf

		If !lErrorPRD //!EOF
			cProduto  := aRetAux[1]
			cIDOpc    := aRetAux[2]
			nPontoPed := aRetAux[3]
			nEstSeg   := aRetAux[4]
			cNivelAtu := aRetAux[5]
		EndIf

		//Seta impressao do percentual de cobertura do calculo a cada x segundos
		If lLogPerc
			nSecAtu  := Seconds()
			If (nSecAtu - nSecUlt) > 10
				lPercent := .T.
			EndIf
		EndIf

		//Checa cancelamento a cada X produtos
		If Mod((nDistribuicoes + nInexistLoop), ::oParametros["nX_Para_Cancel"]) == 0 .AND.;
		   oStatus:getStatus("status") == "4"
			Exit
		EndIf

		If (lErrorPRD .Or. (cNivelAtu  != cNivelAnt))  //EOF ou Troca de Nivel
			If lErrorPRD
				lCacheLocal := .T.
				nMaxaCache  := Len(aCachePRD)
			EndIf

			lExit := oStatus:getStatus("status") == "4" .OR.; //Cancelado
			::aguardaProcesso(lErrorPRD, @cNivelAtu, @cNivelAnt, @nInexistente, @nInexistLoop, @nReinicios, @nDistribuicoes, @nIgnorados, @cProduto)

			If lExit
				Exit //Sai do WHILE
			EndIf
		EndIf
	EndDo

	For nIndCache := 1 to Len(aCachePRD)
		aSize(aCachePRD[nIndCache], 0)
	Next
	aSize(aCachePRD, 0)

	FreeObj(oExisteMAT)
	oExisteMAT := Nil

	aSize(aCposPRD, 0)
	aCposPRD := Nil

	::oLogs:log(STR0065 + Time(), "L") //"Fim do loop master de delegacao dos processamentos - "

	If ::oDados:oParametros['lAnalisaMemoriaPosLoop']
		::oDados:oProdutos:analiseMemoria(::oDados:oParametros['lAnalisaMemoriaSplit'], "ANALISE DE MEMORIA APOS LOOP NIVEIS")
	EndIf

Return

/*/{Protheus.doc} revisaoProduto
Busca a revis�o de determinado produto

@author    lucas.franca
@since     27/06/2019
@version 1.0
@param 01 - cProduto, caracter, codigo do produto
@param 02 - cRoteiro, caracter, retorna por refer�ncia o roteiro para produ��o do produto
@return cRevisao, caracter, revis�o utilizada pelo produto
/*/
METHOD revisaoProduto(cProduto, cRoteiro) CLASS MrpDominio
	Local aAreaPRD := Nil
	Local aRetAux  := {}
	Local cRevisao := "   "
	Local lAtual   := cProduto == ::oDados:oProdutos:cCurrentKey

	If !lAtual
		aAreaPRD := ::oDados:retornaArea("PRD")
	EndIf

	aRetAux  := ::oDados:retornaCampo("PRD", 1, cProduto, {"PRD_REVATU", "PRD_ROTEIR"}, , lAtual, , /*lProximo*/, , , .T. /*lVarios*/)
	cRevisao := aRetAux[1]

	If cRoteiro == Nil .OR. Empty(cRoteiro)
		cRoteiro := aRetAux[2]
	EndIf

	If !lAtual
		::oDados:setaArea(aAreaPRD)
	EndIf

Return cRevisao

/*/{Protheus.doc} periodoAnterior
Identifica o periodo anterior do produto existente na matriz
@author    brunno.costa
@since     08/07/2019
@version 1.0
@param 01 - cChaveProd  , caracter, codigo do produto
@param 02 - nPeriodo    , numero  , periodo atual referencia
@param 03 - aPRDPeriodos, array   , array com os dados de periodos existentes na matriz para o cChaveProd - RETORNO POR REFERENCIA
@PARAM 04 - lErrorMAT   , logico  , indica falha no retorno da variavel de controle dos periodos do produto
@return nReturn, numero , periodo anterior do produto existente na matriz
/*/
METHOD periodoAnterior(cChaveProd, nPeriodo, aPRDPeriodos, lErrorMAT) CLASS MrpDominio
	Local nAux      := 0
	Local nAux2
	Local nReturn   := 0
	Local oDados        := ::oDados

	Default aPRDPeriodos := {}
	Default lErrorMAT    := .F.

	oDados:oMatriz:getAllList("Periodos_Produto_" + cChaveProd, @aPRDPeriodos, @lErrorMAT)
	If !lErrorMAT
		aSort(aPRDPeriodos, , , { |x,y| x[2] < y[2] } )
		nAux2 := 1
		While nAux2 > 0
			nAux2 := aScan(aPRDPeriodos, { |nPer| nPer[2] < nPeriodo }, (nAux + 1) )
			If nAux2 > 0
				nAux := nAux2
			EndIf
		EndDo
		If nAux > 0
			nReturn := aPRDPeriodos[nAux][2]
		Else
			lErrorMAT  := .T.
		EndIf
	EndIf

Return nReturn

/*/{Protheus.doc} periodoPosterior
Identifica o periodo posterior do produto existente na matriz
@author    brunno.costa
@since     08/07/2019
@version 1.0
@param 01 - cChaveProd  , caracter, codigo do produto com ID Opcional
@param 02 - nPeriodo    , numero  , periodo atual referencia
@param 03 - aPRDPeriodos, array   , array com os dados de periodos existentes na matriz para o cChaveProd
@PARAM 04 - lErrorMAT   , logico  , indica falha no retorno da variavel de controle dos periodos do produto
@return nReturn, numero , periodo posterior do produto existente na matriz
/*/
METHOD periodoPosterior(cChaveProd, nPeriodo, aPRDPeriodos, lErrorMAT) CLASS MrpDominio
	Local aPRDPeriodos := {}
	Local nAux         := 0
	Local nReturn      := 0
	Local oDados       := ::oDados

	If aPRDPeriodos == Nil
		oDados:oMatriz:getAllList("Periodos_Produto_" + cChaveProd, @aPRDPeriodos, @lErrorMAT)
		If !lErrorMAT
			aSort(aPRDPeriodos, , , { |x,y| x[2] < y[2] } )
		EndIf
	EndIf

	If !Empty(aPRDPeriodos)
		nAux := aScan(aPRDPeriodos, { |nPer| nPer[2] > nPeriodo } )
		If nAux > 0
			nReturn := aPRDPeriodos[nAux][2]
		Else
			lErrorMAT  := .T.
		EndIf
	EndIf

Return nReturn

/*/{Protheus.doc} saldoInicial
Identifica o saldo inicial do periodo
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cChaveProd, caracter, codigo do produto com IDOpcional
@param 02 - nPeriodo  , numero  , periodo referencia
@param 03 - lSomaNec  , logico  , indica se deve somar a necessidade do periodo anterior
@return nSaldo, numero, saldo do periodo anterior
/*/
METHOD saldoInicial(cChaveProd, nPeriodo, lSomaNec) CLASS MrpDominio
	Local aAreaMAT
	Local aPRDPeriodos  := {}
	Local cChaveMAT
	Local lErrorMAT     := .F.
	Local lErrorPRD     := .F.
	Local nSaldo        := 0
	Local nSldPosterior := 0
	Local oDados        := ::oDados

	Default lSomaNec := .T.

	aAreaMAT    := oDados:retornaArea("MAT")

	//Verifica saldo inicial referencia
	cChaveMAT := DtoS(::aPeriodos[nPeriodo]) + cChaveProd

	//Identifica o saldo atual na tabela Produtos
	nSaldo := oDados:retornaCampo("PRD", 1, cChaveProd, "PRD_SLDDIS", @lErrorPRD)

	If oDados:existeMatriz(cChaveProd)

		//Identifica o saldo inicial atual na tabela Matriz
		If nSaldo == 0 .Or. nSaldo == Nil
			nSaldo := oDados:retornaCampo("MAT", 1, cChaveMAT, "MAT_SLDINI", @lErrorMAT)
			If lErrorMAT
				nSaldo := 0
			EndIf
		EndIf

		//Identifica periodo existente anterior
		nPerInicio := ::periodoAnterior(cChaveProd, nPeriodo, @aPRDPeriodos, @lErrorMAT)

		//Identifica saldo final de registro anterior da Matriz
		If nPerInicio > 0
			nSaldo := ::saldoFinal(cChaveProd, nPerInicio, lSomaNec, nSaldo)
		Endif

		//Caso nao tenha identificado o saldo ainda, verifica saldo inicial em registro posterior da Matriz
		If nSaldo == 0 .And. lErrorMAT// .AND. oDados:foiCalculado(cProduto, nPeriodo)
			nSldPosterior := ::saldoPosterior(cChaveProd, nPeriodo, .F., .F., aPRDPeriodos)
			If nSldPosterior > 0
				nSaldo := nSldPosterior
			EndIf
		EndIf
	EndIf

	oDados:setaArea(aAreaMAT)

Return nSaldo

/*/{Protheus.doc} saldoFinal
Identifica o saldo final do produto no per�odo atual
@author    brunno.costa
@since     08/07/2019
@version 1.0
@param 01 - cProduto, caracter, codigo do produto
@param 02 - nPeriodo, numero  , periodo referencia
@param 03 - lSomaNec, logico  , indica se deve somar a necessidade do periodo anterior
@parma 04 - nSaldo  , numero  , conteudo default
@return nSaldo, numero, saldo final do periodo atual
/*/
METHOD saldoFinal(cProduto, nPeriodo, lSomaNec, nSaldo) CLASS MrpDominio
	Local cChaveMAT
	Local lErrorMAT     := .F.
	Local nPerAux
	Local nSldAnterior  := 0
	Local nNecess       := 0
	Local oDados        := ::oDados

	Default lSomaNec    := .T.
	Default nSaldo        := 0

	For nPerAux := nPerInicio to 1 step -1
		cChaveMAT    := DtoS(::aPeriodos[nPerAux]) + cProduto
		lErrorMAT    := .F.
		nSldAnterior := oDados:retornaCampo("MAT", 1, cChaveMAT, "MAT_SALDO", @lErrorMAT)

		If !lErrorMAT     //Encontrou produto na matriz
			If lSomaNec
				nNecess := oDados:retornaCampo("MAT", 1, cChaveMAT, "MAT_NECESS", @lErrorMAT)
			EndIf
			nSaldo := nSldAnterior + nNecess
			Exit

		Else              //Nao encontrou produto na matriz
			nSldAnterior := 0
			Loop

		EndIf

	Next nPerAux

Return nSaldo

/*/{Protheus.doc} saldoPosterior
Identifica o saldo inicial do produto no periodo posterior ao periodo atual
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto    , caracter, codigo do produto
@param 02 - nPeriodo    , numero  , periodo atual referencia
@param 03 - lSldIniAtual, logico  , indica avalia saldo inicial do registro atual da matriz
@param 04 - lSldProduto , logico  , indica se avalia o saldo atual da tabela de produtos
@param 05 - aPRDPeriodos, array   , array com os dados de periodos existentes na matriz para o cProduto
@return nSaldo, numero, saldo inicial posterior ao periodo atual
/*/
METHOD saldoPosterior(cProduto, nPeriodo, lSldIniAtual, lSldProduto, aPRDPeriodos) CLASS MrpDominio
	Local aAreaMAT
	Local cChaveMAT
	Local lErrorMAT   := .F.
	Local lErrorPRD   := .F.
	Local nPerAux
	Local nSaldo      := 0
	Local nSldAux     := 0
	Local nPerInicio  := Len(::aPeriodos)

	Default oDados       := ::oDados
	Default lSldIniAtual := .F.
	Default lSldProduto  := .T.

	aAreaMAT  := oDados:retornaArea("MAT")
	cChaveMAT := DtoS(::aPeriodos[nPeriodo]) + cProduto
	lErrorMAT := .F.

	//Identifica o saldo atual na tabela Produtos
	If lSldProduto
		nSaldo := oDados:retornaCampo("PRD", 1, cProduto, "PRD_SLDDIS", @lErrorPRD)
	EndIf

	If oDados:existeMatriz(cProduto)
		//Identifica o saldo inicial atual na tabela Matriz
		If lSldIniAtual
			If nSaldo == 0 .Or. nSaldo == Nil
				nSaldo := oDados:retornaCampo("MAT", 1, cChaveMAT, "MAT_SLDINI", @lErrorMAT)
				If lErrorMAT
					nSaldo := 0
				EndIf
			EndIf
		EndIf

		//Verifica Periodo existente posterior
		nPerInicio := ::periodoPosterior(cProduto, nPeriodo, aPRDPeriodos, @lErrorMAT)

		//Identifica saldo inicial de registro posterior da Matriz
		If nPerInicio > 0
			For nPerAux := nPerInicio to Len(::aPeriodos)
				cChaveMAT := DtoS(::aPeriodos[nPerAux]) + cProduto
				lErrorMAT := .F.
				nSldAux   := oDados:retornaCampo("MAT", 1, cChaveMAT, "MAT_SLDINI", @lErrorMAT)

				If !lErrorMAT       //Encontrou produto na matriz
					nSaldo := nSldAux
					Exit

				Else                //Nao encontrou produto na matriz
					nSldAux := 0
					Loop

				EndIf

			Next nPerAux
		EndIf
	EndIf

	oDados:setaArea(aAreaMAT)

Return nSaldo

/*/{Protheus.doc} politicaDeEstoque
Aplica politica de estoque a quantidade do produto
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto   , caracter, codigo do produto
@param 02 - nSaldoAtu  , numero  , saldo atual do produto
@param 03 - lAtual     , logico  , indica se deve reposicionar no cadastro de parametros de produtos
@param 04 - nPeriodo   , numero  , per�odo onde a demanda est� sendo calculada.
@param 05 - lReinicia  , logico  , Retorna valor por refer�ncia. Indica se o c�lculo do produto deve ser reiniciado.
@param 06 - aBaixaPorOP, array   , array com os dados de rastreabilidade origem (Documentos Pais)
								   {{1 - Id Rastreabilidade,;
								     2 - Documento Pai,;
								     3 - Quantidade Necessidade,;
								     4 - Quantidade Estoque,;
								     5 - Quantidade Baixa Estoque,;
								     6 - Quantidade Substitui��o},...}
@param 07 - lAlternativo, l�gixo , indica se este produto � substituto (alternativo)
@return nNecess      , numero  , quantidade apos aplicacao da politica de estoque
/*/
METHOD politicaDeEstoque(cProduto, nSaldoAtu, lAtual, nPeriodo, lReinicia, aBaixaPorOP, lAlternativo, lRecursiva) CLASS MrpDominio
	Local aLotes      := {}
	Local aIndBxAtu   := {}
	Local lRastreia   := Self:oParametros["lRastreia"]
	Local lErrorPRD   := .F.
	Local lCalcula    := .T.
	Local lAtuRastr   := .F.
	Local lSubProduto := .F.
	Local nConsEstq   := 0
	Local nPontoPed   := 0
	Local nNecess     := 0
	Local nNecOri     := 0
	Local nNecAux     := 0
	Local nDiferenca  := 0
	Local nEstoque    := 0
	Local nIndex      := 0
	Local nTotal      := 0
	Local nSldAux     := 0
	Local nQtdNec     := 0
	Local nNecInic    := 0
	Local nPosPP      := 0
	Local oDados      := ::oDados

	Default lAtual     := cProduto == ::oDados:oProdutos:cCurrentKey
	Default nPeriodo   := 0
	Default lRecursiva := .F.

	lReinicia := .F.

	If nPeriodo > 0 .And. ;
	   nSaldoAtu < 0 .And. ;
	   ::oParametros["nLeadTime"] != 1 .And. ;
	   !::oPeriodos:verificaDataUtil(::aPeriodos[nPeriodo])
		//Adiciona o pr�ximo per�odo para o c�lculo. Somente ir� gerar necessidade em per�odos �teis.
		lCalcula  := .F.
		lReinicia := .T.
	EndIf

	If lCalcula
		If ::oParametros["lPontoPedido"] .And. !lAlternativo
			nPontoPed := oDados:retornaCampo("PRD", 1, cProduto, "PRD_PPED", @lErrorPRD, lAtual /*lAtual*/, , , , , .F. /*lVarios*/)
		Else
			nPontoPed := 0
		EndIf

		If nSaldoAtu < 0
			//Saldo atual � negativo.
			//Gera necessidade para igualar o saldo a 0
			nNecess := Abs(nSaldoAtu)
		EndIf

		If lRastreia .And. aBaixaPorOP != Nil
			nNecOri := nNecess //Armazena a necessidade original

			//Percorre o aBaixaPorOp aplicando as regras de lotes para cada necessidade.
			nTotBxOp   := Len(aBaixaPorOP)
			aLotesBxOP := Array(nTotBxOp)
			nNecess    := 0
			For nIndex := 1 To nTotBxOp
				nConsEstq  := 0
				nTotal     := 0
				lAtuRastr  := .F.

				//Atualiza controle de saldo em estoque
				If nIndex == 1
					nEstoque   := aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE] - aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE]
					lAtuRastr := .T.
				Else
					If nSldAux != 0
						lAtuRastr := .T.
						nEstoque  += nSldAux
						aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE] := nEstoque
					EndIf
				EndIf

				If AllTrim(aBaixaPorOP[nIndex][ABAIXA_POS_TIPO_PAI]) $ "|"+STR0144+"|" //Ponto Ped.
					nPosPP := nIndex
					Loop
				EndIf

				If aBaixaPorOP[nIndex][ABAIXA_POS_TIPO_PAI] == "Pr�-OP"
					lAtuRastr                                       := .T.
					aBaixaPorOP[nIndex][ABAIXA_POS_NECESSIDADE]     := aBaixaPorOP[nIndex][ABAIXA_POS_NEC_ORIG]
					aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE] := 0
				Endif

				If aBaixaPorOP[nIndex][ABAIXA_POS_NEC_ORIG] < 0
					lSubProduto := .T.
				EndIf

				nNecInic   := aBaixaPorOP[nIndex][ABAIXA_POS_NECESSIDADE]

				//Atualiza a qtd. de necessidade
				nQtdNec := aBaixaPorOP[nIndex][ABAIXA_POS_NEC_ORIG]
				//Atualiza a qtd. de baixa de estoque.
				//Somente baixa estoque se:
				//-> n�o for do tipo PMP, e;
				//-> n�o for registro de alternativo
				//-> n�o for ponto de pedido
				If !(AllTrim(aBaixaPorOP[nIndex][ABAIXA_POS_TIPO_PAI]) $ "|1|"+STR0144+"|"); //"PMP|Ponto Ped."
				   .AND. aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] >= 0;

					//Zera consumo de estoque do produto original referente REGRA DO TIPO 3
					If aBaixaPorOP[nIndex][ABAIXA_POS_REGRA_ALT] == "3"
						aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE] := 0

					Else
						//Tem necessidade maior que o estoque dispon�vel. Atualiza a QTD de consumo de estoque.
						If (aBaixaPorOP[nIndex][ABAIXA_POS_NEC_ORIG]) >= aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE]
							aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE] := aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE]

						//Tem necessidade menor que o estoque. Atualiza a qtd. de consumo de estoque.
						Else
							aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE] := aBaixaPorOP[nIndex][ABAIXA_POS_NEC_ORIG]

						EndIf

						//Abate consumo de estoque
						nQtdNec -= aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE]
					EndIf

					//Abate da necessidade ENTRADA DE SUBSTITUI��O
					If (nQtdNec - aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO]) >= 0
						nQtdNec -= aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO]
					EndIf

				EndIf

				//Atualiza a qtd. de baixa de estoque.
				If aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] < 0;
				   .OR. aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE] < 0
					aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE] := 0
				EndIf
				nConsEstq += aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE]

				//Atualiza a qtd. de estoque.
				If aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE] < 0
					aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE] := 0
				EndIf

				//ACRESCENTA necessidade Substitu�da por REGRA DOS TIPOS 2 ou 3 - ALTERNATICO
				IF aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] < 0;
					.AND. (aBaixaPorOP[nIndex][ABAIXA_POS_REGRA_ALT] $ "|2|3|")
					nQtdNec += ABS(aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] )
					nQtdNec -= aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE]

				EndIf

				//Aplica as pol�ticas de estoque.
				nNecAux := nQtdNec
				If nQtdNec > 0
					aLotes := Self:calcularLotes(cProduto, nQtdNec, @nTotal)
				Else
					aLotes := Self:calcularLotes(cProduto, 0, @nTotal)
				EndIf


				//Armazena o array de controle das quebras de quantidades para produ��o/compra.
				aBaixaPorOP[nIndex][ABAIXA_POS_QUEBRAS_QUANTIDADE] := Self:ajustarArrayQuebrasQtd(aLotes)

				If Len(aLotes) > 1
					lAtuRastr := .T.
				EndIf

				//ALTERNATIVO COM REGRA DO TIPO 1 (Diferente de 2 e 3)
				IF aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] < 0;
				   .AND. !(aBaixaPorOP[nIndex][ABAIXA_POS_REGRA_ALT] $ "|2|3|")

					//ACRESCENTA necessidade Substitu�da por REGRA DO TIPO 1 (Diferente de 2 e 3)
					If (aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE] + nTotal;
						- aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE]);
						> ABS(aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO])

						If lAlternativo
							nQtdNec += ABS(aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] + aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE])
						EndIf

					//****************************************************************************//
					//AJUSTA necessidade Substitu�da acima da quantidade dispon�vel, REGRA DO TIPO 1
					//****************************************************************************//
					ElseIf !lAlternativo
						nDiferenca := Abs(aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO])
						nDiferenca -= aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE]
						nDiferenca += nTotal
						nDiferenca -= aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE]

						If nDiferenca > 0
							::oAlternativo:desfazParcialSubstituicao(aBaixaPorOP[nIndex][ABAIXA_POS_CHAVE_SUBST], cProduto, nDiferenca, nPeriodo, @aBaixaPorOP)
						EndIf

						nQtdNec += ABS(aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] + aBaixaPorOP[nIndex][ABAIXA_POS_CONSUMO_ESTOQUE])

					EndIf
				Endif

				//Verifica se ap�s aplicar a pol�tica de estoque deste produto, foi gerada uma necessidade maior do que
				//era necess�rio. Neste caso, utiliza a vari�vel nSldAux para controlar as sobras de estoque e reutilizar para as
				//pr�ximas necessidades.
				If nTotal > nQtdNec .AND. nQtdNec > 0
					//A necessidade que ser� gerada � maior que o necess�rio devido as pol�ticas de estoque do produto.
					//Atualiza a vari�vel nSldAux para considerar a sobra de saldo para os pr�ximos rastreios.
					nSldAux += nTotal - nQtdNec

					//Atualiza no baixaPorOP a qtd. que ser� produzida;
					aBaixaPorOP[nIndex][ABAIXA_POS_NECESSIDADE] := nTotal

					//Atualiza vari�vel para indicar que deve ser feito atualiza��o do arquivo de rastreio.
					lAtuRastr := .T.

				ElseIf nNecInic != nTotal
					//Atualiza no baixaPorOP a qtd. que ser� produzida;
					aBaixaPorOP[nIndex][ABAIXA_POS_NECESSIDADE] := nTotal

					//Atualiza vari�vel para indicar que deve ser feito atualiza��o do arquivo de rastreio.
					lAtuRastr := .T.

				EndIf

				If lAtuRastr .OR. lSubProduto
					//Sempre que existir quebras na produ��o/compra, deve atualizar o arquivo de rastreio.
					//Armazena o �ndice do array aBaixaPorOp que dever� ser atualizado.
					//A atualiza��o tamb�m ir� ocorrer caso seja identificado altera��o no saldo devido
					//as quantidades de produ��o serem maiores do que o necess�rio.
					aAdd(aIndBxAtu, nIndex)
				EndIf

				//Sumariza a qtd. dos lotes para registrar na matriz.
				nNecess  += nTotal

				aSize(aLotes, 0)

			Next nIndex

			//Verifica se a necessidade antes de calcular os lotes
			//� maior que a necessidade ap�s calcular os lotes.
			//Nesse caso, calcula novamente o lote somente para a diferen�a.
			If nNecOri > nNecess
				nNecDif := nNecOri - nNecess
				nTotal  := 0
				aLotes  := Self:calcularLotes(cProduto, nNecDif, @nTotal)
				//Sumariza a qtd. dos lotes para registrar na matriz.
				nNecess += nTotal
			EndIf
		Else
			aLotes  := Self:calcularLotes(cProduto, nNecess, @nTotal)
			//Sumariza a qtd. dos lotes para registrar na matriz.
			nNecess := nTotal
		EndIf

		aSize(aLotes, 0)

		//Se houver ponto de pedido, avalia no final do c�lculo
		If nPosPP > 0 .And. nPontoPed > 0
			nQtdNec := nPontoPed - (nSaldoAtu+nNecess)

			If nQtdNec < 0
				//Possui saldo para atender o PP. N�o gera necessidade.
				nQtdNec := 0
			EndIf
			nTotal  := 0

			aLotes := Self:calcularLotes(cProduto, nQtdNec, @nTotal)

			aBaixaPorOP[nPosPP][ABAIXA_POS_NEC_ORIG   ] := nQtdNec
			aBaixaPorOP[nPosPP][ABAIXA_POS_NECESSIDADE] := nTotal
			aBaixaPorOP[nPosPP][ABAIXA_POS_QTD_ESTOQUE] := nSaldoAtu+nNecess
			If aBaixaPorOP[nPosPP][ABAIXA_POS_QTD_ESTOQUE] < 0
				aBaixaPorOP[nPosPP][ABAIXA_POS_QTD_ESTOQUE] := 0
			EndIf
			nNecess += aBaixaPorOP[nPosPP][ABAIXA_POS_NECESSIDADE]

			aBaixaPorOP[nPosPP][ABAIXA_POS_QUEBRAS_QUANTIDADE] := Self:ajustarArrayQuebrasQtd(aLotes)

			aSize(aLotes, 0)

			aAdd(aIndBxAtu, nPosPP)
		EndIf

		If Len(aIndBxAtu) > 0
			//Grava as informa��es de controle para as quebras de produ��o/compra
			//no arquivo de rastreabilidade.
			Self:oRastreio:atualizaRastreio(cProduto, nPeriodo, @aBaixaPorOP, aIndBxAtu)
			aSize(aIndBxAtu, 0)
		EndIf

		If lAlternativo
			oDados:oLogs:log(STR0131 + AllTrim(cProduto) + STR0132 + cValToChar(nPeriodo), "XX")
			soDominio:oRastreio:atualizaSubstituicao(cProduto, nPeriodo, @aBaixaPorOP, .T.)

			//Chamada recursiva da pol�tica de estoque MrpDominio
			nNecess := Self:politicaDeEstoque(cProduto, (nSaldoAtu + nNecess), Nil, nPeriodo, lReinicia, aBaixaPorOP, .F., .T.)

		EndIf

	EndIf

Return nNecess

/*/{Protheus.doc} explodeAgrupadoPorRevisao
Explode a estrutura do produto para gerar demandas para os componentes
@author    brunno.costa
@since     20/09/2019
@version 1.0
@param 01 - cChaveProd , caracter, codigo do produto pai + ID Opcional
@param 02 - cProduto   , caracter, codigo do produto pai
@param 03 - nPerLead   , n�mero  , per�odo do leadtime
@param 04 - nPeriodo   , n�mero  , identificador do periodo
@param 05 - nPerMaximo , n�mero  , identificador do periodo m�ximo
@param 06 - cIDOpcPai  , caracter, identificador do opcional cProduto selecionado
@param 07 - aBaixaPorOP, array   , array com os dados de rastreabilidade origem (Documentos Pais)
								   {{1 - Id Rastreabilidade,;
								     2 - Documento Pai,;
								     3 - Quantidade Necessidade,;
								     4 - Quantidade Estoque,;
								     5 - Quantidade Baixa Estoque,;
								     6 - Quantidade Substitui��o},...}
/*/
METHOD explodeAgrupadoPorRevisao(cChaveProd, cProduto, nPerLead, nPeriodo, nPerMaximo, cIDOpcPai, aBaixaPorOP) CLASS MrpDominio

	//Agrupa as demandas por revis�o
	Local aRevisoes := ::agrupaRevisoes(cChaveProd, nPerLead, @aBaixaPorOP)
	Local nTotal    := Len(aRevisoes)
	Local nInd

	For nInd := 1 to nTotal
		::oLogs:log(STR0039 + AllTrim(cChaveProd) + STR0018 + cValToChar(nPeriodo) + "/" + cValToChar(nPerMaximo) + STR0038 + cValToChar(aRevisoes[nInd][aRevisoes_POS_QTDE_TOTAL]), "7") //"Explosao Estrutura : " + " - Periodo: " + " - Necessidade (MAT_NECESS): "
		::explodirEstrutura(cProduto,;
							aRevisoes[nInd][aRevisoes_POS_QTDE_TOTAL],;
							nPerLead,;
							nPeriodo,;
							"",;
							cIDOpcPai,;
							aRevisoes[nInd][aRevisoes_POS_aBaixaPorOP],;
							Iif(Empty(aRevisoes[nInd][aRevisoes_POS_REVISAO]), Nil, aRevisoes[nInd][aRevisoes_POS_REVISAO]),;
							Iif(Empty(aRevisoes[nInd][aRevisoes_POS_ROTEIRO]), Nil, aRevisoes[nInd][aRevisoes_POS_ROTEIRO]),;
							aRevisoes[nInd][aRevisoes_POS_LOCAL],;
							/*11*/,;
							/*12*/,;
							aRevisoes[nInd][aRevisoes_POS_VersaoProd])
	Next

	//Limpa Array aRevisoes
	For nInd := 1 to Len(aRevisoes)
		aSize(aRevisoes[nInd], 0)
		aRevisoes[nInd] := Nil
	Next
	aSize(aRevisoes, 0)

Return Nil

/*/{Protheus.doc} explodirEstrutura
Explode a estrutura do produto para gerar demandas para os componentes
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto    , caracter, codigo do produto pai
@param 02 - nNecPai     , numeric , quantidade de necessidade do produto pai
@param 03 - nPerLead    , numeric , per�odos de leadtime
@param 04 - nPeriodo    , numeric , numero do per�odo
@param 05 - cFantasma   , caracter, C�digo do produto pai que n�o � fantasma. Se estiver preenchido, indica que est� explodindo a estrutura de um produto fantasma.
@param 06 - cIDOpcPai   , caracter, identificador do opcional cProduto selecionado
@param 07 - aBaixaPorOP , array   , array com os dados de rastreabilidade origem (Documentos Pais)
								   {{1 - Id Rastreabilidade,;
								     2 - Documento Pai,;
								     3 - Quantidade Necessidade,;
								     4 - Quantidade Estoque,;
								     5 - Quantidade Baixa Estoque,;
								     6 - Quantidade Substitui��o},...}
@param 08 - cRevisao   , caracter, revis�o do produto pai
@param 09 - cRoteiro   , caracter, roteiro de produ��o do produto Pai
@param 10 - cLocal     , caracter, local de consumo padr�o
@param 11 - cList      , caracter, chave produto + chr(13) + per�odo referente chaves da aBaixaPorOP
@param 12 - oRastrPais , objeto  , objeto Json para controle/otimiza��o das altera��es de rastreabilidade no regitro Pai durante explos�o na estrutura
@param 13 - cVersao    , caracter, c�digo da vers�o de produ��o utilizada na explos�o
@return lReturn, logico, indica se o produto possui estrutura
/*/
METHOD explodirEstrutura(cProduto, nNecPai, nPerLead, nPeriodo, cFantasma, cIDOpcPai, aBaixaPorOP, cRevisao, cRoteiro, cLocal, cList, oRastrPais, cVersao) CLASS MrpDominio

	Local aAreaMAT      := ::oDados:retornaArea("MAT")
	Local aAreaPRD      := ::oDados:retornaArea("PRD")
	Local aComponentes  := {}
	Local aAux          := {}
	Local aOperacoes    := {}
	Local cComponente   := ""
	Local cBloqueio     := ""
	Local cChave
	Local cIDOpc
	Local cTRT
	Local cOpcGrp
	Local cOpcItem
	Local cRegra
	Local cRevIniCmp   := ""
	Local cRevFimCmp   := ""
	Local dData        := ::aPeriodos[nPerLead]
	Local dDtIniCmp    := ""
	Local dDtFimCmp    := ""
	Local dLeadTime
	Local lError       := .F.
	Local lRastreia    := ::oParametros["lRastreia"]
	Local lReturn      := .T.
	Local lUpdRstPai   := oRastrPais == Nil
	Local nNecComp     := 0
	Local nQtdEstrut   := 0
	Local nInd
	Local nPerLedCmp   := 0
	Local nTotal
	Local oEstruturas  := ::oDados:oEstruturas
	Local lQtdFixa     := .F.
	Local nFPotencia
	Local nFatPerda    := 0

	Default cIDOpcPai  := ""
	Default cFantasma  := ""
	Default cLocal     := ""
	Default cList      := cProduto + Iif(!Empty(cIDOpcPai),"|"+cIDOpcPai,"") + chr(13) + cValToChar(nPeriodo)
	Default cVersao    := ""
	Default oRastrPais := JsonObject():New()

	If cRevisao == Nil .OR. Empty(cRevisao) .OR. cRoteiro == Nil .OR. Empty(cRoteiro)
		cRevisao   := ::revisaoProduto(cProduto, @cRoteiro)
	EndIf

	//Retorna array com os componentes da estrutura
	oEstruturas:getRow(1, cProduto,, @aComponentes)

	If !Empty(aComponentes)
		If lRastreia .AND. aBaixaPorOP != Nil
			nTotal     := Len(aBaixaPorOP)
			For nInd := 1 to nTotal
				lError := .F.
				cChave             := aBaixaPorOP[nInd][ABAIXA_POS_CHAVE]
				aAux               := ::oRastreio:oDados_Rastreio:oDados:getItemAList(cList, cChave, @lError)
				If !lError .AND. oRastrPais[cChave] == Nil
					oRastrPais[cChave] := aClone(aAux)
				EndIf
			Next
		EndIf

		//Percorre os componentes da estrutura
		nTotal := Len(aComponentes)
		For nInd := 1 to nTotal
			cComponente := aComponentes[nInd][::oDados:posicaoCampo("EST_CODFIL")]
			nQtdEstrut  := aComponentes[nInd][::oDados:posicaoCampo("EST_QTD")]
			nNecComp    := nQtdEstrut
			cTRT        := aComponentes[nInd][::oDados:posicaoCampo("EST_TRT")]
			dDtIniCmp   := aComponentes[nInd][::oDados:posicaoCampo("EST_VLDINI")]
			dDtFimCmp   := aComponentes[nInd][::oDados:posicaoCampo("EST_VLDFIM")]
			cRevIniCmp  := aComponentes[nInd][::oDados:posicaoCampo("EST_REVINI")]
			cRevFimCmp  := aComponentes[nInd][::oDados:posicaoCampo("EST_REVFIM")]
			cRegra      := aComponentes[nInd][::oDados:posicaoCampo("EST_ALTERN")]
			lQtdFixa    := IIf(aComponentes[nInd][::oDados:posicaoCampo("EST_FIXA")] == '1', .T., .F. )
			nFatPerda   := aComponentes[nInd][::oDados:posicaoCampo("EST_PERDA")]
			aSize(aOperacoes, 0)
			aOperacoes  := aComponentes[nInd][::oDados:posicaoCampo("EST_OPERA")]

			//Verifica se o componente est� v�lido, de acordo com a data da necessidade.
			If !Empty(dDtIniCmp) .Or. !Empty(dDtFimCmp)
				If  dDtIniCmp > dData  .Or. dDtFimCmp < dData
					Loop
				EndIf
			EndIf

			//Verifica se a revis�o do componente est� v�lida de acordo com a revis�o atual do produto PAI.
			If cRevIniCmp > cRevisao .Or. cRevFimCmp < cRevisao
				Loop
			EndIf

			//Desconsidera opcionais desselecionados
			cOpcGrp  := aComponentes[nInd][::oDados:posicaoCampo("EST_GRPOPC")]
			cOpcItem := aComponentes[nInd][::oDados:posicaoCampo("EST_ITEOPC")]
			If !Empty(cOpcGrp) .AND. !::oOpcionais:selecionado(cIDOpcPai, cOpcGrp, cOpcItem)
				Loop
			EndIf

			lError := .F.
			cBloqueio := ::oDados:retornaCampo("PRD", 1, cComponente, "PRD_BLOQUE", @lError, .F., , /*lProximo*/, , , .F. /*lVarios*/)

			If !lError .And. cBloqueio == "1"
				//Produto bloqueado.
				Loop
			EndIf

			nNecComp := Self:ajustarNecessidadeExplosao(cComponente, nNecPai, nNecComp, lQtdFixa, @nFPotencia, aComponentes[nInd][::oDados:posicaoCampo("EST_POTEN")], nFatPerda)
			If nNecComp != 0
				cIDOpc := ::oOpcionais:retornaIDComponente(cIDOpcPai, cComponente, cTRT)

				//Avalia produto fantasma
				If aComponentes[nInd][::oDados:posicaoCampo("EST_FANT")]
					::oFantasma:processaFantasma(cProduto, cComponente, nNecComp, nPerLead, cIDOpc, aClone(aBaixaPorOP), cList, @oRastrPais, Iif(Empty(cFantasma), cProduto, cFantasma))

				Else
					::oLogs:log(STR0017 + AllTrim(cComponente) + STR0018 + cValToChar(nPerLead) + STR0019 + cValToChar(nNecComp), "19") //"Saida estrutura :" + " - Periodo: " + " - nNecess: "

					Self:oSeletivos:setaProdutoValido(cComponente)

					//Faz o Lock
					::oDados:oMatriz:trava(DtoS(dData) + cComponente + Iif(!Empty(cIDOpc), "|" + cIDOpc, ""))

					If lRastreia .And. aBaixaPorOP != Nil

						//Evento 004 - Identifica Per�odo LeadTime para Gerar o Evento 004
						If Self:oDados:oParametros["lEventLog"] .AND. nNecComp != 0
							nPerLedCmp := nPeriodo
							dLeadTime  := soDominio:oDados:aPeriodos[nPeriodo]
							soDominio:oLeadTime:aplicar(cComponente, @nPerLedCmp, @dLeadTime, nNecComp, .F.)  //Aplica o Lead Time do produto
						EndIf

						::oRastreio:inclusoesNecessidade(aBaixaPorOP, cComponente, cIDOpc, cTRT, nQtdEstrut, nPerLead, cRegra, @nNecComp, cRevisao, cRoteiro, , @oRastrPais, lQtdFixa, nFPotencia, nFatPerda, Iif(Empty(cFantasma), cProduto, cFantasma), cVersao, !Empty(cFantasma), aOperacoes, dLeadTime)
					EndIf

					::oDados:gravaSaidaEstrutura(dData, cComponente, nNecComp, nPerLead, cIDOpc, .T., .F.) //Mant�m Lock

					//Libera Lock
					::oDados:oMatriz:destrava(DtoS(dData) + cComponente + Iif(!Empty(cIDOpc), "|" + cIDOpc, ""))

				EndIf
			EndIf
		Next
		If lRastreia .and. aBaixaPorOP != Nil
			nTotal     := Len(aBaixaPorOP)
			For nInd := 1 to nTotal
				lError := .F.
				cChave := aBaixaPorOP[nInd][ABAIXA_POS_CHAVE]
				If oRastrPais[cChave] != Nil .AND. lUpdRstPai .AND. Empty(cFantasma)
					::oRastreio:oDados_Rastreio:oDados:setItemAList(cList, cChave, oRastrPais[cChave], @lError)
					aSize(oRastrPais[cChave], 0)
					oRastrPais[cChave] := Nil
				EndIf
			Next
		EndIf
	EndIf

	::oDados:setaArea(aAreaPRD)
	aSize(aAreaPRD, 0)

	::oDados:setaArea(aAreaMAT)
	If Empty(cFantasma)
		::oDados:gravaCampo("MAT", 1, DtoS(::aPeriodos[nPeriodo]) + cProduto, "MAT_EXPLOD", .T.)
	EndIf

Return lReturn

/*/{Protheus.doc} ajustarNecessidadeExplosao
Ajusta a Necessidade da Explosao de Estrutura do Componente com Base na Necessidade do Produto Pai, Fator, Perda, Qtd.Fixa, ente outros.
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01, cComponente, caracter, codigo do componente
@param 02, nNecPai    , numero  , quantidade da necessidade do produto pai
@param 03, nNecComp   , numero  , quantidade do componente na estrutura do produto pai
@param 04, lQtdFixa   , logico  , indica se o componente utiliza a quantidade fixa
@param 05, nFPotencia , numero  , fator de potencia do componente para aplicacao
@param 06, nFPotEstru , numero  , fator de potencia do componente na estrutura do produto pai
@param 07, nFatPerda  , numero  , fator de perda do componente na estrutura do produto pai
@return nNecComp, numero, necessidade do componente apos ajustes de necessiade na explosao da estrutura
/*/
METHOD ajustarNecessidadeExplosao(cComponente, nNecPai, nNecComp, lQtdFixa, nFPotencia, nFPotEstru, nFatPerda) CLASS MrpDominio

	If Self:oMOD:produtoMOD(cComponente, Self:oDados)
		nNecComp := Self:oMOD:converteQuantidadeMOD(nNecComp, nNecPai, lQtdFixa, Self:oParametros)
	ElseIf !lQtdFixa
		nNecComp := nNecComp * nNecPai
	EndIf
	nNecComp := Self:aplicarPotencia(cComponente, @nFPotencia, nNecComp, nFPotEstru)
	nNecComp := Self:aplicarPerda(nNecComp, nFatPerda)
	nNecComp := ::oDados:oDominio:arredondarExplosao(cComponente, nNecComp)

Return nNecComp

/*/{Protheus.doc} aplicarPotencia
Aplica potencia do produto
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 cProduto  , caracter, codigo do produto relacionado 
@param 02 nFPotencia, numero  , fator de potencia para utilizacao, retorna por referencia
@param 03 nNecComp  , numero  , necessidade do componente apos aplicacao do fator de potencia
@param 04 nFPotEstru, numero  , fator de potencia para o produto na estrutura do produto pai
@return nNecComp, numero, necessidade ap�s aplica��o do fator de potencia
/*/
METHOD aplicarPotencia(cProduto, nFPotencia, nNecComp, nFPotEstru) CLASS MrpDominio

	If nFPotencia == Nil .AND. ::oDados:oDominio:getUsoPotencia(cProduto)
		nFPotencia := nFPotEstru
	EndIf

	If nFPotencia != Nil .AND. nFPotencia > 0
		nNecComp := nNecComp * (nFPotencia/100)
	EndIf

Return nNecComp

/*/{Protheus.doc} aplicarPerda
Aplica perda do produto
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 nNecComp , numero, necessidade original do componente
@param 02 nFatPerda, numero, fator de perda para aplica��o
@return nNecComp, numero, necessidade ap�s aplica��o do fator de perda
/*/
METHOD aplicarPerda(nNecComp, nFatPerda) CLASS MrpDominio
	If nFatPerda > 0
		nNecComp := (nNecComp/(100-nFatPerda))*100
	EndIf
Return nNecComp



/*/{Protheus.doc} periodoMaxComponentes
Altera o periodo maximo de calculo dos componentes do produto para nPeriodo
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto, caracter, codigo do produto pai
@param 02 - nPeriodo, numero  , periodo maximo a ser atualizado nos componentes
@return lReturn, logico, indica se o produto possui estrutura
/*/
METHOD periodoMaxComponentes(cProduto, nPeriodo) CLASS MrpDominio
	Local aAreaPRD     := Nil
	Local aComponentes := {}
	Local aPendencias  := {}
	Local cComponente  := ""
	Local lError       := .F.
	Local lExecuta
	Local lReturn      := .T.
	Local nMenorPerio
	Local nIndAux      := 0
	Local nIndComp
	Local nPos         := 0
	Local nPerMaxCmp   := -1
	Local nPerInicio
	Local oPendencias  := ::oDados:oPendencias
	Local oEstruturas  := ::oDados:oEstruturas

	aAreaPRD     := ::oDados:retornaArea("PRD")

	//Retorna array com os componentes da estrutura
	oEstruturas:getRow(1, cProduto,, @aComponentes)

	If !Empty(aComponentes)
		aAreaMAT    := ::oDados:retornaArea("MAT")

		//Percorre componentes da estrutura
		For nIndComp := 1 to Len(aComponentes)
			cComponente := aComponentes[nIndComp][::oDados:posicaoCampo("EST_CODFIL")]
			oPendencias:trava(cComponente)

			//Recupera array de pendencias pai deste componente
			aPendencias := ::oDados:retornaLinha("PEN", 1, cComponente, @lError, .T.)
			lExecuta    := .T.
			If lError .And. nPeriodo == -1
				lExecuta := .F.
			ElseIf lError
				aPendencias := {}
			EndIf

			If lExecuta
				//Grava periodo maximo array
				nPos := aScan(aPendencias, {|x| AllTrim(x[1]) == AllTriM(cProduto) })
				If nPos > 0
					aPendencias[nPos][2] := nPeriodo
				Else
					aAdd(aPendencias, {cProduto, nPeriodo})
				EndIf
				::oDados:gravaLinha("PEN", 1, cComponente, aPendencias, @lError, .T.)

				//Avalia array e grava menor periodo na PRD
				For nIndAux := 1 to Len(aPendencias)
					If aPendencias[nIndAux][2] != -1
						If nMenorPerio == Nil .Or. aPendencias[nIndAux][2] < nMenorPerio
							nMenorPerio := aPendencias[nIndAux][2]
						EndIf
					EndIf
				Next
				If nMenorPerio == Nil
					nMenorPerio := -1
				EndIf

				//Grava periodo maximo
				nPerMaxCmp := ::oDados:retornaCampo("PRD", 1, cComponente, "PRD_NPERMA" , @lError)
				If nPerMaxCmp != nMenorPerio
					If ::oDados:reservaProduto(cComponente, @nPerInicio, @nPerMaxCmp);
						.And. nPerMaxCmp != nMenorPerio
						::oDados:gravaCampo("PRD", 1, cComponente, "PRD_NPERMA", nPerMaxCmp, .T.)
					EndIf
					::oDados:liberaProduto(cComponente)

				EndIf
			EndIf
			oPendencias:destrava(cComponente)
		Next
	EndIf

	::oDados:setaArea(aAreaPRD)

Return lReturn

/*/{Protheus.doc} checarFimLoop
Identifica se deve finalizar o loopNiveis
@author    brunno.costa
@since     25/04/2019
@version 1.0
@return lReturn, logico, indica se pode finalizar o loopNiveis, nao existem pendencias
/*/
METHOD checarFimLoop(nInexistLoop, cNivelAtu) CLASS MrpDominio
	Local lReturn     := .T.
	Local nProdutos   := 0
	Local nProdutosOk := 0
	Local oProdutos   := ::oDados:oProdutos

	nProdutos   := oProdutos:getflag("nProdutosT")
	nProdutosOk := oProdutos:getflag("nProdCalcT") + nInexistLoop

	::oLogs:log(STR0012 + " " + STR0008 + "("+cValToChar(nProdutos)+") x " + STR0013 + "("+cValToChar(nProdutosOk)+") - " + STR0014 + cNivelAtu, "1") //"Analise reinicio do loop de niveis" + "Total Produtos" + "Total lProdutos Ok" + "Nivel Atual: "

	//Limpa variaveis de controle do loop
	oProdutos:setflag("nProdCalcT", 0, .F., .F.)
	nInexistLoop := 0

	If nProdutos != nProdutosOk
		lReturn := .F.
	EndIf

	If lReturn
		oProdutos:setflag("cNivMin", "00", .F., .F.)
		::oLogs:log(STR0012 + STR0020 + oProdutos:getflag("cNivMin", .F., .F.), "1") //"Analise reinicio do loop de niveis" + " - retorno true - Nivel minimo: "
	Else
		If oProdutos:getflag("cNivMin", .F., .F.) != "00"
			::oLogs:log(STR0012 + STR0021 + oProdutos:getflag("cNivMin", .F., .F.), "1") //"Analise reinicio do loop de niveis" + "- retorno false - Nivel minimo: "
		Else
			::oLogs:log(STR0012 + STR0021 + oProdutos:getflag("cNivMin", .F., .F.) + " " +;
			            STR0008 + "("+cValToChar(nProdutos)+") != " + STR0013 + "("+cValToChar(nProdutosOk)+")", "1") //"Analise reinicio do loop de niveis" + "- retorno false - Nivel minimo: " + "Total Produtos" + "Total Produtos Ok"
		EndIf
	EndIf

Return lReturn

/*/{Protheus.doc} destruir
Funcao responsavel por destruir os objetos e finalizar threads
@author    brunno.costa
@since     25/04/2019
@version 1.0
/*/
METHOD destruir() CLASS MrpDominio

	Local lEventLog := Self:oEventos:lHabilitado

	//Destroi objeto da camada de dados
	::oDados:destruir()

	Sleep(2000)
	//Finaliza as Threads de processamento
	PCPIPCFinish(::oParametros["cSemaforoThreads"], 100, ::oParametros["nThreads"])

	//Finaliza as Threads de exporta��o
	PCPIPCFinish(::oParametros["cSemaforoThreads"] + "MAT", 100, ::oParametros["nThreads_MAT"])
	PCPIPCFinish(::oParametros["cSemaforoThreads"] + "AGL", 100, ::oParametros["nThreads_AGL"])
	If lEventLog
		PCPIPCFinish(::oParametros["cSemaforoThreads"] + "EVT", 100, ::oParametros["nThreads_EVT"])
	EndIf
Return

/*/{Protheus.doc} aguardaProcesso
Aguarda o t�rmino do processamento do LoopNiveis.
Somente retorna true quando o processo das threads filhas estiver finalizado.

@author    lucas.franca
@since     04/07/2019
@version 1.0
@return lExit, logico, indica que o processamento foi finalizado
/*/
METHOD aguardaProcesso(lFimPrd, cNivelAtu, cNivelAnt, nInexistente, nInxstLoop, nReinicios, nDistrib, nIgnorados) CLASS MrpDominio
	Local cNewNiv      := ""
	Local cNivAux      := ""
	Local cProduto     := ""
	Local lExit        := .F.
	Local nThreads     := ::oParametros["nThreads"]
	Local nAux         := 0
	Local nProcess     := 0
	Local nProdutoNX   := 0
	Local nProdutoTX   := 0
	Local nProcessNv   := 0
	Local nTentativa   := 0
	Local nJobLiveLock := 0
	Local oDados       := ::oDados
	Local oProdutos    := oDados:oProdutos
	Local oMatriz      := oDados:oMatriz
	//Local oLiveLock    := oDados:oLiveLock

	//Incrementa inexistentes
	oProdutos:setflag("nProdCalcN" + cNivelAnt, nInexistente, .F., .F., .T.)
	oProdutos:setflag("nProcessNv"            , nInexistente, .F., .F., .T.)
	oProdutos:setflag("nProcess"              , nInexistente, .F., .F., .T.)
	::oLogs:log(STR0102 + cNivelAnt + STR0103 + cValTOChar(nInexistente), "42") //"Incremento nProcessNv no nivel " 'XX' ", inexistentes "

	nInexistente := 0

	//Ao trocar de nivel, aguarda finalizar todas as Threads do nivel anterior
	If ::oParametros["lPorNivel"] .AND. nThreads > 0
		While IPCCount(::oParametros["cSemaforoThreads"]) < nThreads
			nTentativa++
			If nTentativa > 1000
				::oLogs:log(STR0146 + "IPCCount(::oParametros[cSemaforoThreads]) < nThreads : " + cValToChar(IPCCount(::oParametros["cSemaforoThreads"])) + " <  " + cValToChar(nThreads), "1") //"Falha Aguardando Troca de Nivel - 1000 tentativas - "
				Exit
			EndIf
			Sleep(50)
		EndDo
		nTentativa := 0

		nProdutoNX := oProdutos:getflag("nProdutosN" + cNivelAnt)
		nProcessNv := oProdutos:getflag("nProcessNv")
		While nProcessNv < nProdutoNX
			nTentativa++
			If nTentativa > 10
				::oLogs:log(STR0004 + "(" + cValToChar(nProcessNv)+") < " + STR0005 + "("+cValToChar(nProdutoNX)+")", "1") //"Aguardando troca de nivel - Falha 10 tentativas: Produtos Ok no Nivel" + "Produtos nivel"
				Exit
			EndIf
			Sleep(50)
			nProcessNv  := oProdutos:getflag("nProcessNv")
		EndDo
		nTentativa := 0
		oProdutos:setflag("nProcessNv", 0, .F., .F.)

	//Ao executar todos os niveis, aguarda finalizar todas as Threads, exceto thread de LiveLock
	ElseIf lFimPrd
		If nThreads > 0
			//nJobLiveLock := oLiveLock:getflag("nJobLiveLock")
			//While (IPCCount(::oParametros["cSemaforoThreads"]) + nJobLiveLock) < nThreads
			nTentativa := 0
			While IPCCount(::oParametros["cSemaforoThreads"]) < nThreads
				nTentativa++
				If nTentativa > 1000
					::oLogs:log(STR0146 + "IPCCount(::oParametros[cSemaforoThreads]) < nThreads : " + cValToChar(IPCCount(::oParametros["cSemaforoThreads"])) + " <  " + cValToChar(nThreads), "1") //"Falha Aguardando Troca de Nivel - 1000 tentativas - "
					Exit
				EndIf
				nAux := 50
				Sleep(nAux)
			EndDo
		Endif

		nProdutoTX := oProdutos:getflag("nProdutosT")
		nProcess   := oProdutos:getflag("nProcess")
		While oProdutos:getflag("nProcess") < nProdutoTX
			nTentativa++
			If nTentativa > 10
				::oLogs:log(STR0007 + "("+cValToChar(nProcess)+") < " + STR0008 + "("+cValToChar(nProdutoTX)+")", "1") //"Aguardando troca de nivel - Falha 10 tentativas: Total Produtos Ok" + "Total Produtos"
				Exit
			EndIf
			Sleep(50)
			nProcess := oProdutos:getflag("nProcess")
		EndDo
		nTentativa := 0
		oProdutos:setflag("nProcess", 0, .F., .F.)
	EndIf

	//Seta minimo atual
	If ::oParametros["lPorNivel"]
		nProdutoNX := oProdutos:getflag("nProdutosN" + cNivelAnt)
		If nProdutoNX	== oProdutos:getflag("nProdCalcN" + cNivelAnt)
			oMatriz:trava("cNivMin")
			If oProdutos:getflag("cNivMin") == cNivelAnt
				oProdutos:setflag("cNivMin", cNivelAtu)
				::oLogs:log(StrTran(StrTran(STR0010, "CNIVANT", cNivelAnt), "CNIVATU", cNivelAtu), "1") //"Troca nivel minimo atual de 'CNIVANT' para 'CNIVATU'"
			EndIf
			oMatriz:destrava("cNivMin")
		EndIf

	Else

		cNivAux    := oProdutos:getflag("cNivMin")
		nProdutoNX := oProdutos:getflag("nProdutosN" + cNivAux)
		If nProdutoNX	== oProdutos:getflag("nProdCalcN" + cNivAux)
			oMatriz:trava("cNivMin")
			For nAux := Val(cNivAux)+1 to 99
				cNewNiv   := PadL(cValToChar(nAux), 2, "0")
				If oProdutos:getflag("nProdCalcN"  + cNewNiv) != Nil
					Exit
				EndIf
			Next
			oProdutos:setflag("cNivMin", cNewNiv)
			::oLogs:log(StrTran(StrTran(STR0010, "CNIVANT", cNivelAnt), "CNIVATU", cNivelAtu), "1") //"Troca nivel minimo atual de 'CNIVANT' para 'CNIVATU'"
			oMatriz:destrava("cNivMin")
		EndIf
	EndIf

	If lFimPrd  //EOF
		If nThreads > 0
			//Ao executar todos os niveis, aguarda finalizar todas as Threads
			nTentativa := 0
			While IPCCount(::oParametros["cSemaforoThreads"]) < nThreads
				nTentativa++
				If nTentativa > 1000
					::oLogs:log(STR0146 + "IPCCount(::oParametros[cSemaforoThreads]) < nThreads : " + cValToChar(IPCCount(::oParametros["cSemaforoThreads"])) + " <  " + cValToChar(nThreads), "1") //"Falha Aguardando Troca de Nivel - 1000 tentativas - "
					Exit
				EndIf
				Sleep(50)
			EndDo
		Endif

		//Analisa necessidade de reinicio do loop
		oMatriz:trava("cNivMin")
		cNivelAtu := oProdutos:getflag("cNivMin", .F., .F.)
		cProduto  := oProdutos:getflag("cPriNivel" + cNivelAtu, .F., .F.)
		If nJobLiveLock == 0 .AND. ::checarFimLoop(@nInxstLoop, cNivelAtu) //Finaliza
			oProdutos:setflag("cNivMin", "00", .F., .F.)
			oMatriz:destrava("cNivMin")
			::oLogs:log("**************************************************", "1")
			::oLogs:log(STR0015, "1") //"Termino execucao do loop de niveis"
			::oLogs:log(STR0066 + cValToChar(nReinicios)     , "L") //"Reinicios do loop de calculo: "
			::oLogs:log(STR0067 + cValToChar(nDistrib) , "L") //"Distribuicoes de demanda para calculo da necessidade dos produtos: "
			::oLogs:log(STR0068 + cValToChar(nIgnorados)     , "L") //"Checagens de produtos sem pendencias / ignorados: "
			::oLogs:log("**************************************************", "1")
			lExit := .T.

		Else                 //Reinicia
			nReinicios++
			cProduto  := oProdutos:getflag("cPriNivel01", .F., .F.)
			::oLogs:log("**************************************************", "1")
			::oLogs:log(STR0016 + AllTrim(cProduto) + " - " + Time(), "1") //"Reiniciando o loop no produto: "
			::oLogs:log(STR0066 + cValToChar(nReinicios)            , "1") //"Reinicios do loop de calculo: "
			::oLogs:log(STR0067 + cValToChar(nDistrib)        , "1") //"Distribuicoes de demanda para calculo da necessidade dos produtos: "
			::oLogs:log(STR0068 + cValToChar(nIgnorados)            , "1") //"Checagens de produtos sem pendencias / ignorados: "
			::oLogs:log("**************************************************", "1")

			oProdutos:setflag("cNivMin", "01", .F., .F.)
			oMatriz:destrava("cNivMin")

		EndIf
		nJobLiveLock := 0
	EndIf

Return lExit

/*/{Protheus.doc} registraProcessados
Funcao respons�vel por atualizar os status dos registros que foram processados
@author    marcelo.neumann
@since     08/07/2019
@version 1.0
/*/
METHOD registraProcessados() CLASS MrpDominio

	::oDados:oCargaMemoria:registraProcessados()

Return

/*/{Protheus.doc} arredondarExplosao
Faz o arredondamento da quantidade de explos�o da estrutura de acordo com os par�metros do produto.

@author    lucas.franca
@since     14/10/2019
@version 1.0
@param cCompon , Character, C�digo do produto que est� sendo calculada a necessidade
@param nQtdOrig, Numeric  , Quantidade original calculada durante a explos�o.
@return nQuant , Numeric  , Quantidade arredondada.
/*/
METHOD arredondarExplosao(cCompon, nNecess) CLASS MrpDominio
	Local aAreaPRD := {}
	Local aDados   := {}
	Local cNum     := ""
	Local nQuant   := nNecess
	Local nDecimal := 0
	Local nDif     := 0
	Local lAtual   := cCompon == ::oDados:oProdutos:cCurrentKey
	Local lError   := .F.

	If !lAtual
		aAreaPRD := ::oDados:retornaArea("PRD")
	EndIf

	aDados := ::oDados:retornaCampo("PRD", 1, cCompon, {"PRD_TIPDEC","PRD_NUMDEC"}, @lError, lAtual, , /*lProximo*/, , , .T. /*lVarios*/)

	If !lError
		Do Case
			Case aDados[1] == 1 //Normal
				//Arredonda de acordo com a qtd. de decimais da estrutura.
				nQuant := Round(nQuant, ::oParametros["nStructurePrecision"])
			Case aDados[1] == 2 //Arredonda
				//Se os decimais forem >= a 5, arredonda para cima. Caso contr�rio, arredonda para baixo.
				nQuant := Round(nQuant, aDados[2])
			Case aDados[1] == 3 //Incrementa
				//Se houver qualquer quantidade decimal, ir� arredondar para cima.
				nDecimal := nQuant - Int(nQuant)
				nQuant   := Int(nQuant)
				If nDecimal <> 0
					If aDados[2] > 0
						//Se utiliza casas decimais, ir� incrementar na qtd. decimal.
						nDif     := nDecimal - NoRound(nDecimal, aDados[2])
						nDecimal := NoRound(nDecimal, aDados[2])
						If nDif <> 0
							cNum := "0." + StrZero(1, aDados[2])
							nDecimal += Val(cNum)
							nQuant   += nDecimal
						Else
							//Se nDif for igual a 0, todos os decimais devem ser considerados sem fazer incremento.
							nQuant += nDecimal
						EndIf
					Else
						//N�o utiliza decimais, ir� incrementar a quantidade inteira.
						nQuant++
					EndIf
				EndIf
			Case aDados[1] == 4 //Trunca
				//Desconsidera os decimais, sem fazer arredondamento p/ cima.
				nDecimal := nQuant - Int(nQuant)
				nQuant := Int(nQuant)
				If aDados[2] > 0
					nQuant += NoRound(nDecimal, aDados[2])
				EndIf
		EndCase
		aSize(aDados  , 0)
	EndIf

	If !lAtual
		::oDados:setaArea(aAreaPRD)
		aSize(aAreaPRD, 0)
	EndIf

Return nQuant

/*/{Protheus.doc} agrupaRevisoes
Funcao respons�vel por atualizar os status dos registros que foram processados
@author    brunno.costa
@since     16/10/2019
@version 1.0
@param 01 - cProduto   , caracter, codigo do produto
@param 02 - nPeriodo   , numero  , periodo referencia
@param 03 - aBaixaPorOP, array   , array com os dados de rastreabilidade origem (Documentos Pais)
								   {{1 - Id Rastreabilidade,;
								     2 - Documento Pai,;
								     3 - Quantidade Necessidade,;
								     4 - Quantidade Estoque,;
								     5 - Quantidade Baixa Estoque,;
								     6 - Quantidade Substitui��o},...}
@return aRevisoes, Array, array com os dados agrupados por revis�o:
                {cRevisao           ,
				 cRoteiro           ,
				 cLocal             ,
				 nQtde Total Revis�o,
				 aBaixaPorOP        }
/*/
METHOD agrupaRevisoes(cProduto, nPeriodo, aBaixaPorOP) CLASS MrpDominio

	Local aRevisoes   := {}
	Local aNames      := {}
	Local aVersao     := {}
	Local cRotPad     := ""
	Local cRevPad     := ::revisaoProduto(cProduto, @cRotPad)
	Local cRevisao    := ""
	Local nInd        := 0
	Local nNecess     := 0
	Local nTotal      := Len(aBaixaPorOP)
	Local oRevisoes   := Iif(nTotal > 0, JsonObject():New(), Nil)
	Local nPosRevisao := ::oVersaoDaProducao:getPosicao("REVISAO", "aVersao")
	Local nPosRoteiro := ::oVersaoDaProducao:getPosicao("ROTEIRO", "aVersao")
	Local nPosLocal   := ::oVersaoDaProducao:getPosicao("LOCAL"  , "aVersao")
	Local nPosCodigo  := ::oVersaoDaProducao:getPosicao("CODIGO" , "aVersao")
	Local lVersao     := ::oVersaoDaProducao:possui(cProduto)

	For nInd := 1 to nTotal
		//Identifica a Vers�o da Produ��o
		nNecess := aBaixaPorOP[nInd][ABAIXA_POS_NECESSIDADE]

		If aBaixaPorOP[nInd][ABAIXA_POS_QTD_SUBSTITUICAO] > 0
			nNecess -= aBaixaPorOP[nInd][ABAIXA_POS_QTD_SUBSTITUICAO]
		EndIf

		If lVersao
			aVersao := ::oVersaoDaProducao:identifica(cProduto, nNecess, ::aPeriodos[nPeriodo])
		EndIf

		If !lVersao .OR. Empty(aVersao) .OR. Empty(aVersao[nPosRevisao])
			cRevisao := cRevPad
			aVersao  := {cRevPad,;
						cRotPad,; //TODO - Revisar regra do roteiro
						"",""}       //TODO - Inserir regra de local padr�o
		Else
			cRevisao := aVersao[nPosRevisao]
			aVersao := {cRevisao,;
						aVersao[nPosRoteiro],;
						aVersao[nPosLocal],;
						aVersao[nPosCodigo]}
		EndIf

		//Agrupa resultados em objeto JSON
		If oRevisoes[cRevisao] == Nil
			oRevisoes[cRevisao]    := {aVersao[nPosRevisao],;
									   aVersao[nPosRoteiro],;
									   aVersao[nPosLocal]  ,;
									   nNecess             ,;
									   {aBaixaPorOP[nInd]} ,;
									   aVersao[nPosCodigo]}
			aSize(aVersao, 0)
		Else
			oRevisoes[cRevisao][aRevisoes_POS_QTDE_TOTAL] += nNecess
			aAdd(oRevisoes[cRevisao][aRevisoes_POS_aBaixaPorOP], aBaixaPorOP[nInd])

		EndIf
	Next

	If nTotal == 0
		aAdd(aRevisoes, {cRevPad,;
						 cRotPad,;           //TODO - Revisar regra do roteiro
						 ""      ,;          //TODO - Inserir regra de local padr�o
						 nNecess ,;
						 {}      ,;
						 ""      })
	Else
		//Converte Json em Array de Retorno
		aNames := oRevisoes:GetNames()
		nTotal := Len(aNames)
		For nInd := 1 to nTotal
			cRevisao := aNames[nInd]
			aAdd(aRevisoes, oRevisoes[cRevisao])
		Next

		aSize(aNames, 0)
		aNames := Nil

		FreeObj(oRevisoes)
	EndIf

	//Limpa array ::aVersoes da Mem�ria
	::oVersaoDaProducao:limpaMemoria()

Return aRevisoes

/*/{Protheus.doc} calcularLotes
Calcula os lotes do produto

@author    lucas.franca
@since     22/10/2019
@version 1.0
@param 01 - cProduto   , caracter, codigo do produto
@param 02 - nNecess    , numero  , necessidade do produto
@param 03 - nQtdLotes  , numero  , retorna por refer�ncia a qtd. sumarizada dos lotes.
@return aQtd, Array, Array com as quantidades de necessidade do produto
/*/
METHOD calcularLotes(cProduto, nNecess, nQtdLotes) CLASS MrpDominio
	Local aDadosPrd  := {}
	Local aAreaPRD   := {}
	Local aQtd       := {}
	Local cNivel     := ""
	Local lAtual     := cProduto == ::oDados:oProdutos:cCurrentKey
	Local lUsaEmbal  := Self:oParametros["lPackingQuantityFirst"]
	Local lUsaLtMin  := Self:oParametros["lBreakByMinimunLot"]
	Local lQuebra    := .F.
	Local nIndex     := 0
	Local nTotal     := 0
	Local nLE        := 0
	Local nQE        := 0
	Local nToler     := 0
	Local nTolerPrd  := 0
	Local nLoteMin   := 0
	Local nQtdUlt    := 0

	If nNecess > 0
		//Busca as informa��es do Produto.
		If !lAtual
			aAreaPRD := Self:oDados:retornaArea("PRD")
		EndIf

		aDadosPrd := Self:oDados:retornaCampo("PRD", 1, cProduto, {"PRD_LE", "PRD_QTEMB", "PRD_LM", "PRD_TOLER", "PRD_NIVEST"}, , lAtual, , /*lProximo*/, , , .T. /*lVarios*/)

		If !lAtual
			Self:oDados:setaArea(aAreaPRD)
		EndIf

		nLE       := aDadosPrd[1] //Lote econ�mico
		nQE       := aDadosPrd[2] //Quantidade embalagem
		nLoteMin  := aDadosPrd[3] //Lote M�nimo
		nTolerPrd := aDadosPrd[4] //Toler�ncia
		cNivel    := aDadosPrd[5] //N�vel do produto

		//Verifica se o lote deve ser quebrado, ou se deve ser somado.
		If cNivel == "99"
			lQuebra  := Self:oParametros["lPurchaseRequestPerLot"]
			nLoteMin := nQE //N�vel 99, utiliza como lote m�nimo a QTD DE EMBALAGEM.
		Else
			lQuebra  := Self:oParametros["lProductionOrderPerLot"]
		EndIf

		//Verifica se o Lote Econ�mico deve ser substitu�do pelo lote m�nimo
		If nLE == 0 .And. nLoteMin > 0 .And. Self:oParametros["lMinimunLotAsEconomicLot"]
			nLE := nLoteMin
		Endif

		If nLE > 0
			nToler := ( nLE * nTolerPrd ) / 100
			// Tenta Lote Minimo e apos isso Lote Economico
			If lUsaEmbal .And. nNecess <= nLoteMin
				aAdd(aQtd, nLoteMin)
				nNecess := 0

			ElseIf nNecess < nLE

				If lUsaLtMin .And. nLoteMin > 0
					//Faz a quebra por lote m�nimo
					nTotal := Ceiling(nLE / nLoteMin)

					IF nTotal < 1
						nTotal := 1
					EndIf

					For nIndex := 1 To nTotal
						aAdd(aQtd, nLoteMin)
					Next nIndex
				Else
					//Necessidade menor que lote econ�mico, ir� produzir o lote econ�mico.
					aAdd(aQtd, nLE)
				EndIf
				nNecess := 0
			EndIf

			While nNecess > 0
				If nNecess < Iif(lUsaLtMin, nLoteMin, nLE) - nToler
					If nLoteMin > 0
						//Produz o lote m�nimo
						If nNecess <= nLoteMin
							aAdd(aQtd, nLoteMin)
						Else
							If nNecess > ( Round( nNecess / nLoteMin, 0) * nLoteMin )
								nQtdUlt := ( Round( nNecess / nLoteMin, 0) + 1 ) * nLoteMin
							Else
								nQtdUlt := Round( nNecess / nLoteMin, 0) * nLoteMin
							EndIf
							aAdd(aQtd, nQtdUlt)
						Endif
					Else
						//N�o possui lote m�nimo, produz o restante para fechar a qtd.
						aAdd(aQtd, nNecess)
					EndIf
				Else
					//Produz o lote m�nimo ou econ�mico, de acordo com a parametriza��o.
					If lUsaLtMin
						aAdd(aQtd, Iif(nLoteMin == 0, nNecess, nLoteMin))
					Else
						aAdd(aQtd, Iif(nLE == 0, nNecess, nLE))
					EndIf

					If nNecess <= Iif(lUsaLtMin, nLoteMin, nLE) + nToler
						nNecess := 0
					EndIf
				EndIf

				//Desconta a qtd. do lote da qtd total, e verifica se � necess�rio nova quebra (volta o loop).
				nNecess -= aQtd[Len(aQtd)]
			End
		Else
			//N�o possui lote econ�mico, verifica se utiliza o lote m�nimo.
			aAdd(aQtd, Max(nLoteMin, nNecess))
		EndIf
	Else
		//Necessidade zerada ou negativa.
		aAdd(aQtd, nNecess)
	EndIf

	//Sumarizador das quantidades.
	nQtdLotes := 0

	nTotal := Len(aQtd)
	For nIndex := 1 To nTotal
		nQtdLotes += aQtd[nIndex]
	Next

	//Se n�o utiliza a quebra, soma todas as qtds. da quebra em uma �nica quantidade.
	If !lQuebra
		aSize(aQtd, 0)
		aAdd(aQtd, nQtdLotes)
	EndIf

	aSize(aDadosPrd, 0)
	aSize(aAreaPRD , 0)

Return aQtd

/*/{Protheus.doc} ajustarArrayQuebrasQtd
Converte o ARRAY de lotes (array com as quantidades quebradas) para uma matriz
contendo a quantidade e o documento.

@author    lucas.franca
@since     22/10/2019
@version 1.0
@param 01 - cProduto   , caracter, codigo do produto
@param 02 - nNecess    , numero  , necessidade do produto
@param 03 - nQtdLotes  , numero  , retorna por refer�ncia a qtd. sumarizada dos lotes.
@return aQtd, Array, Array com as quantidades de necessidade do produto
/*/
METHOD ajustarArrayQuebrasQtd(aQuant) CLASS MrpDominio
	Local aQtdDocum := Nil
	Local nTotal    := Len(aQuant)
	Local nIndex    := 0

	aQtdDocum := Array(nTotal)

	For nIndex := 1 To nTotal
		aQtdDocum[nIndex] := {aQuant[nIndex], ""}
	Next nIndex
Return aQtdDocum

METHOD validaLotesVencidos(cProduto, nPeriodo, nSldInicial, aRetLotes, cChaveProd) CLASS MrpDominio
	Local aPRDPeriodos  := {}
	Local aRetAux       := {}
	Local lErrorMAT     := .F.
	Local nSaldo        := nSldInicial
	Local nPerAnter     := 0
	Local nX            := 0
	Local nQTdLote      := 0
	Local cChave        := ""
	Local oDados        := ::oDados

	nPerAnter := ::periodoAnterior(cChaveProd, nPeriodo, @aPRDPeriodos, @lErrorMAT)
	If nPerAnter > 0 .AND. aRetLotes != Nil
		For nX := 1 To Len(aRetLotes)
			If aRetLotes[nX][2] >= ::aPeriodos[nPerAnter] .And. aRetLotes[nX][2] < ::aPeriodos[nPeriodo]
				nQTdLote += aRetLotes[nX][1]
			EndIf
		Next nX

		IF nQTdLote > 0
			If oDados:existeMatriz(cChaveProd, nPerAnter, @cChave)
				aRetAux   := oDados:retornaCampo("MAT", 1, cChave, {"MAT_SAIPRE","MAT_SAIEST"}, @lErrorMAT, , , , , , .T. /*lVarios*/)
				If nQTdLote - (aRetAux[1] + aRetAux[2] ) > 0
					If ( nQTdLote - (aRetAux[1] + aRetAux[2] ) ) <= nSldInicial
						nSaldo := nSldInicial - ( nQTdLote - (aRetAux[1] + aRetAux[2] ) )
					Else
						nSaldo := 0
					EndIf
				EndIf
			Else
				If nQTdLote <= nSaldo
					nSaldo -= nQTdLote
				Else
					nSaldo := 0
				EndIf
			EndIf
		EndIf
	EndIf

Return nSaldo

/*/{Protheus.doc} MRPCalculo
Calculo a necessidade do produto (Function - JOB)
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - oDominio    , objeto  , objeto da classe MrpDominio passado somente sem threads adicionais
@param 02 - cProduto    , caracter, codigo do produto pra calcular a necessidade
@param 03 - cIDOpc      , caracter, codigo do IDOpc do produto
@param 04 - nRecursivas , numero  , numero de chamadas recursivas
@param 05 - lPercent    , logico  , indica se deve imprimir o pecentual atual do processamento de calculo
@param 06 - nInexistLoop, numero  , utilizado quando lPercent == true, indica o numero de produtos pulados devido inexistencia da matriz
@param 07 - lTLiveLock  , logico  , indica que a chamada e oriunda de thread exclusiva de livelock
@return lReturn, logico, indica se realizou o calculo do produto sem pendencias
/*/
Function MRPCalculo(oDominio, cProduto, cIDOpc, nRecursivas, lPercent, nInexistLoop, lTLiveLock)

	Local aBaixaPorOP     := {}
	Local aDesfezAlt      := {}
	Local aMinMaxAlt      := {}
	Local aPRDPeriodos    := {}
	Local aRetAux         := {}
	Local aRetLotes       := {}
	Local cLogAux
	Local cProdOrig       := ""
	Local cChave          := ""
	Local cChaveProd      := cProduto + Iif(!Empty(cIDOpc) , "|" + cIDOpc, "")
	Local cNivelAtu
	Local dLeadTime
	Local lReturn         := .T.
	Local lErrorMAT       := .F.
	Local lExplode        := .T.
	Local lFalha          := .F.
	Local lErrorPRD       := .F.
	Local lWait           := .F.
	Local lConsumiu       := .F.
	Local lRastreia
	Local nConsumo        := 0
	Local nPerAux1
	Local nPerAux2
	Local nPerIndMin      := 1
	Local nPerIndMax      := 1
	Local nPeriodo        := -1
	Local nPerLead
	Local nSldInicial     := 0
	Local nEntPrev        := 0
	Local nSaiPrev        := 0
	Local nSaiEstru       := 0
	Local nSaldo          := 0
	Local nSldAux         := 0
	Local nNecess         := 0
	Local nOldNec         := 0
	Local nOldSldIni      := 0
	Local nPerInicio
	Local nPerMaximo
	Local nPerMinGrv
	Local nPerMaxGrv
	Local nPerCalGrv      := -1
	Local nPosAlt
	Local nUltPerCal
	Local nInd
	Local nThreads
	Local nThreadPRD
	Local oDados
	Local oLogs
	Local oMatriz
	Local oProdutos
	Local oLiveLock
	Local lErrorAlt     := .F.
	Local lReinicia     := .F.
	Local lAlternativo  := .F.
	Local lSubProduto   := .F.

	Local nTotAlt
	Local nIndAlt
	Local aAuxAlt

	Default nRecursivas := 0
	Default lPercent    := .F.
	Default lTLiveLock  := .F.

	If slRecria .Or. ((soDominio == Nil .Or. soDominio:aPeriodos == Nil .Or. Len(soDominio:aPeriodos) == 0) .AND. oDominio == Nil)
		If slRecria
			slRecria := .F.
			If soDominio != Nil
				FreeObj(soDominio)
			EndIf
		EndIf
		MRPPrepDom()

	ElseIf oDominio != Nil
		soDominio := oDominio

	EndIf

	oDados        := soDominio:oDados
	soAlternativo := soDominio:oAlternativo
	oLogs         := soDominio:oLogs
	oProdutos     := oDados:oProdutos
	oMatriz       := oDados:oMatriz
	oLiveLock     := oDados:oLiveLock
	lRastreia     := soDominio:oParametros["lRastreia"]
	nThreads      := soDominio:oParametros["nThreads"]

	If oDados:reservaProduto(cChaveProd, @nPerInicio, @nPerMaximo, .F., @nUltPerCal, @cNivelAtu, @nThreadPRD)
		//Log inicio de calculo do produto, apos reserva: - Inicio
		cLogAux := STR0024 + cNivelAtu + STR0025 + AllTrim(cChaveProd) + STR0026 + cValToChar(nPerInicio) + "/" + cValToChar(nPerMaximo) + STR0027 + cValToChar(nUltPerCal) //" - Nivel " + " - Inicio de calculo do produto: " + " - Periodos: " + " - Ultimo periodo calculado: "
		oLogs:log(cLogAux, "2")
		If nPerInicio <= nPerMaximo .And. nPerInicio != 1
			oLogs:log(cLogAux, "3")
		EndIf
		//Log inicio de calculo do produto, apos reserva: - Fim

		//Avalia periodos onde o produto existe
		//Verifica Periodo existente igual ou maior
		If !oDados:existeMatriz(cChaveProd)
			//Cria lista deste produto para registrar os periodos
			If !oDados:oMatriz:existList("Periodos_Produto_" + cChaveProd)
				oDados:oMatriz:createList("Periodos_Produto_" + cChaveProd)
			EndIf
		EndIf
		oDados:oMatriz:setItemList("Periodos_Produto_" + cChaveProd, "1", 1)
		oDados:oMatriz:getAllList("Periodos_Produto_" + cChaveProd, @aPRDPeriodos, @lErrorMAT)
		If !lErrorMAT
			aSort(aPRDPeriodos, , , { |x,y| x[2] < y[2] } )
		EndIf
		nPerIndMin := aScan(aPRDPeriodos, { |nPer| nPer[2] >= nPerInicio } )
		nPerIndMin := Iif(nPerIndMin == 0, 1, nPerIndMin)

		//Identifica maior periodo
		nPerIndMax := 0
		nPerAux2   := -1
		While nPerAux2 > 0 .or. nPerAux2 == -1
			nPerAux2 := aScan(aPRDPeriodos, { |nPer| nPer[2] <= nPerMaximo }, (nPerIndMax + 1) )
			If nPerAux2 > 0 .and. aPRDPeriodos[nPerAux2][2] <= oDados:oParametros["nPeriodos"]
				nPerIndMax := nPerAux2
			EndIf
		EndDo

		//Identifica exist�ncia de Alternativos e sequ�ncias: Min-Max
		aMinMaxAlt := oDados:oAlternativos:getItemAList("min_max", cChaveProd, @lErrorAlt)

		aRetAux     := oDados:retornaCampo("PRD", 1, cChaveProd, {"PRD_LOTVNC", "PRD_LSUBPR"}, @lErrorPRD, .F. /*lAtual*/, , , , , .T. /*lVarios*/)
		aRetLotes   := aRetAux[1]
		lSubProduto := aRetAux[2]

		//Percorre Todos os Periodos Existentes do Produto na Matriz
		For nPerAux1 := nPerIndMin to nPerIndMax
			nPeriodo    := aPRDPeriodos[nPerAux1][2]
			lWait       := .F.
			nEntPrev    := 0
			nSaiPrev    := 0
			nSaiEstru   := 0
			nSaldo      := 0
			nNecess     := 0
			nConsumo    := 0
			lErrorMAT   := .F.
			lReinicia   := .F.

			If !oDados:foiCalculado(cChaveProd, nPeriodo, .T.)
				If nPeriodo == nPerInicio
					nSldInicial := soDominio:saldoInicial(cChaveProd, nPerInicio)
					nOldSldIni  := nSldInicial
				Else
					nSldInicial := soDominio:validaLotesVencidos(cProduto,nPeriodo,nSldInicial,aRetLotes,cChaveProd)
				EndIf

				//Loga Evento 001 - Saldo Inicial Negativo (Primeiro Per�odo)
				If nPeriodo == 1 .AND. nSldInicial < 0
					soDominio:oEventos:loga("001", cChaveProd, soDominio:aPeriodos[nPeriodo], {nSldInicial})
				EndIf

				//Matriz Flegada? - Verifica existencia de registro + reserva
				If oDados:existeMatriz(cChaveProd, nPeriodo, @cChave)
					//Grava Saldo Inicial
					oLogs:log(STR0018 + cValToChar(nPeriodo) + STR0028 + AllTrim(cChaveProd) + STR0029 + cValToChar(nSldInicial), "4") //" - Periodo: " + " - Produto: " + "- Saldo inicial (MAT_SLDINI): "
					oDados:gravaCampo("MAT", 1, cChave  , "MAT_SLDINI", nSldInicial, .T.)
					lConsumiu   := .T.

					//Efetua calculo base do MRP
					aRetAux   := oDados:retornaCampo("MAT", 1, cChave, {"MAT_ENTPRE", "MAT_SAIPRE", "MAT_SAIEST"}, @lErrorMAT, , , , , , .T. /*lVarios*/)
					nEntPrev  := aRetAux[1]
					nSaiPrev  := aRetAux[2]
					nSaiEstru := aRetAux[3]

					nSaldo  := nSldInicial + nEntPrev - nSaiPrev - nSaiEstru

					oLogs:log(STR0018 + cValToChar(nPeriodo) + STR0028 + AllTrim(cChaveProd) + " -> " + cValToChar(nSaldo) + " := " + cValToChar(nSldInicial) + " + " + cValToChar(nEntPrev) + " - " + cValToChar(nSaiPrev) + " - " + cValToChar(nSaiEstru), "CS") //" - Periodo: " + " - Produto: " +

				Else
					nSaldo  := nSldInicial
					lErrorMAT := .T.
				EndIf
			Else
				If oDados:existeMatriz(cChaveProd, nPeriodo, @cChave)
					If lRastreia
						aRetAux     := oDados:retornaCampo("MAT", 1, cChave, {"MAT_SLDINI", "MAT_ENTPRE", "MAT_SAIPRE", "MAT_SALDO", "MAT_SAIEST"}, .T. /*lAtual*/, , , , , .T. /*lVarios*/)
						If aRetAux != Nil
							nSldInicial := aRetAux[1]
							nEntPrev    := aRetAux[2]
							nSaiPrev    := aRetAux[3]
							nSaldo      := aRetAux[4]
							nSaiEstru   := aRetAux[5]
						Else
							aRetAux := {}
							If nPeriodo == nPerInicio
								nSldInicial := soDominio:saldoInicial(cChaveProd, nPerInicio)
								nOldSldIni  := nSldInicial
							EndIf
							nSaldo  := nSldInicial
						EndIf

					Else
						nSaldo    := oDados:retornaCampo("MAT", 1, cChave, "MAT_SALDO", @lErrorMAT, .T. /*lAtual*/)
					EndIf

				Else
					If nPeriodo == nPerInicio
						nSldInicial := soDominio:saldoInicial(cChaveProd, nPerInicio)
						nOldSldIni  := nSldInicial
					EndIf
					nSaldo  := nSldInicial
				EndIf
			EndIf

			//Desfaz Explos�es Anteriores deste Produto
			If lRastreia
				soDominio:oRastreio:desfazExplosoes(cChaveProd, nPeriodo)
			EndIf

			//Desfaz substituicoes deste produto original
			If !lErrorAlt
				nConsumo := -nSaldo
				nSaldo   := soAlternativo:desfazSubstituicoes(cProduto, nSaldo, nPeriodo, , @aDesfezAlt)
				nConsumo += nSaldo
				nEntPrev += nConsumo
			EndIf

			//Identifica chaves Pai para Gera��o de Rastreabilidade, consome saldos estoque
			If lRastreia
				aBaixaPorOP := soDominio:oRastreio:baixaPorOP(cProduto, cIDOpc, (nSldInicial + nEntPrev), nPeriodo, @lAlternativo, nSaiPrev+nSaiEstru)

				//Desfaz substitui��es no rastreamento
				If nConsumo != 0 .AND. !lErrorAlt
					soDominio:oRastreio:desfazSubstituicoes(cChaveProd, nPeriodo, @aBaixaPorOP, nConsumo)
				EndIf

				nSaldo := soDominio:reduzPMPSaldo(nSaldo, aBaixaPorOP)
			EndIf

			//Consome alternativos deste produto
			If nSaiEstru > 0 .AND. !lErrorAlt
				nSldAux := nSaldo
				nSaldo  := soAlternativo:consumirAlternativos(cProduto, nSaldo, nPeriodo, cProdOrig, @lWait, cIDOpc, nSaiEstru, aBaixaPorOP, aMinMaxAlt)

				If lWait
					nPerMinGrv := nPeriodo
					nPerMaxGrv := nPeriodo
					soDominio:periodoMaxComponentes(cChaveProd, (nPeriodo - 1))
					lReturn := .F.
					oLogs:log(STR0032 + AllTrim(cChaveProd) + STR0018 + cValToChar(nPeriodo), "5") //"Interrompendo calculo produto, nao conseguiu consumir os Alternativos, produto: " + " - Periodo: "
					If nThreads > 1
						oLiveLock:setResult(cChaveProd, 1, .F., .T., .T.)
					EndIf

				Else
					If lRastreia .AND. nSldAux != nSaldo
						soDominio:oRastreio:atualizaSubstituicao(cChaveProd, nPeriodo, @aBaixaPorOP)
					EndIf
					nTotAlt     := Len(soAlternativo:aPeriodos_Alternativos)
					For nIndAlt := 1 to nTotAlt
						aAuxAlt := soAlternativo:aPeriodos_Alternativos[nIndAlt]

						//Verifica se o alternativo teve consumo diferente
						nPosAlt := aScan(aDesfezAlt, {|x| AllTrim(x[2]) == AllTrim(aAuxAlt[1]) })
						If nPosAlt == 0 .OR. aAuxAlt[3] != aDesfezAlt[nPosAlt][5]//{cProduto, cAlternativo, nPeriodo, nConsAlt, nConsOrig, ::cOrdem}
							oDados:gravaPeriodosProd(aAuxAlt[1], aAuxAlt[2])
						EndIf
					Next
					aSize(soAlternativo:aPeriodos_Alternativos, 0)

				EndIf
			EndIf

			If !lErrorMAT
				oLogs:log(STR0018 + cValToChar(nPeriodo) + STR0028 + AllTrim(cChaveProd) + STR0033 + cValToChar(nSaldo), "4") //" - Periodo: " + " - Produto: " + " - Saldo (MAT_SALDO): "
				oDados:gravaCampo("MAT", 1, cChave, "MAT_SALDO" , nSaldo)
			EndIf

			If nPerCalGrv == -1 .Or. nPerCalGrv < nPeriodo
				nPerCalGrv := nPeriodo
			EndIf

			//Interrompe o c�lculo quando um dos produtos alternativos ainda n�o foi calculado
			If !lReturn
				Exit
			EndIf

			//Aplica politica de estoque
			nNecess := soDominio:politicaDeEstoque(cChaveProd, nSaldo, , nPeriodo, @lReinicia, @aBaixaPorOP, lAlternativo)


			//Identifica que o produto deve ser calculado novamente, mas iniciando do pr�ximo per�odo.
			//Necess�rio para quando utiliza calend�rio e o per�odo atual n�o � uma data �til.
			//Ir� jogar as necessidades no primeiro per�odo �til de acordo com o calend�rio.
			If lReinicia
				If nPeriodo+1 < oDados:oParametros["nPeriodos"]
					oDados:oMatriz:setItemList("Periodos_Produto_" + cChaveProd, cValToChar(nPeriodo+1), nPeriodo+1)
					If aScan(aPRDPeriodos, {|x| x[2] == nPeriodo+1}) == 0
						aPRDPeriodos[nPerAux1][1] := cValToChar(nPeriodo+1)
						aPRDPeriodos[nPerAux1][2] := nPeriodo+1
					Else
						//O pr�ximo per�odo j� est� no array de per�odos do produto, ent�o n�o � necess�rio reiniciar.
						lReinicia := .F.
					EndIf
				Else
					lReinicia := .F.
				EndIf
			EndIf

			//TODO - REMOVER?
			//Atualiza periodo maximo dos componentes
			If nPerInicio > 1
				soDominio:periodoMaxComponentes(cChaveProd, -1)
			EndIf

			If nNecess > 0 .And. lErrorMAT
				//Inclui novo registro na Matriz, quando ha necessidade e ainda nao existe
				If !oDados:existeMatriz(cChaveProd, nPeriodo) .And. oMatriz:trava(cChave)
					lErrorMAT := .F.
					oLogs:log(STR0034 + AllTrim(cChaveProd) + STR0018 + cValToChar(nPeriodo) + STR0035 + cValToChar(0), "6") //"Inclui novo registro na Matriz: " + " - Periodo: " + " - nNecessidade: "
					oDados:atualizaMatriz(soDominio:aPeriodos[nPeriodo], cProduto, cIDOpc)
					oDados:gravaPeriodosProd(cChaveProd, nPeriodo)
					oMatriz:destrava(cChave)

				EndIf

				//Grava Saldo Inicial
				oLogs:log(cValToChar(nPeriodo) + " - " + AllTrim(cChaveProd) + " - " + "MAT_SLDINI: " + cValToChar(nSldInicial), "4")
				oDados:gravaCampo("MAT", 1, cChave, "MAT_SLDINI", nSldInicial)
				lConsumiu   := .T.

				//Grava Saldo
				oLogs:log(STR0018 + cValToChar(nPeriodo) + STR0028 + AllTrim(cChaveProd) + STR0033 + cValToChar(nSaldo), "4") //" - Periodo: " + " - Produto: " + " - Saldo (MAT_SALDO): "
				oDados:gravaCampo("MAT", 1, cChave, "MAT_SALDO" , nSaldo, .T.)
				If nPerCalGrv == -1 .Or. nPerCalGrv < nPeriodo
					nPerCalGrv := nPeriodo
				EndIf
			EndIf

			//TODO - Vale a pena deixar este GET l� em cima junto da primeira busca??
			nOldNec := oDados:retornaCampo("MAT", 1, cChave, "MAT_NECESS", @lErrorMAT)

			//Calcula LeadTime
			nPerLead  := nPeriodo
			dLeadTime := soDominio:aPeriodos[nPeriodo]
			soDominio:oLeadTime:aplicar(cChaveProd, @nPerLead, @dLeadTime, nNecess)  //Aplica o Lead Time do produto
			oDados:gravaCampo("MAT", 1, cChave, "MAT_DTINI" , dLeadTime)


			//Explosao negativa da estrutura: desfaz explos�es anteriores em caso de reprocessamento de alternativos ou fantasmas NAS RESVIS�ES CORRESPONDENTES A EXPLOS�O ANTERIOR
			//Quando lRastreia = true, desfaz explos�es na matriz via objeto de Rastreio: oRastreio:desfazExplosoes
			If !lRastreia .AND. (lExplode .Or. nNecess != nOldNec) .And. oDados:retornaCampo("MAT", 1, cChave, "MAT_EXPLOD", @lErrorMAT, .T. /*lAtual*/)
				If nOldNec != 0
					oLogs:log(STR0036 + AllTrim(cChaveProd) + STR0026 + cValToChar(nPeriodo) + "/" + cValToChar(nPerMaximo) + STR0037 + cValToChar(nOldNec), "7") //"Explosao Estrutura (negativa):" + " - Periodos: " + " - Necessidade anterior: "
					If !(lSubProduto .AND. soDominio:oSubProduto:processar(cProduto, cIDOpc, nPeriodo, -nOldNec))
						soDominio:explodirEstrutura(cProduto, -nOldNec, , nPerLead, nPeriodo, "", cIDOpc)
					EndIf
					oDados:gravaCampo("MAT", 1, cChave, "MAT_EXPLOD", .F.)
				EndIf
			EndIf

			If !lErrorMAT
				oLogs:log(STR0018 + cValToChar(nPeriodo) + STR0028 + AllTrim(cChaveProd) + STR0038 + cValToChar(nNecess), "7") //" - Periodo: " + " - Produto: " + " - Necessidade (MAT_NECESS): "
				oDados:gravaCampo("MAT", 1, cChave, "MAT_NECESS", nNecess, .T.)
			EndIf

			//Explode nivel da estrutura
			If lExplode
				If !lErrorMAT .And. nNecess != 0
					If (lSubProduto .AND. soDominio:oSubProduto:processar(cProduto, cIDOpc, nPeriodo, nNecess))
						nPerMinGrv := nPeriodo
						nPerMaxGrv := nPeriodo
						nPerCalGrv := nPeriodo - 1
						soDominio:periodoMaxComponentes(cChaveProd, (nPeriodo - 1))
						lReturn := .F.
						oLogs:log(STR0148 + AllTrim(cChaveProd) + STR0018 + cValToChar(nPeriodo), "5") //"Interrompe c�lculo ap�s gerar necessidade do subproduto: "

						//Interrompe o C�LCULO - SubProduto
						Exit
					Else
						If lRastreia
							soDominio:explodeAgrupadoPorRevisao(cChaveProd, cProduto, nPerLead, nPeriodo, nPerMaximo, cIDOpc, aBaixaPorOP)

						Else
							oLogs:log(STR0039 + AllTrim(cChaveProd) + STR0018 + cValToChar(nPeriodo) + "/" + cValToChar(nPerMaximo) + STR0038 + cValToChar(nNecess), "7") //"Explosao Estrutura : " + " - Periodo: " + " - Necessidade (MAT_NECESS): "
							soDominio:explodirEstrutura(cProduto, nNecess, nPerLead, nPeriodo, "", cIDOpc, aBaixaPorOP)

						EndIf
					EndIf
				EndIf

				//Determina periodo minimo para gravacao na PRD
				If (nPerAux1 + 1) <= Len(aPRDPeriodos)
					nPerMinGrv := aPRDPeriodos[nPerAux1 + 1][2]          //Proximo
				Else
					nPerMinGrv := soDominio:oParametros["nPeriodos"] + 1 //FIM: Ultimo +1
				EndIf
			EndIf

			If lReinicia
				nPerAux1--
			EndIf

			nSldInicial := (nNecess + nSaldo)

			//Limpa Array aBaixaPorOP
			For nInd := 1 to Len(aBaixaPorOP)
				aSize(aBaixaPorOP[nInd], 0)
				aBaixaPorOP[nInd] := Nil
			Next
			aSize(aBaixaPorOP, 0)
		Next

		//Revisa necessidade de reinicio caso haja alteracao do nPerInicio a partir de outra Thread
		oProdutos:trava(cChaveProd)
		If oDados:retornaCampo("PRD", 1, cChaveProd, "PRD_REINIC", @lErrorPRD, .F. /*lAtual*/)
			oDados:gravaCampo("PRD", 1, cChaveProd, "PRD_REINIC", .F., .T.)
			oProdutos:destrava(cChaveProd)
			If nThreads > 1
				oLiveLock:setResult(cChaveProd, 1, .F., .T., .T.)
			EndIf
			If nRecursivas < soDominio:oParametros["nRecursivas_LiveLock"]
				oLogs:log(STR0077 + AllTrim(cChaveProd) + STR0078 + cValToChar(nPerInicio), "1") //"Reinicio de calculo do produto '" + "' devido alteracao na matriz a partir de outra Thread que requer recalculo a partir do periodo: "
				MRPCalculo(oDominio, cChaveProd,, (nRecursivas + 1))
			Else
			    soDominio:oDados:incrementaTotalizador(cChaveProd, .T., .F.) //Incrementa Processo
				oLogs:log(STR0091 + AllTrim(cChaveProd) + STR0092 + cValToChar(nPerInicio), "1") //"Calculo do produto '" + "' interrompido devido LiveLock - Alteracao na matriz a partir de outra Thread que requer recalculo a partir do periodo: "
			EndIf

			//Libera PRD para outras Threads
			oDados:liberaProduto(cChaveProd)
			cLogAux := STR0024 + cNivelAtu + STR0045 + AllTrim(cChaveProd) + STR0026 + cValToChar(nPerInicio) + "/" + cValToChar(nPerMaximo) + STR0027 + cValToChar(nUltPerCal) //" - Nivel " + " - Liberou reserva do produto " + " - Periodos: " + " - Ultimo periodo calculado: "
			oLogs:log(cLogAux, "2")
			If nPerInicio <= nPerMaximo .And. nPerInicio != 1
				oLogs:log(cLogAux, "3")
			EndIf

			If lPercent //Chama impressao de % de cobertura de calculo
				oLogs:percentualAtual(oProdutos, nInexistLoop, oProdutos:getflag("nProdCalcT"))
			EndIf

			Return .T.
		Else
			oProdutos:destrava(cChaveProd)
		EndIf

		nPerCalGrv := Iif(nPerMinGrv > soDominio:oParametros["nPeriodos"], nPerMinGrv, nPerCalGrv)
		oDados:gravaPeriodosProd(cChaveProd, nPerMinGrv, nPerMaxGrv, nPerCalGrv)

		If nPerCalGrv != soDominio:oParametros["nPeriodos"]
			//" - Produto: " + " - Periodo Minimo: " + " - Periodo Maximo: " + " - Ult. Calculado: "
			oLogs:log(STR0028 + AllTrim(cChaveProd) + STR0040 + cValToChar(nPerMinGrv) + STR0041 + cValToChar(nPerMaxGrv) + STR0042 + cValToChar(nPerCalGrv), "8")
		EndIf

		If nPerMaximo != soDominio:oParametros["nPeriodos"]
			//"Fim loopNiveis produto com pendencia!" + " - Periodo Maximo: " + "Ultimo Periodo para Calculo"
			oLogs:log(STR0043 + STR0041 + "("+cValToChar(nPerMaximo)+") != " + STR0044 + " ("+cValToChar(soDominio:oParametros["nPeriodos"])+"): " + cChaveProd + Iif(lExplode,"lExplode == true", "lExplode == false"), "9")
			lReturn := .F.

		EndIf

		If lReturn
			soDominio:oDados:incrementaTotalizador(cChaveProd, .F., .T.) //Incrementa Calculo
		EndIf

		//Libera PRD para outras Threads
		oDados:liberaProduto(cChaveProd)
		cLogAux := STR0024 + cNivelAtu + STR0045 + AllTrim(cChaveProd) + STR0026 + cValToChar(nPerInicio) + "/" + cValToChar(nPerMaximo) + STR0027 + cValToChar(nPerCalGrv) //" - Nivel " + " - Liberou reserva do produto " + " - Periodos: " + " - Ultimo periodo calculado: "
		oLogs:log(cLogAux, "2")
		If nPerInicio <= nPerMaximo .And. nPerInicio != 1
			oLogs:log(cLogAux, "3")
		EndIf
	Else
		lFalha := .T.
		//oLogs:log(STR0046 + AllTrim(cChaveProd) + STR0047 + cValtoChar(nThreadPRD), "10") //"Falha na reserva do produto " + " - Produto reservado para outra Thread: "
		//Comentado para fins de performance, mantido para facilitar analises de falhas
	EndIf

	If !lTLiveLock
		soDominio:oDados:incrementaTotalizador(cChaveProd, .T., .F.) //Incrementa Processo
	EndIf

	If lPercent //Chama impressao de % de cobertura de calculo
		oLogs:percentualAtual(oProdutos, nInexistLoop, oProdutos:getflag("nProdCalcT"))
	EndIf

	//Limpa Array aBaixaPorOP
	For nInd := 1 to Len(aBaixaPorOP)
		aSize(aBaixaPorOP[nInd], 0)
		aBaixaPorOP[nInd] := Nil
	Next
	aSize(aBaixaPorOP, 0)
	aBaixaPorOP := Nil

	aSize(aRetAux, 0)
	aRetAux := Nil

	For nInd := 1 to Len(aPRDPeriodos)
		aSize(aPRDPeriodos[nInd], 0)
		aPRDPeriodos[nInd] := Nil
	Next
	aSize(aPRDPeriodos, 0)
	aPRDPeriodos := Nil

Return lReturn

/*/{Protheus.doc} PCPCalcLL
Chamada single-thread das execucoes relacionadas a Live-Lock (Function - JOB)
@author    brunno.costa
@since     25/04/2019
@version 1.0
/*/
Function PCPCalcLL()
	Local aLiveLock  := {}
	Local cChaveProd
	Local lPendencia := .F.
	Local lFirst     := .T.
	Local nInd       := 0
	Local oDados
	Local oLiveLock
	Local oLogs
	Local oStatus
	Local nCalculos  := 0

	If slRecria .Or. soDominio == Nil
		If slRecria
			slRecria := .F.
			If soDominio != Nil
				FreeObj(soDominio)
			EndIf
		EndIf
		MRPPrepDom()
	EndIf

	oDados     := soDominio:oDados
	oLiveLock  := oDados:oLiveLock
	oLogs      := soDominio:oLogs
	oStatus    := MrpDados_Status():New(oDados:oParametros["ticket"])

	oLogs:log(STR0115 + cValToChar(ThreadID()), "5") //"Inicio calculos single-thread: "
	While lFirst .OR. lPendencia
		lFirst     := .F.
		lPendencia := .F.
		aSize(aLiveLock, 0)
		oLiveLock:getAllRes(@aLiveLock)
		For nInd := 1 to Len(aLiveLock)
			nCalculos++
			cChaveProd := aLiveLock[nInd][1]
			If oDados:possuiPendencia(cChaveProd)
				MRPCalculo(, cChaveProd, , , , , .T./*lTLiveLock*/)
				lPendencia := .T.
			Else
				oLiveLock:setResult(cChaveProd, 0)
			EndIf

			//Checa cancelamento a cada X delegacoes/calculos
			If Mod(nCalculos, oDados:oParametros["nX_Para_Cancel"]) == 0
				If oStatus:getStatus("status") == "4"
					Exit
				EndIf
				oLogs:log(STR0147 + cValToChar(nCalculos), "5") //"PCPCalcLL Loop Calculos: "
			EndIf

		Next nInd
	EndDo
	oLogs:log(STR0116 + cValToChar(ThreadID()), "5") //"Fim calculos single-thread: "
	oLiveLock:setflag("nJobLiveLock", 0)
Return

/*/{Protheus.doc} MRPPrepDom
Retorna o objeto do Dominio
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - lAvPeriodos, l�gico, indica se deve avaliar a cria��o do array aPeriodos
                                 (Controlado atrav�s do 4o par�metro da vari�vel global PCP_aParam: 1 - MrpDominio:calcularNivel(),
								 2 - Pr�-Carga Mem�ria, 3 - P�s Carga Calend�rio - Carga Complementar)
@return oDominio, object, objeto referente ao dom�nio
/*/
Function MRPPrepDom(lAvPeriodos)

	Local aParam
	Local aPeriodos
	Local nInd
	Local oLogs
	Local oParametros
	Local oStatus

	Default lAvPeriodos := .F.

	If soDominio == Nil .OR. Len(soDominio:oDados:aPeriodos) == 0
		GetGlbVars("PCP_aParam", @aParam)
		If aParam != Nil
			oParametros      := JsonObject():New()
			oParametros:fromJson(aParam[1])
			//Retorna Data para padr�o de oParametros
			For nInd := 1 to Len(aParam[3])
				oParametros[aParam[3][nInd]] := StoD(StrTran(oParametros[aParam[3][nInd]], "-", ""))
			Next

			oLogs     := MrpDados_Logs():New(oParametros)
			aPeriodos := aParam[2]
			soDominio := MrpDominio():New(oParametros, oLogs, .T. /*lRecursiva*/, aPeriodos)

			If lAvPeriodos .AND. aParam[4] != "3" //1 - MrpDominio:calcularNivel(), 2 - Pr�-Carga Mem�ria, 3 - P�s Carga Calend�rio - Carga Complementar
				oStatus := MrpDados_Status():New(soDominio:oDados:oParametros["ticket"])
				If oStatus:getStatus("status") != "4"
					Sleep(100)
					soDominio := MRPPrepDom(lAvPeriodos)
				EndIf
			EndIf
		EndIf
	EndIf


Return soDominio

/*/{Protheus.doc} calcularNivel
Processamento MRP - Realiza o c�lculo dos n�veis dos produtos

@type Method
@author marcelo.neumann
@since 21/11/2019
@version P12.1.27
@return Nil
*/
METHOD calcularNivel() CLASS MrpDominio

    Local oStatus := MrpDados_Status():New(::oParametros["ticket"])

	If AllTrim(::oParametros["cAutomacao"]) != "1"
		//Grava variaveis globais com os dados para popular as Statics das Threads
		PutGlbVars("PCP_aParam", {::oParametros:toJson(), {}, {}, "1"})

		oStatus:setStatus("niveis", "2") //"Executando"
		oStatus:persistir(Self:oDados)
		oStatus:setStatus("tempo_recalculo_niveis" , MicroSeconds())
		If ::oParametros["nThreads"] > 1 .AND. AllTrim(::oParametros["cAutomacao"]) != "2"
			PCPIPCGO(::oParametros["cSemaforoThreads"], .F., "PCPRecaNiv", ::oParametros["ticket"]) //Delega para processamento em Thread
		Else
			oStatus:preparaAmbiente(::oDados)
			PCPRecaNiv(::oParametros["ticket"], ::oParametros["cAutomacao"])
		EndIf

	EndIf

Return

/*/{Protheus.doc} PCPRecaNiv
Retorna o objeto do Dominio
@author marcelo.neumann
@since 21/11/2019
@version 1.0
@param cTicket, caracter, n�mero do ticket que est� sendo processado
@param cAutomacao, caracter, C�digo identificador de execu��o da automa��o de testes
@return Nil
/*/
Function PCPRecaNiv(cTicket, cAutomacao)

	Local cErro   := ""
	Local oStatus := MrpDados_Status():New(cTicket)

	Default cAutomacao := "0"

	If cAutomacao == "2" .OR. MRPProced(@cErro) //Desvia Automa��o - Base ADVPR n�o possui procedure
		oStatus:setStatus("niveis", "3") //Conclu�do
	Else
		oStatus:gravaErro("niveis", cErro)
	EndIf

Return

/*/{Protheus.doc} getUsoPotencia
busca o uso de pot�ncia pelo produto em quest�o
@author    douglas.heydt
@since     22/11/2019
@version 1.0
@param 01 - cCompon, character, componente no qual consta informa��o se usa ou n�o pot�ncia
@return lPotencia, l�gico, .T. caso usa pot�ncia, .F. falso caso n�o use.
/*/
METHOD getUsoPotencia(cCompon) CLASS MrpDominio
	Local aAreaPRD  := {}
	Local cPotencia := ""
	Local lAtual    := cCompon == ::oDados:oProdutos:cCurrentKey
	Local lError    := .F.
	Local lPotencia := .F.

	If !lAtual
		aAreaPRD := ::oDados:retornaArea("PRD")
	EndIf

	cPotencia := ::oDados:retornaCampo("PRD", 1, cCompon, "PRD_CPOTEN", @lError, lAtual, , /*lProximo*/, , , .F. /*lVarios*/)
	If !lError .AND. cPotencia == "1"
		lPotencia := .T.
	EndIf

	If !lAtual
		::oDados:setaArea(aAreaPRD)
		aSize(aAreaPRD, 0)
	EndIf

Return lPotencia

/*/{Protheus.doc} reduzPMPSaldo
Refaz o saldo atual considerando as demandas do tipo Plano Mestre (PMP).

Como o PMP n�o faz baixa de estoque, e a quantidade do PMP � adicionada como sa�da,
retira as quantidades referentes ao PMP do saldo final.

@author    lucas.franca
@since     06/02/2020
@version 1.0
@param 01 - nSaldo     , Numeric, Saldo atual calculado.
@param 02 - aBaixaPorOP, Array  , array com os dados de rastreabilidade origem (Documentos Pais)
								   {{1 - Id Rastreabilidade,;
								     2 - Documento Pai,;
								     3 - Quantidade Necessidade,;
								     4 - Quantidade Estoque,;
								     5 - Quantidade Baixa Estoque,;
								     6 - Quantidade Substitui��o},...}
@return nSaldo, Numeric, Novo saldo, descontando as quantidades de PMP
/*/
METHOD reduzPMPSaldo(nSaldo, aBaixaPorOP) CLASS MrpDominio
	Local nTotal := 0
	Local nIndex := 0

	If aBaixaPorOP == Nil
		Return nSaldo
	EndIf

	nTotal := Len(aBaixaPorOP)

	For nIndex := 1 To nTotal
		If aBaixaPorOP[nIndex][ABAIXA_POS_TIPO_PAI] == "1"
			nSaldo += aBaixaPorOP[nIndex][ABAIXA_POS_NEC_ORIG]
		EndIf
	Next nIndex

Return nSaldo
