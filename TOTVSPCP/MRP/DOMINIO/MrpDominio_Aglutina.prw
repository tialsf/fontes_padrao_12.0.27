#INCLUDE 'protheus.ch'
#INCLUDE 'MRPDominio.ch'

#DEFINE ADADOS_POS_ORIGEM            1
#DEFINE ADADOS_POS_DOCUMENTO         2
#DEFINE ADADOS_POS_PRODUTO           3
#DEFINE ADADOS_POS_ID_OPCIONAL       4
#DEFINE ADADOS_POS_TRT               5
#DEFINE ADADOS_POS_QUANTIDADE        6
#DEFINE ADADOS_POS_PERIODO           7
#DEFINE ADADOS_POS_ADOC_PAI          8
#DEFINE ADADOS_POS_ADOC_PAI_ORI      1
#DEFINE ADADOS_POS_ADOC_PAI_DOC      2
#DEFINE ADADOS_POS_ADOC_PAI_QTD      3
#DEFINE ADADOS_POS_ADOC_PAI_PRODUTO  4
#DEFINE ADADOS_POS_ADOC_PAI_QTD_EMPE 5
#DEFINE ADADOS_POS_ADOC_PAI_QTD_SUBS 6
#DEFINE ADADOS_POS_ROTEIRO           9
#DEFINE ADADOS_POS_OPERACAO          10
#DEFINE ADADOS_POS_AGLUTINA          11

/*/{Protheus.doc} MrpDominio_Aglutina
Classe de Controle da Rastreabilidade de Aglutina��es de Rastreabilidade
@author    brunno.costa
@since     21/11/2019
@version   1
/*/
CLASS MrpDominio_Aglutina FROM LongClassName

	DATA oDados       AS Object //Instancia da classe de dados do MRP
	DATA oAglutinacao AS Object //Instancia de dados MrpDados_Global para controle das aglutina��es
	DATA cAglutinaSC  AS String
	DATA cAglutinaOP  AS String
	DATA lRastreiaAgl AS Logical
	DATA nIndRSubst   AS Integer
	DATA nIndrNecOr   AS Integer
	DATA nIndRegra    AS String

	//M�todos P�blicos
	METHOD new() CONSTRUCTOR

	//M�todos relacionados a Carga em Mem�ria
	METHOD aguardaCargaEstrutura()         //Aguarda Carga das Estruturas de Produtos
	METHOD aguardaProdutoCarga()           //Aguarda Carga de Produtos
	METHOD incluiRastreios(cLista, nTotal) //Processa Inclus�es de Rastreabilidade Default
	METHOD prepara(cLista, cOrigem, aDocPai, cDocumento, cProduto, cIDOpc, cTRT, nQuantidade, nPeriodo, cRegra, lDemanda, lAglutina, lQuebra, cRoteiro, cOperacao) //Avalia e Prepara Registro para Inser��o Aglutinada ou N�o

	//M�todos relacionados ao C�lculo
	METHOD preparaEInclui(cLista, cOrigem, aDocPai, cDocumento, cProduto, cIDOpc, cTRT, nQuantidade, nPeriodo, cRegra, lAglutina, lQuebra, cRoteiro, cOperacao) //Avalia necessidade de aglutina��o e inclui a rastreabilidade
	METHOD reiniciaAglutinacaoDocumentoPai(cLista, cOrigem, cDocumento, cProduto, cTRT, nPeriodo, aDocsPai)

	//M�todos relacionados � Exporta��o
	METHOD ajustesExportacao(aAglutinacao, aNovasAglu)                               //Efetua ajustes de empenho e substitui��o nos registros da aglutina��o que ser�o efetivados na HWG
	METHOD geraRegistrosAlternativos(cProduto, nPeriodo, cDocumento, cTRT, cTpDocOri, cDocOri, cProdOri, aAlteDeman, aNovasAglu, nSubstitui, oJSConOrig, oJSConAlt) //Duplica registros referente alternativos
	METHOD retornaRastreio(cProduto, nPeriodo, cDocPai, cTRT, cListOrig)             //Retorna registro aRastreio relacionado
	METHOD retornaAlternativos(cComponente, nPeriodo)                                //Retorna registros referente alternativos aAlteDeman

	//Outros M�todos
	METHOD avaliaAglutinacao(cProduto, lDemanda, cOrigem) //Avalia necessidade de aglutina��o
	METHOD getPosicao()        //Retorna a posicao do campo no array de dados

	//M�todos Internos
	METHOD proximoIDAuto(cLista)   //Identifica o pr�ximo ID Autom�tico

ENDCLASS

/*/{Protheus.doc} MrpDominio_Aglutina
M�todo construtor da classe MrpDominio_Aglutina
@author    brunno.costa
@since     21/11/2019
@version   1
@param 01 - oDados , objeto, instancia da camada de dados do MRP
@return, Self, objeto, instancia desta classe
/*/
METHOD new(oDados) CLASS MrpDominio_Aglutina

	::oDados       := oDados
	::oAglutinacao := oDados:oAglutinacao
	::cAglutinaSC  := oDados:oParametros["cConsolidatePurchaseRequest"]
	::cAglutinaOP  := oDados:oParametros["cConsolidateProductionOrder"]
	::lRastreiaAgl := oDados:oParametros["lRastreiaAglutinacao"]
	::nIndRSubst   := oDados:oRastreio:getPosicao("SUBSTITUICAO")
	::nIndrNecOr   := oDados:oRastreio:getPosicao("NEC_ORIGINAL")
	::nIndRegra    := oDados:oRastreio:getPosicao("REGRA_ALTERNATIVO")

Return Self

//********************************************/
/*M�TODOS UTILIZADOS NA CARGA EM MEM�RIA     */
//********************************************/

/*/{Protheus.doc} aguardaCargaEstrutura
Aguarda Conclus�o da Carga de Estruturas em Mem�ria
@author    brunno.costa
@since     21/11/2019
@version   1
/*/
METHOD aguardaCargaEstrutura() CLASS MrpDominio_Aglutina

	Local oStatus := MrpDados_Status():New(Self:oDados:oParametros["ticket"])

	If ::oDados:oParametros["cConsolidatePurchaseRequest"] != "2";
		.OR. ::oDados:oParametros["cConsolidateProductionOrder"] != "2"
		While ::oDados:oEstruturas:getFlag("termino_carga") != "S" .AND. oStatus:getStatus("status") != "4"
			Sleep(50)
		EndDo
	EndIf

Return Nil

/*/{Protheus.doc} aguardaProdutoCarga
Aguarda Conclus�o da Carga de Estruturas em Mem�ria
@author    brunno.costa
@since     25/03/2020
@version   12.1.30
/*/
METHOD aguardaProdutoCarga() CLASS MrpDominio_Aglutina

	Local oStatus := MrpDados_Status():New(Self:oDados:oParametros["ticket"])

	While Self:oDados:oProdutos:getFlag("termino_carga") != "S" .AND. oStatus:getStatus("status") != "4"
		Sleep(50)
	EndDo

Return Nil

/*/{Protheus.doc} incluiRastreios
Efetiva a Inclus�o das Rastreabilidades Default em Mem�ria - Utilizado na Carga em Mem�ria
@author    brunno.costa
@since     21/11/2019
@version   1
@param 01 - cLista, caracter, identificador da Lista de
/*/
METHOD incluiRastreios(cLista, nTotal) CLASS MrpDominio_Aglutina

	Local aLista    := {}
	Local lError    := .F.
	Local nIndex    := 0
	Local oRastreio := ::oDados:oDominio:oRastreio

	::oAglutinacao:getAllAList(cLista, @aLista, lError)

	If !lError
		nTotal     := Len(aLista)
		For nIndex := 1 To nTotal
			oRastreio:incluiNecessidade(aLista[nIndex][2][ADADOS_POS_ORIGEM]     ,;
										aLista[nIndex][2][ADADOS_POS_DOCUMENTO]  ,;
										aLista[nIndex][2][ADADOS_POS_PRODUTO]    ,;
										aLista[nIndex][2][ADADOS_POS_ID_OPCIONAL],;
										aLista[nIndex][2][ADADOS_POS_TRT]        ,;
										aLista[nIndex][2][ADADOS_POS_QUANTIDADE] ,;
										aLista[nIndex][2][ADADOS_POS_PERIODO]     )
			aSize(aLista[nIndex][2], 0)
			aLista[nIndex][2] := Nil
			aSize(aLista[nIndex], 0)
			aLista[nIndex]    := Nil
		Next nIndex
		aSize(aLista, 0)
		aLista := Nil
	EndIf

Return

/*/{Protheus.doc} prepara
Prepara o Registro para Inser��o Aglutinada ou Desaglutinada
@author    brunno.costa
@since     21/11/2019
@param 01 - cLista, caracter, c�digo da lista identificadora da tabela utilizada:
                              1 = Carga de Demandas (PCPCargDem)
							  2 = Carga de Empenhos
							  3 = Carga de outras Sa�das Previstas
							  4 = Explos�o da Estrutura
@param 02 - cOrigem    , caracter, identificador de origem do registro
@param 03 - aDocPai    , array   , {Tipo do Documento Pai, Documento Pai, Quantidade Necessidade Pai, Produto Pai}
@param 04 - cDocumento , caracter, identificador do documento - retorna por refer�ncia
@param 05 - cProduto   , caracter, c�digo do produto
@param 06 - cIDOpc     , caracter, c�digo do ID do Opcional
@param 07 - cTRT       , caracter, c�digo do TRT
@param 08 - nQuantidade, n�mero  , quantidade da necessidade
@param 09 - nPeriodo   , n�mero  , per�odo da necessidade
@param 10 - cRegra     , caracter,  regra de consumo dos alternativos:
								1- Valida Original; Valida Alternativo; Compra Original
								2- Valida Original; Valida Alternativo; Compra Alternativo
								3- Valida Alternativo; Compra Alternativo
@param 11 - lDemanda   , l�gico  , indica se refere-se a origem de carga de demanda
@param 12 - lAglutina  , l�gico  , indica se deve considerar aglutina��o na chave do registro
@param 13 - lQuebra    , l�gico  , indica origem em quebra de Lote Econ�mico ou Qtd. Embalagem (nIndQuebra > 1)
@param 14 - cRoteiro   , caracter, c�digo do roteiro de opera��es do produto Pai
@param 15 - cOperacao  , caracter, c�digo da opera��o deste componente no roteiro do produto Pai
@return cList, caracter, identificador desta sess�o de vari�veis globais
@version   1
@return aDados, array, aDados,array com os dados para inclus�o da necessidade:
                    {1 - Origem,
					 2 - Documento,
					 3 - Produto,
					 4 - TRT,
					 5 - Quantidade,
					 6 - Per�odo,
					 7 - Array Doc Pai{Tipo Oridem Doc Pai, Documento, Quantidade},
					 8 - lAglutina relacionado}
/*/
METHOD prepara(cLista, cOrigem, aDocPai, cDocumento, cProduto, cIDOpc, cTRT, nQuantidade, nPeriodo, cRegra, lDemanda, lAglutina, lQuebra, cRoteiro, cOperacao) CLASS MrpDominio_Aglutina

	Local aDados     := {}
	Local cChave
	Local cListaBkp  := cLista
	Local cFilAux    := ""
	Local lAglutina
	Local lError     := .F.
	Local nPos       := 0
	Local oRastreio  := ::oDados:oDominio:oRastreio
	Local aListas    := {"1", "2", "3", "4"}
	Local nIndListas

	Default cIDOpc    := ""
	Default cRegra    := "1"
	Default lDemanda  := .T.
	Default lAglutina := ::avaliaAglutinacao(cProduto, lDemanda, cOrigem)
	Default lQuebra   := .F.
	Default cRoteiro  := ""
	Default cOperacao := ""

	//Identifica a Chave do Rastreio
	If lAglutina
		cChave := cFilAux + cValToChar(nPeriodo) + cProduto + Iif(!Empty(cIDOpc) , "|" + cIDOpc, "")
	Else
		cChave := cFilAux + cOrigem + cDocumento + chr(13) + cValToChar(nPeriodo) + cProduto + Iif(!Empty(cIDOpc) , "|" + cIDOpc, "")
	EndIf

	//Adiciona registro no objeto aglutinador de rastreabilidade
	If lDemanda .OR. cLista != "4"
		::oAglutinacao:trava(cChave)
	EndIf

	If lAglutina .OR. lDemanda .OR. lQuebra
		::oAglutinacao:getFlag(cLista + chr(13) + cChave, @lError)
		If lAglutina .AND. lError .AND. cLista != "4"
			For nIndListas := 1 to 3
				lError := .F.
				If aListas[nIndListas] != cLista
					::oAglutinacao:getFlag(aListas[nIndListas] + chr(13) + cChave, @lError)
					If !lError
						cLista := aListas[nIndListas]
						Exit
					EndIf
				Else
					lError := .T.
				EndIf
			Next
		EndIf

		If lError
			::oAglutinacao:setFlag(cLista + chr(13) + cChave, .T.)
			If lAglutina
				If cLista == "4"        //Explos�o da Estrutura
					cDocumento := oRastreio:oDados_Rastreio:proximaOP()
				Else
					cDocumento := ::proximoIDAuto(cLista)
				EndIf
			EndIf
			aDados := {cOrigem                                    ,; //ADADOS_POS_ORIGEM
					   cDocumento                                 ,; //ADADOS_POS_DOCUMENTO
					   cProduto                                   ,; //ADADOS_POS_PRODUTO
					   cIDOpc                                     ,; //ADADOS_POS_ID_OPCIONAL
					   cTRT                                       ,; //ADADOS_POS_TRT
					   nQuantidade                                ,; //ADADOS_POS_QUANTIDADE
					   nPeriodo                                   ,; //ADADOS_POS_PERIODO
					   Iif(::lRastreiaAgl                         ,; //ADADOS_POS_ADOC_PAI
					       {{aDocPai[ADADOS_POS_ADOC_PAI_ORI]     ,;  //Origem Documento Pai
						     aDocPai[ADADOS_POS_ADOC_PAI_DOC]     ,;  //Documento Pai
							 nQuantidade                          ,;  //Quantidade
							 aDocPai[3] }}                        ,;  //Produto Pai
					       {})                                    ,;
					   cRoteiro                                   ,;  //ADADOS_POS_ROTEIRO
					   cOperacao                                  ,;  //ADADOS_POS_OPERACAO
					   lAglutina                                  }  //ADADOS_POS_AGLUTINA

		Else
			aDados := ::oAglutinacao:getItemAList(cLista, cChave)
			aDados[ADADOS_POS_QUANTIDADE] += nQuantidade
			If Empty(cDocumento)
				cDocumento                := aDados[ADADOS_POS_DOCUMENTO]
			EndIf
			If ::lRastreiaAgl
				nPos := aScan(aDados[ADADOS_POS_ADOC_PAI],;
				              {|x|  x[ADADOS_POS_ADOC_PAI_ORI]     == aDocPai[1];
							  .AND. x[ADADOS_POS_ADOC_PAI_DOC]     == aDocPai[2];
							  .AND. x[ADADOS_POS_ADOC_PAI_PRODUTO] == aDocPai[3] })
				If nPos == 0
					//Se possui mais de um documento aglutinado, altera a origem para o tipo "Consolidado".
					If AllTrim(cLista) != "2" //!Pr�-OP
						aDados[ADADOS_POS_ORIGEM] := "0"
					EndIf

					aAdd(aDados[ADADOS_POS_ADOC_PAI],;
					    {aDocPai[ADADOS_POS_ADOC_PAI_ORI],;
						 aDocPai[ADADOS_POS_ADOC_PAI_DOC],;
						 nQuantidade                     ,;
						 aDocPai[3]                      })
				Else
					If nQuantidade > 0
						aDados[ADADOS_POS_ADOC_PAI][nPos][ADADOS_POS_ADOC_PAI_QTD] += nQuantidade
					Else
						aDel(aDados[ADADOS_POS_ADOC_PAI], nPos)
					EndIf
				EndIf
			EndIf

		EndIf
		::oAglutinacao:setItemAList(cLista, cChave, aDados)
		cLista := cListaBkp
		If lAglutina .AND. cLista != "4" .And. cLista != "2"
			::oAglutinacao:setFlag("4" + chr(13) + cChave, .T.)
			::oAglutinacao:setItemAList("4", cChave, aDados)
		EndIf

	Else
		aDados := {cOrigem                                    ,; //ADADOS_POS_ORIGEM
				   cDocumento                                 ,; //ADADOS_POS_DOCUMENTO
				   cProduto                                   ,; //ADADOS_POS_PRODUTO
				   cIDOpc                                     ,; //ADADOS_POS_ID_OPCIONAL
				   cTRT                                       ,; //ADADOS_POS_TRT
				   nQuantidade                                ,; //ADADOS_POS_QUANTIDADE
				   nPeriodo                                   ,; //ADADOS_POS_PERIODO
				   Iif(::lRastreiaAgl                         ,; //ADADOS_POS_ADOC_PAI
				       {{aDocPai[ADADOS_POS_ADOC_PAI_ORI]     ,;  //Origem Documento Pai
					     aDocPai[ADADOS_POS_ADOC_PAI_DOC]     ,;  //Documento Pai
						 nQuantidade                          ,;  //Quantidade
					     aDocPai[3] }}                        ,;  //Produto Pai
				       {})                                    ,;
				   cRoteiro                                   ,;  //ADADOS_POS_ROTEIRO
				   cOperacao                                  ,;  //ADADOS_POS_OPERACAO
				   lAglutina                                  }  //ADADOS_POS_AGLUTINA
	EndIf

	If lDemanda .OR. cLista != "4"
		::oAglutinacao:destrava(cChave)
	EndIf

Return aDados

/*/{Protheus.doc} prepara
Prepara o Registro para Inser��o Aglutinada ou Desaglutinada
@author    brunno.costa
@since     21/11/2019
@param 01 - cLista, caracter, c�digo da lista identificadora da tabela utilizada:
                              1 = Carga de Demandas (PCPCargDem)
							  2 = Carga de Empenhos
							  3 = Carga de outras Sa�das Previstas
							  4 = Explos�o da Estrutura
@param 02 - cOrigem    , caracter, identificador de origem do registro
@param 03 - cDocumento , caracter, identificador do documento - retorna por refer�ncia
@param 04 - cProduto   , caracter, c�digo do produto
@param 05 - cTRT       , caracter, c�digo do TRT
@param 06 - nPeriodo   , n�mero  , per�odo da necessidade
@param 07 - aDocsPai   , array   , array com os documentos Pais retornado por refer�ncia {{tipo documento, documento, necessidade original}, {..}, ...}
@version   1
/*/
METHOD reiniciaAglutinacaoDocumentoPai(cLista, cOrigem, cDocumento, cProduto, cTRT, nPeriodo, aDocsPai) CLASS MrpDominio_Aglutina

	Local aDados
	Local cFilAux    := ""
	Local cChave     := cFilAux + cValToChar(nPeriodo) + cProduto + chr(13)
	Local lError     := .F.

	//Adiciona registro no objeto aglutinador de rastreabilidade
	::oAglutinacao:trava(cChave)

	::oAglutinacao:getFlag(cChave, @lError)
	If !lError
		aDados   := ::oAglutinacao:getItemAList(cLista, cChave)
		aDocsPai := aDados[ADADOS_POS_ADOC_PAI]
		::oAglutinacao:delFlag(cChave, @lError)
	EndIf

	::oAglutinacao:destrava(cChave)

Return

//********************************************************/
/*OUTROS M�TODOS                                         */
//********************************************************/

/*/{Protheus.doc} avaliaAglutinacao
Avalia necessidade de aglutina��o
@author    brunno.costa
@since     21/11/2019
@version   1
@param 01 - cProduto, caracter, c�digo do produto para an�lise
@param 02 - lDemanda, l�gico  , indica se a an�lise deve ser feita para carga de Demandas
@param 03 - cOrigem , caracter, origem da demanda
@return lAglutina, l�gico, indica se dever� realizar a aglutina��o
/*/
METHOD avaliaAglutinacao(cProduto, lDemanda, cOrigem) CLASS MrpDominio_Aglutina

	Local lAglutina := .F.

	Default lDemanda := .F.
	Default cOrigem  := ""

	If ::oDados:possuiEstrutura(cProduto)
		If cOrigem != "1" //Se a origem n�o � um Plano Mestre
			If ::cAglutinaOP == "2" .OR. (::cAglutinaOP == "3" .AND. !lDemanda)
				lAglutina := .F.
			Else
				lAglutina := .T.
			EndIf
		Else
			//Se for um PMP, ir� aglutinar somente se o par�metro estiver
			//selecionado para aglutinar tudo.
			If ::cAglutinaOP == "1"
				lAglutina := .T.
			Else
				lAglutina := .F.
			EndIf
		EndIf
	Else
		If cOrigem != "1" //Se a origem n�o � um Plano Mestre
			If ::cAglutinaSC == "2" .OR. (::cAglutinaSC == "3" .AND. !lDemanda)
				lAglutina := .F.
			Else
				lAglutina := .T.
			EndIf
		Else
			//Se for um PMP, ir� aglutinar somente se o par�metro estiver
			//selecionado para aglutinar tudo.
			If ::cAglutinaSC == "1"
				lAglutina := .T.
			Else
				lAglutina := .F.
			EndIf
		EndIf
	EndIf

Return lAglutina

/*/{Protheus.doc} getPosicao
Avalia necessidade de aglutina��o
@author    brunno.costa
@since     21/11/2019
@version   1
@param 01 - cCampo, caracter, string com o nome do campo relacionado aos dados de rastreabilidade
@Return nReturn, n�mero, posi��o padr�o do registro no array de dados
/*/
METHOD getPosicao(cCampo) CLASS MrpDominio_Aglutina

	Local nReturn := 0

	Do Case
		Case cCampo == "ORIGEM"
			nReturn := ADADOS_POS_ORIGEM
		Case cCampo == "DOCUMENTO"
			nReturn := ADADOS_POS_DOCUMENTO
		Case cCampo == "PRODUTO"
			nReturn := ADADOS_POS_PRODUTO
		Case cCampo == "TRT"
			nReturn := ADADOS_POS_TRT
		Case cCampo == "QUANTIDADE"
			nReturn := ADADOS_POS_QUANTIDADE
		Case cCampo == "PERIODO"
			nReturn := ADADOS_POS_PERIODO
		Case cCampo == "ADOC_PAI"
			nReturn := ADADOS_POS_ADOC_PAI
		Case cCampo == "ADOC_PAI_ORI"
			nReturn := ADADOS_POS_ADOC_PAI_ORI
		Case cCampo == "ADOC_PAI_DOC"
			nReturn := ADADOS_POS_ADOC_PAI_DOC
		Case cCampo == "ADOC_PAI_PRODUTO"
			nReturn := ADADOS_POS_ADOC_PAI_PRODUTO
		Case cCampo == "ADOC_PAI_QTD"
			nReturn := ADADOS_POS_ADOC_PAI_QTD
		Case cCampo == "ADOC_PAI_QTD_EMPE"
			nReturn := ADADOS_POS_ADOC_PAI_QTD_EMPE
		Case cCampo == "ADOC_PAI_QTD_SUBS"
			nReturn := ADADOS_POS_ADOC_PAI_QTD_SUBS
		Case cCampo == "AGLUTINA"
			nReturn := ADADOS_POS_AGLUTINA
		Case cCampo == "ROTEIRO"
			nReturn := ADADOS_POS_ROTEIRO
		Case cCampo == "OPERACAO"
			nReturn := ADADOS_POS_OPERACAO

	EndCase

Return nReturn


//********************************************************/
/*M�TODOS UTILIZADOS DURANTE O CALCULO DO MRP            */
//********************************************************/

/*/{Protheus.doc} preparaEInclui
Avalia necessidade de aglutina��o e inclui a rastreabilidade
@author    brunno.costa
@since     21/11/2019
@version   1
@param 01 - cLista, caracter, c�digo da lista identificadora da tabela utilizada:
                              1 = Carga de Demandas (PCPCargDem)
							  2 = Carga de Empenhos
							  3 = Carga de outras Sa�das Previstas
							  4 = Explos�o da Estrutura
@param 02 - cOrigem    , caracter, identificador de origem do registro
@param 03 - aDocPai    , array   , {Tipo do Documento Pai, Documento Pai}
@param 04 - cDocumento , caracter, identificador do documento - retorna por refer�ncia
@param 05 - cProduto   , caracter, c�digo do produto
@param 06 - cIDOpc     , caracter, c�digo do ID do Opcional
@param 07 - cTRT       , caracter, c�digo do TRT
@param 08 - nQuantidade, n�mero  , quantidade da necessidade
@param 09 - nPeriodo   , n�mero  , per�odo da necessidade
@param 10 - cRegra     , caracter,  regra de consumo dos alternativos:
								1- Valida Original; Valida Alternativo; Compra Original
								2- Valida Original; Valida Alternativo; Compra Alternativo
								3- Valida Alternativo; Compra Alternativo
@param 11 - lAglutina  , l�gico  , indica se deve considerar aglutina��o na chave do registro
@param 12 - lQuebra    , l�gico  , indica origem em quebra de Lote Econ�mico ou Qtd. Embalagem (nIndQuebra > 1)
@param 13 - cRoteiro   , caracter, c�digo do roteiro de opera��es do produto Pai
@param 14 - cOperacao  , caracter, c�digo da opera��o deste componente no roteiro do produto Pai
@return cList, caracter, identificador desta sess�o de vari�veis globais
@version   1
@return aDados, array, aDados,array com os dados para inclus�o da necessidade:
                    {1 - Origem,
					 2 - Documento,
					 3 - Produto,
					 4 - TRT,
					 5 - Quantidade,
					 6 - Per�odo,
					 7 - Array Doc Pai{Tipo Oridem Doc Pai, Documento, Quantidade},
					 8 - lAglutina relacionado}
/*/
METHOD preparaEInclui(cLista, cOrigem, aDocPai, cDocumento, cProduto, cIDOpc, cTRT, nQuantidade, nPeriodo, cRegra, lAglutina, lQuebra, cRoteiro, cOperacao) CLASS MrpDominio_Aglutina

	Local aDados    := ::prepara(cLista, cOrigem, aDocPai, @cDocumento, cProduto, cIDOpc, cTRT, nQuantidade, nPeriodo, cRegra, .F., lAglutina, lQuebra, cRoteiro, cOperacao)
	Local oRastreio := ::oDados:oDominio:oRastreio

	Default cIDOpc := ""

	oRastreio:incluiNecessidade(aDados[ADADOS_POS_ORIGEM]    ,;
								aDados[ADADOS_POS_DOCUMENTO] ,;
								aDados[ADADOS_POS_PRODUTO]   ,;
								cIDOpc                       ,;
								aDados[ADADOS_POS_TRT]       ,;
								nQuantidade                  ,;
								aDados[ADADOS_POS_PERIODO]   ,;
								                             ,;
								cRegra                       ,;
								                             ,;
								cRoteiro                     ,;
								cOperacao                     )

Return aDados


//********************************************************/
/*M�TODOS UTILIZADOS DURANTE A EXPORTA��O DO MRP         */
//********************************************************/

/*/{Protheus.doc} ajustesExportacao
Efetua ajustes de empenho e substitui��o nos registros da aglutina��o que ser�o efetivados na HWG
@author    brunno.costa
@since     10/12/2019
@version   1
@param 01 - aAglutinacao, array, array com os dados da aglutina��o que ser�o inseridos na HWG passados por refer�ncia
@param 02 - aNovasAglu  , array, array com os dados das novas aglutina��es retornado por refer�ncia - alternativos
/*/
METHOD ajustesExportacao(aAglutinacao, aNovasAglu)  CLASS MrpDominio_Aglutina

	Local aAlteDeman    //C�digos dos Alternativos da Substitui��o
	Local aRastreio
	Local cProduto
	Local nDocItem    := 0
	Local nDoctos     := Len(aAglutinacao[ADADOS_POS_ADOC_PAI])
	Local cDocumento  := ""
	Local cIDOpc      := ""
	Local cTRT        := ""
	Local nPeriodo    := 0
	Local nNecOriDoc  := 0
	Local nTotalEmpe  := 0
	Local nEmpenho   := 0
	Local nSubstitui := 0
	Local nTotSubAux := 0
	Local nTotalSubs := 0
	Local oJSConOrig := JsonObject():New()
	Local oJSConAlt  := JsonObject():New()

	Default aNovasAglu := {}

	cProduto   := aAglutinacao[ADADOS_POS_PRODUTO]
	nPeriodo   := aAglutinacao[ADADOS_POS_PERIODO]
	cDocumento := aAglutinacao[ADADOS_POS_DOCUMENTO]
	cTRT       := aAglutinacao[ADADOS_POS_TRT]
	cIDOpc     := aAglutinacao[ADADOS_POS_ID_OPCIONAL]

	aRastreio := ::retornaRastreio(cProduto+ Iif(!Empty(cIDOpc) , "|" + cIDOpc, ""), nPeriodo, cDocumento, cTRT)
	nTotSubAux := aRastreio[::nIndRSubst]
	nTotalEmpe := aRastreio[::nIndRNecOr] - nTotSubAux
	nTotalSubs := nTotSubAux
	For nDocItem := 1 to nDoctos
		nSubstitui := 0
		nEmpenho   := 0
		nNecOriDoc := aAglutinacao[ADADOS_POS_ADOC_PAI][nDocItem][ADADOS_POS_ADOC_PAI_QTD]

		If nNecOriDoc >= nTotSubAux
			nSubstitui := nTotSubAux
			nTotSubAux := 0
			nEmpenho   := nNecOriDoc - nSubstitui

		ElseIf nNecOriDoc < nTotSubAux
			If nTotSubAux > 0
				nSubstitui := nNecOriDoc
				nTotSubAux -= nSubstitui
			EndIf
			nEmpenho   := nNecOriDoc - nSubstitui

		EndIf

		aAdd(aAglutinacao[ADADOS_POS_ADOC_PAI][nDocItem], nEmpenho)
		aAdd(aAglutinacao[ADADOS_POS_ADOC_PAI][nDocItem], nSubstitui)

		If nSubstitui > 0
			aAlteDeman := ::retornaAlternativos(cProduto, nPeriodo)
			::geraRegistrosAlternativos(cProduto, nPeriodo, cDocumento, cTRT,;
									aAglutinacao[ADADOS_POS_ADOC_PAI][nDocItem][ADADOS_POS_ADOC_PAI_ORI]    ,;
									aAglutinacao[ADADOS_POS_ADOC_PAI][nDocItem][ADADOS_POS_ADOC_PAI_DOC]    ,;
									aAglutinacao[ADADOS_POS_ADOC_PAI][nDocItem][ADADOS_POS_ADOC_PAI_PRODUTO],;
									aAlteDeman, @aNovasAglu, nSubstitui, @oJSConOrig, @oJSConAlt)
			aSize(aAlteDeman, 0)
			aAlteDeman := Nil
		EndIf
	Next
	aSize(aRastreio, 0)
	aRastreio := Nil

	FreeObj(oJSConOrig)
	oJSConOrig := Nil

Return

/*/{Protheus.doc} geraRegistrosAlternativos
Gera registros referente alternativos
@author    brunno.costa
@since     10/12/2019
@version   1
@param 01 - cProduto  , caracter, c�digo do produto relacionado
@param 02 - nPeriodo  , n�mero  , per�odo relacionado
@param 03 - cDocumento, caracter, c�digo do documento relacionado
@param 04 - cTRT      , caracter, c�digo do TRT relacionado
@param 05 - cTpDocOri , caracter, tipo do documento pai relacionado
@param 06 - cDocOri   , caracter, documento pai relacionado
@param 07 - cProdOri  , caracter, c�digo do produto pai
@param 08 - aAlteDeman , array  , array com os c�digos dos alternativos
@param 09 - aNovasAglu, array   , array retornado por refer�ncia com as novas linhas de aglutina��o para exporta��o
/*/
METHOD geraRegistrosAlternativos(cProduto, nPeriodo, cDocumento, cTRT, cTpDocOri, cDocOri, cProdOri, aAlteDeman, aNovasAglu, nSubstitui, oJSConOrig, oJSConAlt)  CLASS MrpDominio_Aglutina

	Local aAlternati
	Local aDocPaiIt    := {}
	Local cAlternati
	Local cChvJSAlt
	Local cChvJSOrig
	Local cListOrig
	Local nAlternati := Len(aAlteDeman)
	Local nEmpAltern := 0
	Local nSubstAlte := 0
	Local nIndAlt    := 0
	Local nSldOrig   := 0
	Local nSldAlt    := 0

	For nIndAlt    := 1 to nAlternati
		If nSubstitui <= 0
			Exit
		EndIf

		aSize(aDocPaiIt, 0)
		cAlternati := aAlteDeman[nIndAlt]
		cListOrig  := cProduto + chr(13) + cValToChar(nPeriodo)
		aAlternati := ::retornaRastreio(cAlternati, nPeriodo, cDocumento, cTRT, cListOrig)

		If Empty(aAlternati)
			Loop
		EndIf

		cChvJSAlt  := cAlternati + chr(13) + cValToChar(nPeriodo) + chr(13) + cDocumento + chr(13) + cTRT
		cChvJSOrig := cListOrig  + chr(13) + cTpDocOri + chr(13) + cDocOri + chr(13) + cValToChar(nPeriodo) + chr(13) + cDocumento + chr(13) + cTRT

		//Avalia Saldo Substitui��o do Produto Original
		If oJSConOrig[cChvJSOrig] == Nil
			nSldOrig               := 0
			oJSConOrig[cChvJSOrig] := nSldOrig
		Else
			nSldOrig := oJSConOrig[cChvJSOrig]
		EndIf

		//Avalia Saldo Substitui��o do Produto Alternativo
		If oJSConAlt[cChvJSAlt] == Nil
			nSldAlt              := aAlternati[::nIndRSubst]
			oJSConAlt[cChvJSAlt] := nSldAlt
		Else
			nSldAlt := oJSConAlt[cChvJSAlt]
		EndIf

		If nSldAlt == 0
			Loop
		EndIf

		If Abs(nSldAlt) <= Abs(nSubstitui - nSldOrig)
			nSubstAlte := nSldAlt
		Else
			nSubstAlte := -(nSubstitui - nSldOrig)
		EndIf
		nSubstitui += nSubstAlte
		nEmpAltern := aAlternati[::nIndRNecOr] - nSubstAlte
		oJSConOrig[cChvJSOrig] += Abs(nSubstAlte)
		oJSConAlt[cChvJSAlt]   += Abs(nSubstAlte)

		aAdd(aNovasAglu, ::prepara("5",;
		                           "OP",;
								   {cTpDocOri, cDocOri, cProdOri},;
								   cDocumento,;
								   cAlternati,;
								   "",;
								   cTRT,;
		                           aAlternati[::nIndRNecOr],;
								   nPeriodo,;
								   aAlternati[::nIndRegra],;
								   .F.,;
								   .F.))

		aAdd(aTail(aTail(aNovasAglu)[ADADOS_POS_ADOC_PAI]), nEmpAltern)
		aAdd(aTail(aTail(aNovasAglu)[ADADOS_POS_ADOC_PAI]), nSubstAlte)

	Next
Return

/*/{Protheus.doc} retornaRastreio
Retorna registro aRastreio relacionado
@author    brunno.costa
@since     10/12/2019
@version   1
@param 01 - cProduto , caracter, c�digo do produto
@param 02 - nPeriodo , n�mero  , n�mero do per�odo
@param 03 - cDocPai  , caracter, c�digo do documento pai do registro
@param 04 - cTRT     , caracter, c�digo do TRT do produto na estrutura
@param 05 - cListOrig, caracter, chave do registro original do Rastreio, quando alternativo
@return aRastreio, array, array com os dados do rastreio deste registro
/*/
METHOD retornaRastreio(cProduto, nPeriodo, cDocPai, cTRT, cListOrig)  CLASS MrpDominio_Aglutina
Return ::oDados:oDominio:oRastreio:retornaRastreio(cProduto, nPeriodo, cDocPai, cTRT, cListOrig)

/*/{Protheus.doc} retornaAlternativos
Retorna registros referente alternativos aAlteDeman
@author    brunno.costa
@since     10/12/2019
@version   1
@param 01 - cProduto, caracter, c�digo do componente relacionado
@param 02 - nPeriodo , n�mero  , n�mero do per�odo
@Return aAlteDeman, array, array com os c�digos dos produtos alternativos consumidos
/*/
METHOD retornaAlternativos(cProduto, nPeriodo)  CLASS MrpDominio_Aglutina
Return ::oDados:oDominio:oRastreio:retornaAlternativos(cProduto, nPeriodo)



//********************************************************/
/*M�TODOS UTILIZADOS DURANTE O PROCESSAMENTO E CONCLUS�O */
//********************************************************/

/*/{Protheus.doc} proximoIDAuto
Identifica o pr�ximo ID Autom�tico
@author    brunno.costa
@since     21/11/2019
@version   1
@param 01 - cLista, caracter, c�digo da lista identificadora da tabela utilizada:
                              1 = Carga de Demandas (PCPCargDem)
							  2 = Carga de Empenhos
@return cIDAuto, pr�ximo ID Autom�tico
/*/
METHOD proximoIDAuto(cLista)  CLASS MrpDominio_Aglutina

	Local cIDAuto := ""

	::oAglutinacao:trava(cLista + chr(13) + "cIDAuto" + chr(13))

	cIDAuto := ::oAglutinacao:getFlag(cLista + chr(13) + "cIDAuto" + chr(13))
	cIDAuto := Soma1(cIDAuto)
	::oAglutinacao:setFlag(cLista + chr(13) + "cIDAuto" + chr(13), cIDAuto)

	::oAglutinacao:destrava(cLista + chr(13) + "cIDAuto" + chr(13))

Return cIDAuto
