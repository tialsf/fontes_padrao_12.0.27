#INCLUDE 'protheus.ch'
#INCLUDE 'MRPDominio.ch'

#DEFINE AREGRAS_POS_ID_RASTREIO 1
#DEFINE AREGRAS_POS_REGRA       2
#DEFINE AREGRAS_POS_SALDO_OP    3

/*/{Protheus.doc} MrpDominio_Alternativo
Regras de negocio MRP - Produtos alternativos
@author    brunno.costa
@since     25/04/2019
@version   1
/*/
CLASS MrpDominio_Alternativo FROM LongClassName

	DATA aPeriodos_Alternativos AS ARRAY
	DATA aBaixaPorOP            AS ARRAY
	DATA cOrdem                 AS STRING
	DATA cTipoFator             AS STRING
	DATA lConsome               AS LOGICAL
	DATA nFator                 AS INTEGER
	DATA oDados                 AS Object
	DATA oDominio               AS Object
	DATA oLeadTime              AS Object

	METHOD new() CONSTRUCTOR
	METHOD aplicaFator(nQuantidade, cTipoFator, nFator, lDesfaz)
	METHOD aplicaProdutoFator(cChaveProd, cAlternativo, nQuantidade, lInversa)
	METHOD consumirAlternativos(cProduto, nSaldo, nPeriodo, cProdOrig, lWait, cIDOpc, aBaixaPorOP, aMinMaxAlt)
	METHOD desfazSubstituicoes(cProduto, nSaldo, nPeriodo, cSomentAlt)
	METHOD desfazParcialSubstituicao(cProduto, nQuantidade, nPeriodo)
	METHOD existeSubstituicao(cProduto, cAlternati, nPeriodo)
	METHOD loopSubstituicaoAnterior(cProduto, cAlternati, nPeriodo, cPath)
	METHOD consumirProduto(nPeriodo, cProduto, nSobraAnt, lCalculado, lEncontrou, cProdOrig)
	METHOD consumirOpcionalProduto(nPeriodo, cProduto, nSobraAnt, lCalculado, lEncontrou, cProdOrig, cIDOpcOrig, lForcaAlt, lCompraAlt)
	METHOD percorreAlternativos(cProduto, nSaldo, nPeriodo, cProdOrig, lWait, cIDOpc, nSaiEstrut, aQtdRegras, lCompraAlt, aMinMaxAlt)
	METHOD registraMatrizes(cProduto, cAlternativo, nPeriodo, nConsOrig, nConsAlt)
	METHOD registraSubstituicao(cProduto, cAlternativo, nPeriodo, nConsOrig, nConsAlt)
	METHOD saldoOPNecessidade(cProduto, nPeriodo, cDctoPai, cRegra)
	METHOD validaVigencia(nPeriodo, dVigencia)

ENDCLASS

/*/{Protheus.doc} new
Metodo construtor
@author    brunno.costa
@since     25/04/2019
@version   1
@param 01 - oDominio, objeto, instancia da camada de dominio principal
/*/
METHOD new(oDominio) CLASS MrpDominio_Alternativo
	::oDominio               := oDominio
	::oDados                 := oDominio:oDados
	::oLeadTime              := MrpDominio_LeadTime():New(oDominio)
	::lConsome               := .T.
	::aPeriodos_Alternativos := {}

	//Corrige Apontamento
	::oDominio:oRastreio:oAlternativo := Self
Return Self

/*/{Protheus.doc} consumirAlternativos
Consome alternativos do produto
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto   , caracter, codigo do produto original para analise dos alternativos
@param 02 - nSaldo     , numero  , saldo do produto original para consumir alternativos
@param 03 - nPeriodo   , numero  , periodo para consumo
@param 04 - cProdOrig  , caracter, codigo do produto precessor ao original (caso este ja seja um alternativo)
@param 05 - lWait      , logico  , retorna por referencia indicando se existe na Matriz de Calculo, mas nao foi calculado. Interrompe o consumo.
@param 06 - cIDOpc     , caracter, Id do opcional relacionado
@param 07 - nSaiEstru  , n�mero  , quantidade sa�da estrutura do produto original
@param 08 - aBaixaPorOP, array   , array com os dados de rastreabilidade origem (Documentos Pais)
								   {{1 - Id Rastreabilidade,;
								     2 - Documento Pai,;
								     3 - Quantidade Necessidade,;
								     4 - Quantidade Estoque,;
								     5 - Quantidade Baixa Estoque,;
								     6 - Quantidade Substitui��o},...}
@param 09 - aMinMaxAlt  , array  , array com o nMin e nMax de sequ�ncia do alternativo
@return nSldPoster, numero, saldo posterior ao consumo dos alternativos
/*/
METHOD consumirAlternativos(cProduto, nSaldo, nPeriodo, cProdOrig, lWait, cIDOpc, nSaiEstrut, aBaixaPorOP, aMinMaxAlt) CLASS MrpDominio_Alternativo

	/*cRegra   , caracter, regra de consumo dos alternativos:
	1- Valida Original; Valida Alternativo; Compra Original
	2- Valida Original; Valida Alternativo; Compra Alternativo
	3- Valida Alternativo; Compra Alternativo*/

	Local cRegra       := "1"

	Local nIndex       := 0
	Local nTotal       := Len(aBaixaPorOP)
	Local nSldOP       := 0
	Local nSldOPs      := 0
	Local aQtdRegras   := {}
	Local nSldPoster   := nSaldo
	Local lCompraAlt   := .F.
	Local lRastreia    := ::oDominio:oParametros["lRastreia"]
	Local nSubstitui   := 0

	Default cProdOrig  := ""
	Default lWait      := .F.
	Default nSaiEstrut := 0

	If lRastreia
		If nSaiEstrut > 0
			For nIndex := 1 to nTotal
				//Identifica o saldo da necessidade desta OP e a Regra de considera��o dos alternativos
				cRegra       := "1"
				nSldOP  := - ::saldoOPNecessidade(cProduto, nPeriodo, aBaixaPorOP[nIndex][1], @cRegra)
				nSldOPs += nSldOP
				aAdd(aQtdRegras, {aBaixaPorOP[nIndex][1], cRegra, nSldOP})
				If cRegra $ "|2|3|"
					//::oDominio:oLogs:log("For�a consumo do alternativo, produto '" + AllTrim(cProduto) + "'", "33")
					lCompraAlt := .T.
				EndIf
			Next
		EndIf

	Else
		nSldOPs := nSaldo
	EndIf

	//Percorre os alternativos e consome o saldo conforme regra
	If nSldOPs < 0
		::lConsome := .F.
		nSldPoster := ::percorreAlternativos(cProduto, nSldOPs, nPeriodo, cProdOrig, @lWait, cIDOpc, @nSaiEstrut, aQtdRegras, lCompraAlt, aMinMaxAlt)
		If !lWait
			::lConsome := .T.
			aSize(::aPeriodos_Alternativos, 0)
			nSldPoster := ::percorreAlternativos(cProduto, nSldOPs, nPeriodo, cProdOrig, @lWait, cIDOpc, @nSaiEstrut, aQtdRegras, lCompraAlt, aMinMaxAlt)
		EndIf
		nSubstitui := nSldPoster - nSldOPs
		nSaldo     += nSubstitui
	EndIf

Return nSaldo

/*/{Protheus.doc} percorreAlternativos
Consome alternativos do produto
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto , caracter, codigo do produto original para analise dos alternativos
@param 02 - nSldOrig , numero  , saldo do produto original para consumir alternativos
@param 03 - nPeriodo , numero  , periodo para consumo
@param 04 - cProdOrig, caracter, codigo do produto precessor ao original (caso este ja seja um alternativo)
@param 05 - lWait    , logico  , retorna por referencia indicando se existe na Matriz de Calculo, mas nao foi calculado. Interrompe o consumo.
@param 06 - cIDOpc   , caracter, Id do opcional relacionado
@param 07 - nSaiEstrut, n�mero , quantidade de sa�da estrutura deste produto
@param 08 - aQtdRegras, array  , quantidade correspondente a cada uma das regras de OP em ordem de prioridade
								{cID, cRegra, nQuantidade}
@param 09 - lCompraAlt, l�gico , indica se existe alguma OP com regra 2 ou 3: compra alternativo
@param 10 - aMinMaxAlt  , array  , array com o nMin e nMax de sequ�ncia do alternativo
@return nSldPoster, numero, saldo posterior ao consumo dos alternativos
/*/
METHOD percorreAlternativos(cProduto, nSldOrig, nPeriodo, cProdOrig, lWait, cIDOpc, nSaiEstrut, aQtdRegras, lCompraAlt, aMinMaxAlt) CLASS MrpDominio_Alternativo

	Local aCampos      := {"ALT_ALTERN", "ALT_DATA", "ALT_TPFAT", "ALT_FATOR", "ALT_ORDEM"}
	Local aCposAlt     := {}
	Local cAlternati
	Local cMenorAlt    := ""
	Local cIndAlt      := aMinMaxAlt[1]
	Local cMaxAlt      := aMinMaxAlt[2]
	Local lCalculado   := .T.
	Local lEncontrou   := .F.
	Local lErrorALT    := .F.
	Local lRastreia    := ::oDominio:oParametros["lRastreia"]
	Local nIndex       := 0
	Local nSldInic     := nSldOrig
	Local nSldPoster   := nSldOrig
	Local nSldInic2    := 0
	Local nSldPoste2   := 0
	Local nSaiEstOri   := 0
	Local nQtdNecAlt   := 0
	Local nTotal       := 0
	Local nSldCnvAlt   := 0
	Local nSldAltPos   := 0
	Local nSubstitui   := 0
	Local nTotSubst    := 0
	Local nBkpSubsti   := 0

	Default cProdOrig  := ""
	Default lWait      := .F.
	Default nSaiEstrut := 0
	Default lCompraAlt := .F.

	//Ajusta saldo para quantidade m�xima da sa�da estrutura
	nSaiEstOri := nSaiEstrut

	//Avalia Alternativos - Somente para quantidade Sa�da Estrutura
	While nSaiEstrut > 0 .And. nSldPoster < 0

		aSize(aCposAlt, 0)
		If cIndAlt > cMaxAlt
			Exit
		EndIf

		cChvAltern := cProduto + cIndAlt
		aCposAlt     := ::oDados:retornaCampo("ALT", 1, cChvAltern, aCampos, @lErrorALT,,,,,,.T.)

		If lErrorALT
			aCposAlt  := {}
			lErrorALT := .F.
			cIndAlt := Soma1(cIndAlt)
			Loop
		EndIf

		//Valida vig�ncia do alternativo
		If !::validaVigencia(nPeriodo, aCposAlt[2])
			cIndAlt := Soma1(cIndAlt)
			Loop
		EndIf

		cAlternati   := aCposAlt[1]
		::cTipoFator := aCposAlt[3]
		::nFator     := aCposAlt[4]
		::cOrdem     := aCposAlt[5]

		//Nao avalia alternativo correspondente a produto original (protecao loop)
		If !Empty(cProdOrig) .And. AllTrim(cProdOrig) == AllTrim(cAlternati)
			cIndAlt := Soma1(cIndAlt)
			Loop
		EndIf

		//N�o avalia alternativo relacionado a substitui��o REVERSA
		If ::existeSubstituicao(cAlternati, cProduto, nPeriodo)
			cIndAlt := Soma1(cIndAlt)
			Loop
		EndIf

		//N�o avalia alternativo relacionado a substitui��o ANTERIOR
		If ::loopSubstituicaoAnterior(cProduto, cAlternati, nPeriodo)
			cIndAlt := Soma1(cIndAlt)
			Loop
		EndIf

		//Identifica primeira sequencia
		cMenorAlt := Iif(cMenorAlt == "", cIndAlt, cMenorAlt)

		//Identifica Saldo Convertido
		nSldCnvAlt  := Iif(nSldOrig == 0, 0, ::aplicaFator(nSldOrig, ::cTipoFator, ::nFator, .F.))

		//Consome saldo
		If Empty(cIDOpc)
			nSldAltPos := ::consumirProduto(nPeriodo, cAlternati, nSldCnvAlt, @lCalculado, @lEncontrou, cProduto, , .F., lCompraAlt)
		Else
			nSldAltPos := ::consumirOpcionalProduto(nPeriodo, cAlternati, nSldCnvAlt, @lCalculado, @lEncontrou, cProduto, cIDOpc,  .F., lCompraAlt)
		EndIf
		nSldPoster := Iif(nSldAltPos == 0, 0, ::aplicaFator(nSldAltPos, ::cTipoFator, ::nFator, .T.))
		nSubstitui := nSldPoster - nSldInic
		nSldInic   := nSldPoster
		nBkpSubsti := nSubstitui
		nSldOrig   += nSubstitui
		nTotSubst  += nSubstitui

		//Existe na MAT, mas nao foi calculado, interrompe
		If lEncontrou .And. !lCalculado
			//"Consumo alternativos interrompido: encontrou alternativo '" + "' do produto '" + "' na Matriz, mas este nao foi calculado."
			::oDominio:oLogs:log(STR0074 + AllTrim(cAlternati) + STR0075 + AllTrim(cProduto) + STR0076, "5")
			lWait := .T.
			Exit
		Else
			lWait := .F.
		EndIf

		cIndAlt := Soma1(cIndAlt)
	EndDo

	//Checar se h� necessidades diferente do tipo 1, se sim, gerar substitui��o para primeiro alternativo
	//Gera Necessidade do Alternativo - Regra "2" ou "3"
	If !lWait .AND. cMenorAlt != ""
		nTotal   := Len(aQtdRegras)

		//Identifica a quantidade a gerar necessidade para o Alternativo
		If lRastreia
			For nIndex := 1 to nTotal
				If nTotSubst > 0
					If nTotSubst >= (-aQtdRegras[nIndex][AREGRAS_POS_SALDO_OP])
						nTotSubst += aQtdRegras[nIndex][AREGRAS_POS_SALDO_OP]
						aQtdRegras[nIndex][AREGRAS_POS_SALDO_OP] := 0
					Else
						aQtdRegras[nIndex][AREGRAS_POS_SALDO_OP] += nTotSubst
						nTotSubst := 0
					EndIf
				EndIf
				If aQtdRegras[nIndex][AREGRAS_POS_REGRA] $ "|2|3|"
					nQtdNecAlt += aQtdRegras[nIndex][AREGRAS_POS_SALDO_OP]
				EndIf
			Next
		EndIf

		//For�a Gera��o da Necessidade do Alternativo
		If nQtdNecAlt < 0
			lErrorALT  := .F.
			cChvAltern := cProduto + cMenorAlt
			aCposAlt     := ::oDados:retornaCampo("ALT", 1, cChvAltern, aCampos, @lErrorALT,,,,,,.T.)
			cAlternati   := aCposAlt[1]
			::cTipoFator := aCposAlt[3]
			::nFator     := aCposAlt[4]
			::cOrdem     := aCposAlt[5]

			nSldInic2  := nQtdNecAlt

			//Identifica Saldo Convertido
			nSldCnvAlt  := Iif(nQtdNecAlt == 0, 0, ::aplicaFator(nQtdNecAlt, ::cTipoFator, ::nFator, .F.))

			If Empty(cIDOpc)
				nSldAltPos := ::consumirProduto(nPeriodo, cAlternati, nSldCnvAlt, @lCalculado, @lEncontrou, cProduto, .T., lCompraAlt)
			Else
				nSldAltPos := ::consumirOpcionalProduto(nPeriodo, cAlternati, nSldCnvAlt, @lCalculado, @lEncontrou, cProduto, cIDOpc, .T., lCompraAlt)
			EndIf

			nSldPoste2     := Iif(nSldAltPos == 0, 0, ::aplicaFator(nSldAltPos, ::cTipoFator, ::nFator, .T.))
			nSubstitui     := nSldPoste2 - nSldInic2
			nSldOrig       += nSubstitui
		EndIf
	EndIf

Return nSldOrig

/*/{Protheus.doc} consumirProduto
Consome periodo e retorna o saldo posterior
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - nPeriodo  , numero  , periodo referencia para consumo
@param 02 - cProduto  , caracter, codigo do produto a ser consumido (alternativo) (ou codigo + ID Opcional)
@param 03 - nSobraAnt , numero  , saldo da necessidade antes de consumir este produto (alternativo)
@param 04 - lCalculado, logico  , retorna por referencia se o produto foi calculado no periodo
@param 05 - lEncontrou, logico  , retorna por referencia se encontrou o produto na matriz de calculo
@param 06 - cProdOrig , caracter, codigo do produto original
@param 07 - lForcaAlt , l�gico  , for�a o consumo do alternativo na quantidade total
@param 08 - lCompraAlt, l�gico  , indica se existe alguma OP com regra 2 ou 3: compra alternativo
@return nSobraPos, numero, retorna o saldo apos consumir o produto
/*/
METHOD consumirProduto(nPeriodo, cProduto, nSobraAnt, lCalculado, lEncontrou, cProdOrig, lForcaAlt, lCompraAlt) CLASS MrpDominio_Alternativo

	Local aAreaMAT
	Local aRetAux
	Local cChaveMAT
	Local lErrorAlt       := .F.
	Local lInterrompe     := .F.
	Local lReservaMatriz  := .F.
	Local lReservaProduto := .F.
	Local lRastreia       := ::oDominio:oParametros["lRastreia"]
	Local nPerAux
	Local nSobra          := 0
	Local nSobraPos       := nSobraAnt
	Local nConsOrig
	Local nConsAlt
	Local nSldAltern      := 0
	Local nThreads        := ::oDominio:oParametros["nThreads"]
	Local oLiveLock       := ::oDados:oLiveLock

	Default lForcaAlt     := .F.
	Default lCompraAlt    := .F.

	aAreaMAT    := ::oDados:retornaArea("MAT")

	//Busca para tras
	For nPerAux := nPeriodo to 1 step -1
		cChaveMAT  := DtoS(::oDominio:aPeriodos[nPerAux]) + cProduto
		lErrorAlt  := .F.
		::oDados:retornaCampo("MAT" , 1, cChaveMAT, "MAT_SALDO", @lErrorAlt)
		nSldAltern := ::oDominio:saldoInicial(cProduto, nPerAux)

		//Encontrou na Matriz de Calculo ou Possui Saldo
		If !lErrorAlt
			lEncontrou := .T.

			If lEncontrou .and. !(lCalculado := ::oDados:foiCalculado(cProduto, nPerAux)) .AND. !lForcaAlt
				::oDominio:oLogs:log(STR0053 + AllTrim(cProdOrig) + STR0054 + " -> " + AllTrim(cProduto) + STR0018 + cValToChar(nPerAux), "11") //"Alternativo ainda nao calculado, interrompe: " + "(Original)" + "(Alternativo)" + " - Periodo: "
				If nThreads > 1
					oLiveLock:setResult(cProduto , 1, .F., .T., .T.)
					oLiveLock:setResult(cProdOrig, 1, .F., .T., .T.)
				EndIf
				Exit
			EndIf

			//Existe na Matriz calculado
			If lCalculado .OR. lForcaAlt
				lReservaMatriz := .F.
				nSobra         := 0
				If lReservaProduto := ::oDados:reservaProduto(cProduto)
					//Verifica o saldo no final do processo
					aRetAux    := ::oDados:retornaCampo("MAT", 1, cChaveMAT, {"MAT_NECESS","MAT_SALDO"}, @lErrorAlt, , , , , , .T. /*lVarios*/)
					nNecAltern := aRetAux[1]
					nSldAltern := aRetAux[2]
					nSldAltern += nNecAltern

					If lRastreia
						nSldAltern -= ::oDominio:oRastreio:necPPed_ES(cProduto, nPerAux)
					EndIf

					If nSldAltern > 0 .OR. lForcaAlt

						If ::lConsome
							//Registra o consumo do alternativo nas matrizes (Origem e Destino)
							::oDominio:oLogs:log(STR0056 + AllTrim(cProduto) + STR0055 + "  - Per�odo: " + cValToChar(nPerAux), "14") //"Reservou o produto alternativo " + "(Alternativo)"
							::oDominio:oLogs:log(STR0056 + AllTrim(cProduto) + STR0055 + "  - Per�odo: " + cValToChar(nPerAux) + ", lForcaAlt = " + Iif(lForcaAlt, "true", "false"), "14") //"Reservou o produto alternativo " + "(Alternativo)"
							::oDominio:oLogs:log(STR0056 + AllTrim(cProduto) + STR0055 + "  - Per�odo: " + cValToChar(nPerAux) + ", nSobraPos = " + cValToChar(nSobraPos), "14") //"Reservou o produto alternativo " + "(Alternativo)"
							::oDominio:oLogs:log(STR0056 + AllTrim(cProduto) + STR0055 + "  - Per�odo: " + cValToChar(nPerAux) + ", nSldAltern = " + cValToChar(nSldAltern), "14") //"Reservou o produto alternativo " + "(Alternativo)"

							If lForcaAlt
								nConsAlt := -nSobraPos
								nSobra   := 0
							Else
								nSobra   := nSobraPos + nSldAltern
								nSobra   := If(nSobra > 0, 0, nSobra)
								nConsAlt := -(nSobraPos - nSobra)
							EndIf

							::oDominio:oLogs:log(STR0056 + AllTrim(cProduto) + STR0055 + "  - Per�odo: " + cValToChar(nPerAux) + ", nSobra = " + cValToChar(nSobra), "14") //"Reservou o produto alternativo " + "(Alternativo)"
							::oDominio:oLogs:log(STR0056 + AllTrim(cProduto) + STR0055 + "  - Per�odo: " + cValToChar(nPerAux) + ", nConsAlt = " + cValToChar(nConsAlt), "14") //"Reservou o produto alternativo " + "(Alternativo)"

							//Identifica quantidade correspondente no produto original
							nConsOrig := Iif(nConsAlt == 0, 0, ::aplicaFator(nConsAlt, ::cTipoFator, ::nFator, .T.))

							::registraSubstituicao(cProdOrig, cProduto, nPeriodo, @nConsOrig, @nConsAlt, @nSobra)

							//Registra substituicao e registros nas matrizes (Origem e Destino)
							::registraMatrizes(cProdOrig, cProduto, nPeriodo, nConsOrig, nConsAlt)
							::oDados:setaArea(aAreaMAT)

							//Registra a substituicao

							nSobraPos   := nSobra
							nSobra      := 0
						EndIf

						//Interrompe While: apos consumir o saldo do alternativo
						::oDominio:oLogs:log(STR0060 + AllTrim(cProdOrig) + STR0054 + " -> " + AllTrim(cProduto) + STR0055 + STR0018 + cValToChar(nPeriodo), "15") //"Interrompe While: apos consumir o saldo do alternativo " + "(Original)" + "(Alternativo)" + " - Periodo: "

					Else
						::oDominio:oLogs:log(STR0063 + AllTrim(cProdOrig) + STR0054 + " -> " + AllTrim(cProduto) + STR0055 + STR0018 + cValToChar(nPeriodo), "17") //"Interrompe While: Nao ha saldo do alternativo: " + " - Periodo: "

					EndIf
					::oDados:liberaProduto(cProduto)
					::oDominio:oLogs:log(STR0059 + AllTrim(cProduto) + STR0055 + STR0018 + cValToChar(nPeriodo), "14") //"Liberou produto alternativo " + "(Alternativo)" + " - Periodo: "
					lInterrompe := .T.
				Else

					If nThreads > 1
						oLiveLock:setResult(cProduto , 1, .F., .T., .T.)
						oLiveLock:setResult(cProdOrig, 1, .F., .T., .T.)
					EndIf

					lCalculado  := .F.
					lInterrompe := .T.
					::oDominio:oLogs:log(STR0061 + AllTrim(cProdOrig) + STR0054 + " -> " + AllTrim(cProduto) + STR0055 + STR0018 + cValToChar(nPeriodo), "16") //"Interrompe While: ao tentar consumir saldo de alternativo bloqueado " + " - Periodo: "
					Exit
				EndIf

				Exit
			EndIf

		//N�o encontrou alternativo na matriz de calculo
		Else
			If nSldAltern > 0 .OR. lCompraAlt
				If !::oDados:existeMatriz(cProduto, nPerAux) .And. ::oDados:oMatriz:trava(cChaveMAT)

					//Decrementa totalizador relacionado ao oDominio:loopNiveis
					If !::oDados:existeMatriz(cProduto) .or. !::oDados:possuiPendencia(cProduto, .T.)
						::oDados:decrementaTotalizador(cProduto)
					EndIf

					//"Inclui novo registro na Matriz: " + " - Periodo: " + " - nNecessidade: "
					::oDados:oLogs:log(STR0034 + AllTrim(cProduto) + STR0018 + cValToChar(nPerAux) + STR0035 + cValToChar(0), "6")
					::oDados:atualizaMatriz(::oDominio:aPeriodos[nPerAux], cProduto, Nil, {"MAT_SLDINI", "MAT_SALDO"}, {nSldAltern, nSldAltern})
					::oDados:gravaPeriodosProd(cProduto, nPerAux)
					::oDados:oMatriz:destrava(cChaveMAT)
					nPerAux++
					Loop

				ElseIf ::oDados:existeMatriz(cProduto, nPerAux)
				    nPerAux++
					Loop

				EndIf
			Else
				::oDominio:oLogs:log(STR0064 + AllTrim(cProdOrig) + STR0054 + " -> " + AllTrim(cProduto) + STR0055 + STR0018 + cValToChar(nPerAux), "18") //"Nao encontrou alternativo na matriz de calculo no periodo: " + "(Original)" + "(Alternativo)" + " - Periodo: "
				Loop
			EndIf

		EndIf
		If lInterrompe
			Exit
		EndIf
	Next nPerAux

	::oDados:setaArea(aAreaMAT)

Return nSobraPos

/*/{Protheus.doc} registraMatrizes
Registra substituicao e alteracoes de entradas e saidas previstas nas matrizes
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto    , caracter, codigo do produto original (substituido)
@param 02 - cAlternativo, caracter, codigo do produto alternativo (substituto)
@param 03 - nPeriodo    , numero  , periodo
@param 04 - nConsOrig   , numero  , quantidade substitu�da do produto original
@param 05 - nConsAlt    , numero  , quantidade consumida do produto alternativo
@return Nil
/*/
METHOD registraMatrizes(cProduto, cAlternativo, nPeriodo, nConsOrig, nConsAlt) CLASS MrpDominio_Alternativo

	Local aAreaMAT   := ::oDados:retornaArea("MAT")
	Local cChaveOri  := DtoS(::oDominio:aPeriodos[nPeriodo]) + cProduto
	Local cChvAltern := DtoS(::oDominio:aPeriodos[nPeriodo]) + cAlternativo

	//Atualiza Saida Prevista
	::oDominio:oLogs:log(STR0018 + cValToChar(nPeriodo) + STR0028 + AllTrim(cAlternativo) + STR0057 + " (+)" + cValToChar(nConsAlt), "12") //" - Periodo: " + " - Produto: " + " - Saida Prevista (MAT_SAIPRE): "
	::oDados:gravaCampo("MAT", 1, cChvAltern, "MAT_SAIPRE", nConsAlt , .F., .T.)	//.T. = Acrescenta
	::oDados:gravaCampo("MAT", 1, cChvAltern, "MAT_SALDO" , -nConsAlt, .F., .T.)	//.T. = Acrescenta

	//Decrementa totalizador relacionado ao oDominio:loopNiveis
	If !::oDados:possuiPendencia(cProduto, .T.)
		::oDados:decrementaTotalizador(cProduto)
	EndIf

	//Ajusta periodo inicial produto alternativo consumido
	::oDominio:oLogs:log(STR0028 + cAlternativo + STR0055 + STR0040 + cValToChar(nPeriodo), "13") //" - Produto: " + "(Alternativo)" + " - Periodo Minimo"

	//Atualiza Entrada Prevista - Produto Original
	::oDominio:oLogs:log(STR0018 + cValToChar(nPeriodo) + STR0028 + AllTrim(cProduto) + STR0058 + " (+)" + cValToChar(nConsOrig), "12") //" - Periodo: " + " - Produto: " + " - Entrada Prevista (MAT_ENTPRE): "
	::oDados:gravaCampo("MAT", 1, cChaveOri, "MAT_ENTPRE", nConsOrig, .F., .T.)   //.T. = Acrescenta

	//Adiciona produto e per�odo no controle de grava��o do campo "Per�odo DE" na l�gica de c�lculo
	aAdd(::aPeriodos_Alternativos, {cAlternativo, nPeriodo, nConsOrig})

	::oDados:setaArea(aAreaMAT)

Return

/*/{Protheus.doc} consumirOpcionalProduto
Consome periodo e retorna o saldo posterior - Produto com opcional
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - nPeriodo  , numero  , periodo referencia para consumo
@param 02 - cProduto  , caracter, codigo do produto a ser consumido (alternativo)
@param 03 - nSobraAnt , numero  , saldo da necessidade antes de consumir este produto (alternativo)
@param 04 - lCalculado, logico  , retorna por referencia se o produto foi calculado no periodo
@param 05 - lEncontrou, logico  , retorna por referencia se encontrou o produto na matriz de calculo
@param 06 - cProdOrig , caracter, codigo do produto original
@param 07 - cIDOpcOrig, caracter, ID do opcional original
@param 08 - lForcaAlt , l�gico  , for�a o consumo do alternativo na quantidade total
@param 09 - lCompraAlt, l�gico  , indica se existe alguma regra com regra 2 ou 3: compra alternativo
@return nSobraPos, numero, retorna o saldo apos consumir o produto
/*/
METHOD consumirOpcionalProduto(nPeriodo, cProduto, nSobraAnt, lCalculado, lEncontrou, cProdOrig, cIDOpcOrig, lForcaAlt, lCompraAlt) CLASS MrpDominio_Alternativo

	Local aAreaMAT
	Local aAreaPRD  := ::oDados:retornaArea("PRD")
	Local aOpcional
	Local aOpcOrig
	Local aRetAux
	Local cIDOpc    := ""
	Local cOpcional := ""
	Local cOpcOrig  := ""
	Local lErrorPRD := .F.
	Local nSobraPos := nSobraAnt

	Default lForcaAlt := .F.

	aAreaMAT    := ::oDados:retornaArea("MAT")

	//Consome saldo de alternativo sem estrutura
	If !::oDados:possuiEstrutura(cProduto)
		nSobraPos := ::consumirProduto(nPeriodo, cProduto, nSobraAnt, @lCalculado, @lEncontrou, cProdOrig, lForcaAlt, lCompraAlt)

	Else
		//Verifica existencia de alternativo com este opcional na tabela de Produtos
		aRetAux  := ::oDados:retornaCampo("PRD", 1, cProduto, {"PRD_COD", "PRD_IDOPC"}, @lErrorPRD, , , , , , .T. /*lVarios*/)
		While !lErrorPRD .and. nSobraPos < 0 .and. (cProdAux == cProduto .OR. Empty(cProdAux))
			cProdAux := aRetAux[1]
			cIDOpc   := aRetAux[2]

			If !Empty(cIDOpc)
				cOpcional := ::oDados:retornaCampo("OPC", 2, cIDOpc    , "OPC_OPCION")
				aOpcional := StrTokArr(cOpcional, "|")
				aOpcional := aSort(aOpcional)
				cOpcional := AllTrim(Upper(Array2Str(aOpcional, .F.)))

				cOpcOrig  := Iif(Empty(cOpcOrig), ::oDados:retornaCampo("OPC", 2, cIDOpcOrig, "OPC_OPCION"), cOpcOrig)
				aOpcOrig  := Iif(Empty(aOpcOrig), StrTokArr(cOpcOrig, "|"), aOpcOrig)
				aOpcOrig  := aSort(aOpcOrig)
				cOpcOrig  := AllTrim(Upper(Array2Str(aOpcOrig, .F.)))

				//Consome alternativo com mesmo opcional selecionado
				If cOpcional == cOpcOrig
					cProduto  := cProduto + "|" + cIDOpc
					nSobraPos := ::consumirProduto(nPeriodo, cProduto, nSobraAnt, @lCalculado, @lEncontrou, cProdOrig, lForcaAlt)
				EndIf
			EndIf

			aRetAux  := ::oDados:retornaCampo("PRD", 1, Nil, {"PRD_COD", "PRD_IDOPC"}, @lErrorPRD, , , .T./*lProximo*/, , , .T. /*lVarios*/)
		EndDo

	EndIf

	::oDados:setaArea(aAreaMAT)
	::oDados:setaArea(aAreaPRD)

Return nSobraPos

/*/{Protheus.doc} registraSubstituicao
Registra a substituicao do produto pelo alternativo
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto    , caracter, codigo do produto avaliado (alternativo de outro produto)
@param 02 - nPerOrig    , numero  , indicador do periodo atual
@param 03 - cAlternativo, caracter, codigo do produto avaliado (alternativo de outro produto)
@param 04 - nPerDest    , numero  , indicador do periodo atual
@param 05 - nConsOrig   , numero  , quantidade substitu�da do produto original
@param 06 - nConsAlt    , numero  , quantidade consumida do produto alternativo
/*/
METHOD registraSubstituicao(cProduto, cAlternativo, nPeriodo, nConsOrig, nConsAlt, nSobra) CLASS MrpDominio_Alternativo

	Local aRegistro
	Local aAlteDeman    := {} //Alternativos da Demanda
	Local cOrigDeman          //Origem da demanda - Produto Original + Per�odo
	Local cKeyOri       := ""
	Local cChave
	Local nPos
	Local lError        := .F.

	cOrigDeman := "KEY_" + AllTrim(cProduto) + "_" + cValToChar(nPeriodo)//Original + Periodo
	cChave     := cOrigDeman + "_" + AllTrim(cAlternativo)               //Original + Periodo + Alternativo

	//Atualiza controle de destinos de substituicao do produto origem
	aAlteDeman := ::oDados:oAlternativos:getItemAList("substituicoes_produtos", cOrigDeman)
	If aAlteDeman == Nil
		aAlteDeman := {cAlternativo}
		::oDados:oAlternativos:setItemAList("substituicoes_produtos", cOrigDeman, aAlteDeman, .F., .T., .F.)
	Else
		nPos := aScan(aAlteDeman, {|x| x == cAlternativo})
		If nPos == 0
			aAdd(aAlteDeman, cAlternativo)
		EndIf
		::oDados:oAlternativos:setItemAList("substituicoes_produtos", cOrigDeman, aAlteDeman, .F., .T., .F.)
	EndIf

	//Registra dados da substituicao
	aRegistro := ::oDados:oAlternativos:getItemAList("substituicoes_dados", cChave, @lError)
	If lError
		aRegistro := {cProduto, cAlternativo, nPeriodo, nConsAlt, nConsOrig, ::cOrdem, -1, -1}
	Else
		//Limita Substitui��o na quantidade dispon�vel ap�s aplica��o das pol�ticas de estoque
		IF nConsOrig > 0 .and. aRegistro[7] >= 0 .AND. (aRegistro[4]+nConsAlt) - aRegistro[7] > 0
			nSobra       -= (aRegistro[4]+nConsAlt) - aRegistro[7]
			nConsAlt     := aRegistro[7]
			nConsOrig    := aRegistro[8]

			//Atualiza Valor da Substitui��o
			aRegistro[4] := nConsAlt
			aRegistro[5] := nConsOrig

			//Reseta Limites
			aRegistro[7] := -1
			aRegistro[8] := -1
		Else
			aRegistro[4] += nConsAlt
			aRegistro[5] += nConsOrig

		EndIf
	EndIf
	::oDados:oAlternativos:setItemAList("substituicoes_dados", cChave, aRegistro, .F., .T., .F.)

	//Atualiza Total Substitui��o
	cKeyOri      := "KEY_ORI_" + AllTrim(cProduto) + "_" + cValToChar(nPeriodo)//Original + Periodo
	::oDados:oAlternativos:setflag(cKeyOri, nConsOrig, .F., .T., .T.)//Soma

	::oDominio:oLogs:log(STR0109 + AllTrim(cProduto) + STR0110 + AllTrim(cAlternativo) + STR0111 + cValToChar(nPeriodo)+"': + " + cValToChar(nConsAlt) + "(Total "+cValToChar(aRegistro[4])+").", "33") //"Registra substituicao do produto '" + "' pelo produto '" + "' no periodo '"

Return

/*/{Protheus.doc} desfazSubstituicoes
Desfaz substitucoes anteriores deste produto alternativo quando saldo atual gera necessidade
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto, caracter, codigo do produto avaliado (alternativo de outro produto)
@param 02 - nSaldo    , numero  , saldo atual do produto no periodo
@param 03 - nPeriodo  , numero  , indicador do periodo atual
@param 04 - cSomentAlt, caracter, C�digo do produto alternativo para desfazer as substitui��es, quando em branco, desfaz todas.
@return nSobraPos, numero, retorna o saldo apos consumir o produto
/*/
METHOD desfazSubstituicoes(cProduto, nSaldo, nPeriodo, cSomentAlt, aDesfezAlt) CLASS MrpDominio_Alternativo

	Local aAlteDeman //Alternativos da Demanda
	Local aRegistro
	Local aSubstit     := {}
	Local cAlternativo
	Local cChave
	Local cOrigDeman      := "KEY_" + AllTrim(cProduto) + "_" + cValToChar(nPeriodo) //Original + Periodo
	Local cKeyOri
	Local lRastreia    := ::oDominio:oParametros["lRastreia"]
	Local nInd
	Local nConsAlt
	Local nConsOrig
	Local nPos

	Default cSomentAlt := ""
	Default aDesfezAlt := {}

	//Prote��o de execu��o duplicada via oRastreio:dezfazExplosoes ou PCOCalcNec- Lock
	If lRastreia
		::oDados:oAlternativos:trava(cOrigDeman)
	EndIf

	//Atualiza controle de destinos de substituicao do produto origem
	aAlteDeman := ::oDados:oAlternativos:getItemAList("substituicoes_produtos", cOrigDeman)

	//Possui substituicoes
	If aAlteDeman != Nil .AND. !Empty(aAlteDeman)
		//Recupera registros das substituicoes
		For nInd := 1 to Len(aAlteDeman)
			cAlternativo := aAlteDeman[nInd]
			cChave       := cOrigDeman + "_" + AllTrim(cAlternativo) //Original + Periodo + Alternativo
			aRegistro    := ::oDados:oAlternativos:getItemAList("substituicoes_dados", cChave)
			If aRegistro != Nil
				aAdd(aSubstit, {cChave, aClone(aRegistro)})
			EndIf
		Next

		//Ordena da maior a menor
		aSubstit := aSort(aSubstit, , , {|x,y| x[2][4] > y[2][4] })//TODO, REVISAR NECESSIDADE
		For nInd := 1 to Len(aSubstit)
			cChave       := aSubstit[nInd][1]
			aRegistro    := aSubstit[nInd][2]
			cAlternativo := aRegistro[2]
			nConsAlt     := aRegistro[4]
			nConsOrig    := aRegistro[5]

			If nConsOrig != 0 .AND. nConsAlt != 0 .AND. (Empty(cSomentAlt) .or. AllTrim(cAlternativo) == AllTrim(cSomentAlt))
				nSaldo       := nSaldo - nConsAlt

				::oDominio:oLogs:log(STR0112 + AllTrim(aRegistro[1]) + STR0110 + AllTrim(aRegistro[2]) + STR0111 + cValToChar(nPeriodo)+"': "+cValToChar(nConsOrig)+" <- "+cValToChar(nConsAlt)+".", "34") //"Desfaz a substituicao do produto '" + "' pelo produto '" + "' no periodo '"

				aAdd(aDesfezAlt, aClone(aRegistro))
				aRegistro[4] := 0
				aRegistro[5] := 0
				::oDados:oAlternativos:setItemAList("substituicoes_dados", cChave, aRegistro, .F., .T., .F.)

				//Atualiza Matrizes
				::registraMatrizes(aRegistro[1], aRegistro[2], aRegistro[3], -nConsOrig, -nConsAlt)

				//Atualiza Total Substitui��o
				cKeyOri      := "KEY_ORI_" + AllTrim(cProduto) + "_" + cValToChar(nPeriodo)//Original + Periodo
				::oDados:oAlternativos:setflag(cKeyOri, -nConsOrig, .F., .T., .T.)//Soma
			Endif

		Next

		//Limpa conteudo global
		nPos := aScan(aAlteDeman, {|x| x == cAlternativo})
		If nPos > 0
			aDel(aAlteDeman, nPos)
			aSize(aAlteDeman, (Len(aAlteDeman) - 1))
			::oDados:oAlternativos:setItemAList("substituicoes_produtos", cOrigDeman, aAlteDeman, .F., .T., .F.)
		EndIf

	EndIf

	//Prote��o de execu��o duplicada via oRastreio:dezfazExplosoes ou PCOCalcNec- UnLock
	If lRastreia
		::oDados:oAlternativos:destrava(cOrigDeman)
	EndIf

Return nSaldo

/*/{Protheus.doc} desfazParcialSubstituicao
Desfaz substitucoes anteriores deste produto alternativo quando saldo atual gera necessidade
@author    brunno.costa
@since     13/02/2020
@version 1.0
@param 01 - cChaveSubs  , caracter, string com a chave de substitui��o do registro
@param 02 - cAlternativo, caracter, c�digo do produto alternativo relacionado
@param 03 - nQuantidade , numero  , quantidade para desfazer a substitui��o
@param 04 - nPeriodo    , numero  , indicador do periodo atual
@param 05 - aBaixaPorOP , array   , recebe e retorna por refer�ncia array aBaixaPorOP relacionado ao processo
@return nSobraPos, numero, retorna o saldo apos consumir o produto
/*/
METHOD desfazParcialSubstituicao(cChaveSubs, cAlternativo, nQuantidade, nPeriodo, aBaixaPorOP) CLASS MrpDominio_Alternativo

	Local aAlteDeman //Alternativos da Demanda
	Local aRegistro
	Local aSubstit     := {}
	Local cChave
	Local cOrigDeman
	Local cProduto     := Left(cChaveSubs, 90)
	Local lRastreia    := ::oDominio:oParametros["lRastreia"]
	Local nInd
	Local nConsAlt
	Local nConsOrig

	Default cAlternativo := ""

	cOrigDeman := "KEY_" + AllTrim(cProduto) + "_" + cValToChar(nPeriodo) //Original + Periodo

	//Prote��o de execu��o duplicada via oRastreio:dezfazExplosoes ou PCOCalcNec- Lock
	If lRastreia
		::oDados:oAlternativos:trava(cOrigDeman)
	EndIf

	//Atualiza controle de destinos de substituicao do produto origem
	aAlteDeman := ::oDados:oAlternativos:getItemAList("substituicoes_produtos", cOrigDeman)

	//Possui substituicoes
	If aAlteDeman != Nil .AND. !Empty(aAlteDeman)

		//Self:oDominio:oRastreio:desfazSubstituicoes(cProduto, nPeriodo, @aBaixaPorOP, -nQuantidade, cAlternativo)

		//Recupera registros das substituicoes
		For nInd := 1 to Len(aAlteDeman)
			If aAlteDeman[nInd] == cAlternativo
				cChave       := cOrigDeman + "_" + AllTrim(cAlternativo) //Original + Periodo + Alternativo
				aRegistro    := ::oDados:oAlternativos:getItemAList("substituicoes_dados", cChave)
				If aRegistro != Nil
					aAdd(aSubstit, {cChave, aClone(aRegistro)})
				EndIf
			EndIf
		Next

		//Ordena da maior a menor
		aSubstit := aSort(aSubstit, , , {|x,y| x[2][4] > y[2][4] })//TODO, REVISAR NECESSIDADE
		For nInd := 1 to Len(aSubstit)
			cChave       := aSubstit[nInd][1]
			aRegistro    := aSubstit[nInd][2]
			cAlternativo := aRegistro[2]

			If nQuantidade == 0
				Exit

			Else
				//Limite de consumo de alternativos
				aRegistro[7] := aRegistro[4]
				aRegistro[8] := aRegistro[5]
				nConsAlt     := aRegistro[7] //Quantidade Limite Alternativo
				nConsOrig    := aRegistro[8] //Quantidade Limite Original

				If nConsOrig != 0 .AND. nConsAlt != 0

					If nConsOrig > nQuantidade
						nConsAlt    -= nQuantidade * nConsAlt / nConsOrig
						nConsOrig   -= nQuantidade
						nQuantidade := 0

						aRegistro[7] := nConsAlt
						aRegistro[8] := nConsOrig
					Else
						nConsAlt     := 0
						nConsOrig    := 0
						nQuantidade  -= nConsOrig

						aRegistro[7] := 0
						aRegistro[8] := 0
					EndIf

					::oDominio:oLogs:log(STR0112 + AllTrim(aRegistro[1]) + STR0110 + AllTrim(aRegistro[2]) + STR0111 + cValToChar(nPeriodo)+"': "+cValToChar(nConsOrig)+" <- "+cValToChar(nConsAlt)+".", "34") //"Desfaz a substituicao do produto '" + "' pelo produto '" + "' no periodo '"

					::oDados:oAlternativos:setItemAList("substituicoes_dados", cChave, aRegistro, .F., .T., .F.)
				Endif
			Endif

		Next

	EndIf

	//Prote��o de execu��o duplicada via oRastreio:dezfazExplosoes ou PCOCalcNec- UnLock
	If lRastreia
		::oDados:oAlternativos:destrava(cOrigDeman)
	EndIf

Return

/*/{Protheus.doc} existeSubstituicao
Verifica se existe substitui��o do cProduto pelo cAlternativo no nPeriodo
@author    brunno.costa
@since     06/01/2020
@version 1.0
@param 01 - cProduto    , caracter, codigo do produto avaliado (alternativo de outro produto)
@param 02 - cAlternativo, caracter, c�digo do produto alternativo para an�lise
@param 03 - nPeriodo    , numero  , indicador do periodo atual
@return lExiste, l�gico, indica se existe substitui��o do cProduto pelo cAlternativo no nPeriodo
/*/
METHOD existeSubstituicao(cProduto, cAlternati, nPeriodo) CLASS MrpDominio_Alternativo

	Local lReturn    := .F.
	Local cOrigDeman := "KEY_" + AllTrim(cProduto) + "_" + cValToChar(nPeriodo) //Original + Periodo
	Local aAlteDeman := ::oDados:oAlternativos:getItemAList("substituicoes_produtos", cOrigDeman)
	Local nPos       := 0

	If aAlteDeman != Nil
		nPos := aScan(aAlteDeman, cAlternati)
		If nPos > 0
			lReturn := .T.
		EndIf
	EndIf

Return lReturn

/*/{Protheus.doc} loopSubstituicaoAnterior
Identifica a ocorr�ncia de loop em substitui��o anterior que impede a substitui��o atual
Produto A com B Alternativo e com substitui��o
Produto B com C Alternativo e com substitui��o
Produto C com A Alternativo e com substitui��o
@author    brunno.costa
@since     06/01/2020
@version 1.0
@param 01 - cProduto  , caracter, codigo do produto avaliado (alternativo de outro produto)
@param 02 - cAlternati, caracter, c�digo do produto alternativo para an�lise
@param 03 - nPeriodo  , numero  , indicador do periodo atual
@param 04 - cPath     , caracter, concatena��o de alternativos: cPath + chr(13) + aAlteDeman[nIndAlt]
@return lReturn, l�gico, indica se existe substitui��o do cProduto pelo cAlternativo no nPeriodo
/*/
METHOD loopSubstituicaoAnterior(cProduto, cAlternati, nPeriodo, cPath) CLASS MrpDominio_Alternativo

	Local lReturn       := .F.
	Local cOrigDeman    := "KEY_" + AllTrim(cAlternati) + "_" + cValToChar(nPeriodo) //Original + Periodo
	Local aAlteDeman    := ::oDados:oAlternativos:getItemAList("substituicoes_produtos", cOrigDeman)
	Local nPos          := 0
	Local nIndAlt       := 0
	Local nAlternativos := 0

	Default cPath := ""

	If aAlteDeman != Nil
		nPos := aScan(aAlteDeman, cProduto)

		If nPos > 0
			lReturn := .T.
		ElseIf cAlternati $ cPath
			lReturn := .T.
		EndIf

		If !lReturn
			nAlternativos := Len(aAlteDeman)
			For nIndAlt := 1 to nAlternativos
				lReturn := ::loopSubstituicaoAnterior(cProduto, aAlteDeman[nIndAlt], nPeriodo, cPath + chr(13) + aAlteDeman[nIndAlt])
				If lReturn
					Exit
				EndIf
			Next
		EndIf
	EndIf

Return lReturn

/*/{Protheus.doc} saldoOPNecessidade
Identifica o saldo dispon�vel da necessidade desta OP
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto , caracter, c�digo do produto original
@param 02 - nPeriodo , n�mero  , n�mero do per�odo para an�lise
@param 03 - cChave   , caracter, chave do documento pai da rastreabilidade: documento pai + chr(13) + TRT
@param 04 - cRegra   , caracter, retorna por refer�ncia a regra para consumo de alternativos:
								1- Valida Original; Valida Alternativo; Compra Original
								2- Valida Original; Valida Alternativo; Compra Alternativo
								3- Valida Alternativo; Compra Alternativo
@return nSldOp       , numero, saldo dispon�vel da necessidade desta OP
/*/
METHOD saldoOPNecessidade(cProduto, nPeriodo, cChave, cRegra) CLASS MrpDominio_Alternativo
	Local nSldOp := 0
	Local aDados := {}
	Local lError := .F.
	Local cList  := cProduto + chr(13) + cValToChar(nPeriodo)

	aDados := ::oDominio:oRastreio:oDados_Rastreio:oDados:getItemAList(cList, cChave, @lError)

	If !lError
		If ("OP"$aDados[::oDominio:oRastreio:getPosicao("TIPOPAI")])
			cRegra := aDados[::oDominio:oRastreio:getPosicao("REGRA_ALTERNATIVO")]
			nSldOp := aDados[::oDominio:oRastreio:getPosicao("NECESSIDADE")]
		EndIf
	EndIf

Return nSldOp

/*/{Protheus.doc} validaVigencia
Identifica se o alternativo possui data de vig�ncia v�lida
@author    brunno.costa
@since     29/10/2019
@version 1.0
@param 01 - nPeriodo , n�mero, identificador num�rico do per�odo
@param 02 - dVigencia, data  , data de vig�ncia do alternativo
@return lDTValida, l�gico, indica se a data de vig�ncia � v�lida
/*/
METHOD validaVigencia(nPeriodo, dVigencia) CLASS MrpDominio_Alternativo
	Local lDTValida := Empty(dVigencia) .OR. dVigencia <= ::oDominio:aPeriodos[nPeriodo]
Return lDTValida

/*/{Protheus.doc} aplicaFator
Aplica o fator de convers�o do alternativo
@author    brunno.costa
@since     29/10/2019
@version 1.0
@param 01 - nQuantidade , n�mero  , quantidade para convers�o
@param 02 - cTipoFator  , caracter, tipo do fator de convers�o:
                                    1 - Multiplica��o
									2 - Divis�o
@param 03 - nFator      , n�mero  , fator de convers�o
@param 04 - lInversa    , l�gico  , indica e deve inverter a convers�o (Utilizada nos casos onde deve desfazer a substitui��o)
@return nNovaQtde       , numero, saldo dispon�vel da necessidade desta OP
/*/
METHOD aplicaFator(nQuantidade, cTipoFator, nFator, lInversa) CLASS MrpDominio_Alternativo

	Local nNovaQtde := nQuantidade
	Local cTipo     := cTipoFator

	Default lInversa := .F.

	//Inverte fatores de convers�o ao desfazer
	If lInversa
		If cTipoFator == "1"
			cTipo := "2"

		ElseIf cTipoFator == "2"
			cTipo := "1"

		EndIf
	EndIf

	If cTipo == "1"     //Multiplica��o
		nNovaQtde := nQuantidade * nFator

	ElseIf cTipo == "2" //Divis�o
		nNovaQtde := nQuantidade / nFator

	EndIf

Return nNovaQtde

/*/{Protheus.doc} aplicaProdutoFator
Aplica o fator de convers�o do alternativo por Produto
@author    brunno.costa
@since     29/10/2019
@version 1.0
@param 01 - nQuantidade , n�mero  , quantidade para convers�o
@param 02 - cTipoFator  , caracter, tipo do fator de convers�o:
                                    1 - Multiplica��o
									2 - Divis�o
@param 03 - nFator      , n�mero  , fator de convers�o
@param 04 - lInversa    , l�gico  , indica e deve inverter a convers�o (Utilizada nos casos onde deve desfazer a substitui��o)
@return nNovaQtde       , numero, saldo dispon�vel da necessidade desta OP
/*/
METHOD aplicaProdutoFator(cChaveProd, cAlternativo, nQuantidade, lInversa) CLASS MrpDominio_Alternativo

	Local aCampos       := {"ALT_ALTERN", "ALT_TPFAT", "ALT_FATOR"}
	Local aCposAlt      := {}
	Local aMinMaxAlt
	Local cCodAltern
	Local cIndAlt
	Local cMaxAlt
	Local lErrorAlt     := .F.
	Local nReturn       := nQuantidade

	aMinMaxAlt    := ::oDados:oAlternativos:getItemAList("min_max", cChaveProd, @lErrorAlt) //Identifica exist�ncia de Alternativos e sequ�ncias: Min-Max

	If !lErrorAlt
		cIndAlt := aMinMaxAlt[1]
		cMaxAlt := aMinMaxAlt[2]
		While .T.
			If cIndAlt > cMaxAlt
				Exit
			EndIf

			aSize(aCposAlt, 0)
			cChvAltern   := cChaveProd + cIndAlt
			aCposAlt     := ::oDados:retornaCampo("ALT", 1, cChvAltern, aCampos, @lErrorALT,,,,,,.T.)

			If lErrorALT
				aCposAlt  := {}
				lErrorALT := .F.
				cIndAlt := Soma1(cIndAlt)
				Loop
			EndIf

			cCodAltern   := aCposAlt[1]
			cTipoFator   := aCposAlt[2]
			nFator       := aCposAlt[3]

			If cAlternativo == cCodAltern
				nReturn := Iif(nQuantidade ==0 , 0, ::aplicaFator(nQuantidade, cTipoFator, nFator, lInversa))
			EndIf

			cIndAlt := Soma1(cIndAlt)
		EndDo
	Endif

Return nReturn