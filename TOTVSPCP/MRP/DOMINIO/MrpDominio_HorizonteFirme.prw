#INCLUDE 'protheus.ch'
#INCLUDE 'MRPDominio.ch'

/*/{Protheus.doc} MrpDominio_HorizonteFirme
Regras de negocio MRP - Horizonte Firme
@author    brunno.costa
@since     25/04/2019
@version   1
/*/
CLASS MrpDominio_HorizonteFirme FROM LongClassName

	METHOD new() CONSTRUCTOR
	METHOD atributosDefault(cAlias, cCpoProd, cAliasPRD, cFlagDia, cFlagSem, cFlagMes, cTpHorizon, cHorizonte, cJoin)
	METHOD criaScriptIN(dDataIni, cAliasPRD, cAlias, cCpoProd, cCpoData, cWhere, cScriptIN)
	METHOD scriptMSSQL(dDataIni, cAliasPRD, cAlias, cCpoProd, cCpoData)
	METHOD scriptOracle(dDataIni, cAliasPRD, cAlias, cCpoProd, cCpoData)
	METHOD scriptPostgres(dDataIni, cAliasPRD, cAlias, cCpoProd, cCpoData)

ENDCLASS

/*/{Protheus.doc} MrpDominio_HorizonteFirme
Construtor da Classe Horizonte Firme
@author    brunno.costa
@since     25/04/2019
@version   1
/*/
METHOD new() CLASS MrpDominio_HorizonteFirme
Return Self

/*/{Protheus.doc} atributosDefault
Atribui Conte�do Default aos Atributos Padr�es do Select
@author    brunno.costa
@since     11/05/2020
@version   1
@param 01 - cAlias    , caracter, indica o alias para relacionamento com HWA ou SVK
@param 02 - cCpoProd  , caracter, indica o campo do produto para relacionamento referente cAlias
@param 03 - cAliasPRD , caracter, indica o alias para relacionamento: HWA ou SVK
@param 04 - cFlagDia  , caracter, (Retorno por Referencia) conteudo padrao para indicar dia
@param 05 - cFlagSem  , caracter, (Retorno por Referencia) conteudo padrao para indicar semana
@param 06 - cFlagMes  , caracter, (Retorno por Referencia) conteudo padrao para indicar mes
@param 07 - cTpHorizon, caracter, (Retorno por Referencia) coluna para comparacao do tipo de horizonte firme
@param 08 - cHorizonte, caracter, (Retorno por Referencia) coluna para comparacao do horizonte firme
@param 09 - cJoin     , caracter, (Retorno por Referencia) script sql padrao para o relacionamento
/*/
METHOD atributosDefault(cAlias, cCpoProd, cAliasPRD, cFlagDia, cFlagSem, cFlagMes, cTpHorizon, cHorizonte, cJoin) CLASS MrpDominio_HorizonteFirme

	Default cJoin := ""

	cFlagDia   := "1"
	cFlagSem   := "2"
	cFlagMes   := "3"

	If cAliasPRD == "HWA"
		cTpHorizon := "HWA.HWA_TPHFIX"
		cHorizonte := "HWA.HWA_HORFIX"
		cJoin      += " JOIN  " + RetSqlName("HWA") + " HWA ON HWA.HWA_PROD = " + cAlias+"."+cCpoProd

	ElseIf cAliasPRD == "SVK"
		cTpHorizon := "SVK.VK_TPHOFIX"
		cHorizonte := "SVK.VK_HORFIX"
		cJoin      += " JOIN  " + RetSqlName("SVK") + " SVK ON SVK.VK_COD = " + cAlias+"."+cCpoProd

	EndIf

Return

/*/{Protheus.doc} criaScriptIN
Gera Script para Filtro dos Registros para Exclus�o em SQL Server
@author    brunno.costa
@since     11/05/2020
@version   1
@param 01 dDataIni  , data, data de processamento do MRP
@param 02 cAliasPRD , caracter, alias da tabela de produtos refer�ncia
@param 03 cAlias    , caracter, alias da tabela referencia para identifica��o dos RECNOS
@param 04 cCpoProd  , caracter, campo produto da tabela cAlias
@param 04 cCpoData  , caracter, campo data para compara��o da tabela cAlias
@param 05 cWhere    , caracter, condi��o compementar do bloco where
@param 06 cScriptIN , caracter, retorna por refer�ncia complemento de WHERE XX_CAMPO IN (cScriptIN)
@return cScriptIN, caracter, complemento de WHERE XX_CAMPO IN (cScriptIN)
/*/
METHOD criaScriptIN(dDataIni, cAliasPRD, cAlias, cCpoProd, cCpoData, cWhere, cScriptIN) CLASS MrpDominio_HorizonteFirme

	Local cBanco     := TCGetDB()
	Local cJoin      := ""
	Local cHorizonte := ""
	Local cTpHorizon := ""

	Default cScriptIN := ""

	Self:atributosDefault(cAlias + "_1", cCpoProd, cAliasPRD, /*@cFlagDia*/, /*@cFlagSem*/, /*@cFlagMes*/, @cTpHorizon, @cHorizonte, @cJoin)

	cScriptIN += " SELECT " + cAlias + "_1" + ".R_E_C_N_O_ RECNO "
	cScriptIN += " FROM " + RetSqlName(cAlias) + " " + cAlias + "_1"
	cScriptIN +=   cJoin
	cScriptIN += " WHERE " + cAlias + "_1" + ".D_E_L_E_T_=' ' AND "

	If !Empty(cWhere)
		cWhere    := StrTran(cWhere, " D_E_L_E_T_", " " + cAlias+"_1.D_E_L_E_T_") + " AND "
		cScriptIN += StrTran(cWhere, cAlias+".", cAlias+"_1.")
	EndIf

	//Trecho Valida��o Produto
	cScriptIN += " COALESCE(" + cTpHorizon + ", ' ') <> ' ' AND COALESCE(" + cHorizonte + ", 0) > 0 AND "

	cScriptIN += " ( " + cAliasPRD + ".D_E_L_E_T_=' ' AND "

	//Ou Regra Tabela
	If cBanco == "ORACLE"
		cScriptIN += Self:scriptOracle(dDataIni, cAliasPRD, cAlias + "_1", cCpoProd, cCpoData)

	ElseIf cBanco == "POSTGRES"
		cScriptIN += Self:scriptPostgres(dDataIni, cAliasPRD, cAlias + "_1", cCpoProd, cCpoData)

	Else
		cScriptIN += Self:scriptMSSQL(dDataIni, cAliasPRD, cAlias + "_1", cCpoProd, cCpoData)

	EndIf

	cScriptIN += " ) " //(cAliasPRD.D_E_L_E_T_

Return cScriptIN

/*/{Protheus.doc} scriptMSSQL
Gera Script para Filtro dos Registros em SQL Server
@author    brunno.costa
@since     11/05/2020
@version   1
@param 01 dDataIni  , data, data de processamento do MRP
@param 02 cAliasPRD , caracter, alias da tabela de produtos refer�ncia
@param 03 cAlias    , caracter, alias da tabela referencia para identifica��o dos RECNOS
@param 04 cCpoProd  , caracter, campo produto da tabela cAlias
@param 05 cCpoData  , caracter, campo data para compara��o da tabela cAlias
Return cScriptIN, caracter, trecho complementar do script em SQL Server
/*/
METHOD scriptMSSQL(dDataIni, cAliasPRD, cAlias, cCpoProd, cCpoData) CLASS MrpDominio_HorizonteFirme

	Local cFlagDia
	Local cFlagSem
	Local cFlagMes
	Local cScriptIN  := ""
	Local cDataIni   := DtoS(dDataIni)
	Local cHorizonte := ""
	Local cTpHorizon := ""

	Self:atributosDefault(cAlias, cCpoProd, cAliasPRD, @cFlagDia, @cFlagSem, @cFlagMes, @cTpHorizon, @cHorizonte, Nil)

	//Ou Regra Tabela
	cScriptIN += "("
	cScriptIN += "CONVERT(DATETIME, " + cAlias+"."+cCpoData + ") <= "

	cScriptIN += "(CASE WHEN " + cTpHorizon + " = '" + cFlagDia + "' THEN (CONVERT(DATETIME, '" + cDataIni + "') + "  + cHorizonte + ") "
	cScriptIN +=      " WHEN " + cTpHorizon + " = '" + cFlagSem + "' THEN (CONVERT(DATETIME, '" + cDataIni + "') + (" + cHorizonte + " * 7 ) ) "
	cScriptIN +=      " WHEN " + cTpHorizon + " = '" + cFlagMes + "' THEN (CONVERT(DATETIME, '" + cDataIni + "') + (" + cHorizonte + " * 30) ) "
	cScriptIN +=                                    " ELSE (CONVERT(DATETIME, '" + cDataIni + "') + (" + cHorizonte + " * 365) ) "
	cScriptIN += " END) "
	cScriptIN += ")"

Return cScriptIN

/*/{Protheus.doc} scriptOracle
Gera Script para Filtro dos Registros em Oracle
@author    brunno.costa
@since     11/05/2020
@version   1
@param 01 dDataIni  , data, data de processamento do MRP
@param 02 cAliasPRD , caracter, alias da tabela de produtos refer�ncia
@param 03 cAlias    , caracter, alias da tabela referencia para identifica��o dos RECNOS
@param 04 cCpoProd  , caracter, campo produto da tabela cAlias
@param 05 cCpoData  , caracter, campo data para compara��o da tabela cAlias
Return cScriptIN, caracter, trecho complementar do script em Oracle
/*/
METHOD scriptOracle(dDataIni, cAliasPRD, cAlias, cCpoProd, cCpoData) CLASS MrpDominio_HorizonteFirme

	Local cFlagDia
	Local cFlagSem
	Local cFlagMes
	Local cScriptIN  := ""
	Local cDataIni   := DtoS(dDataIni)
	Local cHorizonte := ""
	Local cTpHorizon := ""

	Self:atributosDefault(cAlias, cCpoProd, cAliasPRD, @cFlagDia, @cFlagSem, @cFlagMes, @cTpHorizon, @cHorizonte, Nil)

	//Ou Regra Tabela
	cScriptIN += "("
	cScriptIN += "TO_DATE(" + cAlias+"."+cCpoData + ", 'YYYYMMDD') <= "

	cScriptIN += "(CASE WHEN " + cTpHorizon + " = '" + cFlagDia + "' THEN (TO_DATE('" + cDataIni + "', 'YYYYMMDD') + "  + cHorizonte + ") "
	cScriptIN +=      " WHEN " + cTpHorizon + " = '" + cFlagSem + "' THEN (TO_DATE('" + cDataIni + "', 'YYYYMMDD') + (" + cHorizonte + " * 7 ) ) "
	cScriptIN +=      " WHEN " + cTpHorizon + " = '" + cFlagMes + "' THEN (TO_DATE('" + cDataIni + "', 'YYYYMMDD') + (" + cHorizonte + " * 30) ) "
	cScriptIN +=                                    " ELSE (TO_DATE('" + cDataIni + "', 'YYYYMMDD') + (" + cHorizonte + " * 365) ) "
	cScriptIN += " END) "
	cScriptIN += ")"

Return cScriptIN

/*/{Protheus.doc} scriptPostgres
Gera Script para Filtro dos Registros para Exclus�o em SQL Server
@author    brunno.costa
@since     11/05/2020
@version   1
@param 01 dDataIni  , data, data de processamento do MRP
@param 02 cAliasPRD , caracter, alias da tabela de produtos refer�ncia
@param 03 cAlias    , caracter, alias da tabela referencia para identifica��o dos RECNOS
@param 04 cCpoProd  , caracter, campo produto da tabela cAlias
@param 05 cCpoData  , caracter, campo data para compara��o da tabela cAlias
Return cScriptIN, caracter, trecho complementar do script em Postgres
/*/
METHOD scriptPostgres(dDataIni, cAliasPRD, cAlias, cCpoProd, cCpoData) CLASS MrpDominio_HorizonteFirme

	Local cFlagDia
	Local cFlagSem
	Local cFlagMes
	Local cScriptIN  := ""
	Local cDataIni := DtoS(dDataIni)
	Local cHorizonte := ""
	Local cTpHorizon := ""

	Self:atributosDefault(cAlias, cCpoProd, cAliasPRD, @cFlagDia, @cFlagSem, @cFlagMes, @cTpHorizon, @cHorizonte, Nil)

	//Ou Regra Tabela
	cScriptIN += "("
	cScriptIN += "TO_DATE(" + cAlias+"."+cCpoData + ", 'YYYYMMDD') <= "

	cScriptIN += "(CASE WHEN " + cTpHorizon + " = '" + cFlagDia + "' THEN (TO_DATE('" + cDataIni + "', 'YYYYMMDD') + " + cHorizonte + "*interval'1 days' ) "
	cScriptIN +=      " WHEN " + cTpHorizon + " = '" + cFlagSem + "' THEN (TO_DATE('" + cDataIni + "', 'YYYYMMDD') + " + cHorizonte + "*interval'1 weeks' ) "
	cScriptIN +=      " WHEN " + cTpHorizon + " = '" + cFlagMes + "' THEN (TO_DATE('" + cDataIni + "', 'YYYYMMDD') + " + cHorizonte + "*interval'1 months' ) "
	cScriptIN +=                                                   " ELSE (TO_DATE('" + cDataIni + "', 'YYYYMMDD') + " + cHorizonte + "*interval'1 years' ) "
	cScriptIN += " END) "
	cScriptIN += ")"

Return cScriptIN