#INCLUDE 'protheus.ch'
#INCLUDE 'MRPDominio.ch'

/*/{Protheus.doc} MrpDominio_LocalConsumo
Regras de Negocio - Atribuição Armazem de Consumo no campo HWC_LOCAL
@author    brunno.costa
@since     03/07/2020
@version   12.1.27
/*/
CLASS MrpDominio_LocalConsumo FROM LongClassName

	METHOD new() CONSTRUCTOR
	METHOD aplicar(oParametros, cTicket)
	METHOD montaSelect(oParametros)
	METHOD updateSQLServer(oParametros)
	METHOD updateOracle(oParametros)
	METHOD updatePostgres(oParametros)

ENDCLASS

/*/{Protheus.doc} new
Metodo construtor
@author    brunno.costa
@since     03/07/2020
@version   12.1.27
/*/
METHOD new() CLASS MrpDominio_LocalConsumo
Return Self

/*/{Protheus.doc} aplicar
Aplica Local de Consumo na HWC (HWC_LOCAL)
@author    brunno.costa
@since     03/07/2020
@version   12.1.27
@param 01 - oParametros, objeto, parametros de execucao do ticket do MRP
/*/
METHOD aplicar(oParametros, cTicket) CLASS MrpDominio_LocalConsumo

	Local cBanco := TCGetDB()

	If cBanco == "ORACLE"
		Self:updateOracle(oParametros)
	ElseIf "MSSQL" $ cBanco
		Self:updateSQLServer(oParametros)
	ElseIf cBanco == "POSTGRES"
		Self:updatePostgres(oParametros)
	EndIf

Return Self

/*/{Protheus.doc} montaSelect
Monta SELECT com os dados do Update
@author    brunno.costa
@since     03/07/2020
@version   12.1.27
@param 01 - oParametros, objeto, parametros de execucao do ticket do MRP
@return cQuery, caracter, script SELECT para update
/*/
METHOD montaSelect(oParametros) CLASS MrpDominio_LocalConsumo

	Local cQuery := ""

	//cInProcessLocation      -> MV_LOCPROC
	//lUsesInProcessLocation  -> MV_GRVLOCP
	//lUsesProductIndicator   -> SuperGetMv(cParametro, .F., "SB1") == "SBZ"

    cQuery += " SELECT HWC.HWC_PRODUT, "
    cQuery +=        " HWC.HWC_LOCAL, "
    cQuery += 	     " HWA.HWA_APROPR, "
    cQuery += 	     " HWA.HWA_LOCPAD, "
    cQuery += 	     " T4M.T4M_ARMCON, "
    cQuery += 	     " HWC.R_E_C_N_O_ RECNOHWC, "

	If oParametros["lUsesInProcessLocation"]
    	cQuery += 	   " ( CASE HWA_APROPR WHEN '2' "
		cQuery +=        " THEN '" + oParametros["cInProcessLocation"] + "' "
		cQuery +=        " ELSE COALESCE(T4M.T4M_ARMCON, HWA.HWA_LOCPAD)
		cQuery +=        " END ) NOVO_LOCAL "
	Else
		cQuery += 	   " COALESCE(T4M.T4M_ARMCON, HWA.HWA_LOCPAD) NOVO_LOCAL "
	EndIf

    cQuery += " FROM (SELECT HWC_PRODUT, HWC_LOCAL, R_E_C_N_O_, HWC_DATA, HWC_QTNECE "
    cQuery +=       " FROM " + RetSqlName("HWC") + " "
    cQuery +=       " WHERE (D_E_L_E_T_ = ' ') "
	cQuery +=         " AND (HWC_TICKET = '" + oParametros["ticket"] + "') "
	cQuery +=         " AND (HWC_FILIAL = '" + xFilial("HWC") + "') "
	cQuery +=     " ) HWC "
    cQuery += 	  " INNER JOIN "

	If oParametros["lUsesProductIndicator"]
		cQuery +=        " ( SELECT HWA.HWA_PROD, HWA.HWA_APROPR, COALESCE(HWE_LOCPAD, HWA.HWA_LOCPAD) HWA_LOCPAD "
		cQuery +=          " FROM " + RetSqlName("HWA") + " HWA "
		cQuery +=              " LEFT OUTER JOIN "
		cQuery +=                 " ( SELECT HWE_PROD, HWE_LOCPAD "
		cQuery +=                 " FROM " + RetSqlName("HWE") + " HWE "
		cQuery +=                 " WHERE (D_E_L_E_T_ = ' ') "
		cQuery +=                   " AND (HWE_FILIAL = '" + xFilial("HWE") + "') "
		cQuery +=              " ) HWE "
		cQuery +=              " ON HWE.HWE_PROD = HWA.HWA_PROD "
		cQuery +=          " WHERE (HWA.D_E_L_E_T_ = ' ') "
		cQuery +=            " AND (HWA_FILIAL = '" + xFilial("HWA") + "') "
		cQuery +=        " ) HWA "

	Else
		cQuery +=        " ( SELECT HWA.HWA_PROD, HWA.HWA_APROPR, HWA.HWA_LOCPAD "
		cQuery +=          " FROM " + RetSqlName("HWA") + " HWA "
		cQuery +=          " WHERE (HWA.D_E_L_E_T_ = ' ') "
		cQuery +=            " AND (HWA_FILIAL = '" + xFilial("HWA") + "') "
		cQuery +=        " ) HWA "

	EndIf

    cQuery +=       " ON HWC.HWC_PRODUT = HWA.HWA_PROD "
    cQuery += 	  " LEFT OUTER JOIN "
    cQuery +=        " ( SELECT T4M_PROD, T4M_DTINI, T4M_DTFIN, T4M_QNTDE, T4M_QNTATE, (CASE T4M_ARMCON WHEN ' ' THEN NULL ELSE T4M_ARMCON END) T4M_ARMCON "
    cQuery +=        " FROM " + RetSqlName("T4M")
    cQuery +=        " WHERE (D_E_L_E_T_ = ' ') "
	cQuery +=          " AND (T4M_FILIAL = '" + xFilial("T4M") + "') "
	cQuery +=        " ) T4M "
    cQuery +=     " ON  HWC.HWC_QTNECE BETWEEN T4M.T4M_QNTDE AND T4M.T4M_QNTATE "
    cQuery += 	  " AND ((HWC.HWC_DATA BETWEEN T4M.T4M_DTINI AND T4M.T4M_DTFIN) OR (T4M.T4M_DTINI = ' ' AND T4M.T4M_DTFIN = ' ') ) "
    cQuery += 	  " AND HWC.HWC_PRODUT = T4M.T4M_PROD "

Return cQuery

/*/{Protheus.doc} updateSQLServer
Aplica Update no HWC_LOCAL em banco SQL Server
@author    brunno.costa
@since     03/07/2020
@version   12.1.27
/*/
METHOD updateSQLServer(oParametros) CLASS MrpDominio_LocalConsumo

	Local cQuery  := Self:montaSelect(oParametros)
	Local cUpdate := ""

	cUpdate += " UPDATE " + RetSqlName("HWC")
	cUpdate += " SET HWC_LOCAL = TRB.NOVO_LOCAL "
	cUpdate += " FROM " + RetSqlName("HWC") + " HWC "
	cUpdate +=      " INNER JOIN (" + cQuery + ") TRB "
	cUpdate +=      " ON HWC.R_E_C_N_O_ = TRB.RECNOHWC  "

	If TcSqlExec(cUpdate) < 0
		Final(STR0167, tcSQLError() + cQuery) //"Erro ao atualizar o armazem de consumo."
	EndIf

Return

/*/{Protheus.doc} updateOracle
Aplica Update no HWC_LOCAL em banco Oracle
@author    brunno.costa
@since     03/07/2020
@version   12.1.27
/*/
METHOD updateOracle(oParametros) CLASS MrpDominio_LocalConsumo

	Local cQuery  := Self:montaSelect(oParametros)
	Local cUpdate := ""

	cUpdate := " MERGE INTO " + RetSqlName("HWC") + " HWC "
	cUpdate +=      " USING (" + cQuery + ") TRB "
	cUpdate +=         " ON (HWC.R_E_C_N_O_ = TRB.RECNOHWC ) "
	cUpdate +=  " WHEN MATCHED THEN "
	cUpdate +=             " UPDATE SET HWC_LOCAL = NOVO_LOCAL "

	If TcSqlExec(cUpdate) < 0
		Final(STR0167, tcSQLError() + cQuery) //"Erro ao atualizar o armazem de consumo."
	EndIf

Return

/*/{Protheus.doc} updatePostgres
Aplica Update no HWC_LOCAL em banco Postgres
@author    brunno.costa
@since     03/07/2020
@version   12.1.27
/*/
METHOD updatePostgres(oParametros) CLASS MrpDominio_LocalConsumo

	Local cQuery  := Self:montaSelect(oParametros)
	Local cUpdate := ""

	cUpdate += " UPDATE " + RetSqlName("HWC")
	cUpdate += " SET HWC_LOCAL = TRB.NOVO_LOCAL "
	cUpdate += " FROM (" + cQuery + ") TRB "
	cUpdate += " WHERE " + RetSqlName("HWC") + ".R_E_C_N_O_ = TRB.RECNOHWC "

	If TcSqlExec(cUpdate) < 0
		Final(STR0167, tcSQLError() + cQuery) //"Erro ao atualizar o armazem de consumo."
	EndIf

Return