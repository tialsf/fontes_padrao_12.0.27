#INCLUDE 'protheus.ch'
#INCLUDE 'MrpDominio.ch'

/*/{Protheus.doc} MrpDominio_MOD
Processamento de Produtos MOD
@author    brunno.costa
@since     25/04/2019
@version   1
/*/
CLASS MrpDominio_MOD FROM LongClassName
	METHOD new() CONSTRUCTOR
	METHOD adicionaCampoHWA(aFields, oParametros)
	METHOD campoCCustoNoDicionario()
	METHOD converteQuantidadeMOD(nG1Quant, nNecessPai, lQtdFixa, oParametros)
	METHOD produtoMOD(cProduto)
ENDCLASS

/*/{Protheus.doc} new
Metodo construtor
@author    brunno.costa
@since     28/07/2020
@version   1
Return Self, objeto, instancia desta classe
/*/
METHOD new() CLASS MrpDominio_MOD
Return Self

/*/{Protheus.doc} new
Adiciona Campo HWA_MOD no Json de Produtos - Carga Memoria
@author    brunno.costa
@since     28/07/2020
@version   1
@param 01, aFields    , array, array de campos aFields retornado por referencia
@param 02, oParametros, objeto, instancia do objeto Json de parametros
/*/
METHOD adicionaCampoHWA(aFields, oParametros) CLASS MrpDominio_MOD
	Local cBanco  := Upper(TcGetDb())
	Local cSubstr := Iif("MSSQL"$cBanco, "SUBSTRING", "SUBSTR")
	If oParametros["lUsesLaborProduct"] .AND. Self:campoCCustoNoDicionario()
		aAdd(aFields, { "HWA_MOD"  , "VC", "HWA", "(CASE " + cSubstr + "(HWA.HWA_PROD, 1, 3) WHEN 'MOD' " +;
														"THEN 'T' "                                       +;
														"ELSE (CASE HWA.HWA_CCUSTO WHEN ' ' "             +;
																"THEN 'F' "                               +;
																"ELSE 'T' "                               +;
															" END) "                                      +;
													" END)"                                               })
	Else
		aAdd(aFields, { "HWA_MOD"  , "VC", "HWA", "(CASE " + cSubstr + "(HWA.HWA_PROD, 1, 3) WHEN 'MOD' " +;
                                                        "THEN 'T' "                                       +;
						                                "ELSE 'F' "                                       +;
					                              " END)"                                                 })
	EndIf

Return


/*/{Protheus.doc} campoCCustoNoDicionario
Verifica existencia do campo HWA_CCUSTO no dicionario de dados
@author    brunno.costa
@since     28/07/2020
@version   1
Return lReturn, logico, indica se possui o campo HWA_CCUSTO no dicionario
/*/
METHOD campoCCustoNoDicionario() CLASS MrpDominio_MOD
	Local nTamanho := GetSX3Cache("HWA_CCUSTO","X3_TAMANHO")
Return nTamanho != Nil .AND. nTamanho > 0

/*/{Protheus.doc} converteQuantidadeMOD
Converte a quantidade da necessidade de produto MOD
@author    brunno.costa
@since     28/07/2020
@version   1
@param 01, nG1Quant   , numero, quantidade do produto na estrutura
@param 02, nNecessPai , numero, necessidade do produto pai
@param 03, lQtdFixa   , logico, indicador de quantidade fixa na estrutura
@param 04, oParametros, objeto, instancia do objeto Json de parametros
@return nReturn, numero, quantidade da necessidade do protudo MOD convertida
/*/
METHOD converteQuantidadeMOD(nG1Quant, nNecessPai, lQtdFixa, oParametros) CLASS MrpDominio_MOD
	Local nReturn := nG1Quant
	If oParametros["cStandardTimeUnit"] == "N"        //MV_TPHR
		nReturn := Int(nG1Quant)
		nReturn += ((nG1Quant - nReturn) / 60) * 100
	EndIf
	If !lQtdFixa
		If oParametros["cUnitOfLaborInTheBOM"] != "H" //MV_UNIDMOD
			nReturn := nNecessPai / nReturn
		Else
			nReturn := nNecessPai * nReturn
		EndIf
	EndIf
Return nReturn

/*/{Protheus.doc} produtoMOD
Indica se o produto � MOD
@author    brunno.costa
@since     28/07/2020
@version   1
@param 01, cProduto, caracter, codigo do produto
@param 02, oDados  , objeto  , instancia da camada de dados
@return lMOD, logico, indica se o produto � MOD
/*/
METHOD produtoMOD(cProduto, oDados) CLASS MrpDominio_MOD
	Local lMOD := oDados:oProdutos:getFlag("|MOD|"+cProduto+"|")
	If lMOD == Nil
		lMOD := .F.
	EndIf
Return lMOD
