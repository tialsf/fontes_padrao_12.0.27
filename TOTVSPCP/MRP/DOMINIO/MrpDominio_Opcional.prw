#INCLUDE 'protheus.ch'
#INCLUDE 'MRPDominio.ch'

Static snTamCod    := 90

Static sOPC_KEY      := 1
Static sOPC_KEY2     := 2
Static sOPC_OPCION   := 3
Static sOPC_ID       := 4
Static sOPC_IDPAI    := 5
Static sOPC_IDMASTER := 6
Static sOPC_T4JRECNO := 7
Static sOPC_T4QRECNO := 8
Static sTAM_OPC      := 8

Static sPRD_ESTSEG := 3
Static sPRD_SLDDIS := 6
Static sPRD_NIVEST := 7
Static sPRD_CHAVE2 := 8
Static sPRD_IDOPC  := 14

/*/{Protheus.doc} MrpDominio_Opcional
Regras de negocio MRP - Produtos Opcionais
@author    brunno.costa
@since     14/05/2019
@version   1
/*/
CLASS MrpDominio_Opcional FROM LongClassName

	DATA oDados AS OBJECT

	METHOD new(oDados) CONSTRUCTOR
	METHOD adicionaLista(cProduto, cOpcional)
	METHOD converteJsonEmID(cJson)
	METHOD copiaProduto(cProduto, cIDOpc)
	METHOD criaIDsIntermediarios(oJson, cIDMaster)
	METHOD insereProdutosOpcionais()
	METHOD retornaChave(cChave2)
	METHOD retorna2Chave(cChave)
	METHOD retornaIDComponente(cComponente, cIDOpcPai)
	METHOD retornaIDMaster(cJson)
	METHOD selecionado(cIDOpcSel, cGrupo, cItem)
	METHOD montaT4SPath(cIDRegistro, cPathOrig)
	METHOD montaT4QPath(cIDRegistro, cPathOrig)

ENDCLASS

/*/{Protheus.doc} new
Metodo construtor
@author    brunno.costa
@since     14/05/2019
@version   1
@param 01 - oDados, objeto, instancia da classe de dados
@return Self, objeto, instancia da classe
/*/
METHOD new(oDados) CLASS MrpDominio_Opcional
	::oDados := oDados
Return Self

/*/{Protheus.doc} converteJsonEmID
Converte Json despadronizado em ID Master - ID da demanda na Matriz
@author    brunno.costa
@since     14/05/2019
@version   1
@param 01 - cJsonOrig , caracter, string Json de opcionais - despadronizado
@parma 02 - lOrigemCSV, l�gico  , indica chamada atrav�s de CSV
@param 03 - nRecnoT4J , n�mero  , c�digo do RECNO referente registro na T4J quando existir MEMO Opcional desta origem
@param 04 - nRecnoT4Q , n�mero  , c�digo do RECNO referente registro na T4Q quando existir MEMO Opcional desta origem
@param 05 - cPathOrig , caracter, string com o path do opcional atual para pesquisa do cIDInterme (ID intermedi�rio)
@param 06 - cIDInterme, caracter, utilizado em conjunto com os par�metros 05 e 06 para retornar por referencia o ID do intermediario
@return cReturn, caracter, ID Master referente a string JSON
/*/
METHOD converteJsonEmID(cJsonOrig, lOrigemCSV, nRecnoT4J, nRecnoT4Q, cPathOrig, cIDInterme) CLASS MrpDominio_Opcional
	Local cJson
	Local cReturn := ""
	Local oJson

	Default lOrigemCSV := .F.
	Default nRecnoT4J  := 0
	Default nRecnoT4Q  := 0

	If !Empty(cJsonOrig)
		oJson := JsonObject():new()

		//Padroniza string recebida
		If lOrigemCSV
			cJsonOrig := '{"OPTIONAL": ' + cJsonOrig + '}'
		EndIf

		oJson:fromJson(cJsonOrig)
		cJson := oJson:toJson()

		cReturn := ::retornaIDMaster(cJson)

		::criaIDsIntermediarios(oJson, cReturn, lOrigemCSV, nRecnoT4J, nRecnoT4Q, cPathOrig, @cIDInterme)

	EndIf

Return cReturn

/*/{Protheus.doc} retornaIDMaster
Retorna ID Master com base em cJson padronizado - ID da demanda na Matriz
@author    brunno.costa
@since     14/05/2019
@version   1
@param 01 - cJsonOrig, caracter, string Json de opcionais - padronizado
@return cReturn, caracter, ID Master referente a string JSON
/*/
METHOD retornaIDMaster(cJson) CLASS MrpDominio_Opcional

	Local lError  := .F.
	Local cReturn := ::oDados:oJsonOpcionais:getnKey(1, cJson, @lError, .F.)

	If cReturn == Nil
		If ::oDados:oJsonOpcionais:addKey(1, cJson, @lError, .F., Nil, @cReturn)
			cReturn := "M" + cValToChar(cReturn)
		Else
			cReturn := ""
		EndIf

	Else
		cReturn := "M" + cValToChar(cReturn)
	EndIf

Return cReturn

/*/{Protheus.doc} criaIDsIntermediarios
Cria os ID's intermediarios para os Json recebidos
Estes ID's diferenciam registros sem opcionais na matriz do MRP dos registros oriundos de Opcionais
@author    brunno.costa
@since     14/05/2019
@version   1
@param 01 - oJson     , objeto  , instancia de objeto Json de Opcionais
@param 02 - cIDMaster , caracter, ID Master para criar os IDs intermediarios
@param 03 - lOrigemCSV, l�gico  , indica origem de CSV
@param 04 - nRecnoT4J , n�mero  , c�digo do RECNO referente registro na T4J quando existir MEMO Opcional desta origem
@param 05 - nRecnoT4Q , n�mero  , c�digo do RECNO referente registro na T4Q quando existir MEMO Opcional desta origem
@param 06 - cPathOrig , caracter, string com o path do opcional atual para pesquisa do cIDInterme (ID intermedi�rio)
@param 07 - cIDInterme, caracter, utilizado em conjunto com o par�metro 06 para retornar por referencia o ID do intermediario
/*/
METHOD criaIDsIntermediarios(oJson, cIDMaster, lOrigemCSV, nRecnoT4J, nRecnoT4Q, cPathOrig, cIDInterme) CLASS MrpDominio_Opcional

	Local aRegistro
	Local aPathArray  := {}
	Local cChave2
	Local cChave
	Local cFilAux     := ""
	Local cIDPai      := ""
	Local cNewPath    := ""
	Local cPath
	Local cOpcionais
	Local cProduto
	Local nOpcionais
	Local nIndOpc
	Local nIndPath
	Local nPathProds
	Local oOpcionais  := ::oDados:oOpcionais
	Local lAddRow     := .F.
	Local nPosAux     := 0

	Default cIDInterme := ""

	nOpcionais := Len(oJson["OPTIONAL"])
	For nIndOpc := 1 to nOpcionais
		cPath      := oJson["OPTIONAL"][nIndOpc]["key"]
		cOpcionais := oJson["OPTIONAL"][nIndOpc]["value"]
		aPathArray := StrTokArr(cPath, "|")
		nPathProds := Len(aPathArray)
		cNewPath   := ""
		nPosAux    := 0

		If lOrigemCSV
			cOpcionais := StrTran(cOpcionais, ",", ";")
		EndIf

		//Percorre produtos do Path
		For nIndPath := 1 to nPathProds
			cIDPai   := ::oDados:retornaCampo("OPC", 1, cIDMaster + Iif(!Empty(cNewPath), "|" + cNewPath, ""), "OPC_ID")

			If Empty(cNewPath)
				cNewPath += RTrim(aPathArray[nIndPath])
			Else
				cNewPath += "|" + RTrim(aPathArray[nIndPath])
			EndIf

			If lOrigemCSV
				cNewPath := StrTran(cNewPath, ",", ";")
			EndIf

			cChave := cIDMaster + "|" + cNewPath                 //Chave primaria

			oOpcionais:trava(cChave)
			If !oOpcionais:getRow(1, cChave, Nil, @aRegistro, .F., .F.)
				If Empty(cIDPai)
					cIDPai    := ""
					cID       := cIDMaster
					cChave2   := cFilAux +  cIDMaster              //Chave estrangeira tabela Matriz MRP
				Else
					cID       := "I" + cValToChar(oOpcionais:getRowsNum(.F.) + 1)
					cChave2   := cFilAux +  cIDMaster + "|" +  cID //Chave estrangeira tabela Matriz MRP
					nRecnoT4J := 0
					nRecnoT4Q := 0
				EndIf

				aRegistro                := Array(sTAM_OPC)
				aRegistro[sOPC_KEY]      := cChave
				aRegistro[sOPC_KEY2]     := cChave2
				aRegistro[sOPC_OPCION]   := Iif(nIndPath == nPathProds,  cOpcionais, "")
				aRegistro[sOPC_ID]       := cID
				aRegistro[sOPC_IDPAI]    := cIDPai
				aRegistro[sOPC_IDMASTER] := cIDMaster
				aRegistro[sOPC_T4JRECNO] := nRecnoT4J
				aRegistro[sOPC_T4QRECNO] := nRecnoT4Q

				lAddRow := .T.

			Else
				If Empty(aRegistro[sOPC_OPCION])
					aRegistro[sOPC_OPCION] := Iif(nIndPath == nPathProds,  cOpcionais, "")
					lAddRow := .T.
				ElseIf !(cOpcionais$aRegistro[sOPC_OPCION])
					aRegistro[sOPC_OPCION] += Iif(nIndPath == nPathProds,  "|" + cOpcionais, "")
					lAddRow := .T.
				EndIf

			EndIf

			//Identifica o ID do Opcional Intermediario
			If cPathOrig != Nil;
			   .AND. Empty(cIDInterme);
			   .AND. "|" + AllTrim(cPathOrig) + "|" == "|" + AllTrim(cNewPath) + "|"
				cIDInterme := aRegistro[sOPC_KEY2]
			EndIf

			If lAddRow
				If lOrigemCSV
					cProduto := StrTokArr(aPathArray[nIndPath], ",")[1]
				Else
					cProduto := StrTokArr(aPathArray[nIndPath], ";")[1]
				EndIf
				::adicionaLista(PadR(cProduto, snTamCod), cChave2)

				If !oOpcionais:addRow(cChave, aRegistro, .F., .F., cChave2)
					::oDados:oLogs:log(STR0088, "E") //"Falha durante a criacao de ID's de opcionais intermediarios."
				EndIf
			EndIf
			oOpcionais:destrava(cChave)
		Next

	Next nInd

Return

/*/{Protheus.doc} retornaChave
Retorna a chave com base na chave 2
@author    brunno.costa
@since     14/05/2019
@version   1
@param 01 - cChave2, caracter, string referente chave extrangeira tabela Matriz MRP - Calculo (ID Master + ID)
@return cChave, caracter, chave primaria da tabela de opcionais (ID Master + Path)
/*/
METHOD retornaChave(cChave2) CLASS MrpDominio_Opcional
Return ::oDados:retornaCampo("OPC", 2, cChave2, "OPC_KEY")

/*/{Protheus.doc} retornaChave2
Retorna a chave 2 (chave extrangeira tabela Matriz MRP - Calculo) com base na chave
@author    brunno.costa
@since     14/05/2019
@version   1
@param 01 - cChave, caracter, chave primaria da tabela de opcionais (ID Master + Path)
@return cChave2, caracter, string referente chave extrangeira tabela Matriz MRP - Calculo (ID Master + ID)
/*/
METHOD retorna2Chave(cChave) CLASS MrpDominio_Opcional
Return ::oDados:retornaCampo("OPC", 1, cChave, "OPC_KEY2")

/*/{Protheus.doc} retornaIDComponente
Retorna cChave2 do componente com base no ID Opcional do pai
@author    brunno.costa
@since     14/05/2019
@version   1
@param 01 - cIDOpcPai  , caracter, ID Opcional do produto pai
@param 02 - cComponente, caracter, codigo do componente
@param 03 - cTRT       , caracter, TRT do componente
@return cChave2, caracter, string referente chave extrangeira tabela Matriz MRP - Calculo (ID Master + ID)
/*/
METHOD retornaIDComponente(cIDOpcPai, cComponente, cTRT) CLASS MrpDominio_Opcional
	Local cPathPai
	Local cReturn  := ""
	Default cTRT := ""
	If !Empty(cIDOpcPai)
		cPathPai := ::retornaChave(cIDOpcPai)
		If !Empty(cPathPai)
			cReturn  := ::retorna2Chave(cPathPai + "|" + RTrim(cComponente) + ";" + AllTrim(cTRT))
		EndIf
	EndIf
Return cReturn

/*/{Protheus.doc} selecionado
Identifica se o opcional esta selecionado
@author    brunno.costa
@since     14/05/2019
@version   1
@param 01 - cIDOpcSel, caracter, ID do opcional selecionado
@param 02 - cGrupo   , caracter, grupo de opcionais da SG1
@param 03 - cItem    , caracter, item opcionais da SG1
@return lReturn, logico, indica se o opcional esta selecionado
/*/
METHOD selecionado(cIDOpcSel, cGrupo, cItem) CLASS MrpDominio_Opcional
	Local cSelecoes   := ""
	Local lReturn     := .F.

	If !Empty(cIDOpcSel)
		cSelecoes   := ::oDados:retornaCampo("OPC", 2, cIDOpcSel, "OPC_OPCION")
		lReturn     := !Empty(cSelecoes) .AND. ((cGrupo + ";" + cItem) $ cSelecoes)
	EndIf

Return lReturn

/*/{Protheus.doc} adicionaLista
Adiciona ID de opcional a lista de processamento dos produtos opcionais
@author    brunno.costa
@since     14/05/2019
@version   1
@param 01 - cProduto , caracter, codigo do produto
@param 02 - cIDOpc   , caracter, ID do opcional
/*/
METHOD adicionaLista(cProduto, cIDOpc) CLASS MrpDominio_Opcional
	Local oOpcionais := ::oDados:oOpcionais
	If !oOpcionais:existList("OPC_" + cProduto)
		oOpcionais:createList("OPC_" + cProduto)
	EndIf
	oOpcionais:setItemList("OPC_" + cProduto, cIDOpc, "")

	If !oOpcionais:existList("OPC_Produtos_")
		oOpcionais:createList("OPC_Produtos_")
	EndIf
	oOpcionais:setItemList("OPC_Produtos_", cProduto, "")
Return

/*/{Protheus.doc} insereProdutosOpcionais
Insere os produtos opcionais no cadastro de produtos
@author    brunno.costa
@since     14/05/2019
@version   1
/*/
METHOD insereProdutosOpcionais() CLASS MrpDominio_Opcional
	Local aProdsOPC  := {}
	Local aIDsOPC    := {}
	Local cIDOpc     := ""
	Local cProduto   := ""
	Local lError     := .F.
	Local nIndProd   := 0
	Local nIndIDs    := 0
	Local nProdsOPC  := 0
	Local nIDsOPC    := 0
	Local oOpcionais := ::oDados:oOpcionais

	If oOpcionais:existList("OPC_Produtos_" + cProduto)
		oOpcionais:getAllList("OPC_Produtos_", aProdsOPC, @lError)
		nProdsOPC := Len(aProdsOPC)

		For nIndProd := 1 to nProdsOPC
			cProduto := PadR(aProdsOPC[nIndProd][1], snTamCod)
			aIDsOPC  := {}
			oOpcionais:getAllList("OPC_" + cProduto, aIDsOPC, @lError)
			nIDsOPC  := Len(aIDsOPC)

			For nIndIDs := 1 to nIDsOPC

				cIDOpc := aIDsOPC[nIndIDs][1]
				::copiaProduto(cProduto, cIDOpc)

			Next nIndIDs

		Next nIndProd

		oOpcionais:order(2, @lError) //Reordena
	EndIf

Return

/*/{Protheus.doc} copiaProduto
Copia o cadastro na tabela de produtos
@author    brunno.costa
@since     14/05/2019
@version   1
@param 01 - cProduto, caracter, codigo do produto
@param 02 - cIDOpc  , caracter, ID do opcional relacionado ao produto novo
/*/
METHOD copiaProduto(cProduto, cIDOpc) CLASS MrpDominio_Opcional

	Local lError    := .F.
	Local lReturn   := .T.
	Local aProduto  := ::oDados:retornaLinha("PRD", 1, cProduto, @lError, .F.)

	If !lError
		aProduto[sPRD_ESTSEG] := 0
		aProduto[sPRD_SLDDIS] := 0
		aProduto[sPRD_IDOPC]  := cIDOpc
		aProduto[sPRD_CHAVE2] := aProduto[sPRD_CHAVE2] + "|" + cIDOpc

		::oDados:gravaLinha("PRD", 1, cProduto + "|" + cIDOpc, aProduto, @lError, .F., .T.) //Inclui copia
		lReturn := !lError

		::oDados:oProdutos:setflag("nProdutosN" + aProduto[sPRD_NIVEST], 1, .F., .F., .T.) //incrementa
		::oDados:oProdutos:setflag("nProdCalcN" + aProduto[sPRD_NIVEST], 0, .F., .F.)
	EndIf

Return lReturn

/*/{Protheus.doc} montaT4SPath
Monta Path da T4S e Identifica a string do Opcional Selecionada
@author    brunno.costa
@since     21/01/2020
@version   1
@param 01 - cIDRegistro, caracter, ID do registro na T4S
@param 02 - cComponente, caracter, codigo do componente
@param 03 - aPathOrig  , caracter, retorna por referencia os paths do registro
/*/
METHOD montaT4SPath(cIDRegistro, cComponente, aPathOrig) CLASS MrpDominio_Opcional

	Local cAliasTop    := GetNextAlias()
	Local cBanco       := TCGetDB()

	cQuery := " WITH MOUNTPATH(ULTIDREG, T4Q_PROD, T4S_PROD, T4S_OPORIG, T4S_QTD, T4S_QSUSP, "
	cQuery += "      T4S_SEQ, "
	cQuery +=      " ULTOP, T4Q_OP, ULTOPC, ULTPROD, CODE_PATH, NIVEL) "
	cQuery +=      " AS (SELECT T4S_IDREG                              AS ULTIDREG, "
	cQuery +=                 " T4Q_PROD, "
	cQuery +=                 " T4S_PROD, "
	cQuery +=                 " T4S_OPORIG, "
	cQuery +=                 " T4S_QTD, "
	cQuery +=                 " T4S_QSUSP, "
	cQuery +=                 " T4S_SEQ, "
	cQuery +=                 " T4Q_OP                                 AS ULTOP, "
	cQuery +=                 " T4Q_OP, "
	cQuery +=                 " T4Q_ERPOPC                             AS ULTOPC, "
	cQuery +=                 " T4Q_PROD                               AS ULTPROD, "
	cQuery +=                 " Cast(Rtrim(T4Q_PROD) || '|' || Rtrim(T4S_PROD) || ';' "
	cQuery +=                      " || Rtrim(T4S_SEQ) AS VARCHAR(8000)) AS CODE_PATH, "
	cQuery +=                 " 1                                      AS NIVEL "
	cQuery +=          " FROM   " + RetSqlName( "T4Q" ) + " T4Q "
	cQuery +=                 " INNER JOIN " + RetSqlName( "T4S" ) + " T4S "
	cQuery +=                         " ON T4Q_OP = T4S_OP "
	cQuery +=          " WHERE  T4Q.D_E_L_E_T_ = ' ' "
	cQuery +=                 " AND T4S.D_E_L_E_T_ = ' ' "
	cQuery +=                 " AND T4Q.T4Q_OPPAI = ' ' "
	cQuery +=                 " AND T4Q.T4Q_FILIAL = '"+xFilial("T4Q")+"' "
	cQuery +=                 " AND T4S.T4S_FILIAL = '"+xFilial("T4S")+"' "
	cQuery +=          " UNION ALL "
	cQuery +=          " SELECT MOUNTPATH.ULTIDREG, "
	cQuery +=                 " T4QS.T4Q_PROD, "
	cQuery +=                 " T4QS.T4S_PROD, "
	cQuery +=                 " T4QS.T4S_OPORIG, "
	cQuery +=                 " T4QS.T4S_QTD, "
	cQuery +=                 " T4QS.T4S_QSUSP, "
	cQuery +=                 " T4QS.T4S_SEQ, "
	cQuery +=                 " T4QS.T4Q_OP                                       AS ULTOP, "
	cQuery +=                 " T4QS.T4Q_OP, "
	cQuery +=                 " MOUNTPATH.ULTOPC                                  AS ULTOPC, "
	cQuery +=                 " T4QS.T4Q_PROD                                     AS ULTPROD, "
	cQuery +=                 " Cast(Rtrim(CODE_PATH) || '|' || Rtrim(T4QS.T4S_PROD) "
	cQuery +=                      " || ';' || Rtrim(T4QS.T4S_SEQ) AS VARCHAR(8000)) AS CODE_PATH, "
	cQuery +=                 " MOUNTPATH.NIVEL + 1                               AS NIVEL "
	cQuery +=          " FROM   (SELECT T4Q_PROD, "
	cQuery +=                         " T4S_PROD, "
	cQuery +=                         " T4S_QTD, "
	cQuery +=                         " T4S_QSUSP, "
	cQuery +=                         " T4S_OPORIG, "
	cQuery +=                         " T4S_IDREG, "
	cQuery +=                         " T4S_SEQ, "
	cQuery +=                         " T4Q_OPPAI, "
	cQuery +=                         " T4Q_OP, "
	cQuery +=                         " T4Q_ERPOPC "
	cQuery +=                  " FROM   " + RetSqlName( "T4Q" ) + " T4Q "
	cQuery +=                         " INNER JOIN " + RetSqlName( "T4S" ) + " T4S "
	cQuery +=                                 " ON T4Q_OP = T4S_OP "
	cQuery +=                  " WHERE  T4Q.D_E_L_E_T_ = ' ' "
	cQuery +=                         " AND T4S.D_E_L_E_T_ = ' ' "
	cQuery +=                         " AND T4Q.T4Q_FILIAL = '"+xFilial("T4Q")+"' "
	cQuery +=                         " AND T4S.T4S_FILIAL = '"+xFilial("T4S")+"' ) [AS T4QS] "
	cQuery +=                 " INNER JOIN MOUNTPATH "
	cQuery +=                         " ON MOUNTPATH.T4S_OPORIG = T4QS.T4Q_OP) "
	cQuery += " SELECT CODE_PATH, "
	If !("|2|" $  ::oDados:oParametros["cDocumentType"])
		cQuery += " T4S_QTD - T4S_QSUSP QTD "
	Else
		cQuery += " T4S_QTD QTD "
	EndIf
	cQuery += " FROM   MOUNTPATH "
	cQuery +=        " INNER JOIN (SELECT ULTIDREG, "
	cQuery +=                           " Min(NIVEL) AS MINNIVEL "
	cQuery +=                    " FROM   MOUNTPATH "
	cQuery +=                    " WHERE  MOUNTPATH.ULTIDREG = '" + cIDRegistro + "'"
	cQuery +=                    " GROUP  BY MOUNTPATH.ULTIDREG) MINNIVEL "
	cQuery +=                " ON MINNIVEL.MINNIVEL = MOUNTPATH.NIVEL "
	cQuery +=                   " AND MINNIVEL.ULTIDREG = MOUNTPATH.ULTIDREG   " "

	//Realiza ajustes da Query para cada banco
	If cBanco == "POSTGRES"

		//Altera sintaxe da clausula WITH
		cQuery := StrTran(cQuery, 'WITH ', 'WITH recursive ')

		//Medida paliativa banco POSTGRES. Banco suporta VarChar(8000), entretanto DbAccess com PostGres funciona em bases desatualizadas
		//cQuery := StrTran(cQuery,"VarChar(8000)","VarChar(255)")

		//Corrige Falhas internas de Bin�rio - POSTGRES
		cQuery := StrTran(cQuery, CHR(13), " ")
		cQuery := StrTran(cQuery, CHR(10), " ")
		cQuery := StrTran(cQuery, CHR(09), " ")

	ElseIf "MSSQL" $ cBanco
		//Substitui a fun��o Trim
		cQuery := StrTran(cQuery, "Trim(", "RTrim(")
		//Substitui concatena��o || por +
		cQuery := StrTran(cQuery, '||', '+')

	ElseIf cBanco != "ORACLE"
		//Substitui concatena��o || por +
		cQuery := StrTran(cQuery, '||', '+')
	EndIf

	If cBanco == "ORACLE"
		cQuery := StrTran(cQuery,"VARCHAR(8000)","VARCHAR(4000)")
		cQuery := StrTran(cQuery,"[AS T4QS]","T4QS")
	Else
		cQuery := StrTran(cQuery,"[AS T4QS]","AS T4QS")
	EndIf

	dbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAliasTop, .T., .T.)
	While !(cAliasTop)->(Eof())
		aAdd(aPathOrig, {AllTrim((cAliasTop)->CODE_PATH), (cAliasTop)->QTD})
		(cAliasTop)->(DbSkip())
	EndDo
	(cAliasTop)->(dbCloseArea())

Return

/*/{Protheus.doc} montaT4QPath
Monta Path da T4D e Identifica a string do Opcional Selecionada
@author    brunno.costa
@since     21/01/2020
@version   1
@param 01 - cIDRegistro, caracter, codigo do registro na T4Q
@param 02 - cOP        , caracter, codigo da OP
@param 03 - aPathOrig  , caracter, retorna por referencia os paths do registro
/*/
METHOD montaT4QPath(cIDRegistro, cOP, aPathOrig) CLASS MrpDominio_Opcional

	Local cAliasTop    := GetNextAlias()
	Local cBanco       := TCGetDB()

	cQuery := " WITH MOUNTPATH(ULTIDREG, T4Q_PROD, T4S_PROD, T4S_QTD, T4Q_QUANT, T4S_SEQ, ULTOP, "
	cQuery += "      T4Q_OP, T4S_OPORIG, ULTOPC, ULTPROD, CODE_PATH, NIVEL) "
	cQuery +=      " AS (SELECT T4Q_IDREG                              AS ULTIDREG, "
	cQuery +=                 " T4Q_PROD, "
	cQuery +=                 " T4S_PROD, "
	cQuery +=                 " T4Q_QUANT, "
	cQuery +=                 " T4Q_QUANT, "
	cQuery +=                 " T4S_SEQ, "
	cQuery +=                 " T4Q_OP                                 AS ULTOP, "
	cQuery +=                 " T4Q_OP, "
	cQuery +=                 " T4S_OPORIG, "
	cQuery +=                 " T4Q_ERPOPC                             AS ULTOPC, "
	cQuery +=                 " T4Q_PROD                               AS ULTPROD, "
	cQuery +=                 " Cast(Rtrim(T4Q_PROD) AS VARCHAR(8000)) AS CODE_PATH, "
	cQuery +=                 " 1                                      AS NIVEL "
	cQuery +=          " FROM   " + RetSqlName( "T4Q" ) + " T4Q "
	cQuery +=                 " INNER JOIN " + RetSqlName( "T4S" ) + " T4S "
	cQuery +=                         " ON T4Q_OP = T4S_OP "
	cQuery +=          " WHERE  T4Q.D_E_L_E_T_ = ' ' "
	cQuery +=                 " AND T4S.D_E_L_E_T_ = ' ' "
	cQuery +=                 " AND T4Q.T4Q_OPPAI = ' ' "
	cQuery +=                 " AND T4Q.T4Q_FILIAL = '"+xFilial("T4Q")+"' "
	cQuery +=                 " AND T4S.T4S_FILIAL = '"+xFilial("T4S")+"' "
	cQuery +=          " UNION ALL "
	cQuery +=          " SELECT T4QS.T4Q_IDREG, "
	cQuery +=                 " T4QS.T4Q_PROD, "
	cQuery +=                 " T4QS.T4S_PROD, "
	cQuery +=                 " MOUNTPATH.T4S_QTD, "
	cQuery +=                 " T4QS.T4Q_QUANT, "
	cQuery +=                 " T4QS.T4S_SEQ, "
	cQuery +=                 " T4QS.T4Q_OP                                       AS ULTOP, "
	cQuery +=                 " T4QS.T4Q_OP, "
	cQuery +=                 " T4QS.T4S_OPORIG, "
	cQuery +=                 " MOUNTPATH.ULTOPC                                  AS ULTOPC, "
	cQuery +=                 " T4QS.T4Q_PROD                                     AS ULTPROD, "
	cQuery +=                 " Cast(Rtrim(CODE_PATH) || '|' "
	cQuery +=                      " || Rtrim(MOUNTPATH.T4S_PROD) || ';' "
	cQuery +=                      " || Rtrim(MOUNTPATH.T4S_SEQ) AS VARCHAR(8000)) AS CODE_PATH, "
	cQuery +=                 " MOUNTPATH.NIVEL + 1                               AS NIVEL "
	cQuery +=          " FROM   (SELECT T4Q_PROD, "
	cQuery +=                         " T4S_PROD, "
	cQuery +=                         " T4S_QTD, "
	cQuery +=                         " T4S_IDREG, "
	cQuery +=                         " T4S_SEQ, "
	cQuery +=                         " T4Q_OPPAI, "
	cQuery +=                         " T4Q_OP, "
	cQuery +=                         " T4Q_ERPOPC, "
	cQuery +=                         " T4Q_IDREG, "
	cQuery +=                         " T4Q_QUANT, "
	cQuery +=                         " T4S_OPORIG "
	cQuery +=                  " FROM   " + RetSqlName( "T4Q" ) + " T4Q "
	cQuery +=                         " INNER JOIN " + RetSqlName( "T4S" ) + " T4S "
	cQuery +=                                 " ON T4Q_OP = T4S_OP "
	cQuery +=                  " WHERE  T4Q.D_E_L_E_T_ = ' ' "
	cQuery +=                         " AND T4S.D_E_L_E_T_ = ' ' "
	cQuery +=                         " AND T4Q_IDREG = '" + cIDRegistro + "' "
	cQuery +=                         " AND T4Q.T4Q_FILIAL = '"+xFilial("T4Q")+"' "
	cQuery +=                         " AND T4S.T4S_FILIAL = '"+xFilial("T4S")+"' ) [AS T4QS] "
	cQuery +=                 " INNER JOIN MOUNTPATH "
	cQuery +=                         " ON MOUNTPATH.T4S_OPORIG = T4QS.T4Q_OP) "
	cQuery += " SELECT DISTINCT CODE_PATH, "
	cQuery +=        " T4Q_QUANT "
	cQuery += " FROM   MOUNTPATH "
	cQuery +=        " INNER JOIN (SELECT ULTIDREG, "
	cQuery +=                           " Max(NIVEL) AS MAXNIVEL "
	cQuery +=                    " FROM   MOUNTPATH "
	cQuery +=                    " WHERE  MOUNTPATH.ULTIDREG = '" + cIDRegistro + "' "
	cQuery +=                    " GROUP  BY MOUNTPATH.ULTIDREG) MAXNIVEL "
	cQuery +=                " ON MAXNIVEL.MAXNIVEL = MOUNTPATH.NIVEL "
	cQuery +=                   " AND MAXNIVEL.ULTIDREG = MOUNTPATH.ULTIDREG  " "

	//Realiza ajustes da Query para cada banco
	If cBanco == "POSTGRES"

		//Altera sintaxe da clausula WITH
		cQuery := StrTran(cQuery, 'WITH ', 'WITH recursive ')

		//Medida paliativa banco POSTGRES. Banco suporta VarChar(8000), entretanto DbAccess com PostGres funciona em bases desatualizadas
		//cQuery := StrTran(cQuery,"VarChar(8000)","VarChar(255)")

		//Corrige Falhas internas de Bin�rio - POSTGRES
		cQuery := StrTran(cQuery, CHR(13), " ")
		cQuery := StrTran(cQuery, CHR(10), " ")
		cQuery := StrTran(cQuery, CHR(09), " ")

	ElseIf cBanco == "MSSQL"
		//Substitui a fun��o Trim
		cQuery := StrTran(cQuery, "Trim(", "RTrim(")
		//Substitui concatena��o || por +
		cQuery := StrTran(cQuery, '||', '+')

	ElseIf cBanco != "ORACLE"
		//Substitui concatena��o || por +
		cQuery := StrTran(cQuery, '||', '+')
	EndIf

	If cBanco == "ORACLE"
		cQuery := StrTran(cQuery,"VARCHAR(8000)","VARCHAR(4000)")
		cQuery := StrTran(cQuery,"[AS T4QS]","T4QS")
	Else
		cQuery := StrTran(cQuery,"[AS T4QS]","AS T4QS")
	EndIf

	dbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAliasTop, .T., .T.)
	If !(cAliasTop)->(Eof())
		aAdd(aPathOrig, {AllTrim((cAliasTop)->CODE_PATH), (cAliasTop)->T4Q_QUANT})
		//(cAliasTop)->(DbSkip())
	EndIf
	(cAliasTop)->(dbCloseArea())

Return