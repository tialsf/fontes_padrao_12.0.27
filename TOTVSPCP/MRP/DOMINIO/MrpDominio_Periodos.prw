#INCLUDE 'protheus.ch'
#INCLUDE 'MRPDominio.ch'

/*/{Protheus.doc} MrpDominio_Periodos
Regras de negocio MRP - Periodos
@author    marcelo.neumann
@since     10/06/2019
@version   1
/*/
CLASS MrpDominio_Periodos FROM LongClassName

	DATA oDominio      AS OBJECT
	DATA oDatasUteis   AS OBJECT
	DATA oStatus       AS OBJECT
	DATA nPrimeiroUtil AS NUMERIC
	DATA dPrimeiraUtil AS DATE
	DATA dUltimaData   AS DATE

	METHOD new() CONSTRUCTOR
	METHOD criarArrayPeriodos()
	METHOD buscaPeriodoDaData()
	METHOD buscaProximoDiaUtil()
	METHOD buscaDataPeriodo(dData, nTipo, dInicio)
	METHOD proximaData(dData, nTipo, dInicio)
	METHOD primeiroPeriodoUtil()
	METHOD primeiraDataUtil()
	METHOD ultimaDataDoMRP()
	METHOD verificaDataUtil(dData, lError)

ENDCLASS

/*/{Protheus.doc} new
Metodo construtor
@author    marcelo.neumann
@since     10/06/2019
@version   1
@param 01 - oDominio, numero  , objeto da camada de dominio
/*/
METHOD new(oDominio) CLASS MrpDominio_Periodos
	::oDominio      := oDominio
	::oDatasUteis   := JsonObject():New()
	::nPrimeiroUtil := 0
	::dPrimeiraUtil := Nil
	::dUltimaData   := Nil
	::oStatus       := MrpDados_Status():New(oDominio:oParametros["ticket"])

Return Self

/*/{Protheus.doc} criarArrayPeriodos
Gera array de periodos
@author    marcelo.neumann
@since     25/04/2019
@version 1.0
@param 01 - dData    , data  , indica a data inicial
@param 02 - nTipo    , numero, indica o tipo de periodo a ser utilizado
@param 03 - nPeriodos, numero, indica a quantidade de periodos a ser utilizada
/*/
METHOD criarArrayPeriodos(dData, nTipo, nPeriodos) CLASS MrpDominio_Periodos

	Local cErro       := ""
	Local cUteis      := ""
	Local dInicio     := Nil
	Local dDtLimite   := Nil
	Local nInd        := 0
	Local lError      := .F.
	Local lExistUtil  := .F.

	Default dData     := dDataBase
	Default nTipo     := 1
	Default nPeriodos := 30

	dInicio := dData

	::oDominio:aPeriodos := {}

	//Calcula a primeira data a ser verificada
	If nTipo <> 1
		dData := Self:buscaDataPeriodo(dData, nTipo, dInicio)
	EndIf

	//Carrega os demais per�odos
	For nInd := 1 To (nPeriodos)
		aAdd(::oDominio:aPeriodos, dData)

		::oDominio:oLogs:log(STR0018 + cValToChar(nInd) + " - " + DtoS(dData), "21") //"Periodo '"

		dData := ::proximaData(dData, nTipo, dInicio)
	Next nInd

	//Se utiliza o calend�rio, valida se todas as datas existem no calend�rio
	If Self:oDominio:oParametros["nLeadTime"] <> 1
		dData     := ::oDominio:aPeriodos[1]
		dDtLimite := ::ultimaDataDoMRP()

		While dData <= dDtLimite
			cUteis := Self:oDominio:oDados:retornaCampo("CAL", 1, DToS(dData), "CAL_UTEIS", @lError)

			//Retorna erro se a data n�o existir no calend�rio
			If lError
				cErro := STR0099 + DToC(dData) + STR0100 //"Data " XX/XX/XX " nao encontrada no calendario do MRP."
				Self:oDominio:oDados:oLogs:log(cErro, "21")
				Self:oStatus:gravaErro("memoria", cErro)
				Exit
			ElseIf cUteis <> "00:00"
				lExistUtil := .T.
			EndIf
			dData++
   		Enddo

		If !lExistUtil .And. !lError
			cErro := STR0141 + DToC(::oDominio:aPeriodos[1]) + STR0142 + DToC(dDtLimite) + "." //"N�o existe dia �til no per�odo de processamento informado: " + XX/XX/XX + " at� " + XX/XX/XX
			Self:oDominio:oDados:oLogs:log(cErro, "21")
			Self:oStatus:gravaErro("memoria", cErro)
		EndIf
	EndIf

Return

/*/{Protheus.doc} buscaDataPeriodo
Identifica qual � a data que deve ser utilizada no per�odo, considerando o calend�rio.
Utilizada para quando os per�odos do MRP n�o s�o do tipo Di�rio.
M�todo utilizado durante a cria��o do aPeriodos.

@author lucas.franca
@since 10/10/2019
@version 1.0
@param 01 - dData  , date   , Data atual que est� sendo considerada.
@param 02 - nTipo  , numeric, Tipo do processamento do MRP.
@param 03 - dInicio, date   , Data inicial do MRP
@return     dData  , date   , Data que dever� ser considerada para o per�odo.
/*/
METHOD buscaDataPeriodo(dData, nTipo, dInicio) CLASS MrpDominio_Periodos
	Local dDataAux := Nil

	//Somente processa caso utilize calend�rio e a data atual n�o � �til.
	If Self:oDominio:oParametros["nLeadTime"] != 1 .And. !Self:verificaDataUtil(dData)
		dDataAux := Self:buscaProximoDiaUtil(dData)
		If nTipo == 2 //Semanal
			//Se a pr�xima data �til for diferente da data atual, ir� verificar se as duas datas pertencem a mesma semana.
			If dData != dDataAux
				//Faz a conta para compara o 1� dia da semana das duas datas.
				//Se o 1� dia da semana das duas datas forem iguais, significa que as duas datas pertencem a mesma semana.
				If (dData - (Dow(dData) - 1)) == (dDataAux - (Dow(dDataAux) - 1))
					//A pr�xima data �til pertence a mesma semana da data original.
					//Ir� criar o per�odo utilizando a data �til encontrada.
					dData := dDataAux
				Else
					//A pr�xima data �til n�o pertence a mesma semana da data original.
					//Ir� criar o per�odo utilizando o primeiro dia da semana, ou a data inicial do MRP.
					dDataAux := dData - (Dow(dData) - 1)
					If dDataAux >= dInicio
						dData := dDataAux
					EndIf
				EndIf
			EndIf
		ElseIf nTipo == 3 //Quinzenal
			If Year(dData) == Year(dDataAux) .And. Month(dData) == Month(dDataAux) .And. ;
			   ((Day(dDataAux) < 15 .And. Day(dData) < 15) .Or. (Day(dDataAux) >= 15 .And. Day(dData) >= 15))
				//Datas est�o na mesma quinzena. Utiliza a pr�xima data �til como data do per�odo
				dData := dDataAux
			Else
				If Day(dData) < 15
					dData := FirstDate(dData)
				Else
					dData := FirstDate(dData) + 14
				EndIf
			EndIf
		ElseIf nTipo == 4 //Mensal
			If Year(dData) == Year(dDataAux) .And. Month(dData) == Month(dDataAux)
				//Pr�xima data �til pertence ao mesmo mes/ano da data original.
				//Ir� utilizar a pr�xima data �til como data do per�odo.
				dData := dDataAux
			Else
				//Se n�o existir data �til no m�s, ir� criar o per�odo na data do primeiro dia do per�odo.
				dData := FirstDate(dData)
			EndIf
		EndIf

		//A data do per�odo � menor que a data de in�cio do MRP.
		//Utiliza como data do per�odo a data de in�cio do MRP.
		If dData < dInicio
			dData := dInicio
		EndIf
	EndIf
Return dData

/*/{Protheus.doc} buscaPeriodoDaData
Identifica a posi��o da data no per�odo, caso n�o exista, pode retornar o per�odo anterior (requer ::aPeriodos ordenado)
@author brunno.costa
@since 04/06/2019
@version 1.0
@param 01 - dData     , array  , data para conversao
@param 02 - lMenorIgua, l�gico , indica se deve retornar o per�odo anterior caso n�o exista um correspondente � data:
                                 true  -> retorna o per�odo anterior (default)
								 false -> retorna 0 caso n�o encontre per�odo para a data
@return     nPosicao , numerico, indica a posicao do array de per�odos correpondente � data (dData)
/*/
METHOD buscaPeriodoDaData(dData, lMenorIgua) CLASS MrpDominio_Periodos

	Local nPosicao := 0
	Local nInf     := 1                         // limite inferior
    Local nSup     := Len(::oDominio:aPeriodos) // limite superior
    Local nMeio

	Default lMenorIgua := .T.

    While (nInf < nSup)
        nMeio := (nInf + nSup - 1 - Mod((nInf + nSup-1), 2)) / 2
		If (dData == ::oDominio:aPeriodos[nMeio])
			nPosicao := nMeio
			Exit
        ElseIf (dData < ::oDominio:aPeriodos[nMeio])
			nSup := nMeio - 1
        Else
			nInf := nMeio + 1
		EndIf
    Enddo

    If nInf == nSup
		nPosicao := nInf
	EndIf

	//Se estiver buscando o menor ou igual, verifica se a data realmente � menor ou igual que a do per�odo
	If lMenorIgua .And. nPosicao == 0
		nPosicao := nSup
	EndIf

	//Se estiver buscando o menor ou igual, verifica se a data realmente � menor ou igual que a do per�odo
	If lMenorIgua .And. nPosicao > 0
		If dData < ::oDominio:aPeriodos[nPosicao]
			nPosicao--
		EndIf
	EndIf

Return nPosicao

/*/{Protheus.doc} buscaProximoDiaUtil
Busca o pr�ximo dia �til no calend�rio MRP partindo da data enviada
@author marcelo.neumann
@since 24/06/2019
@version 1.0
@param 01 - dDataPar, data, data de partida para buscar o pr�ximo dia �til
@return     dData   , data, data do pr�ximo dia �til de acordo com o calend�rio MRP
/*/
METHOD buscaProximoDiaUtil(dDataPar) CLASS MrpDominio_Periodos

	Local dData  := dDataPar
	Local lError := .F.

	//Verifica se a data possui hora de trabalho
	While !Self:verificaDataUtil(dData, @lError)
		If lError
			dData := dDataPar
			Exit
		EndIf
		dData++
	EndDo

Return dData

/*/{Protheus.doc} proximaData
Calcula e retorna qual ser� a pr�xima data de acordo com o tipo de processamento
@author marcelo.neumann
@since 11/07/2019
@version 1.0
@param 01 - dData, data  , data de partida para buscar a pr�xima
@param 02 - nTipo, numero, indica o tipo de periodo a ser utilizado: 1- Di�rio
                                                                     2- Semanal
																	 3- Quinzenal
																	 4- Mensal
@param 03 - dInicio, data, Data inicial do processo do MRP
@return     dData, data  , pr�xima data de acordo com o tipo
/*/
METHOD proximaData(dData, nTipo, dInicio) CLASS MrpDominio_Periodos

	Do Case
		Case nTipo == 1 //Di�rio
			dData++

		Case nTipo == 2 //Semanal
			dData += 7
			While Dow(dData) > 1
				dData--
			End

		Case nTipo == 3 //Quinzenal
			If Day(dData) < 15
				dData := FirstDate(dData) + 14
			Else
				dData := FirstDate(MonthSum(dData,1))
			EndIf

		Case nTipo == 4 //Mensal
			dData := FirstDate(MonthSum(dData,1))
	EndCase

	If nTipo <> 1
	   dData := Self:buscaDataPeriodo(dData, nTipo, dInicio)
	EndIf

Return dData

/*/{Protheus.doc} primeiroPeriodoUtil
Retorna qual � o primeiro per�odo �til de acordo com o calend�rio, respeitando a parametriza��o de
utiliza��o do calend�rio. Mant�m o primeiro per�odo salvo na vari�vel nPrimeiroUtil para n�o fazer a busca
todas as vezes que este m�todo for executado.

@author lucas.franca
@since 02/10/2019
@version 1.0
@return Self:nPrimeiroUtil, Numeric, primeiro per�odo �til de acordo com o calend�rio.
/*/
Method primeiroPeriodoUtil() CLASS MrpDominio_Periodos
	If Self:nPrimeiroUtil == 0
		If Self:oDominio:oParametros["nLeadTime"] <> 1
			Self:dPrimeiraUtil := Self:buscaProximoDiaUtil(Self:oDominio:aPeriodos[1])
			Self:nPrimeiroUtil := Self:buscaPeriodoDaData(Self:dPrimeiraUtil)
		Else
			Self:nPrimeiroUtil := 1
		EndIf
	EndIf
Return Self:nPrimeiroUtil

/*/{Protheus.doc} primeiraDataUtil
Retorna qual � a primeira data �til de acordo com o calend�rio, respeitando a parametriza��o de
utiliza��o do calend�rio. Mant�m o primeiro per�odo salvo na vari�vel dPrimeiraUtil para n�o fazer a busca
todas as vezes que este m�todo for executado.

@author lucas.franca
@since 02/10/2019
@version 1.0
@return Self:nPrimeiroUtil, Numeric, primeiro per�odo �til de acordo com o calend�rio.
/*/
Method primeiraDataUtil() CLASS MrpDominio_Periodos
	If Self:dPrimeiraUtil == Nil
		Self:primeiroPeriodoUtil()
	EndIf
Return Self:dPrimeiraUtil

/*/{Protheus.doc} verificaDataUtil
Verifica no Calend�rio se determinada data � �til ou n�o, de acordo com
a parametriza��o nLeadTime.

@author lucas.franca
@since 02/10/2019
@version 1.0
@return lDataUtil, logic, .T. se a data dData for �til de acordo com o calend�rio.
/*/
Method verificaDataUtil(dData, lError) CLASS MrpDominio_Periodos
	Local cData     := ""
	Local cUteis    := ""
	Local lDataUtil := .T.
	Default lError  := .F.

	If Self:oDominio:oParametros["nLeadTime"] <> 1
		cData := DtoS(dData)
		If Self:oDatasUteis[cData] == Nil
			cUteis := Self:oDominio:oDados:retornaCampo("CAL", 1, cData, "CAL_UTEIS", @lError)
			If lError .Or. cUteis == "00:00"
				Self:oDatasUteis[cData] := .F.
				If lError
					::oDominio:oDados:oLogs:log(STR0099 + DToC(dData) + STR0100, "21") //"Data " XX/XX/XX " nao encontrada no calendario do MRP."
				EndIf
			Else
				Self:oDatasUteis[cData] := .T.
			EndIf
		EndIf
		lDataUtil := Self:oDatasUteis[cData]
	EndIf
Return lDataUtil

/*/{Protheus.doc} ultimaDataDoMRP
Retorna a �ltima data que deve ser considerada no MRP.

@author lucas.franca
@since 11/10/2019
@version 1.0
@return dData, date, �ltima data do MRP.
/*/
METHOD ultimaDataDoMRP() CLASS MrpDominio_Periodos
	Local dData    := Nil
	Local nLastPer := Len(Self:oDominio:aPeriodos)

	If Self:dUltimaData == Nil
		If Self:oDominio:oParametros["nTipoPeriodos"] == 1 //Per�odo di�rio
			//Ir� utilizar como �ltima data do MRP a mesma data do �ltimo per�odo.
			Self:dUltimaData := Self:oDominio:aPeriodos[nLastPer]
		ElseIf Self:oDominio:oParametros["nTipoPeriodos"] == 2 //Per�odo semanal
			//Ir� utilizar como �ltima data do MRP o �ltimo dia da �ltima semana de processamento.
			Self:dUltimaData := Self:oDominio:aPeriodos[nLastPer]
			While Dow(Self:dUltimaData) < 7
				Self:dUltimaData++
			End
		ElseIf Self:oDominio:oParametros["nTipoPeriodos"] == 3 //Per�odo quinzenal
			//Ir� utilizar como �ltima data do MRP o �ltimo dia da �ltima quinzena de processamento.
			Self:dUltimaData := Self:oDominio:aPeriodos[nLastPer]
			If Day(Self:dUltimaData) < 15
				//Primeira quinzena do m�s.
				While Day(Self:dUltimaData) < 14
					Self:dUltimaData++
				End
			Else
				//Segunda quinzena do m�s. Utiliza �ltimo dia do m�s
				Self:dUltimaData := FirstDate(MonthSum(Self:dUltimaData,1)) - 1
			EndIf
		Else //Per�odo mensal
			//Ir� utilizar como �ltima data do MRP o �ltimo dia do m�s.
			Self:dUltimaData := Self:oDominio:aPeriodos[nLastPer]
			Self:dUltimaData := FirstDate(MonthSum(Self:dUltimaData,1)) - 1
		EndIf
	EndIf

	dData := Self:dUltimaData
Return dData