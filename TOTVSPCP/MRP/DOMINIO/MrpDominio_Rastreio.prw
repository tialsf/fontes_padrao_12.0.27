#INCLUDE 'protheus.ch'
#INCLUDE 'MRPDominio.ch'

#DEFINE ABAIXA_POS_CHAVE              1
#DEFINE ABAIXA_POS_DOCPAI             2
#DEFINE ABAIXA_POS_NECESSIDADE        3
#DEFINE ABAIXA_POS_QTD_ESTOQUE        4
#DEFINE ABAIXA_POS_CONSUMO_ESTOQUE    5
#DEFINE ABAIXA_POS_QTD_SUBSTITUICAO   6
#DEFINE ABAIXA_POS_QUEBRAS_QUANTIDADE 7
#DEFINE ABAIXA_POS_TIPO_PAI           8
#DEFINE ABAIXA_POS_NEC_ORIG           9
#DEFINE ABAIXA_POS_REGRA_ALT         10
#DEFINE ABAIXA_POS_CHAVE_SUBST       11
#DEFINE ABAIXA_SIZE                  11

#DEFINE ADADOS_POS_TIPOPAI                 1
#DEFINE ADADOS_POS_DOCPAI                  2
#DEFINE ADADOS_POS_DOCFILHO                3
#DEFINE ADADOS_POS_COMPONENTE              4
#DEFINE ADADOS_POS_PERIODO                 5
#DEFINE ADADOS_POS_TRT                     6
#DEFINE ADADOS_POS_QTD_ESTOQUE             7
#DEFINE ADADOS_POS_CONSUMO_ESTOQUE         8
#DEFINE ADADOS_POS_NEC_ORIGINAL            9
#DEFINE ADADOS_POS_NECESSIDADE             10
#DEFINE ADADOS_POS_QTD_SUBSTITUICAO        11
#DEFINE ADADOS_POS_CONSUMO_SUBSTITU        12
#DEFINE ADADOS_POS_QTD_SUBST_ORIGINAL      13 //Quantidade de substitui��o convertida no fator do produto original
#DEFINE ADADOS_POS_CHAVE_SUBSTITUICAO      14
#DEFINE ADADOS_POS_REGRA_ALTERNATIVO       15
#DEFINE ADADOS_POS_SUBST_ORDEM             16
#DEFINE ADADOS_POS_REVISAO                 17
#DEFINE ADADOS_POS_ROTEIRO                 18 //ROTEIRO DO PRODUTO PAI
#DEFINE ADADOS_POS_OPERACAO                19 //OPERA��O DO PRODUTO ATUAL NO ROTEIRO DO PRODUTO PAI
#DEFINE ADADOS_POS_ROTEIRO_DOCUMENTO_FILHO 20 //ROTEIRO DO PRODUTO ATUAL NO DOCUMENTO FILHO
#DEFINE ADADOS_POS_LOCAL                   21
#DEFINE ADADOS_POS_QUEBRAS_QUANTIDADE      22
#DEFINE ADADOS_POS_COMP_EXPL_ESTRUT        23
#DEFINE ADADOS_POS_ID_OPCIONAL             24
#DEFINE ADADOS_POS_VERSAO_PRODUCAO         25
#DEFINE ADADOS_SIZE                        25

#DEFINE AINDICE3_POS_ID              1
#DEFINE AINDICE3_POS_IND2            2
#DEFINE AINDICE3_POS_IND3            3

Static snTamCod      := 90

/*/{Protheus.doc} MrpDominio_Rastreio
Classe de controle do rastreio de OP's
@author    brunno.costa
@since     25/04/2019
@version   1
/*/
CLASS MrpDominio_Rastreio FROM LongClassName

	DATA oDados          AS Object //Instancia da classe de dados do MRP
	DATA oDados_Rastreio AS Object //Inst�ncia da classe Global_Data de rastreabilidade
	DATA oParametros     AS Object //Instancia do objeto JSON de par�metros
	DATA oAlternativo    AS Object //Instancia da classe de dom�nio de alternativos - Matriz
	DATA nIOpeRotei      AS NUMERIC
	DATA nIOpeOpera      AS NUMERIC

	//M�todos P�blicos
	METHOD atualizaSubstituicao(cComponente, nPeriodo, aBaixaPorOP, lAltReg1)
	METHOD atualizaRastreio(cComponente, nPeriodo, aBaixaPorOP, aIndexAtu)
	METHOD baixaPorOP(cComponente, nEstoque, nPeriodo)
	METHOD desfazExplosoes(cComponente, nPeriodo)
	METHOD desfazSubstituicoes(cProduto, nPeriodo, aBaixaPorOP, nConsumo)
	METHOD destruir()
	METHOD getPosicao(cCampo)
	METHOD incluiNecessidade(cTipoPai, cDocPai, cComponente, cIDOpc, cTRT, nQuantidade, nPeriodo, cListOrig, cRegra, cOrdSubst, cRoteiro, cOperacao)
	METHOD inclusoesNecessidade(aBaixaPorOP, cComponente, cIDOpc, cTRT, nQuantidade, nPeriodo, cRegra, nQtdSEst, cRevisao, cRoteiro, cLocal, oRastrPais, lQtdFixa, nFPotencia, nFatPerda, cCodPai, cVersao, lFantasma)
	METHOD retornaRastreio(cProduto, nPeriodo, cDocPai, cTRT, cListOrig)
	METHOD retornaAlternativos(cProduto, nPeriodo)
	METHOD necPPed_ES(cProduto, nPeriodo)
	METHOD new() CONSTRUCTOR

	//Outros internos
	METHOD avaliaSubstOriginal(aAlternati, nEstoque, nSubstitui, oSldSubst)
	METHOD deletaRastreios(cList, cChave, nPeriodo, aComponentes)
	METHOD politicaDeEstoque(cProduto, nPeriodo, nEstoque)
	METHOD todosAltConsumidos(cProdOri, cExcecao, nPeriodo, cPrimeiro)
	METHOD forcaSubstituicaoAlternativo(cProdOri, nPeriodo, nQuantidade, aOriginal, cAlternativo, nSubstitui)
	METHOD forcaRecalculoOriginal(cProduto, nSaldo, nPeriodo, cAlternativo)
	METHOD retornaOperacao(cRoteiro, aOperacoes)

ENDCLASS

/*/{Protheus.doc} MrpDominio_Rastreio
M�todo construtor da classe MrpDominio_Rastreio
@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - lCreate, l�gico, indica se deve criar a sess�o global de dados
@param 02 - oDados , objeto, instancia da camada de dados do MRP
@return, Self, objeto, instancia desta classe
/*/
METHOD new(oDados) CLASS MrpDominio_Rastreio

	Self:oDados          := oDados
	Self:oDados_Rastreio := oDados:oRastreio
	Self:oParametros     := oDados:oParametros
	Self:oAlternativo    := oDados:oDominio:oAlternativo
	Self:nIOpeRotei      := oDados:posicaoCampo("OPE_ROTE")
    Self:nIOpeOpera      := oDados:posicaoCampo("OPE_OPERA")

Return Self

/*/{Protheus.doc} inclusoesNecessidade
Inclui Rastreio da Necessidade Original referente Rastreabilidade Precessora
@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - aBaixaPorOP, array   , array com os dados de rastreabilidade origem (Documentos Pais)
							   {{1 - Id Rastreabilidade,;
							     2 - Documento Pai,;
							     3 - Quantidade Necessidade,;
							     4 - Quantidade Estoque,;
							     5 - Quantidade Baixa Estoque,;
							     6 - Quantidade Substitui��o
								 7 - ID Registro Substitu�do
								 8 - Tipo do Documento Pai},...}
@param 02 - cComponente, caracter, c�digo do componente a ser utilizado na inclus�o de necessidade
@param 03 - cIDOpc     , caracter, c�digo do ID do opcional
@param 04 - cTRT       , caracter, c�digo do TRT a ser utilizado na inclus�o de necessidade
@param 05 - nQuantidade, n�mero  , quantidade refer�ncia para inclus�o das necessidades
@param 06 - nPeriodo   , n�mero  , per�odo refer�ncia para inclus�o das necessidades
@param 07 - cRegra     , caracter,  regra de consumo dos alternativos:
								1- Valida Original; Valida Alternativo; Compra Original
								2- Valida Original; Valida Alternativo; Compra Alternativo
								3- Valida Alternativo; Compra Alternativo
@param 08 - nQtdSEst   , n�mero  , retorna a quantidade total do componente, considerando os arredondamentos.
@param 09 - cRevisao   , caracter, revis�o do produto Pai
@param 10 - cRoteiro   , caracter, roteiro de produ��o do produto Pai
@param 11 - cLocal    , caracter, local de consumo padr�o
@param 12 - oRastrPais, objeto  , objeto Json para controle/otimiza��o das altera��es de rastreabilidade no regitro Pai durante explos�o na estrutura
@param 13 - lQtdFixa  , l�gico, indica se o componente possui quantidade fixa ou n�o, caso .T., n�o executa multiplica��o da quantidade do componente pela qtd do pai
@param 14 - nFPotencia, num�rico, indica o fator de pot�ncia do componente, fator que age sobre a necessidade de cada componente
@param 15 - nFatPerda , num�rico, indica o fator de perda do componente
@param 16 - cCodPai   , caracter, c�digo do produto pai
@param 17 - cVersao   , caracter, c�digo da vers�o da produ��o
@param 18 - lFantasma , l�gico  , indica se � inclus�o relacionada a produto fantasma
@param 19 - aOperacoes, array   , opera��es existentes relacionadas a este componente
@param 20 - dLeadTime , data    , data lead time do componente utilizado no evento 004
@return Nil
/*/
METHOD inclusoesNecessidade(aBaixaPorOP, cComponente, cIDOpc, cTRT, nQuantidade, nPeriodo, ;
                            cRegra, nQtdSEst, cRevisao, cRoteiro, cLocal, oRastrPais, lQtdFixa, ;
                            nFPotencia, nFatPerda, cCodPai, cVersao, lFantasma, aOperacoes, dLeadTime) CLASS MrpDominio_Rastreio

	Local aDados     := {}
	Local aAreaPRD   := {}
	Local cChave     := ""
	Local cDocFilho  := ""
	Local cDocPai    := ""
	Local cOperacao  := ""
	Local lError     := .F.
	Local lAglutina  := .F.
	Local lAtual     := cComponente == ::oDados:oProdutos:cCurrentKey
	Local nEstoque   := 0
	Local nIndex     := 0
	Local nIndQuebra := 0
	Local nTotal     := Len(aBaixaPorOP)
	Local nTotQuebra := 0
	Local nNecPai    := 0

	Default cIDOpc     := ""
	Default cCompFilho := ""
	Default cTRTFilho  := ""
	Default cRegra     := "1"
	Default cRevisao   := ""
	Default cRoteiro   := ""
	Default cLocal     := ""
	Default cVersao    := ""
	Default lQtdFixa   := .F.
	Default nFatPerda  := 0
	Default nFPotencia  := 0
	Default aOperacoes := {}

	//Mant�m o componente posicionado.
	If !lAtual
		aAreaPRD := ::oDados:retornaArea("PRD")
		::oDados:retornaCampo("PRD", 1, cComponente, "PRD_COD", , lAtual, , /*lProximo*/, , , .F. /*lVarios*/)
	EndIf

	nQtdSEst  := 0
	lAglutina := ::oDados:oDominio:oAglutina:avaliaAglutinacao(cComponente)

	//Gera registro de rastreabilidade quando inexist�ncia de OP
	If nTotal == 0
		nTotal := 1
		aAdd(aBaixaPorOP, {"ESTOQUE", "", 1, 0, 0, 0, {},"ESTOQUE"})
	EndIf

	//STR0120 - "Inclusoes necessidade do componente '"
	//STR0121 - "' de TRT '"
	//STR0122 - "' no per�odo '"
	//STR0123 - "' referente quantidade: "
	::oDados:oLogs:log(STR0120 + AllTrim(cComponente) + STR0121 + cTRT + STR0122 + cValToChar(nPeriodo) + STR0123 + cValToChar(nQuantidade), "R9")

	For nIndex := 1 to nTotal
		lError    := .F.

		nTotQuebra := Len(aBaixaPorOP[nIndex][ABAIXA_POS_QUEBRAS_QUANTIDADE])
		If nTotQuebra < 1 //Se n�o tiver o array de quebras, cria uma quebra com a qtd. total.
			aSize(aBaixaPorOP[nIndex][ABAIXA_POS_QUEBRAS_QUANTIDADE], 0)
			aBaixaPorOP[nIndex][ABAIXA_POS_QUEBRAS_QUANTIDADE] := {{aBaixaPorOP[nIndex][ABAIXA_POS_NECESSIDADE], ""}}
			nTotQuebra := 1
		EndIf

		cChave   := aBaixaPorOP[nIndex][ABAIXA_POS_CHAVE]

		//Atualiza o rastreio com as quebras de produ��o/compra definidas pelas pol�ticas de estoque.
		If oRastrPais[cChave] != Nil .AND. !lFantasma
			oRastrPais[cChave][ADADOS_POS_QUEBRAS_QUANTIDADE] := aClone(aBaixaPorOP[nIndex][ABAIXA_POS_QUEBRAS_QUANTIDADE])
		EndIf

		For nIndQuebra := 1 To nTotQuebra
			cDocFilho := ""
			//Para cada quebra de produ��o, ir� gerar um novo registro de rastreabilidade filho, com um novo documento.
			nNecPai   := aBaixaPorOP[nIndex][ABAIXA_POS_QUEBRAS_QUANTIDADE][nIndQuebra][1]

			If nIndQuebra == 1
				nEstoque := aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE]
			EndIf

			If nNecPai == 0
				Loop
			EndIf

			nNecComp := ::oDados:oDominio:ajustarNecessidadeExplosao(cComponente, nNecPai, nQuantidade, lQtdFixa, @nFPotencia, nFPotencia, nFatPerda)

			//Gera Evento 004 - Data de necessidade invalida - Data anterior a database (Novas necessidades do MRP com data anterior a base do sistema)
			If Self:oDados:oParametros["lEventLog"] .AND. nNecComp != 0 .AND. dLeadTime < Self:oDados:oParametros["dDataIni"]
				Self:oDados:oDominio:oEventos:loga("004", cComponente, dLeadTime, {AllTrim(Left(cCodPai, snTamCod)), nNecComp, dLeadTime, aBaixaPorOP[nIndex][ABAIXA_POS_DOCPAI], "", ""})
			EndIf

			//Soma a qtd no totalizador p/ retornar a qtd total arredondada.
			nQtdSEst += nNecComp

			cDocPai := aBaixaPorOP[nIndex][ABAIXA_POS_DOCPAI]
			cDocPai += chr(13) + cValToChar(nIndQuebra)

			If oRastrPais[cChave] == Nil
				If !lAglutina
					cDocFilho := cChave
				EndIf

				//Atualiza necessidade do Documento Filho
				::oDados:oDominio:oAglutina:preparaEInclui("4", "OP", {aBaixaPorOP[nIndex][ABAIXA_POS_TIPO_PAI], cDocPai, cCodPai}, @cDocFilho, cComponente, cIDOpc, cTRT, nNecComp, nPeriodo, cRegra, lAglutina, nIndQuebra != 1)

			Else
				//Gera ID Documento Filho
				If lAglutina
					cDocFilho := ::oDados_Rastreio:proximaOP()
				Else
					If !Empty(oRastrPais[cChave][ADADOS_POS_DOCFILHO]) .AND. nIndQuebra == 1
						cDocFilho := oRastrPais[cChave][ADADOS_POS_DOCFILHO]

					ElseIf Empty(aBaixaPorOP[nIndex][ABAIXA_POS_QUEBRAS_QUANTIDADE][nIndQuebra][2])
						If oRastrPais[cChave][ADADOS_POS_TIPOPAI] == "OP"
							cDocFilho := ::oDados_Rastreio:proximaOPItem(oRastrPais[cChave][ADADOS_POS_DOCPAI])
						Else
							cDocFilho := ::oDados_Rastreio:proximaOP()
						EndIf
					Else
						cDocFilho := aBaixaPorOP[nIndex][ABAIXA_POS_QUEBRAS_QUANTIDADE][nIndQuebra][2]
					EndIf
				EndIf

				//Atualiza Rastreio Registro Pai na Quebra
				If !lFantasma
					oRastrPais[cChave][ADADOS_POS_QUEBRAS_QUANTIDADE][nIndQuebra][2]  := cDocFilho
				EndIf
				aBaixaPorOP[nIndex][ABAIXA_POS_QUEBRAS_QUANTIDADE][nIndQuebra][2] := cDocFilho

				//Atualiza necessidade do Documento Filho
				cOperacao := Self:retornaOperacao(cRoteiro, aOperacoes)
				::oDados:oDominio:oAglutina:preparaEInclui("4", "OP", {aBaixaPorOP[nIndex][ABAIXA_POS_TIPO_PAI], cDocPai, cCodPai}, @cDocFilho, cComponente, cIDOpc, cTRT, nNecComp, nPeriodo, cRegra, lAglutina, nIndQuebra != 1, cRoteiro, cOperacao)

				If nIndQuebra == 1
					//Somente atualiza estas informa��es quando for o primeiro
					//registro da quebra de produ��o/compra. Para os demais registros da quebra,
					//estes dados sempre ser�o iguais. As informa��es diferentes para cada
					//quebra estar�o armazenadas dentro do array ABAIXA_POS_QUEBRAS_QUANTIDADE{"QTD","DOCUMENTO"}
					If Empty(oRastrPais[cChave][ADADOS_POS_DOCFILHO])
						oRastrPais[cChave][ADADOS_POS_DOCFILHO           ] := cDocFilho
					EndIf
					oRastrPais[cChave][ADADOS_POS_QTD_ESTOQUE            ] := nEstoque
					oRastrPais[cChave][ADADOS_POS_REVISAO                ] := cRevisao
					oRastrPais[cChave][ADADOS_POS_ROTEIRO_DOCUMENTO_FILHO] := cRoteiro
					oRastrPais[cChave][ADADOS_POS_LOCAL                  ] := cLocal
					oRastrPais[cChave][ADADOS_POS_VERSAO_PRODUCAO        ] := cVersao
					If oRastrPais[cChave][ADADOS_POS_COMP_EXPL_ESTRUT] == Nil
						oRastrPais[cChave][ADADOS_POS_COMP_EXPL_ESTRUT   ] := {}
					Endif
					aAdd(oRastrPais[cChave][ADADOS_POS_COMP_EXPL_ESTRUT], (cComponente + Iif(!Empty(cIDOpc),"|"+cIDOpc,"") + chr(13) + cValToChar(nPeriodo) + chr(13) + cTRT + chr(13) + ""))
				EndIf
			EndIf

		Next nIndQuebra

	Next

	//Retorna o posicionamento do componente.
	If !lAtual
		::oDados:setaArea(aAreaPRD)
		aSize(aAreaPRD, 0)
	EndIf

	If aDados != Nil
		aSize(aDados, 0)
		aDados := Nil
	EndIf

Return Nil


/*/{Protheus.doc} retornaOperacao
Retorna a opera��o do roteiro relacionada ao componente
@author    brunno.costa
@since     15/04/2020
@version   1
@param 01 - cRoteiro  , caracter, c�digo do roteiro relacionado ao componente
@param 02 - aOperacoes, array   , array com as opera��es deste produto e componente
@return cOperacao, array, array com os dados do rastreio deste registro
/*/

METHOD retornaOperacao(cRoteiro, aOperacoes) CLASS MrpDominio_Rastreio

	Local cOperacao := ""
	Local nPos      := 0

	If !Empty(cRoteiro) .AND. !Empty(aOperacoes)
		nPos := aScan(aOperacoes, {|x| AllTrim(x[Self:nIOpeRotei]) == cRoteiro})
		If nPos > 0
			cOperacao := aOperacoes[nPos][Self:nIOpeOpera]
		EndIf
	EndIf

Return cOperacao

/*/{Protheus.doc} retornaRastreio
Retorna registro de rastreabilidade do documento
@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - cProduto , caracter, c�digo do produto
@param 02 - nPeriodo , n�mero  , n�mero do per�odo
@param 03 - cDocPai  , caracter, c�digo do documento pai do registro
@param 04 - cTRT     , caracter, c�digo do TRT do produto na estrutura
@param 05 - cListOrig, caracter, chave do registro original do Rastreio, quando alternativo
@return aRastreio, array, array com os dados do rastreio deste registro
/*/

METHOD retornaRastreio(cProduto, nPeriodo, cDocPai, cTRT, cListOrig) CLASS MrpDominio_Rastreio

	Local aRastreio
	Local cList
	Local cChave

	Default cListOrig := ""

	cList  := cProduto + chr(13) + cValToChar(nPeriodo)
	cChave := cDocPai + chr(13) + cTRT + chr(13) + cListOrig

	aRastreio := ::oDados_Rastreio:oDados:getItemAList(cList, cChave/*, @lError*/)

Return aRastreio

/*/{Protheus.doc} retornaAlternativos
Retorna C�digos dos Alternativos
@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - cProduto, caracter, c�digo do componente relacionado
@param 02 - nPeriodo , n�mero  , n�mero do per�odo
@Return aAlteDeman, array, array com os c�digos dos produtos alternativos consumidos
/*/

METHOD retornaAlternativos(cProduto, nPeriodo) CLASS MrpDominio_Rastreio

	Local cOrigDeman := "KEY_" + AllTrim(cProduto) + "_" + cValToChar(nPeriodo)
	Local aAlteDeman := ::oDados:oAlternativos:getItemAList("substituicoes_produtos", cOrigDeman)

Return aAlteDeman


/*/{Protheus.doc} incluiNecessidade
Inclui Rastreio da Necessidade Original
@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - cTipoPai   , caracter, tipo do Documento Pai
@param 02 - cDocPai    , caracter, identificador do documento Pai
@param 03 - cComponente, caracter, c�digo do componente
@param 04 - cIDOpc     , caracter, ID do opcional deste produto
@param 05 - cTRT       , caracter, c�digo do TRT
@param 06 - nQuantidade, n�mero  , quantidade da necessidade
@param 07 - nPeriodo   , n�mero  , per�odo da necessidade
@param 08 - cListOrig  , caracter, cList relacionado ao registro original da substitui��o
@param 09 - cRegra     , caracter,  regra de consumo dos alternativos:
								1- Valida Original; Valida Alternativo; Compra Original
								2- Valida Original; Valida Alternativo; Compra Alternativo
								3- Valida Alternativo; Compra Alternativo
@param 10 - cOrdSubst  , caracter, indica a ordem do alternativo na substitui��o
@param 11 - cRoteiro   , caracter, c�digo do roteiro de opera��es do produto Pai
@param 12 - cOperacao  , caracter, c�digo da opera��o deste componente no roteiro do produto Pai
@return cList, caracter, identificador desta sess�o de vari�veis globais
/*/

METHOD incluiNecessidade(cTipoPai, cDocPai, cComponente, cIDOpc, cTRT, nQuantidade, nPeriodo, cListOrig, cRegra, cOrdSubst, cRoteiro, cOperacao) CLASS MrpDominio_Rastreio

	Local aDados   := {}
	Local cList    := ""
	Local lError   := .F.
	Local cChave   := ""

	Default cTipoPai    := "ID"
	Default cDocPai     := ""
	Default cTRT        := ""
	Default cIDOpc      := ""
	Default cListOrig   := ""
	Default cOrdSubst   := "00"
	Default cRegra      := "1"
	Default cRoteiro    := ""
	Default cOperacao   := ""

	//Recupera ID do Registro (ou Insere Novo e Recupera)
	cList := cComponente + Iif(!Empty(cIDOpc) , "|" + cIDOpc, "") + chr(13) + cValToChar(nPeriodo)

	::oDados_Rastreio:oDados:createAList(cList)

	cChave := cDocPai + chr(13) + cTRT + chr(13) + cListOrig
	aDados := ::oDados_Rastreio:oDados:getItemAList(cList, cChave, @lError)
	If lError//Inclui novo registro
		lError := .F.
		aDados := Array(ADADOS_SIZE)
		aDados[ADADOS_POS_PERIODO                ] := nPeriodo
		aDados[ADADOS_POS_TIPOPAI                ] := AllTrim(cTipoPai)
		aDados[ADADOS_POS_DOCPAI                 ] := cDocPai
		aDados[ADADOS_POS_DOCFILHO               ] := ""
		aDados[ADADOS_POS_COMP_EXPL_ESTRUT       ] := {}
		aDados[ADADOS_POS_COMPONENTE             ] := cComponente
		aDados[ADADOS_POS_ID_OPCIONAL            ] := cIDOpc
		aDados[ADADOS_POS_TRT                    ] := cTRT
		aDados[ADADOS_POS_NEC_ORIGINAL           ] := nQuantidade
		aDados[ADADOS_POS_CONSUMO_ESTOQUE        ] := 0
		aDados[ADADOS_POS_NECESSIDADE            ] := Iif(nQuantidade < 0, 0, nQuantidade)
		aDados[ADADOS_POS_QTD_ESTOQUE            ] := 0
		aDados[ADADOS_POS_QTD_SUBSTITUICAO       ] := 0
		aDados[ADADOS_POS_CONSUMO_SUBSTITU       ] := 0
		aDados[ADADOS_POS_CHAVE_SUBSTITUICAO     ] := cListOrig
		aDados[ADADOS_POS_SUBST_ORDEM            ] := cOrdSubst
		aDados[ADADOS_POS_REGRA_ALTERNATIVO      ] := cRegra
		aDados[ADADOS_POS_QUEBRAS_QUANTIDADE     ] := {}
		aDados[ADADOS_POS_ROTEIRO                ] := cRoteiro
		aDados[ADADOS_POS_OPERACAO               ] := cOperacao
		aDados[ADADOS_POS_ROTEIRO_DOCUMENTO_FILHO] := ""

		//STR0124 - "Inclui necessidade do produto '"
		//STR0125 - "' e documento '"
		//STR0126 - "TRT: "
		::oDados:oLogs:log(STR0124 + AllTrim(aDados[ADADOS_POS_COMPONENTE]) + STR0125 + aDados[ADADOS_POS_DOCPAI]  + " (" + STR0126 + aDados[ADADOS_POS_TRT]  + ")': " + cValToChar(Iif(nQuantidade < 0, 0, nQuantidade)), "R6")

	Else //Atualiza registro existente

		//STR0127 - "Atualiza necessidade do produto '"
		//STR0125 - "' e documento '"
		//STR0126 - "TRT: "
		::oDados:oLogs:log(STR0127 + AllTrim(aDados[ADADOS_POS_COMPONENTE]) + STR0125 + aDados[ADADOS_POS_DOCPAI]  + " (" + STR0126 + aDados[ADADOS_POS_TRT]  + ")': " + cValToChar(aDados[ADADOS_POS_NEC_ORIGINAL]) + " + " + cValToChar(nQuantidade), "R7")
		// Se for Ponto de pedido, substitui a quantidade
		If aDados[ADADOS_POS_TIPOPAI] == STR0144  //Pont. Ped.
			aDados[ADADOS_POS_NEC_ORIGINAL] := nQuantidade
		Else
			aDados[ADADOS_POS_NEC_ORIGINAL] += nQuantidade
		EndIf

	EndIf
	::oDados_Rastreio:oDados:setItemAList(cList, cChave, aDados, @lError)

	aSize(aDados, 0)
	aDados := Nil

Return cList

/*/{Protheus.doc} desfazSubstituicoes
Desfaz substitui��o de Rastreabilidade

@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - cProduto   , caracter, c�digo do produto original relacionado
@param 02 - nPeriodo   , n�mero  , identificador do per�odo relacionado a substitui��o
@param 03 - aBaixaPorOP, array   , array com os dados de rastreabilidade origem (Documentos Pais)
							   {{1 - Id Rastreabilidade,;
							     2 - Documento Pai,;
							     3 - Quantidade Necessidade,;
							     4 - Quantidade Estoque,;
							     5 - Quantidade Baixa Estoque,;
							     6 - Quantidade Substitui��o
								 7 - ID Registro Substitu�do
								 8 - Regra de Consumo do Alternativo},...}
@param 04 - nConsumo    , n�mero   , quantidade de consumo da substitui��o a ser desfeita
@param 05 - cAlternativo, caracter , c�digo do produto alternativo que deve ser desfeita a substitui��o, quando vazio, desfaz de todos.
/*/

METHOD desfazSubstituicoes(cProduto, nPeriodo, aBaixaPorOP, nConsumo, cAlternativo) CLASS MrpDominio_Rastreio

	Local aDados     := {}
	Local aDadosAlt  := {}
	Local cChave
	Local cList      := cProduto + chr(13) + cValToChar(nPeriodo)
	Local cListAlt   := ""
	Local lError     := .F.
	Local nIndex
	Local nTotal     := Len(aBaixaPorOP)
	Local nSubstitui := -nConsumo

	Default cAlternativo := ""

	//STR0128 - "Desfaz substitui��es do produto '"
	//STR0122 - "' no per�odo '"
	//STR0129 - "' referente consumo: "
	::oDados:oLogs:log(STR0128 + AllTrim(cProduto) + STR0122 + cValToChar(nPeriodo) + STR0129 + cValToChar(nConsumo), "R8")

	For nIndex := 1 to nTotal
		aDadosAlt := {}
		lError    := .F.
		cChave    := aBaixaPorOP[nIndex][1]

		If nSubstitui == 0
			Exit
		EndIf

		aDados := ::oDados_Rastreio:oDados:getItemAList(cList, cChave, @lError)
		If !lError
			If !Empty(aDados[ADADOS_POS_CHAVE_SUBSTITUICAO])
				Loop
			EndIf

			If !Empty(cAlternativo)
				cListAlt := cAlternativo + chr(13) + cValToChar(nPeriodo)
				aDadosAlt := ::oDados_Rastreio:oDados:getItemAList(cListAlt, cChave, @lError)
				If lError
					Loop
				ElseIf !Empty(aDadosAlt[ADADOS_POS_CHAVE_SUBSTITUICAO])
					::oDados_Rastreio:oDados:delItemAList(cListAlt, cChave)
				EndIf
			Endif

			If nSubstitui < aDados[ADADOS_POS_QTD_SUBSTITUICAO]
				//STR0128 - "Desfaz substitui��es do produto '"
				//STR0122 - "' no per�odo '"
				//STR0130 - "' pelo alternativo '"
				//STR0129 - "' referente consumo: "
				::oDados:oLogs:log(STR0128 + AllTrim(cProduto) + STR0122 + cValToChar(nPeriodo) + STR0130 + AllTrim(cAlternativo) + STR0129 + cValToChar(nSubstitui), "R8")

				aBaixaPorOP[nIndex][ABAIXA_POS_NECESSIDADE]      += nSubstitui
				aDados[ADADOS_POS_QTD_SUBSTITUICAO]              -= nSubstitui
				aDados[ADADOS_POS_NECESSIDADE]                   += nSubstitui
				aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] := 0
				nSubstitui                                       := 0

				//Desconta necessidade original negativa
				If aDados[ADADOS_POS_NECESSIDADE] > 0 .AND. aDados[ADADOS_POS_NEC_ORIGINAL] < 0
					aDados[ADADOS_POS_NECESSIDADE] += aDados[ADADOS_POS_NEC_ORIGINAL]
				EndIf

				::oDados_Rastreio:oDados:setItemAList(cList, cChave, aDados)
				Exit
			Else
				//STR0128 - "Desfaz substitui��es do produto '"
				//STR0122 - "' no per�odo '"
				//STR0130 - "' pelo alternativo '"
				//STR0129 - "' referente consumo: "
				::oDados:oLogs:log(STR0128 + AllTrim(cProduto) + STR0122 + cValToChar(nPeriodo) + STR0130 + AllTrim(cAlternativo) + STR0129 + cValToChar(aDados[ADADOS_POS_QTD_SUBSTITUICAO]), "R8")

				aBaixaPorOP[nIndex][ABAIXA_POS_NECESSIDADE]      += aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO]
				aDados[ADADOS_POS_NECESSIDADE]                   += aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO]
				nSubstitui                                       -= aDados[ADADOS_POS_QTD_SUBSTITUICAO]
				aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] := 0
				aDados[ADADOS_POS_QTD_SUBSTITUICAO]              := 0

				//Desconta necessidade original negativa
				If aDados[ADADOS_POS_NECESSIDADE] > 0 .AND. aDados[ADADOS_POS_NEC_ORIGINAL] < 0
					aDados[ADADOS_POS_NECESSIDADE] += aDados[ADADOS_POS_NEC_ORIGINAL]
				EndIf

				::oDados_Rastreio:oDados:setItemAList(cList, cChave, aDados)
			EndIf
		EndIf
	Next

Return

/*/{Protheus.doc} atualizaSubstituicao
Atualiza Substitui��o - Produto Original

@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - cComponente , caracter, c�digo do componente para an�lise
@param 02 - nPeriodo    , n�mero  , per�odo para an�lise
@param 03 - aBaixaPorOP , array   , retorna por refer�ncia array com os dados de rastreabilidade origem (Documentos Pais)
@param 04 - lAltReg1, logico  , indica se refere-se a atualiza��o no registro do produto alternativo com regra do tipo 1
/*/
METHOD atualizaSubstituicao(cComponente, nPeriodo, aBaixaPorOP, lAltReg1) CLASS MrpDominio_Rastreio

	Local aDados
	Local aNames
	Local aDadosChvs  := {}
	Local aAlteDeman  := {} //C�digos dos Alternativos da Substitui��o
	Local aRastreio   := {}
	Local cChvLock
	Local cChave
	Local cList       := cComponente + chr(13) + cValToChar(nPeriodo)
	Local cKeyOri
	Local lAlterou    := .F.
	Local lError      := .F.
	Local lPrimeiro   := .T.
	Local lZerou      := .F.
	Local nAux        := 0
	Local nContador   := 0
	Local nIndex
	Local nSubstitui  := 0
	Local nTotal      := Len(aBaixaPorOP)
	Local nTotalSubs  := 0
	Local nTotalDest  := 0
	Local nDestinos
	Local nEstoque    := 0
	Local oSldSubst

	If nTotal > 0
		//Analise Substitui��o - Produto ALTERNATIVO
		If lAltReg1
			oSldSubst := JsonObject():New()

			For nIndex := 1 To nTotal
				//Recupera o registro de rastreabilidade
				lError    := .F.
				lZerou    := .F.
				aRastreio := ::oDados_Rastreio:oDados:getItemAList(cList, aBaixaPorOP[nIndex][ABAIXA_POS_CHAVE], @lError)
				If !lError
					If (AllTrim(aRastreio[ADADOS_POS_TIPOPAI]) $ "|" + STR0143 + "|" + STR0144 + "|1|") //"Est.Seg." | "Ponto Ped." | "PMP"
						Loop
					EndIf

					//Se conseguiu recuperar o registro, faz o lock e atualiza os dados de acordo com o aBaixaPorOP
					cChvLock := "dados_" + aRastreio[ADADOS_POS_DOCPAI];
										 + aRastreio[ADADOS_POS_COMPONENTE];
										 + aRastreio[ADADOS_POS_ID_OPCIONAL];
										 + aRastreio[ADADOS_POS_TRT]
					::oDados_Rastreio:oDados:lock(cChvLock)
					aRastreio := ::oDados_Rastreio:oDados:getItemAList(cList, aBaixaPorOP[nIndex][1], @lError)
					If lPrimeiro
						lPrimeiro := .F.
						nEstoque  := aRastreio[ADADOS_POS_QTD_ESTOQUE]

					ElseIf lAlterou
						aRastreio[ADADOS_POS_QTD_ESTOQUE]           := nEstoque
						aBaixaPorOP[nIndex][ABAIXA_POS_QTD_ESTOQUE] := nEstoque

					EndIf
					lAlterou    := .F.
					nSubstitui  := aRastreio[ADADOS_POS_QTD_SUBSTITUICAO]

					If !Empty(aRastreio[ADADOS_POS_CHAVE_SUBSTITUICAO]);
				   		.AND. !(aRastreio[ADADOS_POS_REGRA_ALTERNATIVO] $ "|2|3|")

						//Recupera Quantidade da Substitui��o no Produto Original
						If nEstoque == 0
							::avaliaSubstOriginal(@aRastreio, @nEstoque, @nSubstitui, @oSldSubst, .T., @aBaixaPorOP, lAltReg1)
							aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] := 0
							aRastreio[ADADOS_POS_QTD_SUBSTITUICAO]           := 0
							aRastreio[ADADOS_POS_QTD_SUBST_ORIGINAL]         := 0
							nSubstitui                                       := 0
							nEstoque                                         := 0
							lAlterou                                         := .T.
							lZerou                                           := .T.

						Else
							::avaliaSubstOriginal(@aRastreio, @nEstoque, @nSubstitui, @oSldSubst, , @aBaixaPorOP, lAltReg1)
							aRastreio[ADADOS_POS_QTD_SUBSTITUICAO          ] := nSubstitui
							aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] := nSubstitui
							lAlterou                                         := .T.
							If nSubstitui == 0
								lZerou                                       := .T.
							EndIf
						EndIf
					Else

						If nSubstitui >= 0
							nEstoque += nSubstitui;
									+ aRastreio[ADADOS_POS_NECESSIDADE];
									- aRastreio[ADADOS_POS_NEC_ORIGINAL]
						Else
							nEstoque += nSubstitui;
									+ aRastreio[ADADOS_POS_NECESSIDADE];
									- (aRastreio[ADADOS_POS_NEC_ORIGINAL];
									- aRastreio[ADADOS_POS_CONSUMO_ESTOQUE])
						EndIf

					EndIf

					//Atualiza rastreabilidade do produto alternativo
					If lZerou
						::oDados_Rastreio:oDados:delItemAList(cList, aBaixaPorOP[nIndex][1])
					ElseIf lAlterou
						::oDados_Rastreio:oDados:setItemAList(cList, aBaixaPorOP[nIndex][1], aRastreio)
					EndIf
					::oDados_Rastreio:oDados:unLock(cChvLock)

				EndIf
				If !lError
					aSize(aRastreio, 0)
				EndIf
			Next

			aNames := oSldSubst:GetNames()
			nTotal := Len(aNames)
			For nIndex := 1 To nTotal
				oSldSubst[aNames[nIndex]] := Nil
			Next nIndex
			aSize(aNames, 0)
			aNames := Nil

			FreeObj(oSldSubst)
			oSldSubst := Nil

		Else //Analise Substitui��o - Produto ORIGINAL
			cKeyOri := "KEY_ORI_" + AllTrim(cComponente) + "_" + cValToChar(nPeriodo)
			nAux    := ::oDados:oAlternativos:getFlag(cKeyOri)
			If nAux != Nil
				nTotalSubs += nAux
			EndIf

			If nTotalSubs > 0
				//STR0131 - "Atualizando substitui��o no produto original '"
				//STR0132 - "' e per�odo '"
				::oDados:oLogs:log(STR0131 + AllTrim(cComponente) + STR0132 + cValToChar(nPeriodo) + "': " + cValTochar(nTotalSubs), "R1")

				//Recupera registros de rastreabilidade das chaves
				nTotal := Len(aBaixaPorOP)
				For nIndex := 1 to nTotal
					lError := .F.
					aDados := {}
					cChave := aBaixaPorOP[nIndex][1]
					aDados := ::oDados_Rastreio:oDados:getItemAList(cList, cChave, @lError)
					If !lError
						cChvLock := "dados_" + aDados[ADADOS_POS_DOCPAI];
											 + aDados[ADADOS_POS_COMPONENTE];
											 + aDados[ADADOS_POS_ID_OPCIONAL];
											 + aDados[ADADOS_POS_TRT]
						::oDados_Rastreio:oDados:lock(cChvLock)
						nContador++
						aAdd(aDadosChvs, {cChave, aDados, StrZero(nContador, 5)})
					EndIf
				Next
				//Atualiza Total de Registros V�lidos e Ordena por OP
				aDadosChvs := aSort(aDadosChvs, , , {|x,y| Iif(x[2][ADADOS_POS_NEC_ORIGINAL] < 0, "0", "1");
				                                         + Iif(x[2][ADADOS_POS_TIPOPAI] == "Pr�-OP", "0", "1");
				                                         + Iif(x[2][ADADOS_POS_TIPOPAI] == STR0144, "1", "0"); //Ponto Ped.
				                                         + PadL(x[2][ADADOS_POS_CHAVE_SUBSTITUICAO], 130);
				                                         + PadL(x[2][ADADOS_POS_DOCPAI], 100);
				                                         + x[3];
				                                         <;
				                                           Iif(y[2][ADADOS_POS_NEC_ORIGINAL] < 0, "0", "1");
				                                         + Iif(y[2][ADADOS_POS_TIPOPAI] == "Pr�-OP", "0", "1");
				                                         + Iif(y[2][ADADOS_POS_TIPOPAI] == STR0144, "1", "0"); //Ponto Ped.
				                                         + PadL(y[2][ADADOS_POS_CHAVE_SUBSTITUICAO], 130);
				                                         + PadL(y[2][ADADOS_POS_DOCPAI], 100);
				                                         + y[3] } )

				//Percorre Registros de Rastreabilidade
				nTotal := Len(aDadosChvs)
				For nIndex := 1 to nTotal
					lAlterou   := .F.
					lError     := .F.
					nSubstitui := 0
					aRastreio  := aDadosChvs[nIndex][2]

					If !Empty(aRastreio[ADADOS_POS_CHAVE_SUBSTITUICAO]) .OR. !("OP"$aRastreio[ADADOS_POS_TIPOPAI])
						//Recalcular Ponto de Pedido - pq? pra ajustar saldo de PP gerado por consumo de alternativo desfeito
						If (AllTrim(aRastreio[ADADOS_POS_TIPOPAI]) $ "|" + STR0144 + "|") //"Ponto Ped."
						EndIf
						Loop
					EndIf

					If (aRastreio[ADADOS_POS_QTD_SUBSTITUICAO] - nTotalSubs) > 0
						aRastreio[ADADOS_POS_NECESSIDADE]           += (aRastreio[ADADOS_POS_QTD_SUBSTITUICAO] - nTotalSubs)
						aBaixaPorOP[nIndex][ABAIXA_POS_NECESSIDADE] := aRastreio[ADADOS_POS_NECESSIDADE]
						lAlterou                                    := .T.
					EndIf

					//Atualiza Substitui��o e Necessidades
					If nTotalSubs != 0
						IF nTotalSubs == aRastreio[ADADOS_POS_NECESSIDADE]
							nSubstitui += nTotalSubs
							nTotalSubs := 0

						ElseIf aRastreio[ADADOS_POS_NECESSIDADE] > nTotalSubs
							nSubstitui += nTotalSubs
							nTotalSubs := 0

						ElseIf aRastreio[ADADOS_POS_NECESSIDADE] < nTotalSubs
							nSubstitui += aRastreio[ADADOS_POS_NECESSIDADE]
							nTotalSubs -= nSubstitui

						EndIf
					EndIf

					::oDados:oLogs:log(STR0131 + AllTrim(cComponente) + STR0132 + cValToChar(nPeriodo) + "': aRastreio[ADADOS_POS_NECESSIDADE]: " + cValTochar(aRastreio[ADADOS_POS_NECESSIDADE]), "R1")
					::oDados:oLogs:log(STR0131 + AllTrim(cComponente) + STR0132 + cValToChar(nPeriodo) + "': nSubstitui: " + cValTochar(nSubstitui), "R1")
					::oDados:oLogs:log(STR0131 + AllTrim(cComponente) + STR0132 + cValToChar(nPeriodo) + "': nTotalSubs: " + cValTochar(nTotalSubs), "R1")

					//Atualiza array para retorno
					aBaixaPorOP[nIndex][ABAIXA_POS_QTD_SUBSTITUICAO] := nSubstitui

					//Atualiza vari�veis globais
					If aRastreio[ADADOS_POS_QTD_SUBSTITUICAO] != nSubstitui
						lAlterou := .T.
						aRastreio[ADADOS_POS_QTD_SUBSTITUICAO] := nSubstitui
						::oDados:oLogs:log(STR0131 + AllTrim(cComponente) + STR0132 + cValToChar(nPeriodo) + "': " + cValTochar(nTotalSubs) + " nSubstitui: " + cValTochar(nSubstitui), "R1")
					EndIf

					//Atualiza rastreabilidade do produto original
					If lAlterou
						::oDados_Rastreio:oDados:setItemAList(cList, aDadosChvs[nIndex][1], aRastreio)
					EndIf

					If nSubstitui != 0
						//Inclui registro do produto Alternativo na Rastreabilidade
						aAlteDeman  := ::retornaAlternativos(cComponente, nPeriodo)
						nTotalDest := Len(aAlteDeman)
						For nDestinos := 1 to nTotalDest
							cDestino := aAlteDeman[nDestinos]

							::incluiNecessidade(aRastreio[ADADOS_POS_TIPOPAI]          ,;
												aRastreio[ADADOS_POS_DOCPAI]           ,;
												cDestino                               ,;
												""                                     ,;
												aRastreio[ADADOS_POS_TRT]              ,;
												0                                      ,;
												nPeriodo                               ,;
												cList                                  ,;
												aRastreio[ADADOS_POS_REGRA_ALTERNATIVO],;
												PadL(nDestinos, 2, "0")                ,;
												aRastreio[ADADOS_POS_ROTEIRO]          ,;
												aRastreio[ADADOS_POS_OPERACAO]          )

						Next
					EndIf

					cChvLock := "dados_" + aRastreio[ADADOS_POS_DOCPAI];
										+ aRastreio[ADADOS_POS_COMPONENTE];
										+ aRastreio[ADADOS_POS_ID_OPCIONAL];
										+ aRastreio[ADADOS_POS_TRT]
					::oDados_Rastreio:oDados:unLock(cChvLock)

				Next
			EndIf

		EndIf
	EndIf

	For nIndex := 1 to Len(aDadosChvs)
		aSize(aDadosChvs[nIndex][2], 0)
		aDadosChvs[nIndex][2] := Nil

		aSize(aDadosChvs[nIndex], 0)
		aDadosChvs[nIndex] := Nil
	Next
	aSize(aDadosChvs, 0)
	aDadosChvs := Nil

	aSize(aRastreio, 0)
	aRastreio := Nil

	aSize(aAlteDeman, 0)
	aAlteDeman := Nil

Return

/*/{Protheus.doc} desfazExplosoes
Desfaz Explosoes Deste Componente
@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - cChaveProd , caracter, c�digo do componente com Opcional
@param 02 - nPeriodo   , n�mero  , identificador de per�odo
/*/
METHOD desfazExplosoes(cChaveProd, nPeriodo) CLASS MrpDominio_Rastreio

	Local aChaves
	Local aDados    := {}
	Local cList     := cChaveProd + chr(13) + cValToChar(nPeriodo)
	Local lError    := .F.
	Local nIndex
	Local nTotal    := 0

	If ::oDados_Rastreio:oDados:existAList(cList)
		aChaves  := ::oDados_Rastreio:getDocsComponente(cList)
		If aChaves == Nil
			aChaves := {}
		EndIf
		nTotal := Len(aChaves)
		If nTotal > 0
			//STR0133 - "Desfazendo explos�es de rastreabilidade do produto '"
			//STR0132 - "' e per�odo '"
			::oDados:oLogs:log(STR0133 + AllTrim(cChaveProd) + STR0132 + cValToChar(nPeriodo) + "'.", "R2")
		EndIf
		For nIndex := 1 to nTotal
			lError := .F.
			aDados := ::oDados_Rastreio:oDados:getItemAList(cList, aChaves[nIndex], lError)
			If !lError .and. !Empty(aDados)
				If !Empty(aDados[ADADOS_POS_DOCFILHO])
					::deletaRastreios(cList, aDados[ADADOS_POS_DOCFILHO], nPeriodo, aDados[ADADOS_POS_COMP_EXPL_ESTRUT])
				EndIf
				//TODO - REVISAR NECESSIDADE DE LIMPAR NUMERA��O CASO � EXISTA MAIS OP
				//aDados[ADADOS_POS_DOCFILHO     ]   := ""
				aDados[ADADOS_POS_CONSUMO_ESTOQUE] := 0
				aDados[ADADOS_POS_NECESSIDADE  ]   := Iif(aDados[ADADOS_POS_NEC_ORIGINAL] < 0, 0, aDados[ADADOS_POS_NEC_ORIGINAL])
				::oDados_Rastreio:oDados:setItemAList(cList, aChaves[nIndex], aDados)
			EndIf
		Next
	EndIf

	aSize(aDados, 0)
	aDados := Nil

Return

/*/{Protheus.doc} deletaRastreios
Exclui Rastreios relacionados ao cDocPai
@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - cList       , caracter, c�digo da lista: produto + chr(13) + per�odo
@param 02 - cChave      , caracter, c�digo do documento Pai + chr(13) + TRT
@param 03 - nPeriodo    , n�mero  , n�mero do per�odo relacionado
@param 04 - aComponentes, array   , componentes referente explos�o na estrutura
/*/
METHOD deletaRastreios(cList, cChave, nPeriodo, aComponentes) CLASS MrpDominio_Rastreio

	Local aDados       := {}
	Local aAuxComp
	Local cComponente
	Local dLeadTime
	Local nIndex
	Local nOldNec
	Local nPerLead
	Local nSaldo
	Local nTotal
	Local cChvLock
	Local lError       := .F.
	Local cListAux
	Local cChaveAux
	Local cChaveMAT
	Local cIDOpc

	Default aComponentes := ::oDados_Rastreio:getCmpDocPai(cList, cChave)

	nTotal    := If(aComponentes == Nil, 0, Len(aComponentes))

	If nTotal > 0
		//STR0134 - "Deletando rastreios da lista (Produto e Serie) '"
		//STR0135 - "' e chave (Documento e TRT)'"
		::oDados:oLogs:log(STR0134 + StrTran(cList, chr(13), " ") + STR0135 + cChave + "'.", "R3")
	EndIf

	For nIndex := 1 to nTotal
		lError      := .F.
		aDados      := {}
		cComponente := aComponentes[nIndex]                  //cComponente + chr(13) + cValToChar(nPeriodo) + chr(13) + cTRT + chr(13) + cChvSubst
		aAuxComp    := Strtokarr2(cComponente, chr(13), .T.)
		cListAux    := aAuxComp[1] + chr(13) + aAuxComp[2]
		cChaveAux   := cChave + chr(13) + aAuxComp[3] + chr(13) + aAuxComp[4]
		aDados      := ::oDados_Rastreio:oDados:getItemAList(cListAux, cChaveAux, @lError)
		If !lError
			cChvLock   := "dados_" + aDados[ADADOS_POS_DOCPAI];
					               + aDados[ADADOS_POS_COMPONENTE];
								   + aDados[ADADOS_POS_ID_OPCIONAL];
					               + aDados[ADADOS_POS_TRT]
			aSize(aDados, 0)
			::oDados_Rastreio:oDados:Lock(cChvLock)
			aDados     := ::oDados_Rastreio:oDados:getItemAList(cListAux, cChaveAux, @lError)
			If !lError
				cProduto   := aDados[ADADOS_POS_COMPONENTE]
				nPeriodo   := aDados[ADADOS_POS_PERIODO]
				cIDOpc     := aDados[ADADOS_POS_ID_OPCIONAL]
				cChaveMAT  := DtoS(::oDados:aPeriodos[nPeriodo]) + Iif(cProduto == Nil, "",cProduto + Iif(Empty(cIDOpc), "","|" + cIDOpc))
				nOldNec    := aDados[ADADOS_POS_NECESSIDADE]
				cRevAtuPai := aDados[ADADOS_POS_REVISAO]
				cChaveProd := aDados[ADADOS_POS_COMPONENTE] + Iif(cProduto == Nil, "",cProduto + Iif(Empty(cIDOpc), "","|" + cIDOpc))

				//Desfaz Substitui��es Matriz
				nSaldo     := ::oDados:retornaCampo("MAT", 1, cChaveMAT, "MAT_SALDO")
				::oAlternativo:desfazSubstituicoes(cProduto, nSaldo, nPeriodo)

				//Calcula LeadTime
				nPerLead  := nPeriodo
				dLeadTime := ::oDados:oDominio:aPeriodos[nPeriodo]
				::oDados:oDominio:oLeadTime:aplicar(cProduto, @nPerLead, @dLeadTime, -nOldNec)  //Aplica o Lead Time do produto

				//Desfaz Explos�o Matriz - Explosao negativa da estrutura: desfaz explos�es anteriores em caso de reprocessamento de alternativos ou fantasmas
				::oDados:gravaSaidaEstrutura(::oDados:aPeriodos[nPeriodo], cProduto, -aDados[ADADOS_POS_NEC_ORIGINAL], nPeriodo, cIDOpc)
				::oDados:oLogs:log(STR0036 + AllTrim(cChaveProd) + STR0026 + cValToChar(nPeriodo) + "/X" + STR0037 + cValToChar(nOldNec), "7") //"Explosao Estrutura (negativa):" + " - Periodos: " + " - Necessidade anterior: "
				::oDados:oDominio:explodirEstrutura(cProduto, -nOldNec, nPerLead, nPeriodo, .F., cIDOpc, , cRevAtuPai)

				//Recursividade para exclus�o de rastreios filhos
				If !lError .AND. !Empty(aDados[ADADOS_POS_DOCFILHO])
					cListAux := aDados[ADADOS_POS_COMPONENTE] + chr(13) + cValToChar(aDados[ADADOS_POS_PERIODO])
					::deletaRastreios(cListAux, aDados[ADADOS_POS_DOCFILHO], nPeriodo, aDados[ADADOS_POS_COMP_EXPL_ESTRUT])
				EndIf

				//DELETA Evento 009 - Saldo em estoque menor ou igual ao ponto de pedido
				If ::oDados:oParametros["lEventLog"] .AND. aDados[ADADOS_POS_TIPOPAI] == STR0144  //Pont. Ped.
					::oDados:oDominio:oEventos:loga("009", cChaveProd, ::oDados:aPeriodos[nPeriodo], {0, 0, ::oDados:aPeriodos[nPeriodo], "HWA"})

				//DELETA Evento 004 - Data de necessidade invalida - Data anterior a database (Novas necessidades do MRP com data anterior a base do sistema)
				ElseIf Self:oDados:oParametros["lEventLog"]
					Self:oDados:oDominio:oEventos:loga("004", cComponente, Self:oDados:aPeriodos[nPeriodo], {AllTrim(Left(cList, snTamCod)), -nOldNec, Self:oDados:aPeriodos[nPeriodo], aDados[ADADOS_POS_DOCPAI], "", ""})

				EndIf

				::oDados_Rastreio:oDados:delItemAList(cListAux, cChaveAux)
			EndIf
			::oDados_Rastreio:oDados:unLock(cChvLock)

			aSize(aDados, 0)

		EndIf
	Next

	aDados := Nil

	aSize(aComponentes, 0)
	aComponentes := Nil

Return

/*/{Protheus.doc} getPosicao
Retorna a posi��o do campo na tabela
@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - cCampo, caracter, string com o nome do campo relacionado aos dados de rastreabilidade
@Return nReturn, n�mero, posi��o padr�o do registro no array de dados
/*/
METHOD getPosicao(cCampo) CLASS MrpDominio_Rastreio
Return ::oDados_Rastreio:getPosicao(cCampo)

/*/{Protheus.doc} destruir
Destroi os objetos e variaveis desta classe
@author brunno.costa
@since 15/08/2019
@version 1.0
/*/
METHOD destruir() CLASS MrpDominio_Rastreio
Return ::oDados_Rastreio:oDados:destruir()

/*/{Protheus.doc} politicaDeEstoque
Realiza tratamentos de pol�tica de estoque
@author brunno.costa
@since 15/08/2019
@version 1.0
@param 01 - cProduto   , caracter, c�digo do produto para tratamento
@param 02 - nPeriodo   , n�mero  , per�odo para an�lise
@param 03 - nEstoque   , n�mero  , saldo inicial do produto
@param 04 - nSaidas    , n�mero  , quantidade de sa�das do per�odo
@return nEstoque, n�mero, retorna o saldo inicial para an�lise com as tratativas de estoque
/*/
METHOD politicaDeEstoque(cProduto, nPeriodo, nEstoque, nSaidas) CLASS MrpDominio_Rastreio
	Local aDados      := {}
	Local aAreaPRD    := {}
	Local cChave      := ""
	Local lAtual      := cProduto == ::oDados:oProdutos:cCurrentKey
	Local nEstSeg     := 0
	Local nPontoPed   := 0
	Local nPerUtil    := ::oDados:oDominio:oPeriodos:primeiroPeriodoUtil()

	If ::oParametros['lEstoqueSeguranca'] .Or. ::oParametros['lPontoPedido']
		If !lAtual
			aAreaPRD := Self:oDados:retornaArea("PRD")
		EndIf

		aDados    := Self:oDados:retornaCampo("PRD", 1, cProduto, {"PRD_ESTSEG","PRD_PPED"}, .F., lAtual, , , , , .T. /*lVarios*/)
		nEstSeg   := aDados[1]
		If ::oParametros['lPontoPedido']
			nPontoPed := aDados[2]
		EndIf

		aSize(aDados, 0)

		If !lAtual
			Self:oDados:setaArea(aAreaPRD)
		EndIf
	EndIf

	If nPerUtil == nPeriodo .And. nEstSeg > 0
		If ::oParametros['lEstoqueSeguranca']
			//Gera registro de rastreabilidade referente Estoque de Seguran�a
			If (nEstoque + nEstSeg) < nEstSeg
				cChave := chaveESeg(cProduto, nPeriodo, .F.)
				::incluiNecessidade(Left(STR0143, 10), cChave, cProduto, "", "", nEstSeg, nPeriodo)//"Est.Seg."
				Self:oDados:gravaCampo("PRD", 1, , {"PRD_SLDDIS", "PRD_ESTSEG"}, {nEstSeg, -nEstSeg}, .T., .T.,, .T. /*lVarios*/)
				nEstoque += nEstSeg
			EndIf
		Else
			nEstoque += nEstSeg
		EndIf
	EndIf

	//Gera registro de rastreabilidade referente Ponto de Pedido
	If ::oParametros['lPontoPedido'] .And. nPontoPed > 0
		If nPontoPed > (nEstoque - nSaidas)
			cChave := chavePPed(cProduto, nPeriodo, .F.)
			::incluiNecessidade(Left(STR0144, 10), cChave, cProduto, "", "", (nPontoPed - (nEstoque - nSaidas)), nPeriodo)//"Ponto Ped."

			//Gera Evento 009 - Saldo em estoque menor ou igual ao ponto de pedido
			If ::oDados:oParametros["lEventLog"]
				::oDados:oDominio:oEventos:loga("009", cProduto, ::oDados:aPeriodos[nPeriodo], {nPontoPed, (nEstoque - nSaidas), ::oDados:aPeriodos[nPeriodo], "HWA"})
			EndIf
		EndIf
	EndIf

	If nEstoque < 0
		nEstoque := 0
	EndIf

Return nEstoque

/*/{Protheus.doc} todosAltConsumidos
Identifica se todos os demais alternativos foram consumidos
@author brunno.costa
@since 15/08/2019
@version 1.0
@param 01 - cProdOri, caracter, c�digo do produto original
@param 02 - cExcecao, caracter, c�digo do produto alternativo atual, considerado como exce��o na regra
@param 03 - nPeriodo, n�mero  , per�odo relacionado
@return lReturn, l�gico, indica se todos os demais alternativos do produto original foram consumidos
/*/
METHOD todosAltConsumidos(cProdOri, cExcecao, nPeriodo) CLASS MrpDominio_Rastreio

	Local aChaves    := {}
	Local aAlteDeman := {} //C�digos dos Alternativos da Substitui��o
	Local aRastreio  := {}
	Local cAlternativo
	Local cList      := ""
	Local cOrigDeman := "KEY_" + AllTrim(cProdOri) + "_" + cValToChar(nPeriodo)//Original + Periodo
	Local cPrimeiro
	Local nInd
	Local nIndAlt
	Local lReturn    := .T.
	Local lError     := .F.

	//Atualiza controle de destinos de substituicao do produto origem
	aAlteDeman := ::oDados:oAlternativos:getItemAList("substituicoes_produtos", cOrigDeman)

	//Recupera registros das substituicoes
	For nInd := 1 to Len(aAlteDeman)
		cAlternativo := aAlteDeman[nInd]

		If nInd == 1
			cPrimeiro := cAlternativo
		EndIf

		//Desconsidera alternativo referente produto atual
		If AllTrim(cAlternativo) == AllTrim(cExcecao)
			Loop
		EndIf

		cList := cAlternativo + chr(13) + cValToChar(nPeriodo)
		aChaves := ::oDados_Rastreio:getDocsComponente(cList)
		For nIndAlt := 1 to Len(aChaves)
			aRastreio := {}
			aRastreio := ::oDados_Rastreio:oDados:getItemAList(cList, aChaves[nIndAlt], @lError)

			If !::oDados:foiCalculado(cAlternativo, nPeriodo)
				lReturn := .F.
				Exit

			ElseIf (aRastreio[ADADOS_POS_QTD_ESTOQUE] == (aRastreio[ADADOS_POS_CONSUMO_ESTOQUE] + aRastreio[ADADOS_POS_QTD_SUBSTITUICAO]));
			   .and. aRastreio[ADADOS_POS_QTD_SUBSTITUICAO] != aRastreio[ADADOS_POS_CONSUMO_SUBSTITU]
				lReturn := .F.
				Exit

			EndIf
		Next

		//Verifica se foi calculado
		If !lReturn
			Exit
		EndIf

	Next

	aSize(aChaves, 0)
	aChaves := Nil

	aSize(aAlteDeman, 0)
	aAlteDeman := Nil

	aSize(aRastreio, 0)
	aRastreio := Nil

Return lReturn

/*/{Protheus.doc} baixaPorOP
Percorre Rastreabilidade do Componente
Atribui baixas de estoque ao Rastreio
Prioridade de consumo de estoque: OP's (menor -> maior) e por �ltimo Ponto Pedido/Estoque de Seguran�a

@author    brunno.costa
@since     15/08/2019
@version   1
@param 01 - cComponente, caracter, c�digo do componente para an�lise
@param 02 - cIDOpc     , caracter, c�digo do ID do Opcional
@param 03 - nEstoque   , n�mero  , quantidade em estoque
@param 04 - nPeriodo   , n�mero  , per�odo para an�lise
@PARAM 05 - lAltReg1   , logico  , retorna por refer�ncia indicando se este produto � substituto (alternativo)
@param 06 - nSaidas    , n�mero  , quantidade de sa�das do per�odo
@return aBaixaPorOP, array   , array com os dados de rastreabilidade origem (Documentos Pais)
                               {{1 - Id Rastreabilidade,;
                                 2 - Documento Pai,;
                                 3 - Quantidade Necessidade,;
                                 4 - Quantidade Estoque,;
                                 5 - Quantidade Baixa Estoque,;
                                 6 - Quantidade Substitui��o
                                 7 - Array com as quebras de pol�ticas de estoque},...}
/*/
METHOD baixaPorOP(cComponente, cIDOpc, nEstoque, nPeriodo, lAltReg1, nSaidas) CLASS MrpDominio_Rastreio

	Local aBaixaPorOP := {}
	Local aChaves     := {}
	Local aDados      := {}
	Local aDadosChvs  := {}
	Local nContador   := 0
	Local aRastreio   := {}
	Local aNames      := {}
	Local cChave      := ""
	Local cList       := cComponente + Iif(!Empty(cIDOpc) , "|" + cIDOpc, "") + chr(13) + cValToChar(nPeriodo)
	Local cListOrig   := ""
	Local lError      := .F.
	Local nIndex      := 0
	Local nSubstitui  := 0
	Local nTotal      := 0
	Local oSldSubst

	//Aplica o rastreio relacionado a pol�ticas de estoque
	nEstoque := ::politicaDeEstoque(cComponente, nPeriodo, nEstoque, nSaidas)

	//Verifica chaves do Componente
	aChaves  := ::oDados_Rastreio:getDocsComponente(cList)
	nTotal   := Len(aChaves)

	//Recupera registros de rastreabilidade dos chaves
	For nIndex := 1 to nTotal
		lError    := .F.
		aDados    := {}
		cChave    := aChaves[nIndex]
		aDados    := ::oDados_Rastreio:oDados:getItemAList(cList, cChave, @lError)
		If !lError
			nContador++
            aAdd(aDadosChvs, {cChave, aDados, StrZero(nContador, 5)})
			cListOrig := ""
		EndIf
	Next

	//N�o existe chaves deste componente
	nTotal := Len(aDadosChvs)
	If nTotal == 0
		Return aBaixaPorOP
	EndIf

	//Atualiza Total de Registros V�lidos e Ordena por OP
	aDadosChvs := aSort(aDadosChvs, , , {|x,y| Iif(x[2][ADADOS_POS_NEC_ORIGINAL] < 0, "0", "1");
	                                             + Iif(x[2][ADADOS_POS_TIPOPAI] == "Pr�-OP", "0", "1");
	                                             + Iif(x[2][ADADOS_POS_TIPOPAI] == STR0144, "1", "0"); //Ponto Ped.
	                                             + PadL(x[2][ADADOS_POS_CHAVE_SUBSTITUICAO], 130);
	                                             + PadL(x[2][ADADOS_POS_DOCPAI], 100);
	                                             + x[3];
	                                             <;
	                                             Iif(y[2][ADADOS_POS_NEC_ORIGINAL] < 0, "0", "1");
	                                             + Iif(y[2][ADADOS_POS_TIPOPAI] == "Pr�-OP", "0", "1");
	                                             + Iif(y[2][ADADOS_POS_TIPOPAI] == STR0144, "1", "0"); //Ponto Ped.
	                                             + PadL(y[2][ADADOS_POS_CHAVE_SUBSTITUICAO], 130);
	                                             + PadL(y[2][ADADOS_POS_DOCPAI], 100);
	                                             + y[3] } )

	oSldSubst    := JsonObject():New()
	//Percorre Registros de Rastreabilidade
	For nIndex := 1 to nTotal
		cChave                            := aDadosChvs[nIndex][1]
		aRastreio                         := aDadosChvs[nIndex][2]
		aRastreio[ADADOS_POS_QTD_ESTOQUE] := nEstoque

		//N�o recalcula registro de necessidade para Ponto Pedido e Estoque de Seguran�a
		If (AllTrim(aRastreio[ADADOS_POS_TIPOPAI]) $ "|" + STR0144 + "|1|") //"Ponto Ped." | "PMP"

			//Monta array para retorno
			aAdd(aBaixaPorOP, {cChave,;
			                   aRastreio[ADADOS_POS_DOCPAI],;
			                   aRastreio[ADADOS_POS_NECESSIDADE],;
			                   aRastreio[ADADOS_POS_QTD_ESTOQUE],;
			                   aRastreio[ADADOS_POS_CONSUMO_ESTOQUE],;
			                   0,;
			                   aRastreio[ADADOS_POS_QUEBRAS_QUANTIDADE],;
	                                   aRastreio[ADADOS_POS_TIPOPAI],;
	                                   aRastreio[ADADOS_POS_NEC_ORIGINAL],;
	                                   aRastreio[ADADOS_POS_REGRA_ALTERNATIVO],;
	                                   aRastreio[ADADOS_POS_CHAVE_SUBSTITUICAO]})

		Else
			lError     := .F.
			If AllTrim(aRastreio[ADADOS_POS_TIPOPAI]) != STR0143 //Est.Seg.
				nSubstitui := aRastreio[ADADOS_POS_QTD_SUBSTITUICAO]

				//Acrescenta estoque referente estrutura negativa
				nEstoque   -= Iif(aRastreio[ADADOS_POS_NEC_ORIGINAL] < 0, aRastreio[ADADOS_POS_NEC_ORIGINAL], 0)

				//Recupera Quantidade da Substitui��o no Produto Original
				If !Empty(aRastreio[ADADOS_POS_CHAVE_SUBSTITUICAO])
					If (aRastreio[ADADOS_POS_REGRA_ALTERNATIVO] $ "|2|3|")
						::avaliaSubstOriginal(@aRastreio, @nEstoque, @nSubstitui, @oSldSubst)
					Else
						lAltReg1 := .T.
					EndIf
				Else
					nEstoque   += nSubstitui
				EndIf
			EndIf

			//Atualiza Baixas de Estoque - Regras que avaliam estoque do produto original
			If aRastreio[ADADOS_POS_NEC_ORIGINAL] > 0;
			   .AND. aRastreio[ADADOS_POS_REGRA_ALTERNATIVO] != "3"

				If nEstoque >= aRastreio[ADADOS_POS_NEC_ORIGINAL] .AND. nEstoque > 0
					nEstoque -= aRastreio[ADADOS_POS_NEC_ORIGINAL]
					aRastreio[ADADOS_POS_CONSUMO_ESTOQUE] := aRastreio[ADADOS_POS_NEC_ORIGINAL]

				ElseIf (aRastreio[ADADOS_POS_CONSUMO_ESTOQUE] + nEstoque) <= aRastreio[ADADOS_POS_QTD_ESTOQUE]
					aRastreio[ADADOS_POS_CONSUMO_ESTOQUE] += nEstoque
					aRastreio[ADADOS_POS_NECESSIDADE]     := aRastreio[ADADOS_POS_NEC_ORIGINAL] -nEstoque
					nEstoque := 0

				EndIf
			EndIf

			//Recalcula Necessidade
			aRastreio[ADADOS_POS_NECESSIDADE] := Iif(aRastreio[ADADOS_POS_NEC_ORIGINAL] < 0, 0, aRastreio[ADADOS_POS_NEC_ORIGINAL])
			aRastreio[ADADOS_POS_NECESSIDADE] -= aRastreio[ADADOS_POS_QTD_SUBSTITUICAO]
			If aRastreio[ADADOS_POS_REGRA_ALTERNATIVO] != "3" .OR. nSubstitui < 0
				aRastreio[ADADOS_POS_NECESSIDADE] -= aRastreio[ADADOS_POS_QTD_ESTOQUE]

				//Desconta necessidade original negativa
				If aRastreio[ADADOS_POS_NECESSIDADE] > 0 .AND. aRastreio[ADADOS_POS_NEC_ORIGINAL] < 0
					aRastreio[ADADOS_POS_NECESSIDADE] += aRastreio[ADADOS_POS_NEC_ORIGINAL]
				EndIf
			EndIf
			aRastreio[ADADOS_POS_NECESSIDADE] := Iif(aRastreio[ADADOS_POS_NECESSIDADE] < 0, 0, aRastreio[ADADOS_POS_NECESSIDADE])

			//Monta array para retorno
			aAdd(aBaixaPorOP, {cChave,;
							   aRastreio[ADADOS_POS_DOCPAI],;
							   aRastreio[ADADOS_POS_NECESSIDADE],;
							   aRastreio[ADADOS_POS_QTD_ESTOQUE],;
							   aRastreio[ADADOS_POS_CONSUMO_ESTOQUE],;
							   nSubstitui,;
							   aRastreio[ADADOS_POS_QUEBRAS_QUANTIDADE],;
							   aRastreio[ADADOS_POS_TIPOPAI],;
							   aRastreio[ADADOS_POS_NEC_ORIGINAL],;
							   aRastreio[ADADOS_POS_REGRA_ALTERNATIVO],;
							   aRastreio[ADADOS_POS_CHAVE_SUBSTITUICAO]})

			//Atualiza registro global
			::oDados_Rastreio:oDados:createAList(cList)
			::oDados_Rastreio:oDados:setItemAList(cList, cChave, aRastreio, @lError)
		EndIf

	Next

	For nIndex := 1 to Len(aDadosChvs)
		aSize(aDadosChvs[nIndex][2], 0)
		aDadosChvs[nIndex][2] := Nil

		aSize(aDadosChvs[nIndex], 0)
		aDadosChvs[nIndex] := Nil
	Next
	aSize(aDadosChvs, 0)
	aDadosChvs := Nil

	aNames := oSldSubst:GetNames()
	nTotal := Len(aNames)
	For nIndex := 1 To nTotal
		oSldSubst[aNames[nIndex]] := Nil
	Next nIndex
	aSize(aNames, 0)
	aNames := Nil

	FreeObj(oSldSubst)
	oSldSubst := Nil

	aSize(aRastreio, 0)
	aRastreio := Nil

Return aBaixaPorOP

/*/{Protheus.doc} avaliaSubstOriginal
Avalia e recupera a quantidade de substitui��o deste alternativo no produto original
@author brunno.costa
@since 15/08/2019
@version 1.0
@param 01 - aAlternati , array , retorna por refer�ncia array com o registro de rastreabilidade referente produto alternativo availiado
@param 02 - nEstoque   , n�mero, retorna por refer�ncia quantidade de estoque inicial deste produto alternativo
@param 03 - nSubstAlt  , n�mero, retorna por refer�ncia quantidade substitu�da do produto original para este alternativo
@param 04 - oSldSubst  , objeto, objeto Json que controla os saldos de substitui��o por produto + per�odo + alternativo
@param 05 - lForcaOrig , l�gico, indica se for�a rec�lculo do produto original
@param 06 - aBaixaPorOP, array , array aBaixasPorOP relacionado a opera��o
@param 07 - lAltReg1   , l�gico, indica se refere-se a registro de produto alternativo com regra do tipo 1
/*/
METHOD avaliaSubstOriginal(aAlternati, nEstoque, nSubstAlt, oSldSubst, lForcaOrig, aBaixaPorOP, lAltReg1) CLASS MrpDominio_Rastreio

	Local aOriginal         := {}
	lOCAL aListOrig         := StrTokArr(aAlternati[ADADOS_POS_CHAVE_SUBSTITUICAO], chr(13) )
	Local cAlternativo      := aAlternati[ADADOS_POS_COMPONENTE]
	Local cChave
	Local cChaveSubs
	Local cChvLock
	Local cListOrig         := aListOrig[1] + chr(13) + aListOrig[2]
	Local lAllAltCons       := .F.
	Local lError            := .F.
	Local nDesfazSub        := 0
	Local nFalta            := 0
	Local nFaltaOrig        := 0
	Local nPeriodo          := aAlternati[ADADOS_POS_PERIODO]
	Local nSubstIni         := nSubstAlt
	Local nSubstOrig        := 0
	Local lNewKey           := .F.

	Default lForcaOrig  := .F.
	Default aBaixaPorOP := {}
	Default lAltReg1    := .F.

	cChave    := aAlternati[ADADOS_POS_DOCPAI] + chr(13) + aAlternati[ADADOS_POS_TRT] + chr(13) + ""
	cChvLock  := cListOrig + cChave

	::oDados_Rastreio:oDados:lock(cChvLock)
	aOriginal := ::oDados_Rastreio:oDados:getItemAList(cListOrig, cChave, @lError)

	If !lError

		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nSubstIni: " + cValToChar(nSubstIni), "R4")

		cChaveSubs   := "KEY_" + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + "_" + cValToChar(nPeriodo) + "_" + AllTrim(cAlternativo)
		If oSldSubst[cChaveSubs] == Nil
			lNewKey                := .T.
			oSldSubst[cChaveSubs] := ::oDados:oAlternativos:getItemAList("substituicoes_dados", cChaveSubs, @lError)[5] //{1-cProduto, 2-cAlternativo, 3-nPeriodo, 4-nConsAlt, 5-nConsOrig, 6-cOrdem, 7-nLimiteAlt, 8-nLimiteOrig}
		EndIf

		//Recupera Consumo Anterior (Sem a Substitui��o Atual)
		If nSubstAlt != 0
			nSubstOrig := ::oAlternativo:aplicaProdutoFator(aOriginal[ADADOS_POS_COMPONENTE], cAlternativo, nSubstAlt, .T.)
			::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nSubstOrig: " + cValToChar(nSubstOrig), "RS")
			If !lNewKey
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "oSldSubst[cChaveSubs]: " + cValToChar(oSldSubst[cChaveSubs]), "RS")
				oSldSubst[cChaveSubs] -= nSubstOrig
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "oSldSubst[cChaveSubs]: " + cValToChar(oSldSubst[cChaveSubs]), "RS")
			EndIf
		EndIf

		If aOriginal[ADADOS_POS_QTD_SUBSTITUICAO] > 0
			If (aOriginal[ADADOS_POS_QTD_SUBSTITUICAO] - aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]) > 0
				nFaltaOrig := aOriginal[ADADOS_POS_QTD_SUBSTITUICAO] - aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]
			EndIf

			//Recompoe Consumo Substitui��o Sem este Alternativo
			::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nFaltaOrig: " + cValToChar(nFaltaOrig), "RS")
			::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nSubstOrig: " + cValToChar(nSubstOrig), "RS")
			::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]: " + cValToChar(aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]), "RS")
			aOriginal[ADADOS_POS_CONSUMO_SUBSTITU] += nSubstOrig
			aOriginal[ADADOS_POS_CONSUMO_SUBSTITU] := Iif(aOriginal[ADADOS_POS_CONSUMO_SUBSTITU] < 0, 0, aOriginal[ADADOS_POS_CONSUMO_SUBSTITU])
			::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]: " + cValToChar(aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]), "RS")
		EndIf


		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nFaltaOrig: " + cValToChar(nFaltaOrig), "R4")
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nSubstOrig: " + cValToChar(nSubstOrig), "R4")

		If oSldSubst[cChaveSubs] < nFaltaOrig  .AND. oSldSubst[cChaveSubs] >= 0//Saldo substitui��o deste documento+periodo+ALTERNATIVO < nFalta
			nFalta    := ::oAlternativo:aplicaProdutoFator(aOriginal[ADADOS_POS_COMPONENTE], cAlternativo, oSldSubst[cChaveSubs], .F.)
		Else
			nFalta    := ::oAlternativo:aplicaProdutoFator(aOriginal[ADADOS_POS_COMPONENTE], cAlternativo, nFaltaOrig, .F.)
		Endif
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "oSldSubst[cChaveSubs]: " + cValToChar(oSldSubst[cChaveSubs]), "R4")
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nFalta: " + cValToChar(nFalta), "R4")
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nEstoque: " + cValToChar(nEstoque), "R4")

		//Substitui toda necessidade faltante - Estoque Dispon�vel
		If nFalta > 0 .AND. (nEstoque - nFalta) >= 0
			nSubstAlt  := -nFalta

		 //Consome somente a quantidade em estoque
		ElseIf nFalta > 0 .AND.  nEstoque > 0 .AND. !(aOriginal[ADADOS_POS_REGRA_ALTERNATIVO] $ "|2|3|")
			nSubstAlt  := -nEstoque

		//Mant�m substitui��o original
		ElseIf nFalta == 0
			nSubstAlt := nSubstIni

		//Substitui toda necessidade faltante - Estoque Indispon�vel
		Else
			nSubstAlt := -nFalta

		EndIf

		//Identifica quantidade do produto original substitu�da
		nSubstOrig := ::oAlternativo:aplicaProdutoFator(aOriginal[ADADOS_POS_COMPONENTE], cAlternativo, nSubstAlt, .T.)

		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nSubstOrig: " + cValToChar(nSubstOrig), "R4")
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nSubstAlt: " + cValToChar(nSubstAlt), "R4")

		//For�a rec�lculo do produto Original - Ap�s Pol�tica de Estoque
		If lForcaOrig
			lAllAltCons := ::todosAltConsumidos(aOriginal[ADADOS_POS_COMPONENTE], cAlternativo, nPeriodo)
			If lAllAltCons
				nDesfazSub                             := -nSubstOrig
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nDesfazSub: " + cValToChar(nDesfazSub), "RS")
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_QTD_SUBSTITUICAO]: " + cValToChar(aOriginal[ADADOS_POS_QTD_SUBSTITUICAO]), "RS")
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]: " + cValToChar(aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]), "RS")
				aOriginal[ADADOS_POS_QTD_SUBSTITUICAO] -= nDesfazSub
				aOriginal[ADADOS_POS_CONSUMO_SUBSTITU] += nSubstOrig
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_QTD_SUBSTITUICAO]: " + cValToChar(aOriginal[ADADOS_POS_QTD_SUBSTITUICAO]), "RS")
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]: " + cValToChar(aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]), "RS")

			Else
				lForcaOrig := .F.
				aOriginal[ADADOS_POS_CONSUMO_SUBSTITU] += nSubstOrig
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nSubstOrig: " + cValToChar(nSubstOrig), "RS")
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]: " + cValToChar(aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]), "RS")

			EndIf


		//Avalia necessidade de rec�lculo do produto Original - Regra do Tipo 1
		ElseIf (nFaltaOrig + nSubstOrig) > 0 .AND. lAltReg1
			lAllAltCons := ::todosAltConsumidos(aOriginal[ADADOS_POS_COMPONENTE], cAlternativo, nPeriodo)
			If lAllAltCons
				nDesfazSub                             := (nFaltaOrig + nSubstOrig)
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nDesfazSub: " + cValToChar(nDesfazSub), "RS")
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_QTD_SUBSTITUICAO]: " + cValToChar(aOriginal[ADADOS_POS_QTD_SUBSTITUICAO]), "RS")
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]: " + cValToChar(aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]), "RS")
				aOriginal[ADADOS_POS_QTD_SUBSTITUICAO] -= nDesfazSub
				lForcaOrig                             := .T.
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_QTD_SUBSTITUICAO]: " + cValToChar(aOriginal[ADADOS_POS_QTD_SUBSTITUICAO]), "RS")
				::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]: " + cValToChar(aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]), "RS")

			EndIf

		//For�a substitui��o do produto - Regras do Tipo 2 ou 3
		ElseIf aOriginal[ADADOS_POS_REGRA_ALTERNATIVO] $ "|2|3|" .AND. (nFaltaOrig + nSubstOrig) < 0
			lAllAltCons := ::todosAltConsumidos(aOriginal[ADADOS_POS_COMPONENTE], cAlternativo, nPeriodo)
			If lAllAltCons .AND. nFalta > 0
				nSubstOrig := 0
				nSubstAlt  := 0
				nFaltaAlt  := ::oAlternativo:aplicaProdutoFator(aOriginal[ADADOS_POS_COMPONENTE], cAlternativo, nFalta, .F.)
				::forcaSubstituicaoAlternativo(aOriginal[ADADOS_POS_COMPONENTE], nPeriodo, nFalta, nFaltaAlt, aOriginal, cAlternativo, @nSubstOrig, @nSubstAlt, nSubstIni)
			EndIf

		EndIf
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nSubstOrig: " + cValToChar(nSubstOrig), "R4")

		//Atualiza estoque final do produto alternativo
		nEstoque += nSubstAlt
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nEstoque: " + cValToChar(nEstoque), "R4")

		//Abate do controle de substitui��o Json Documento + Componente
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "nSubstIni: " + cValToChar(nSubstIni), "RS")
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "oSldSubst[cChaveSubs]: " + cValToChar(oSldSubst[cChaveSubs]), "RS")
		oSldSubst[cChaveSubs] += nSubstOrig
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "oSldSubst[cChaveSubs]: " + cValToChar(oSldSubst[cChaveSubs]), "RS")

		//Atualiza Saldo de Substitui��o no Registro do Produto Original
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]: " + cValToChar(aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]), "RS")
		aOriginal[ADADOS_POS_CONSUMO_SUBSTITU] -= nSubstOrig
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + "aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]: " + cValToChar(aOriginal[ADADOS_POS_CONSUMO_SUBSTITU]), "RS")

		//Atualiza registro produto Original
		::oDados_Rastreio:oDados:setItemAList(cListOrig, cChave, aOriginal)

		//Forca recalculo do produto Original devido a necessidade de desfazer parte da substituicao
		If lForcaOrig
			::forcaRecalculoOriginal(aOriginal[ADADOS_POS_COMPONENTE], nDesfazSub, nPeriodo, cAlternativo, @aBaixaPorOP)
		EndIf

		//STR0136 - "Avaliando a substitui��o do Alternativo: '"
		//STR0137 - "' - Original: '"
		//STR0138 - "'. Documento '"
		//STR0126 - "TRT: "
		::oDados:oLogs:log(STR0136 + AllTrim(aAlternati[ADADOS_POS_COMPONENTE]) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + " ) " + STR0132 + cValToChar(nPeriodo) + "': " + cValToChar(nSubstAlt), "R4")

		//Atualiza substitui��o
		aAlternati[ADADOS_POS_QTD_SUBSTITUICAO]   := nSubstAlt
		aAlternati[ADADOS_POS_QTD_SUBST_ORIGINAL] := nSubstOrig

		aSize(aOriginal, 0)
		aOriginal := Nil


	EndIf
	::oDados_Rastreio:oDados:unlock(cChvLock)

Return Nil

/*/{Protheus.doc} forcaSubstituicaoAlternativo
For�a Substitui��o para o Primeiro Alternativo
@author brunno.costa
@since 15/08/2019
@version 1.0
@param 01 - cProdOri    , caracter, c�digo do produto original
@param 02 - nPeriodo    , n�mero  , per�odo relacionado
@param 03 - nQtdOrig    , n�mero  , quantidade para for�ar substitui��o - Fator referente Produto Original
@param 04 - nQtdAlt     , n�mero  , quantidade para for�ar substitui��o - Fator referente Produto Alternativo
@param 05 - aOriginal   , array   , array com os dados de rastreabilidade do registro original da substitui��o
@param 06 - cAlternativo, caracter, c�digo do produto alternativo em an�lise
@param 07 - nSubstOrig  , n�mero  , recebe e retorna por refer�ncia a quantidade de substitui��o atual - Fator referente Produto Original
@param 08 - nSubstAlt   , n�mero  , recebe e retorna por refer�ncia a quantidade de substitui��o atual - Fator referente Produto Alternativo
@param 09 - nSubstIni   , n�mero  , quantidade de substitui��o inicial - pr�via a avaliaSubstOriginal  - Fator referente Produto Alternativo
/*/
METHOD forcaSubstituicaoAlternativo(cProdOri, nPeriodo, nQtdOrig, nQtdAlt, aOriginal, cAlternativo, nSubstOrig, nSubstAlt, nSubstIni) CLASS MrpDominio_Rastreio

	Local aCampos    := {"ALT_ALTERN", "ALT_DATA"}
	Local aCposAlt   := {}
	Local aRastreio  := {}
	Local nSequencia := 1
	Local cChvAltern
	Local cChave     := aOriginal[ADADOS_POS_DOCPAI] + chr(13) + aOriginal[ADADOS_POS_TRT] + chr(13) + aOriginal[ADADOS_POS_COMPONENTE] + chr(13) + cValToChar(aOriginal[ADADOS_POS_PERIODO])
	Local cList
	Local lError
	Local cPriAltern

	//Identifica primeiro alternativo
	lError     := .T.
	While lError
		lError     := .F.
		cChvAltern := cProdOri + cValToChar(nSequencia)
		aCposAlt := ::oDados:retornaCampo("ALT", 1, cChvAltern, aCampos, @lError,,,,,,.T.)
		nSequencia++

		If !lError .AND. !::oAlternativo:validaVigencia(nPeriodo, aCposAlt[2])
			lError     := .T.
		EndIf

	EndDo

	If !lError
		cPriAltern := aCposAlt[1]
	EndIf

	//Chave da sess�o do primeiro alternativo
	cList      := cPriAltern + chr(13) + cValToChar(nPeriodo)
	lError     := .F.

	::incluiNecessidade(aOriginal[ADADOS_POS_TIPOPAI]           ,;
						aOriginal[ADADOS_POS_DOCPAI]            ,;
						cPriAltern                              ,;
						""                                      ,;
						aOriginal[ADADOS_POS_TRT]               ,;
						0                                       ,;
						nPeriodo                                ,;
						aOriginal[ADADOS_POS_CHAVE_SUBSTITUICAO],;
						aOriginal[ADADOS_POS_REGRA_ALTERNATIVO] ,;
						aOriginal[ADADOS_POS_ROTEIRO]           ,;
						aOriginal[ADADOS_POS_OPERACAO]           )

	//Primeiro alternativo corresponde ao registro atual
	If cAlternativo == cPriAltern
		nSubstOrig -= nQtdOrig
		nSubstAlt  -= nQtdAlt

		//STR0139 - "For�a substitui��o deste Alternativo '"
		//STR0137 - "' - Original: '"
		//STR0138 - "'. Documento: '"
		//STR0126 - "TRT: "
		::oDados:oLogs:log(STR0139 + AllTrim(cAlternativo) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + ")': " + cValToChar(nQtdOrig) + " <- " + cValToChar(nQtdAlt), "R9")

	//Alternativo corresponde a outro registro
	Else

		aRastreio := ::oDados_Rastreio:oDados:getItemAList(cList, cChave, @lError)

		If !lError
			////Se conseguiu recuperar o registro, faz o lock e atualiza os dados de acordo com o aBaixaPorOP
			//cChvLock := "dados_" + aRastreio[ADADOS_POS_DOCPAI];
			//					 + aRastreio[ADADOS_POS_COMPONENTE];
			//					 + aRastreio[ADADOS_POS_ID_OPCIONAL];
			//					 + aRastreio[ADADOS_POS_TRT]
			//::oDados_Rastreio:oDados:lock(cChvLock)
			//aRastreio := ::oDados_Rastreio:oDados:getItemAList(cList, cChave, @lError)
			//If !lError
				aRastreio[ADADOS_POS_QTD_SUBSTITUICAO] -= nQtdOrig
				//STR0140 - "For�a substitui��o DO Alternativo '"
				//STR0137 - "' - Original: '"
				//STR0138 - "'. Documento: '"
				//STR0126 - "TRT: "
				::oDados:oLogs:log(STR0140 + AllTrim(cPriAltern) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + ")': " + cValToChar(nQtdOrig) + " <- " + cValToChar(nQtdAlt), "R9")
				::oDados:oLogs:log(STR0140 + AllTrim(cPriAltern) + STR0137 + AllTrim(aOriginal[ADADOS_POS_COMPONENTE]) + STR0138 + aOriginal[ADADOS_POS_DOCPAI] + " (" + STR0126 + AllTrim(aOriginal[ADADOS_POS_TRT]) + ")': " + cValToChar(aRastreio[ADADOS_POS_QTD_SUBSTITUICAO]) , "R9")


				//Recalcula Necessidade
				aRastreio[ADADOS_POS_NECESSIDADE] := Iif(aRastreio[ADADOS_POS_NEC_ORIGINAL] < 0, 0, aRastreio[ADADOS_POS_NEC_ORIGINAL])
				aRastreio[ADADOS_POS_NECESSIDADE] -= aRastreio[ADADOS_POS_QTD_SUBSTITUICAO]
				aRastreio[ADADOS_POS_NECESSIDADE] -= aRastreio[ADADOS_POS_QTD_ESTOQUE]
				aRastreio[ADADOS_POS_NECESSIDADE] := Iif(aRastreio[ADADOS_POS_NECESSIDADE] < 0, 0, aRastreio[ADADOS_POS_NECESSIDADE])

				//Desconta necessidade original negativa
				If aRastreio[ADADOS_POS_NECESSIDADE] > 0 .AND. aRastreio[ADADOS_POS_NEC_ORIGINAL] < 0
					aRastreio[ADADOS_POS_NECESSIDADE] += aRastreio[ADADOS_POS_NEC_ORIGINAL]
				EndIf

				//Altera rastreio do alternativo
				::oDados_Rastreio:oDados:setItemAList(cList, cChave, aRastreio)
			//EndIf
			//::oDados_Rastreio:oDados:unlock(cChvLock)
		EndIf

		//For�a recalculo do produto alternativo para re-explodir a estrutura
		::oDados:gravaPeriodosProd(cPriAltern, nPeriodo)

		//Decrementa totalizador relacionado ao oDominio:loopNiveis
		If !::oDados:possuiPendencia(cPriAltern, .T.)
			::oDados:decrementaTotalizador(cPriAltern)
		EndIf

	EndIf

Return

/*/{Protheus.doc} forcaRecalculoOriginal
For�a Rec�lculo do Produto Original neste Per�odo
@author brunno.costa
@since 13/02/2020
@version 1.0
@param 01 - cProduto    , caracter, c�digo do produto original
@param 02 - nSaldo      , n�mero  , recebe e retorna por refer�ncia o saldo do produto p�s desfazer substitui��o parcial
@param 03 - nPeriodo    , n�mero  , c�digo do per�odo relacionado
@param 04 - cAlternativo, caracter, c�digo do produto alternativo que deve desfazer a substitui��o
@param 05 - aBaixaPorOP , n�mero  , recebe e retorna por refer�ncia array aBaixaPorOP relacionado a opera��o
/*/
METHOD forcaRecalculoOriginal(cProduto, nSaldo, nPeriodo, cAlternativo, aBaixaPorOP) CLASS MrpDominio_Rastreio

	//Desfaz substitui��o - Matriz e Rastreio
	::oAlternativo:desfazParcialSubstituicao(cProduto, cAlternativo, nSaldo, nPeriodo, @aBaixaPorOP)

	//Altera produto para rec�lculo
	::oDados:gravaPeriodosProd(cProduto, nPeriodo, , (nPeriodo - 1))

	//Decrementa totalizador relacionado ao oDominio:loopNiveis
	If !::oDados:possuiPendencia(cProduto, .T.)
		::oDados:decrementaTotalizador(cProduto)
	EndIf

Return

/*/{Protheus.doc} atualizaRastreio
Atualiza os dados do rastreio de acordo com os dados existentes no array aBaixaPorOP

@author lucas.franca
@since 23/10/2019
@version 1.0
@param 01 - cProduto    , caracter, c�digo do produto
@param 02 - nPeriodo    , n�mero  , per�odo relacionado
@param 03 - aBaixaPorOP , array   , array com os dados de rastreabilidade origem (Documentos Pais)
							   {{1 - Id Rastreabilidade,;
							     2 - Documento Pai,;
							     3 - Quantidade Necessidade,;
							     4 - Quantidade Estoque,;
							     5 - Quantidade Baixa Estoque,;
							     6 - Quantidade Substitui��o
								 7 - Array com as quebras de pol�ticas de estoque},...}
@param 04 - aIndexAtu   , Array   , Array com as posi��es do aBaixaPorOP que precisam ser atualizadas. (Opcional)
/*/
METHOD atualizaRastreio(cProduto, nPeriodo, aBaixaPorOP, aIndexAtu) CLASS MrpDominio_Rastreio
	Local aDocsPai    := {}
	Local aRastreio   := {}
	Local cChvLock    := ""
	Local cList       := cProduto + chr(13) + cValToChar(nPeriodo)
	Local lError      := .F.
	Local lUsaIndex   := .F.
	Local lAglutina
	Local nIndex      := 0
	Local nIndBaixa   := 0
	Local nIndDoc     := 0
	Local nIndQuebra  := 0
	Local nQtdQuebra  := 0
	Local nQtdHWG     := 0
	Local nQtdPai
	Local nTotDocs    := 0
	Local nTotal      := Len(aBaixaPorOP)
	Local nTotQuebra  := 0
	Local oAglutina   := ::oDados:oDominio:oAglutina

	If nTotal > 0
		//o array aIndexAtu cont�m os �ndices do array aBaixaPorOP que realmente precisam ser atualizados.
		//Se este array n�o existir, ser� feita a atualiza��o de todos os �ndices do aBaixaPorOP recebido.
		If aIndexAtu != Nil .And. Len(aIndexAtu) > 0
			lUsaIndex := .T.
			nTotal    := Len(aIndexAtu)
		EndIf
		For nIndex := 1 To nTotal
			If lUsaIndex
				nIndBaixa := aIndexAtu[nIndex]
			Else
				nIndBaixa := nIndex
			EndIf
			//Recupera o registro de rastreabilidade
			lError    := .F.
			aRastreio := ::oDados_Rastreio:oDados:getItemAList(cList, aBaixaPorOP[nIndBaixa][1], @lError)
			If !lError
				//Se conseguiu recuperar o registro, faz o lock e atualiza os dados de acordo com o aBaixaPorOP
				cChvLock := "dados_" + aRastreio[ADADOS_POS_DOCPAI];
									 + aRastreio[ADADOS_POS_COMPONENTE];
									 + aRastreio[ADADOS_POS_ID_OPCIONAL];
									 + aRastreio[ADADOS_POS_TRT]
				::oDados_Rastreio:oDados:lock(cChvLock)
				//aRastreio := ::oDados_Rastreio:oDados:getItemAList(cList, aBaixaPorOP[nIndex][1], @lError)

				aRastreio[ADADOS_POS_NEC_ORIGINAL      ] := aBaixaPorOP[nIndBaixa][ABAIXA_POS_NEC_ORIG          ]
				aRastreio[ADADOS_POS_NECESSIDADE       ] := aBaixaPorOP[nIndBaixa][ABAIXA_POS_NECESSIDADE       ]
				aRastreio[ADADOS_POS_QTD_ESTOQUE       ] := aBaixaPorOP[nIndBaixa][ABAIXA_POS_QTD_ESTOQUE       ]
				aRastreio[ADADOS_POS_CONSUMO_ESTOQUE   ] := aBaixaPorOP[nIndBaixa][ABAIXA_POS_CONSUMO_ESTOQUE   ]

				aRastreio[ADADOS_POS_QTD_SUBSTITUICAO  ] := aBaixaPorOP[nIndBaixa][ABAIXA_POS_QTD_SUBSTITUICAO  ]
				aRastreio[ADADOS_POS_QUEBRAS_QUANTIDADE] := aClone(aBaixaPorOP[nIndBaixa][ABAIXA_POS_QUEBRAS_QUANTIDADE])

				//Atualiza aglutina��o do produto
				lAglutina := oAglutina:avaliaAglutinacao(cProduto, .F.)
				If lAglutina
					nTotQuebra := Len(aRastreio[ADADOS_POS_QUEBRAS_QUANTIDADE])
					If nTotQuebra != 1
						oAglutina:reiniciaAglutinacaoDocumentoPai("4",;
																  aRastreio[ADADOS_POS_TIPOPAI],;
																  aRastreio[ADADOS_POS_DOCPAI],;
																  aRastreio[ADADOS_POS_COMPONENTE],;
																  aRastreio[ADADOS_POS_TRT],;
																  aRastreio[ADADOS_POS_PERIODO],;
																  @aDocsPai)
						For nIndQuebra := 1 to nTotQuebra
							cDocPai := aBaixaPorOP[nIndBaixa][ABAIXA_POS_DOCPAI]
							cDocPai += chr(13) + cValToChar(nIndQuebra)

							nQtdQuebra := aRastreio[ADADOS_POS_QUEBRAS_QUANTIDADE][nIndQuebra][1]

							//Distribui as quebras por documento originador
							For nIndDoc := 1 to nTotDocs
								nQtdPai    := aDocsPai[nIndDoc][3]
								If nQtdPai == 0  //Ignora documento originador com quantidade zerada
									Loop
								EndIf

								//Identifica quantidade da aglutina��o referente esta quebra e documento
								nQtdHWG    := Iif(nQtdQuebra < nQtdPai, nQtdQuebra, nQtdPai)

								oAglutina:prepara("4",;
											      aRastreio[ADADOS_POS_TIPOPAI],;
												  {aDocsPai[nIndDoc][1], aDocsPai[nIndDoc][2], nQtdHWG},;
											      aRastreio[ADADOS_POS_DOCPAI],;
											      aRastreio[ADADOS_POS_COMPONENTE],;
												  aRastreio[ADADOS_POS_ID_OPCIONAL],;
											      aRastreio[ADADOS_POS_TRT],;
											      nQtdHWG,;
											      aRastreio[ADADOS_POS_PERIODO],;
											      aRastreio[ADADOS_POS_REGRA_ALTERNATIVO],;
											      .F.,;
											      .F.,;
											      .T.)

								//Desconta quantidade da quebra da necessidade no documento originador
								aDocsPai[nIndDoc][3] -= nQtdHWG

								If nQtdPai < nQtdQuebra
									nQtdQuebra -= nQtdPai
								Else
									Exit
								EndIf
							Next
						Next
					EndIf
				EndIf

				//Atualiza rastreabilidade do produto
				::oDados_Rastreio:oDados:setItemAList(cList, aBaixaPorOP[nIndBaixa][1], aRastreio)
				::oDados_Rastreio:oDados:unLock(cChvLock)

				aSize(aRastreio, 0)
			EndIf
		Next
	EndIf
Return

/*/{Protheus.doc} necPPed_ES
Retorna qual � a quantidade de necessidade por ponto de pedido + estoque de seguran�a para o produto no per�odo.

@author lucas.franca
@since 24/03/2020
@version 1.0
@param 01 - cProduto, caracter, c�digo do produto
@param 02 - nPeriodo, n�mero  , per�odo relacionado
@return nNecess   , n�mero  , Quantidade de necessidade por ponto de pedido
/*/
METHOD necPPed_ES(cProduto, nPeriodo) CLASS MrpDominio_Rastreio
	Local aDados  := {}
	Local cList   := ""
	Local cChave  := ""
	Local lError  := .F.
	Local nNecess := 0

	//Recupera ID do Registro (ou Insere Novo e Recupera)
	cList := cProduto + chr(13) + cValToChar(nPeriodo)

	cChave := chavePPed(cProduto, nPeriodo, .T.)
	aDados := ::oDados_Rastreio:oDados:getItemAList(cList, cChave, @lError)
	If !lError
		nNecess += aDados[ADADOS_POS_NECESSIDADE]
		aSize(aDados, 0)
	EndIf
	lError := .F.
	If nPeriodo == 1
		cChave := chaveESeg(cProduto, nPeriodo, .T.)
		aDados := ::oDados_Rastreio:oDados:getItemAList(cList, cChave, @lError)
		If !lError
			nNecess += aDados[ADADOS_POS_NECESSIDADE]
			aSize(aDados, 0)
		EndIf
	EndIf

Return nNecess

/*/{Protheus.doc} chavePPed
Monta a chave de registro para utilizar na rastreabilidade do ponto de pedido.

@type  Static Function
@author lucas.franca
@since 24/03/2020
@version P12.1.30
@param 01 - cProduto , caracter, c�digo do produto
@param 02 - nPeriodo , n�mero  , per�odo relacionado
@param 03 - lCompleta, logico  , indica que ir� montar a chave completa para utiliza��o no getItemAList
@return cChave, character, Chave de registro para utilizar no ponto de pedido.
/*/
Static Function chavePPed(cProduto, nPeriodo, lCompleta)
	Local cChave := ""
	cChave := STR0144 + "_" + AllTrim(cProduto) + "_" + cValToChar(nPeriodo)

	If lCompleta
		cChave += chr(13) + "" + chr(13) + ""
	EndIf
Return cChave

/*/{Protheus.doc} chaveESeg
Monta a chave de registro para utilizar na rastreabilidade do ponto de pedido.

@type  Static Function
@author lucas.franca
@since 24/03/2020
@version P12.1.30
@param 01 - cProduto , caracter, c�digo do produto
@param 02 - nPeriodo , n�mero  , per�odo relacionado
@param 03 - lCompleta, logico  , indica que ir� montar a chave completa para utiliza��o no getItemAList
@return cChave, character, Chave de registro para utilizar no ponto de pedido.
/*/
Static Function chaveESeg(cProduto, nPeriodo, lCompleta)
	Local cChave := ""

	cChave := STR0143 + "_"+AllTrim(cProduto)+"_"+cValToChar(nPeriodo)

	If lCompleta
		cChave += chr(13) + "" + chr(13) + ""
	EndIf
Return cChave
