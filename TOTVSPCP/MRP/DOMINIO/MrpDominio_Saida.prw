#INCLUDE 'protheus.ch'
#INCLUDE 'MRPDominio.ch'

Static sCRLF         := chr(13)+chr(10) //Quebra de linha
Static soDados       := Nil             //Instancia da camada de dominio
Static soDominio     := Nil             //Instancia da camada de dominio

Static sOPC_KEY      := 1
Static sOPC_KEY2     := 2
Static sOPC_OPCION   := 3
Static sOPC_ID       := 4
Static sOPC_IDPAI    := 5
Static sOPC_IDMASTER := 6
Static sOPC_T4JRECNO := 7
Static sOPC_T4QRECNO := 8

Static snIPeriodo
Static snITipoPai
Static snIDocPai
Static snIDocFilh
Static snICompone
Static snIID_OPC
Static snITRT
Static snINecOrig
Static snIQtdEstq
Static snIConEstq
Static snISubstit
Static snISubsOri
Static snINecessi
Static snIChvSubs
Static snIIDRevis
Static snIRoteiro
Static snIOperacao
Static snIRotFilho
Static snILocal
Static snIQuebras
Static snIVersao

Static snXProd
Static snXDocAgl
Static snXDocs
Static snXTpDocOr
Static snXDocOri
Static snXProdOri
Static snXNeces
Static snXEmpen
Static snXSubst

/*/{Protheus.doc} MrpDominio_Saida
Regras de negocio MRP - Saida de Dados
@author    brunno.costa
@since     25/04/2019
@version   1
/*/
CLASS MrpDominio_Saida FROM LongClassName

	DATA oDominio AS OBJECT

	METHOD new(oDominio) CONSTRUCTOR
	METHOD aguardaTermino(nResultados, oDominio)
	METHOD exportarAglutinacaoRastreio(oDominio)
	METHOD exportarResultados(nResultados)
	METHOD exportarOpcionais(oDominio)
	METHOD exportarRastreio()
	METHOD exportarEventos(oDoninio)

ENDCLASS

/*/{Protheus.doc} new
Metodo construtor
@author    brunno.costa
@since     25/04/2019
@version   1
/*/
METHOD new(oDominio) CLASS MrpDominio_Saida

	::oDominio := oDominio

Return Self

/*/{Protheus.doc} exportarOpcionais
Exporta resultados
@author brunno.costa
@since 25/04/2019
@version 1.0
@param oDominio, objeto, objeto da camada de dominio
@return Nil
/*/
METHOD exportarOpcionais(oDominio) CLASS MrpDominio_Saida

	Local aLenFields     := {}
	Local aOpcionais     := {}
	Local aResultados    := {}
	Local cAliasQry      := ""
	Local nInd           := 0
	Local nMaxRecno      := 0
	Local nTentativas    := 0
	Local oDados         := oDominio:oDados
	Local oOpcionais     := oDados:oOpcionais
	Local oStatus        := oDominio:oLogs:oStatus

	//Retorna todos os dados de opcionais da Matriz de Opcionais
	oOpcionais:getAllRow(aOpcionais, .F.)

	//Retorna caso n�o possua opcional
	If Empty(aOpcionais)
		Return
	EndIf

	//Cria vari�vel de controle de RECNO da tabela HWD
	If AllTrim(oDominio:oParametros["cAutomacao"]) != "1"
		oStatus:preparaAmbiente(oDominio:oDados)

		cAliasQry := GetNextAlias()
		BeginSql Alias cAliasQry
			SELECT MAX(ISNULL(R_E_C_N_O_,0)) RECNO
			FROM %table:HWD%
		EndSql
		If !(cAliasQry)->(Eof())
			nMaxRecno := (cAliasQry)->RECNO
		EndIf
		(cAliasQry)->(DbCloseArea())

		oOpcionais:setflag("cFilial", xFilial("HWD"))

		//Busca o tamanho dos campos CHAR para o PadR
		aAdd(aLenFields, FWSizeFilial())
		aAdd(aLenFields, GetSx3Cache("HWD_TICKET","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWD_IDMAST","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWD_IDPAI" ,"X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWD_ID"    ,"X3_TAMANHO"))
	Else
		oOpcionais:setflag("cFilial", oDominio:oParametros["cFilAnt"])
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
	EndIf
	oOpcionais:setflag("nMaxRecno", nMaxRecno)

	DbSelectArea("T4J")
	DbSelectArea("T4Q")

	//Carrega o array com os registros a serem inseridos
	nOpcionais := Len(aOpcionais)
	For nInd := 1 To nOpcionais
		LinOPCBD(aOpcionais[nInd], @aResultados, oOpcionais, oDominio:oParametros["ticket"], aLenFields)
	Next nInd

	If AllTrim(oDominio:oParametros["cAutomacao"]) != "1"
		//Insere os registros
		cCols := "HWD_FILIAL,HWD_TICKET,HWD_IDMAST,HWD_IDPAI,HWD_ID,HWD_KEY,HWD_KEYMAT,HWD_OPCION,HWD_ERPMOP,HWD_ERPOPC,R_E_C_N_O_"
		While nTentativas < 10
			If TCDBInsert(RetSqlName("HWD"), cCols, aResultados) < 0
				Sleep(500)
				nTentativas++
			Else
				Exit
			EndIf
		EndDo
		If nTentativas == 10
			UserException(tcSQLError())
		EndIf
	EndIf

	aSize(aResultados, 0)
	aSize(aLenFields , 0)

Return

/*/{Protheus.doc} LinOPCBD
Gera Linha de Exporta��o da Tabela de Opcionais - BD
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - aOpcional  , array   , linha do opcional
@param 02 - aResultados, array   , retorna por refer�ncia array dos opcionais
@param 03 - oOpcionais , objeto  , objeto Data_Global referente Opcionais
@param 04 - cTicket    , caracter, ticket referente execu��o do MRP
@param 05 - aLenFields , array   , array com o tamanho dos campos CHAR para gravar com PadR
/*/
Static Function LinOPCBD(aOpcional, aResultados, oOpcionais, cTicket, aLenFields)

	Local aLinAux := {}
	Local nRecno  := 0

	//Adiciona campos na ordem de cCols
	aAdd(aLinAux, PadR(oOpcionais:getflag("cFilial"), aLenFields[1])) //HWD_FILIAL
	aAdd(aLinAux, PadR(cTicket                      , aLenFields[2])) //HWD_TICKET
	aAdd(aLinAux, PadR(aOpcional[2][sOPC_IDMASTER]  , aLenFields[3])) //HWD_IDMAST
	aAdd(aLinAux, PadR(aOpcional[2][sOPC_IDPAI]     , aLenFields[4])) //HWD_IDPAI
	aAdd(aLinAux, PadR(aOpcional[2][sOPC_ID]        , aLenFields[5])) //HWD_ID
	aAdd(aLinAux, aOpcional[1]                                      ) //HWD_KEY
	aAdd(aLinAux, aOpcional[2][sOPC_KEY2]                           ) //HWD_KEYMAT
	aAdd(aLinAux, StrTran(aOpcional[2][sOPC_OPCION] , ";", ","))      //HWD_OPCION

	If aOpcional[2][sOPC_T4JRECNO] == 0
		If aOpcional[2][sOPC_T4QRECNO] == 0
			aAdd(aLinAux, "")                   //HWD_ERPMOP
			aAdd(aLinAux, "")                   //HWD_ERPOPC
		Else
			T4Q->(DbGoTo(aOpcional[2][sOPC_T4QRECNO]))
			aAdd(aLinAux, T4Q->T4Q_ERPMOP)      //HWD_ERPMOP
			aAdd(aLinAux, T4Q->T4Q_ERPOPC)      //HWD_ERPOPC
		EndIf
	Else
		T4J->(DbGoTo(aOpcional[2][sOPC_T4JRECNO]))
		aAdd(aLinAux, T4J->T4J_ERPMOP)      //HWD_ERPMOP
		aAdd(aLinAux, T4J->T4J_ERPOPC)      //HWD_ERPOPC
	EndIf

	//Identifica o pr�ximo RECNO
	oOpcionais:setflag("nMaxRecno", @nRecno, .F., .F., .F., .T.)
	aAdd(aLinAux, nRecno)

	aAdd(aResultados, aLinAux)

Return

/*/{Protheus.doc} exportarResultados
Exporta resultados
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - nResultados, numero, retorna por referencia a quantidade de resultados - Single Thread
@param 02 - oDominio   , objeto, objeto da camada de dominio
@return Nil
/*/
METHOD exportarResultados(nResultados, oDominio) CLASS MrpDominio_Saida

	nResultados := oDominio:oDados:tamanhoLista("MAT")

	If nResultados > 0
		If oDominio:oParametros["nThreads_MAT"] <= 1
			MrpExpResM(nResultados, oDominio)
		Else
			StartJob("MrpExpResM", GetEnvServer(), .F., nResultados)
		EndIf
	Else
		oDominio:oDados:oMatriz:setflag("nUsdThread" , 0, .F., .F.)
		oDominio:oDados:oMatriz:setflag("nResultados", 0, .F., .F.)
	EndIf

Return

/*/{Protheus.doc} MrpExpResM
Exporta Resultados Matriz - Master
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - nResultados, numero, quantidade de resultados
@param 02 - oDominio   , objeto, objeto da camada de dominio - Single Thread
@return Nil
/*/
Function MrpExpResM(nResultados, oDominio)

	Local aLenFields  := {}
	Local cAliasQry   := ""
	Local nIndAux     := 0
	Local nInicio     := 0
	Local nFim        := 0
	Local nMaxRecno   := 0
	Local nMedia      := 0
	Local nThreads    := 0
	Local nUsdThread  := 0
	Local oDados
	Local oMatriz
	Local oStatus

	If oDominio == Nil
		If soDominio == Nil
			PrepStatics()
		EndIf
		oDominio := soDominio
	EndIf

	oDados   := oDominio:oDados
	oLogs    := oDominio:oLogs
	oMatriz  := oDados:oMatriz
	oStatus  := oDominio:oLogs:oStatus

	//Cria vari�vel de controle de RECNO da tabela HWB
	If AllTrim(oDominio:oParametros["cAutomacao"]) != "1"
		oStatus:preparaAmbiente(oDominio:oDados)

		cAliasQry := GetNextAlias()
		BeginSql Alias cAliasQry
			SELECT MAX(ISNULL(R_E_C_N_O_,0)) RECNO
			FROM %table:HWB%
		EndSql
		If !(cAliasQry)->(Eof())
			nMaxRecno := (cAliasQry)->RECNO
		EndIf
		(cAliasQry)->(DbCloseArea())

		oMatriz:setflag("cFilial", xFilial("HWB"))

		//Busca o tamanho dos campos CHAR para o PadR
		aAdd(aLenFields, FWSizeFilial())
		aAdd(aLenFields, GetSx3Cache("HWB_TICKET","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWB_PRODUT","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWB_IDOPC" ,"X3_TAMANHO"))
	Else
		oMatriz:setflag("cFilial", oDominio:oParametros["cFilAnt"])
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
	EndIf
	oMatriz:setflag("nMaxRecno", nMaxRecno)

	nThreads := oDominio:oParametros["nThreads_MAT"]

	If nThreads <= 1
		MrpExpResS(0, 1, nResultados, aLenFields)
	Else
	    //Processamento por Thread
		nMedia := Round(nResultados / nThreads, 0)
		If nMedia < 3000
			nMedia := 3000
		EndIf

		For nIndAux := 1 to nThreads
			nInicio := nMedia * (nIndAux - 1) + 1
			If (nMedia * nIndAux) < nResultados
				nFim := nMedia*nIndAux
			ElseIf nInicio <= nResultados
				nFim := nResultados
			Else
				nFim := 0
			EndIf
			If nFim > 0 .And. nIndAux == nThreads .And. nFim < nResultados
				nFim := nResultados
			EndIf
			If nFim > 0
				nUsdThread++
				oMatriz:setflag("lResultT" + PadL(nIndAux, 2, "0"), .F., .F., .F.)

				//Delega carga para Thread
				PCPIPCGO(oDominio:oParametros["cSemaforoThreads"] + "MAT", .F., "MrpExpResS", nIndAux, nInicio, nFim, aLenFields)
			EndIf
		Next nIndAux
	EndIf

	oMatriz:setflag("nUsdThread" , nUsdThread, .F., .F.)
	oMatriz:setflag("nResultados", nResultados, .F., .F.)

	aSize(aLenFields, 0)

Return

/*/{Protheus.doc} MrpExpResS
Exporta Resultados Matriz - Slave
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - nThreadsSeq, numerico, identificador sequencial de Thread delegada
@param 02 - nPrimeiro  , numerico, primeiro resultado a ser exportado por esta Thread
@param 03 - nUltimo    , numerico, ultimo resultado a ser exportado por esta Thread
@param 04 - aLenFields , array   , array com o tamanho dos campos CHAR para gravar com PadR
@return Nil
/*/
Function MrpExpResS(nThreadsSeq, nPrimeiro, nUltimo, aLenFields)

	Local aBackup
	Local aRetAux     := {}
	Local cName       := ""
	Local aResultados := {}
	Local lErrorMAT   := .F.
	Local nIndAux     := 0
	Local nTentativas := 0
	Local oDados
	Local oLogs
	Local oMatriz
	Local oStatus

	If soDominio == Nil
		PrepStatics()
	EndIf

	oDados  := soDominio:oDados
	oLogs   := soDominio:oLogs
	oMatriz := oDados:oMatriz
	oStatus := soDominio:oLogs:oStatus

	oLogs:log(STR0048 + cValToChar(nThreadsSeq) + " " + cValToChar(nPrimeiro) + "-" + cValToChar(nUltimo), "32") //"Inicio exportacao de resultados na Thread: "
	cName := "cResultT" + PadL(nThreadsSeq, 2, "0")
	oMatriz:setResult(cName, "", .F., .F.)

	For nIndAux := nPrimeiro To nUltimo
		oMatriz:position(nIndAux, @lErrorMAT)
		If lErrorMAT
			Exit
		EndIf

		aRetAux   := oDados:retornaCampo("MAT", 1, ,;
		                                 {"MAT_DATA"  , "MAT_PRODUT",;
		                                  "MAT_SLDINI", "MAT_ENTPRE",;
		                                  "MAT_SAIPRE", "MAT_SAIEST",;
										  "MAT_SALDO" , "MAT_NECESS",;
										  "MAT_IDOPC" , "MAT_DTINI"}, @lErrorMAT  ,;
		                                 .T. /*lAtual*/, , , , .T. /*lSort*/, .T. /*lVarios*/)

		If Empty(aRetAux[2]);
		   .Or. !soDominio:oSeletivos:consideraProduto(aRetAux[2],,.T.);
		   .Or. (aRetAux[4] == 0 .AND. aRetAux[5] == 0 .AND. aRetAux[6] == 0 .AND. aRetAux[8] == 0)
			Loop
		EndIf

		LinMatBD(aRetAux, @aResultados, oLogs, oMatriz, soDominio:oParametros["ticket"], aLenFields)

		//Transferencias parciais de string para variavel global / Banco
		If nThreadsSeq > 0 .AND. Mod(nIndAux, 500) == 0
			If !(AllTrim(soDominio:oParametros["cAutomacao"]) $ "|1|2|")
				//Grava resultados no banco de dados
				oStatus:preparaAmbiente(oDados)
				cCols := "HWB_FILIAL,HWB_TICKET,HWB_DATA,HWB_PRODUT,HWB_IDOPC,HWB_QTSLES,HWB_QTENTR,HWB_QTSAID,HWB_QTSEST,HWB_QTSALD,HWB_QTNECE,HWB_DTINIC,R_E_C_N_O_"
				While nTentativas < 10
					If TCDBInsert(RetSqlName("HWB"), cCols, aResultados) < 0
						Sleep(500)
						nTentativas++
					Else
						Exit
					EndIf
				EndDo
				If nTentativas == 10
					UserException(tcSQLError())
					nTentativas := 0
				EndIf
			EndIf
			aSize(aResultados, 0)
		EndIf
	Next nIndAux

	If !Empty(aResultados)
		If AllTrim(soDominio:oParametros["cAutomacao"]) != "1"
			//Grava resultados no banco de dados
			oStatus:preparaAmbiente(oDados)
			cCols := "HWB_FILIAL,HWB_TICKET,HWB_DATA,HWB_PRODUT,HWB_IDOPC,HWB_QTSLES,HWB_QTENTR,HWB_QTSAID,HWB_QTSEST,HWB_QTSALD,HWB_QTNECE,HWB_DTINIC,R_E_C_N_O_"
			While nTentativas < 10

				//Corrige ordena��o para compara��o em automa��o
				If AllTrim(soDominio:oParametros["cAutomacao"]) == "2"
					aBackup     := aClone(aResultados)
					aResultados := aSort(aResultados, , , { | x,y | x[1] + x[2] + DtoS(x[3]) + x[4] + x[5];
					                                               <;
																    y[1] + y[2] + DtoS(y[3]) + y[4] + y[5];
					                                  } )
					//Corrige ordena��o de RECNO
					nUltimo := Len(aBackup)
					For nIndAux := 1 To nUltimo
						aResultados[nIndAux][13] := aBackup[nIndAux][13]
					Next
				EndIf
				If TCDBInsert(RetSqlName("HWB"), cCols, aResultados) < 0
					Sleep(500)
					nTentativas++
				Else
					Exit
				EndIf
			EndDo
			If nTentativas == 10
				UserException(tcSQLError())
			EndIf
		EndIf
		aSize(aResultados, 0)

	EndIf
	oMatriz:setflag("lResultT" + PadL(nThreadsSeq, 2, "0"), .T., .F., .F.)

	oLogs:log(STR0049 + cValToChar(nThreadsSeq) + STR0050 + cValToChar(nPrimeiro) + "-" + cValToChar(nUltimo) + " " + Time(), "32") //"Fim exportacao de resultados na Thread: " + " - Resultados: "

Return

/*/{Protheus.doc} LinMatBD
Gera Linha de Exporta��o da Tabela da Matriz - BD
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - aRetAux    , array   , linha do opcional
@param 02 - aResultados, array   , retorna por refer�ncia array dos resultados da Matriz
@param 03 - oMatriz    , objeto  , objeto Data_Global referente Matriz de Resultados
@param 04 - cTicket    , caracter, ticket referente execu��o do MRP
@param 05 - aLenFields , array   , array com o tamanho dos campos CHAR para gravar com PadR
/*/
Static Function LinMatBD(aRetAux, aResultados, oLogs, oMatriz, cTicket, aLenFields)

	Local aLinAux := {}
	Local nRecno  := 0

	//Adiciona campos na ordem de cCols
	aAdd(aLinAux, PadR(oMatriz:getflag("cFilial"), aLenFields[1])) //HWB_FILIAL
	aAdd(aLinAux, PadR(cTicket                   , aLenFields[2])) //HWB_TICKET
	aAdd(aLinAux, aRetAux[1])                                      //HWB_DATA
	aAdd(aLinAux, PadR(aRetAux[2]                , aLenFields[3])) //HWB_PRODUT
	aAdd(aLinAux, PadR(aRetAux[9]                , aLenFields[4])) //HWB_IDOPC
	aAdd(aLinAux, aRetAux[3])                                      //HWB_QTSLES
	aAdd(aLinAux, aRetAux[4])                                      //HWB_QTENTR
	aAdd(aLinAux, aRetAux[5])                                      //HWB_QTSAID
	aAdd(aLinAux, aRetAux[6])                                      //HWB_QTSEST
	aAdd(aLinAux, aRetAux[7])                                      //HWB_QTSALD
	aAdd(aLinAux, aRetAux[8])                                      //HWB_QTNECE
	aAdd(aLinAux, aRetAux[10])                                     //HWB_DTINIC

	//Identifica o pr�ximo RECNO
	oMatriz:setflag("nMaxRecno", @nRecno, .F., .F., .F., .T.)
	aAdd(aLinAux, nRecno)

	aAdd(aResultados, aLinAux)

Return

/*/{Protheus.doc} exportarRastreio
Exporta Rastreio
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - oDominio, objeto, objeto da camada de dominio
/*/
METHOD exportarRastreio(oDominio) CLASS MrpDominio_Saida

	If oDominio:oParametros["nThreads"] <= 1
		MrpExpRasM(oDominio)
	Else
		StartJob("MrpExpRasM", GetEnvServer(), .F.)
	EndIf

Return

/*/{Protheus.doc} MrpExpRasM
Exporta rastreabilidade - Master
@author    brunno.costa
@since     23/10/2019
@version 1.0
@param 01 - oDominio, objeto, objeto da classe MrpDominio passado somente sem threads adicionais - Single thread
/*/
Function MrpExpRasM(oDominio)

	Local aLenFields  := {}
	Local aPeriodos   := {}
	Local aPerProds   := {}
	Local aSessoes    := {}
	Local cAliasQry   := ""
	Local cList       := ""
	Local cChaveProd  := ""
	Local lError      := .F.
	Local nDelegacoes := 0
	Local nIPer       := 0
	Local nInd        := 0
	Local nMaxRecno   := 0
	Local nProdutos   := 0
	Local nThreads    := 0
	Local oRastreio
	Local oStatus

	If oDominio == Nil
		If soDominio == Nil
			PrepStatics()
		EndIf
		oDominio := soDominio
	EndIf

	oStatus   := oDominio:oLogs:oStatus
	oRastreio := oDominio:oRastreio
	nThreads  := oDominio:oParametros["nThreads"]

	VarIsUID("UIDs_PCPMRP")
	VarGetXA( "UIDs_PCPMRP", @aSessoes)
	nTotal := Len(aSessoes)
	For nInd := 1 to nTotal
		If "Periodos_Produto_" $ aSessoes[nInd][1]
			aAdd(aPerProds, Substr(aSessoes[nInd][1], 17 + At("Periodos_Produto_", aSessoes[nInd][1])))
		EndIf
	Next
	aSize(aSessoes, 0)
	aSessoes := Nil

	//Retorna todos os dados de rastreio
	nProdutos := Len(aPerProds)
	If nProdutos > 0
		If oDominio:oEventos:lHabilitado
			oDominio:oEventos:analisaDocumentos(aPerProds)
		EndIf

		//Cria vari�vel de controle de RECNO da tabela HWC
		If oDominio:oParametros["cAutomacao"] != "1"
			oStatus:preparaAmbiente(oDominio:oDados)

			cAliasQry := GetNextAlias()
			BeginSql Alias cAliasQry
				SELECT MAX(ISNULL(R_E_C_N_O_,0)) RECNO
				FROM %table:HWC%
			EndSql
			If !(cAliasQry)->(Eof())
				nMaxRecno := (cAliasQry)->RECNO
			EndIf
			(cAliasQry)->(DbCloseArea())

			oRastreio:oDados_Rastreio:oDados:setflag("cFilial", xFilial("HWC"))

			//Busca o tamanho dos campos CHAR para o PadR
			aAdd(aLenFields, FWSizeFilial())
			aAdd(aLenFields, GetSx3Cache("HWC_TICKET","X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_TPDCPA","X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_DOCPAI","X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_DOCFIL","X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_PRODUT","X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_IDOPC" ,"X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_TRT"   ,"X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_CHVSUB","X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_CHAVE" ,"X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_REV"   ,"X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_ROTEIR","X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_OPERAC","X3_TAMANHO"))

			dbSelectArea("HWC")
			If FieldPos("HWC_ROTFIL") > 0
				aAdd(aLenFields, GetSx3Cache("HWC_ROTFIL","X3_TAMANHO"))
			Else
				aAdd(aLenFields, 0)
			EndIf

			aAdd(aLenFields, GetSx3Cache("HWC_LOCAL" ,"X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_DOCERP","X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_TDCERP","X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_STATUS","X3_TAMANHO"))
			aAdd(aLenFields, GetSx3Cache("HWC_VERSAO","X3_TAMANHO"))
		Else
			oRastreio:oDados_Rastreio:oDados:setflag("cFilial", oDominio:oParametros["cFilAnt"])
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
			aAdd(aLenFields, 1)
		EndIf
		oRastreio:oDados_Rastreio:oDados:setflag("nMaxRecno", nMaxRecno)

		If AllTrim(oDominio:oParametros["cAutomacao"]) == "2"
			aPerProds := aSort(aPerProds)
		EndIf

		//Percorre os dados do rastreio
		For nInd := 1 To nProdutos
			lError := .F.

			cChaveProd := aPerProds[nInd]
			oDominio:oDados:oMatriz:getAllList("Periodos_Produto_"+cChaveProd, @aPeriodos, @lError)

			If !lError
				nPeriodos := Len(aPeriodos)
				For nIPer := 1 To nPeriodos
					cList := cChaveProd + chr(13) + aPeriodos[nIPer][1]
					If oRastreio:oDados_Rastreio:oDados:existAList(cList)
						//Executa fun��o de exporta��o os resultados com todos os registros
						If nThreads <= 1
							MrpExpRasS(0, cList, aLenFields)

						//Delega carga para Thread
						Else
							nDelegacoes++
							PCPIPCGO(oDominio:oParametros["cSemaforoThreads"], .F., "MrpExpRasS", nDelegacoes, cList, aLenFields)
						EndIf
					EndIf
				Next nIPer
			EndIf
			aSize(aPeriodos, 0)
		Next nInd
		oRastreio:oDados_Rastreio:oDados:setflag("nDelegacoes_Rastreio", nDelegacoes, .F., .F.)

		//Limpa Array Local aPerProds
		aSize(aPerProds, 0)
		aPerProds := Nil

		aSize(aLenFields, 0)
	Else
		oRastreio:oDados_Rastreio:oDados:setflag("nDelegacoes_Rastreio", 0, .F., .F.)
	EndIf

Return

/*/{Protheus.doc} MrpExpRasS
Exporta rastreabilidade - Slave
@author    brunno.costa
@since     23/10/2019
@version 1.0
@param 01 - nThreadsSeq, numerico, identificador sequencial de Thread delegada
@param 02 - cList      , caracter, identificador da sess�o de vari�veis globais - Produto + chr(13) + Per�odo
@param 03 - aLenFields , array   , array com o tamanho dos campos CHAR para gravar com PadR
/*/
Function MrpExpRasS(nThreadsSeq, cList, aLenFields)

	Local aBackup
	Local aDocumentos := {}
	Local aLinha      := {}
	Local aResultados := {}
	Local cCols       := ""
	Local nIDoc       := 0
	Local nIndAux
	Local nTentativas := 0
	Local oDados
	Local oLogs
	Local oMOD        := MrpDominio_MOD():New()
	Local oRastreio
	Local oStatus

	If soDominio == Nil
		PrepStatics()
	EndIf

	oDados    := soDominio:oDados
	oLogs     := soDominio:oLogs
	oRastreio := soDominio:oRastreio
	oStatus   := soDominio:oLogs:oStatus

	oRastreio:oDados_Rastreio:oDados:setflag("lResultT" + cValToChar(nThreadsSeq), .F., .F., .F.)

	If snIPeriodo == Nil
		snIPeriodo  := oRastreio:getPosicao("PERIODO")
		snITipoPai  := oRastreio:getPosicao("TIPOPAI")
		snIDocPai   := oRastreio:getPosicao("DOCPAI")
		snIDocFilh  := oRastreio:getPosicao("DOCFILHO")
		snICompone  := oRastreio:getPosicao("COMPONENTE")
		snIID_OPC   := oRastreio:getPosicao("ID_OPCIONAL")
		snITRT      := oRastreio:getPosicao("TRT")
		snINecOrig  := oRastreio:getPosicao("NEC_ORIGINAL")
		snIQtdEstq  := oRastreio:getPosicao("QTD_ESTOQUE")
		snIConEstq  := oRastreio:getPosicao("CONSUMO_ESTOQUE")
		snISubstit  := oRastreio:getPosicao("SUBSTITUICAO")
		snISubsOri  := oRastreio:getPosicao("QTD_SUBST_ORIGINAL")
		snINecessi  := oRastreio:getPosicao("NECESSIDADE")
		snIChvSubs  := oRastreio:getPosicao("CHAVE_SUBSTITUICAO")
		snIIDRevis  := oRastreio:getPosicao("REVISAO")
		snIRoteiro  := oRastreio:getPosicao("ROTEIRO")
		snIOperacao := oRastreio:getPosicao("OPERACAO")
		snIRotFilho := oRastreio:getPosicao("ROTEIRO_DOCUMENTO_FILHO")
		snILocal    := oRastreio:getPosicao("LOCAL")
		snIQuebras  := oRastreio:getPosicao("QUEBRAS_QUANTIDADE")
		snIVersao   := oRastreio:getPosicao("VERSAO_PRODUCAO")
	EndIf

	oRastreio:oDados_Rastreio:oDados:getAllAList(cList, @aDocumentos)

	aDocumentos := aSort(aDocumentos, , , {|x,y| Iif(x[2][snINecOrig] < 0 , "0", "1");
	                                           + Iif(x[2][snITipoPai] == "Pr�-OP" , "0", "1");
	                                           + Iif(x[2][snITipoPai] == STR0144, "1", "0"); //"Ponto Ped."
	                                           + PadL(x[2][snIChvSubs], 130);
	                                           + PadL(x[2][snIDocPai], 100);
	                                           <;
	                                           Iif(y[2][snINecOrig] < 0 , "0", "1");
	                                           + Iif(y[2][snITipoPai] == "Pr�-OP" , "0", "1");
	                                           + Iif(y[2][snITipoPai] == STR0144, "1", "0"); //"Ponto Ped."
	                                           + PadL(y[2][snIChvSubs], 130);
	                                           + PadL(y[2][snIDocPai], 100)} )
	nDocumentos := Len(aDocumentos)
	For nIDoc := 1 To nDocumentos
		aLinha := aDocumentos[nIDoc][2]

		If     !soDominio:oSeletivos:consideraProduto(aLinha[snICompone]);
		   .OR. oMOD:produtoMOD(aLinha[snICompone], soDominio:oDados)
			aLinha[snINecessi] := 0
		EndIf

		//N�o imprime registros zerados
		If (aLinha[snINecOrig] == 0;
			.AND. aLinha[snIConEstq] == 0;
			.AND. aLinha[snINecessi] == 0;
			.AND. aLinha[snISubstit] == 0;
			.AND. !(("|"+AllTrim(aLinha[snITipoPai]+"|")) $ "|Pr�-OP|");
		  .OR.;
			("|"+AllTrim(aLinha[snITipoPai]+"|") $ "|Pr�-OP|";
			.AND. aLinha[snIQtdEstq] == 0;
			.AND. aLinha[snINecessi] == 0))
			Loop
		EndIf

		//Prepara linha de resultados para exporta��o em banco de dados
		LinRastBD(aLinha, @aResultados, oLogs, oRastreio, soDominio:oParametros["ticket"], aLenFields)
		aSize(aLinha, 0)

		//Transferencias parciais de string para variavel global / Banco
		If Mod(nIDoc, 1000) == 0
			If !(AllTrim(soDominio:oParametros["cAutomacao"]) $ "|1|2|")
				//Grava resultados no banco de dados
				oStatus:preparaAmbiente(oDados)
				dbSelectArea("HWC")
				If FieldPos("HWC_ROTFIL") > 0
					cCols := "HWC_FILIAL,HWC_TICKET,HWC_DATA,HWC_TPDCPA,HWC_DOCPAI,HWC_DOCFIL,HWC_PRODUT,HWC_IDOPC,HWC_TRT,HWC_QTNEOR,HWC_QTSLES,HWC_QTBXES,HWC_QTSUBS,HWC_QTSBVL,HWC_QTEMPE,HWC_QTNECE,HWC_CHVSUB,HWC_CHAVE,HWC_REV,HWC_ROTEIR,HWC_OPERAC,HWC_ROTFIL,HWC_LOCAL,HWC_SEQUEN,HWC_VERSAO,R_E_C_N_O_"
				Else
					cCols := "HWC_FILIAL,HWC_TICKET,HWC_DATA,HWC_TPDCPA,HWC_DOCPAI,HWC_DOCFIL,HWC_PRODUT,HWC_IDOPC,HWC_TRT,HWC_QTNEOR,HWC_QTSLES,HWC_QTBXES,HWC_QTSUBS,HWC_QTSBVL,HWC_QTEMPE,HWC_QTNECE,HWC_CHVSUB,HWC_CHAVE,HWC_REV,HWC_ROTEIR,HWC_OPERAC,HWC_LOCAL,HWC_SEQUEN,HWC_VERSAO,R_E_C_N_O_"
				EndIf
				While nTentativas < 10
					If ( TCDBInsert(RetSqlName("HWC"), cCols, aResultados ) ) < 0
						Sleep(500)
						nTentativas++
					Else
						Exit
					EndIf
				EndDo
				If nTentativas == 10
					UserException(tcSQLError())
				EndIf
			EndIf
			aSize(aResultados, 0)
		EndIf
	Next
	aSize(aDocumentos, 0)

	If !Empty(aResultados) .AND. oStatus:getStatus("status") != "4"//N�o Cancelado
		dbSelectArea("HWC")
		If FieldPos("HWC_ROTFIL") > 0
			cCols := "HWC_FILIAL,HWC_TICKET,HWC_DATA,HWC_TPDCPA,HWC_DOCPAI,HWC_DOCFIL,HWC_PRODUT,HWC_IDOPC,HWC_TRT,HWC_QTNEOR,HWC_QTSLES,HWC_QTBXES,HWC_QTSUBS,HWC_QTSBVL,HWC_QTEMPE,HWC_QTNECE,HWC_CHVSUB,HWC_CHAVE,HWC_REV,HWC_ROTEIR,HWC_OPERAC,HWC_ROTFIL,HWC_LOCAL,HWC_SEQUEN,HWC_VERSAO,R_E_C_N_O_"
		Else
			cCols := "HWC_FILIAL,HWC_TICKET,HWC_DATA,HWC_TPDCPA,HWC_DOCPAI,HWC_DOCFIL,HWC_PRODUT,HWC_IDOPC,HWC_TRT,HWC_QTNEOR,HWC_QTSLES,HWC_QTBXES,HWC_QTSUBS,HWC_QTSBVL,HWC_QTEMPE,HWC_QTNECE,HWC_CHVSUB,HWC_CHAVE,HWC_REV,HWC_ROTEIR,HWC_OPERAC,HWC_LOCAL,HWC_SEQUEN,HWC_VERSAO,R_E_C_N_O_"
		EndIf
		If AllTrim(soDominio:oParametros["cAutomacao"]) != "1"
			//Grava resultados no banco de dados
			oStatus:preparaAmbiente(oDados)

			//Corrige ordena��o para compara��o em automa��o
			If AllTrim(soDominio:oParametros["cAutomacao"]) == "2"
				aBackup     := aClone(aResultados)
				dbSelectArea("HWC")
				If FieldPos("HWC_ROTFIL") > 0
					aResultados := aSort(aResultados, , , { | x,y | Iif(x[4] == STR0144, "1", "0") + x[19] + x[7] + DtoS(x[3]) + x[4] + x[5] + StrZero(x[26], 5);
																	<;
																	Iif(y[4] == STR0144, "1", "0") + y[19] + y[7] + DtoS(y[3]) + y[4] + y[5] + StrZero(y[26], 5);
														} )
				Else
					aResultados := aSort(aResultados, , , { | x,y | Iif(x[4] == STR0144, "1", "0") + x[18] + x[7] + DtoS(x[3]) + x[4] + x[5] + StrZero(x[25], 5);
																	<;
																	Iif(y[4] == STR0144, "1", "0") + y[18] + y[7] + DtoS(y[3]) + y[4] + y[5] + StrZero(y[25], 5);
														} )
				EndIf
				//Corrige ordena��o de RECNO
				nUltimo := Len(aBackup)
				For nIndAux := 1 To nUltimo
					aTail(aResultados[nIndAux]) := aTail(aBackup[nIndAux])
				Next
			EndIf

			While nTentativas < 10
				If TCDBInsert(RetSqlName("HWC"), cCols, aResultados) < 0
					Sleep(500)
					nTentativas++
				Else
					Exit
				EndIf
			EndDo
			If nTentativas == 10
				UserException(tcSQLError())
			EndIf
		EndIf
		aSize(aResultados, 0)
	EndIf

	oRastreio:oDados_Rastreio:oDados:setflag("lResultT" + cValToChar(nThreadsSeq), .T., .F., .F.)

Return

/*/{Protheus.doc} LinRastBD
Adiciona linha do registro no controle de inser��o no banco
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - aLinha     , array   , linha do rastreio
@param 02 - aResultados, array   , retorna por refer�ncia array dos resultados de rastreabilidade
@param 03 - oRastreio  , objeto  , objeto Data_Global referente rastreabilidade
@param 04 - cTicket    , caracter, ticket referente execu��o do MRP
@param 05 - aLenFields , array   , array com o tamanho dos campos CHAR para gravar com PadR
/*/
Static Function LinRastBD(aLinha, aResultados, oLogs, oRastreio, cTicket, aLenFields)

	Local aLinAux    := {}
	Local nRecno     := 0
	Local nIndQuebra := 0
	Local nTotEmp    := 0
	Local nTotNecOri := 0
	Local nTotSubst  := 0
	Local nTotSubOr  := 0
	Local nTotQuebra := 0
	Local nEmpGrv    := 0
	Local nNecOriGrv := 0
	Local nSubsGrv   := 0
	Local nSubOriGrv := 0
	Local nQtdEstGrv := 0
	Local nConEstGrv := 0
	Local nSequen    := 1
	Local oDados     := soDominio:oRastreio:oDados_Rastreio:oDados

	//Prote��o para caso n�o tenha sido gerado o array de controle das quebras
	//de produ��o/compra. Ir� gerar uma quebra com a qtd. total
	If aLinha[snIQuebras] == Nil .Or. Len(aLinha[snIQuebras]) < 1
		aLinha[snIQuebras] := {}
		aAdd(aLinha[snIQuebras], {aLinha[snINecessi], aLinha[snIDocFilh]})
	EndIf

	//TODO - Prote��o contra BUG identificado ap�s merge da issue DMANNEWPCP-2760 apresentando aLinha[snISubstit] positivo quando deveria ser negativo - Apenas base codifica��o - N�o identificado ponto onde o valor perde o sinal
	If aLinha[snISubsOri] != Nil .AND. aLinha[snISubstit]  != Nil .AND. aLinha[snISubsOri] < 0 .AND. aLinha[snISubstit] > 0
		aLinha[snISubstit] := -aLinha[snISubstit]
	EndIf

	//Armazena o total de empenhos, necessidades e substitui��es deste rastreio
	//Utiliza os totalizadores para controlar as quebras que ser�o feitas para exporta��o
	//das quebras de produ��o/compras.
	nTotNecOri := aLinha[snINecOrig]
	nTotSubst  := Iif(aLinha[snISubstit] == Nil, 0        , aLinha[snISubstit])
	nTotSubOr  := Iif(aLinha[snISubsOri] == Nil, nTotSubst, aLinha[snISubsOri])
	nTotEmp    := nTotNecOri - nTotSubst

	//Para cada elemento do array de quebras, ir� gerar um registro de rastreabilidade.
	//O primeiro elemento do array de quebras sempre � referente ao registro principal da rastreabilidade.
	//Os demais elementos sempre s�o elementos de produ��o ou compra. Por isso nunca possuem estoque ou consumo.
	nTotQuebra := Len(aLinha[snIQuebras])
	For nIndQuebra := 1 To nTotQuebra

		//Limpa vari�veis auxiliares
		nEmpGrv    := 0
		nNecOriGrv := 0
		nSubsGrv   := 0
		nSubOriGrv := 0
		nQtdEstGrv := 0
		nConEstGrv := 0

		//Adiciona campos na ordem de cCols
		aAdd(aLinAux, PadR(oDados:getflag("cFilial")                           , aLenFields[1])) //HWC_FILIAL
		aAdd(aLinAux, PadR(cTicket                                             , aLenFields[2])) //HWC_TICKET
		aAdd(aLinAux, soDominio:aPeriodos[aLinha[snIPeriodo]])                                   //HWC_DATA
		aAdd(aLinAux, PadR(aLinha[snITipoPai]                                  , aLenFields[3])) //HWC_TPDCPA
		aAdd(aLinAux, PadR(aLinha[snIDocPai]                                   , aLenFields[4])) //HWC_DOCPAI

		If Empty(aLinha[snIQuebras][nIndQuebra][2]) .And. nTotQuebra == 1 .And. aLinha[snIQuebras][nIndQuebra][1] > 0
			aAdd(aLinAux, PadR(aLinha[snIDocFilh]                              , aLenFields[5])) //HWC_DOCFIL
		Else
			aAdd(aLinAux, PadR(aLinha[snIQuebras][nIndQuebra][2]               , aLenFields[5])) //HWC_DOCFIL
		EndIf

		aAdd(aLinAux, PadR(aLinha[snICompone]                                  , aLenFields[6])) //HWC_PRODUT
		aAdd(aLinAux, PadR(Iif(Empty(aLinha[snIID_OPC]), "",aLinha[snIID_OPC]) , aLenFields[7])) //HWC_IDOPC
		aAdd(aLinAux, PadR(aLinha[snITRT]                                      , aLenFields[8])) //HWC_TRT

		If nIndQuebra == 1
			If nTotQuebra == 1
				//Se a produ��o n�o foi quebrada, ir� imprimir os totalizadores
				nEmpGrv    := nTotEmp
				nNecOriGrv := nTotNecOri
				nSubsGrv   := nTotSubst
				nSubOriGrv := nTotSubOr
			Else
				//Se houve quebras na produ��o, ir� calcular quais os valores devem ser impressos
				//neste registro de rastreabilidade, de acordo com as quantidades que foram quebradas para a produ��o/compra.
				nEmpGrv := aLinha[snIConEstq] + aLinha[snIQuebras][nIndQuebra][1]
				If nTotSubst >= 0
					nNecOriGrv := aLinha[snIConEstq] + aLinha[snISubstit] + aLinha[snIQuebras][nIndQuebra][1]
					nSubsGrv   := nTotSubst
					nSubOriGrv := nTotSubOr
				Else
					If aLinha[snIQuebras][nIndQuebra][1] > 0
						//Deve entrar aqui quando � um produto alternativo,
						//Que possui estoque, e que foi produzido.
						//Soma na qtd. do empenho o saldo de estoque.
						nEmpGrv += aLinha[snIQtdEstq]
					EndIf
					nNecOriGrv := 0
					nSubsGrv   := -nEmpGrv
				EndIf
			EndIf

			nQtdEstGrv := aLinha[snIQtdEstq]
			nConEstGrv := aLinha[snIConEstq]
		Else
			//N�o � o primeiro elemento da quebra, ent�o n�o vai gravar quantidades de estoque e de consumo.
			//Somente calcula a qtd. necessidade original e empenho de acordo com o valor de necessidade deste registro para grava��o.
			nEmpGrv    := Iif(nTotEmp    < aLinha[snIQuebras][nIndQuebra][1], nTotEmp   , aLinha[snIQuebras][nIndQuebra][1])
			nNecOriGrv := Iif(nTotNecOri < aLinha[snIQuebras][nIndQuebra][1], nTotNecOri, aLinha[snIQuebras][nIndQuebra][1])

			nQtdEstGrv := 0
			nConEstGrv := 0
		EndIf

		aAdd(aLinAux, nNecOriGrv) //HWC_QTNEOR - Necessidade original
		aAdd(aLinAux, nQtdEstGrv) //HWC_QTSLES - Qtd. estoque
		aAdd(aLinAux, nConEstGrv) //HWC_QTBXES - Qtd. Baixa
		aAdd(aLinAux, nSubsGrv  ) //HWC_QTSUBS - Qtd. Substitui��o
		aAdd(aLinAux, nSubOriGrv) //HWC_QTSBVL - Qtd. Substitui��o Valida��o

		If aLinha[snITipoPai] == "OP"
			aAdd(aLinAux, nEmpGrv) //HWC_QTEMPE
		Else
			aAdd(aLinAux, "0")     //HWC_QTEMPE
		EndIf

		aAdd(aLinAux, aLinha[snIQuebras][nIndQuebra][1])                                       //HWC_QTNECE - Necessidade
		aAdd(aLinAux, PadR(StrTran(aLinha[snIChvSubs], CHR(13), "")         , aLenFields[ 9])) //HWC_CHVSUB
		aAdd(aLinAux, PadR(aLinha[snICompone]+cValToChar(aLinha[snIPeriodo]), aLenFields[10])) //HWC_CHAVE
		aAdd(aLinAux, PadR(cValToChar(aLinha[snIIDRevis])                   , aLenFields[11])) //HWC_REV
		aAdd(aLinAux, PadR(cValToChar(aLinha[snIRoteiro])                   , aLenFields[12])) //HWC_ROTEIR
		aAdd(aLinAux, PadR(cValToChar(aLinha[snIOperacao])                  , aLenFields[13])) //HWC_OPERAC

		dbSelectArea("HWC")
		If FieldPos("HWC_ROTFIL") > 0
			aAdd(aLinAux, PadR(cValToChar(aLinha[snIRotFilho])                  , aLenFields[14])) //HWC_ROTFIL
		EndIf

		aAdd(aLinAux, PadR(cValToChar(aLinha[snILocal])                     , aLenFields[15])) //HWC_LOCAL

		//Desconta os valores j� impressos das vari�veis totalizadoras.
		nTotEmp    -= nEmpGrv
		nTotNecOri -= nNecOriGrv
		nTotSubst  -= nSubsGrv

		//Adiciona o campo HWC_SEQUEN. Este campo diferencia os registros quando existe alguma quebra
		//referente �s pol�ticas de estoque.
		aAdd(aLinAux, nSequen) //HWC_SEQUEN

		dbSelectArea("HWC")
		If FieldPos("HWC_ROTFIL") > 0
			aAdd(aLinAux, PadR(aLinha[snIVersao], aLenFields[19]))
		Else
			aAdd(aLinAux, PadR(aLinha[snIVersao], aLenFields[18]))
		EndIf


		//Incrementa a sequ�ncia para o pr�ximo registro (se houver).
		nSequen++

		//Identifica o pr�ximo RECNO
		oDados:setflag("nMaxRecno", @nRecno, .F., .F., .F., .T.)
		aAdd(aLinAux, nRecno)

		aAdd(aResultados, aClone(aLinAux))
		aSize(aLinAux   , 0)
	Next nIndQuebra

Return

/*/{Protheus.doc} exportarAglutinacaoRastreio
Exporta Aglutinacao do Rastreio
@author    brunno.costa
@since     25/11/2019
@version 1.0
@param 01 - oDominio   , objeto, objeto da camada de dominio
/*/
METHOD exportarAglutinacaoRastreio(oDominio) CLASS MrpDominio_Saida

	If oDominio:oParametros["nThreads_AGL"] <= 1
		cResultados := MrpExpAglM(oDominio)
	Else
		StartJob("MrpExpAglM", GetEnvServer(), .F.)
	EndIf

Return

/*/{Protheus.doc} MrpExpAglM
Exporta aglutina��o de rastreabilidade - Master
@author    brunno.costa
@since     23/10/2019
@version 1.0
@param 01 - oDominio, objeto, objeto da classe MrpDominio passado somente sem threads adicionais - Single thread
/*/
Function MrpExpAglM(oDominio)

	Local aAglutinacao := {}
	Local aLenFields   := {}
	Local aListas      := {"4"}
	Local cAliasQry    := ""
	Local nDelegacoes  := 0
	Local nIlAglutina  := 0
	Local nInd         := 0
	Local nIndLis      := 0
	Local nMaxRecno    := 0
	Local nThreads     := 0
	Local nTotal       := 0
	Local oAglutinacao
	Local oStatus

	If oDominio == Nil
		If soDominio == Nil
			PrepStatics()
		EndIf
		oDominio := soDominio
	EndIf

	oStatus      := oDominio:oLogs:oStatus
	oAglutinacao := oDominio:oAglutina:oAglutinacao
	nThreads     := oDominio:oParametros["nThreads_AGL"]

	//Cria vari�vel de controle de RECNO da tabela HWG
	If AllTrim(oDominio:oParametros["cAutomacao"]) != "1"
		oStatus:preparaAmbiente(oDominio:oDados)

		cAliasQry := GetNextAlias()
		BeginSql Alias cAliasQry
			SELECT MAX(ISNULL(R_E_C_N_O_,0)) RECNO
			FROM %table:HWG%
		EndSql
		If !(cAliasQry)->(Eof())
			nMaxRecno := (cAliasQry)->RECNO
		EndIf
		(cAliasQry)->(DbCloseArea())

		oAglutinacao:setflag("cFilial", xFilial("HWG"))

		//Busca o tamanho dos campos CHAR para o PadR
		aAdd(aLenFields, FWSizeFilial())
		aAdd(aLenFields, GetSx3Cache("HWG_TICKET","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWG_PROD"  ,"X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWG_DOCAGL","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWG_TPDCOR","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWG_DOCORI","X3_TAMANHO"))
	Else
		oAglutinacao:setflag("cFilial", oDominio:oParametros["cFilAnt"])
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
		aAdd(aLenFields, 1)
	EndIf
	oAglutinacao:setflag("nMaxRecno", nMaxRecno)

	nIlAglutina := oDominio:oAglutina:getPosicao("AGLUTINA")

	For nIndLis := 1 To Len(aListas)
		lError := .F.
		oAglutinacao:getAllAList(aListas[nIndLis], @aAglutinacao, @lError)

		If !lError
			//Retorna todos os dados de rastreio da aglutina��o - Lista 1
			nTotal := Len(aAglutinacao)
			If nTotal > 0
				//Processamento por Thread
				For nInd := 1 to nTotal
					//Desconsidera registros n�o aglutinados
					IF !aAglutinacao[nInd][2][nIlAglutina]
						Loop
					EndIf
					cChave := aAglutinacao[nInd][1]
					nDelegacoes++
					If oDominio:oParametros["nThreads_AGL"] <= 1
						MrpExpAglS(nDelegacoes, aListas[nIndLis], cChave, aLenFields)
					Else
						PCPIPCGO(oDominio:oParametros["cSemaforoThreads"] + "AGL", .F., "MrpExpAglS", nDelegacoes, aListas[nIndLis], cChave, aLenFields)
					EndIf
				Next nInd
			EndIf

			//Limpa Array Local aAglutinacao
			aSize(aAglutinacao, 0)
		EndIf
	Next nIndLis

	oAglutinacao:setflag("nDelegacoes_Rastreio", nDelegacoes, .F., .F.)

Return

/*/{Protheus.doc} MrpExpAglS
Exporta aglutina��o de rastreabilidade - Slave
@author    brunno.costa
@since     23/10/2019
@version 1.0
@param 01 - nThreadsSeq, numerico, identificador sequencial de Thread delegada
@param 02 - cLista     , caracter, c�digo da lista identificadora da tabela utilizada:
                              1 = Carga de Demandas (PCPCargDem)
							  2 = Carga de Empenhos
							  3 = Carga de outras Sa�das Previstas
							  4 = Explos�o da Estrutura
@param 03 - cChave     , caracter, chave do registro aglutinado
									If lAglutina
										cChave := cFilAux + cValToChar(nPeriodo) + cProduto + chr(13)
									Else
										cChave := cFilAux + cOrigem + cDocumento + chr(13) + cValToChar(nPeriodo) + cProduto
									EndIf
@param 04 - aLenFields , array   , array com o tamanho dos campos CHAR para gravar com PadR

/*/
Function MrpExpAglS(nThreadsSeq, cLista, cChave, aLenFields)

	Local aLinha      := {}
	Local aNovasLin   := {}
	Local lError      := .F.
	Local oAglutinacao
	Local nIndAlt
	Local nAlternativos

	If soDominio == Nil
		PrepStatics()
	EndIf

	oAglutinacao := soDominio:oAglutina:oAglutinacao

	If snXProd == Nil
		snXProd    := soDominio:oAglutina:getPosicao("PRODUTO")
		snXDocAgl  := soDominio:oAglutina:getPosicao("DOCUMENTO")
		snXDocs    := soDominio:oAglutina:getPosicao("ADOC_PAI")
		snXTpDocOr := soDominio:oAglutina:getPosicao("ADOC_PAI_ORI")
		snXDocOri  := soDominio:oAglutina:getPosicao("ADOC_PAI_DOC")
		snXProdOri := soDominio:oAglutina:getPosicao("ADOC_PAI_PRODUTO")
		snXNeces   := soDominio:oAglutina:getPosicao("ADOC_PAI_QTD")
		snXEmpen   := soDominio:oAglutina:getPosicao("ADOC_PAI_QTD_EMPE")
		snXSubst   := soDominio:oAglutina:getPosicao("ADOC_PAI_QTD_SUBS")
	EndIf

	aLinha := oAglutinacao:getItemAList(cLista, cChave, @lError)
	soDominio:oAglutina:ajustesExportacao(@aLinha, @aNovasLin)

	If !lError
		MrpExpAglR(aLinha, aLenFields, oAglutinacao)

		If aNovasLin != Nil .AND. !Empty(aNovasLin)
			nAlternativos := Len(aNovasLin)
			For nIndAlt := 1 to nAlternativos
				MrpExpAglR(aNovasLin[nIndAlt], aLenFields, oAglutinacao)
			Next
		EndIf
	EndIf

	oAglutinacao:setflag("lResultT" + cValToChar(nThreadsSeq), .T., .F., .F.)

Return

/*/{Protheus.doc} MrpExpAglR
Exporta aglutina��o de rastreabilidade - Insers�o de Resultados
@author    brunno.costa
@since     23/10/2019
@version 1.0
@param 01 - aLinha       , array   , array com os dados do registro da aglutina��o (HWG)
@param 02 - aLenFields   , array   , array com o tamanho dos campos CHAR para gravar com PadR
@param 03 - oAglutinacao , objeto  , objeto MrpData_Global de aglutina��o
/*/
Static Function MrpExpAglR(aLinha, aLenFields, oAglutinacao)
	Local aDocOrig    := {}
	Local aResultados := {}
	Local cCols       := ""
	Local nDocumentos := Len(aLinha[snXDocs])
	Local nIDoc       := 0
	Local nLen        := 0
	Local nRecno      := 0
	Local nTentativas := 0
	Local oStatus     := soDominio:oLogs:oStatus
	Local oDados      := soDominio:oDados

	For nIDoc := 1 to nDocumentos
		//Identifica o pr�ximo RECNO
		oAglutinacao:setflag("nMaxRecno", @nRecno, .F., .F., .F., .T.)

		//Tratamento de erro VM - Base IES - Rela��o com aDel da MrpDominio_Aglutina:prepara()
		If Len(aLinha)          >= snXDocs .AND. aLinha[snXDocs]        != Nil .AND.;
			Len(aLinha[snXDocs]) >= nIDoc   .AND. aLinha[snXDocs][nIDoc] != Nil
			nLen := Len(aLinha[snXDocs][nIDoc])
		Else
			nLen := 0
		EndIf

		If (Iif(nLen >= snXEmpen , aLinha[snXDocs][nIDoc][snXNeces] + aLinha[snXDocs][nIDoc][snXEmpen], 0 ) != 0)
			If chr(13) $ aLinha[snXDocs][nIDoc][snXDocOri]
				aDocOrig := Strtokarr2( aLinha[snXDocs][nIDoc][snXDocOri], chr(13), .T.)
			Else
				aSize(aDocOrig, 0)
				aAdd(aDocOrig, aLinha[snXDocs][nIDoc][snXDocOri])
				aAdd(aDocOrig, 1)
			EndIf
			aAdd(aResultados, {PadR(oAglutinacao:getflag("cFilial")   , aLenFields[1]),; //HWG_FILIAL
							   PadR(soDominio:oParametros["ticket"]   , aLenFields[2]),; //HWG_TICKET
							   PadR(aLinha[snXProd]                   , aLenFields[3]),; //HWG_PROD
							   PadR(aLinha[snXDocAgl]                 , aLenFields[4]),; //HWG_DOCAGL
							   PadR(aLinha[snXDocs][nIDoc][snXTpDocOr], aLenFields[5]),; //HWG_TPDCOR
							   PadR(aDocOrig[1]                       , aLenFields[6]),; //HWG_DOCORI
							   aDocOrig[2]                                            ,; //HWG_SEQORI
							   PadR(aLinha[snXDocs][nIDoc][snXProdOri], aLenFields[3]),; //HWG_PRODOR
							   aLinha[snXDocs][nIDoc][snXNeces]                       ,; //HWG_NECESS
							   aLinha[snXDocs][nIDoc][snXEmpen]                       ,; //HWG_QTEMPE
							   aLinha[snXDocs][nIDoc][snXSubst]                       ,; //HWG_QTSUBS
							   nRecno})
			aSize(aLinha[snXDocs][nIDoc], 0)
		EndIf
	Next
	aSize(aLinha, 0)

	If !Empty(aResultados) .AND. oStatus:getStatus("status") != "4" //N�o Cancelado
		cCols := "HWG_FILIAL,HWG_TICKET,HWG_PROD,HWG_DOCAGL,HWG_TPDCOR,HWG_DOCORI,HWG_SEQORI,HWG_PRODOR,HWG_NECESS,HWG_QTEMPE,HWG_QTSUBS,R_E_C_N_O_"
		If AllTrim(soDominio:oParametros["cAutomacao"]) != "1"
			//Grava resultados no banco de dados
			oStatus:preparaAmbiente(oDados)
			While nTentativas < 10
				If TCDBInsert(RetSqlName("HWG"), cCols, aResultados) < 0
					Sleep(500)
					nTentativas++
				Else
					Exit
				EndIf
			EndDo
			If nTentativas == 10
				UserException(tcSQLError())
			EndIf
		EndIf
	EndIf
Return

/*/{Protheus.doc} exportarEventos
Exportar Log de Eventos do MRP
@author brunno.costa
@since 04/05/2020
@version 1.0
@param 01 - oDominio, objeto, objeto da camada de dominio
@param 02 - aArrEvt , array , array com os eventos para exporta��o
@return Nil
/*/
METHOD exportarEventos(oDominio, aArrEvt) CLASS MrpDominio_Saida

	Local aLenFields     := {}
	Local aEventos       := {}
	Local aResultados    := {}
	Local cAliasQry      := ""
	Local cSQLName       := ""
	Local cEvento        := ""
	Local nInd           := 0
	Local nMaxRecno      := 0
	Local nTentativas    := 0
	Local oDados         := oDominio:oDados
	Local oEventos       := oDados:oEventos
	Local oStatus        := oDominio:oLogs:oStatus
	Local nArrInd        := 0
	Local nArrTot
	Local nEventos       := 0
	Local lExpDocs       := aArrEvt == Nil

	Default aArrEvt      := {"001","004","005","006","008","009"}

	oEventos:setFlag("lExportacaoConcluida", .F.)

	//Cria vari�vel de controle de RECNO da tabela HWM
	If Empty(aLenFields)
		oStatus:preparaAmbiente(oDominio:oDados)
		cSQLName := RetSqlName("HWM")
		cAliasQry := GetNextAlias()
		BeginSql Alias cAliasQry
			SELECT MAX(ISNULL(R_E_C_N_O_,0)) RECNO
			FROM %table:HWM%
		EndSql
		If !(cAliasQry)->(Eof())
			nMaxRecno := (cAliasQry)->RECNO
		EndIf
		(cAliasQry)->(DbCloseArea())

		oEventos:setflag("cFilial", xFilial("HWM"))

		//Busca o tamanho dos campos CHAR para o PadR
		aAdd(aLenFields, FWSizeFilial())
		aAdd(aLenFields, GetSx3Cache("HWM_TICKET","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWM_PRODUT","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWM_EVENTO","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWM_LOGMRP","X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWM_DOC"   ,"X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWM_ITEM"  ,"X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWM_ALIAS" ,"X3_TAMANHO"))
		aAdd(aLenFields, GetSx3Cache("HWM_PRDORI","X3_TAMANHO"))
		oEventos:setflag("nMaxRecno", nMaxRecno)
	EndIf

	nArrTot := Len(aArrEvt)
	For nArrInd := 1 to nArrTot
		cEvento := aArrEvt[nArrInd]

		//Retorna todos os dados de opcionais da Matriz de Opcionais
		oEventos:getAllAList(cEvento, @aEventos, .F.)

		//Retorna caso n�o possua opcional
		If Empty(aEventos)
			Loop
		EndIf

		DbSelectArea("HWM")

		//Carrega o array com os registros a serem inseridos
		nEventos := Len(aEventos)
		For nInd := 1 To nEventos
			oDominio:oEventos:montaDadosImpressao(cEvento, aEventos[nInd], @aResultados, aLenFields)
			If Mod(nInd, 100) == 0
				If AllTrim(oDominio:oParametros["cAutomacao"]) != "1" .AND. Len(aResultados) > 0
					//Insere os registros
					cCols := "HWM_FILIAL,HWM_TICKET,HWM_PRODUT,HWM_EVENTO,HWM_LOGMRP,HWM_DOC,HWM_ITEM,HWM_ALIAS,HWM_PRDORI,R_E_C_N_O_"
					While nTentativas < 10
						If TCDBInsert(cSQLName, cCols, aResultados) < 0
							Sleep(500)
							nTentativas++
						Else
							Exit
						EndIf
					EndDo
					If nTentativas == 10
						nTentativas := 0
						UserException(tcSQLError())
						Exit
					EndIf
					aSize(aResultados, 0)
				EndIf
			EndIf
		Next nInd

		If AllTrim(oDominio:oParametros["cAutomacao"]) != "1" .AND. Len(aResultados) > 0
			//Insere os registros
			cCols := "HWM_FILIAL,HWM_TICKET,HWM_PRODUT,HWM_EVENTO,HWM_LOGMRP,HWM_DOC,HWM_ITEM,HWM_ALIAS,HWM_PRDORI,R_E_C_N_O_"
			While nTentativas < 10
				If TCDBInsert(cSQLName, cCols, aResultados) < 0
					Sleep(500)
					nTentativas++
				Else
					Exit
				EndIf
			EndDo
			If nTentativas == 10
				UserException(tcSQLError())
			EndIf
		EndIf

		aSize(aResultados, 0)
		aSize(aEventos, 0)
	Next
	aSize(aLenFields, 0)

	If lExpDocs
		While oEventos:getFlag("lProcessamentoConcluido") == Nil .OR. !oEventos:getFlag("lProcessamentoConcluido")
			Sleep(50)
		EndDo
		Self:exportarEventos(oDominio, {"002","003","007"})
	Else
		oEventos:setFlag("lExportacaoConcluida", .T.)
	EndIf

Return

/*/{Protheus.doc} aguardaTermino
Aguarda T�rmino da Exporta��o dos Resultados
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - nResultados, numero, retorna por referencia a quantidade de resultados
@param 02 - oDominio   , objeto, objeto da camada de dominio
@return cResultados, caracter, string de resultados
/*/
METHOD aguardaTermino(nResultados, oDominio) CLASS MrpDominio_Saida

	Local aResultados  := {}
	Local cRetAux      := ""
	Local lConcluiu    := .F.
	Local lError       := .F.
	Local lRetAux
	Local cLinha       := STR0022 //"DATA;PRODUTO;SLD.DISP;ENT.PREV.;SAI.PREV.;SAI.ESTRUT.;SALDO;NECESSIDADE;ID.OPCIONAL;DT.INICIO"
	Local cResultados  := ""
	Local nIndAux
	Local oDados       := oDominio:oDados
	Local oAglutinacao := oDominio:oAglutina:oAglutinacao
	Local oMatriz      := oDados:oMatriz
	Local oEventos     := oDados:oEventos
	Local oRastreio    := oDominio:oRastreio
	Local nMaximo
	Local oStatus      := oDominio:oLogs:oStatus
	Local lCancelado   := .F.

	cResultados += cLinha + sCRLF
	oDominio:oLogs:log(cLinha, "30")

	//Aguarda T�rmino Exporta��o da Matriz
	While !lCancelado .AND. oMatriz:getflag("nUsdThread") == Nil
		Sleep(50)
		If oStatus:getStatus("status") == "4"//Cancelado
			lCancelado := .T.
		EndIF
	EndDo
	nMaximo := oMatriz:getflag("nUsdThread")
	While !lCancelado .AND. !lConcluiu
		cRetAux   := ""
		lConcluiu := .T.
		For nIndAux := 1 to nMaximo
			cRetAux := ""
			lRetAux := oMatriz:getflag("lResultT" + PadL(nIndAux, 2, "0"))
			If lRetAux == Nil .Or. !lRetAux
				lConcluiu := .F.
				Exit
			EndIf
		Next nIndAux
		Sleep(50)
		If oStatus:getStatus("status") == "4"//Cancelado
			lCancelado := .T.
		EndIF
	EndDo
	If !lCancelado
		oMatriz:getAllRes(@aResultados, @lError)
		aEval(aResultados, {|x| cResultados += x[2], x[2] := "", oMatriz:setResult(x[1], "", .F., .F.) })
		aSize(aResultados, 0)
		cResultados += STR0023 + cValToChar(nResultados) + sCRLF //"Resultados: "
	EndIf

	//Aguarda T�rmino Exporta��o de Rastreio
	While !lCancelado .AND. oRastreio:oDados_Rastreio:oDados:getflag("nDelegacoes_Rastreio") == Nil
		Sleep(50)
		If oStatus:getStatus("status") == "4"//Cancelado
			lCancelado := .T.
		EndIF
	EndDo
	If !lCancelado
		nMaximo   := oRastreio:oDados_Rastreio:oDados:getflag("nDelegacoes_Rastreio")
	EndIf
	lConcluiu := .F.
	While !lCancelado .AND. !lConcluiu
		cRetAux   := ""
		lConcluiu := .T.
		For nIndAux := 1 to nMaximo
			cRetAux := ""
			lRetAux := oRastreio:oDados_Rastreio:oDados:getflag("lResultT" + cValToChar(nIndAux))
			If lRetAux == Nil .Or. !lRetAux
				lConcluiu := .F.
				Exit
			EndIf
		Next nIndAux
		Sleep(50)
		If oStatus:getStatus("status") == "4"//Cancelado
			lCancelado := .T.
		EndIF
	EndDo

	//Aplica Regra HWC_LOCAL
	MrpDominio_LocalConsumo():aplicar(oDominio:oParametros, oDominio:oParametros["ticket"])

	If oDados:oParametros['lRastreiaAglutinacao']
		//Aguarda T�rmino Exporta��o do Rastreio da Aglutina��o
		While !lCancelado .AND. oAglutinacao:getflag("nDelegacoes_Rastreio") == Nil
			Sleep(50)
			If oStatus:getStatus("status") == "4"//Cancelado
				lCancelado := .T.
			EndIF
		EndDo
		nMaximo   := oAglutinacao:getflag("nDelegacoes_Rastreio")
		lConcluiu := .F.
		While !lCancelado .AND. !lConcluiu
			cRetAux   := ""
			lConcluiu := .T.
			For nIndAux := 1 to nMaximo
				cRetAux := ""
				lRetAux := oAglutinacao:getflag("lResultT" + cValToChar(nIndAux))
				If lRetAux == Nil .Or. !lRetAux
					lConcluiu := .F.
					Exit
				EndIf
			Next nIndAux
			Sleep(50)
			If oStatus:getStatus("status") == "4"//Cancelado
				lCancelado := .T.
			EndIF
		EndDo
	EndIf

	If oDados:oParametros['lEventLog']
		//Aguarda T�rmino Exporta��o do Log de Eventos
		While !lCancelado .AND. oEventos:getflag("lExportacaoConcluida") == Nil
			Sleep(50)
			If oStatus:getStatus("status") == "4"//Cancelado
				lCancelado := .T.
			EndIF
		EndDo
		lConcluiu := .F.
		While !lCancelado .AND. !lConcluiu
			lConcluiu := oEventos:getflag("lExportacaoConcluida")
			Sleep(50)
			If oStatus:getStatus("status") == "4"//Cancelado
				lCancelado := .T.
			EndIF
		EndDo
	EndIf

	If oDominio:oDados:oParametros['lAnalisaMemoriaPosExpRastreio']
		oDominio:oDados:oProdutos:analiseMemoria(oDominio:oDados:oParametros['lAnalisaMemoriaSplit'], STR0119) //An�lise de Mem�ria Ap�s Exporta��o do Rastreio
	EndIf

Return cResultados

/*/{Protheus.doc} PrepStatics
Exporta resultados por Thread
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - aParam, array, array com os parametros para preparacao das statics
/*/
Static Function PrepStatics()

	SET DATE FRENCH; Set(_SET_EPOCH, 1980)

	soDominio := MRPPrepDom()
	soDados   := soDominio:oDados

Return