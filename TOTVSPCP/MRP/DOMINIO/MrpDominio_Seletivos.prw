#INCLUDE 'protheus.ch'
#INCLUDE 'MRPDominio.ch'

Static _lBloqueio

/*/{Protheus.doc} MrpDominio_Seletivos
Regras de Negocio - Filtros Seletivos Multivalorados
@author    brunno.costa
@since     27/05/2020
@version   12.1.27
/*/
CLASS MrpDominio_Seletivos FROM LongClassName

	//Declaracao de propriedades da classe
	DATA oDominio         AS OBJECT
	DATA oDados           AS OBJECT
	DATA lSeletivoProduto AS LOGICAL

	METHOD new() CONSTRUCTOR
	METHOD limpaInMemoria()

	//Metodos para Montagem de Script SQL
	METHOD scriptExistsProdutoSQL(cProdCol, lOnlyBlock)
	METHOD scriptRecursivaEstrutura()
	METHOD scriptInSQL(cColuna, cParametro)

	//Metodos exclusivos da Regra Seletivo de Produtos
	METHOD consideraProduto(cProduto, oDados)
	METHOD criaTabelaTemporariaProdutos()
	METHOD loadProdutosValidos()
	METHOD setaProdutoValido(cProduto)

ENDCLASS

/*/{Protheus.doc} new
Metodo construtor
@author    brunno.costa
@since     27/05/2020
@version   12.1.27
@param 01 - oDominio, objeto, inst�ncia da classe de dominio
/*/
METHOD new(oDominio) CLASS MrpDominio_Seletivos
	Self:oDominio         := oDominio
	Self:oDados           := oDominio:oDados:oSeletivos
	Self:lSeletivoProduto := !Empty(oDominio:oDados:oParametros["cProducts"])
Return Self

/*/{Protheus.doc} scriptExistsProdutoSQL
Retorna Script SQL EXISTS() para complemento a clausulas Where
@author    brunno.costa
@since     27/05/2020
@version   12.1.27
@param 01 cProdCol  , caracter, coluna referente ao c�digo do produto para compara��o com a tabela HWA
@param 02 lOnlyBlock, Logic   , identifica que ser� adicionado somente o filtro de produtos bloqueados.
@return   cWhere  , caracter, cl�usula WHERE com o filtro do produto
/*/
METHOD scriptExistsProdutoSQL(cProdCol, lOnlyBlock) CLASS MrpDominio_Seletivos

	Local cWhere     := ""

	Default lOnlyBlock := .F.

	cWhere := " EXISTS (SELECT 1 "
	cWhere +=           " FROM " + RetSqlName("HWA") + " HWA "
	cWhere +=          " WHERE HWA.HWA_FILIAL = '" + xFilial("HWA") + "'"
	cWhere +=            " AND HWA.HWA_PROD   = "  + cProdCol
	cWhere +=            " AND HWA.D_E_L_E_T_ = ' ' "

	If !lOnlyBlock
		cWhere +=        " AND HWA.HWA_MRP    = '1' "
	EndIf

	If prdBlock()
		//Somente produtos com status diferente de bloqueado.
		cWhere +=        " AND HWA.HWA_BLOQUE <> '1' "
	EndIf

	//Seletivo de Grupos de Materiais
	If !lOnlyBlock .And. !Empty(Self:oDominio:oDados:oParametros["cProductGroups"])
		cWhere     += " AND " + Self:scriptInSQL("HWA.HWA_GRUPO", "cProductGroups")
	EndIf

	//Seletivo de Tipos de Materiais
	If !lOnlyBlock .And. !Empty(Self:oDominio:oDados:oParametros["cProductTypes"])
		cWhere += " AND " + Self:scriptInSQL("HWA.HWA_TIPO", "cProductTypes")
	EndIf

	//Seletivo de Produtos
	If Self:lSeletivoProduto
		cWhere += " AND EXISTS (SELECT PRODUTOS.HWA_PROD
		cWhere +=             " FROM (SELECT HWA_1.HWA_PROD HWA_PROD "
		cWhere +=                   " FROM " + RetSqlName("HWA") + " HWA_1 "
		cWhere +=                   " WHERE HWA_1.HWA_FILIAL = '" + xFilial("HWA") + "'"
		cWhere +=                     " AND HWA_1.D_E_L_E_T_ = ' ' "
		cWhere +=                     " AND " + Self:scriptInSQL("HWA_1.HWA_PROD", "cProducts")
		cWhere +=                     "UNION ALL "
		cWhere +=                      Self:scriptRecursivaEstrutura()
		cWhere +=                    ") PRODUTOS "
		cWhere +=             " WHERE PRODUTOS.HWA_PROD   = "  + cProdCol
		cWhere +=              ")"
	EndIf

	cWhere += ")"
Return cWhere

/*/{Protheus.doc} scriptInSQL
Retorna Script SQL IN para complemento a clausulas Where
@author    brunno.costa
@since     27/05/2020
@version   12.1.27
@param 01 cColuna   , caracter, coluna a ser montado o AND
@param 02 cParametro, caracter, par�metro a ser tratado
@return cWhereIn    , data    , par�metro quebrado para utiliza��o no IN do SELECT
/*/
METHOD scriptInSQL(cColuna, cParametro) CLASS MrpDominio_Seletivos

	Local cConteudo := Self:oDominio:oParametros[cParametro]
	Local aLista
	Local cColOrig  := cColuna
	Local cWhereIn  := ""
	Local nIndexIN  := 0
	Local nIndexLis := 1
	Local nIndDe    := 0
	Local nIndAte   := 0
	Local nLenLista := 0
	Local nQtdINs   := 0

	cColuna := "COLUNA_DEFAULT"

	Self:oDados:trava(cParametro)

	cWhereIn := Self:oDados:getFlag(cParametro)

	If Empty(cWhereIn)
		cWhereIn := ""
		aLista   := StrTokArr2(SubStr(cConteudo, 2, Len(cConteudo)-2), "|", .F.)
		If Empty(aLista)
			aAdd(aLista, " ")
		EndIf
		nLenLista := Len(aLista)
		nQtdINs   := Ceiling(nLenLista / 1000)

		cWhereIn += "("

		//Percorre de 1000 em 1000 por conta da limita��o do IN
		For nIndexIN := 1 To nQtdINs
			If nIndexIN > 1
				cWhereIn += " OR "
			EndIf

			nIndDe  := nIndexLis
			nIndAte := nIndexIN * 1000
			If nIndAte > nLenLista
				nIndAte := nLenLista
			EndIf

			cWhereIn += cColuna + " IN ("

			For nIndexLis := nIndDe To nIndAte
				cWhereIn += "'" + aLista[nIndexLis] + "',"
			Next nIndexLis

			//Remove a �ltima v�rgula
			cWhereIn := Stuff(cWhereIn, Len(cWhereIn), 1, '') + ")"
		Next nIndexIN

		cWhereIn +=	")"
		Self:oDados:setFlag(cParametro, cWhereIn)
	EndIf

	Self:oDados:destrava(cParametro)

	cWhereIn := StrTran(cWhereIn, cColuna, cColOrig)

Return cWhereIn

/*/{Protheus.doc} scriptRecursivaEstrutura
Retorna Script SQL IN para complemento a clausulas Where - Query Recursiva de Estruturas
@author    brunno.costa
@since     27/05/2020
@version   12.1.27
@return cScript, caracter, script para union referente query recursiva
/*/
METHOD scriptRecursivaEstrutura() CLASS MrpDominio_Seletivos

	Local cArqTrab := ""
	Local cScript  := ""

	Self:oDados:trava("PCPA712_PRD")
	cArqTrab := Self:oDados:getFlag("PCPA712_PRD")
	If Empty(cArqTrab)
		cArqTrab := Self:criaTabelaTemporariaProdutos()
		Self:oDados:setFlag("PCPA712_PRD", cArqTrab)
	EndIf
	Self:oDados:destrava("PCPA712_PRD")

	cScript := " SELECT T4N_COMP "
	cScript += " FROM " + cArqTrab

Return cScript

/*/{Protheus.doc} criaTabelaTemporariaProdutos
Cria a tabela tempor�ria para armazenar os poss�veis c�digos de produtos seletivos (estrutura)
@author brunno.costa
@since 27/05/2020
@version P12.1.30
@return cArqTrab, caracter, nome do arquivo de trabalho criado
/*/
METHOD criaTabelaTemporariaProdutos() CLASS MrpDominio_Seletivos

	Local aFields    := {}
	Local cArqTrab   := "PCPA712_PRD"
	Local cBanco     := TCGetDB()
	Local cQuery     := ""
	Local cInsert    := ""
	Local lOk        := .T.

	//Adiciona Campos
	aAdd(aFields, {})
	aAdd(aFields[1], "T4N_COMP")
	aAdd(aFields[1], GetSX3Cache("T4N_COMP", "X3_TIPO"))
	aAdd(aFields[1], GetSX3Cache("T4N_COMP", "X3_TAMANHO"))
	aAdd(aFields[1], GetSX3Cache("T4N_COMP", "X3_DECIMAL"))

	//Deleta Tabela no Banco, caso exista
	lOk := TCDelFile(cArqTrab)

	//Cria Tabela no Banco
	dbCreate(cArqTrab, aFields, "TOPCONN")
	
	cInsert := " INSERT "
	cInsert += "  INTO " + cArqTrab + "(T4N_COMP, R_E_C_N_O_) "

	cQuery := " WITH EstruturaRecursiva(T4N_PROD, T4N_COMP)"
	cQuery += " AS ("

	cQuery +=       " SELECT T4N.T4N_PROD,"
	cQuery +=              " T4N.T4N_COMP"
	cQuery +=        " FROM " + RetSqlName( "T4N" ) + " T4N "
	cQuery +=       " WHERE T4N.D_E_L_E_T_ = ' '"
	cQuery +=         " AND T4N.T4N_FILIAL  = '" + xFilial("T4N") + "' "
	cQuery +=         " AND (" + Self:scriptInSQL("T4N.T4N_PROD", "cProducts") + ")"

	cQuery +=       " UNION ALL "

	cQuery +=       " SELECT T4N.T4N_PROD,"
	cQuery +=              " T4N.T4N_COMP"
	cQuery +=        " FROM " + RetSqlName( "T4N" ) + " T4N "
	cQuery +=             " INNER JOIN EstruturaRecursiva Qry_Recurs"
	cQuery +=                    " ON Qry_Recurs.T4N_COMP = T4N.T4N_PROD  "
	cQuery +=       " WHERE T4N.D_E_L_E_T_ = ' '"
	cQuery +=         " AND T4N.T4N_FILIAL  = '" + xFilial("T4N") + "' "

	cQuery +=   " )"

	If cBanco != "ORACLE"
		cQuery += cInsert
	EndIf
	
	cQuery += " SELECT DADOS.T4N_COMP, "
	cQuery +=        " ROW_NUMBER() OVER(ORDER BY DADOS.T4N_COMP ASC) RECNO "
	cQuery += " FROM (SELECT DISTINCT Resultado.T4N_COMP "
	cQuery +=       " FROM EstruturaRecursiva Resultado) DADOS "

	If cBanco == "ORACLE"
		cQuery := cInsert + cQuery
	EndIf

	//Realiza ajustes da Query para cada banco
	If cBanco == "POSTGRES"

		//Altera sintaxe da clausula WITH
		cQuery := StrTran(cQuery, 'WITH ', 'WITH recursive ')

		//Corrige Falhas internas de Bin�rio - POSTGRES
		cQuery := StrTran(cQuery, CHR(13), " ")
		cQuery := StrTran(cQuery, CHR(10), " ")
		cQuery := StrTran(cQuery, CHR(09), " ")
		
	EndIf

	If TcSqlExec(cQuery) < 0
		Final(STR0168, tcSQLError() + cQuery) //"Erro ao carregar o seletivo de produtos."
	EndIf

Return cArqTrab

/*/{Protheus.doc} loadProdutosValidos
Efetua Carga Inicial no Controle de Seletivo de Produtos
@author    brunno.costa
@since     27/05/2020
@version   12.1.27
/*/
METHOD loadProdutosValidos() CLASS MrpDominio_Seletivos

	Local cParametro := "cProductsX"
	Local cConteudo  := Self:oDominio:oParametros["cProducts"]
	Local aLista
	Local cCarregado := ""
	Local nIndexLis  := 1
	Local nLenLista  := 0

	If Self:lSeletivoProduto
		Self:oDados:trava(cParametro)

		cCarregado := Self:oDados:getFlag(cParametro)
		If Empty(cCarregado)
			cCarregado := ""
			aLista     := StrTokArr2(SubStr(cConteudo, 2, Len(cConteudo)-2), "|", .F.)
			nLenLista  := Len(aLista)
			For nIndexLis := 1 To nLenLista
				Self:setaProdutoValido(aLista[nIndexLis])
			Next nIndexLis
			cCarregado := "OK"
			Self:oDados:setFlag(cParametro, cCarregado)
		EndIf

		Self:oDados:destrava(cParametro)
	EndIf

Return

/*/{Protheus.doc} limpaInMemoria
Limpa Strings IN da Memoria
@author    brunno.costa
@since     27/05/2020
@version   12.1.27
/*/

METHOD limpaInMemoria() CLASS MrpDominio_Seletivos

	Local cArqTrab := Self:oDados:getFlag("PCPA712_PRD")

	Self:oDados:setFlag("cDemandCodes"  , "")
	Self:oDados:setFlag("cDocuments"    , "")
	Self:oDados:setFlag("cProductGroups", "")
	Self:oDados:setFlag("cProductTypes" , "")
	Self:oDados:setFlag("cWarehouses"   , "")
	Self:oDados:setFlag("cProducts"     , "")

	//Exclui tabela de produtos do banco
	If !Empty(cArqTrab)
		TCDelFile(cArqTrab)
	EndIf

Return

/*/{Protheus.doc} setaProdutoValido
Seta Produto Valido - Seletivo de Produtos
@author    brunno.costa
@since     27/05/2020
@version   12.1.27
@param 01 cProduto, caracter, c�digo do produto
/*/
METHOD setaProdutoValido(cProduto) CLASS MrpDominio_Seletivos
	If Self:lSeletivoProduto
		Self:oDados:setFlag("|" + AllTrim(cProduto) + "|", .T.)
	EndIf
Return

/*/{Protheus.doc} consideraProduto
Avalia se o produto est� dentro das condi��es de filtro do c�lculo
@author    brunno.costa
@since     27/05/2020
@version   12.1.27
@param 01 cProduto   , caracter, c�digo do produto
@param 02 oDados     , logico  , classe de dados do MRP
@param 03 lExpHWB    , logico  , indica se a chamada � a partir da exportacao dos resultados da HWB
@return lConsidera, l�gico, indica se o produto deve ou n�o ser calculado
/*/
METHOD consideraProduto(cProduto, oDados, lExpHWB) CLASS MrpDominio_Seletivos

	Local aRetPrd     := {}
	Local aAreaPRD    := {}
	Local cChavProd   := cProduto
	Local lAtual      := .T.
	Local lConsidera  := .T.
	Local lError      := .F.
	Local lGet        := .F.
	Local oParametros

	Default oDados      := Self:oDominio:oDados
	Default lExpHWB := .F.

	If !lExpHWB
		cProduto := AllTrim(cProduto)

		If Self:lSeletivoProduto
			lGet := Self:oDados:getFlag("|" + cProduto + "|", @lError)
			If lError .OR. !lGet
				lConsidera := .F.
				lError     := .F.
			EndIf
		EndIf

		If lConsidera
			lConsidera := Self:oDados:getFlag("|L|" + cChavProd)
			If lConsidera == Nil

				If oDados:oProdutos:nIndice == 2
					If oDados:oProdutos:cCurrentKey == Nil .Or. Right(oDados:oProdutos:cCurrentKey, Len(oDados:oProdutos:cCurrentKey)-2) != cChavProd
						lAtual := .F.
					EndIf
				ElseIf oDados:oProdutos:cCurrentKey == Nil .Or. oDados:oProdutos:cCurrentKey != cChavProd
					lAtual := .F.
				EndIf

				If !lAtual
					aAreaPRD := oDados:retornaArea("PRD")
				EndIf

				aRetPrd := oDados:retornaCampo("PRD", 1, cChavProd, {"PRD_TIPO", "PRD_GRUPO", "PRD_MRP", "PRD_BLOQUE"}, , lAtual , , , , , .T. /*lVarios*/)

				If !lAtual
					oDados:setaArea(aAreaPRD)
					aSize(aAreaPRD, 0)
				EndIf

				//Avalia se o produto n�o est� bloqueado (PRD_BLOQUE == 2), e se o produto deve ser utilizado no MRP ( PRD_MRP  == 1)
				If (Empty(aRetPrd[4]) .Or. aRetPrd[4] == "2") .And. (aRetPrd[3] == "1" .OR. Empty(aRetPrd[3]))
					oParametros := Self:oDominio:oParametros
					//Avalia se o produto est� dentro do filtro
					lConsidera := .T.

					If !Empty(oParametros["cProductTypes"]) .And. !(aRetPrd[1] $ oParametros["cProductTypes"])
						lConsidera := .F.
					Else
						If !Empty(oParametros["cProductGroups"]) .And. !("|"+aRetPrd[2]+"|" $ oParametros["cProductGroups"])
							lConsidera := .F.
						EndIf
					EndIf
				Else
					lConsidera := .F.
				EndIf

				 Self:oDados:setFlag("|L|" + cChavProd, lConsidera)

			EndIf

		EndIf
	EndIf

Return lConsidera

/*/{Protheus.doc} prdBlock
Verifica se deve ser considerado o par�metro de produtos bloqueados da tabela HWA.

@type  Static Function
@author lucas.franca
@since 11/02/2020
@version P12.1.27
@return lBloqueio, Logic, Identifica se deve considerar o par�metro de produtos bloqueados.
/*/
Static Function prdBlock()
	Local lBloqueio := .F.

	If _lBloqueio == Nil
		If Empty(GetSX3Cache("HWA_BLOQUE", "X3_TAMANHO"))
			_lBloqueio := .F.
		Else
			_lBloqueio := .T.
		EndIf
	EndIf

	lBloqueio := _lBloqueio
Return lBloqueio
