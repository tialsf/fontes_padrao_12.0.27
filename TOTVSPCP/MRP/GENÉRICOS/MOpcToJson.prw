#INCLUDE "TOTVS.CH"

Static _nTamPrd := Nil
Static _nTamTrt := Nil
Static _nTamGrp := Nil
Static _nTamItm := Nil

/*/{Protheus.doc} MOpcToJson
Faz a convers�o das informa��es dos opcionais do formato MEMO (Array convertido para string) 
para o formato JSON, aplicando algumas regras para diminuir o volume de informa��es. 

@type  Function
@author lucas.franca
@since 11/05/2019
@version P12.1.25
@param mOpc , String , String do campo MEMO de Opcionais
@param nType, Numeric, Indica o tipo de retorno do JSON. 
                        1=Retorna uma STRING representando o array de opcionais em JSON.
						2=Retorna o ARRAY com os objetos JSON.
@return xOpcJson, String, Informa��es de opcionais convertidas para Json, em formato STRING ou ARRAY, de acordo com o par�metro nType
/*/
Function MOpcToJson(mOpc, nType)
	Local aOpc       := Nil
	Local cAliasOpc  := ""
	Local cConcat    := ""
	Local cPai       := ""
	Local cComp      := ""
	Local cTrt       := ""
	Local cQuery     := ""
	Local cNewPath   := ""
	Local cNewOpc    := ""
	Local cNameSG1   := RetSqlName("SG1")
	Local cFilSG1    := xFilial("SG1")
	Local nIndex     := 0
	Local nTam       := 0
	Local nTamPath   := 0
	Local oNewOpc    := Nil
	Local xOpcJson   := Nil

	TamFields()

	//Verifica se a string recebida por par�metro � v�lida para processar a convers�o.
	//Se n�o for, ir� retornar NIL.
	If mOpc != Nil .And. mOpc != "" .And. (aOpc := Str2Array(mOpc,.F.)) != Nil
		nTam := Len(aOpc)
		
		If Upper(TcGetDb()) $ 'ORACLE,DB2,POSTGRES,INFORMIX' //CZI_DOC
			cConcat += "||"
		Else
			cConcat += "+"
		EndIf

		oNewOpc := JsonObject():New()

		For nIndex := 1 To nTam
			//Se n�o existir informa��o de grupo/item opcional, desconsidera este item.
			If AllTrim(aOpc[nIndex][2]) == '/'
				Loop
			EndIf

			//Recupera as informa��es do PATH, referente ao �ltimo Produto Pai + Produto Componente + TRT Componente.
			nTamPath := Len(aOpc[nIndex][1])

			//Se � uma informa��o do 1� n�vel, deve desconsiderar o TRT, pois no primeiro produto PAI n�o tem esta informa��o
			If nTamPath == (_nTamPrd*2)+_nTamTrt
				cPai  := SubStr(Right(aOpc[nIndex][1], (_nTamPrd*2)+3), 1, _nTamPrd)
			Else
				cPai  := SubStr(Right(aOpc[nIndex][1], (_nTamPrd*2)+_nTamTrt+3), 1, _nTamPrd)
			EndIf
			cComp := SubStr(Right(aOpc[nIndex][1], _nTamPrd+_nTamTrt), 1, _nTamPrd)
			cTrt  := Right(aOpc[nIndex][1], _nTamTrt)

			//Verifica na SG1 se para o PAI+COMP+TRT, existem GRUPO+ITEM opcionais cadastrados, e se s�o v�lidos
			//de acordo com os opcionais que est�o marcados no array.
			cQuery := " SELECT COUNT(*) TOTAL "
			cQuery +=   " FROM " + cNameSG1 + " SG1 "
			cQuery +=  " WHERE SG1.G1_FILIAL  = '" + cFilSG1 + "' "
			cQuery +=    " AND SG1.G1_COD     = '" + cPai + "' "
			cQuery +=    " AND SG1.G1_COMP    = '" + cComp + "' "
			cQuery +=    " AND SG1.G1_TRT     = '" + cTrt + "' "
			cQuery +=    " AND SG1.D_E_L_E_T_ = ' ' "
			cQuery +=    " AND '" + aOpc[nIndex][2] + "' LIKE '%'" + cConcat + "SG1.G1_GROPC" + cConcat + "SG1.G1_OPC" + cConcat + "'%'"

			cAliasOpc := GetNextAlias()

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasOpc,.T.,.T.)
			If (cAliasOpc)->(TOTAL) > 0
				cNewPath := ConvPath(aOpc[nIndex][1])
				cNewOpc  := ConvOpc(aOpc[nIndex][2])

				addJson(cNewPath, cNewOpc, @oNewOpc)
			EndIf
			(cAliasOpc)->(dbCloseArea())

		Next nIndex
		xOpcJson := getStrOpc(@oNewOpc, nType)

		FreeObj(oNewOpc)
		oNewOpc := Nil
	EndIf
Return xOpcJson

/*/{Protheus.doc} TamFields
Carrega as vari�veis est�ticas com o tamanho dos campos.

@type  Static Function
@author lucas.franca
@since 11/05/2019
@version P12.1.25
/*/
Static Function TamFields()
	If _nTamGrp == Nil
		_nTamGrp := GetSx3Cache("GA_GROPC","X3_TAMANHO")
	EndIf
	If _nTamItm == Nil
		_nTamItm := GetSx3Cache("GA_OPC","X3_TAMANHO")
	EndIf
	If _nTamPrd == Nil
		_nTamPrd := GetSx3Cache("B1_COD","X3_TAMANHO")
	EndIf
	If _nTamTrt == Nil
		_nTamTrt := GetSx3Cache("G1_TRT","X3_TAMANHO")
	EndIf
Return Nil

/*/{Protheus.doc} ConvPath
Converte um PATH da estrutura para o formato que dever� ser utilizado no JSON.
Remove os espa�os desnecess�rios e adiciona separadores nas informa��es.

@type  Static Function
@author lucas.franca
@since 11/05/2019
@version P12.1.25
@param cPath, String, Path da estrutura que dever� ser convertida.
@return cNewPath, String, Path da estrutura convertida.
/*/
Static Function ConvPath(cPath)
	Local cNewPath := ""
	Local cProduto := ""
	Local cTrt     := ""
	Local lFirst   := .T.

	//Remove o �ltimo COMPONENTE+TRT do Path.
	cPath := SubStr(cPath, 1, Len(cPath)-(_nTamPrd+_nTamTrt))

	While !Empty(cPath)
		cProduto := Left(cPath, _nTamPrd)
		cTrt     := SubStr(cPath, _nTamPrd+1, _nTamTrt)
		If lFirst
			cNewPath := RTrim(cProduto)
			cPath    := SubStr(cPath, _nTamPrd+1, Len(cPath))
		Else
			cNewPath += "|" + RTrim(cProduto) + ";" + cTrt
			cPath    := SubStr(cPath, _nTamPrd+_nTamTrt+1, Len(cPath))
		EndIf
		
		lFirst := .F.
	End
Return cNewPath

/*/{Protheus.doc} ConvOpc
Converte os opcionais, retirando espa�os desnecess�rios e adicionando separadores nas informa��es.

@type  Static Function
@author lucas.franca
@since 11/05/2019
@version P12.1.25
@param cOpc, String, Opcionais que ser�o convertidos
@return cNewOpc, String, Opcionais convertidos.
/*/
Static Function ConvOpc(cOpc)
	Local aOpc    := StrTokArr(cOpc, '/')
	Local cNewOpc := ""
	Local cGrupo  := ""
	Local cItem   := ""
	Local nIndex  := 0
	Local nTam    := Len(aOpc)

	For nIndex := 1 To nTam
		cGrupo := SubStr(aOpc[nIndex], 1, _nTamGrp)
		cItem  := SubStr(aOpc[nIndex], _nTamGrp+1, _nTamItm)

		If nIndex > 1
			cNewOpc += "|"
		EndIf

		cNewOpc += RTrim(cGrupo) + ';' + RTrim(cItem)
	Next nIndex
	
Return cNewOpc

/*/{Protheus.doc} addJson
Adiciona a informa��o de PATH e OPCIONAL no objeto JSON.

@type  Static Function
@author lucas.franca
@since 11/05/2019
@version P12.1.25
@param cNewPath , String, Path de estrutura que dever� ser salvo no JSON
@param cNewOpc  , String, Opcionais da estrutura que devem ser salvos no JSON
@param oOpcJson , Object, Objeto JSON com as informa��es dos opcionais. Passar por refer�ncia.
@return Nil
/*/
Static Function addJson(cNewPath, cNewOpc, oOpcJson)
	Local aQuebraOpc := {}
	Local nIndex     := 0
	Local nTam       := 0

	If oOpcJson[cNewPath] == Nil
		//PATH ainda n�o existe, apenas adiciona o PATH e Opcionais no objeto.
		oOpcJson[cNewPath] := JsonObject():New()
		oOpcJson[cNewPath]["key"]   := cNewPath
		oOpcJson[cNewPath]["value"] := cNewOpc
	Else
		//PATH j� existe. Verifica se precisa adicionar algum opcional no PATH j� existente.
		aQuebraOpc := StrTokArr(cNewOpc, '|')
		nTam := Len(aQuebraOpc)
		For nIndex := 1 To nTam
			If At(aQuebraOpc[nIndex], oOpcJson[cNewPath]["value"]) == 0
				//Se o GRUPO+ITEM n�o existir ainda no objeto JSON, adiciona.
				oOpcJson[cNewPath]["value"] += "|" + aQuebraOpc[nIndex]
			EndIf
		Next nIndex
	EndIf

Return Nil

/*/{Protheus.doc} getStrOpc
Converte o OBJETO Json criado com as novas informa��es de Opcionais para STRING

@type  Static Function
@author lucas.franca
@since 11/05/2019
@version P12.1.25
@param oNewOpc, Object, Objeto JSON com as informa��es dos opcionals
@param nType  , Numeric, Indica o tipo de retorno do JSON. 
                          1=Retorna uma STRING representando o array de opcionais em JSON.
                          2=Retorna o ARRAY com os objetos JSON.
@return xNewOpc, String, Informa��es de opcionais convertidas para Json, em formato STRING ou ARRAY, de acordo com o par�metro nType
/*/
Static Function getStrOpc(oNewOpc, nType)
	Local aKeys   := oNewOpc:GetNames()
	Local aNewOpc := {}
	Local xNewOpc := ""
	Local nIndex  := 0
	Local nTam    := Len(aKeys)

	//Formata as informa��es contidas no JSON criado, para ser um ARRAY com as informa��es de PATH e OPCIONAL
	For nIndex := 1 To nTam
		aAdd(aNewOpc, oNewOpc[aKeys[nIndex]])
	Next nIndex

	//Converte o ARRAY para o formato JSON.
	If nType == 1
		xNewOpc := FwJsonSerialize(aNewOpc,.F.,.F.)
	Else
		xNewOpc := aNewOpc
	EndIf
Return xNewOpc