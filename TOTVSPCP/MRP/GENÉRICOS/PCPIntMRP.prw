#INCLUDE "TOTVS.CH"
#INCLUDE "PCPINTMRP.CH"

#DEFINE CRLF CHR(10)
#DEFINE TAMANHO  1
#DEFINE DECIMAL  2
#DEFINE ERP      1
#DEFINE MRP      2
#DEFINE TABELA   1
#DEFINE CAMPOS   2
#DEFINE SINAL    3

Static _lPCPMADI  := SuperGetMV('MV_PCPMADI',.F.,.F.)
Static _nTmFil    := Nil
Static _nTmApi    := Nil
Static _nTmStatus := Nil
Static _nTmID     := Nil
Static _nTmProg   := Nil
Static _nTmMsgRet := Nil
Static _nTmAtrJsn := 15

/*/{Protheus.doc} IntNewMRP
Verifica se a integra��o com o novo MRP est� ativada.

@type  Function
@author lucas.franca
@since 17/05/2019
@version P12.1.25
@param cApi   , Caracter, API para verificar a integra��o
@param lOnline, Logical , Identifica se a API est� configurada para ser
                          executada de modo Online (.T.), ou em BATH (.F.).
						  Passar por refer�ncia.
@return lAtivo, Logical , Indica se a integra��o est� ativa ou n�o
/*/
Function IntNewMRP(cApi, lOnline)
	Local lAtivo := .F.

	//Verifica se a integra��o est� ativa.
	If !FWAliasInDic("T4P",.F.)
		//Se a tabela n�o existe, retorna .F.
		Return .F.
	EndIf

	//Carrega vari�veis com tamanho dos campos
	cargaTam()

	//Ajusta o tamanho da vari�vel cApi
	cApi := PadR(cApi, _nTmApi)

	T4P->(dbSetOrder(1))
	If T4P->(dbSeek(xFilial("T4P")+cApi)) .And. T4P->T4P_ATIVO == '1'
		lAtivo := .T.
		//Se a integra��o estiver ativa, verifica o modo de integra��o (Online ou Batch)
		If T4P->T4P_TPEXEC == '1'
			lOnline := .T.
		Else
			lOnline := .F.
		EndIf
	Else
		lAtivo := .F.
	EndIf
Return lAtivo

/*/{Protheus.doc} TTPendMRP
Cria a tabela tempor�ria para utilizar no processamento das pend�ncias do MRP.
@type  Function
@author lucas.franca
@since 24/06/2019
@version P12.1.27
@return oTemp, Object, Objeto da tabela tempor�ria que deve ser utilizado no processamento das pend�ncias.
/*/
Function TTPendMRP()
	Local oTemp := FwTemporaryTable():New()
	Local aFields    := {}
	Local aArea      := GetArea()

	//Carrega vari�veis com tamanho dos campos
	cargaTam()

	aAdd(aFields, {"CODIGO" , "C", _nTmID    , 0}) //C�digo �nico do registro
	aAdd(aFields, {"STATUS" , "C", 1         , 0}) //Status do processo (1=OK;2=Erro;3=Excluir pendencia)
	aAdd(aFields, {"ATRJSON", "C", _nTmAtrJsn, 0}) //Identificador do atributo onde o registro est� no JSON de retorno.
	aAdd(aFields, {"POSICAO", "N", 10        , 0}) //Posi��o do registro no JSON de retorno.
	aAdd(aFields, {"MSGRET" , "C", _nTmMsgRet, 0}) //Posi��o do registro no JSON de retorno.
	aAdd(aFields, {"MSGENV" , "M", 80        , 0}) //Posi��o do registro no JSON de retorno.

	oTemp:SetFields(aFields)
	oTemp:AddIndex("01", {"CODIGO"} )
	oTemp:Create()

	RestArea(aArea)
Return oTemp

/*/{Protheus.doc} PrcPendMRP
Fun��o respons�vel por atualizar a tabela de pend�ncias de integra��o do MRP

@type Function
@author douglas.heydt
@since 18/06/2019
@version P12.1.27
@param aReturn  , Array    , Array com os itens retornados pela API
@param cApi     , Character, Nome da Api que gerou o registro
@param oJsonData, Object   , Objeto Json que cont�m os registros para atualiza��o (utilizado apenas em caso de erro)
@param oTTPend  , Object   , Objeto da tabela tempor�ria utilizada no processamento. Tabela criada pela fun��o TTPendMRP
@param lReproc  , Logical  , Indica se � reprocessamento.
@param aSuccess , Array    , Array com os itens integrados corretamente pela API. Opcional. Passar por refer�ncia.
@param aError   , Array    , Array com os itens que n�o puderam ser integrados. Opcional. Passar por refer�ncia.
@param lAllError, logico   , Var�avel que indica se foi poss�vel executar a leitura do retorno da API. Opcional. Passar por refer�ncia.
@param cTipo    , Character, Indica o tipo do movimento. 1=Inclus�o; 2=Exclus�o.
@param cUUID    , Character, Identificador do processo do SCHEDULE. Utilizado para atualiza��o de pend�ncias.
/*/
Function PrcPendMRP(aReturn, cApi, oJsonData, oTTPend, lReproc, aSuccess, aError, lAllError, cTipo, cUUID)
	Local cError   := ""
	Local lUsaEnv  := .F.
	Local oJsonRet := JsonObject():New()

	Default cUUID := ""

	cError := oJsonRet:FromJson(aReturn[2])

	If !Empty(cError)
		atuTTPend(oTTPend, oJsonData, .T., cApi, oJsonData, @lUsaEnv)
		//Se n�o conseguiu interpretar o JSON, ir� marcar todos os registros como erro de integra��o.
		GrvPendMRP(0, oJsonData, lReproc, cApi, oTTPend, cTipo, cUUID)
		lAllError := .T.
	Else
		If aReturn[1] == 204
			//Exclus�o com sucesso. Atualiza pend�ncia de acordo
			//com os dados que foram enviados.
			atuTTPend(oTTPend, oJsonData, .F., cApi, oJsonData, @lUsaEnv)
			GrvPendMRP(aReturn[1], oJsonData, lReproc, cApi, oTTPend, cTipo, cUUID)
		Else
			atuTTPend(oTTPend, oJsonRet, .F., cApi, oJsonData, @lUsaEnv)
		EndIf

		If aReturn[1] == 201 .Or. aReturn[1] == 207 .Or. aReturn[1] == 400 .Or. aReturn[1] == 503
			GrvPendMRP(aReturn[1], Iif(lUsaEnv, oJsonData, oJsonRet), lReproc, cApi, oTTPend, cTipo, cUUID)
		EndIf

		//Alimenta os arrays aSuccess e aError
		If (aReturn[1] == 201 .Or. aReturn[1] == 207) .And. oJsonRet["items"] != Nil .And. Len(oJsonRet["items"]) > 0
			aSuccess := oJsonRet["items"]
		EndIf
		If aReturn[1] == 207 .And. oJsonRet["_messages"] != Nil .And. Len(oJsonRet["_messages"]) > 0
			aError := oJsonRet["_messages"]
		EndIf
		If (aReturn[1] == 400 .Or. aReturn[1] == 503)                     .And. ;
		   (oJsonRet["details"] != Nil .And. Len(oJsonRet["details"]) > 0 .Or.  ;
		   lUsaEnv .And. oJsonData["items"] != Nil)
			If lUsaEnv
				aError := oJsonData["items"]
				lAllError := .T.
			Else
				aError := oJsonRet["details"]
			EndIf
		EndIf
		If aReturn[1] == 204 .And. oJsonData["items"] != Nil .And. Len(oJsonData["items"]) > 0
			aSuccess := oJsonData["items"]
		EndIf
	EndIf
Return

/*/{Protheus.doc} GrvPendMRP
Fun��o respons�vel por deletar todos os registros da tabela T4R que tenham sido integrados com sucesso

@type Function
@author douglas.heydt
@since 18/06/2019
@version P12.1.27
@param nCode   , Numeral  , c�digo de retorno da API
@param oJsonRet, Character, Objeto Json que cont�m os registros para atuliza��o
@param lReproc , logico   , Indica o status do registro. 1 = Pendente; 2 = Reprocesso com erro;
@param cApi    , Character, Nome da Api que gerou o registro
@param oTTPend , Object   , Objeto da tabela tempor�ria utilizada no processamento. Tabela criada pela fun��o TTPendMRP
@param cTipo   , Character, Indica o tipo do movimento. 1=Inclus�o; 2=Exclus�o.
@param cUUID   , Character, Identificador do processo do SCHEDULE. Utilizado para atualiza��o de pend�ncias.
/*/
Function GrvPendMRP(nCode, oJsonRet, lReproc, cApi, oTTPend, cTipo, cUUID)

	Default cUUID := ""

	If nCode == 201 .Or. nCode == 204 .Or. nCode == 207 .Or. ("|"+AllTrim(Upper(cApi))+"|" $ "|MRPSTOCKBALANCE|MRPREJECTEDINVENTORY|" .And. !Empty(cUUID))
		/*busca e deleta os registros integrados com sucesso*/
		delSuccess(cApi, oTTPend, cUUID)
	EndIf
	If nCode == 207 .Or. nCode == 400 .Or. nCode == 503 .Or. nCode == 0
		/*atualiza/insere na tabela T4R todos os registros com problemas*/
		updErrors(nCode, oJsonRet, cApi, lReproc, oTTPend, cTipo, cUUID)
	EndIf
Return

/*/{Protheus.doc} delSuccess
Fun��o respons�vel por deletar todos os registros da tabela T4R que tenham sido integrados com sucesso

@type  Static Function
@author douglas.heydt
@since 18/06/2019
@version P12.1.27
@param cApi    , Character, Nome da Api que gerou o registro
@param oTTPend , Object   , Objeto da tabela tempor�ria utilizada no processamento. Tabela criada pela fun��o TTPendMRP
@param cUUID   , Character, Identificador do processo do SCHEDULE. Utilizado para atualiza��o de pend�ncias.
/*/
Static Function delSuccess(cApi, oTTPend, cUUID)
	Local cQuery  := ""

	Default cUUID := ""

	cQuery := " DELETE FROM " + RetSqlName("T4R")
	cQuery +=  " WHERE T4R_FILIAL = '" + xFilial("T4R") + "' "
	cQuery +=    " AND T4R_API    = '" + cApi + "' "
	If !Empty(cUUID)
		cQuery += " AND T4R_IDPRC = '" + cUUID + "' "
	EndIf
	//Quando � a API de estoque e est� executando via Schedule, apaga as pend�ncias de processamento relacionadas independente do status;
	If !("|"+AllTrim(Upper(cApi))+"|" $ "|MRPSTOCKBALANCE|MRPREJECTEDINVENTORY|" .And. !Empty(cUUID))
		cQuery +=    " AND EXISTS ( SELECT 1 "
		cQuery +=                   " FROM " + oTTPend:GetRealName()
		cQuery +=                  " WHERE STATUS = '1' "
		cQuery +=                    " AND CODIGO = T4R_IDREG ) "
	EndIf

	If TcSqlExec(cQuery) < 0
		Final(STR0003, tcSQLError()+cQuery) //"Erro ao excluir as pend�ncias."
	EndIf
Return

/*/{Protheus.doc} updErrors
Fun��o respons�vel por avaliar o objeto retornado pelo REST e atualizar/inserir registros da tabela T4R

@type  Static Function
@author douglas.heydt
@since 18/06/2019
@version P12.1.27
@param nCode   , Numeral  , C�digo de erro do REST
@param oJsonRet, Character, Objeto Json que cont�m os registros para atuliza��o
@param cApi    , Character, Nome da Api que gerou o registro
@param lReproc , logico   , Indica o status do registro. 1 = Pendente; 2 = Reprocesso com erro;
@param oTTPend , Object   , Objeto da tabela tempor�ria utilizada no processamento. Tabela criada pela fun��o TTPendMRP
@param cTipo   , Character, Indica o tipo do movimento. 1=Inclus�o; 2=Exclus�o.
@param cUUID   , Character, Identificador do processo do SCHEDULE. Utilizado para atualiza��o de pend�ncias.
/*/
Static Function updErrors(nCode, oJsonRet, cApi, lReproc, oTTPend, cTipo, cUUID)
	Local aInserts  := {}
	Local cAliasIns := GetNextAlias()
	Local cEstrut   := ""
	Local cFilT4R   := xFilial("T4R")
	Local cSql      := ""
	Local cReprHora := Nil
	Local cReprData := Nil
	Local cJsonEnv  := ""
	Local nIndex    := 0

	Default cUUID := ""

	//Carrega vari�veis com tamanho dos campos
	cargaTam()

	If lReproc
		cReprHora := Time()
		cReprData := dDataBase
	EndIf

	//Deleta os registros com status 3 na temp table.
	//Status 3 = Registro deletado, onde a API retornou erro de registro n�o encontrado.
	cSql := " DELETE FROM " + RetSqlName("T4R")
	cSql +=  " WHERE T4R_FILIAL = '" + xFilial("T4R") + "' "
	cSql +=    " AND T4R_API    = '" + cApi + "' "
	If !Empty(cUUID)
		cSql += " AND T4R_IDPRC = '" + cUUID + "' "
	EndIf
	cSql +=    " AND EXISTS ( SELECT 1 "
	cSql +=                   " FROM " + oTTPend:GetRealName()
	cSql +=                  " WHERE STATUS = '3' "
	cSql +=                    " AND CODIGO = T4R_IDREG ) "

	If TcSqlExec(cSql) < 0
		Final(STR0003, tcSQLError()+cSql) //"Erro ao excluir as pend�ncias."
	EndIf

	//Faz o UPDATE dos registros que j� existem na T4R.
	If AllTrim(Upper(TcGetDb())) == "ORACLE"
		cSql := " MERGE INTO " + RetSqlName("T4R") + " T4R "
		cSql +=      " USING (SELECT CODIGO, MSGRET, MSGENV "
		cSql +=               " FROM " + oTTPend:GetRealName()
		cSql +=              " WHERE STATUS     = '2') TMP "
		cSql +=         " ON (T4R_FILIAL    = '" + cFilT4R + "' "
		cSql +=         " AND T4R_API       = '" + cApi + "' "
		If !Empty(cUUID)
			cSql +=     " AND T4R_IDPRC = '" + cUUID + "' "
		EndIf
		cSql +=         " AND T4R.T4R_IDREG = TMP.CODIGO ) "
		cSql +=  " WHEN MATCHED THEN "
		cSql +=             " UPDATE SET T4R.T4R_MSGRET = TMP.MSGRET, "
		cSql +=                        " T4R.T4R_MSGENV = TMP.MSGENV, "
		cSql +=                        " T4R.T4R_TIPO   = '" + cTipo + "', "
		If Empty(cUUID)
			cSql +=                    " T4R.T4R_IDPRC  = ' ', "
		EndIf
		If lReproc
			cSql +=                    " T4R.T4R_STATUS = '2', "
			cSql +=                    " T4R.T4R_DTREP  = '" + DtoS(cReprData) + "', "
			cSql +=                    " T4R.T4R_HRREP  = '" + cReprHora + "' "
		Else
			cSql +=                    " T4R.T4R_STATUS = '1', "
			cSql +=                    " T4R.T4R_DTREP  = ' ', "
			cSql +=                    " T4R.T4R_HRREP  = ' ', "
			cSql +=                    " T4R.T4R_DTENV  = '" + DtoS(dDataBase) + "', "
			cSql +=                    " T4R.T4R_HRENV  = '" + TIME() + "', "
			cSql +=                    " T4R.T4R_PROG   = '" + PadR(FunName(), _nTmProg) + "' "
		EndIf
	Else
		cSql := " UPDATE " + RetSqlName("T4R")
		cSql += " SET T4R_MSGRET = MSGRET, "
		cSql +=     " T4R_MSGENV = MSGENV, "
		cSql +=     " T4R_TIPO   = '" + cTipo + "', "
		If Empty(cUUID)
			cSql += " T4R_IDPRC  = ' ', "
		EndIf
		If lReproc
			cSql += " T4R_STATUS = '2', "
			cSql += " T4R_DTREP  = '" + DtoS(cReprData) + "', "
			cSql += " T4R_HRREP  = '" + cReprHora + "' "
		Else
			cSql += " T4R_STATUS = '1', "
			cSql += " T4R_DTREP  = ' ', "
			cSql += " T4R_HRREP  = ' ', "
			cSql += " T4R_DTENV  = '" + DtoS(dDataBase) + "', "
			cSql += " T4R_HRENV  = '" + TIME() + "', "
			cSql += " T4R_PROG   = '" + PadR(FunName(), _nTmProg) + "' "
		EndIf
		cSql +=  " FROM " + oTTPend:GetRealName()
		cSql += " WHERE T4R_FILIAL = '" + cFilT4R + "' "
		cSql +=   " AND T4R_API    = '" + cApi + "' "
		If !Empty(cUUID)
			cSql += " AND T4R_IDPRC = '" + cUUID + "' "
		EndIf
		cSql +=   " AND T4R_IDREG  = CODIGO "
		cSql +=   " AND STATUS     = '2' "
	EndIf

	If TcSqlExec(cSql) < 0
		Final(STR0004, tcSQLError()+cSql) //"Erro ao atualizar as pend�ncias."
	EndIf

	//Busca os registros que est�o com erro, e ainda n�o existem na T4R para inserir.
	cSql := " SELECT CODIGO, STATUS, ATRJSON, POSICAO, MSGRET "
	cSql +=   " FROM " + oTTPend:GetRealName()
	cSql +=  " WHERE STATUS = '2' "
	cSql +=    " AND NOT EXISTS ( SELECT 1 "
	cSql +=                       " FROM " + RetSqlName("T4R")
	cSql +=                      " WHERE T4R_FILIAL = '" + cFilT4R + "' "
	cSql +=                        " AND T4R_API    = '" + cApi + "' "
	cSql +=                        " AND T4R_IDREG  = CODIGO ) "

	dbUseArea(.T.,"TOPCONN",TCGenQry(,,cSql),cAliasIns,.F.,.F.)
	While (cAliasIns)->(!Eof())
		cEstrut := AllTrim((cAliasIns)->(ATRJSON))
		nIndex  := (cAliasIns)->(POSICAO)
		If (nCode == 207 .Or. nCode >= 400) .And. ValType(oJsonRet[cEstrut][nIndex]["code"]) == "N" .And. oJsonRet[cEstrut][nIndex]["code"] == 404
			//Se ocorreu erro durante a exclus�o, e o erro ocorrido foi de n�o encontrar  o registro
			//N�o gera pend�ncia.
			(cAliasIns)->(dbSkip())
			Loop
		EndIf
		If cEstrut == "items"
			cJsonEnv := oJsonRet[cEstrut][nIndex]:toJson()
		Else
			cJsonEnv := oJsonRet[cEstrut][nIndex]["detailedMessage"]:toJson()
		EndIf
		aAdd(aInserts, {PadR(cFilT4R              , _nTmFil)   ,;
		                PadR(cApi                 , _nTmApi)   ,;
		                PadR('1'                  , _nTmStatus),;
		                PadR((cAliasIns)->(CODIGO), _nTmID)    ,;
		                dDataBase                              ,;
		                TIME()                                 ,;
		                PadR(FunName()            , _nTmProg)  ,;
		                PadR((cAliasIns)->(MSGRET), _nTmMsgRet),;
		                cJsonEnv                               ,;
		                cReprHora                              ,;
		                cReprData                              ,;
		                cTipo                                  })
		(cAliasIns)->(dbSkip())
	End
	(cAliasIns)->(dbCloseArea())
	If Len(aInserts) > 0
		InsPendMrp(aInserts)
	EndIf

	/*
		Se possuir o cUUID, significa que � uma execu��o via SCHEDULE.
		Neste caso, procura por registros de pend�ncia do mesmo IDREG, onde o campo T4R_IDPRC est� em branco.
		Se encontrar, ir� excluir a pend�ncia que est� com o campo T4R_IDPRC preenchido, mantendo somente o registro
		que est� com o T4R_IDPRC em branco.
	*/
	If !Empty(cUUID)
		cSql := " DELETE FROM " + RetSqlName("T4R")
		cSql += " WHERE T4R_FILIAL = '" + cFilT4R + "' "
		cSql +=   " AND T4R_API    = '" + cApi + "' "
		cSql +=   " AND T4R_IDPRC  = '" + cUUID + "' "
		cSql +=   " AND EXISTS ( SELECT 1 "
		cSql +=                  " FROM " + RetSqlName("T4R") + " T4RAUX "
		cSql +=                 " WHERE T4RAUX.T4R_FILIAL = '" + cFilT4R + "' "
		cSql +=                   " AND T4RAUX.T4R_API    = '" + cApi + "' "
		cSql +=                   " AND T4RAUX.T4R_IDPRC  = ' ' "
		cSql +=                   " AND T4RAUX.T4R_IDREG  = T4R_IDREG )"

		If TcSqlExec(cSql) < 0
			Final(STR0004, tcSQLError()+cSql) //"Erro ao atualizar as pend�ncias."
		EndIf

		//Limpa o campo T4R_IDPRC dos registros que ficaram com o status PENDENTE.
		cSql := " UPDATE " + RetSqlName("T4R")
		cSql +=    " SET T4R_IDPRC = ' ' "
		cSql += " WHERE T4R_FILIAL = '" + cFilT4R + "' "
		cSql +=   " AND T4R_API    = '" + cApi + "' "
		cSql +=   " AND T4R_IDPRC  = '" + cUUID + "' "

		If TcSqlExec(cSql) < 0
			Final(STR0004, tcSQLError()+cSql) //"Erro ao atualizar as pend�ncias."
		EndIf

	EndIf

Return

/*/{Protheus.doc} InsPendMrp
Cria sem�foro para inclus�o da tabela T4R, para controlar corretamente o RECNO da tabela.
Utilizado somente quando a tabela T4R n�o possuir RECNO Auto Incremental.
Tamb�m realiza a inclus�o de registros na tabela T4R

@type  Static Function
@author douglas.heydt
@since 18/06/2019
@version P12.1.27
@param aInserts , Array  , Array contendo os registros que devem ser inseridos na tabela T4R
/*/
Static Function InsPendMrp(aInserts)
	Local lRet 	 	:= .T.
	Local cTabela	:= "T4R"
	Local cTabSql 	:= "%"+RetSqlName(cTabela)+"%"
	Local nRecno 	:= 0
	Local nIndex	:= 0
	Local lAutRec	:= InfoSX2("T4R",'X2_AUTREC') == '1'
	Local cCols 	:= "T4R_FILIAL,T4R_API,T4R_STATUS,T4R_IDREG,T4R_DTENV,T4R_HRENV,T4R_PROG,T4R_MSGRET,T4R_MSGENV,T4R_HRREP,T4R_DTREP,T4R_TIPO"

	If !lAutRec
		cCols += ",R_E_C_N_O_"

		//Cria sem�foro para inclus�o em modo exclusivo.
		lRet := SemafTable(.T., cTabela)
		If !lRet
			Final(STR0002)//"N�o foi poss�vel obter acesso exclusivo � base de dados !"
		Else
			//Recupera o maior RECNO da tabela.
			cAlias := GetNextAlias()
			BeginSql Alias cAlias
				%noparser%
				SELECT MAX(R_E_C_N_O_) MAXREC
					FROM %Exp:cTabSql%
			EndSql
			nRecno := (cAlias)->MAXREC
			(cAlias)->(dbCloseArea())

			//Adiciona a informa��o do RECNO no array de dados.
			nTotal := Len(aInserts)
			For nIndex := 1 To nTotal
				nRecno++
				aAdd(aInserts[nIndex],nRecno)
			Next nIndex
		EndIf
	EndIf

	//Executa o INSERT dos dados em bloco.
	If TCDBInsert(RetSqlName(cTabela),cCols,aInserts) < 0
		lRet := .F.
		Final(STR0005, tcSQLError()) //"Erro ao inserir os dados das pend�ncias."
	Endif
	If !lAutRec
		//Libera sem�foro.
		SemafTable(.F., cTabela)
	EndIf
Return

/*/{Protheus.doc} SemafTable
Cria sem�foro para inclus�o da tabela T4N, para controlar corretamente o RECNO da tabela.
Utilizado somente quando a tabela T4N n�o possuir RECNO Auto Incremental.

@type  Static Function
@author douglas.heydt
@since 18/06/2019
@version P12.1.27
@param lLock , Logical  , Se verdadeiro, ir� tentar fazer o LOCK. Se falso, ir� liberar o lock.
@param cTable, Character, Tabela que est� sendo processada para criar o sem�foro.
@return lRet , Logical  , Se verdadeiro, conseguiu fazer o lock.
/*/
Static Function SemafTable(lLock, cTable)
	Local nTry := 0

	If lLock
		While !LockByName("PENDMRP"+cTable+cEmpAnt,.T.,.T.)
			nTry++
			If nTry > 1000
				Return .F.
			EndIf
			Sleep(100)
		End
	Else
		UnLockByName("PENDMRP"+cTable+cEmpAnt,.T.,.T.)
	EndIf
Return .T.

/*/{Protheus.doc} atuTTPend
Fun��o respons�vel por alimentar a tabela tempor�ria com os dados que ser�o atualizados
na tabela de pend�ncias.

@type  Static Function
@author lucas.franca
@since 24/06/2019
@version P12.1.27
@param oTemp    , Object   , Objeto da tabela tempor�ria. Tabela criada pela fun��o TTPendMRP.
@param oDados   , Object   , Objeto JSON com os dados que ser�o processados.
@param lError   , Logic    , Indica se os dados devem ser considerados todos como erro.
@param cApi     , Character, API que est� sendo processada.
@param oDadosEnv, Object   , Dados que foram enviados para a API
@param lUsaEnv  , Logic    , Retorna por refer�ncia se deve utilizar os dados que foram enviados para incluir pend�ncias
@return Nil
/*/
Static Function atuTTPend(oTemp, oDados, lError, cApi, oDadosEnv, lUsaEnv)
	Local aDados   := {}
	Local cSql     := ""
	Local cCols    := ""
	Local cCode    := ""
	Local cStatus  := "1"
	Local cMessage := ""
	Local mJsonEnv := ""
	Local nIndex   := 0
	Local nTotal   := 0

	lUsaEnv := .F.

	//Carrega vari�veis com tamanho dos campos
	cargaTam()

	//Limpa os registros que est�o na temp
	cSql := " DELETE FROM " + oTemp:GetRealName()
	If TcSqlExec(cSql) < 0
		Final(STR0006, tcSQLError() + cSql) //"Erro ao atualizar os dados."
	EndIf
	//Monta o array com os dados para gera��o de pendencias.
	If oDados["items"] != Nil
		nTotal := Len(oDados["items"])
		If lError
			cStatus  := "2"
			cMessage := STR0001 //"N�o foi poss�vel interpretar a mensagem retornada pela API, todos os items foram registrados como pend�ncia de integra��o."
		Else
			cStatus  := "1"
			cMessage := ""
			mJsonEnv := ""
		EndIf
		For nIndex := 1 To nTotal
			If lError
				mJsonEnv := oDados["items"][nIndex]:ToJson()
			EndIf
			If !lError .And. cApi == "MRPSTOCKBALANCE" .And. oDados["items"][nIndex]["code"] == Nil
				//Tratativa para o m�todo CLEAR da api MRPSTOCKBALANCE, para n�o apagar as pend�ncias neste momento.
				Loop
			EndIf
			cCode := getCode(oDados["items"][nIndex], cApi)
			aAdd(aDados, {PadR(cCode   , _nTmID)    ,;
			              PadR(cStatus ,_nTmStatus) ,;
			              PadR("items" , _nTmAtrJsn),;
			              nIndex                    ,;
			              PadR(cMessage, _nTmMsgRet),;
			              mJsonEnv                  })
		Next nIndex
	EndIf
	If oDados["_messages"] != Nil
		nTotal  := Len(oDados["_messages"])
		cStatus := "2"
		For nIndex := 1 To nTotal
			cMessage := oDados["_messages"][nIndex]["message"]
			mJsonEnv := oDados["_messages"][nIndex]["detailedMessage"]:ToJson()
			cCode    := getCode(oDados["_messages"][nIndex]["detailedMessage"], cApi)
			If oDados["_messages"][nIndex]["code"] == 404
				cStatus := "3"
			Else
				cStatus := "2"
			EndIf
			aAdd(aDados, {PadR(cCode      , _nTmID)    ,;
			              PadR(cStatus    , _nTmStatus),;
			              PadR("_messages", _nTmAtrJsn),;
			              nIndex                       ,;
			              PadR(cMessage   , _nTmMsgRet),;
			              mJsonEnv                     })
		Next nIndex
	EndIf
	If oDados["details"] != Nil
		nTotal  := Len(oDados["details"])
		cStatus := "2"
		For nIndex := 1 To nTotal
			cMessage := oDados["details"][nIndex]["message"]
			mJsonEnv := oDados["details"][nIndex]["detailedMessage"]:ToJson()
			cCode    := getCode(oDados["details"][nIndex]["detailedMessage"], cApi)
			If oDados["details"][nIndex]["code"] == 404
				cStatus := "3"
			Else
				cStatus := "2"
			EndIf
			aAdd(aDados, {PadR(cCode    , _nTmID)    ,;
			              PadR(cStatus  , _nTmStatus),;
			              PadR("details", _nTmAtrJsn),;
			              nIndex                     ,;
			              PadR(cMessage , _nTmMsgRet),;
			              mJsonEnv                   })
		Next nIndex
	EndIf

	//Verifica se ocorreu algum erro onde n�o retornou os dados processados.
	If !lError               .And. ;
	   Len(aDados) == 0      .And. ;
	   oDados["code"] != Nil .And. ;
	   oDados["code"] == 400 .And. ;
	   oDadosEnv != Nil      .And. ;
	   oDadosEnv["items"] != Nil

		//Adiciona como pend�ncia todos os dados que foram enviados.
		nTotal := Len(oDadosEnv["items"])
		cStatus  := "2"
		cMessage := oDados["detailedMessage"]
		mJsonEnv := ""

		For nIndex := 1 To nTotal
			mJsonEnv := oDadosEnv["items"][nIndex]:ToJson()

			cCode := getCode(oDadosEnv["items"][nIndex], cApi)
			aAdd(aDados, {PadR(cCode   , _nTmID)    ,;
			              PadR(cStatus ,_nTmStatus) ,;
			              PadR("items" , _nTmAtrJsn),;
			              nIndex                    ,;
			              PadR(cMessage, _nTmMsgRet),;
			              mJsonEnv                  })
		Next nIndex

		If nTotal > 0
			lUsaEnv := .T.
		EndIf

	EndIf

	//Insere os dados na temp table
	If Len(aDados) > 0
		cCols := "CODIGO,STATUS,ATRJSON,POSICAO,MSGRET,MSGENV"
		If TCDBInsert(oTemp:cTableName,cCols,aDados) < 0
			Final(STR0006,TCSqlError()) //"Erro ao atualizar os dados."
		EndIf
	EndIf
Return Nil

/*/{Protheus.doc} getCode
Identifica qual � o c�digo que deve ser utilizado para gerar a pend�ncia

@type  Static Function
@author lucas.franca
@since 16/07/2019
@version P12.1.27
@param oData, Object   , Objeto JSON com os dados
@param cApi , Character, C�digo da API
@return cCode, Character, C�digo para gerar a pend�ncia
/*/
Static Function getCode(oData, cApi)
	Local cCode := ""

	If cApi == "MRPBILLOFMATERIAL"
		cCode := oData["branchId"] + oData["product"]
	ElseIf cApi == "MRPSTOCKBALANCE" .And. oData["code"] == Nil
		cCode := oData["branchId"] + oData["product"] + oData["warehouse"]
	ElseIf cApi == "MRPWAREHOUSEGROUP"
        cCode := oData["branchId"] + oData["warehouseGroup"]
	Else
		cCode := oData["code"]
	EndIf
Return cCode

/*/{Protheus.doc} cargaTam
Fun��o que carrega as vari�veis de escopo STATIC com o tamanho dos campos utilizados neste fonte.

@type  Static Function
@author lucas.franca
@since 25/06/2019
@version P12.1.27
/*/
Static Function cargaTam()
	_nTmFil    := Iif(_nTmFil    == Nil, FwSizeFilial()                        ,_nTmFil   )
	_nTmApi    := Iif(_nTmApi    == Nil, GetSx3Cache("T4P_API"   ,"X3_TAMANHO"),_nTmApi   )
	_nTmStatus := Iif(_nTmStatus == Nil, GetSx3Cache("T4R_STATUS","X3_TAMANHO"),_nTmStatus)
	_nTmID     := Iif(_nTmID     == Nil, GetSx3Cache("T4R_IDREG" ,"X3_TAMANHO"),_nTmID    )
	_nTmProg   := Iif(_nTmProg   == Nil, GetSx3Cache("T4R_PROG"  ,"X3_TAMANHO"),_nTmProg  )
	_nTmMsgRet := Iif(_nTmMsgRet == Nil, GetSx3Cache("T4R_MSGRET","X3_TAMANHO"),_nTmMsgRet)
Return

/*/{Protheus.doc} MRPVldSync
Verifica se � necess�rio executar a sincroniza��o para alguma API, de acordo com o conte�do do campo T4P_ALTER.

@type  Static Function
@author lucas.franca
@since 25/06/2019
@version P12.1.27
@param lExibeHelp, logico  , Identifica se deve ser exibido a mensagem de alerta.
@param cCodAPI   , caracter, c�digo da API para sincronizar
@param lBtnSincr , logico  , indica se exibe o bot�o sincronizar agora
@param lMrp      , logico  , Identifica que a chamada da fun��o � feita pelo PCPA712
@return aApiAlter, Array   , Array com os c�digos de API que necessitam de sincroniza��o.
/*/
Function MRPVldSync(lExibeHelp, cCodAPI, lBtnSincr, lMRp)
	Local aApiAlter := {}
	Local aBotoes
	Local cAlias    := ""
	Local cMessage  := ""
	Local cQuery    := ""
	Local cUpdate   := ""
	Local nIndex    := 0
	Local nOpcAviso := 0
	Local oDescri   := Nil
	Local nMrpSinc  := SuperGetMV("MV_MRPSINC", .F., 1)
	Local lAlterada := .F.
	Local lInativa  := .F.

	Default lBtnSincr := .F.
	Default lMRp      := .F.

	If lBtnSincr
		aBotoes   := {STR0026, STR0009} //"Sincronizar Agora", "OK"
	Else
		aBotoes   := {STR0009} //"OK"
	EndIf

	//Se a integra��o estiver ligada, faz a verifica��o.
	If IntNewMRP("MRPDEMANDS")

		//Verifica necessidade de sincroniza��o da API MrpDemands referente inclus�o de novo campo - T4J_CODE
		If !Empty(GetSx3Cache("T4J_CODE" ,"X3_TAMANHO"))
			cUpdate := " UPDATE " + RetSqlName("T4P")         + ;
			           " SET T4P_ALTER = '2' "                + ;
			           " WHERE   (D_E_L_E_T_ = ' ')  "        + ;
					       " AND (T4P_ALTER = '0') "          + ;
					       " AND (T4P_API = 'MRPDEMANDS') "   + ;
						   " AND ((SELECT COUNT(*) AS QTD "   + ;
						         " FROM " + RetSqlName("T4J") + ;
						         " WHERE (D_E_L_E_T_ = ' ') " + ;
								   " AND (T4J_CODE = ' ')) > 0) "
			TcSqlExec(cUpdate)
		EndIf

		cAlias := GetNextAlias()
		cQuery := " SELECT DISTINCT T4P.T4P_API, T4P.T4P_ALTER "
		cQuery +=   " FROM " + RetSqlName("T4P") + " T4P "
		cQuery +=  " WHERE T4P.T4P_FILIAL = '" + xFilial("T4P") + "' "
		cQuery +=    " AND T4P.D_E_L_E_T_ = ' ' "
		If nMrpSinc <> 3 .Or. !lMRp
			cQuery += " AND T4P.T4P_ALTER  IN ('1','2') "
		EndIf

		If !Empty(cCodAPI)
			cQuery += " AND T4P.T4P_API = '" + cCodAPI + "' "
		ElseIf !_lPCPMADI
			cQuery += " AND T4P.T4P_API NOT IN ('MRPTRANSPORTINGLANES','MRPWAREHOUSEGROUP') "
		EndIf

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)
		While (cAlias)->(!Eof())
			aAdd(aApiAlter, (cAlias)->(T4P_API))
			If (cAlias)->(T4P_ALTER) == "1"
				lInativa  := .T.
			ElseIf (cAlias)->(T4P_ALTER) == "2"
				lAlterada := .T.
			EndIf
			(cAlias)->(dbSkip())
		End
		(cAlias)->(dbCloseArea())

		If !lMrp .Or. nMrpSinc == 1 .OR. (lAlterada .AND. nMrpSinc == 2)
			If lExibeHelp .And. Len(aApiAlter) > 0
				oDescri := P139AllAPI()

				If lInativa
					cMessage := STR0007 + CRLF + CRLF //"A integra��o com o MRP esteve desativada por algum per�odo. Para garantir a integridade das informa��es do MRP ser� necess�rio executar a sincroniza��o das seguintes entidades: "
				ElseIf lAlterada
					cMessage := STR0039 + CRLF + CRLF //"Existem atualiza��es nas API's do MRP que requerem sincroniza��o para garantir a integridade das informa��es. Ser� necess�rio executar a sincroniza��o das seguintes entidades: "
				EndIf

				For nIndex := 1 To Len(aApiAlter)
					cMessage += CRLF
					If oDescri[AllTrim(aApiAlter[nIndex])] == Nil
						cMessage += " - " + AllTrim(aApiAlter[nIndex]) + Space(97- Len(AllTrim(aApiAlter[nIndex])))
					Else
						cMessage += " - " + oDescri[AllTrim(aApiAlter[nIndex])] + Space(97- Len(oDescri[AllTrim(aApiAlter[nIndex])]))
					EndIf
				Next nIndex

				nOpcAviso := Aviso(STR0008, cMessage, aBotoes, 3) //"Aten��o"

				FreeObj(oDescri)
				oDescri := Nil
			EndIf
		EndIf

		If lMRp .And. Len(aApiAlter) > 0
			PCPA140(.F. ,aApiAlter ,.F.) //sincroniza todas as APIs identificadas
		EndIf

	EndIf
Return aApiAlter

/*/{Protheus.doc} MRPVldTrig
Valida se a Trigger do MRP est� configurada corretamente

@type  Function
@author brunno.costa
@since 07/08/2019
@version P12.1.28
@param 01 - lExibeHelp, l�gico  , Identifica se deve ser exibido a mensagem de alerta.
@param 02 - cCodAPI   , caracter, c�digo da API para validar/corrigir as triggers
@param 03 - lInstala  , l�gico  , indica se deve realizar as corre��es nas triggers
@param 04 - oModel    , objeto  , objeto do modelo da PCPA139
@param 05 - lExecJob  , l�gico  , Identifica se o usu�rio ser� questionado quanto a execu��o de schedules pendentes.
@param 06 - aQryCompl , array   , retorna por refer�ncia array com queryes complementares para execu��o InTTS
@param 07 - lReteste  , l�gico  , indica se est� sendo feito o RE-TESTE (chamada recursiva)
@return lReturn, l�gico, indica se a trigger relacionada a API est� configurada corretamente
/*/
Function MRPVldTrig(lExibeHelp, cCodAPI, lInstala, oModel, lExecJob, aQryCompl, lReteste)

	Local aAPI      := {}
	Local aExecJob  := {}
	Local cAlias    := ""
	Local cAPI      := ""
	Local cFullError:= ""
	Local cError    := ""
	Local cMessage  := ""
	Local cMsgparc  := ""
	Local cQuery    := ""
	Local cMsgDesTr := ""
	Local lRetestar := .F.
	Local lReturn   := .T.
	Local lIntegra  := IntNewMRP("MRPDEMANDS")
	Local lOnline   := .F.
	Local lUpdAlt1  := .F.
	Local lUpdLocal := aQryCompl == Nil
	Local nIndex    := 0
	Local oDescri   := Nil
	Local oTrigger
	Local oPosMdlGrd
	Local oMdlGrid  := IIF(oModel != Nil, oModel:GetModel("T4PDETAIL"), Nil)
	Local oMdlHwl   := IIF(oModel != Nil, oModel:GetModel("HWLMASTER"), Nil)

	Default lExibeHelp := .T.
	Default lInstala   := .F.
	Default lExecJob   := .F.
	Default lReteste   := .F.

	If lUpdLocal
		aQryCompl := {}
	EndIf

	If oMdlGrid == Nil
		//Se a integra��o estiver ligada, faz a verifica��o.
		cAlias := GetNextAlias()
		cQuery := " SELECT DISTINCT T4P.T4P_API "
		cQuery +=   " FROM " + RetSqlName("T4P") + " T4P "
		cQuery +=  " WHERE T4P.T4P_FILIAL = '" + xFilial("T4P") + "' "
		cQuery +=    " AND T4P.D_E_L_E_T_ = ' ' "

		If !Empty(cCodAPI)
			cQuery += " AND T4P.T4P_API = '" + cCodAPI + "' "
		ElseIf !_lPCPMADI
			cQuery += " AND T4P.T4P_API NOT IN ('MRPTRANSPORTINGLANES','MRPWAREHOUSEGROUP') "
		EndIf

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)
		While (cAlias)->(!Eof())
			aAdd(aAPI, (cAlias)->(T4P_API))
			(cAlias)->(dbSkip())
		End
		(cAlias)->(dbCloseArea())

		lIntegra := IntNewMRP(cCodAPI, @lOnline)
	Else
		oPosMdlGrd := JsonObject():New()
		For nIndex := 1 to oMdlGrid:Length(.F.)
			oPosMdlGrd[AllTrim(oMdlGrid:GetValue("T4P_API", nIndex))] := nIndex

			If (Empty(cCodAPI) .And. (_lPCPMADI .Or. (!_lPCPMADI               .And. ;
			    oMdlGrid:GetValue("T4P_API", nIndex) != "MRPTRANSPORTINGLANES" .And. ;
				oMdlGrid:GetValue("T4P_API", nIndex) != "MRPWAREHOUSEGROUP"))) .OR.  ;
				AllTrim(oMdlGrid:GetValue("T4P_API", nIndex)) == AllTrim(cCodAPI)
				aAdd(aAPI, oMdlGrid:GetValue("T4P_API", nIndex))
			EndIf
		Next
		aAdd(aAPI, "MRPPRODUCTINDICATOR")
		oPosMdlGrd["MRPPRODUCTINDICATOR"] := oPosMdlGrd["MRPPRODUCT"]

		If FWAliasInDic( "HW9", .F. )
			aAdd(aAPI, "MRPBOMROUTING")
			oPosMdlGrd["MRPBOMROUTING"] := oPosMdlGrd["MRPBILLOFMATERIAL"]
		EndIf

		lIntegra := oMdlGrid:GetValue("T4P_ATIVO", 1) == "1"
	EndIf

	If lExibeHelp
		oDescri := P139AllAPI()
	EndIf

	oTrigger := MRPTrigger():New()

	//Verifica se existem JOBS pendentes de processar
	//para as APIs que est�o sendo desativadas ou alteradas para ONLINE.
	If lExecJob
		For nIndex := 1 To Len(aAPI)
			cAPI := AllTrim(aAPI[nIndex])

			If oMdlGrid == Nil
				lIntegra := IntNewMRP(cAPI, @lOnline)
			Else
				lIntegra := oMdlGrid:GetValue("T4P_ATIVO", 1) == "1"
				lOnline  := oMdlGrid:GetValue("T4P_TPEXEC", oPosMdlGrd[cAPI]) == "1"
			EndIf

			//Se a integra��o est� ativada, verifica se existem pend�ncias na tabela T4R
			If lIntegra .And. (oMdlGrid == NIL .Or. (oMdlGrid <> NIL .And. lOnline ))
				If PendSchedu(cAPI)
					aAdd(aExecJob, cAPI)
				EndIf
			EndIf
		Next nIndex

		If Len(aExecJob) > 0
			If lExibeHelp
				/*
					N�o retirar as chamadas do Space(50) na montagem do cMessage.
					Foi feito para que a janela calcule o tamanho corretamente
					quando existe quebras de linha.
				*/
				cMessage := STR0029 + CRLF + Space(50) + CRLF //"Existem pend�ncias na fila de processamento da integra��o do MRP para as seguintes APIs:"
				For nIndex := 1 To Len(aExecJob)
					cMessage += AllTrim(aExecJob[nIndex]) + " - " + oDescri[AllTrim(aExecJob[nIndex])] + CRLF
				Next nIndex

				If oMdlGrid == Nil
					cMessage += Space(50) + CRLF + "<b>" + STR0033 + "</b>" //Deseja executar a integra��o agora?
				Else
					cMessage += Space(50) + CRLF + STR0030 + CRLF //"O Tipo de execu��o da API somente pode ser alterado de Schedule para Online quando n�o existem pend�ncias de processamento."
					cMessage += Space(50) + CRLF + "<b>" + STR0031 + "</b>" //Deseja executar a integra��o e alterar o tipo para Online?
				EndIf

				If lReteste
					Help(' ', 1,"Help", , STR0037 + ;    //"N�o foi poss�vel eliminar todas as pend�ncias."
					        CRLF + CRLF + STR0038, 1, 0) //"Esse processo n�o ser� interrompido."
				Else
					If SupergetMV("MV_MRPSINC", .F., 1) <> 1 .Or. MsgYesNo(cMessage, STR0032) //"Aten��o!"
						lRetestar := .T.
					Else
						lReturn   := .F.
					EndIf
				EndIf
				cMessage := ""
			Else
				lReturn  := .F.
			EndIf
		EndIf
	EndIf

	/*TRIGGERS COM DUAS TABELAS PRECISAM DESSE TRATAMENTO PARA CRIAREM A TRIGGER EM AMBAS AS TABELAS*/
	aadd(aApi, "MRPWAREHOUSEGROUP#CHILD")
	aadd(aApi, "MRPTRANSPORTINGLANES#CHILD")
	aadd(aApi, "MRPPRODUCT#CHILD")

	If oMdlHwl != Nil
		oTrigger:lNetChange := oModel:GetModel("HWLMASTER"):GetValue("HWL_NETCH") == "1"
	EndIf

	If lReturn
		For nIndex := 1 To Len(aAPI)

			cAPI := AllTrim(aAPI[nIndex])
			/*caso possua o posFixo #child, retira o mesmo para n�o influenciar em outros
			trechos de c�digo, o #child � importante apenas apara o m�todo MRPIniTrig*/
			If At("#CHILD", cApi) > 0
				cApi := StrTran(cApi, "#CHILD", "" )
			EndIf

			If oMdlGrid == Nil
				lIntegra := IntNewMRP(cAPI, @lOnline)
			Else
				lIntegra := oMdlGrid:GetValue("T4P_ATIVO", 1) == "1"
				lOnline  := oMdlGrid:GetValue("T4P_TPEXEC", oPosMdlGrd[cAPI]) == "1"
			EndIf

			MRPIniTrig(AllTrim(aAPI[nIndex]), oTrigger)

			//API Schedule e trigger desabilitada/desinstalada
			If lIntegra .AND. !lOnline .AND. (!oTrigger:isTriggerInstalled() .OR. !oTrigger:isTriggerUpdated())

				If lExibeHelp
					cMessage += CRLF
					cMsgparc += CRLF
					If oDescri[AllTrim(aAPI[nIndex])] == Nil
						cMsgDesTr := AllTrim(aAPI[nIndex])
					Else
						cMsgDesTr := oDescri[AllTrim(aAPI[nIndex])]
					EndIf

					If lInstala
						cMessage += " - " + cMsgDesTr + ", " + STR0019 //trigger instalada no banco de dados.
						cMsgparc += " - " + cMsgDesTr
					Else
						cMessage += " - " + cMsgDesTr + ", " + STR0036 //trigger desatualizada.
						cMsgparc += " - " + cMsgDesTr
					EndIf
				EndIf

				If lInstala
					If !oTrigger:installTrigger()   //Instala/atualiza a trigger.
						cError     += CRLF + CRLF + " - " + STR0021 + " '" + AllTrim(cAPI) + "'" //Erro na instala��o da trigger
						cFullError += CRLF + CRLF + " - " + STR0021 + " '" + AllTrim(cAPI) + "': " + AllTrim(oTrigger:getError()) //Erro na instala��o da trigger
						lReturn := .F.
					Else
						aAdd(aQryCompl, "UPDATE " + RetSqlName("T4P") + " SET T4P_ALTER = '1' WHERE D_E_L_E_T_ = ' ' AND T4P_API = '" + cAPI + "'")
					EndIf
				Else
					lReturn := .F.
				EndIf

			//API Online e trigger existente
			ElseIf (!lIntegra .OR. lOnline) .AND. oTrigger:isTriggerInstalled()

				If lExibeHelp
					cMessage += CRLF
					cMsgparc += CRLF
					If oDescri[AllTrim(aAPI[nIndex])] == Nil
						cMsgDesTr := AllTrim(aAPI[nIndex])
					Else
						cMsgDesTr := oDescri[AllTrim(aAPI[nIndex])]
					EndIf

					cMessage += " - " + cMsgDesTr + ", " + STR0020 //trigger desinstalada no banco de dados.
					cMsgparc += " - " + cMsgDesTr
				EndIf

				If lInstala
					If !oTrigger:uninstallTrigger()   //Desinstala a trigger.
						cError     += CRLF + CRLF + " - " + STR0022 + " '" + AllTrim(cAPI) + "'"//Erro na desinstala��o da trigger
						cFullError += CRLF + CRLF + " - " + STR0021 + " '" + AllTrim(cAPI) + "': " + AllTrim(oTrigger:getError()) //Erro na instala��o da trigger
						lReturn := .F.
					EndIf
				EndIf
			EndIf

			If oMdlGrid == Nil
				If !lIntegra
					aAdd(aQryCompl, "UPDATE " + RetSqlName("T4P") + " SET T4P_ALTER = ' ' WHERE D_E_L_E_T_ = ' ' AND T4P_API = '" + cAPI + "'")
				ElseIf !lUpdAlt1
					lUpdAlt1 := .T.
					aAdd(aQryCompl, "UPDATE " + RetSqlName("T4P") + " SET T4P_ALTER = '1' WHERE D_E_L_E_T_ = ' ' AND T4P_ALTER = ' '")
				EndIf
			EndIf
		Next nIndex

		If lExibeHelp .AND. !lReturn
			If !lInstala
				cMessage := STR0023 +  CRLF + cMsgparc //"Foram identificadas falhas na configura��o da integra��o com o MRP das API's listadas abaixo. Acesse a rotina de Configura��o do MRP (PCPA139) para executar a corre��o autom�tica dos problemas." +  CRLF + cMessage
				Aviso(STR0008, cMessage, {STR0009}, 2) //"Aten��o" //"OK"
			Else
				If Empty(cError)
					cMessage := STR0024 +  CRLF + cMessage //"Foram identificadas e corrigidas falhas na configura��o da integra��o com o MRP das API's listadas abaixo:"
					Aviso(STR0008, cMessage, {STR0009}, 3) //"Aten��o" //"OK"
				Else
					cMessage := AllTrim(STR0025) + AllTrim(cError) //"Foram identificadas falhas na configura��o da integra��o com o MRP das API's e ocorreram erros que impediram a conclus�o do processo de atualiza��o para as API's: "
					LogMsg('PCPIntMRP', 0, 0, 1, '', '', Replicate("-",70) + CHR(10) + AllTrim(STR0025 + CRLF + cFullError) + CHR(10) + Replicate("-",70))
					Help(' ', 1,"Help" ,,cMessage + CRLF + CRLF + AllTrim(STR0027), 1, 1, , , , , , {STR0028}) //"Consulte 'console.log' para visualizar o erro completo." + "Entre em contato com o suporte."

				EndIf
			EndIf
		EndIf

		If lReturn .And. lExecJob .And. Len(aExecJob) > 0
			FWMsgRun(, {|| prcPendenc(aExecJob) }, STR0034, STR0035) //"Aguarde" ### "Processando pend�ncias..."
		EndIf

		If lReturn .and. lUpdLocal
			For nIndex := 1 to Len(aQryCompl)
				TcSqlExec(aQryCompl[nIndex])
			Next
		EndIf
	EndIf

	If lRetestar
		MRPVldTrig(lExibeHelp, cCodAPI, lInstala, oMdlGrid, lExecJob, aQryCompl, .T.)
	EndIf

	FreeObj(oDescri)
	oDescri := Nil

	//Destruir o objeto.
	oTrigger:Destroy()
	FreeObj(oTrigger)

Return lReturn

/*/{Protheus.doc} MRPIniTrig
Inicia o objeto MRPTrigger com as configura��es de cada tabela

@type  Function
@author lucas.franca
@since 14/08/2019
@version P12.1.28
@param 01 - cCodAPI , caracter, c�digo da API para configurar o objeto oTrigger.
@param 02 - oTrigger, Object  , Inst�ncia do objeto MRPTrigger para reutiliza��o. Se n�o enviado, ser� criada uma nova inst�ncia.
@return oTrigger, Object, Inst�ncia do objeto MRPTrigger configurado.
/*/
Function MRPIniTrig(cCodAPI, oTrigger)

	If oTrigger == Nil
		oTrigger := MRPTrigger():New()
	EndIf

	oTrigger:clear()  //Limpar os dados para verificar a trigger de outra tabela.

	Do Case
		Case cCodAPI == "MRPDEMANDS"
			oTrigger:configureTable("SVR","MRPDEMANDS",;
									{"VR_FILIAL","VR_CODIGO","VR_SEQUEN","VR_PROD","VR_DATA","VR_TIPO","VR_DOC","VR_QUANT","VR_LOCAL","VR_MOPC","VR_OPC","R_E_C_N_O_"},;
									{"VR_FILIAL","VR_CODIGO","VR_SEQUEN"},.T.,,,,,{"VR_FILIAL","VR_PROD"},{})

		Case cCodAPI == "MRPPRODUCTIONVERSION"
			oTrigger:configureTable("SVC","MRPPRODUCTIONVERSION",;
									{"VC_FILIAL","VC_VERSAO","VC_PRODUTO","VC_DTINI","VC_DTFIM","VC_QTDDE","VC_QTDATE","VC_REV", "VC_ROTEIRO", "VC_LOCCONS"},;
									{"VC_FILIAL","VC_VERSAO","VC_PRODUTO"},.T.,,,,,{"VC_FILIAL","VC_PRODUTO"},{})

		Case cCodAPI == "MRPBILLOFMATERIAL"
			oTrigger:configureTable("SG1","MRPBILLOFMATERIAL",{},;
									{"G1_FILIAL","G1_COD"},.F.,,,,,{"G1_FILIAL","G1_COD"},{})

		Case cCodAPI == "MRPALLOCATIONS"
			oTrigger:configureTable("SD4","MRPALLOCATIONS", {},;
									{"D4_FILIAL","D4_COD","D4_TRT","D4_LOCAL","D4_OP","D4_OPORIG","D4_DATA"},.F.,,,,,{"D4_FILIAL","D4_COD"},{})

		Case cCodAPI == "MRPPRODUCTIONORDERS"
			oTrigger:configureTable("SC2","MRPPRODUCTIONORDERS",;
									{"C2_FILIAL","C2_NUM","C2_ITEM","C2_SEQUEN","C2_SEQPAI","C2_ITEMGRD","C2_PRODUTO","C2_LOCAL","C2_QUANT",;
									 "C2_QUJE","C2_PERDA","C2_DATPRI","C2_DATPRF","C2_DATRF","C2_MOPC","C2_OPC","R_E_C_N_O_"},;
									{"C2_FILIAL","C2_NUM","C2_ITEM","C2_SEQUEN","C2_ITEMGRD"},.T.,,,,,{"C2_FILIAL","C2_PRODUTO"},{})

		Case cCodAPI == "MRPPURCHASEORDER"
			oTrigger:configureTable("SC1","MRPPURCHASEORDER",;
									{"C1_FILIAL","C1_NUM","C1_ITEM","C1_ITEMGRD","C1_PRODUTO","C1_OP","C1_DATPRF","C1_QUANT","C1_QUJE","C1_LOCAL","C1_TPOP","C1_RESIDUO"},;
									{"C1_FILIAL","C1_NUM","C1_ITEM","C1_ITEMGRD"},.T.,,,,,{"C1_FILIAL","C1_PRODUTO"},{})

		Case cCodAPI == "MRPPURCHASEREQUEST"
			oTrigger:configureTable("SC7","MRPPURCHASEREQUEST",;
									{"C7_FILIAL","C7_NUM","C7_ITEM","C7_ITEMGRD","C7_PRODUTO","C7_OP","C7_DATPRF","C7_QUANT","C7_QUJE","C7_LOCAL","C7_TPOP","C7_RESIDUO"},;
									{"C7_FILIAL","C7_NUM","C7_ITEM","C7_ITEMGRD"},.T.,,,,,{"C7_FILIAL","C7_PRODUTO"},{})

		Case cCodAPI == "MRPSTOCKBALANCE"
			oTrigger:configureTable("SB2","MRPSTOCKBALANCE", {},;
									{"B2_FILIAL","B2_COD","B2_LOCAL"},.F.,,,,,{"B2_FILIAL","B2_COD"},{})

		Case cCodAPI == "MRPREJECTEDINVENTORY"
			oTrigger:configureTable("SD7","MRPREJECTEDINVENTORY", {},;
									{"D7_FILIAL","D7_PRODUTO","D7_LOCDEST","D7_DATA"},.F.,,,,,{"D7_FILIAL","D7_PRODUTO"},{})

		Case cCodAPI == "MRPCALENDAR"
			oTrigger:configureTable("SVZ","MRPCALENDAR",;
									{"VZ_FILIAL","VZ_CALEND","VZ_DATA","VZ_HORAINI","VZ_HORAFIM","VZ_INTERVA"},;
									{"VZ_FILIAL","VZ_CALEND","VZ_DATA"},.T.)

		Case cCodAPI == "MRPWAREHOUSEGROUP"
			oTrigger:configureTable("SVN","MRPWAREHOUSEGROUP",{},{"VN_FILIAL","VN_LOCGRP"},.F.)

		Case cCodAPI == "MRPWAREHOUSEGROUP#CHILD"
			oTrigger:configureTable("SVO","MRPWAREHOUSEGROUP",{},{"VO_FILIAL","VO_LOCGRP"},.F.,/*cTabelaPai*/,/*aFieldsPai*/, /*aRelPai*/, .F.)

		Case cCodAPI == "MRPTRANSPORTINGLANES"
			oTrigger:configureTable("SVD","MRPTRANSPORTINGLANES",{},{"VD_FILIAL","R_E_C_N_O_"},.F.)

		Case cCodAPI == "MRPTRANSPORTINGLANES#CHILD"
			oTrigger:configureTable("SVQ","MRPTRANSPORTINGLANES",{},{},.F.,"SVD",{"VD_FILIAL","R_E_C_N_O_"},;
			{{"VD_FILIAL","VQ_FILIAL"},{"VD_ORIGEM","VQ_ORIGEM"},{"VD_DESTINO","VQ_DESTINO"}}, .F.)

		Case cCodApi == "MRPPRODUCT"
			oTrigger:configureTable("SB1","MRPPRODUCT",{},{"B1_FILIAL","B1_COD"},.F.,,,,,{"B1_FILIAL","B1_FILIAL"},,{})

		Case cCodAPI == "MRPPRODUCT#CHILD"
			oTrigger:configureTable("SVK","MRPPRODUCT",{},{"VK_FILIAL","VK_COD"},.F.,/*cTabelaPai*/,/*aFieldsPai*/, /*aRelPai*/, .F.,,,,,{"VK_FILIAL","VK_COD"},,{})

		Case cCodApi == "MRPPRODUCTINDICATOR"
			oTrigger:configureTable("SBZ","MRPPRODUCTINDICATOR",{},{"BZ_FILIAL","BZ_COD"},.F.,,,,,{"BZ_FILIAL","BZ_COD"},,{})

		Case cCodApi == "MRPBOMROUTING"
			oTrigger:configureTable("SGF","MRPBOMROUTING", {}, {"GF_FILIAL","GF_PRODUTO","GF_ROTEIRO","GF_OPERAC"},.F.,,,,,{"GF_FILIAL","GF_PRODUTO","GF_ROTEIRO","GF_OPERAC"},,{})

		Case cCodApi == "MRPWAREHOUSE"
			oTrigger:configureTable("NNR","MRPWAREHOUSE",;
									{"NNR_FILIAL","NNR_CODIGO","NNR_TIPO","NNR_MRP"},;
									{"NNR_FILIAL","NNR_CODIGO"},.T.)
	EndCase

Return oTrigger

/*/{Protheus.doc} PendSchedu
Verifica se existe alguma pend�ncia de processamento
por schedule para uma API espec�fica na tabela T4R

@type  Static Function
@author lucas.franca
@since 14/08/2019
@version P12.1.28
@param cCodApi, Character, C�digo da API para verifica��o
@return lExiste, Logic, Identifica se existe registro pendente de processamento na tabela T4R para esta API
/*/
Static Function PendSchedu(cCodApi)
	Local cAliasQry := GetNextAlias()
	Local lExiste   := .F.

	BeginSql Alias cAliasQry
		SELECT 1
		  FROM %table:T4R% T4R
		 WHERE T4R.T4R_FILIAL = %xfilial:T4R%
		   AND T4R.T4R_API    = %Exp:cCodAPI%
		   AND T4R.T4R_STATUS = '3'
	EndSql

	If (cAliasQry)->(!Eof())
		lExiste := .T.
	EndIf
	(cAliasQry)->(dbCloseArea())

Return lExiste

/*/{Protheus.doc} prcPendenc
Executa as pend�ncias das APIs

@type  Static Function
@author lucas.franca
@since 14/08/2019
@version P12.1.28
@param aExecJob, Array , Array com as APIs que devem ser processadas
@return Nil
/*/
Static Function prcPendenc(aExecJob)
	Local nIndex := 0

	For nIndex := 1 To Len(aExecJob)
		PCPA141RUN(aExecJob[nIndex])
	Next nIndex
Return Nil

/*/{Protheus.doc} updDemands
Atualiza a tabela de demandas do ERP com o n�mero da execu��o do MRP

@type Function
@author renan.roeder
@since 06/03/2020
@version P12.1.28
@param cNrMrp, Caracter , String com o n�mero da execu��o do MRP
@return Nil
/*/
Function updDemands(cNrMrp)
	Local aDemands := {}
	Local aFiltro  := {}
	Local aResult  := {}
	Local cErro    := ""
	Local cUpdate  := ""
	Local cWhere   := ""
	Local lRet     := .T.
	Local lNext    := .T.
	Local nPage    := 1
	Local nSize    := 1000
	Local nX       := 0
	Local nTamCod  := GetSx3Cache("VR_CODIGO","X3_TAMANHO")
	Local nTamFil  := GetSx3Cache("VR_FILIAL","X3_TAMANHO")
	Local nTamSeq  := GetSx3Cache("VR_SEQUEN","X3_TAMANHO")
	Local oJsonDem := JsonObject():New()

	aAdd(aFiltro, {"ticket", cNrMrp})

	While lNext
		aResult := MrpDemGAll(aFiltro,,nPage,nSize)
		If !aResult[1]
			lNext := .F.
			If aResult[3] == 404
				lRet := .T.
			Else
				lRet := .F.
			EndIf
		Else
			cErro   := oJsonDem:FromJson(aResult[2])
			If Empty(cErro)
				aDemands := oJsonDem["items"]
				For nX := 1 To Len(aDemands)
					cWhere := "VR_FILIAL = '"+SubStr(aDemands[nX]["code"],1,nTamFil)+"' AND VR_CODIGO = '"+SubStr(aDemands[nX]["code"],nTamFil+1,nTamCod)+"' AND VR_SEQUEN = "+SubStr(aDemands[nX]["code"],nTamFil+nTamCod+1,nTamSeq)+" "
					cUpdate := "UPDATE " +RetSqlName("SVR")+ " SET VR_NRMRP = '"+cNrMrp+"' WHERE " + cWhere
					If TcSqlExec(cUpdate) < 0
						lRet  := .F.
						lNext := .F.
						Exit
					EndIf
				Next nX
				If !oJsonDem["hasNext"]
					lNext := .F.
				Else
					If lRet
						nPage++
					EndIf
				EndIf
			Else
				lRet  := .F.
				lNext := .F.
			EndIf
		EndIf
	EndDo

Return lRet

/*/{Protheus.doc} VldTblComp
Valida se o compartilhamento das tabelas do erp est� igual as tabelas do mrp.

@type Function
@author renan.roeder
@since 17/07/2020
@version P12.1.31
@return lRet, Logical, Retorna .T. se as tabelas est�o compativeis.
/*/
Function VldTblComp()
	Local aTabelas   := {}
	Local aIncons    := {}
	Local cMessagePr := ""
	Local cMessageSo := ""
	Local cModEmpErp := ""
	Local cModUniErp := ""
	Local cModFilErp := ""
	Local cModEmpMrp := ""
	Local cModUniMrp := ""
	Local cModFilMrp := ""	
	Local lRet       := .T.
	Local nX         := 0
	Local nLenIncons := 0

	aTabelas := { {"SVX","HW0"},;
				  {"SB1","HWA"},;
				  {"SVB","T4J"},;
				  {"SD4","T4S"},;
				  {"SG1","T4N"},;
				  {"SC2","T4Q"},;
				  {"SVC","T4M"},;
				  {"SC1","T4T"},;
				  {"SC7","T4U"},;
				  {"SB2","T4V"},;
				  {"SGI","T4O"},;
				  {"SBZ","HWE"},;
				  {"SVD","HW4"},;
				  {"SVQ","HW5"},;
				  {"SVN","HW6"},;
				  {"SGF","HW9"}	}

	For nX := 1 To Len(aTabelas)
		
		If ! AliasInDic(aTabelas[nX][2])
			Loop
		EndIf

		cModEmpErp := FWModeAccess(aTabelas[nx,1],1)
		cModUniErp := FWModeAccess(aTabelas[nx,1],2)
		cModFilErp := FWModeAccess(aTabelas[nx,1],3)

		cModEmpMrp := FWModeAccess(aTabelas[nx,2],1)
		cModUniMrp := FWModeAccess(aTabelas[nx,2],2)
		cModFilMrp := FWModeAccess(aTabelas[nx,2],3)

		If cModEmpErp != cModEmpMrp .Or. cModUniErp != cModUniMrp .Or. cModFilErp != cModFilMrp
			aAdd(aIncons,{{aTabelas[nx,1],cModEmpErp,cModUniErp,cModFilErp},{aTabelas[nx,2],cModEmpMrp,cModUniMrp,cModFilMrp}})
		EndIf

	Next nX

	nLenIncons := Len(aIncons)
	If nLenIncons > 0
		lRet := .F.

		cMessagePr := STR0040        //"Foram identificadas inconsist�ncias em rela��o ao modo de compartilhamento das tabelas do ERP com as tabelas do MRP. "
		cMessageSo := STR0041 + CRLF //"As seguintes tabelas devem ter seu modo de compartilhamento ajustado: "

		For nX := 1 To nLenIncons
			cMessageSo += CRLF
			cMessageSo += aIncons[nX][1][1] + " - " + aIncons[nX][2][1] + " - " + AllTrim(FWX2Nome(aIncons[nX][1][1]))
		Next nIndex

		Help(' ', 1,"Help" ,,cMessagePr, 1, 1, , , , , , {cMessageSo})
	EndIf

Return lRet

/*/{Protheus.doc} VldCampTam
Valida se o tamanho dos campos numericos das tabelas do ERP est�o compat�veis com os
campos correspondentes das tabelas do MRP.

@type Function
@author renan.roeder
@since 02/08/2020
@version P12.1.31
@return Array, Array, [1] Mensagem de inconsist�ncias [2] Quantidade de Campos validados com sucesso [3] Quantidade de Campos com diferen�a no tamanho.
/*/
Function VldCampTam()
	Local aCampos    := {}
	Local aInfERP    := {}
	Local aInfMRP    := {}
	Local aIncons    := {}
	Local aInfC1     := {}
	Local aInfC2     := {}
	Local cAviso     := ""
	Local cValid     := ""
	Local nIndCampos := 0
	Local nIndCmpTab := 0
	Local nIndIncons := 0
	Local nSuc       := 0
	Local nErr       := 0

	aCampos := ArrValMRP()

	//OPENSXS()
	For nIndCampos := 1 To Len(aCampos)
		For nIndCmpTab := 1 To Len(aCampos[nIndCampos][ERP][CAMPOS])
			aInfERP   := TAMSX3(aCampos[nIndCampos][ERP][CAMPOS][nIndCmpTab])
			aInfMRP   := TAMSX3(aCampos[nIndCampos][MRP][CAMPOS][nIndCmpTab])
			cValid := "aInfERP["+STR(TAMANHO)+"] "+aCampos[nIndCampos][SINAL]+" aInfMRP["+STR(TAMANHO)+"] .And. aInfERP["+STR(DECIMAL)+"] "+aCampos[nIndCampos][SINAL]+" aInfMRP["+STR(DECIMAL)+"]
			If (Len(aInfERP) > 0 .And. Len(aInfMRP) > 0) .And. !(&cValid)
				aAdd(aIncons,{aCampos[nIndCampos][MRP][TABELA],aCampos[nIndCampos][MRP][CAMPOS][nIndCmpTab],aInfERP[TAMANHO],aInfERP[DECIMAL]})
				nErr++
			Else
				nSuc++
			EndIf
		Next nIndCmpTab
	Next nIndCampos

	aInfC1   := TAMSX3("C1_QUANT")
	aInfC2   := TAMSX3("C2_QUANT")
	aInfMRP  := TAMSX3("HWC_QTNECE")
	If (aInfC1[TAMANHO] > aInfMRP[TAMANHO] .Or. aInfC1[DECIMAL] > aInfMRP[DECIMAL]) .Or. (aInfC2[TAMANHO] > aInfMRP[TAMANHO] .Or. aInfC2[DECIMAL] > aInfMRP[DECIMAL])
		If aInfC1[TAMANHO] > aInfC2[TAMANHO]
			aInfERP := aInfC1
		ElseIf aInfC1[TAMANHO] == aInfC2[TAMANHO]
			If aInfC1[DECIMAL] > aInfC2[DECIMAL]
				aInfERP := aInfC1
			Else
				aInfERP := aInfC2
			EndIf
		Else
			aInfERP := aInfC2
		EndIf
		aAdd(aIncons,{"HWC","HWC_QTNECE",aInfERP[TAMANHO],aInfERP[DECIMAL]})
	EndIf

	If Len(aIncons)
		For nIndIncons := 1 To Len(aIncons)
			cAviso += CHR(10)
			//STR0042 "Campo " STR0043 " da tabela " STR0044 " deve ter tamanho "
			cAviso += STR0042 + aIncons[nIndIncons][2] + STR0043 + aIncons[nIndIncons][1] + STR0044 + AllTrim(STR(aIncons[nIndIncons][3])) + IIF(aIncons[nIndIncons][4] > 0, "," + AllTrim(STR(aIncons[nIndIncons][4])),"")
		Next nIndIncons
	EndIf

Return {cAviso,nSuc,nErr}

/*/{Protheus.doc} ArrValMRP
Monta o array com os campos a serem validados para a integra��o com o MRP. 
@type Static Function
@author renan.roeder
@since 02/08/2020
@version P12.1.31
@return aCampos, Array, Array com os campos as serem validados.
/*/
Static Function ArrValMRP()
	Local aCampos   := {}
	
	aCampos := {  {{"SB1",{"B1_EMAX","B1_EMIN","B1_ESTSEG","B1_LE","B1_LM","B1_LOTESBP","B1_PE","B1_QE","B1_TOLER"}},{"HWA",{"HWA_EMAX","HWA_EMIN","HWA_ESTSEG","HWA_LE","HWA_LM","HWA_LOTSBP","HWA_PE","HWA_QE","HWA_TOLER"}},"=="},;
				  {{"SVK",{"VK_HORFIX"}}                                                                            ,{"HWA",{"HWA_HORFIX"}},"=="},;
				  {{"SBZ",{"BZ_EMAX","BZ_EMIN","BZ_ESTSEG","BZ_LE","BZ_LM","BZ_HORFIX","BZ_PE","BZ_QE","BZ_TOLER"}} ,{"HWE",{"HWE_EMAX","HWE_EMIN","HWE_ESTSEG","HWE_LE","HWE_LM","HWE_HORFIX","HWE_PE","HWE_QE","HWE_TOLER"}},"=="},;
				  {{"SB2",{"B2_QATU"}}                                                                              ,{"HWC",{"HWC_QTBXES"}},"=="},;
				  {{"SVJ",{"VJ_QUANT"}}                                                                             ,{"HWC",{"HWC_QTSUBS"}},"=="},;
				  {{"SD4",{"D4_QUANT"}}                                                                             ,{"HWC",{"HWC_QTEMPE"}},"=="},;
				  {{"SB2",{"B2_LOCAL"}}                                                                             ,{"HWC",{"HWC_LOCAL"}},"<="},;
				  {{"SD4",{"D4_QUANT"}}                                                                             ,{"HWG",{"HWG_QTEMPE"}},"<="},;
				  {{"SD7",{"D7_QTDE"}}                                                                              ,{"HWX",{"HWX_QTDE"}},"=="},;
				  {{"SD2",{"D2_QUANT"}}                                                                             ,{"HWX",{"HWX_QTDEV"}},"=="},;
				  {{"SB2",{"B2_LOCAL"}}                                                                             ,{"HWX",{"HWX_LOCAL"}},"<="},;
				  {{"SVQ",{"VQ_QNTINI","VQ_QNTFIM"}}                                                                ,{"HW5",{"HW5_QTDINI","HW5_QTDFIN"}},"=="},;
				  {{"SB2",{"B2_LOCAL"}}                                                                             ,{"HW6",{"HW6_LOCAL"}},"<="},;
				  {{"SVR",{"VR_QUANT"}}                                                                             ,{"T4J",{"T4J_QUANT"}},"=="},;
				  {{"SB2",{"B2_LOCAL"}}                                                                             ,{"T4J",{"T4J_LOCAL"}},"<="},;
				  {{"SVC",{"VC_QTDDE","VC_QTDATE"}}                                                                 ,{"T4M",{"T4M_QNTDE","T4M_QNTATE"}},"=="},;
				  {{"SG1",{"G1_QUANT","G1_PERDA","G1_POTENCI"}}                                                     ,{"T4N",{"T4N_QTD","T4N_PERDA","T4N_POTEN"}},"=="},; 
				  {{"SB1",{"B1_QB"}}                                                                                ,{"T4N",{"T4N_QTDB"}},"=="},;
				  {{"SGI",{"GI_FATOR"}}                                                                             ,{"T4O",{"T4O_FATCON"}},"=="},;
				  {{"SC2",{"C2_QUANT","C2_QUJE"}}                                                                   ,{"T4Q",{"T4Q_QUANT","T4Q_SALDO"}},"=="},;
				  {{"SB2",{"B2_LOCAL"}}                                                                             ,{"T4Q",{"T4Q_LOCAL"}},"<="},;
				  {{"SD4",{"D4_QUANT","D4_QSUSP"}}                                                                  ,{"T4S",{"T4S_QTD","T4S_QSUSP"}},"=="},;
				  {{"SB2",{"B2_LOCAL"}}                                                                             ,{"T4S",{"T4S_LOCAL"}},"<="},;
				  {{"SC1",{"C1_QUANT","C1_QUJE"}}                                                                   ,{"T4T",{"T4T_QTD","T4T_QUJE"}},"=="},;
				  {{"SB2",{"B2_LOCAL"}}                                                                             ,{"T4T",{"T4T_LOCAL"}},"<="},;
				  {{"SC7",{"C7_QUANT","C7_QUJE"}}                                                                   ,{"T4U",{"T4U_QTD","T4U_QUJE"}},"=="},;
				  {{"SB2",{"B2_LOCAL"}}                                                                             ,{"T4U",{"T4U_LOCAL"}},"<="},;
				  {{"SB2",{"B2_QATU","B2_QNPT","B2_QTNP","B2_QATU"}}                                                ,{"T4V",{"T4V_QTD","T4V_QNPT","T4V_QTNP","T4V_QTIND"}},"=="},; 
				  {{"SB8",{"B8_LOTECTL","B8_NUMLOTE"}}                                                              ,{"T4V",{"T4V_LOTE","T4V_SLOTE"}},"=="},;
				  {{"SDD",{"DD_SALDO"}}                                                                             ,{"T4V",{"T4V_SLDBQ"}},"=="},;
				  {{"SB2",{"B2_LOCAL"}}                                                                             ,{"T4V",{"T4V_LOCAL"}},"<="};
				}
Return aCampos