#INCLUDE "TOTVS.CH"
#INCLUDE "PCPMultiThreadError.CH"

/*/{Protheus.doc} PCPMultiThreadError
Controla Erros em Processamento Multi-Thread
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
/*/
CLASS PCPMultiThreadError FROM LongClassName

	//Declaracao de propriedades da classe
	DATA cErrorUID          AS String
	DATA nTentativasConexao AS Numeric

	//Metodos publicos
	METHOD new(cErrorUID, lCriaSecao, nTentConec) CONSTRUCTOR
	METHOD executa(cEmpAux, cFilAux, cFunName, oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar08, oVar09, oVar10, nTentativa, bRecover)
	METHOD final(aError)
	METHOD getaError()
	METHOD possuiErro()
	METHOD startJob(cFunName, cEnv, lWait, cEmpAux, cFilAux, oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar08, oVar09, oVar10, bRecover)

	//Metodos internos
	METHOD criarSecaoGlobal(cErrorUID)
	METHOD destroy()
	METHOD preparaAmbiente(cEmpAux, cFilAux, nTentativa)

ENDCLASS

/*/{Protheus.doc} new
Metodo construtor
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@param 01 - cErrorUID , caracter, nome da secao global que sera utilizada no controle de erros multi-thread
@param 02 - lCriaSecao, logico  , indica se deve criar a secao global
@param 03 - nTentConec, numero  , numero maximo de tentativas de conexao com ambiente RPCSETENV
@return Self, objeto, instancia da classe
/*/
METHOD new(cErrorUID, lCriaSecao, nTentConec) CLASS PCPMultiThreadError
	Default nTentConec := 10
	Default lCriaSecao := .F.
	Self:cErrorUID          := cErrorUID
	Self:nTentativasConexao := nTentConec
	If lCriaSecao
		Self:criarSecaoGlobal(cErrorUID)
	EndIf
Return Self

/*/{Protheus.doc} startJob
Chamada Protegida da cFunName atrav�s de StartJOB() + PCPMLTHSTJ
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@param 01 - cFunName    , caracter    , indica o nome da funcao que sera executada pela Thread
@param 02 - cEnv        , caracter    , cEnv enviado para StartJOB()
@param 03 - lWait       , logico      , lWait enviado para StartJOB()
@param 04 - cEmpAux     , caracter    , empresa para abertura de ambiente
@param 05 - cFilAux     , caracter    , filial para abertura de ambiente
@param 06 -> 15 - oVar01, nao definido, 1a -> 10 variavel que sera repassada para a funcao
@param 16 - bRecover    , bloco       , bloco de codigo para execucao em recover
@return Ret, Se lWait for verdadeiro (.T.) o retorno do StartJob ser� o retorno da fun��o a ser executada como um JOB (cName), caso lWait seja falso (.F.) o retorno � nulo (NIL).
/*/
METHOD startJob(cFunName, cEnv, lWait, cEmpAux, cFilAux, oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar08, oVar09, oVar10, bRecover) CLASS PCPMultiThreadError
Return StartJob("PCPMLTHSTJ", cEnv, lWait, Self:cErrorUID, cEmpAux, cFilAux, cFunName, oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar08, oVar09, oVar10, bRecover)

/*/{Protheus.doc} PCPMLTHSTJ
Fun��o Iniciao da Chamada PCPMultiThreadError:startJob()
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@param 01 - cErrorUID   , caracter    , codigo da secao de controle de erros global
@param 02 - cEmpAux     , caracter    , empresa para abertura de ambiente
@param 03 - cFilAux     , caracter    , filial para abertura de ambiente
@param 04 - cFunName    , caracter    , indica o nome da funcao que sera executada pela Thread
@param 05 -> 14 - oVar01, nao definido, 1a -> 10 variavel que sera repassada para a funcao
@param 15 - bRecover    , bloco       , bloco de codigo para execucao em recover
@return Nil
/*/
Function PCPMLTHSTJ(cErrorUID, cEmpAux, cFilAux, cFunName, oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar08, oVar09, oVar10, bRecover)
	PCPMLTHEXE(cErrorUID, .F./*lCriaSecao*/, cEmpAux, cFilAux, cFunName, oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar08, oVar09, oVar10, 1/*nTentativa*/, bRecover)
	LogMsg("PCPMLTHERR", 0, 0, 1, '', '', "[INFO ][SERVER] [Thread " + cValToChar(ThreadID()) + "] Thread finished [-PCPMLTHSTJ " + cErrorUID + " " + cFunName + " - " + Time() + "]")
Return

/*/{Protheus.doc} PCPMLTHEXE
Chamada da Execucao da Classe PCPMultiThreadError em Function
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@param 01 - cErrorUID   , caracter    , codigo da secao de controle de erros global
@param 02 - lCriaSecao  , logico      , indica se deve criar a secao de variaveis globais
@param 03 - cEmpAux     , caracter    , empresa para abertura de ambiente
@param 04 - cFilAux     , caracter    , filial para abertura de ambiente
@param 05 - cFunName    , caracter    , indica o nome da funcao que sera executada pela Thread
@param 06 -> 15 - oVar01, nao definido, 1a -> 10 variavel que sera repassada para a funcao
@param 16 - nTentativa  , numero      , indica a numeracao de tentativas de conexao ao ambiente
@param 17 - bRecover    , bloco       , bloco de codigo para execucao em recover
@return Nil
/*/
Function PCPMLTHEXE(cErrorUID, lCriaSecao, cEmpAux, cFilAux, cFunName, oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar08, oVar09, oVar10, nTentativa, bRecover)
	Local oSelf
	Default lCriaSecao := .F.
	Default nTentativa := 1
	oSelf := PCPMultiThreadError():New(cErrorUID, lCriaSecao)
	SET DATE FRENCH; Set(_SET_EPOCH, 1980)
	oSelf:executa(cEmpAux, cFilAux, cFunName, oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar08, oVar09, oVar10, nTentativa, bRecover)
Return

/*/{Protheus.doc} executa
Executa Function cFunName Protegida com ErrorBlock - Ass�ncrono
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@param 01 - cEmpAux     , caracter    , empresa para abertura de ambiente
@param 02 - cFilAux     , caracter    , filial para abertura de ambiente
@param 03 - cFunName    , caracter    , indica o nome da funcao que sera executada pela Thread
@param 04 -> 13 - oVar01, nao definido, 1a -> 10 variavel que sera repassada para a funcao
@param 14 - nTentativa  , numero      , indica a numeracao de tentativas de conexao ao ambiente
@param 15 - bRecover    , bloco       , bloco de codigo para execucao em recover
@return Nil
/*/
METHOD executa(cEmpAux, cFilAux, cFunName, oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar08, oVar09, oVar10, nTentativa, bRecover) CLASS PCPMultiThreadError

	Local lExecuta := .T.
	Local oBkpError

	Default nTentativa := 1

	If cEmpAux != NIL .AND. cFilAux != NIL .AND. !Empty(cEmpAux + cFilAux)
		lExecuta := Self:preparaAmbiente(cEmpAux, cFilAux, nTentativa)
	EndIf

	If lExecuta
		If !Empty(cFunName)
			nTentativa := Self:nTentativasConexao + 1
			oBkpError  := ErrorBlock( {|oErro| PCPMLTHERR(oErro, Self:cErrorUID, nTentativa, Self:nTentativasConexao)  } )

			BEGIN SEQUENCE
				&cFunName.(oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar09, oVar10)

			RECOVER
				If bRecover != Nil
					Eval(bRecover)
				EndIf

			END SEQUENCE

			ErrorBlock( oBkpError )
		EndIf

	ElseIf nTentativa < Self:nTentativasConexao
		//Inicia nova thread para tentar a preparacao do ambiente novamente
		StartJob("PCPMLTHEXE", getEnvServer(), .F./*lWait*/, Self:cErrorUID, .F./*lCriaSecao*/, cEmpAux, cFilAux, cFunName, oVar01, oVar02, oVar03, oVar04, oVar05, oVar06, oVar07, oVar08, oVar09, oVar10, (nTentativa + 1), bRecover)

	EndIf

Return Nil

/*/{Protheus.doc} PCPMLTHEXE
Fun��o para tratar erros de execu��o do JOB
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@param 01 - oErro     , objeto    , instancia do parametro default de erro da ErrorBLock()
@param 02 - cErrorUID , caracter  , codigo identificador da secao de controle de erros
@param 03 - nTentativa, numero    , indicador da quantidade de tentativas de conexao com o ambiente RPCSETENV
@param 04 - nMaximo   , numero    , indicador da quantidade maxima de tentativas de conexao com o ambiente RPCSETENV
@return Nil
/*/
Function PCPMLTHERR(oErro, cErrorUID, nTentativa, nMaximo)

	Local aError
	Local lRetAux := .F.

	Default nTentativa := nMaximo + 1

	LogMsg("PCPMLTHERR", 0, 0, 1, '', '', "PCPMLTHERR - " + cValToChar(ThreadID()) + " - " + AllTrim(oErro:description) + CHR(10) + AllTrim(oErro:ErrorStack) + CHR(10) + oErro:ErrorEnv)

	If nTentativa > nMaximo
		If !VarIsUID(cErrorUID)
			VarSetUID(cErrorUID)
		EndIf
		VarBeginT(cErrorUID, "aError")
		lRetAux := VarGetA(cErrorUID, "aError", @aError)
		If !lRetAux .OR. aError == Nil
			aError := {}
		EndIf
		aAdd(aError, {"PCPIPCLogE"              ,;
		              AllTrim(oErro:description),;
					  AllTrim(oErro:ErrorStack) ,;
					  oErro:ErrorEnv            })
		VarSetA(cErrorUID, "aError", aError)
		VarSetX(cErrorUID, "lError", .T.)
		VarEndT(cErrorUID, "aError")
	EndIf

	BREAK
Return

/*/{Protheus.doc} preparaAmbiente
Prepara o ambiente (conexao com banco) de acordo com os parametros
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@param 01 - cEmpAux   , caracter, empresa para abertura de ambiente
@param 02 - cFilAux   , caracter, filial para abertura de ambiente
@param 03 - nTentativa, numero  , indicador da quantidade de tentativas de conexao com o ambiente RPCSETENV
@return lReturn, logico, indica se conseguiu conectar com o ambiente
/*/
METHOD preparaAmbiente(cEmpAux, cFilAux, nTentativa) CLASS PCPMultiThreadError

	Local oBkpError  := ErrorBlock( {|oErro| PCPMLTHERR(oErro, Self:cErrorUID, nTentativa, Self:nTentativasConexao)  } )
	Local lReturn    := .F.

	BEGIN SEQUENCE

		If Empty(cEmpAux)
			lReturn := .T.
		Else
			RpcSetType(3)
			lReturn := RpcSetEnv(cEmpAux, cFilAux, Nil, Nil, "PCP", Nil)
			If lReturn .AND. nTentativa > 1
				LogMsg("PCPMLTHERR", 0, 0, 1, '', '', "PCPMLTHERR - " + cValToChar(ThreadID()) + STR0001 + cValToChar(nTentativa) + STR0002 + cValToChar(Self:nTentativasConexao)) //" - Sucesso no preparo do ambiente. Tentativa: " + " de "
			EndIf
		EndIf

	RECOVER
		If !Empty(cEmpAux) .And. !lReturn
			If nTentativa <= Self:nTentativasConexao
				Sleep(1000)
				LogMsg("PCPMLTHERR", 0, 0, 1, '', '', "PCPMLTHERR - " + cValToChar(ThreadID()) + STR0003 + cValToChar(nTentativa+1) + STR0002 + cValToChar(Self:nTentativasConexao)) //" - Falha no preparo do ambiente. Executando nova tentativa. " + " de "
			EndIf
		EndIf

	END SEQUENCE

	ErrorBlock( oBkpError )

Return lReturn

/*/{Protheus.doc} criarSecaoGlobal
Cria a se��o de vari�veis globais que ser� utilizada no processamento
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@param 01 - cErrorUID, caracter, codigo identificaor da secao global
@return Nil
/*/
METHOD criarSecaoGlobal(cErrorUID) CLASS PCPMultiThreadError
	Default cErrorUID := Self:cErrorUID
	If !VarSetUID(Self:cErrorUID)
		LogMsg("PCPMultiThreadError", 0, 0, 1, '', '', "PCPMultiThreadError - " + Self:cErrorUID + STR0004) //" - Erro na cria��o da se��o de vari�veis globais."
	EndIf
Return Nil

/*/{Protheus.doc} possuiErro
Verifica a Ocorrencia de Erros
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@return lError, logico, indica ocorrencia de erro na execucao multithread
/*/
METHOD possuiErro() CLASS PCPMultiThreadError
	Local lError    := .F.
	Local lRetAux

	If VarIsUID(Self:cErrorUID)
		VarGetX(Self:cErrorUID, "lError", @lRetAux)
		If lRetAux != Nil .AND. lRetAux
			lError := .T.
		EndIf
	EndIf

Return lError

/*/{Protheus.doc} getaError
Retorna Array de Erro
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@return aError, array, retorna array de erro: aError[x] := {{"PCPIPCLogE"              ,;
															AllTrim(oErro:description),;
															AllTrim(oErro:ErrorStack) ,;
															oErro:ErrorEnv            })}
/*/
METHOD getaError() CLASS PCPMultiThreadError
	Local aError    := {}
	VarGetA(Self:cErrorUID, "aError", @aError)
	If aError == Nil
		aError := {}
	EndIf
Return aError

/*/{Protheus.doc} destroy
Destroi Sessao de Variaveis Globais
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@return lRet, logico, Indica se conseguiu remover todos os valores das chaves da sess�o <cUID> das tableas "Tabela X" e "Tabela A", assim como todas as transa��es de chaves.
/*/
METHOD destroy() CLASS PCPMultiThreadError
Return VarClean(Self:cErrorUID)

/*/{Protheus.doc} final
Exibe Erros e Finaliza a Thread Atual
@type  Method
@author  brunno.costa
@since   17/07/2020
@version P12.1.27
@param 01 - aError, array, array de erro: aError[x] := {{"PCPIPCLogE"              ,;
                                                         AllTrim(oErro:description),;
                                                         AllTrim(oErro:ErrorStack) ,;
                                                         oErro:ErrorEnv            })}
@return Nil
/*/
METHOD final(aError) CLASS PCPMultiThreadError

	Local aError    := {}
	Local nInd      := 0
	Local nErros    := 0
	Local cMsgErro  := ""

	If aError == Nil .OR. Empty(aError)
		VarGetA(Self:cErrorUID, "aError", @aError)
		Self:destroy()
	EndIf

	If Empty(aError)
		Final(STR0005, STR0006) //"Erro indeterminado." , "Entre em contato com o departamento de TI e solicite consulta ao console.log."
	ELse
		nErros := Len(aError)
		For nInd := 1 to nErros
			cMsgErro += aError[nInd][1] + aError[nInd][2] +  aError[nInd][3] + aError[nInd][4] + CHR(10) + CHR(10)
		Next
		Final(STR0007, cMsgErro) //"Erro durante execu��o do MRP"
	EndIf

Return
