#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "PCPA712.CH"

#DEFINE IND_EMPTY '*' + CHR(13) + '*'

Static slMarkAll := .F.

/*/{Protheus.doc} FiltroMultivalorado
Classe para constru��o de tela de filtro multivalorado
@author Marcelo Neumann
@since 31/07/2019
@version P12
/*/
CLASS FiltroMultivalorado FROM LongClassName

	DATA aFields      AS ARRAY
	DATA aFieldRet    AS ARRAY
	DATA aFilter      AS ARRAY
	DATA aSelected    AS ARRAY
	DATA cAlias       AS STRING
	DATA cModelName   AS STRING
	DATA cTitulo      AS STRING
	DATA lPermVazio   AS LOGICAL
	DATA nIndice      AS INTEGER
	DATA oModel       AS OBJECT
	DATA oPreSelected AS OBJECT
	DATA oView        AS OBJECT

	METHOD New() CONSTRUCTOR
	METHOD AbreTela()
	METHOD ButtonOk()
	METHOD Destroy()
	METHOD GetSelected()
	METHOD SetPreSelected()

ENDCLASS

/*/{Protheus.doc} New
Construtor da classe para Filtro Multivalorado
@author Marcelo Neumann
@since 31/07/2019
@version P12
@param 01 cAlias   , caracter, alias para busca dos registros
@param 02 aFields  , array   , array com os campos a serem exibidos na view
@param 03 aFieldRet, array   , array com os campos a serem retornados
@param 04 aFilter  , array   , array com os filtros a serem aplicados na consulta
@param 05 nIndice   , num�rico, �ndice a ser utilizado na consulta do alias
@param 06 cModelName, caracter, nome para o modelo dos resultados
                                (necess�rio informar nomes diferentes caso um mesmo programa reutilize a classe)
@param 07 cTitulo   , caracter, t�tulo para a janela
@param 08 lPermVazio, l�gico  , define se o filtro deve conter um elemento vazio
@return   Self      , objeto  , classe FiltroMultivalorado
/*/
METHOD New(cAlias, aFields, aFieldRet, aFilter, nIndice, cModelName, cTitulo, lPermVazio) CLASS FiltroMultivalorado

	Default nIndice    := 1
	Default cModelName := "GRID_RESULTS"
	Default cTitulo    := STR0148 //"Consulta"

	::oPreSelected := JsonObject():New()
	::aSelected    := {}
	::aFields      := aClone(aFields)
	::aFieldRet    := aClone(aFieldRet)
	::aFilter      := aClone(aFilter)
	::cAlias       := cAlias
	::cModelName   := cModelName
	::cTitulo      := cTitulo
	::nIndice      := nIndice
	::oModel       := Nil
	::oView        := Nil
	::lPermVazio   := lPermVazio

Return Self

/*/{Protheus.doc} Destroy
M�todo para limpar da mem�ria os objetos utilizados pela classe
@author Marcelo Neumann
@since 31/07/2019
@version P12
@return Nil
/*/
METHOD Destroy() CLASS FiltroMultivalorado

	If ::oModel <> Nil
		::oModel:DeActivate()
		::oModel:Destroy()
	EndIf

	If ::oView <> Nil
		::oView:DeActivate()
	EndIf

	aSize(::aFields  , 0)
	aSize(::aFieldRet, 0)
	aSize(::aFilter  , 0)
	aSize(::aSelected, 0)

	FreeObj(::oPreSelected)
	FreeObj(::oModel)
	FreeObj(::oView)

Return

/*/{Protheus.doc} AbreTela
M�todo para abrir a tela de consulta/filtro multivalorado
@author Marcelo Neumann
@since 31/07/2019
@version P12
@return lConfirm, l�gico, indica se a tela foi Confirmada ou Cancelada
/*/
METHOD AbreTela() CLASS FiltroMultivalorado

	Local aArea     := GetArea()
	Local aButtons  := { {.F.,Nil    },{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,STR0146},; //"Confirmar"
	                     {.T.,STR0147},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil    } } //"Cancelar"
	Local lConfirm  := .T.
	Local oViewExec := FWViewExec():New()

	If ::oModel == Nil
		::oModel := ModelDef(::Self)
	EndIf

	(::cAlias)->(dbSetOrder(1))
	(::cAlias)->(MsSeek(xFilial(::cAlias)))

	::oModel:SetOperation(MODEL_OPERATION_UPDATE)
	::oModel:Activate()

	If ::oView == Nil
		::oView := ViewDef(::Self)
		::oView:SetOperation(MODEL_OPERATION_VIEW)
	EndIf

	//Vari�vel de controle da op��o de Marcar Todos
	slMarkAll := .F.

	oViewExec:setModel(::oModel)
	oViewExec:setView(::oView)
	oViewExec:setTitle(::cTitulo)
	oViewExec:setOperation(MODEL_OPERATION_VIEW)
	oViewExec:setReduction(65)
	oViewExec:setButtons(aButtons)
	oViewExec:SetCloseOnOk({|| ::ButtonOk()})
	oViewExec:setCancel({|| SetModify(::oView,.F.)})
	oViewExec:openView(.F.)

	If oViewExec:getButtonPress() == VIEW_BUTTON_OK
		lConfirm := .T.
	Else
		lConfirm := .F.
	Endif

	RestArea(aArea)

Return lConfirm

/*/{Protheus.doc} ModelDef
Defini��o do Modelo
@author Marcelo Neumann
@since 31/07/2019
@version P12
@param 01 oSelf , objeto, objeto referente � classe
@return   oModel, objeto, modelo definido
/*/
Static Function ModelDef(oSelf)

	Local oModel    := MPFormModel():New('PCPA712Sel')
	Local oStruCab  := FWFormModelStruct():New()
	Local oStruGrid := FWFormStruct(1, oSelf:cAlias, {|cCampo| "|" + AllTrim(cCampo) + "|" $ "|" + ArrTokStr(oSelf:aFields, "|", 0) + "|"})

	//Altera as estruturas dos modelos
 	AltStruMod(@oStruCab, @oStruGrid)

	//FLD_INVISIVEL - Modelo "invis�vel"
	oModel:addFields('FLD_INVISIVEL', /*cOwner*/, oStruCab, , , {|| LoadMdlFld()})
	oModel:GetModel("FLD_INVISIVEL"):SetDescription(STR0148) //"Consulta"
	oModel:GetModel("FLD_INVISIVEL"):SetOnlyQuery(.T.)

	//::cModelName - Grid de resultados (o nome deve ser atribuido atrav�s da propriedade ::cModelName)
	oModel:AddGrid(oSelf:cModelName, "FLD_INVISIVEL", oStruGrid, , , , ,{|oGridModel| LoadMdlGrd(oGridModel, oSelf)})
	oModel:GetModel(oSelf:cModelName):SetDescription(STR0150) //"Resultados"
	oModel:GetModel(oSelf:cModelName):SetOnlyQuery(.T.)
	oModel:GetModel(oSelf:cModelName):SetOptional(.T.)
	oModel:GetModel(oSelf:cModelName):SetNoDeleteLine(.T.)
	oModel:GetModel(oSelf:cModelName):SetNoInsertLine(.T.)

	oModel:SetDescription(STR0148) //"Consulta"
	oModel:SetPrimaryKey({})

Return oModel

/*/{Protheus.doc} ViewDef
Defini��o da View
@author Marcelo Neumann
@since 31/07/2019
@version P12
@param 01 oSelf, objeto, objeto referente � classe
@return   oView, objeto, view definida
/*/
Static Function ViewDef(oSelf)

	Local oStruGrid := FWFormStruct(2, oSelf:cAlias, {|cCampo| "|" + AllTrim(cCampo) + "|" $ "|" + ArrTokStr(oSelf:aFields, "|", 0) + "|"})
	Local oView     := FWFormView():New()

	//Altera os campos da estrutura para a view
	AltStrView(@oStruGrid, oSelf)

	//Defini��es da View
	oView:SetModel(oSelf:oModel)

	//V_GRID_RESULTS - View da Grid com resultado da pesquisa
	oView:AddGrid("V_GRID_RESULTS", oStruGrid, oSelf:cModelName)

	//Relaciona a SubView com o Box
	oView:CreateHorizontalBox("BOX_GRID", 100)
	oView:SetOwnerView("V_GRID_RESULTS", 'BOX_GRID')

	//Fun��o chamada ap�s ativar a View
	oView:SetAfterViewActivate({|oView| AfterView(oView)})

	//Habilita os bot�es padr�es de filtro e pesquisa
	oView:SetViewProperty("V_GRID_RESULTS", "GRIDFILTER", {.T.})
	oView:SetViewProperty("V_GRID_RESULTS", "GRIDSEEK", {.T.})

	//Seta a��o ap�s o checkbox
	oView:SetFieldAction("LSELECT", { |oView| SetModify(oView, .F.) })

	//Seta para n�o exibir a mensagem de Modifica��o
	oView:showUpdateMsg(.F.)

Return oView

/*/{Protheus.doc} LoadMdlFld
Carga do modelo mestre (invis�vel)
@author Marcelo Neumann
@since 31/07/2019
@version P12
@return aLoad, array, array de load do modelo preenchido
/*/
Static Function LoadMdlFld()

	Local aLoad := {}

   aAdd(aLoad, {"A"}) //dados
   aAdd(aLoad, 1    ) //recno

Return aLoad

/*/{Protheus.doc} ViewDef
Carga do modelo detalhe (consulta)
@author Marcelo Neumann
@since 31/07/2019
@version P12
@param 01 oGridModel, objeto, modelo que deve ser carregado
@param 02 oSelf     , objeto, objeto referente � classe
@return   aLoad     , array , array de load do modelo preenchido
/*/
Static Function LoadMdlGrd(oGridModel, oSelf)

	Local aFields    := {}
	Local aLoad      := {}
	Local aRegistro  := {}
	Local aStruct    := oGridModel:oFormModelStruct
	Local aStrFields := aStruct:aFields
	Local cNextAlias := GetNextAlias()
	Local cQuery     := ""
	Local cOrderBy   := SqlOrder((oSelf:cAlias)->(IndexKey(oSelf:nIndice)))
	Local lSelect    := .F.
	Local nInd       := 1
	Local nLenAux    := 0
	Local nLenFields := 0
	Local aOrder     := StrTokArr(cOrderBy, ",")

	//Busca os campos reais da estrutura do modelo
	nLenAux := Len(aStrFields)
	For nInd := 1 To nLenAux
		If !aStruct:GetProperty(aStrFields[nInd][3], MODEL_FIELD_VIRTUAL)
			//Adiciona o nome do campo e o tipo
			aAdd(aFields, {aStrFields[nInd][3], aStrFields[nInd][4]})
		EndIf
	Next nInd

	cQuery := "SELECT DISTINCT 1"

	//Preenche o SELECT com os campos reais do modelo
	nLenFields := Len(aFields)
	For nInd := 1 To nLenFields
		cQuery += ", " + aFields[nInd][1]
	Next nInd

	//Preenche o SELECT com os campos utilizados no ORDER BY (caso n�o tenha sido inclu�do ainda)
	nLenAux := Len(aOrder)
	For nInd := 1 To nLenAux
		If !aOrder[nInd] $ cQuery
			cQuery += ", " + aOrder[nInd]
		EndIf
	Next nInd

	cQuery +=  " FROM " + RetSqlName(oSelf:cAlias)
	cQuery += " WHERE D_E_L_E_T_ = ' '"

	//Acrescenta os filtros definidos na classe
	nLenAux := Len(oSelf:aFilter)
	For nInd := 1 To nLenAux
		cQuery += " AND (" + oSelf:aFilter[nInd] + ")"
	Next nInd

	cQuery += " ORDER BY " + cOrderBy

	//Inclui uma linha de c�digo em branco quando necess�rio
	If oSelf:lPermVazio
		If aFields[1][1] == oSelf:aFieldRet[1]
			If oSelf:oPreSelected[IND_EMPTY] == Nil
				lSelect := .F.
			Else
				lSelect := .T.
				oSelf:oPreSelected[IND_EMPTY] := Len(aLoad) + 1
			EndIf
		EndIf
		aAdd(aLoad, {0, {" ", STR0182, lSelect} }) //"Grupo n�o informado"
	EndIf

	//Realiza a consulta dos registros
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cNextAlias,.T.,.T.)

	//Percorre os registros encontrados, atualizando o modelo
	While !(cNextAlias)->(Eof())
		aSize(aRegistro,0)
		lSelect := .F.

		//Preenche os campos do modelo
		For nInd := 1 To nLenFields
			//Campo DATE precisa ser convertido
			If aFields[nInd][2] == "D"
				aAdd(aRegistro, SToD((cNextAlias)->&(aFields[nInd][1])))
			Else
				aAdd(aRegistro, (cNextAlias)->&(aFields[nInd][1]))
			EndIf

			//Verifica se o registro j� deve vir selecionado
			If aFields[nInd][1] == oSelf:aFieldRet[1]
				If oSelf:oPreSelected[AllTrim((cNextAlias)->&(aFields[nInd][1]))] == Nil
					lSelect := .F.
				Else
					lSelect := .T.
					oSelf:oPreSelected[AllTrim((cNextAlias)->&(aFields[nInd][1]))] := Len(aLoad) + 1
				EndIf
			EndIf
		Next nInd

		aAdd(aRegistro, lSelect)

		aAdd(aLoad, {0, aClone(aRegistro)})

		(cNextAlias)->(DbSkip())
	EndDo
	(cNextAlias)->(dbCloseArea())

Return aLoad

/*/{Protheus.doc} AltStruMod
Edita os campos da estrutura do Model
@author Marcelo Neumann
@since 31/07/2019
@version P12
@param 01 oStruCab , object, estrutura do modelo FLD_INVISIVEL
@param 02 oStruGrid, object, estrutura do modelo GRID_RESULTS
@return Nil
/*/
Static Function AltStruMod(oStruCab, oStruGrid)

	//Adiciona campo
	oStruCab:AddField(STR0148, STR0148, "ARQ", "C", 1, 0, , , {}, .T., , .F., .F., .F., , ) //"Consulta"

	//Adiciona o checkbox para selecionar o registro
	oStruGrid:AddField(STR0149  ,; // [01]  C   Titulo do campo  - "Selecionado?"
                       STR0149  ,; // [02]  C   ToolTip do campo - "Selecionado?"
                       "LSELECT",; // [03]  C   Id do Field
                       "L"      ,; // [04]  C   Tipo do campo
                       1        ,; // [05]  N   Tamanho do campo
                       0        ,; // [06]  N   Decimal do campo
                       Nil      ,; // [07]  B   Code-block de valida��o do campo
                       Nil      ,; // [07]  B   Code-block de valida��o When do campo
                       NIL      ,; // [09]  A   Lista de valores permitido do campo
                       .F.      ,; // [10]  L   Indica se o campo tem preenchimento obrigat�rio
                       NIL      ,; // [11]  B   Code-block de inicializacao do campo
                       .F.      ,; // [12]  L   Indica se trata-se de um campo chave
                       .F.      ,; // [13]  L   Indica se o campo n�o pode receber valor em uma opera��o de update
                       .T.      )  // [14]  L   Indica se o campo � virtual

	oStruGrid:SetProperty(oStruGrid:aFields[1][3], MODEL_FIELD_OBRIGAT, .F.)

Return Nil

/*/{Protheus.doc} AltStrView
Edita os campos da estrutura da View
@author Marcelo Neumann
@since 31/07/2019
@version P12
@param 01 oStrDetail, object, estrutura da View V_GRID_RESULTS
@return Nil
/*/
Static Function AltStrView(oStruGrid, oSelf)

	Local nInd    := 1
	Local nLength := Len(oSelf:aFields)

	//Reornena os campos de acordo com o que foi setado na classe e seta para n�o edit�vel
	For nInd := 1 To nLength
		If oStruGrid:HasField(oSelf:aFields[nInd])
			oStruGrid:SetProperty(oSelf:aFields[nInd], MVC_VIEW_CANCHANGE, .F.)
			oStruGrid:SetProperty(oSelf:aFields[nInd], MVC_VIEW_ORDEM    , cValToChar(nInd+1))
		EndIf
	Next nInd

	//Se o modelo possui dados, inclui a coluna com o checkbox
	If !oSelf:oModel:GetModel(oSelf:cModelName):IsEmpty()
		oStruGrid:AddField("LSELECT"                    ,; // [01]  C   Nome do Campo
							"01"                        ,; // [02]  C   Ordem
							""                          ,; // [03]  C   Titulo do campo
							""                          ,; // [04]  C   Descricao do campo
							NIL                         ,; // [05]  A   Array com Help
							"L"                         ,; // [06]  C   Tipo do campo
							NIL                         ,; // [07]  C   Picture
							NIL                         ,; // [08]  B   Bloco de Picture Var
							NIL                         ,; // [09]  C   Consulta F3
							.T.                         ,; // [10]  L   Indica se o campo � alteravel
							NIL                         ,; // [11]  C   Pasta do campo
							NIL                         ,; // [12]  C   Agrupamento do campo
							NIL                         ,; // [13]  A   Lista de valores permitido do campo (Combo)
							NIL                         ,; // [14]  N   Tamanho maximo da maior op��o do combo
							NIL                         ,; // [15]  C   Inicializador de Browse
							.T.                         ,; // [16]  L   Indica se o campo � virtual
							NIL                         ,; // [17]  C   Picture Variavel
							NIL                         )  // [18]  L   Indica pulo de linha ap�s o campo
	EndIf

Return Nil

/*/{Protheus.doc} AfterView
Fun��o executada ap�s ativar a view
@author Marcelo Neumann
@since 31/07/2019
@version P12
@param 01 oView, object, objeto da View
@return Nil
/*/
Static Function AfterView(oView)

	//Seta funcionalidade de marcar/desmarcar todos clicando no cabe�alho
	oView:GetSubView("V_GRID_RESULTS"):oBrowse:aColumns[1]:bHeaderClick := {|| MarcaTodos(oView) }

	//Seta o modelo como n�o alterado
	SetModify(oView, .F.)

Return Nil

/*/{Protheus.doc} MarcaTodos
Fun��o executada ao clicar no cabe�alho do CheckBox
@author Marcelo Neumann
@since 31/07/2019
@version P12
@param 01 oView, object, objeto da View
@return Nil
/*/
Static Function MarcaTodos(oView)

	Local oViewGrid := oView:GetSubView("V_GRID_RESULTS")
	Local oMdlGrid  := oViewGrid:GetModel()
	Local aFilLines := oViewGrid:GetFilLines()
	Local nLinAtua  := oMdlGrid:GetLine()
	Local nInd      := 1
	Local nLength   := Len(aFilLines)

	slMarkAll := !slMarkAll

	//Percorre a grid marcando/descmarcando todos os registros (considerando o filtro)
	For nInd := 1 to nLength
		oMdlGrid:GoLine(aFilLines[nInd])
		oMdlGrid:LoadValue("LSELECT", slMarkAll)
	Next nInd

	//Atualiza a Grid
	oMdlGrid:GoLine(nLinAtua)

	//Tratamento para caso os registros estejam filtrados
	If Len(aFilLines) == oMdlGrid:Length()
		oViewGrid:DeActivate(.T.)
		oViewGrid:Activate()
	Else
		oViewGrid:Refresh()
	EndIf

Return Nil

/*/{Protheus.doc} ButtonOk
M�todo chamado ao pressionar o bot�o "Confirmar" da tela
@author Marcelo Neumann
@since 31/07/2019
@version P12
@return lRet, Logical, retorna true para permitir fechar a tela
/*/
METHOD ButtonOk() CLASS FiltroMultivalorado

	Local aLinesChg := {}
	Local aRetLine  := {}
	Local aNames
	Local nIndFlds  := 1
	Local nIndLines := 1
	Local nLenFld   := 0
	Local nLenLin   := 0
	Local oModel    := ::oView:GetModel()

	//Se algum registro veio pr�-selecionado, seta a linha como modificada
	If ::oPreSelected <> NIL
		aNames  := ::oPreSelected:GetNames()
		nLenLin := Len(aNames)
		For nIndLines := 1 To nLenLin
			If ::oPreSelected[aNames[nIndLines]] <> Nil .And. ::oPreSelected[aNames[nIndLines]] > 0
				::oModel:GetModel(::cModelName):SetLineModify(::oPreSelected[aNames[nIndLines]])
			EndIf
		Next nIndLines
	EndIf

	//Verifica se foi clicado em algum checkbox
	aLinesChg := oModel:GetModel(::cModelName):GetLinesChanged()
	If Len(aLinesChg) > 0
		aSize(::aSelected, 0)
	EndIf

	//Percorre as linhas que tiveram altera��o
	nLenLin := Len(aLinesChg)
	For nIndLines := 1 To nLenLin
		//Se o registro ficou marcado, preenche o array de retorno (aSelected)
		If oModel:GetModel(::cModelName):GetValue("LSELECT", aLinesChg[nIndLines])
			aSize(aRetLine, 0)

			//Preenche os campos que ser�o retornados (definidos na classe)
			nLenFld := Len(::aFieldRet)
			For nIndFlds := 1 To nLenFld
				aAdd(aRetLine, { ::aFieldRet[nIndFlds], ;
				                 oModel:GetModel(::cModelName):GetValue(::aFieldRet[nIndFlds], aLinesChg[nIndLines]) })
			Next nIndFlds

			//Se selecionou o registro em branco, transforma no caracter especial (Json n�o permite chave em branco)
			If aRetLine[1][2] == " "
			 	aRetLine[1][2] := IND_EMPTY
			EndIf

			aAdd(::aSelected, aClone(aRetLine))
		EndIf
	Next nIndLines

	SetModify(::oView,.T.)

Return .T.

/*/{Protheus.doc} GetSelected
M�todo para recuperar os registros que foram selecionados na consulta
@author Marcelo Neumann
@since 31/07/2019
@version P12
@return aSelected, array, array com os registros que foram selecionados
/*/
METHOD GetSelected() CLASS FiltroMultivalorado

Return ::aSelected

/*/{Protheus.doc} SetPreSelected
Seta os registros que dever�o vir pr�-selecionados ao abrir a tela
@author Marcelo Neumann
@since 31/07/2019
@version P12
@param 01 aPreSel, array, registros que devem vir pr�-selecionados na consulta
                          (considerar como chave o campo informado na primeira posi��o da propriedade ::aFieldRet)
/*/
METHOD SetPreSelected(aPreSel) CLASS FiltroMultivalorado

	Local nIndSelect := 1
	Local nLenPreSel := Len(aPreSel)

	//Inicializa a propriedade que grava os pr�-selecionados
	FreeObj(::oPreSelected)
	::oPreSelected := JsonObject():New()

	//Grava a chave dos registros pr�-selecionados
	For nIndSelect := 1 To nLenPreSel
		If Empty(aPreSel[nIndSelect])
			::oPreSelected[IND_EMPTY] := 0
		Else
			::oPreSelected[AllTrim(aPreSel[nIndSelect])] := 0
		EndIf

	Next nIndSelect

Return Nil

/*/{Protheus.doc} SetModify
Seta o indicador de modificado do modelo
@author Marcelo Neumann
@since 31/07/2019
@version P12
@param 01 oView, object, objeto da View
@param 02 lMod , logic , indica se o modelo ser� setado para modificado ou n�o
@return .T.
/*/
Static Function SetModify(oView, lMod)

	Local oModel := oView:GetModel()

	oModel:lModify := lMod
	oView:lModify  := lMod

Return .T.