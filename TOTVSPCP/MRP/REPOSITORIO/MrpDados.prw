#INCLUDE 'protheus.ch'
#INCLUDE 'MrpDados.ch'

Static soDominio     := Nil             //Instancia da camada de dominio
Static slTravaOut    := .F.
Static slTravaSE     := .T.
Static slTravaOCI    := .T.

Static snTamCod      := 90
Static sPRD_FILIAL   := 1
Static sPRD_COD      := 2
Static sPRD_ESTSEG   := 3
Static sPRD_LE       := 4 //Lote econ�mico.
Static sPRD_PE       := 5
Static sPRD_SLDDIS   := 6
Static sPRD_NIVEST   := 7
Static sPRD_CHAVE2   := 8
Static sPRD_NPERAT   := 9
Static sPRD_NPERMA   := 10 //Ultimo periodo permitido calcular - limitacao de bloqueio
Static sPRD_THREAD   := 11
Static sPRD_NPERCA   := 12 //Ultimo periodo calculado
Static sPRD_REINIC   := 13
Static sPRD_IDOPC    := 14
Static sPRD_HORFIR   := 15
Static sPRD_TPHOFI   := 16
Static sPRD_DTHOFI   := 17
Static sPRD_TIPE     := 18
Static sPRD_PPED     := 19
Static sPRD_REVATU   := 20
Static sPRD_TIPDEC   := 21
Static sPRD_NUMDEC   := 22
Static SPRD_ROTEIR   := 23
Static sPRD_QTEMB    := 24 //Qtd. Embalagem
Static sPRD_LM       := 25 //Lote M�nimo
Static sPRD_TOLER    := 26 //Toler�ncia
Static sPRD_TIPO     := 27
Static sPRD_GRUPO    := 28
Static sPRD_RASTRO   := 29
Static sPRD_MRP      := 30
Static sPRD_EMAX     := 31
Static sPRD_PROSBP   := 32
Static sPRD_LOTSBP   := 33
Static sPRD_ESTORI   := 34
Static sPRD_APROPR   := 35
Static sPRD_LOTVNC   := 36
Static sPRD_CPOTEN   := 37
Static sPRD_BLOQUE   := 38
Static sPRD_LSUBPR   := 39

Static sEST_FILIAL   := 1
Static sEST_CODPAI   := 2
Static sEST_CODFIL   := 3
Static sEST_QTD      := 4
Static sEST_FANT     := 5
Static sEST_TRT      := 6
Static sEST_GRPOPC   := 7
Static sEST_ITEOPC   := 8
Static sEST_VLDINI   := 9
Static sEST_VLDFIM   := 10
Static sEST_REVINI   := 11
Static sEST_REVFIM   := 12
Static sEST_ALTERN   := 13
Static sEST_FIXA	 := 14
Static sEST_POTEN    := 15
Static sEST_PERDA    := 16
Static sEST_QTDB     := 17
Static sEST_OPERA    := 18

Static sMAT_FILIAL   := 1
Static sMAT_DATA     := 2
Static sMAT_PRODUT   := 3
Static sMAT_SLDINI   := 4
Static sMAT_ENTPRE   := 5
Static sMAT_SAIPRE   := 6
Static sMAT_SAIEST   := 7
Static sMAT_SALDO    := 8
Static sMAT_NECESS   := 9
Static sMAT_TPREPO   := 10
Static sMAT_EXPLOD   := 11
Static sMAT_THREAD   := 12
Static sMAT_IDOPC    := 13
Static sMAT_DTINI    := 14

Static sALT_FILIAL   := 1
Static sALT_PRODUT   := 2
Static sALT_ALTERT   := 3
Static sALT_FATOR    := 4
Static sALT_TPFAT    := 5
Static sALT_ORDEM    := 6
Static sALT_DATA     := 7
Static sTAM_ALT      := 7

Static sOPC_KEY      := 1
Static sOPC_KEY2     := 2
Static sOPC_OPCION   := 3
Static sOPC_ID       := 4
Static sOPC_IDPAI    := 5
Static sOPC_IDMASTER := 6
Static sOPC_T4JRECNO := 7
Static sOPC_T4QRECNO := 8
Static sTAM_OPC      := 8

Static sCAL_FILIAL   := 1
Static sCAL_DATA     := 2
Static sCAL_HRINI    := 3
Static sCAL_HRFIM    := 4
Static sCAL_INTER    := 5
Static sCAL_UTEIS    := 6

Static sOPE_ROTE  := 1
Static sOPE_OPERA := 2
Static sTAM_OPE   := 2

Static saMAT

/*/{Protheus.doc} MrpDados
Classe para manipula��o de dados via variaveis globais
@author    brunno.costa
@since     25/04/2019
@version   1
/*/
CLASS MrpDados FROM LongClassName

	DATA aPeriodos          AS ARRAY
	DATA oAlternativos      AS OBJECT
	DATA oAglutinacao       AS OBJECT
	DATA oRastreio          AS OBJECT
	DATA oDominio           AS OBJECT
	DATA oParametros        AS OBJECT
	DATA oMatriz            AS OBJECT
	DATA oProdutos          AS OBJECT
	DATA oEstruturas        AS OBJECT
	DATA oSubProdutos       AS OBJECT
	DATA oLiveLock          AS OBJECT
	DATA oLogs              AS OBJECT
	DATA oJsonOpcionais     AS OBJECT
	DATA oOpcionais         AS OBJECT
	DATA oPendencias        AS OBJECT
	DATA oCargaMemoria      AS OBJECT
	DATA oCalendario        AS OBJECT
	DATA oVersaoDaProducao  AS OBJECT
	DATA oEventos           AS OBJECT
	DATA oSeletivos         AS OBJECT
	DATA lReordena          AS LOGICAL

	METHOD new() CONSTRUCTOR
	METHOD inicializaTabelas()

	//Metodos pontuais de manipula��o de dados em tabela
	METHOD gravaCampo()
	METHOD gravaLinha()
	METHOD retornaCampo()
	METHOD retornaLinha()
	METHOD retornaLista()
	METHOD tamanhoLista()
	METHOD posicaoCampo(cCampo)

	//Sets de atributos
	METHOD setAPeriodos(aPeriodos)
	METHOD setoDominio(oDominio)

	//Outros metodos
	METHOD atualizaMatriz(dData, cProduto, cIDOpc, nPerMinimo, aFields, aValores, lMantTrava)
	METHOD decrementaTotalizador(cProduto)
	METHOD incrementaTotalizador(cProduto)
	METHOD existeMatriz()
	METHOD foiCalculado(cProduto, nPeriodo, lAvMinimo)
	METHOD gravaPeriodosProd(cProduto, nPerMinimo, nPerMaximo, nUltPerCal)
	METHOD gravaSaidaEstrutura()
	METHOD isComponente(cProduto)
	METHOD possuiEstrutura(cProduto)
	METHOD possuiPendencia(cProduto, lNoThread, cIDOpc)

	//Metodos para protecao de posicionamento
	METHOD retornaArea()
	METHOD setaArea()
	METHOD trava()
	METHOD destrava()
	METHOD reservaProduto()
	METHOD liberaProduto()

	METHOD destruir()

ENDCLASS

/*/{Protheus.doc} MrpDados
M�todo construtor da classe MrpDados
@author    brunno.costa
@since     25/04/2019
@version   1
@param 01 - oParametros, objeto  , Objeto JSON com todos os parametros do MRP - Consulte MRPAplicacao():parametrosDefault()
@param 02 - aPeriodos  , array   , array com as datas dos periodos de processamento
@param 03 - oLogs      , objeto  , instancia da classe de logs
@param 04 - lRecursiva , numero  , indica se refere-se a execucao recursiva
/*/
METHOD new(oParametros, aPeriodos, oLogs, lRecursiva) CLASS MrpDados

	Default lRecursiva := .F.

	::oParametros        := oParametros
	::aPeriodos          := aPeriodos
	::oLogs              := oLogs
	::lReordena          := .F.

	If lRecursiva .or. ::oParametros["nOpcCarga"] == 2
		::oLogs:log("MrpDados:New() - " + STR0008 + " - " + cValToChar(ThreadID()), "40") //Recursiva
		::inicializaTabelas(.F.)

	Else
		::oLogs:log("MrpDados:New() - " + STR0009 + " - " + cValToChar(ThreadID()), "40") //Nao Recursiva
		::inicializaTabelas(.T.)

	EndIf

	::oCargaMemoria := MrpDados_CargaMemoria():New( Self )

Return Self

/*/{Protheus.doc} inicializaTabelas
Inicializa tabela do MRP:
- MAT - Matriz de movimentos do MRP
- PRD - Parametros de produtos
- EST - Estruturas de produtos
@author    brunno.costa
@since     25/04/2019
@version   1
@param 01 - lCreate   , logico, indica se deve instanciar novos objetos globais
/*/
METHOD inicializaTabelas(lCreate) CLASS MrpDados

	Local cChaveExec := ::oParametros["cChaveExec"]
	Local aSessoes   := {}
	Local nInd
	Local oStatus := MrpDados_Status():New(::oParametros["ticket"])

	Default lCreate := .T.

	If lCreate .AND. (::oParametros["nOpcCarga"] != 2) //!Carga Movimento em Mem�ria
		//Avalia controle de sessoes de variaveis globais e limpa residuos, caso necessario
		If VarIsUID("UIDs_PCPMRP")
			//Protege limpeza de mem�ria
			VarBeginT( "UIDs_PCPMRP", "UIDs_PCPMRP" )

			VarGetXA( "UIDs_PCPMRP", @aSessoes)

            VarBeginT( "UIDs_PCPMRP_LOCK", "UIDs_PCPMRP" )

			//Libera trecho de limpeza de mem�ria
			VarEndT( "UIDs_PCPMRP", "UIDs_PCPMRP" )

			If VarIsUID("UIDs_PCPMRP") .AND. !VarClean("UIDs_PCPMRP")
				Self:oLogs:log("MrpData_Global - VarClean(UIDs_PCPMRP) FALSE", "E")
			EndIf

		    //Elimina residuos de variaveis globais
		    If !Empty(aSessoes)
		    	VarGetXA( "UIDs_PCPMRP", @aSessoes)
				VarEndT( "UIDs_PCPMRP_LOCK", "UIDs_PCPMRP" )
		    	For nInd := 1 to Len(aSessoes)
		    		If !VarClean( aSessoes[nInd][1] )
                        Self:oLogs:log("MrpData_Global - VarClean(aSessoes[nInd][1]) FALSE: '" + AllTrim(aSessoes[nInd][1]) + "'", "E")
					EndIf
		    	Next nInd
			Else
			    VarEndT( "UIDs_PCPMRP_LOCK", "UIDs_PCPMRP" )

		    EndIf

		EndIf

		//Instancia sessao de controle das sessoes globais
		If !VarSetUID( "UIDs_PCPMRP", .T.)
			Self:oLogs:log("MrpData_Global - VarSetUID( UIDs_PCPMRP, .T.) FALSE", "E")
		EndIf
	EndIf

	::oMatriz := MrpData_Global():New( cChaveExec, "MAT", lCreate, ::oLogs, oStatus)
	::oMatriz:setlOrder ( .T. )

	::oProdutos := MrpData_Global():New( cChaveExec, "PRD", lCreate, ::oLogs, oStatus)
	::oProdutos:setlOrder( .T. )

	::oEstruturas := MrpData_Global():New( cChaveExec, "EST", lCreate, ::oLogs, oStatus)
	::oEstruturas:setlOrder( .T. )

	::oSubProdutos := MrpData_Global():New( cChaveExec, "SUB", lCreate, ::oLogs, oStatus)
	::oSubProdutos:setlOrder( .T. )

	::oAglutinacao := MrpData_Global():New( cChaveExec, "AGL", lCreate, ::oLogs, oStatus)
	::oAglutinacao:createAList("1") //Carga de Demandas
	::oAglutinacao:createAList("2") //Carga de Empenhos
	::oAglutinacao:createAList("3") //Carga de Sa�das Outras Previstas
	::oAglutinacao:createAList("4") //Explos�o de Estrutura
	If lCreate
		::oAglutinacao:setFlag("1" + chr(13) + "cIDAuto" + chr(13), "DEM0000000")
		::oAglutinacao:setFlag("2" + chr(13) + "cIDAuto" + chr(13), "EMP0000000")
		::oAglutinacao:setFlag("3" + chr(13) + "cIDAuto" + chr(13), "SAI0000000")
	EndIf

	::oAlternativos := MrpData_Global():New( cChaveExec, "ALT", lCreate, ::oLogs, oStatus)
	::oAlternativos:setlOrder( .T. )
	::oAlternativos:createList("substituicoes_produtos")
	::oAlternativos:createAList("substituicoes_dados")
	::oAlternativos:createAList("min_max")

	::oPendencias := MrpData_Global():New( cChaveExec, "PEN", lCreate, ::oLogs, oStatus)
	::oPendencias:setlOrder( .T. )

	::oLiveLock := MrpData_Global():New( cChaveExec, "LIVELOCK", lCreate, ::oLogs, oStatus)
	::oLiveLock:setlOrder( .T. )

	::oJsonOpcionais := MrpData_Global():New( cChaveExec, "OPC_JSON", lCreate, ::oLogs, oStatus)
	::oJsonOpcionais:setlOrder( .F. )

	::oOpcionais := MrpData_Global():New( cChaveExec, "OPC", lCreate, ::oLogs, oStatus)
	::oOpcionais:setlOrder( .F. )

	::oCalendario := MrpData_Global():New( cChaveExec, "CAL", lCreate, ::oLogs, oStatus)
	::oCalendario:setlOrder( .F. )

	::oVersaoDaProducao := MrpData_Global():New( cChaveExec, "_TAB*VDP_", lCreate, ::oLogs, oStatus)
	::oVersaoDaProducao:setlOrder( .F. )

	::oSeletivos := MrpData_Global():New( cChaveExec, "SELETIVOS", lCreate, ::oLogs, oStatus)
	::oSeletivos:setlOrder( .F. )

	::oEventos := MrpData_Global():New( cChaveExec, "EVENTOS", lCreate, ::oLogs, oStatus)
	::oEventos:setlOrder( .F. )

	If lCreate
		::oEventos:createAList("001")
		::oEventos:createAList("002")
		::oEventos:createAList("003")
		::oEventos:createAList("004")
		::oEventos:createAList("005")
		::oEventos:createAList("006")
		::oEventos:createAList("007")
		::oEventos:createAList("008")
		::oEventos:createAList("009")
		::oEventos:createAList("Entradas")
	EndIf

	If ::oParametros["lRastreia"]
		::oRastreio  := MrpDados_Rastreio():New(lCreate, Self)
	EndIf

Return

/*/{Protheus.doc} atualizaMatriz
Atualiza registro da Matriz
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - dData      , data    , data da saida da estrutura
@param 02 - cProduto   , caracter, codigo do produto
@param 03 - cIDOpc     , caracter, ID do opcional
@param 04 - aFields    , array   , nomes dos campos para atualizar
@param 05 - aValores   , array   , valores dos campos para atualizar
@param 06 - cChave     , caracter, chave do registro na matriz
@param 07 - lMantTrava , l�gico  , indica se deve manter a trava ap�s sa�da da fun��o - utilizado com rastreamento
@param 08 - lFazLock   , l�gico  , Indica se deve ser feito o lock. Se este par�metro for .F., a fun��o chamadora dever� fazer o lock.
@return lReturn, logico, indica se conseguiu atualizar a matriz
/*/
METHOD atualizaMatriz(dData, cProduto, cIDOpc, aFields, aValores, cChave, lMantTrava, lFazLock) CLASS MrpDados

	Local aAux        := Array(14)
	Local aRegistro
	Local cChaveProd
	Local cFilAux     := ""
	Local lError      := .F.
	Local nInd        := 0
	Local lReturn     := .T.

	Default cIDOpc     := ""
	Default aFields    := {}
	Default aValores   := {}
	Default lMantTrava := .F.
	Default lFazLock   := .T.

	cProduto    := PadR(AllTrim(cProduto), snTamCod)
	cChaveProd  := cProduto + Iif(!Empty(cIDOpc), "|" + cIDOpc, "")
	cChave      := cFilAux + DtoS(dData) + cChaveProd

	If lFazLock
		::oMatriz:trava(cChave)
	EndIf
	If ::oMatriz:getRow(1, cChave, Nil, @aRegistro, .F., slTravaSE)
		For nInd := 1 to Len(aFields)
			aRegistro[&("s" + aFields[nInd])] += aValores[nInd]
		Next

		If !::possuiPendencia(cProduto, .T., cIDOpc)
			::decrementaTotalizador(cChaveProd)
		EndIf

		If !::oMatriz:updRow(1, cChave, Nil, aRegistro, .F., slTravaSE)
			lReturn := .F.
		EndIf

	Else
		aAux[sMAT_FILIAL] := cFilAux
		aAux[sMAT_DATA]   := dData
		aAux[sMAT_PRODUT] := cProduto
		aAux[sMAT_SLDINI] := 0
		aAux[sMAT_ENTPRE] := 0
		aAux[sMAT_SAIPRE] := 0
		aAux[sMAT_SAIEST] := 0
		aAux[sMAT_SALDO]  := 0
		aAux[sMAT_NECESS] := 0
		aAux[sMAT_TPREPO] := " "
		aAux[sMAT_EXPLOD] := .F.
		aAux[sMAT_THREAD] := -1
		aAux[sMAT_IDOPC]  := cIDOpc
		aAux[sMAT_DTINI]  := dData

		For nInd := 1 to Len(aFields)
			aAux[&("s" + aFields[nInd])] += aValores[nInd]
		Next

		If !::existeMatriz(cProduto, , , cIDOpc) .OR. !::possuiPendencia(cProduto, .T., cIDOpc)
			::decrementaTotalizador(cChaveProd)
		EndIf

		If !::oMatriz:addRow(cChave, aAux, .F., .F.,,cFilAux+cChaveProd)
			lReturn := .F.
		Else
			//Cria lista deste produto para registrar os periodos
			If !::oMatriz:existList("Periodos_Produto_"+cChaveProd)
				::oMatriz:createList("Periodos_Produto_"+cChaveProd)
			EndIf
			nPeriodo := ::oDominio:oPeriodos:buscaPeriodoDaData(dData, .T.)
			::oMatriz:setItemList("Periodos_Produto_"+cChaveProd, cValToChar(nPeriodo), nPeriodo)
		EndIf

		::oMatriz:setFlag("cExistMAT_" + cChaveProd, .T., lError, .F.)

	EndIf

	If !lMantTrava
		::oMatriz:destrava(cChave)
	EndIf

Return lReturn

/*/{Protheus.doc} gravaSaidaEstrutura
Metodo responsavel por realizar a gravacao de saidas da estrutura
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - dData      , data    , data da saida da estrutura
@param 02 - cComponente, caracter, codigo do componente
@param 03 - nQuantidade, numerico, quantidade do componente
@param 04 - nPerMinimo , numerico, periodo minimo
@param 05 - cIDOpc     , caracter, ID do opcional
@param 06 - lMantTrava , l�gico  , indica se deve manter a trava ap�s sa�da da fun��o - utilizado com rastreamento
@param 07 - lFazLock   , l�gico  , Indica se deve ser feito o lock. Se este par�metro for .F., a fun��o chamadora dever� fazer o lock.
/*/
METHOD gravaSaidaEstrutura(dData, cComponente, nQuantidade, nPerMinimo, cIDOpc, lMantTrava, lFazLock) CLASS MrpDados

	Local cChave

	Default lFazLock := .T.

	//Atualiza ou inclui matriz
	If !::atualizaMatriz(dData, cComponente, cIDOpc, {"MAT_SAIEST"}, {nQuantidade}, @cChave, lMantTrava, lFazLock)
		::oLogs:log(STR0014 + cChave, "E") //"Erro na gravacao da saida estrutura - Chave: "
	EndIf

	//Atualiza periodo minimo de processamento do componente
	If nPerMinimo != Nil
		::oLogs:log(STR0015 + AllTrim(cComponente) + Iif(!Empty(cIDOpc), "|" + cIDOpc, "") + Iif(!Empty(cIDOpc), "|" + cIDOpc, "") + STR0016 + cValToChar(nQuantidade) + STR0017 + cValToChar(nPerMinimo), "27") //"Saida Estrutura -> Periodos Produto: " + " - Quantidade: " + " - Periodo Minimo: "
		::gravaPeriodosProd(cComponente + Iif(!Empty(cIDOpc), "|" + cIDOpc, ""), nPerMinimo)
	EndIf

Return

/*/{Protheus.doc} gravaPeriodosProd
Grava periodos Minimo, Maximo e Ultimo Calculado do Produto
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cProduto  , caracter, codigo do produto
@param 02 - nPerMinimo, numero  , periodo minimo (DE)
@param 03 - nPerMaximo, numero  , periodo maximo (ATE)
@param 04 - nUltPerCal, numero  , ultimo periodo calculado
/*/
METHOD gravaPeriodosProd(cProduto, nPerMinimo, nPerMaximo, nUltPerCal) CLASS MrpDados

	Local aAreaPRD   := {}
	Local aRetAux
	Local lAtual     := .F.
	Local lError     := .F.
	Local lReinicia
	Local nPerMinAtu := -1
	Local nPerMaxAtu := -1
	Local nUltPerAtu := -1
	Local nThreadRes := 0	//ID da Thread que realizou a reserva do registro

	Default nPerMinimo := -1
	Default nPerMaximo := -1
	Default nUltPerCal := -1
	Default lPreLock   := .F.

	::trava("PRD", cProduto)

	nPerMinimo := Iif(nUltPerCal > nPerMinimo .AND. nUltPerCal > 0, nUltPerCal, nPerMinimo)

	::oLogs:log(STR0018 + AllTrim(cProduto) + " " + STR0020 + cValtoChar(nPerMinimo) + " " + STR0022 + cValToChar(nPerMaximo) + " " + STR0021 + cValToChar(nUltPerCal), "22") //"Inicio atualizacao periodos do produto: "
	While !(nPerMinimo == Nil .and. nPerMaximo == Nil .and. nUltPerCal == Nil)

		//Alteracao periodo minimo (DE)
		If nPerMinimo != Nil .AND. nPerMinimo != -1
			aRetAux    := ::retornaCampo("PRD", 1, cProduto, {"PRD_NPERAT", "PRD_THREAD", "PRD_REINIC"}, @lError, lAtual, .F., .F., , ,.T. /*lVarios*/)
			If lError .AND. lAtual
				lError := .F.
				lAtual := .F.
				Sleep(50)
				Loop

			ElseIf lError
				lError := .F.
				Sleep(50)
				Loop

			EndIf
			nPerMinAtu := aRetAux[1]
			nThreadRes := aRetAux[2]
			lReinicia  := aRetAux[3]
			lAtual     := .T.
			If nPerMinAtu > nPerMinimo;  //Atualiza para tras
			   .OR. (nPerMinAtu >= nPerMinimo .AND. nThreadRes > 0 .AND. nThreadRes != ThreadID()); //Atualiza para tras
			   .OR. (!lReinicia .AND. nPerMinAtu <  nPerMinimo .AND. nThreadRes > 0 .AND. nThreadRes == ThreadID()) //Atualiza para frente

				If nPerMinimo < ::oParametros["nPeriodos"]
					::oLogs:log(STR0019 + AllTrim(cProduto) + STR0020 + cValToChar(nPerMinimo), "23") //" - Produto: " + " - Periodo Atual (PRD_NPERAT): "
				EndIf
				If nThreadRes > 0 .AND. nThreadRes != ThreadID()
					If nPerMinAtu > nPerMinimo
						::gravaCampo("PRD", 1, cProduto, {"PRD_NPERAT", "PRD_REINIC"}, {nPerMinimo, .T.}, .T.,,, .T. /*lVarios*/)
					Else
						::gravaCampo("PRD", 1, cProduto, "PRD_REINIC", .T., .T.)
					EndIf
				Else
					::gravaCampo("PRD", 1, cProduto, "PRD_NPERAT", nPerMinimo, .T.)
				EndIf
				nUltPerAtu := ::retornaCampo("PRD", 1, cProduto, "PRD_NPERCA", @lError, .T.)
				::oLogs:log(STR0019 + AllTrim(cProduto) + STR0021 + cValToChar((nPerMinimo - 1)), "24") //" - Produto: " + " - Ult. Periodo Calculado (PRD_NPERCA): "
				::gravaCampo("PRD", 1, cProduto, "PRD_NPERCA", (nPerMinimo - 1), .T.)
				nPerMinimo := Nil
			Else
				If nThreadRes > 0 .AND. nThreadRes != ThreadID()
					::gravaCampo("PRD", 1, cProduto, "PRD_REINIC", .T., .T.)

				EndIf

				::oLogs:log(STR0040 + AllTrim(cProduto) + STR0020 + cValtoChar(nPerMinimo) + " -> Nil - Atual: " + cValToChar(nPerMinAtu), "23") //"Atualizacao de periodos produto: " + " - Periodo Minimo: "
				nPerMinimo := Nil

			EndIf
		Else
			nPerMinimo := Nil

		EndIf

		//Alteracao periodo maximo (ATE)
		If nPerMaximo != Nil .AND. nPerMaximo != -1
			nPerMaxAtu := ::retornaCampo("PRD", 1, cProduto, "PRD_NPERMA", @lError, lAtual, .F., .F.)
			If lError .AND. lAtual
				lError := .F.
				lAtual := .F.
				Sleep(50)
				Loop

			ElseIf lError
				lError := .F.
				Sleep(50)
				Loop

			EndIf
			lAtual     := .T.
			If (nPerMaxAtu > nPerMaximo .OR. (nPerMaxAtu == -1))
				::oLogs:log(STR0019 + AllTrim(cProduto) + STR0022 + cValToChar(nPerMaximo), "25") //" - Produto: " + " - Periodo Maximo (PRD_NPERMA): "
				::gravaCampo("PRD", 1, cProduto, "PRD_NPERMA", nPerMaximo, .T., .F., .F.)
				nPerMaximo := Nil
			Else
				::oLogs:log(STR0040 + AllTrim(cProduto) + STR0022 + cValtoChar(nPerMaximo) + " -> Nil - Atual: " + cValToChar(nPerMaxAtu), "25") //"Atualizacao de periodos produto: " +" - Periodo Maximo: "
				nPerMaximo := Nil

			EndIf
		Else
			nPerMaximo := Nil
		EndIf

		//Alteracao ultimo periodo calculado
		If nUltPerCal != Nil .AND. nUltPerCal != -1
			nUltPerAtu := ::retornaCampo("PRD", 1, cProduto, "PRD_NPERCA", @lError, lAtual)
			If lError .AND. lAtual
				lError := .F.
				lAtual := .F.
				Sleep(50)
				Loop

			ElseIf lError
				lError := .F.
				Sleep(50)
				Loop

			EndIf
			lAtual     := .T.
			If nUltPerCal < ::oParametros["nPeriodos"]
				::oLogs:log(STR0019 + AllTrim(cProduto) + STR0021 + cValToChar(nUltPerCal), "24") //" - Produto: " + " - Ult. Periodo Calculado (PRD_NPERCA): "
			EndIf
			::gravaCampo("PRD", 1, cProduto, "PRD_NPERCA", nUltPerCal, .T., .F., .F.)
			nUltPerCal := Nil
		Else
			::oLogs:log(STR0040 + AllTrim(cProduto) + STR0021 + cValtoChar(nUltPerCal) + " -> Nil - Atual: " + cValToChar(nUltPerAtu), "24") //"Atualizacao de periodos produto: " + " - Ult. Calculado: "
			nUltPerCal := Nil

		EndIf
	EndDo

	::destrava("PRD", cProduto)
	If !Empty(aAreaPRD)
		::setaArea(aAreaPRD)
	EndIf

Return

/*/{Protheus.doc} possuiPendencia
Identifica se o produto possui pendencia de calculo
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto , caracter, codigo do produto
@param 02 - lNoThread, logico  , considera true somente produtos sem reserva de Thread
@param 03 - cIDOpc   , caracter, ID do opcional relacionado
@return lReturn, logico, indica se o produto possui pendencias de calculo
/*/
METHOD possuiPendencia(cProduto, lNoThread, cIDOpc) CLASS MrpDados
	Local aAreaPRD    := Nil
	Local aRetAux
	Local cChaveProd
	Local lAtual      := .F.//(cProduto + Iif(!Empty(cIDOpc), "|" + cIDOpc, "")) == ::oProdutos:cCurrentKey
	Local lError      := .F.
	Local lReturn     := .F.
	Local nThreadRes  := 0
	Local nPerInicio

	Default lNoThread := .F.
	Default cIDOpc    := ""

	cChaveProd := cProduto + Iif(!Empty(cIDOpc), "|" + cIDOpc, "")

	If !lAtual
		aAreaPRD := ::retornaArea("PRD")
	EndIf

	If lNoThread
		aRetAux    := ::retornaCampo("PRD", 1, cChaveProd, {"PRD_NPERAT", "PRD_THREAD"} , @lError, lAtual, , , , , .T. /*lVarios*/)
		nPerInicio := aRetAux[1]
		nThreadRes := aRetAux[1]
	Else
		nPerInicio := ::retornaCampo("PRD", 1, cChaveProd, "PRD_NPERAT" , @lError, lAtual)
	EndIf

	If nPerInicio <= ::oParametros["nPeriodos"] .and. nThreadRes <= 0
		lReturn := .T.
		::oLogs:log("Possui pendencia: " + cChaveProd + " - Periodo atual: " + cValToChar(nPerInicio) , "29")
	Else
		::oLogs:log("Nao possui pendencia: " + cChaveProd + " - Periodo atual: " + cValToChar(nPerInicio) , "29")
	EndIf

	If aAreaPRD != Nil
		::setaArea(aAreaPRD)
	EndIf
Return lReturn

/*/{Protheus.doc} foiCalculado
Verifica se o produto ja foi calculado neste periodo
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto , caracter, codigo do produto a ser avaliado
@param 02 - nPeriodo , numero  , periodo a ser avaliado
@param 03 - lAvMinimo, logico  , indica se deve avaliar o periodo atual
            (se ultimo periodo calculado >= periodo atual, considera periodo anterior ao atual como ultimo calculado)
@return lReturn, logico, indica se o produto foi calculado no periodo
/*/
METHOD foiCalculado(cProduto, nPeriodo, lAvMinimo) CLASS MrpDados
	Local aAreaPRD   := Nil
	Local aRetAux
	Local lAtual     := .T.
	Local lError     := .F.
	Local lReturn    := .T.
	Local nUltPerCal

	Default lAvMinimo := .F.

	If cProduto != ::retornaCampo("PRD", 1, NIL, "PRD_COD" , @lError, .T. /*lAtual*/)
		aAreaPRD := ::retornaArea("PRD")
		lAtual   := .F.
	EndIf

	If lAvMinimo
		aRetAux   := ::retornaCampo("PRD", 1, cProduto,	{"PRD_NPERCA", "PRD_NPERAT"}, @lError, lAtual /*lAtual*/, , , , , .T. /*lVarios*/)
		nUltPerCal := aRetAux[1]
		If nUltPerCal >= aRetAux[2]
			nUltPerCal := (aRetAux[2] - 1)
		EndIf
	Else
		nUltPerCal := ::retornaCampo("PRD", 1, cProduto, "PRD_NPERCA" , @lError, lAtual)
	EndIf

	If nPeriodo > 0	.And. nPeriodo > nUltPerCal
		lReturn := .F.
	EndIf

	If aAreaPRD != Nil
		::setaArea(aAreaPRD)
	EndIf
Return lReturn

/*/{Protheus.doc} reservaProduto
Reserva PRD para esta Thread
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cProduto  , caracter, codigo do produto
@param 02 - nPerInicio, numero  , retorna por referencia o periodo minimo (DE)
@param 03 - nPerMaximo, numero  , retorna por referencia o periodo maximo (ATE)
@param 04 - lForca    , logico  , forca a liberacao quando o registro esta bloqueado para outra Thread
@param 05 - nUltPerCal, numero  , retorna por referencia o ultimo periodo calculado
@param 06 - cNivel    , caracter, retorna por referencia o nivel do produto
@param 07 - nThreadsLk, numero  , retorna por referencia a Thread que estava com o lock no registro
@return lReservou, logico, indica se conseguiu realizar a reserva do produto
/*/
METHOD reservaProduto(cProduto, nPerInicio, nPerMaximo, lForca, nUltPerCal, cNivel, nThreadsLk) CLASS MrpDados

	Local aPendencias := {}
	Local aRetAux
	Local lReservou   := .F.
	Local lAtual      := cProduto == ::oProdutos:cCurrentKey
	Local lTrava      := .F.
	Local lErrorPRD   := .F.
	Local lErrorPEN   := .F.
	Local nIndAux
	Local nThreadID   := ThreadID()

	Default lForca      := .F.
	Default nThreadsLk  := 0

	nThreadsLk := ::retornaCampo("PRD", 1, cProduto, "PRD_THREAD", @lErrorPRD, lAtual)

	If nThreadsLk == nThreadID
		lReservou  := .T.

	ElseIf ((nThreadsLk <= 0) .OR. (lForca .and. nThreadsLk == 1))
		::oProdutos:trava(cProduto)
		lTrava     := .T.
		nThreadsLk := ::retornaCampo("PRD", 1 , cProduto, "PRD_THREAD", @lErrorPRD)
		If nThreadsLk <= 0 .or. (lForca .and. nThreadsLk == 1)
			::gravaCampo("PRD", 1, cProduto, "PRD_THREAD", nThreadID)

			lReservou  := .T.
		Else

			cNivel     := ::retornaCampo("PRD", 1 , cProduto, "PRD_NIVEST", @lErrorPRD)
			lReservou  := .F.
		EndIf
	EndIf

	If lReservou
		 aRetAux := ::retornaCampo("PRD", 1, cProduto, {"PRD_NPERAT", "PRD_NPERMA",;
		                                                "PRD_NPERCA", "PRD_NIVEST"}, @lErrorPRD, lTrava /*lAtual*/, , , , , .T. /*lVarios*/)

		nPerInicio := aRetAux[1]
		nPerMaximo := aRetAux[2]
		nUltPerCal := aRetAux[3]
		cNivel     := aRetAux[4]

		nPerInicio := Iif(nPerInicio < 0, 1, nPerInicio)
		nPerMaximo := Iif(nPerMaximo < 0 .OR. nPerInicio > nPerMaximo, ::oParametros["nPeriodos"], nPerMaximo)

		If lTrava
			::oProdutos:destrava(cProduto)
		EndIf

		//Analisa pendencias Pais para limitar calculo ATE
		If nPerMaximo != ::oParametros["nPeriodos"]
			//Recupera array de pendencias pai deste componente
			aPendencias := ::retornaLinha("PEN", 1, cProduto, @lErrorPEN, .T.)
			lExecuta    := .T.
			If lErrorPEN
				nPerMaximo := ::oParametros["nPeriodos"]
			Else
				//Avalia array e grava menor periodo na PRD
				For nIndAux := 1 to Len(aPendencias)
					If aPendencias[nIndAux][2] != -1
						If nPerMaximo == Nil .OR. aPendencias[nIndAux][2] < nPerMaximo
							nPerMaximo := aPendencias[nIndAux][2]
						EndIf
					EndIf
				Next
				//TODO revisar/remover
				If nPerMaximo == Nil
					nPerMaximo := ::oParametros["nPeriodos"]
					::oLogs:log("nPerMaximo := nPeriodos - _Global - " + cValToChar(ProcLine()), "26")
				EndIf
			EndIf
		EndIf
	ElseIf lTrava
		::oProdutos:destrava(cProduto)

	EndIf

Return lReservou

/*/{Protheus.doc} liberaProduto
Libera PRD para desta Thread
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cProduto, caracter, codigo do produto
@param 02 - lForca  , logico  , indica se forca a liberacao quando reservado para outra Thread
@return lLiberou, logico, indica se conseguiu liberar o produto
/*/
METHOD liberaProduto(cProduto, lForca) CLASS MrpDados

	Local lAtual      := cProduto == ::oProdutos:cCurrentKey
	Local lErrorPRD  := .F.
	Local lLiberou   := .F.

	Default lForca   := .F.

	//Nao trava registro para evitar deadlock
	If ::retornaCampo("PRD", 1, cProduto, "PRD_THREAD", @lErrorPRD, lAtual /*lAtual*/) == ThreadID();
	   .OR. lForca
		::gravaCampo("PRD", 1, cProduto, "PRD_THREAD", 0, , ,.T.)
		lLiberou   := .T.
	EndIf

Return lLiberou

/*/{Protheus.doc} gravaCampo
Grava campo na tabela
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cTabela, array   , Array com os saldos em estoque
@param 02 - nIndice, numerico, indice da tabela
@param 03 - cChave , caracter, chave do registro na tabela
@param 04 - cCampo , caracter, nome do campo
@param 05 - oValor , vari�vel, valor do campo
@param 06 - lAtual , logico  , indica se atualiza o posicionamento atual
@param 07 - lAcresc, logico  , acrescenta (soma)
@param 08 - lTrava , logico  , indica se deve travar o registro antes de gravar
@param 09 - lVarios, logico  , indica se realiza a gravacao de varios campos
@return lReturn, logico, indica se gravou o conteudo no campo
/*/
METHOD gravaCampo(cTabela, nIndice, cChave, cCampo, oValor, lAtual, lAcresc, lTrava, lVarios) CLASS MrpDados

	Local aRegistro
	Local cChaveGrv   := ""
	Local cFilAux
	Local lReturn     := .T.
	Local nAux
	Local oTabela

	Default lAtual    := .F.
	Default lAcresc   := .F.
	Default lTrava    := .F.
	Default lVarios   := .F.

	If oValor == Nil
		Return .F.
	EndIf

	If cTabela == "MAT"
		oTabela := ::oMatriz

	ElseIf cTabela == "PRD"
		oTabela := ::oProdutos

	ElseIf cTabela == "EST"
		oTabela := ::oEstruturas

	EndIf

	cFilAux   := oTabela:getFilial()

	If lAtual
		aRegistro := oTabela:aCurrentRow
		cChaveGrv := oTabela:getcKeyCurrent()
		nIndice   := oTabela:getIndice()
	Else
		cChaveGrv := cFilAux + cChave
	EndIf

	//Trava registro da tabela
	::trava(cTabela, cChaveGrv)

	If lAtual .or. oTabela:getRow(nIndice, cChaveGrv, Nil, @aRegistro, .F., lTrava )	//(cKey, nPos, aReturn, lError, lTrava)
		If lVarios
			For nAux := 1 to Len(cCampo)
				If lAcresc
					aRegistro[&("s"+cCampo[nAux])] += oValor[nAux]
				Else
					aRegistro[&("s"+cCampo[nAux])] := oValor[nAux]
				EndIf
			Next nAux
		Else
			If lAcresc
				aRegistro[&("s"+cCampo)] += oValor
			Else
				aRegistro[&("s"+cCampo)] := oValor
			EndIf
		EndIf
		oTabela:updRow(nIndice, cChaveGrv, Nil, aRegistro, .F., lTrava)
	Else
		lReturn   := .F.
	EndIf

	//Destrava registro da tabela
	::destrava(cTabela, cChaveGrv)

Return lReturn

/*/{Protheus.doc} retornaLinha
Retorna conteudo do campo na tabela
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cTabela   , array   , Array com os saldos em estoque
@param 02 - nIndice   , numerico, indice da tabela
@param 03 - cChave    , caracter, chave do registro na tabela
@param 04 - lError    , logico  , passagem por referencia para retornar erro
@param 05 - lTrava    , logico  , operacao com ou sem trava
@return oValor, vari�vel, conte�do do campo
/*/
METHOD retornaLinha(cTabela, nIndice, cChave, lError, lTrava) CLASS MrpDados

	Local aRegistro
	Local nPos      := 0
	Local oTabela
	Local oValor

	Default lError    := .F.
	Default lAtual    := .F.
	Default lPrimeiro := .F.
	Default lProximo  := .F.
	Default lSort     := .T.

	If cTabela == "MAT"
		oTabela := ::oMatriz

	ElseIf cTabela == "PRD"
		oTabela := ::oProdutos

	ElseIf cTabela == "EST"
		oTabela := ::oEstruturas

	ElseIf cTabela == "SUB"
		oTabela := ::oSubProdutos

	ElseIf cTabela == "ALT"
		oTabela := ::oAlternativos

	ElseIf cTabela == "PEN"
		oTabela := ::oPendencias

	ElseIf cTabela == "CAL"
		oTabela := ::oCalendario

	EndIf

	nPos := oTabela:getnKey(nIndice, cChave, @lError, lTrava)
	oTabela:getRow(nIndice, Nil, nPos, @aRegistro, @lError, slTravaOut)

	If !lError
		oValor    := aRegistro
	EndIf

Return oValor

/*/{Protheus.doc} gravaLinha
Grava linha na tabela
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cTabela   , array   , Array com os saldos em estoque
@param 02 - nIndice   , numerico, indice da tabela
@param 03 - cChave    , caracter, chave do registro na tabela
@param 04 - aRegistro , array   , array com os dados da linha
@param 05 - lError    , logico  , passagem por referencia para retornar erro
@param 06 - lTrava     , logico  , operacao com ou sem trava
@return oValor, vari�vel, conte�do do campo
/*/
METHOD gravaLinha(cTabela, nIndice, cChave, aRegistro, lError, lTrava, lNova) CLASS MrpDados

	Local oTabela
	Local lReturn

	Default lError    := .F.
	Default lAtual    := .F.
	Default lPrimeiro := .F.
	Default lProximo  := .F.
	Default lSort     := .T.
	Default lNova     := .F.

	If cTabela == "MAT"
		oTabela := ::oMatriz

	ElseIf cTabela == "PRD"
		oTabela := ::oProdutos

	ElseIf cTabela == "EST"
		oTabela := ::oEstruturas

	ElseIf cTabela == "SUB"
		oTabela := ::oSubProdutos

	ElseIf cTabela == "ALT"
		oTabela := ::oAlternativos

	ElseIf cTabela == "PEN"
		oTabela := ::oPendencias

	ElseIf cTabela == "CAL"
		oTabela := ::oCalendario

	EndIf

	IF lNova
		lReturn := oTabela:addRow(cChave, @aRegistro, @lError, slTravaOut)
	Else
	lReturn := oTabela:updRow(nIndice, cChave, Nil, @aRegistro, @lError, slTravaOut)
	EndIf

Return lReturn


/*/{Protheus.doc} retornaCampo
Retorna conteudo do campo na tabela
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cTabela    , array   , id da tabela
@param 02 - nIndice    , numerico, indice da tabela
@param 03 - cChave     , caracter, chave do registro na tabela
@param 04 - cCampo     , caracter, nome do campo
@param 05 - lError     , logico  , passagem por referencia para retornar erro
@param 06 - lAtual     , logico  , indica se deve retornar campo do registro
@param 07 - lPrimeiro  , logico  , indica se deve retornar campo do registro do primeiro registro do indice
@param 08 - lProximo   , logico  , indica se deve retornar campo do registro do proximo registro do indice
@param 09 - lSort      , logico  , indica se deve reordenar a tabela no indice referencia - em conjunto com ::lReordena
@param 10 - lExportacao, logico  , indica se os retornos sao sequenciais na operacao de exportacao
@param 11 - lVarios    , logico  , indica se retorna mais de um campo
@return oValor, vari�vel, conte�do do campo (ou dos campos)
/*/
METHOD retornaCampo(cTabela, nIndice, cChave, cCampo, lError, lAtual, lPrimeiro, lProximo, lSort, lExportacao, lVarios) CLASS MrpDados

	Local aRegistro
	Local cFilAux
	Local nAux
	Local nPos        := 0
	Local oValor
	Local oTabela
	Local cChaveAux

	Default lError    := .F.
	Default lAtual    := .F.
	Default lPrimeiro := .F.
	Default lProximo  := .F.
	Default lSort     := .T.
	Default lVarios   := .F.

	If cTabela == "MAT"
		oTabela := ::oMatriz

	ElseIf cTabela == "PRD"
		oTabela := ::oProdutos

	ElseIf cTabela == "EST"
		oTabela := ::oEstruturas

	ElseIf cTabela == "SUB"
		oTabela := ::oSubProdutos

	ElseIf cTabela == "ALT"
		oTabela := ::oAlternativos

	ElseIf cTabela == "OPC"
		oTabela := ::oOpcionais

	ElseIf cTabela == "CAL"
		oTabela := ::oCalendario

	EndIf

	//Reordena apos alteracao de campo chave
	If (::lReordena .AND. oTabela:getlOrder())
		oTabela:order(cTabela, nIndice, @lError)

	EndIf

	If lAtual .and. oTabela:nCurrentKey != Nil .and. oTabela:nCurrentKey > 0
		nPos      := oTabela:nCurrentKey
		aRegistro := oTabela:aCurrentRow

	ElseIf lAtual .and. oTabela:cCurrentKey != Nil .and. !Empty(oTabela:cCurrentKey)
		aRegistro := oTabela:aCurrentRow
		cChaveAux := oTabela:cCurrentKey

	ElseIf lAtual
		lPrimeiro := .T.
		lAtual    := .F.

	EndIf

	If lPrimeiro
		nPos := 1

	ElseIf lProximo
		If oTabela:nCurrentKey != Nil .and. oTabela:nCurrentKey > 0
			nPos := oTabela:nCurrentKey
			nPos := nPos + 1

		ElseIf !Empty(oTabela:cCurrentKey)
			aRegistro := oTabela:aCurrentRow
			cChaveAux := oTabela:cCurrentKey
			nPos      := oTabela:getnKey(oTabela:nIndice, cChaveAux, lError, slTravaOut)
			nPos      := nPos + 1

		Else
			nPos := 1
		EndIf

		If lExportacao
			If Empty(saMAT)
				saMAT := ::retornaLista(cTabela, @lError)
			EndIf
			aRegistro := saMAT[nPos][2]
		Else
			oTabela:getRow(nIndice, Nil, nPos, @aRegistro, @lError, slTravaOut)
		EndIf

	ElseIf !lAtual .AND. nIndice == 1
		cFilAux   := ""
		cChaveAux := cFilAux+cChave
		If (nPos == 0 .Or. nPos == Nil) .AND. Empty(cChaveAux)
			lError    := .T.
			nPos      := 0
		EndIf

	ElseIf !lAtual .AND. nIndice == 2
		cFilAux  := ""
		//TODO - Revisar utilizacao e performance, possibilidade de remover referente redundancia com Dados_Global..
		nPos     := oTabela:getnKey(nIndice, cFilAux+cChave, lError, slTravaOut)
		If nPos == 0 .Or. nPos == Nil
			lError := .T.
			nPos := 0
		Else
			cChaveAux := cFilAux+cChave
		EndIf

	EndIf

	If nPos == 0 .AND. Empty(cChaveAux)
		lError := .T.
	EndIf

	If !lError .and. Empty(aRegistro)
		If lExportacao
			If Empty(saMAT)
				saMAT := ::retornaLista(cTabela, @lError)
			EndIf
			//aRegistro := saMAT[nPos][2][1][2]
			aRegistro := saMAT[nPos][2]

		ElseIf !oTabela:getRow(nIndice, cChaveAux, nPos, @aRegistro, @lError, slTravaOut)
			lError := .T.
			//::oLogs:log(STR0024 + cTabela + " - " + cValtoChar(nPos), "EA") //"Erro ao retornar linha da tabela - linha: "
			//Comentado para fins de performance, mantido para facilitar analises de falhas
		EndIf
	EndIf

	If !lError
		If lVarios
			oValor    := {}
			For nAux := 1 to Len(cCampo)
				aAdd(oValor, aRegistro[&("s"+cCampo[nAux])])
			Next nAux
		Else
			oValor    := aRegistro[&("s"+cCampo)]
		EndIf
	EndIf

	If !lAtual
		oTabela:nCurrentKey := nPos
		oTabela:nIndice     := nIndice
		oTabela:cCurrentKey := cChaveAux
	EndIf

Return oValor

/*/{Protheus.doc} tamanhoLista
Retorna o tamanho da tabela/lista
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cTabela    , array   , id da tabela/lista
@return nTamanho, numero, tamanho da lista
/*/
METHOD tamanhoLista(cTabela)  CLASS MrpDados
	Local nTamanho
	Local oTabela

	If cTabela == "MAT"
		oTabela := ::oMatriz

	ElseIf cTabela == "PRD"
		oTabela := ::oProdutos

	ElseIf cTabela == "EST"
		oTabela := ::oEstruturas

	ElseIf cTabela == "SUB"
		oTabela := ::oSubProdutos

	ElseIf cTabela == "ALT"
		oTabela := ::oAlternativos

	ElseIf cTabela == "CAL"
		oTabela := ::oCalendario

	EndIf

	nTamanho := oTabela:getRowsNum()

Return nTamanho

/*/{Protheus.doc} tamanhoLista
Retorna a lista/tabela
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cTabela, array , id da tabela/lista
@param 02 - lError , logico, retorna ocorrencia de erro por referencia
@param 03 - lSort  , logico, indica se deve reordenar a tabela
@return aTabela, array, array com os dados da tabela/lista
/*/
METHOD retornaLista(cTabela, lError, lSort)  CLASS MrpDados

	Local aTabela := {}
	Local lHash   := .F.
	Local oTabela
	Local oHash

	Default lSort := .F.

	If cTabela == "MAT"
		oTabela := ::oMatriz

	ElseIf cTabela == "PRD"
		oTabela := ::oProdutos

	ElseIf cTabela == "EST"
		oTabela := ::oEstruturas

	ElseIf cTabela == "SUB"
		oTabela := ::oSubProdutos

	EndIf

	If Empty(saMAT)
		oTabela:getAllRow(@aTabela, @lError)

		If lHash
			oHash   := AToHM( aTabela, 1, 0 )
			aSize(aTabela, 0)
			If !lError
				lError := !HMList(oHash, @aTabela )
			EndIf
			oHash := NIL
		Else
			If lSort
				aTabela := aSort(aTabela, , , {|x,y| "|"+x[1]+"|" < "|"+y[1]+"|" })
			EndIf
		EndIf
	Else
		aTabela := saMAT
	EndIf

Return aTabela

/*/ {Protheus.doc} IsComponente
Verifica se o produto e um componente
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cProduto, caracter, codigo do produto
@return     lIsComponente, logico, indica se e um componente
/*/
METHOD isComponente(cProduto) CLASS MrpDados
	Local cFilAux := ::oEstruturas:getFilial(Nil, Nil)												//lError, lTrava
	Local lIsComponente := ::oEstruturas:getFlag("lIsComponente" + cFilAux + cProduto, Nil, Nil)	//(cChave, lError, lTrava)
	If lIsComponente == Nil
		lIsComponente := .F.
	EndIf
Return lIsComponente

/*/ {Protheus.doc} possuiEstrutura
Verifica se o produto possui estrutura
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cProduto, caracter, codigo do produto
@return     lReturn, logico, indica se possui estrutura
/*/
METHOD possuiEstrutura(cProduto) CLASS MrpDados
	Local lReturn := .F.
	Local lError  := .F.
	If ::oEstruturas:getnKey(1, cProduto, @lError) > 0 .AND. !lError
		lReturn := .T.
	EndIf
Return lReturn

/*/{Protheus.doc} retornaArea
Retorna a area atual do alias especifico
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cTabela, array   , Array com os saldos em estoque
@return aResultados, array, Array com os resultados do MAT {Data, Produto, Necessidade, Saida Estrutura}
/*/
METHOD retornaArea(cTabela) CLASS MrpDados

	Local aReturn   := Array(5)
	Local oTabela

	If cTabela == "MAT"
		oTabela := ::oMatriz

	ElseIf cTabela == "PRD"
		oTabela := ::oProdutos

	ElseIf cTabela == "EST"
		oTabela := ::oEstruturas

	ElseIf cTabela == "SUB"
		oTabela := ::oSubProdutos

	EndIf

	aReturn[1] := cTabela
	aReturn[2] := oTabela:nIndice
	aReturn[3] := oTabela:nCurrentKey
	aReturn[4] := oTabela:aCurrentRow
	aReturn[5] := oTabela:cCurrentKey

Return aClone(aReturn)

/*/{Protheus.doc} setaArea
Restaura uma area atual
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - aArea, array   , Array com area a ser restaurada
/*/
METHOD setaArea(aArea) CLASS MrpDados

	Local cTabela    := aArea[1]
	Local oTabela

	If cTabela == "MAT"
		oTabela := ::oMatriz

	ElseIf cTabela == "PRD"
		oTabela := ::oProdutos

	ElseIf cTabela == "EST"
		oTabela := ::oEstruturas

	ElseIf cTabela == "SUB"
		oTabela := ::oSubProdutos

	EndIf

	oTabela:nIndice     := aArea[2]
	oTabela:nCurrentKey := aArea[3]
	oTabela:aCurrentRow := aArea[4]
	oTabela:cCurrentKey := aArea[5]

Return

/*/{Protheus.doc} destruir
Destroi os objetos e variaveis da camada de dados
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - aArea, array   , Array com area a ser restaurada
/*/
METHOD destruir() CLASS MrpDados

	Local aSessoes

	::oAlternativos:destroy()
	::oEstruturas:destroy()
	::oSubProdutos:destroy()
	::oMatriz:destroy()
	::oLiveLock:destroy()
	::oJsonOpcionais:destroy()
	::oOpcionais:destroy()
	::oPendencias:destroy()
	::oProdutos:destroy()
	::oCalendario:destroy()

	If ::oParametros["lRastreia"]
		::oRastreio:destruir()
	EndIf

	saMAT := Nil

	//Delega Limpeza de Mem�ria
	StartJob("PCPCleanVr", GetEnvServer(), .F., ::oParametros["cChaveExec"], ::oParametros["ticket"], ::oParametros["lAguardaDescarga"])

	//Aguarda sempre elimina��o ao menos desta trabalha para evitar intertravamento.
	While VarIsUID("UIDs_PCPMRP")
		Sleep(200)
		VarGetXA( "UIDs_PCPMRP", @aSessoes)
		If Empty(aSessoes)
			Exit
		Else
			aSize(aSessoes, 0)
		EndIf
	EndDo

Return

/*/{Protheus.doc} trava
Trava o registro da tabela
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cTabela, caracter, id da tabela
@param 02 - cChave , caracter, chave da tabela no indice primario
@return lRet, logico, indica se conseguiu travar o registro
/*/
METHOD trava(cTabela, cChave) CLASS  MrpDados
	Local oTabela
	Local lRet      := .F.

	Default lReloadAtual := .T.

	If cTabela == "MAT"
		oTabela := ::oMatriz

	ElseIf cTabela == "PRD"
		oTabela := ::oProdutos

	ElseIf cTabela == "EST"
		oTabela := ::oEstruturas

	ElseIf cTabela == "SUB"
		oTabela := ::oSubProdutos

	EndIf

	cFilAux := oTabela:getFilial()
	lRet    := oTabela:trava( cFilAux + cChave )

Return lRet

/*/{Protheus.doc} destrava
Destrava o registro da tabela
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cTabela, caracter, id da tabela
@param 02 - cChave , caracter, chave da tabela no indice primario
@return lRet, logico, indica se conseguiu destravar o registro
/*/
METHOD destrava(cTabela, cChave) CLASS  MrpDados
	Local oTabela
	Local lRet      := .F.
	Local cFilAux

	If cTabela == "MAT"
		oTabela := ::oMatriz

	ElseIf cTabela == "PRD"
		oTabela := ::oProdutos

	ElseIf cTabela == "EST"
		oTabela := ::oEstruturas

	ElseIf cTabela == "SUB"
		oTabela := ::oSubProdutos

	EndIf

	cFilAux := oTabela:getFilial()
	lRet    := oTabela:destrava( cFilAux + cChave )

Return lRet

/*/{Protheus.doc} existeMatriz
Verifica se o produto existe na Matriz
@author brunno.costa
@since 25/04/2019
@version 1.0
@param 01 - cProduto , caracter, codigo do produto ou codigo do produto + cIDOpc
@param 02 - nPeriodo , numero  , periodo
@param 03 - cChaveAux, caracter, chave
@return lPossuiMAT, logico, indica se o produto existe na matriz no periodo (ou se a chave existe na matriz)
/*/
METHOD existeMatriz(cProduto, nPeriodo, cChaveAux, cIDOpc) CLASS MrpDados
	Local cFilAux    := ""
	Local lError     := .F.
	Local lPossuiMAT
	Local lErrorMAT  := .F.

	Default cProduto   := ""
	Default cIDOpc     := ""
	Default cChaveAux  := cFilAux + Iif(cProduto == Nil, "",cProduto + Iif(Empty(cIDOpc), "","|" + cIDOpc))

	If nPeriodo == Nil
		lPossuiMAT := ::oMatriz:getFlag("cExistMAT_" + cChaveAux, @lError, slTravaOut)
		If lPossuiMAT == Nil .OR. lError
			lPossuiMAT := .F.
		EndIf
	Else
		cChaveAux := DtoS(::aPeriodos[nPeriodo]) + cProduto + Iif(Empty(cIDOpc), "","|" + cIDOpc)
		::retornaCampo("MAT"       /*cTabela*/, 1 /*nIndice*/, cChaveAux /*cChave*/,;
		              "MAT_SALDO"  /*cCampo*/, @lErrorMAT /*lError*/)
		lPossuiMAT := !lErrorMAT
	EndIf
Return lPossuiMAT


/*/{Protheus.doc} setAPeriodos
Seta propriedade aPeriodos
@author marcelo.neumann
@since 25/06/2019
@version 1.0
@param 01 - aPeriodos, array, array com as datas dos periodos de processamento
@return Nil
/*/
METHOD setAPeriodos(aPeriodos) CLASS MrpDados

	::aPeriodos := aPeriodos

Return

/*/{Protheus.doc} setoDominio
Seta propriedade oDominio
@author brunno.costa
@since 08/07/2019
@version 1.0
@param 01 - aPeriodos, array, array com as datas dos periodos de processamento
@return Nil
/*/
METHOD setoDominio(oDominio) CLASS MrpDados

	::oDominio := oDominio

Return

/*/{Protheus.doc} decrementaTotalizador
Decrementa o totalizador relacionado analise de conclusao do loopNiveis
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto  , caracter, codigo do produto / chave do produto
@return nSobraPos, numero, retorna o saldo apos consumir o produto
/*/
METHOD decrementaTotalizador(cProduto) CLASS MrpDados

	Local aAreaPRD
	Local cNivelPrd
	Local lErrorPRD := .F.
	Local lAtual    := cProduto == ::oProdutos:cCurrentKey

	//Atualiza controles de processamento da loopNiveis
	If !lAtual
		aAreaPRD  := ::retornaArea("PRD")
	EndIf

	cNivelPrd := ::retornaCampo("PRD", 1, cProduto, "PRD_NIVEST", @lErrorPRD, .F. /*lAtual*/)
	cNivelPrd := Iif(cNivelPrd == Nil, "99", cNivelPrd)

	::oProdutos:setflag("nProdCalcN"  + cNivelPrd, -1, .F., .T., .T.) //Decrementa
	::oProdutos:setflag("nProdCalcT"             , -1, .F., .T., .T.) //Decrementa

	::oLogs:log(STR0048 + cNivelPrd + STR0049 + AllTrim(cProduto), "42") //"Decremento nProcessNv no nivel " xx ", produto "

	If !lAtual
		::setaArea(aAreaPRD)
	EndIf

Return

/*/{Protheus.doc} incrementaTotalizador
Incrementa o totalizador relacionado analise de conclusao do loopNiveis
@author    brunno.costa
@since     25/04/2019
@version 1.0
@param 01 - cProduto  , caracter, codigo do produto / chave do produto
@return nSobraPos, numero, retorna o saldo apos consumir o produto
/*/
METHOD incrementaTotalizador(cProduto, lProcess, lCalculado) CLASS MrpDados

	Local aAreaPRD
	Local cNivelPrd
	Local lErrorPRD := .F.
	Local lAtual    := cProduto == ::oProdutos:cCurrentKey

	Default lProcess := .F.

	//Atualiza controles de processamento da loopNiveis
	If !lAtual
		aAreaPRD  := ::retornaArea("PRD")
	EndIf

	cNivelPrd := ::retornaCampo("PRD", 1, cProduto, "PRD_NIVEST", @lErrorPRD, lAtual /*lAtual*/)
	cNivelPrd := Iif(cNivelPrd == Nil, "99", cNivelPrd)

	If lProcess
		::oProdutos:setflag("nProcessNv" + cNivelPrd, +1, .F., .T., .T.)
		::oProdutos:setflag("nProcess"              , +1, .F., .T., .T.)
		::oLogs:log(STR0050 + Iif(cNivelPrd == Nil, "'vazio'", cNivelPrd) + STR0049 + AllTrim(cProduto), "42") //"Incremento controle LoopNiveis delegacoes processadas (nProcessNv) no nivel " xx ", produto: "
	EndIf

	If lCalculado
		::oProdutos:setflag("nProdCalcN"  + cNivelPrd, +1, .F., .T., .T.)
		::oProdutos:setflag("nProdCalcT"             , +1, .F., .T., .T.)
		::oLogs:log(STR0051 + cNivelPrd + STR0049 + AllTrim(cProduto), "42") //"Incremento controle LoopNiveis produtos calculados (nProdCalcT) no nivel " xx ", produto: "
	EndIf

	If !lAtual
		::setaArea(aAreaPRD)
	EndIf

Return

/*/{Protheus.doc} posicaoCampo
Retorna a posicao do campo na tabela
@author    brunno.costa
@since     08/07/2019
@version 1.0
@param 01 - cCampo  , caracter, identificador do campo
@return nSobraPos, numero, posicao do  campo na tabela
/*/
METHOD posicaoCampo(cCampo) CLASS MrpDados
Return &("s" + cCampo)

/*/{Protheus.doc} PCPCleanVr
Chama VarClean
@author    brunno.costa
@since     25/04/2019
@version 1.0
/*/
Function PCPCleanVr(cChaveExec, cTicket, lAguardaDescarga)

	Local aSessoes  := {}
	Local cGlobalKey
	Local lPassou    := .F.
	Local lOk        := .T.
	Local nInd
	Local oStatus    := MrpDados_Status():New(cTicket)

	saMAT := Nil

	If soDominio == Nil
		soDominio := MRPPrepDom()
	EndIf

	//Protege limpeza de mem�ria
	VarBeginT( "UIDs_PCPMRP", "UIDs_PCPMRP" )

	//Elimina residuos de variaveis globais
	If VarIsUID("UIDs_PCPMRP")
		VarGetXA( "UIDs_PCPMRP", @aSessoes)

		soDominio:oDados:trava("MAT", "UIDs_PCPMRP")
		VarEndT( "UIDs_PCPMRP", "UIDs_PCPMRP" )

		If VarIsUID("UIDs_PCPMRP") .AND. !VarClean("UIDs_PCPMRP")

			soDominio:oLogs:log("MrpData_Global -VarClean(UIDs_PCPMRP) FALSE", "E")
			StartJob("PCPCleanVr", GetEnvServer(), .F., cChaveExec, cTicket, lAguardaDescarga)

			//Libera trecho de limpeza de mem�ria
			soDominio:oDados:destrava("MAT", "UIDs_PCPMRP")

			Return
		EndIf

		lPassou    := .T.

		//Libera trecho de limpeza de mem�ria
		soDominio:oDados:destrava("MAT", "UIDs_PCPMRP")

		For nInd := 1 to Len(aSessoes)
			cGlobalKey := aSessoes[nInd][1]
			If (cChaveExec $ cGlobalKey .OR. oStatus:getStatus("status") != "4") .AND. VarIsUID( cGlobalKey )
				If  VarBeginT( cGlobalKey + "_LOCK", cGlobalKey ) .AND. VarIsUID( cGlobalKey )
					If !VarClean( cGlobalKey )
						soDominio:oLogs:log("MrpData_Global - VarClean( cGlobalKey ) FALSE: '" + AllTrim(cGlobalKey) + "'", "E")
					EndIf
					VarEndT( cGlobalKey + "_LOCK", cGlobalKey )
				EndIf
			EndIf
		Next nInd
	EndIf

	//Libera trecho de limpeza de mem�ria
	If !lPassou
		VarEndT( "UIDs_PCPMRP", "UIDs_PCPMRP" )
	EndIf

	If VarBeginT( cChaveExec + "_LOCK", cChaveExec ) .AND. VarIsUID(cChaveExec)
		If !VarClean(cChaveExec)
			soDominio:oLogs:log("MrpData_Global - VarClean(cChaveExec) FALSE: '" + AllTrim(cChaveExec) + "'", "E")
		EndIf
	EndIf
	VarEndT( cChaveExec + "_LOCK", cChaveExec )

	If !lAguardaDescarga .AND. soDominio != Nil
		If oStatus:getStatus("memoria") <> "9"
			oStatus:setStatus("memoria", "4") //Descarregado
		EndIf
		oStatus:persistir(soDominio:oDados)
		//Aguarda finaliza��o ou cancelamento

		While oStatus:getStatus("finalizado") != "true" .And. oStatus:getStatus("status", @lOk) != "4"
			If !lOk
				Exit
			EndIf
			Sleep(50)
		End
		oStatus:Destruir()
	EndIf

Return
