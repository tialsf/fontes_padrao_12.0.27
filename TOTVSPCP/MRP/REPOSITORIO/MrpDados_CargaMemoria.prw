#INCLUDE "TOTVS.CH"
#INCLUDE 'MrpDados.ch'

Static soDominio
Static soDados
Static snTamCod      := 90
Static _lBloqueio    := Nil

Static sPRD_FILIAL   := 1
Static sPRD_COD      := 2
Static sPRD_ESTSEG   := 3
Static sPRD_LE       := 4 //Lote econ�mico.
Static sPRD_PE       := 5
Static sPRD_SLDDIS   := 6
Static sPRD_NIVEST   := 7
Static sPRD_CHAVE2   := 8
Static sPRD_NPERAT   := 9
Static sPRD_NPERMA   := 10 //Ultimo periodo permitido calcular - limitacao de bloqueio
Static sPRD_THREAD   := 11
Static sPRD_NPERCA   := 12 //Ultimo periodo calculado
Static sPRD_REINIC   := 13
Static sPRD_IDOPC    := 14
Static sPRD_HORFIR   := 15
Static sPRD_TPHOFI   := 16
Static sPRD_DTHOFI   := 17
Static sPRD_TIPE     := 18
Static sPRD_PPED     := 19
Static sPRD_REVATU   := 20
Static sPRD_TIPDEC   := 21
Static sPRD_NUMDEC   := 22
Static SPRD_ROTEIR   := 23
Static sPRD_QTEMB    := 24 //Qtd. Embalagem
Static sPRD_LM       := 25 //Lote M�nimo
Static sPRD_TOLER    := 26 //Toler�ncia
Static sPRD_TIPO     := 27
Static sPRD_GRUPO    := 28
Static sPRD_RASTRO   := 29
Static sPRD_MRP      := 30
Static sPRD_EMAX     := 31
Static sPRD_PROSBP   := 32
Static sPRD_LOTSBP   := 33
Static sPRD_ESTORI   := 34
Static sPRD_APROPR   := 35
Static sPRD_LOTVNC   := 36
Static sPRD_CPOTEN   := 37
Static sPRD_BLOQUE   := 38
Static sPRD_LSUBPR   := 39
Static sTAM_PRD      := 39

Static sEST_FILIAL   := 1
Static sEST_CODPAI   := 2
Static sEST_CODFIL   := 3
Static sEST_QTD      := 4
Static sEST_FANT     := 5
Static sEST_TRT      := 6
Static sEST_GRPOPC   := 7
Static sEST_ITEOPC   := 8
Static sEST_VLDINI   := 9
Static sEST_VLDFIM   := 10
Static sEST_REVINI   := 11
Static sEST_REVFIM   := 12
Static sEST_ALTERN   := 13
Static sEST_FIXA	 := 14
Static sEST_POTEN    := 15
Static sEST_PERDA    := 16
Static sEST_QTDB     := 17
Static sEST_OPERA    := 18
Static sTAM_EST      := 18

Static sMAT_FILIAL   := 1
Static sMAT_DATA     := 2
Static sMAT_PRODUT   := 3
Static sMAT_SLDINI   := 4
Static sMAT_ENTPRE   := 5
Static sMAT_SAIPRE   := 6
Static sMAT_SAIEST   := 7
Static sMAT_SALDO    := 8
Static sMAT_NECESS   := 9
Static sMAT_TPREPO   := 10
Static sMAT_EXPLOD   := 11
Static sMAT_THREAD   := 12
Static sMAT_IDOPC    := 13
Static sMAT_DTINI    := 14
Static sTAM_MAT      := 14

Static sALT_FILIAL   := 1
Static sALT_PRODUT   := 2
Static sALT_ALTERT   := 3
Static sALT_FATOR    := 4
Static sALT_TPFAT    := 5
Static sALT_ORDEM    := 6
Static sALT_DATA     := 7
Static sTAM_ALT      := 7

Static sOPC_KEY      := 1
Static sOPC_KEY2     := 2
Static sOPC_OPCION   := 3
Static sOPC_ID       := 4
Static sOPC_IDPAI    := 5
Static sOPC_IDMASTER := 6
Static sOPC_T4JRECNO := 7
Static sOPC_T4QRECNO := 8
Static sTAM_OPC      := 8

Static sCAL_FILIAL   := 1
Static sCAL_DATA     := 2
Static sCAL_HRINI    := 3
Static sCAL_HRFIM    := 4
Static sCAL_INTER    := 5
Static sCAL_UTEIS    := 6
Static sTAM_CAL      := 6

Static sVDP_FILIAL   := 1
Static sVDP_PROD     := 2
Static sVDP_DTINI    := 3
Static sVDP_DTFIN    := 4
Static sVDP_QNTDE    := 5
Static sVDP_QNTATE   := 6
Static sVDP_REV      := 7
Static sVDP_ROTEIRO  := 8
Static sVDP_LOCAL    := 9
Static sVDP_CODIGO   := 10
Static sTAM_VDP      := 10

Static sOPE_ROTE  := 1
Static sOPE_OPERA := 2
Static sTAM_OPE   := 2

/*/{Protheus.doc} MrpDados_CargaMemoria
Classe para carregar os dados em memoria
@author marcelo.neumann
@since 08/07/2019
@version 1
/*/
CLASS MrpDados_CargaMemoria FROM LongClassName

	DATA oDados AS OBJECT
	DATA oLogs  AS OBJECT

	METHOD new() CONSTRUCTOR

	METHOD calendario()
	METHOD carregaRegistros()
	METHOD demandas()
	METHOD entradasEmpenho()
	METHOD entradasOP()
	METHOD entradasSC()
	METHOD entradasPC()
	METHOD estruturas()
	METHOD subproduto()
	METHOD preCarga()
	METHOD produtos()
	METHOD registraProcessados()
	METHOD saidasPrevistas()
	METHOD versaoDaProducao()

EndCLASS

/*/{Protheus.doc} new
M�todo construtor da classe MrpDados_CargaMemoria
@author marcelo.neumann
@since 08/07/2019
@version 1
@param 01 - oDados, objeto, objeto da camada de dados
/*/
METHOD new(oDados) CLASS MrpDados_CargaMemoria

	//Atribui propriedades
	::oDados  := oDados
	::oLogs   := oDados:oLogs

	soDominio := Nil
	soDados   := Nil

Return Self

/*/{Protheus.doc} preCarga
M�todo construtor da classe MrpDados_CargaMemoria
@author marcelo.neumann
@since 08/07/2019
@version 1
/*/
METHOD preCarga() CLASS MrpDados_CargaMemoria

	//Grava variaveis globais com os dados para popular as Statics das Threads
	PutGlbVars("PCP_aParam", {::oDados:oParametros:toJson(), {}, {}, "2"})

	::calendario()
	::estruturas()
	::versaoDaProducao()
	::subproduto()

Return Self

/*/{Protheus.doc} carregaRegistros
Realiza a carga inicial dos registros
@author marcelo.neumann
@since 08/07/2019
@version 1.0
/*/
METHOD carregaRegistros() CLASS MrpDados_CargaMemoria

	Local aDatas      := {}
	Local cJson       := ""
	Local lEST_Ok     := .F.
	Local lSUB_Ok     := .F.
	Local lMAT_OP_Ok  := .F.
	Local lMAT_Em_Ok  := .F.
	Local lMAT_PC_Ok  := .F.
	Local lMAT_SC_Ok  := .F.
	Local lMAT_Dem_Ok := .F.
	//Local lMAT_Pr_Ok  := .F.
	Local lPRD_Ok     := .F.
	Local lVDP_Ok     := .F.
	Local lError      := .F.
	Local nInd        := 0
	Local oParametros := ::oDados:oParametros
	Local oStatus     := MrpDados_Status():New(::oDados:oParametros["ticket"])
	Local aNames      := oParametros:GetNames()
	Local nTempoIni   := MicroSeconds()

	::oLogs:log("carregaRegistros;inicio;" + Time(), "41")

	//Ajusta Data para padr�o do JSON
	For nInd := 1 to Len(aNames)
		If ValType(oParametros[aNames[nInd]]) == "D"
			aAdd(aDatas, aNames[nInd])
			oParametros[aNames[nInd]] := convDate(oParametros[aNames[nInd]])
		EndIf
	Next
	cJson := oParametros:toJson()

	//Retorna Data para padr�o de oParametros
	For nInd := 1 to Len(aDatas)
		oParametros[aDatas[nInd]] := StoD(StrTran(oParametros[aDatas[nInd]], "-", ""))
	Next

	//Grava variaveis globais com os dados para popular as Statics das Threads
	PutGlbVars("PCP_aParam", {cJson, ::oDados:aPeriodos, aDatas, "3"})

	//Inicia carga dos dados em memoria
	::produtos()
	::demandas()
	::entradasOP()
	::entradasEmpenho()
	::entradasSC()
	::entradasPC()

	//Efetua Carga Inicial no Controle de Seletivo de Produtos
	Self:oDados:oDominio:oSeletivos:loadProdutosValidos()

	While !lPRD_Ok    .Or. ;
	      !lVDP_Ok    .Or. ;
	      !lEST_Ok    .Or. ;
	      !lSUB_Ok    .Or. ;
		  !lMAT_Dem_Ok.Or. ;
	      !lMAT_OP_Ok .Or. ;
	      !lMAT_Em_Ok .Or. ;
	      !lMAT_SC_Ok

		If !lPRD_Ok
			If ::oDados:oProdutos:getFlag("termino_carga", @lError) == "S"
				lPRD_Ok := .T.
			EndIf
		EndIf

		If !lVDP_Ok
			lVDP_Ok    := ::oDados:oVersaoDaProducao:getFlag("termino_carga_VDP", @lError) == "S"
		EndIf

		If !lEST_Ok
			lEST_Ok    := ::oDados:oEstruturas:getFlag("termino_carga", @lError) == "S"
		EndIf

		If !lSUB_Ok
			lSUB_Ok    := ::oDados:oSubProdutos:getFlag("termino_carga", @lError) == "S"
		EndIf

		If !lMAT_OP_Ok
			lMAT_OP_Ok := ::oDados:oMatriz:getFlag("termino_carga_OP", @lError) == "S"
		EndIf

		If !lMAT_Em_Ok
			lMAT_Em_Ok := ::oDados:oMatriz:getFlag("termino_carga_Emp", @lError) == "S"
		EndIf

		If !lMAT_SC_Ok
			lMAT_SC_Ok := ::oDados:oMatriz:getFlag("termino_carga_SC", @lError) == "S"
		EndIf

		If !lMAT_PC_Ok
			lMAT_PC_Ok := ::oDados:oMatriz:getFlag("termino_carga_PC", @lError) == "S"
		EndIf

		If !lMAT_Dem_Ok
			lMAT_Dem_Ok := ::oDados:oMatriz:getFlag("termino_carga_saida", @lError) == "S"
		EndIf

		//If !lMAT_Pr_Ok
		//	lMAT_Pr_Ok := ::oDados:oMatriz:getFlag("termino_carga_prevista", @lError) == "S"
		//EndIf

		Sleep(50)
	EndDo

	//Limpa Mem�ria Referente STR IN de Seletivos
	Self:oDados:oDominio:oSeletivos:limpaInMemoria()

	//Se est� carregando apenas os movimentos, corrige execu��o para inserir produtos opcionais
	::oDados:oDominio:oOpcionais:insereProdutosOpcionais()
	If oStatus:getStatus("status") != "4"    //Checa cancelamento
		::oDados:oProdutos:order(2, @lError) //Reordena
	EndIf

	If ::oDados:oParametros['lAnalisaMemoriaPosCarga']
		::oDados:oProdutos:analiseMemoria(::oDados:oParametros['lAnalisaMemoriaSplit'], STR0070) //"An�lise de Mem�ria Ap�s Carga Mem�ria"
	EndIf

	::oLogs:log("carregaRegistros;fim;" + Time(), "41")
	::oLogs:log(STR0080 + cValToChar(MicroSeconds() - nTempoIni), "41") //"Tempo total da carga "

Return

/*/{Protheus.doc} calendario
Carrega dados na tabela de Calend�rio (CAL)
@author marcelo.neumann
@since 08/07/2019
@version 1.0
/*/
METHOD calendario() CLASS MrpDados_CargaMemoria

	soDados := ::oDados
	PCPCargaCa(Self)

Return

/*/{Protheus.doc} PCPCargaCa
Processa a carga dos dados do calend�rio MRP
@type Function
@author marcelo.neumann
@since 08/10/2019
@version P12.1.28
@param oSelf, objeto, refer�ncia da classe Calendario
@return Nil
/*/
Function PCPCargaCa(oSelf)

	Local nTempoIni   := MicroSeconds()
	Local aAux        := {}
	Local aFields     := {{"HW0_DATA"  , "D", "HW0", ""     }, ;
	                      {"HW0_HRINI" , "C", "HW0", ""     }, ;
	                      {"HW0_HRFIM" , "C", "HW0", ""     }, ;
	                      {"HW0_INTERV", "C", "HW0", ""     }, ;
	                      {"HW0_TOTH"  , "C", "HW0", "00:00"}}
	Local aRegEmJson  := {}
	Local cFilAux     := ""
	Local cWhere 	  := ""
	Local lCargaCSV   := oSelf:oDados:oParametros["lCargaCalendarioCSV"]
	Local nIndRegs    := 1
	Local nTotal      := 0
	Local oCalendario := oSelf:oDados:oCalendario
	Local oLogs       := oSelf:oDados:oLogs
	Local oStatus     := MrpDados_Status():New(oSelf:oDados:oParametros["ticket"])

	//Busca os registros a serem processados (via CSV ou tabela)
	If lCargaCSV
		cWhere     := "aRegistro[1] >= '" + DToS(soDados:oParametros["dDataIni"]) + "'"
		aRegEmJson := CsvToJson("calendario.csv", aFields, cWhere)
	Else
		If PreparaAmb()
			cWhere     := "HW0.HW0_DATA >= '" + DToS(soDados:oParametros["dDataIni"]) + "'"
			cWhere     += " AND HW0.HW0_FILIAL = '" + xFilial("HW0") + "' "
			aRegEmJson := GetRegDB("HW0", , cWhere, aFields)
		EndIf
	EndIf

	nTotal := Len(aRegEmJson)
	oCalendario:setFlag("qtd_registros_total", nTotal, , , .T.)
	oCalendario:setFlag("qtd_registros_lidos", 0     , , , .T.)

	If nTotal > 0
		aAux := Array(sTAM_CAL)
	EndIf

	For nIndRegs := 1 To nTotal
		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, oSelf:oDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
			oCalendario:setFlag("termino_carga", "S")
			Exit
		EndIf

		oRegistros := JsonObject():new()
		oRegistros:fromJSON(aRegEmJson[nIndRegs])
		cChave     := cFilAux + oRegistros["aRegs"][1]["HW0_DATA"]

		//Guarda o registro na Matriz
		If !oCalendario:getRow(1, cChave)
			aAux[sCAL_FILIAL] := cFilAux
			aAux[sCAL_DATA]   := StoD(oRegistros["aRegs"][1]["HW0_DATA"])
			aAux[sCAL_HRINI]  := oRegistros["aRegs"][1]["HW0_HRINI"]
			aAux[sCAL_HRFIM]  := oRegistros["aRegs"][1]["HW0_HRFIM"]
			aAux[sCAL_INTER]  := oRegistros["aRegs"][1]["HW0_INTERV"]
			aAux[sCAL_UTEIS]  := oRegistros["aRegs"][1]["HW0_TOTH"]

			If !oCalendario:addRow(cChave, aAux)
				If oStatus:getStatus("status") == "4"
					Exit
				Else
					oLogs:log(STR0055 + "HW0", "E") //"Erro na carga de dados da tabela para memoria: "
				EndIf
			EndIf
		EndIf

		FreeObj(oRegistros)
		oRegistros := Nil

		oCalendario:setFlag("qtd_registros_lidos", 1, , , .T.)
	Next nIndRegs

	oLogs:log(STR0046 + cValToChar(MicroSeconds() - nTempoIni)            , "CM") //"Tempo de carga tabela Calend�rio "
	oLogs:logb({|| STR0047 + cValToChar(oSelf:oDados:tamanhoLista("CAL"))}, "CM") //"Quantidade de dias (calend�rio) carregados em memoria: "

	//Grava flag de conclus�o global
	oCalendario:setFlag("termino_carga", "S")

	//Limpa os arrays da mem�ria
	aSize(aAux, 0)
	aAux := Nil
	aSize(aFields, 0)
	aFields := Nil
	aSize(aRegEmJson, 0)
	aRegEmJson := Nil

Return

/*/{Protheus.doc} registraProcessados
Funcao respons�vel por atualizar os status dos registros que foram processados
@author    marcelo.neumann
@since     08/07/2019
@version 1.0
/*/
METHOD registraProcessados() CLASS MrpDados_CargaMemoria
	Local cWhere  := ""
	Local cUpdate := ""
	Local lNrMrp  := .F.
	//Verifica se a carga foi feita via banco
	If !::oDados:oParametros["lCargaDemandasCSV"]
		If PreparaAmb()
			dbSelectArea("T4J")
			If FieldPos("T4J_NRMRP") > 0
				lNrMrp := .T.
			EndIf
			cWhere  := " T4J_FILIAL   = '" + xFilial("T4J") + "' "
			cWhere  += " AND T4J_PROC = '3'"
			cUpdate := IIF(lNrMrp,"T4J_PROC = '1', T4J_NRMRP = '" + ::oDados:oParametros["ticket"] + "'", "T4J_PROC = '1'")
			If !UpdRegsDB("T4J",   ; //UPDATE (tabela a ser buscada)
			              cUpdate, ; //SET    (campos a serem atualizados)
			              cWhere)   //WHERE  (filtro a ser aplicado)

				::oDados:oLogs:log(STR0057 + TCSQLError(), "E") //"Erro ao atualizar os registros que foram processados: "
			EndIf
		EndIf
	EndIf

Return

/*/{Protheus.doc} demandas
Carrega dados na tabela de Movimenta��es (MAT) com as Saidas Firmes
@author marcelo.neumann
@since 11/07/2019
@version 1.0
/*/
METHOD demandas() CLASS MrpDados_CargaMemoria

	//Verifica se a carga ser� multi-thread
	If ::oDados:oParametros["nThreads"] >= 1 .And. ::oDados:oParametros["cAutomacao"] != "2"
		PCPIPCGO(::oDados:oParametros["cSemaforoThreads"], .F., "PCPCargDem", ::oDados:oParametros["lCargaDemandasCSV"]) //Delega para processamento em Thread
	Else
		PCPCargDem(::oDados:oParametros["lCargaDemandasCSV"]) //Execucao single-thread
	EndIf

Return

/*/{Protheus.doc} PCPCargDem
Processa a carga dos dados das demandas (Sa�das Firmes)
@author marcelo.neumann
@since 11/07/2019
@version 1.0
@param 01 lCargaCSV, l�gico, indica se a carga ser� feita via arquivo CSV ou via tabela
/*/
Function PCPCargDem(lCargaCSV)

	Local nTempoIni  := MicroSeconds()
	Local aAux       := {}
	Local aFields    := {{"T4J_PROD"  , "C", "T4J", ""}, ;
						 {"T4J_DATA"  , "D", "T4J", ""}, ;
						 {"T4J_QUANT" , "N", "T4J", ""}, ;
						 {"T4J_ORIGEM", "C", "T4J", ""}, ;
						 {"T4J_MOPC"  , "O", "T4J", ""}, ;
						 {"T4J_IDREG" , "C", "T4J", ""}, ;
						 {"R_E_C_N_O_", "N", "T4J", 0}}
	Local aPeriodos  := {}
	Local aRegEmJson := {}
	Local aRegistro  := {}
	Local cChave     := ""
	Local cChaveProd := ""
	Local cError     := ""
	Local cFilAux    := ""
	Local cOpcional  := ""
	Local cProduto   := ""
	Local cWhere     := ""
	Local dEntrega
	Local dOriginal
	Local dIniDemand
	Local dFimDemand
	Local dLimite
	Local nIndRegs   := 1
	Local nTotal     := 0
	Local nPeriodo   := 0
	Local oLogs
	Local oMatriz
	Local oOpcionais
	Local oRastreio
	Local oRegistros
	Local oStatus
	Local lT4JCODE

	//Prepara o ambiente (quando for uma nova thread)
	PrepStatics()
	oMatriz    := soDados:oMatriz
	oLogs      := soDados:oLogs
	oOpcionais := soDominio:oOpcionais
	aPeriodos  := soDados:aPeriodos
	oRastreio  := soDominio:oRastreio
	dIniDemand := Iif(soDados:oParametros["dInicioDemandas"] == Nil, aPeriodos[1], soDados:oParametros["dInicioDemandas"])
	dFimDemand := Iif(soDados:oParametros["dFimDemandas"] == Nil   , DataLimite(), soDados:oParametros["dFimDemandas"])
	oStatus    := MrpDados_Status():New(soDados:oParametros["ticket"])

	lT4JCODE := !Empty(GetSx3Cache("T4J_CODE" ,"X3_TAMANHO"))
	If lT4JCODE
		aAdd(aFields, {"T4J_CODE", "C", "T4J", ""})
	EndIf

	If soDominio:oParametros["lRastreia"]
		soDominio:oAglutina:aguardaCargaEstrutura()
	EndIf

	//Corrige data fim demandas superior ao �ltimo periodo
	If dFimDemand > soDominio:oPeriodos:ultimaDataDoMRP()
		If soDominio:oParametros["lEventLog"]
			dLimite := soDominio:oPeriodos:ultimaDataDoMRP()
		Else
			dFimDemand := soDominio:oPeriodos:ultimaDataDoMRP()
		EndIf
	EndIf

	//Busca os registros a serem processados (via CSV ou tabela)
	If lCargaCSV
		cWhere     := "aRegistro[2] >= '" + DtoS(dIniDemand) + "' .And. aRegistro[2] <= '" + DtoS(dFimDemand) + "'"
		aRegEmJson := CsvToJson("demandas.csv", aFields, cWhere)
	Else
		If PreparaAmb()
			cWhere := " T4J_DATA >= '" + DtoS(dIniDemand) + "'"
			cWhere += " AND T4J_DATA <= '" + DtoS(dFimDemand) + "'"
			cWhere += " AND T4J_FILIAL = '" + xFilial("T4J") + "' "

			//Par�metro para considerar ou n�o as demandas j� processadas
			If soDados:oParametros["lDemandsProcessed"]
				cWhere += " AND (T4J_PROC = '2' OR T4J_PROC = '1')"
			Else
				cWhere += " AND (T4J_PROC = '2')"
			EndIf

			//Par�metro para filtrar os documentos
			If !Empty(soDados:oParametros["cDocuments"])
				cWhere += " AND " + soDominio:oSeletivos:scriptInSQL("T4J_DOC", "cDocuments")
			EndIf

			//Par�metro para filtrar os armaz�ns
			If !Empty(soDados:oParametros["cWarehouses"])
				cWhere += " AND " + soDominio:oSeletivos:scriptInSQL("T4J_LOCAL", "cWarehouses")
			EndIf

			//Par�metro para filtrar os c�digos de demandas
			If lT4JCODE .AND. !Empty(soDados:oParametros["cDemandCodes"])
				cWhere += " AND " + soDominio:oSeletivos:scriptInSQL("T4J_CODE", "cDemandCodes")
			EndIf

			//Valida o filtro de Tipo ou Grupo de Produtos
			cWhere += " AND " + soDominio:oSeletivos:scriptExistsProdutoSQL("T4J_PROD")

			//Par�metro de sele��o dos tipos de demandas a serem processadas
			If !Empty(soDados:oParametros["cDemandType"])
				cWhere += " AND (   "
				If "1" $ soDados:oParametros["cDemandType"]
					cWhere += "T4J_ORIGEM = '3' OR "
				EndIf
				If "2" $ soDados:oParametros["cDemandType"]
					cWhere += "T4J_ORIGEM = '2' OR "
				EndIf
				If "3" $ soDados:oParametros["cDemandType"]
					cWhere += "T4J_ORIGEM = '1' OR "
				EndIf
				If "4" $ soDados:oParametros["cDemandType"]
					cWhere += "T4J_ORIGEM = '4' OR "
				EndIf
				If "9" $ soDados:oParametros["cDemandType"]
					cWhere += "T4J_ORIGEM = '9' OR "
				EndIf
				cWhere += "1 != 1)"

				If UpdRegsDB("T4J",            ; //UPDATE (tabela a ser buscada)
							 "T4J_PROC = '3'", ; //SET    (campos a serem atualizados)
							 cWhere)             //WHERE  (filtro a ser aplicado)

					cWhere := " T4J.T4J_FILIAL = '" + xFilial("T4J") + "' "
					cWhere += " AND T4J.T4J_PROC = '3'"

					aRegEmJson := GetRegDB("T4J", , cWhere, aFields)
				Else
					oLogs:log(STR0056 + TCSQLError(), "E") //"Erro na separa��o dos registros a serem carregados: "
				EndIf
			EndIf
		EndIf
	EndIf

	nTotal := Len(aRegEmJson)
	oMatriz:setFlag("qtd_registros_total", nTotal, , , .T.)
	oMatriz:setFlag("qtd_registros_lidos", 0     , , , .T.)

	If nTotal > 0
		aAux := Array(sTAM_MAT)
	EndIf

	//Processa todos os registros encontrados
	For nIndRegs := 1 To nTotal
		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
			oMatriz:setFlag("termino_carga_saida", "S")
			Exit
		EndIf

		oRegistros := JsonObject():new()
		cError     := oRegistros:fromJSON(aRegEmJson[nIndRegs])
		If !Empty(cError)
			cError := oRegistros:fromJSON(JsonString(aRegEmJson[nIndRegs]))
			If !Empty(cError)
				oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegEmJson[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
				Loop
			EndIf
		EndIf

		dOriginal := StoD(oRegistros["aRegs"][1]["T4J_DATA"])
		dEntrega  := ConvDatPer(dOriginal, aPeriodos, @nPeriodo, .T.)
		cProduto := PadR(oRegistros["aRegs"][1]["T4J_PROD"], snTamCod)

		If soDominio:oParametros["lEventLog"]
			//Loga Evento 005 - Data de necessidade invalida - Data posterior ao prazo maximo do MRP
			If dLimite != Nil .AND. dOriginal > dLimite
				soDominio:oEventos:loga("005", cProduto, dOriginal, {cProduto, Val(oRegistros["aRegs"][1]["T4J_QUANT"]), dOriginal, "T4J", oRegistros["aRegs"][1]["T4J_CODE"]})
				Loop

			//Gera Evento 004 - Data de necessidade invalida - Data anterior a database (Novas necessidades do MRP com data anterior a base do sistema)
			ElseIf dOriginal < soDados:oParametros["dDataIni"]
				soDominio:oEventos:loga("004", cProduto, dOriginal,;
				                        {"", Val(oRegistros["aRegs"][1]["T4J_QUANT"]), dOriginal,;
										oRegistros["aRegs"][1]["T4J_IDREG"],;
										"",;
										"T4J"})

			EndIf
		EndIf

		//Tratamento para quando possui o Opcional
		If oRegistros["aRegs"][1]["T4J_MOPC"] != NIL .AND. !Empty(oRegistros["aRegs"][1]["T4J_MOPC"]["OPTIONAL"])
			cOpcional  := oOpcionais:converteJsonEmID(oRegistros["aRegs"][1]["T4J_MOPC"]:toJson(), , Val(oRegistros["aRegs"][1]["R_E_C_N_O_"]))
		Else
			cOpcional  := ""
		EndIf
		cChaveProd := cProduto + Iif(Empty(cOpcional),"", "|" + cOpcional)

		cChave     := cFilAux + DtoS(dEntrega) + cChaveProd
		aSize(aRegistro, 0)

		//Guarda o registro na Matriz
		oMatriz:trava(cChave)
		If oMatriz:getRow(1, cChave, Nil, @aRegistro, .F., .T.)
			aRegistro[sMAT_SAIPRE] += Val(oRegistros["aRegs"][1]["T4J_QUANT"])

			If !oMatriz:updRow(1, cChave, Nil, aRegistro, .F., .T.)
				If oStatus:getStatus("status") == "4"
					Exit
				Else
					oLogs:log(STR0055 + "T4J", "E") //"Erro na carga de dados da tabela para memoria: "
				EndIf
			EndIf
		Else
			aAux[sMAT_FILIAL] := cFilAux
			aAux[sMAT_DATA]   := dEntrega
			aAux[sMAT_PRODUT] := cProduto
			aAux[sMAT_SLDINI] := 0
			aAux[sMAT_ENTPRE] := 0
			aAux[sMAT_SAIPRE] := Val(oRegistros["aRegs"][1]["T4J_QUANT"])
			aAux[sMAT_SAIEST] := 0
			aAux[sMAT_SALDO]  := 0
			aAux[sMAT_NECESS] := 0
			aAux[sMAT_TPREPO] := " "
			aAux[sMAT_EXPLOD] := .F.
			aAux[sMAT_THREAD] := -1
			aAux[sMAT_IDOPC]  := cOpcional
			aAux[sMAT_DTINI]  := dEntrega

			If !oMatriz:addRow(cChave, aAux, , , , cFilAux + cChaveProd)
				If oStatus:getStatus("status") == "4"
					Exit
				Else
					oLogs:log(STR0055 + "T4J", "E") //"Erro na carga de dados da tabela para memoria: "
				EndIf
			EndIf

			//Cria lista deste produto para registrar os periodos
			If !oMatriz:existList("Periodos_Produto_" + cChaveProd)
				oMatriz:createList("Periodos_Produto_" + cChaveProd)
			EndIf
			oMatriz:setItemList("Periodos_Produto_" + cChaveProd, cValToChar(nPeriodo), nPeriodo)

			If !Empty(cOpcional)
				oOpcionais:adicionaLista(cProduto, cOpcional)
			EndIf
		EndIf

		If soDominio:oParametros["lRastreia"] .and. dEntrega <= dFimDemand
			soDominio:oAglutina:prepara("1"                                     ,; //1 - Carga de Demandas
			                            oRegistros["aRegs"][1]["T4J_ORIGEM"]    ,;
										{oRegistros["aRegs"][1]["T4J_ORIGEM"], oRegistros["aRegs"][1]["T4J_IDREG"], ""},;
			                            oRegistros["aRegs"][1]["T4J_IDREG"]     ,;
										cProduto                                ,;
										cOpcional                               ,;
										""                                      ,;
										Val(oRegistros["aRegs"][1]["T4J_QUANT"]),;
										nPeriodo                                 )
		EndIf

		oMatriz:destrava(cChave)

		FreeObj(oRegistros)
		oRegistros := Nil

		oMatriz:setFlag("qtd_registros_lidos", 1, , , .T.)
	Next nIndRegs

	oLogs:log(STR0039 + cValToChar(MicroSeconds() - nTempoIni), "CM") //"Tempo de carga Saidas Firmes "
	oLogs:log(STR0027 + cValToChar(nTotal)                    , "CM") //"Quantidade de saidas firmes carregadas em memoria: "

	//Inclui rastreabilidade desta sa�da prevista
	If soDominio:oParametros["lRastreia"]
		nTempoIni  := MicroSeconds()
		soDominio:oAglutina:incluiRastreios("1", @nTotal)                  //1 - Carga de Demandas
		oLogs:log(STR0062 + cValToChar(MicroSeconds() - nTempoIni), "CM") //"Tempo carga rastreabilidade demandas: "
		oLogs:log(STR0063 + cValToChar(nTotal)                    , "CM") //"Quantidade de rastreabilidades demandas inclusas: "
	EndIf

	//Grava flag de conclus�o global
	oMatriz:setFlag("termino_carga_saida", "S")

	//Limpa as vari�veis da mem�ria
	aSize(aAux, 0)
	aAux := Nil
	aSize(aFields, 0)
	aFields := Nil
	aSize(aRegistro, 0)
	aRegistro := Nil
	aSize(aRegEmJson, 0)
	aRegEmJson := Nil

Return

/*/{Protheus.doc} produtos
Carrega dados na tabela de Produtos (PRD)
@author marcelo.neumann
@since 08/07/2019
@version 1.0
/*/
METHOD produtos() CLASS MrpDados_CargaMemoria

	//Verifica se a carga ser� multi-thread
	If ::oDados:oParametros["nThreads"] >= 1 .And. ::oDados:oParametros["cAutomacao"] != "2"
		PCPIPCGO(::oDados:oParametros["cSemaforoThreads"], .F., "PCPCargaPr") //Delega para processamento em Thread
	Else
		PCPCargaPr() //Execucao single-thread
	EndIf

Return

/*/{Protheus.doc} PCPCargaPr
Processa a carga dos dados dos Produtos
@author marcelo.neumann
@since 08/07/2019
@version 1.0
/*/
Function PCPCargaPr()

	Local nTempoIni  := MicroSeconds()
	Local aAux       := {}
	Local aFields    := {}
	Local aNames     := {}
	Local aPeriodos
	Local aRegEmJson := {}
	Local aRegistro  := {}
	Local aRelation  := {}
	Local cError
	Local cTablePrd  := ""
	Local cChave     := ""
	Local cIDT4V     := ""
	Local cIDT4VOld  := ""
	Local cOldChave  := ""
	Local cFilAux    := ""
	Local cProduto   := ""
	Local cValLote   := ""
	Local cWhere     := ""
	Local cJoin      := ""
	Local lHWAROTOPE := .F.
	Local lHWX       := .F.
	Local lT4VSldBlq := .F.
	Local nContador  := 0
	Local nIndItem   := 0
	Local nIndRegs   := 1
	Local nIndSlds   := 0
	Local nTotal     := 0
	Local nTotSlds   := 0
	Local nSaldo     := 0
	Local nLenPer    := 0
	Local oLogs      := Nil
	Local oRegistros := Nil
	Local oStatus    := Nil
	Local oTotNiveis := JsonObject():New()

	//Seta objetos do totalizador
	oTotNiveis["nProdutosN"] := JsonObject():New()
	oTotNiveis["nProdCalcN"] := JsonObject():New()

	//Prepara o ambiente (quando for uma nova thread)
	PrepStatics()
	aPeriodos := soDados:aPeriodos
	nLenPer   := Len(aPeriodos)
	cTablePrd := IIf(soDados:oParametros["lUsesProductIndicator"], "HWE", "HWA")
	aFields   := loadFields(cTablePrd)
	oProdutos := soDados:oProdutos
	oLogs     := soDados:oLogs
	oStatus   := MrpDados_Status():New(soDados:oParametros["ticket"])

	dbSelectArea("HWA")
	If FieldPos("HWA_ROTOPE") > 0
		lHWAROTOPE := .T.
	EndIf

	dbSelectArea("T4V")
	If FieldPos("T4V_SLDBQ") > 0
		lT4VSldBlq := .T.
	EndIf

	lHWX := FWAliasInDic("HWX",.F.)

	//Busca os registros a serem processados
	If PreparaAmb()
		cWhere := "HWA.HWA_FILIAL = '" + xFilial("HWA") + "'"

		//Relaciona Tabela de Saldos
		cJoin := " HWA.HWA_PROD = T4V.T4V_PROD "
		cJoin += " AND T4V.D_E_L_E_T_ = ' ' "
		cJoin += " AND T4V.T4V_FILIAL = '" + xFilial("T4V") + "' "
		cJoin += " AND T4V.T4V_QTD <> 0 "

		//Par�metro para filtrar os armaz�ns
		If !Empty(soDados:oParametros["cWarehouses"])
			cJoin += " AND " + soDominio:oSeletivos:scriptInSQL("T4V.T4V_LOCAL", "cWarehouses")
		EndIf

		//Valida o filtro de Tipo ou Grupo de Produtos
		cJoin += " AND " + soDominio:oSeletivos:scriptExistsProdutoSQL("T4V.T4V_PROD")

		If !soDados:oParametros["lExpiredLot"]
			cJoin += " AND (T4V_LOTE = ' ' OR T4V_VALID >= '" + DToS(aPeriodos[1]) + "') "
		EndIf

		cJoin += LocalMRP("T4V.T4V_LOCAL")

		aAdd(aRelation, {"T4V", "LEFT OUTER JOIN", cJoin})

		//Relaciona Tabela de Rejeitos em CQ
		If soDados:oParametros["lSubtraiRejeitosCQ"] .And. lHWX
			cJoin := " HWA.HWA_PROD = HWX.HWX_PROD "
			cJoin += " AND T4V.T4V_LOCAL = HWX.HWX_LOCAL "
			If !Empty(soDados:oParametros["cWarehouses"])
				cJoin += " AND " + soDominio:oSeletivos:scriptInSQL("HWX.HWX_LOCAL", "cWarehouses")
			EndIf
			cJoin += LocalMRP("HWX.HWX_LOCAL")
			If Len(aPeriodos) > 1
				cJoin += " AND HWX.HWX_DATNF < '" + DToS(aPeriodos[2]) + "' "
			Else
				cJoin += " AND HWX.HWX_DATNF <= '" + DToS(aPeriodos[1]) + "' "
			EndIf
			aAdd(aRelation, {"HWX", "LEFT OUTER JOIN", cJoin})
		EndIf

		//Relaciona Tabela Indicador de Produtos
		If cTablePrd == "HWE"
			cJoin := " HWA.HWA_PROD = HWE.HWE_PROD "
			cJoin += " AND HWE.D_E_L_E_T_ = ' ' "
			cJoin += " AND HWE.HWE_FILIAL = '" + xFilial("HWE") + "' "
			aAdd(aRelation, {"HWE", "LEFT OUTER JOIN", cJoin})
		EndIf

		//Aguarda o c�clulo dos n�veis para fazer o SELECT na HWA
		If AgCalcNiv(oStatus)
			aRegEmJson := GetProdDB("HWA", aRelation, cWhere, aFields)
		Else
			oProdutos:setFlag("termino_carga", "S")
		EndIf
	EndIf

	nTotal := Len(aRegEmJson)
	oProdutos:setFlag("qtd_registros_total", nTotal, , , .T.)
	oProdutos:setFlag("qtd_registros_lidos", 0     , , , .T.)

	If nTotal > 0
		aAux := Array(sTAM_PRD)
	EndIf

	//Processa todos os registros encontrados
	For nIndRegs := 1 to nTotal
		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
			oProdutos:setFlag("termino_carga", "S")
			Exit
		EndIf

		oRegistros := JsonObject():new()
		cError     := oRegistros:fromJSON(aRegEmJson[nIndRegs])
		If !Empty(cError)
			cError := oRegistros:fromJSON(JsonString(aRegEmJson[nIndRegs]))
			If !Empty(cError)
				Loop
				oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegEmJson[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
			EndIf
		EndIf

		If Empty(cError)
			cProduto := PadR(oRegistros["aRegs"][1][cTablePrd+"_PROD"], snTamCod)
			cChave   := cFilAux + cProduto
			aSize(aRegistro, 0)

			aAux[sPRD_FILIAL] := cFilAux
			aAux[sPRD_COD   ] := cProduto
			aAux[sPRD_NPERAT] := -1
			aAux[sPRD_NPERMA] := -1
			aAux[sPRD_NPERCA] := -1
			aAux[sPRD_THREAD] := -1
			aAux[sPRD_REINIC] := .F.
			aAux[sPRD_IDOPC ] := ""
			aAux[sPRD_DTHOFI] := Nil
			aAux[sPRD_REVATU] := oRegistros["aRegs"][1][cTablePrd+"_REVATU"]
			aAux[sPRD_NIVEST] := oRegistros["aRegs"][1]["HWA_NIVEL"]
			aAux[sPRD_TIPE  ] := oRegistros["aRegs"][1][cTablePrd+"_TIPE"]
			aAux[sPRD_TIPO  ] := oRegistros["aRegs"][1]["HWA_TIPO"]
			aAux[sPRD_GRUPO ] := oRegistros["aRegs"][1]["HWA_GRUPO"]
			aAux[sPRD_RASTRO] := oRegistros["aRegs"][1]["HWA_RASTRO"]
			aAux[sPRD_MRP   ] := oRegistros["aRegs"][1][cTablePrd+"_MRP"]
			aAux[sPRD_PROSBP] := oRegistros["aRegs"][1]["HWA_PROSBP"]
			aAux[sPRD_ESTORI] := oRegistros["aRegs"][1]["HWA_ESTORI"]
			aAux[sPRD_APROPR] := oRegistros["aRegs"][1]["HWA_APROPR"]

			If lHWAROTOPE
				aAux[sPRD_ROTEIR] := oRegistros["aRegs"][1]["HWA_ROTOPE"]
			EndIf

			aAux[sPRD_LE    ] := Val(oRegistros["aRegs"][1][cTablePrd+"_LE"])
			aAux[sPRD_PE    ] := Val(oRegistros["aRegs"][1][cTablePrd+"_PE"])
			aAux[sPRD_PPED  ] := Val(oRegistros["aRegs"][1][cTablePrd+"_EMIN"])
			aAux[sPRD_TIPDEC] := Val(oRegistros["aRegs"][1]["HWA_TIPDEC"])
			aAux[sPRD_NUMDEC] := Val(oRegistros["aRegs"][1]["HWA_NUMDEC"])
			aAux[sPRD_QTEMB ] := Val(oRegistros["aRegs"][1][cTablePrd+"_QE"])
			aAux[sPRD_LM    ] := Val(oRegistros["aRegs"][1][cTablePrd+"_LM"])
			aAux[sPRD_TOLER ] := Val(oRegistros["aRegs"][1][cTablePrd+"_TOLER"])
			aAux[sPRD_EMAX  ] := Val(oRegistros["aRegs"][1][cTablePrd+"_EMAX"])
			aAux[sPRD_LOTSBP] := Val(oRegistros["aRegs"][1]["HWA_LOTSBP"])
			aAux[sPRD_CHAVE2] := cFilAux + aAux[sPRD_NIVEST] + cProduto
			aAux[sPRD_LOTVNC] := {}
			aAux[sPRD_CPOTEN] := oRegistros["aRegs"][1]["HWA_CPOTEN"]

			If oRegistros["aRegs"][1]["HWA_MOD"] == "T"
				soDados:oProdutos:setFlag("|MOD|"+cProduto+"|", .T.)
			EndIf

			aAux[sPRD_LSUBPR] := .F. //Indica se o produto � subproduto (possui estrutura negativa)

			//Altera��es referente SALDO INICIAL - INICIO
			cIDT4V := oRegistros["aRegs"][1]["T4V_IDREG"]
			If cIDT4V == cIDT4VOld .AND. cChave == cOldChave
				nSaldo := 0

			Else
				nSaldo := Val(oRegistros["aRegs"][1]["T4V_QTD"])
				If !soDados:oParametros["lExpiredLot"]
					cValLote := oRegistros["aRegs"][1]["T4V_VALID"]
				EndIf

				//Se o par�metro da tela "Estoque EM Terceiro" estiver como "1 - Soma", soma o valor do campo T4V_QNPT
				If soDados:oParametros["lEMTerceiro"]
					nSaldo += Val(oRegistros["aRegs"][1]["T4V_QNPT"])
				EndIf

				//Se o par�metro da tela "Estoque DE Terceiro" estiver como "1 - Subtrai", subtrai o valor do campo T4V_QTNP
				If soDados:oParametros["lDETerceiro"]
					nSaldo -= Val(oRegistros["aRegs"][1]["T4V_QTNP"])
				EndIf

				//Se o par�metro da tela "Saldo Bloqueado por Lote" estiver como "1 - Subtrai", subtrai o valor do campo T4V_SLDBQ
				If lT4VSldBlq .And. soDados:oParametros["lSubtraiLoteBloqueado"]
					nSaldo -= Val(oRegistros["aRegs"][1]["T4V_SLDBQ"])
				EndIf
			EndIf
			cIDT4VOld := cIDT4V

			//Se o par�metro da tela "Estoque Rejeitado pelo CQ" estiver como "1 - Subtrai", subtrai o valor do campo (HWX_QTDE - HWX_QTDEV)
			If soDados:oParametros["lSubtraiRejeitosCQ"] .And. lHWX
				nSaldo -= (Val(oRegistros["aRegs"][1]["HWX_QTDE"]) - Val(oRegistros["aRegs"][1]["HWX_QTDEV"]))
			EndIf

			aAux[sPRD_SLDDIS] := nSaldo
			If !soDados:oParametros["lExpiredLot"] .And. !Empty(cValLote) .And. aPeriodos[nLenPer] > SToD(cValLote)
				aAdd(aAux[sPRD_LOTVNC], {nSaldo, SToD(cValLote)})
			EndIf
			//Altera��es referente SALDO INICIAL - FIM


			//Considerar Horizonte Firme somente se for informado o Tipo
			If Empty(oRegistros["aRegs"][1][cTablePrd+"_TPHFIX"])
				aAux[sPRD_HORFIR] := 0
				aAux[sPRD_TPHOFI] := Nil
			Else
				aAux[sPRD_HORFIR] := Val(oRegistros["aRegs"][1][cTablePrd+"_HORFIX"])
				aAux[sPRD_TPHOFI] := oRegistros["aRegs"][1][cTablePrd+"_TPHFIX"]
			EndIf

			//Considera o estoque de seguran�a na carga. Desfaz caso o par�metro lEstoqueSeguranca for .F.
			IF cChave == cOldChave
				oRegistros["aRegs"][1][cTablePrd+"_ESTSEG"] := "0"
			EndIf
			aAux[sPRD_ESTSEG] := Val(oRegistros["aRegs"][1][cTablePrd+"_ESTSEG"])
			aAux[sPRD_SLDDIS] += -aAux[sPRD_ESTSEG]

			If prdBlock()
				aAux[sPRD_BLOQUE] := oRegistros["aRegs"][1]["HWA_BLOQUE"]
				If aAux[sPRD_BLOQUE] == "1"//Se bloqueado, n�o aplica Estoque de Seguran�a e Ponto de Pedido
					aAux[sPRD_SLDDIS] += aAux[sPRD_ESTSEG]
					aAux[sPRD_ESTSEG] := 0
					aAux[sPRD_PPED]   := 0
				EndIf
			Else
				aAux[sPRD_BLOQUE] := "2"
			EndIf

			//Guarda o registro na Matriz
			If cChave == cOldChave .AND. oProdutos:getRow(1, cChave, Nil, @aRegistro)
				aAux[sPRD_ESTSEG] := aRegistro[sPRD_ESTSEG]
				aAux[sPRD_PPED  ] := aRegistro[sPRD_PPED  ]
				aAux[sPRD_SLDDIS] += aRegistro[sPRD_SLDDIS]

				nTotSlds   := Len(aAux[sPRD_LOTVNC])
				For nIndSlds := 1 to nTotSlds
					aAdd(aRegistro[sPRD_LOTVNC], aAux[sPRD_LOTVNC][nIndSlds] )
				Next
				aSize(aAux[sPRD_LOTVNC], 0)
				aAux[sPRD_LOTVNC] := aRegistro[sPRD_LOTVNC]

				If !oProdutos:updRow(1, cChave, Nil, aAux)
					If oStatus:getStatus("status") == "4"
						Exit
					Else
						oLogs:log(STR0031 + cTablePrd, "E") //"Erro na carga de dados do arquivo .CSV para memoria: "
					EndIf
				EndIf
			Else
				cOldChave := cChave
				If oTotNiveis["nProdutosN"] == Nil
					oTotNiveis["nProdutosN"][aAux[sPRD_NIVEST]] := 1
				Else
					oTotNiveis["nProdutosN"][aAux[sPRD_NIVEST]]++
				EndIf
				oTotNiveis["nProdutosN"][aAux[sPRD_NIVEST]] := 0

				If !oProdutos:addRow(cChave, aAux)
					If oStatus:getStatus("status") == "4"
						Exit
					Else
						oLogs:log(STR0031 + cTablePrd, "E") //"Erro na carga de dados do arquivo .CSV para memoria: "
					EndIf
				EndIf
			EndIf
		EndIf

		FreeObj(oRegistros)
		oRegistros := Nil

		nContador++

		//Atualiza contador de registros lidos
		If Mod(nContador, 50) == 0
			oProdutos:setFlag("qtd_registros_lidos", nContador, , , .T.)
			nContador := 0
		EndIf
	Next nIndRegs

	//Atualiza contador de registros lidos
	If nContador > 0
		oProdutos:setFlag("qtd_registros_lidos", nContador, , , .T.)
		nContador := 0
	EndIf

	//Atualiza totalizador de produtos por n�vel
	aNames := oTotNiveis["nProdutosN"]:GetNames()
	nTotal := Len(aNames)
	For nIndItem := 1 to nTotal
		oProdutos:setflag("nProdutosN" + aNames[nIndItem], oTotNiveis["nProdutosN"][aNames[nIndItem]], .F., .F., .T.) //incrementa
	Next

	//Atualiza contador de produtos calculados por n�vel
	aNames := oTotNiveis["nProdCalcN"]:GetNames()
	nTotal := Len(aNames)
	For nIndItem := 1 to nTotal
		oProdutos:setflag("nProdCalcN" + aNames[nIndItem], oTotNiveis["nProdCalcN"][aNames[nIndItem]], .F., .F., .T.) //incrementa
	Next

	//Grava flag de conclus�o global
	oProdutos:setFlag("termino_carga", "S")

	oLogs:log(STR0035 + cValToChar(MicroSeconds() - nTempoIni), "CM") //"Tempo de carga tabela Produtos "
	oLogs:log(STR0028 + cValToChar(nTotal)                    , "CM") //"Quantidade de produtos carregados em memoria: "

	//Limpa os arrays da mem�ria
	aSize(aAux, 0)
	aAux := Nil
	aSize(aFields, 0)
	aFields := Nil
	aSize(aRegEmJson, 0)
	aRegEmJson := Nil
	aSize(aRegistro, 0)
	aRegistro := Nil

	FreeObj(oTotNiveis["nProdutosN"])
	FreeObj(oTotNiveis["nProdCalcN"])
	FreeObj(oTotNiveis)

Return

/*/{Protheus.doc} estruturas
Carrega dados na tabela de Estruturas (EST)
@author marcelo.neumann
@since 08/07/2019
@version 1.0
/*/
METHOD estruturas() CLASS MrpDados_CargaMemoria

	//Verifica se a carga ser� multi-thread
	If ::oDados:oParametros["nThreads"] >= 1 .And. ::oDados:oParametros["cAutomacao"] != "2"
		PCPIPCGO(::oDados:oParametros["cSemaforoThreads"], .F., "PCPCargaEs", ::oDados:oParametros["lCargaEstruturaCSV"]) //Delega para processamento em Thread
	Else
		PCPCargaEs(::oDados:oParametros["lCargaEstruturaCSV"]) //Execucao single-thread
	EndIf

Return

/*/{Protheus.doc} PCPCargaEs
Processa a carga dos dados das Estruturas
@author marcelo.neumann
@since 08/07/2019
@version 1.0
@param 01 lCargaCSV, l�gico, indica se a carga ser� feita via arquivo CSV ou via tabela
/*/
Function PCPCargaEs(lCargaCSV)

	Local nTempoIni  := MicroSeconds()
	Local aAuxAlt    := {}
	Local aAuxEst    := {}
	Local aAuxOper   := {}
	Local aFieldsAlt := {}
	Local aFieldsEst := {{"T4N_PROD"  , "C", "T4N", ""        }, ;
	                     {"T4N_COMP"  , "C", "T4N", ""        }, ;
	                     {"T4N_QTD"   , "N", "T4N", ""        }, ;
	                     {"T4N_QTDB"  , "N", "T4N", ""        }, ;
	                     {"T4N_FANTAS", "C", "T4N", "F"       }, ;
	                     {"T4N_SEQ"   , "C", "T4N", ""        }, ;
	                     {"T4N_GROPC" , "C", "T4N", ""        }, ;
	                     {"T4N_ITOPC" , "C", "T4N", ""        }, ;
	                     {"T4N_DTINI" , "D", "T4N", "19800101"}, ;
	                     {"T4N_DTFIM" , "D", "T4N", "29991231"}, ;
	                     {"T4N_REVINI", "C", "T4N", "   "     }, ;
	                     {"T4N_REVFIM", "C", "T4N", "ZZZ"     }, ;
	                     {"T4O_ESTOQ" , "C", "T4O", "1"       }, ;
						 {"T4N_FIXA"  , "C", "T4N", "2"       }, ;
						 {"T4N_POTEN" , "N", "T4N", "0"       }, ;
						 {"T4N_PERDA" , "N", "T4N", "0"       }, ;
	                     {"T4N_IDREG" , "C", "T4N", ""        }, ;
	                     {"T4O_ALTERN", "C", "T4O", ""        }, ;
	                     {"T4O_FATCON", "N", "T4O", ""        }, ;
	                     {"T4O_TPCONV", "C", "T4O", ""        }, ;
	                     {"T4O_SEQ"   , "C", "T4O", ""        }, ;
	                     {"T4O_DATA"  , "D", "T4O", ""        }}
	Local aNames     := {}
	Local aRegJsonAl := {}
	Local aRegJsonEs := {}
	Local cChave     := ""
	Local cChvOpers  := ""
	Local cComp      := ""
	Local cProdPai   := ""
	Local cError
	Local cFilAux    := ""
	Local cProduto   := ""
	Local cWhere     := ""
	Local cJoin      := ""
	Local nIndRegs   := 1
	Local nTotalEst  := 0
	Local nTotalAlt  := 0
	Local oAlternati
	Local oChavesEst
	Local oEstJson
	Local oEstrutura
	Local oJsnMinMax
	Local oLogs
	Local oRegistros
	Local oStatus
	Local oJsonOper := JsonObject():New()

	//Prepara o ambiente (quando for uma nova thread)
	PrepStatics()
	oLogs      := soDados:oLogs
	oEstrutura := soDados:oEstruturas
	oAlternati := soDados:oAlternativos
	oStatus    := MrpDados_Status():New(soDados:oParametros["ticket"])

	//Busca os registros a serem processados (via CSV ou tabela)
	If lCargaCSV
		aFieldsAlt := {aFieldsEst[1] , ;
		               aFieldsEst[17], ;
		               aFieldsEst[18], ;
		               aFieldsEst[19], ;
					   aFieldsEst[20], ;
		               aFieldsEst[21]}
		aSize(aFieldsEst, 16)
		aRegJsonEs := CsvToJson("estruturas.csv"  , aFieldsEst)
		aRegJsonAl := CsvToJson("alternativos.csv", aFieldsAlt)

		nTotalEst := Len(aRegJsonEs)
		nTotalAlt := Len(aRegJsonAl)

		oAlternati:setFlag("qtd_registros_total", nTotalAlt)
		oAlternati:setFlag("qtd_registros_lidos", 0)
	Else
		If PreparaAmb()
			cWhere := " T4N.T4N_FILIAL = '" + xFilial("T4N") + "' "

			If prdBlock()
				cWhere += " AND " + soDominio:oSeletivos:scriptExistsProdutoSQL("T4N.T4N_COMP", .T.)
			EndIf

			cJoin  := " T4N.T4N_IDREG = T4O.T4O_IDEST "
			cJoin  += " AND T4O.D_E_L_E_T_ = ' ' "
			cJoin  += " AND T4O.T4O_FILIAL = '" + xFilial("T4O") + "' "
			cJoin  += " AND " + soDominio:oSeletivos:scriptExistsProdutoSQL("T4O.T4O_ALTERN")

			aRegJsonEs := GetRegDB("T4N", {{"T4O", "LEFT OUTER JOIN", cJoin}}, cWhere, aFieldsEst)
		EndIf
		nTotalEst := Len(aRegJsonEs)
		nTotalAlt := nTotalEst

		oAlternati:setFlag("qtd_registros_total", 0)
		oAlternati:setFlag("qtd_registros_lidos", 0)
	EndIf

	oEstrutura:setFlag("qtd_registros_total", nTotalEst)
	oEstrutura:setFlag("qtd_registros_lidos", 0)

	If nTotalEst > 0
		oEstJson   := JsonObject():New()
		oChavesEst := JsonObject():New()
		aAuxEst    := Array(sTAM_EST)
	EndIf

	If nTotalAlt > 0
		oJsnMinMax := JsonObject():New()
		aAuxAlt    := Array(sTAM_ALT)
	EndIf

	If FwAliasInDic("HW9", .F.)
		oJsonOper := RetOperCmp() //Retorna Objeto Json de Opera��es por Componente
	EndIf

	//Processa todos os registros encontrados
	For nIndRegs := 1 to nTotalEst
		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
			oAlternati:setFlag("termino_carga", "S") //Grava flag de conclusao global
		   	oEstrutura:setFlag("termino_carga", "S") //Grava flag de conclusao global
			Exit
		EndIf

		oRegistros := JsonObject():new()
		cError     := oRegistros:fromJSON(aRegJsonEs[nIndRegs])
		If !Empty(cError)
			cError := oRegistros:fromJSON(JsonString(aRegJsonEs[nIndRegs]))
			If !Empty(cError)
				Loop
				oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegJsonEs[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
			EndIf
		EndIf

		If Empty(cError)
			cComp     := PadR(oRegistros["aRegs"][1]["T4N_COMP"], snTamCod)
			cProdPai  := PadR(oRegistros["aRegs"][1]["T4N_PROD"], snTamCod)
			cChave    := cFilAux + oRegistros["aRegs"][1]["T4N_IDREG"]
			cChvOpers := cProdPai + cComp + oRegistros["aRegs"][1]["T4N_SEQ"] //Produto + Componente + TRT
			If oChavesEst[cChave] == Nil .Or. oChavesEst[cChave]
				oChavesEst[cChave] := lCargaCSV

				aAuxEst[sEST_FILIAL] := cFilAux
				aAuxEst[sEST_CODPAI] := cProdPai
				aAuxEst[sEST_CODFIL] := cComp
				aAuxEst[sEST_QTD]    := Val(oRegistros["aRegs"][1]["T4N_QTD"])
				aAuxEst[sEST_QTDB]   := Val(oRegistros["aRegs"][1]["T4N_QTDB"])
				aAuxEst[sEST_FANT]   := oRegistros["aRegs"][1]["T4N_FANTAS"] == "T"
				aAuxEst[sEST_TRT]    := oRegistros["aRegs"][1]["T4N_SEQ"]
				aAuxEst[sEST_GRPOPC] := oRegistros["aRegs"][1]["T4N_GROPC"]
				aAuxEst[sEST_ITEOPC] := oRegistros["aRegs"][1]["T4N_ITOPC"]
				aAuxEst[sEST_VLDINI] := StoD(oRegistros["aRegs"][1]["T4N_DTINI"])
				aAuxEst[sEST_VLDFIM] := StoD(oRegistros["aRegs"][1]["T4N_DTFIM"])
				aAuxEst[sEST_REVINI] := oRegistros["aRegs"][1]["T4N_REVINI"]
				aAuxEst[sEST_REVFIM] := oRegistros["aRegs"][1]["T4N_REVFIM"]
				aAuxEst[sEST_ALTERN] := oRegistros["aRegs"][1]["T4O_ESTOQ"]
				aAuxEst[sEST_FIXA]   := oRegistros["aRegs"][1]["T4N_FIXA"]
				aAuxEst[sEST_POTEN]  := Val(oRegistros["aRegs"][1]["T4N_POTEN"])
				aAuxEst[sEST_PERDA]  := Val(oRegistros["aRegs"][1]["T4N_PERDA"])

				If oJsonOper[cChvOpers] == Nil
					aAuxEst[sEST_OPERA]   := {}
				Else
					aAuxEst[sEST_OPERA]   := oJsonOper[cChvOpers]
				EndIf

				cChave := cFilAux + aAuxEst[sEST_CODPAI]
				If oEstJson[cChave] == Nil
					oEstJson[cChave] := {}
				EndIf
				aAdd(oEstJson[cChave], aClone(aAuxEst))
			EndIf

			If !lCargaCSV
				If !Empty(oRegistros["aRegs"][1]["T4O_ALTERN"])
					aAuxAlt[sALT_FILIAL] := cFilAux
					aAuxAlt[sALT_PRODUT] := cComp
					aAuxAlt[sALT_ALTERT] := PadR(oRegistros["aRegs"][1]["T4O_ALTERN"], snTamCod)
					aAuxAlt[sALT_TPFAT]  := oRegistros["aRegs"][1]["T4O_TPCONV"]
					aAuxAlt[sALT_FATOR]  := Val(oRegistros["aRegs"][1]["T4O_FATCON"])
					aAuxAlt[sALT_ORDEM]  := oRegistros["aRegs"][1]["T4O_SEQ"]
					aAuxAlt[sALT_DATA]   := StoD(oRegistros["aRegs"][1]["T4O_DATA"])

					If oJsnMinMax[cComp] == Nil
						oJsnMinMax[cComp] := {"", ""} //MIN, MAX
					EndIf

					//Registra Menor e Maior Sequ�ncia
					If cProduto != cComp .AND. !Empty(oJsnMinMax[cProduto])
						soDados:oAlternativos:setItemAList("min_max", cProduto, oJsnMinMax[cProduto], .F., .T., .F.)
					EndIf

					//Identifica Menor e Maior Sequ�ncia
					cProduto                := cComp
					oJsnMinMax[cProduto][1] := Iif(oJsnMinMax[cProduto][1] == "" .OR. oJsnMinMax[cProduto][1] > aAuxAlt[sALT_ORDEM], aAuxAlt[sALT_ORDEM], oJsnMinMax[cProduto][1])
					oJsnMinMax[cProduto][2] := Iif(oJsnMinMax[cProduto][2] == "" .OR. oJsnMinMax[cProduto][2] < aAuxAlt[sALT_ORDEM], aAuxAlt[sALT_ORDEM], oJsnMinMax[cProduto][2])

					cChave := cFilAux + cComp + aAuxAlt[sALT_ORDEM]
					If !oAlternati:getRow(1, cChave, Nil)
						If !oAlternati:addRow(cChave, aAuxAlt)
							If oStatus:getStatus("status") == "4"
								Exit
							Else
								oLogs:log(STR0055 + "T4O", "E") //"Erro na carga de dados da tabela para memoria: "
							EndIf
						EndIf
					EndIf
				EndIf
			EndIf
		EndIf

		FreeObj(oRegistros)
		oRegistros := Nil

		oEstrutura:setFlag("qtd_registros_lidos", 1, , , .T.)
	Next nIndRegs

	If nTotalEst > 0
		aNames    := oEstJson:GetNames()
		nTotalEst := Len(aNames)

		For nIndRegs := 1 to nTotalEst
			If !oEstrutura:addRow(aNames[nIndRegs], oEstJson[aNames[nIndRegs]])
				If oStatus:getStatus("status") == "4"
					Exit
				Else
					oLogs:log(STR0055 + "T4N", "E") //"Erro na carga de dados da tabela para memoria: "
				EndIf
			EndIf
		Next
		aSize(aNames, 0)
	EndIf

	//Se a carga � via CSV, os dados s�o lidos separadamente
	If lCargaCSV
		//Processa todos os registros encontrados
		For nIndRegs := 1 To nTotalAlt
			//Checa cancelamento a cada X delegacoes
			If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
				oAlternati:setFlag("termino_carga", "S") //Grava flag de conclusao global
		   		oEstrutura:setFlag("termino_carga", "S") //Grava flag de conclusao global
				Exit
			EndIf

			oRegistros := JsonObject():new()
			cError     := oRegistros:fromJSON(aRegJsonAl[nIndRegs])
			If !Empty(cError)
				cError := oRegistros:fromJSON(JsonString(aRegJsonAl[nIndRegs]))
				If !Empty(cError)
					Loop
					oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegJsonAl[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
				EndIf
			EndIf

			cComp                := PadR(oRegistros["aRegs"][1]["T4N_PROD"], snTamCod)
			aAuxAlt[sALT_FILIAL] := cFilAux
			aAuxAlt[sALT_PRODUT] := cComp
			aAuxAlt[sALT_ALTERT] := PadR(oRegistros["aRegs"][1]["T4O_ALTERN"], snTamCod)
			aAuxAlt[sALT_TPFAT]  := oRegistros["aRegs"][1]["T4O_TPCONV"]
			aAuxAlt[sALT_FATOR]  := Val(oRegistros["aRegs"][1]["T4O_FATCON"])
			aAuxAlt[sALT_ORDEM]  := oRegistros["aRegs"][1]["T4O_SEQ"]
			aAuxAlt[sALT_DATA]   := StoD(oRegistros["aRegs"][1]["T4O_DATA"])

			If oJsnMinMax[cComp] == Nil
				oJsnMinMax[cComp] := {"", ""} //MIN, MAX
			EndIf

			//Registra Menor e Maior Sequ�ncia
			If cProduto != cComp .AND. !Empty(oJsnMinMax[cProduto])
				soDados:oAlternativos:setItemAList("min_max", cProduto, oJsnMinMax[cProduto], .F., .T., .F.)
			EndIf

			//Identifica Menor e Maior Sequ�ncia
			cProduto                := cComp
			oJsnMinMax[cProduto][1] := Iif(oJsnMinMax[cProduto][1] == "" .OR. oJsnMinMax[cProduto][1] > aAuxAlt[sALT_ORDEM], aAuxAlt[sALT_ORDEM], oJsnMinMax[cProduto][1])
			oJsnMinMax[cProduto][2] := Iif(oJsnMinMax[cProduto][2] == "" .OR. oJsnMinMax[cProduto][2] < aAuxAlt[sALT_ORDEM], aAuxAlt[sALT_ORDEM], oJsnMinMax[cProduto][2])

			cChave := cFilAux + cComp + aAuxAlt[sALT_ORDEM]

			If !oAlternati:getRow(1, cChave, Nil)
				If !oAlternati:addRow(cChave, aAuxAlt)
					If oStatus:getStatus("status") == "4"
						Exit
					Else
						oLogs:log(STR0055 + "T4O", "E") //"Erro na carga de dados da tabela para memoria: "
					EndIf
				EndIf
			EndIf

			FreeObj(oRegistros)
			oRegistros := Nil

			oAlternati:setFlag("qtd_registros_lidos", 1, , , .T.)
		Next nIndRegs
	EndIf

	//Registra Menor e Maior Sequ�ncia
	If !Empty(cProduto) .AND. !Empty(oJsnMinMax[cProduto])
		soDados:oAlternativos:setItemAList("min_max", cProduto, oJsnMinMax[cProduto], .F., .T., .F.)
	EndIf

	//Limpa Json Alternativos local
	If nTotalAlt > 0
		aNames := oJsnMinMax:GetNames()
		nTotal := Len(aNames)
		aSize(aAuxAlt, 0)
		For nIndRegs := 1 to nTotal
			IF oJsnMinMax[aNames[nIndRegs]] != Nil
				aAuxAlt := oJsnMinMax[aNames[nIndRegs]]
				aSize(aAuxAlt, 0)
				oJsnMinMax[cProduto] := Nil
			EndIf
		Next nIndRegs
		oJsnMinMax := Nil
	EndIf

	//Limpa Json Opera��es por Componente
	If nTotalAlt > 0
		aSize(aNames, 0)
		aNames := oJsonOper:GetNames()
		nTotal := Len(aNames)
		For nIndRegs := 1 to nTotal
			IF oJsonOper[aNames[nIndRegs]] != Nil
				aAuxOper := oJsonOper[aNames[nIndRegs]]
				aSize(aAuxOper, 0)
				oJsonOper[cProduto] := Nil
			EndIf
		Next nIndRegs
		oJsonOper := Nil
	EndIf

	oLogs:log(STR0037 + cValToChar(MicroSeconds() - nTempoIni)             , "CM") //"Tempo de carga tabela Estruturas + Alternativos "
	oLogs:logb({|| STR0030 + cValToChar(soDados:tamanhoLista("EST")) + "*"}, "CM") //"Quantidade de estruturas carregadas em memoria: "
	oLogs:logb({|| STR0029 + cValToChar(soDados:tamanhoLista("ALT"))}      , "CM") //"Quantidade de alternativos carregados em memoria: "

	//Grava flag de conclusao global
	oEstrutura:setFlag("termino_carga", "S")
	oAlternati:setFlag("termino_carga", "S")

	//Limpa as vari�veis da mem�ria
	aSize(aAuxAlt, 0)
	aAuxAlt := Nil
	aSize(aAuxEst, 0)
	aAuxEst := Nil
	aSize(aFieldsAlt, 0)
	aFieldsAlt := Nil
	aSize(aFieldsEst, 0)
	aFieldsEst := Nil
	aSize(aNames, 0)
	aNames := Nil
	aSize(aRegJsonAl, 0)
	aRegJsonAl := Nil
	aSize(aRegJsonEs, 0)
	aRegJsonEs := Nil

	FreeObj(oChavesEst)
	oChavesEst := Nil
	FreeObj(oEstJson)
	oEstJson := Nil

Return

/*/{Protheus.doc} RetOperCmp
Carrega dados no objeto Json de Opera��es por Componente
@author brunno.costa
@since 15/04/2020
@version 1.0
@return oJsonOper, objeto, objeto json com os registros de opera��es por componentes
/*/
Static Function RetOperCmp()

	Local nTempoIni  := MicroSeconds()
	Local aAuxOper   := {}
	Local aFields    := {{"HW9_FILIAL", "C", "HW9"}, ;
	                     {"HW9_PROD"  , "C", "HW9"}, ;
	                     {"HW9_ROTEIR", "C", "HW9"}, ;
	                     {"HW9_OPERAC", "C", "HW9"}, ;
	                     {"HW9_COMP"  , "C", "HW9"}, ;
	                     {"HW9_TRT"   , "C", "HW9"}}
	Local aRegJson   := {}
	Local cChave     := ""
	Local cError
	Local cWhere     := ""
	Local nIndRegs   := 1
	Local nTotal     := 0
	Local oJsonOper  := JsonObject():New()
	Local oEstrutura
	Local oLogs
	Local oRegistros
	Local oStatus

	//Prepara o ambiente (quando for uma nova thread)
	PrepStatics()
	oLogs      := soDados:oLogs
	oEstrutura := soDados:oEstruturas
	oStatus    := MrpDados_Status():New(soDados:oParametros["ticket"])

	If PreparaAmb()
		cWhere := " HW9.HW9_FILIAL = '" + xFilial("HW9") + "' "

		If prdBlock()
			cWhere += " AND " + soDominio:oSeletivos:scriptExistsProdutoSQL("HW9.HW9_PROD", .T.)
			cWhere += " AND " + soDominio:oSeletivos:scriptExistsProdutoSQL("HW9.HW9_COMP", .T.)
		EndIf

		aRegJson := GetRegDB("HW9", {}, cWhere, aFields)
	EndIf
	nTotal := Len(aRegJson)

	If nTotal > 0
		aAuxOper   := Array(sTAM_OPE)
	EndIf

	oEstrutura:setFlag("qtd_registros_operacoes_total", nTotal)
	oEstrutura:setFlag("qtd_registros_operacoes_lidos", 0)

	//Processa todos os registros encontrados
	For nIndRegs := 1 to nTotal

		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
			Exit
		EndIf

		oRegistros := JsonObject():new()
		cError     := oRegistros:fromJSON(aRegJson[nIndRegs])
		If !Empty(cError)
			cError := oRegistros:fromJSON(JsonString(aRegJson[nIndRegs]))
			If !Empty(cError)
				Loop
				oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegJson[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
			EndIf
		EndIf

		If Empty(cError)
			//Produto + Componente + TRT
			cChave := PadR(oRegistros["aRegs"][1]["HW9_PROD"], snTamCod)
			cChave += PadR(oRegistros["aRegs"][1]["HW9_COMP"], snTamCod)
			cChave += oRegistros["aRegs"][1]["HW9_TRT"]

			If oJsonOper[cChave] == Nil
				oJsonOper[cChave] := {}
			EndIf

			aAuxOper[sOPE_ROTE]  := oRegistros["aRegs"][1]["HW9_ROTEIR"]
			aAuxOper[sOPE_OPERA] := oRegistros["aRegs"][1]["HW9_OPERAC"]
			aAdd(oJsonOper[cChave], aClone(aAuxOper))

		EndIf

		FreeObj(oRegistros)
		oRegistros := Nil

		oEstrutura:setFlag("qtd_registros_operacoes_lidos", 1, , , .T.)
	Next nIndRegs

	oLogs:log(STR0082 + cValToChar(MicroSeconds() - nTempoIni)        , "CM") //"Tempo de carga tabela Opera��es por Componente "
	oLogs:logb({|| STR0083 + cValToChar(soDados:tamanhoLista("EST")) }, "CM") //"Quantidade de Opera��es por Componente carregadas em memoria: "

	//Limpa as vari�veis da mem�ria
	aSize(aAuxOper, 0)
	aAuxOper := Nil

Return oJsonOper

/*/{Protheus.doc} subproduto
Carrega dados na tabela de Estruturas Negativas (Subprodutos)
@author brunno.costa
@since 24/03/2020
@version 12.1.30
/*/
METHOD subproduto() CLASS MrpDados_CargaMemoria

	//Verifica se a carga ser� multi-thread
	If ::oDados:oParametros["nThreads"] >= 1
		PCPIPCGO(::oDados:oParametros["cSemaforoThreads"], .F., "PCPCargaSU") //Delega para processamento em Thread
	Else
		PCPCargaSU() //Execucao single-thread
	EndIf

Return

/*/{Protheus.doc} PCPCargaSU
Processa a carga dos dados das Estruturas Negativas (Subprodutos)
@author brunno.costa
@since 24/03/2020
@version 12.1.30
/*/
Function PCPCargaSU()

	Local nTempoIni  := MicroSeconds()
	Local aAuxSubPrd := {}
	Local aFieldSubP := {{"T4N_PROD"  , "C", "T4N", ""        }, ;
	                     {"T4N_COMP"  , "C", "T4N", ""        }, ;
	                     {"T4N_QTD"   , "N", "T4N", ""        }, ;
	                     {"T4N_QTDB"  , "N", "T4N", ""        }, ;
	                     {"T4N_FANTAS", "C", "T4N", "F"       }, ;
	                     {"T4N_SEQ"   , "C", "T4N", ""        }, ;
	                     {"T4N_GROPC" , "C", "T4N", ""        }, ;
	                     {"T4N_ITOPC" , "C", "T4N", ""        }, ;
	                     {"T4N_DTINI" , "D", "T4N", "19800101"}, ;
	                     {"T4N_DTFIM" , "D", "T4N", "29991231"}, ;
	                     {"T4N_REVINI", "C", "T4N", "   "     }, ;
	                     {"T4N_REVFIM", "C", "T4N", "ZZZ"     }, ;
	                     {"T4N_FIXA"  , "C", "T4N", "2"       }, ;
	                     {"T4N_POTEN" , "N", "T4N", "0"       }, ;
	                     {"T4N_PERDA" , "N", "T4N", "0"       }, ;
	                     {"T4N_IDREG" , "C", "T4N", ""        }}
	Local aNames     := {}
	Local aRegJsonSU := {}
	Local cChave     := ""
	Local cOldChave  := ""
	Local cComp      := ""
	Local cError
	Local cFilAux    := ""
	Local cJoin      := ""
	Local cRecursive := ""
	Local nIndRegs   := 1
	Local nTotSubPro := 0
	Local oSubPJson
	Local oSubProdutos
	Local oProdutos
	Local oLogs
	Local oRegistros
	Local oStatus
	Local lProdBLQ

	//Prepara o ambiente (quando for uma nova thread)
	PrepStatics()
	oLogs        := soDados:oLogs
	oProdutos    := soDados:oProdutos
	oSubProdutos := soDados:oSubProdutos
	oStatus      := MrpDados_Status():New(soDados:oParametros["ticket"])
	lProdBLQ     := prdBlock()

	//Busca os registros a serem processados
	If PreparaAmb()
		cRecursive  := " WITH EstruturaNegativa (xT4N_COMP, xT4N_PROD, xT4N_QTD, xT4N_QTDB, xT4N_DTINI, xT4N_DTFIM, xT4N_FIXA, xT4N_REVINI, xT4N_REVFIM, xT4N_FANTAS, xT4N_GROPC, xT4N_ITOPC, T4N_RECNO) "
		cRecursive  += " AS ( "

		/*QUERY ORIGEM -> QUANTIDADE NEGATIVA*/
		cRecursive  +=   " SELECT T4N.T4N_COMP, T4N.T4N_PROD, T4N.T4N_QTD, T4N.T4N_QTDB, T4N.T4N_DTINI, T4N.T4N_DTFIM, T4N.T4N_FIXA, T4N.T4N_REVINI, T4N.T4N_REVFIM, T4N.T4N_FANTAS, T4N.T4N_GROPC, T4N.T4N_ITOPC, T4N.T4N_RECNO "
		cRecursive  +=  	" FROM    (SELECT        T4N_PROD, T4N_COMP, T4N_QTD, (CASE WHEN T4N_QTDB <= 0 THEN 1 ELSE T4N_QTDB END) T4N_QTDB, T4N_DTINI, T4N_DTFIM, T4N_FIXA, T4N_REVINI, T4N_REVFIM, T4N_FANTAS, T4N_GROPC, T4N_ITOPC, R_E_C_N_O_ T4N_RECNO "
		cRecursive  +=  				" FROM            " + RetSqlName("T4N")
		cRecursive  +=  				" WHERE        (D_E_L_E_T_ = ' ') AND (T4N_FILIAL = '" + xFilial("T4N") + "') AND (T4N_QTD < 0)) T4N "
		cRecursive  +=  	" UNION ALL "

		/*RECURSAO -> PRODUTO PAI, FANTASMA OU NEGATIVO*/
		cRecursive  +=   " SELECT T4N.T4N_COMP, T4N.T4N_PROD, T4N.T4N_QTD, T4N.T4N_QTDB, T4N.T4N_DTINI, T4N.T4N_DTFIM, T4N.T4N_FIXA, T4N.T4N_REVINI, T4N.T4N_REVFIM, T4N.T4N_FANTAS, T4N.T4N_GROPC, T4N.T4N_ITOPC, T4N.T4N_RECNO "
		cRecursive  +=  	" FROM EstruturaNegativa "
		cRecursive  +=  		" INNER JOIN "
		cRecursive  +=  			  " ( SELECT        T4N_PROD, T4N_COMP, T4N_QTD, (CASE WHEN T4N_QTDB <= 0 THEN 1 ELSE T4N_QTDB END) T4N_QTDB, T4N_DTINI, T4N_DTFIM, T4N_FIXA, T4N_REVINI, T4N_REVFIM, T4N_FANTAS, T4N_GROPC, T4N_ITOPC, R_E_C_N_O_ T4N_RECNO "
		cRecursive  +=  				" FROM            " + RetSqlName("T4N")
		cRecursive  +=  				" WHERE        (D_E_L_E_T_ = ' ') AND (T4N_FILIAL = '" + xFilial("T4N") + "') ) T4N "
		cRecursive  +=  			" ON T4N.T4N_COMP = EstruturaNegativa.xT4N_PROD "
		cRecursive  +=  	" WHERE T4N.T4N_FANTAS = 'T' OR T4N.T4N_QTD < 0 "
		cRecursive  +=  " ) "

		/*SELECT RECURSAO COM RELATION PRODUTOS PARA FILTROS*/
		cJoin  +=  " INNER JOIN (SELECT EstruturaNegativa.T4N_RECNO "
		cJoin  +=  " FROM   EstruturaNegativa INNER JOIN "
		cJoin  +=  			   " ( SELECT        COALESCE(HWE.HWE_PROD, HWA.HWA_PROD) HWA_PROD, COALESCE(HWE.HWE_REVATU, HWA.HWA_REVATU) HWA_REVATU, HWA.HWA_ESTORI "
		cJoin  +=  				 " FROM          (SELECT        HWA_PROD, HWA_REVATU, HWA_ESTORI "
		cJoin  +=  								" FROM            " + RetSqlName("HWA")
		cJoin  +=  								" WHERE        (D_E_L_E_T_ = ' ') AND (HWA_FILIAL = '" + xFilial("HWA") + "') AND (HWA_PROSBP = '1')) HWA "
		cJoin  +=  					" LEFT OUTER JOIN "
		cJoin  +=  							  " ( SELECT        HWE_PROD, HWE_REVATU "
		cJoin  +=  								" FROM            " + RetSqlName("HWE")
		cJoin  +=  								" WHERE        (D_E_L_E_T_ = ' ') AND (HWE_FILIAL = '" + xFilial("HWE") + "') ) HWE "
		cJoin  +=  					" ON HWA.HWA_PROD = HWE.HWE_PROD) HWA_COMP "
		cJoin  +=  			" ON HWA_COMP.HWA_PROD = EstruturaNegativa.xT4N_COMP "
		cJoin  +=  			    " AND (HWA_COMP.HWA_ESTORI = ' ' OR HWA_COMP.HWA_ESTORI = EstruturaNegativa.xT4N_PROD) "
		cJoin  +=  		 " INNER JOIN "
		cJoin  +=  			   " ( SELECT        COALESCE (HWE.HWE_PROD, HWA.HWA_PROD) HWA_PROD, COALESCE (HWE.HWE_REVATU, HWA.HWA_REVATU) HWA_REVATU "
		cJoin  +=  				 " FROM          (SELECT        HWA_PROD, HWA_REVATU "
		cJoin  +=  								" FROM            " + RetSqlName("HWA")
		cJoin  +=  								" WHERE        (D_E_L_E_T_ = ' ') AND (HWA_FILIAL = '" + xFilial("HWA") + "')

		//Somente produtos com status diferente de bloqueado.
		If lProdBLQ
			cJoin +=                                         " AND (HWA_BLOQUE <> '1') "
		EndIf

		cJoin  +=  		         " ) HWA "
		cJoin  +=  					" LEFT OUTER JOIN "
		cJoin  +=  							  " ( SELECT        HWE_PROD, HWE_REVATU "
		cJoin  +=  								" FROM            " + RetSqlName("HWE")
		cJoin  +=  								" WHERE        (D_E_L_E_T_ = ' ') AND (HWE_FILIAL = '" + xFilial("HWE") + "')

		cJoin  +=  		                      " ) HWE "
		cJoin  +=  					" ON HWA.HWA_PROD = HWE.HWE_PROD) HWA_PAI "
		cJoin  +=  			" ON HWA_PAI.HWA_PROD = EstruturaNegativa.xT4N_PROD "
		cJoin  +=  		    " AND HWA_PAI.HWA_REVATU BETWEEN EstruturaNegativa.xT4N_REVINI AND EstruturaNegativa.xT4N_REVFIM) SubProdutos "

		aRegJsonSU := GetRegDB("T4N", {{"", cJoin, "SubProdutos.T4N_RECNO = T4N.R_E_C_N_O_"}}, "1 = 1", aFieldSubP, "T4N_COMP,T4N_PROD", cRecursive)
	EndIf
	nTotSubPro := Len(aRegJsonSU)

	oSubProdutos:setFlag("qtd_registros_total", nTotSubPro)
	oSubProdutos:setFlag("qtd_registros_lidos", 0)

	If nTotSubPro > 0
		oSubPJson  := JsonObject():New()
		aAuxSubPrd := Array(sTAM_EST)
	EndIf

	//Processa todos os registros encontrados
	For nIndRegs := 1 to nTotSubPro
		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
		   	oSubProdutos:setFlag("termino_carga", "S") //Grava flag de conclusao global
			Exit
		EndIf

		oRegistros := JsonObject():new()
		cError     := oRegistros:fromJSON(aRegJsonSU[nIndRegs])
		If !Empty(cError)
			cError := oRegistros:fromJSON(JsonString(aRegJsonSU[nIndRegs]))
			If !Empty(cError)
				Loop
				oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegJsonSU[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
			EndIf
		EndIf

		If Empty(cError)
			cComp  := PadR(oRegistros["aRegs"][1]["T4N_COMP"], snTamCod)
			aAuxSubPrd[sEST_FILIAL] := cFilAux
			aAuxSubPrd[sEST_CODPAI] := PadR(oRegistros["aRegs"][1]["T4N_PROD"], snTamCod)
			aAuxSubPrd[sEST_CODFIL] := cComp
			aAuxSubPrd[sEST_QTD]    := Val(oRegistros["aRegs"][1]["T4N_QTD"])
			aAuxSubPrd[sEST_QTDB]   := Val(oRegistros["aRegs"][1]["T4N_QTDB"])
			aAuxSubPrd[sEST_FANT]   := oRegistros["aRegs"][1]["T4N_FANTAS"] == "T"
			aAuxSubPrd[sEST_TRT]    := oRegistros["aRegs"][1]["T4N_SEQ"]
			aAuxSubPrd[sEST_GRPOPC] := oRegistros["aRegs"][1]["T4N_GROPC"]
			aAuxSubPrd[sEST_ITEOPC] := oRegistros["aRegs"][1]["T4N_ITOPC"]
			aAuxSubPrd[sEST_VLDINI] := StoD(oRegistros["aRegs"][1]["T4N_DTINI"])
			aAuxSubPrd[sEST_VLDFIM] := StoD(oRegistros["aRegs"][1]["T4N_DTFIM"])
			aAuxSubPrd[sEST_REVINI] := oRegistros["aRegs"][1]["T4N_REVINI"]
			aAuxSubPrd[sEST_REVFIM] := oRegistros["aRegs"][1]["T4N_REVFIM"]
			aAuxSubPrd[sEST_ALTERN] := oRegistros["aRegs"][1]["T4O_ESTOQ"]
			aAuxSubPrd[sEST_FIXA]   := oRegistros["aRegs"][1]["T4N_FIXA"]
			aAuxSubPrd[sEST_POTEN]  := Val(oRegistros["aRegs"][1]["T4N_POTEN"])
			aAuxSubPrd[sEST_PERDA]  := Val(oRegistros["aRegs"][1]["T4N_PERDA"])

			cChave := cFilAux + cComp
			If oSubPJson[cChave] == Nil
				oSubPJson[cChave] := {}
			EndIf
			aAdd(oSubPJson[cChave], aClone(aAuxSubPrd))

		EndIf

		FreeObj(oRegistros)
		oRegistros := Nil

		oSubProdutos:setFlag("qtd_registros_lidos", 1, , , .T.)
	Next nIndRegs


	If nTotSubPro > 0
		aNames    := oSubPJson:GetNames()
		nTotSubPro := Len(aNames)
		For nIndRegs := 1 to nTotSubPro
			If !oSubProdutos:addRow(aNames[nIndRegs], oSubPJson[aNames[nIndRegs]])
				If oStatus:getStatus("status") == "4"
					Exit
				Else
					oLogs:log(STR0055 + "T4N", "E") //"Erro na carga de dados da tabela para memoria: "
				EndIf
			EndIf
		Next

		//Aguarda Conclus�o da Carga de Produtos - Atualiza Flag de SubProduto
		soDominio:oAglutina:aguardaProdutoCarga()
		For nIndRegs := 1 to nTotSubPro
			cChave := aNames[nIndRegs]
			aSize(aAuxSubPrd, 0)
			If cChave != cOldChave .AND. oProdutos:getRow(1, cChave, Nil, @aAuxSubPrd)
				If oSubPJson[aNames[nIndRegs]][1][sEST_QTD] < 0
					aAuxSubPrd[sPRD_LSUBPR] := .T.
					If !oProdutos:updRow(1, cChave, Nil, aAuxSubPrd)
						If oStatus:getStatus("status") == "4"
							Exit
						Else
							oLogs:log(STR0031 + cTablePrd, "E") //"Erro na carga de dados do arquivo .CSV para memoria: "
						EndIf
					EndIf
					cOldChave := cChave
				EndIf
			EndIf
		Next

		aSize(aNames, 0)
	EndIf

	oLogs:log(STR0078 + cValToChar(MicroSeconds() - nTempoIni)             , "CM") //"Tempo de carga tabela SubProdutos "
	oLogs:logb({|| STR0079 + cValToChar(soDados:tamanhoLista("SUB"))}, "CM") //"Quantidade de SubProdutos carregadas em memoria: "

	//Grava flag de conclusao global
	oSubProdutos:setFlag("termino_carga", "S")

	//Limpa as vari�veis da mem�ria
	aSize(aAuxSubPrd, 0)
	aAuxSubPrd := Nil
	aSize(aFieldSubP, 0)
	aFieldSubP := Nil
	aSize(aNames, 0)
	aNames := Nil
	aSize(aRegJsonSU, 0)
	aRegJsonSU := Nil

	FreeObj(oSubPJson)
	oSubPJson := Nil

Return

/*/{Protheus.doc} entradasOP
Carrega dados de OPs na tabela de Entradas (MAT)
@author douglas.heydt
@since 13/09/2019
@version 1.0
/*/
METHOD entradasOP() CLASS MrpDados_CargaMemoria

	//Verifica se a carga ser� multi-thread
	If ::oDados:oParametros["nThreads"] >= 1 .And. ::oDados:oParametros["cAutomacao"] != "2"
		PCPIPCGO(::oDados:oParametros["cSemaforoThreads"], .F., "PCPCargaOP") //Delega para processamento em Thread
	Else
		PCPCargaOP()
	EndIf

Return

/*/{Protheus.doc} PCPCargaOP
Processa a carga dos dados das OPs
@author marcelo.neumann
@since 08/07/2019
@version 1.0
/*/
Function PCPCargaOP()

	Local nTempoIni  := MicroSeconds()
	Local aAux       := {}
	Local aFieldsOP  := {{"T4Q_PROD"  , "C", "T4Q"}, ;
	                     {"T4Q_SALDO" , "N", "T4Q"}, ;
	                     {"T4Q_DATA"  , "D", "T4Q"}, ;
	                     {"T4Q_OP"    , "C", "T4Q"}, ;
	                     {"T4Q_MOPC"  , "O", "T4Q"}, ;
						 {"R_E_C_N_O_", "N", "T4Q"}, ;
						 {"T4Q_IDREG" , "C", "T4Q"}}
	Local aPeriodos  := {}
	Local aPathOrig  := {}
	Local aRegEmJson := {}
	Local aRegistro  := {}
	Local cChave     := ""
	Local cChaveProd := ""
	Local cError     := ""
	Local cFilAux    := ""
	Local cIDInterme := ""
	Local cOpcional  := ""
	Local cProduto   := ""
	Local cWhere     := ""
	Local cWhereIN   := ""
	Local dEntrega
	Local lEventLog
	Local lHorFirme
	Local nIndOpc    := 0
	Local nIndRegs   := 1
	Local nPeriodo   := 0
	Local nResto     := 0
	Local nTotal     := 0
	Local nTotOPC    := 0
	Local nQtdEntPrv := 0
	Local oLogs
	Local oMatriz
	Local oOpcionais
	Local oRegistros
	Local oStatus
	Local oJsEvtEntr

	//Prepara o ambiente (quando for uma nova thread)
	PrepStatics()
	oMatriz    := soDados:oMatriz
	oLogs      := soDados:oLogs
	oOpcionais := soDominio:oOpcionais
	aPeriodos  := soDados:aPeriodos
	oStatus    := MrpDados_Status():New(soDados:oParametros["ticket"])

	lEventLog := soDados:oParametros["lEventLog"]
	If lEventLog
		oJsEvtEntr := JsonObject():New()
	EndIf

	lHorFirme := soDados:oParametros["lHorizonteFirme"] .AND. "|1.1|" $ soDados:oParametros["cDocumentType"]

	If PreparaAmb()
		cWhere     := WhereT4Q(lHorFirme)

		//Atribui Filtro Relacionado ao Horizonte Firme
		If lHorFirme
			MrpDominio_HorizonteFirme():criaScriptIN(soDados:oParametros["dDataIni"], "HWA", "T4Q", "T4Q_PROD", "T4Q_DATPRI", cWhere, @cWhereIN)
			cWhere   += " AND ((T4Q.T4Q_TIPO <> '1') OR (T4Q.R_E_C_N_O_ IN ("
			cWhere   += cWhereIN
			cWhere   += ")))"
		EndIf
		aRegEmJson := GetRegDB("T4Q", , cWhere, aFieldsOP)

		nTotal     := Len(aRegEmJson)

		oMatriz:setFlag("qtd_registros_total", nTotal)
		oMatriz:setFlag("qtd_registros_lidos", 0)
	EndIf

	If nTotal > 0
		aAux := Array(sTAM_MAT)
	EndIf

	//Processa todos os registros encontrados
	For nIndRegs := 1 To nTotal
		cIDInterme := Nil
		aPathOrig  := {}

		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
			Exit
		EndIf

		oRegistros := JsonObject():new()
		cError     := oRegistros:fromJSON(aRegEmJson[nIndRegs])
		If !Empty(cError)
			cError := oRegistros:fromJSON(JsonString(aRegEmJson[nIndRegs]))
			If !Empty(cError)
				Loop
				oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegEmJson[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
			EndIf
		EndIf

		cProduto := PadR(oRegistros["aRegs"][1]["T4Q_PROD"], snTamCod)
		dEntrega := ConvDatPer(StoD(oRegistros["aRegs"][1]["T4Q_DATA"]), aPeriodos, @nPeriodo)

		//Loga Evento 006 - Documento planejado em atraso
		If soDominio:oParametros["lEventLog"] .AND. StoD(oRegistros["aRegs"][1]["T4Q_DATA"]) < soDados:oParametros["dDataIni"]
			soDominio:oEventos:loga("006", cProduto, StoD(oRegistros["aRegs"][1]["T4Q_DATA"]), {StoD(oRegistros["aRegs"][1]["T4Q_DATA"]), oRegistros["aRegs"][1]["T4Q_OP"], "", "T4Q"})
		EndIf

		//Tratamento para quando possui o Opcional
		If oRegistros["aRegs"][1]["T4Q_MOPC"] != NIL .AND. !Empty(oRegistros["aRegs"][1]["T4Q_MOPC"]["OPTIONAL"])
			oOpcionais:montaT4QPath(oRegistros["aRegs"][1]["T4Q_IDREG"], oRegistros["aRegs"][1]["T4Q_OP"], @aPathOrig)
			nTotOPC := Len(aPathOrig)
			For nIndOpc := 1 to nTotOPC
				cIDInterme := ""
				cOpcional  := oOpcionais:converteJsonEmID(oRegistros["aRegs"][1]["T4Q_MOPC"]:toJson(),,, Val(oRegistros["aRegs"][1]["R_E_C_N_O_"]), aPathOrig[nIndOpc][1], @cIDInterme)
				cOpcional  := cIDInterme
				cChaveProd := cProduto + Iif(Empty(cOpcional),"", "|" + cOpcional)
				cChave     := cFilAux + DtoS(dEntrega) + cChaveProd
				aSize(aRegistro, 0)

				nQtdEntPrv := aPathOrig[nIndOpc][2]

				//Guarda o registro na Matriz
				oMatriz:trava(cChave)
				If oMatriz:getRow(1, cChave, Nil, @aRegistro, .F., .T.)
					aRegistro[sMAT_ENTPRE] += nQtdEntPrv

					If !oMatriz:updRow(1, cChave, Nil, aRegistro, .F., .T.)
						If oStatus:getStatus("status") == "4"
							Exit
						Else
							oLogs:log(STR0055 + "T4Q", "E") //"Erro na carga de dados da tabela para memoria: "
						EndIf
					EndIf
				Else
					aAux[sMAT_FILIAL] := cFilAux
					aAux[sMAT_DATA]   := dEntrega
					aAux[sMAT_PRODUT] := cProduto
					aAux[sMAT_SLDINI] := 0
					aAux[sMAT_ENTPRE] := nQtdEntPrv
					aAux[sMAT_SAIPRE] := 0
					aAux[sMAT_SAIEST] := 0
					aAux[sMAT_SALDO]  := 0
					aAux[sMAT_NECESS] := 0
					aAux[sMAT_TPREPO] := " "
					aAux[sMAT_EXPLOD] := .F.
					aAux[sMAT_THREAD] := -1
					aAux[sMAT_IDOPC]  := cOpcional
					aAux[sMAT_DTINI]  := dEntrega

					If !oMatriz:addRow(cChave, aAux, , , , cFilAux + cChaveProd)
						If oStatus:getStatus("status") == "4"
							Exit
						Else
							oLogs:log(STR0055 + "T4Q", "E") //"Erro na carga de dados da tabela para memoria: "
						EndIf
					EndIf

					//Cria lista deste produto para registrar os periodos
					If !oMatriz:existList("Periodos_Produto_" + cChaveProd)
						oMatriz:createList("Periodos_Produto_" + cChaveProd)
					EndIf
					oMatriz:setItemList("Periodos_Produto_" + cChaveProd, cValToChar(nPeriodo), nPeriodo)

					If !Empty(cOpcional)
						oOpcionais:adicionaLista(cProduto, cOpcional)
					EndIf
				EndIf
				oMatriz:destrava(cChave)

				If lEventLog
					If oJsEvtEntr[cChaveProd] == Nil
						oJsEvtEntr[cChaveProd] := {}
					EndIf

					If oJsEvtEntr[cChaveProd] == Nil
						oJsEvtEntr[cChaveProd] := {}
						oJsEvtEntr[cChaveProd+"pri"] := If(soDados:possuiEstrutura(cProduto), 1, 30)
					EndIf

					//Adiciona {cDocumento, cItem, cAlias, nPeriodo, nQuantidade, nControle, lPossuiLog, nPriConsumo, nPrioridade}
					aAdd(oJsEvtEntr[cChaveProd], {oRegistros["aRegs"][1]["T4Q_OP"], "", "T4Q", nPeriodo, nQtdEntPrv, nQtdEntPrv, .F., 0, oJsEvtEntr[cChaveProd+"pri"]})
				EndIf
			Next
		Else
			cOpcional := ""
			cChaveProd := cProduto + Iif(Empty(cOpcional),"", "|" + cOpcional)
			cChave     := cFilAux + DtoS(dEntrega) + cChaveProd
			aSize(aRegistro, 0)

			nQtdEntPrv := Val(oRegistros["aRegs"][1]["T4Q_SALDO"])

			//Guarda o registro na Matriz
			oMatriz:trava(cChave)
			If oMatriz:getRow(1, cChave, Nil, @aRegistro, .F., .T.)
				aRegistro[sMAT_ENTPRE] += nQtdEntPrv

				If !oMatriz:updRow(1, cChave, Nil, aRegistro, .F., .T.)
					If oStatus:getStatus("status") == "4"
						Exit
					Else
						oLogs:log(STR0055 + "T4Q", "E") //"Erro na carga de dados da tabela para memoria: "
					EndIf
				EndIf
			Else
				aAux[sMAT_FILIAL] := cFilAux
				aAux[sMAT_DATA]   := dEntrega
				aAux[sMAT_PRODUT] := cProduto
				aAux[sMAT_SLDINI] := 0
				aAux[sMAT_ENTPRE] := nQtdEntPrv
				aAux[sMAT_SAIPRE] := 0
				aAux[sMAT_SAIEST] := 0
				aAux[sMAT_SALDO]  := 0
				aAux[sMAT_NECESS] := 0
				aAux[sMAT_TPREPO] := " "
				aAux[sMAT_EXPLOD] := .F.
				aAux[sMAT_THREAD] := -1
				aAux[sMAT_IDOPC]  := cOpcional
				aAux[sMAT_DTINI]  := dEntrega

				If !oMatriz:addRow(cChave, aAux, , , , cFilAux + cChaveProd)
					If oStatus:getStatus("status") == "4"
						Exit
					Else
						oLogs:log(STR0055 + "T4Q", "E") //"Erro na carga de dados da tabela para memoria: "
					EndIf
				EndIf

				//Cria lista deste produto para registrar os periodos
				If !oMatriz:existList("Periodos_Produto_" + cChaveProd)
					oMatriz:createList("Periodos_Produto_" + cChaveProd)
				EndIf
				oMatriz:setItemList("Periodos_Produto_" + cChaveProd, cValToChar(nPeriodo), nPeriodo)

				If !Empty(cOpcional)
				oOpcionais:adicionaLista(cProduto, cOpcional)
				EndIf
			EndIf
			oMatriz:destrava(cChave)

			If lEventLog
				If oJsEvtEntr[cChaveProd] == Nil
					oJsEvtEntr[cChaveProd] := {}
				EndIf
				//Adiciona {cDocumento, cItem, cAlias, nPeriodo, nQuantidade, nControle}
				aAdd(oJsEvtEntr[cChaveProd], {oRegistros["aRegs"][1]["T4Q_OP"], "", "T4Q", nPeriodo, nQtdEntPrv, nQtdEntPrv, .F., 0, 0})
			EndIf
		EndIf

		FreeObj(oRegistros)
		oRegistros := Nil

		//Atualiza contador de registros lidos
		nResto := Mod(nIndRegs, 50)
		If nResto == 0
			oMatriz:setFlag("qtd_registros_lidos", 50, , , .T.)
		EndIf
	Next nIndRegs

	If nResto > 0
		oMatriz:setFlag("qtd_registros_lidos", nResto, , , .T.)
	EndIf

	oLogs:log(STR0059 + cValToChar(MicroSeconds() - nTempoIni)       , "CM") //"Tempo de carga de Ordens de Produ��o "
	oLogs:log(STR0060 + cValToChar(nTotal)                           , "CM") //"Quantidade de Ordens de Produ��o carregadas em mem�ria: "
	oLogs:logb({|| STR0026 + cValToChar(soDados:tamanhoLista("MAT"))}, "CM") //"Quantidade de itens da matriz carregados em memoria: "

	//Grava flag de conclus�o global
	oMatriz:setFlag("termino_carga_OP", "S")

	If lEventLog
		soDominio:oEventos:incluiEntradas(oJsEvtEntr, "T4Q")
	EndIf

	//Limpa os arrays da mem�ria
	aSize(aAux, 0)
	aAux := Nil
	aSize(aFieldsOP, 0)
	aFieldsOP := Nil
	aSize(aRegEmJson, 0)
	aRegEmJson := Nil
	aSize(aRegistro, 0)
	aRegistro := Nil
	FreeObj(oJsEvtEntr)
	oJsEvtEntr := Nil

Return

/*/{Protheus.doc} entradasEmpenho
Carrega dados de Empenhos na tabela de Entradas (MAT)
@author marcelo.neumann
@since 16/03/2020
@version 1.0
/*/
METHOD entradasEmpenho() CLASS MrpDados_CargaMemoria

	//Verifica se a carga ser� multi-thread
	If ::oDados:oParametros["nThreads"] >= 1
		PCPIPCGO(::oDados:oParametros["cSemaforoThreads"], .F., "PCPCargaEm") //Delega para processamento em Thread
	Else
		PCPCargaEm()
	EndIf

Return

/*/{Protheus.doc} PCPCargaEm
Processa a carga dos dados dos Empenhos
@author marcelo.neumann
@since 16/03/2020
@version 1.0
/*/
Function PCPCargaEm()

	Local nTempoIni  := MicroSeconds()
	Local aAux       := {}
	Local aFieldsEmp := {{"T4Q_MOPC"  , "O", "T4Q"}, ;
						 {"R_E_C_N_O_", "N", "T4Q"}, ;
	                     {"T4S_PROD"  , "C", "T4S"}, ;
	                     {"T4S_QTD"   , "N", "T4S"}, ;
	                     {"T4S_DT"    , "D", "T4S"}, ;
	                     {"T4S_QSUSP" , "N", "T4S"}, ;
						 {"T4S_IDREG" , "C", "T4S"}}
	Local aPeriodos  := {}
	Local aPathOrig  := {}
	Local aPrepAglut := {}
	Local aRegEmJson := {}
	Local aRegistro  := {}
	Local cChave     := ""
	Local cChaveProd := ""
	Local cError     := ""
	Local cFilAux    := ""
	Local cIDInterme := ""
	Local cJoinT4S   := ""
	Local cOpcional  := ""
	Local cProduto   := ""
	Local cWhere     := ""
	Local cWhereIN   := ""
	Local dEntrega
	Local dFimDemand
	Local dLimite
	Local lHorFirme
	Local nIndOpc    := 0
	Local nIndRegs   := 1
	Local nTotOPC    := 0
	Local nTotal     := 0
	Local nPeriodo   := 0
	Local nResto     := 0
	Local nQtdSaiPre := 0
	Local oLogs
	Local oMatriz
	Local oOpcionais
	Local oRegistros
	Local oStatus

	//Prepara o ambiente (quando for uma nova thread)
	PrepStatics()
	oMatriz    := soDados:oMatriz
	oLogs      := soDados:oLogs
	oOpcionais := soDominio:oOpcionais
	aPeriodos  := soDados:aPeriodos
	dFimDemand := Iif(soDados:oParametros["dFimDemandas"] == Nil, DataLimite(), soDados:oParametros["dFimDemandas"])
	oStatus    := MrpDados_Status():New(soDados:oParametros["ticket"])

	//Corrige data fim demandas superior ao �ltimo periodo
	If dFimDemand > soDominio:oPeriodos:ultimaDataDoMRP()
		If soDominio:oParametros["lEventLog"]
			dLimite := soDominio:oPeriodos:ultimaDataDoMRP()
			aAdd(aFieldsEmp, {"T4S_OP", "C", "T4S"})
		Else
			dFimDemand := soDominio:oPeriodos:ultimaDataDoMRP()
		EndIf
	EndIf

	lHorFirme := soDados:oParametros["lHorizonteFirme"] .AND. "|1.1|" $ soDados:oParametros["cDocumentType"]

	//Busca os registros a serem processados
	If PreparaAmb()
		//Cl�usula ON do INNER JOIN entre T4Q e T4S (s� trazer os empenhos que tiverem OP)
		cJoinT4S := "T4Q.T4Q_OP     = T4S.T4S_OP"
		cJoinT4S += " AND T4S.D_E_L_E_T_ = ' '"
		cJoinT4S += " AND T4S.T4S_FILIAL = '" + xFilial("T4S") + "'"
		If !("|2|" $ soDados:oParametros["cDocumentType"])
			cJoinT4S += " AND (T4S.T4S_QTD - T4S.T4S_QSUSP) <> 0"
		EndIf
		cJoinT4S += " AND " + soDominio:oSeletivos:scriptExistsProdutoSQL("T4S.T4S_PROD")    //Valida o filtro de Tipo ou Grupo de Produtos

		//Par�metro para filtrar os armaz�ns
		If !Empty(soDados:oParametros["cWarehouses"])
			cJoinT4S += " AND " + soDominio:oSeletivos:scriptInSQL("T4S.T4S_LOCAL", "cWarehouses")
		EndIf

		cWhere     := WhereT4Q(lHorFirme)

		//Atribui Filtro Relacionado ao Horizonte Firme
		If lHorFirme
			MrpDominio_HorizonteFirme():criaScriptIN(soDados:oParametros["dDataIni"], "HWA", "T4Q", "T4Q_PROD", "T4Q_DATPRI", cWhere, @cWhereIN)
			cWhere   += " AND ((T4Q.T4Q_TIPO <> '1') OR (T4Q.R_E_C_N_O_ IN ("
			cWhere   += cWhereIN
			cWhere   += ")))"
		EndIf
		aRegEmJson := GetRegDB("T4Q", {{"T4S", "INNER JOIN", cJoinT4S}}, cWhere, aFieldsEmp)

		nTotal     := Len(aRegEmJson)

		oMatriz:setFlag("qtd_registros_total", nTotal)
		oMatriz:setFlag("qtd_registros_lidos", 0)
	EndIf

	If nTotal > 0
		aAux := Array(sTAM_MAT)
	EndIf

	//Flag de controle das threads referentes ao aglutina:prepara()
	soDados:oMatriz:setFlag("ExecPrepAglutina", 0)

	//Processa todos os registros encontrados
	For nIndRegs := 1 To nTotal
		cIDInterme := ""
		aPathOrig  := {}

		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
			Exit
		EndIf

		oRegistros := JsonObject():new()
		cError     := oRegistros:fromJSON(aRegEmJson[nIndRegs])
		If !Empty(cError)
			cError := oRegistros:fromJSON(JsonString(aRegEmJson[nIndRegs]))
			If !Empty(cError)
				Loop
				oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegEmJson[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
			EndIf
		EndIf

		cProduto := PadR(oRegistros["aRegs"][1]["T4S_PROD"], snTamCod)
		dEntrega := ConvDatPer(StoD(oRegistros["aRegs"][1]["T4S_DT"]), soDados:aPeriodos, @nPeriodo, .T.)

		If !Empty(oRegistros["aRegs"][1]["T4S_PROD"])
			//Tratamento para quando possui o Opcional
			If oRegistros["aRegs"][1]["T4Q_MOPC"] != nil .and. !Empty(oRegistros["aRegs"][1]["T4Q_MOPC"]["OPTIONAL"])
				oOpcionais:montaT4SPath(oRegistros["aRegs"][1]["T4S_IDREG"], cProduto, @aPathOrig)
			EndIf

			nTotOPC := Len(aPathOrig)
			If nTotOPC > 0
				For nIndOpc := 1 to nTotOPC
					cIDInterme := ""
					cOpcional  := oOpcionais:converteJsonEmID(oRegistros["aRegs"][1]["T4Q_MOPC"]:toJson(),,, Val(oRegistros["aRegs"][1]["R_E_C_N_O_"]), aPathOrig[nIndOpc][1], @cIDInterme)
					cOpcional  := cIDInterme
					cChaveProd := cProduto + Iif(Empty(cOpcional),"", "|" + cOpcional)
					cChave     := cFilAux + DtoS(dEntrega) + cChaveProd
					aSize(aRegistro, 0)

					//Loga Evento 005 - Data de necessidade invalida - Data posterior ao prazo maximo do MRP
					If soDominio:oParametros["lEventLog"] .AND. dLimite != Nil .AND. StoD(oRegistros["aRegs"][1]["T4S_DT"]) > dLimite
						soDominio:oEventos:loga("005", cChaveProd, StoD(oRegistros["aRegs"][1]["T4S_DT"]), {cChaveProd,  aPathOrig[nIndOpc][2], StoD(oRegistros["aRegs"][1]["T4S_DT"]), "T4S", oRegistros["aRegs"][1]["T4S_OP"]})
						Loop
					EndIf

					//Guarda o registro na Matriz
					oMatriz:trava(cChave)
					If oMatriz:getRow(1, cChave, Nil, @aRegistro, .F., .T.)
						aRegistro[sMAT_SAIPRE] += aPathOrig[nIndOpc][2]

						If !oMatriz:updRow(1, cChave, Nil, aRegistro, .F., .T.)
							If oStatus:getStatus("status") == "4"
								Exit
							Else
								oLogs:log(STR0055 + "T4S", "E") //"Erro na carga de dados da tabela para memoria: "
							EndIf
						EndIf
					Else
						aAux[sMAT_FILIAL] := cFilAux
						aAux[sMAT_DATA]   := dEntrega
						aAux[sMAT_PRODUT] := cProduto
						aAux[sMAT_SLDINI] := 0
						aAux[sMAT_ENTPRE] := 0
						aAux[sMAT_SAIPRE] := aPathOrig[nIndOpc][2]
						aAux[sMAT_SAIEST] := 0
						aAux[sMAT_SALDO]  := 0
						aAux[sMAT_NECESS] := 0
						aAux[sMAT_TPREPO] := " "
						aAux[sMAT_EXPLOD] := .F.
						aAux[sMAT_THREAD] := -1
						aAux[sMAT_IDOPC]  := cOpcional
						aAux[sMAT_DTINI]  := dEntrega

						If oMatriz:addRow(cChave, aAux, , , , cFilAux + cChaveProd)
							//Cria lista deste produto para registrar os periodos
							If !oMatriz:existList("Periodos_Produto_" + cChaveProd)
								oMatriz:createList("Periodos_Produto_" + cChaveProd)
							EndIf
							oMatriz:setItemList("Periodos_Produto_" + cChaveProd, cValToChar(nPeriodo), nPeriodo)
						Else
							If oStatus:getStatus("status") == "4"
								Exit
							Else
								oLogs:log(STR0055 + "T4S", "E") //"Erro na carga de dados da tabela para memoria: "
							EndIf
						EndIf
					EndIf
					oMatriz:destrava(cChave)

					If soDominio:oParametros["lRastreia"] .and. dEntrega <= dFimDemand
						//Se roda em multi-thread, agrupa registros para delegar � outra thread
						If soDados:oParametros["nThreads"] >= 1
							aAdd(aPrepAglut, {oRegistros["aRegs"][1]["T4S_IDREG"], ;
											  cProduto,                            ;
											  cOpcional,                           ;
											  aPathOrig[nIndOpc][2],               ;
											  nPeriodo})

							//Delega a cada 1000 empenhos
							If Mod(nIndRegs, 1000) == 0
								soDados:oMatriz:setFlag("ExecPrepAglutina",1,,,.T.)
								PCPIPCGO(soDados:oParametros["cSemaforoThreads"], .F., "PCPAglPrep", aPrepAglut, .T.)
								aSize(aPrepAglut,0)
							EndIf
						Else
							PCPAglPrep({{oRegistros["aRegs"][1]["T4S_IDREG"], ;
									     cProduto,                            ;
									     cOpcional,                           ;
									     aPathOrig[nIndOpc][2],               ;
									     nPeriodo}},                          ;
										 .F.)
						EndIf
					EndIf
				Next
			Else
				cOpcional  := ""
				cChaveProd := cProduto + Iif(Empty(cOpcional),"", "|" + cOpcional)
				cChave     := cFilAux + DtoS(dEntrega) + cChaveProd
				aSize(aRegistro, 0)

				If !("|2|" $ soDados:oParametros["cDocumentType"])
					nQtdSaiPre := Val(oRegistros["aRegs"][1]["T4S_QTD"]) - Val(oRegistros["aRegs"][1]["T4S_QSUSP"])
				Else
					nQtdSaiPre := Val(oRegistros["aRegs"][1]["T4S_QTD"])
				EndIf

				//Loga Evento 005 - Data de necessidade invalida - Data posterior ao prazo maximo do MRP
				If soDominio:oParametros["lEventLog"] .AND. dLimite != Nil .AND. StoD(oRegistros["aRegs"][1]["T4S_DT"]) > dLimite
					soDominio:oEventos:loga("005", cProduto, StoD(oRegistros["aRegs"][1]["T4S_DT"]), {cProduto, nQtdSaiPre, StoD(oRegistros["aRegs"][1]["T4S_DT"]), "T4S", oRegistros["aRegs"][1]["T4S_OP"]})
					Loop
				EndIf

				//Guarda o registro na Matriz
				oMatriz:trava(cChave)
				If oMatriz:getRow(1, cChave, Nil, @aRegistro, .F., .T.)

					aRegistro[sMAT_SAIPRE] += nQtdSaiPre

					If !oMatriz:updRow(1, cChave, Nil, aRegistro, .F., .T.)
						If oStatus:getStatus("status") == "4"
							Exit
						Else
							oLogs:log(STR0055 + "T4S", "E") //"Erro na carga de dados da tabela para memoria: "
						EndIf
					EndIf
				Else
					aAux[sMAT_FILIAL] := cFilAux
					aAux[sMAT_DATA]   := dEntrega
					aAux[sMAT_PRODUT] := cProduto
					aAux[sMAT_SLDINI] := 0
					aAux[sMAT_ENTPRE] := 0
					aAux[sMAT_SAIPRE] := nQtdSaiPre
					aAux[sMAT_SAIEST] := 0
					aAux[sMAT_SALDO]  := 0
					aAux[sMAT_NECESS] := 0
					aAux[sMAT_TPREPO] := " "
					aAux[sMAT_EXPLOD] := .F.
					aAux[sMAT_THREAD] := -1
					aAux[sMAT_IDOPC]  := cOpcional
					aAux[sMAT_DTINI]  := dEntrega

					If oMatriz:addRow(cChave, aAux, , , , cFilAux + cChaveProd)
						//Cria lista deste produto para registrar os periodos
						If !oMatriz:existList("Periodos_Produto_" + cChaveProd)
							oMatriz:createList("Periodos_Produto_" + cChaveProd)
						EndIf
						oMatriz:setItemList("Periodos_Produto_" + cChaveProd, cValToChar(nPeriodo), nPeriodo)
					Else
						If oStatus:getStatus("status") == "4"
							Exit
						Else
							oLogs:log(STR0055 + "T4S", "E") //"Erro na carga de dados da tabela para memoria: "
						EndIf
					EndIf
				EndIf
				oMatriz:destrava(cChave)

				If soDominio:oParametros["lRastreia"] .and. dEntrega <= dFimDemand
					//Se roda em multi-thread, agrupa registros para delegar � outra thread
					If soDados:oParametros["nThreads"] >= 1
						aAdd(aPrepAglut, {oRegistros["aRegs"][1]["T4S_IDREG"],   ;
										  cProduto,                              ;
										  cOpcional,                             ;
								  		  Val(oRegistros["aRegs"][1]["T4S_QTD"]),;
										  nPeriodo})

						//Delega a cada 1000 empenhos
						If Mod(nIndRegs, 1000) == 0
							soDados:oMatriz:setFlag("ExecPrepAglutina",1,,,.T.)
							PCPIPCGO(soDados:oParametros["cSemaforoThreads"], .F., "PCPAglPrep", aPrepAglut, .T.) //Delega para processamento em Thread
							aSize(aPrepAglut,0)
						EndIf
					Else
						PCPAglPrep({{oRegistros["aRegs"][1]["T4S_IDREG"],   ;
								     cProduto,                              ;
								     cOpcional,                             ;
								     Val(oRegistros["aRegs"][1]["T4S_QTD"]),;
								     nPeriodo}},                            ;
									 .F.)
					EndIf
				EndIf
			EndIf
		EndIf

		FreeObj(oRegistros)
		oRegistros := Nil

		//Atualiza contador de registros lidos
		nResto := Mod(nIndRegs, 50)
		If nResto == 0
			oMatriz:setFlag("qtd_registros_lidos", 50, , , .T.)
		EndIf
	Next nIndRegs

	//Se roda em multi-thread e o agrupador n�o chegou em 1000, chama o aglutina:prepara
	If soDados:oParametros["nThreads"] >= 1
		If Mod(nIndRegs-1, 1000) > 0
			PCPAglPrep(aPrepAglut, .F.)
			aSize(aPrepAglut,0)
		EndIf
	EndIf

	If nResto > 0
		oMatriz:setFlag("qtd_registros_lidos", nResto, , , .T.)
	EndIf

	If soDominio:oParametros["lRastreia"]
		//Aguarda encerrar todas as threads para incluir a rastreabilidade
		While oMatriz:getFlag("ExecPrepAglutina") <> 0
			Sleep(500)
		End

		//Inclui rastreabilidade dos empenhos
		soDominio:oAglutina:incluiRastreios("2")  //2 - Carga de Empenhos
	EndIf

	oLogs:log(STR0076 + cValToChar(MicroSeconds() - nTempoIni)       , "CM") //"Tempo de carga de Empenhos "
	oLogs:log(STR0077 + cValToChar(nTotal)                           , "CM") //"Quantidade de Empenhos carregados em mem�ria: "
	oLogs:logb({|| STR0026 + cValToChar(soDados:tamanhoLista("MAT"))}, "CM") //"Quantidade de itens da matriz carregados em memoria: "

	//Grava flag de conclus�o global
	oMatriz:setFlag("termino_carga_Emp", "S")

	//Limpa os arrays da mem�ria
	aSize(aAux, 0)
	aAux := Nil
	aSize(aFieldsEmp, 0)
	aFieldsEmp := Nil
	aSize(aRegEmJson, 0)
	aRegEmJson := Nil
	aSize(aRegistro, 0)
	aRegistro := Nil

Return

/*/{Protheus.doc} WhereT4Q
Retorna o filtro para busca das Ordens de Produ��o (usado tamb�m na carga dos Empenhos)
@author marcelo.neumann
@since 16/03/2019
@version 1.0
@param 01 lHorFirme, l�gico, indica se deve considerar o horizonte firme de documentos previstos
@return cWhere, caracter, filtro de busca das OPs
/*/
Static Function WhereT4Q(lHorFirme)

	Local cWhere     := ""
	Local cWhereTipo := ""
	Local cWhereSitu := ""
	Local dLimite    := soDominio:oPeriodos:ultimaDataDoMRP()

	cWhereTipo := ANDT4QTIPO(lHorFirme)

	cWhereSitu := ANDT4QSITU()

	cWhere := " T4Q.T4Q_SALDO > 0 " + " AND " + cWhereTipo + " AND " + cWhereSitu

	If !Empty(dLimite)
		cWhere += " AND T4Q.T4Q_DATA <= '" + DtoS(dLimite) + "'"
	EndIf

	//S� considera o PA da OP para filtragem quando n�o � utilizado o seletivo de produtos, neste caso � utilizado o filtro pelo componente (T4S)
	If !soDominio:oSeletivos:lSeletivoProduto
		cWhere += " AND " + soDominio:oSeletivos:scriptExistsProdutoSQL("T4Q.T4Q_PROD")
	EndIf

	cWhere += " AND T4Q.T4Q_FILIAL = '" + xFilial("T4Q") + "'"

	//Par�metro para filtrar os armaz�ns
	If !Empty(soDados:oParametros["cWarehouses"])
		cWhere += " AND " + soDominio:oSeletivos:scriptInSQL("T4Q.T4Q_LOCAL", "cWarehouses")
	EndIf

Return cWhere

/*/{Protheus.doc} ANDT4QTIPO
Retorna o filtro do Tipo da OP de acordo com o que foi informado na tela
@author renan.roeder
@since 14/07/2020
@version 1.0
@param 01 lHorFirme, l�gico, indica se deve considerar o horizonte firme de documentos previstos
@return cCondicao, caracter, filtro do tipo das OPs
/*/
Static Function ANDT4QTIPO(lHorFirme)
	Local cCondicao := ""

	cCondicao := "(T4Q.T4Q_TIPO = '4' "

	If "|1.3|" $ soDados:oParametros["cDocumentType"] .OR. lHorFirme
		cCondicao += "OR T4Q.T4Q_TIPO = '1' "
	EndIf
	cCondicao += ")"

Return cCondicao

/*/{Protheus.doc} ANDT4QSITU
Retorna o filtro da Situa��o da OP de acordo com o que foi informado na tela
@author renan.roeder
@since 14/07/2020
@version 1.0
@return cCondicao, caracter, filtro do tipo das OPs
/*/
Static Function ANDT4QSITU
	Local cCondicao := ""

	cCondicao := "(T4Q.T4Q_SITUA = '1' "

	//cDocumentType = 3 (Sacramentadas) = T4Q.T4Q_SITUA = '2' (Sacramentadas)
	If "|3|" $ soDados:oParametros["cDocumentType"]
		cCondicao += " OR T4Q.T4Q_SITUA = '2'"
	EndIf

	//cDocumentType = 2 (Suspensas) = T4Q.T4Q_SITUA = '3' (Suspensas)
	If "|2|" $ soDados:oParametros["cDocumentType"]
		cCondicao += " OR T4Q.T4Q_SITUA = '3'"
	EndIf
	cCondicao += ")"

Return cCondicao


/*/{Protheus.doc} PCPAglPrep
Prepara o empenho no fonte de Aglutina��o
@author marcelo.neumann
@since 18/03/2019
@version 1.0
@param 01 aPrepAglut, array, indica se a carga ser� feita via arquivo CSV ou via tabela
@return Nil
/*/
Function PCPAglPrep(aPrepAglut, lThread)

	Local nTotEmp := Len(aPrepAglut)
	Local nIndex  := 0

	PrepStatics()

	For nIndex := 1 To nTotEmp
		soDominio:oAglutina:prepara("2"                                  ,; //2 - Carga de Empenhos
									"Pr�-OP"                             ,;
									{"Pr�-OP", aPrepAglut[nIndex][1], ""},;
									aPrepAglut[nIndex][1]                ,;
									aPrepAglut[nIndex][2]                ,;
									aPrepAglut[nIndex][3]                ,;
									""                                   ,;
									aPrepAglut[nIndex][4]                ,;
									aPrepAglut[nIndex][5]                )
	Next nIndex

	If lThread
		soDados:oMatriz:setFlag("ExecPrepAglutina",-1,,,.T.)
	EndIf

Return

/*/{Protheus.doc} entradasSC
Carrega dados de solicita��es de compra na tabela de Entradas Firmes (MAT)
@author douglas.heydt
@since 09/09/2019
@version 1.0
/*/
METHOD entradasSC() CLASS MrpDados_CargaMemoria

	//Verifica se a carga ser� multi-thread
	If ::oDados:oParametros["nThreads"] >= 1 .And. ::oDados:oParametros["cAutomacao"] != "2"
		PCPIPCGO(::oDados:oParametros["cSemaforoThreads"], .F., "PCPCargaSC", ::oDados:oParametros["lCargaScCSV"]) //Delega para processamento em Thread
	Else
		PCPCargaSC(::oDados:oParametros["lCargaScCSV"]) //Execucao single-thread
	EndIf

Return

/*/{Protheus.doc} PCPCargaSC
Processa a carga dos dados das solicita��es de compra
@author marcelo.neumann
@since 08/07/2019
@version 1.0
@param 01 lCargaCSV, l�gico, indica se a carga ser� feita via arquivo CSV ou via tabela
/*/
Function PCPCargaSC(lCargaCSV)

	Local nTempoIni  := MicroSeconds()
	Local aAux       := {}
	Local aFields    := {{"T4T_PROD" , "C", "T4T", "" }, ;
	                     {"T4T_QTD"  , "N", "T4T", "" }, ;
	                     {"T4T_DTENT", "D", "T4T", "" }, ;
	                     {"T4T_QUJE" , "N", "T4T", "0"}}
	Local aPeriodos  := {}
	Local aRegEmJson := {}
	Local aRegistro  := {}
	Local cChave     := ""
	Local cError     := ""
	Local cFilAux    := ""
	Local cProduto   := ""
	Local cWhere 	 := ""
	Local cWhereIN   := ""
	Local cJoinT4Q   := ""
	Local dEntrega
	Local dLimite    := Nil
	Local lEventLog
	Local lHorFirme
	Local nIndRegs   := 1
	Local nPeriodo   := 0
	Local nTotal     := 0
	Local nQtdEntPrv := 0
	Local oLogs
	Local oMatriz
	Local oRegistros
	Local oStatus
	Local oJsEvtEntr

	//Prepara o ambiente (quando for uma nova thread)
	PrepStatics()
	oMatriz    := soDados:oMatriz
	oLogs      := soDados:oLogs
	aPeriodos  := soDados:aPeriodos
	dLimite    := soDominio:oPeriodos:ultimaDataDoMRP()
	oStatus    := MrpDados_Status():New(soDados:oParametros["ticket"])

	lEventLog := soDados:oParametros["lEventLog"]
	If lEventLog
		oJsEvtEntr := JsonObject():New()
	EndIf

	lHorFirme := soDados:oParametros["lHorizonteFirme"] .AND. "|1.1|" $ soDados:oParametros["cDocumentType"]

	//Adiciona Documento e Sequ�ncia ao gerar Log de Eventos
	If lEventLog
		aAdd(aFields, {"T4T_NUM"  , "C", "T4T", "" })
		aAdd(aFields, {"T4T_SEQ"  , "C", "T4T", "" })
	EndIf

	//Busca os registros a serem processados (via CSV ou tabela)
	If lCargaCSV
		aRegEmJson := CsvToJson("entradasSC.csv", aFields)
	Else
		If PreparaAmb()

			cJoinT4Q := "T4Q.T4Q_FILIAL = '" + xFilial("T4T") + "'"
			cJoinT4Q += " AND T4Q.T4Q_OP = T4T.T4T_OP"
			cJoinT4Q += " AND T4Q.D_E_L_E_T_ = ' '"

			cWhere := " (( " + ANDT4QTIPO(lHorFirme) + " AND " + ANDT4QSITU() + " ) OR T4T.T4T_OP = ' ')"

			cWhere += " AND ( T4T.T4T_TIPO = '1' "

			If "|1.3|" $ soDados:oParametros["cDocumentType"] .OR. lHorFirme
				cWhere +=  "OR T4T.T4T_TIPO = '2' "
			EndIf

			cWhere += ")"

			If !Empty(dLimite)
				cWhere += " AND T4T.T4T_DTENT <= '" + DtoS(dLimite) + "' "
			EndIf

			//Valida o filtro de Tipo ou Grupo de Produtos
			cWhere += " AND " + soDominio:oSeletivos:scriptExistsProdutoSQL("T4T.T4T_PROD")
			cWhere += " AND T4T.T4T_FILIAL = '" + xFilial("T4T") + "' "

			//Par�metro para filtrar os armaz�ns
			If !Empty(soDados:oParametros["cWarehouses"])
				cWhere += " AND " + soDominio:oSeletivos:scriptInSQL("T4T.T4T_LOCAL", "cWarehouses")
			EndIf

			//Atribui Filtro Relacionado ao Horizonte Firme
			If lHorFirme
				MrpDominio_HorizonteFirme():criaScriptIN(soDados:oParametros["dDataIni"], "HWA", "T4T", "T4T_PROD", "T4T_DTENT", cWhere, @cWhereIN)
				cWhere   += " AND ((T4T.T4T_TIPO <> '2') OR (T4T.R_E_C_N_O_ IN ("
				cWhere   += cWhereIN
				cWhere   += ")))"
			EndIf
			aRegEmJson := GetRegDB("T4T", {{"T4Q", "LEFT JOIN", cJoinT4Q}}, cWhere, aFields)

		EndIf
	EndIf

	nTotal := Len(aRegEmJson)
	oMatriz:setFlag("qtd_registros_total", nTotal, , , .T.)
	oMatriz:setFlag("qtd_registros_lidos", 0     , , , .T.)

	If nTotal > 0
		aAux := Array(sTAM_MAT)
	EndIf

	//Processa todos os registros encontrados
	For nIndRegs := 1 to nTotal
		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
			oMatriz:setFlag("termino_carga_SC", "S")
			Exit
		EndIf

		oRegistros := JsonObject():new()
		cError     := oRegistros:fromJSON(aRegEmJson[nIndRegs])
		If !Empty(cError)
			cError := oRegistros:fromJSON(JsonString(aRegEmJson[nIndRegs]))
			If !Empty(cError)
				Loop
				oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegEmJson[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
			EndIf
		EndIf

		cProduto := PadR(oRegistros["aRegs"][1]["T4T_PROD"], snTamCod)
		dEntrega := ConvDatPer(StoD(oRegistros["aRegs"][1]["T4T_DTENT"]), aPeriodos, @nPeriodo, .F.)

		//Loga Evento 006 - Documento planejado em atraso
		If lEventLog .AND. StoD(oRegistros["aRegs"][1]["T4T_DTENT"]) < soDados:oParametros["dDataIni"]
			soDominio:oEventos:loga("006", cProduto, StoD(oRegistros["aRegs"][1]["T4T_DTENT"]), {StoD(oRegistros["aRegs"][1]["T4T_DTENT"]), oRegistros["aRegs"][1]["T4T_NUM"], oRegistros["aRegs"][1]["T4T_SEQ"], "T4T"})
		EndIf

		cChave := cFilAux + DtoS(dEntrega) + cProduto
		aSize(aRegistro, 0)

		nQtdEntPrv := (Val(oRegistros["aRegs"][1]["T4T_QTD"]) - Val(oRegistros["aRegs"][1]["T4T_QUJE"]))

		//Guarda o registro na Matriz
		oMatriz:trava(cChave)
		If oMatriz:getRow(1, cChave, Nil, @aRegistro, .F., .T.)

			aRegistro[sMAT_ENTPRE] += nQtdEntPrv

			If !oMatriz:updRow(1, cChave, Nil, aRegistro, .F., .T.)
				If oStatus:getStatus("status") == "4"
					Exit
				Else
					oLogs:log(STR0055 + "T4T", "E") //"Erro na carga de dados da tabela para memoria: "
				EndIf
			EndIf
		Else
			aAux[sMAT_FILIAL] := cFilAux
			aAux[sMAT_DATA]   := dEntrega
			aAux[sMAT_PRODUT] := cProduto
			aAux[sMAT_SLDINI] := 0
			aAux[sMAT_ENTPRE] := nQtdEntPrv
			aAux[sMAT_SAIPRE] := 0
			aAux[sMAT_SAIEST] := 0
			aAux[sMAT_SALDO]  := 0
			aAux[sMAT_NECESS] := 0
			aAux[sMAT_TPREPO] := " "
			aAux[sMAT_EXPLOD] := .F.
			aAux[sMAT_THREAD] := -1
			aAux[sMAT_IDOPC]  := ""
			aAux[sMAT_DTINI]  := dEntrega

			If !oMatriz:addRow(cChave, aAux, , , , cProduto)
				If oStatus:getStatus("status") == "4"
					Exit
				Else
					oLogs:log(STR0055 + "T4T", "E") //"Erro na carga de dados da tabela para memoria: "
				EndIf
			EndIf

			//Cria lista deste produto para registrar os periodos
			If !oMatriz:existList("Periodos_Produto_" + cProduto)
				oMatriz:createList("Periodos_Produto_" + cProduto)
			EndIf
			oMatriz:setItemList("Periodos_Produto_" + cProduto, cValToChar(nPeriodo), nPeriodo)
		EndIf

		oMatriz:destrava(cChave)

		If lEventLog
			If oJsEvtEntr[cProduto] == Nil
				oJsEvtEntr[cProduto] := {}
			EndIf
			//Adiciona {cDocumento, cItem, cAlias, nPeriodo, nQuantidade, nControle, lPossuiLog, nPriConsumo, nPrioridade}
			aAdd(oJsEvtEntr[cProduto], {oRegistros["aRegs"][1]["T4T_NUM"], oRegistros["aRegs"][1]["T4T_SEQ"], "T4T", nPeriodo, nQtdEntPrv, nQtdEntPrv, .F., 0, 20})
		EndIf

		FreeObj(oRegistros)
		oRegistros := Nil

		oMatriz:setFlag("qtd_registros_lidos", 1, , , .T.)
	Next nIndRegs

	oLogs:log(STR0036 + cValToChar(MicroSeconds() - nTempoIni)       , "CM") //"Tempo de carga de Solicita��es de Compras "
	oLogs:log(STR0061 + cValToChar(nTotal)                           , "CM") //"Quantidade de Solicita��es de Compra carregadas em mem�ria: "
	oLogs:logb({|| STR0026 + cValToChar(soDados:tamanhoLista("MAT"))}, "CM") //"Quantidade de itens da matriz carregados em memoria: "

	//Grava flag de conclus�o global
	oMatriz:setFlag("termino_carga_SC", "S")

	If lEventLog
		soDominio:oEventos:incluiEntradas(oJsEvtEntr, "T4T")
	EndIf

	//Limpa os arrays da mem�ria
	aSize(aAux, 0)
	aAux := Nil
	aSize(aFields, 0)
	aFields := Nil
	aSize(aRegEmJson, 0)
	aRegEmJson := Nil
	aSize(aRegistro, 0)
	aRegistro := Nil
	FreeObj(oJsEvtEntr)
	oJsEvtEntr := Nil

Return

/*/{Protheus.doc} entradasPC
Carrega dados de pedidos de compras na tabela de Entradas Firmes (MAT)
@author marcelo.neumann
@since 04/10/2019
@version 1.0
/*/
METHOD entradasPC() CLASS MrpDados_CargaMemoria

	//Verifica se a carga ser� multi-thread
	If ::oDados:oParametros["nThreads"] >= 1 .And. ::oDados:oParametros["cAutomacao"] != "2"
		PCPIPCGO(::oDados:oParametros["cSemaforoThreads"], .F., "PCPCargaPC") //Delega para processamento em Thread
	Else
		PCPCargaPC() //Execucao single-thread
	EndIf

Return

/*/{Protheus.doc} PCPCargaPC
Processa a carga dos dados dos pedidos de compras
@author marcelo.neumann
@since 04/10/2019
@version 1.0
@param 01 lCargaCSV, l�gico, indica se a carga ser� feita via arquivo CSV ou via tabela
/*/
Function PCPCargaPC()

	Local nTempoIni  := MicroSeconds()
	Local aAux       := {}
	Local aFields    := {{"T4U_PROD" , "C", "T4U", "" }, ;
	                     {"T4U_QTD"  , "N", "T4U", "" }, ;
	                     {"T4U_DTENT", "D", "T4U", "" }, ;
	                     {"T4U_QUJE" , "N", "T4U", "0"}}
	Local aPeriodos  := {}
	Local aRegEmJson := {}
	Local aRegistro  := {}
	Local cChave     := ""
	Local cError     := ""
	Local cFilAux    := ""
	Local cProduto   := ""
	Local cWhere 	 := ""
	Local cWhereIN   := ""
	Local dEntrega
	Local dLimite    := Nil
	Local lEventLog
	Local lHorFirme
	Local nIndRegs   := 1
	Local nPeriodo   := 0
	Local nTotal     := 0
	Local nQtdEntPrv := 0
	Local oLogs
	Local oMatriz
	Local oRegistros
	Local oStatus
	Local oJsEvtEntr

	//Prepara o ambiente (quando for uma nova thread)
	PrepStatics()
	oMatriz    := soDados:oMatriz
	oLogs      := soDados:oLogs
	aPeriodos  := soDados:aPeriodos
	dLimite    := soDominio:oPeriodos:ultimaDataDoMRP()
	oStatus    := MrpDados_Status():New(soDados:oParametros["ticket"])

	lEventLog := soDados:oParametros["lEventLog"]
	If lEventLog
		oJsEvtEntr := JsonObject():New()
	EndIf

	//Adiciona Documento e Sequ�ncia ao gerar Log de Eventos
	If lEventLog
		aAdd(aFields, {"T4U_NUM"  , "C", "T4U", "" })
		aAdd(aFields, {"T4U_SEQ"  , "C", "T4U", "" })
	EndIf

	lHorFirme := soDados:oParametros["lHorizonteFirme"] .AND. "|1.1|" $ soDados:oParametros["cDocumentType"]

	//Busca os registros a serem processados
	If PreparaAmb()
		cJoinT4Q := "T4Q.T4Q_FILIAL = '" + xFilial("T4U") + "'"
		cJoinT4Q += " AND T4Q.T4Q_OP = T4U.T4U_OP"
		cJoinT4Q += " AND T4Q.D_E_L_E_T_ = ' '"

		cWhere := " (( " + ANDT4QTIPO(lHorFirme) + " AND " + ANDT4QSITU() + " ) OR T4U.T4U_OP = ' ')"

		cWhere += " AND T4U.T4U_QUJE < T4U.T4U_QTD"
		cWhere += " AND (T4U.T4U_TIPO = '1' "

		If "|1.3|" $ soDados:oParametros["cDocumentType"] .OR. lHorFirme
			cWhere +=  "OR T4U.T4U_TIPO = '2' "
		EndIf

		cWhere += ")"

		If !Empty(dLimite)
			cWhere += " AND T4U.T4U_DTENT <= '" + DtoS(dLimite) + "' "
		EndIf

		//Valida o filtro de Tipo ou Grupo de Produtos
		cWhere += " AND " + soDominio:oSeletivos:scriptExistsProdutoSQL("T4U.T4U_PROD")
		cWhere += " AND T4U.T4U_FILIAL = '" + xFilial("T4U") + "' "

		//Par�metro para filtrar os armaz�ns
		If !Empty(soDados:oParametros["cWarehouses"])
			cWhere += " AND " + soDominio:oSeletivos:scriptInSQL("T4U.T4U_LOCAL", "cWarehouses")
		EndIf

		//Atribui Filtro Relacionado ao Horizonte Firme
		If lHorFirme
			MrpDominio_HorizonteFirme():criaScriptIN(soDados:oParametros["dDataIni"], "HWA", "T4U", "T4U_PROD", "T4U_DTENT", cWhere, @cWhereIN)
			cWhere   += " AND ((T4U.T4U_TIPO <> '2') OR (T4U.R_E_C_N_O_ IN ("
			cWhere   += cWhereIN
			cWhere   += ")))"
		EndIf
		aRegEmJson := GetRegDB("T4U", {{"T4Q", "LEFT JOIN", cJoinT4Q}}, cWhere, aFields)
	EndIf

	nTotal := Len(aRegEmJson)
	oMatriz:setFlag("qtd_registros_total", nTotal, , , .T.)
	oMatriz:setFlag("qtd_registros_lidos", 0     , , , .T.)

	If nTotal > 0
		aAux := Array(sTAM_MAT)
	EndIf

	//Processa todos os registros encontrados
	For nIndRegs := 1 to nTotal
		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
			oMatriz:setFlag("termino_carga_PC", "S")
			Exit
		EndIf

		oRegistros := JsonObject():new()
		cError     := oRegistros:fromJSON(aRegEmJson[nIndRegs])
		If !Empty(cError)
			cError := oRegistros:fromJSON(JsonString(aRegEmJson[nIndRegs]))
			If !Empty(cError)
				Loop
				oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegEmJson[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
			EndIf
		EndIf

		cProduto := PadR(oRegistros["aRegs"][1]["T4U_PROD"], snTamCod)
		dEntrega := ConvDatPer(StoD(oRegistros["aRegs"][1]["T4U_DTENT"]), aPeriodos, @nPeriodo, .F.)

		//Loga Evento 006 - Documento planejado em atraso
		If lEventLog .AND. StoD(oRegistros["aRegs"][1]["T4U_DTENT"]) < soDados:oParametros["dDataIni"]
			soDominio:oEventos:loga("006", cProduto, StoD(oRegistros["aRegs"][1]["T4U_DTENT"]), {StoD(oRegistros["aRegs"][1]["T4U_DTENT"]), oRegistros["aRegs"][1]["T4U_NUM"], oRegistros["aRegs"][1]["T4U_SEQ"], "T4U"})
		EndIf

		cChave := cFilAux + DtoS(dEntrega) + cProduto
		aSize(aRegistro, 0)

		nQtdEntPrv := (Val(oRegistros["aRegs"][1]["T4U_QTD"]) - Val(oRegistros["aRegs"][1]["T4U_QUJE"]))

		//Guarda o registro na Matriz
		oMatriz:trava(cChave)
		If oMatriz:getRow(1, cChave, Nil, @aRegistro, .F., .T.)

			aRegistro[sMAT_ENTPRE] += nQtdEntPrv

			If !oMatriz:updRow(1, cChave, Nil, aRegistro, .F., .T.)
				If oStatus:getStatus("status") == "4"
					Exit
				Else
					oLogs:log(STR0055 + "T4U", "E") //"Erro na carga de dados da tabela para memoria: "
				EndIf
			EndIf
		Else
			aAux[sMAT_FILIAL] := cFilAux
			aAux[sMAT_DATA]   := dEntrega
			aAux[sMAT_PRODUT] := cProduto
			aAux[sMAT_SLDINI] := 0
			aAux[sMAT_ENTPRE] := nQtdEntPrv
			aAux[sMAT_SAIPRE] := 0
			aAux[sMAT_SAIEST] := 0
			aAux[sMAT_SALDO]  := 0
			aAux[sMAT_NECESS] := 0
			aAux[sMAT_TPREPO] := " "
			aAux[sMAT_EXPLOD] := .F.
			aAux[sMAT_THREAD] := -1
			aAux[sMAT_IDOPC]  := ""
			aAux[sMAT_DTINI]  := dEntrega

			If !oMatriz:addRow(cChave, aAux, , , , cProduto)
				If oStatus:getStatus("status") == "4"
					Exit
				Else
					oLogs:log(STR0055 + "T4U", "E") //"Erro na carga de dados da tabela para memoria: "
				EndIf
			EndIf

			//Cria lista deste produto para registrar os periodos
			If !oMatriz:existList("Periodos_Produto_" + cProduto)
				oMatriz:createList("Periodos_Produto_" + cProduto)
			EndIf
			oMatriz:setItemList("Periodos_Produto_" + cProduto, cValToChar(nPeriodo), nPeriodo)
		EndIf

		oMatriz:destrava(cChave)

		If lEventLog
			If oJsEvtEntr[cProduto] == Nil
				oJsEvtEntr[cProduto] := {}
			EndIf
			//Adiciona {cDocumento, cItem, cAlias, nPeriodo, nQuantidade, nControle, lPossuiLog, nPriConsumo, nPrioridade}
			aAdd(oJsEvtEntr[cProduto], {oRegistros["aRegs"][1]["T4U_NUM"], oRegistros["aRegs"][1]["T4U_SEQ"], "T4U", nPeriodo, nQtdEntPrv, nQtdEntPrv, .F., 0, 10})
		EndIf

		FreeObj(oRegistros)
		oRegistros := Nil

		oMatriz:setFlag("qtd_registros_lidos", 1, , , .T.)
	Next nIndRegs

	oLogs:log(STR0066 + cValToChar(MicroSeconds() - nTempoIni)       , "CM") //"Tempo de carga de Pedidos de Compra "
	oLogs:log(STR0067 + cValToChar(nTotal)                           , "CM") //"Quantidade de Pedidos de Compra carregados em mem�ria: "
	oLogs:logb({|| STR0026 + cValToChar(soDados:tamanhoLista("MAT"))}, "CM") //"Quantidade de itens da matriz carregados em memoria: "

	//Grava flag de conclus�o global
	oMatriz:setFlag("termino_carga_PC", "S")

	If lEventLog
		soDominio:oEventos:incluiEntradas(oJsEvtEntr, "T4U")
	EndIf

	//Limpa os arrays da mem�ria
	aSize(aAux, 0)
	aAux := Nil
	aSize(aFields, 0)
	aFields := Nil
	aSize(aRegEmJson, 0)
	aRegEmJson := Nil
	aSize(aRegistro, 0)
	aRegistro := Nil

Return

/*/{Protheus.doc} versaoDaProducao
Carrega dados de solicita��es de compra na tabela de Entradas Firmes (MAT)
@author brunno.costa
@since 19/09/2019
@version 1.0
/*/
METHOD versaoDaProducao() CLASS MrpDados_CargaMemoria

	//Verifica se a carga ser� multi-thread
	If ::oDados:oParametros["nThreads"] >= 1 .And. ::oDados:oParametros["cAutomacao"] != "2"
		PCPIPCGO(::oDados:oParametros["cSemaforoThreads"], .F., "PCPCargaVP", ::oDados:oParametros["lCargaVpCSV"]) //Delega para processamento em Thread
	Else
		PCPCargaVP(::oDados:oParametros["lCargaVpCSV"]) //Execucao single-thread
	EndIf

Return

/*/{Protheus.doc} PCPCargaVP
Processa a carga dos dados dA Vers�o da Produ��o
@author brunno.costa
@since 16/10/2019
@version 1.0
@param 01 lCargaCSV, l�gico, indica se a carga ser� feita via arquivo CSV ou via tabela
/*/
Function PCPCargaVP(lCargaCSV)

	Local nTempoIni  := MicroSeconds()
	Local aAux       := {}
	Local aFields    := {{"T4M_IDREG" , "C", "T4M", ""  }, ;
	                     {"T4M_PROD"  , "C", "T4M", ""  }, ;
	                     {"T4M_DTINI" , "D", "T4M", ""  }, ;
	                     {"T4M_DTFIN" , "D", "T4M", ""  }, ;
	                     {"T4M_QNTDE" , "N", "T4M", "0" }, ;
	                     {"T4M_QNTATE", "N", "T4M", "0" }, ;
						 {"T4M_ROTEIR", "C", "T4M", ""  }, ;
						 {"T4M_ARMCON", "C", "T4M", ""  }, ;
	                     {"T4M_REV"   , "C", "T4M", ""  }  }

	Local aRegEmJson := {}
	Local aRegistro  := {}
	Local cChave     := ""
	Local cError     := ""
	Local cFilAux    := ""
	Local cProduto   := ""
	Local cWhere 	 := ""
	Local nIndRegs   := 1
	Local nTotal     := 0
	Local oLogs
	Local oVersDaPrd
	Local oRegistros
	Local oStatus

	//Prepara o ambiente (quando for uma nova thread)
	PrepStatics()
	oVersDaPrd := soDados:oVersaoDaProducao
	oLogs      := soDados:oLogs
	oStatus    := MrpDados_Status():New(soDados:oParametros["ticket"])

	//Busca os registros a serem processados (via CSV ou tabela)
	If lCargaCSV
		aRegEmJson := CsvToJson("versaoDaProducao.csv", aFields)
	Else
		If PreparaAmb()
			cWhere := " T4M.T4M_FILIAL = '" + xFilial("T4M") + "' "

			aRegEmJson := GetRegDB("T4M", , cWhere, aFields)
		EndIf
	EndIf

	nTotal := Len(aRegEmJson)
	oVersDaPrd:setFlag("qtd_registros_total", nTotal, , , .T.)
	oVersDaPrd:setFlag("qtd_registros_lidos", 0     , , , .T.)

	If nTotal > 0
		aAux := Array(sTAM_VDP)
	EndIf

	//Processa todos os registros encontrados
	For nIndRegs := 1 to nTotal
		//Checa cancelamento a cada X delegacoes
		If (nIndRegs == 1 .OR. Mod(nIndRegs, soDados:oParametros["nX_Para_Cancel"]) == 0) .AND. oStatus:getStatus("status") == "4"
			oVersDaPrd:setFlag("termino_carga_VDP", "S")
			Exit
		EndIf

		oRegistros := JsonObject():new()
		cError     := oRegistros:fromJSON(aRegEmJson[nIndRegs])
		If !Empty(cError)
			cError := oRegistros:fromJSON(JsonString(aRegEmJson[nIndRegs]))
			If !Empty(cError)
				Loop
				oLogs:log(STR0081 + ProcName(0) + " - Line " + cValToChar(ProcLine(0)) + ": '" + AllTrim(aRegEmJson[nIndRegs]) + "'", "E") //"Erro na convers�o do arquivo Json - Function "
			EndIf
		EndIf

		cProduto := PadR(oRegistros["aRegs"][1]["T4M_PROD"], snTamCod)
		cChave   := oRegistros["aRegs"][1]["T4M_IDREG"]

		//Guarda o registro na Matriz
		oVersDaPrd:trava(cChave)
		If !oVersDaPrd:existAList(cProduto)
			oVersDaPrd:createAList(cProduto)
		EndIf

		aAux[sVDP_FILIAL]  := cFilAux
		aAux[sVDP_PROD]    := cProduto
		aAux[sVDP_DTINI]   := StoD(oRegistros["aRegs"][1]["T4M_DTINI"])
		aAux[sVDP_DTFIN]   := StoD(oRegistros["aRegs"][1]["T4M_DTFIN"])
		aAux[sVDP_QNTDE]   := Val(oRegistros["aRegs"][1]["T4M_QNTDE"])
		aAux[sVDP_QNTATE]  := Val(oRegistros["aRegs"][1]["T4M_QNTATE"])
		aAux[sVDP_REV]     := oRegistros["aRegs"][1]["T4M_REV"]
		aAux[sVDP_ROTEIRO] := oRegistros["aRegs"][1]["T4M_ROTEIR"]
		aAux[sVDP_LOCAL]   := oRegistros["aRegs"][1]["T4M_ARMCON"]
		aAux[sVDP_CODIGO]  := RTrim(cChave)

		If !oVersDaPrd:setItemAList(cProduto, cChave, aAux)
			If oStatus:getStatus("status") == "4"
				Exit
			Else
				oLogs:log(STR0055 + "T4M", "E") //"Erro na carga de dados da tabela para memoria: "
			EndIf
		EndIf

		oVersDaPrd:destrava(cChave)

		FreeObj(oRegistros)
		oRegistros := Nil

		oVersDaPrd:setFlag("qtd_registros_lidos", 1, , , .T.)
	Next nIndRegs

	oLogs:log(STR0064 + cValToChar(MicroSeconds() - nTempoIni), "CM") //"Tempo de carga de Vers�o da Produ��o "
	oLogs:log(STR0065 + cValToChar(nTotal)                    , "CM") //"Quantidade de registros da Vers�o da Produ��o carregadas em mem�ria: "

	//Grava flag de conclus�o global
	oVersDaPrd:setFlag("termino_carga_VDP", "S")

	//Limpa os arrays da mem�ria
	aSize(aAux, 0)
	aAux := Nil
	aSize(aFields, 0)
	aFields := Nil
	aSize(aRegEmJson, 0)
	aRegEmJson := Nil
	aSize(aRegistro, 0)
	aRegistro := Nil

Return

/*/{Protheus.doc} PrepStatics
Prepara o ambiente (vari�veis est�ticas) de acordo com as vari�veis globais
@author marcelo.neumann
@since 08/07/2019
@version 1.0
@param 01 - lAvPeriodos, l�gico, indica se deve avaliar a cria��o do array aPeriodos
                                 (Controlado atrav�s do 4o par�metro da vari�vel global PCP_aParam: 1 - MrpDominio:calcularNivel(),
								 2 - Pr�-Carga Mem�ria, 3 - P�s Carga Calend�rio - Carga Complementar)
/*/
Static Function PrepStatics(lAvPeriodos)

	Default lAvPeriodos := .F.

	soDominio := MRPPrepDom(lAvPeriodos)
	soDados   := soDominio:oDados

Return

/*/{Protheus.doc} PreparaAmb
Prepara o ambiente (conex�o com banco) de acordo com os par�metros
@author marcelo.neumann
@since 08/07/2019
@version 1.0
/*/
Static Function PreparaAmb()

	Local lRet := .T.

	If Type("cEmpAnt") == "U"
		If Empty(soDados:oParametros["cEmpAnt"]) .Or. Empty(soDados:oParametros["cFilAnt"])
			soDados:oLogs:log(STR0058, "E") //"Erro ao preparar o ambiente. Par�metros 'cEmpAnt' e 'cFilAnt' n�o informados."
			lRet := .F.
		Else
			RpcSetType(3)
			RpcSetEnv(soDados:oParametros["cEmpAnt"], soDados:oParametros["cFilAnt"], Nil, Nil, "PCP", Nil)
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} UpdRegsDB
Seleciona e atualiza os registros que ser�o/foram processados
@author marcelo.neumann
@since 08/07/2019
@version 1.0
@param 01 - cTable , caracter, tabela a ser buscada/atualizada
@param 02 - cUpdSet, caracter, campos a serem atualizados
@param 03 - cFilter, caracter, filtro a ser aplicado na busca
@return     lOk    , l�gico  , indica se os registros foram selecionados e atualizado com sucesso
/*/
Static Function UpdRegsDB(cTable, cUpdSet, cFilter)

	Local cQuery := ""
	Local lOk    := .T.

	cQuery := "UPDATE " + RetSqlName(cTable) + ;
	            " SET " + cUpdSet            + ;
	          " WHERE D_E_L_E_T_ = ' '"

	If !Empty(cFilter)
		cQuery += " AND " + cFilter
	EndIf

	If TCSqlExec(cQuery) < 0
		lOk := .F.
	EndIf

Return lOk

/*/{Protheus.doc} GetRegDB
Retorna um array com os registros (em formato JSON) a serem processados
@author marcelo.neumann
@since 08/07/2019
@version 1.0
@param 01 - cTable , caracter, tabela principal que ser� buscada
@param 02 - aJoin  , array   , tabelas auxiliares: [1][1] - Alias
                                                   [1][2] - Comando para o Join ("LEFT OUTER JOIN", "INNER JOIN")
												   [1][3] - Relacionamento entre as tabelas
@param 03 - cFilter, caracter, filtro a ser aplicado na busca
@param 04 - aFields, array   , array com os campos a serem retornados: [1][1] - Nome da coluna 1
                                                                       [1][2] - Tipo da coluna: C (caracter)
																	                            N (num�rico)
																								D (data)
																								O (opcional)
@param 05 - cOrder, caracter, ordena��o dos registros que ser�o retornados na busca
@return aRegEmJson , array   , data convertida no periodo. Em caso de data superior ao ultimo periodo retorna Nil.
/*/
Static Function GetRegDB(cTable, aJoin, cFilter, aFields, cOrder, cRecursive)

	Local aRegEmJson := {}
	Local cQuery     := ""
	Local cAliasQry  := GetNextAlias()
	Local cBanco     := TCGetDB()
	Local cFim       := ""
	Local cQryFields := ""
	Local cQryCondic := ""
	Local cQryOpcion := ""
	Local nInd       := 0
	Local nTotalFor  := Len(aFields)

	Default aJoin      := {}
	Default cFilter    := "1 = 1"
	Default cOrder     := ""
	Default cRecursive := ""

	//ORACLE
	If cBanco == "ORACLE"
		cQryFields := "'{'"
		For nInd := 1 To nTotalFor
			Do Case
				Case aFields[nInd][2] == "C"
					cQryFields += " || '" + '"' + aFields[nInd][1] + '":"' + "' || RTRIM(" + aFields[nInd][3] + "." + aFields[nInd][1] + ") || '" + '",' + "'"

				Case aFields[nInd][2] == "O"
					cQryOpcion := " || '" + '"' + aFields[nInd][1] + '":{"OPTIONAL":' + "' || DECODE(LENGTH(" + aFields[nInd][3] + "." + aFields[nInd][1] + "), 1, '" + '""' + "', RTRIM(" + aFields[nInd][3] + "." + aFields[nInd][1] + ")), "

				Otherwise
					cQryFields += " || '" + '"' + aFields[nInd][1] + '":"' + "' || " + aFields[nInd][3] + "." + aFields[nInd][1] + " || '" + '",' + "'"
			EndCase
		Next nInd

		cQryFields += cQryOpcion
		cQryFields := Stuff(cQryFields, Len(cQryFields)-1, 1, '') + " REGISTRO_EM_JSON"

	//SQL SERVER
	ElseIf "MSSQL" $ cBanco
		cQryFields := "'{'"
		For nInd := 1 To nTotalFor
			Do Case
				Case aFields[nInd][2] == "C"
					cQryFields += " + '" + '"' + aFields[nInd][1] + '":"' + "' + RTRIM(ISNULL(" + aFields[nInd][3] + "." + aFields[nInd][1] + ", '')) + '" + '",' + "'"

				Case aFields[nInd][2] == "O"
					cQryOpcion := " + '" + '"' + aFields[nInd][1] + '":{"OPTIONAL":' + "' + ISNULL(RTRIM(" + aFields[nInd][3] + "." + aFields[nInd][1] + "), '" + '""' + "'), "

				Otherwise
					cQryFields += " + '" + '"' + aFields[nInd][1] + '":"' + "' + CAST(ISNULL(" + aFields[nInd][3] + "." + aFields[nInd][1] + ", '') AS VARCHAR) + '" + '",' + "'"
			EndCase
		Next nInd

		cQryFields += cQryOpcion
		cQryFields := "CAST(" + Stuff(cQryFields, Len(cQryFields)-1, 1, '') + " AS VARCHAR(8000)) AS REGISTRO_EM_JSON"

	//POSTGRES
	ElseIf cBanco == "POSTGRES"
		cQryFields := "CONCAT('{'"
		For nInd := 1 To nTotalFor
			Do Case
				Case aFields[nInd][2] == "C"
					cQryFields += " , '" + '"' + aFields[nInd][1] + '":"' + "' , RTRIM(" + aFields[nInd][3] + "." + aFields[nInd][1] + ") , '" + '",' + "'"

				Case aFields[nInd][2] == "O"
					cQryOpcion := " , '" + '"' + aFields[nInd][1] + '":{"OPTIONAL":' + "', COALESCE(SUBSTRING(ENCODE(" + aFields[nInd][3] + "." + aFields[nInd][1] + ", 'ESCAPE'), 1, LENGTH(" + aFields[nInd][3] + "." +  aFields[nInd][1] + ") - 1), '" + '""' + "'), "

				Otherwise
					cQryFields += " , '" + '"' + aFields[nInd][1] + '":"' + "' , " + aFields[nInd][3] + "." + aFields[nInd][1] + " , '" + '",' + "'"
			EndCase
		Next nInd

		cQryFields += cQryOpcion
		cQryFields := Stuff(cQryFields, Len(cQryFields)-1, 1, '') + ") REGISTRO_EM_JSON"
	EndIf

	cQryFields := "%" + cQryFields + "%"
	cQryCondic := "%" + RetSqlName(cTable) + " " + cTable

	//Trata as querys que possuem JOIN
	nTotalFor := Len(aJoin)
	For nInd := 1 To nTotalFor
		cQryCondic += " " + aJoin[nInd][2] + " " + Iif(Empty(aJoin[nInd][1]), "", RetSqlName(aJoin[nInd][1])) + " " + aJoin[nInd][1] + " ON " + aJoin[nInd][3] + ""
	Next nInd

	cQryCondic += " WHERE " + cTable + ".D_E_L_E_T_ = ' ' AND " + cFilter + " "

	If !Empty(cOrder)
		cQryCondic += " ORDER BY " + cOrder + "%"
	Else
		cQryCondic += "%"
	EndIf

	If Empty(cRecursive)
		BeginSql Alias cAliasQry
			%noparser%
			SELECT %Exp:cQryFields%
			FROM %Exp:cQryCondic%
		EndSql

	Else
		cQuery := cRecursive
		cQuery += " SELECT " + StrTran(cQryFields, "%", "")
		cQuery += " FROM " + StrTran(cQryCondic, "%", "")

		//Altera sintaxe da clausula WITH
		If cBanco == "POSTGRES"
			cQuery := StrTran(cQuery, 'WITH ', 'WITH recursive ')
		EndIf

		dbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAliasQry, .T., .T.)

	EndIf

	//Trata o fechamento da String (JSON) caso possua campo/chave Opcional
	If !Empty(cQryOpcion)
		cFim := "}}]}"
	Else
		cFim := "}]}"
	EndIf

	While (cAliasQry)->(!Eof())
		aAdd(aRegEmJson, '{"aRegs":[' + AllTrim((cAliasQry)->REGISTRO_EM_JSON) + cFim)

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

Return aRegEmJson

/*/{Protheus.doc} CsvToJson
Converte os dados do CSV em um array com os registros (em formato JSON) a serem processados
@author marcelo.neumann
@since 10/07/2019
@version 1.0
@param 01 - cArquivo , caracter, nome do arquivo a ser importado
@param 02 - aFields  , array   , array com os campos a serem retornados: [1][1] - Nome da coluna 1
                                                                         [1][2] - Tipo da coluna: C (caracter)
															                                      N (num�rico)
																							      D (data)
																							      O (opcional)
												   	                     [1][3] - Relacionamento entre as tabelas (n�o usado no CSV)
												   	                     [1][4] - Inicializador padr�o do campo
@param 03 - cCondicao , caracter, instru��o ADVPL (IF) a ser aplicada para validar o registro
@return     aRegEmJson, array   , data convertida no periodo. Em caso de data superior ao ultimo periodo retorna Nil.
/*/
Static Function CsvToJson(cArquivo, aFields, cCondicao)

	Local aLinhas    := {}
	Local aRegEmJson := {}
	Local aRegistro  := {}
	Local cJson      := ""
	Local cOpcional  := ""
	Local nIndCol    := 1
	Local nIndLin    := 1
	Local nLenFields := Len(aFields)
	Local nLenLinhas := 0
	Local nLenRegist := 0
	Local oFile      := FWFileReader():New(soDados:oParametros['cDiretorioCSV'] + cArquivo)

	Default cCondicao := ".T."

	// Se houver erro de abertura abandona processamento
	If !oFile:Open()
		soDados:oLogs:log(STR0032 + cArquivo, "E") //"Falha na abertura do arquivo: "
		Return aRegEmJson
	EndIf

	aLinhas := oFile:getAllLines()
	oFile:Close()
	FreeObj(oFile)

	nLenLinhas  := Len(aLinhas)
	For nIndLin := 1 To nLenLinhas
		aSize(aRegistro, 0)
		aRegistro := StrTokArr(aLinhas[nIndLin], ";")

		If &(cCondicao)
			nLenRegist := Len(aRegistro)
			cJson      := '{"aRegs":[{'

			For nIndCol := 1 To nLenFields
				If nIndCol <= nLenRegist
					If aFields[nIndCol][2] == "O"
						If Empty(aRegistro[nIndCol])
							cOpcional := '""'
						Else
							cOpcional := StrTran(aRegistro[nIndCol], ',', ';')
							cOpcional := StrTran(cOpcional, '};{', '},{')
							cOpcional := StrTran(cOpcional, '";"', '","')
						EndIf
						cJson += '"' + aFields[nIndCol][1] + '":{"OPTIONAL":' + cOpcional + '},'
					Else
						cJson += '"' + aFields[nIndCol][1] + '":"' + aRegistro[nIndCol] + '",'
					EndIf
				Else
					If aFields[nIndCol][2] == "O"
						cJson += '"' + aFields[nIndCol][1] + '":{"OPTIONAL":""},'
					ElseIf ValType(aFields[nIndCol][4]) == "C"
						cJson += '"' + aFields[nIndCol][1] + '":"' + aFields[nIndCol][4] + '",'
					EndIf
				EndIf
			Next nIndCol

			cJson := Stuff(cJson, Len(cJson), 1, '') + '}]}'

			aAdd(aRegEmJson, cJson)
		EndIf
	Next nIndLin

	//Limpa os arrays da mem�ria
	aSize(aRegistro, 0)
	aRegistro := Nil
	aSize(aLinhas, 0)
	aLinhas := Nil

Return aRegEmJson

/*/{Protheus.doc} ConvDatPer
Converte a data para uma data referente ao per�odo (menor ou igual)
@author marcelo.neumann
@since 08/07/2019
@version 1.0
@param 01 - dData     , data  , data a ser convertida
@param 02 - aPeriodos , array , array com os periodos
@param 03 - nIndPeriod, numero, indice do periodo atual retornado por referencia
@param 04 - lVerCalend, logico, indica se dever� ser considerado dia �til do calend�rio
@return     dData     , data  , data convertida no periodo. Em caso de data superior ao ultimo periodo retorna Nil.
/*/
Static Function ConvDatPer(dData, aPeriodos, nIndPeriod, lVerCalend)

	Local nLeadTime := soDominio:oParametros["nLeadTime"]
	Local nIndAux   := 0

	nIndPeriod := soDominio:oPeriodos:buscaPeriodoDaData(dData)
	nIndAux    := nIndPeriod

	If nLeadTime != 1 .And. lVerCalend
		While nIndPeriod > 0 .And. ;
		      !soDominio:oPeriodos:verificaDataUtil(aPeriodos[nIndPeriod])
			nIndPeriod--
		EndDo
		//Se n�o encontrou uma data �til anterior a data atual, utiliza o primeiro per�odo �til.
		If nIndAux > 0 .And. nIndPeriod == 0
			nIndPeriod := soDominio:oPeriodos:primeiroPeriodoUtil()
		EndIf
	EndIf

	If nIndPeriod == 0
		If dData < aPeriodos[1]
			If nLeadTime != 1 .And. lVerCalend
				//Quando utiliza calend�rio e a demanda
				//� de uma data anterior ao primeiro per�odo do MRP,
				//joga a demanda para o primeiro per�odo �til do MRP.
				dData      := soDominio:oPeriodos:buscaProximoDiaUtil(aPeriodos[1])
				nIndPeriod := soDominio:oPeriodos:buscaPeriodoDaData(dData)
			EndIf

			If nIndPeriod == 0
				nIndPeriod := 1
				dData      := aPeriodos[1]
			EndIf
		Else
			nIndPeriod := Len(aPeriodos)
			dData      := aTail(aPeriodos)
		EndIf
	Else
		dData := aPeriodos[nIndPeriod]
	EndIf

Return dData

/*/{Protheus.doc} DataLimite
Busca a data limite do processamento
@author marcelo.neumann
@since 08/07/2019
@version 1.0
@return dData, data, data limite do processamento do MRP (�ltimo dia do �ltimo per�odo)
/*/
Static Function DataLimite()

	Local dData := aTail(soDados:aPeriodos)

	dData := soDominio:oPeriodos:proximaData(dData, soDados:oParametros["nTipoPeriodos"])
	dData--

Return dData

/*/{Protheus.doc} convDate
Converte uma data do tipo DATE para o formato string AAAA-MM-DD

@type  Static Function
@author brunno.costa
@since 29/07/2019
@version P12.1.27
@param dData, Date, Data que ser� convertida
@return cData, Caracter, Data convertida para o formato utilizado na integra��o.
/*/
Static Function convDate(dData)
	Local cData := ""

	cData := StrZero(Year(dData),4) + "-" + StrZero(Month(dData),2) + "-" + StrZero(Day(dData),2)
Return cData

/*/{Protheus.doc} AgCalcNiv
Aguarda o t�rmino do c�lculo de n�veis
@author marcelo.nemann
@since 20/11/2019
@version 1.0
@param  oStatus  , objeto, inst�ncia do objeto de status
@return lRecalcOk, logico, indica se o c�lculo foi realizado com sucesso
/*/
Static Function AgCalcNiv(oStatus)

	Local lRecalcOk := .F.
	Local nSecRecNv := oStatus:getStatus("tempo_recalculo_niveis")

	While oStatus:getStatus("niveis") <> "9"
		If oStatus:getStatus("niveis") == "3"
			lRecalcOk := .T.
			Exit
		EndIf
		Sleep(50)
	EndDo

	oStatus:setStatus("tempo_recalculo_niveis" , MicroSeconds() - nSecRecNv)

Return lRecalcOk

/*/{Protheus.doc} loadFields
Carrega o mapa de campos que ser� utilizado para carga do produto
@author douglas.heydt
@since 05/12/2019
@version 1.0
@param cTablePrd, caracter, tabela principal da qual ser�o buscados os dados
@return aFields , array   , mapa dos campos necess�rios para carga
/*/
Static Function loadFields(cTablePrd)

	Local aFields := { {cTablePrd+"_PROD"  , "C", cTablePrd, "HWA.HWA_PROD"   }, ;
                     {cTablePrd+"_ESTSEG", "N", cTablePrd, "HWA.HWA_ESTSEG" }, ;
                     {cTablePrd+"_LE"    , "N", cTablePrd, "HWA.HWA_LE"     }, ;
                     {cTablePrd+"_PE"    , "N", cTablePrd, "HWA.HWA_PE"     }, ;
                     {cTablePrd+"_HORFIX", "N", cTablePrd, "HWA.HWA_HORFIX" }, ;
                     {cTablePrd+"_TPHFIX", "C", cTablePrd, "HWA.HWA_TPHFIX" }, ;
                     {cTablePrd+"_TIPE"  , "C", cTablePrd, "HWA.HWA_TIPE"   }, ;
                     {cTablePrd+"_EMIN"  , "N", cTablePrd, "HWA.HWA_EMIN"   }, ;
                     {cTablePrd+"_REVATU", "C", cTablePrd, "HWA.HWA_REVATU" }, ;
                     {cTablePrd+"_LOCPAD", "C", cTablePrd, "HWA.HWA_LOCPAD" }, ;
                     {cTablePrd+"_QE"    , "N", cTablePrd, "HWA.HWA_QE"     }, ;
                     {cTablePrd+"_LM"    , "N", cTablePrd, "HWA.HWA_LM"     }, ;
                     {cTablePrd+"_TOLER" , "N", cTablePrd, "HWA.HWA_TOLER"  }, ;
                     {cTablePrd+"_MRP"   , "C", cTablePrd, "HWA.HWA_MRP"    }, ;
                     {cTablePrd+"_EMAX"  , "N", cTablePrd, "HWA.HWA_EMAX"   }, ;
                     {"HWA_NIVEL"        , "C", "HWA"    , "'99'"           }, ;
                     {"HWA_TIPDEC"       , "C", "HWA"    , "'1'"            }, ;
                     {"HWA_NUMDEC"       , "C", "HWA"    , "'0'"            }, ;
                     {"HWA_TIPO"         , "C", "HWA"    , "''"             }, ;
                     {"HWA_GRUPO"        , "C", "HWA"    , "''"             }, ;
                     {"HWA_RASTRO"       , "C", "HWA"    , "''"             }, ;
                     {"HWA_PROSBP"       , "C", "HWA"    , "''"             }, ;
                     {"HWA_LOTSBP"       , "N", "HWA"    , "0"              }, ;
                     {"HWA_ESTORI"       , "C", "HWA"    , "''"             }, ;
                     {"HWA_APROPR"       , "C", "HWA"    , "''"             }, ;
                     {"HWA_CPOTEN"       , "C", "HWA"    , "''"             }}

	MrpDominio_MOD():adicionaCampoHWA(@aFields, soDados:oParametros)

	If prdBlock()
		aAdd(aFields, {"HWA_CPOTEN", "C", "HWA", "'2'"})
		aAdd(aFields, {"HWA_BLOQUE", "C", "HWA", "'2'"})
	EndIf

	dbSelectArea("HWA")
	If FieldPos("HWA_ROTOPE") > 0
		aAdd(aFields, {"HWA_ROTOPE", "C", "HWA", "''"   })
	EndIf

	//******** Adiciona Campos de Estoque - INICIO ********
	aAdd(aFields, {"T4V_QTD"  , "N", "T4V", "0" } )
	aAdd(aFields, {"T4V_QTIND", "N", "T4V", "0"} )
	aAdd(aFields, {"T4V_IDREG", "C", "T4V", "''"} )

	//Se o par�metro da tela "Estoque EM Terceiro" estiver como "1 - Soma", carregar para mem�ria o valor do campo T4V_QNPT
	If soDados:oParametros["lEMTerceiro"]
		aAdd(aFields, {"T4V_QNPT" , "N", "T4V", "0"})
	EndIf

	//Se o par�metro da tela "Estoque DE Terceiro" estiver como 1 - Subtrai", carregar para mem�ria o valor do campo T4V_QTNP
	If soDados:oParametros["lDETerceiro"]
		aAdd(aFields, {"T4V_QTNP" , "N", "T4V", "0"})
	EndIf

	//Se o par�metro da tela "Saldo Bloqueado por Lote" estiver como "1 - Subtrai", carregar para mem�ria o valor do campo T4V_SLDBQ
	If soDados:oParametros["lSubtraiLoteBloqueado"]
		dbSelectArea("T4V")
		If FieldPos("T4V_SLDBQ") > 0
			aAdd(aFields, {"T4V_SLDBQ" , "N", "T4V", "0"})
		EndIf
	EndIf

	//Se o par�metro da tela "Estoque Rejeitado pelo CQ" estiver como "1 - Subtrai", carregar para mem�ria o valor do campo (HWX_QTDE - HWX_QTDEV)
	If soDados:oParametros["lSubtraiRejeitosCQ"] .And. FWAliasInDic("HWX",.F.)
		aAdd(aFields, {"HWX_QTDE" , "N", "HWX", "0"})
		aAdd(aFields, {"HWX_QTDEV", "N", "HWX", "0"})
	EndIf

	//Se o par�metro MV_LOTVENC estiver como  "N", carregar para mem�ria o valor dos campos T4V_LOTE, T4V_SLOTE e T4V_VALID.
	If !soDados:oParametros["lExpiredLot"]
		aAdd(aFields, {"T4V_LOTE" , "C", "T4V", "''"})
		aAdd(aFields, {"T4V_SLOTE", "C", "T4V", "''"})
		aAdd(aFields, {"T4V_VALID", "D", "T4V", "''"})
	EndIf
	//******** Adiciona Campos de Estoque - FIM ********

Return aFields

/*/{Protheus.doc} GetProdDB
Retorna um array com os registros de PRODUTOS (em formato JSON) a serem processados
@author douglas.heydt
@since 05/12/2019
@version 1.0
@param 01 - cTable , caracter, tabela principal que ser� buscada
@param 02 - aJoin  , array   , tabelas auxiliares: [1][1] - Alias
                                                   [1][2] - Comando para o Join ("LEFT OUTER JOIN", "INNER JOIN")
												   [1][3] - Relacionamento entre as tabelas
@param 03 - cFilter, caracter, filtro a ser aplicado na busca
@param 04 - aFields, array   , array com os campos a serem retornados: [1][1] - Nome da coluna 1
                                                                       [1][2] - Tipo da coluna: C (caracter)
																	                            N (num�rico)
																								D (data)
																								O (opcional)
@return aRegEmJson , array   , registros do banco de dados convertidos em formato JSON
/*/
Static Function GetProdDB(cTable, aJoin, cFilter, aFields)

	Local aRegEmJson := {}
	Local cAliasQry  := GetNextAlias()
	Local cBanco     := TCGetDB()
	Local cQryFields := ""
	Local cQryCondic := ""
	Local nInd       := 0
	Local nTotalFor  := Len(aFields)

	Default aJoin    := {}
	Default cFilter  := "1 = 1"

	//ORACLE
	If cBanco == "ORACLE"
		cQryFields := "'{'"
		For nInd := 1 To nTotalFor
			Do Case
				Case aFields[nInd][2] == "C"
					cQryFields += " || '" + '"' + aFields[nInd][1] + '":"' + "' || RTRIM( COALESCE(" + aFields[nInd][3] + "." + aFields[nInd][1] + ", " + aFields[nInd][4] + ")) || '" + '",' + "'"

				Case aFields[nInd][2] == "VC" //VIRTUAL - CARACTER
					cQryFields += " || '" + '"' + aFields[nInd][1] + '":"' + "' || RTRIM(" + aFields[nInd][4] + ") || '" + '",' + "'"

				Otherwise
					cQryFields += " || '" + '"' + aFields[nInd][1] + '":"' + "' || COALESCE(" + aFields[nInd][3] + "." + aFields[nInd][1] + ", " + aFields[nInd][4] + " ) || '" + '",' + "'"
			EndCase
		Next nInd

		cQryFields := Stuff(cQryFields, Len(cQryFields)-1, 1, '') + " REGISTRO_EM_JSON"

	//SQL SERVER
	ElseIf "MSSQL" $ cBanco
		cQryFields := "'{'"
		For nInd := 1 To nTotalFor
			Do Case
				Case aFields[nInd][2] == "C"
					cQryFields += " + '" + '"' + aFields[nInd][1] + '":"' + "' + RTRIM(COALESCE(" + aFields[nInd][3] + "." + aFields[nInd][1] + ", " + aFields[nInd][4] + ")) + '" + '",' + "'"

				Case aFields[nInd][2] == "VC" //VIRTUAL - CARACTER
					cQryFields += " + '" + '"' + aFields[nInd][1] + '":"' + "' + RTRIM(" + aFields[nInd][4] + ") + '" + '",' + "'"

				Otherwise
					cQryFields += " + '" + '"' + aFields[nInd][1] + '":"' + "' + CAST(COALESCE(" + aFields[nInd][3] + "." + aFields[nInd][1] + ", " + aFields[nInd][4] + ") AS VARCHAR) + '" + '",' + "'"
			EndCase
		Next nInd

		cQryFields := "CAST(" + Stuff(cQryFields, Len(cQryFields)-1, 1, '') + " AS VARCHAR(8000)) AS REGISTRO_EM_JSON"

	//POSTGRES
	ElseIf cBanco == "POSTGRES"
		cQryFields := "CONCAT('{'"
		For nInd := 1 To nTotalFor
			Do Case
				Case aFields[nInd][2] == "C"
					cQryFields += Iif(nInd > 1, '","', " , '" + '"') + aFields[nInd][1] + '":"' + "' , RTRIM( COALESCE(" + aFields[nInd][3] + "." + aFields[nInd][1] + " , " + aFields[nInd][4] + ")) , '"

				Case aFields[nInd][2] == "VC" //VIRTUAL - CARACTER
					cQryFields += Iif(nInd > 1, '","', " , '" + '"')  + aFields[nInd][1] + '":"' + "' , RTRIM(" + aFields[nInd][4] + ") , '"

				Otherwise
					cQryFields += Iif(nInd > 1, '","', " , '" + '"') + aFields[nInd][1] + '":"' + "' , COALESCE(" + aFields[nInd][3] + "." + aFields[nInd][1] + ", " + aFields[nInd][4] + ") , '"
			EndCase

		Next nInd
		cQryFields += '"' + "') REGISTRO_EM_JSON"
	EndIf

	cQryFields := "%" + cQryFields + "%"
	cQryCondic := "%" + RetSqlName(cTable) + " " + cTable

	//Trata as querys que possuem JOIN
	nTotalFor := Len(aJoin)
	For nInd := 1 To nTotalFor
		cQryCondic += " " + aJoin[nInd][2] + " " + RetSqlName(aJoin[nInd][1]) + " " + aJoin[nInd][1] + " ON " + aJoin[nInd][3] + ""
	Next nInd

	cQryCondic += " WHERE " + cTable + ".D_E_L_E_T_ = ' ' AND " + cFilter + "%"

	BeginSql Alias cAliasQry
		%noparser%
		SELECT %Exp:cQryFields%
		  FROM %Exp:cQryCondic%
		  ORDER BY HWA_NIVEL, HWA_PROD, T4V_VALID
	EndSql

	While (cAliasQry)->(!Eof())
		aAdd(aRegEmJson, '{"aRegs":[' + AllTrim((cAliasQry)->REGISTRO_EM_JSON) + "}]}")

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

Return aRegEmJson

/*/{Protheus.doc} prdBlock
Verifica se deve ser considerado o par�metro de produtos bloqueados da tabela HWA.

@type  Static Function
@author lucas.franca
@since 11/02/2020
@version P12.1.27
@return lBloqueio, Logic, Identifica se deve considerar o par�metro de produtos bloqueados.
/*/
Static Function prdBlock()
	Local lBloqueio := .F.

	If _lBloqueio == Nil
		If Empty(GetSX3Cache("HWA_BLOQUE", "X3_TAMANHO"))
			_lBloqueio := .F.
		Else
			_lBloqueio := .T.
		EndIf
	EndIf

	lBloqueio := _lBloqueio
Return lBloqueio

/*/{Protheus.doc} JsonString
Realiza Replace de Caracteres Especiais para o Padr�o JSON

@type  Static Function
@author brunno.costa
@since 07/04/2020
@version P12.1.30
@return cReturn, caracter, retorna string cJsonOrig com replace de caracteres especiais
/*/
Static Function JsonString(cJsonOrig)
	Local cReturn := ""
	Default cJsonOrig := ""
	cReturn := cJsonOrig
	cReturn := StrTran(cReturn, "'"    , "\'")
	cReturn := StrTran(cReturn, '\'    , '\\')

	//Ajustes Aspas Dupla
	cReturn := StrTran(cReturn, '"'              , '\"')
	cReturn := StrTran(cReturn, '\",\"'          , '","')
	cReturn := StrTran(cReturn, '\":\"'          , '":"')
	cReturn := StrTran(cReturn, '\"aRegs\"'      , '"aRegs"')
	cReturn := StrTran(cReturn, '[{\"'           , '[{"')
	cReturn := StrTran(cReturn, '\"}]'           , '"}]')
	cReturn := StrTran(cReturn, '\"}}]'          , '"}}]')
	cReturn := StrTran(cReturn, '\":{\"OPTIONAL"', '":{"OPTIONAL"')

	cReturn := StrTran(cReturn, '/'    , '\/')
	cReturn := StrTran(cReturn, Chr(8) , '\b') // backspace
	cReturn := StrTran(cReturn, Chr(12), '\f') // formfeed
	cReturn := StrTran(cReturn, Chr(10), '\n') // newline
	cReturn := StrTran(cReturn, Chr(13), '\r') // carriage
	cReturn := StrTran(cReturn, Chr(9) , '\t') // horizontal tab
Return cReturn

/*/{Protheus.doc} LocalMRP
Retorna instru��o SQL para validar os saldos em estoque de locais que s�o considerados no MRP.

@type  Static Function
@author renan.roeder
@since 11/08/2020
@version P12.1.31
@param  cCampo , caracter, campo da tabela de saldos para relacionar a tabela HWY
@return cExists, caracter, instru��o sql para validar armaz�ns que entram no MRP.
/*/
Static Function LocalMRP(cCampo)
	Local cExists := ""

	If FWAliasInDic("HWY",.F.)
		cExists := " AND EXISTS (SELECT 1 FROM "+RetSqlName("HWY")+" HWY WHERE HWY_FILIAL = '"+xFilial("HWY")+"' AND HWY.HWY_COD = "+cCampo+" AND HWY.HWY_MRP = '1') "
	EndIf

Return cExists
