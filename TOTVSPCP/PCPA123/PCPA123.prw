#INCLUDE "PROTHEUS.CH"
#INCLUDE "PCPA123.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "FWMVCDEF.CH"                 
#include "TbIconn.ch"
#INCLUDE "FWCOMMAND.CH"

/*/{Protheus.doc} PCPA136
Cadastro da Malha de Distribui��o
@author douglas.heydt
@since 01/05/2018
@version 1.0
@return Nil
/*/
Function PCPA123()
	Local aArea      := GetArea()
	Local oBrowse
	Private aOrigems  := {}
	Private aDestinos := {}
	
	//Prote��o do fonte para n�o ser utilizado pelos clientes neste momento.
	If !(FindFunction("RodaNewPCP") .And. RodaNewPCP())
		HELP(' ',1,"Help" ,,STR0034,2,0,,,,,,) //"Rotina n�o dispon�vel nesta release."
		Return
	EndIf

	oBrowse := BrowseDef()
	oBrowse:Activate()	
	RestArea(aArea)
Return NIL

/*/{Protheus.doc} BrowseDef
Cria��o do Browse da rotina
@author douglas.heydt
@since 01/05/2018
@version P12
@return oBrowse, object, objeto do tipo FWMBrowse
/*/
Static Function BrowseDef()
	Local oBrowse
	oBrowse := FWMBrowse():New()	
	oBrowse:SetAlias('SVD')
	oBrowse:SetDescription(STR0001)
Return oBrowse

/*/{Protheus.doc} MenuDef
Defini��o do Menu
@author douglas.heydt
@since 01/05/2018
@version P12
@return aRotina (vetor com botoes da EnchoiceBar)
/*/
Static Function MenuDef()
	Local aRotina := {}
	ADD OPTION aRotina Title STR0002 Action 'PCPA123MNU(2)' OPERATION OP_VISUALIZAR	ACCESS 0 //Visualizar
	ADD OPTION aRotina Title STR0003 Action 'PCPA123MNU(3)' OPERATION OP_INCLUIR	ACCESS 0 //Incluir
	ADD OPTION aRotina Title STR0004 Action 'PCPA123MNU(4)' OPERATION OP_ALTERAR	ACCESS 0 //Alterar
	ADD OPTION aRotina Title STR0005 Action 'PCPA123MNU(5)' OPERATION OP_EXCLUIR	ACCESS 0 //Excluir
Return aRotina

/*/{Protheus.doc} ModelDef
Defini��o do Modelo
@author douglas.heydt
@since 01/05/2018
@version P12
@return oModel, object, modelo de dados
/*/
Static Function ModelDef()
	Local oModel 	
	Local oStruSVD := FWFormStruct(1, "SVD" )
	Local oStruSVQ := FWFormStruct(1, "SVQ")
	Local oEvent   := PCPA123EVDEF():New()
	Local oEventApi := PCPA123API():New()
	
	oStruSVQ:AddTrigger("VQ_PRODUTO", "B1_DESC", {|| .T. }, {|| lDescPrd() })
	oStruSVD:AddTrigger("VD_ORIGEM", "VD_DESCORI", {|| .T. }, {|| l123Desc(1) })
	oStruSVD:AddTrigger("VD_DESTINO", "VD_DESCDES", {|| .T. }, {|| l123Desc(2) })

	//Adiciona campos no modelo.
	addFldMdl(oStruSVD,oStruSVQ)
		
	oModel := MPFormModel():New('PCPA123')
	oModel:AddFields( 'SVDMASTER', /*cOwner*/, oStruSVD)
	oModel:AddGrid( "SVQDETAIL", "SVDMASTER", oStruSVQ)
	
	oModel:SetPrimaryKey({})
	oModel:SetRelation("SVQDETAIL",{{"VQ_FILIAL","xFilial('SVQ')"},{"VQ_ORIGEM" ,"VD_ORIGEM"},{"VQ_DESTINO" ,"VD_DESTINO"}},SVQ->(IndexKey(1)))
	
	oModel:GetModel("SVQDETAIL"):SetUniqueLine({"VQ_PRODUTO","VQ_DTINI","VQ_DTFIM","VQ_QNTINI","VQ_QNTFIM"})
	
	oStruSVD:SetProperty("VD_FILORG" , MODEL_FIELD_OBRIGAT, .T.)
	oStruSVD:SetProperty("VD_ORIGEM" , MODEL_FIELD_OBRIGAT, .T.)
	oStruSVD:SetProperty("VD_DESTINO", MODEL_FIELD_OBRIGAT, .T.)
	oStruSVD:SetProperty("VD_FILORG" , MODEL_FIELD_NOUPD, .T.)
	oStruSVD:SetProperty("VD_ORIGEM" , MODEL_FIELD_NOUPD, .T.)
	oStruSVD:SetProperty("VD_FILDEST", MODEL_FIELD_VALID,FWBuildFeature(STRUCT_FEATURE_VALID,"a123VldFil('VD_FILDEST')"))
	oStruSVD:SetProperty("VD_FILDEST", MODEL_FIELD_INIT,FWBuildFeature(STRUCT_FEATURE_INIPAD,"cFilAnt"))
	
	oStruSVQ:SetProperty("VQ_PRODUTO", MODEL_FIELD_OBRIGAT, .T.)
	oStruSVQ:SetProperty("VQ_DTINI"  , MODEL_FIELD_OBRIGAT, .T.)
	oStruSVQ:SetProperty("VQ_DTFIM"  , MODEL_FIELD_OBRIGAT, .T.)
	oStruSVQ:SetProperty("VQ_QNTINI" , MODEL_FIELD_OBRIGAT, .T.)
	oStruSVQ:SetProperty("VQ_QNTFIM" , MODEL_FIELD_OBRIGAT, .T.)
	
	oModel:SetDescription(STR0001)
	oModel:GetModel('SVDMASTER'):SetDescription(STR0001)
	oModel:GetModel('SVQDETAIL'):SetDescription(STR0008)
	oModel:GetModel( 'SVQDETAIL' ):SetOptional(.T.)
	
	oModel:AddRules( 'SVQDETAIL', 'VQ_PRODUTO','SVDMASTER', 'VD_ORIGEM' , 3 )
	oModel:AddRules( 'SVQDETAIL', 'VQ_DTFIM','SVQDETAIL', 'VQ_DTINI' , 3 )
	oModel:AddRules( 'SVQDETAIL', 'VQ_QNTFIM','SVQDETAIL', 'VQ_QNTINI' , 3 )
	oModel:AddRules( 'SVDMASTER', 'VD_ORIGEM','SVDMASTER', 'VD_FILORG' , 3 )
	oModel:AddRules( 'SVDMASTER', 'VD_DESTINO','SVDMASTER', 'VD_FILDEST' , 3 )

	oModel:InstallEvent("PCPA123EVDEF", /*cOwner*/, oEvent)
	oModel:InstallEvent("PCPA123API"  , /*cOwner*/, oEventApi)
	
Return oModel

/*/{Protheus.doc} ViewDef
Defini��o da View
@author douglas.heydt
@since 01/05/2018
@version P12
@return oView, object, objeto de View
/*/
Static Function ViewDef()	
	Local oModel    := FWLoadModel( 'PCPA123' )
	Local oStruSVD  := FWFormStruct(2,'SVD',{|cCampo|�AllTrim(cCampo)�$�"VD_ORIGEM"})//FWFormStruct( 2, "SVD")
	Local oStruSS   := FWFormStruct(2,'SVD',{|cCampo|�AllTrim(cCampo)�$�"VD_FILDEST|VD_DESTINO|"})//FWFormStruct( 2, "SVD")
	Local oStruSVQ  := FWFormStruct( 2, "SVQ")
	Local lExibeFil := multiFil()
	Local oView

	oView :=FWFormView():New()
	oView:SetModel(oModel)
	
	oView:AddField("VIEW_SVD", oStruSVD, 'SVDMASTER' )
	oView:AddField("VIEW_SS", oStruSS, 'SVDMASTER' )
	oView:AddGrid("VIEW_SVQ", oStruSVQ, 'SVQDETAIL' )
	oView:EnableTitleView("VIEW_SVQ",STR0008) 
	
	oView:CreateHorizontalBox( 'UPPER' , 150 ,,.T.)
	oView:CreateHorizontalBox( 'LOWER' , 100 )
	
	oView:CreateVerticalBox( 'UPPER_M' ,80 , "UPPER")
	oView:CreateVerticalBox( 'UPPER_R' ,20 , "UPPER")

	oView:CreateHorizontalBox( 'UPPER_M_TOP' , 50, "UPPER_M")
	oView:CreateHorizontalBox( 'UPPER_M_BOTTOM', 50,  "UPPER_M")
	
	oView:SetOwnerView( 'VIEW_SVD', 'UPPER_M_TOP')
	oView:SetOwnerView( 'VIEW_SS', 'UPPER_M_BOTTOM')
	oView:SetOwnerView( 'VIEW_SVQ', 'LOWER')
	
	//Adiciona campos na view
	addFldView(oStruSVD,oStruSS,oStruSVQ)
	
	oStruSVQ:RemoveField( 'VQ_ORIGEM' )
	oStruSVQ:RemoveField( 'VQ_DESTINO' ) 
	
	//Consulta padr�o para o campo FILIAL DESTINO
	oStruSS:SetProperty("VD_FILDEST", MVC_VIEW_LOOKUP, "SOP001")

	//Se o ambiente n�o est� parametrizado para multi-empresas, n�o exibe os campos de filial origem e filial destino
	If !lExibeFil
		oStruSVD:RemoveField("VD_FILORG")
		oStruSS:RemoveField("VD_FILDEST")
	EndIf
Return oView

//--------------------------------------------------------------------
/*/{Protheus.doc} PCPA123MNU()
Fun��o que executa a view do programa. 
Necess�rio desvio da abertura por essa fun��o para que os campos 
sejam exibidos corretamente de acordo com a regra definida pela fun��o multiFil()

@param nOpcao	- Identifica a opera��o que est� sendo executada (inclus�o/exclus�o/altera��o/visualiza��o)

@author Lucas Konrad Fran�a
@since 10/10/2018
@version 1.0
@return nOK	- Identifica se o usu�rio confirmou (nOk==0) ou cancelou (nOk==1) a opera��o.
/*/
//--------------------------------------------------------------------
Function PCPA123MNU(nOpcao)
	Local nOpc   := 2
	Local nOk    := 0
	Local cTexto := ""

	Do Case
		Case nOpcao == 2
			nOpc   := MODEL_OPERATION_VIEW
			cTexto := STR0002 //Visualizar
		Case nOpcao == 3
			nOpc   := MODEL_OPERATION_INSERT
			cTexto := STR0003 //Incluir
		Case nOpcao == 4
			nOpc   := MODEL_OPERATION_UPDATE
			cTexto := STR0004 //Alterar
		Case nOpcao == 5
			nOpc   := MODEL_OPERATION_DELETE
			cTexto := STR0005 //Excluir
	EndCase
	nOk := FWExecView(cTexto, "PCPA123", nOpc,,,,,,,,,)
Return nOk

//--------------------------------------------------------------------
/*/{Protheus.doc} addFldMdl()
Adiciona campos no modelo de dados (ModelDef)

@param oStruSVD	- Objeto Struct da tabela SVD.
@param oStruSVQ	- Objeto Struct da tabela SVQ.

@author Lucas Konrad Fran�a
@since 10/10/2018
@version 1.0
@return Nil
/*/
//--------------------------------------------------------------------
Static Function addFldMdl(oStruSVD,oStruSVQ)
	//Adiciona o campo da Filial para mostrar em tela (Filial Origem.)
	oStruSVD:AddField( ;
		STR0020	,;	// 	[01]  C   Titulo do campo   //"Filial Orig."
		STR0019	,;	// 	[02]  C   ToolTip do campo //"Filial origem"
		"VD_FILORG"				,;	// 	[03]  C   Id do Field
		"C"						,;	// 	[04]  C   Tipo do campo
		TAMSX3("VD_FILIAL")[1]	,;	// 	[05]  N   Tamanho do campo
		0						,;	// 	[06]  N   Decimal do campo
		FWBuildFeature( STRUCT_FEATURE_VALID, "a123VldFil('VD_FILORG')")	,;	// 	[07]  B   Code-block de valida��o do campo
		NIL						,;	// 	[08]  B   Code-block de valida��o When do campo
		Nil						,;	//	[09]  A   Lista de valores permitido do campo
		.F.						,;	//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
		FWBuildFeature( STRUCT_FEATURE_INIPAD, "cFilAnt")	,;	//	[11]  B   Code-block de inicializacao do campo
		.F.						,;	//	[12]  L   Indica se trata-se de um campo chave
		.F.						,;	//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
		.T.						)	// 	[14]  L   Indica se o campo � virtual

	oStruSVD:AddField( ;           // Ord. Tipo Desc.
		STR0009, ;           		// [01] C Titulo do campo
		STR0009, ;           	  // [02] C ToolTip do campo
		"VD_DESCORI", ;            // [03] C identificador (ID) do Field
		'C' , ;                    // [04] C Tipo do campo
		TamSx3("VN_DESCGRU")[1], ; // [05] N Tamanho do campo
		0 , ;                      // [06] N Decimal do campo
		NIL, ;                     // [07] B Code-block de valida��o do campo
		NIL, ;                     // [08] B Code-block de valida��o When do campo
		, ;                        // [09] A Lista de valores permitido do campo
		.F., ;                     // [10] L Indica se o campo tem preenchimento obrigat�rio
		FWBuildFeature( STRUCT_FEATURE_INIPAD, "l123IniGrp(1)"),; // [11] B Code-block de inicializacao do campo
		.F., ;                     // [12] L Indica se trata de um campo chave
		.F., ;                     // [13] L Indica se o campo pode receber valor em uma opera��o de update.
		.T. )                      // [14] L Indica se o campo � virtual
	
	oStruSVD:AddField( ;           // Ord. Tipo Desc.
		STR0010, ;                 // [01] C Titulo do campo
		STR0010, ;                 // [02] C ToolTip do campo
		"VD_DESCDES", ;            // [03] C identificador (ID) do Field
		'C' , ;                    // [04] C Tipo do campo
		TamSx3("VN_DESCGRU")[1], ; // [05] N Tamanho do campo
		0 , ;                      // [06] N Decimal do campo
		NIL, ;                     // [07] B Code-block de valida��o do campo
		NIL, ;                     // [08] B Code-block de valida��o When do campo
		, ;                        // [09] A Lista de valores permitido do campo
		.F., ;                     // [10] L Indica se o campo tem preenchimento obrigat�rio
		FWBuildFeature( STRUCT_FEATURE_INIPAD, "l123IniGrp(2)"),; // [11] B Code-block de inicializacao do campo
		.F., ;                     // [12] L Indica se trata de um campo chave
		.F., ;                     // [13] L Indica se o campo pode receber valor em uma opera��o de update.
		.T. )                      // [14] L Indica se o campo � virtual
	                   
	oStruSVQ:AddField( ;           // Ord. Tipo Desc.
		STR0011, ;                 // [01] C Titulo do campo
		STR0011, ;                 // [02] C ToolTip do campo
		"B1_DESC", ;               // [03] C identificador (ID) do Field
		'C' , ;                    // [04] C Tipo do campo
		60, ; 	  // [05] N Tamanho do campo
		0 , ;                      // [06] N Decimal do campo
		NIL, ;                     // [07] B Code-block de valida��o do campo
		NIL, ;                     // [08] B Code-block de valida��o When do campo
		, ;                        // [09] A Lista de valores permitido do campo
		.F., ;                     // [10] L Indica se o campo tem preenchimento obrigat�rio
		FWBuildFeature( STRUCT_FEATURE_INIPAD, "l132IniPrd()"),;     // [11] B Code-block de inicializacao do campo
		.F., ;                     // [12] L Indica se trata de um campo chave
		.F., ;                     // [13] L Indica se o campo pode receber valor em uma opera��o de update.
		.T. )                      // [14] L Indica se o campo � virtual

Return Nil

//--------------------------------------------------------------------
/*/{Protheus.doc} addFldView()
Adiciona campos no modelo de dados (ModelDef)

@param oStruSVD	- Objeto Struct da tabela SVD (ORIGEM).
@param oStruSS	- Objeto Struct da tabela SVD (DESTINO).
@param oStruSVQ	- Objeto Struct da tabela SVQ.

@author Lucas Konrad Fran�a
@since 10/10/2018
@version 1.0
@return Nil
/*/
//--------------------------------------------------------------------
Static Function addFldView(oStruSVD,oStruSS,oStruSVQ)
	// Adicionado campo de filial (Filial origem)
	oStruSVD:AddField( 			;
		"VD_FILORG"				,;	// [01]  C   Nome do Campo
		"00"					,;	// [02]  C   Ordem
		STR0020	,;	// [03]  C   Titulo do campo //"Filial Orig."
		STR0019	,;	// [04]  C   Descricao do campo //"Filial origem"
		NIL						,;	// [05]  A   Array com Help
		"C"						,;	// [06]  C   Tipo do campo
		""						,;	// [07]  C   Picture
		NIL						,;	// [08]  B   Bloco de Picture Var
		"SOP001"					,;	// [09]  C   Consulta F3
		.T.						,;	// [10]  L   Indica se o campo � alteravel
		NIL						,;	// [11]  C   Pasta do campo
		NIL						,;	// [12]  C   Agrupamento do campo
		NIL						,;	// [13]  A   Lista de valores permitido do campo (Combo)
		NIL						,;	// [14]  N   Tamanho maximo da maior op��o do combo
		NIL						,;	// [15]  C   Inicializador de Browse
		.T.						,;	// [16]  L   Indica se o campo � virtual
		NIL						,;	// [17]  C   Picture Variavel
		NIL						)	// [18]  L   Indica pulo de linha ap�s o campo	

	oStruSVD:AddField( ;      // Ord. Tipo Desc.
		"VD_DESCORI" , ;       // [01] C Nome do Campo
		"2" , ;               // [02] C Ordem
		AllTrim( STR0009 ) , ; // [03] C Titulo do campo 
		AllTrim( STR0009 ) , ; // [04] C Descri��o do campo 
		{ STR0009 } , ;        // [05] A Array com Help 
		'C' , ;                // [06] C Tipo do campo
		''  , ;                // [07] C Picture
		NIL , ;                // [08] B Bloco de Picture Var
		''  , ;                // [09] C Consulta F3
		.F. , ;                // [10] L Indica se o campo � edit�vel
		NIL , ;                // [11] C Pasta do campo
		NIL , ;                // [12] C Agrupamento do campo
		NIL , ;                // [13] A Lista de valores permitido do campo (Combo)
		NIL , ;                // [14] N Tamanho M�ximo da maior op��o do combo
		NIL , ;                // [15] C Inicializador de Browse
		.T. , ;                // [16] L Indica se o campo � virtual
		NIL )                  // [17] C Picture Vari�vel    

	oStruSS:AddField( ;       // Ord. Tipo Desc.
		"VD_DESCDES" , ;       // [01] C Nome do Campo
		"2" , ;               // [02] C Ordem
		AllTrim( STR0010 ) , ; // [03] C Titulo do campo 
		AllTrim( STR0010 ) , ; // [04] C Descri��o do campo 
		{ STR0010 } , ;        // [05] A Array com Help 
		'C' , ;                // [06] C Tipo do campo
		''  , ;                // [07] C Picture
		NIL , ;                // [08] B Bloco de Picture Var
		''  , ;                // [09] C Consulta F3
		.F. , ;                // [10] L Indica se o campo � edit�vel
		NIL , ;                // [11] C Pasta do campo
		NIL , ;                // [12] C Agrupamento do campo
		NIL , ;                // [13] A Lista de valores permitido do campo (Combo)
		NIL , ;                // [14] N Tamanho M�ximo da maior op��o do combo
		NIL , ;                // [15] C Inicializador de Browse
		.T. , ;                // [16] L Indica se o campo � virtual
		NIL )                  // [17] C Picture Vari�vel          

	oStruSVQ:AddField( ;       // Ord. Tipo Desc.
		"B1_DESC" , ;          // [01] C Nome do Campo
		"05" , ;               // [02] C Ordem
		AllTrim( STR0011 ) , ; // [03] C Titulo do campo 
		AllTrim( STR0011 ) , ; // [04] C Descri��o do campo 
		{STR0011 } , ;         // [05] A Array com Help 
		'C' , ;                // [06] C Tipo do campo
		''  , ;                // [07] C Picture
		NIL , ;                // [08] B Bloco de Picture Var
		''  , ;                // [09] C Consulta F3
		.F. , ;                // [10] L Indica se o campo � edit�vel
		NIL , ;                // [11] C Pasta do campo
		NIL , ;                // [12] C Agrupamento do campo
		NIL , ;                // [13] A Lista de valores permitido do campo (Combo)
		NIL , ;                // [14] N Tamanho M�ximo da maior op��o do combo
		NIL , ;                // [15] C Inicializador de Browse
		.T. , ;                // [16] L Indica se o campo � virtual
		NIL )                  // [17] C Picture Vari�vel

Return Nil

//--------------------------------------------------------------------
/*/{Protheus.doc} lDescPrd()
Funcao para preencher o campo virtual de descricao do produto
@author Douglas Heydt
@since 11/06/2018
@version 1.0
@return cDescri
/*/
//--------------------------------------------------------------------
Function lDescPrd()

	Local oModel := FWModelActive()
	Local oModelSVQ:= oModel:GetModel("SVQDETAIL")
	Local aArea 	:= GetArea()
	Local cFilSB1	:= xFilial("SB1")
	Local cCodSB1  := Iif(!Empty(oModelSVQ:GetValue("VQ_PRODUTO")), oModelSVQ:GetValue("VQ_PRODUTO"),&(READVAR()))
	Local cDescri
	If !Empty(cCodSB1)		
		dbSelectArea("SB1")
		dbSetOrder(1)
		If dbSeek( cFilSB1+cCodSB1)
				 cDescri := Alltrim(SB1->B1_DESC)
		EndIf       
	EndIf
	RestArea(aArea)
Return cDescri

//--------------------------------------------------------------------
/*/{Protheus.doc} DiffDate()
Verifica se as datas sao validas ( inicial menor que final )
@author Douglas Heydt
@since 14/06/2018
@version 1.0
@return lRet
/*/
//--------------------------------------------------------------------
Function DiffDate(dtIni, dtFin)

	Local lRet := .T.
	If YEAR(dtFin) == YEAR(dtIni)
		If MONTH(dtFin) == MONTH(dtIni)
			If DAY(dtFin) < DAY(dtIni)
				lRet := .F.
			EndIf
		ElseIf MONTH(dtFin) < MONTH(dtIni)
			lRet := .F.
		EndIf
	ElseIf YEAR(dtFin) < YEAR(dtIni)
		lRet := .F.			
	EndIf
	
Return lRet

//--------------------------------------------------------------------
/*/{Protheus.doc} l123Desc()
Funcao para preencher os campos descri��o dos grupos de armaz�m
@param campo : 1- Origem 2- Destino
@author Douglas Heydt
@since 11/06/2018
@version 1.0
@return cDescri
/*/
//--------------------------------------------------------------------
Function l123Desc(cField)
	Local oModel := FWModelActive()
	Local oModelSVD:= oModel:GetModel("SVDMASTER")
	Local aArea 	:= GetArea()
	Local cFilSVN	:= xFilial("SVN")
	Local cDescri
	Local cCodSVN

	If cField == 1
		cCodSVN := oModelSVD:GetValue("VD_ORIGEM")
		cFilSVN := oModelSVD:GetValue("VD_FILORG")
	ElseIf cField == 2
		cCodSVN := oModelSVD:GetValue("VD_DESTINO")
		cFilSVN := oModelSVD:GetValue("VD_FILDEST")
	EndIf
	If !Empty(cCodSVN)		
		dbSelectArea("SVN")
		dbSetOrder(1)
		If dbSeek( cFilSVN+cCodSVN)
				 cDescri := Alltrim(SVN->VN_DESCGRU)
		EndIf       
	EndIf
	RestArea(aArea)
Return cDescri

//--------------------------------------------------------------------
/*/{Protheus.doc} l123IniGrp()
Inicializador padr�o dos campos descri��o dos grupos de armaz�m
@author Douglas Heydt
@since 21/06/2018
@version 1.0
@return cDescri
/*/
//--------------------------------------------------------------------
Function l123IniGrp(cTipo)
	Local oModel  := FWModelActive()
	Local nOp     := oModel:GetOperation()
	Local cDescri := ""
	Local cGrupo  := ""
	Local cFilReg := ""

	If nOp <> 3 
		If cTipo == 1
			cFilReg := SVD->VD_FILIAL
			cGrupo  := SVD->VD_ORIGEM
		Else
			cFilReg := SVD->VD_FILDEST
			cGrupo  := SVD->VD_DESTINO
		EndIf
		If !Empty(cGrupo) .And. !Empty(cFilReg)
			SVN->(dbSetOrder(1))
			If SVN->(dbSeek(cFilReg+cGrupo))
				cDescri := SVN->VN_DESCGRU
			EndIf
		EndIf
	EndIf
Return cDescri

//--------------------------------------------------------------------
/*/{Protheus.doc} l132IniPrd()
Inicializador padr�o dos campos descri��o dos produtos
@author Douglas Heydt
@since 21/06/2018
@version 1.0
@return cDescri
/*/
//--------------------------------------------------------------------
Function l132IniPrd()
	Local oModel := FWModelActive()
	Local nOp := oModel:GetOperation()
	Local cDescri := ""

	If nOp <> 3 .And. !Empty(SVQ->VQ_PRODUTO)
		cDescri := POSICIONE("SB1", 1, xFilial("SB1") + SVQ->VQ_PRODUTO, "B1_DESC")
	EndIf
Return cDescri

//--------------------------------------------------------------------
/*/{Protheus.doc} a123VldFil()
Fun��o para validar a filial informada.

@param cField	- Identifica qual � o campo (Origem ou Destino)

@author Lucas Konrad Fran�a
@since 09/10/2018
@version 1.0
@return lRet	- Indica se o valor informado est� v�lido.
/*/
//--------------------------------------------------------------------
Function a123VldFil(cField)
	Local lRet := .T.
	Local oModel := FWModelActive()
	Local cFil   := oModel:GetModel("SVDMASTER"):GetValue(cField)

	If !Empty(cFil)
		//Valida se a filial existe no arquivo de empresas
		If !FwFilExist(cEmpAnt,cFil)
			lRet := .F.
			Help(,,"Help",,STR0015,; //"Filial informada n�o existe."
						1,0,,,,,,{STR0016}) //"Informe uma filial que esteja cadastrada para o grupo de empresas utilizado."
		EndIf

		//Valida se a filial est� cadastrada no grupo de empresas (PCPA106)
		If lRet .And. !multiFil(cFil)
			lRet := .F.
			Help(,,"Help",,STR0017,; //"Filial informada n�o est� cadastrada no cadastro de Empresas Centralizadoras."
						1,0,,,,,,{STR0018}) //"Informe uma filial que esteja cadastrada no cadastro de Empresas Centralizadoras (PCPA106)."
		EndIf
	EndIf
Return lRet

//--------------------------------------------------------------------
/*/{Protheus.doc} a123ConPad()
Inicializa a consulta padr�o dos grupos de armaz�m, de acordo com a filial informada.
Consulta padr�o deve ser utilizada apenas para os campos VD_ORIGEM e VD_DESTINO.

@return	- lRet	- Identifica se o usu�rio selecionou algum item na consulta padr�o.

@author Lucas Konrad Fran�a
@since 09/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Function a123ConPad()
	Local lRet    := .T.
	Local oModel  := FWModelActive()
	Local cField  := ReadVar()
	Local cFilOri := cFilAnt
	
	//Verifica se est� utilizando a consulta padr�o no campo VD_ORIGEM ou no campo VD_DESTINO para atualizar a cFilAnt.
	If "VD_ORIGEM" $ cField
		cFilAnt := oModel:GetModel("SVDMASTER"):GetValue("VD_FILORG")
	Else
		cFilAnt := oModel:GetModel("SVDMASTER"):GetValue("VD_FILDEST")
	EndIf

	//Executa a consulta padr�o.
	lRet := ConPad1(,,,"SVNCOD")

	//Retorna filial original
	cFilAnt := cFilOri
Return lRet

//--------------------------------------------------------------------
/*/{Protheus.doc} multiFil()
Identifica se a filial pertence a um grupo de empresas cadastrado no PCPA106.

@param	- cFil	- Filial para valida��o. Se n�o passado, assume o cFilAnt.
@return	- lRet	- Indica se a filial pertence a algum grupo de empresas.

@author Lucas Konrad Fran�a
@since 09/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function multiFil(cFil)
	Local lRet      := .F.
	Local cQuery    := ""
	Local cAliasQry := "FILGRUPO"
	Local aInfoFil  := {}
	Local cGrpEmp   := "" 
	Local cCodEmp   := "" 
	Local cCodUNeg  := "" 
	Local cCodFil   := "" 

	Default cFil    := cFilAnt

	aInfoFil := FWArrFilAtu(cEmpAnt,cFil)

	If Len(aInfoFil) > 0
		//Separa o c�digo da filial em Grupo de empresa/Empresa/Unidade de neg�cio/Filial para montar a query.
		cGrpEmp   := aInfoFil[SM0_GRPEMP]
		cCodEmp   := aInfoFil[SM0_EMPRESA]
		cCodUNeg  := aInfoFil[SM0_UNIDNEG]
		cCodFil   := aInfoFil[SM0_FILIAL]
	
		cQuery := " SELECT SOP.OP_CDEPCZ "
		cQuery +=   " FROM " + RetSqlName("SOP") + " SOP "
		cQuery +=  " WHERE SOP.OP_FILIAL  = '" + xFilial("SOP") + "' "
		cQuery +=    " AND SOP.D_E_L_E_T_ = ' ' "
		cQuery +=    " AND ((SOP.OP_CDEPCZ  = '" + cGrpEmp + "' "
		cQuery +=      " AND SOP.OP_EMPRCZ  = '" + cCodEmp + "' "
		cQuery +=      " AND SOP.OP_UNIDCZ  = '" + cCodUNeg + "' "
		cQuery +=      " AND SOP.OP_CDESCZ  = '" + cCodFil + "') "
		cQuery +=      " OR (SOP.OP_CDEPGR  = '" + cGrpEmp + "' "
		cQuery +=      " AND SOP.OP_EMPRGR  = '" + cCodEmp + "' "
		cQuery +=      " AND SOP.OP_UNIDGR  = '" + cCodUNeg + "' "
		cQuery +=      " AND SOP.OP_CDESGR  = '" + cCodFil + "')) "

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQry,.T.,.T.)

		If (cAliasQry)->(!Eof())
			lRet := .T.
		EndIf
		(cAliasQry)->(dbCloseArea())
	EndIf
Return lRet

//--------------------------------------------------------------------
/*/{Protheus.doc} A123FILSOP()
Monta a tela de consulta padr�o com base nas informa��es de filiais da tabela SOP.

@return	- lRet	- Indica se o usu�rio selecionou alguma informa��o.

@author Lucas Konrad Fran�a
@since 10/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Function A123FILSOP()
	Local lRet     := .F.
	Local nRet     := 0
	Local aLista   := {}
	Local cQuery   := ""
	Local cAlias   := "FILSOP"
	Local nTamEmp  := Len(FWSM0Layout(cEmpAnt,1))
	Local nTamUNeg := Len(FWSM0Layout(cEmpAnt,2))
	Local nTamFil  := Len(FWSM0Layout(cEmpAnt,3))
	Local nTamSM0  := FWSizeFilial(cEmpAnt)
	Local cFil     := ""

	//Busca as filiais que pertencem ao grupo de empresas logado.
	cQuery := " SELECT DISTINCT SOP.OP_EMPRGR EMPRESA, "
	cQuery +=                 " SOP.OP_UNIDGR UNIDADE, "
	cQuery +=                 " SOP.OP_CDESGR FILIAL "
	cQuery +=   " FROM " + RetSqlName("SOP") + " SOP "
	cQuery +=  " WHERE SOP.D_E_L_E_T_ = ' ' "
	cQuery +=    " AND SOP.OP_FILIAL  = '" + xFilial("SOP") + "' "
	cQuery +=    " AND SOP.OP_CDEPGR  = '" + cEmpAnt + "' "
	cQuery +=    " AND SOP.OP_CDEPCZ  = '" + cEmpAnt + "' "
	cQuery += " UNION "
	cQuery += " SELECT DISTINCT SOP.OP_EMPRCZ EMPRESA, "
	cQuery +=                 " SOP.OP_UNIDCZ UNIDADE, "
	cQuery +=                 " SOP.OP_CDESCZ FILIAL "
	cQuery +=   " FROM " + RetSqlName("SOP") + " SOP "
	cQuery +=  " WHERE SOP.D_E_L_E_T_ = ' ' "
	cQuery +=    " AND SOP.OP_FILIAL  = '" + xFilial("SOP") + "' "
	cQuery +=    " AND SOP.OP_CDEPGR  = '" + cEmpAnt + "' "
	cQuery +=    " AND SOP.OP_CDEPCZ  = '" + cEmpAnt + "' "
	cQuery += " ORDER BY 1,2,3 "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.T.,.T.)

	While (cAlias)->(!Eof())
		cFil := PadR(PadR((cAlias)->(EMPRESA),nTamEmp) + PadR((cAlias)->(UNIDADE),nTamUNeg) + PadR((cAlias)->(FILIAL),nTamFil),nTamSM0)
		aAdd(aLista,{cFil,FwFilName(cEmpAnt,cFil)})
		(cAlias)->(dbSkip())
	End
	(cAlias)->(dbCloseArea())

	DEFINE MSDIALOG oDlg TITLE STR0014 FROM 0,0 TO 243,500 PIXEL //"Filiais"

	oGrupo := TcBrowse():New( 002, 002, 245, 100,,,, oDlg,,,,,,,,,,,, .F.,, .T.,, .F., )
	oGrupo:AddColumn(TcColumn():New(STR0012,{ || OemToAnsi(aLista[oGrupo:nAt,1]) },"",nil,nil,nil,040,.F.,.F.,nil,nil,nil,.F.,nil)) //"C�digo"
	oGrupo:AddColumn(TcColumn():New(STR0013,{ || OemToAnsi(aLista[oGrupo:nAt,2]) },"",nil,nil,nil,200,.F.,.F.,nil,nil,nil,.F.,nil)) //Filial"
	oGrupo:SetArray(aLista)
	oGrupo:bLDblClick := {|| {oDlg:End(), nRet := oGrupo:nAt}}

	DEFINE SBUTTON FROM 107,003 TYPE 1 ACTION (oDlg:End(), nRet := oGrupo:nAt)  ENABLE OF oDlg
	DEFINE SBUTTON FROM 107,032 TYPE 2 ACTION (oDlg:End())  ENABLE OF oDlg
	ACTIVATE MSDIALOG oDlg CENTER

	If nRet > 0
		VAR_IXB := aLista[nRet,1]
		lRet := .T.
	Else
		lRet := .F.
	EndIf
Return lRet