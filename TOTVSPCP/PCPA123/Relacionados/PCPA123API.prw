#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"

//Define constantes para utilizar nos arrays.
//Em outros fontes, utilizar a fun��o A123APICnt para recuperar o valor das constantes.
//Ao criar novas constantes, adicionar na fun��o A123APICnt
//Campos do Grupo (cabe�alho)
#DEFINE ARRAY_MALHA_CAB_POS_FILIAL  1
#DEFINE ARRAY_MALHA_CAB_POS_CODE	2
#DEFINE ARRAY_MALHA_CAB_POS_ARMORG  3
#DEFINE ARRAY_MALHA_CAB_POS_FILDES  4
#DEFINE ARRAY_MALHA_CAB_POS_ARMDES  5
#DEFINE ARRAY_MALHA_CAB_POS_LISTA   6
#DEFINE ARRAY_MALHA_CAB_SIZE        6

//Campos das exce��es (lista)
#DEFINE ARRAY_MALHA_EXCE_POS_PROD  	 1
#DEFINE ARRAY_MALHA_EXCE_POS_DTINI 	 2
#DEFINE ARRAY_MALHA_EXCE_POS_DTFIN   3
#DEFINE ARRAY_MALHA_EXCE_POS_QTDINI  4
#DEFINE ARRAY_MALHA_EXCE_POS_QTDFIN  5
#DEFINE ARRAY_MALHA_EXCE_SIZE        5

/*/{Protheus.doc} A123APICnt
Recupera o valor das constantes utilizadas para
auxiliar na montagem do array de vers�o da produ��o para integra��o.

@type  Function
@author douglas.heydt
@since 20/09/2019
@version P12.1.27
@param cInfo, Caracter, Define qual constante se deseja recuperar o valor.
@return nValue, Numeric, Valor da constante
/*/
Function A123APICnt(cInfo)
	Local nValue := ARRAY_MALHA_CAB_SIZE
	Do Case
		Case cInfo == "ARRAY_MALHA_CAB_POS_FILIAL"
			nValue := ARRAY_MALHA_CAB_POS_FILIAL
		Case cInfo == "ARRAY_MALHA_CAB_POS_CODE"
			nValue := ARRAY_MALHA_CAB_POS_CODE
		Case cInfo == "ARRAY_MALHA_CAB_POS_ARMORG"
			nValue := ARRAY_MALHA_CAB_POS_ARMORG
		Case cInfo == "ARRAY_MALHA_CAB_POS_FILDES"
			nValue := ARRAY_MALHA_CAB_POS_FILDES
		Case cInfo == "ARRAY_MALHA_CAB_POS_ARMDES"
			nValue := ARRAY_MALHA_CAB_POS_ARMDES
        Case cInfo == "ARRAY_MALHA_CAB_POS_LISTA"
			nValue := ARRAY_MALHA_CAB_POS_LISTA
		Case cInfo == "ARRAY_MALHA_CAB_SIZE"
			nValue := ARRAY_MALHA_CAB_SIZE
		Case cInfo == "ARRAY_MALHA_EXCE_POS_PROD"
			nValue := ARRAY_MALHA_EXCE_POS_PROD
		Case cInfo == "ARRAY_MALHA_EXCE_POS_DTINI"
			nValue := ARRAY_MALHA_EXCE_POS_DTINI
		Case cInfo == "ARRAY_MALHA_EXCE_POS_DTFIN"
			nValue := ARRAY_MALHA_EXCE_POS_DTFIN
		Case cInfo == "ARRAY_MALHA_EXCE_POS_QTDINI"
			nValue := ARRAY_MALHA_EXCE_POS_QTDINI
		Case cInfo == "ARRAY_MALHA_EXCE_POS_QTDFIN"
			nValue := ARRAY_MALHA_EXCE_POS_QTDFIN
		Case cInfo == "ARRAY_MALHA_EXCE_SIZE"
			nValue := ARRAY_MALHA_EXCE_SIZE
		Otherwise
			nValue := ARRAY_MALHA_EXCE_SIZE
	EndCase
Return nValue

/*/{Protheus.doc} PCPA123API
Eventos de integra��o do Cadastro de Grupos de Armaz�m

@author douglas.heydt
@since 20/09/2019
@version P12.1.27
/*/
CLASS PCPA123API FROM FWModelEvent
	DATA lIntegraMRP    AS LOGIC
	DATA lIntegraOnline AS LOGIC
	DATA oTTPend        AS OBJECT

	METHOD New() CONSTRUCTOR
	METHOD InTTS(oModel, cModelId)
	METHOD Destroy()

ENDCLASS

/*/{Protheus.doc} NEW
M�todo construtor do evento de integra��o das integra��es do Cadastro de Malhas de Distribui��o

@author douglas.heydt
@since 20/09/2019
@version P12.1.27
/*/
METHOD New() CLASS PCPA123API

	Local lUsaMalha := SuperGetMV("MV_PCPMADI",.F.,.F.)

	::lIntegraMRP    := .F.
	::lIntegraOnline := .F.

	If lUsaMalha
		::lIntegraMRP := IntNewMRP("MRPTRANSPORTINGLANES", @::lIntegraOnline)

		If ::lIntegraMRP .And. ::lIntegraOnline
			::oTTPend    := TTPendMRP()
		EndIf
	EndIf

Return Self

/*/{Protheus.doc} InTTS
M�todo que � chamado pelo MVC quando ocorrer as a��es do commit Ap�s as grava��es
por�m antes do final da transa��o.

@author douglas.heydt
@since 20/09/2019
@version P12.1.27
@param oModel  , Object  , Modelo principal
@param cModelId, Caracter, Id do submodelo
@return Nil
/*/
METHOD InTTS(oModel, cModelId) CLASS PCPA123API

	//S� executa a integra��o se estiver parametrizado como Online
	If ::lIntegraMRP == .F. .Or. ::lIntegraOnline == .F.
		Return
	EndIf

	integraAPI(oModel, Self)

Return Nil

/*/{Protheus.doc} integraAPI
Integra dados com a API
@author douglas.heydt
@since 20/09/2019
@version P12.1.27
@param oModel, Object, Modelo principal
@param Self  , objeto, instancia atual desta classe
@return Nil
/*/
Static Function integraAPI(oModel, Self)
	Local aDadosDel  := {}
	Local aDadosInc  := {}
	Local aDadosDet  := {}
	Local cFilialSVD := ""
	Local nIndex     := 0
	Local nTotal     := 0
	Local nPos       := 0
	Local oMdlCab    := oModel:GetModel("SVDMASTER")
	Local oMdlDet    := oModel:GetModel("SVQDETAIL")

	If Empty(oMdlCab:GetValue("VD_FILIAL"))
		cFilialSVD := xFilial("SVD")
	Else
		cFilialSVD := oMdlCab:GetValue("VD_FILIAL")
	EndIf

	If oModel:GetOperation() <> MODEL_OPERATION_DELETE
		aAdd(aDadosInc, Array(ARRAY_MALHA_CAB_SIZE))

		//Adiciona as informa��es de cabe�alho no array de inclus�o/atualiza��o
		aDadosInc[1][ARRAY_MALHA_CAB_POS_FILIAL] := cFilialSVD
		aDadosInc[1][ARRAY_MALHA_CAB_POS_ARMORG ] := oMdlCab:GetValue("VD_ORIGEM")
        aDadosInc[1][ARRAY_MALHA_CAB_POS_CODE] := cFilialSVD + cValToChar(oMdlCab:GetDataId())
        aDadosInc[1][ARRAY_MALHA_CAB_POS_FILDES ] := oMdlCab:GetValue("VD_FILDEST")
        aDadosInc[1][ARRAY_MALHA_CAB_POS_ARMDES ] := oMdlCab:GetValue("VD_DESTINO")
		aDadosInc[1][ARRAY_MALHA_CAB_POS_LISTA ] := {}

		aDadosDet := Array(ARRAY_MALHA_EXCE_SIZE)

        //Adiciona as linhas para enviar � API
        nTotal := oMdlDet:Length()

        For nIndex := 1 To nTotal
            If oMdlDet:IsDeleted(nIndex) .Or. Empty(oMdlDet:GetValue("VQ_PRODUTO", nIndex))
                Loop
            EndIf

            //Adiciona nova linha no array de inclus�o/atualiza��o
            aSize(aDadosDet, 0)
            aDadosDet := Array(ARRAY_MALHA_EXCE_SIZE)
            nPos      := Len(aDadosDet)

            //Adiciona as informa��es dos armaz�ns no array de inclus�o/atualiza��o
            aDadosDet[ARRAY_MALHA_EXCE_POS_PROD]    := oMdlDet:GetValue("VQ_PRODUTO"    , nIndex)
            aDadosDet[ARRAY_MALHA_EXCE_POS_DTINI]   := oMdlDet:GetValue("VQ_DTINI"      , nIndex)
            aDadosDet[ARRAY_MALHA_EXCE_POS_DTFIN]   := oMdlDet:GetValue("VQ_DTFIM"      , nIndex)
            aDadosDet[ARRAY_MALHA_EXCE_POS_QTDINI]  := oMdlDet:GetValue("VQ_QNTINI"     , nIndex)
            aDadosDet[ARRAY_MALHA_EXCE_POS_QTDFIN]  := oMdlDet:GetValue("VQ_QNTFIM"     , nIndex)

            aAdd(aDadosInc[1][ARRAY_MALHA_CAB_POS_LISTA], aClone(aDadosDet))
        Next nIndex

    Else
        //Faz o DELETE do Grupo
        aAdd(aDadosDel, Array(ARRAY_MALHA_CAB_SIZE))
        aDadosDel[1][ARRAY_MALHA_CAB_POS_FILIAL] := cFilialSVD
        aDadosDel[1][ARRAY_MALHA_CAB_POS_CODE ] := cFilialSVD + cValToChar(oMdlCab:GetDataId())
        PCPA123INT("DELETE", aDadosDel, Self:oTTPend)
    EndIf

	//Se tem alguma informa��o para inserir
	If Len(aDadosInc) > 0
		PCPA123INT("INSERT", aDadosInc, Self:oTTPend)
		aSize(aDadosInc, 0)
	EndIf

Return

/*/{Protheus.doc} Destroy
M�todo destrutor do evento de integra��o.

@author douglas.heydt
@since 20/09/2019
@version P12.1.27
/*/
METHOD Destroy() CLASS PCPA123API
	//Deleta a tabela tempor�ria.
	If ::lIntegraMRP .And. ::lIntegraOnline
		::oTTPend:Delete()
		::oTTPend := Nil
	EndIf
Return Self

/*/{Protheus.doc} PCPA123INT
Fun��o que executa a integra��o da Malha de Distribui��o com o MRP.

@type  Function
@author douglas.heydt
@since 20/09/2019
@version P12.1.27
@param cOperation, Caracter, Opera��o que ser� executada ('DELETE'/'INSERT'/'SYNC')
@param aDados    , Array   , Array com os dados que devem ser integrados com o MRP.
@param oTTPend   , Object  , Objeto da tabela tempor�ria utilizada no processamento das pend�ncias. Tabela criada pela fun��o TTPendMRP
@param cUUID     , Caracter, Identificador do processo do SCHEDULE. Utilizado para atualiza��o de pend�ncias.
@param lOnlyDel  , Logic   , Indica que est� sendo executada uma opera��o de Sincroniza��o apenas excluindo os dados existentes (envia somente filial).
@return Nil
/*/
Function PCPA123INT(cOperation, aDados, oTTPend, aSuccess, aError, cUUID, lOnlyDel)
	Local aReturn   := {}
	Local lAllError := .F.
	Local nIndexCab := 0
	Local nIndexLst := 0
	Local nTotalCab := 0
	Local nTotalLst := 0
	Local oJsonData := Nil
	Local cApi 		:= "MRPTRANSPORTINGLANES"

	Default aSuccess := {}
	Default aError   := {}
	Default lOnlyDel := .F.

	nTotalCab := Len(aDados)
	oJsonData := JsonObject():New()

	oJsonData["items"] := Array(nTotalCab)

	For nIndexCab := 1 To nTotalCab
		oJsonData["items"][nIndexCab] := JsonObject():New()
		oJsonData["items"][nIndexCab]["branchId"]   := aDados[nIndexCab][ARRAY_MALHA_CAB_POS_FILIAL]
		If ! (lOnlyDel .And. cOperation == "SYNC")
			oJsonData["items"][nIndexCab]["code"]       := aDados[nIndexCab][ARRAY_MALHA_CAB_POS_CODE]

			If cOperation $ "|INSERT|SYNC|"

				oJsonData["items"][nIndexCab]["startWarehouseGroup"]    := aDados[nIndexCab][ARRAY_MALHA_CAB_POS_ARMORG]
				oJsonData["items"][nIndexCab]["branchIdDest"]           := aDados[nIndexCab][ARRAY_MALHA_CAB_POS_FILDES]
				oJsonData["items"][nIndexCab]["endWarehouseGroup"]      := aDados[nIndexCab][ARRAY_MALHA_CAB_POS_ARMDES]

				nTotalLst := Len(aDados[nIndexCab][ARRAY_MALHA_CAB_POS_LISTA])
				oJsonData["items"][nIndexCab]["listOfMRPLaneException"] := Array(nTotalLst)

				For nIndexLst := 1 To nTotalLst
					oJsonData["items"][nIndexCab]["listOfMRPLaneException"][nIndexLst] := JsonObject():New()
					oJsonData["items"][nIndexCab]["listOfMRPLaneException"][nIndexLst]["product"]           := aDados[nIndexCab][ARRAY_MALHA_CAB_POS_LISTA][nIndexLst][ARRAY_MALHA_EXCE_POS_PROD]
					oJsonData["items"][nIndexCab]["listOfMRPLaneException"][nIndexLst]["startDate"]         := convDate(aDados[nIndexCab][ARRAY_MALHA_CAB_POS_LISTA][nIndexLst][ARRAY_MALHA_EXCE_POS_DTINI])
					oJsonData["items"][nIndexCab]["listOfMRPLaneException"][nIndexLst]["endDate"]           := convDate(aDados[nIndexCab][ARRAY_MALHA_CAB_POS_LISTA][nIndexLst][ARRAY_MALHA_EXCE_POS_DTFIN])
					oJsonData["items"][nIndexCab]["listOfMRPLaneException"][nIndexLst]["startQuantity"]     := aDados[nIndexCab][ARRAY_MALHA_CAB_POS_LISTA][nIndexLst][ARRAY_MALHA_EXCE_POS_QTDINI]
					oJsonData["items"][nIndexCab]["listOfMRPLaneException"][nIndexLst]["endQuantity"]       := aDados[nIndexCab][ARRAY_MALHA_CAB_POS_LISTA][nIndexLst][ARRAY_MALHA_EXCE_POS_QTDFIN]
				Next nIndexLst
			EndIf
		EndIf
	Next nIndexCab

	If cOperation $ "|INSERT|SYNC|"
		If cOperation == "INSERT"
			aReturn := MrpTLPost(oJsonData)
		Else
			aReturn := MrpTLSync(oJsonData)
		EndIf
		PrcPendMRP(aReturn, cApi, oJsonData, oTTPend, .F., @aSuccess, @aError, @lAllError, '1', cUUID)
	Else
		aReturn := MrpTLDel(oJsonData)
		PrcPendMRP(aReturn, cApi, oJsonData, oTTPend, .F., @aSuccess, @aError, @lAllError, '2', cUUID)
	EndIf

	FreeObj(oJsonData)
	oJsonData := Nil

Return Nil


/*/{Protheus.doc} convDate
Converte uma data do tipo DATE para o formato string AAAA-MM-DD
@type  Static Function
@author douglas.heydt
@since 23/07/2019
@version P12
@param dData, Date, Data que ser� convertida
@return cData, Caracter, Data convertida para o formato utilizado na integra��o.
/*/
Static Function convDate(dData)

	Local cData := StrZero(Year(dData),4) + "-" + StrZero(Month(dData),2) + "-" + StrZero(Day(dData),2)

Return cData