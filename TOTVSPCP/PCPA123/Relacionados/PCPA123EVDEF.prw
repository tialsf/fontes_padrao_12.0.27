#INCLUDE "PROTHEUS.CH"
#INCLUDE "PCPA123.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "FWMVCDEF.CH"                 
#INCLUDE "FWADAPTEREAI.CH"
#include "TbIconn.ch"

/*/{Protheus.doc} PCPA123EVDEF
@author Douglas Heydt
@since 01/05/2018
@version P12.1.17 
/*/
CLASS PCPA123EVDEF FROM FWModelEvent

	DATA cFilialBkp

	METHOD New() CONSTRUCTOR
	METHOD FieldPreVld()
	METHOD GridLinePosVld()	
	METHOD GridLinePreVld()
	METHOD ModelPosVld()
	METHOD BeforeTTS()
	METHOD AfterTTS()
	
ENDCLASS

METHOD New() CLASS  PCPA123EVDEF
	::cFilialBkp := Nil
Return

/*/{Protheus.doc} GridLinePreVld
M�todo que � chamado pelo MVC quando ocorrer as a��es de pre valida��o da linha do Grid
@author Douglas Heydt
@since 01/05/2018
@version 1.0
@param 01 oSubModel    , Objeto  , Modelo principal
@param 02 cModelId     , Caracter, Id do submodelo
@param 03 nLine        , Num�rico, Linha do grid
@param 04 cAction      , Caracter, A��o executada no grid, podendo ser: ADDLINE, UNDELETE, DELETE, SETVALUE, CANSETVALUE, ISENABLE
@param 05 cId          , Caracter, nome do campo
@param 06 xValue       , Vari�vel, Novo valor do campo
@param 07 xCurrentValue, Vari�vel, Valor atual do campo
@return lRet
/*/
METHOD GridLinePreVld(oSubModel, cModelID, nLine, cAction, cId, xValue, xCurrentValue) CLASS  PCPA123EVDEF
	Local lRet := .T.
	If cModelID == "SVQDETAIL"
		If cAction == "SETVALUE"
			If cId == "VQ_PRODUTO"
				lRet := ExistCpo("SB1",xValue,1)
			EndIf
		EndIf
	EndIf
Return lRet

/*/{Protheus.doc} GridLinePosVld
M�todo que � chamado pelo MVC quando ocorrer as a��es de pos valida��o da linha do Grid
@author douglas.heydt
@since 01/05/2018
@version 1.0
@param 01 oSubModel    , Objeto  , Modelo principal
@param 02 cModelId     , Caracter, Id do submodelo
@param 03 nLine        , Num�rico, Linha do grid
@return lOK
/*/
METHOD GridLinePosVld(oSubModel, cModelID, nLine) CLASS  PCPA123EVDEF
	Local aDados := {}
	Local nLine	
	//Variaveis  para colher os ultimos dados inseridos na grid
	Local cProd
	Local cDtIni
	Local cDataFin
	Local nQtIni
	Local nQtFin
	Local nX
	//Variaveis de controle
	Local lRet 		:= .T.
	Local lRngDt 	:= .F.
	Local lRngQt	:= .F.
	
	aDados := oSubModel:GetData()
	
	If (!Empty(oSubModel:GetValue("VQ_DTINI")) .And. !Empty(oSubModel:GetValue("VQ_DTFIM"))) .And. lRet
		If !DiffDate(oSubModel:GetValue("VQ_DTINI"), oSubModel:GetValue("VQ_DTFIM"))
			Help(" ",1,"P123DATE")
			lRet := .F.
		EndIf
	EndIf
	
	If (!Empty(oSubModel:GetValue("VQ_QNTINI")) .And. !Empty(oSubModel:GetValue("VQ_QNTFIM"))) .And. lRet
		If oSubModel:GetValue("VQ_QNTINI") > oSubModel:GetValue("VQ_QNTFIM")
			Help(" ",1,"P123QUANT")
			lRet := .F.
		EndIf
	Endif
	oSubModel:GoLine(oSubModel:Length())
	nLine 		:= oSubModel:GetLine()
	cProd 		:= oSubModel:GetValue("VQ_PRODUTO")
	cDtIni 		:= oSubModel:GetValue("VQ_DTINI")
	cDataFin	:= oSubModel:GetValue("VQ_DTFIM")
	nQtIni 		:= oSubModel:GetValue("VQ_QNTINI")
	nQtFin 		:= oSubModel:GetValue("VQ_QNTFIM")
	
	If nLine > 1 .And. !oSubModel:IsDeleted(nLine)
		//Faz uma verifica��o entre o ultimo registro inserido e todos os outros ( com o mesmo produto )
		//para determinar se os intervalos de data e quantidade est�o contidos entre si,
		// o que n�o pode ocorrer
		oSubModel:GoLine(oSubModel:Length())
		For nX :=1 To (oSubModel:Length() -1)
			oSubModel:GoLine(nX)
			If oSubModel:GetValue("VQ_PRODUTO") == cProd .And. !oSubModel:isDeleted(nX)
				
				lRngDt := vldDtRng(cDtIni, cDataFin, oSubModel:GetValue("VQ_DTINI"), oSubModel:GetValue("VQ_DTFIM"))
				lRngQt := vldQtRang(nQtIni, nQtFin, oSubModel:GetValue("VQ_QNTINI"), oSubModel:GetValue("VQ_QNTFIM"))
				//caso tanto data quanto quantidade estiverem dividindo intervalos ou a data e quantidade
				//iniciais forem iguais entre os dois registros( chave unica) o commit � impedido 
				If (lRngDt .And. lRngQt) .Or. (cDtIni == oSubModel:GetValue("VQ_DTINI") .And. nQtIni == oSubModel:GetValue("VQ_QNTINI") )
					Help(" ",1,"P123RANGE")
					lRet := .F.
					EXIT
				EndIf
			EndIf
		Next nX
	EndIf
	
Return lRet

/*/{Protheus.doc} FieldPreVld
M�todo chamado pelo MVC quando ocorrer a a��o de pr� valida��o do Field
@author douglas.heydt
@since 01/05/2018
@version 1.0
@return lRet
/*/
METHOD FieldPreVld(oSubModel, cModelID, cAction, cId, xValue) CLASS  PCPA123EVDEF
	Local lRet := .T.
	Local cDestino := xValue
	Local cFilBkp  := cFilAnt

	If cModelID == "SVDMASTER"
		If cAction == "SETVALUE" .And. !Empty(xValue)
			/*Valida filiais escolhidas, o par�metro MMV_PCPMADI deve estar ativo para permitir o uso da filial*/
			If cId == "VD_FILORG" .Or. cId == "VD_FILDEST"
				If !SUPERGETMV( "MV_PCPMADI", .F., .F., xValue )
					Help( , , 'A123FILMADI', , STR0023+xValue+STR0024, 1, 0, NIL, NIL, NIL, NIL, NIL, {STR0025+xValue}) //STR0024" n�o pode ser utilizada na malha de distribui��o pois o par�metro MV_PCPMADI n�o est� ativo" //STR0025"Verifique o par�metro MV_PCPMADI da Filial "
					lRet := .F.
				EndIf
			EndIf
			////*Valida��es do campo ORIGEM*/
			If cId == "VD_ORIGEM" .And. lRet
				cFilAnt  := oSubModel:GetValue("VD_FILORG")
				lRet := ExistCpo("SVN",xValue,1)
				If	lRet
					lRet := l123VldOri(xValue)//valida se o grupo escolhido ja � origem
				Else
					Help(,,"Help",,STR0021,1,0,,,,,,) //"Grupo Origem n�o cadastrado."
				EndIf
				cFilAnt := cFilBkp
			EndIf
			///*Valida��es do campo DESTINO*/
			If cId == "VD_DESTINO" .And. lRet
				cFilAnt  := oSubModel:GetValue("VD_FILDEST")
				lRet := ExistCpo("SVN",xValue,1)
				
				If lRet
					lRet := l123VldDest(xValue)
				Else
					Help(,,"Help",,STR0022,1,0,,,,,,) //"Grupo destino n�o cadastrado."
				EndIf
				
				If lRet
					aOrigems := {}
					aDestinos := {}
					aAdd(aDestinos,oSubModel:GetValue("VD_FILDEST")+cDestino )
					aAdd(aOrigems, oSubModel:GetValue("VD_FILORG")+oSubModel:GetValue("VD_ORIGEM") )
					lRet := lRecur(oSubModel:GetValue("VD_FILORG"),oSubModel:GetValue("VD_ORIGEM"),oSubModel:GetValue("VD_FILDEST"),xValue)	
					If !lCmpArrGrp()
						Help(" ",1,"P123CONSIS")
						lRet:= .F.
					EndIf
				EndIf				
				cFilAnt := cFilBkp
			EndIf
			
		EndIf
	EndIf
	
Return lRet

/*/{Protheus.doc} ModelPosVld()
M�todo que � chamado pelo MVC quando ocorrer as a��es de valida��o do modelo.

@author Lucas Konrad Fran�a
@since 10/10/2018
@version 1.0
/*/
METHOD ModelPosVld(oModel, cModelId) CLASS PCPA123EVDEF
	Local lRet      := .T.
	Local oModelSVD := oModel:GetModel("SVDMASTER")
	Local oModelSVQ := oModel:GetModel("SVQDETAIL")
	Local nInd      := 0


	//Valida se o grupo origem/destino s�o iguais.
	If oModelSVD:GetValue("VD_FILORG")+oModelSVD:GetValue("VD_ORIGEM") == oModelSVD:GetValue("VD_FILDEST")+oModelSVD:GetValue("VD_DESTINO")
		lRet := .F.
		Help(" ",1,"P123VAZIO")
	EndIf

	//Valida se o grupo origem � v�lido. Executa novamente o FieldPreVld, pois a filial origem pode ter sido alterada ap�s preencher o grupo origem.
	If lRet .And. oModel:GetOperation() == MODEL_OPERATION_INSERT .And. ;
		!::FieldPreVld(oModelSVD,"SVDMASTER","SETVALUE","VD_ORIGEM",oModelSVD:GetValue("VD_ORIGEM"))
		lRet := .F.
	EndIf

	//Valida se o grupo destino � v�lido. Executa novamente o FieldPreVld, pois a filial destino pode ter sido alterada ap�s preencher o grupo destino.
	If lRet .And. !::FieldPreVld(oModelSVD,"SVDMASTER","SETVALUE","VD_DESTINO",oModelSVD:GetValue("VD_DESTINO"))
		lRet := .F.
	EndIf

	//Executa novamente as valida��es de linha da grid, pois o grupo origem/filial origem podem ter sido alterados ap�s preencher os dados da grid.
	If lRet
		For nInd := 1 To oModelSVQ:Length()
			oModelSVQ:GoLine(nInd)
			If !::GridLinePosVld(oModelSVQ, "SVQDETAIL", nInd)
				lRet := .F.
				Exit
			EndIf
		Next nInd	
	EndIf

	If lRet
		If !SUPERGETMV( "MV_PCPMADI", .F., .F., oModelSVD:GetValue("VD_FILORG") )
			Help( , , 'A123FILMADI', , STR0023+oModelSVD:GetValue("VD_FILORG")+STR0024, 1, 0, NIL, NIL, NIL, NIL, NIL, {STR0025+oModelSVD:GetValue("VD_FILORG")}) //STR0024" n�o pode ser utilizada na malha de distribui��o pois o par�metro MV_PCPMADI n�o est� ativo" //STR0025"Verifique o par�metro MV_PCPMADI da Filial "
			lRet := .F.
		EndIf
	EndIf

	If lRet
		If !SUPERGETMV( "MV_PCPMADI", .F., .F., oModelSVD:GetValue("VD_FILDEST") )
			Help( , , 'A123FILMADI', , STR0023+oModelSVD:GetValue("VD_FILDEST")+STR0024, 1, 0, NIL, NIL, NIL, NIL, NIL, {STR0025+oModelSVD:GetValue("VD_FILDEST")}) //STR0024" n�o pode ser utilizada na malha de distribui��o pois o par�metro MV_PCPMADI n�o est� ativo" //STR0025"Verifique o par�metro MV_PCPMADI da Filial "
			lRet := .F.
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} BeforeTTS()
M�todo que � chamado pelo MVC quando ocorrer as a��es do commit antes da transa��o

@author Lucas Konrad Fran�a
@since 09/10/2018
@version 1.0
/*/
METHOD BeforeTTS(oModel, cModelId) CLASS PCPA123EVDEF
	
	//Se for inclus�o, atualiza o cFilAnt de acordo com a filial origem informada em tela.
	If oModel:GetOperation() == MODEL_OPERATION_INSERT
		::cFilialBkp := cFilAnt
		cFilAnt := oModel:GetModel("SVDMASTER"):GetValue("VD_FILORG")
		//oModel:SetValue("SVDMASTER","VD_FILIAL",oModel:GetModel("SVDMASTER"):GetValue("VD_FILORG"))
	EndIf
Return

/*/{Protheus.doc} AfterTTS()
M�todo que � chamado pelo MVC quando ocorrer as a��es do commit ap�s transa��o

@author Lucas Konrad Fran�a
@since 09/10/2018
@version 1.0
/*/
METHOD AfterTTS(oModel, cModelId) CLASS PCPA123EVDEF
	//Restaura a filial corrente (cFilAnt) que foi alterada no BeforeTTS
	If oModel:GetOperation() == MODEL_OPERATION_INSERT .And. !Empty(::cFilialBkp)
		cFilAnt := ::cFilialBkp
		::cFilialBkp := Nil
	EndIf
Return


/*/{Protheus.doc} lCmpArrGrp()
Fun��o para validar as informa��es da filial

@author douglas.heydt
@since 01/05/2018
@version 1.0
/*/
Function lCmpArrGrp()

	Local lRet		:= .F.
	Local nX := 0
	
	For nX := 1 To Len(aOrigems)
		If aScan(aDestinos, aOrigems[nX]) == 0
			//caso achar algum diferente, retorna .T. informando que os arrays sao diferentes
			lRet := .T.
			Exit
		EndIf
	Next nX
Return lRet

/*/{Protheus.doc} lCmpArrGrp()
Fun��o para validar as informa��es da filial

@author douglas.heydt
@since 01/05/2018
@version 1.0
/*/
Function lRecur(cFilOri, cOrigem, cFilDest, cDestino)
	Local aArea 	:= GetArea()
	Local lRet		:= .T.
	Local cFilSVD	:= xFilial("SVD",cFilOri)
	
	dbSelectArea("SVD")
	dbSetOrder(2)
	/*procura se existe um registro com a origem atual como destino*/
	If dbSeek( cFilSVD+cOrigem)		
		lRecur(SVD->VD_FILIAL, SVD->VD_ORIGEM)
		aAdd(aDestinos,SVD->(VD_FILDEST+VD_DESTINO))
		aAdd(aOrigems, SVD->(VD_FILIAL+VD_ORIGEM))
	EndIf       
	RestArea(aArea)

Return lRet

/*/{Protheus.doc} vldDtRng()
Valida se as datas est�o no mesmo intervalo entre si.
@param dtIni : data inicial do primeiro intervalo
@param dtFin : data final do primeiro intervalo
@param dtProvIni : data inicial do segundo intervalo
@param dtProvFin : data final do segundo intervalo
@author Douglas Heydt
@since 14/06/2018
@version 1.0
@return lRet - .T. quando as datas dividirem alguma parte do mesmo intervalo
/*/
Function vldDtRng(dtIni, dtFin, dtProvIni, dtProvFin)
	Local lRange1 := .F.
	Local lRange2 := .F.
	Local lRange3 := .F.
	Local lRange4 := .F.
	Local lRet := .F.

	If dtProvIni >= dtIni .And. dtProvIni < dtFin
		lRange1 := .T.
	EndIf
	If dtProvFin >= dtIni .And. dtProvFin < dtFin
		lRange2 := .T.
	EndIf
	
	If dtIni > dtProvIni  .And. dtFin < dtProvIni
		lRange3 := .T.
	EndIf
	If dtFin > dtProvIni  .And. dtFin < dtProvFin
		lRange4 := .T.
	EndIf
	
	If lRange1 .Or. lRange2 .Or. lRange3 .Or. lRange4
		lRet := .T.
	EndIf

Return lRet

/*/{Protheus.doc} vldDtRng()
Valida se as quantidades est�o no mesmo intervalo entre si.
@param nQtIni : quantidade inicial do primeiro intervalo
@param nQtFin : quantidade final do primeiro intervalo
@param nQtProvIni : quantidade inicial do segundo intervalo
@param nQtProvFin : quantidade final do segundo intervalo
@author Douglas Heydt
@since 14/06/2018
@version 1.0
@return lRet - .T. quando as quantidades dividirem alguma parte do mesmo intervalo
/*/
Function vldQtRang(nQtIni, nQtFin, nQtProvIni, nQtProvFin)
	Local lRange1 := .F.
	Local lRange2 := .F.
	Local lRange3 := .F.
	Local lRange4 := .F.
	Local lRet := .F.
	
	If nQtProvIni >= nQtIni .And. nQtProvIni < nQtFin
		lRange1 := .T.
	EndIf
	
	If nQtIni >= nQtProvIni .And. nQtIni < nQtProvFin
		lRange2 := .T.
	EndIf
	
	If nQtProvFin > nQtIni .And. nQtProvFin < nQtFin
		lRange3 := .T.
	EndIf
	
	If nQtFin > nQtProvIni .And. nQtFin < nQtProvFin
		lRange4 := .T.
	EndIf
	
	If lRange1 .Or. lRange2 .Or. lRange3 .Or. lRange4
		lRet := .T.
	EndIf

Return lRet

/*/{Protheus.doc} l123VldOri()
Verifica se j� existe registro para o armaz�m origem registrado
@author Douglas Heydt
@since 15/06/2018
@version 1.0
@return lRet
/*/
Function l123VldOri(xValue)
	Local aArea 	:= GetArea()
	Local lRet		:= .T.
	Local cFilSVD	:= xFilial("SVD")

	If !Empty(xValue)
		dbSelectArea("SVD")
		dbSetOrder(1)
		If dbSeek( cFilSVD+xValue)
				Help(" ",1,"P123ORIGEM")
				lRet := .F.
		EndIf       
	EndIf
	RestArea(aArea)
Return lRet

/*/{Protheus.doc} l123VldDest()
Valida se o campo destino pode ser modificado ( se existe origem )
e se o valor informado � diferente do origem
@author Douglas Heydt
@since 15/06/2018
@version 1.0
@return lRet
/*/
Function l123VldDest(xValue)
	Local oModel    := FWModelActive()
	Local oModelSVD := oModel:GetModel("SVDMASTER")
	Local aArea 	:= GetArea()
	Local lRet		:= .T.
	
	If !Empty(oModelSVD:GetValue("VD_ORIGEM"))
		If xValue == oModelSVD:GetValue("VD_ORIGEM") .And. oModelSVD:GetValue("VD_FILORG") == oModelSVD:GetValue("VD_FILDEST")
			Help(" ",1,"P123VAZIO")
			lRet := .F.				
		EndIf
	Else
		Help(" ",1,"P123VAZIO")
		lRet := .F.
	EndIf
	
	If !Empty(xValue)
		SVD->(dbSetOrder(2))
		SVD->(dbSeek(xFilial("SVD",oModelSVD:GetValue("VD_FILDEST"))+xValue))
		While !SVD->(Eof()) .And. SVD->VD_FILDEST == oModelSVD:GetValue("VD_FILDEST") .And. SVD->VD_DESTINO == xValue
			If SVD->(RECNO()) != oModel:aModelStruct[1][3]:nDataId
				Help(" ",1,"P123DESTINO")
				lRet := .F.
				Exit
			EndIf

			SVD->(dbSkip())
		EndDo
	EndIf
	RestArea(aArea)
Return lRet