#Include 'Protheus.ch'
#Include 'FWMVCDef.ch'
#Include 'PCPA125.ch'

Static lExisHWX     := AliasInDic("HWS")

/*/{Protheus.doc} PCPA125
//Rotina de Programa x Usuarios
@author Thiago Zoppi
@since 12/05/2018
@version 1.0
/*/
Function PCPA125()
Local obrowse

aRotina := MenuDef() 
obrowse:= BrowseDef()
obrowse:Activate()

Return 

//PARA ADAPTAR AO MVC LOCALIZADO 
Static Function BrowseDef()
Local oBrowse

	If !AliasInDic("SOX") .Or. !AliasInDic("SOY") .Or. !AliasInDic("SOZ")
		Help( ,, 'PCPA125',, STR0007, 1, 0 ) //"Tabela SOX ou SOY ou SOZ n�o cadastrada no sistema!"
	Else 
		oBrowse := FWMBrowse():New()	
		obrowse:SetAlias('SOX')
		obrowse:SetDescription(STR0001) //Formulario do Apontamento de Producao
		obrowse:SetMenuDef('PCPA125')
	EndIf
Return oBrowse

Static Function MenuDef()
Local aRotina := {}
	ADD OPTION aRotina TITLE 'Incluir' 		ACTION 'VIEWDEF.PCPA125' OPERATION 3 ACCESS 0   // incluir
	ADD OPTION aRotina TITLE 'Alterar' 		ACTION 'VIEWDEF.PCPA125' OPERATION 4 ACCESS 0   // Alterar
	ADD OPTION aRotina TITLE 'Excluir' 		ACTION 'VIEWDEF.PCPA125' OPERATION 5 ACCESS 0   // Excluir
	ADD OPTION aRotina TITLE 'Visualizar' 	ACTION 'VIEWDEF.PCPA125' OPERATION 2 ACCESS 0   // Visualizar

Return aRotina  
//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Defini��o do modelo de Dados
@author Thiago Zoppi
@since 11/05/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
Local oModel
Local oStr1		 := FWFormStruct(1,'SOX')
Local oStr2		 := FWFormStruct(1,'SOY')
Local oStr3		 := FWFormStruct(1,'SOZ')
Local oStr4
Local oEvent     := PCPA125EVDEF():New()	
Local bLoadHWS

oModel := MPFormModel():New('MODELNAME', { ||IIF(lExisHWX, PCPA125PRE(oModel), .t.) }, { || IIF(lExisHWX, PCPA125GRV(oModel), .t.) })
oStr1:SetProperty('OX_PRGAPON', MODEL_FIELD_NOUPD, .T.)  

If lExisHWX 
	oStr4 := FWFormModelStruct():New()
	oStr4:AddTable( "HWS", {"HWS_CDMQ"}, STR0008 ) //"M�quina"
	oStr4:AddIndex( 1,"01","HWS_CDMQ",STR0008,"","",.T. ) //"M�quina"
	oStr4:AddField("","","MARCA","L",1,0,Nil,Nil,Nil,Nil,{|| .F.},Nil,Nil,.T.)
	oStr4:AddField(STR0008,"","HWS_CDMQ","C",10,0,Nil,Nil,Nil,Nil,{|| .F.},Nil,Nil,.T.) //"M�quina"
	oStr4:AddField(STR0009,"","HWS_DSMQ","C",40,0,Nil,Nil,Nil,Nil,{|| .F.},Nil,Nil,.T.) //"Descri��o"
	bLoadHWS   := {||LoadHWS( oStr4, .f. )}
EndIf

oModel:addFields('OXMASTER',,oStr1)
oModel:addGrid('DETAIL_SOY','OXMASTER',oStr2)
oModel:addGrid('DETAIL_SOZ','OXMASTER',oStr3)
If lExisHWX
	oModel:addGrid('DETAIL_HWS','OXMASTER',oStr4, , , , ,bLoadHWS )
EndIf
oModel:SetPrimaryKey({'OX_FILIAL','OX_FORM' })
oModel:SetRelation('DETAIL_SOZ', { { 'OZ_FILIAL' , 'xFilial("SOX")'	}, { 'OZ_CODFORM', 'OX_FORM' } }, SOZ->(IndexKey(1)) )
oModel:SetRelation('DETAIL_SOY', { { 'OY_FILIAL' , 'xFilial("SOX")'	}, { 'OY_CODFORM', 'OX_FORM' } }, 'OY_FILIAL+OY_CODFORM+OY_CAMPO' )
If lExisHWX
	oModel:SetRelation('DETAIL_HWS', { { 'HWS_FILIAL', 'xFilial("HWS")'	}, { 'HWS_FORM'  , 'OX_FORM' } }, HWS->(IndexKey(1)) )
EndIf

oStr1:SetProperty( 'OX_FORM'   	    , MODEL_FIELD_OBRIGAT,.T.)
oStr1:SetProperty( 'OX_IMAGEM'   	, MODEL_FIELD_OBRIGAT,.T.)
oStr1:SetProperty( 'OX_DESCR'   	, MODEL_FIELD_OBRIGAT,.T.)
oStr1:SetProperty( 'OX_PRGAPON'   	, MODEL_FIELD_VALID, { || VldPrgApon(oModel)} )

If SOX->(FieldPos("OX_CRONOM")) >  0
	oStr1:SetProperty( 'OX_CRONOM'   	, MODEL_FIELD_VALID, { || VldCronom(oModel)} )
EndIf

oStr2:SetProperty( 'OY_CODFORM'   	, MODEL_FIELD_OBRIGAT,.F.)
oStr3:SetProperty( 'OZ_CODFORM'   	, MODEL_FIELD_OBRIGAT,.F.)
oStr3:SetProperty( 'OZ_DESCGRP'   	, MODEL_FIELD_OBRIGAT,.F.)

If lExisHWX
	oStr4:SetProperty( 'HWS_CDMQ'		, MODEL_FIELD_WHEN, { || .F. } )
	oStr4:SetProperty( 'HWS_DSMQ'		, MODEL_FIELD_WHEN, { || .F. } )
EndIf

oModel:SetDescription(STR0001) //Formulario do Apontamento de Producao 
oModel:getModel('OXMASTER'):SetDescription(STR0003)   //Cabecalho 
oModel:getModel('DETAIL_SOY'):SetDescription(STR0004) //Detalhes de Campos
oModel:getModel('DETAIL_SOZ'):SetDescription(STR0005) //Detalhes Usuarios
If lExisHWX
	oModel:getModel('DETAIL_HWS'):SetDescription(STR0010) //"Detalhes Maquina"
	oModel:getModel('DETAIL_HWS'):SetOnlyQuery(.T.)
	oModel:getModel('DETAIL_HWS'):SetOptional(.T.)

	oModel:getModel('DETAIL_HWS'):SetNoInsertLine(.T.)
	oModel:getModel('DETAIL_HWS'):SetNoDeleteLine(.T.)
EndIf

//ATIVAR EVENTOS
oModel:InstallEvent("PCPA125EVDEF", /*cOwner*/, oEvent)

If lExisHWX
	oModel:SetActivate( {|oModel| LoadHWS(oStr4, .t.) } )
EndIf

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Defini��o do interface
@author Thiago Zoppi
@since 11/05/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView
Local oModel	:= FWLoadModel( 'PCPA125' )
Local oStr1		:= FWFormStruct(2, 'SOX')
Local oStr2		:= FWFormStruct(2, 'SOY')
Local oStr3		:= FWFormStruct(2, 'SOZ')
Local oStr4
oStr4 := FWFormViewStruct():New()

oStr4:AddField("MARCA"   ,"01","","",{} ,"L","@BMP",Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,.T.)
If lExisHWX
	oStr4:AddField("HWS_CDMQ","02",STR0008,"",{} ,"C","@!",Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,.T.) //"M�quina"
	oStr4:AddField("HWS_DSMQ","03",STR0009,STR0011,{} ,"C","@!",Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,.T.) //"Descri��o" ###"Descri��o da Maquina"
EndIf

oView := FWFormView():New()
oView:SetModel(oModel)

oView:AddField('VIEW_SOX', oStr1,'OXMASTER' )
oView:AddGrid('VIEW_SOY' , oStr2,'DETAIL_SOY')
oView:AddGrid('VIEW_SOZ' , oStr3,'DETAIL_SOZ')
If lExisHWX
	oView:AddGrid('VIEW_HWS' , oStr4,'DETAIL_HWS')
EndIf

oView:CreateHorizontalBox( 'BOX2', 20)
oView:CreateHorizontalBox( 'BOX1', 80)
oView:CreateVerticalBox( 'BOXFORM3', 100, 'BOX2')
oView:CreateFolder( 'FOLDER5', 'BOX1')
oView:AddSheet('FOLDER5','SHEET6', STR0017) //"Campos"
oView:AddSheet('FOLDER5','SHEET8', STR0018) //"Usuarios"
iF lExisHWX
	oView:AddSheet('FOLDER5','SHEET9', STR0016) //"Maquinas"
EndIf

oView:CreateHorizontalBox( 'BOXFORM8', 100, /*owner*/, /*lUsePixel*/, 'FOLDER5', 'SHEET8')
oView:SetOwnerView('VIEW_SOZ','BOXFORM8')
oView:CreateHorizontalBox( 'BOXFORM6', 100, /*owner*/, /*lUsePixel*/, 'FOLDER5', 'SHEET6')
oView:SetOwnerView('VIEW_SOY','BOXFORM6')
oView:SetOwnerView('VIEW_SOX','BOXFORM3')
If lExisHWX
	oView:CreateHorizontalBox( 'BOXFORM9', 100, /*owner*/, /*lUsePixel*/, 'FOLDER5', 'SHEET9')
	oView:SetOwnerView('VIEW_HWS','BOXFORM9')
Endif

//oView:SetVldFolder({|| PCP125VLDF()})
oView:SetVldFolder({|cID,nOldSheet, nSelSheet| PCP125VLDF(cID,nOldSheet,nSelSheet)})

//REMOVER CAMPOS
oStr2:RemoveField( 'OY_CODFORM' )
oStr3:RemoveField( 'OZ_CODFORM' )

//ALTERAR PROPRIEDADES DOS CAMPOS 
oStr2:SetProperty('OY_CAMPO'	, MVC_VIEW_CANCHANGE,.F.)
oStr2:SetProperty('OY_DESCAMP'	, MVC_VIEW_CANCHANGE,.F.)
oStr1:SetProperty('OX_IMAGEM'	, MVC_VIEW_TITULO, STR0002 ) // Titulo do campo Imagem "Icone" 

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} A125FolMaq
Mensagem ao focar a aba 'Maquina'
@author Marcos Wagner Jr.
@since 11/05/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function PCP125VLDF(cID,nOldSheet,nSelSheet)
Local oModel := FwModelActive()
Local oView  := FwViewActive()

If nSelSheet == 3 .And. oModel:GetModel("OXMASTER"):GetValue("OX_PRGAPON") <> "4"
	//MsgStop(STR0013,STR0012) ///"Op��o disponivel apenas para apontamento igual a 'Produ��o Ch�o de Fabrica' !" ###"Aten��o"
	//oView:SelectFolder("FOLDER5", "Campos",2)
	Return .f.
EndIf

Return .T.

// ---------------------------------------------------------
/*/{Protheus.doc} LoadHWS
Carrega grid para edica das informacoes
@author Marcos Wagner Jr.
@since 12/05/2020
@version 1.0
/*/
// ---------------------------------------------------------
Static Function LoadHWS(oStrHWS, lActivate)
Local aLoad      := {}
Local oModel     := FwModelActive()
Local oModelGrd  := oModel:GetModel('DETAIL_HWS')
Local cQuery     := ""
Local cAliasTmp  := GetNextAlias()
Local nI         := 0
Local nField     := 0
Local nOperation := oModel:GetOperation()
Local cProgApont := oModel:GetModel("OXMASTER"):GetValue("OX_PRGAPON")

	If lActivate .Or. cProgApont == '4'
		cQuery := " SELECT CYB.CYB_CDMQ, CYB.CYB_DSMQ "
		cQuery +=   " FROM "+RetSQLName("CYB")+" CYB "
		cQuery +=  " WHERE CYB.CYB_FILIAL = '"+xFilial("CYB")+"'"
		cQuery +=    " AND CYB.D_E_L_E_T_ = ' '"
		dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery), cAliasTmp, .F., .T.)
		While (cAliasTmp)->(!EoF())

			nField++
			AAdd(aLoad,{ nField ,{} } )

			If nOperation == 3
				AAdd(aLoad[nField, 2], .F.  )
			Else
				dbSelectArea('HWS')
				dbSetOrder(1)
				If dbSeek(xFilial('HWS')+SOX->OX_FORM+(cAliasTmp)->CYB_CDMQ)
					AAdd(aLoad[nField, 2], .T.  )
				else
					AAdd(aLoad[nField, 2], .F.  )
				EndIf
			EndIf

			AAdd(aLoad[nField, 2], (cAliasTmp)->CYB_CDMQ )
			AAdd(aLoad[nField, 2], (cAliasTmp)->CYB_DSMQ )

			dbSelectArea(cAliasTmp)
			(cAliasTmp)->(DbSkip())
		EndDO
		(cAliasTmp)->(DbCloseArea())
	EndIf

	If lActivate .AND. nOperation == 3

		For nI := 1 to Len(aLoad)
			oModelGrd:SetNoInsertLine(.F.)
			oModelGrd:SetNoDeleteLine(.F.)

			oModelGrd:InitLine()
			oModelGrd:GoLine(1)

			If nI > 1
				oModelGrd:AddLine()
			Endif

			oModelGrd:GoLine(oModelGrd:Length())
			oModelGrd:LoadValue("MARCA"    ,aLoad[nI][2][1])
			oModelGrd:LoadValue("HWS_CDMQ" ,SubStr(aLoad[nI][2][2],1,10))
			oModelGrd:LoadValue("HWS_DSMQ" ,aLoad[nI][2][3])

			oModelGrd:SetNoInsertLine(.T.)
			oModelGrd:SetNoDeleteLine(.T.)
			oModelGrd:GoLine(1)
		Next
	EndIf

Return aLoad

// ---------------------------------------------------------
/*/{Protheus.doc} PCPA125VLD
Gravacao do Modelo
@author Marcos Wagner Jr.
@since 19/05/2020
@version 1.0
/*/
// ---------------------------------------------------------
Static Function PCPA125VLD(oModel)
Local lRet := .t.
Local oHWS  := oModel:GetModel('DETAIL_HWS')
Local nI         := 0
Local nOperation := oModel:GetOperation()
Local cProgApont := oModel:GetModel("OXMASTER"):GetValue("OX_PRGAPON")

	If (nOperation == 3 .Or. nOperation == 4) .And. cProgApont == '4'
		lRet := .f.
		For nI := 1 To oHWS:Length()
			oHWS:GoLine(nI)
			If oHWS:GetValue("MARCA")
				lRet := .t.
				Exit
			EndIf
		Next

		If !lRet
			oModel:SetErrorMessage('DETAIL_HWS', 'MARCA' , 'DETAIL_HWS' , 'MARCA' , STR0012, STR0014, STR0015) //"Aten��o" ### "Nenhuma M�quina foi selecionada." ### "Dever� ser selecionada pelo menos uma m�quina."
		EndIf
	EndIf

Return lRet

// ---------------------------------------------------------
/*/{Protheus.doc} PCPA125PRE
Gravacao do Modelo
@author Marcos Wagner Jr.
@since 12/05/2020
@version 1.0
/*/
// ---------------------------------------------------------
Static Function PCPA125PRE(oModel)
Local oView  := FwViewActive()
Local cProgApont := oModel:GetModel("OXMASTER"):GetValue("OX_PRGAPON")
Local nOperation := oModel:GetOperation()

If (nOperation <> 3 .AND. cProgApont <> '4')
	oView:HideFolder( 'FOLDER5', STR0016, 2) //"Maquinas"
	oView:SelectFolder('FOLDER5', STR0017,2) //"Campos"
EndIf

Return .t.

// ---------------------------------------------------------
/*/{Protheus.doc} PCPA125GRV
Gravacao do Modelo
@author Marcos Wagner Jr.
@since 12/05/2020
@version 1.0
/*/
// ---------------------------------------------------------
Static Function PCPA125GRV(oModel)
Local aOldArea   := GetArea()
Local aAreaHWS   := HWS->(GetArea())
Local oHWS       := oModel:GetModel('DETAIL_HWS')
Local cFormular  := oModel:GetModel("OXMASTER"):GetValue("OX_FORM")
Local cCdMq
Local nI         := 0
Local nOperation := oModel:GetOperation()

If !PCPA125VLD(oModel)
	Return .F.
EndIf

For nI := 1 To oHWS:Length()
	oHWS:GoLine(nI)
	cCdMq := oHWS:GetValue("HWS_CDMQ")

	dbSelectArea("HWS")
	dbSetOrder(1)
	If dbSeek(xFilial("HWS")+cFormular+cCdMq)
		If nOperation == 5 .OR. !oHWS:GetValue("MARCA")
			RecLock("HWS",.f.)
			dbDelete()
			HWS->(MsUnlock())
		EndIf
	ElseIf oHWS:GetValue("MARCA")
		RecLock("HWS",.t.)
		HWS->HWS_FILIAL := xFilial('HWS')
		HWS->HWS_FORM   := cFormular
		HWS->HWS_CDMQ   := cCdMq
		HWS->(MsUnlock())
	EndIf

Next nI

RestArea(aAreaHWS)
RestArea(aOldArea)

Return .t.

Static Function VldPrgApon(oModel)
Local oView       := FwViewActive()
Local cProgApont  := oModel:GetModel("OXMASTER"):GetValue("OX_PRGAPON")
Local lCronometr  := .F.
Local lExisOxPar  := .F.

dbSelectArea('SOX')
lExisOxPar := If (SOX->(FieldPos("OX_PARADA")) >  0, .T., .F.)
lExisOxCro := If (SOX->(FieldPos("OX_CRONOM")) >  0, .T., .F.)

If cProgApont == '4'
	If lExisOxCro
		lCronometr := IIF((oModel:GetModel("OXMASTER"):GetValue("OX_CRONOM")) == "1", .T., .F.)
	EndIf

	oView:SelectFolder('FOLDER5', STR0016,2) //"Maquinas"
	If !lCronometr .And. lExisOxCro 
		oModel:GetModel("OXMASTER"):ClearField("OX_TPPROG")
	EndIf
Else
	If lExisOxPar  
		oModel:GetModel("OXMASTER"):ClearField("OX_PARADA")
	EndIf
	If lExisOxCro
		oModel:GetModel("OXMASTER"):ClearField("OX_CRONOM")
		oModel:GetModel("OXMASTER"):ClearField("OX_TPPROG")
	EndIf
	oView:HideFolder( 'FOLDER5', STR0016, 2) //"Maquinas"
EndIf

oView:SelectFolder('FOLDER5', STR0017,2) //"Campos"

Return .t.

Static Function VldCronom(oModel)
Local oView       := FwViewActive()
Local cProgApont  := oModel:GetModel("OXMASTER"):GetValue("OX_PRGAPON")
Local lCronometr  := .F.

dbSelectArea('SOX')
lExisOxCro := If (SOX->(FieldPos("OX_CRONOM")) >  0, .T., .F.)

If cProgApont == '4'
	If lExisOxCro
		lCronometr := IIF((oModel:GetModel("OXMASTER"):GetValue("OX_CRONOM")) == "1", .T., .F.)	
	EndIf

	oView:SelectFolder('FOLDER5', STR0016,2) //"Maquinas"
	If !lCronometr .And. lExisOxCro
		oModel:GetModel("OXMASTER"):ClearField("OX_TPPROG")
	EndIf
EndIf

oView:SelectFolder('FOLDER5', STR0017,2) //"Campos"

Return .t.

Function PCPA125EPa()
Local lRet       := .T.
Local oModel     := FWModelActive()
Local cProgApont := oModel:GetModel("OXMASTER"):GetValue("OX_PRGAPON")

If cProgApont == '4'
	lRet := .T.
Else	
	lRet := .F.
EndIf

Return lRet

Function PCPA125ETp()
Local lRet       := .T.
Local oModel     := FWModelActive()
Local lCronometr := oModel:GetModel("OXMASTER"):GetValue("OX_CRONOM")

If lCronometr == '1'
	lRet := .T.
Else	
	lRet := .F.
	oModel:GetModel("OXMASTER"):ClearField("OX_TPPROG")
EndIf

Return lRet