#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "FWMVCDEF.CH"                 
#INCLUDE "FWADAPTEREAI.CH"
#include "TbIconn.ch"
#include "TopConn.ch"

/*/{Protheus.doc} PCPA125EVDEF
//EVENTOS PCPA125
@author Thiago Zoppi
@since 12/05/2018
/*/
CLASS PCPA125EVDEF FROM FWModelEvent
	DATA cCodigo

	METHOD New() CONSTRUCTOR
	METHOD FieldPreVld()
	METHOD Activate()

ENDCLASS

METHOD New() CLASS  PCPA125EVDEF
	::cCodigo := ""
Return

/*/{Protheus.doc} Activate
//M�todo que � chamado pelo MVC quando ocorrer a ativa��o do Model.
@author tp.thiago.zoppi
@since 15/05/2018
/*/
METHOD Activate(oModel, lCopy) CLASS PCPA125EVDEF
	Local lRet := .T.
	Local nOperation := oModel:getoperation()
	Local oModelSOY := oModel:GetModel("DETAIL_SOY")

	If Noperation = 4  // N�O DEIXA APAGAR E INSERIR NOVAS LINHA
		oModelSOY:SetNoDeleteLine(.T.)
		oModelSOY:SetNoInsertLine(.T.)
	Else
		oModelSOY:SetNoDeleteLine(.F.)
		oModelSOY:SetNoInsertLine(.F.)
	EndIf

RETURN lRet

/*/{Protheus.doc} FieldPreVld
//M�todo que � chamado pelo MVC quando ocorrer a a��o de pr� valida��o do Field
@author tp.thiago.zoppi
@since 15/05/2018
@version 1.0
/*/
METHOD FieldPreVld(oSubModel, cModelID, cAction, cId, xValue) CLASS PCPA125EVDEF
	Local lRet		 := .T.
	Local oModel	 := FWModelActive()
	Local oModelSOY	 := oModel:GetModel("DETAIL_SOY")
	Local oView    	 := FWViewActive()
	Local aCampSOY	 := {}
	Local aFields    := {}
	Local nX		 := 0 
	Local nLinha	 := 1
	Local nTamHeader := 0
	Local aCamposADD := 0
	
	If (cAction == 'UNDELETE' .Or. cAction == 'SETVALUE' ) .AND. cId = 'OX_PRGAPON' //.AND. XVALUE != oSubModel:GetValue("OX_PRGAPON")

		If oModelSOY:Length() > 1 
            
            //Apaga todas a linhas da grid
                          
			For nX := 1 to oModelSOY:Length() 
				oModelSOY:GoLine(nX) 
				oModelSOY:DeleteLine(.T.,.T.)
			Next nX
			
			//LIMPAR LINHAS DELETAS, NAO DEIXANDO LINHAS CINZAS NA GRID		
			nTamHeader	:= LEN(oModelSOY:aHeader)
			oModelSOY:SetNoInsertLine(.F.)
			oModelSOY:SetNoDeletetLine(.F.)
			oModelSOY:GoLine(1)
			ASIZE(oModelSOY:aDataModel, 1)
			ASIZE(oModelSOY:aCols, 1)	
			
			For nX := 1 To nTamHeader
				oModel:ClearField('DETAIL_SOY' , oModelSOY:aHeader[nX][2])
			Next nX
			
			//AddLine For�a um refresh no grid, os aSizes removem a nova linha em branco.
			oModelSOY:AddLine()
			ASIZE(oModelSOY:aDataModel, 1)
			ASIZE(oModelSOY:aCols, 1)
			
			oModelSOY:GoLine(1)
			oModelSOY:UnDeleteLine() // sempre fica uma linha deletada na grid, entao retiramos o delete;
					
		EndIf 	

		If xvalue = '1' //CAMPOS DA ROTINA MATA250
			aCamposADD := {"D3_TM"     , "D3_COD"    , "D3_UM"     , "D3_QUANT"  , "D3_CONTA",;
						   "D3_OP"     , "D3_LOCAL"  , "D3_DOC"    , "D3_EMISSAO", "D3_CC",;
						   "D3_PARCTOT" , "D3_SEGUM", "D3_QTSEGUM", "D3_PERDA"  , "D3_LOTECTL",;
						   "D3_DTVALID","D3_POTENCI",IIF(IntWms(),"D3_SERVIC",'')}
		ElseIf xvalue = '2' //CAMPOS DA ROTINA MATA680
			aCamposADD := {"H6_OP"     , "H6_PRODUTO", "H6_OPERAC" , "H6_RECURSO", "H6_FERRAM" ,;
						   "H6_DATAINI", "H6_HORAINI", "H6_DATAFIN", "H6_HORAFIN", "H6_QTDPROD",;
						   "H6_QTDPERD", "H6_PT"     , "H6_DTAPONT", "H6_DESDOBR", "H6_TEMPO"  ,;
						   "H6_LOTECTL", "H6_NUMLOTE", "H6_DTVALID", "H6_OPERADO", "H6_SEQ"    ,;
						   "H6_QTDPRO2", "H6_POTENCI", "H6_RATEIO" , "H6_LOCAL"}
		ElseIf xvalue = '3' //CAMPOS DA ROTINA MATA681
			aCamposADD := {"H6_OP"     , "H6_PRODUTO", "H6_OPERAC" , "H6_RECURSO", "H6_FERRAM" ,;
						   "H6_DATAINI", "H6_HORAINI", "H6_DATAFIN", "H6_HORAFIN", "H6_QTDPROD",;
						   "H6_QTDPERD", "H6_PT"     , "H6_DTAPONT", "H6_DESDOBR", "H6_TEMPO"  ,;
						   "H6_LOTECTL", "H6_NUMLOTE", "H6_DTVALID", "H6_OBSERVA", "H6_OPERADO",;
						   "H6_SEQ"    , "H6_QTDPRO2", "H6_POTENCI", "H6_RATEIO" , "H6_LOCAL"}
		ElseIF xvalue = '4'
			aCamposADD := {'CYV_CDMQ'  ,'CYV_NRORPO','CYV_IDATQO','CYV_CDAT','CYV_CDACRP','CYV_DTBGSU',;
						   'CYV_HRBGSU','CYV_DTEDSU','CYV_HREDSU','CYV_CDSU','CYV_QTATAP','CYV_DTRPBG',;
						   'CYV_HRRPBG','CYV_DTRPED','CYV_HRRPED','CYV_CDTN','CYV_NRDO'  ,'CYV_NRSR'  ,;
						   'CYV_CDDP'  ,'CYV_CDLOSR','CYV_DTVDLO','CYW_CDOE','CYW_CDGROE','CZ0_CDFE'}
		ElseIF xvalue = '5' //CAMPO DA ROTINA MATA250 SEM O CAMPO D3_OP
			aCamposADD := {"D3_TM"     , "D3_COD"    , "D3_UM"     , "D3_QUANT"  , "D3_CONTA",;
						   "D3_LOCAL"  , "D3_DOC"    , "D3_EMISSAO", "D3_CC"     , "D3_PARCTOT",;
						   "D3_SEGUM"  , "D3_QTSEGUM", "D3_PERDA"  , "D3_LOTECTL", "D3_DTVALID",;
						   "D3_POTENCI",IIF(IntWms(),"D3_SERVIC",'')}
		Else
			aCampSOY := {}			
		EndIf

		For nX := 1 to Len(aCamposADD)
			If !Empty(aCamposADD[nX])
				Aadd(aCampSOY,{aCamposADD[nX],FWSX3Util():GetDescription(aCamposADD[nX])})
			EndIf
		Next

		oModelSOY:goline(oModelSOY:getline())
		
		For nX := 1 To len(aCampSOY)

			If  ! EMPTY(oModelSOY:GETVALUE("OY_CAMPO"))
				oModelSOY:AddLine()
			EndIf

			oModelSOY:SETVALUE("OY_CAMPO"  ,aCampSOY[nx][1])
			oModelSOY:SETVALUE("OY_DESCAMP",aCampSOY[nx][2])
			oModelSOY:SETVALUE("OY_CODBAR" ,'2')
			oModelSOY:SETVALUE("OY_VISIVEL",'1')
			oModelSOY:SETVALUE("OY_EDITA"  ,'1')
			oModelSOY:SETVALUE("OY_VALPAD" , "")
		Next nX

		If oModelSOY:Length() > 0	
			oModelSOY:goline(nLinha)
		EndIf

	EndIf
	
	oModelSOY:SetNoDeletetLine(.T.) //Bloqueia exclusao de linhas
	
Return lRet




