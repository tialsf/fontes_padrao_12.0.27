#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "PCPA126.CH"

/*/{Protheus.doc} PCPA126()
Cadastro de Grupos de Armaz�m
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return NIL
/*/
Function PCPA126()

	Local aArea := GetArea()
	Local oBrowse

	//Prote��o do fonte para n�o ser utilizado pelos clientes neste momento.
	If !(FindFunction("RodaNewPCP") .And. RodaNewPCP())
		HELP(' ',1,"Help" ,,STR0022,2,0,,,,,,) //"Rotina n�o dispon�vel nesta release."
		Return
	EndIf

	oBrowse := BrowseDef()
	oBrowse:Activate()

	RestArea(aArea)

Return Nil

/*/{Protheus.doc} BrowseDef()
Defini��o do Menu
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return oBrowse, objeto, objeto FWMBrowse criado
/*/
Static Function BrowseDef()

	Local oBrowse := FWMBrowse():New()

	oBrowse:SetAlias("SVN")
	oBrowse:SetDescription(STR0001) //"Grupo de Armaz�m"

Return oBrowse

/*/{Protheus.doc} MenuDef()
Defini��o do Menu
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return aRotina (vetor com botoes da EnchoiceBar)
/*/
Static Function MenuDef()

	Private aRotina := {}

	ADD OPTION aRotina TITLE STR0002 ACTION "VIEWDEF.PCPA126" OPERATION OP_VISUALIZAR ACCESS 0 //"Visualizar"
	ADD OPTION aRotina TITLE STR0003 ACTION "VIEWDEF.PCPA126" OPERATION OP_INCLUIR    ACCESS 0 //"Incluir"
	ADD OPTION aRotina TITLE STR0004 ACTION "VIEWDEF.PCPA126" OPERATION OP_ALTERAR    ACCESS 0 //"Alterar"
	ADD OPTION aRotina TITLE STR0005 ACTION "VIEWDEF.PCPA126" OPERATION OP_EXCLUIR    ACCESS 0 //"Excluir"

Return aRotina

/*/{Protheus.doc} ModelDef()
Defini��o do Modelo
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return oModel
/*/
Static Function ModelDef()

	Local oModel
	Local oStruSVN	:= FWFormStruct(1,"SVN")
	Local oStruSVO	:= FWFormStruct(1,"SVO",{|cCampo| ! P126FormVa(cCampo) $ "|VO_LOCGRP|"})
	Local oStruNNR	:= FWFormStruct(1,"NNR",{|cCampo|   P126FormVa(cCampo) $ "|NNR_CODIGO|NNR_DESCRI|"})
	Local oEventPad	:= PCPA126EVDEF():New()
	Local oEventApi := PCPA126API():New()

	oModel := MPFormModel():New("PCPA126", , )

	//--Mestre (SVN - Grupo de Armaz�m)
	oStruSVN:SetProperty( "VN_TIPO", MODEL_FIELD_VALID, {||P126VldTip(oModel)} )
	oStruSVN:SetProperty( "VN_TIPO", MODEL_FIELD_NOUPD, .T. )

	//--Detalhe (SVO - Armaz�ns do Grupo)
	oStruSVO:SetProperty( "VO_LOCAL", MODEL_FIELD_VALID, {||P126VldArm(oModel)} )

	oModel:AddFields("SVN_MASTER", /*cOwner*/, oStruSVN)
	oModel:GetModel( "SVN_MASTER" ):SetDescription(STR0001) //"Grupo de Armaz�m"

	P126AddDes( .T. , @oStruSVO, oModel )

	oModel:AddGrid(  "NNR_DETAIL", "SVN_MASTER", oStruNNR )
	oModel:AddGrid(  "SVO_DETAIL", "SVN_MASTER", oStruSVO )
	oModel:GetModel( "SVO_DETAIL" ):SetDescription(STR0006) //"Armaz�ns do Grupo"

	oModel:GetModel( "SVO_DETAIL" ):SetOptional(.T.)

	oModel:SetPrimaryKey( {} )
	oModel:SetRelation("SVO_DETAIL",{{"VO_FILIAL","xFilial('SVO')"},{"VO_LOCGRP","VN_LOCGRP"}}, SVO->(IndexKey(2)))

	oModel:GetModel( "SVO_DETAIL" ):SetUniqueLine( {"VO_LOCAL"} )

	//--Detalhe (Armaz�ns que n�o possuem grupo)
	oModel:GetModel( "NNR_DETAIL" ):SetDescription(STR0007) //"Armaz�ns sem Grupo"
	oModel:GetModel( "NNR_DETAIL" ):SetOptional(.T.)
	oModel:GetModel( "NNR_DETAIL" ):SetOnlyView()
	oModel:GetModel( "NNR_DETAIL" ):SetOnlyQuery()
	oModel:SetRelation("NNR_DETAIL",{{"NNR_FILIAL","xFilial('NNR')"}}, NNR->(IndexKey(1)))
	cQryNNR := " NOT EXISTS (SELECT 1 " 							+ ;
							  "FROM " + RetSqlName("SVO") + " SVO " + ;
							 "WHERE SVO.VO_FILIAL  = NNR_FILIAL "	+ ;
							   "AND SVO.VO_LOCAL = NNR_CODIGO "	+ ;
							   "AND SVO.D_E_L_E_T_ = ' ') "
	oModel:GetModel("NNR_DETAIL"):SetLoadFilter( , cQryNNR )

	//-Demais defini��es do modelo
	oModel:SetDescription(STR0001) //"Grupo de Armaz�m"
	oModel:InstallEvent("PCPA126EVDEF", /*cOwner*/, oEventPad)
	oModel:InstallEvent("PCPA126API"  , /*cOwner*/, oEventApi)

Return oModel

/*/{Protheus.doc} ViewDef()
Defini��o da View
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return oView
/*/
Static Function ViewDef()

	Local oModel	:= FWLoadModel("PCPA126")
	Local oStruSVN	:= FWFormStruct(2,"SVN")
	Local oStruSVO	:= FWFormStruct(2,"SVO",{|cCampo| ! P126FormVa(cCampo) $ "|VO_LOCGRP|"})
	Local oStruNNR	:= FWFormStruct(2,"NNR",{|cCampo|   P126FormVa(cCampo) $ "|NNR_CODIGO|NNR_DESCRI|"})
	Local oView

	oView := FWFormView():New()
	oView:SetModel( oModel )

	oStruSVO:SetProperty( "VO_DEFAULT", MVC_VIEW_ORDEM , "05" )
	P126AddDes( .F. , @oStruSVO, oModel )

	oView:AddField("VIEW_SVN", oStruSVN, "SVN_MASTER" )
	oView:AddGrid( "VIEW_SVO", oStruSVO, "SVO_DETAIL" )
	oView:AddGrid( "VIEW_NNR", oStruNNR, "NNR_DETAIL" )

	oView:CreateHorizontalBox( "HEADER"   , 100, , .T.) //-- 100 Pixels
	oView:CreateHorizontalBox( "VISIVEL" , 100  )
	oView:CreateHorizontalBox( "INVISIVEL", 0 )

	oView:SetOwnerView( "VIEW_SVN", "HEADER" )
	//--Os boxs VISIVEL e INVISIVEL s�o setados no M�todo Activate() da Classe PCPA126EVDEF

	oView:SetOnlyView("VIEW_NNR")

Return oView

/*/{Protheus.doc} P126AddDes()
Adiciona campo de Descri��o do Armaz�m
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
/*/
Static Function P126AddDes(lModel, oStru, oModel)

	//--Verifica a inst�ncia (Model ou View)
	If lModel
		//--Campo Descri��o do Armaz�m
		oStru:AddField(	RetTitle("NNR_DESCRI")					,;	// [01]  C   Titulo do campo  - Produto
						RetTitle("NNR_DESCRI")					,;	// [02]  C   ToolTip do campo - C�digo do Produto
						"SVO_DESCARM"	   						,;	// [03]  C   Id do Field
						"C"										,;	// [04]  C   Tipo do campo
						GetSx3Cache("NNR_DESCRI","X3_TAMANHO")	,;	// [05]  N   Tamanho do campo
						0										,;	// [06]  N   Decimal do campo
						NIL										,;	// [07]  B   Code-block de valida��o do campo
						NIL							   			,;	// [08]  B   Code-block de valida��o When do campo
						NIL										,; 	// [09]  A   Lista de valores permitido do campo
						.F.										,; 	// [10]  L   Indica se o campo tem preenchimento obrigat�rio
						{|oModel| P126DesArm( .F., oModel )}	,;	// [11]  B   Code-block de inicializacao do campo
						NIL										,;	// [12]  L   Indica se trata-se de um campo chave
						NIL										,;	// [13]  L   Indica se o campo pode receber valor em uma opera��o de update.
						.T.										)   // [14]  L   Indica se o campo � virtual
	Else
		//--Campo Descri��o do Armaz�m
		oStru:AddField(	"SVO_DESCARM"			,;	// [01]  C   Nome do Campo
						"04"					,;	// [02]  C   Ordem
						RetTitle("NNR_DESCRI")	,;	// [03]  C   Titulo do campo
						RetTitle("NNR_DESCRI")	,;	// [04]  C   Descricao do campo
						NIL						,;	// [05]  A   Array com Help
						"C"						,; 	// [06]  C   Tipo do campo
						""						,;	// [07]  C   Picture
						NIL						,;	// [08]  B   Bloco de Picture Var
						NIL						,;	// [09]  C   Consulta F3
						.F.						,;	// [10]  L   Indica se o campo � alteravel
						NIL						,;	// [11]  C   Pasta do campo
						NIL						,;	// [12]  C   Agrupamento do campo
						NIL						,;	// [13]  A   Lista de valores permitido do campo (Combo)
						NIL						,;	// [14]  N   Tamanho maximo da maior op��o do combo
						NIL						,;	// [15]  C   Inicializador de Browse
						.T.						,;	// [16]  L   Indica se o campo � virtual
						NIL						,;	// [17]  C   Picture Variavel
						NIL						)	// [18]  L   Indica pulo de linha ap�s o campo
	EndIf

Return

/*/{Protheus.doc} P126VldTip
Valida��o do campo "Tipo"
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return .T.
/*/
Static Function P126VldTip(oModel)

	Local nTipo := oModel:GetModel("SVN_MASTER"):GetValue("VN_TIPO")

	//--Se for do tipo Filial n�o deve permitir a altera��o da Grid
	If nTipo == "1"
		oModel:GetModel("SVO_DETAIL"):DelAllLine()
		oModel:GetModel("SVO_DETAIL"):SetNoInsertLine( .T. )
		oModel:GetModel("SVO_DETAIL"):SetNoUpdateLine( .T. )
		oModel:GetModel("SVO_DETAIL"):SetNoDeleteLine( .T. )
	Else
		oModel:GetModel("SVO_DETAIL"):SetNoInsertLine( .F. )
		oModel:GetModel("SVO_DETAIL"):SetNoUpdateLine( .F. )
		oModel:GetModel("SVO_DETAIL"):SetNoDeleteLine( .F. )
	EndIf

Return .T.

/*/{Protheus.doc} P126VldArm
Valida��o do campo "Armaz�m"
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return lRet
/*/
Static Function P126VldArm(oModel)

	Local cDescArm := P126DesArm( .T., oModel:GetModel("SVO_DETAIL") )
	Local lRet     := .T.

	If Empty(cDescArm)
		lRet := .F.
	Else
		oModel:LoadValue("SVO_DETAIL", "SVO_DESCARM", cDescArm)
	EndIf

Return lRet

/*/{Protheus.doc} P126DesArm()
Carrega descri��o do armaz�m
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return cDesc
/*/
Static Function P126DesArm(lGatilho, oModelSVO)

	Local cDesc		:= ""
	Local cArmazem	:= ""
	Local aArea 	:= GetArea()
	Local nOperation:= oModelSVO:GetOperation()

	Default lGatilho := .F.

	If lGatilho
		cArmazem := oModelSVO:GetValue("VO_LOCAL")
	Else
		If nOperation != MODEL_OPERATION_INSERT .And. oModelSVO:GetLine() == 0
        	cArmazem := SVO->VO_LOCAL
		EndIf
	EndIf

	If !Empty(cArmazem)
		cDesc := Posicione("NNR", 1, xFilial("NNR")+cArmazem, "NNR_DESCRI")
	EndIf

	RestArea(aArea)

Return cDesc

/*/{Protheus.doc} P126FormVa()
Fun��o para formatar a vari�vel para utiliza��o do operador $
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return cVar
/*/
Static Function P126FormVa(cVar)

	cVar := "|" + AlLTrim(cVar) + "|"

Return cVar