#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"

//Define constantes para utilizar nos arrays.
//Em outros fontes, utilizar a fun��o A126APICnt para recuperar o valor das constantes.
//Ao criar novas constantes, adicionar na fun��o A126APICnt
//Campos do Grupo (cabe�alho)
#DEFINE ARRAY_GRUPOARM_CAB_POS_FILIAL  1
#DEFINE ARRAY_GRUPOARM_CAB_POS_GRUPO   2
#DEFINE ARRAY_GRUPOARM_CAB_POS_TIPO    3
#DEFINE ARRAY_GRUPOARM_CAB_POS_LISTA   4
#DEFINE ARRAY_GRUPOARM_CAB_SIZE        4
//Campos dos armaz�ns (lista)
#DEFINE ARRAY_GRUPOARM_ARM_POS_ARMAZEM 1
#DEFINE ARRAY_GRUPOARM_ARM_POS_DEFAULT 2
#DEFINE ARRAY_GRUPOARM_ARM_POS_CODE    3
#DEFINE ARRAY_GRUPOARM_ARM_SIZE        3

/*/{Protheus.doc} A126APICnt
Recupera o valor das constantes utilizadas para auxiliar na montagem do array para integra��o

@type  Function
@author marcelo.neumann
@since 20/09/2019
@version P12.1.27
@param cInfo, Caracter, Define qual constante se deseja recuperar o valor.
@return nValue, Numeric, Valor da constante
/*/
Function A126APICnt(cInfo)
	Local nValue := ARRAY_GRUPOARM_ARM_SIZE
	Do Case
		Case cInfo == "ARRAY_GRUPOARM_CAB_POS_FILIAL"
			nValue := ARRAY_GRUPOARM_CAB_POS_FILIAL
		Case cInfo == "ARRAY_GRUPOARM_CAB_POS_GRUPO"
			nValue := ARRAY_GRUPOARM_CAB_POS_GRUPO
		Case cInfo == "ARRAY_GRUPOARM_CAB_POS_TIPO"
			nValue := ARRAY_GRUPOARM_CAB_POS_TIPO
		Case cInfo == "ARRAY_GRUPOARM_CAB_POS_LISTA"
			nValue := ARRAY_GRUPOARM_CAB_POS_LISTA
		Case cInfo == "ARRAY_GRUPOARM_CAB_SIZE"
			nValue := ARRAY_GRUPOARM_CAB_SIZE
		Case cInfo == "ARRAY_GRUPOARM_ARM_POS_ARMAZEM"
			nValue := ARRAY_GRUPOARM_ARM_POS_ARMAZEM
		Case cInfo == "ARRAY_GRUPOARM_ARM_POS_DEFAULT"
			nValue := ARRAY_GRUPOARM_ARM_POS_DEFAULT
		Case cInfo == "ARRAY_GRUPOARM_ARM_POS_CODE"
			nValue := ARRAY_GRUPOARM_ARM_POS_CODE
		Case cInfo == "ARRAY_GRUPOARM_ARM_SIZE"
			nValue := ARRAY_GRUPOARM_ARM_SIZE
		Otherwise
			nValue := ARRAY_GRUPOARM_ARM_SIZE
	EndCase
Return nValue

/*/{Protheus.doc} PCPA126API
Eventos de integra��o do Cadastro de Grupos de Armaz�m

@author marcelo.neumann
@since 20/09/2019
@version P12.1.27
/*/
CLASS PCPA126API FROM FWModelEvent

	DATA lIntegraMRP    AS LOGIC
	DATA lIntegraOnline AS LOGIC
	DATA oTTPend        AS OBJECT

	METHOD New() CONSTRUCTOR
	METHOD InTTS(oModel, cModelId)
	METHOD Destroy()

ENDCLASS

/*/{Protheus.doc} NEW
M�todo construtor do evento de integra��o das integra��es do Cadastro de Grupos de Armaz�m

@author marcelo.neumann
@since 20/09/2019
@version P12.1.27
/*/
METHOD New() CLASS PCPA126API

	Local lUsaMalha := SuperGetMV("MV_PCPMADI",.F.,.F.)

	::lIntegraMRP    := .F.
	::lIntegraOnline := .F.

	If lUsaMalha
		::lIntegraMRP := IntNewMRP("MRPWAREHOUSEGROUP", @::lIntegraOnline)

		If ::lIntegraMRP .And. ::lIntegraOnline
			::oTTPend    := TTPendMRP()
		EndIf
	EndIf

Return Self

/*/{Protheus.doc} InTTS
M�todo que � chamado pelo MVC quando ocorrer as a��es do commit Ap�s as grava��es por�m antes do final da transa��o

@author marcelo.neumann
@since 20/09/2019
@version P12.1.27
@param oModel  , Object  , Modelo principal
@param cModelId, Caracter, Id do submodelo
@return Nil
/*/
METHOD InTTS(oModel, cModelId) CLASS PCPA126API

	//S� executa a integra��o se estiver parametrizado como Online
	If ::lIntegraMRP == .F. .Or. ::lIntegraOnline == .F.
		Return
	EndIf

	integraAPI(oModel, Self)

Return Nil

/*/{Protheus.doc} integraAPI
Integra dados com a API

@author marcelo.neumann
@since 20/09/2019
@version P12.1.27
@param oModel, Object, Modelo principal
@param Self  , objeto, instancia atual desta classe
@return Nil
/*/
Static Function integraAPI(oModel, Self)
	Local aDadosDel  := {}
	Local aDadosInc  := {}
	Local aDadosDet  := {}
	Local cFilialSVN := ""
	Local nIndex     := 0
	Local nTotal     := 0
	Local nPos       := 0
	Local oMdlCab    := oModel:GetModel("SVN_MASTER")
	Local oMdlDet    := oModel:GetModel("SVO_DETAIL")

	If Empty(oMdlCab:GetValue("VN_FILIAL"))
		cFilialSVN := xFilial("SVN")
	Else
		cFilialSVN := oMdlCab:GetValue("VN_FILIAL")
	EndIf

	If oModel:GetOperation() == MODEL_OPERATION_DELETE
		aAdd(aDadosDel, Array(ARRAY_GRUPOARM_CAB_SIZE))
		aDadosDel[1][ARRAY_GRUPOARM_CAB_POS_FILIAL] := cFilialSVN
		aDadosDel[1][ARRAY_GRUPOARM_CAB_POS_GRUPO ] := oMdlCab:GetValue("VN_LOCGRP")

		PCPA126INT("DELETE", aDadosDel, Self:oTTPend)
	Else
		aAdd(aDadosInc, Array(ARRAY_GRUPOARM_CAB_SIZE))

		//Adiciona as informa��es de cabe�alho no array de inclus�o/atualiza��o
		aDadosInc[1][ARRAY_GRUPOARM_CAB_POS_FILIAL] := cFilialSVN
		aDadosInc[1][ARRAY_GRUPOARM_CAB_POS_GRUPO ] := oMdlCab:GetValue("VN_LOCGRP")
		aDadosInc[1][ARRAY_GRUPOARM_CAB_POS_TIPO  ] := oMdlCab:GetValue("VN_TIPO")
		aDadosInc[1][ARRAY_GRUPOARM_CAB_POS_LISTA ] := {}

		If aDadosInc[1][ARRAY_GRUPOARM_CAB_POS_TIPO] == "1"
			aDadosDet := Array(ARRAY_GRUPOARM_ARM_SIZE)

			//Adiciona as informa��es dos armaz�ns no array de inclus�o/atualiza��o
			aDadosDet[ARRAY_GRUPOARM_ARM_POS_CODE] := cFilialSVN + oMdlCab:GetValue("VN_LOCGRP")

			aAdd(aDadosInc[1][ARRAY_GRUPOARM_CAB_POS_LISTA], aDadosDet)
		Else
			//Adiciona as linhas para enviar � API
			nTotal := oMdlDet:Length()

			For nIndex := 1 To nTotal
				If oMdlDet:IsDeleted(nIndex)
					Loop
				EndIf

				//Adiciona nova linha no array de inclus�o/atualiza��o
				aSize(aDadosDet, 0)
				aDadosDet := Array(ARRAY_GRUPOARM_ARM_SIZE)
				nPos      := Len(aDadosDet)

				//Adiciona as informa��es dos armaz�ns no array de inclus�o/atualiza��o
				aDadosDet[ARRAY_GRUPOARM_ARM_POS_ARMAZEM] := oMdlDet:GetValue("VO_LOCAL"   , nIndex)
				aDadosDet[ARRAY_GRUPOARM_ARM_POS_DEFAULT] := oMdlDet:GetValue("VO_DEFAULT" , nIndex)
				aDadosDet[ARRAY_GRUPOARM_ARM_POS_CODE   ] := cFilialSVN                    + ;
															 oMdlCab:GetValue("VN_LOCGRP") + ;
															 oMdlDet:GetValue("VO_LOCAL"   , nIndex)

				aAdd(aDadosInc[1][ARRAY_GRUPOARM_CAB_POS_LISTA], aClone(aDadosDet))
			Next nIndex
		EndIf

		//Se tem alguma informa��o para inserir
		If Len(aDadosDet) > 0
			PCPA126INT("INSERT", aDadosInc, Self:oTTPend)
		EndIf
	EndIf

	aSize(aDadosDel, 0)
	aSize(aDadosInc, 0)
	aSize(aDadosDet, 0)

Return

/*/{Protheus.doc} Destroy
M�todo destrutor do evento de integra��o

@author douglas.heydt
@since 20/09/2019
@version P12.1.27
/*/
METHOD Destroy() CLASS PCPA126API
	//Deleta a tabela tempor�ria.
	If ::lIntegraMRP .And. ::lIntegraOnline
		::oTTPend:Delete()
		::oTTPend := Nil
	EndIf
Return Self

/*/{Protheus.doc} PCPA126INT
Fun��o que executa a integra��o do Grupo de Armaz�m com o MRP

@type  Function
@author marcelo.neumann
@since 20/09/2019
@version P12.1.27
@param cOperation, Caracter, Opera��o que ser� executada ('DELETE'/'INSERT'/'SYNC')
@param aDados    , Array   , Array com os dados que devem ser integrados com o MRP.
@param oTTPend   , Object  , Objeto da tabela tempor�ria utilizada no processamento das pend�ncias. Tabela criada pela fun��o TTPendMRP
@param aSuccess  , Array   , Carrega os registros que foram integrados com sucesso
@param aError    , Array   , Carrega os registros que n�o foram integrados por erro
@param cUUID     , Caracter, Identificador do processo do SCHEDULE. Utilizado para atualiza��o de pend�ncias.
@param lOnlyDel  , Logic   , Indica que est� sendo executada uma opera��o de Sincroniza��o apenas excluindo os dados existentes (envia somente filial).
@return Nil
/*/
Function PCPA126INT(cOperation, aDados, oTTPend, aSuccess, aError, cUUID, lOnlyDel)
	Local aReturn   := {}
	Local lAllError := .F.
	Local nIndexCab := 0
	Local nIndexLst := 0
	Local nTotalCab := 0
	Local nTotalLst := 0
	Local oJsonData := Nil
	Local cApi 		:= "MRPWAREHOUSEGROUP"

	Default aSuccess := {}
	Default aError   := {}
	Default lOnlyDel := .F.

	nTotalCab := Len(aDados)
	oJsonData := JsonObject():New()

	oJsonData["items"] := Array(nTotalCab)

	For nIndexCab := 1 To nTotalCab
		oJsonData["items"][nIndexCab] := JsonObject():New()
		oJsonData["items"][nIndexCab]["branchId"]       := aDados[nIndexCab][ARRAY_GRUPOARM_CAB_POS_FILIAL]
		If ! (lOnlyDel .And. cOperation == "SYNC")
			oJsonData["items"][nIndexCab]["warehouseGroup"] := aDados[nIndexCab][ARRAY_GRUPOARM_CAB_POS_GRUPO]

			If cOperation $ "|INSERT|SYNC|"
				nTotalLst := Len(aDados[nIndexCab][ARRAY_GRUPOARM_CAB_POS_LISTA])

				oJsonData["items"][nIndexCab]["typeGroup"]          := aDados[nIndexCab][ARRAY_GRUPOARM_CAB_POS_TIPO]
				oJsonData["items"][nIndexCab]["listOfMRPWarehouse"] := Array(nTotalLst)

				For nIndexLst := 1 To nTotalLst
					oJsonData["items"][nIndexCab]["listOfMRPWarehouse"][nIndexLst] := JsonObject():New()
					oJsonData["items"][nIndexCab]["listOfMRPWarehouse"][nIndexLst]["warehouse"]        := aDados[nIndexCab][ARRAY_GRUPOARM_CAB_POS_LISTA][nIndexLst][ARRAY_GRUPOARM_ARM_POS_ARMAZEM]
					oJsonData["items"][nIndexCab]["listOfMRPWarehouse"][nIndexLst]["warehouseDefault"] := aDados[nIndexCab][ARRAY_GRUPOARM_CAB_POS_LISTA][nIndexLst][ARRAY_GRUPOARM_ARM_POS_DEFAULT]
					oJsonData["items"][nIndexCab]["listOfMRPWarehouse"][nIndexLst]["code"]             := aDados[nIndexCab][ARRAY_GRUPOARM_CAB_POS_LISTA][nIndexLst][ARRAY_GRUPOARM_ARM_POS_CODE]
				Next nIndexLst
			EndIf
		EndIf
	Next nIndexCab

	If cOperation $ "|INSERT|SYNC|"
		If cOperation == "INSERT"
			aReturn := MrpGrAPost(oJsonData)
		Else
			aReturn := MrpGrASync(oJsonData)
		EndIf
		PrcPendMRP(aReturn, cApi, oJsonData, oTTPend, .F., @aSuccess, @aError, @lAllError, '1', cUUID)
	Else
		aReturn := MrpGrADel(oJsonData)
		PrcPendMRP(aReturn, cApi, oJsonData, oTTPend, .F., @aSuccess, @aError, @lAllError, '2', cUUID)
	EndIf

	FreeObj(oJsonData)
	oJsonData := Nil

Return Nil