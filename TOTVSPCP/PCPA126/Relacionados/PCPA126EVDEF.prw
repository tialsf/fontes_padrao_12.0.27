#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWADAPTEREAI.CH"
#INCLUDE "PCPA126.CH"

/*/{Protheus.doc} PCPA126EVDEF
Eventos padr�es da manuten��o dos processos produtivos
@author Marcelo Neumann
@since 12/06/2018
@version P12.1.17
/*/

CLASS PCPA126EVDEF FROM FWModelEvent

	METHOD New() CONSTRUCTOR

	METHOD Activate()
	METHOD GridLinePosVld()
	METHOD ModelPosVld()
	METHOD IniGrid()

ENDCLASS

METHOD New() CLASS  PCPA126EVDEF

Return

//---------------------------------------------------------------------
/*/{Protheus.doc} Activate()
M�todo chamado pelo MVC quando ocorre a ativa��o do Model
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return
/*/
//---------------------------------------------------------------------
METHOD Activate(oModel, lCopy) CLASS PCPA126EVDEF

	If oModel:GetOperation() == MODEL_OPERATION_INSERT
		//Quando inclus�o, inicializa com o tipo 2 - Local de estoque.
		oModel:GetModel("SVN_MASTER"):SetValue("VN_TIPO","2")
	EndIf

	::IniGrid( oModel )

Return

//---------------------------------------------------------------------
/*{Protheus.doc} a131CmpIt(aCalendar)
Retorna um array com os periodos dos calendarios do grupo de armazem selecionado
@author Douglas Heydt
@since 18/07/2018
@version 1.0
@return lRet
/*/
//---------------------------------------------------------------------
Function a131CmpIt(aCalendar)

	Local cAliasSVX := GetNextAlias()
	Local cQuery 	:= ""
	Local nX		:= 0
	Local cCal		:= ""
	Local aItCal	:= {}
	Local aCalends	:= {}
	cCal := " VX_CALEND = '"+aCalendar[1]+"'" 
		
	For nX := 2 To Len(aCalendar)
		cCal += " OR VX_CALEND = '"+aCalendar[nX]+"' "
	Next nX
	cQuery := "select VX_FILIAL, VX_CALEND, VX_DATAINI, VX_DATAFIM FROM "+RetSQLName('SVX')+" WHERE VX_FILIAL = '"+xFilial("SVX")+"' AND "+cCal+" AND D_E_L_E_T_ <> '*' "
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSVX,.T.,.T.) 
	dbSelectArea(cAliasSVX)
	
	While (cAliasSVX)->(!Eof())
		aAdd(aItCal, (cAliasSVX)->VX_CALEND)
		aAdd(aItCal, (cAliasSVX)->VX_DATAINI)
		aAdd(aItCal, (cAliasSVX)->VX_DATAFIM)
		aAdd(aCalends, aItCal)
		aItCal := {}
		(cAliasSVX)->(DbSkip())
	EndDo

Return aCalends

//---------------------------------------------------------------------
/*/{Protheus.doc} GridLinePosVld()
M�todo chamado pelo MVC quando ocorrer as a��es de pos valida��o da linha do Grid
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return lRet
/*/
//---------------------------------------------------------------------
METHOD GridLinePosVld(oSubModel, cModelID, nLine) CLASS PCPA126EVDEF

	Local lRet		:= .T.
	Local aArea		:= GetArea()
	Local oModel	:= FWModelActive()
	Local cGrupo	:= oModel:GetModel("SVN_MASTER"):GetValue("VN_LOCGRP",	nLine)
	Local cArmazem	:= ""

	If cModelID = "SVO_DETAIL"
		//Valida se o Armaz�m j� est� em outro grupo
		cArmazem := oSubModel:GetValue("VO_LOCAL",	nLine)
		dbSelectArea("SVO")
		SVO->(dbSetOrder(1))
		If SVO->(DbSeek(xFilial("SVO") + cArmazem))
			If !SVO->(Eof())					 .And. ;
				SVO->VO_FILIAL == xFilial("SVO") .And. ;
				SVO->VO_LOCAL  == cArmazem		 .And. ;
				SVO->VO_LOCGRP != cGrupo

				lRet := .F.
				Help( ,  , "Help", ,  STR0012 + AllTrim(SVO->VO_LOCGRP),;	//"Esse armaz�m j� est� cadastrado no grupo "
					 1, 0, , , , , , {STR0013}) 							//"Remova esse armaz�m do outro grupo para poder inclu�-lo neste."
			EndIf
		EndIf

	EndIf

	RestArea(aArea)

Return lRet

//---------------------------------------------------------------------
/*/{Protheus.doc} ModelPosVld()
M�todo chamado pelo MVC quando ocorrer as a��es de pos valida��o do Model
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return lRet
/*/
//---------------------------------------------------------------------
METHOD ModelPosVld(oModel, cModelID) CLASS PCPA126EVDEF

	Local lRet		:= .T.
	Local nTipo		:= oModel:GetModel("SVN_MASTER"):GetValue("VN_TIPO")
	Local oModelGrid:= oModel:GetModel("SVO_DETAIL")
	Local nQtdGrid	:= NIL
	Local nLin		:= NIL
	Local lDefLine	:= 0

	If !SUPERGETMV( "MV_PCPMADI", .F., .F., cFilAnt)
		Help( , , 'A126FILMADI', , STR0023, ;//"N�o � poss�vel alterar o(s) registro(s) pois o par�metro de Malhas de Distribui��o est� desativado"
				1, 0, NIL, NIL, NIL, NIL, NIL, {STR0024})//"Ative o par�metro MV_PCPMADI para utilizar os recursos de Malha de Distribui��o"
		lRet := .F.
	EndIf
	
	If lRet
		//Se for do tipo Filial o grid n�o pode estar preenchido
		If nTipo == "1"
			oModelGrid:SetNoDeleteLine( .F. )
			oModelGrid:DelAllLine()

			If ! ExistChav("SVN", nTipo, 2)
				lRet := .F.
				oModelGrid:SetNoDeleteLine( .T. )
				Help( ,  , "Help", ,  STR0008,; //"J� existe um Grupo de Armaz�m cadastrado com o tipo Filial."
					1, 0, , , , , , {STR0009}) //"Exclua o grupo de tipo Filial ou informe o tipo Local de Estoque."
			EndIf
		Else
			If Empty( oModelGrid:GetValue("VO_LOCAL", 1) )
				lRet := .F.
				Help( ,  , "Help", ,  STR0016,; //"Nenhum amaz�m informado."
					1, 0, , , , , , {STR0017}) //"Informe pelo menos um armaz�m para o grupo."
			Else
				If oModelGrid:SeekLine( {{"VO_DEFAULT", "1"}}, .F., .F. )
					nQtdGrid := oModelGrid:Length( .F. )
					For nLin := 1 To nQtdGrid
						If ! oModelGrid:IsDeleted(nLin)
							//Valida se j� foi informado algum Armaz�m como o default nesse grupo
							If oModelGrid:GetValue("VO_DEFAULT", nLin) == "1"
								If lDefLine = 0
									lDefLine := nLin
								Else
									lRet := .F.
									Help( ,  , "Help", ,  STR0010,; //"J� foi informado um armaz�m como Padr�o do grupo."
										1, 0, , , , , , {STR0011}) //"Informe somente um armaz�m com o Padr�o igual a 1-Sim."
									Exit
								EndIf
							EndIf

						EndIf
					Next nLin
				Else
					lRet := .F.
					Help( ,  , "Help", ,  STR0014,; //"Amaz�m padr�o n�o informado."
						1, 0, , , , , , {STR0015}) //"Informe um armaz�m com o campo Padr�o igual a Sim."
				EndIf
			EndIf
		EndIf
	EndIf
	
Return lRet

//---------------------------------------------------------------------
/*{Protheus.doc} IniGrid()
Inicializa��o da Grid de Armaz�ns
@author Marcelo Neumann
@since 12/06/2018
@version 1.0
@return
/*/
//---------------------------------------------------------------------
METHOD IniGrid(oModel) CLASS PCPA126EVDEF

	Local oView		:= FwViewActive()
	Local nTipo		:= oModel:GetModel("SVN_MASTER"):GetValue("VN_TIPO")

	If nTipo == '1' .And. oView != Nil
		oView:SetOwnerView( "VIEW_NNR", "VISIVEL" )
		oView:SetOwnerView( "VIEW_SVO", "INVISIVEL" )
	Else
		oModel:GetModel("SVO_DETAIL"):SetNoInsertLine( .F. )
		oModel:GetModel("SVO_DETAIL"):SetNoUpdateLine( .F. )
		oModel:GetModel("SVO_DETAIL"):SetNoDeleteLine( .F. )
		If oModel:GetOperation() == MODEL_OPERATION_INSERT
			//Quando inclus�o, inicializa o campo VO_DAFAULT para 2 - N�o.
			oModel:GetModel("SVO_DETAIL"):SetValue("VO_DEFAULT","2")
		EndIf
		If oView != Nil
			oView:SetOwnerView( "VIEW_SVO", "VISIVEL" )
			oView:SetOwnerView( "VIEW_NNR", "INVISIVEL" )
		EndIf
	EndIf
Return

//---------------------------------------------------------------------
/*{Protheus.doc} P126RetDt()
Busca as datas de periodos definidos para cada calend�rio
@author Douglas Heydt
@since 12/06/2018
@version 1.0
@oaram - cCodCal : Codigo do calendario
@oaram - ldataIni : data inicial, .T. ou .F. para definir qual campo retornar
@return
/*/
//---------------------------------------------------------------------
Function P126RetDt(cCodCal, lDtIni)

	Local aArea 	:= GetArea()

	If !Empty(cCodCal)		
		SVX->(dbSetOrder(1))
		If SVX->(dbSeek( xFilial("SVX")+cCodCal))
				If lDtIni
					RestArea(aArea)
				 	Return SVX->VX_DATAINI
				ElseIf !lDtIni
					RestArea(aArea)
					Return SVX->VX_DATAFIM
				EndIf
		EndIf       
	EndIf
	RestArea(aArea)
Return

//---------------------------------------------------------------------
/*{Protheus.doc} l126VldDt()
Verifica se dois periodos distintos possuem datas concorrentes
@author Douglas Heydt
@since 12/06/2018
@version 1.0
@param dtIni : data inicial do per�odo 1
@param dtFin : data final do per�odo 1
@param dtProvIni  : data inicial do per�odo 2
@param dtProvFin  : data final do per�odo 2
@return
/*/
//---------------------------------------------------------------------
Function l126VldDt(dtIni, dtFin, dtProvIni, dtProvFin)

	Local lRange1 := .F.
	Local lRange2 := .F.
	Local lRange3 := .F.
	Local lRange4 := .F.
	Local lRange5 := .F.
	Local lRet := .F.
	If dtProvIni >= dtIni .And. dtProvIni < dtFin
		lRange1 := .T.
	EndIf
	If dtProvFin >= dtIni .And. dtProvFin < dtFin
		lRange2 := .T.
	EndIf
	
	If dtIni > dtProvIni  .And. dtFin < dtProvIni
		lRange3 := .T.
	EndIf
	If dtFin > dtProvIni  .And. dtFin < dtProvFin
		lRange4 := .T.
	EndIf

	If dtIni >= dtProvIni  .And. dtFin <= dtProvFin
		lRange5 := .T.
	EndIf
	
	If lRange1 .Or. lRange2 .Or. lRange3 .Or. lRange4 .Or. lRange5
		lRet := .T. 
	EndIf

Return lRet

