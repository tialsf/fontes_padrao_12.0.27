#INCLUDE "PROTHEUS.CH"
#INCLUDE "PCPA130.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} PCPA130
Produto X Grupo de Armaz�m
@author Douglas Heydt
@since 03/07/2018
@version P12
@return NIL
/*/
Function PCPA130()

	//Prote��o do fonte para n�o ser utilizado pelos clientes neste momento.
	If !(FindFunction("RodaNewPCP") .And. RodaNewPCP())
		HELP(' ',1,"Help" ,,STR0009,2,0,,,,,,) //"Rotina n�o dispon�vel nesta release."
		Return
	EndIf

Return NIL

/*/{Protheus.doc} ModelDef
Defini��o do Modelo
@author Douglas Heydt
@since 03/07/2018
@version P12
@return oModel
/*/
Static Function ModelDef()

	Local oStruSVP_M := FWFormStruct(1, 'SVP' ,{|cCampo| AllTrim(cCampo) $ "VP_PRODUTO"})
	Local oStruSVP_D := FWFormStruct(1, 'SVP' )
	Local oEvent     := PCPA130EVDEF():New()
	Local oModel     := MPFormModel():New('PCPA130' )

	oStruSVP_M:AddField(STR0008             , ;  // [01] C Titulo do campo
	                    STR0008             , ;  // [02] C ToolTip do campo
	                    "B1_DESC"           , ;  // [03] C identificador (ID) do Field
	                    'C'                 , ;  // [04] C Tipo do campo
	                    TamSx3("B1_DESC")[1], ;  // [05] N Tamanho do campo
	                    0                   , ;  // [06] N Decimal do campo
	                    NIL                 , ;  // [07] B Code-block de valida��o do campo
	                    NIL                 , ;  // [08] B Code-block de valida��o When do campo
	                    NIL                 , ;  // [09] A Lista de valores permitido do campo
	                    .F.                 , ;  // [10] L Indica se o campo tem preenchimento obrigat�rio
	                    FWBuildFeature( STRUCT_FEATURE_INIPAD, "pcpa130Ini()"), ;  // [11] B Code-block de inicializacao do campo
	                    .F.                 , ;  // [12] L Indica se trata de um campo chave
	                    .F.                 , ;  // [13] L Indica se o campo pode receber valor em uma opera��o de update.
	                    .T.                 )    // [14] L Indica se o campo � virtual

	oStruSVP_D:AddTrigger("VP_LOCGRP", "VP_LOCGRPD", {|| .T. }, {|| PCPA130Prd() })

	oModel:AddFields('SVPMASTER', /*cOwner*/ , oStruSVP_M)
	oModel:AddGrid(  "SVPDETAIL", "SVPMASTER", oStruSVP_D)

	oStruSVP_M:SetProperty('VP_PRODUTO' , MODEL_FIELD_NOUPD   , .T.)
	oStruSVP_D:SetProperty('VP_PRODUTO' , MODEL_FIELD_NOUPD   , .T.)
	oStruSVP_D:SetProperty('VP_PRODUTO' , MODEL_FIELD_OBRIGAT , .F.)

	oModel:SetPrimaryKey({})
	oModel:SetRelation('SVPDETAIL', { {'VP_FILIAL', 'xFilial("SVP")'},{'VP_PRODUTO', 'SVPMASTER.VP_PRODUTO'} }, SVP->(IndexKey(2)) )

	oModel:GetModel("SVPDETAIL"):SetUniqueLine({'VP_LOCGRP'})
	oModel:GetModel('SVPDETAIL'):SetOptional(.T.)

	oModel:SetDescription( STR0001 )//"Produto X Grupo de Armaz�m"

	oModel:InstallEvent("PCPA130EVDEF", /*cOwner*/, oEvent)

Return oModel

/*/{Protheus.doc} ViewDef
Defini��o da View
@author Douglas Heydt
@since 03/07/2018
@version P12
@return oView
/*/
Static Function ViewDef()

	Local oStruSVP_M := FWFormStruct( 2, 'SVP' ,{|cCampo| AllTrim(cCampo) $ "VP_PRODUTO"})
	Local oStruSVP_D := FWFormStruct( 2, 'SVP')
	Local oView      := FWFormView():New()

	oView:SetModel(FWLoadModel('PCPA130'))

	oView:AddField('HEADER_SVP', oStruSVP_M, 'SVPMASTER')
	oView:AddGrid( 'VIEW_SVP'  , oStruSVP_D, 'SVPDETAIL')

	oView:CreateHorizontalBox( 'UPPER',  76, , .T. )
	oView:CreateHorizontalBox( 'LOWER', 100 )

	oView:SetOwnerView( 'HEADER_SVP', 'UPPER' )
	oView:SetOwnerView( 'VIEW_SVP'  , 'LOWER' )

	oStruSVP_M:AddField("B1_DESC"       , ;  // [01] C Nome do Campo
	                    "05"            , ;  // [02] C Ordem
	                    AllTrim(STR0008), ;  // [03] C Titulo do campo
	                    AllTrim(STR0008), ;  // [04] C Descri��o do campo
	                    {""}            , ;  // [05] A Array com Help
	                    'C'             , ;  // [06] C Tipo do campo
	                    ''              , ;  // [07] C Picture
	                    NIL             , ;  // [08] B Bloco de Picture Var
	                    ''              , ;  // [09] C Consulta F3
	                    .F.             , ;  // [10] L Indica se o campo � edit�vel
	                    NIL             , ;  // [11] C Pasta do campo
	                    NIL             , ;  // [12] C Agrupamento do campo
	                    NIL             , ;  // [13] A Lista de valores permitido do campo (Combo)
	                    NIL             , ;  // [14] N Tamanho M�ximo da maior op��o do combo
	                    NIL             , ;  // [15] C Inicializador de Browse
	                    .T.             , ;  // [16] L Indica se o campo � virtual
	                    NIL             )    // [17] C Picture Vari�vel

	oStruSVP_D:RemoveField("VP_PRODUTO")

Return oView

/*/{Protheus.doc} PCPA130Des()
Funcao para preencher os campos descri��o dos grupos de armaz�m no inicializador padr�o
@author Douglas Heydt
@since 03/07/2018
@version 1.0
@return cDescri
/*/
Function PCPA130Des()

	Local aArea   := GetArea()
	Local cCodSVP := SVP->VP_LOCGRP
	Local cDescri := ""

	If !Empty(cCodSVP)
		SVN->(dbSetOrder(1))
		If SVN->(dbSeek( xFilial("SVN")+cCodSVP))
			cDescri := Alltrim(SVN->VN_DESCGRU)
		EndIf
	EndIf

	RestArea(aArea)

Return cDescri

/*/{Protheus.doc} PCPA130Prd()
Funcao para preencher os campos descri��o dos grupos via trigger
@author Douglas Heydt
@since 09/07/2018
@version 1.0
@return cDescri
/*/
Function PCPA130Prd()

	Local aArea     := GetArea()
	Local oModel    := FWModelActive()
	Local oModelSVP := oModel:GetModel("SVPDETAIL")
	Local cCodSVP   := oModelSVP:GetValue("VP_LOCGRP")
	Local cDescri   := ""

	If !Empty(cCodSVP)
		SVN->(dbSetOrder(1))
		If SVN->(dbSeek( xFilial("SVN")+cCodSVP))
			cDescri := Alltrim(SVN->VN_DESCGRU)
		EndIf
	EndIf

	RestArea(aArea)

Return cDescri

/*/{Protheus.doc} pcpa130Ini()
Inicializador padr�o dos campos descri��o dos produtos
@author Douglas Heydt
@since 21/06/2018
@version 1.0
@return cDescri
/*/
Function pcpa130Ini()

	Local aArea   := GetArea()
	Local cDescri := ""

	SB1->(dbSetOrder(1))
	If SB1->(dbSeek( xFilial("SB1")+SB1->B1_COD))
		 cDescri := Alltrim(SB1->B1_DESC)
	EndIf

	RestArea(aArea)

Return cDescri