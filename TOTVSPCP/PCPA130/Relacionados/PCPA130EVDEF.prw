#INCLUDE "PROTHEUS.CH"
#INCLUDE "PCPA130.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "FWMVCDEF.CH"                 
#INCLUDE "FWADAPTEREAI.CH"
#include "TbIconn.ch"

/*/{Protheus.doc} PCPA130EVDEF
Eventos padr�o do cadastro de roteiros
@author Douglas Heydt
@since 02/07/2018
@version P12.1.17 
/*/

CLASS PCPA130EVDEF FROM FWModelEvent
	
	METHOD New() CONSTRUCTOR
	METHOD VldActivate()
	METHOD ModelPosVld()
	METHOD GridLinePreVld()

	
	
ENDCLASS

METHOD New() CLASS  PCPA130EVDEF

Return

/*/{Protheus.doc} VldActivate
M�todo chamado na valida��o de ativa��o do modelo
@author lucas.franca
@since 23/01/2019
@param oModel	- Modelo de dados principal
@param cModelID	- ID do modelo de dados
@return	lRet	- Identifica se o modelo poder� ser ativado ou n�o.
@version P12.1.17 
/*/
METHOD VldActivate(oModel, cModelID) CLASS PCPA130EVDEF
	Local lRet := .T.

	//Prote��o do fonte para n�o ser utilizado pelos clientes neste momento.
	If !(FindFunction("RodaNewPCP") .And. RodaNewPCP())
		HELP(' ',1,"Help" ,,STR0009,2,0,,,,,,) //"Rotina n�o dispon�vel nesta release."
		lRet := .F.
	EndIf
Return lRet 

METHOD GridLinePreVld(oSubModel, cModelID, nLine, cAction, cId, xValue, xCurrentValue) CLASS PCPA130EVDEF
	Local lRet := .T.
	Local aArea 	:= GetArea()
	
	If cModelId == "SVPDETAIL" .And. cAction == "DELETE"
		SVQ->(dbSetOrder(2))
		If SVQ->(dbSeek( xFilial("SVQ")+oSubModel:GetValue("VP_PRODUTO")+oSubModel:GetValue("VP_LOCGRP")))
			Help(NIL, NIL, STR0007, NIL, STR0006, 1, 0, NIL, NIL, NIL, NIL, NIL,NIL)//"A rela��o Produto X Grupo de armaz�m n�o pode ser eliminada pois existem registros para esse produto e grupo na malha de distribui��o"
			lRet := .F.
		EndIf
		SVQ->(dbSetOrder(3))
		If SVQ->(dbSeek( xFilial("SVQ")+oSubModel:GetValue("VP_PRODUTO")+oSubModel:GetValue("VP_LOCGRP")))
			Help(NIL, NIL, STR0007, NIL, STR0006, 1, 0, NIL, NIL, NIL, NIL, NIL,NIL)//"A rela��o Produto X Grupo de armaz�m n�o pode ser eliminada pois existem registros para esse produto e grupo na malha de distribui��o"
			lRet := .F.
		EndIf
	EndIf
	
Return lRet



METHOD ModelPosVld(oModel, cID) CLASS PCPA130EVDEF
	
	Local lRet := .T.
	Local oModelSVP := oModel:GetModel("SVPDETAIL")
	Local nX
	Local aArea 	:= GetArea()
	
	For nX := 1 To oModelSVP:Length()
		oModelSVP:GoLine(nX)
		If oModelSVP:isDeleted(nX)
			SVQ->(dbSetOrder(2))
			If SVQ->(dbSeek( xFilial("SVQ")+oModelSVP:GetValue("VP_PRODUTO")+oModelSVP:GetValue("VP_LOCGRP")))
				Help(NIL, NIL, STR0007, NIL, STR0006, 1, 0, NIL, NIL, NIL, NIL, NIL,NIL)//"A rela��o Produto X Grupo de armaz�m n�o pode ser eliminada pois existem registros para esse produto e grupo na malha de distribui��o"
				lRet := .F.
				Exit
			EndIf
			SVQ->(dbSetOrder(3))
			If SVQ->(dbSeek( xFilial("SVQ")+oModelSVP:GetValue("VP_PRODUTO")+oModelSVP:GetValue("VP_LOCGRP")))
				Help(NIL, NIL, STR0007, NIL, STR0006, 1, 0, NIL, NIL, NIL, NIL, NIL,NIL)
				lRet := .F.
				Exit
			EndIf
		EndIf
	Next nX
		
Return lRet





