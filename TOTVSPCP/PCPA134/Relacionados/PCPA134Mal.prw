#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "PCPA134.CH"
#INCLUDE "FWEDITPANEL.CH"

#DEFINE FOLDER_OK_F  "FOLDER5"
#DEFINE FOLDER_OK_A  "FOLDER6"
#DEFINE FOLDER_NOK_F "FOLDER7"
#DEFINE FOLDER_NOK_A "FOLDER8"

//Constantes das posi��es do array saTree
#DEFINE IND_ATREE_NODEID   1
#DEFINE IND_ATREE_TIPO     2
#DEFINE IND_ATREE_PROMPT   3
#DEFINE IND_ATREE_ESTOQUE  4
#DEFINE IND_ATREE_CONSUMO  5

Static soDbTree := NIL
Static saTree   := {}
Static scPictQt := PesqPictQt("B2_QATU")

/*/{Protheus.doc} PCPA134Mal
Abre uma tela com a consulta da malha de distribui��o do produto
@author Marcelo Neumann
@since 10/12/2018
@version 1.0
@param 01 oViewPai, object, objeto da View Principal
@return lRet, logical, identifica se a View ser� aberta
/*/
Function PCPA134Mal(oViewPai)

	Local oMdlMalha
	Local oModelPai := oViewPai:GetModel()
	Local cPai      := oModelPai:GetModel("GRID_ONDE"):GetValue("G1_COD")
	Local cComp     := oModelPai:GetModel("GRID_ONDE"):GetValue("G1_COMP")
	Local cTrt      := oModelPai:GetModel("GRID_ONDE"):GetValue("G1_TRT")
	Local cLocCons  := oModelPai:GetModel("GRID_ONDE"):GetValue("G1_LOCCONS")
	Local aAreaSG1  := SG1->(GetArea())
	Local aButtons   := { {.F.,Nil    },{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},;
	                      {.T.,STR0031},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil} } //"Fechar"
	Local lRet      := .T.

	If !Empty(cLocCons)
		oMdlMalha := FWLoadModel("PCPA134Mal")
		oMdlMalha:SetOperation(MODEL_OPERATION_INSERT)
		oMdlMalha:Activate()
		oMdlMalha:GetModel("FLD_MALHA"):LoadValue("G1_COD"    , cPai    )
		oMdlMalha:GetModel("FLD_MALHA"):LoadValue("G1_COMP"   , cComp   )
		oMdlMalha:GetModel("FLD_MALHA"):LoadValue("G1_TRT"    , cTrt    )
		oMdlMalha:GetModel("FLD_MALHA"):LoadValue("G1_LOCCONS", cLocCons)

		FWExecView(STR0023               , ; //Titulo da janela - "Malha de Distribui��o"
				   'PCPA134Mal'          , ; //Nome do programa-fonte
				   MODEL_OPERATION_INSERT, ; //Indica o c�digo de opera��o (usar� a do modelo oMdlMalha)
				   NIL                   , ; //Objeto da janela em que o View deve ser colocado
				   NIL                   , ; //Bloco de valida��o do fechamento da janela
				   NIL                   , ; //Bloco de valida��o do bot�o OK
				   55                    , ; //Percentual de redu��o da janela
				   aButtons              , ; //Bot�es que ser�o habilitados na janela
		           NIL                   , ; //Bloco de valida��o do bot�o Cancelar
				   NIL                   , ; //Identificador da op��o do menu
				   NIL                   , ; //Indica o relacionamento com os bot�es da tela
				   oMdlMalha)                //Model que ser� usado pelo View

		oMdlMalha:DeActivate()
		oMdlMalha:Destroy()
	Else
		Help( ,  , "Help", ,  STR0024,; //"Local de Estoque n�o informado."
		     1, 0, , , , , , {STR0025}) //"Selecione um produto com Local de Estoque."
		lRet := .F.
	EndIf

	SG1->(RestArea(aAreaSG1))

Return lRet

/*/{Protheus.doc} ModelDef
Defini��o do Modelo
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@return oModel
/*/
Static Function ModelDef()

	Local oModel   := MPFormModel():New('PCPA134Mal', {|oModel| SetModify(oModel)}, , , {|oModel| SetModify(oModel)})
	Local oStruCab := FWFormStruct(1, "SG1", {|cCampo| "|" + AllTrim(cCampo) + "|" $ "|G1_COD|G1_COMP|G1_TRT|G1_LOCCONS|"})

	//Altera os campos da estrutura
	AltStruMod(@oStruCab)

	//FLD_MALHA - Modelo do cabe�alho
	oModel:AddFields("FLD_MALHA", /*cOwner*/, oStruCab)
	oModel:GetModel("FLD_MALHA"):SetDescription(STR0023) //"Malha de Distribui��o"
	oModel:GetModel("FLD_MALHA"):SetOnlyQuery()
	oModel:GetModel("FLD_MALHA"):SetOnlyView()

	oModel:SetDescription(STR0023) //"Malha de Distribui��o"
	oModel:SetPrimaryKey({})

Return oModel

/*/{Protheus.doc} ViewDef
Defini��o da View
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@return oView
/*/
Static Function ViewDef()

	Local oView    := FWFormView():New()
	Local oStruCab := FWFormStruct(2, "SG1", {|cCampo| "|" + AllTrim(cCampo) + "|" $ "|G1_COD|G1_COMP|"})

	//Altera os campos da estrutura para a view
	AltStrView(@oStruCab)

	oView:SetModel(FWLoadModel("PCPA134Mal"))

	//V_FLD_MALHA - View do Cabe�alho
	oView:AddField("V_FLD_MALHA", oStruCab, "FLD_MALHA")

	//V_TREE_MALHA - View da Tree com a Malha de Distribui��o
	oView:AddOtherObject("V_TREE_MALHA", {|oPanel| CriaTree(oPanel)})

	//Divis�o da tela
	oView:CreateHorizontalBox("BOX_HEADER", 135, , .T.)
	oView:CreateHorizontalBox("BOX_TREE"  , 100)

	//Relaciona a SubView com o Box
	oView:SetOwnerView("V_FLD_MALHA" , 'BOX_HEADER')
	oView:SetOwnerView("V_TREE_MALHA", 'BOX_TREE')

	oView:SetViewProperty("V_FLD_MALHA", "SETLAYOUT", { FF_LAYOUT_HORZ_DESCR_TOP , 3 } )

	//Adiciona bot�es na tela
	oView:AddUserButton(STR0034, "", {|| BtnLegenda()}, STR0034, , , .T.) //"Legenda"

	//Fun��o chamada ap�s ativar a View (carrega a Tree)
	oView:SetAfterViewActivate({|oView| AfterView(oView)})

	//Fun��o chamada ap�s informar o campo Consumo
	oView:SetFieldAction("NQTDCONSUMO", {|oView, cIDView, cField, xValue| AttConsumo(oView, cIDView, cField, xValue)})

Return oView

/*/{Protheus.doc} AltStruMod
Edita os campos da estrutura do Model
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@param 01 oStruCab, object, estrutura do modelo FLD_MALHA
@return Nil
/*/
Static Function AltStruMod(oStruCab)

	//Adiciona novos campos
	oStruCab:AddField(RetTitle("B1_DESC")                  ,; // [01]  C   Titulo do campo
                      RetTitle("B1_DESC")                  ,; // [02]  C   ToolTip do campo
                      "CDESCCMP"                           ,; // [03]  C   Id do Field
                      "C"                                  ,; // [04]  C   Tipo do campo
                      GetSx3Cache("B1_DESC","X3_TAMANHO")  ,; // [05]  N   Tamanho do campo
                      0                                    ,; // [06]  N   Decimal do campo
					  NIL                                  ,; // [07]  B   Code-block de valida��o do campo
					  NIL                                  ,; // [08]  B   Code-block de valida��o When do campo
					  NIL                                  ,; // [09]  A   Lista de valores permitido do campo
                      .F.                                  ,; // [10]  L   Indica se o campo tem preenchimento obrigat�rio
					  NIL                                  ,; // [11]  B   Code-block de inicializacao do campo
					  NIL                                  ,; // [12]  L   Indica se trata-se de um campo chave
                      .T.                                  ,; // [13]  L   Indica se o campo n�o pode receber valor em uma opera��o de update
                      .T.)                                    // [14]  L   Indica se o campo � virtual

	oStruCab:AddField(STR0111                              ,; // [01]  C   Titulo do campo  - "Consumo"
	                  STR0111                              ,; // [02]  C   ToolTip do campo - "Consumo"
	                  "NQTDCONSUMO"                        ,; // [03]  C   Id do Field
	                  "N"                                  ,; // [04]  C   Tipo do campo
	                  GetSx3Cache("B2_QATU","X3_TAMANHO")  ,; // [05]  N   Tamanho do campo
	                  GetSx3Cache("B2_QATU","X3_DECIMAL")  ,; // [06]  N   Decimal do campo
					  NIL                                  ,; // [07]  B   Code-block de valida��o do campo
					  NIL                                  ,; // [08]  B   Code-block de valida��o When do campo
					  NIL                                  ,; // [09]  A   Lista de valores permitido do campo
                      .F.                                  ,; // [10]  L   Indica se o campo tem preenchimento obrigat�rio
					  NIL                                  ,; // [11]  B   Code-block de inicializacao do campo
					  NIL                                  ,; // [12]  L   Indica se trata-se de um campo chave
                      .F.                                  ,; // [13]  L   Indica se o campo n�o pode receber valor em uma opera��o de update
                      .T.)                                    // [14]  L   Indica se o campo � virtual

	oStruCab:AddField(RetTitle("B1_DESC")                  ,; // [01]  C   Titulo do campo
	                  RetTitle("B1_DESC")                  ,; // [02]  C   ToolTip do campo
	                  "CDESCPAI"                           ,; // [03]  C   Id do Field
	                  "C"                                  ,; // [04]  C   Tipo do campo
	                  GetSx3Cache("B1_DESC","X3_TAMANHO")  ,; // [05]  N   Tamanho do campo
	                  0                                    ,; // [06]  N   Decimal do campo
					  NIL                                  ,; // [07]  B   Code-block de valida��o do campo
					  NIL                                  ,; // [08]  B   Code-block de valida��o When do campo
					  NIL                                  ,; // [09]  A   Lista de valores permitido do campo
                      .F.                                  ,; // [10]  L   Indica se o campo tem preenchimento obrigat�rio
					  NIL                                  ,; // [11]  B   Code-block de inicializacao do campo
					  NIL                                  ,; // [12]  L   Indica se trata-se de um campo chave
                      .T.                                  ,; // [13]  L   Indica se o campo n�o pode receber valor em uma opera��o de update
                      .T.)                                    // [14]  L   Indica se o campo � virtual

	//Altera propriedades dos campos
	oStruCab:SetProperty("G1_COMP"   , MODEL_FIELD_OBRIGAT, .F.)
	oStruCab:SetProperty("G1_COD"    , MODEL_FIELD_INIT   , FWBuildFeature(STRUCT_FEATURE_INIPAD, " "))
	oStruCab:SetProperty("G1_COMP"   , MODEL_FIELD_INIT   , FWBuildFeature(STRUCT_FEATURE_INIPAD, " "))
	oStruCab:SetProperty("G1_TRT"    , MODEL_FIELD_INIT   , FWBuildFeature(STRUCT_FEATURE_INIPAD, " "))
	oStruCab:SetProperty("G1_LOCCONS", MODEL_FIELD_INIT   , FWBuildFeature(STRUCT_FEATURE_INIPAD, " "))
 	oStruCab:SetProperty("G1_COD"    , MODEL_FIELD_VALID  , FWBuildFeature(STRUCT_FEATURE_VALID , " "))
	oStruCab:SetProperty("G1_COMP"   , MODEL_FIELD_VALID  , FWBuildFeature(STRUCT_FEATURE_VALID , " "))
	oStruCab:SetProperty("G1_TRT"    , MODEL_FIELD_VALID  , FWBuildFeature(STRUCT_FEATURE_VALID , " "))
	oStruCab:SetProperty("G1_LOCCONS", MODEL_FIELD_VALID  , FWBuildFeature(STRUCT_FEATURE_VALID , " "))

Return Nil

/*/{Protheus.doc} AltStrView
Edita os campos da estrutura da View
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@param 01 oStruCab, object, estrutura da View V_FLD_MALHA
@return Nil
/*/
Static Function AltStrView(oStruCab)

	//Adiciona novos campos
	oStruCab:AddField("CDESCCMP"                 ,; // [01]  C   Nome do Campo
	                  "2"                        ,; // [02]  C   Ordem
	                  STR0007                    ,; // [03]  C   Titulo do campo - "Descri��o"
	                  STR0007                    ,; // [04]  C   Descricao do campo - "Descri��o"
	                  NIL                        ,; // [05]  A   Array com Help
	                  "C"                        ,; // [06]  C   Tipo do campo
	                  "@S20"                     ,;	// [07]  C   Picture
					  NIL                        ,; // [08]  B   Bloco de Picture Var
					  NIL                        ,; // [09]  C   Consulta F3
					  .F.                        ,; // [10]  L   Indica se o campo � alteravel
					  NIL                        ,; // [11]  C   Pasta do campo
					  NIL                        ,; // [12]  C   Agrupamento do campo
					  NIL                        ,; // [13]  A   Lista de valores permitido do campo (Combo)
					  NIL                        ,; // [14]  N   Tamanho maximo da maior op��o do combo
					  NIL                        ,; // [15]  C   Inicializador de Browse
					  .T.                        ,; // [16]  L   Indica se o campo � virtual
					  NIL                        ,; // [17]  C   Picture Variavel
					  NIL                        )  // [18]  L   Indica pulo de linha ap�s o campo

	oStruCab:AddField("NQTDCONSUMO"              ,; // [01]  C   Nome do Campo
	                  "3"                        ,; // [02]  C   Ordem
	                  STR0111                    ,; // [03]  C   Titulo do campo - "Consumo"
	                  STR0111                    ,; // [04]  C   Descricao do campo - "Consumo"
	                  NIL                        ,; // [05]  A   Array com Help
	                  "N"                        ,; // [06]  C   Tipo do campo
	                  PesqPict('SB2','B2_QATU')  ,; // [07]  C   Picture
					  NIL                        ,; // [08]  B   Bloco de Picture Var
					  NIL                        ,; // [09]  C   Consulta F3
					  .T.                        ,; // [10]  L   Indica se o campo � alteravel
					  NIL                        ,; // [11]  C   Pasta do campo
					  NIL                        ,; // [12]  C   Agrupamento do campo
					  NIL                        ,; // [13]  A   Lista de valores permitido do campo (Combo)
					  NIL                        ,; // [14]  N   Tamanho maximo da maior op��o do combo
					  NIL                        ,; // [15]  C   Inicializador de Browse
					  .T.                        ,; // [16]  L   Indica se o campo � virtual
					  NIL                        ,; // [17]  C   Picture Variavel
					  NIL                        )  // [18]  L   Indica pulo de linha ap�s o campo

	oStruCab:AddField("CDESCPAI"                 ,; // [01]  C   Nome do Campo
	                  "5"                        ,; // [02]  C   Ordem
	                  STR0007                    ,; // [03]  C   Titulo do campo - "Descri��o"
	                  STR0007                    ,; // [04]  C   Descricao do campo - "Descri��o"
	                  NIL                        ,; // [05]  A   Array com Help
	                  "C"                        ,; // [06]  C   Tipo do campo
	                  "@S20"                     ,;	// [07]  C   Picture
					  NIL                        ,; // [08]  B   Bloco de Picture Var
					  NIL                        ,; // [09]  C   Consulta F3
					  .F.                        ,; // [10]  L   Indica se o campo � alteravel
					  NIL                        ,; // [11]  C   Pasta do campo
					  NIL                        ,; // [12]  C   Agrupamento do campo
					  NIL                        ,; // [13]  A   Lista de valores permitido do campo (Combo)
					  NIL                        ,; // [14]  N   Tamanho maximo da maior op��o do combo
					  NIL                        ,; // [15]  C   Inicializador de Browse
					  .T.                        ,; // [16]  L   Indica se o campo � virtual
					  NIL                        ,; // [17]  C   Picture Variavel
					  NIL                        )  // [18]  L   Indica pulo de linha ap�s o campo

	//Altera propriedades dos campos
	oStruCab:SetProperty("G1_COMP" , MVC_VIEW_ORDEM     , "1")
	oStruCab:SetProperty("G1_COMP" , MVC_VIEW_CANCHANGE , .F.)
	oStruCab:SetProperty("G1_COMP" , MVC_VIEW_LOOKUP    , Nil) //Remove a Lupa de pesquisa
	If GetSx3Cache("G1_COMP", "X3_TAMANHO") > 15
		oStruCab:SetProperty("G1_COMP", MVC_VIEW_PICT, "@S10")
	EndIf

	oStruCab:SetProperty("NQTDCONSUMO", MVC_VIEW_INSERTLINE, .T.) //Quebra linha

	oStruCab:SetProperty("G1_COD"  , MVC_VIEW_TITULO    , STR0006) //"Produto"
	oStruCab:SetProperty("G1_COD"  , MVC_VIEW_ORDEM     , "4")
	oStruCab:SetProperty("G1_COD"  , MVC_VIEW_CANCHANGE , .F.)
	oStruCab:SetProperty("G1_COD"  , MVC_VIEW_LOOKUP    , Nil)
	If GetSx3Cache("G1_COD", "X3_TAMANHO") > 15
		oStruCab:SetProperty("G1_COD", MVC_VIEW_PICT, "@S10")
	EndIf

Return Nil

/*/{Protheus.doc} CriaTree
Cria o objeto da Tree no painel
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@param 01 oPanel, object, objeto do painel onde ser� criada a Tree
@return Nil
/*/
Static Function CriaTree(oPanel)

	Local cHeader := PadR(STR0110, 120) + ";" + ; //"Malha"
	                 PadR(STR0113,  20) + ";" + ; //"Estoque"
	                 PadR(STR0111,  20) + ";" + ; //"Consumo"
	                 PadR(STR0112,  20) + ""    //"Saldo"

	//Cria a Tree
	soDbTree := DbTree():New(0, 0, 100, 100, oPanel, {|| }, , .F.,,, cHeader)
	soDbTree:Align := CONTROL_ALIGN_ALLCLIENT

	//Alinha as colunas 2 (Estoque), 3 (Consumo) e 4 (Saldo) � direita (descomentar ap�s corre��o da issue MTEC-2504)
	//soDbTree:SetColsAlign({ {2, 3} , {3, 3} , {4, 3} })

	If saTree == NIL
		saTree := {}
	Else
		aSize(saTree,0)
	EndIf

Return Nil

/*/{Protheus.doc} DescrProd
Busca descri��o do Produto
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@param 01 cProd, characters, c�digo do produto para fazer a busca na SB1
@return cDesc, characters, descri��o do produto
/*/
Static Function DescrProd(cProd)

	Local aAreaSB1 := SB1->(GetArea())
	Local cDesc    := ""

	SB1->(dbSetOrder(1))
	If SB1->(MsSeek(xFilial("SB1") + cProd))
		cDesc := SB1->B1_DESC
	EndIf

	SB1->(RestArea(aAreaSB1))

Return AllTrim(cDesc)

/*/{Protheus.doc} DescrArm
Busca descri��o do Armaz�m
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@param 01 cCodFilial, characters, c�digo da filial do armaz�m
@param 02 cArmazem  , characters, c�digo do armaz�m para fazer a busca na NNR
@return cDesc, characters, descri��o do armaz�m
/*/
Static Function DescrArm(cCodFilial, cArmazem)

	Local aAreaNNR := NNR->(GetArea())
	Local cDesc    := ""

	NNR->(dbSetOrder(1))
	If NNR->(MsSeek(cCodFilial + cArmazem))
		cDesc := NNR->NNR_DESCRI
	EndIf

	NNR->(RestArea(aAreaNNR))

Return AllTrim(cDesc)

/*/{Protheus.doc} AfterView
Fun��o executada ap�s ativar a view
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@param 01 oView, object, objeto da View
@return Nil
/*/
Static Function AfterView(oView)

	Local oModel := oView:GetModel()
	Local cProd  := oModel:GetModel("FLD_MALHA"):GetValue("G1_COD" )
	Local cComp  := oModel:GetModel("FLD_MALHA"):GetValue("G1_COMP")
	Local cLocal := oModel:GetModel("FLD_MALHA"):GetValue("G1_LOCCONS")

	//Carrega as descri��es
	oModel:GetModel("FLD_MALHA"):LoadValue("CDESCCMP", DescrProd(cComp))
	oModel:GetModel("FLD_MALHA"):LoadValue("CDESCPAI", DescrProd(cProd))

	//Insere os dados na Tree
	MontaTree(cComp, cLocal)

	//Atualiza a view
	oView:Refresh()
	soDbTree:SetFocus()

	SetModify(oModel)

Return Nil

/*/{Protheus.doc} MontaTree
Insere os dados da Malha de Distribui��o na Tree
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@param 01 cComp   , characters, c�digo do Componente a ser pesquisada a Malha
@param 02 cLocCons, characters, c�digo do Local de Consumo do Componente
@return Nil
/*/
Static Function MontaTree(cComp, cLocCons)

	Local aAreaSVO   := SVO->(GetArea())
	Local aAreaSVN   := SVN->(GetArea())
	Local aAreaSVD   := SVD->(GetArea())
	Local cDesLocCon := DescrArm(xFilial("NNR"), cLocCons)
	Local cPrompt    := ""
	Local aMalha     := PCPXMDLOC(cComp              , ;
	                              cLocCons           , ;
				    			  .T. /*lRetLocais*/ , ; //Carregar a posi��o 7 do array com todos os Locais dos Grupos de Armaz�ns (Origem)
				    			  .F. /*lFiltraExc*/ , ; //N�o filtra as exce��es com a Data (dDtDemanda) e Quantidade (nQuant)
				    			      /*dDtDemanda*/ , ;
				    			      /*nQuant*/     , ;
				    			  1   /*nPesqSaldo*/ )   //Pesquisa o saldo de todos os locais

	If soDbTree == NIL
		Return
	EndIf

	soDbTree:BeginUpdate()
	//Se n�o h� malha cadastrada ou se o componente � exce��o na malha
	If Empty(aMalha)
		cPrompt := STR0026 + AllTrim(cLocCons) + " (" + AllTrim(cDesLocCon) + ")" //"N�o h� Malha de Distribui��o cadastrada para o Local de Consumo "
		soDbTree:AddTreeItem(cPrompt)
	ElseIf aMalha[1][8] == 2
		cPrompt := STR0035 + AllTrim(cLocCons) + " (" + AllTrim(cDesLocCon) + ")" //"O componente est� cadastrado como exce��o na Malha de Distribui��o do Local de Consumo "
		soDbTree:AddTreeItem(cPrompt)
	Else
		cPrompt := STR0027 + ": " + AllTrim(cLocCons) + " - " + AllTrim(cDesLocCon) //"Local de Consumo"
		cPrompt := PadR(cPrompt, 100)
		soDbTree:AddTree(cPrompt, .T., FOLDER_OK_F, FOLDER_OK_A)
		aAdd(saTree, {soDbTree:CurrentNodeId, "PAI", cPrompt, 0, NIL})

		//Adiciona na Tree a Malha de Distribui��o carregada com a fun��o PCPXMDLOC()
		AddMalha(aMalha, cComp)
	EndIf
	soDbTree:EndTree()
	soDbTree:EndUpdate()
	soDbTree:Refresh()

	SVO->(RestArea(aAreaSVO))
	SVN->(RestArea(aAreaSVN))
	SVD->(RestArea(aAreaSVD))

Return Nil

/*/{Protheus.doc} AddMalha
Adiciona os Grupos de Armaz�m na Tree de acordo com a Malha de Distribui��o
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@param 01 aMalha, array     , array carregado atrav�s da fun��o PCPXMDLOC()
@param 02 cComp , characters, c�digo do componente
@return Nil
/*/
Static Function AddMalha(aMalha, cComp)

	Local nIndGrp    := 0
	Local cFilialDes := aMalha[1][3]
	Local cGrupoDes  := aMalha[1][2]
	Local cIdLocPai  := soDbTree:CurrentNodeId
	Local cPrompt    := ""
	Local aIdCollaps := {}
	Local nEstoqTot  := 0
	Local nEstoqGrp  := 0
	Local nPosArTree := 1

	//Adiciona o Grupo de Armaz�m atual na Tree (Destino)
	cPrompt := STR0028 + ": " + AllTrim(cGrupoDes) //"Grupo de Armaz�m"
	soDbTree:AddItem(cPrompt, , FOLDER_OK_F, FOLDER_OK_A, , , 2)
	soDbTree:PTGotoToNode(LastNode())
	aAdd(saTree, {soDbTree:CurrentNodeId, "GRUPO", cPrompt, 0, NIL})
	aAdd(aIdCollaps, soDbTree:CurrentNodeId)

	//Adiciona os armaz�ns na Tree
	nEstoqGrp := AddLocais(cFilialDes, PCPXRetLoc(cFilialDes, cGrupoDes, 1, cComp, 0))
	nEstoqTot := nEstoqGrp
	aAdd(aIdCollaps, soDbTree:CurrentNodeId)
	SumEstoq(2, nEstoqGrp)

	//Percorre o array carregado pela fun��o PCPXMDLOC()
	For nIndGrp := 1 To Len(aMalha)
		//Se a filial do Grupo Origem for diferente da corrente, informa a filial na Tree
		If !Empty(aMalha[nIndGrp][5]) .And. aMalha[nIndGrp][5] <> cFilAnt
			cPrompt := STR0030 + " "  + AllTrim(aMalha[nIndGrp][5]) + " - " + ; //"Filial"
			           STR0028 + ": " + AllTrim(aMalha[nIndGrp][4])             //"Grupo de Armaz�m"
		Else
			cPrompt := STR0028 + ": " + AllTrim(aMalha[nIndGrp][4])  //"Grupo de Armaz�m"
		EndIf

		//Verifica a Exce��o para adicionar o Grupo de Armaz�m na Tree (Origem)
		If aMalha[nIndGrp][8] == 0
			//0 - N�o considera
			soDbTree:AddItem(cPrompt, , FOLDER_OK_F, FOLDER_OK_A, , , 2)
		ElseIf aMalha[nIndGrp][8] == 1
			//1 - A exce��o depende da data/quantidade
			soDbTree:AddItem(cPrompt, , FOLDER_NOK_F, FOLDER_NOK_A, , , 2)
		Else
			//2 - A exce��o n�o depende da data/quantidade
			Exit
		EndIf
		soDbTree:PTGotoToNode(LastNode())
		aAdd(saTree, {soDbTree:CurrentNodeId, "GRUPO", cPrompt, 0, NIL})
		aAdd(aIdCollaps, soDbTree:CurrentNodeId)

		//Adiciona os armaz�ns na Tree
		nPosArTree := Len(saTree)
		nEstoqGrp  := AddLocais(aMalha[nIndGrp][5], aMalha[nIndGrp][7])
		nEstoqTot  += nEstoqGrp
		aAdd(aIdCollaps, soDbTree:CurrentNodeId)
		SumEstoq(nPosArTree, nEstoqGrp)
	Next nIndGrp

	//Percorre a Tree (fim->in�cio) para fechar os n�veis
	For nIndGrp := Len(aIdCollaps) To 1 Step -1
		soDbTree:PTGotoToNode(aIdCollaps[nIndGrp])
		soDbTree:PTCollapse()
	Next nIndGrp

	SumEstoq(1, nEstoqTot)

	//Foca no primeiro item da Tree
	soDbTree:PTGotoToNode(cIdLocPai)

Return Nil

/*/{Protheus.doc} AddLocais
Adiciona os Armaz�ns do Grupo de Armaz�m na Tree
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@param 01 cFilGrupo, characters, c�digo da Filial do Armaz�m
@param 02 aLocais  , array     , array com os Armaz�ns do Grupo:
                                 aLocais[1]        := Tipo do Grupo ('1'-Filial
								                                     '2'-Grupo de Estoque)
                                 aLocais[2][nX][1] := Local
								 aLocais[2][nX][2] := Default
								 aLocais[2][nX][3] := Saldo em estoque
@return nEstoqGrp, numeric, soma do saldo em estoque dos armaz�ns
/*/
Static Function AddLocais(cFilGrupo, aLocais)

	Local cIdGrupo  := soDbTree:CurrentNodeId
	Local cPrompt   := ""
	Local nInd      := 0
	Local nEstoqGrp := 0

	//Adiciona a pasta "Armaz�ns" na Tree
	soDbTree:AddItem(STR0029, , FOLDER_OK_F, FOLDER_OK_A, , , 2) //"Armaz�ns"
	soDbTree:PTGotoToNode(LastNode())
	aAdd(saTree, {LastNode(), "ARM_MST", STR0029})

	//Adiciona os armaz�ns dentro da pasta
	For nInd := 1 To Len(aLocais[2])
		cPrompt := AllTrim(aLocais[2][nInd][1]) + " - " + DescrArm(cFilGrupo, aLocais[2][nInd][1]) + ";" + Transform(aLocais[2][nInd][3], scPictQt)
		soDbTree:AddItem(cPrompt, , FOLDER_OK_F, FOLDER_OK_A, , , 2)
		aAdd(saTree, {LastNode(), "ARM_DET", cPrompt, aLocais[2][nInd][3], NIL})
		nEstoqGrp += aLocais[2][nInd][3]
	Next nInd
	soDbTree:PTGotoToNode(cIdGrupo)

Return nEstoqGrp

/*/{Protheus.doc} LastNode
Retorna o ID do �ltimo n� da Tree
@author Marcelo Neumann
@since 11/12/2018
@version 1.0
@return cLastNode, characters, quantidade de n�s da tree formatado com "0" � esquerda
/*/
Static Function LastNode()

Return PadL(soDbTree:PTGetNodeCount(), Len(soDbTree:CurrentNodeId), "0")

/*/{Protheus.doc} BtnLegenda
Bot�o de legenda da tela
@author Marcelo Neumann
@since 26/12/2018
@version 1.0
@param 01 oView, object, objeto da view ativa
@return Nil
/*/
Static Function BtnLegenda()

	Local oDlg
	Local oBmp1
	Local oBmp2
	Local oBut1

	DEFINE MSDIALOG oDlg TITLE STR0034 OF oMainWnd PIXEL FROM 0,0 TO 202,360 //"Legenda"
	@ 003, 003 TO 080,178 LABEL STR0034 PIXEL //"Legenda"
	@ 016, 010 BITMAP oBmp1 RESNAME FOLDER_OK_F SIZE 16,16 NOBORDER PIXEL
	@ 019, 024 SAY OemToAnsi(STR0032) OF oDlg PIXEL //"Componente n�o � exce��o."
	@ 030, 010 BITMAP oBmp2 RESNAME FOLDER_NOK_F SIZE 16,16 NOBORDER PIXEL
	@ 033, 024 SAY OemToAnsi(STR0033) OF oDlg PIXEL //"Componente pode ser exce��o."

	DEFINE SBUTTON oBut1 FROM 085, 152 TYPE 1 ACTION (oDlg:End()) ENABLE of oDlg

	ACTIVATE MSDIALOG oDlg CENTERED

Return Nil

/*/{Protheus.doc} SetModify
Seta o indicador de modificado do modelo para Falso
@author Marcelo Neumann
@since 18/03/2019
@version P12
@param oModel, object, modelo a ser setado o lModify para .F.
@return .T.
/*/
Static Function SetModify(oModel)

	oModel:lModify := .F.

Return .T.

/*/{Protheus.doc} AttConsumo
Atualiza a coluna Consumo da tree
@author Marcelo Neumann
@since 18/03/2019
@version P12
@param 01 oView   , object    , objeto da View
@param 02 cIDView , characters, ID da View
@param 03 cField  , characters, nome do campo que perdeu o foco
@param 04 nQtdCons, characters, quantidade informada no campo Consumo
@return Nil
/*/
Static Function AttConsumo(oView, cIDView, cField, nQtdCons)

	Local nConsumido := 0
	Local cPrompt    := ""
	Local nInd       := 0
	Local nQtdConGrp := nQtdCons

	SetModify(oView:GetModel())

	//Se o usu�rio informou Quantidade negativa, considera como zero
	If nQtdCons < 0
		nQtdCons   := 0
		nQtdConGrp := 0
	EndIf

	//Atualiza a linha 1 da tree
	cPrompt := saTree[1][IND_ATREE_PROMPT]   + ";" + ;
	           Transform(nQtdCons, scPictQt) + ";" + ;
			   Transform(saTree[1][IND_ATREE_ESTOQUE] - nQtdCons, scPictQt)
	soDbTree:PTChangePrompt(cPrompt, saTree[1][IND_ATREE_NODEID])

	//Percorre o array com dados da Tree
	For nInd := 1 To Len(saTree)
		//Verifica se a posi��o da tree corresponde ao Grupo de Armaz�m
		If saTree[nInd][IND_ATREE_TIPO] == "GRUPO"
			//Desconta o saldo em estoque do armaz�m da Quantidade de Consumo
			If saTree[nInd][IND_ATREE_ESTOQUE] > 0 .And. nQtdConGrp > 0
				If saTree[nInd][IND_ATREE_ESTOQUE] < nQtdCons
					nQtdConGrp -= saTree[nInd][IND_ATREE_ESTOQUE]
					nConsumido := saTree[nInd][IND_ATREE_ESTOQUE]
				Else
					nConsumido := nQtdConGrp
					nQtdConGrp := 0
				EndIf
			EndIf

			//S� atualiza se necess�rio
			If Empty(saTree[nInd][IND_ATREE_CONSUMO]) .Or. saTree[nInd][IND_ATREE_CONSUMO] <> nConsumido
				//Adiciona o estoque utilizado
				cPrompt := saTree[nInd][IND_ATREE_PROMPT] + ";" + Transform(nConsumido, scPictQt)
				soDbTree:PTChangePrompt(cPrompt, saTree[nInd][IND_ATREE_NODEID])
				nConsumido := 0
			EndIf

		//Verifica se a posi��o da tree corresponde ao Armaz�m
		ElseIf saTree[nInd][IND_ATREE_TIPO] == "ARM_DET"
			//Desconta o estoque do armaz�m da Quantidade de Consumo
			If saTree[nInd][IND_ATREE_ESTOQUE] > 0 .And. nQtdCons > 0
				If saTree[nInd][IND_ATREE_ESTOQUE] < nQtdCons
					nQtdCons   -= saTree[nInd][IND_ATREE_ESTOQUE]
					nConsumido := saTree[nInd][IND_ATREE_ESTOQUE]
				Else
					nConsumido := nQtdCons
					nQtdCons   := 0
				EndIf
			EndIf

			//S� atualiza se necess�rio
			If Empty(saTree[nInd][IND_ATREE_CONSUMO]) .Or. saTree[nInd][IND_ATREE_CONSUMO] <> nConsumido
				//Adiciona o estoque utilizado
				cPrompt := saTree[nInd][IND_ATREE_PROMPT] + ";" + Transform(nConsumido, scPictQt)
				soDbTree:PTChangePrompt(cPrompt, saTree[nInd][IND_ATREE_NODEID])
				nConsumido := 0
			EndIf
		EndIf
	Next nInd

	SetModify(oView:GetModel())

Return

/*/{Protheus.doc} SumEstoq
Atualiza a coluna Estoque da tree
@author Marcelo Neumann
@since 18/03/2019
@version P12
@param 01 nIndTree, numeric, posi��o no array aTree do registro a ser alterado
@param 02 nEstoq  , numeric, saldo em estoque a ser adicionado na coluna Estoque
@return Nil
/*/
Static Function SumEstoq(nIndTree, nEstoq)

	cPrompt := saTree[nIndTree][IND_ATREE_PROMPT] + ";" + Transform(nEstoq, scPictQt)
	soDbTree:PTChangePrompt(cPrompt, saTree[nIndTree][IND_ATREE_NODEID])
	saTree[nIndTree][IND_ATREE_PROMPT]  := cPrompt
	saTree[nIndTree][IND_ATREE_ESTOQUE] := nEstoq

Return