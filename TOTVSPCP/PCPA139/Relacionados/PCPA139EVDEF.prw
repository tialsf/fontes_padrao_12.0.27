#INCLUDE "TOTVS.CH"
#INCLUDE "PCPA139.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "FWMVCDEF.CH"
#include "TbIconn.ch"

/*/{Protheus.doc} PCPA139EVDEF
Eventos padr�es do cadastro de roteiros
@author Douglas Heydt
@since 25/04/2018
@version P12.1.17
/*/

CLASS PCPA139EVDEF FROM FWModelEvent

	DATA aQryCompl AS Array

	METHOD New() CONSTRUCTOR
	METHOD GridLinePreVld(oSubModel, cModelID)
	METHOD InTTS(oModel, cModelId)
	METHOD ModelPosVld(oModel, cModelId)
	METHOD Before(oModel, cModelId)

ENDCLASS

METHOD New() CLASS  PCPA139EVDEF
	::aQryCompl := {}
Return

/*/{Protheus.doc} GridLinePreVld
Pr�-valida��o dos modelos
@author Douglas Heydt
@since 19/05/2019
@version 1.0

@param oSubModel	- Modelo de dados
@param cModelId		- ID do modelo de dados
@param nLine		- Linha do grid
@param cAction		- A��o que est� sendo realizada no grid, podendo ser: ADDLINE, UNDELETE, DELETE, SETVALUE, CANSETVALUE, ISENABLE
@param cId			- Nome do campo
@param xValue		- Novo valor do campo
@param xCurrentValue- Valor atual do campo
@return lRet		- Indica se a linha est� v�lida
/*/
METHOD GridLinePreVld(oSubModel, cModelID, nLine, cAction, cId, xValue, xCurrentValue) CLASS PCPA139EVDEF

    Local lRet := .T.

    If cModelID == "T4PDETAIL"
        If cAction == "CANSETVALUE" .And. cId == "T4P_ATIVO"
            lRet := .F.

        ElseIf cAction == "SETVALUE" .And. cId == "T4P_TPEXEC"
			If AllTrim(oSubModel:GetValue("T4P_API", nLine)) $ "|MRPPURCHASEORDER|MRPPURCHASEREQUEST|MRPSTOCKBALANCE|MRPALLOCATIONS|MRPWAREHOUSE|" .And. xValue == "1"
	            Help(' ',1,"Help" ,,STR0014 + AllTrim(oSubModel:GetValue("T4P_API", nLine)) + STR0015,; //"A API 'X' n�o pode ser configurada como online."
			         2,0,,,,,, {STR0016}) //"Utilize a configura��o schedule."
            	lRet := .F.
			ElseIf AllTrim(oSubModel:GetValue("T4P_API", nLine)) $ "|MRPBILLOFMATERIAL|MRPCALENDAR|" .And. xValue == "2"
				Help(' ',1,"Help" ,,STR0014 + AllTrim(oSubModel:GetValue("T4P_API", nLine)) + STR0022,; //"A API 'X' n�o pode ser configurada como schedule."
			         2,0,,,,,, {STR0023}) //"Utilize a configura��o online."
            	lRet := .F.
			ElseIf Empty(xValue)
            	Help(' ',1,"Help" ,,STR0017,; //"Tipo de execu��o inv�lido!"
			         2,0,,,,,, {STR0018}) //"Selecione um tipo de execu��o v�lido: online ou schedule."
            	lRet := .F.
			EndIf
        EndIf
    EndIf

Return lRet

/*/{Protheus.doc} InTTS
M�todo que � chamado pelo MVC quando ocorrer as a��es do commit Ap�s as grava��es por�m
antes do final da transa��o.
Esse evento ocorre uma vez no contexto do modelo principal.

@author brunno.costa
@since 08/08/2019
@version 1.0

@param oModel	- Modelo principal
@param cModelId	- Id do submodelo
@return Nil
/*/
METHOD InTTS(oModel, cModelId) CLASS PCPA139EVDEF

	Local nIndex := 0
	Local nTotal := Len(::aQryCompl)

	//Executa comandos complementares oriundos da valida��o MRPVldTrig
	For nIndex := 1 to nTotal
		TcSqlExec(::aQryCompl[nIndex])
	Next
	aSize(::aQryCompl, 0)

	//Atualiza flag de controle da necessidade de sincroniza��o de API's
	If IntNewMRP("MRPDEMANDS")
		TcSqlExec("UPDATE " + RetSqlName("T4P") + " SET T4P_ALTER = '1' WHERE D_E_L_E_T_ = ' ' AND T4P_ALTER = ' '")
	Else
		TcSqlExec("UPDATE " + RetSqlName("T4P") + " SET T4P_ALTER = ' ' WHERE D_E_L_E_T_ = ' ' ")
	EndIf

Return

/*/{Protheus.doc} ModelPosVld
M�todo que � chamado pelo MVC quando ocorrer as a��es de p�s-valida��o do Model
Esse evento ocorre uma vez no contexto do modelo principal

@author brunno.costa
@since 13/08/2019
@version P12
@param oModel  , object    , modelo principal
@param cModelId, characters, ID do submodelo de dados
@return Nil
/*/
METHOD ModelPosVld(oModel, cModelId) CLASS PCPA139EVDEF

	Local lRet       := .T.
	Local lAtivo     := .F.
	Local oMdlHWL    := oModel:GetModel("HWLMASTER")
	Local oMdlT4P    := oModel:GetModel("T4PDETAIL")

	If oMdlHWL != Nil 
		lAtivo := oMdlHWL:GetValue("HWL_ATIVO") == "1"
	ElseIf oMdlT4P != Nil 
		lAtivo := oMdlT4P:GetValue("T4P_ATIVO", 1) == "1"
	EndIf

	If lAtivo 
		lRet := VldTblComp()
		If !lRet
			Return lRet
		EndIf		
	EndIf
	
	::aQryCompl := IIf(::aQryCompl == Nil, {}, ::aQryCompl)

	If !MRPVldTrig(.T.,, .T., oModel, .T., @::aQryCompl)
		lRet := .F.
		MRPVldTrig(.F.,, .T., Nil, .F.)
	EndIf

Return lRet

/*/{Protheus.doc} Before
M�todo que � chamado pelo MVC quando ocorrer as a��es do commit
antes da grava��o de cada submodelo (field ou cada linha de uma grid)

@author renan.roeder
@since 13/03/2020
@version P12
@param oModel  , object    , Sub modelo
@param cModelId, characters, Id do submodelo
@return Nil
/*/
METHOD Before(oModel, cModelId) CLASS PCPA139EVDEF
	Local cTpExe   := "1"
	Local cAtivo   := "1"
	Local lRet     := .T.
	Local nIndex   := 0
	Local oMdlGrid := oModel

	If cModelID == "T4PDETAIL"
		//Atualiza MRPPRODUCTINDICATOR conforme MRPPRODUCT
		For nIndex := 1 to oMdlGrid:Length(.F.)
			If AllTrim(oMdlGrid:GetValue("T4P_API", nIndex)) == "MRPPRODUCT"
				cTpExe := oMdlGrid:GetValue("T4P_TPEXEC", nIndex)
				cAtivo := oMdlGrid:GetValue("T4P_ATIVO" , nIndex)
				Exit
			EndIf
		Next
		TcSqlExec("UPDATE " + RetSqlName("T4P") + " SET T4P_TPEXEC = '" + cTpExe + "', T4P_ATIVO = '" + cAtivo + "' WHERE T4P_API = 'MRPPRODUCTINDICATOR' ")

		//Atualiza MRPBOMROUTING conforme MRPBILLOFMATERIAL
		For nIndex := 1 to oMdlGrid:Length(.F.)
			If AllTrim(oMdlGrid:GetValue("T4P_API", nIndex)) == "MRPBILLOFMATERIAL"
				cTpExe := oMdlGrid:GetValue("T4P_TPEXEC", nIndex)
				cAtivo := oMdlGrid:GetValue("T4P_ATIVO" , nIndex)
				Exit
			EndIf
		Next
		TcSqlExec("UPDATE " + RetSqlName("T4P") + " SET T4P_TPEXEC = '" + cTpExe + "', T4P_ATIVO = '" + cAtivo + "' WHERE T4P_API = 'MRPBOMROUTING' ")
	EndIf

Return lRet
