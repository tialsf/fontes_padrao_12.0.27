#INCLUDE "TOTVS.CH"
#Include "PCPA140.CH"

#DEFINE IND_TELA_PRODUTOS     1
#DEFINE IND_TELA_INDICA_PROD  2
#DEFINE IND_TELA_ESTRUTURA    3
#DEFINE IND_TELA_VERSAO_PROD  4
#DEFINE IND_TELA_OPER_COMPON  5
#DEFINE IND_TELA_CALENDARIOS  6
#DEFINE IND_TELA_DEMANDAS     7
#DEFINE IND_TELA_ORDEM_PROD   8
#DEFINE IND_TELA_EMPENHOS     9
#DEFINE IND_TELA_SOL_COMPRAS  10
#DEFINE IND_TELA_PED_COMPRAS  11
#DEFINE IND_TELA_ESTOQUES     12
#DEFINE IND_TELA_CQ           13
#DEFINE IND_TELA_ARMAZEM      14
#DEFINE IND_TELA_MALHA_DISTR  15
#DEFINE IND_TELA_GRUPO_ARM    16

#DEFINE IND_PAR_CHECKED       1
#DEFINE IND_PAR_ALIAS         2
#DEFINE IND_PAR_QUERY         3
#DEFINE IND_PAR_QTD_TOTAL     4
#DEFINE IND_PAR_QTD_SUCESSO   5
#DEFINE IND_PAR_QTD_ERRO      6
#DEFINE IND_PAR_DESCRICAO     7
#DEFINE IND_PAR_API           8
#DEFINE IND_PAR_PROCESSADO    9
#DEFINE IND_PAR_VISIVEL      10
#DEFINE IND_PAR_MSG_ERRO     11
#DEFINE IND_PAR_STATUS       12
#DEFINE QTD_IND_PAR          12

/*/{Protheus.doc} PCPA140
Programa de Sincroniza��o de dados com o MRP
@author  Marcelo Neumann
@version P12
@since   17/07/2019
/*/
Function PCPA140(lLogSync, aApiAlter, lTela)

	Local lChecked  := .F.
	Local lUsaMalha := SuperGetMv('MV_PCPMADI',.F.,.F.)
	Local oCheckAll
	Local oChkPrd
	Local oBtnProc
	Local oBtnSair
	Local oDlgSinc
	Local oPnlBottom
	Local oPnlTop
	Local oTFont := TFont():New('Arial',,-13,,.T.)

	Default lLogSync  := .T.
	Default lTela     := .T.
	Default aApiAlter := {}

	Private aParTela := {}
	Private nLinha   := 25
	Private nLinhaC2 := 25
	Private lVisual  := lTela
	Private nCountItem := 0

	//Se a tabela T4R n�o estiver em modo compartilhado, n�o permite abertura da tela
	If !FWModeAccess("T4R",1) == "C" .Or. !FWModeAccess("T4R",2) == "C" .Or. !FWModeAccess("T4R",3) == "C"
		HELP(' ', 1, "Help",,STR0053 ,; //"A rotina n�o pode ser inciada pois tabela T4R (pend�ncias do MRP) est� com modo de compartilhamento incorreto)."
		     2, 0, , , , , , {STR0054}) //"Altere o modo de compartilhamento da tabela T4R para 'Compartilhado'."
		Return
	Else
		//Se a integra��o n�o estiver habilitada, n�o permite utilizar a tela de sincroniza��o.
		If !IntNewMRP("MRPDEMANDS")
			HELP(' ', 1, "Help",, STR0030,; //"Integra��o com o MRP n�o est� habilitada."
				2, 0, , , , , , {STR0031}) //"Ative a integra��o com o MRP para utilizar o programa de Sincroniza��o."
			Return
		EndIf
	EndIf

	If !lTela
		lUsaMalha := .F.
	EndIf

	If Len(aApiAlter) == 0
		//Define a tela e seus pain�is
		DEFINE DIALOG oDlgSinc TITLE STR0001 FROM 0,0 TO 430, 460 PIXEL //"Sincronizador MRP"

		oPnlTop := TPanel():New(01, 01, , oDlgSinc, , , , , , 415, 300, .T.,.T.)
		oPnlTop:Align := CONTROL_ALIGN_TOP

		oPnlBottom := TPanel():New(300, 01, ,oDlgSinc, , , , , , 415, 20, .T.,.T.)
		oPnlBottom:Align := CONTROL_ALIGN_BOTTOM

		//Cria o checkbox para marcar/desmarcar todos
		@ 15, 70 CHECKBOX oCheckAll VAR lChecked PROMPT STR0002 ON CHANGE MarcaTodos(lChecked, oCheckAll, oChkPrd) PIXEL OF oPnlTop SIZE 100,015 MESSAGE "" font oTFont //"Marca/Desmarca todos"
	EndIf

	//Adiciona os par�metros da tela
	oChkPrd := AddParam(oPnlTop, IND_TELA_PRODUTOS, "SB1", P139GetAPI("MRPPRODUCT"  ), "MRPPRODUCT"          , lTela)     //"Produtos"
	AddParam(oPnlTop, IND_TELA_INDICA_PROD, "SBZ", P139GetAPI("MRPPRODUCTINDICATOR" ), "MRPPRODUCTINDICATOR" , lTela)     //"Indicadores Produtos"
	AddParam(oPnlTop, IND_TELA_ESTRUTURA  , "SG1", P139GetAPI("MRPBILLOFMATERIAL"   ), "MRPBILLOFMATERIAL"   , lTela)     //"Estrutura"
	AddParam(oPnlTop, IND_TELA_VERSAO_PROD, "SVC", P139GetAPI("MRPPRODUCTIONVERSION"), "MRPPRODUCTIONVERSION", lTela)     //"Vers�o da Produ��o"
	AddParam(oPnlTop, IND_TELA_OPER_COMPON, "SGF", P139GetAPI("MRPBOMROUTING"       ), "MRPBOMROUTING"       , lTela .AND. FWAliasInDic( "HW9", .F. ))  //"Opera��es por Componente"
	AddParam(oPnlTop, IND_TELA_CALENDARIOS, "SVZ", P139GetAPI("MRPCALENDAR"         ), "MRPCALENDAR"         , lTela)     //"Calend�rios"
	AddParam(oPnlTop, IND_TELA_DEMANDAS   , "SVR", P139GetAPI("MRPDEMANDS"          ), "MRPDEMANDS"          , lTela)     //"Demandas"
	AddParam(oPnlTop, IND_TELA_ORDEM_PROD , "SC2", P139GetAPI("MRPPRODUCTIONORDERS" ), "MRPPRODUCTIONORDERS" , lTela)     //"Ordem de Produ��o"
	AddParam(oPnlTop, IND_TELA_EMPENHOS   , "SD4", P139GetAPI("MRPALLOCATIONS"      ), "MRPALLOCATIONS"      , lTela)     //"Empenhos"
	AddParam(oPnlTop, IND_TELA_SOL_COMPRAS, "SC1", P139GetAPI("MRPPURCHASEORDER"    ), "MRPPURCHASEORDER"    , lTela)     //"Solicita��es de Compras"
	AddParam(oPnlTop, IND_TELA_PED_COMPRAS, "SC7", P139GetAPI("MRPPURCHASEREQUEST"  ), "MRPPURCHASEREQUEST"  , lTela)     //"Pedidos de Compras"
	AddParam(oPnlTop, IND_TELA_ESTOQUES   , "SB2", P139GetAPI("MRPSTOCKBALANCE"     ), "MRPSTOCKBALANCE"     , lTela)     //"Estoques"
	AddParam(oPnlTop, IND_TELA_CQ         , "SD7", P139GetAPI("MRPREJECTEDINVENTORY"), "MRPREJECTEDINVENTORY", lTela .AND. FWAliasInDic( "HWX", .F. ))  //"CQ"
	AddParam(oPnlTop, IND_TELA_ARMAZEM    , "NNR", P139GetAPI("MRPWAREHOUSE"        ), "MRPWAREHOUSE"        , lTela .AND. FWAliasInDic( "HWY", .F. ))  //"Armazens"
	AddParam(oPnlTop, IND_TELA_MALHA_DISTR, "SVD", P139GetAPI("MRPTRANSPORTINGLANES"), "MRPTRANSPORTINGLANES", lUsaMalha) //"Malha de Distribui��o"
	AddParam(oPnlTop, IND_TELA_GRUPO_ARM  , "SVN", P139GetAPI("MRPWAREHOUSEGROUP"   ), "MRPWAREHOUSEGROUP"	 , lUsaMalha) //"Grupo de Armaz�ms"
	

	If Len(aApiAlter) > 0
		procExt(aApiAlter)
	Else
		//Bot�es
		@ 03, 015 BUTTON oBtnSair PROMPT STR0004 SIZE 70,12 WHEN (.T.) ACTION (oDlgSinc:End()) OF oPnlBottom PIXEL //"Sair"
		@ 03, 150 BUTTON oBtnProc PROMPT STR0005 SIZE 70,12 WHEN (.T.) ACTION (Processar())    OF oPnlBottom PIXEL //"Processar"
		//Abre a tela
		ACTIVATE MSDIALOG oDlgSinc CENTERED ON INIT Iif(lLogSync, logSync(), .T.)
	EndIf

Return

/*/{Protheus.doc} AddParam
Adiciona um par�metro na tela (Checkbox, Descri��o e Bot�o de filtro)
@author  Marcelo Neumann
@version P12
@since   17/07/2019
@param   01 oDialog   , objeto  , tela onde ser�o criados os componentes
@param   02 nIndTela  , num�rico, indicador da posi��o do item na tela
@param   03 cAlias    , caracter, tabela a ser utilizada no filtro
@param   04 cDescricao, caracter, descri��o/label do checkbox
@param   05 cApi      , caracter, nome da API que esse par�metro utilizar�
@param   06 lVisivel  , l�gico  , indica se o checkbox dever� ser exibido
@return  oCheck       , objeto  , objeto com o checkbox criado
/*/
Static Function AddParam(oDialog, nIndTela, cAlias, cDescricao, cApi, lVisivel)

	Local oCheck
	Local oTFont := TFont():New('Arial',,-12)

	//Adiciona uma posi��o no array de controle (aParTela)
	aAdd(aParTela, Array(QTD_IND_PAR))

	//Atribui default para os campos
	aParTela[nIndTela][IND_PAR_CHECKED]      := .F.
	aParTela[nIndTela][IND_PAR_API]          := cApi
	aParTela[nIndTela][IND_PAR_PROCESSADO]   := .F.
	aParTela[nIndTela][IND_PAR_VISIVEL]      := lVisivel
	aParTela[nIndTela][IND_PAR_MSG_ERRO]	 := ""
	IniParTela()

	//Cria na tela o checkbox
	If lVisivel
		If @nCountItem > 9
			nLinhaC2 += 15
			@ nLinhaC2, 130 CHECKBOX oCheck VAR aParTela[nIndTela][IND_PAR_CHECKED] PROMPT cDescricao PIXEL OF oDialog SIZE 150,015 MESSAGE "" FONT oTFont
		Else
			nLinha += 15
			@ nLinha, 15 CHECKBOX oCheck VAR aParTela[nIndTela][IND_PAR_CHECKED] PROMPT cDescricao PIXEL OF oDialog SIZE 150,015 MESSAGE "" FONT oTFont
		EndIf
		@nCountItem++
	EndIf

	aParTela[nIndTela][IND_PAR_ALIAS]     := cAlias
	aParTela[nIndTela][IND_PAR_DESCRICAO] := cDescricao

Return oCheck

/*/{Protheus.doc} MarcaTodos
Marca/Desmarca todos os checkboxs
@author  Marcelo Neumann
@version P12
@since   17/07/2019
@param   01 lChecked , l�gico, indica se foi marcado ou desmarcado o checkbox de marcar todos
@param   02 oCheckAll, objeto, objeto do checkbox Marca/Desmarca todos
@param   03 oChecks  , objeto, objeto dos demais checkboxs para atualizar o conte�do em tela
@return  .T.
/*/
Static Function MarcaTodos(lChecked, oCheckAll, oChecks)

	Local nInd   := 1
	Local nTotal := Len(aParTela)

	//Marca todos os par�metros que podem ser marcados
	For nInd := 1 To nTotal
		If aParTela[nInd][IND_PAR_VISIVEL]
			aParTela[nInd][IND_PAR_CHECKED] := lChecked
		EndIf
	Next nInd

	//D� foco para atualizar a tela com as marca��es
	SetFocus(oChecks:HWND)
	SetFocus(oCheckAll:HWND)

Return .T.

/*/{Protheus.doc} Processar
Fun��o principal de processamento dos dados
@author  Marcelo Neumann
@version P12
@since   17/07/2019
@return  NIL
/*/
Static Function Processar()

	Local nTotal := 0

	//Verifica se foi selecionado algum registro para processar
	If aScan(aParTela, {|x| x[IND_PAR_CHECKED]}) < 1
		Help('', 1, "Help", , STR0008,; //"Nenhum registro selecionado."
		     2, 0, , , , , , {STR0009}) //"Marque algum registro para prosseguir com a sincroniza��o."
		Return .F.
	EndIf

	//Filtra os registros retornando a quantidade
	nTotal += CalculaReg(IND_TELA_CALENDARIOS)
	nTotal += CalculaReg(IND_TELA_DEMANDAS   )
	nTotal += CalculaReg(IND_TELA_EMPENHOS   )
	nTotal += CalculaReg(IND_TELA_ESTOQUES   )
	nTotal += CalculaReg(IND_TELA_ESTRUTURA  )
	nTotal += CalculaReg(IND_TELA_ORDEM_PROD )
	nTotal += CalculaReg(IND_TELA_OPER_COMPON)
	nTotal += CalculaReg(IND_TELA_PED_COMPRAS)
	nTotal += CalculaReg(IND_TELA_SOL_COMPRAS)
	nTotal += CalculaReg(IND_TELA_VERSAO_PROD)
	nTotal += CalculaReg(IND_TELA_MALHA_DISTR)
	nTotal += CalculaReg(IND_TELA_GRUPO_ARM  )
	nTotal += CalculaReg(IND_TELA_PRODUTOS   )
	nTotal += CalculaReg(IND_TELA_INDICA_PROD)
	nTotal += CalculaReg(IND_TELA_CQ         )
	nTotal += CalculaReg(IND_TELA_ARMAZEM    )

	If !lVisual
		Processa( {|| GerenProc(nTotal) }, STR0016, STR0015,.F.) //"Aguarde..." "Sincronizando..."
	Else
		If nTotal == 0
			If MsgYesNo(STR0060, STR0014) //"Deseja continuar com a sincroniza��o dos registros?" "Aten��o"
				//Monta a barra de progresso e os par�metros
				Processa( {|| GerenProc(nTotal) }, STR0016, STR0015,.F.) //"Aguarde..." "Sincronizando..."
			EndIf
		ElseIf MsgYesNo(STR0012 + cValToChar(nTotal) + STR0013, STR0014) //"Ser�o processados" X "registros, deseja continuar?" "Aten��o"
			//Monta a barra de progresso e os par�metros
			Processa( {|| GerenProc(nTotal) }, STR0016, STR0015,.F.) //"Aguarde..." "Sincronizando..."
		EndIf
	EndIf

	IniParTela()

Return

/*/{Protheus.doc} IniParTela
Inicializa o array com os par�metros da tela
@author  Marcelo Neumann
@version P12
@since   17/07/2019
@return  NIL
/*/
Static Function IniParTela()

	Local nInd := 1

	For nInd := 1 To Len(aParTela)
		If aParTela[nInd][IND_PAR_QTD_TOTAL] > 0
			aParTela[nInd][IND_PAR_QTD_TOTAL]    := 0
			aParTela[nInd][IND_PAR_QTD_SUCESSO]  := 0
			aParTela[nInd][IND_PAR_QTD_ERRO]     := 0
		EndIf
		aParTela[nInd][IND_PAR_PROCESSADO] := .F.
		aParTela[nInd][IND_PAR_MSG_ERRO]   := ""
	Next nInd

Return

/*/{Protheus.doc} GerenProc
Gerencia o processamento dos par�metros
@author  Marcelo Neumann
@version P12
@since   17/07/2019
@param   01 nTotal  , num�rico, total de registros que ser�o processados
@return  NIL
/*/
Static Function GerenProc(nTotal)
	Local aJobs      := {}
	Local aRetVal    := {}
	Local aProc      := {}
	Local cValue     := ""
	Local cCarga     := ""
	Local lCargaAll  := .F.
	Local nCountTot  := 0
	Local nCountAux  := 0
	Local nIndex     := 0
	Local nFinished  := 0	
	Local nQtdErrors := 0

	//Adequa o total de passos da barra de progresso superior
	nTotal := nTotal + 4

	ProcRegua(nTotal)

	IncProc(STR0049) //"Iniciando processamentos"

	aRetVal := VldCampTam()
	
	If aRetVal[3] == 0
		For nIndex := 1 To Len(aParTela)
			If aParTela[nIndex][IND_PAR_CHECKED]
				aParTela[nIndex][IND_PAR_PROCESSADO] := .F.
				cValue := cValToChar(ThreadId()) + "PCPA140JOB" + cValToChar(nIndex)

				aAdd(aJobs, {cValue, nIndex})
				PutGlbValue(cValue, "0")
				PutGlbValue(cValue+"TOTAL"  , "0")
				PutGlbValue(cValue+"CARGA"  , "0")
				PutGlbValue(cValue+"SUCESSO", "0")
				PutGlbValue(cValue+"ERRO"   , "0")
				PutGlbValue(cValue+"LOG_ERROR"   , "0")
				GlbUnLock()

				StartJob("PCPA140JOB", GetEnvServer(), .F., cEmpAnt, cFilAnt, cValue, nIndex, aParTela)
			EndIf
		Next nIndex
	Else
		For nIndex := 1 To Len(aParTela)
			aParTela[nIndex][IND_PAR_PROCESSADO]  := .F.
			aParTela[nIndex][IND_PAR_MSG_ERRO]    := STR0063 //"N�o processado devido inconsist�ncias encontradas na valida��o da integridade da base de dados."
			aParTela[nIndex][IND_PAR_QTD_SUCESSO] := 0
			aParTela[nIndex][IND_PAR_QTD_ERRO]    := 0
		Next nIndex
		aAdd(aProc, {.T.,"VAL","",aRetVal[2]+aRetVal[3],aRetVal[2],aRetVal[3],STR0061,"VALIDADOR",.T.,.T.,aRetVal[1], Nil})
		nQtdErrors++
	EndIf


	IncProc(STR0050) //"Aguardando processamentos"

	//Aguarda os jobs finalizarem
	While .T.
		nCountAux := 0
		nFinished := 0
		lCargaAll := .T.

		For nIndex := 1 To Len(aJobs)
			nCountAux += Val(GetGlbValue(aJobs[nIndex][1]+"TOTAL"))

			If aParTela[aJobs[nIndex][2]][IND_PAR_PROCESSADO]
				nFinished++
				Loop
			EndIf

			cValue    := GetGlbValue(aJobs[nIndex][1])
			cCarga    := GetGlbValue(aJobs[nIndex][1]+"CARGA")

			If cCarga == "0" .Or. Empty(cCarga)
				lCargaAll := .F.
			EndIf

			If cValue == "1"
				//Processou com sucesso
				aParTela[aJobs[nIndex][2]][IND_PAR_QTD_SUCESSO]  := Val(GetGlbValue(aJobs[nIndex][1]+"SUCESSO"))
				aParTela[aJobs[nIndex][2]][IND_PAR_QTD_ERRO]     := Val(GetGlbValue(aJobs[nIndex][1]+"ERRO"))
				aParTela[aJobs[nIndex][2]][IND_PAR_PROCESSADO]   := .T.
				If aParTela[aJobs[nIndex][2]][IND_PAR_QTD_ERRO] > 0
					nQtdErrors += aParTela[aJobs[nIndex][2]][IND_PAR_QTD_ERRO]
				EndIf
				nFinished++

			ElseIf cValue == "2"
				aParTela[aJobs[nIndex][2]][IND_PAR_MSG_ERRO] := GetGlbValue(aJobs[nIndex][1]+"LOG_ERROR")
				nQtdErrors ++
				nFinished++
			EndIf
		Next nIndex

		If nFinished == Len(aJobs)
			Exit
		EndIf

		//Atualiza a barra de progresso
		If lCargaAll
			IncProc(STR0052) //"Enviando dados..."
			ProcRegua(0)
			SysRefresh()
		Else
			For nIndex := 1 To (nCountAux-nCountTot)
				IncProc(STR0051 + cValToChar(nCountAux) + STR0019 + cValToChar(nTotal-4)) //"Preparando dados... " x " de " y
				PutGlbValue("PERCENTUALSINC", CVALTOCHAR(Round((nCountTot*100)/(nTotal-4), 2)))
			Next nIndex
			nCountTot := nCountAux
		EndIf

		Sleep(50)
	End

	If nQtdErrors > 0
		PutGlbValue("QTDERROSSINC", cValToChar(nQtdErrors))
	EndIf

	IncProc(STR0027) //"Atualizando parametros"
	UpdateT4P()

	//Limpa os valores das vari�veis globais criadas.
	For nIndex := 1 To Len(aJobs)
		ClearGlbValue(aJobs[nIndex][1])
		ClearGlbValue(aJobs[nIndex][1]+"TOTAL"  )
		ClearGlbValue(aJobs[nIndex][1]+"CARGA"  )
		ClearGlbValue(aJobs[nIndex][1]+"SUCESSO")
		ClearGlbValue(aJobs[nIndex][1]+"ERRO"   )
		ClearGlbValue(aJobs[nIndex][1]+"LOG_ERROR")
		GlbUnLock()
	Next nIndex

	
	
	For nIndex := 1 To Len(aParTela)
		If aParTela[nIndex][IND_PAR_CHECKED]
			If aParTela[nIndex][IND_PAR_QTD_SUCESSO] == Nil
				aParTela[nIndex][IND_PAR_QTD_SUCESSO] := 0
			EndIf
			If aParTela[nIndex][IND_PAR_QTD_ERRO] == Nil
				aParTela[nIndex][IND_PAR_QTD_ERRO] := 0
			EndIf			
			aAdd(aProc, aParTela[nIndex])
		EndIf
	Next nIndex

	//Mostra a tela com o resultado do processamento
	IncProc(STR0022) //"Sumarizando os resultados..."
	If lVisual
		ResultProc(aProc)
	EndIf
	aSize(aProc, 0)

Return

/*/{Protheus.doc} PCPA140JOB
Fun��o para executar a sincroniza��o em multi-thread.

@type  Function
@author lucas.franca
@since 06/08/2019
@version P12.1.28
@param cEmp      , Character, C�digo da empresa para conex�o
@param cFil      , Character, C�digo da filial para conex�o
@param cJobName  , Character, Nome identificador do JOB
@param nSync     , Numeric  , Identificador da entidade a sincronizar.
@param aParamTela, Array    , Array com os par�metros da tela.
@return Nil
/*/
Function PCPA140JOB(cEmp, cFil, cJobName, nSync, aParamTela)

	Private aParTela := aParamTela

	ErrorBlock({|e| A140Error(e, cJobName) })

	Begin Sequence

		RpcSetType(3)
		RpcSetEnv(cEmp, cFil)

		SetFunName("PCPA140") //Seta a fun��o inicial para PCPA140

		Do Case
			Case nSync == IND_TELA_CALENDARIOS
				SincCalend(cJobName)
			Case nSync == IND_TELA_DEMANDAS
				SincDemand(cJobName)
			Case nSync == IND_TELA_EMPENHOS
				SincEmpe(cJobName)
			Case nSync == IND_TELA_ESTRUTURA
				SincEstrut(cJobName)
			Case nSync == IND_TELA_OPER_COMPON
				SincOpComp(cJobName)
			Case nSync == IND_TELA_ORDEM_PROD
				SincOrdPrd(cJobName)
			Case nSync == IND_TELA_PED_COMPRAS
				SincPedCom(cJobName)
			Case nSync == IND_TELA_SOL_COMPRAS
				SincSolCom(cJobName)
			Case nSync == IND_TELA_VERSAO_PROD
				SincPrdVer(cJobName)
			Case nSync == IND_TELA_ESTOQUES
				SincStock(cJobName)
			Case nSync == IND_TELA_CQ
				SincCQ(cJobName)
			Case nSync == IND_TELA_MALHA_DISTR
				SincMalha(cJobName)
			Case nSync == IND_TELA_GRUPO_ARM
				SincGrupo(cJobName)
			Case nSync == IND_TELA_PRODUTOS
				SincProd(cJobName)
			Case nSync == IND_TELA_INDICA_PROD
				SincIndPrd(cJobName)
			Case nSync == IND_TELA_ARMAZEM
				SincArmaz(cJobName)
		EndCase
		PutGlbValue(cJobName, "1")
		GlbUnLock()

		//Caso ocorra algum erro, seta a flag com o valor 2.
		RECOVER
			PutGlbValue(cJobName, "2")
			GlbUnLock()

	End Sequence

Return Nil

/*/{Protheus.doc} A140Error
Fun��o para tratativa de erros de execu��o

@type  Function
@author lucas.franca
@since 06/08/2019
@version P12.1.28
@param e, Object, Objeto com os detalhes do erro ocorrido
/*/
Function A140Error(e, cJobName)
	Local cMessage := AllTrim(e:description) + CHR(10) + AllTrim(e:ErrorStack)
	LogMsg('PCPA140JOB', 0, 0, 1, '', '', ;
	       Replicate("-",70) + CHR(10) + cMessage + CHR(10) + Replicate("-",70))
	PutGlbValue(cJobName+"LOG_ERROR", cMessage)
	BREAK
Return

/*/{Protheus.doc} SincCalend
Sincroniza os Calend�rios
@author  brunno.costa
@version P12
@since   05/08/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincCalend(cJobName)

	Local aDadosInc  := {}
	Local aError     := {}
	Local aSuccess   := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nAtual     := 0
	Local nPos       := 0
	Local oTTPend

	//Cria tempor�rias para a PCPA131INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_CALENDARIOS][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_CALENDARIOS][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		COLUMN VZ_DATA AS DATE
		SELECT  VZ_FILIAL,
				VZ_CALEND,
				VZ_DATA,
				VZ_HORAINI,
				VZ_HORAFIM,
				VZ_INTERVA,
				R_E_C_N_O_
		FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(A131APICnt("ARRAY_CALENDAR_SIZE")))
		nPos := Len(aDadosInc)

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][A131APICnt("ARRAY_CALENDAR_POS_FILIAL")] := (cAliasQry)->VZ_FILIAL
		aDadosInc[nPos][A131APICnt("ARRAY_CALENDAR_POS_CALEND")] := (cAliasQry)->VZ_CALEND
		aDadosInc[nPos][A131APICnt("ARRAY_CALENDAR_POS_DATA"  )] := (cAliasQry)->VZ_DATA
		aDadosInc[nPos][A131APICnt("ARRAY_CALENDAR_POS_HRAINI")] := (cAliasQry)->VZ_HORAINI
		aDadosInc[nPos][A131APICnt("ARRAY_CALENDAR_POS_HRAFIM")] := (cAliasQry)->VZ_HORAFIM
		aDadosInc[nPos][A131APICnt("ARRAY_CALENDAR_POS_INTER" )] := (cAliasQry)->VZ_INTERVA

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(A131APICnt("ARRAY_CALENDAR_SIZE")))
		aDadosInc[1][A131APICnt("ARRAY_CALENDAR_POS_FILIAL")] := xFilial("SVZ")
		lOnlyDel := .T.
	EndIf
	//Chama a fun��o do PCPA131API para integrar os registros
	PCPA131INT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA131INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincEmpe
Sincroniza os Empenhos
@author  brunno.costa
@version P12
@since   05/08/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincEmpe(cJobName)

	Local aDadosInc  := {}
	Local aError     := {}
	Local aSuccess   := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nAtual     := 0
	Local nPos       := 0
	Local nTamQtd    := GetSx3Cache("D4_QUANT", "X3_TAMANHO")
	Local nTamDec    := GetSx3Cache("D4_QUANT", "X3_DECIMAL")
	Local oTTPend

	//Cria tempor�rias para a PCPA381INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_EMPENHOS][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_EMPENHOS][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		COLUMN D4_DATA  AS DATE
		COLUMN D4_QUANT AS NUMERIC(nTamQtd, nTamDec)
		COLUMN D4_QSUSP AS NUMERIC(nTamQtd, nTamDec)
		SELECT  D4_FILIAL,
				D4_COD,
				D4_OP,
				D4_OPORIG,
				D4_DATA,
				D4_TRT,
				SUM(D4_QUANT) AS D4_QUANT,
				SUM(D4_QSUSP) AS D4_QSUSP,
				D4_LOCAL
		FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(A381APICnt("ARRAY_SIZE")))
		nPos := Len(aDadosInc)

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][A381APICnt("ARRAY_POS_FILIAL" )] := (cAliasQry)->D4_FILIAL
		aDadosInc[nPos][A381APICnt("ARRAY_POS_PROD"   )] := (cAliasQry)->D4_COD
		aDadosInc[nPos][A381APICnt("ARRAY_POS_OP"     )] := (cAliasQry)->D4_OP
		aDadosInc[nPos][A381APICnt("ARRAY_POS_OP_ORIG")] := (cAliasQry)->D4_OPORIG
		aDadosInc[nPos][A381APICnt("ARRAY_POS_DATA"   )] := (cAliasQry)->D4_DATA
		aDadosInc[nPos][A381APICnt("ARRAY_POS_SEQ"    )] := (cAliasQry)->D4_TRT
		aDadosInc[nPos][A381APICnt("ARRAY_POS_QTD"    )] := (cAliasQry)->D4_QUANT
		aDadosInc[nPos][A381APICnt("ARRAY_POS_QSUSP"  )] := (cAliasQry)->D4_QSUSP
		aDadosInc[nPos][A381APICnt("ARRAY_POS_LOCAL"  )] := (cAliasQry)->D4_LOCAL

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(A381APICnt("ARRAY_SIZE")))
		aDadosInc[1][A381APICnt("ARRAY_POS_FILIAL" )] := xFilial("SD4")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do PCPA381API para integrar os registros
	PCPA381INT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA381INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincDemand
Sincroniza as Demandas
@author  Marcelo Neumann
@version P12
@since   17/07/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincDemand(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nAtual     := 0
	Local nPos       := 0
	Local nTamQtd    := GetSx3Cache("VR_QUANT", "X3_TAMANHO")
	Local nTamDec    := GetSx3Cache("VR_QUANT", "X3_DECIMAL")
	Local oTTInteg
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTInteg := P136APITMP()
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_DEMANDAS][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_DEMANDAS][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		COLUMN VR_DATA  AS DATE
		COLUMN VR_QUANT AS NUMERIC(nTamQtd, nTamDec)
		SELECT VR_FILIAL,
				VR_CODIGO,
				VR_SEQUEN,
				VR_PROD,
				VR_DATA,
				VR_TIPO,
				VR_DOC,
				VR_QUANT,
				VR_LOCAL,
				VR_OPC,
				VR_MOPC
			FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(A136APICnt("ARRAY_DEMAND_SIZE")))
		nPos := Len(aDadosInc)

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_FILIAL" )] := (cAliasQry)->VR_FILIAL
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_CODE"   )] := (cAliasQry)->VR_CODIGO
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_SEQUEN" )] := (cAliasQry)->VR_SEQUEN
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_PROD"   )] := (cAliasQry)->VR_PROD
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_REV"    )] := ""
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_DATA"   )] := (cAliasQry)->VR_DATA
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_TIPO"   )] := (cAliasQry)->VR_TIPO
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_DOC"    )] := (cAliasQry)->VR_DOC
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_QUANT"  )] := (cAliasQry)->VR_QUANT
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_LOCAL"  )] := (cAliasQry)->VR_LOCAL
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_OPC"    )] := (cAliasQry)->VR_MOPC
		aDadosInc[nPos][A136APICnt("ARRAY_DEMAND_POS_STR_OPC")] := (cAliasQry)->VR_OPC

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(A136APICnt("ARRAY_DEMAND_SIZE")))
		aDadosInc[1][A136APICnt("ARRAY_DEMAND_POS_FILIAL" )] := xFilial("SVR")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do PCPA136API para integrar os registros
	PCPA136INT("SYNC", aDadosInc, oTTInteg, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA136INT
	oTTInteg:Delete()
	oTTPend:Delete()
	FreeObj(oTTInteg)
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincPedCom
Sincroniza os Pedidos de Compras
@author  brunno.costa
@version P12
@since   05/08/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincPedCom(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cQryCondic := ""
	Local cAliasQry  := GetNextAlias()
	Local lOnlyDel   := .F.
	Local nPos       := 0
	Local nAtual     := 0
	Local nTamQtd    := GetSx3Cache("C7_QUANT", "X3_TAMANHO")
	Local nTamDec    := GetSx3Cache("C7_QUANT", "X3_DECIMAL")
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_PED_COMPRAS][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_PED_COMPRAS][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		COLUMN C7_DATPRF AS DATE
		COLUMN C7_QUANT  AS NUMERIC(nTamQtd, nTamDec)
		COLUMN C7_QUJE   AS NUMERIC(nTamQtd, nTamDec)
		SELECT 	C7_FILIAL,
				C7_NUM,
				C7_ITEM,
				C7_PRODUTO,
				C7_OP,
				C7_DATPRF,
				C7_QUANT,
				C7_QUJE,
				C7_LOCAL,
				C7_TPOP,
				C7_ITEMGRD,
				R_E_C_N_O_
			FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(PEDCAPICnt("ARRAY_PEDCOM_SIZE")))
		nPos := Len(aDadosInc)

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_FILIAL" )] := (cAliasQry)->C7_FILIAL
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_NUM"    )] := (cAliasQry)->C7_NUM
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_ITEM"   )] := (cAliasQry)->C7_ITEM
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_PROD"   )] := (cAliasQry)->C7_PRODUTO
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_OP"     )] := (cAliasQry)->C7_OP
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_DATPRF" )] := (cAliasQry)->C7_DATPRF
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_QTD"    )] := (cAliasQry)->C7_QUANT
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_QUJE"   )] := (cAliasQry)->C7_QUJE
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_LOCAL"  )] := (cAliasQry)->C7_LOCAL
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_TIPO"   )] := (cAliasQry)->C7_TPOP
		aDadosInc[nPos][PEDCAPICnt("ARRAY_PEDCOM_POS_ITGRD"  )] := (cAliasQry)->C7_ITEMGRD

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(PEDCAPICnt("ARRAY_PEDCOM_SIZE")))
		aDadosInc[1][PEDCAPICnt("ARRAY_PEDCOM_POS_FILIAL")] := xFilial("SC7")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do MrpPurchaseRequestAPI.prw para integrar os registros
	PCPPEDCINT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA136INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincStock
Sincroniza os Estoques
@author  brunno.costa
@version P12
@since   06/08/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincStock(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nPos       := 0
	Local nAtual     := 0
	Local nTamFil    := FwSizeFilial()
	Local nTamPrd    := GetSx3Cache("B8_PRODUTO","X3_TAMANHO")
	Local nTamLoc    := GetSx3Cache("B8_LOCAL"  ,"X3_TAMANHO")
	Local nTamLote   := GetSx3Cache("B8_LOTECTL","X3_TAMANHO")
	Local nTamSubLt  := GetSx3Cache("B8_NUMLOTE","X3_TAMANHO")
	Local nTamQtd    := GetSx3Cache("B2_QATU"   ,"X3_TAMANHO")
	Local nTamDec    := GetSx3Cache("B2_QATU"   ,"X3_DECIMAL")
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_ESTOQUES][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_ESTOQUES][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		COLUMN availableQuantity     AS NUMERIC(nTamQtd, nTamDec)
		COLUMN consignedOut          AS NUMERIC(nTamQtd, nTamDec)
		COLUMN consignedIn           AS NUMERIC(nTamQtd, nTamDec)
		COLUMN unavailableQuantity   AS NUMERIC(nTamQtd, nTamDec)
		COLUMN expirationDate        AS DATE
		COLUMN blockedBalance 		 AS NUMERIC(nTamQtd, nTamDec)
		SELECT  branchId,
				product,
				warehouse,
				lot,
				sublot,
				expirationDate,
				availableQuantity,
				consignedOut,
				consignedIn,
				unavailableQuantity,
				blockedBalance
		FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(EstqAPICnt("ARRAY_ESTOQUE_SIZE")))
		nPos := Len(aDadosInc)

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_FILIAL"   )] := PadR((cAliasQry)->branchId , nTamFil)
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_PROD"     )] := PadR((cAliasQry)->product  , nTamPrd)
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_LOCAL"    )] := PadR((cAliasQry)->warehouse, nTamLoc)
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_LOTE"     )] := PadR((cAliasQry)->lot      , nTamLote)
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_SUBLOTE"  )] := PadR((cAliasQry)->sublot   , nTamSubLt)
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_VALIDADE" )] := (cAliasQry)->expirationDate
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_QTD"      )] := (cAliasQry)->availableQuantity
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_QTD_NPT"  )] := (cAliasQry)->consignedOut
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_QTD_TNP"  )] := (cAliasQry)->consignedIn
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_QTD_IND"  )] := (cAliasQry)->unavailableQuantity
		aDadosInc[nPos][EstqAPICnt("ARRAY_ESTOQUE_POS_QTD_BLQ"  )] := (cAliasQry)->blockedBalance
		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(EstqAPICnt("ARRAY_ESTOQUE_SIZE")))
		aDadosInc[1][EstqAPICnt("ARRAY_ESTOQUE_POS_FILIAL")] := xFilial("SB2")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do MrpPurchaseRequestAPI.prw para integrar os registros
	PcpEstqInt("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA136INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincCQ
Sincroniza CQ
@author  brunno.costa
@version P12
@since   13/07/2020
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincCQ(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cAlias     := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nPos       := 0
	Local nAtual     := 0
	Local nTamFil    := FwSizeFilial()
	Local nTamPrd    := GetSx3Cache("D7_PRODUTO","X3_TAMANHO")
	Local nTamLoc    := GetSx3Cache("D7_LOCDEST","X3_TAMANHO")
	Local nTamQtd    := GetSx3Cache("D7_QTDE"   ,"X3_TAMANHO")
	Local nTamDec    := GetSx3Cache("D7_QTDE"   ,"X3_DECIMAL")
	Local nTamData   := 8
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_CQ][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_CQ][IND_PAR_QUERY]
	BeginSql Alias cAlias
		COLUMN quantity         AS NUMERIC(nTamQtd, nTamDec)
		COLUMN returnedQuantity AS NUMERIC(nTamQtd, nTamDec)
		SELECT branchId,
			   product,
			   warehouse,
			   invoiceDate,
			   quantity,
			   returnedQuantity
		FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAlias)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(CQAPICnt("ARRAY_CQ_SIZE")))
		nPos := Len(aDadosInc)

		aDadosInc[nPos][CQAPICnt("ARRAY_CQ_POS_FILIAL"   )] := PadR((cAlias)->branchId   , nTamFil)
		aDadosInc[nPos][CQAPICnt("ARRAY_CQ_POS_PROD"     )] := PadR((cAlias)->product    , nTamPrd)
		aDadosInc[nPos][CQAPICnt("ARRAY_CQ_POS_LOCAL"    )] := PadR((cAlias)->warehouse  , nTamLoc)
		aDadosInc[nPos][CQAPICnt("ARRAY_CQ_POS_DATA"     )] := PadR((cAlias)->invoiceDate, nTamData)
		aDadosInc[nPos][CQAPICnt("ARRAY_CQ_POS_QTDE"     )] := (cAlias)->quantity
		aDadosInc[nPos][CQAPICnt("ARRAY_CQ_POS_QTD_DEV"  )] := (cAlias)->returnedQuantity

		cChave := aDadosInc[nPos][CQAPICnt("ARRAY_CQ_POS_FILIAL")] +;
				  aDadosInc[nPos][CQAPICnt("ARRAY_CQ_POS_PROD"  )] +;
				  aDadosInc[nPos][CQAPICnt("ARRAY_CQ_POS_LOCAL" )] +;
				  aDadosInc[nPos][CQAPICnt("ARRAY_CQ_POS_DATA")]

		(cAlias)->(dbSkip())
	End
	(cAlias)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(CQAPICnt("ARRAY_CQ_SIZE")))
		aDadosInc[1][CQAPICnt("ARRAY_CQ_POS_FILIAL")] := xFilial("SD7")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do MrpPurchaseRequestAPI.prw para integrar os registros
	PcpCQInt("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA136INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincOrdPrd
Sincroniza as Ordens de Produ��o
@author  brunno.costa
@version P12
@since   05/08/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincOrdPrd(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local lOnlyDel   := .F.
	Local nAtual     := 0
	Local cQryCondic := ""
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_ORDEM_PROD][IND_PAR_API])

	//Consulta os registros
	cQryCondic := QryCondSC2(.F.)
	dbSelectArea("SC2")
	SC2->(dbSetOrder(1))
	SC2->(dbSetFilter({|| &cQryCondic}, cQryCondic))
	SC2->(dbGoTop())

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While SC2->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		A650AddInt(aDadosInc, , "SYNC")

		SC2->(dbSkip())
	End
	SC2->(dbClearFilter())
	SC2->(dbGoTop())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(A650APICnt("ARRAY_OP_SIZE")))
		aDadosInc[1][A650APICnt("ARRAY_OP_POS_FILIAL")] := xFilial("SC2")
		aDadosInc[1][A650APICnt("ARRAY_OP_POS_XOPER")]  := "SYNC"
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do MrpPurchaseRequestAPI.prw para integrar os registros
	MATA650INT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA136INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincSolCom
Sincroniza as Solicita��es de Compras
@author  Marcelo Neumann
@version P12
@since   05/08/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincSolCom(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nPos       := 0
	Local nAtual     := 0
	Local nTamQtd    := GetSx3Cache("C1_QUANT", "X3_TAMANHO")
	Local nTamDec    := GetSx3Cache("C1_QUANT", "X3_DECIMAL")
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_SOL_COMPRAS][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_SOL_COMPRAS][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		COLUMN C1_DATPRF AS DATE
		COLUMN C1_QUANT  AS NUMERIC(nTamQtd, nTamDec)
		COLUMN C1_QUJE   AS NUMERIC(nTamQtd, nTamDec)
		SELECT 	C1_FILIAL,
				C1_NUM,
				C1_ITEM,
				C1_PRODUTO,
				C1_OP,
				C1_DATPRF,
				C1_QUANT,
				C1_QUJE,
				C1_LOCAL,
				C1_TPOP,
				C1_ITEMGRD
			FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(SOLCAPICnt("ARRAY_SOLCOM_SIZE")))
		nPos := Len(aDadosInc)

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_FILIAL" )] := (cAliasQry)->C1_FILIAL
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_NUM"    )] := (cAliasQry)->C1_NUM
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_ITEM"   )] := (cAliasQry)->C1_ITEM
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_PROD"   )] := (cAliasQry)->C1_PRODUTO
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_OP"     )] := (cAliasQry)->C1_OP
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_DATPRF" )] := (cAliasQry)->C1_DATPRF
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_QTD"    )] := (cAliasQry)->C1_QUANT
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_QUJE"   )] := (cAliasQry)->C1_QUJE
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_LOCAL"  )] := (cAliasQry)->C1_LOCAL
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_TIPO"   )] := IIf( Empty((cAliasQry)->C1_TPOP), "1", (cAliasQry)->C1_TPOP )
		aDadosInc[nPos][SOLCAPICnt("ARRAY_SOLCOM_POS_ITGRD"  )] := (cAliasQry)->C1_ITEMGRD

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(SOLCAPICnt("ARRAY_SOLCOM_SIZE")))
		aDadosInc[1][SOLCAPICnt("ARRAY_SOLCOM_POS_FILIAL")] := xFilial("SC1")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do MrpPurchaseRequestAPI.prw para integrar os registros
	PCPSOLCINT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA136INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincPrdVer
Sincroniza as vers�es de produ��o
@author  Ricardo Prandi
@version P12
@since   01/08/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincPrdVer(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nPos       := 0
	Local nAtual     := 0
	Local nTamQtd    := GetSx3Cache("VC_QTDDE", "X3_TAMANHO")
	Local nTamDec    := GetSx3Cache("VC_QTDDE", "X3_DECIMAL")
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_VERSAO_PROD][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_VERSAO_PROD][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		COLUMN VC_DTINI  AS DATE
		COLUMN VC_DTFIM  AS DATE
		COLUMN VC_QTDDE  AS NUMERIC(nTamQtd, nTamDec)
		COLUMN VC_QTDATE AS NUMERIC(nTamQtd, nTamDec)
		SELECT VC_FILIAL,
				VC_VERSAO,
				VC_PRODUTO,
				VC_DTINI,
				VC_DTFIM,
				VC_QTDDE,
				VC_QTDATE,
				VC_REV,
				VC_ROTEIRO,
				VC_LOCCONS
			FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(A119APICnt("ARRAY_PRODVERS_SIZE")))
		nPos := Len(aDadosInc)

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][A119APICnt("ARRAY_PRODVERS_POS_FILIAL" )] := (cAliasQry)->VC_FILIAL
		aDadosInc[nPos][A119APICnt("ARRAY_PRODVERS_POS_CODE"   )] := (cAliasQry)->VC_VERSAO
		aDadosInc[nPos][A119APICnt("ARRAY_PRODVERS_POS_PROD"   )] := (cAliasQry)->VC_PRODUTO
		aDadosInc[nPos][A119APICnt("ARRAY_PRODVERS_POS_DTINI"  )] := (cAliasQry)->VC_DTINI
		aDadosInc[nPos][A119APICnt("ARRAY_PRODVERS_POS_DTFIM"  )] := (cAliasQry)->VC_DTFIM
		aDadosInc[nPos][A119APICnt("ARRAY_PRODVERS_POS_QTDINI" )] := (cAliasQry)->VC_QTDDE
		aDadosInc[nPos][A119APICnt("ARRAY_PRODVERS_POS_QTDFIM" )] := (cAliasQry)->VC_QTDATE
		aDadosInc[nPos][A119APICnt("ARRAY_PRODVERS_POS_REVISAO")] := (cAliasQry)->VC_REV
		aDadosInc[nPos][A119APICnt("ARRAY_PRODVERS_POS_ROTEIRO")] := (cAliasQry)->VC_ROTEIRO
		aDadosInc[nPos][A119APICnt("ARRAY_PRODVERS_POS_LOCAL"  )] := (cAliasQry)->VC_LOCCONS

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(A119APICnt("ARRAY_PRODVERS_SIZE")))
		aDadosInc[1][A119APICnt("ARRAY_PRODVERS_POS_FILIAL")] := xFilial("SVC")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do PCPA136API para integrar os registros
	PCPA119INT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA136INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincEstrut
Sincroniza as estruturas
@author  lucas.franca
@version P12
@since   05/08/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincEstrut(cJobName)
	Local aSuccess   := {}
	Local aError     := {}
	Local aDadosInc  := {}
	Local aDadosCab  := {}
	Local aDadosCmp  := {}
	Local cQuery     := ""
	Local cAliasQry  := GetNextAlias()
	Local cPrdTable  := SuperGetMv("MV_ARQPROD",.F.,"SB1")
	Local cQryCondic := ""
	Local cPaiAtu    := Nil
	Local lOnlyDel   := .F.
	Local nAtual     := 0
	Local nTamQtd    := GetSx3Cache("G1_QUANT", "X3_TAMANHO")
	Local nDecQtd    := GetSx3Cache("G1_QUANT", "X3_DECIMAL")
	Local nTamPerda  := GetSx3Cache("G1_PERDA", "X3_TAMANHO")
	Local nDecPerda  := GetSx3Cache("G1_PERDA", "X3_DECIMAL")
	Local oTTPend

	//Cria tempor�rias para a PCPA131INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_ESTRUTURA][IND_PAR_API])

	cQryCondic := aParTela[IND_TELA_ESTRUTURA][IND_PAR_QUERY]

	//Ajusta a condi��o WHERE, retirando as condi��es que s�o necess�rias apenas para o Embedded SQL
	//Retira os caracteres %
	cQryCondic := StrTran(cQryCondic, "%","")

	//Retira tudo que estiver antes da palavra chave WHERE.
	cQryCondic := SubStr(cQryCondic, At("WHERE", cQryCondic))

	cQuery := "SELECT SG1.G1_FILIAL, "
	cQuery +=       " SG1.G1_COD, "
	cQuery +=       " SG1.G1_COMP, "
	cQuery +=       " SG1.G1_TRT, "
	cQuery +=       " SG1.G1_QUANT, "
	cQuery +=       " SG1.G1_INI, "
	cQuery +=       " SG1.G1_FIM, "
	cQuery +=       " SG1.G1_REVINI, "
	cQuery +=       " SG1.G1_REVFIM, "
	cQuery +=       " SG1.G1_PERDA, "
	cQuery +=       " SG1.G1_FIXVAR, "
	cQuery +=       " SG1.G1_POTENCI, "
	cQuery +=       " SG1.G1_GROPC, "
	cQuery +=       " SG1.G1_OPC, "
	cQuery +=       " SG1.G1_LOCCONS, "
	cQuery +=       " SG1.R_E_C_N_O_ AS RECSG1, "
	cQuery +=       " (SELECT SB1P.B1_QB "
	cQuery +=          " FROM " + RetSqlName("SB1") + " SB1P "
	cQuery +=         " WHERE SB1P.B1_FILIAL = '" + xFilial("SB1") + "' "
	cQuery +=           " AND SB1P.B1_COD    = SG1.G1_COD "
	cQuery +=           " AND SB1P.D_E_L_E_T_ = ' ' ) AS QTDBASE, "
	cQuery +=       " (SELECT COUNT(SGI.GI_FILIAL) "
	cQuery +=          " FROM " + RetSqlName("SGI") + " SGI "
	cQuery +=         " WHERE SGI.GI_FILIAL  = '" + xFilial("SGI") + "' "
	cQuery +=           " AND SGI.GI_PRODORI = SG1.G1_COMP "
	cQuery +=           " AND SGI.D_E_L_E_T_ = ' ' ) AS ALTERNATIVOS, "

	If cPrdTable == "SBZ"
		cQuery +=   " CASE "
		cQuery +=      " WHEN SG1.G1_FANTASM = ' ' THEN "
		cQuery +=         " CASE "
		cQuery +=            " WHEN SBZ.BZ_FANTASM = 'S' THEN 'T' "
		cQuery +=            " WHEN SBZ.BZ_FANTASM = 'N' THEN 'F' "
		cQuery +=            " ELSE "
		cQuery +=               " CASE "
		cQuery +=                  " WHEN SB1.B1_FANTASM = 'S' THEN 'T' "
		cQuery +=                  " WHEN SB1.B1_FANTASM = 'N' THEN 'F' "
		cQuery +=                  " ELSE 'F' "
		cQuery +=               " END "
		cQuery +=         " END "
		cQuery +=      " WHEN SG1.G1_FANTASM = '1' THEN 'T' "
		cQuery +=      " ELSE 'F' "
		cQuery +=   " END AS FANTASMA "

		cQuery +=  " FROM "      + RetSqlName("SG1") + " SG1 "
		cQuery += " INNER JOIN " + RetSqlName("SB1") + " SB1 "
		cQuery +=         " ON SB1.B1_FILIAL = '" + xFilial("SB1") + "' "
		cQuery +=        " AND SB1.B1_COD    = SG1.G1_COMP "
		cQuery +=        " AND SB1.D_E_L_E_T_ = ' ' "

		cQuery += " LEFT OUTER JOIN " + RetSqlName("SBZ") + " SBZ "
		cQuery +=              " ON SBZ.BZ_FILIAL  = '" + xFilial("SBZ") + "' "
		cQuery +=             " AND SB1.B1_COD     = SBZ.BZ_COD "
		cQuery +=             " AND SBZ.D_E_L_E_T_ = ' ' "

	Else
		cQuery +=   " CASE "
		cQuery +=      " WHEN SG1.G1_FANTASM = ' ' THEN "
		cQuery +=         " CASE "
		cQuery +=            " WHEN (SELECT SB1.B1_FANTASM "
		cQuery +=                    " FROM " + RetSqlName("SB1") + " SB1 "
		cQuery +=                   " WHERE SB1.B1_FILIAL  = '" + xFilial("SB1") + "' "
		cQuery +=                     " AND SB1.D_E_L_E_T_ = ' ' "
		cQuery +=                     " AND SB1.B1_COD     = SG1.G1_COMP ) = 'S' THEN 'T' "
		cQuery +=            " ELSE 'F' "
		cQuery +=         " END "
		cQuery +=      " WHEN SG1.G1_FANTASM = '1' THEN 'T' "
		cQuery +=      " ELSE 'F' "
		cQuery +=   " END AS FANTASMA "

		cQuery +=   " FROM " + RetSqlName("SG1") + " SG1 "
	EndIf
	cQuery += cQryCondic
	cQuery +=  " ORDER BY SG1.G1_FILIAL, "
	cQuery +=           " SG1.G1_COD, "
	cQuery +=           " SG1.G1_COMP, "
	cQuery +=           " SG1.G1_TRT "

	cQuery := ChangeQuery(cQuery)

	dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cQuery),cAliasQry,.T.,.T.)
	TcSetField(cAliasQry, "G1_INI"  , "D", 8        , 0)
	TcSetField(cAliasQry, "G1_FIM"  , "D", 8        , 0)
	TcSetField(cAliasQry, "G1_QUANT", "N", nTamQtd  , nDecQtd)
	TcSetField(cAliasQry, "G1_PERDA", "N", nTamPerda, nDecPerda)

	nAtual := 0

	//Monta os dados de cabe�alho do primeiro pai (se existir)
	If (cAliasQry)->(!Eof())
		cPaiAtu := (cAliasQry)->(G1_COD)
		nAtual++

		aDadosCab := Array(A200APICnt("ARRAY_ESTRU_CAB_SIZE"))
		//Dados do cabe�alho
		aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_FILIAL")] := (cAliasQry)->(G1_FILIAL)
		aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_PAI"   )] := (cAliasQry)->(G1_COD)
		aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_QBASE" )] := (cAliasQry)->(QTDBASE)
		aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_COMPON")] := {}
	EndIf

	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		If cPaiAtu != (cAliasQry)->(G1_COD)
			nAtual++
			cPaiAtu := (cAliasQry)->(G1_COD)
			PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
			GlbUnLock()

			aAdd(aDadosInc, aClone(aDadosCab))
			aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_FILIAL")] := (cAliasQry)->(G1_FILIAL)
			aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_PAI"   )] := (cAliasQry)->(G1_COD)
			aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_QBASE" )] := (cAliasQry)->(QTDBASE)
			aSize(aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_COMPON")], 0)
		EndIf

		aDadosCmp := Array(A200APICnt("ARRAY_ESTRU_CMP_SIZE"))
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_COMP"   )] := (cAliasQry)->G1_COMP
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_SEQ"    )] := (cAliasQry)->G1_TRT
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_QTDNEC" )] := (cAliasQry)->G1_QUANT
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_VLDINI" )] := (cAliasQry)->G1_INI
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_VLDFIM" )] := (cAliasQry)->G1_FIM
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_REVINI" )] := (cAliasQry)->G1_REVINI
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_REVFIM" )] := (cAliasQry)->G1_REVFIM
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_PERDA"  )] := (cAliasQry)->G1_PERDA
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_QTDFIXA")] := (cAliasQry)->G1_FIXVAR
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_POTENC" )] := (cAliasQry)->G1_POTENCI
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_GRPOPC" )] := (cAliasQry)->G1_GROPC
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_ITEMOPC")] := (cAliasQry)->G1_OPC
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_LOCAL"  )] := (cAliasQry)->G1_LOCCONS
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_FANTASM")] := Iif(AllTrim((cAliasQry)->FANTASMA) == "T", .T., .F.)
		aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_RECNO"  )] := (cAliasQry)->RECSG1
		If (cAliasQry)->ALTERNATIVOS > 0
			aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_ALTERNATIVO")] := A200APIAlt((cAliasQry)->(G1_COD), (cAliasQry)->G1_COMP)
		Else
			aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_ALTERNATIVO")] := {}
		EndIf
		aAdd(aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_COMPON")], aClone(aDadosCmp))

		aSize(aDadosCmp[A200APICnt("ARRAY_ESTRU_CMP_POS_ALTERNATIVO")], 0)
		aSize(aDadosCmp, 0)

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Adiciona o �ltimo pai no aDadosInc
	If nAtual > 0
		aAdd(aDadosInc, aClone(aDadosCab))
		aSize(aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_COMPON")], 0)
		aSize(aDadosCab, 0)
	EndIf

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta array de dados
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.

		aDadosCab := Array(A200APICnt("ARRAY_ESTRU_CAB_SIZE"))
		//Dados do cabe�alho
		aDadosCab[A200APICnt("ARRAY_ESTRU_CAB_POS_FILIAL")] := xFilial("SG1")

		aAdd(aDadosInc, aClone(aDadosCab))
		lOnlyDel := .T.
	EndIf

	If Len(aDadosInc) > 0
		PCPA200INT("SYNC", aDadosInc, oTTPend, Nil, @aSuccess, @aError, lOnlyDel)
	EndIf

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA131INT
	oTTPend:Delete()
	FreeObj(oTTPend)

	aSize(aDadosInc, 0)
Return

/*/{Protheus.doc} SincMalha
Sincroniza a Malha de Distribui��o
@author  Marcelo Neumann
@version P12
@since   07/10/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincMalha(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cAliasQry  := GetNextAlias()
	Local cCode      := ""
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nPosCab    := 0
	Local nPosExc    := 0
	Local nAtual     := 0
	Local nTamQtd    := GetSx3Cache("VQ_QNTINI", "X3_TAMANHO")
	Local nTamDec    := GetSx3Cache("VQ_QNTINI", "X3_DECIMAL")
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_MALHA_DISTR][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_MALHA_DISTR][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		COLUMN VQ_DTINI  AS DATE
		COLUMN VQ_DTFIM  AS DATE
		COLUMN VQ_QNTINI AS NUMERIC(nTamQtd, nTamDec)
		COLUMN VQ_QNTFIM AS NUMERIC(nTamQtd, nTamDec)
		SELECT 	SVD.VD_FILIAL  AS VD_FILIAL ,
				SVD.VD_ORIGEM  AS VD_ORIGEM ,
				SVD.VD_FILDEST AS VD_FILDEST,
				SVD.VD_DESTINO AS VD_DESTINO,
				SVQ.VQ_PRODUTO AS VQ_PRODUTO,
				SVQ.VQ_DTINI   AS VQ_DTINI  ,
				SVQ.VQ_DTFIM   AS VQ_DTFIM  ,
				SVQ.VQ_QNTINI  AS VQ_QNTINI ,
				SVQ.VQ_QNTFIM  AS VQ_QNTFIM ,
				SVD.R_E_C_N_O_ AS VD_R_E_C_N_O_
			FROM %Exp:cQryCondic%
			ORDER BY SVD.VD_FILIAL ,
					SVD.VD_ORIGEM ,
					SVD.VD_FILDEST,
					SVD.VD_DESTINO
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		If Empty(cCode) .Or. cCode <> (cAliasQry)->(VD_FILIAL + cValToChar(VD_R_E_C_N_O_))
			//Adiciona nova linha no array de inclus�o/atualiza��o
			aAdd(aDadosInc, Array(A123APICnt("ARRAY_MALHA_CAB_SIZE")))
			nPosCab := Len(aDadosInc)
			cCode   := (cAliasQry)->(VD_FILIAL + cValToChar(VD_R_E_C_N_O_))

			//Adiciona as informa��es no array de inclus�o/atualiza��o
			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_FILIAL")] := (cAliasQry)->VD_FILIAL
			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_ARMORG")] := (cAliasQry)->VD_ORIGEM
			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_FILDES")] := (cAliasQry)->VD_FILDEST
			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_ARMDES")] := (cAliasQry)->VD_DESTINO
			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_CODE"  )] := cCode
			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_LISTA" )] := {}
		EndIf

		If !Empty((cAliasQry)->VQ_PRODUTO)
			aAdd(aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_LISTA")], Array(A123APICnt("ARRAY_MALHA_EXCE_SIZE")))
			nPosExc := Len(aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_LISTA")])

			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_LISTA")][nPosExc][A123APICnt("ARRAY_MALHA_EXCE_POS_PROD"  )] := (cAliasQry)->VQ_PRODUTO
			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_LISTA")][nPosExc][A123APICnt("ARRAY_MALHA_EXCE_POS_DTINI" )] := (cAliasQry)->VQ_DTINI
			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_LISTA")][nPosExc][A123APICnt("ARRAY_MALHA_EXCE_POS_DTFIN" )] := (cAliasQry)->VQ_DTFIM
			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_LISTA")][nPosExc][A123APICnt("ARRAY_MALHA_EXCE_POS_QTDINI")] := (cAliasQry)->VQ_QNTINI
			aDadosInc[nPosCab][A123APICnt("ARRAY_MALHA_CAB_POS_LISTA")][nPosExc][A123APICnt("ARRAY_MALHA_EXCE_POS_QTDFIN")] := (cAliasQry)->VQ_QNTFIM
		EndIf

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(A123APICnt("ARRAY_MALHA_CAB_SIZE")))
		aDadosInc[1][A123APICnt("ARRAY_MALHA_CAB_POS_FILIAL")] := xFilial("SVD")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do MrpPurchaseRequestAPI.prw para integrar os registros
	PCPA123INT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA136INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincGrupo
Sincroniza Grupos de Armaz�m
@author  douglas.heydt
@version P12
@since   09/10/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincGrupo(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local cGrupo	 := ""
	Local lOnlyDel   := .F.
	Local nPosCab    := 0
	Local nPosLocal  := 0
	Local nAtual     := 0
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_GRUPO_ARM][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_GRUPO_ARM][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		SELECT SVN.VN_FILIAL AS VN_FILIAL,
				SVN.VN_LOCGRP AS VN_LOCGRP,
				SVN.VN_TIPO AS VN_TIPO,
				SVO.VO_LOCAL AS VO_LOCAL,
				SVO.VO_DEFAULT AS VO_DEFAULT
			FROM %Exp:cQryCondic%
			ORDER BY SVN.VN_FILIAL, SVN.VN_LOCGRP, SVO.VO_LOCAL
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		If cGrupo <> (cAliasQry)->(VN_FILIAL + VN_LOCGRP)
			//Adiciona nova linha no array de inclus�o/atualiza��o
			aAdd(aDadosInc, Array(A126APICnt("ARRAY_GRUPOARM_CAB_SIZE")))
			nPosCab := Len(aDadosInc)
			cGrupo := (cAliasQry)->(VN_FILIAL + VN_LOCGRP)

			//Adiciona as informa��es no array de inclus�o/atualiza��o
			aDadosInc[nPosCab][A126APICnt("ARRAY_GRUPOARM_CAB_POS_FILIAL")] := (cAliasQry)->VN_FILIAL
			aDadosInc[nPosCab][A126APICnt("ARRAY_GRUPOARM_CAB_POS_GRUPO")] := (cAliasQry)->VN_LOCGRP
			aDadosInc[nPosCab][A126APICnt("ARRAY_GRUPOARM_CAB_POS_TIPO")] := (cAliasQry)->VN_TIPO
			aDadosInc[nPosCab][A126APICnt("ARRAY_GRUPOARM_CAB_POS_LISTA" )] := {}
		EndIf
		aAdd(aDadosInc[nPosCab][A126APICnt("ARRAY_GRUPOARM_CAB_POS_LISTA")], Array(A126APICnt("ARRAY_GRUPOARM_ARM_SIZE")))
		nPosLocal := Len(aDadosInc[nPosCab][A126APICnt("ARRAY_GRUPOARM_CAB_POS_LISTA")])

		aDadosInc[nPosCab][A126APICnt("ARRAY_GRUPOARM_CAB_POS_LISTA")][nPosLocal][A126APICnt("ARRAY_GRUPOARM_ARM_POS_ARMAZEM"  )] := (cAliasQry)->VO_LOCAL
		aDadosInc[nPosCab][A126APICnt("ARRAY_GRUPOARM_CAB_POS_LISTA")][nPosLocal][A126APICnt("ARRAY_GRUPOARM_ARM_POS_DEFAULT" )] := (cAliasQry)->VO_DEFAULT
		aDadosInc[nPosCab][A126APICnt("ARRAY_GRUPOARM_CAB_POS_LISTA")][nPosLocal][A126APICnt("ARRAY_GRUPOARM_ARM_POS_CODE" )] := (cAliasQry)->(VN_FILIAL + VN_LOCGRP + VO_LOCAL)

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(A126APICnt("ARRAY_GRUPOARM_CAB_SIZE")))
		aDadosInc[1][A126APICnt("ARRAY_GRUPOARM_CAB_POS_FILIAL")] := xFilial("SVN")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do MrpPurchaseRequestAPI.prw para integrar os registros
	PCPA126INT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA136INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincProd
Sincroniza os Produtos
@author  marcelo.neumann
@version P12
@since   23/10/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincProd(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nPos       := 0
	Local nAtual     := 0
	Local nTamQE     := GetSx3Cache("B1_QE"    , "X3_TAMANHO")
	Local nDecQE     := GetSx3Cache("B1_QE"    , "X3_DECIMAL")
	Local nTamEMIN   := GetSx3Cache("B1_EMIN"  , "X3_TAMANHO")
	Local nDecEMIN   := GetSx3Cache("B1_EMIN"  , "X3_DECIMAL")
	Local nTamESTSEG := GetSx3Cache("B1_ESTSEG", "X3_TAMANHO")
	Local nDecESTSEG := GetSx3Cache("B1_ESTSEG", "X3_DECIMAL")
	Local nTamLE     := GetSx3Cache("B1_LE"    , "X3_TAMANHO")
	Local nDecLE     := GetSx3Cache("B1_LE"    , "X3_DECIMAL")
	Local nTamLM     := GetSx3Cache("B1_LM"    , "X3_TAMANHO")
	Local nDecLM     := GetSx3Cache("B1_LM"    , "X3_DECIMAL")
	Local nTamEMAX   := GetSx3Cache("B1_EMAX"  , "X3_TAMANHO")
	Local nDecEMAX   := GetSx3Cache("B1_EMAX"  , "X3_DECIMAL")
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_PRODUTOS][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_PRODUTOS][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		COLUMN B1_QE     AS NUMERIC(nTamQE    , nDecQE    )
		COLUMN B1_EMIN   AS NUMERIC(nTamEMIN  , nDecEMIN  )
		COLUMN B1_ESTSEG AS NUMERIC(nTamESTSEG, nDecESTSEG)
		COLUMN B1_LE     AS NUMERIC(nTamLE    , nDecLE    )
		COLUMN B1_LM     AS NUMERIC(nTamLM    , nDecLM    )
		COLUMN B1_EMAX   AS NUMERIC(nTamEMAX  , nDecEMAX  )
		SELECT SB1.B1_FILIAL  AS B1_FILIAL ,
				SB1.B1_COD     AS B1_COD    ,
				SB1.B1_LOCPAD  AS B1_LOCPAD ,
				SB1.B1_TIPO    AS B1_TIPO   ,
				SB1.B1_GRUPO   AS B1_GRUPO  ,
				SB1.B1_QE      AS B1_QE     ,
				SB1.B1_EMIN    AS B1_EMIN   ,
				SB1.B1_ESTSEG  AS B1_ESTSEG ,
				SB1.B1_PE      AS B1_PE     ,
				SB1.B1_TIPE    AS B1_TIPE   ,
				SB1.B1_LE      AS B1_LE     ,
				SB1.B1_LM      AS B1_LM     ,
				SB1.B1_TOLER   AS B1_TOLER  ,
				SB1.B1_TIPODEC AS B1_TIPODEC,
				SB1.B1_RASTRO  AS B1_RASTRO ,
				SB1.B1_MRP     AS B1_MRP    ,
				SB1.B1_REVATU  AS B1_REVATU ,
				SB1.B1_EMAX    AS B1_EMAX   ,
				SB1.B1_PRODSBP AS B1_PRODSBP,
				SB1.B1_LOTESBP AS B1_LOTESBP,
				SB1.B1_ESTRORI AS B1_ESTRORI,
				SB1.B1_APROPRI AS B1_APROPRI,
				SB1.B1_CPOTENC AS B1_CPOTENC,
				SB1.B1_MSBLQL  AS B1_MSBLQL ,
				SB1.B1_CONTRAT AS B1_CONTRAT,
				SB1.B1_OPERPAD AS B1_OPERPAD,
				SB1.B1_CCCUSTO AS B1_CCCUSTO,
				SVK.VK_HORFIX  AS VK_HORFIX ,
				SVK.VK_TPHOFIX AS VK_TPHOFIX
			FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(A010APICnt("ARRAY_PROD_SIZE")))
		nPos++

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_FILIAL"   )] := (cAliasQry)->B1_FILIAL
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_PROD"     )] := (cAliasQry)->B1_COD
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_LOCPAD"   )] := (cAliasQry)->B1_LOCPAD
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_TIPO"     )] := (cAliasQry)->B1_TIPO
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_GRUPO"    )] := (cAliasQry)->B1_GRUPO
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_QE"       )] := (cAliasQry)->B1_QE
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_EMIN"     )] := (cAliasQry)->B1_EMIN
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_ESTSEG"   )] := (cAliasQry)->B1_ESTSEG
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_PE"       )] := (cAliasQry)->B1_PE
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_TIPE"     )] := M010CnvFld("B1_TIPE"   , (cAliasQry)->B1_TIPE)
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_LE"       )] := (cAliasQry)->B1_LE
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_LM"       )] := (cAliasQry)->B1_LM
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_TOLER"    )] := (cAliasQry)->B1_TOLER
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_TIPDEC"   )] := M010CnvFld("B1_TIPODEC", (cAliasQry)->B1_TIPODEC)
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_RASTRO"   )] := M010CnvFld("B1_RASTRO" , (cAliasQry)->B1_RASTRO)
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_MRP"      )] := M010CnvFld("B1_MRP"    , (cAliasQry)->B1_MRP)
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_REVATU"   )] := (cAliasQry)->B1_REVATU
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_EMAX"     )] := (cAliasQry)->B1_EMAX
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_PROSBP"   )] := M010CnvFld("B1_PRODSBP", (cAliasQry)->B1_PRODSBP)
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_LOTSBP"   )] := (cAliasQry)->B1_LOTESBP
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_ESTORI"   )] := (cAliasQry)->B1_ESTRORI
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_APROPR"   )] := M010CnvFld("B1_APROPRI", (cAliasQry)->B1_APROPRI)
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_CPOTEN"   )] := (cAliasQry)->B1_CPOTENC
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_HORFIX"   )] := (cAliasQry)->VK_HORFIX
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_TPHFIX"   )] := (cAliasQry)->VK_TPHOFIX
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_NUMDEC"   )] := "0" //Protheus n�o utiliza esse campo, passar 0 fixo
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_IDREG"    )] := (cAliasQry)->B1_FILIAL+(cAliasQry)->B1_COD
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_BLOQUEADO")] := (cAliasQry)->B1_MSBLQL
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_CONTRATO" )] := M010CnvFld("B1_CONTRAT", (cAliasQry)->B1_CONTRAT)
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_ROTEIRO"  )] := (cAliasQry)->B1_OPERPAD
		aDadosInc[nPos][A010APICnt("ARRAY_PROD_POS_CCUSTO"   )] := (cAliasQry)->B1_CCCUSTO

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(A010APICnt("ARRAY_PROD_SIZE")))
		aDadosInc[1][A010APICnt("ARRAY_PROD_POS_FILIAL")] := xFilial("SB1")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do MrpPurchaseRequestAPI.prw para integrar os registros
	MATA010INT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na PCPA136INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincOpComp
Sincroniza as Opera��es por Componente
@author  brunno.costa
@version P12
@since   13/04/2020
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincOpComp(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nPos       := 0
	Local nAtual     := 0
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_OPER_COMPON][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_OPER_COMPON][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		SELECT  GF_FILIAL,
		        GF_PRODUTO,
				GF_ROTEIRO,
				GF_OPERAC,
				GF_COMP,
				GF_TRT
			FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(A637APICnt("ARRAY_SIZE")))
		nPos++

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][A637APICnt("ARRAY_POS_FILIAL"     )] := (cAliasQry)->GF_FILIAL
		aDadosInc[nPos][A637APICnt("ARRAY_POS_PRODUTO"    )] := (cAliasQry)->GF_PRODUTO
		aDadosInc[nPos][A637APICnt("ARRAY_POS_ROTEIRO"    )] := (cAliasQry)->GF_ROTEIRO
		aDadosInc[nPos][A637APICnt("ARRAY_POS_OPERACAO"   )] := (cAliasQry)->GF_OPERAC
		aDadosInc[nPos][A637APICnt("ARRAY_POS_COMPONENTE" )] := (cAliasQry)->GF_COMP
		aDadosInc[nPos][A637APICnt("ARRAY_POS_TRT"        )] := (cAliasQry)->GF_TRT
		aDadosInc[nPos][A637APICnt("ARRAY_POS_IDREG"      )] := (cAliasQry)->(GF_FILIAL + GF_PRODUTO + GF_ROTEIRO + GF_OPERAC + GF_COMP + GF_TRT)

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(A637APICnt("ARRAY_SIZE")))
		aDadosInc[1][A637APICnt("ARRAY_POS_FILIAL")] := xFilial("SGF")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do MRPBOMROUTING.prw para integrar os registros
	MATA637INT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, /*cUUID*/, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na MATA019INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} SincIndPrd
Sincroniza os Produtos
@author  renan.roeder
@version P12
@since   19/11/2019
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincIndPrd(cJobName)

	Local aDadosInc  := {}
	Local aSuccess   := {}
	Local aError     := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nPos       := 0
	Local nAtual     := 0
	Local nTamQE     := GetSx3Cache("BZ_QE"    , "X3_TAMANHO")
	Local nDecQE     := GetSx3Cache("BZ_QE"    , "X3_DECIMAL")
	Local nTamEMIN   := GetSx3Cache("BZ_EMIN"  , "X3_TAMANHO")
	Local nDecEMIN   := GetSx3Cache("BZ_EMIN"  , "X3_DECIMAL")
	Local nTamESTSEG := GetSx3Cache("BZ_ESTSEG", "X3_TAMANHO")
	Local nDecESTSEG := GetSx3Cache("BZ_ESTSEG", "X3_DECIMAL")
	Local nTamLE     := GetSx3Cache("BZ_LE"    , "X3_TAMANHO")
	Local nDecLE     := GetSx3Cache("BZ_LE"    , "X3_DECIMAL")
	Local nTamLM     := GetSx3Cache("BZ_LM"    , "X3_TAMANHO")
	Local nDecLM     := GetSx3Cache("BZ_LM"    , "X3_DECIMAL")
	Local nTamEMAX   := GetSx3Cache("BZ_EMAX"  , "X3_TAMANHO")
	Local nDecEMAX   := GetSx3Cache("BZ_EMAX"  , "X3_DECIMAL")
	Local oTTPend

	//Cria tempor�rias para a PCPA136INT
	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_INDICA_PROD][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_INDICA_PROD][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		COLUMN BZ_QE     AS NUMERIC(nTamQE    , nDecQE    )
		COLUMN BZ_EMIN   AS NUMERIC(nTamEMIN  , nDecEMIN  )
		COLUMN BZ_ESTSEG AS NUMERIC(nTamESTSEG, nDecESTSEG)
		COLUMN BZ_LE     AS NUMERIC(nTamLE    , nDecLE    )
		COLUMN BZ_LM     AS NUMERIC(nTamLM    , nDecLM    )
		COLUMN BZ_EMAX   AS NUMERIC(nTamEMAX  , nDecEMAX  )
		SELECT BZ_FILIAL ,
				BZ_COD    ,
				BZ_LOCPAD ,
				BZ_QE     ,
				BZ_EMIN   ,
				BZ_ESTSEG ,
				BZ_PE     ,
				BZ_TIPE   ,
				BZ_LE     ,
				BZ_LM     ,
				BZ_TOLER  ,
				BZ_MRP    ,
				BZ_REVATU ,
				BZ_EMAX   ,
				BZ_HORFIX ,
				BZ_TPHOFIX
			FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(A019APICnt("ARRAY_IND_PROD_POS_SIZE")))
		nPos++

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_FILIAL")] := (cAliasQry)->BZ_FILIAL
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_PROD"  )] := (cAliasQry)->BZ_COD
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_LOCPAD")] := (cAliasQry)->BZ_LOCPAD
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_QE"    )] := (cAliasQry)->BZ_QE
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_EMIN"  )] := (cAliasQry)->BZ_EMIN
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_ESTSEG")] := (cAliasQry)->BZ_ESTSEG
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_PE"    )] := (cAliasQry)->BZ_PE
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_TIPE"  )] := M019CnvFld("BZ_TIPE"   , (cAliasQry)->BZ_TIPE)
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_LE"    )] := (cAliasQry)->BZ_LE
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_LM"    )] := (cAliasQry)->BZ_LM
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_TOLER" )] := (cAliasQry)->BZ_TOLER
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_MRP"   )] := M019CnvFld("BZ_MRP"    , (cAliasQry)->BZ_MRP)
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_REVATU")] := (cAliasQry)->BZ_REVATU
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_EMAX"  )] := (cAliasQry)->BZ_EMAX
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_HORFIX")] := (cAliasQry)->BZ_HORFIX
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_TPHFIX")] := (cAliasQry)->BZ_TPHOFIX
		aDadosInc[nPos][A019APICnt("ARRAY_IND_PROD_POS_IDREG" )] := (cAliasQry)->BZ_FILIAL+(cAliasQry)->BZ_COD

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(A019APICnt("ARRAY_IND_PROD_POS_SIZE")))
		aDadosInc[1][A019APICnt("ARRAY_IND_PROD_POS_FILIAL")] := xFilial("SBZ")
		lOnlyDel := .T.
	EndIf

	//Chama a fun��o do mrpproductindicator.prw para integrar os registros
	MATA019INT("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	//Elimina as tempor�rias usadas na MATA019INT
	oTTPend:Delete()
	FreeObj(oTTPend)

Return

/*/{Protheus.doc} CalculaReg
Prepara o filtro e calcula a quantidade de registros a serem processados
@author  Marcelo Neumann
@version P12
@since   17/07/2019
@param   nIndTela, num�rico, indicador da posi��o do item na tela
@return  QUANT, num�rico, quantidade de registros encontrados com o filtro
/*/
Static Function CalculaReg(nIndTela)

	Local cAliasQry  := GetNextAlias()
	Local cQuery     := ""
	Local cQryCondic := ""
	Local cBkpQuery  := ""
	Local cBanco := AllTrim(Upper(TcGetDb()))

	//Se n�o estiver checado, n�o processa
	If !aParTela[nIndTela][IND_PAR_CHECKED]
		Return 0
	EndIf

	cQryCondic := " WHERE D_E_L_E_T_ = ' ' "

	//Ajustes Query
	Do Case
		Case nIndTela == IND_TELA_EMPENHOS
			cQryCondic := " SD4 INNER JOIN " + RetSqlName("SC2") + " SC2 ON "

			If cBanco == "POSTGRES"
				cQryCondic += " TRIM(SD4.D4_OP) = TRIM((SC2.C2_NUM || SC2.C2_ITEM || SC2.C2_SEQUEN || SC2.C2_ITEMGRD )) "
			Else
				cQryCondic += " SD4.D4_OP = (SC2.C2_NUM || SC2.C2_ITEM || SC2.C2_SEQUEN || SC2.C2_ITEMGRD ) "
			EndIf

			cQryCondic += " AND SD4.D4_QUANT <> 0 ";
						+ " AND SD4.D_E_L_E_T_ = ' ' ";
						+ " AND SD4.D4_FILIAL = '" + xFilial("SD4") + "' ";
						+ " AND SC2.C2_FILIAL = '" + xFilial("SC2") + "' ";
						+ " AND SC2.D_E_L_E_T_ = ' ' ";
						+ " AND SC2.C2_DATRF = ' ' ";
						+ " AND (SC2.C2_QUANT-SC2.C2_QUJE - ";
						+ Iif(SuperGetMV("MV_PERDINF",.F.,.F.), "0", "C2_PERDA") + ") >= 0 ";
						+ " GROUP BY D4_FILIAL, D4_COD,D4_OP,D4_OPORIG,D4_DATA,D4_TRT,D4_LOCAL"
			cBkpQuery := cQryCondic
			cQryCondic := " (SELECT D4_FILIAL, D4_COD,D4_OPORIG,D4_DATA,D4_TRT,D4_LOCAL FROM " + RetSqlName("SD4") + cQryCondic +" )"

			If cBanco == "ORACLE"
				cQryCondic += " QUANT"
			Else
				cQryCondic += " AS QUANT"
			EndIf

		Case nIndTela == IND_TELA_ESTOQUES
			cQryCondic := " ( ";
			          + " SELECT  branchId, ";
				      + " product, ";
				      + " warehouse, ";
				      + " lot, ";
				      + " sublot, ";
				      + " expirationDate, ";
				      + " SUM(availableQuantity) as availableQuantity, ";
				      + " SUM(consignedOut) as consignedOut, ";
				      + " SUM(consignedIn) as consignedIn, ";
				      + " SUM(unavailableQuantity) as unavailableQuantity, ";
					  + " SUM(blockedBalance) as blockedBalance ";
				      + " FROM ";
						    + " (SELECT SB2.B2_FILIAL as branchId, ";
									+ " SB2.B2_COD    as product, ";
									+ " SB2.B2_LOCAL  as warehouse, ";
									+ " ''            as lot, ";
									+ " ''            as sublot, ";
									+ " ''            as expirationDate, ";
									+ " (CASE WHEN B1_RASTRO IN ('L', 'S') THEN 0 ELSE B2_QATU  END) as availableQuantity, ";
									+ " SB2.B2_QNPT   as consignedOut, ";
									+ " SB2.B2_QTNP   as consignedIn, ";
									+ " 0             as unavailableQuantity, ";
									+ " 0             as blockedBalance ";
									+ " FROM " + RetSqlName("SB2");
									+ " SB2 INNER JOIN (SELECT B1_COD, B1_RASTRO ";
									+                   " FROM " + RetSqlName("SB1");
									+                  " WHERE D_E_L_E_T_ = ' ' ";
									+                    " AND B1_FILIAL = '" + xFilial("SB1") + "' ) SB1 ";
									+ " 	ON SB2.B2_COD = SB1.B1_COD ";
							+ " WHERE SB2.D_E_L_E_T_ = ' ' ";
							+   " AND SB2.B2_FILIAL  = '" + xFilial("SB2") + "' ";
							+ " UNION ";
							+ " SELECT  SB8.B8_FILIAL  as branchId, ";
									+ " SB8.B8_PRODUTO as product, ";
									+ " SB8.B8_LOCAL   as warehouse, ";
									+ " SB8.B8_LOTECTL as lot, ";
									+ " SB8.B8_NUMLOTE as sublot, ";
									+ " SB8.B8_DTVALID as expirationDate, ";
									+ " SB8.B8_SALDO   as availableQuantity, ";
									+ " 0              as consignedOut, ";
									+ " 0              as consignedIn, ";
									+ " 0              as unavailableQuantity, ";
									+ " 0              as blockedBalance";
							+ " FROM " + RetSqlName("SB8") + " SB8 ";
								+ " INNER JOIN (SELECT B1_COD, B1_RASTRO ";
											+ " FROM " + RetSqlName("SB1");
											+ " WHERE D_E_L_E_T_ = ' ' ";
											+ " AND B1_FILIAL = '" + xFilial("SB1") + "' ";
											+ " AND B1_RASTRO IN ('L', 'S')) SB1 ";
												+ " ON SB8.B8_PRODUTO = SB1.B1_COD ";
							+ " WHERE SB8.D_E_L_E_T_ = ' ' ";
							+ " AND SB8.B8_SALDO > 0 ";
							+ " AND SB8.B8_FILIAL = '" + xFilial("SB8") + "' ";
							+ " UNION ";
							+ " SELECT	SDD.DD_FILIAL   as branchId, ";
									+ "	SDD.DD_PRODUTO  as product, ";
									+ "	SDD.DD_LOCAL    as warehouse, ";
									+ "	SDD.DD_LOTECTL  as lot, ";
									+ "	SDD.DD_NUMLOTE  as sublot, ";
									+ "	SB8b.B8_DTVALID as expirationDate, ";
									+ "	0			    as availableQuantity, ";
									+ "	0               as consignedOut, ";
									+ "	0               as consignedIn, ";
									+ "	0               as unavailableQuantity, ";
									+ "	SDD.DD_SALDO    as blockedBalance ";
							+ " FROM "+RetSqlName("SDD")+" SDD ";
							 	+ " INNER JOIN ( SELECT B8_PRODUTO, B8_DTVALID, B8_LOCAL, B8_LOTECTL, B8_NUMLOTE ";
										+ " FROM "+RetSqlName("SB8");
										+ " WHERE D_E_L_E_T_ = ' ' AND B8_FILIAL = '"+xFilial("SB8")+"' ) SB8b ";
								 		+ " ON SB8b.B8_PRODUTO = SDD.DD_PRODUTO ";
										+ " AND SB8b.B8_LOCAL = SDD.DD_LOCAL ";
										+ " AND SB8b.B8_LOTECTL = SDD.DD_LOTECTL ";
										+ " AND SB8b.B8_NUMLOTE = SDD.DD_NUMLOTE ";
							+ " WHERE SDD.D_E_L_E_T_ = ' '  AND SDD.DD_SALDO > 0 AND SDD.DD_MOTIVO <> 'VV' AND SDD.DD_FILIAL = '"+xFilial("SDD")+"' ";
							+") SB2a";
						+ " GROUP BY branchId, ";
						         + " product, ";
						         + " warehouse, ";
						         + " lot, ";
						         + " sublot, ";
						         + " expirationDate ";
						+ " HAVING ";
						        + " SUM(availableQuantity)";
								 + " +SUM(consignedOut)";
								 + " +SUM(consignedIn)";
								 + " +SUM(unavailableQuantity)";
								 + " +SUM(blockedBalance) != 0 ";
						+ " ) SB2b "

		Case nIndTela == IND_TELA_PED_COMPRAS
			cQryCondic += " AND C7_FILIAL  = '" + xFilial("SC7") + "' ";
			            + " AND C7_QUJE    < C7_QUANT ";
						+ " AND C7_RESIDUO = '" + CriaVar("C7_RESIDUO",.F.) + "' "

		Case nIndTela == IND_TELA_CQ
			cQryCondic := " ( "
			cQryCondic +=  " SELECT branchId, "
			cQryCondic +=         " product, "
			cQryCondic +=         " SUM(quantity) as quantity, "
			cQryCondic +=         " warehouse, "
			cQryCondic +=         " invoiceDate, "
			cQryCondic +=         " SUM(returnedQuantity) as returnedQuantity "
			cQryCondic +=    " FROM (
			cQryCondic +=          " SELECT SD7.D7_FILIAL         as branchId, "
			cQryCondic +=                 " SD7.D7_PRODUTO        as product, "
			cQryCondic +=                 " SD7.D7_QTDE           as quantity, "
			cQryCondic +=                 " SD7.D7_LOCAL          as warehouse, "
			cQryCondic +=                 " SD7.D7_DATA           as invoiceDate, "
			cQryCondic +=                 " COALESCE(D2_QUANT, 0) as returnedQuantity "
			cQryCondic +=            " FROM (SELECT D7_FILIAL, D7_PRODUTO, SUM(D7_QTDE) as D7_QTDE, D7_LOCAL, D7_DATA, D7_FORNECE, D7_LOJA, D7_DOC, D7_SERIE, D7_TIPO "
			cQryCondic +=                  " FROM " + RetSqlName("SD7")
			cQryCondic +=                  " WHERE D7_TIPO IN (2,6) "
			cQryCondic +=                    " AND D7_FILIAL = '" + xFilial("SD7") + "' "
			cQryCondic +=                    " AND D7_ESTORNO <> 'S' "
			cQryCondic +=                  " GROUP BY D7_FILIAL, D7_PRODUTO, D7_LOCAL, D7_DATA, D7_FORNECE, D7_LOJA, D7_DOC, D7_SERIE, D7_TIPO) SD7 "
			cQryCondic +=              " LEFT JOIN ( SELECT SUM(D2_QUANT) D2_QUANT, "
			cQryCondic +=                                 " D2_FILIAL, "
			cQryCondic +=                                 " D2_TIPO, "
			cQryCondic +=                                 " D2_CLIENTE, "
			cQryCondic +=                                 " D2_LOJA, "
			cQryCondic +=                                 " D2_NFORI, "
			cQryCondic +=                                 " D2_SERIORI, "
			cQryCondic +=                                 " D2_COD "
			cQryCondic +=                           " FROM " + RetSqlName("SD2") + " SD2 "
			cQryCondic +=                          " WHERE D_E_L_E_T_ = ' ' "
			cQryCondic +=                                " AND D2_TIPO = 'D' "
			cQryCondic +=                                " AND D2_FILIAL = '" + xFilial("SD2") + "' "
			cQryCondic +=                           " GROUP BY
			cQryCondic +=                                 " D2_FILIAL, "
			cQryCondic +=                                 " D2_TIPO, "
			cQryCondic +=                                 " D2_CLIENTE, "
			cQryCondic +=                                 " D2_LOJA, "
			cQryCondic +=                                 " D2_NFORI, "
			cQryCondic +=                                 " D2_SERIORI, "
			cQryCondic +=                                 " D2_COD ) SD2a "
			cQryCondic +=               " ON  SD7.D7_FORNECE = SD2a.D2_CLIENTE "
			cQryCondic +=               " AND SD7.D7_LOJA    = SD2a.D2_LOJA "
			cQryCondic +=               " AND SD7.D7_DOC     = SD2a.D2_NFORI "
			cQryCondic +=               " AND SD7.D7_SERIE   = SD2a.D2_SERIORI "
			cQryCondic +=               " AND SD7.D7_PRODUTO = SD2a.D2_COD "
			cQryCondic +=               " AND SD7.D7_TIPO    = 2 "
			cQryCondic +=            " ) SD7a"
			cQryCondic +=          " GROUP BY branchId, "
			cQryCondic +=                   " product, "
			cQryCondic +=                   " warehouse, "
			cQryCondic +=                   " invoiceDate "
			cQryCondic += " ) SD7b "

		Case nIndTela == IND_TELA_ORDEM_PROD
			cQryCondic += QryCondSC2(.T.)

		Case nIndTela == IND_TELA_SOL_COMPRAS
			cQryCondic += " AND C1_FILIAL  = '" + xFilial("SC1") + "' ";
			            + " AND C1_QUJE    < C1_QUANT ";
						+ " AND C1_RESIDUO = '" + CriaVar("C1_RESIDUO",.F.) + "' ";
						+ IIf(SuperGetMV("MV_MRPSCRE",.F.,.T.), "", " AND C1_ORIGEM <> 'MATA106' ")

		Case nIndTela == IND_TELA_ESTRUTURA
			cQryCondic := " WHERE SG1.D_E_L_E_T_ = ' ' "
			cQryCondic +=   " AND SG1.G1_FILIAL = '" + xFilial("SG1") + "' "
			cBkpQuery  := "%" + RetSqlName(aParTela[nIndTela][IND_PAR_ALIAS]) + " SG1 " + cQryCondic + "%"
			cQryCondic := " (SELECT DISTINCT ";
			                    + " SG1.G1_FILIAL, SG1.G1_COD ";
						 + " FROM " + RetSqlName("SG1") + " SG1 ";
						 +  cQryCondic + ") CNT "

		Case nIndTela == IND_TELA_MALHA_DISTR
			cQryCondic :=  " SVD LEFT OUTER JOIN " + RetSqlName("SVQ") + " SVQ"             + ;
						  	           " ON SVD.VD_FILIAL  = '" + xFilial("SVD") + "'" + ;
									  " AND SVQ.VQ_FILIAL  = '" + xFilial("SVQ") + "'" + ;
						  	          " AND SVQ.VQ_ORIGEM  = SVD.VD_ORIGEM"            + ;
						  	          " AND SVQ.VQ_DESTINO = SVD.VD_DESTINO"           + ;
						  	          " AND SVQ.D_E_L_E_T_ = ' '"                      + ;
					     " WHERE SVD.D_E_L_E_T_ = ' ' "                                + ;
						   " AND SVD.VD_FILIAL  = '" + xFilial("SVD") + "' "

		Case nIndTela == IND_TELA_GRUPO_ARM
			cQryCondic :=  " SVN LEFT OUTER JOIN " + RetSqlName("SVO") + " SVO"            + ;
						  	           " ON SVN.VN_FILIAL = '" + xFilial("SVN") + "'" + ;
									  " AND SVO.VO_FILIAL = '" + xFilial("SVO") + "'" + ;
						  	          " AND SVN.VN_LOCGRP = SVO.VO_LOCGRP"            + ;
									  " AND SVO.D_E_L_E_T_ = ' '"                     + ;
					      " WHERE SVN.D_E_L_E_T_ = ' ' "                              + ;
						  " AND SVN.VN_FILIAL  = '" + xFilial("SVN") + "' "

		Case nIndTela == IND_TELA_PRODUTOS
			cQryCondic :=  " SB1 LEFT OUTER JOIN " + RetSqlName("SVK") + " SVK"             + ;
						  	           " ON SB1.B1_FILIAL  = '" + xFilial("SB1") + "'" + ;
									  " AND SVK.VK_FILIAL  = '" + xFilial("SVK") + "'" + ;
						  	          " AND SVK.VK_COD     = SB1.B1_COD"               + ;
						  	          " AND SVK.D_E_L_E_T_ = ' '"                      + ;
					      " WHERE SB1.D_E_L_E_T_ = ' ' "                               + ;
						  " AND SB1.B1_FILIAL  = '" + xFilial("SB1") + "' "

		Otherwise
			cQryCondic += " AND "
			If Left(aParTela[nIndTela][IND_PAR_ALIAS], 1) == "S"
				cQryCondic += SubStr(aParTela[nIndTela][IND_PAR_ALIAS] + "_FILIAL = '" + xFilial(aParTela[nIndTela][IND_PAR_ALIAS]) + "' ", 2)
			Else
				cQryCondic += aParTela[nIndTela][IND_PAR_ALIAS] + "_FILIAL = '" + xFilial(aParTela[nIndTela][IND_PAR_ALIAS]) + "' "
			EndIf
	EndCase

	If nIndTela != IND_TELA_ESTOQUES .AND. nIndTela != IND_TELA_ESTRUTURA .AND. nIndTela != IND_TELA_CQ
		If nIndTela == IND_TELA_EMPENHOS
			cBkpQuery := "%" + RetSqlName(aParTela[nIndTela][IND_PAR_ALIAS]) + cBkpQuery + "%"
			cQryCondic := "%" + cQryCondic + "%"

		Else
			cQryCondic := "%" + RetSqlName(aParTela[nIndTela][IND_PAR_ALIAS]) + cQryCondic + "%"
		EndIf
	Else
		cQryCondic := "%" + cQryCondic + "%"

	EndIf

	If "MSSQL" $ cBanco
		If nIndTela == IND_TELA_EMPENHOS
			cBkpQuery := StrTran(cBkpQuery, "||", "+")
		Endif

		cQryCondic := StrTran(cQryCondic, "||", "+")
	EndIf

	cQuery := " SELECT COUNT(*) QTDREG "
	cQuery +=   " FROM " + StrTran(cQryCondic, "%", " ")

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQry,.T.,.T.)

	aParTela[nIndTela][IND_PAR_QTD_TOTAL] := (cAliasQry)->QTDREG
	If Empty(cBkpQuery)
		aParTela[nIndTela][IND_PAR_QUERY] := cQryCondic
	Else
	   	aParTela[nIndTela][IND_PAR_QUERY] := cBkpQuery
	EndIf

	(cAliasQry)->(dbCloseArea())

Return aParTela[nIndTela][IND_PAR_QTD_TOTAL]

/*/{Protheus.doc} ResultProc
Exibe uma tela com o resultado da sincroniza��o
@author  Marcelo Neumann
@version P12
@since   17/07/2019
@return  NIL
/*/
Static Function ResultProc(aDados)

	Local aLinhas := {}
	Local nInd    := 0
	Local oDlgResult
	Local oOk 	:= LoadBitmap(GetResources(),'br_verde')
	Local oWArn := LoadBitmap(GetResources(),'br_amarelo')
	Local oErr  := LoadBitmap(GetResources(),'br_vermelho')
	Local oLegenda

	Private aProc   := aDados
	Private cMsgRet := ""
	Private oBrowse
	Private oMsgRet

	For nInd := 1 To Len(aProc)
		If !Empty(aProc[nInd][IND_PAR_MSG_ERRO]) .And. ( aProc[1][IND_PAR_ALIAS] != "VAL" .Or. nInd == 1 )
			oLegenda := oErr
		ElseIf aProc[nInd][IND_PAR_QTD_ERRO] > 0 .Or. !aProc[nInd][IND_PAR_PROCESSADO]
			oLegenda := oWArn
		Else
			oLegenda := oOk
		EndIf

		aAdd(aLinhas, { oLegenda    					  , ;
						aProc[nInd][IND_PAR_DESCRICAO]    , ;
						aProc[nInd][IND_PAR_QTD_SUCESSO]  , ;
						aProc[nInd][IND_PAR_QTD_ERRO]})
	Next nInd

	If Len(aLinhas) < 1
		aAdd(aLinhas, {"", "", ""})
	EndIf

	DEFINE DIALOG oDlgResult TITLE STR0023 FROM 0,0 TO 520,460 PIXEL //"Resultado" //350 460

	oPanelSup := TPanel():New(0,0,,oDlgResult,,,,,, 400, 230, .F., .T.)
	oPanelSup:Align := CONTROL_ALIGN_TOP

	oBrowse := TWBrowse():New(01,01,230,190, ,{" ",STR0055,STR0025,STR0026},{10,140,30,30},oPanelSup,,,,,{||},,,,,,,.F.,,.T.,,.F.,,,) //"Rotina" "Sucesso" "Erro"
	oBrowse:SetArray(aLinhas)
	oBrowse:bLine := {|| { aLinhas[oBrowse:nAt,1], aLinhas[oBrowse:nAt,2], aLinhas[oBrowse:nAt,3], aLinhas[oBrowse:nAt,4]}}
	oBrowse:bChange := { || AlteraMemo(oBrowse:nAt) }

	oPanelInf := TPanel():New(190,0,,oDlgResult,,,,,, 400, 50, .F., .F.)

	TSay():New(05, 05, {|| STR0056 }, oPanelInf, , , , , , .T., , , 100, 20) //"Mensagem:"
	oMsgRet := tMultiget():new(15, 05, {|u| If(PCount()==0,cMsgRet,cMsgRet:=u)}, oPanelInf,220,30,,,,,,.T.,,,{||.T.},,,.T.)

	DEFINE SBUTTON FROM 245, 205 TYPE 1 ACTION (oDlgResult:End()) ENABLE OF oDlgResult

	ACTIVATE MSDIALOG oDlgResult CENTERED

Return

/*/{Protheus.doc} AlteraMemo
Atualiza��o do campo Memo da tela
@author  Douglas.heydt
@version P12
@since   05/11/2019
@return  NIL
/*/
Static Function AlteraMemo(nAt)

	cMsgRet := ""

	If !Empty(aProc[nAt][IND_PAR_MSG_ERRO])
		cMsgRet += STR0057 + aProc[nAt][IND_PAR_MSG_ERRO]//"Ocorreu uma falha de processamento: "
	Else
		cMsgRet += STR0058 + cValToChar(aProc[nAt][IND_PAR_QTD_SUCESSO])+CRLF //"Registros processados com sucesso: "
		cMsgRet += STR0059 + cValToChar(aProc[nAt][IND_PAR_QTD_ERRO])+CRLF //"Registros processados com erro (n�o sincronizados): "
	EndIf

	SetFocus(oMsgRet:HWND)
	SetFocus(oBrowse:HWND)

Return NIl

/*/{Protheus.doc} logSync
Verifica se existem APIs que devem ser sincronizadas e exibe alerta ao usu�rio.

@type  Static Function
@author lucas.franca
@since 29/07/2019
@version P12.1.28
@return .T.
/*/
Static Function logSync()

	MRPVldSync(.T.)

Return .T.

/*/{Protheus.doc} UpdateT4P
Atualiza o campo T4P_ALTER para as APIs que foram processadas.

@type  Static Function
@author lucas.franca
@since 29/07/2019
@version P12.1.28
@return .T.
/*/
Static Function UpdateT4P()
	Local cApiAtu_0 := ""
	Local cApiAtu_1 := ""
	Local nIndex    := 0

	For nIndex := 1 To Len(aParTela)
		If aParTela[nIndex][IND_PAR_CHECKED]
			If Empty(aParTela[nIndex][IND_PAR_MSG_ERRO])
				If aParTela[nIndex][IND_PAR_PROCESSADO]
					If !Empty(cApiAtu_0)
						cApiAtu_0 += ","
					EndIf
					cApiAtu_0 += "'" + aParTela[nIndex][IND_PAR_API] + "' "
				EndIf
			Else
				If !Empty(cApiAtu_1)
					cApiAtu_1 += ","
				EndIf
				cApiAtu_1 += "'" + aParTela[nIndex][IND_PAR_API] + "' "
			EndIf
		EndIf
	Next nIndex

	ExecUpdT4P(cApiAtu_0, "0")
	ExecUpdT4P(cApiAtu_1, "1")

Return .T.

/*/{Protheus.doc} ExecUpdT4P
Executa o UPDATE do campo T4P_ALTER

@type  Static Function
@author marcelo.neumann
@since 24/03/2020
@version P12.1.28
@param 01 cApis , Character, APIsa serem atualizadas (instru��o IN)
@param 02 cAlter, Character, Indicador a ser gravado na coluna T4P_ALTER
@return Nil
/*/
Static Function ExecUpdT4P(cApis, cAlter)

	Local cUpdate := ""

	If !Empty(cApis)
		cUpdate := "UPDATE " + RetSqlName("T4P")                  + ;
		             " SET T4P_ALTER  = '" + cAlter         + "'" + ;
				   " WHERE T4P_FILIAL = '" + xFilial("T4P") + "'" + ;
		             " AND D_E_L_E_T_ = ' '"                      + ;
		             " AND T4P_API IN (" + cApis + ")"

		If TcSqlExec(cUpdate) < 0
			Final(STR0028, tcSQLError()) //"Erro ao atualizar as parametriza��es do MRP."
		EndIf
	EndIf

Return Nil

/*/{Protheus.doc} limpaT4R
Limpa as pend�ncias (T4R) de determinada API.

@type  Static Function
@author lucas.franca
@since 30/07/2019
@version P12.1.28
@param cApi, Character, C�digo da API que ser� utilizado para limpar a tabela T4R
@return Nil
/*/
Static Function limpaT4R(cApi)
	Local cSqlDel := ""

	cSqlDel := " DELETE FROM " + RetSqlName("T4R")
	cSqlDel +=  " WHERE T4R_FILIAL = '" + xFilial("T4R") + "' "
	cSqlDel +=    " AND T4R_API    = '" + cApi + "' "

	If TcSqlExec(cSqlDel) < 0
		Final(STR0029, tcSQLError()) //"Erro ao excluir as pend�ncias do MRP."
	EndIf
Return Nil

/*/{Protheus.doc} QryCondSC2
Retorna a query utilizada na busca das Ordens de Produ��o.

@type  Static Function
@author marcelo.neumann
@since 13/08/2019
@version P12.1.28
@param lSql, Logical, Indica se deve retornar o filtro no formato SQL ou ADVPL
@return cQryCondic, Character, Query para filtro na SC2 no formato SQL ou ADVPL
/*/
Static Function QryCondSC2(lSql)
	Local cQryCondic := ""
	Local cPerda     := "C2_PERDA"

	If SuperGetMV("MV_PERDINF",.F.,.F.)
		cPerda := "0"
	EndIf

	If lSql
		cQryCondic := " AND C2_DATRF = ' ' AND (C2_QUANT - C2_QUJE - " + cPerda + ") > 0 "
		cQryCondic += " AND C2_FILIAL = '" + xFilial("SC2") + "' "
	Else
		cQryCondic := " Empty(C2_DATRF) .And. (C2_QUANT - C2_QUJE - " + cPerda + ") > 0 "
		cQryCondic += " .And. C2_FILIAL = '" + xFilial("SC2") + "' "
	EndIf

Return cQryCondic

/*/{Protheus.doc} procExt
Realiza o processamento da sincroniza��o sem tela

@type  Static Function
@author douglas.heydt
@since 16/03/2020
@version P12.1.28
@param aApiAlter, Array, Array com as APIs que devem ser processadas
/*/
Static Function procExt(aApiAlter)

	Local nIndex := 0
	Local nPos   := 0

	For nIndex := 1 To Len(aApiAlter)
		nPos := aScan(aParTela, {|x| Alltrim(x[IND_PAR_API]) == Alltrim(aApiAlter[nIndex]) })

		If nPos > 0
			aParTela[nPos][IND_PAR_CHECKED] := .T.
		EndIf
	Next nX

	processar()

Return

/*/{Protheus.doc} SincArmaz
Sincroniza os armazens
@author  douglas.heydt
@version P12
@since   07/08/2020
@param   cJobName, Character, Nome do JOB de processamento
@return  NIL
/*/
Static Function SincArmaz(cJobName)

	Local aDadosInc  := {}
	Local aError     := {}
	Local aSuccess   := {}
	Local cAliasQry  := GetNextAlias()
	Local cQryCondic := ""
	Local lOnlyDel   := .F.
	Local nAtual     := 0
	Local nPos       := 0
	Local oTTPend

	oTTPend  := TTPendMRP()

	//Limpa a tabela de pend�ncias para as demandas;
	limpaT4R(aParTela[IND_TELA_ARMAZEM][IND_PAR_API])

	//Consulta os registros
	cQryCondic := aParTela[IND_TELA_ARMAZEM][IND_PAR_QUERY]
	BeginSql Alias cAliasQry
		SELECT  NNR_FILIAL,
				NNR_CODIGO,
				NNR_TIPO,
				NNR_MRP,
				R_E_C_N_O_
		FROM %Exp:cQryCondic%
	EndSql

	nAtual := 0
	//Carrega o array aDadosInc com os registros a serem integrados
	While (cAliasQry)->(!Eof())
		//Atualiza as barras de progresso
		nAtual++
		PutGlbValue(cJobName+"TOTAL", cValToChar(nAtual))
		GlbUnLock()

		//Adiciona nova linha no array de inclus�o/atualiza��o
		aAdd(aDadosInc, Array(WHAPICnt("ARRAY_WH_SIZE")))
		nPos := Len(aDadosInc)

		//Adiciona as informa��es no array de inclus�o/atualiza��o
		aDadosInc[nPos][WHAPICnt("ARRAY_WH_POS_FILIAL")] := (cAliasQry)->NNR_FILIAL
		aDadosInc[nPos][WHAPICnt("ARRAY_WH_POS_COD"   )] := (cAliasQry)->NNR_CODIGO
		aDadosInc[nPos][WHAPICnt("ARRAY_WH_POS_TIPO"  )] := (cAliasQry)->NNR_TIPO
		aDadosInc[nPos][WHAPICnt("ARRAY_WH_POS_MRP"   )] := (cAliasQry)->NNR_MRP

		(cAliasQry)->(dbSkip())
	End
	(cAliasQry)->(dbCloseArea())

	//Vari�vel para identificar que terminou a carga e ir� come�ar o processo de integra��o.
	PutGlbValue(cJobName+"CARGA", "1")
	GlbUnLock()

	If nAtual == 0
		//Se n�o tem dados para enviar, alimenta o array aDadosInc
		//com uma posi��o contendo somente a filial
		//para que os registros existentes no MRP sejam exclu�dos.
		aAdd(aDadosInc, Array(WHAPICnt("ARRAY_WH_SIZE")))
		aDadosInc[1][WHAPICnt("ARRAY_WH_POS_FILIAL")] := xFilial("NNR")
		lOnlyDel := .T.
	EndIf
	//Chama a fun��o do PCPA131API para integrar os registros
	PcpWHInt("SYNC", aDadosInc, oTTPend, @aSuccess, @aError, lOnlyDel)

	//Guarda os registros integrados com sucesso e n�o integrados
	PutGlbValue(cJobName+"SUCESSO", cValToChar(Len(aSuccess)))
	PutGlbValue(cJobName+"ERRO"   , cValToChar(Len(aError)))

	oTTPend:Delete()
	FreeObj(oTTPend)

Return
