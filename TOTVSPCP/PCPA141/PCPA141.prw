#INCLUDE "TOTVS.CH"
#INCLUDE "PCPA141.CH"

Static __aCodApi := Nil
Static __cAPIs   := ""
Static __cUUID   := ""
Static __nSizeID := Nil

/*/{Protheus.doc} PCPA141
SCHEDULE de envio de dados para o MRP

@type  Static Function
@author lucas.franca
@since 07/08/2019
@version P12.1.28
/*/
Function PCPA141()
	Local cApi := AllTrim(MV_PAR01)

	PCPA141RUN(cApi)
Return

/*/{Protheus.doc} SchedDef
Parametriza��es do SCHEDULE de envio de dados do MRP

@type  Static Function
@author lucas.franca
@since 07/08/2019
@version P12.1.28
@return aParam, Array, Array com os par�metros para execu��o do schedule
/*/
Static Function SchedDef()
	Local aOrd   := {}
	Local aParam := {}

	cargaAPI()

	aParam := { "P",;
	            "PCPA141",;
	            "T4R",;
	            aOrd, }
Return aParam

/*/{Protheus.doc} cargaAPI
Verifica se � necess�rio carregar dados na tabela T4P para
exibir na consulta padr�o da configura��o da API.

@type  Static Function
@author lucas.franca
@since 07/08/2019
@version P12.1.28
/*/
Static Function cargaAPI()
	Local cFil    := xFilial("T4P")
	Local nTamApi := GetSx3Cache("T4P_API","X3_TAMANHO")
	Local nIndex  := 0

	If __aCodApi == Nil
		__aCodApi := RetApiZoom()
	EndIf

	T4P->(dbSetOrder(1))

	For nIndex := 1 To Len(__aCodApi)
		If ! T4P->(dbSeek(cFil+PadR(__aCodApi[nIndex][1], nTamApi)))
			RecLock("T4P",.T.)
				T4P->T4P_FILIAL := cFil
				T4P->T4P_API    := PadR(__aCodApi[nIndex][1], nTamApi)
				T4P->T4P_ATIVO  := "2"
			MsUnLock()
		EndIf
	Next nIndex
Return

/*/{Protheus.doc} PCPA141VLD
Verifica se o valor digitado no pergunte PCPA141 est� v�lido.

@type  Function
@author lucas.franca
@since 07/08/2019
@version P12.1.28
@return lRet, Logic, Identifica se o valor digitado est� v�lido.
/*/
Function PCPA141VLD()
	Local cValAPI := AllTrim(MV_PAR01)
	Local lRet    := .T.

	If __aCodApi == Nil
		__aCodApi := RetApiZoom()
	EndIf

	If aScan(__aCodApi, {|x| x[1] == cValAPI}) <= 0
		lRet := .F.
		HELP(' ', 1, "HELP",, STR0001,; //"API informada n�o � v�lida para o processamento."
		     2, 0, , , , , , {STR0002}) //"Informe um c�digo de API que seja v�lido para a execu��o dos processos."
	EndIf

Return lRet

/*/{Protheus.doc} PCPA141DSC
Retorna a descri��o de uma API

@type  Function
@author lucas.franca
@since 07/08/2019
@version P12.1.28
@param cApi  , Character, C�digo da API
@return cDesc, Character, Descri��o da API
/*/
Function PCPA141DSC(cApi)
	Local cDesc := ""
	Local nPos  := 0

	If __aCodApi == Nil
		__aCodApi := RetApiZoom()
	EndIf

	cApi := AllTrim(cApi)

	nPos :=  aScan(__aCodApi, {|x| x[1] == cApi})
	If nPos > 0
		cDesc := __aCodApi[nPos][2]
	EndIf
Return cDesc

/*/{Protheus.doc} PCPA141RUN
Executa o processamento das APIs

@type  Function
@author lucas.franca
@since 07/08/2019
@version P12.1.28
@param cApi  , Character, C�digo da API
@return Nil
/*/
Function PCPA141RUN(cApi)
	Local lOnline := .T.


	//Se a integra��o est� desativada, n�o executa o JOB
	If !IntNewMRP(cApi, @lOnline)
		Return
	EndIf

	//Se a API est� configurada como Online, n�o executa o JOB
	If lOnline
		Return
	EndIf

	ErrorBlock({|e| A141Error(e) })

	Begin Sequence
		If marcarT4R(cApi)
			Do Case
				Case cApi == "MRPDEMANDS"
					PCPA141DEM(__cUUID) //Demandas
				Case cApi == "MRPPURCHASEORDER"
					PCPA141SCO(__cUUID) //Solicita��o de compra
				Case cApi == "MRPPURCHASEREQUEST"
					PCPA141OCO(__cUUID) //Pedido de compra
				Case cApi == "MRPALLOCATIONS"
					PCPA141EMP(__cUUID) //Empenhos
				Case cApi == "MRPPRODUCTIONORDERS"
					PCPA141OP(__cUUID)  //Ordem de produ��o
				Case cApi == "MRPSTOCKBALANCE"
					PCPA141EST(__cUUID) //Estoque
				Case cApi == "MRPREJECTEDINVENTORY"
					PCPA141CQ(__cUUID)
				Case cApi == "MRPPRODUCTIONVERSION"
					PCPA141VEP(__cUUID) //Vers�o da Produ��o
				Case cApi == "MRPTRANSPORTINGLANES"
					PCPA141MAL(__cUUID) //Malha de Distribui��o
				Case cApi == "MRPWAREHOUSEGROUP"
                    PCPA141GA(__cUUID)  //Grupo de Armaz�m
				Case cApi == "MRPWAREHOUSE"
                    PCPA141AMZ(__cUUID)  //Armaz�m
				Case cApi == "MRPPRODUCT"
                    PCPA141PRD(__cUUID)  //Produtos
				Case cApi == "MRPBILLOFMATERIAL"
					//procEstrut()
				Case cApi == "MRPCALENDAR"
					//procCalend()
				Case cApi == "MRPPRODUCTINDICATOR"
					PCPA141IPR(__cUUID)
			EndCase
		EndIf
	RECOVER
		If !Empty(__cUUID)
			StartJob("PCPA141ERR", GetEnvServer(), .T., cEmpAnt, cFilAnt, __cUUID, cApi)
		EndIf
	End Sequence

	__cUUID := ""
Return Nil

/*/{Protheus.doc} A141Error
Fun��o para tratativa de erros de execu��o

@type  Function
@author lucas.franca
@since 09/08/2019
@version P12.1.28
@param e, Object, Objeto com os detalhes do erro ocorrido
/*/
Function A141Error(e)
	LogMsg('PCPA141RUN', 0, 0, 1, '', '', ;
	       Replicate("-",70) + CHR(10) + AllTrim(e:description) + CHR(10) + AllTrim(e:ErrorStack) + CHR(10) + Replicate("-",70))
	BREAK
Return

/*/{Protheus.doc} PCPA141ERR
Em caso de erro na execu��o, libera o campo T4R_IDPRC para que os registros n�o fique travado.

@type  Function
@author lucas.franca
@since 09/08/2019
@version P12.1.28
@param cEmp , Character, C�digo da empresa para conex�o.
@param cFil , Character, C�digo da filial para conex�o.
@param cUUID, Character, C�digo identificador do processo na tabela T4R
@param cApi , Character, C�digo da API em processamento
@return Nil
/*/
Function PCPA141ERR(cEmp, cFil, cUUID, cApi)
	Local cSql := ""

	RpcSetType(3)
	RpcSetEnv(cEmp, cFil)

	//Se j� existir registros com o IDPRC diferente do atual, exclui os registros
	//da T4R que est�o com o IDPRC igual o atual.
	cSql := " DELETE FROM " + RetSqlName("T4R")
	cSql +=  " WHERE T4R_FILIAL = '" + xFilial("T4R") + "' "
	cSql +=    " AND T4R_API    = '" + cApi + "' "
	cSql +=    " AND T4R_IDPRC  = '" + cUUID + "' "
	cSql +=    " AND T4R_IDREG IN( SELECT T4R.T4R_IDREG "
	cSql +=                        " FROM " + RetSqlName("T4R") + " T4R "
	cSql +=                       " WHERE T4R_FILIAL = '" + xFilial("T4R") + "' "
	If cApi == "MRPPRODUCT"
		cSql +=                   "   AND (T4R_API = 'MRPPRODUCT' OR T4R_API = 'MRPPRODUCTINDICATOR') "
	Else
		cSql +=                   "   AND T4R_API    = '" + cApi + "' "
	EndIf
	cSql +=                         " AND T4R_IDPRC <> '" + cUUID + "' )"

	If TcSqlExec(cSql) < 0
		Final(STR0003, TcSqlError()) //"Erro ao restaurar os dados de pend�ncias."
	EndIf

	//Limpa o campo T4R_IDPRC dos registros que est�o ligados a este processamento
	cSql := " UPDATE " + RetSqlName("T4R")
	cSql +=    " SET T4R_IDPRC = ' ' "
	cSql +=  " WHERE T4R_FILIAL = '" + xFilial("T4R") + "' "
	If cApi == "MRPPRODUCT"
		cSql += " AND (T4R_API = 'MRPPRODUCT' OR T4R_API = 'MRPPRODUCTINDICATOR') "
	Else
		cSql += " AND T4R_API  = '" + cApi + "' "
	EndIf
	cSql +=    " AND T4R_IDPRC = '" + cUUID + "' "

	If TcSqlExec(cSql) < 0
		Final(STR0003, TcSqlError()) //"Erro ao restaurar os dados de pend�ncias."
	EndIf

Return Nil

/*/{Protheus.doc} marcarT4R
Marca os registros da tabela T4R que ser�o processados
por este processo.

@type  Static Function
@author lucas.franca
@since 07/08/2019
@version P12.1.28
@param 01 - cApi      , Character, C�digo da API
@param 02 - nTentativa, numero, indicador interno de controle de execucao
@return lRet, Logic    , Indica se conseguiu marcar os registros da T4R
/*/
Static Function marcarT4R(cApi, nTentativa)
	Local cSql := ""
	Local cId  := UUIDRandomSeq()
	Local lRet := .T.
	Default nTentativa := 1

	If __nSizeID == Nil
		__nSizeID := GetSx3Cache("T4R_IDPRC", "X3_TAMANHO")
	EndIf

	cId := PadR(cId, __nSizeID)

	__cUUID := cId

	cSql := " UPDATE " + RetSqlName("T4R")
	cSql +=    " SET T4R_IDPRC  = '" + cId + "' "
	cSql +=  " WHERE T4R_IDPRC  = ' ' "
	If cApi == "MRPPRODUCT"
		cSql += " AND (T4R_API  = 'MRPPRODUCT' OR T4R_API = 'MRPPRODUCTINDICATOR') "
	ElseIf cApi == "MRPSTOCKBALANCE"
		cSql += " AND (T4R_API  = 'MRPSTOCKBALANCE' OR T4R_API = 'MRPREJECTEDINVENTORY') "
	Else
		cSql += " AND T4R_API   = '" + cApi + "' "
	EndIf
	cSql +=    " AND T4R_STATUS = '3' "

	If TcSqlExec(cSql) < 0
		If nTentativa > 10
			lRet := .F.
			LogMsg('PCPA141RUN', 0, 0, 1, '', '', STR0004 + TcSqlError()) //"Erro ao marcar os registros para processamento. "
		Else
			Sleep(500)
			lRet := marcarT4R(cApi, (nTentativa + 1))
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} RetApiZoom
Retorna as Api�s que ser�o mostradas no zoom do schedule

@type  Static Function
@author ricardo.prandi
@since 27/12/2019
@version P12.1.27
@return aRet, Array, Array contendo as Api�s que ser�o mostradas no zoom
/*/
Static Function RetApiZoom()

	aRet := {}

	aAdd(aRet,{"MRPDEMANDS"          ,P139GetAPI("MRPDEMANDS"          )}) //"Demandas"
    aAdd(aRet,{"MRPPRODUCTIONVERSION",P139GetAPI("MRPPRODUCTIONVERSION")}) //"Vers�o da Produ��o"
    aAdd(aRet,{"MRPBILLOFMATERIAL"   ,P139GetAPI("MRPBILLOFMATERIAL"   )}) //"Estruturas"
    aAdd(aRet,{"MRPALLOCATIONS"      ,P139GetAPI("MRPALLOCATIONS"      )}) //"Empenhos"
    aAdd(aRet,{"MRPPRODUCTIONORDERS" ,P139GetAPI("MRPPRODUCTIONORDERS" )}) //"Ordens de produ��o"
    aAdd(aRet,{"MRPPURCHASEORDER"    ,P139GetAPI("MRPPURCHASEORDER"    )}) //"Solicita��es de Compras"
    aAdd(aRet,{"MRPPURCHASEREQUEST"  ,P139GetAPI("MRPPURCHASEREQUEST"  )}) //"Pedidos de Compras"
    aAdd(aRet,{"MRPSTOCKBALANCE"     ,P139GetAPI("MRPSTOCKBALANCE"     )}) //"Saldo em Estoque"
    aAdd(aRet,{"MRPCALENDAR"         ,P139GetAPI("MRPCALENDAR"         )}) //"Calend�rio"
	aAdd(aRet,{"MRPPRODUCT"          ,P139GetAPI("MRPPRODUCT"          )}) //"Produtos"
	aAdd(aRet,{"MRPWAREHOUSE"        ,P139GetAPI("MRPWAREHOUSE"        )}) //"Armaz�ns"

	If SuperGetMv("MV_PCPMADI",.F.,.F.)
		aAdd(aRet,{"MRPWAREHOUSEGROUP"   ,P139GetAPI("MRPWAREHOUSEGROUP"   )}) //"Grupos de Armaz�m"
		aAdd(aRet,{"MRPTRANSPORTINGLANES",P139GetAPI("MRPTRANSPORTINGLANES")}) //"Malha de Distribui��o"
	EndIf

Return aRet

/*/{Protheus.doc} PCPA141FIL
Filtra as APIs listadas no Zoom do Schedule

@type  Static Function
@author brunno.costa
@since 15/07/2020
@version P12.1.27
@return lReturn, logico, indica se o registro atualmente posicionado deve ser exibido no zoom do schedule
/*/
Function PCPA141FIL()

	Local lReturn := .T.

	If Empty(__cAPIs)
		__cAPIs += "|MRPDEMANDS|"           //"Demandas"
		__cAPIs += "|MRPPRODUCTIONVERSION|" //"Vers�o da Produ��o"
		__cAPIs += "|MRPBILLOFMATERIAL|"    //"Estruturas"
		__cAPIs += "|MRPALLOCATIONS|"       //"Empenhos"
		__cAPIs += "|MRPPRODUCTIONORDERS|"  //"Ordens de produ��o"
		__cAPIs += "|MRPPURCHASEORDER|"     //"Solicita��es de Compras"
		__cAPIs += "|MRPPURCHASEREQUEST|"   //"Pedidos de Compras"
		__cAPIs += "|MRPSTOCKBALANCE|"      //"Saldo em Estoque"
		__cAPIs += "|MRPCALENDAR|"          //"Calend�rio"
		__cAPIs += "|MRPPRODUCT|"           //"Produtos"
		__cAPIs += "|MRPWAREHOUSE|"         //"Armaz�ns"

		If SuperGetMv("MV_PCPMADI",.F.,.F.)
			__cAPIs += "|MRPWAREHOUSEGROUP|"    //"Grupos de Armaz�m"
			__cAPIs += "|MRPTRANSPORTINGLANES|" //"Malha de Distribui��o"
		EndIf
	EndIf

	lReturn := "|" + AllTrim(Upper(T4P->T4P_API)) + "|" $ __cAPIs .AND. T4P->T4P_TPEXEC == "2"

Return lReturn
