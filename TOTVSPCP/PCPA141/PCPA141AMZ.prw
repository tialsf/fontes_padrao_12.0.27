#INCLUDE "TOTVS.CH"

Static _nTamFil  := Nil
Static _nTamCode := Nil
Static _nTamTipo := Nil
Static _nTamMRP  := Nil

/*/{Protheus.doc} PCPA141AMZ
Executa o processamento dos registros de armaz�m

@type  Function
@author brunno.costa
@since 11/08/2020
@version P12.1.27
@param cUUID, Character, Identificador do processo para buscar os dados na tabela T4R.
/*/
Function PCPA141AMZ(cUUID)
	Local aDados     := {}
	Local aDadosDel  := {}
	Local aDadosInc  := {}
	Local cAliasT4R  := GetNextAlias()
	Local cError     := ""
	Local oTmpPend   := Nil
	Local oJson      := JsonObject():New()
	Local cQryBase	 := ""

	If _nTamFil == Nil
		_nTamFil   := FwSizeFilial()
		_nTamCode  := GetSX3Cache("NNR_CODIGO", "X3_TAMANHO")
		_nTamTipo  := GetSX3Cache("NNR_TIPO"  , "X3_TAMANHO")
		_nTamMRP   := GetSX3Cache("NNR_MRP"   , "X3_TAMANHO")
	EndIf

	//Campos que ser�o usados
	cQryFields := "%NNR.NNR_FILIAL," +;
				  " NNR.NNR_CODIGO,"+;
				  " NNR.NNR_TIPO,"+;
				  " NNR.NNR_MRP,"+;
				  " NNR.R_E_C_N_O_%"

	//Relacionamento entre Grupo e Local de estoque
	cQryBase := RetSqlName("NNR") + " NNR" + ;
				" WHERE NNR.D_E_L_E_T_ = ' '"

	BeginSql Alias cAliasT4R
		SELECT T4R.T4R_TIPO,
		       T4R.R_E_C_N_O_,
		       T4R.T4R_IDREG,
			   T4R.T4R_DADOS
		  FROM %Table:T4R% T4R
		 WHERE T4R.T4R_FILIAL = %xfilial:T4R%
		   AND T4R.T4R_API    = 'MRPWAREHOUSE'
		   AND T4R.T4R_STATUS = '3'
		   AND T4R.T4R_IDPRC  = %Exp:cUUID%
	EndSql

	While (cAliasT4R)->(!Eof())
		cError := oJson:FromJson((cAliasT4R)->(T4R_DADOS))
		If Empty(cError)

			aSize(aDados, 0)
			aDados := Array(WHAPICnt("ARRAY_WH_SIZE"))

			aDados[WHAPICnt("ARRAY_WH_POS_FILIAL") ] := PadR(oJson["NNR_FILIAL"] , _nTamFil )
			aDados[WHAPICnt("ARRAY_WH_POS_COD")    ] := PadR(oJson["NNR_CODIGO"] , _nTamCode)
			aDados[WHAPICnt("ARRAY_WH_POS_TIPO")   ] := PadR(oJson["NNR_TIPO"]   , _nTamTipo)
			aDados[WHAPICnt("ARRAY_WH_POS_MRP")    ] := PadR(oJson["NNR_MRP"]    , _nTamMRP )

			//S� atualiza os dados que n�o possuem o res�duo zerado (C1_RESIDUO = ' '). Caso contr�rio, exclui
			If (cAliasT4R)->(T4R_TIPO) = "1"
				aAdd(aDadosInc, aClone(aDados))
			Else
				aAdd(aDadosDel, aClone(aDados))
			EndIf
		EndIf

		(cAliasT4R)->(dbSkip())
	End
	(cAliasT4R)->(dbCloseArea())

	//Cria as tabelas tempor�rias utilizadas na integra��o
	If Len(aDadosDel) > 0 .Or. Len(aDadosInc) > 0
		oTmpPend  := TTPendMRP()
	EndIf

	//Executa a integra��o para exclus�o
	If Len(aDadosDel) > 0
		PcpWHInt("DELETE", aDadosDel, oTmpPend, Nil, Nil, cUUID)
	EndIf

	//Executa a integra��o para inclus�o/atualiza��o.
	If Len(aDadosInc) > 0
		PcpWHInt("INSERT", aDadosInc, oTmpPend, Nil, Nil, cUUID)
	EndIf

	If oTmpPend != Nil
		oTmpPend:Delete()
		oTmpPend := Nil
	EndIf

	aSize(aDadosDel, 0)
	aSize(aDadosInc, 0)
	aSize(aDados   , 0)

Return
