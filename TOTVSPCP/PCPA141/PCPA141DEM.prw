#INCLUDE "TOTVS.CH"

/*/{Protheus.doc} PCPA141DEM
Executa o processamento dos registros de demandas

@type  Function
@author lucas.franca
@since 07/08/2019
@version P12.1.28
@param cUUID, Character, Identificador do processo para buscar os dados na tabela T4R.
/*/
Function PCPA141DEM(cUUID)
    Local aDados    := {}
    Local aDadosDel := {}
    Local aDadosInc := {}
    Local cAlias    := GetNextAlias()
    Local cError    := ""
    Local nTamFil   := FwSizeFilial()
    Local nTamCod   := GetSx3Cache("VR_CODIGO","X3_TAMANHO")
    Local nTamPrd   := GetSx3Cache("VR_PROD"  ,"X3_TAMANHO")
    Local nTamDoc   := GetSx3Cache("VR_DOC"   ,"X3_TAMANHO")
    Local nTamLoc   := GetSx3Cache("VR_LOCAL" ,"X3_TAMANHO")
    Local oJson     := JsonObject():New()
    Local oTmpInMrp := Nil
    Local oTmpPend  := Nil

    BeginSql Alias cAlias
		SELECT T4R.T4R_TIPO,
		       T4R.R_E_C_N_O_,
		       T4R.T4R_DADOS
		  FROM %Table:T4R% T4R
		 WHERE T4R.T4R_FILIAL = %xfilial:T4R%
		   AND T4R.T4R_API    = 'MRPDEMANDS'
		   AND T4R.T4R_STATUS = '3'
		   AND T4R.T4R_IDPRC  = %Exp:cUUID%
    EndSql

    While (cAlias)->(!Eof())
        cError := oJson:FromJson((cAlias)->(T4R_DADOS))
        If Empty(cError)
            //Ajusta tamanho dos dados que s�o do tipo String.
            oJson["VR_FILIAL"] := PadR(oJson["VR_FILIAL"], nTamFil)
            oJson["VR_CODIGO"] := PadR(oJson["VR_CODIGO"], nTamCod)
            oJson["VR_PROD"]   := PadR(oJson["VR_PROD"]  , nTamPrd)
            oJson["VR_DOC"]    := PadR(oJson["VR_DOC"]   , nTamDoc)
            oJson["VR_LOCAL"]  := PadR(oJson["VR_LOCAL"] , nTamLoc)

            aSize(aDados, 0)
            aDados := Array(A136APICnt("ARRAY_DEMAND_SIZE"))

            aDados[A136APICnt("ARRAY_DEMAND_POS_FILIAL")] := oJson["VR_FILIAL"]
            aDados[A136APICnt("ARRAY_DEMAND_POS_CODE")  ] := oJson["VR_CODIGO"]
            aDados[A136APICnt("ARRAY_DEMAND_POS_SEQUEN")] := oJson["VR_SEQUEN"]
            If (cAlias)->(T4R_TIPO) != "2"
                aDados[A136APICnt("ARRAY_DEMAND_POS_PROD")  ] := oJson["VR_PROD"]
                aDados[A136APICnt("ARRAY_DEMAND_POS_REV")   ] := ""
                aDados[A136APICnt("ARRAY_DEMAND_POS_DATA")  ] := StoD(oJson["VR_DATA"])
                aDados[A136APICnt("ARRAY_DEMAND_POS_TIPO")  ] := oJson["VR_TIPO"]
                aDados[A136APICnt("ARRAY_DEMAND_POS_DOC")   ] := oJson["VR_DOC"]
                aDados[A136APICnt("ARRAY_DEMAND_POS_QUANT") ] := oJson["VR_QUANT"]
                aDados[A136APICnt("ARRAY_DEMAND_POS_LOCAL") ] := oJson["VR_LOCAL"]
                If oJson["VR_MOPC"] != Nil .And. oJson["VR_MOPC"] .And. !Empty(oJson["R_E_C_N_O_"])
                    SVR->(dbGoTo(oJson["R_E_C_N_O_"]))
                    If SVR->(Recno()) == oJson["R_E_C_N_O_"] //Prote��o para s� colocar o MOPC se posicionar no registro correto.
                        aDados[A136APICnt("ARRAY_DEMAND_POS_OPC")] := SVR->VR_MOPC
                        aDados[A136APICnt("ARRAY_DEMAND_POS_STR_OPC")] := SVR->VR_OPC
                    EndIf
                Else
                    aDados[A136APICnt("ARRAY_DEMAND_POS_OPC")] := Nil
                    aDados[A136APICnt("ARRAY_DEMAND_POS_STR_OPC")] := Nil
                EndIf
            EndIf

            If (cAlias)->(T4R_TIPO) == "1"
                aAdd(aDadosInc, aClone(aDados))
            Else
                aAdd(aDadosDel, aClone(aDados))
            EndIf
        EndIf

        (cAlias)->(dbSkip())
    End
    (cAlias)->(dbCloseArea())

    //Cria as tabelas tempor�rias utilizadas na integra��o das demandas
    If Len(aDadosDel) > 0 .Or. Len(aDadosInc) > 0
        oTmpInMrp := P136APITMP()
        oTmpPend  := TTPendMRP()
    EndIf

    //Executa a integra��o para exclus�o de demandas
    If Len(aDadosDel) > 0
        PCPA136INT("DELETE", aDadosDel, oTmpInMrp, oTmpPend, Nil, Nil, cUUID)
    EndIf

    //Executa a integra��o para inclus�o/atualiza��o de demandas.
    If Len(aDadosInc) > 0
        PCPA136INT("INSERT", aDadosInc, oTmpInMrp, oTmpPend, Nil, Nil, cUUID)
    EndIf

    If oTmpInMrp != Nil
        oTmpInMrp:Delete()
        oTmpInMrp := Nil
    EndIf
    If oTmpPend != Nil
        oTmpPend:Delete()
        oTmpPend := Nil
    EndIf

    aSize(aDadosDel, 0)
    aSize(aDadosInc, 0)
    aSize(aDados   , 0)

    FreeObj(oJson)
    oJson := Nil
Return