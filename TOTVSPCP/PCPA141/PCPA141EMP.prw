#INCLUDE "TOTVS.CH"

/*/{Protheus.doc} PCPA141EMP
Executa o processamento dos registros de Empenhos

@type  Function
@author marcelo.neumann
@since 16/08/2019
@version P12.1.28
@param cUUID, Character, Identificador do processo para buscar os dados na tabela T4R.
/*/
Function PCPA141EMP(cUUID)
	Local aDados     := {}
	Local aDadosDel  := {}
	Local aDadosInc  := {}
	Local cAliasT4R  := GetNextAlias()
	Local cAliasSD4  := GetNextAlias()
	Local oJson      := JsonObject():New()
	Local oTmpPend   := Nil
	Local cQryCondic := ""
	//Vari�veis para extra��o da Chave
	Local cFilialSD4 := ""
	Local cCodSD4    := ""
	Local cTrtSD4    := ""
	Local cLocalSD4  := ""
	Local cOpSD4     := ""
	Local cOpOrigD4  := ""
	Local dDtSD4     := Nil
	Local nTamFil    := FwSizeFilial()
	Local nTamCod    := GetSx3Cache("D4_COD"  , "X3_TAMANHO")
	Local nTamTrt    := GetSx3Cache("D4_TRT"  , "X3_TAMANHO")
	Local nTamLocal  := GetSx3Cache("D4_LOCAL", "X3_TAMANHO")
	Local nTamOp     := GetSx3Cache("D4_OP"   , "X3_TAMANHO")
	Local nTamQtd    := GetSx3Cache("D4_QUANT", "X3_TAMANHO")
	Local nTamDec    := GetSx3Cache("D4_QUANT", "X3_DECIMAL")

	BeginSql Alias cAliasT4R
		SELECT T4R.T4R_TIPO,
		       T4R.R_E_C_N_O_,
		       T4R.T4R_IDREG
		  FROM %Table:T4R% T4R
		 WHERE T4R.T4R_FILIAL = %xfilial:T4R%
		   AND T4R.T4R_API    = 'MRPALLOCATIONS'
		   AND T4R.T4R_STATUS = '3'
		   AND T4R.T4R_IDPRC  = %Exp:cUUID%
	EndSql

	While (cAliasT4R)->(!Eof())
		//Extrai a chave da SD4 gravada na T4R
		cFilialSD4 := SubStr((cAliasT4R)->T4R_IDREG, 1 , nTamFil)
		cCodSD4    := SubStr((cAliasT4R)->T4R_IDREG, 1 + nTamFil , nTamCod)
		cTrtSD4    := SubStr((cAliasT4R)->T4R_IDREG, 1 + nTamFil + nTamCod , nTamTrt)
		cLocalSD4  := SubStr((cAliasT4R)->T4R_IDREG, 1 + nTamFil + nTamCod + nTamTrt , nTamLocal)
		cOpSD4     := SubStr((cAliasT4R)->T4R_IDREG, 1 + nTamFil + nTamCod + nTamTrt + nTamLocal, nTamOp)
		cOpOrigD4  := SubStr((cAliasT4R)->T4R_IDREG, 1 + nTamFil + nTamCod + nTamTrt + nTamLocal + nTamOp, nTamOp)
		dDtSD4     := StoD(SubStr((cAliasT4R)->T4R_IDREG, 1 + nTamFil + nTamCod + nTamTrt + nTamLocal + nTamOp + nTamOp, 8))

		//Se for Inclus�o, faz SUM das quantidades com a chave do empenho
		If (cAliasT4R)->(T4R_TIPO) == "1"
			cQryCondic := "%" + RetSqlName("SD4")                      + ;
			              " WHERE D4_FILIAL  = '" + cFilialSD4   + "'" + ;
			                " AND D4_COD     = '" + cCodSD4      + "'" + ;
			                " AND D4_TRT     = '" + cTrtSD4      + "'" + ;
			                " AND D4_LOCAL   = '" + cLocalSD4    + "'" + ;
			                " AND D4_OP      = '" + cOpSD4       + "'" + ;
			                " AND D4_OPORIG  = '" + cOpOrigD4    + "'" + ;
			                " AND D4_DATA    = '" + DtoS(dDtSD4) + "'" + ;
			                " AND D_E_L_E_T_ = ' '"                    + ;
			              " GROUP BY D4_FILIAL,"                       + ;
			                       " D4_COD,"                          + ;
			                       " D4_OP,"                           + ;
			                       " D4_OPORIG,"                       + ;
			                       " D4_DATA,"                         + ;
			                       " D4_TRT,"                          + ;
			                       " D4_LOCAL%"

			BeginSql Alias cAliasSD4
				COLUMN D4_DATA AS DATE
				COLUMN QUANT   AS NUMERIC(nTamQtd, nTamDec)
				COLUMN QSUSP   AS NUMERIC(nTamQtd, nTamDec)
				SELECT D4_FILIAL,
					   D4_COD,
					   D4_OP,
					   D4_OPORIG,
					   D4_DATA,
					   D4_TRT,
					   Sum(D4_QUANT) QUANT,
					   Sum(D4_QSUSP) QSUSP,
					   D4_LOCAL
				FROM %Exp:cQryCondic%
			EndSql

			While (cAliasSD4)->(!Eof())
				aSize(aDados, 0)
				aDados := Array(A381APICnt("ARRAY_SIZE"))
				aDados[A381APICnt("ARRAY_POS_FILIAL" )] := (cAliasSD4)->D4_FILIAL
				aDados[A381APICnt("ARRAY_POS_PROD"   )] := (cAliasSD4)->D4_COD
				aDados[A381APICnt("ARRAY_POS_SEQ"    )] := (cAliasSD4)->D4_TRT
				aDados[A381APICnt("ARRAY_POS_LOCAL"  )] := (cAliasSD4)->D4_LOCAL
				aDados[A381APICnt("ARRAY_POS_OP"     )] := (cAliasSD4)->D4_OP
				aDados[A381APICnt("ARRAY_POS_OP_ORIG")] := (cAliasSD4)->D4_OPORIG
				aDados[A381APICnt("ARRAY_POS_DATA"   )] := (cAliasSD4)->D4_DATA
				aDados[A381APICnt("ARRAY_POS_QTD"    )] := (cAliasSD4)->QUANT
				aDados[A381APICnt("ARRAY_POS_QSUSP"  )] := (cAliasSD4)->QSUSP

				aAdd(aDadosInc, aClone(aDados))

				(cAliasSD4)->(dbSkip())
			End
			(cAliasSD4)->(dbCloseArea())

		//Se for Exclus�o, n�o precisa buscar a SD4
		Else
			aSize(aDados, 0)
			aDados := Array(A381APICnt("ARRAY_SIZE"))
			aDados[A381APICnt("ARRAY_POS_FILIAL" )] := cFilialSD4
			aDados[A381APICnt("ARRAY_POS_PROD"   )] := cCodSD4
			aDados[A381APICnt("ARRAY_POS_SEQ"    )] := cTrtSD4
			aDados[A381APICnt("ARRAY_POS_LOCAL"  )] := cLocalSD4
			aDados[A381APICnt("ARRAY_POS_OP"     )] := cOpSD4
			aDados[A381APICnt("ARRAY_POS_OP_ORIG")] := cOpOrigD4
			aDados[A381APICnt("ARRAY_POS_DATA"   )] := dDtSD4

			aAdd(aDadosDel, aClone(aDados))
		EndIf
		(cAliasT4R)->(dbSkip())
	End
	(cAliasT4R)->(dbCloseArea())

	//Cria as tabelas tempor�rias utilizadas na integra��o dos empenhos
	If Len(aDadosDel) > 0 .Or. Len(aDadosInc) > 0
		oTmpPend := TTPendMRP()
	EndIf

	//Executa a integra��o para exclus�o dos empenhos
	If Len(aDadosDel) > 0
		PCPA381INT("DELETE", aDadosDel, oTmpPend, Nil, Nil, cUUID)
	EndIf

	//Executa a integra��o para inclus�o/atualiza��o dos empenhos
	If Len(aDadosInc) > 0
		PCPA381INT("INSERT", aDadosInc, oTmpPend, Nil, Nil, cUUID)
	EndIf

	If oTmpPend != Nil
		oTmpPend:Delete()
		oTmpPend := Nil
	EndIf

	aSize(aDadosDel, 0)
	aSize(aDadosInc, 0)
	aSize(aDados   , 0)

	FreeObj(oJson)
	oJson := Nil

Return