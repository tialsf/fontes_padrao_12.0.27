#INCLUDE "TOTVS.CH"

/*/{Protheus.doc} PCPA141GA
Executa o processamento dos registros de grupo de armaz�m

@type  Function
@author douglas.heydt
@since 10/10/2019
@version P12.1.28
@param cUUID, Character, Identificador do processo para buscar os dados na tabela T4R.
/*/
Function PCPA141GA(cUUID)
	Local aDados     := {}
	Local aDadosDel  := {}
	Local aDadosDet  := {}
	Local aDadosInc  := {}
	Local cAliasT4R  := GetNextAlias()
	Local cAliasSVN  := GetNextAlias()
	Local oTmpPend   := Nil
	Local cQryCondic := ""
	Local cQryBase	 := ""

	//Vari�veis para extra��o da Chave
	Local cFilialSVN := ""
	Local cLocGrpSVN := ""
	Local nTamFil    := FwSizeFilial()
	Local nTamLocGrp := GetSx3Cache("VN_LOCGRP"  , "X3_TAMANHO")
	
	//Campos que ser�o usados
	cQryFields := "%SVN.VN_FILIAL," +;
				  " SVN.VN_LOCGRP," +;
				  " SVN.VN_TIPO,"   +;
				  " SVO.VO_LOCAL,"  +;
				  " SVO.VO_DEFAULT%"

	//Relacionamento entre Grupo e Local de estoque
	cQryBase := ""+RetSqlName("SVN") + " SVN"					+;
			    " LEFT OUTER JOIN " + RetSqlName("SVO") + " SVO"  	+;
			    " ON SVN.VN_FILIAL  = '" + xFilial("SVN") + "'" 	+;
				" AND SVO.VO_FILIAL  = '" + xFilial("SVO") + "'" 	+;
				" AND SVN.VN_LOCGRP = SVO.VO_LOCGRP "				+;
			    " AND SVO.D_E_L_E_T_ = ' ' "						+;
				" WHERE SVN.D_E_L_E_T_ = ' '"						
				
	BeginSql Alias cAliasT4R
		SELECT T4R.T4R_TIPO,
		       T4R.R_E_C_N_O_,
		       T4R.T4R_IDREG
		  FROM %Table:T4R% T4R
		 WHERE T4R.T4R_FILIAL = %xfilial:T4R%
		   AND T4R.T4R_API    = 'MRPWAREHOUSEGROUP'
		   AND T4R.T4R_STATUS = '3'
		   AND T4R.T4R_IDPRC  = %Exp:cUUID%
	EndSql

	While (cAliasT4R)->(!Eof())
		//Extrai a chave da SVN gravada na T4R
		cFilialSVN := SubStr((cAliasT4R)->T4R_IDREG, 1 , nTamFil)
		cLocGrpSVN := SubStr((cAliasT4R)->T4R_IDREG, 1 + nTamFil , nTamLocGrp)

		//Se for Exclus�o, basta enviar a chave gravada no T4R_IDREG
		If (cAliasT4R)->(T4R_TIPO) == "2"
			aSize(aDados, 0)
			aDados := Array(A126APICnt("ARRAY_GRUPOARM_CAB_SIZE"))
			aDados[A126APICnt("ARRAY_GRUPOARM_CAB_POS_FILIAL")] := cFilialSVN
			aDados[A126APICnt("ARRAY_GRUPOARM_CAB_POS_GRUPO") ] := cLocGrpSVN

			aAdd(aDadosDel, aClone(aDados))
		Else
			cQryCondic := "%" + cQryBase + " AND SVN.VN_LOCGRP = '" + cLocGrpSVN + "' " + ;
			                                 " AND SVN.VN_FILIAL  = '" + cFilialSVN + "'%"
			BeginSql Alias cAliasSVN
				SELECT %Exp:cQryFields%
				  FROM %Exp:cQryCondic%
			EndSql

			//Se encontrou o registro, grava no array de inclus�o
			If (cAliasSVN)->(!Eof())
				aSize(aDados, 0)
				aDados := Array(A126APICnt("ARRAY_GRUPOARM_CAB_SIZE"))

				aDados[A126APICnt("ARRAY_GRUPOARM_CAB_POS_FILIAL")] := (cAliasSVN)->VN_FILIAL
				aDados[A126APICnt("ARRAY_GRUPOARM_CAB_POS_GRUPO") ] := (cAliasSVN)->VN_LOCGRP
				aDados[A126APICnt("ARRAY_GRUPOARM_CAB_POS_TIPO")  ] := (cAliasSVN)->VN_TIPO
				aDados[A126APICnt("ARRAY_GRUPOARM_CAB_POS_LISTA") ] := {}

				//Se o produto est� preenchido � porque possui exce��o. Percorre todas as exce��es
				While (cAliasSVN)->(!Eof())
					aSize(aDadosDet, 0)
					aDadosDet := Array(A126APICnt("ARRAY_GRUPOARM_ARM_SIZE"))

					aDadosDet[A126APICnt("ARRAY_GRUPOARM_ARM_POS_ARMAZEM")] := (cAliasSVN)->VO_LOCAL
					aDadosDet[A126APICnt("ARRAY_GRUPOARM_ARM_POS_DEFAULT")] := (cAliasSVN)->VO_DEFAULT
					aDadosDet[A126APICnt("ARRAY_GRUPOARM_ARM_POS_CODE")   ] := (cAliasSVN)->VN_FILIAL+(cAliasSVN)->VN_LOCGRP+(cAliasSVN)->VO_LOCAL

					aAdd(aDados[A126APICnt("ARRAY_GRUPOARM_CAB_POS_LISTA")], aClone(aDadosDet))

					(cAliasSVN)->(dbSkip())
				End

				aAdd(aDadosInc, aClone(aDados))

				(cAliasSVN)->(dbCloseArea())
			EndIf
		EndIf

		(cAliasT4R)->(dbSkip())
	End
	(cAliasT4R)->(dbCloseArea())

	//Cria as tabelas tempor�rias utilizadas na integra��o das solicita��es de compra
	If Len(aDadosDel) > 0 .Or. Len(aDadosInc) > 0
		oTmpPend  := TTPendMRP()
	EndIf

	//Executa a integra��o para exclus�o de solicita��es de compra
	If Len(aDadosDel) > 0
		PCPA126INT("DELETE", aDadosDel, oTmpPend, Nil, Nil, cUUID)
	EndIf

	//Executa a integra��o para inclus�o/atualiza��o de solicita��es de compra.
	If Len(aDadosInc) > 0
		PCPA126INT("INSERT", aDadosInc, oTmpPend, Nil, Nil, cUUID)
	EndIf

	If oTmpPend != Nil
		oTmpPend:Delete()
		oTmpPend := Nil
	EndIf

	aSize(aDadosDel, 0)
	aSize(aDadosDet, 0)
	aSize(aDadosInc, 0)
	aSize(aDados   , 0)

Return