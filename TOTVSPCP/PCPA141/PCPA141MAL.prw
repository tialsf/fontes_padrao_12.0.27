#INCLUDE "TOTVS.CH"

/*/{Protheus.doc} PCPA141MAL
Executa o processamento dos registros de Malha de Distribui��o

@type  Function
@author marcelo.neumann
@since 15/10/2019
@version P12.1.28
@param cUUID, Character, Identificador do processo para buscar os dados na tabela T4R.
/*/
Function PCPA141MAL(cUUID)

	Local aDados     := {}
	Local aDadosDel  := {}
	Local aDadosDet  := {}
	Local aDadosInc  := {}
	Local cAliasT4R  := GetNextAlias()
	Local cAliasSVD  := GetNextAlias()
	Local cQryFields := ""
	Local cQryCoBase := ""
	Local cQryCondic := ""
	Local oTmpPend   := Nil

	//Vari�veis para extra��o da Chave
	Local cFilialSVD := ""
	Local cRecno     := ""
	Local nTamFil    := FwSizeFilial()
	Local nTamQtd    := GetSx3Cache("VQ_QNTINI", "X3_TAMANHO")
	Local nTamDec    := GetSx3Cache("VQ_QNTINI", "X3_DECIMAL")

	//Campos que ser�o usados
	cQryFields := "%SVD.VD_ORIGEM,"  + ;
				  " SVD.VD_FILDEST," + ;
				  " SVD.VD_DESTINO," + ;
				  " SVQ.VQ_PRODUTO," + ;
				  " SVQ.VQ_DTINI,"   + ;
				  " SVQ.VQ_DTFIM,"   + ;
				  " SVQ.VQ_QNTINI,"  + ;
				  " SVQ.VQ_QNTFIM%"

	//Relacionamento entre Malha e Produto Exce��o
	cQryCoBase := RetSqlName("SVD") + " SVD"                      + ;
			    " LEFT OUTER JOIN " + RetSqlName("SVQ") + " SVQ"  + ;
			      " ON SVQ.VQ_FILIAL  = '" + xFilial("SVQ") + "'" + ;
			     " AND SVQ.VQ_ORIGEM  = SVD.VD_ORIGEM"            + ;
			     " AND SVQ.VQ_DESTINO = SVD.VD_DESTINO"           + ;
			     " AND SVQ.D_E_L_E_T_ = ' '"                      + ;
			   " WHERE SVD.D_E_L_E_T_ = ' '"

	//Busca os registros a serem processados pelo Job
	BeginSql Alias cAliasT4R
		SELECT T4R.T4R_TIPO,
		       T4R.R_E_C_N_O_,
		       T4R.T4R_IDREG
		  FROM %Table:T4R% T4R
		 WHERE T4R.T4R_FILIAL = %xfilial:T4R%
		   AND T4R.T4R_API    = 'MRPTRANSPORTINGLANES'
		   AND T4R.T4R_STATUS = '3'
		   AND T4R.T4R_IDPRC  = %Exp:cUUID%
	EndSql

	While (cAliasT4R)->(!Eof())
		//Extrai a chave (Filial + Recno) gravada na T4R
		cFilialSVD := SubStr((cAliasT4R)->T4R_IDREG, 1 , nTamFil)
		cRecno     := AllTrim(SubStr((cAliasT4R)->T4R_IDREG, 1 + nTamFil))

		//Se for Exclus�o, basta enviar a chave gravada no T4R_IDREG
		If (cAliasT4R)->(T4R_TIPO) == "2"
			aSize(aDados, 0)
			aDados := Array(A123APICnt("ARRAY_MALHA_CAB_SIZE"))

			aDados[A123APICnt("ARRAY_MALHA_CAB_POS_FILIAL")] := cFilialSVD
			aDados[A123APICnt("ARRAY_MALHA_CAB_POS_CODE")  ] := cFilialSVD + cRecno

			aAdd(aDadosDel, aClone(aDados))
		Else
			//Se for Inclus�o, busca as informa��es na SVD e SVQ
			cQryCondic := "%" + cQryCoBase + " AND SVD.R_E_C_N_O_ = "  + cRecno + ;
			                                 " AND SVD.VD_FILIAL  = '" + cFilialSVD + "'%"

			BeginSql Alias cAliasSVD
				COLUMN VQ_DTINI  AS DATE
				COLUMN VQ_DTFIM  AS DATE
				COLUMN VQ_QNTINI AS NUMERIC(nTamQtd, nTamDec)
				COLUMN VQ_QNTFIM AS NUMERIC(nTamQtd, nTamDec)
				SELECT %Exp:cQryFields%
				  FROM %Exp:cQryCondic%
			EndSql

			//Se encontrou o registro, grava no array de inclus�o
			If (cAliasSVD)->(!Eof())
				aSize(aDados, 0)
				aDados := Array(A123APICnt("ARRAY_MALHA_CAB_SIZE"))

				aDados[A123APICnt("ARRAY_MALHA_CAB_POS_FILIAL")] := cFilialSVD
				aDados[A123APICnt("ARRAY_MALHA_CAB_POS_CODE")  ] := cFilialSVD + cRecno
				aDados[A123APICnt("ARRAY_MALHA_CAB_POS_ARMORG")] := (cAliasSVD)->VD_ORIGEM
				aDados[A123APICnt("ARRAY_MALHA_CAB_POS_FILDES")] := (cAliasSVD)->VD_FILDEST
				aDados[A123APICnt("ARRAY_MALHA_CAB_POS_ARMDES")] := (cAliasSVD)->VD_DESTINO
				aDados[A123APICnt("ARRAY_MALHA_CAB_POS_LISTA") ] := {}

				//Se o produto est� preenchido � porque possui exce��o. Percorre todas as exce��es
				If !Empty((cAliasSVD)->VQ_PRODUTO)
					While (cAliasSVD)->(!Eof())
						aSize(aDadosDet, 0)
						aDadosDet := Array(A123APICnt("ARRAY_MALHA_EXCE_SIZE"))

						aDadosDet[A123APICnt("ARRAY_MALHA_EXCE_POS_PROD")  ] := (cAliasSVD)->VQ_PRODUTO
						aDadosDet[A123APICnt("ARRAY_MALHA_EXCE_POS_DTINI") ] := (cAliasSVD)->VQ_DTINI
						aDadosDet[A123APICnt("ARRAY_MALHA_EXCE_POS_DTFIN") ] := (cAliasSVD)->VQ_DTFIM
						aDadosDet[A123APICnt("ARRAY_MALHA_EXCE_POS_QTDINI")] := (cAliasSVD)->VQ_QNTINI
						aDadosDet[A123APICnt("ARRAY_MALHA_EXCE_POS_QTDFIN")] := (cAliasSVD)->VQ_QNTFIM

						aAdd(aDados[A123APICnt("ARRAY_MALHA_CAB_POS_LISTA")], aClone(aDadosDet))

						(cAliasSVD)->(dbSkip())
					End
				EndIf

				aAdd(aDadosInc, aClone(aDados))

				(cAliasSVD)->(dbCloseArea())
			EndIf
		EndIf

		(cAliasT4R)->(dbSkip())
	End
	(cAliasT4R)->(dbCloseArea())

	//Cria as tabelas tempor�rias utilizadas na integra��o das solicita��es de compra
	If Len(aDadosDel) > 0 .Or. Len(aDadosInc) > 0
		oTmpPend  := TTPendMRP()
	EndIf

	//Executa a integra��o para exclus�o de solicita��es de compra
	If Len(aDadosDel) > 0
		PCPA123INT("DELETE", aDadosDel, oTmpPend, Nil, Nil, cUUID)
	EndIf

	//Executa a integra��o para inclus�o/atualiza��o de solicita��es de compra.
	If Len(aDadosInc) > 0
		PCPA123INT("INSERT", aDadosInc, oTmpPend, Nil, Nil, cUUID)
	EndIf

	If oTmpPend != Nil
		oTmpPend:Delete()
		oTmpPend := Nil
	EndIf

	aSize(aDadosDel, 0)
	aSize(aDadosDet, 0)
	aSize(aDadosInc, 0)
	aSize(aDados   , 0)

Return