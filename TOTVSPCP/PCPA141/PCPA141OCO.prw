#INCLUDE "TOTVS.CH"

Static _nTamFil   := Nil
Static _nTamNum   := Nil
Static _nTamItem  := Nil
Static _nTamItGrd := Nil

/*/{Protheus.doc} PCPA141OCO
Executa o processamento dos registros de Pedido de Compra

@type  Function
@author douglas.heydt
@since 13/08/2019
@version P12.1.28
@param cUUID, Character, Identificador do processo para buscar os dados na tabela T4R.
/*/
Function PCPA141OCO(cUUID)

	Local aDados    := {}
	Local aDadosDel := {}
	Local aDadosInc := {}
	Local cAlias    := GetNextAlias()
	Local cError    := ""
	Local oJson     := JsonObject():New()
	Local oTmpPend  := Nil

	If _nTamFil == Nil
		_nTamFil   := FwSizeFilial()
		_nTamNum   := GetSX3Cache("C7_NUM"    , "X3_TAMANHO")
		_nTamItem  := GetSX3Cache("C7_ITEM"   , "X3_TAMANHO")
		_nTamItGrd := GetSX3Cache("C7_ITEMGRD", "X3_TAMANHO")
	EndIf

	BeginSql Alias cAlias
		SELECT T4R.T4R_TIPO,
		       T4R.R_E_C_N_O_,
		       T4R.T4R_DADOS 
		  FROM %Table:T4R% T4R 
		  WHERE T4R.T4R_FILIAL = %xfilial:T4R%
		   AND T4R.T4R_API = 'MRPPURCHASEREQUEST'
		   AND T4R.T4R_STATUS = '3'
		   AND T4R.T4R_IDPRC = %Exp:cUUID%
	EndSql

	While (cAlias)->(!Eof())
		cError := oJson:FromJson((cAlias)->(T4R_DADOS))
		If Empty(cError)

			aSize(aDados, 0)
			aDados := Array(PEDCAPICnt("ARRAY_PEDCOM_SIZE"))

			aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_FILIAL") ] := PadR(oJson["C7_FILIAL"] , _nTamFil)
			aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_NUM")    ] := PadR(oJson["C7_NUM"]    , _nTamNum)
			aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_ITEM")   ] := PadR(oJson["C7_ITEM"]   , _nTamItem)
			aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_ITGRD")  ] := PadR(oJson["C7_ITEMGRD"], _nTamItGrd)
			aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_RESIDUO")] := Padr(oJson["C7_RESIDUO"], 1)

			//S� atualiza os dados que n�o possuem o res�duo zerado (C7_RESIDUO = ' '). Caso contr�rio, exclui
			If (cAlias)->(T4R_TIPO) = "1" .And. aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_RESIDUO")] == " " .And. oJson["C7_QUJE"] < oJson["C7_QUANT"]
				aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_PROD")	] := oJson["C7_PRODUTO"]
				aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_OP")    ] := oJson["C7_OP"]
				aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_DATPRF")] := StoD(oJson["C7_DATPRF"])
				aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_QTD")   ] := oJson["C7_QUANT"]
				aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_QUJE")  ] := oJson["C7_QUJE"]
				aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_LOCAL") ] := oJson["C7_LOCAL"]
				aDados[PEDCAPICnt("ARRAY_PEDCOM_POS_TIPO")  ] := oJson["C7_TPOP"]

				aAdd(aDadosInc, aClone(aDados))
			Else
				aAdd(aDadosDel, aClone(aDados))
			EndIf
		EndIf
		
		(cAlias)->(dbSkip())
	End
	(cAlias)->(dbCloseArea())

	//Cria as tabelas tempor�rias utilizadas na integra��o das solicita��es de compra
	If Len(aDadosDel) > 0 .Or. Len(aDadosInc) > 0
		oTmpPend  := TTPendMRP()
	EndIf

	//Executa a integra��o para exclus�o de solicita��es de compra
	If Len(aDadosDel) > 0
		PCPPEDCINT("DELETE", aDadosDel, oTmpPend, Nil, Nil, cUUID)
	EndIf

	//Executa a integra��o para inclus�o/atualiza��o de solicita��es de compra.
	If Len(aDadosInc) > 0
		PCPPEDCINT("INSERT", aDadosInc, oTmpPend, Nil, Nil, cUUID)
	EndIf

	If oTmpPend != Nil
		oTmpPend:Delete()
		oTmpPend := Nil
	EndIf

	aSize(aDadosDel, 0)
	aSize(aDadosInc, 0)
	aSize(aDados   , 0)

	FreeObj(oJson)
	oJson := Nil
Return