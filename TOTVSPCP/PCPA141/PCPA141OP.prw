#INCLUDE "TOTVS.CH"

/*/{Protheus.doc} PCPA141OP
Executa o processamento dos registros de Ordens de produ��o

@type  Function
@author marcelo.neumann
@since 16/08/2019
@version P12.1.28
@param cUUID, Character, Identificador do processo para buscar os dados na tabela T4R.
/*/
Function PCPA141OP(cUUID)
	Local aDados    := {}
	Local cAlias    := GetNextAlias()
	Local cError    := ""
	Local oJson     := JsonObject():New()
	Local oTmpPend  := Nil

	//Cria a tabela tempor�ria utilizada na integra��o
	oTmpPend := TTPendMRP()

	BeginSql Alias cAlias
		SELECT T4R.T4R_TIPO,
		       T4R.R_E_C_N_O_,
		       T4R.T4R_DADOS
		  FROM %Table:T4R% T4R
		 WHERE T4R.T4R_FILIAL = %xfilial:T4R%
		   AND T4R.T4R_API    = 'MRPPRODUCTIONORDERS'
		   AND T4R.T4R_STATUS = '3'
		   AND T4R.T4R_IDPRC  = %Exp:cUUID%
	EndSql

	While (cAlias)->(!Eof())
		cError := oJson:FromJson((cAlias)->(T4R_DADOS))
		If Empty(cError)
			SC2->(dbGoTo(oJson["R_E_C_N_O_"]))
			If SC2->(Recno()) == oJson["R_E_C_N_O_"]
				A650AddInt(aDados, , IIf((cAlias)->(T4R_TIPO) == "1", "INSERT", "DELETE"))
			EndIf
		EndIf

		(cAlias)->(dbSkip())
	End
	(cAlias)->(dbCloseArea())

	//Executa a integra��o das ordens de produ��o
	If Len(aDados) > 0
		oTmpPend := TTPendMRP()
		MATA650INT("", aDados, oTmpPend, Nil, Nil, cUUID)
		oTmpPend:Delete()
		oTmpPend := Nil
	EndIf

	aSize(aDados, 0)

	FreeObj(oJson)
	oJson := Nil

Return