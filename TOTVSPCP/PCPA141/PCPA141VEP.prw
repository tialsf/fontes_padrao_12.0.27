#INCLUDE "TOTVS.CH"

/*/{Protheus.doc} PCPA141VEP
Executa o processamento dos registros de Vers�o da Produ��o

@type  Function
@author marcelo.neumann
@since 27/08/2019
@version P12.1.28
@param cUUID, Character, Identificador do processo para buscar os dados na tabela T4R.
/*/
Function PCPA141VEP(cUUID)

	Local aDados    := {}
	Local aDadosDel := {}
	Local aDadosInc := {}
	Local cAlias    := GetNextAlias()
	Local cError    := ""
	Local nTamFil   := FwSizeFilial()
	Local nTamVer   := GetSx3Cache("VC_VERSAO" ,"X3_TAMANHO")
	Local nTamPrd   := GetSx3Cache("VC_PRODUTO","X3_TAMANHO")
	Local oJson     := JsonObject():New()
	Local oTmpPend  := Nil

	BeginSql Alias cAlias
		SELECT T4R.T4R_TIPO,
		       T4R.R_E_C_N_O_,
		       T4R.T4R_DADOS
		  FROM %Table:T4R% T4R
		 WHERE T4R.T4R_FILIAL = %xfilial:T4R%
		   AND T4R.T4R_API    = 'MRPPRODUCTIONVERSION'
		   AND T4R.T4R_STATUS = '3'
		   AND T4R.T4R_IDPRC  = %Exp:cUUID%
	EndSql

	While (cAlias)->(!Eof())
		cError := oJson:FromJson((cAlias)->(T4R_DADOS))

		If Empty(cError)
			aSize(aDados, 0)
			aDados := Array(A119APICnt("ARRAY_PRODVERS_SIZE"))

			aDados[A119APICnt("ARRAY_PRODVERS_POS_FILIAL")] := PadR(oJson["VC_FILIAL"] , nTamFil)
			aDados[A119APICnt("ARRAY_PRODVERS_POS_CODE")  ] := PadR(oJson["VC_VERSAO"] , nTamVer)
            aDados[A119APICnt("ARRAY_PRODVERS_POS_PROD")  ] := PadR(oJson["VC_PRODUTO"], nTamPrd)

			If (cAlias)->(T4R_TIPO) != "2"
				aDados[A119APICnt("ARRAY_PRODVERS_POS_DTINI")  ] := StoD(oJson["VC_DTINI"])
                aDados[A119APICnt("ARRAY_PRODVERS_POS_DTFIM")  ] := StoD(oJson["VC_DTFIM"])
				aDados[A119APICnt("ARRAY_PRODVERS_POS_QTDINI") ] := oJson["VC_QTDDE"]
			    aDados[A119APICnt("ARRAY_PRODVERS_POS_QTDFIM") ] := oJson["VC_QTDATE"]
                aDados[A119APICnt("ARRAY_PRODVERS_POS_REVISAO")] := oJson["VC_REV"]
			EndIf

			aDados[A119APICnt("ARRAY_PRODVERS_POS_ROTEIRO")    ] := oJson["VC_ROTEIRO"]
			aDados[A119APICnt("ARRAY_PRODVERS_POS_LOCAL")      ] := oJson["VC_LOCCONS"]

			If (cAlias)->(T4R_TIPO) == "1"
				aAdd(aDadosInc, aClone(aDados))
			Else
				aAdd(aDadosDel, aClone(aDados))
			EndIf
		EndIf

		(cAlias)->(dbSkip())
	End
	(cAlias)->(dbCloseArea())

	//Cria as tabelas tempor�rias utilizadas na integra��o da Vers�o da Produ��o
	If Len(aDadosDel) > 0 .Or. Len(aDadosInc) > 0
		oTmpPend  := TTPendMRP()
	EndIf

	//Executa a integra��o para exclus�o da Vers�o da Produ��o
	If Len(aDadosDel) > 0
		PCPA119INT("DELETE", aDadosDel, oTmpPend, Nil, Nil, cUUID)
	EndIf

	//Executa a integra��o para inclus�o/atualiza��o da Vers�o da Produ��o
	If Len(aDadosInc) > 0
		PCPA119INT("INSERT", aDadosInc, oTmpPend, Nil, Nil, cUUID)
	EndIf

	If oTmpPend != Nil
		oTmpPend:Delete()
		oTmpPend := Nil
	EndIf

	aSize(aDadosDel, 0)
	aSize(aDadosInc, 0)
	aSize(aDados   , 0)

	FreeObj(oJson)
	oJson := Nil
Return