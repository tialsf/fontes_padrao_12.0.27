#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'DBTREE.CH'
#INCLUDE "PCPA144.CH"

Static soMenu

/*/{Protheus.doc} PCPA144
Resultados do MRP

@author douglas.heydt
@since 15/11/2019
@version 1.0
/*/

Function PCPA144()

	Local cTitulo    := STR0001//"Resultados MRP"

	Private oBrwProd
	Private oTGet1, oTGet2, oTGet3, oTGet4, oTGet5, oTGet6
	Private aTamanhos  := FWGetDialogSize(oMainwnd)
	Private aProds     := {}
	Private aProdutos  := {}
	Private aResults   := {}
	Private aSubsts    := {}
	Private aParMRP    := {}
	Private aDocs 	   := {}
	Private cTicket    := Space(6)
	Private cSetup     := Space(30)
	Private cPeriodo   := Space(15)
	Private cDataIni   := Space(10)
	Private cDataFim   := Space(10)
	Private cFiltroPrd := Space(GetSx3Cache("B1_COD", "X3_TAMANHO" ))
	Private oDlg, oPnlPai, oLayer
	Private oPnlCab, oFont, oFolderDoc, oPnlFold, oPnlProd,oBrwResult, oPnlRslt1, oPnlRslt, oPnlDocs, oPnlEst, oBrwDocs,  oPnlResult
	Private oPnlSubst,oBrwSubst,lSubsMont,oGetComp,oGetDesc,oGetQtd,oGetDocto
	Private oConTicket, oConParam
	Private oButton,oButton2,oButton3, oButton4, oButton5, oBtnGR1, oButton6
	Private nActiveFld := 1
	Private oJsParametros := JsonObject():New()
	Private nPosGR1Btn  := 0
	Private nWidthDif   := 0
	Private lOpenLat    := .T.

	LoadMVPar("PCPA144FIL", .F.)
	LoadMVPar("PCPA144GR1", .F.)

	oFont := TFont():New( , , 14, , .F.,,,,, .F. , .F.)

	oDlg := MSDialog():New( aTamanhos[1],aTamanhos[2],aTamanhos[3],aTamanhos[4],cTitulo,,,.F.,,,,,,.T.,,,.T. )

	//Cria o painel principal
	oPnlPai := TPanel():New(01,01,,oDlg,,,,,,aTamanhos[4]/2,(aTamanhos[3]/2)-5,.T.,.T.)

	oLayer := FWLayer():New()
	oLayer:Init(oPnlPai,.T.)

	oLayer:addCollumn("ColunaEsq",25,.F.)
	oLayer:addCollumn("ColunaDir",75,.F.)

	oLayer:setColSplit("ColunaEsq",CONTROL_ALIGN_RIGHT,,{||lOpenLat := !lOpenLat, RefreshRes(.T.) })

	oLayer:addWindow("ColunaEsq",'C_Win04',STR0032,100,.T.,.T.,{||RefreshRes() },,{|| }) //"Produtos"

	oPnlProd  := oLayer:getWinPanel("ColunaEsq",'C_Win04')

	oPnlRslt1 := TPanel():New(01,01,,oPnlProd,,,,,,(oPnlProd:nClientWidth*0.5),,.T.,.T.)
	oPnlRslt1:Align :=CONTROL_ALIGN_LEFT

	oLayer:addWindow("ColunaDir",'C_Win01',STR0002,21,.T.,.T.,{||RefreshRes() },,{|| }) //"Ticket"
	oLayer:addWindow("ColunaDir",'C_Win02',STR0033,30,.T.,.F.,{||RefreshRes() },,{|| }) //"Resultados"
	oLayer:addWindow("ColunaDir",'C_Win03',STR0004,49,.T.,.F.,{||RefreshRes() },,{|| }) //"Documentos"

	oPnlCab  := oLayer:getWinPanel("ColunaDir",'C_Win01')
	oPnlResult := oLayer:getWinPanel("ColunaDir",'C_Win02')
	oPnlFold := oLayer:getWinPanel("ColunaDir",'C_Win03')

	oPnlRslt := TPanel():New(01,01,,oPnlResult,,,,,,(oPnlResult:nClientWidth*0.5),,.T.,.T.)
	oPnlRslt:Align :=CONTROL_ALIGN_ALLCLIENT

	oFolderDoc := TFolder():New(0,0,{STR0004, STR0005, STR0006 },{"HEAD1","HEAD2","HEAD3"},oPnlFold,,,,.T.,.F.,oPnlFold:nClientWidth*0.5,(oPnlFold:nClientHeight*0.5),)  //"Documentos"// "Estoque"//"Substitui��o"
	oFolderDoc:Align := CONTROL_ALIGN_ALLCLIENT
	oFolderDoc:bSetOption := {|nFolder| chgFolder(nFolder)}
	If !kendoOk()
		oFolderDoc:HidePage( 2 )
		oFolderDoc:ShowPage( 1 )
	EndIf

	oPnlDocs := TPanel():New(01,200,,oFolderDoc:aDialogs[1],,,,, ,,,.T.,.T.)
	oPnlDocs:Align := CONTROL_ALIGN_ALLCLIENT

	oPnlEst := TPanel():New(01,200,,oFolderDoc:aDialogs[2],,,,, ,,,.T.,.T.)
	oPnlEst:Align := CONTROL_ALIGN_ALLCLIENT
	nPosGR1Btn     := oPnlEst:nClientWidth - 45
	nWidthDif     := ((oPnlEst:nClientWidth / 0.75) - nPosGR1Btn) * 0.9

	oPnlSubst := TPanel():New(0,0,,oFolderDoc:aDialogs[3],,,,,,,,.T.,.T.)
	oPnlSubst:Align := CONTROL_ALIGN_ALLCLIENT

	montaCab()
	gridProd()
	gridResult()
	gridDocs()

	ACTIVATE MSDIALOG oDlg CENTERED
	FreeObj(soMenu)
Return Nil

/*/{Protheus.doc} montaCab
Monta cabe�alho da rotina

@author douglas.heydt
@since 15/11/2019
@version 1.0
/*/
Static Function montaCab()

	Local nTamanho := 0
	Local nAuxCol  := 65

	/*PRIMEIRA LINHA*/
	TSay():New(04, 08, {|| STR0007 }, oPnlCab, ,oFont , , , , .T., , , 30, 20) //"Ticket:"
	oTGet1 := TGet():New( 01,30,{|u| If(PCount()>0,cTicket:=u,cTicket)},oPnlCab,30,13,"@!",{|| vldTicket(cTicket, @aParMrp) },0,/*10*/,/*11*/,.F.,/*13*/,.T.,/*15*/,.F.,/*17*/,.F.,.F.,/*20*/,.F.,.F.,/*23*/,"cTicket")
	oTGet1:cF3 := "PCPA144F3()"

	nTamanho := GetSx3Cache("HWM_TICKET", "X3_TAMANHO" )
	If nTamanho != Nil .AND. nTamanho > 0
		oButton6 := TButton():New( 01, 430, STR0190, oPnlCab,{|| PCPA144Log()}, 60,15,,,.F.,.T.,.F.,,.F.,,,.F. ) //"Eventos"
		nAuxCol  := 0
	EndIf

	oButton3 := TButton():New( 01,  69, STR0015	,oPnlCab,{|| FWMsgRun(,{|| AtuProduto() },STR0038,STR0039) }, 60,15,,,.F.,.T.,.F.,,.F.,,,.F. ) //"Consultar" - "Carregando dados..." "Aguarde"
	oButton  := TButton():New( 01, 300, STR0012	,oPnlCab,{|| AbreParam()}, 60,15,,,.F.,.T.,.F.,,.F.,,,.F. ) //"Par�metros MRP"
	oButton5 := TButton():New( 01, 365, STR0118	,oPnlCab,{|| LoadMVPar("PCPA144FIL", .T.) }, 60,15,,,.F.,.T.,.F.,,.F.,,,.F. ) //"Filtros"

	oButton2 := TButton():New( 21, 365 - nAuxCol, STR0014	,oPnlCab,{|| gerarDocs()}, 60,15,,,.F.,.T.,.F.,,.F.,,,.F. ) //"Gerar"
	oButton4 := TButton():New( 21, 430 - nAuxCol, STR0013	,oPnlCab,{|| oDlg:End()}, 60,15,,,.F.,.T.,.F.,,.F.,,,.F. ) //"Fechar"

	TSay():New(04, 147, {|| STR0008 }, oPnlCab, ,oFont , , , , .T., , , 30, 20) //"Setup:"
	oTGet2 := TGet():New( 01,168,{|u| If(PCount()>0,cSetup:=u,cSetup)},oPnlCab,100,13,"@!",/*08*/,0,/*10*/,/*11*/,.F.,/*13*/,.T.,/*15*/,.F.,/*17*/,.F.,.F.,/*20*/,.T.,.F.,/*23*/,"cSetup")

	//SEGUNDA LINHA
	TSay():New(23, 05, {|| STR0009 }, oPnlCab, ,oFont , , , , .T., , , 30, 20) //"Per�odo:"
	oTGet3 := TGet():New( 21,30,{|u| If(PCount()>0,cPeriodo:=u,cPeriodo)},oPnlCab,100,13,"@!",/*08*/,0,/*10*/,/*11*/,.F.,/*13*/,.T.,/*15*/,.F.,/*17*/,.F.,.F.,/*20*/,.T.,.F.,/*23*/,"cPeriodo")

	TSay():New(23, 147, {|| STR0010 }, oPnlCab, ,oFont , , , , .T., , , 30, 20) //"In�cio:"
	oTGet4 := TGet():New( 21,168,{|u| If(PCount()>0,cDataIni:=u,cDataIni)},oPnlCab,45,13,"@!",/*08*/,0,/*10*/,/*11*/,.F.,/*13*/,.T.,/*15*/,.F.,/*17*/,.F.,.F.,/*20*/,.T.,.F.,/*23*/,"cDataIni")

	TSay():New(23, 222, {|| STR0011 }, oPnlCab, ,oFont , , , , .T., , , 30, 20) //"Fim:"
	oTGet5 := TGet():New( 21,238,{|u| If(PCount()>0,cDataFim:=u,cDataFim)},oPnlCab,45,13,"@!",/*08*/,0,/*10*/,/*11*/,.F.,/*13*/,.T.,/*15*/,.F.,/*17*/,.F.,.F.,/*20*/,.T.,.F.,/*23*/,"cDataFim")

Return Nil

/*/{Protheus.doc} gridProd
Cria a grid de produtos

@author douglas.heydt
@since 15/11/2019
@version 1.0
/*/
Static Function gridProd()
	Local aHeaders	 := {}
	Local aColSizes	 := {}

	oTGet6     := TGet():New( 05, 03,{|u| If(PCount()>0,cFiltroPrd:=u,cFiltroPrd)},oPnlRslt1,95,13,"@!",/*08*/,0,/*10*/,/*11*/,.F.,/*13*/,.T.,/*15*/,.F.,/*17*/,.F.,.F.,/*20*/,.F.,.F.,/*23*/,"cFiltroPrd")
	oTGet6:cF3 := "SB1"

	oButton6  := TButton():New( 05, 100, STR0015 ,oPnlRslt1,{|| ConsultPrd()}, 45,15,,,.F.,.T.,.F.,,.F.,,,.F. )

	aAdd(aProds,{CriaVar("B1_COD"),CriaVar("B1_DESC"),""})

	aAdd(aHeaders, STR0036 ) //"C�digo"
	aAdd(aHeaders, STR0037 ) //"Descri��o"
	aAdd(aHeaders, STR0155 ) //"Opcional"

	aAdd(aColSizes, GetSx3Cache("B1_COD", "X3_TAMANHO" ))
	aAdd(aColSizes, GetSx3Cache("B1_DESC", "X3_TAMANHO" ))

	oBrwProd := TWBrowse():New(023,001, oPnlRslt1:nClientWidth*0.5, (oPnlRslt1:nClientHeight*0.5)-23, , aHeaders, aColSizes, oPnlRslt1,,,, /*bChange*/, , /*bRClick*/, , , /*nClrFore*/,,,,, .T.,, ,{|| .T.}, .T., .T. )
	oBrwProd:SetArray(aProds)
	oBrwProd:bLine   := {||{ aProds[oBrwProd:nAT,1],;
	                         aProds[oBrwProd:nAT,2],;
	                         aProds[oBrwProd:nAt,3]}}
	oBrwProd:bChange := { || AtuResulta(oBrwProd:nAt) }

	MenuContxt()

Return .T.

/*/{Protheus.doc} ConsultPrd
Realizar a consulta de um produto no grid.

@author renan.roeder
@since 23/03/2020
@version 1.0
/*/
Static Function ConsultPrd()
	Local lRet   := .T.
	Local nPos   := 0
	Local nStart := 0

	IF !Empty(cFiltroPrd) .And. Len(aProdutos) > 0

		If AllTrim(cFiltroPrd) $ aProdutos[oBrwProd:nAt][1]
			nStart := oBrwProd:nAt + 1
		Else
			nStart := 1
		EndIf

		nPos := AScan(aProdutos, {|x| AllTrim(cFiltroPrd) $ x[1] },nStart)
		If nPos > 0
			oBrwProd:GoPosition(nPos)
			oBrwProd:Refresh()
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} gridResult
Cria a grid de resultados

@author douglas.heydt
@since 15/11/2019
@version 1.0
/*/
Static Function gridResult()
	Local aAlter     := {}
	Local aHeaderRes := {}
	Local cTitulo    := ""
	Local nTamanho   := 0
	Local nDecimal   := 0
	Local cValid     := ""


	cTitulo  := STR0017 //"Per�odo"
	nTamanho := 8
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderRes,{cTitulo,"HWB_DATA","",nTamanho,nDecimal,cValid,"�","D",""})

	cTitulo  := STR0005 //"Estoque"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderRes,{cTitulo,"HWB_QTSLES","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0018 //"Entradas"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderRes,{cTitulo,"HWB_QTENTR","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0019 //"Sa�das"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderRes,{cTitulo,"HWB_QTSAID","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0020 //"Sa�da Estrut."
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderRes,{cTitulo,"HWB_QTSEST","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0034 //"Saldo Final"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderRes,{cTitulo,"HWB_QTSALD","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0021 //"Necessidade"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderRes,{cTitulo,"HWB_QTNECE","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	IniAResul()

	oBrwResult := MsNewGetDados():New(001,001,oPnlRslt:nClientHeight*0.5,oPnlRslt:nClientWidth*0.5,GD_UPDATE,/*LinhaOk*/,/*tudoOk*/,/*IniCpos*/,aAlter,0,1000,,,"AllwaysFalse",oPnlRslt,aHeaderRes,aResults,{|| AtuDocs() })
	oBrwResult:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
	oBrwResult:oBrowse:lUseDefaultColors := .F.
	oBrwResult:oBrowse:SetBlkBackColor({|| GETDCLR(aResults,oBrwResult:nAt)})
	oBrwResult:oBrowse:SetBlkColor( { || RGB(0,0,0) } )

Return .T.

/*/{Protheus.doc} GETDCLR
Retorna a cor para a linha da grid.

@author lucas.franca
@since 06/02/2020
@version 1.0
/*/
Static Function GETDCLR(aLinha, nLinha)
	Local nCor2 := RGB(28, 157, 189) //Azul
	Local nCor3 := RGB(255, 255, 255) //Branco
	Local nRet := nCor3

	If Len(aLinha) >= nLinha .And. nLinha > 0
		If aLinha[nLinha][Len(aLinha[nLinha])]
			nRet := nCor2
		Else
			nRet := nCor3
		Endif
	EndIf
Return nRet

/*/{Protheus.doc} gridDocs
Cria a grid de documentos

@author douglas.heydt
@since 15/11/2019
@version 1.0
/*/
Static Function gridDocs()
	Local aAlter     := {}
	Local aHeaderDoc := {}
	Local cTitulo    := ""
	Local nTamanho   := 0
	Local nDecimal   := 0
	Local cValid     := ""
	Local cCBOX      := GetSx3Cache("VR_TIPO","X3_CBOX")

	cCBOX += ";" + AllTrim(STR0126) + "=" + AllTrim(STR0130) //";OP=Ordem de Produ��o"
	cCBOX += ";" + AllTrim(STR0127) + "=" + AllTrim(STR0131) //";Pr�-OP=Ordem de Produ��o Pr� Existente"
	cCBOX += ";" + AllTrim(STR0128) + "=" + AllTrim(STR0132) //";Est.Seg.=Estoque de Seguran�a"
	cCBOX += ";" + AllTrim(STR0129) + "=" + AllTrim(STR0133) //";Ponto Ped.=Ponto de Pedido"
	cCBOX += ";" + "0"              + "=" + AllTrim(STR0160) //";0=Consolidado"
	cCBOX += ";" + "SUBPRD"         + "=" + AllTrim(STR0181) //";SUBPRD=Subproduto de OP"

	cTitulo  := STR0023 //"N�mero"
	nTamanho := 11
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_DOCFIL","",nTamanho,nDecimal,cValid,"�","C",""})

	cTitulo  := STR0159 //"Necessidade Original"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_QTNEOR","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0103 //"Estoque"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_QTSLES","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0104 //"Baixa"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_QTBXES","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0105 //"Empenho"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_QTEMPE","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0106 //"Substitui��o"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_QTSUBS","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0024 //"Quantidade"
	nTamanho := 12
	nDecimal := 3
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_QTNECE","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

	cTitulo  := STR0025 //"Armaz�m"
	nTamanho := 2
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_LOCAL","",nTamanho,nDecimal,cValid,"�","C",""})

	cTitulo  := STR0026 //"Entrega"
	nTamanho := 8
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_DATA","",nTamanho,nDecimal,cValid,"�","D",""})

	cTitulo  := STR0022 //"Tipo Pai"
	nTamanho := 30
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_TPDCPA","",nTamanho,nDecimal,cValid,"�","C","",,cCBOX})

	cTitulo  := STR0027 //"Documento pai"
	nTamanho := 50
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_DOCPAI","",nTamanho,nDecimal,cValid,"�","C",""})

	cTitulo  := STR0107 //"TRT"
	nTamanho := 3
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_TRT","",nTamanho,nDecimal,cValid,"�","C",""})

	cTitulo  := STR0028 //"Revis�o"
	nTamanho := 3
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_REV","",nTamanho,nDecimal,cValid,"�","C",""})

	cTitulo  := STR0035 //"Roteiro"
	nTamanho := 2
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_ROTEIR","",nTamanho,nDecimal,cValid,"�","C",""})

	cTitulo  := STR0183 //"Opera��o"
	nTamanho := 2
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_OPERAC","",nTamanho,nDecimal,cValid,"�","C",""})

	cTitulo  := STR0184 //"Tp.Doc.ERP"
	nTamanho := 1
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_TDCERP","",nTamanho,nDecimal,cValid,"�","C",""})


	cTitulo  := STR0029 //"Documento ERP"
	nTamanho := 14
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_DOCERP","",nTamanho,nDecimal,cValid,"�","C",""})

	cTitulo  := STR0154 //"Vers�o da produ��o"
	nTamanho := 4
	nDecimal := 0
	cValid   := ""
	aAdd(aHeaderDoc,{cTitulo,"HWC_VERSAO","",nTamanho,nDecimal,cValid,"�","C",""})

	IniADocs()

	oBrwDocs := MsNewGetDados():New(001,001,oPnlDocs:nClientHeight*0.5,oPnlDocs:nClientWidth*0.5,GD_UPDATE,/*LinhaOk*/,/*tudoOk*/,/*IniCpos*/,aAlter,0,1000,,,"AllwaysFalse",oPnlDocs,aHeaderDoc,aDocs,{|| changeDocs()})
	oBrwDocs:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
	oBrwDocs:oBrowse:lUseDefaultColors := .F.
	oBrwDocs:oBrowse:SetBlkBackColor({|| GETDCLR(aDocs,oBrwDocs:nAt)})
	oBrwDocs:oBrowse:SetBlkColor( { || RGB(0,0,0) } )

Return .T.

/*/{Protheus.doc} AtuProduto
Carrega os produtos conforme o ticket informado em tela.

@return lRet, Logico, Indica se a api encontrou registros para atualizar no grid.
@author renan.roeder
@since 21/11/2019
@version 1.0
/*/
Static Function AtuProduto()
	Local aResProd := {}
	Local aResOpc  := {}
	Local aItems   := {}
	Local cError   := ""
	Local cFilPrd  := xFilial("SB1")
	Local lRet     := .T.
	Local nX       := 0
	Local nTamPrd  := TamSX3("B1_COD")[1]
	Local nLenIte  := 0
	Local oJsonRes := JsonObject():New()
	Local cDescOpc := ""

	aProdutos := {{CriaVar("B1_COD"),CriaVar("B1_DESC"), "", ""}}
	AtuBrwPrd()

	//aResProd := MrpGetSum(cFilAnt, cTicket,"product,optionalId","product,optionalId",,9999,"Matriz_Produto")
	aResProd := MrpGetProd(cTicket)
	If aResProd[1]
		cError := oJsonRes:FromJson(aResProd[2])
	EndIf

	If !Empty(cError) .Or. !aResProd[1]
		aSize(aResults, 0)
		IniAResul()
		oBrwResult:SetArray(aResults)
		oBrwResult:ForceRefresh()
		oBrwResult:GoTop()

		aSize(aDocs, 0)
		IniADocs()
		oBrwDocs:SetArray(aDocs)
		oBrwDocs:ForceRefresh()
		oBrwDocs:GoTop()
		lRet := .F.
		Help( ,  , "Help", ,  STR0206, ; //"N�o existem produtos calculados."
		1, 0, , , , , , {STR0207} ) //"O processamento realizado pelo MRP n�o considerou nenhum produto durante o c�lculo. N�o existem dados para exibir."
	Else
		aSize(aProdutos, 0)
		aProdutos := {}
		aItems  := oJsonRes["items"]
		nLenIte := Len(aItems)
		For nX := 1 to nLenIte
			If oJsParametros["exibe_necessidade_zerada"] .OR. aItems[nX]["necessityQuantity"] > 0
				If Empty(aItems[nX]["optionalId"])
					aAdd(aProdutos,{Padr(aItems[nX]["product"],nTamPrd), POSICIONE("SB1",1,cFilPrd+aItems[nX]["product"],"B1_DESC"), "", " " })
				Else
					aResOpc := MrpGetOPC(cFilAnt, cTicket, aItems[nX]["optionalId"])
					oJson       := JsonObject():New()
					oJson:fromJson(aResOpc[2])
					aSize(aResOpc, 0)
					cDescOpc := AllTrim(oJson["optionalSelected"] + " (" + AllTrim(aItems[nX]["optionalId"]) + ")")
					aAdd(aProdutos,{Padr(aItems[nX]["product"],nTamPrd), POSICIONE("SB1",1,cFilPrd+aItems[nX]["product"],"B1_DESC"), cDescOpc, aItems[nX]["optionalId"]})
				EndIf
			EndIf
		Next nX
		If Len(aProdutos) > 0
			AtuBrwPrd()
			If !Empty(aProdutos[1][1])
				AtuResulta(1)
			EndIf
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} AtuResulta
Carrega os resultados conforme o ticket informado em tela e o produto selecionado no browse.

@param  nAt  , Numerico, Linha posicionada no grid.
@return lRet , Logico  , Indica se a api encontrou registros para atualizar no grid.
@author renan.roeder
@since 22/11/2019
@version 1.0
/*/
Static Function AtuResulta(nAt)
	Local aResMat := {}
	Local aItems   := {}
	Local cError   := ""
	Local lRet     := .T.
	Local nX       := 0
	Local nLenAite := 0
	Local oJsonRes := JsonObject():New()

	aSize(aResults, 0)
	If Len(aProdutos) > 0
		aResMat := MrpGetSPd(cFilAnt, cTicket, aProdutos[nAt][1], aProdutos[nAt][4],"necessityDate,stockBalance,inFlows,outFlows,structureOutFlows,finalBalance,necessityQuantity","necessityDate",,9999,"Matriz")
		cError := oJsonRes:FromJson(aResMat[2])

		If !Empty(cError)
			aSize(aDocs, 0)
			IniADocs()
			oBrwDocs:SetArray(aDocs)
			oBrwDocs:ForceRefresh()
			oBrwDocs:GoTop()
			lRet := .F.
		Else
			aItems := oJsonRes["items"]
			nLenAite := Len(aItems)
			For nX := 1 to nLenAite
				If oJsParametros["exibe_necessidade_zerada"] .OR. aItems[nX]["necessityQuantity"] > 0
					aAdd(aResults, { StoD(StrTran(aItems[nX]["necessityDate"],"-","")),;
					     aItems[nX]["stockBalance"],;
					     aItems[nX]["inFlows"],;
					     aItems[nX]["outFlows"],;
					     aItems[nX]["structureOutFlows"],;
					     aItems[nX]["finalBalance"],;
					     aItems[nX]["necessityQuantity"],;
					     Iif(nX==1,.T.,.F.) })
				EndIf
			Next nX
			If Len(aResults) <= 0
				IniAResul()
				aSize(aDocs, 0)
				IniADocs()
				oBrwDocs:SetArray(aDocs)
				oBrwDocs:ForceRefresh()
				oBrwDocs:GoTop()
			EndIf
			oBrwResult:SetArray(aResults)
			oBrwResult:ForceRefresh()
			oBrwResult:GoTop()

			If nActiveFld == 2
				montaEstoq()

			ElseIf nActiveFld == 3
				montaSubst()
			EndIf
		EndIf
	EndIf
Return lRet

/*/{Protheus.doc} AtuBrwPrd
Atualiza o browse de produto conforme array carregado pela api

@author renan.roeder
@since 22/11/2019
@version 1.0
/*/
Static Function AtuBrwPrd()
	oBrwProd:SetArray(aProdutos)
	oBrwProd:GoTop()
	oBrwProd:bLine := {||{ aProdutos[oBrwProd:nAt,1],;
	                       aProdutos[oBrwProd:nAt,2],;
	                       aProdutos[oBrwProd:nAt,3]}}
	oBrwProd:Refresh()
Return

/*/{Protheus.doc} AtuDocs
Carrega os documentos conforme os campos filial,ticket,produto,data de necessidade.

@return lRet , Logico  , Indica se a api encontrou registros para atualizar no grid.
@author renan.roeder
@since 22/11/2019
@version 1.0
/*/
Static Function AtuDocs()
	Local aResMat  := {}
	Local aItems   := {}
	Local cError   := ""
	Local cVersao  := ""
	Local nX       := 0
	Local nLinha   := oBrwResult:nAt
	Local nLenAite := 0
	Local nTamFil  := FwSizeFilial()
	Local nTamCod  := GetSx3Cache("VC_VERSAO","X3_TAMANHO")
	Local lRet     := .T.
	Local oJsonRes := JsonObject():New()

	//Flag para mudar a cor da linha selecionada
	If Len(aResults) > 0
		For nX := 1 To Len(aResults)
			aResults[nX,Len(aResults[nX])] := .F.
		Next nX
		aResults[nLinha,Len(aResults[nLinha])] := .T.
		oBrwResult:Refresh()
	EndIf

	aSize(aDocs, 0)
	If Len(aProdutos) > 0
		aResMat := MrpGetTPD(cFilAnt, cTicket, aProdutos[oBrwProd:nAt][1], aProdutos[oBrwProd:nAt][4], aResults[oBrwResult:nAt][1],"parentDocumentType,childDocument,originalNecessity,stockBalanceQuantity,quantityStockWriteOff,alocationQuantity,quantitySubstitution,quantityNecessity,consumptionLocation,necessityDate,parentDocument,structureReview,routing,operation,erpDocumentType,erpDocument,recordKey,substitutionKey,sequenceInStructure,productionVersion","recno",,9999,"Rastreio")
		cError := oJsonRes:FromJson(aResMat[2])

		If !Empty(cError)
			lRet := .F.
			IniADocs()
		Else
			aItems := oJsonRes["items"]
			nLenAite := Len(aItems)

			For nX := 1 to nLenAite
				If oJsParametros["exibe_necessidade_zerada"] .OR. aItems[nX]["quantityNecessity"] > 0
					If aItems[nX]["parentDocumentType"] == "1"
						aItems[nX]["parentDocumentType"] := "3"
					ElseIf aItems[nX]["parentDocumentType"] == "3"
						aItems[nX]["parentDocumentType"] := "1"
					EndIf

					cVersao := ""
					If !Empty(aItems[nX]["productionVersion"])
						cVersao := SubStr(aItems[nX]["productionVersion"], nTamFil+1, nTamCod)
					EndIf

					aAdd(aDocs, { aItems[nX]["childDocument"]                      ,;
					              aItems[nX]["originalNecessity"]                  ,;
					              aItems[nX]["stockBalanceQuantity"]               ,;
					              aItems[nX]["quantityStockWriteOff"]              ,;
					              aItems[nX]["alocationQuantity"]                  ,;
					              aItems[nX]["quantitySubstitution"]               ,;
					              aItems[nX]["quantityNecessity"]                  ,;
					              aItems[nX]["consumptionLocation"]                ,;
					              StoD(StrTran(aItems[nX]["necessityDate"],"-","")),;
					              aItems[nX]["parentDocumentType"]                 ,;
					              aItems[nX]["parentDocument"]                     ,;
					              aItems[nX]["sequenceInStructure"]                ,;
					              aItems[nX]["structureReview"]                    ,;
					              aItems[nX]["routing"]                            ,;
								  aItems[nX]["operation"]                          ,;
					              aItems[nX]["erpDocumentType"]                    ,;
								  aItems[nX]["erpDocument"]                        ,;
					              cVersao                                          ,;
					              aItems[nX]["recordKey"]                          ,; //Campo Invis�vel - Chave
					              aItems[nX]["substitutionKey"]                    ,; //Campo Invis�vel - Chave Substitui��o
					              Iif(nX==1, .T., .F.) })
				EndIf
			Next nX
			If Len(aDocs) <= 0
				IniADocs()
			EndIf
		EndIf
	EndIf
	oBrwDocs:SetArray(aDocs)
	oBrwDocs:ForceRefresh()
	oBrwDocs:GoTop()
Return lRet

/*/{Protheus.doc} IniAResul
Inicializa array do grid de resultados

@author renan.roeder
@since 28/11/2019
@version 1.0
/*/
Static Function IniAResul()
	aAdd(aResults, Array(8))
	aResults[1][1] := SToD("")
	aResults[1][2] := 0
	aResults[1][3] := 0
	aResults[1][4] := 0
	aResults[1][5] := 0
	aResults[1][6] := 0
	aResults[1][7] := 0
	aResults[1][8] := .T.
Return

/*/{Protheus.doc} IniADocs
Inicializa array do grid de documentos

@author renan.roeder
@since 28/11/2019
@version 1.0
/*/
Static Function IniADocs()
	aAdd(aDocs, Array(22))
	aDocs[1][1]  := ""
	aDocs[1][2]  := 0
	aDocs[1][3]  := 0
	aDocs[1][4]  := 0
	aDocs[1][5]  := 0
	aDocs[1][6]  := 0
	aDocs[1][7]  := 0
	aDocs[1][8]  := ""
	aDocs[1][9]  := SToD("")
	aDocs[1][10] := ""
	aDocs[1][11] := ""
	aDocs[1][12] := ""
	aDocs[1][13] := ""
	aDocs[1][14] := ""
	aDocs[1][15] := ""
	aDocs[1][16] := ""
	aDocs[1][17] := ""
	aDocs[1][18] := ""
	aDocs[1][19] := ""
	aDocs[1][20] := ""
	aDocs[1][21] := ""
	aDocs[1][22] := .T.
Return

/*/{Protheus.doc} PCPA144F3
Abre a consulta de tickets do MRP.

@author renan.roeder
@since 22/11/2019
@version 1.0
/*/
Function PCPA144F3()
	Local nOpcao := 0
	Local oConTicket

	oConTicket := ConsultaTickets():New()
	nOpcao := oConTicket:AbreTela()
	If nOpcao != 0
		cTicket := oConTicket:GetCodigo()
		oTGet1:CtrlRefresh()
	EndIf
	oConTicket:Destroy()

Return

/*/{Protheus.doc} AbreParam
Abre a consulta de parametros do MRP.

@author renan.roeder
@since 22/11/2019
@version 1.0
/*/
Static Function AbreParam()
	Local lRet := .T.
	oConParam := ConsultaParametros():New(aParMRP)
	oConParam:AbreTela()
	oConParam:Destroy()
Return lRet

/*/{Protheus.doc} PCPA144PAR
Carga de parametros do MRP

@author Renan Roeder
@since 26/11/2019
@version P12
@param cTicketMRP, Character, N�mero do ticket do MRP
@param aParametro, Array    , Retorna por refer�ncia o array com os par�metros do MRP.
                              Estrutura do array:
                              aParametro[nIndex][1] - ID do Registro (utilizado para exibir em tela). Sempre 0
                              aParametro[nIndex][2] -> Array
                              aParametro[nIndex][2][nIndice][1] - Descri��o do par�metro
                              aParametro[nIndex][2][nIndice][2] - Descri��o do conte�do do par�metro
                              aParametro[nIndex][2][nIndice][3] - C�digo do par�metro
                              aParametro[nIndex][2][nIndice][4] - Valor do par�metro
@param lAtuTela  , L�gico   , Indica se os componentes de tela devem ser atualizados.
@return lRet     , L�gico   , Valida��o do campo ticket
/*/
Function PCPA144PAR(cTicketMRP, aParametro, lAtuTela)
	Local aParam   := {}
	Local aItems   := {}
	Local cError   := ""
	Local lRet     := .T.
	Local nX       := 0
	Local nLenIte  := 0
	Local oJsonPar := JsonObject():New()

	Default lAtuTela := .F.

	aParam := MrpGetPar(cFilAnt, cTicketMRP, "ticket,parameter,value,list",,,9999,"Parametros")

	cError := oJsonPar:FromJson(aParam[2])

	aSize(aParametro, 0)
	If Empty(cError)
		aItems := oJsonPar["items"]
		nLenIte := Len(aItems)

		For nX := 1 to nLenIte
			If (aItems[nX]["parameter"] == "structurePrecision";
					.Or. aItems[nX]["parameter"] == "cAutomacao";
					.Or. aItems[nX]["parameter"] == "setupCode";
					.Or. aItems[nX]["parameter"] == "cAutomacao";
					.Or. aItems[nX]["parameter"] == "ticket")

				Loop
			EndIf

			If aItems[nX]["parameter"] == "setupDescription"
				cSetup   := aItems[nX]["value"]
				oTGet2:CtrlRefresh()
				Loop
			EndIf
			If aItems[nX]["parameter"] == "periodType"
				cPeriodo := GetParam({aItems[nX]["parameter"], aItems[nX]["value"]})[2]
				oTGet3:CtrlRefresh()
				Loop
			EndIf
			If aItems[nX]["parameter"] == "demandStartDate"
				cDataIni := GetParam({aItems[nX]["parameter"], aItems[nX]["value"]})[2]
				oTGet4:CtrlRefresh()
				Loop
			EndIf
			If aItems[nX]["parameter"] == "demandEndDate"
				cDataFim := GetParam({aItems[nX]["parameter"], aItems[nX]["value"]})[2]
				oTGet5:CtrlRefresh()
				Loop
			EndIf

			aAdd(aParametro, {0, GetParam({aItems[nX]["parameter"], aItems[nX]["value"], aItems[nX]["list"]}) })
		Next nX

		aParametro := aSort(aParametro,,, { |x, y| x[2][1] < y[2][1] } )

	ElseIf lAtuTela
		cPeriodo := ""
		oTGet3:CtrlRefresh()

		cDataIni := ""
		oTGet4:CtrlRefresh()

		cDataFim := ""
		oTGet5:CtrlRefresh()
	EndIf

	aSize(aItems, 0)
	FreeObj(oJsonPar)

Return lRet

/*/{Protheus.doc} GetParam
Trata os registros para apresentar a tabela de parametros do MRP.
@author Renan Roeder
@since 26/11/2019
@version P12
@return aParametro, array, array com ticket, descri��o e valor do parametro
/*/
Static Function GetParam(aParametro)
	Local aRetorno := Array(4)

	Do Case
	Case aParametro[1] == "user"
		aRetorno[1] := STR0040 //"Usu�rio"
		aRetorno[2] := UsrFullName(aParametro[2])
	Case aParametro[1] == "numberOfPeriods"
		aRetorno[1] := STR0041 //"N�mero de Per�odos"
		aRetorno[2] := aParametro[2]
	Case aParametro[1] == "demandStartDate"
		aRetorno[1] := STR0042 //"Data In�cio Demandas"
		aRetorno[2] := StoD(StrTran(aParametro[2],"-",""))
	Case aParametro[1] == "demandEndDate"
		aRetorno[1] := STR0043 //"Data Fim Demandas"
		aRetorno[2] := StoD(StrTran(aParametro[2],"-",""))
	Case aParametro[1] == "mrpStartDate"
		aRetorno[1] := STR0044 //"Data Inicio MRP"
		aRetorno[2] := StoD(StrTran(aParametro[2],"-",""))
	Case aParametro[1] == "orderPoint"
		aRetorno[1] := STR0045 //"Ponto de Pedido"
		aRetorno[2] := GetSimNao(aParametro[2]) // 1-Sim/2-N�o
	Case aParametro[1] == "firmHorizon"
		aRetorno[1] := STR0046 //"Horizonte Firme"
		aRetorno[2] := GetSimNao(aParametro[2])  // 1-Sim/2-N�o
	Case aParametro[1] == "leadTime"
		aRetorno[1] := STR0047 //"Lead Time"
		aRetorno[2] := GetCalend(aParametro[2])  // 1-Sem Calend�rio/2-Dias Corridos/3-Dias �teis
	Case aParametro[1] == "demandType"
		aRetorno[1] := STR0048 //"Tipo Demanda"
		aRetorno[2] := GetDemanda(aParametro[2])  // 1-Pedido Venda/2-Previs�o Vendas/3-Plano Mestre/4-Empenhos Projeto/5-Manual
	Case aParametro[1] == "documentType"
		aRetorno[1] := STR0049 //"Tipo Documento"
		aRetorno[2] := GetDocumen(aParametro[2])  // 1-Previstas/2-Suspensas/3-Sacramentadas
	Case aParametro[1] == "safetyStock"
		aRetorno[1] := STR0050 //"Estoque Seguran�a"
		aRetorno[2] := GetSimNao(aParametro[2])  // 1-Sim/2-N�o
	Case aParametro[1] == "rejectedQuality"
		aRetorno[1] := STR0051 //"Estoque Rejeitado CQ"
		aRetorno[2] := GetSubMan(aParametro[2])  // 1-Subtrai/2-Mant�m
	Case aParametro[1] == "maxStock"
		aRetorno[1] := STR0052 //"Estoque M�ximo"
		aRetorno[2] := GetSimNao(aParametro[2])  // 1-Sim/2-N�o
	Case aParametro[1] == "consignedIn"
		aRetorno[1] := STR0053 //"Estoque DE Terceiro"
		aRetorno[2] := GetSubMan(aParametro[2])  // 1-Subtrai/2-Mant�m
	Case aParametro[1] == "cEmpAnt"
		aRetorno[1] := STR0054 //"Empresa"
		aRetorno[2] := aParametro[2]
	Case aParametro[1] == "cFilAnt"
		aRetorno[1] := STR0055 //"Filial"
		aRetorno[2] := aParametro[2]
	Case aParametro[1] == "periodType"
		aRetorno[1] := STR0056 //"Tipo de Per�odo"
		aRetorno[2] := GetTpPer(aParametro[2]) // 1-Di�rio/2-Semanal/3-Quinzenal/4-Mensal
	Case aParametro[1] == "consignedOut"
		aRetorno[1] := STR0057 //"Estoque EM Terceiro"
		aRetorno[2] := GetSomaN(aParametro[2]) // 1-Soma/2-N�o Soma
	Case aParametro[1] == "blockedLot"
		aRetorno[1] := STR0058 //"Estoque Bloqueado por Lote"
		aRetorno[2] := GetSubMan(aParametro[2])  // 1-Subtrai/2-Mant�m
	Case aParametro[1] == "consolidatePurchaseRequest"
		aRetorno[1] := STR0100 //"Aglutina Solicita��o de Compras"
		aRetorno[2] := GetAglut(aParametro[2])  // 1-Aglutina/2-N�o Aglutina/3-Aglutina Somente Demandas
	Case aParametro[1] == "consolidateProductionOrder"
		aRetorno[1] := STR0101 //"Aglutina Ordem de Produ��o"
		aRetorno[2] := GetAglut(aParametro[2])  // 1-Aglutina/2-N�o Aglutina/3-Aglutina Somente Demandas
	Case aParametro[1] == "demandsProcessed"
		aRetorno[1] := STR0102 //"Considera Demandas j� Processadas"
		aRetorno[2] := GetSimNao(aParametro[2])  // 1-Sim/2-N�o
	Case aParametro[1] == "usesProductIndicator"
		aRetorno[1] := STR0145 //"Considera a tabela Indicador de Produto"
		aRetorno[2] := GetSimNao(aParametro[2])  // 1-Sim/2-N�o
	Case aParametro[1] == "usesInProcessLocation"
		aRetorno[1] := GetDescMV( "MV_GRVLOCP")
		aRetorno[2] := GetSimNao(aParametro[2])  // 1-Sim/2-N�o
	Case aParametro[1] == "inProcessLocation"
		aRetorno[1] := GetDescMV( "MV_LOCPROC")
		aRetorno[2] := aParametro[2]
	Case aParametro[1] == "lGeraDoc"
		aRetorno[1] := STR0182 //"Gerar Documentos ao T�rmino do C�lculo"
		aRetorno[2] := GetSimNao(aParametro[2])  // 1-Sim/2-N�o
	Case aParametro[1] == "expiredLot"
		aRetorno[1] := GetDescMV( "MV_LOTVENC")
		aRetorno[2] := GetSimNao(aParametro[2]) // 1-Sim/2-N�o
	Case aParametro[1] == "packingQuantityFirst"
		aRetorno[1] := GetDescMV( "MV_USAQTEM" )
		aRetorno[2] := GetSimNao(aParametro[2]) // 1-Sim/2-N�o
	Case aParametro[1] == "productionOrderPerLot"
		aRetorno[1] := GetDescMV( "MV_QUEBROP" )
		aRetorno[2] := GetSimNao(aParametro[2]) // 1-Sim/2-N�o
	Case aParametro[1] == "purchaseRequestPerLot"
		aRetorno[1] := GetDescMV( "MV_QUEBRSC" )
		aRetorno[2] := GetSimNao(aParametro[2]) // 1-Sim/2-N�o
	Case aParametro[1] == "breakByMinimunLot"
		aRetorno[1] := GetDescMV( "MV_FORCALM" )
		aRetorno[2] := GetTFDesc(aParametro[2]) // 1 - verdadeiro/2-falso
	Case aParametro[1] == "minimunLotAsEconomicLot"
		aRetorno[1] := GetDescMV( "MV_SUBSLE" )
		aRetorno[2] := GetTFDesc(aParametro[2]) // 1 - verdadeiro/2-falso
	Case aParametro[1] == "productGroups"
		aRetorno[1] := STR0153 //"Grupo de produtos"
		aRetorno[2] := aParametro[3]
	Case aParametro[1] == "productTypes"
		aRetorno[1] := STR0152 //"Tipo de produtos"
		aRetorno[2] := aParametro[3]
	Case aParametro[1] == "documents"
		aRetorno[1] := STR0151 //"Documentos"
		aRetorno[2] := aParametro[3]
	Case aParametro[1] == "warehouses"
		aRetorno[1] := STR0150 //"Armaz�ns"
		aRetorno[2] := aParametro[3]
	Case aParametro[1] == "demandCodes"
		aRetorno[1] := STR0185 //"Demandas do MRP"
		aRetorno[2] := aParametro[3]
	Case aParametro[1] == "products"
		aRetorno[1] := STR0032 //"Produtos"
		aRetorno[2] := aParametro[3]
	Case aParametro[1] == "eventLog"
		aRetorno[1] := STR0186 //"Gerar Log de Eventos"
		aRetorno[2] := GetSimNao(aParametro[2]) // 1-Sim/2-N�o
	Case aParametro[1] == "productionOrderNumber"
		aRetorno[1] := STR0187 //"Incrementa Ordem de Produ��o"
		aRetorno[2] := GetItemNum(aParametro[2]) // 1-Por Item/2-Por N�mero
	Case aParametro[1] == "purchaseRequestNumber"
		aRetorno[1] := STR0192 //"Incrementa Solicita��o de Compras"
		aRetorno[2] := GetItemNum(aParametro[2]) // 1-Por Item/2-Por N�mero
	Case aParametro[1] == "productionOrderType"
		aRetorno[1] := STR0193 //"Gerar Documentos"
		aRetorno[2] := GetPrvFrm(aParametro[2]) // 1-Previstos/2-Firmes
	Case aParametro[1] == "qualityWarehouse"
		aRetorno[1] := STR0196 //"MV_CQ: Armaz�m usado no controle da qualidade."
		aRetorno[2] := aParametro[2]
	Case aParametro[1] == "usesLaborProduct"
		aRetorno[1] := GetDescMV( "MV_PRODMOD" ) //Indicado para clientes que n�o utilizam produtos m�o de obra (B1_CCCUSTO) e desejam melhorar a performance das rotinas de fechamento.
		aRetorno[2] := Iif(aParametro[2] == "1", STR0204, STR0205) // "1 - Habilita utiliza��o de produto MOD com o preenchimento do campo B1_CCCUSTO." + "2 - Desabilita utiliza��o de produto MOD com o preenchimento do campo B1_CCCUSTO."
	Case aParametro[1] == "standardTimeUnit"
		aRetorno[1] := GetDescMV( "MV_TPHR" ) //Unidade de medida da mao de obra na  estrutura  do produto. H = Horas por peca, P = Pecas por hora.
		aRetorno[2] := Iif(aParametro[2] == "H", STR0200, Iif(aParametro[2] == "P", STR0201, aParametro[2])) //"H = Horas por pe�a" + "P = Pe�as por hora"
	Case aParametro[1] == "unitOfLaborInTheBOM"
		aRetorno[1] := GetDescMV( "MV_UNIDMOD" ) //Define a unidade padrao para tempos utilizados pelo sistema de PCP e ESTOQUE. O tipo de hora pode ser "N" -> Normal ou "C" -> Centesimal.
		aRetorno[2] := Iif(aParametro[2] == "N", STR0202, Iif(aParametro[2] == "C", STR0203, aParametro[2])) //"N = Normal" + "C = Centesimal"
	Case aParametro[1] == "allocationSuggestion"
		aRetorno[1] := STR0208 //"Sugere Lote e Endere�o nos Empenhos"
		aRetorno[2] := GetSimNao(AllTrim(aParametro[2])) // 1-Sim/2-N�o
	Otherwise
		aRetorno[1] := aParametro[1]
		aRetorno[2] := aParametro[2]
	EndCase

	aRetorno[3] := aParametro[1]
	aRetorno[4] := aParametro[2]

Return aRetorno

/*/{Protheus.doc} GetDescMV
Retorna a descri��o do par�metro MV
@author brunno.costa
@since 02/01/2020
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetDescMV( cParam )
	Local cParDesc := ""
	GetMV(cParam)
	cParDesc := StrTran(x6Descric() + " ", "  ", " ")
	cParDesc += StrTran(x6Desc1() + " " , "  ", " ")
	cParDesc += x6Desc2()
	cParDesc := cParam + ": " + StrTran(AllTrim(cParDesc), "  ", " ")
	If "- " $ cParDesc .AND. Substr(cParDesc,  At("- ", cParDesc) - 1, 1) != " "
		cParDesc := StrTran(AllTrim(cParDesc), "- ", "")
	EndIf
Return cParDesc

/*/{Protheus.doc} GetSimNao
Fun��o para trazer a descri��o do parametro informado
@author Renan Roeder
@since 27/11/2019
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetSimNao(cParam)
	Local cParDesc := ""
	If cParam == "1"
		cParDesc := STR0059 //"Sim"
	Else
		cParDesc := STR0060 //"N�o"
	EndIf
Return cParDesc

/*/{Protheus.doc} GetCalend
Fun��o para trazer a descri��o do parametro informado
@author Renan Roeder
@since 27/11/2019
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetCalend(cParam)
	Local cParDesc := ""
	Do Case
	Case cParam == "1"
		cParDesc := STR0061 //"Sem Calend�rio"
	Case cParam == "2"
		cParDesc := STR0062 //"Dias Corridos"
	Case cParam == "3"
		cParDesc := STR0063 //"Dias �teis"
	EndCase
Return cParDesc

/*/{Protheus.doc} GetDemanda
Fun��o para trazer a descri��o do parametro informado
@author Renan Roeder
@since 27/11/2019
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetDemanda(cParam)
	Local cParDesc := ""
	Local cPar := ""
	Local cDes := ""
	Local nIndex   := 0
	For nIndex := 1 To Len(cParam)
		cDes := ""
		cPar := SubStr(cParam,nIndex,1)
		Do Case
		Case cPar == "1"
			cDes := STR0064 //"Pedido de Venda"
		Case cPar == "2"
			cDes := STR0065 //"Previs�o de Vendas"
		Case cPar == "3"
			cDes := STR0066 //"Plano Mestre"
		Case cPar == "4"
			cDes := STR0067 //"Empenhos Projeto"
		Case cPar == "9"
			cDes := STR0068 //"Manual"
		EndCase
		If Empty(cParDesc)
			cPardesc := cDes
		Else
			If !Empty(cDes)
				cPardesc := cPardesc + ", " + cDes
			EndIf
		EndIf
	Next nIndex
Return cParDesc

/*/{Protheus.doc} GetDocumen
Fun��o para trazer a descri��o do parametro informado
@author Renan Roeder
@since 27/11/2019
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetDocumen(cParam)

	Local cParDesc := ""

	If "|1.1|" $ cParam
		cPardesc := STR0197 //"Previstos -> Exclui"
	EndIf

	If "|1.2|" $ cParam
		cPardesc := STR0198 //"Previstos -> N�o Exclui"
	EndIf

	If "|1.3|" $ cParam
		cPardesc := STR0199 //"Previstos -> Entra no MRP"
	EndIf

	If "|2|" $ cParam
		cPardesc := cPardesc + ", " + STR0070
	EndIf

	If "|3|" $ cParam
		cPardesc := cPardesc + ", " + STR0071
	EndIf

Return cParDesc

/*/{Protheus.doc} GetSubMan
Fun��o para trazer a descri��o do parametro informado
@author Renan Roeder
@since 27/11/2019
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetSubMan(cParam)
	Local cParDesc := ""
	If cParam == "1"
		cParDesc := STR0072 //"Subtrai"
	Else
		cParDesc := STR0073 //"Mant�m"
	EndIf
Return cParDesc

/*/{Protheus.doc} GetTpPer
Fun��o para trazer a descri��o do parametro informado
@author Renan Roeder
@since 27/11/2019
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetTpPer(cParam)
	Local cParDesc := ""
	Do Case
	Case cParam == "1"
		cParDesc := STR0074 //"Di�rio"
	Case cParam == "2"
		cParDesc := STR0075 //"Semanal"
	Case cParam == "3"
		cParDesc := STR0076 //"Quinzenal"
	Case cParam == "4"
		cParDesc := STR0077 //"Mensal"
	EndCase
Return cParDesc

/*/{Protheus.doc} GetAglut
Fun��o para trazer a descri��o do parametro informado
@author Renan Roeder
@since 03/12/2019
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetAglut(cParam)
	Local cParDesc := ""
	Do Case
	Case cParam == "1"
		cParDesc := STR0097 //"Aglutina"
	Case cParam == "2"
		cParDesc := STR0098 //"N�o Aglutina"
	Case cParam == "3"
		cParDesc := STR0099 //"Aglutina Somente Demandas"
	EndCase
Return cParDesc

/*/{Protheus.doc} GetSomaN
Fun��o para trazer a descri��o do parametro informado
@author Renan Roeder
@since 27/11/2019
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetSomaN(cParam)
	Local cParDesc := ""
	If cParam == "1"
		cParDesc := STR0078 //"Soma"
	Else
		cParDesc := STR0079 //"N�o Soma"
	EndIf
Return cParDesc

/*/{Protheus.doc} GETTFDesc
Fun��o para trazer a descri��o do parametro informado
@author brunno.costa
@since 02/01/2020
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GETTFDesc(cParam)
	Local cParDesc := ""
	If cParam == "1"
		cParDesc := STR0146 //"verdadeiro"
	Else
		cParDesc := STR0147 //"falso"
	EndIf
Return cParDesc

/*/{Protheus.doc} chgFolder
Change Folder de Documentos
@author brunno.costa
@since 03/12/2019
@version P12
@param 01 nFolder, n�mero, indice do folder
@return lReturn, l�gico, indica se permite selecionar o folder
/*/
Static Function chgFolder(nFolder)
	Local lReturn := .T.

	nActiveFld := nFolder

	If nFolder == 2
		montaEstoq()

	ElseIf nFolder == 3
		montaSubst()

	EndIf

Return lReturn

/*/{Protheus.doc} kendoOk
Verifica se o pacote do KendoChart est� compilado no RPO
@author brunno.costa
@since 13/01/2020
@version P12
/*/
Static Function kendoOk()
	Local aApoInfo   := GetApoInfo("kendouichart.zip")
	Local lCompilado := !Empty(aApoInfo)
Return lCompilado

/*/{Protheus.doc} montaEstoq
Monta tela de Documentos - Estoque
@author brunno.costa
@since 03/12/2019
@version P12
/*/
Static Function montaEstoq()

	Local aSeries    := {}
	Local cTitulo    := ""
	Local oKendo
	Local aData

	If !kendoOk() .OR. oPnlEst == NIL .or. oPnlEst:nClientHeight == 0 .OR. oPnlEst:nClientWidth == 0
		Return
	EndIf

	If Empty(oBrwProd:aArray[oBrwProd:nAT,1])
		lReturn := .F.
	Else
		oKendo  := KendoChart():New(oPnlEst, oPnlEst:nClientHeight, oPnlEst:nClientWidth)
		aData   := DadoEstGR1()

		oKendo:AddChart('chart1', cTitulo, "bottom", .F.)

		If !Empty(oJsParametros["grafico_saldo_inicial"])
			Aadd(aSeries, KendoSeries():New(STR0119,"y1", "x1", , , oJsParametros["grafico_saldo_inicial"]))              //"Saldo Inicial"
		EndIf

		If !Empty(oJsParametros["grafico_entradas"])
			Aadd(aSeries, KendoSeries():New(STR0120,"y2", "x2", , , oJsParametros["grafico_entradas"]))                   //"Entradas"
		EndIf

		If !Empty(oJsParametros["grafico_saidas"])
			Aadd(aSeries, KendoSeries():New(AjAcento(STR0121),"y3", "x3", , , oJsParametros["grafico_saidas"]))           //"Sa�das"
		EndIf

		If !Empty(oJsParametros["grafico_saidas_estrutura"])
			Aadd(aSeries, KendoSeries():New(AjAcento(STR0122),"y4", "x4", , , oJsParametros["grafico_saidas_estrutura"])) //Sa�das Estrutura
		EndIf

		If !Empty(oJsParametros["grafico_saldo_final"])
			Aadd(aSeries, KendoSeries():New(STR0123,"y5", "x5", , , oJsParametros["grafico_saldo_final"]))                //"Saldo Final"
		EndIf

		If !Empty(oJsParametros["grafico_necessidade"])
			Aadd(aSeries, KendoSeries():New(STR0124,"y6", "x6", , , oJsParametros["grafico_necessidade"]))                //"Necessidade"
		EndIf

		If !Empty(oJsParametros["grafico_saldo_posterior"])
			Aadd(aSeries, KendoSeries():New(STR0125,"y7", "x7", , , oJsParametros["grafico_saldo_posterior"]))            //"Saldo Posterior"
		EndIf

		oKendo:SetSeries('chart1', aSeries)
		oKendo:SetData('chart1'  , aData  )

		oBtnGR1 := TBtnBmp2():New( 01, nPosGR1Btn, 26, 26,'engrenagem',,,,{||LoadMVPar("PCPA144GR1", .T.)},oPnlEst,,,.T. )
		oPnlEst:Refresh()

	EndIf

Return Nil

/*/{Protheus.doc} DadoEstGR1
Gera dados para o Gr�fico de Movimenta��o de Estoque
@author brunno.costa
@since 03/12/2019
@version P12
/*/
Static Function DadoEstGR1()
	Local nInd
	Local aData      := {}
	Local oData
	Local nTotal     := Len(aResults)
	Local nPosSldIni := aScan(oBrwResult:aHeader, {|x| AllTrim(x[2]) == "HWB_QTSLES"})
	Local nPosEntrad := aScan(oBrwResult:aHeader, {|x| AllTrim(x[2]) == "HWB_QTENTR"})
	Local nPosSaidas := aScan(oBrwResult:aHeader, {|x| AllTrim(x[2]) == "HWB_QTSAID"})
	Local nPosSaidEs := aScan(oBrwResult:aHeader, {|x| AllTrim(x[2]) == "HWB_QTSEST"})
	Local nPosSaldo  := aScan(oBrwResult:aHeader, {|x| AllTrim(x[2]) == "HWB_QTSALD"})
	Local nPosNecess := aScan(oBrwResult:aHeader, {|x| AllTrim(x[2]) == "HWB_QTNECE"})
	Local nPosData   := aScan(oBrwResult:aHeader, {|x| AllTrim(x[2]) == "HWB_DATA"})
	Local cData

	For nInd := 1 To nTotal
		If oJsParametros["exibe_necessidade_zerada"] .OR. oBrwResult:aCols[nInd, nPosNecess] > 0
			oData := JsonObject():New()
			cData := DtoC(oBrwResult:aCols[nInd, nPosData])
			oData['y1'] := oBrwResult:aCols[nInd, nPosSldIni]
			oData['x1'] := cData

			oData['y2'] := oBrwResult:aCols[nInd, nPosEntrad]
			oData['x2'] := cData

			oData['y3'] := oBrwResult:aCols[nInd, nPosSaidas]
			oData['x3'] := cData

			oData['y4'] := oBrwResult:aCols[nInd, nPosSaidEs]
			oData['x4'] := cData

			oData['y5'] := oBrwResult:aCols[nInd, nPosSaldo]
			oData['x5'] := cData

			oData['y6'] := oBrwResult:aCols[nInd, nPosNecess]
			oData['x6'] := cData

			oData['y7'] := oBrwResult:aCols[nInd, nPosNecess] + oBrwResult:aCols[nInd, nPosSaldo]
			oData['x7'] := cData

			Aadd(aData, oData)
		EndIf
	Next

Return aData


/*/{Protheus.doc} montaSubst
Monta tela de Documentos - Substitui��o
@author brunno.costa
@since 03/12/2019
@version P12
/*/
Static Function montaSubst()

	Local aSubstitui  := {}
	Local aHeaderSub  := {}
	Local cTitulo     := ""
	Local nTamanho    := 0
	Local nDecimal    := 0
	Local cValid      := ""
	Local nIndTRT
	Local nIndData
	Local nIndTPDoc
	Local nIndDocPai
	Local nIndQtdSub
	Local nIndChave
	Local nIndChvSub
	Local cChave
	Local cChaveSubs
	Local cComponente := ""
	Local cDescricao  := ""
	Local cDocumento  := ""
	Local nQtdTotal   := 0
	Local cOptionalID := ""

	If Empty(oBrwDocs:aCols)
		Return
	EndIf

	nIndTRT     := aScan(oBrwDocs:aHeader, {|x| AllTrim(x[2]) == "HWC_TRT"})
	nIndData    := aScan(oBrwDocs:aHeader, {|x| AllTrim(x[2]) == "HWC_DATA"})
	nIndTPDoc   := aScan(oBrwDocs:aHeader, {|x| AllTrim(x[2]) == "HWC_TPDCPA"})
	nIndDocPai  := aScan(oBrwDocs:aHeader, {|x| AllTrim(x[2]) == "HWC_DOCPAI"})
	nIndQtdSub  := aScan(oBrwDocs:aHeader, {|x| AllTrim(x[2]) == "HWC_QTSUBS"})
	nIndChave   := Len(oBrwDocs:aCols[oBrwDocs:nAt]) - 2
	nIndChvSub  := Len(oBrwDocs:aCols[oBrwDocs:nAt]) - 1
	cChave      := oBrwDocs:aCols[oBrwDocs:nAt][nIndChave]
	cChaveSubs  := oBrwDocs:aCols[oBrwDocs:nAt][nIndChvSub]

	If Empty(cChaveSubs)
		If oBrwDocs:aCols[oBrwDocs:nAt][nIndQtdSub] != 0 //Registro Substitu�do
			cChaveSubs  := cChave
			cComponente := Left(cChaveSubs, GetSx3Cache("B1_COD", "X3_TAMANHO" ))
			If "|"$cChaveSubs .AND. CHR(13)$cChaveSubs
				cOptionalID := Substr(cChaveSubs, At("|", cChaveSubs), At(chr(13)-1, cChaveSubs))
			EndIf
			cDescricao  := Posicione("SB1", 1, xFilial("SB1") + cComponente, "B1_DESC")
			cDocumento  := oBrwDocs:aCols[oBrwDocs:nAt][nIndDocPai]
			nQtdTotal   := GetQtdOrig(cComponente                            ,;
			                          cOptionalID                            ,;
			                          oBrwDocs:aCols[oBrwDocs:nAt][nIndData] ,;
			                          oBrwDocs:aCols[oBrwDocs:nAt][nIndTPDoc],;
			                          cDocumento                             ,;
			                          oBrwDocs:aCols[oBrwDocs:nAt][nIndTRT])

		Else
			cComponente := ""
			cOptionalID := ""
			cDescricao  := ""
			cDocumento  := ""
			nQtdTotal   := 0

		EndIf

	Else//Registro Substituto
		cComponente := Left(cChaveSubs, GetSx3Cache("B1_COD", "X3_TAMANHO" ))
		If "|"$cChaveSubs .AND. CHR(13)$cChaveSubs
			cOptionalID := Substr(cChaveSubs, At("|", cChaveSubs), At(chr(13)-1, cChaveSubs))
		EndIf
		cDescricao  := Posicione("SB1", 1, xFilial("SB1") + cComponente, "B1_DESC")
		cDocumento  := oBrwDocs:aCols[oBrwDocs:nAt][nIndDocPai]
		nQtdTotal   := GetQtdOrig(cComponente                            ,;
		                          cOptionalID                            ,;
		                          oBrwDocs:aCols[oBrwDocs:nAt][nIndData] ,;
		                          oBrwDocs:aCols[oBrwDocs:nAt][nIndTPDoc],;
		                          cDocumento                             ,;
		                          oBrwDocs:aCols[oBrwDocs:nAt][nIndTRT])

	EndIf

	criaASubst(oBrwDocs:aCols[oBrwDocs:nAt][nIndTPDoc],;
	           cDocumento                            ,;
	           oBrwDocs:aCols[oBrwDocs:nAt][nIndTRT] ,;
	           cChaveSubs                                 )

	If lSubsMont != Nil
		oGetComp:cText  := cComponente
		oGetDesc:cText  := cDescricao
		oGetQtd:cText   := nQtdTotal
		oGetDocto:cText := cDocumento

	Else

		TSay():New(05, 08, {|| STR0108 }, oPnlSubst, ,oFont , , , , .T., , , 40, 20) //"Componente:"
		oGetComp := TGet():New(02, 50, {|u| If(PCount()>0,cComponente:=u,cComponente)},oPnlSubst,100,13,"@!",/*08*/,0,/*10*/,/*11*/,.F.,/*13*/,.T.,/*15*/,.F.,/*17*/,.F.,.F.,/*20*/,.T.,.F.,/*23*/,"cComponente")
		oGetDesc := TGet():New(02, 150, {|u| If(PCount()>0,cDescricao:=u,cDescricao)},oPnlSubst,200,13,"@!",/*08*/,0,/*10*/,/*11*/,.F.,/*13*/,.T.,/*15*/,.F.,/*17*/,.F.,.F.,/*20*/,.T.,.F.,/*23*/,"cDescricao")

		TSay():New(24, 185, {|| STR0109 }, oPnlSubst, ,oFont , , , , .T., , , 50, 20) //"Quantidade Original:"
		oGetQtd  := TGet():New(21, 250, {|u| If(PCount()>0,nQtdTotal:=u,nQtdTotal)},oPnlSubst,100,13,"@E 99,999,999.999",/*08*/,0,/*10*/,/*11*/,.F.,/*13*/,.T.,/*15*/,.F.,/*17*/,.F.,.F.,/*20*/,.T.,.F.,/*23*/,"nQtdTotal")

		TSay():New(24, 08, {|| STR0110 }, oPnlSubst, ,oFont , , , , .T., , , 30, 20) //"Documento:"
		oGetDocto := TGet():New(21, 50, {|u| If(PCount()>0,cDocumento:=u,cDocumento)},oPnlSubst,100,13,"@!",/*08*/,0,/*10*/,/*11*/,.F.,/*13*/,.T.,/*15*/,.F.,/*17*/,.F.,.F.,/*20*/,.T.,.F.,/*23*/,"cDocumento")

		cTitulo  := STR0111 //"Alternativo"
		nTamanho := 15
		nDecimal := 0
		cValid   := ""
		aAdd(aHeaderSub,{cTitulo,"cAlternativo","",nTamanho,nDecimal,cValid,"�","C",""})

		cTitulo  := STR0112 //"Descri��o"
		nTamanho := 30
		nDecimal := 0
		cValid   := ""
		aAdd(aHeaderSub,{cTitulo,"cDescricao","",nTamanho,nDecimal,cValid,"�","C",""})

		cTitulo  := STR0113 //"Saldo"
		nTamanho := 12
		nDecimal := 3
		cValid   := ""
		aAdd(aHeaderSub,{cTitulo,"nSaldo","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

		cTitulo  := STR0114 //"Empenho"
		nTamanho := 12
		nDecimal := 3
		cValid   := ""
		aAdd(aHeaderSub,{cTitulo,"nEmpenho","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

		cTitulo  := STR0115 //"Necessidade"
		nTamanho := 12
		nDecimal := 3
		cValid   := ""
		aAdd(aHeaderSub,{cTitulo,"nNecess","@E 99,999,999.999",nTamanho,nDecimal,cValid,"�","N",""})

		oBrwSubst := MsNewGetDados():New(040,;
		                                 002,;
		                                 oPnlSubst:nClientHeight-100,;
		                                 oPnlSubst:nClientWidth*0.5,;
		                                 GD_UPDATE,;
		                                 /*LinhaOk*/,;
		                                 /*tudoOk*/,;
		                                 /*IniCpos*/,;
		                                 aSubstitui,;
		                                 0,;
		                                 1000,;
		                                 ,;
		                                 ,;
		                                 "AllwaysFalse",;
		                                 oPnlSubst,;
		                                 aHeaderSub,;
		                                 aSubsts,;
		                                 {|| changeSubs()} )

		oBrwSubst:oBrowse:lUseDefaultColors := .F.
		oBrwSubst:oBrowse:SetBlkBackColor({|| GETDCLR(aSubsts,oBrwSubst:nAt)})
		oBrwSubst:oBrowse:SetBlkColor( { || RGB(0,0,0) } )

		lSubsMont := .T.
	EndIf

	oBrwSubst:SetArray(aSubsts)
	oBrwSubst:ForceRefresh()
	oBrwSubst:GoTop()

Return

/*/{Protheus.doc} criaASubst
Popula dados na Grid de Substitui��o
@author brunno.costa
@since 03/12/2019
@version P12
@param 01 - cTpPai    , caracter, tipo do documento pai
@param 02 - cDocumento, caracter, documento pai
@param 03 - cTRT      , caracter, c�digo do TRT do produto
@param 04 - cChaveSubs, caracter, chave de substitui��o do registro
/*/
Static Function criaASubst(cTpPai, cDocumento, cTRT, cChaveSubs)

	Local cError   := ""
	Local aResMat  := {}
	Local aItems   := {}
	Local nTotal
	Local oJsonRes := JsonObject():New()
	Local nInd     := 0

	aSize(aSubsts, 0)
	If Empty(cChaveSubs)
		aAdd(aSubsts, Array(6))
		aSubsts[1][1] := ""
		aSubsts[1][2] := ""
		aSubsts[1][3] := 0
		aSubsts[1][4] := 0
		aSubsts[1][5] := 0
		aSubsts[1][6] := .T.

	Else
		aResMat := MrpGetSUB(cFilAnt, cTicket, cTpPai, cDocumento, cTRT, cChaveSubs,"componentCode,stockBalanceQuantity,alocationQuantity,quantityNecessity",,,9999,"Rastreio")
		cError := oJsonRes:FromJson(aResMat[2])

		If Empty(cError)
			aItems := oJsonRes["items"]
			nTotal := Len(aItems)

			For nInd := 1 to nTotal
				aAdd(aSubsts, Array(6))
				aSubsts[nInd][1] := aItems[nInd]["componentCode"]
				aSubsts[nInd][2] := Posicione("SB1", 1, xFilial("SB1") + aItems[nInd]["componentCode"], "B1_DESC")
				aSubsts[nInd][3] := aItems[nInd]["stockBalanceQuantity"]
				aSubsts[nInd][4] := aItems[nInd]["alocationQuantity"]
				aSubsts[nInd][5] := aItems[nInd]["quantityNecessity"]
				aSubsts[nInd][6] := Iif(nInd==1,.T.,.F.)
			Next

		EndIf

	EndIf

Return

/*/{Protheus.doc} GetDocOrig
Inicializa Grid de Substitui��o
@author brunno.costa
@since 04/12/2019
@version P12
@param 01 - cProdOrig   , caracter, c�digo do produto relacionado
@param 02 - cOptionalID , caracter, c�digo ID do opcional
@param 03 - dNecessidade, data    , data da necessidade
@param 04 - cTipoPai    , caracter, tipo do documento Pai
@param 05 - cDocumento  , caracter, c�digo do documento
@param 06 - cTRT        , caracter, c�gido da sequ�ncia TRT do registro
@return nQtdOrig, n�mero, quantidade substitui��o original
/*/
Static Function GetQtdOrig(cProdOrig, cOptionalID, dNecessidade, cTipoPai, cDocumento, cTRT)
	Local cError   := ""
	Local aResMat  := {}
	Local aItems   := {}
	Local oJsonRes := JsonObject():New()
	Local nX       := 0
	Local nQtdOrig := 0

	If Len(aProdutos) > 0
		aResMat := MrpGetTPD(cFilAnt, cTicket, cProdOrig, cOptionalID, dNecessidade,"parentDocumentType,parentDocument,sequenceInStructure,quantitySubstitution",,,9999,"Rastreio")
		cError := oJsonRes:FromJson(aResMat[2])

		If Empty(cError)
			aItems := oJsonRes["items"]
			nX     := aScan(aItems, {|x| x["parentDocumentType"]  == cTipoPai   .AND.;
			                             x["parentDocument"]      == cDocumento .AND.;
			                             x["sequenceInStructure"] == cTRT             })
			If nX > 0
				nQtdOrig := aItems[nX]["quantitySubstitution"]
			EndIf

		EndIf
	EndIf
Return nQtdOrig

/*/{Protheus.doc} AjAcento
Converte Acentua��o das Palavras para String HTML
@author brunno.costa
@since 04/12/2019
@version P12
@param 01 - cTexto, caracter, texto para convers�o
@return cTexto, caracter, texto convertido
/*/
Static Function AjAcento(cTexto)
	cTexto = StrTran(cTexto, "�", "&aacute;" )
	cTexto = StrTran(cTexto, "�", "&acirc;" )
	cTexto = StrTran(cTexto, "�", "&agrave;" )
	cTexto = StrTran(cTexto, "�", "&atilde;" )
	cTexto = StrTran(cTexto, "�", "&ccedil;" )
	cTexto = StrTran(cTexto, "�", "&eacute;" )
	cTexto = StrTran(cTexto, "�", "&ecirc;" )
	cTexto = StrTran(cTexto, "�", "&iacute;" )
	cTexto = StrTran(cTexto, "�", "&oacute;" )
	cTexto = StrTran(cTexto, "�", "&ocirc;" )
	cTexto = StrTran(cTexto, "�", "&otilde;" )
	cTexto = StrTran(cTexto, "�", "&uacute;" )
	cTexto = StrTran(cTexto, "�", "&uuml;" )
	cTexto = StrTran(cTexto, "�", "&Aacute;" )
	cTexto = StrTran(cTexto, "�", "&Acirc;" )
	cTexto = StrTran(cTexto, "�", "&Agrave;" )
	cTexto = StrTran(cTexto, "�", "&Atilde;" )
	cTexto = StrTran(cTexto, "�", "&Ccedil;" )
	cTexto = StrTran(cTexto, "�", "&Eacute;" )
	cTexto = StrTran(cTexto, "�", "&Ecirc;" )
	cTexto = StrTran(cTexto, "�", "&Iacute;" )
	cTexto = StrTran(cTexto, "�", "&Oacute;" )
	cTexto = StrTran(cTexto, "�", "&Ocirc;" )
	cTexto = StrTran(cTexto, "�", "&Otilde;" )
	cTexto = StrTran(cTexto, "�", "&Uacute;" )
	cTexto = StrTran(cTexto, "�", "&Uuml;" )
Return AllTrim(cTexto)

/*/{Protheus.doc} AjAcento
Converte Acentua��o das Palavras para String HTML
@author brunno.costa
@since 04/12/2019
@version P12
@param 01 - cTexto, caracter, texto para convers�o
@return cTexto, caracter, texto convertido
/*/
Static Function LoadMVPar(cPergunte, lExibe)
	Local lOk := .T.
	lOk := Pergunte(cPergunte, lExibe)
	If cPergunte == "PCPA144FIL"
		oJsParametros["exibe_necessidade_zerada"] := MV_PAR01 == 1

		If lExibe .And. lOk
			FWMsgRun(,{|| AtuProduto() },STR0038,STR0039)
		EndIf

	ElseIf cPergunte == "PCPA144GR1"
		If MV_PAR01 == 1
			oJsParametros["grafico_saldo_inicial"] := "column"
		ElseIf MV_PAR01 == 2
			oJsParametros["grafico_saldo_inicial"] := "line"
		Else
			oJsParametros["grafico_saldo_inicial"] := ""
		EndIf

		If MV_PAR02 == 1
			oJsParametros["grafico_entradas"] := "column"
		ElseIf MV_PAR02 == 2
			oJsParametros["grafico_entradas"] := "line"
		Else
			oJsParametros["grafico_entradas"] := ""
		EndIf

		If MV_PAR03 == 1
			oJsParametros["grafico_saidas"] := "column"
		ElseIf MV_PAR03 == 2
			oJsParametros["grafico_saidas"] := "line"
		Else
			oJsParametros["grafico_saidas"] := ""
		EndIf

		If MV_PAR04 == 1
			oJsParametros["grafico_saidas_estrutura"] := "column"
		ElseIf MV_PAR04 == 2
			oJsParametros["grafico_saidas_estrutura"] := "line"
		Else
			oJsParametros["grafico_saidas_estrutura"] := ""
		EndIf

		If MV_PAR05 == 1
			oJsParametros["grafico_saldo_final"] := "column"
		ElseIf MV_PAR05 == 2
			oJsParametros["grafico_saldo_final"] := "line"
		Else
			oJsParametros["grafico_saldo_final"] := ""
		EndIf

		If MV_PAR06 == 1
			oJsParametros["grafico_necessidade"] := "column"
		ElseIf MV_PAR06 == 2
			oJsParametros["grafico_necessidade"] := "line"
		Else
			oJsParametros["grafico_necessidade"] := ""
		EndIf

		If MV_PAR07 == 1
			oJsParametros["grafico_saldo_posterior"] := "column"
		ElseIf MV_PAR07 == 2
			oJsParametros["grafico_saldo_posterior"] := "line"
		Else
			oJsParametros["grafico_saldo_posterior"] := ""
		EndIf

		If lExibe .AND. nActiveFld == 2
			montaEstoq()
		EndIf

	EndIf
Return

/*/{Protheus.doc} RefreshRes
Realiza Refresh das Telas Ap�s Resize
@author brunno.costa
@since 04/12/2019
@version P12
@param 01 - lLateral, l�gico, indica se refere-se a opera��o de resize da divis�o lateral
/*/
Static Function RefreshRes(lLateral)
	Default lLateral := .F.

	If lLateral
		If lOpenLat
			nPosGR1Btn -= nWidthDif
		Else
			nPosGR1Btn += nWidthDif
		EndIf
	EndIf

	If nActiveFld == 2
		montaEstoq()
	EndIf

	//For�a Reposicionamento ap�s Maximizar - Grid de Resultados
	oBrwResult:SetArray(aResults)
	oBrwResult:ForceRefresh()
	oBrwResult:GoTop()
	oBrwResult:GoTo(oBrwResult:nAt)

	//For�a Reposicionamento ap�s Maximizar - Grid de Documentos
	oBrwDocs:SetArray(aDocs)
	oBrwDocs:ForceRefresh()
	oBrwDocs:GoTop()
	oBrwDocs:GoTo(oBrwDocs:nAt)
Return

/*/{Protheus.doc} gerarDocs
Executa a gera��o dos documentos do ticket consultado.

@type  Static Function
@author lucas.franca
@since 12/12/2019
@version P12.1.28
/*/
Static Function gerarDocs()

	Local cErrorUID := "PCPA145_"+cTicket
	Local oPCPError := PCPMultiThreadError():New(cErrorUID, .F.)
	Local oTimer

	If !validaGera(cTicket)
		Return
	EndIf

	If MsgYesNo(STR0136,STR0137) //"Deseja iniciar o processo de gera��o de documentos?" # "Aten��o"

		oTimer := TTimer():New(1000, {|| AvalErro("PCPA145_"+cTicket) }, oDlg )
		oTimer:Activate()

		FWMsgRun(, {|| oPCPError:startJob("PCPA145", GetEnvServer(), .T., cEmpAnt, cFilAnt, cTicket, aClone(aParMrp), .F., cErrorUID) }, STR0138, STR0139) //"Processando" # "Gerando documentos..."

		//Verifica oorrencia de erros no PCPA145
		If oPCPError:possuiErro()
			oPCPError:final()
		EndIf
		oPCPError:destroy()
		oTimer:DeActivate()

		//Atualiza os dados em tela.
		AtuResulta(oBrwProd:nAT)
	EndIf

Return

/*/{Protheus.doc} AvalErro
Avalia ocorrencia de erros no PCPA145
@type  Static Function
@author brunno.costa
@since 17/07/2020
@version P12.1.27
@param 01 - cErrorUID, caracter, codigo identificador da secao de controle de erros
/*/
Static Function AvalErro(cErrorUID)
	Local oPCPError := PCPMultiThreadError():New(cErrorUID, .F.)
	//Verifica oorrencia de erros no PCPA145
	If oPCPError:possuiErro()
		oPCPError:final()
	EndIf
Return

/*/{Protheus.doc} validaGera
Valida se um ticket pode iniciar o processamento da gera��o de documentos.

@type  Static Function
@author lucas.franca
@since 16/12/2019
@version P12.1.28
@param cTicket, Character, Ticket do MRP para valida��o
@return lRet, Logical, Identifica se o ticket pode gerar os documentos.
/*/
Static Function validaGera(cTicket)
	Local lRet := .T.

	If Empty(cTicket)
		Help( ,  , "Help", ,  STR0134, ; //"N�o foi informado o ticket de processamento do MRP."
		1, 0, , , , , , {STR0135} ) //"Informe o ticket do MRP antes de gerar os documentos."
		lRet := .F.
	EndIf

	If lRet
		HW3->(dbSetOrder(1))
		If HW3->(dbSeek(xFilial("HW3")+cTicket))
			If HW3->HW3_STATUS != "3"
				Help( ,  , "Help", ,  STR0140, ; //"Somente processamentos do MRP com o status 'Finalizado' podem iniciar a gera��o de documentos."
				1, 0, , , , , , {STR0141} ) //"Informe um Ticket com o status 'Finalizado' para gerar os documentos."
				lRet := .F.
			EndIf
		Else
			Help( ,  , "Help", ,  STR0142, ; //"Ticket n�o encontrado nos processamentos do MRP."
			1, 0, , , , , , {STR0143} ) //"Informe um Ticket v�lido para processamento."
			lRet := .F.
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} MenuContxt
Cria Menu de Contexto - Bot�o Direito

@author brunno.costa
@since 10/01/2018
@version 1.0

@return Nil
/*/
Static Function MenuContxt()

	//Criacao do Menu PopUp com as opcoes para a criacao da arvore
	//de perguntas e respostas.
	MENU soMenu POPUP OF oMainWnd
	MENUITEM STR0148 ACTION ConsultOPC() //"Consultar os Opcionais"
	ENDMENU

	//Criacao da arvore de perguntas e respostas.
	//Ao clicar com o botao direito sera exibido o menu popup.
	oBrwProd:bRClicked := {|o,x,y| (MostraMenu(soMenu, x, y+200)) } // Posicao x,y em relacao a Dialog
	oBrwProd:cToolTip  := STR0149	//"Utilize o bot�o direito do mouse para consular os opcionais."

Return

/*/{Protheus.doc} MostraMenu
Exibe menu contexto da Tree (bot�o direito)

@author brunno.costa
@since 10/01/2018
@version P12

@return Nil
@param oMenu, object, objeto oMenu
@param nCoorX, numeric, coordenada X
@param nCoorY, numeric, coordenada Y
@param oArea, object, objeto oDbTree passado por refer�ncia
@type Function
/*/

Static Function MostraMenu(oMenu, nCoorX, nCoorY, oArea)

	oMenu:Activate(nCoorX, nCoorY)

Return Nil

/*/{Protheus.doc} ConsultOPC
Consulta Opcionais

@author brunno.costa
@since 10/01/2018
@version 1.0

@return Nil
/*/
Static Function ConsultOPC()
	Local cProduto    := aProdutos[oBrwProd:nAT,1]
	Local cOptionalID := aProdutos[oBrwProd:nAT,4]
	Local aResults
	Local oJson

	If Empty(cOptionalID)
		VisOpcPcp(cProduto, "", "", 1)
	Else
		If "|"$cOptionalID
			cOptionalID := Left(cOptionalID, (At("|", cOptionalID)-1) )
		EndIf
		aResults    := MrpGetOPC(cFilAnt, cTicket, cOptionalID)
		oJson       := JsonObject():New()
		oJson:fromJson(aResults[2])
		VisOpcPcp(cProduto, Array2STR(oJson["optionalMemo"], .F.), oJson["optionalString"], 2)
	EndIf

Return

/*/{Protheus.doc} vldTicket
Faz a valida��o do ticket informado.

@type  Static Function
@author lucas.franca
@since 31/01/2020
@version P12.1.27
@param cTicket, Character, Ticket informado.
@param aParMRP, Array    , Array para retorno dos par�metros do MRP.
@return lRet, Logic, Indica se o ticket � v�lido.
/*/
Static Function vldTicket(cNumTicket, aParametro)
	Local aReturn := {}
	Local cStatus := ""
	Local lRet    := .T.
	Local oJson   := Nil

	If !Empty(cNumTicket)
		aReturn := MrpGetPrc(cFilAnt, cNumTicket)
		If aReturn[1]
			oJson := JsonObject():New()
			oJson:FromJson(aReturn[2])

			If ! oJson["status"] $ "3|6"
				cStatus := P144Status(oJson["status"])
				Help( ,  , "Help", ,  STR0156 + " '" + AllTrim(cStatus) + "'. " + STR0157, ; //"Ticket com status" 'xxx'. "Consulta n�o permitida."
			         1, 0, , , , , , {STR0158} ) //"Consulte apenas os tickets com status 'Finalizado' ou 'Documentos gerados'."
				lRet := .F.
			EndIf
		Else
			Help( ,  , "Help", ,  STR0116, ; //"N�o foram encontrados registros para o ticket informado."
			     1, 0, , , , , , {STR0117} ) //"Informe um ticket v�lido para consulta."
			lRet := .F.
		EndIf

		If lRet
			PCPA144PAR(cNumTicket, @aParametro, .T.)
		EndIf
	EndIf
Return lRet

/*/{Protheus.doc} changeDocs
Fun��o executada ao mudar de linha na grid de documentos.

@type  Static Function
@author lucas.franca
@since 06/02/2020
@version P12.1.27
@return .T.
/*/
Static Function changeDocs()
	Local nX     := 0
	Local nLinha := oBrwDocs:nAt

	//Flag para mudar a cor da linha selecionada
	If Len(aDocs) > 0
		For nX := 1 To Len(aDocs)
			aDocs[nX,Len(aDocs[nX])] := .F.
		Next nX
		aDocs[nLinha,Len(aDocs[nLinha])] := .T.
	EndIf
	oBrwDocs:Refresh()
Return .T.

/*/{Protheus.doc} changeSubs
Fun��o executada ao mudar de linha na grid de substitui��es.

@type  Static Function
@author lucas.franca
@since 06/02/2020
@version P12.1.27
@return .T.
/*/
Static Function changeSubs()
	Local nX     := 0
	Local nLinha := oBrwSubst:nAt

	//Flag para mudar a cor da linha selecionada
	If Len(aSubsts) > 0
		For nX := 1 To Len(aSubsts)
			aSubsts[nX,Len(aSubsts[nX])] := .F.
		Next nX
		aSubsts[nLinha,Len(aSubsts[nLinha])] := .T.
	EndIf
	oBrwSubst:Refresh()
Return .T.


/*/{Protheus.doc} PCPA144Log
Abre o log de eventos do MRP

@author Douglas Heydt
@since 11/05/2020
@version 1.0
/*/
Function PCPA144Log()
	Local oLogTicket

	oLogTicket := LogTickets():New()
	oLogTicket:AbreTela()
	oLogTicket:Destroy()
Return

/*/{Protheus.doc} GetItemNum
Fun��o para trazer a descri��o do parametro informado
@author brunno.costa
@since 08/05/2020
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetItemNum(cParam)
    Local cParDesc := ""
    Do Case
    Case cParam == "1"
        cParDesc := STR0188 //"Por Item"
    Case cParam == "2"
        cParDesc := STR0189 //"Por N�mero"
    EndCase
Return cParDesc

/*/{Protheus.doc} GetPrvFrm
Fun��o para trazer a descri��o do parametro informado
@author renan.roeder
@since 04/06/2020
@version P12
@param 01 cParam , caracter , c�digo do parametro
@return cParDesc, caracter, descri��o do parametro
/*/
Static Function GetPrvFrm(cParam)
    Local cParDesc := ""
    Do Case
    Case cParam == "1"
        cParDesc := STR0194 //"Previstos"
    Case cParam == "2"
        cParDesc := STR0195 //"Firmes"
    EndCase
Return cParDesc

/*/{Protheus.doc} A144ClData
Fun��o para limpar a tela principal da rotina quando
o ticket consultado previamente n�o existe mais
@author douglas.heydt
@since 28/07/2020
@version P12
/*/
Function A144ClData()

	cTicket := ""
	oTGet1:CtrlRefresh()

	cSetup := ""
	oTGet2:CtrlRefresh()

	cPeriodo := ""
	oTGet3:CtrlRefresh()

	cDataIni := ""
	oTGet4:CtrlRefresh()

	cDataFim := ""
	oTGet5:CtrlRefresh()

	//Grid Produtos
	aSize(aProdutos, 0)
	AtuBrwPrd()
	aturesulta(0)

	//Grid Resultados
	aSize(aResults, 0)
	IniAResul()
	oBrwResult:SetArray(aResults)
	oBrwResult:ForceRefresh()
	oBrwResult:GoTop()

	//Grid Documento
	aSize(aDocs, 0)
	IniADocs()

Return
