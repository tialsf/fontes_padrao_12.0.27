#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "PCPA144.CH"

/*/{Protheus.doc} ConsultaParametros
Classe para constru��o de consulta de parametros do MRP
@author Renan Roeder
@since 25/11/2019
@version P12
/*/
CLASS ConsultaParametros FROM LongClassName

	DATA aParMRP    AS ARRAY
	DATA oView      AS OBJECT
	DATA oModel     AS OBJECT

	METHOD New(aParMRP) CONSTRUCTOR
	METHOD AbreTela()
	METHOD Destroy()

ENDCLASS

/*/{Protheus.doc} New
Construtor da classe para a Consulta de Parametros
@author Renan Roeder
@since 26/11/2019
@version P12
@return Self, objeto, classe ConsultaParametros
/*/
METHOD New(aParMRP) CLASS ConsultaParametros

	::oView      := Nil
	::oModel     := Nil
	::aParMRP    := aClone(aParMRP)

Return Self

/*/{Protheus.doc} Destroy
M�todo para limpar da mem�ria os objetos utilizados pela classe
@author Renan Roeder
@since 26/11/2019
@version P12
@return Nil
/*/
METHOD Destroy() CLASS ConsultaParametros

	If ::oModel <> Nil
		::oModel:DeActivate()
		::oModel:Destroy()
	EndIf

	FreeObj(::oModel)

	If ::oView <> Nil
		::oView:DeActivate()
	EndIf

	FreeObj(::oView)

Return

/*/{Protheus.doc} AbreTela
M�todo para abrir a tela de consulta
@author Renan Roeder
@since 26/11/2019
@version P12
@return
/*/
METHOD AbreTela() CLASS ConsultaParametros

	Local aArea     := GetArea()
	Local aButtons  := { {.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},;
	                     {.T.,STR0013},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil} } //"Fechar"
	Local oViewExec := FWViewExec():New()

	If ::oModel == Nil
		::oModel := ModelDef(::aParMrp)
	EndIf

	If ::oView == Nil
		::oView := ViewDef()
	EndIf

	oViewExec:setModel(::oModel)
	oViewExec:setView(::oView)
	oViewExec:setTitle(STR0093) //"Parametros MRP"
	oViewExec:setOperation(MODEL_OPERATION_VIEW)
	oViewExec:setReduction(55)
	oViewExec:setButtons(aButtons)
	oViewExec:setCancel({|| .T. })
	oViewExec:openView(.F.)

	RestArea(aArea)

Return

/*/{Protheus.doc} ModelDef
Defini��o do Modelo
@author Renan Roeder
@since 26/11/2019
@version P12
@return oModel, objeto, modelo definido
/*/
Static Function ModelDef(aParMrp)

	Local oModel    := MPFormModel():New('PCPA144Par')
	Local oStruCab  := FWFormModelStruct():New()
	Local oStruGrid := FWFormModelStruct():New()

	 StrGridPar(@oStruGrid, .T.)

	//Cria campo para o modelo invis�vel
	oStruCab:AddField(STR0096, STR0096, "ARQ", "C", 1, 0, , , {}, .T., , .F., .F., .F., , )

	//FLD_INVISIVEL - Modelo "invis�vel"
	oModel:addFields('FLD_INVISIVEL', /*cOwner*/, oStruCab, , , {|| LoadMdlFld()})
	oModel:GetModel("FLD_INVISIVEL"):SetDescription(STR0096)
	oModel:GetModel("FLD_INVISIVEL"):SetOnlyQuery(.T.)

	//::cModelName - Grid de resultados (o nome deve ser atribuido atrav�s da propriedade ::cModelName)
	oModel:AddGrid("GRID_RESULTS", "FLD_INVISIVEL", oStruGrid,,,,,{|| aParMrp })
	oModel:GetModel("GRID_RESULTS"):SetDescription(STR0093) //"Parametros MRP"
	oModel:GetModel("GRID_RESULTS"):SetOnlyQuery(.T.)
	oModel:GetModel("GRID_RESULTS"):SetOptional(.T.)

	oModel:SetDescription(STR0096)
	oModel:SetPrimaryKey({})

Return oModel

/*/{Protheus.doc} ViewDef
Defini��o da View
@author Renan Roeder
@since 26/11/2019
@version P12
@return oView, objeto, view definida
/*/
Static Function ViewDef()

	Local oStruGrid := FWFormViewStruct():New()
	Local oView     := FWFormView():New()
	Local oModel    := FWLoadModel("PCPA144Par")

	StrGridPar(@oStruGrid,.F.)

	//Defini��es da View
	oView:SetModel(oModel)

	//V_GRID_RESULTS - View da Grid com resultado da pesquisa
	oView:AddGrid("V_GRID_RESULTS", oStruGrid, "GRID_RESULTS")

	//Relaciona a SubView com o Box
	oView:CreateHorizontalBox("BOX_GRID", 100)
	oView:SetOwnerView("V_GRID_RESULTS", 'BOX_GRID')

	//Habilita os bot�es padr�es de filtro e pesquisa
	//oView:SetViewProperty("V_GRID_RESULTS", "GRIDFILTER", {.T.})
	//oView:SetViewProperty("V_GRID_RESULTS", "GRIDSEEK", {.T.})

Return oView

/*/{Protheus.doc} LoadMdlFld
Carga do modelo mestre (invis�vel)
@author Renan Roeder
@since 26/11/2019
@version P12
@return aLoad, array, array de load do modelo preenchido
/*/
Static Function LoadMdlFld()

	Local aLoad := {}

   aAdd(aLoad, {"A"}) //dados
   aAdd(aLoad, 1    ) //recno

Return aLoad

/*/{Protheus.doc} StrGridPar
Monta a estrutura do grid
@author Renan Roeder
@since 25/11/2019
@version P12
@return cDescricao, caracter, descri��o do setup selecionado na tela
/*/
Static Function StrGridPar(oStruGrid, lModel)

	If lModel
		//Campos do ModelDef
		oStruGrid:AddField(STR0094     ,;    //    [01]  C   Titulo do campo //"Par�metro"
						   STR0094     ,;    //    [02]  C   ToolTip do campo
						   "PARAM"     ,;    //    [03]  C   Id do Field
						   "C"         ,;    //    [04]  C   Tipo do campo
						   150         ,;    //    [05]  N   Tamanho do campo
						   0           ,;    //    [06]  N   Decimal do campo
						   NIL         ,;    //    [07]  B   Code-block de valida��o do campo
						   NIL         ,;    //    [08]  B   Code-block de valida��o When do campo
						   {}          ,;    //    [09]  A   Lista de valores permitido do campo
						   .F.         ,;    //    [10]  L   Indica se o campo tem preenchimento obrigat�rio
						   Nil         ,;    //    [11]  B   Code-block de inicializacao do campo
						   NIL         ,;    //    [12]  L   Indica se trata-se de um campo chave
						   NIL         ,;    //    [13]  L   Indica se o campo pode receber valor em uma opera��o de update.
						   .T.)              //    [14]  L   Indica se o campo � virtual
		oStruGrid:AddField(STR0095     ,;    //    [01]  C   Titulo do campo //"Valor"
						   STR0095     ,;    //    [02]  C   ToolTip do campo
						   "VALOR"     ,;    //    [03]  C   Id do Field
						   "C"         ,;    //    [04]  C   Tipo do campo
						   100         ,;    //    [05]  N   Tamanho do campo
						   0           ,;    //    [06]  N   Decimal do campo
						   NIL         ,;    //    [07]  B   Code-block de valida��o do campo
						   NIL         ,;    //    [08]  B   Code-block de valida��o When do campo
						   {}          ,;    //    [09]  A   Lista de valores permitido do campo
						   .F.         ,;    //    [10]  L   Indica se o campo tem preenchimento obrigat�rio
						   Nil         ,;    //    [11]  B   Code-block de inicializacao do campo
						   NIL         ,;    //    [12]  L   Indica se trata-se de um campo chave
						   NIL         ,;    //    [13]  L   Indica se o campo pode receber valor em uma opera��o de update.
						   .T.)              //    [14]  L   Indica se o campo � virtual
	Else
		oStruGrid:AddField("PARAM"    ,;    // [01]  C   Nome do Campo
						   "02"       ,;    // [02]  C   Ordem
                           STR0094    ,;    // [03]  C   Titulo do campo //"Par�metro"
                           STR0094    ,;    // [04]  C   Descricao do campo
                           NIL        ,;    // [05]  A   Array com Help
                           "C"        ,;    // [06]  C   Tipo do campo
                           Nil        ,;    // [07]  C   Picture
                           NIL        ,;    // [08]  B   Bloco de PictTre Var
                           NIL        ,;    // [09]  C   Consulta F3
                           .F.        ,;    // [10]  L   Indica se o campo � alteravel
                           NIL        ,;    // [11]  C   Pasta do campo
                           NIL        ,;    // [12]  C   Agrupamento do campo
                           NIL        ,;    // [13]  A   Lista de valores permitido do campo (Combo)
                           NIL        ,;    // [14]  N   Tamanho maximo da maior op��o do combo
                           NIL        ,;    // [15]  C   Inicializador de Browse
                           .T.        ,;    // [16]  L   Indica se o campo � virtual
                           NIL        ,;    // [17]  C   Picture Variavel
                           NIL)             // [18]  L   Indica pulo de linha ap�s o campo
		oStruGrid:AddField("VALOR"    ,;    // [01]  C   Nome do Campo
						   "03"       ,;    // [02]  C   Ordem
                           STR0095    ,;    // [03]  C   Titulo do campo //"Valor"
                           STR0095    ,;    // [04]  C   Descricao do campo
                           NIL        ,;    // [05]  A   Array com Help
                           "C"        ,;    // [06]  C   Tipo do campo
                           Nil        ,;    // [07]  C   Picture
                           NIL        ,;    // [08]  B   Bloco de PictTre Var
                           NIL        ,;    // [09]  C   Consulta F3
                           .F.        ,;    // [10]  L   Indica se o campo � alteravel
                           NIL        ,;    // [11]  C   Pasta do campo
                           NIL        ,;    // [12]  C   Agrupamento do campo
                           NIL        ,;    // [13]  A   Lista de valores permitido do campo (Combo)
                           NIL        ,;    // [14]  N   Tamanho maximo da maior op��o do combo
                           NIL        ,;    // [15]  C   Inicializador de Browse
                           .T.        ,;    // [16]  L   Indica se o campo � virtual
                           NIL        ,;    // [17]  C   Picture Variavel
                           NIL)             // [18]  L   Indica pulo de linha ap�s o campo
	EndIf

Return oStruGrid
