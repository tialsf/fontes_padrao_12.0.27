#INCLUDE "TOTVS.CH"
#INCLUDE "PCPA145.ch"
#INCLUDE "PCPA145DEF.ch"

/*/{Protheus.doc} PCPA145
Gera��o dos documentos (SC2/SC1/SC7/SD4/SB2) de acordo com o
resultado do processamento do MRP.

@type  Function
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param 01 - cTicket   , Character, Ticket de processamento do MRP para gera��o dos documentos
@param 02 - aParams   , Array    , Array com os par�metros utilizados no processamento do MRP.
@param 03 - lAutomacao, Logico   , Indica se a execucao e proveniente de automacao
@param 04 - cErrorUID , character, codigo identificador do controle de erros multi-thread
@return Nil
/*/
Function PCPA145(cTicket, aParams, lAutomacao, cErrorUID)
	Local cCodUsr   := RetCodUsr()
	Local nStart    := MicroSeconds()
	Local oProcesso

	Default lAutomacao := .F.

	PutGlbValue("UIDPRG_PCPA145","INI")
	oProcesso := ProcessaDocumentos():New(cTicket, /*02*/, aParams, cCodUsr, lAutomacao, cErrorUID)
	oProcesso:processar()
	oProcesso:Destroy()
	FreeObj(oProcesso)

	//Ao finalizar a gera��o dos documentos, atualiza o status da tabela HW3 para Gerado.
	HW3->(dbSetOrder(1))
	If HW3->(dbSeek(xFilial("HW3")+cTicket))
		RecLock("HW3", .F.)
			HW3->HW3_STATUS := "6"
		HW3->(MsUnLock())
	EndIf

	ProcessaDocumentos():msgLog(STR0005 + cValToChar(MicroSeconds()-nStart))  //"TERMINO DO PROCESSAMENTO DA GERACAO DE DOCUMENTOS. TEMPO TOTAL: "
Return Nil

/*/{Protheus.doc} PCPA145MRP
Gera��o dos documentos (SC2/SC1/SC7/SD4/SB2) de acordo com o
resultado do processamento do MRP.

@type  Function
@author renan.roeder
@since 27/03/2020
@version P12.1.30
@param 01 - cTicket, Character, Ticket de processamento do MRP para gera��o dos documentos
@param 02 - aParams, Array    , Array com os par�metros utilizados no processamento do MRP.
@param 03 - cErrorUID , character, codigo identificador do controle de erros multi-thread
@return Nil
/*/
Function PCPA145MRP(cTicket, aParams, cErrorUID)
	Local cCodUsr     := ""
	Local nStart      := MicroSeconds()
	Local oProcesso   := Nil

	PutGlbValue("UIDPRG_PCPA145","INI")
	oProcesso := ProcessaDocumentos():New(cTicket, /*02*/, aParams, cCodUsr, , cErrorUID)
	oProcesso:processar()
	oProcesso:Destroy()

	FreeObj(oProcesso)

	//Ao finalizar a gera��o dos documentos, atualiza o status da tabela HW3 para Gerado.
	HW3->(dbSetOrder(1))
	If HW3->(dbSeek(xFilial("HW3")+cTicket))
		RecLock("HW3", .F.)
			HW3->HW3_STATUS := "6"
		HW3->(MsUnLock())
	EndIf

	ProcessaDocumentos():msgLog(STR0005 + cValToChar(MicroSeconds()-nStart))  //"TERMINO DO PROCESSAMENTO DA GERACAO DE DOCUMENTOS. TEMPO TOTAL: "
Return Nil

/*/{Protheus.doc} ProcessaDocumentos
Classe com as regras para gera��o dos documentos do MRP.

@author lucas.franca
@since 12/11/2019
@version P12.1.27
/*/
CLASS ProcessaDocumentos FROM LongClassName

	DATA cCodUsr      AS Character
	DATA cErrorUID    AS Character
	DATA cIncOP       AS Character
	DATA cIncSC       AS Character
	DATA cTicket      AS Character
	DATA cTipoOP      AS Character
	DATA cUIDGeraAE   AS Character
	DATA cUIDGlobal   AS Character
	DATA cUIDIntOP    AS Character
	DATA cUIDParams   AS Character
	DATA cThrGeraAE   AS Character
	DATA cThrJobs     AS Character
	DATA cThrSaldo    AS Character
	DATA cThrSaldoJob AS Character
	DATA cUIDLockSD4  AS Character
	DATA lCopiado     AS Logic
	DATA lOPAglutina  AS Logic
	DATA lSCAglutina  AS Logic
	DATA lIntegraOP   AS Logic
	DATA lSugEmpenho  AS Logic
	DATA nThrGeraAE   AS Numeric
	DATA nThrJobs     AS Numeric
	DATA nThrSaldo    AS Numeric
	DATA nThrSaldoJob AS Numeric

	DATA nToler1UM AS Numeric
	DATA nToler2UM AS Numeric
	DATA lPConvUm AS Logic

	METHOD New(cTicket, lCopia, aParams, cCodUsr, lAutomacao, cErrorUID) CONSTRUCTOR
	METHOD Destroy()

	METHOD aguardaGeraAE()
	METHOD aguardaNivel()
	METHOD aguardaSaldos()
	METHOD atualizaSaldo(cProduto, cLocal, cNivel, nQtd, nTipo, lPrevisto)
	METHOD atualizaProdutoXNivel(cProduto, cNivel)
	METHOD atualizaDeParaDocumentoProduto(cDocMRP, cDocProt)
	METHOD addEmpenho(aEmpenho, nRecno)
	METHOD criarSecaoGlobal()
	METHOD clearCount(cName)
	METHOD ConvUm(cCod, nQtd1, nQtd2, nUnid)
	METHOD delSaldoProd(cNivel)
	METHOD executaIntOP()
	METHOD existEmpenho(aEmpenho, nRecno)
	METHOD getGeraDocAglutinado(cNivel)
	METHOD getProdutoNivel(cNivel, lRet)
	METHOD getProdPaiDocumento(cDocMRP)
	METHOD getSaldosProduto(cProduto, lRet)
	METHOD getDocumentoDePara(cDocMRP)
	METHOD getDocsAglutinados(cDocMRP, cProduto)
	METHOD getTipoDocumento()
	METHOD getCount(cName)
	METHOD getProgress()
	METHOD getDocUni(cName)
	METHOD incCount(cName)
	METHOD initCount(cName)
	METHOD incDocUni(cName)
	METHOD initDocUni(cName)
	METHOD processar()
	METHOD processaSaldos(cNivel)
	METHOD salvaOPIntegracao()
	METHOD updStatusRastreio(cStatus, cDocGerado, cTipDocERP, cTipoDoc, cDocPai, cTrt, cProduto, cChave, cChaveSub, nSequen)

	Static METHOD msgLog(cMsg)

ENDCLASS

/*/{Protheus.doc} New
M�todo construtor da classe de gera��o de documentos do MRP

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cTicket , Character, Ticket de processamento do MRP para gera��o dos documentos
@param lCopia  , L�gico   , Identifica que est� instanciando este objeto nas threads filhas, apenas para consumir os m�todos.
@param aParams , Array    , Array com os par�metros utilizados no processamento do MRP.
@param cCodUsr , Character, C�digo do usu�rio logado no sistema.
@param lAutomacao, Logico   , Indica se a execucao e proveniente de automacao
@return Self
/*/
METHOD New(cTicket, lCopia, aParams, cCodUsr, lAutomacao, cErrorUID) CLASS ProcessaDocumentos

	Local nPos     := 0

	Default cCodUsr    := ""
	Default lCopia     := .F.
	Default lAutomacao := .F.

	Self:cCodUsr      := cCodUsr
	Self:cErrorUID    := cErrorUID
	Self:cIncOP       := "2"
	Self:cIncSC       := "2"
	Self:cTicket      := cTicket
	Self:cTipoOP      := "1"
	Self:cThrGeraAE   := Self:cTicket + "GERAAE"
	Self:cThrJobs     := Self:cTicket + "PROC"
	Self:cThrSaldo    := Self:cTicket + "STOCK"
	Self:cThrSaldoJob := Self:cTicket + "STOCKJOBS"
	Self:cUIDGeraAE   := Self:cTicket + "PEDCOMP"
	Self:cUIDGlobal   := Self:cThrJobs + "DADOS"
	Self:cUIDIntOP    := Self:cThrJobs + "INTOP"
	Self:cUIDParams   := Self:cThrJobs + "PARAMS"
	Self:cUIDLockSD4  := "LOCK_SD4"
	Self:lCopiado     := lCopia
	Self:lIntegraOP   := .F.
	Self:lOPAglutina  := .F.
	Self:lSCAglutina  := .F.

	If lAutomacao
		Self:nThrGeraAE   := 1
		Self:nThrJobs     := 1
		Self:nThrSaldo    := 1
		Self:nThrSaldoJob := 1
	Else
		Self:nThrGeraAE   := 1
		Self:nThrJobs     := 8
		Self:nThrSaldo    := 2
		Self:nThrSaldoJob := 4
	EndIf

	Self:nToler1UM := Nil
	Self:nToler2UM := Nil
	Self:lPConvUm  := Nil

	If !lCopia

		//Salva o array de par�metros em uma vari�vel global para recuperar nas threads filhas.
		If aParams != Nil
			//Verifica par�metro de integra��o de ordens para adicionar no array aParams
			Self:lIntegraOP := Nil
			Ma650MrpOn(@Self:lIntegraOP, "")
			aAdd(aParams, {0, {"INTEGRAOPONLINE", Self:lIntegraOP, "INTEGRAOPONLINE", Self:lIntegraOP}})

			PutGlbVars(Self:cUIDParams, aParams)
		EndIf

		//Abre as threads que ser�o utilizadas no processamento (JOBS)
		PCPIPCStart(Self:cThrJobs, Self:nThrJobs, 0, cEmpAnt, cFilAnt, cErrorUID) //Inicializa as Threads

		//Abre as threads que ser�o utilizadas no processamento (Estoques)
		PCPIPCStart(Self:cThrSaldo   , Self:nThrSaldo   , 0 , , cErrorUID) //Inicializa as Threads (Sem conex�o com banco. Thread MASTER do processamento de saldos)
		PCPIPCStart(Self:cThrSaldoJob, Self:nThrSaldoJob, 0, cEmpAnt, cFilAnt, cErrorUID) //Inicializa as Threads filhas para o processamento dos saldos.

		//Inicia a Threads que ser� utilizada no processamento da gera��o dos pedidos de compras
		PCPIPCStart(Self:cThrGeraAE, Self:nThrGeraAE, 0, cEmpAnt, cFilAnt, cErrorUID)

		//Ativa a Thread para gera��o dos pedidos de compra
		PCPIPCGO(Self:cThrGeraAE, .F., "PCPA145PC", Self:cTicket, Self:cCodUsr)

		//Cria as tabelas tempor�rias em mem�ria para utiliza��o no processamento.
		Self:criarSecaoGlobal()
	Else
		GetGlbVars(Self:cUIDParams, @aParams)
	EndIf

	If aParams == Nil
		aParams := {}
		Self:msgLog(STR0006)  //"Parametros do MRP nao recebidos. Gerando documentos com parametros default."
	EndIf

	//Recupera o par�metro de aglutina��o de ordens de produ��o
	nPos := aScan(aParams, {|x| AllTrim(x[PARAM_DATA][PARAM_POS_CODE]) == "consolidateProductionOrder"})
	If nPos > 0 .And. ;
	   !Empty(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE]) .And. ;
	   AllTrim(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE]) == "1"
		Self:lOPAglutina := .T.
	EndIf

	//Recupera o par�metro de aglutina��o de solicita��es de compra
	nPos := aScan(aParams, {|x| AllTrim(x[PARAM_DATA][PARAM_POS_CODE]) == "consolidatePurchaseRequest"})
	If nPos > 0 .And. ;
	   !Empty(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE]) .And. ;
	   AllTrim(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE]) == "1"
		Self:lSCAglutina := .T.
	EndIf

	//Recupera o par�metro de incremento da ordem de produ��o
	nPos := aScan(aParams, {|x| AllTrim(x[PARAM_DATA][PARAM_POS_CODE]) == "productionOrderNumber"})
	If nPos > 0
		If Empty(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE])
			Self:cIncOP := "2"
		Else
			Self:cIncOP := Alltrim(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE])
		EndIf
	EndIf

	//Recupera o par�metro de incremento da solicita��o de compra
	nPos := aScan(aParams, {|x| AllTrim(x[PARAM_DATA][PARAM_POS_CODE]) == "purchaseRequestNumber"})
	If nPos > 0
		If Empty(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE])
			Self:cIncSC := "2"
		Else
			Self:cIncSC := Alltrim(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE])
		EndIf
	EndIf

	//Recupera o par�metro de tipo do documento
	nPos := aScan(aParams, {|x| AllTrim(x[PARAM_DATA][PARAM_POS_CODE]) == "productionOrderType"})
	If nPos > 0
		If Empty(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE])
			Self:cTipoOP := "1"
		Else
			Self:cTipoOP := Alltrim(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE])
		EndIf
	EndIf

	//Recupera o par�metro de integra��o de ordens de produ��o
	nPos := aScan(aParams, {|x| AllTrim(x[PARAM_DATA][PARAM_POS_CODE]) == "INTEGRAOPONLINE"})
	If nPos > 0 .And. aParams[nPos][PARAM_DATA][PARAM_POS_VALUE] != Nil
		Self:lIntegraOP := aParams[nPos][PARAM_DATA][PARAM_POS_VALUE]
	EndIf

	//Recupera o par�metro de sugestao de lote e endereco no empenho
	nPos := aScan(aParams, {|x| AllTrim(x[PARAM_DATA][PARAM_POS_CODE]) == "allocationSuggestion"})
	If nPos > 0
		Self:lSugEmpenho := AllTrim(aParams[nPos][PARAM_DATA][PARAM_POS_VALUE]) == "1"
	Else
		Self:lSugEmpenho := .F.
	EndIf

Return Self

/*/{Protheus.doc} Destroy
M�todo destrutor da classe de gera��o de documentos do MRP

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@return Nil
/*/
METHOD Destroy() CLASS ProcessaDocumentos

	//Somente a inst�ncia da thread MASTER deve finalizar as threads e limpar a mem�ria.
	If !Self:lCopiado

		//Finaliza as threads utilizadas no processamento
		PCPIPCFinish(Self:cThrJobs    , 300, Self:nThrJobs)
		PCPIPCFinish(Self:cThrSaldo   , 300, Self:nThrSaldo)
		PCPIPCFinish(Self:cThrSaldoJob, 300, Self:nThrSaldoJob)
		PCPIPCFinish(Self:cThrGeraAE  , 300, Self:nThrGeraAE)

		//Limpa da mem�ria as vari�veis globais
		VarClean(Self:cUIDGeraAE)
		VarClean(Self:cUIDGlobal)
		VarClean(Self:cUIDIntOP)
		VarClean(Self:cUIDLockSD4)
		ClearGlbValue(Self:cUIDParams)
	EndIf

Return Nil

/*/{Protheus.doc} criarSecaoGlobal
Cria a se��o de vari�veis globais que ser� utilizada no processamento

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@return Nil
/*/
METHOD criarSecaoGlobal() CLASS ProcessaDocumentos

	If !VarSetUID(Self:cUIDGeraAE)
		Self:msgLog(STR0007 + Self:cUIDGeraAE)  //"Erro na cria��o da se��o de vari�veis globais."
	EndIf

	If !VarSetUID(Self:cUIDGlobal)
		Self:msgLog(STR0007 + Self:cUIDGlobal)  //"Erro na cria��o da se��o de vari�veis globais."
	EndIf

	If !VarSetUID(Self:cUIDIntOP)
		Self:msgLog(STR0007 + Self:cUIDIntOP)  //"Erro na cria��o da se��o de vari�veis globais."
	EndIf

	If !VarSetUID(Self:cUIDLockSD4)
		Self:msgLog(STR0007 + Self:cUIDLockSD4)  //"Erro na cria��o da se��o de vari�veis globais."
	EndIf

Return Nil

/*/{Protheus.doc} processar
M�todo que delega os registros para serem processados nas threads filhas.

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@return Nil
/*/
METHOD processar() CLASS ProcessaDocumentos
	Local aRegistro   := Array(RASTREIO_TAMANHO)
	Local aRegs       := {}
	Local cChavePrd   := ""
	Local cNivAtu     := ""
	Local cNumOpUni   := ""
	Local cNumScUni   := ""
	Local lGerouPC    := .F.
	Local lProcessar  := .T.
	Local lRet        := .T.
	Local nConcluidos := 0
	Local nDelegados  := 0
	Local nTotal      := 0
	Local nPos        := 1
	Local nTamRegs    := 0
	Local nTamPrd     := GetSX3Cache("B1_COD"   , "X3_TAMANHO")
	Local nTamLoc     := GetSX3Cache("B1_LOCPAD", "X3_TAMANHO")
	Local nTamTrt     := GetSX3Cache("G1_TRT"   , "X3_TAMANHO")
	Local nTempoNiv   := 0
	Local oJson       := Nil
	Local oPCPError   := PCPMultiThreadError():New(Self:cErrorUID, .F.)

	nDelegados  := Self:initCount(Self:cThrJobs + "_Delegados")
	nConcluidos := Self:initCount(Self:cThrJobs + "_Concluidos")
	nDelegados  := Self:initCount(Self:cThrSaldoJob + "_Delegados")
	nConcluidos := Self:initCount(Self:cThrSaldoJob + "_Concluidos")
	nDelegados  := Self:initCount(Self:cThrSaldo + "_Delegados")
	nConcluidos := Self:initCount(Self:cThrSaldo + "_Concluidos")

	Self:initDocUni("C2_NUM")

	While lProcessar
		If oPCPError:possuiErro()
			Exit
		EndIf
		//Aguarda as threads filhas terminarem o processamento
		Self:aguardaNivel()
		Self:aguardaSaldos()

		nPos  := 1
		aRegs := MrpGetHWC(Self:cTicket, .T.)

		If aRegs[1] == .F.
			nTamRegs := 0
		Else
			oJson    := aRegs[2]
			nTamRegs := Len(oJson["items"])
			If nTotal == 0
				If !VarSetX(Self:cUIDGlobal, TOTAL_PENDENTES, nTamRegs)
					Self:msgLog(STR0020) //"Erro ao Gravar o total de registros pendentes em mem�ria."
				EndIf
				Self:initCount(CONTADOR_GERADOS)
			EndIf
		EndIf

		//Controle de registros marcados para reiniciar.
		nTotal := 0
		Self:initCount(CONTADOR_REINICIADOS)

		//Armazena o primeiro n�vel
		If nTamRegs > 0
			cNivAtu    := oJson["items"][1]["level"]
			Self:msgLog(STR0008 + cNivAtu)  //"Processando n�vel: "
			nTempoNiv := MicroSeconds()

			//Pega numera��o da OP que ser� usada para gera��o de todos os registros
			If Self:cIncOP == "1" .And. Empty(cNumOpUni) .And. cNivAtu != "99"
				cNumOpUni := Self:incDocUni("C2_NUM")
			EndIf
			//Pega numera��o da SC que ser� usada para gera��o de todos os registros
			If Self:cIncSC == "1" .And. Empty(cNumScUni)
				cNumScUni := GetNumSC1(.T.)
			EndIf
		Else
			//Se a query n�o retornar registros, significa que todos os registros pendentes foram processados.
			//Ir� sair do loop.
			lProcessar := .F.
		EndIf

		While nPos <= nTamRegs

			nTotal++

			aRegistro[RASTREIO_POS_PRODUTO                 ] := PadR(oJson["items"][nPos]["componentCode"], nTamPrd)
			aRegistro[RASTREIO_POS_OPC_ID                  ] := oJson["items"][nPos]["optionalId"]
			aRegistro[RASTREIO_POS_TRT                     ] := PadR(oJson["items"][nPos]["sequenceInStructure"], nTamTrt)
			aRegistro[RASTREIO_POS_DATA_ENTREGA            ] := getDate(oJson["items"][nPos]["necessityDate"])
			aRegistro[RASTREIO_POS_DATA_INICIO             ] := getDate(oJson["items"][nPos]["startDate"])
			aRegistro[RASTREIO_POS_TIPODOC                 ] := oJson["items"][nPos]["parentDocumentType"]
			aRegistro[RASTREIO_POS_DOCPAI                  ] := oJson["items"][nPos]["parentDocument"]
			aRegistro[RASTREIO_POS_DOCFILHO                ] := oJson["items"][nPos]["childDocument"]
			aRegistro[RASTREIO_POS_NECES_ORIG              ] := oJson["items"][nPos]["originalNecessity"]
			aRegistro[RASTREIO_POS_SALDO_EST               ] := oJson["items"][nPos]["stockBalanceQuantity"]
			aRegistro[RASTREIO_POS_BAIXA_EST               ] := oJson["items"][nPos]["quantityStockWriteOff"]
			aRegistro[RASTREIO_POS_QTD_SUBST               ] := oJson["items"][nPos]["quantitySubstitution"]
			aRegistro[RASTREIO_POS_EMPENHO                 ] := oJson["items"][nPos]["alocationQuantity"]
			aRegistro[RASTREIO_POS_NECESSIDADE             ] := oJson["items"][nPos]["quantityNecessity"]
			aRegistro[RASTREIO_POS_REVISAO                 ] := oJson["items"][nPos]["structureReview"]
			aRegistro[RASTREIO_POS_ROTEIRO                 ] := oJson["items"][nPos]["routing"]
			aRegistro[RASTREIO_POS_OPERACAO                ] := oJson["items"][nPos]["operation"]
			aRegistro[RASTREIO_POS_ROTEIRO_DOCUMENTO_FILHO ] := oJson["items"][nPos]["routingChildDocument"]
			aRegistro[RASTREIO_POS_SEQUEN                  ] := oJson["items"][nPos]["breakupSequence"]
			aRegistro[RASTREIO_POS_NIVEL                   ] := oJson["items"][nPos]["level"]
			aRegistro[RASTREIO_POS_LOCAL                   ] := PadR(oJson["items"][nPos]["consumptionLocation"], nTamLoc)
			aRegistro[RASTREIO_POS_CHAVE                   ] := oJson["items"][nPos]["recordKey"]
			aRegistro[RASTREIO_POS_CHAVE_SUBST             ] := oJson["items"][nPos]["substitutionKey"]
			aRegistro[RASTREIO_POS_CONTRATO                ] := oJson["items"][nPos]["purchaseContract"]

			//Se for n�vel 99 e configurado para gerar Pedido de Compra, ser� feito em uma thread separada.
			//A execu��o dessa thread ser� feita na fun��o PCPA145PC
			If aRegistro[RASTREIO_POS_NIVEL] == "99" .And. aRegistro[RASTREIO_POS_CONTRATO] == "1"
				cChavePrd := STRZERO(nTotal, 6, 0)

				//Seta vari�vel indicando que a gera��o do PV ser� executada
				lGerouPC := VarSetA(Self:cUIDGeraAE, cChavePrd, @aRegistro)
			EndIf

			nDelegados := Self:incCount(Self:cThrJobs + "_Delegados")
			//Delega o registro para processamento em thread filha
			PCPIPCGO(Self:cThrJobs, .F., "PCPA145JOB", Self:cTicket, aRegistro, Self:cCodUsr, cNumScUni, lGerouPC)

			//Reseta valor da vari�vel
			lGerouPC := .F.

			nPos++

			//Verifica se mudou de n�vel ou se passou por todos os registros.
			If nPos > nTamRegs .Or. cNivAtu != oJson["items"][nPos]["level"]
				//Aguarda as threads filhas terminarem o processamento
				Self:aguardaNivel()

				//Aguarda t�rmino da gera��o das autoriza��es de entrega
				If cNivAtu == "99"
					Self:aguardaGeraAE()
				EndIf

				//Dispara a atualiza��o dos saldos
				Self:processaSaldos(cNivAtu)

				If nPos <= nTamRegs
					Self:msgLog(STR0009 + cNivAtu + ": " + cValToChar(MicroSeconds()-nTempoNiv))  //"Tempo do n�vel "
					//Se existirem mais n�veis para processar, atualiza a vari�vel de controle
					cNivAtu := oJson["items"][nPos]["level"]
					Self:msgLog(STR0008 + cNivAtu)  //"Processando n�vel: "
					nTempoNiv := MicroSeconds()
				Else
					Self:msgLog(STR0009 + cNivAtu + ": " + cValToChar(MicroSeconds()-nTempoNiv))  //"Tempo do n�vel "
					aSize(oJson["items"], 0)
					FreeObj(oJson)
					Exit
				EndIf
			EndIf
		End

		If lProcessar
			//Aguarda as threads de atualiza��o de estoque finalizarem.
			Self:aguardaSaldos()

			If nTotal == Self:getCount(CONTADOR_REINICIADOS)
				lProcessar := .F.
				Self:msgLog(STR0010)  //"TODOS OS REGISTROS PROCESSADOS FORAM MARCADOS PARA REPROCESSAR. PROCESSAMENTO ENCERRADO PARA N�O ENTRAR EM LOOP."
			EndIf
		EndIf
		aSize(aRegs, 0)
	End

	//Envia comando para encerrar thread de Autoriza��o de entrega
	lRet := VarSetA(Self:cUIDGeraAE, "EndPurchaseOrder", {})

	PutGlbValue("UIDPRG_PCPA145","END")

	//Dispara Sugestao de Lotes e Enderecos
	If !oPCPError:possuiErro() .AND. Self:lSugEmpenho
		PutGlbValue("PCPA151_STATUS","INI")
		oPCPError:startJob("PCPA151T", getEnvServer(), .F., cEmpAnt, cFilAnt, Self:cTicket, oPCPError:cErrorUID)
		While GetGlbValue("PCPA151_STATUS") != "END" .AND. !oPCPError:possuiErro()
			Sleep(1000)
		End
	EndIf

	//Dispara a integra��o das ordens de produ��o.
	//Este processo � executado de forma paralela, e n�o impede a finaliza��o
	//da gera��o dos documentos;
	Self:executaIntOP()

	aSize(aRegistro, 0)
	Self:clearCount(CONTADOR_REINICIADOS)
	Self:clearCount(CONTADOR_GERADOS)
Return

/*/{Protheus.doc} aguardaGeraAE
Aguarda o t�rmino do processamento das autoriza��es de entrega.

@type  Method
@author ricardo.prandi
@since 03/03/2020
@version P12.1.30
@return Nil
/*/
METHOD aguardaGeraAE() CLASS ProcessaDocumentos
	Local aDados     := {}
	Local oPCPError  := PCPMultiThreadError():New(Self:cErrorUID, .F.)

	While .T.
		If VarGetAA(Self:cUIDGeraAE, @aDados)
			If Len(aDados) == 0
				Exit
			EndIf
		Else
			Exit
		EndIf
		If oPCPError:possuiErro()
			Exit
		EndIf
		Sleep(50)
	EndDo
Return

/*/{Protheus.doc} aguardaNivel
Aguarda o t�rmino do processamento do n�vel atual.

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@return Nil
/*/
METHOD aguardaNivel() CLASS ProcessaDocumentos
	Local nDelegados  := 0
	Local nConcluidos := 0
	Local oPCPError   := PCPMultiThreadError():New(Self:cErrorUID, .F.)

	nDelegados  := Self:getCount(Self:cThrJobs + "_Delegados")
	nConcluidos := Self:getCount(Self:cThrJobs + "_Concluidos")

	While nDelegados > nConcluidos
		Sleep(150)
		nDelegados  := Self:getCount(Self:cThrJobs + "_Delegados")
		nConcluidos := Self:getCount(Self:cThrJobs + "_Concluidos")
		If oPCPError:possuiErro()
			Exit
		EndIf
	EndDo
Return

/*/{Protheus.doc} aguardaSaldos
Aguarda o t�rmino do processamento das atualiza��es de saldo

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@return Nil
/*/
METHOD aguardaSaldos() CLASS ProcessaDocumentos
	Local nDelegados  := 0
	Local nConcluidos := 0
	Local oPCPError   := PCPMultiThreadError():New(Self:cErrorUID, .F.)

	//Aguarda o fim das threads de processamento de saldos.
	nDelegados  := Self:getCount(Self:cThrSaldoJob + "_Delegados")
	nConcluidos := Self:getCount(Self:cThrSaldoJob + "_Concluidos")

	While nDelegados > nConcluidos
		Sleep(150)
		nDelegados  := Self:getCount(Self:cThrSaldoJob + "_Delegados")
		nConcluidos := Self:getCount(Self:cThrSaldoJob + "_Concluidos")
		If oPCPError:possuiErro()
			Exit
		EndIf
	EndDo

	//Aguarda o fim das threads de delega��o de saldos.
	nDelegados  := Self:getCount(Self:cThrSaldo + "_Delegados")
	nConcluidos := Self:getCount(Self:cThrSaldo + "_Concluidos")
	While nDelegados > nConcluidos
		Sleep(150)
		nDelegados  := Self:getCount(Self:cThrSaldo + "_Delegados")
		nConcluidos := Self:getCount(Self:cThrSaldo + "_Concluidos")
		If oPCPError:possuiErro()
			Exit
		EndIf
	EndDo
Return

/*/{Protheus.doc} processaSaldos
Dispara o processo de atualiza��o de saldos em estoque.
Este processo � executado de forma paralela ao processo principal.

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cNivel, Character, N�vel dos produtos que ser�o atualizados.
@return Nil
/*/
METHOD processaSaldos(cNivel) CLASS ProcessaDocumentos
	Local nDelegados := 0

	nDelegados := Self:incCount(Self:cThrSaldo + "_Delegados")
	PCPIPCGO(Self:cThrSaldo, .F., "PCPA145SLD", Self:cTicket, cNivel)
Return

/*/{Protheus.doc} atualizaSaldo
Atualiza a tabela de mem�ria para controle dos saldos, adicionando a quantidade recebida em nQtd.
Ser� atualizada a quantidade da tabela de mem�ria de acordo com o par�metro nTipo.

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cProduto, Character, C�digo do produto que ser� atualizado.
@param cLocal  , Character, C�digo do local de estoque.
@param cNivel  , Character, N�vel do produto.
@param nQtd    , Numeric  , Quantidade que ser� adicionada.
@param nTipo   , Numeric  , Identifica qual � a quantidade que ser� atualizada.
                            1 - Entrada (B2_SALPEDI/B2_SALPPRE);
                            2 - Sa�da   (B2_QEMP/B2_QEMPPRE)
@param lPrevisto, Logic   , Identifica se o saldo � de documentos Previstos ou n�o.
@return Nil
/*/
METHOD atualizaSaldo(cProduto, cLocal, cNivel, nQtd, nTipo, lPrevisto) CLASS ProcessaDocumentos
	Local aDados    := {}
	Local cChavePrd := AllTrim(cProduto) + CHR(13) + "SLD"
	Local lRet      := .T.
	Local lCriar    := .F.
	Local nPos      := 0
	Local nIndex    := 0
	Local nTotal    := 0

	//Inicia transa��o
	VarBeginT(Self:cUIDGlobal, cChavePrd)

	//Recupera os estoques deste produto
	aDados := Self:getSaldosProduto(cProduto, @lRet)

	//Se este produto ainda n�o existir, ir� criar a tabela auxiliar de produtos do n�vel.
	If !lRet
		Self:atualizaProdutoXNivel(cProduto, cNivel)
		//Adiciona no array de dados as informa��es deste produto.
		aDados := Array(1)
		nPos   := 1
		lCriar := .T.
	Else
		//Produto j� existe na tabela de saldos.
		//Verifica se o cLocal j� existe.
		nPos := aScan(aDados, {|x| x[SALDOS_POS_LOCAL] == cLocal})
		If nPos == 0
			//Local ainda n�o existe, ir� criar novo elemento no array
			aAdd(aDados, {})
			nPos   := Len(aDados)
			lCriar := .T.
		EndIf
	EndIf

	If lCriar
		//Este produto ou local ainda n�o existem no array de saldos.
		//Ir� criar com as quantidades zeradas.
		aDados[nPos] := Array(SALDOS_TAMANHO)
		aDados[nPos][SALDOS_POS_LOCAL] := cLocal
		aDados[nPos][SALDOS_POS_QTD  ] := Array(SALDOS_QTD_TAMANHO)

		aDados[nPos][SALDOS_POS_QTD][SALDOS_QTD_POS_ENTRADA_FIRME] := 0
		aDados[nPos][SALDOS_POS_QTD][SALDOS_QTD_POS_ENTRADA_PREV ] := 0
		aDados[nPos][SALDOS_POS_QTD][SALDOS_QTD_POS_SAIDA_FIRME  ] := 0
		aDados[nPos][SALDOS_POS_QTD][SALDOS_QTD_POS_SAIDA_PREV   ] := 0
	EndIf

	If nTipo == 1
		If lPrevisto
			aDados[nPos][SALDOS_POS_QTD][SALDOS_QTD_POS_ENTRADA_PREV ] += nQtd
		Else
			aDados[nPos][SALDOS_POS_QTD][SALDOS_QTD_POS_ENTRADA_FIRME] += nQtd
		EndIf
	ElseIf nTipo == 2
		If lPrevisto
			aDados[nPos][SALDOS_POS_QTD][SALDOS_QTD_POS_SAIDA_PREV   ] += nQtd
		Else
			aDados[nPos][SALDOS_POS_QTD][SALDOS_QTD_POS_SAIDA_FIRME  ] += nQtd
		EndIf
	EndIf

	//Seta os dados na tabela de mem�ria
	lRet := VarSetAD(Self:cUIDGlobal, cChavePrd, @aDados )
	//Finaliza a transa��o
	VarEndT(Self:cUIDGlobal, cChavePrd)

	If !lRet
		Self:msgLog(STR0011 + AllTrim(cProduto) + STR0012 + AllTrim(cLocal) + "'.")  //"Erro ao atualizar a tabela de saldos do produto. Produto: '"  //"'. Local:'"
	EndIf

	//Limpa a mem�ria do array de saldos
	nTotal := Len(aDados)
	For nIndex := 1 To nTotal
		aSize(aDados[nIndex][SALDOS_POS_QTD], 0)
		aSize(aDados[nIndex]                , 0)
	Next nIndex
	aSize(aDados, 0)

Return

/*/{Protheus.doc} atualizaProdutoXNivel
Atualiza a tabela de mem�ria para controle de produtos X n�vel.

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cProduto, Character, C�digo do produto que ser� atualizado.
@param cNivel  , Character, N�vel do produto.
@return Nil
/*/
METHOD atualizaProdutoXNivel(cProduto, cNivel) CLASS ProcessaDocumentos
	Local aDados    := {}
	Local cChaveNiv := AllTrim(cNivel) + CHR(13) + "NIV"
	Local lRet      := .T.

	//Inicia transa��o
	VarBeginT(Self:cUIDGlobal, cChaveNiv+"LOCK")

	//Recupera a tabela de produtos X n�veis.
	aDados := Self:getProdutoNivel(cNivel, @lRet)

	//Se n�o existir nenhum produto neste n�vel, ir� criar o array de produtos com o produto atual.
	If !lRet
		aDados := {cProduto}
	Else
		//J� existem produtos neste n�vel, apenas adiciona o produto atual.
		aAdd(aDados, cProduto)
	EndIf

	//Seta os dados na tabela de mem�ria
	lRet := VarSetAD(Self:cUIDGlobal, cChaveNiv, @aDados )
	If !lRet
		Self:msgLog(STR0013 + AllTrim(cProduto) + STR0014 + AllTrim(cNivel) + "'.")  //"Erro ao atualizar a tabela auxiliar de n�veis x produtos. Produto: '"  //"'. N�vel:'"
	EndIf

	//Finaliza a transa��o
	VarEndT(Self:cUIDGlobal, cChaveNiv+"LOCK")

	aSize(aDados, 0)

Return

/*/{Protheus.doc} getProdutoNivel
Recupera os produtos que pertencem ao n�vel e que possuem pend�ncias de atualiza��o de estoques.

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cNivel  , Character, N�vel do produto.
@param lRet    , Logico   , Retorna por refer�ncia se houve erro ao recuperar os dados.
@return aProdutos, Array  , Array com os c�digos dos produtos do n�vel
/*/
METHOD getProdutoNivel(cNivel, lRet) CLASS ProcessaDocumentos
	Local aProdutos := {}
	Local cChaveNiv := AllTrim(cNivel) + CHR(13) + "NIV"

	//Recupera a tabela de produtos X n�veis.
	lRet := VarGetAD(Self:cUIDGlobal, cChaveNiv, @aProdutos )
Return aProdutos

/*/{Protheus.doc} getSaldosProduto
Recupera os saldos de determinado produto

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cProduto, Character, C�digo do produto.
@param lRet    , Logico   , Retorna por refer�ncia se houve erro ao recuperar os dados.
@return aSaldos, Array  , Array com os saldos dos produtos. Os elementos deste array s�o acessados
                          com a utiliza��o das constantes definidas no arquivo PCPA145DEF.CH.
                          A estrutura deste array �:
                          aSaldos[nPos] - Subarray, com o tamanho definido pela constante SALDOS_TAMANHO
                          aSaldos[nPos][SALDOS_POS_LOCAL] - C�digo do local de estoque deste saldo
                          aSaldos[nPos][SALDOS_POS_QTD  ] - Subarray, com o tamanho definido pela constante SALDOS_QTD_TAMANHO
                          aSaldos[nPos][SALDOS_POS_QTD  ][SALDOS_QTD_POS_ENTRADA_FIRME] - Quantidade de entradas firmes para atualiza��o
                          aSaldos[nPos][SALDOS_POS_QTD  ][SALDOS_QTD_POS_ENTRADA_PREV ] - Quantidade de entradas previstas para atualiza��o
                          aSaldos[nPos][SALDOS_POS_QTD  ][SALDOS_QTD_POS_SAIDA_FIRME  ] - Quantidade de sa�das firmes para atualiza��o
                          aSaldos[nPos][SALDOS_POS_QTD  ][SALDOS_QTD_POS_SAIDA_PREV   ] - Quantidade de sa�das previstas para atualiza��o
/*/
METHOD getSaldosProduto(cProduto, lRet) CLASS ProcessaDocumentos
	Local aSaldos   := {}
	Local cChavePrd := AllTrim(cProduto) + CHR(13) + "SLD"

	//Recupera a tabela de produtos X n�veis.
	lRet := VarGetAD(Self:cUIDGlobal, cChavePrd, @aSaldos )
Return aSaldos

/*/{Protheus.doc} delSaldoProd
Deleta da tabela de mem�ria os registros de saldo do produto
Tamb�m elimina o produto da tabela de controle de Produtos x N�veis

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cNivel  , Character, N�vel do produto.
@return Nil
/*/
METHOD delSaldoProd(cNivel) CLASS ProcessaDocumentos
	Local aProdutos := {}
	Local cChavePrd := ""
	Local cChaveNiv := AllTrim(cNivel) + CHR(13) + "NIV"
	Local lRet      := .T.
	Local nTamanho  := 0
	Local nIndex    := 0

	//Inicia transa��o
	VarBeginT(Self:cUIDGlobal, cChaveNiv+"LOCK")

	//Limpa a tabela de saldos dos produtos pertencentes ao n�vel atual
	aProdutos := Self:getProdutoNivel(cNivel, @lRet)
	If lRet
		nTamanho := Len(aProdutos)
		For nIndex := 1 To nTamanho
			cChavePrd := AllTrim(aProdutos[nIndex]) + CHR(13) + "SLD"
			lRet := VarDel(Self:cUIDGlobal, cChavePrd)
			If !lRet
				Self:msgLog(STR0015 + AllTrim(cProduto) + STR0014 + AllTrim(cNivel)+"'.")  //"Erro ao eliminar registro de saldo da mem�ria. Produto: '"  //"'. N�vel: '"
			EndIf
		Next nIndex
	EndIf

	//Limpa a tabela de produtos x n�vel do n�vel atual
	lRet := VarDel(Self:cUIDGlobal, cChaveNiv)
	If !lRet
		Self:msgLog(STR0016 + AllTrim(cNivel) + "'.")  //"Erro ao eliminar registro de produtos x n�vel da mem�ria. N�vel: '"
	EndIf

	//Finaliza a transa��o
	VarEndT(Self:cUIDGlobal, cChaveNiv+"LOCK")

	aSize(aProdutos, 0)
Return

/*/{Protheus.doc} atualizaDeParaDocumentoProduto
Faz a atualiza��o na tabela de DE-PARA em mem�ria.
Armazena o n�mero do documento do MRP, e vincula com o n�mero do documento
gerado no Protheus e o Produto desse documento.

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cDocMRP , Character, Documento gerado pelo MRP
@param aDocProt, Array    , �ndice 1 contendo o documento do Protheus e �ndice dois contendo o produto
@return lRet   , L�gico   , Identifica se consegiu atualizar o arquivo DE-PARA
/*/
METHOD atualizaDeParaDocumentoProduto(cDocMRP, aDocProt) CLASS ProcessaDocumentos
	Local cChaveDoc := AllTrim(cDocMRP) + CHR(13) + "DOCUM"

	lRet := VarSetA(Self:cUIDGlobal, cChaveDoc, aDocProt)
Return lRet

/*/{Protheus.doc} getDocumentoDePara
Consulta um documento do MRP na tabela DE-PARA de documentos,
e retorna o documento do Protheus que foi gerado.
Se n�o encontrar o documento na tabela DE-PARA, ir� retornar Nil.

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cDocMRP  , Character, Documento gerado pelo MRP
@return cDocProt, Character, Documento gerado pelo Protheus
/*/
METHOD getDocumentoDePara(cDocMRP) CLASS ProcessaDocumentos
	Local aDocProd  := {}
	Local cChaveDoc := AllTrim(cDocMRP) + CHR(13) + "DOCUM"
	Local cDocProt  := ""

	lRet := VarGetAD(Self:cUIDGlobal, cChaveDoc, @aDocProd)
	If !lRet .Or. Empty(aDocProd)
		cDocProt := Nil
	Else
		cDocProt := aDocProd[1]
	EndIf
Return cDocProt

/*/{Protheus.doc} getDocsAglutinados
Consulta um documento do MRP na tabela de documentos aglutinados.
Retorna os documentos do Protheus que foram gerados.
Se n�o encontrar algum dos documentos na tabela DE-PARA, ir� retornar um array sem nenhum elemento.

@type  Method
@author lucas.franca
@since 09/12/2019
@version P12.1.27
@param cDocMRP  , Character, Documento gerado pelo MRP
@param cProduto , Character, C�digo do produto
@return aDocs, Array, Lista dos documentos gerados pelo Protheus
                      Estrutura do array:
                      aDocs[nIndex][1] - Documento gerado no ERP Protheus
                      aDocs[nIndex][2] - Quantidade de empenho necess�rio para o documento
/*/
METHOD getDocsAglutinados(cDocMRP, cProduto) CLASS ProcessaDocumentos
	Local aDocs    := {}
	Local cDocPai  := ""
	Local nPos     := 1
	Local nTamRegs := 0
	Local aRegs    := {}
	Local oJson      := JsonObject():New()

	aRegs := MrpGetHWG(Self:cTicket, cDocMRP, cProduto)
	If aRegs[1]
		oJson:FromJson(aRegs[2])
		nTamRegs := Len(oJson["items"])

		While nPos <= nTamRegs

			cDocPai := Self:getDocumentoDePara((oJson["items"][nPos]["childDocument"]))
			If !Empty(cDocPai)
				aAdd(aDocs, {cDocPai, oJson["items"][nPos]["allocation"]})
			Else
				//Um dos documentos pais deste produto aglutinado ainda n�o foi processado.
				//N�o retorna nenhum documento
				aSize(aDocs, 0)
				Exit
			EndIf
			nPos++
		End
		aSize(oJson["items"],0)
	EndIf
	FreeObj(oJson)

Return aDocs

/*/{Protheus.doc} getProdPaiDocumento
Consulta um documento do MRP na tabela DE-PARA de documentos,
e retorna qual o produto est� relacionado a esse documento.
Se n�o encontrar o documento na tabela DE-PARA, ir� retornar Nil.

@type  Method
@author ricardo.prandi
@since 27/11/2019
@version P12.1.27
@param cDocMRP  , Character, Documento gerado pelo MRP
@return cProdDoc, Character, Produto relacionado ao documento
/*/
METHOD getProdPaiDocumento(cDocMRP) CLASS ProcessaDocumentos
	Local aDocProd  := {}
	Local cChaveDoc := AllTrim(cDocMRP) + CHR(13) + "DOCUM"
	Local cProdDoc  := ""

	lRet := VarGetAD(Self:cUIDGlobal, cChaveDoc, @aDocProd)
	If !lRet .Or. Empty(aDocProd)
		cProdDoc := Nil
	Else
		cProdDoc := aDocProd[2]
	EndIf
Return cProdDoc

/*/{Protheus.doc} updStatusRastreio
Faz a atualiza��o do status de um registro de rastreio.

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cStatus   , Character, Novo status do registro.
@param cDocGerado, Character, N�mero do documento gerado pelo ERP.
@param cTipDocERP, Character, Tipo do documento gerado pelo ERP (1-OP/2-SC).
@param cTipoDoc  , Character, Tipo do documento PAI. Utilizado para buscar o registro da tabela HWC.
@param cDocPai   , Character, Numera��o do documento pai deste registro
@param cTrt      , Character, Sequ�ncia de estrutura
@param cProduto  , Character, C�digo do produto
@param cChave    , Character, Chave do registro de rastreio
@param cChaveSub , Character, Chave de substitui��o do registro
@param nSequen   , Numeric  , Sequ�ncia do registro
@return Nil
/*/
METHOD updStatusRastreio(cStatus, cDocGerado, cTipDocERP, cTipoDoc, cDocPai, cTrt, cProduto, cChave, cChaveSub, nSequen) CLASS ProcessaDocumentos

	Local aResult := {}

	aResult := MrpPostRas( Self:cTicket, cStatus, cDocGerado, cTipDocERP, cTipoDoc, cDocPai, cTrt, cProduto, cChave, cChaveSub, nSequen )
	If aResult[1] == 400
		Self:msgLog(aResult[2])
	EndIf

Return

/*/{Protheus.doc} incCount
Faz o incremento de um contador identificado por cName

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cName, Character, Nome do contador a ser incrementado.
@return nCount, Numeric, Valor atual do contador.
/*/
METHOD incCount(cName) CLASS ProcessaDocumentos
	Local nCount := 0

	If !VarSetX(Self:cUIDGlobal, cName, @nCount, 1, 1)
		Self:msgLog(STR0017 + cName)  //"Erro ao incrementar o contador "
	EndIf
Return nCount

/*/{Protheus.doc} initCount
Inicializa um contador identificado por cName para 0

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cName, Character, Nome do contador a ser incrementado.
@return Nil
/*/
METHOD initCount(cName) CLASS ProcessaDocumentos
	If !VarSetX(Self:cUIDGlobal, cName, 0)
		Self:msgLog(STR0017 + cName)  //"Erro ao incrementar o contador "
	EndIf
Return

/*/{Protheus.doc} getCount
Recupera o valor de um contador identificado por cName

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cName, Character, Nome do contador a ser incrementado.
@return nCount, Numeric, Valor atual do contador.
/*/
METHOD getCount(cName) CLASS ProcessaDocumentos
	Local lRet   := .T.
	Local nCount := 0

	lRet := VarGetXD(Self:cUIDGlobal, cName, @nCount)
	If !lRet
		nCount := 0
	EndIf
Return nCount

/*/{Protheus.doc} clearCount
Limpa da mem�ria um contador identificado por cName

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cName, Character, Nome do contador a ser incrementado.
@return Nil
/*/
METHOD clearCount(cName) CLASS ProcessaDocumentos
	If !VarDel(Self:cUIDGlobal, cName)
		Self:msgLog(STR0018 + cName)  //"Erro ao eliminar da mem�ria o contador "
	EndIf
Return

/*/{Protheus.doc} getGeraDocAglutinado
Identifica se a gera��o de documentos est� parametrizada
para algutinar ou n�o os documentos.

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cNivel, Character, N�vel do produto em processamento
@return lAglutina, L�gico, Identifica se o documento deve ser aglutinado.
/*/
METHOD getGeraDocAglutinado(cNivel) CLASS ProcessaDocumentos
	Local lAglutina := .F.

	If cNivel == "99"
		lAglutina := Self:lSCAglutina
	Else
		lAglutina := Self:lOPAglutina
	EndIf
Return lAglutina

/*/{Protheus.doc} getTipoDocumento
Retorna o Tipo do Documento

@type  Method
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@return cTipoDoc, Character, tipo do documento
/*/
METHOD getTipoDocumento() CLASS ProcessaDocumentos
    Local cTipoDoc := IIF(Self:cTipoOP == "1","P","F")

Return cTipoDoc

/*/{Protheus.doc} ConvUm
Faz a convers�o das quantidades de primeira e segunda unidade de medida.
 - C�pia da fun��o ConvUm. C�pia realizada por quest�o de performance.

@type  Method
@author lucas.franca
@since 27/12/2019
@version P12.1.28
@param cCod , Character, C�digo do produto
@param nQtd1, Numeric  , Quantidade da 1� unidade de medida
@param nQtd2, Numeric  , Quantidade da 2� unidade de medida
@param nUnid, Numeric  , Unidade de medida (1� ou 2�)
@return nBack, Numeric , Quantidade convertida de acordo com a unidade de medida.
/*/
METHOD ConvUm(cCod, nQtd1, nQtd2, nUnid) CLASS ProcessaDocumentos
	Local nBack := 0
	Local nValPe:=0

	Self:nToler1UM := Iif(Self:nToler1UM == Nil, QtdComp(GetMV("MV_NTOL1UM")), Self:nToler1UM)
	Self:nToler2UM := Iif(Self:nToler2UM == Nil, QtdComp(GetMV("MV_NTOL2UM")), Self:nToler2UM)
	Self:lPConvUm  := Iif(Self:lPConvUm  == Nil, ExistBlock("CONVUM")        , Self:lPConvUm )

	nBack := If( (nUnid == 1), nQtd1, nQtd2 )

	//Somente posiciona na SB1 se n�o estiver posicionado no produto correto
	If SB1->B1_COD != cCod
		SB1->(dbSetOrder(1))
		SB1->(dbSeek(xFilial("SB1")+cCod))
	EndIf

	If (SB1->B1_CONV != 0)
		If ( SB1->B1_TIPCONV != "D" )
			If ( nUnid == 1 )
				nBack := (nQtd2 / SB1->B1_CONV)
				If Self:nToler1UM > QtdComp(0) .And. ABS(QtdComp(nBack-nQtd1)) <= Self:nToler1UM
					nBack:=nQtd1
				EndIf
			Else
				nBack := (nQtd1 * SB1->B1_CONV)
				If Self:nToler2UM > QtdComp(0) .And. ABS(QtdComp(nBack-nQtd2)) <= Self:nToler2UM
					nBack:=nQtd2
				EndIf
			EndIf
		Else
			If ( nUnid == 1 )
				nBack := (nQtd2 * SB1->B1_CONV)
				If Self:nToler1UM > QtdComp(0) .And. ABS(QtdComp(nBack-nQtd1)) <= Self:nToler1UM
					nBack:=nQtd1
				EndIf
			Else
				nBack := (nQtd1 / SB1->B1_CONV)
				If Self:nToler2UM > QtdComp(0) .And. ABS(QtdComp(nBack-nQtd2)) <= Self:nToler2UM
					nBack:=nQtd2
				EndIf
			EndIf
		EndIf
	EndIf

	// Ponto de Entrada para calcular qtd nas unidades de medida
	If Self:lPConvUm
		nValPe:=ExecBlock("CONVUM",.F.,.F.,{nQtd1,nQtd2,nUnid,nBack})
		If ValType(nValPe) == "N"
			nBack:=nValPe
		EndIf
	EndIf

Return nBack

/*/{Protheus.doc} salvaOPIntegracao
Salva em mem�ria os dados da OP posicionada na tabela SC2 para
integrar ao MRP posteriormente.

@type  Method
@author lucas.franca
@since 27/12/2019
@version P12.1.28
@return Nil
/*/
Method salvaOPIntegracao() CLASS ProcessaDocumentos
	Local aDados := {}

	//Gera os dados da OP posicionada na SC2
	A650AddInt(@aDados)

	//Salva na se��o global de ordens a integrar.
	If !VarSetAD(Self:cUIDIntOP, Str(SC2->(Recno())), @aDados)
		Self:msgLog(STR0019 + Str(SC2->(Recno())))  //"N�o conseguiu adicionar op para integrar com o MRP. "
	EndIf
	aSize(aDados, 0)
Return

/*/{Protheus.doc} executaIntOP
Executa a integra��o das ordens de produ��o
que foram criadas durante o processamento da
gera��o de documentos

@type  Method
@author lucas.franca
@since 27/12/2019
@version P12.1.28
@return Nil
/*/
METHOD executaIntOP() CLASS ProcessaDocumentos
	Local aDados    := {}
	Local oPCPError := PCPMultiThreadError():New(Self:cErrorUID, .F.)
	//Recupera todas as chaves de OP que foram adicionadas na se��o global cUIDIntOP
	If VarGetAA(Self:cUIDIntOP, @aDados)
		If aDados != Nil .And. Len(aDados) > 0
			//Se existir ordens a integrar, abre nova thread para executar a integra��o
			//de forma paralela.
			oPCPError:startJob("PCPA145INT", getEnvServer(), .F., cEmpAnt, cFilAnt, aDados)
			aSize(aDados, 0)
		EndIf
	EndIf
Return

/*/{Protheus.doc} addEmpenho
Adiciona chave do registro do empenho em mem�ria global
para identificar os empenhos j� criados.

@type  Method
@author lucas.franca
@since 27/12/2019
@version P12.1.28
@param aEmpenho, Array  , Array com os dados do empenho que foi criado
                          Dados do array acessados pelas constantes iniciadas em "EMPENHO_POS"
@param nRecno  , Numeric, RECNO do registro que foi criado na tabela SD4
@return Nil
/*/
METHOD addEmpenho(aEmpenho, nRecno) CLASS ProcessaDocumentos
	Local cChaveEmp := aEmpenho[EMPENHO_POS_PRODUTO] +;
	                   aEmpenho[EMPENHO_POS_ORDEM_PRODUCAO]+;
	                   aEmpenho[EMPENHO_POS_TRT]+;
	                   aEmpenho[EMPENHO_POS_LOCAL]+;
	                   aEmpenho[EMPENHO_POS_OP_ORIGEM]

	//Apenas adiciona a chave do empenho com o recno correspondente.
	VarSetX(Self:cUIDGlobal, "EMP"+cChaveEmp, nRecno)
Return

/*/{Protheus.doc} existEmpenho
Verifica se determinado empenho j� foi criado, e retorna o RECNO correspondente.

@type  Method
@author lucas.franca
@since 27/12/2019
@version P12.1.28
@param aEmpenho, Array  , Array com os dados do empenho que ser� verificado.
                          Dados do array acessados pelas constantes iniciadas em "EMPENHO_POS"
@param nRecno  , Numeric, Retorna por refer�ncia o RECNO do registro do empenho, caso exista.
@return lRet   , Logic  , Identifica se o empenho j� foi criado ou n�o.
/*/
METHOD existEmpenho(aEmpenho, nRecno) CLASS ProcessaDocumentos
	Local cChaveEmp := aEmpenho[EMPENHO_POS_PRODUTO] +;
	                   aEmpenho[EMPENHO_POS_ORDEM_PRODUCAO]+;
	                   aEmpenho[EMPENHO_POS_TRT]+;
	                   aEmpenho[EMPENHO_POS_LOCAL]+;
	                   aEmpenho[EMPENHO_POS_OP_ORIGEM]
	Local lRet := .F.

	lRet := VarGetXD(Self:cUIDGlobal, "EMP"+cChaveEmp, @nRecno)
	If !lRet
		nRecno := 0
	EndIf
Return lRet

/*/{Protheus.doc} msgLog
Faz o print de uma mensagem de log no console.

@type Method
@author lucas.franca
@since 13/12/2019
@version P12.1.28
@param cMsg, Character, Mensagem que ser� adicionada no log
@return Nil
/*/
METHOD msgLog(cMsg) CLASS ProcessaDocumentos
	LogMsg('PCPA145', 14, 4, 1, '', '', cMsg)
Return Nil

/*/{Protheus.doc} getProgress
Retorna a porcentagem do total de documentos a serem gerados.

@type  Method
@author renan.roeder
@since 27/03/2020
@version P12.1.30
@return nProgress, Numeric, N�mero relacionado a porcentagem da execu��o.
/*/
METHOD getProgress() CLASS ProcessaDocumentos
	Local nCount    := 0
	Local nTotal    := 0
	Local nProgress := 0

	lRet := VarGetXD(Self:cUIDGlobal, TOTAL_PENDENTES, @nTotal)
	If lRet
		lRet := VarGetXD(Self:cUIDGlobal, CONTADOR_GERADOS, @nCount)
		If lRet
			nProgress := Round( (nCount/nTotal) * 100, 2)
		EndIf
	EndIf
	If !lRet
		If GetGlbValue("UIDPRG_PCPA145") != "END"
			nProgress := 0
		Else
			nProgress := 100
		EndIf
	EndIF
Return nProgress


/*/{Protheus.doc} incDocUni
Grava o pr�ximo n�mero do documento.

@type  Method
@author renan.roeder
@since 27/07/2020
@version P12.1.31
@param cName, Character, Nome do documento a ser incrementado.
@return cNumDoc, Numeric, Valor atual do n�mero do documento.
/*/
METHOD incDocUni(cName) CLASS ProcessaDocumentos
	Local cNumDoc := ""

	VarBeginT(Self:cUIDGlobal, cName )
	cNumDoc := IIF(cName == "C2_NUM",GetNumSC2(.T.),GetNumSC1(.T.))
	If !VarSetXD(Self:cUIDGlobal, cName, cNumDoc )
		Self:msgLog(STR0022 + cName)  //"Erro ao atualizar o n�mero do documento "
	EndIf
	VarEndT(Self:cUIDGlobal, cName)

Return cNumDoc

/*/{Protheus.doc} initDocUni
Inicia a numera��o �nica dos documentos em mem�ria.

@type  Method
@author renan.roeder
@since 27/07/2020
@version P12.1.31
@param cName, Character, Nome do documento a ser incrementado.
@return Nil
/*/
METHOD initDocUni(cName) CLASS ProcessaDocumentos
	If !VarSetX(Self:cUIDGlobal, cName, "")
		Self:msgLog(STR0022 + cName)  //"Erro ao atualizar o n�mero do documento "
	EndIf
Return

/*/{Protheus.doc} getDocUni
Recupera o valor de um contador identificado por cName

@type  Method
@author renan.roeder
@since 27/07/2020
@version P12.1.31
@param cName, Character, Nome do documento a ser incrementado.
@return nCount, Numeric, Valor atual do contador.
/*/
METHOD getDocUni(cName) CLASS ProcessaDocumentos
	Local lRet   := .T.
	Local cNumDoc := ""

	lRet := VarGetXD(Self:cUIDGlobal, cName, @cNumDoc)
	If !lRet
		cNumDoc := ""
	EndIf
Return cNumDoc


/*/{Protheus.doc} getDate
Formata uma string de data no formato AAAA-MM-DD para o formato DATE Advpl

@type  Static Function
@author lucas.franca
@since 26/12/2019
@version P12.1.27
@param cData, Character, Data no formato AAAA-MM-DD
@return dData, Character, Data no formato Date
/*/
Static Function getDate(cData)
	Local dData := Nil
	cData := StrTran(cData,'-','')
	dData := StoD(cData)
Return dData
