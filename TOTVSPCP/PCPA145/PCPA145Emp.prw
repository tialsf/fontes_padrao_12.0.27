#INCLUDE "TOTVS.CH"
#INCLUDE "PCPA145DEF.ch"

Static _cLoteCtl := CriaVar("D4_LOTECTL")
Static _cNumLote := CriaVar("D4_NUMLOTE")
Static _cFilSD4  := xFilial("SD4")

/*/{Protheus.doc} PCPA145Emp
Fun��o para gera��o dos empenhos.

@type  Function
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param oProcesso , Object, Inst�ncia da classe ProcessaDocumentos
@param aDados    , Array , Array com as informa��es do rastreio que ser�o processados.
                           As posi��es deste array s�o acessadas atrav�s das constantes iniciadas
                           com o nome RASTREIO_POS. Estas constantes est�o definidas no arquivo PCPA145DEF.ch
@param aDocPaiERP, Array , Lista dos documentos gerados pelo Protheus
                           Estrutura do array:
                           aDocPaiERP[nIndex][1] - Documento gerado no ERP Protheus
                           aDocPaiERP[nIndex][2] - Quantidade de empenho necess�rio para o documento
@return Nil
/*/
Function PCPA145Emp(oProcesso, aDados, aDocPaiERP)
	Local aDadosEmp  := {}
	Local cTicket    := oProcesso:cTicket
	Local cProdOrig  := " "
	Local cTipoOp    := oProcesso:getTipoDocumento()
	Local lAglutina  := oProcesso:getGeraDocAglutinado(aDados[RASTREIO_POS_NIVEL])
	Local nBaixaEst  := aDados[RASTREIO_POS_BAIXA_EST]
	Local nQtdEmpEst := 0
	Local nPos       := 0
	Local nSaldo     := 0
	Local nIndexDoc  := 0
	Local nTotalDoc  := Len(aDocPaiERP)

	//Se possui chave de substitui��o, � necess�rio buscar qual o produto origem
	If Empty(aDados[RASTREIO_POS_CHAVE_SUBST])
		cProdOrig := " "
	Else
		cProdOrig := retProOrig(aDados[RASTREIO_POS_CHAVE_SUBST], cTicket, aDados[RASTREIO_POS_DOCPAI])
	EndIf

	For nIndexDoc := 1 To nTotalDoc
		If aDocPaiERP[nIndexDoc][2] == 0
			Loop
		EndIf
		nQtdEmpEst := 0
		//Verifca o n�vel, se for nivel 99, gera o registro do empenho com o valor total e finaliza o processo.
		//Se n�o for n�vel 99, � necess�rio quebrar o empenho, gerando 1 registro com o valor da necessidade
		//e outro registro com o valor utilizado de estoque, se houver.
		If aDados[RASTREIO_POS_NIVEL] == "99"
			//Inclui a linha do Array
			Aadd(aDadosEmp,{})
			nPos := Len(aDadosEmp)

			aDadosEmp[nPos] := Array(EMPENHO_TAMANHO)
			aDadosEmp[nPos,EMPENHO_POS_ORDEM_PRODUCAO ] := aDocPaiERP[nIndexDoc][1]
			aDadosEmp[nPos,EMPENHO_POS_DATA_EMPENHO   ] := aDados[RASTREIO_POS_DATA_ENTREGA]
			aDadosEmp[nPos,EMPENHO_POS_PRODUTO        ] := aDados[RASTREIO_POS_PRODUTO     ]
			aDadosEmp[nPos,EMPENHO_POS_LOCAL          ] := aDados[RASTREIO_POS_LOCAL       ]
			aDadosEmp[nPos,EMPENHO_POS_QUANTIDADE     ] := aDocPaiERP[nIndexDoc][2]
			aDadosEmp[nPos,EMPENHO_POS_QTD_ORIGINAL   ] := aDocPaiERP[nIndexDoc][2]
			aDadosEmp[nPos,EMPENHO_POS_TRT            ] := aDados[RASTREIO_POS_TRT         ]
			aDadosEmp[nPos,EMPENHO_POS_ROTEIRO        ] := aDados[RASTREIO_POS_ROTEIRO     ]
			aDadosEmp[nPos,EMPENHO_POS_OPERACAO       ] := aDados[RASTREIO_POS_OPERACAO    ]
			aDadosEmp[nPos,EMPENHO_POS_QTD_SEGUNDA_UM ] := oProcesso:ConvUm(aDados[RASTREIO_POS_PRODUTO], aDocPaiERP[nIndexDoc][2], 0, 2)
			aDadosEmp[nPos,EMPENHO_POS_OP_ORIGEM      ] := " "
			aDadosEmp[nPos,EMPENHO_POS_PRODUTO_PAI    ] := oProcesso:getProdPaiDocumento(aDados[RASTREIO_POS_DOCPAI])
			aDadosEmp[nPos,EMPENHO_POS_PRODUTO_ORIGEM ] := cProdOrig
		Else
			//Verifica se houve baixa do estoque e, caso exista, gerar um registro com o valor baixado,
			//com o campo D4_OPORIG em branco
			//Quando parametrizado para aglutinar, n�o ir� fazer a quebra do empenho.
			If nBaixaEst > 0 .And. !lAglutina
				//Inclui a linha do Array
				Aadd(aDadosEmp,{})
				nPos := Len(aDadosEmp)

				aDadosEmp[nPos] := Array(EMPENHO_TAMANHO)
				aDadosEmp[nPos,EMPENHO_POS_ORDEM_PRODUCAO ] := aDocPaiERP[nIndexDoc][1]
				aDadosEmp[nPos,EMPENHO_POS_DATA_EMPENHO   ] := aDados[RASTREIO_POS_DATA_ENTREGA]
				aDadosEmp[nPos,EMPENHO_POS_PRODUTO        ] := aDados[RASTREIO_POS_PRODUTO     ]
				aDadosEmp[nPos,EMPENHO_POS_LOCAL          ] := aDados[RASTREIO_POS_LOCAL       ]

				//Verifica se a qtd. de estoque atende totalmente ou parcialmente
				//o empenho do documento corrente (aDocPaiERP)
				If nBaixaEst >= aDocPaiERP[nIndexDoc][2]
					aDadosEmp[nPos,EMPENHO_POS_QUANTIDADE  ] := aDocPaiERP[nIndexDoc][2]
					aDadosEmp[nPos,EMPENHO_POS_QTD_ORIGINAL] := aDocPaiERP[nIndexDoc][2]
					nBaixaEst -= aDocPaiERP[nIndexDoc][2]
				Else
					aDadosEmp[nPos,EMPENHO_POS_QUANTIDADE  ] := nBaixaEst
					aDadosEmp[nPos,EMPENHO_POS_QTD_ORIGINAL] := nBaixaEst
					nBaixaEst := 0
				EndIf

				aDadosEmp[nPos,EMPENHO_POS_TRT            ] := aDados[RASTREIO_POS_TRT         ]
				aDadosEmp[nPos,EMPENHO_POS_ROTEIRO        ] := aDados[RASTREIO_POS_ROTEIRO     ]
				aDadosEmp[nPos,EMPENHO_POS_OPERACAO       ] := aDados[RASTREIO_POS_OPERACAO    ]
				aDadosEmp[nPos,EMPENHO_POS_QTD_SEGUNDA_UM ] := oProcesso:ConvUm(aDados[RASTREIO_POS_PRODUTO], aDadosEmp[nPos,EMPENHO_POS_QUANTIDADE], 0, 2)
				aDadosEmp[nPos,EMPENHO_POS_OP_ORIGEM      ] := " "
				aDadosEmp[nPos,EMPENHO_POS_PRODUTO_PAI    ] := oProcesso:getProdPaiDocumento(aDados[RASTREIO_POS_DOCPAI])
				aDadosEmp[nPos,EMPENHO_POS_PRODUTO_ORIGEM ] := cProdOrig

				//Controle de qtd. de empenho gerado pela baixa de estoque
				nQtdEmpEst := aDadosEmp[nPos,EMPENHO_POS_QUANTIDADE]
			EndIf

			//Verifica se h� saldo ainda a ser empenhado.
			nSaldo := aDocPaiERP[nIndexDoc][2] - nQtdEmpEst

			//Caso exista, gera um novo registro na SD4, com o valor restante
			If nSaldo != 0
				//Inclui a linha do Array
				Aadd(aDadosEmp,{})
				nPos := Len(aDadosEmp)

				aDadosEmp[nPos] := Array(EMPENHO_TAMANHO)
				aDadosEmp[nPos,EMPENHO_POS_ORDEM_PRODUCAO ] := aDocPaiERP[nIndexDoc][1]
				aDadosEmp[nPos,EMPENHO_POS_DATA_EMPENHO   ] := aDados[RASTREIO_POS_DATA_ENTREGA]
				aDadosEmp[nPos,EMPENHO_POS_PRODUTO        ] := aDados[RASTREIO_POS_PRODUTO     ]
				aDadosEmp[nPos,EMPENHO_POS_LOCAL          ] := aDados[RASTREIO_POS_LOCAL       ]
				aDadosEmp[nPos,EMPENHO_POS_TRT            ] := aDados[RASTREIO_POS_TRT         ]
				aDadosEmp[nPos,EMPENHO_POS_ROTEIRO        ] := aDados[RASTREIO_POS_ROTEIRO     ]
				aDadosEmp[nPos,EMPENHO_POS_OPERACAO       ] := aDados[RASTREIO_POS_OPERACAO    ]
				aDadosEmp[nPos,EMPENHO_POS_QUANTIDADE     ] := nSaldo
				aDadosEmp[nPos,EMPENHO_POS_QTD_ORIGINAL   ] := nSaldo
				aDadosEmp[nPos,EMPENHO_POS_QTD_SEGUNDA_UM ] := oProcesso:ConvUm(aDados[RASTREIO_POS_PRODUTO], nSaldo, 0, 2)
				aDadosEmp[nPos,EMPENHO_POS_PRODUTO_PAI    ] := oProcesso:getProdPaiDocumento(aDados[RASTREIO_POS_DOCPAI])
				aDadosEmp[nPos,EMPENHO_POS_PRODUTO_ORIGEM ] := cProdOrig
				If lAglutina
					aDadosEmp[nPos,EMPENHO_POS_OP_ORIGEM  ] := " "
				Else
					aDadosEmp[nPos,EMPENHO_POS_OP_ORIGEM  ] := oProcesso:getDocumentoDePara(aDados[RASTREIO_POS_DOCFILHO])
					If aDadosEmp[nPos,EMPENHO_POS_OP_ORIGEM] == Nil
						aDadosEmp[nPos,EMPENHO_POS_OP_ORIGEM] := " "
					EndIf
				EndIf
			EndIf
		EndIf
	Next nIndexDoc

	//Grava os dados na tabela SD4
	gravaSD4(oProcesso,aDadosEmp,lAglutina,aDados[RASTREIO_POS_NIVEL])

	//Produto Negativo na Estrutura - Empenho Negativo = Entrada
	If aDados[RASTREIO_POS_EMPENHO] < 0
		oProcesso:atualizaSaldo(aDados[RASTREIO_POS_PRODUTO]    ,;
								aDados[RASTREIO_POS_LOCAL]      ,;
								aDados[RASTREIO_POS_NIVEL]      ,;
								-aDados[RASTREIO_POS_EMPENHO]   ,;
								1                               ,; //Tipo 1 = Entrada
								IIF(cTipoOp == "P",.T.,.F.))       //Documento Previsto 

	Else
		//Atualiza tabela tempor�ria para atualiza��o de estoques
		oProcesso:atualizaSaldo(aDados[RASTREIO_POS_PRODUTO],;
								aDados[RASTREIO_POS_LOCAL]  ,;
								aDados[RASTREIO_POS_NIVEL]  ,;
								aDados[RASTREIO_POS_EMPENHO],;
								2                           ,; //Tipo 2 = Sa�da
								IIF(cTipoOp == "P",.T.,.F.))   //Documento Previsto

	EndIf
Return

/*/{Protheus.doc} gravaSD4
Fun��o que ir� inserir os dados na tabela SD4

@type  Static Function
@author ricardo.prandi
@since 22/11/2019
@version P12.1.27
@param aDadosEmp, Array, Valores que ser�o inclu�dos na SD4
@param lAglutina, Logico, Indica se algutina as ordens e solicita��es de compras
@param cNivel, Caracter, Indica qual o n�vel do produto que est� sendo gerado
@return Nil
/*/
Static Function gravaSD4(oProcesso, aDadosEmp, lAglutina, cNivel)
	Local lInclui    := .T.
	Local nPos       := 0
	Local nTotal     := 0
	Local nRecno     := 0

	//Se for aglutina��o, � preciso verificar se j� existe o registro na SD4.
	//Caso exista, deve somar os valores, caso n�o exista, incluir um novo registro.
	//Se n�o for aglutina��o, incluir os registros separadamente.

	nTotal := Len(aDadosEmp)

	For nPos := 1 to nTotal
		lInclui := .T.

		If lAglutina;
		   .Or. cNivel == "99";
		   .Or. (cNivel <> "99" .And. Empty(aDadosEmp[nPos,EMPENHO_POS_OP_ORIGEM]));
		   .Or. aDadosEmp[nPos,EMPENHO_POS_QUANTIDADE] < 0

			If oProcesso:existEmpenho(aDadosEmp[nPos], @nRecno)
				SD4->(dbGoTo(nRecno))
				RecLock("SD4",.F.)
					Replace D4_QUANT   With D4_QUANT   + aDadosEmp[nPos,EMPENHO_POS_QUANTIDADE    ]
					Replace D4_QTDEORI With D4_QTDEORI + aDadosEmp[nPos,EMPENHO_POS_QTD_ORIGINAL  ]
					Replace D4_QTSEGUM With D4_QTSEGUM + aDadosEmp[nPos,EMPENHO_POS_QTD_SEGUNDA_UM]
				SD4->(MsUnlock())

				lInclui := .F.
			Else
				lInclui := .T.
			EndIf
		EndIf

		If lInclui
			//Prote��o identifica��o do pr�ximo RECNO da T4R em Trigger
			VarBeginT("LOCK_SD4", "RecLockT")
			RecLock("SD4",.T.)
			VarEndT("LOCK_SD4", "RecLockT")
				Replace D4_FILIAL  With _cFilSD4
				Replace D4_OP      With aDadosEmp[nPos,EMPENHO_POS_ORDEM_PRODUCAO]
				Replace D4_DATA    With aDadosEmp[nPos,EMPENHO_POS_DATA_EMPENHO  ]
				Replace D4_COD     With aDadosEmp[nPos,EMPENHO_POS_PRODUTO       ]
				Replace D4_LOCAL   With aDadosEmp[nPos,EMPENHO_POS_LOCAL         ]
				Replace D4_QUANT   With aDadosEmp[nPos,EMPENHO_POS_QUANTIDADE    ]
				Replace D4_QTDEORI With aDadosEmp[nPos,EMPENHO_POS_QTD_ORIGINAL  ]
				Replace D4_TRT 	   With aDadosEmp[nPos,EMPENHO_POS_TRT           ]
				Replace D4_QTSEGUM With aDadosEmp[nPos,EMPENHO_POS_QTD_SEGUNDA_UM]
				Replace D4_OPORIG  With aDadosEmp[nPos,EMPENHO_POS_OP_ORIGEM     ]
				Replace D4_PRODUTO With aDadosEmp[nPos,EMPENHO_POS_PRODUTO_PAI   ]
				Replace D4_ROTEIRO With aDadosEmp[nPos,EMPENHO_POS_ROTEIRO       ]
				Replace D4_OPERAC  With aDadosEmp[nPos,EMPENHO_POS_OPERACAO      ]
				Replace D4_PRDORG  With aDadosEmp[nPos,EMPENHO_POS_PRODUTO_ORIGEM]
				Replace D4_QTNECES With 0
				Replace D4_NUMLOTE With _cNumLote
				Replace D4_LOTECTL With _cLoteCtl
			SD4->(MsUnlock())

			oProcesso:addEmpenho(aDadosEmp[nPos], SD4->(Recno()))
		EndIf
	Next nPos

Return

/*/{Protheus.doc} retProOrig
Retorna o produto origem de um empenho de alternativo
****Essa fun��o ser� substituida pela API****

@type  Static Function
@author ricardo.prandi
@since 27/11/2019
@version P12.1.27
@param cChave, Caracter, Chave do registro de substitui��o, vindo da tabela HWC
@return cProdOrig, Caracter, C�digo do produto origem
/*/
Static Function retProOrig(cChave,cTicket,cDocPai)

	Local cProdOrig := " "
	Local oJson     := Nil

	aRegs := MrpPrdOrig(cTicket, cChave, cDocPai  )
	If aRegs[1]
		oJson := JsonObject():New()
		oJson:FromJson(aRegs[2])

		If Len(oJson["items"]) > 0
			cProdOrig := oJson["items"][1]["componentCode"]
		EndIf

		aSize(oJson["items"],0)
		FreeObj(oJson)
	EndIf

Return cProdOrig
