#INCLUDE "TOTVS.CH"
#INCLUDE "PCPA145.ch"
#INCLUDE "PCPA145DEF.ch"

Static _oProcesso := Nil
Static _lGeraOS   := Nil

/*/{Protheus.doc} PCPA145JOB
THREAD Filha para Gera��o dos documentos (SC2/SC1/SC7/SD4/SB2) de acordo com o
resultado do processamento do MRP.

@type  Function
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param cTicket  , Character, Ticket de processamento do MRP para gera��o dos documentos
@param aDados   , Array    , Array com as informa��es do rastreio que ser�o processados.
                             As posi��es deste array s�o acessadas atrav�s das constantes iniciadas
                             com o nome RASTREIO_POS. Estas constantes est�o definidas no arquivo PCPA145DEF.ch
@param cCodUsr  , Character, C�digo do usu�rio logado no sistema.
@param cNumScUni, Character, N�mero da SC quando parametrizado para ter numera��o �nica (oProcesso:cIncSC == "1").
@param lGerouPC , L�gico   , Indica que ser� gerado Pedido de Compra e o JOB deve gerar apenas em empenhos.
@return Nil
/*/
Function PCPA145JOB(cTicket, aDados, cCodUsr, cNumScUni, lGerouPC)
	Local aDocPaiERP  := {}
	Local aDadosSCPC  := {}
	Local cDocGerado  := ""
	Local cDocPaiERP  := ""
	Local cTipDocERP  := ""
	Local cChaveLock  := ""
	Local lContinua   := .T.
	Local lAglutina   := .F.
	Local lLockGlobal := .F.

	Default lGerouPC := .F.

	//Verifica se � necess�rio instanciar a classe ProcessaDocumentos nesta thread filha para utiliza��o dos m�todos.
	If _oProcesso == Nil
		_oProcesso := ProcessaDocumentos():New(cTicket, .T., /*03*/, cCodUsr)
	EndIf

	//Verifica par�metro para gera��o de ordem de substitui��o
	_lGeraOS := Iif(_lGeraOS == Nil, SuperGetMV("MV_PCPOS" ,.F.,.F.) .And. FindFunction("geraOrdSub"), _lGeraOS)

	//Verifica se o documento PAI deste registro j� foi gerado.
	If !Empty(aDados[RASTREIO_POS_DOCPAI]) .And. AllTrim(aDados[RASTREIO_POS_TIPODOC]) == "OP"
		cDocPaiERP := _oProcesso:getDocumentoDePara(aDados[RASTREIO_POS_DOCPAI])
		If Empty(cDocPaiERP)
			//Verifica se � um documento aglutinado.
			//Nesse caso, os documentos pais s�o registrados em outro local.
			If _oProcesso:getGeraDocAglutinado(aDados[RASTREIO_POS_NIVEL])
				If aDados[RASTREIO_POS_EMPENHO] <> 0
					aDocPaiERP := _oProcesso:getDocsAglutinados(aDados[RASTREIO_POS_DOCPAI], aDados[RASTREIO_POS_PRODUTO])
				EndIf
			EndIf

			If Len(aDocPaiERP) == 0

				_oProcesso:updStatusRastreio("2"                             ,;
				                             " "                             ,;
											 " "                             ,;
				                             aDados[RASTREIO_POS_TIPODOC    ],;
				                             aDados[RASTREIO_POS_DOCPAI     ],;
				                             aDados[RASTREIO_POS_TRT        ],;
				                             aDados[RASTREIO_POS_PRODUTO    ],;
				                             aDados[RASTREIO_POS_CHAVE      ],;
				                             aDados[RASTREIO_POS_CHAVE_SUBST],;
				                             aDados[RASTREIO_POS_SEQUEN     ])

				//Documento pai deste registro ainda n�o foi gerado.
				//Interrompe o processamento.
				lContinua := .F.

				//Incrementa o contador de registros marcados para calcular posteriormente.
				_oProcesso:incCount(CONTADOR_REINICIADOS)
			Else
				cDocPaiERP := " "
			EndIf
		Else
			aDocPaiERP := {{cDocPaiERP, aDados[RASTREIO_POS_EMPENHO]}}
		EndIf
	EndIf

	If lContinua
		lLockGlobal := .F.
		BEGIN TRANSACTION

			If aDados[RASTREIO_POS_NECESSIDADE] > 0 .And. !lGerouPC //Se gerou PC, n�o entra no processo de gera��o de SC e OP, gera apenas o empenho
				If aDados[RASTREIO_POS_NIVEL] == "99"
					If !Empty(cDocPaiERP)
						aDadosSCPC := AvalNecOP(@aDados, @cDocPaiERP) //Avalia Necessidade de(E) Remo��o do(o) V�nculo com a OP
					Else
						aDadosSCPC := aDados
					EndIf
					cDocGerado := PCPA145SC(@_oProcesso, @aDadosSCPC, cDocPaiERP, , cNumScUni)
					cTipDocERP := "2"
				Else
					cDocGerado := PCPA145OP(@_oProcesso, @aDados, cDocPaiERP )
					cTipDocERP := "1"
				EndIf
			EndIf

			If _lGeraOS .And. aDados[RASTREIO_POS_QTD_SUBST] < 0 .And. !Empty(aDados[RASTREIO_POS_CHAVE_SUBST])
				PCPA145Sub(@_oProcesso, @aDados, @aDocPaiERP)
			EndIf

			If !lGerouPC //Se gerou PC, n�o � necess�rio atualizar o indicador de rastreio novamente.
				_oProcesso:updStatusRastreio("1"                             ,;
			    	                         cDocGerado                      ,;
											 cTipDocERP                      ,;
			            	                 aDados[RASTREIO_POS_TIPODOC    ],;
			                	             aDados[RASTREIO_POS_DOCPAI     ],;
			                    	         aDados[RASTREIO_POS_TRT        ],;
			                        	     aDados[RASTREIO_POS_PRODUTO    ],;
			       	                     	 aDados[RASTREIO_POS_CHAVE      ],;
			        	                     aDados[RASTREIO_POS_CHAVE_SUBST],;
			            	                 aDados[RASTREIO_POS_SEQUEN     ])
			EndIf

			If aDados[RASTREIO_POS_EMPENHO] <> 0
				lAglutina := _oProcesso:getGeraDocAglutinado(aDados[RASTREIO_POS_NIVEL])
				If !lAglutina .Or. (lAglutina .And. aDados[RASTREIO_POS_SEQUEN] == 1)
					cChaveLock  := RTrim(aDados[RASTREIO_POS_PRODUTO]) + CHR(13) + DtoS(aDados[RASTREIO_POS_DATA_ENTREGA]) + "LOCKEMP"
					lLockGlobal := .T.
					VarBeginT(_oProcesso:cUIDGlobal, cChaveLock)
					PCPA145Emp(@_oProcesso, @aDados, @aDocPaiERP)
				EndIf
			EndIf

		END TRANSACTION
		If lLockGlobal
			VarEndT(_oProcesso:cUIDGlobal, cChaveLock)
		EndIf
		_oProcesso:incCount(CONTADOR_GERADOS)
	EndIf

	aSize(aDados     , 0)
	aSize(aDocPaiERP , 0)

	_oProcesso:incCount(_oProcesso:cThrJobs + "_Concluidos")

Return Nil

/*/{Protheus.doc} PCPA145INT
Executa as integra��es pendentes das ordens de produ��o.

@type  Function
@author lucas.franca
@since 26/12/2019
@version P12.1.29
@param aDados, Array    , Array com os dados das ordens de produ��o que ser�o integradas.
@return Nil
/*/
Function PCPA145INT(aDados)
	Local aOrdens  := {}
	Local lIntegra := Nil
	Local nTotal   := 0
	Local nIndex   := 0
	Local oTemp    := Nil
	Local nTempo := MicroSeconds()

	If aDados == Nil .Or. Len(aDados) == 0
		Return
	EndIf

	ProcessaDocumentos():msgLog(STR0002)  //"INICIANDO INTEGRA��O DAS ORDENS DE PRODU��O"

	Ma650MrpOn(@lIntegra, @oTemp)

	If lIntegra
		nTempo := MicroSeconds()
		nTotal := Len(aDados)
		For nIndex := 1 To nTotal
			aAdd(aOrdens, aDados[nIndex][2][1])
		Next nIndex

		MATA650INT("INSERT", aOrdens, oTemp)

		aSize(aOrdens, 0)
		aSize(aDados, 0)
		oTemp:Delete()
		FreeObj(oTemp)
		ProcessaDocumentos():msgLog(STR0003 + cValToChar(MicroSeconds()-nTempo))  //"TEMPO PARA EXECUTAR A INTEGRA��O: "
	EndIf

	ProcessaDocumentos():msgLog(STR0004)  //"FIM DA INTEGRA��O DAS ORDENS DE PRODU��O"
Return Nil

/*/{Protheus.doc} AvalNecOP
Fun��o respons�vel por avaliar a necessidade da OP e remover o v�nculo do registro para gera��o da SC ou PC
quando foi aplicada pol�tica de estoque (necessidade a Necessidade for maior que Necessidade Original - Baixa Estoque - Substitui��o)

@type  Function
@author brunno.costa
@since 08/04/2020
@version P12.1.30

@param 01 - aDados , Array , Array com as informa��es do rastreio que ser�o processados.
                           As posi��es deste array s�o acessadas atrav�s das constantes iniciadas
                           com o nome RASTREIO_POS. Estas constantes est�o definidas no arquivo PCPA145DEF.ch
@param 02 - cDocPaiERP, caracter, c�digo do documento Pai no ERP - retorna por refer�ncia
@return aDadosSCPC, array, aDados com remo��o do v�nculo com a OP, quando for o caso
/*/
Static Function AvalNecOP(aDados, cDocPaiERP)
	Local aDadosSCPC := {}

	If  aDados[RASTREIO_POS_NECESSIDADE] > ( aDados[RASTREIO_POS_NECES_ORIG];
	                                        -aDados[RASTREIO_POS_BAIXA_EST] ;
	                                        -aDados[RASTREIO_POS_QTD_SUBST] )
		aDadosSCPC                       := aClone(aDados)
		aDadosSCPC[RASTREIO_POS_TIPODOC] := ""
		aDadosSCPC[RASTREIO_POS_DOCPAI]  := ""
		cDocPaiERP                       := ""

	Else
		aDadosSCPC                      := aDados

	EndIf

Return aDadosSCPC
