#INCLUDE "TOTVS.CH"
#INCLUDE "PCPA145DEF.ch"

Static _cFunName    := "PCPA144"
Static _lGeraSHY    := SuperGetMV("MV_APS",.F.,"") == "TOTVS" .Or. ;
                       (ExisteSFC("SC2") .And. !IsInCallStack("AUTO650")) .Or. ;
                       SuperGetMV("MV_PCPATOR",.F.,.F.) == .T.
Static _nDiasOci    := SuperGetMV("MV_DIASOCI",.F.,99)
Static _nTamNumOP   := GetSX3Cache("C2_NUM"   , "X3_TAMANHO")
Static _nTamItmOP   := GetSX3Cache("C2_ITEM"  , "X3_TAMANHO")
Static _nTamSeqOP   := GetSX3Cache("C2_SEQUEN", "X3_TAMANHO")
Static _nTamRot     := GetSX3Cache("G2_CODIGO", "X3_TAMANHO")
Static _oCacheIni   := JsonObject():New()
/*/{Protheus.doc} PCPA145OP
Fun��o para gera��o das ordens de produ��o.

@type  Function
@author lucas.franca
@since 12/11/2019
@version P12.1.27
@param oProcesso , Object, Inst�ncia da classe ProcessaDocumentos
@param aDados , Array    , Array com as informa��es do rastreio que ser�o processados.
                           As posi��es deste array s�o acessadas atrav�s das constantes iniciadas
                           com o nome RASTREIO_POS. Estas constantes est�o definidas no arquivo PCPA145DEF.ch
@param cDocPaiERP, Character, C�digo do documento pai deste registro.
@return Nil
/*/
Function PCPA145OP(oProcesso, aDados, cDocPaiERP)
	
	Local aOPC       := {}
	Local cC2MOPC    := " "
	Local cField     := " "
	Local cItemOp    := " "
	Local cNumOp     := " "
	Local cOPGerada  := " " //"XXXXXXX" + aDados[RASTREIO_POS_DOCFILHO]
	Local cRoteiro   := " "
	Local cRotSB1    := " "
	Local cTicket    := oProcesso:cTicket
	Local cTipoOp    := oProcesso:getTipoDocumento()
	Local cSeqOp     := " "
	Local cSeqPai    := " "
	Local lAglutina  := oProcesso:getGeraDocAglutinado(aDados[RASTREIO_POS_NIVEL])
	Local nTotal     := 0
	Local nX         := 0
	Local oJson
	
	//Gera a numera��o da OP:
	//Se aglutina ou o documento pai � branco, busca numera��o nova, caso contr�rio, incrementa a sequencia.
	If lAglutina .Or. Empty(cDocPaiERP)
		If oProcesso:cIncOP == "2"
			//Busca numera��o da OP
			cNumOp := GetNumSC2(.T.)
		Else
			//Busca n�mero �nico da OP, que foi reservado no NEW do PCPA145
			cNumOp := oProcesso:getDocUni("C2_NUM")
		EndIf

		//Incrementa o item da OP
		cItemOp := prxItemOp(@cNumOp, oProcesso)

		//Incrementa a sequencia da OP
		cSeqOp  := proximaSeq(cNumOp+cItemOp, oProcesso)
		
		cSeqPai := Space(_nTamSeqOP)

		//Concatena para guardar a numera��o completa
		cOPGerada := cNumOp + cItemOp + cSeqOp + "   "
	Else
		//Busca n�mero da OP do documento pai gerado
		cNumOp := Left(cDocPaiERP, _nTamNumOP)

		//Grava o item
		cItemOp := SubStr(cDocPaiERP, _nTamNumOP + 1, _nTamItmOP)

		//A sequencia ser� extra�da do documento que o MRP gerou
		cSeqOp := proximaSeq(cNumOp+cItemOp, oProcesso)

		//Concatena para guardar a numera��o completa
		cOPGerada := cNumOp + cItemOp + cSeqOp + "   "

		//Busca SEQPAI usado na gera��o da ordem pai
		cSeqPai := Substr(cDocPaiERP, _nTamNumOP+_nTamItmOP+1, _nTamSeqOP)
	EndIf

	//Posiciona na SB1
	If SB1->B1_COD != aDados[RASTREIO_POS_PRODUTO]
		SB1->(dbSeek(xFilial("SB1")+aDados[RASTREIO_POS_PRODUTO]))
	EndIf

	//Recupera Roteiro do produto
	cRotSB1 := SB1->B1_OPERPAD

	//Recupera informa��es de opcional
	If !Empty(aDados[RASTREIO_POS_OPC_ID])
		If "|"$aDados[RASTREIO_POS_OPC_ID]
            aDados[RASTREIO_POS_OPC_ID] := Left(aDados[RASTREIO_POS_OPC_ID], (At("|", aDados[RASTREIO_POS_OPC_ID])-1) )
        EndIf
		aOPC    := MrpGetOPC(cFilAnt, cTicket, aDados[RASTREIO_POS_OPC_ID])
		oJson   := JsonObject():New()
        oJson:fromJson(aOPC[2])
		cC2MOPC := Array2STR(oJson["optionalMemo"], .F.)
	EndIf

	//Inicia a inclus�o
	RecLock("SC2",.T.)

		//Gravando inicializador padr�o dos campos de usuario
		nTotal := FCount()
		For nX := 1 TO nTotal
			cField := Field(nX)
			If _oCacheIni[cField] == Nil
				If (cField $ "C2_NUM/C2_ITEM/C2_SEQUEN") .Or. !ExistIni(cField, .F.)
					_oCacheIni[cField] := {.F., "", ""}
				Else
					_oCacheIni[cField] := {.T., GetSx3Cache(cField,"X3_RELACAO"), X3Titulo()}
				EndIf
			EndIf

			If _oCacheIni[cField][1]
				SC2->&(cField) := InitPad(_oCacheIni[cField][2], _oCacheIni[cField][3])
			EndIf
		Next nX

		//Dados da numera��o da OP
		Replace	C2_FILIAL  With xFilial("SC2")
		Replace	C2_NUM     With cNumOp
		Replace	C2_ITEM    With cItemOp
		Replace	C2_SEQUEN  With cSeqOp
		Replace C2_ITEMGRD With "  "
		Replace C2_OP      With cOPGerada

		//Dados vindo da API
		Replace	C2_DATPRF  With aDados[RASTREIO_POS_DATA_ENTREGA            ]
		Replace	C2_DATPRI  With aDados[RASTREIO_POS_DATA_INICIO             ]
		Replace	C2_LOCAL   With aDados[RASTREIO_POS_LOCAL                   ]
		Replace C2_PRODUTO With aDados[RASTREIO_POS_PRODUTO                 ]
		Replace	C2_QUANT   With aDados[RASTREIO_POS_NECESSIDADE             ]
		Replace C2_REVISAO With aDados[RASTREIO_POS_REVISAO                 ]
		Replace	C2_ROTEIRO With aDados[RASTREIO_POS_ROTEIRO_DOCUMENTO_FILHO ]

		//Dados vindo da SB1
		Replace	C2_CC      With SB1->B1_CC
		Replace	C2_SEGUM   With SB1->B1_SEGUM
		Replace	C2_UM      With SB1->B1_UM

		//Dados fixos
		Replace C2_BATCH    With "S"
		Replace C2_BLQAPON  With "2"
		Replace	C2_DESTINA  With "E"
		Replace C2_GRADE    With " "
		Replace	C2_IDENT    With "P"
		Replace	C2_ITEMPV   With " "

		If !Empty(aDados[RASTREIO_POS_OPC_ID])
			Replace C2_MOPC     With cC2MOPC
			Replace	C2_OPC      With oJson["optionalString"]
		Else
			Replace C2_MOPC     With " "
			Replace	C2_OPC      With " "
		EndIf

		Replace	C2_PEDIDO   With " "
		Replace	C2_PRIOR    With "500"

		//Dados diversos
		Replace C2_BATROT   With _cFunName
		Replace C2_BATUSR   With oProcesso:cCodUsr
		Replace C2_DIASOCI  With _nDiasOci
		Replace	C2_EMISSAO  With dDataBase
		Replace	C2_QTSEGUM  With oProcesso:ConvUm(aDados[RASTREIO_POS_PRODUTO],aDados[RASTREIO_POS_NECESSIDADE],0,2)
		Replace C2_SEQMRP   With cTicket
		Replace	C2_SEQPAI   With cSeqPai
		Replace C2_TPOP     With cTipoOp

	SC2->(MsUnLock())

	//Verifica roteiro para gera��o da SHY
	If _lGeraSHY
		If Empty(cRoteiro := SC2->C2_ROTEIRO)
			If Empty(cRoteiro := cRotSB1)
				cRoteiro := StrZero(1, _nTamRot)
			EndIf
		EndIf

		//Gera a SHY
		TAPSOperac(cOPGerada,aDados[RASTREIO_POS_PRODUTO],cRoteiro,aDados[RASTREIO_POS_NECESSIDADE])
	EndIf

	//Inclui dados no array para integra��o com o novo MRP
	//O envio das ordens ser� feito somente no fim do processamento, na fun��o PCPA145INT
	If oProcesso:lIntegraOP
		oProcesso:salvaOPIntegracao()
	EndIf

	//Ap�s a gera��o de uma ordem de produ��o, deve ser atualizado o
	//DE-PARA de documentos do MRP.
	oProcesso:atualizaDeParaDocumentoProduto(aDados[RASTREIO_POS_DOCFILHO], {cOPGerada,aDados[RASTREIO_POS_PRODUTO]})

	//Atualiza tabela tempor�ria para atualiza��o de estoques
	oProcesso:atualizaSaldo(aDados[RASTREIO_POS_PRODUTO]    ,;
	                        aDados[RASTREIO_POS_LOCAL]      ,;
	                        aDados[RASTREIO_POS_NIVEL]      ,;
	                        aDados[RASTREIO_POS_NECESSIDADE],;
	                        1                           ,; //Tipo 1 = Entrada
	                        IIF(cTipoOp == "P",.T.,.F.))   //Documento Previsto
Return cOPGerada

/*/{Protheus.doc} proximaSeq
Retorna a pr�xima sequ�ncia v�lida para utiliza��o na gera��o da tabela SC2

@type  Static Function
@author lucas.franca
@since 17/12/2019
@version P12.1.28
@param cChaveOP , Character, Chave identificadora da OP (C2_NUM+C2_ITEM)
@param oProcesso, Object   , Refer�ncia da classe de controle do processamento
@return cSequen , Character, Pr�xima sequ�ncia para utiliza��o na gera��o da tabela SC2
/*/
Static Function proximaSeq(cChaveOP, oProcesso)
	Local cSequen  := ""
	Local lRet     := .T.

	cChaveOP := "SEQOP" + cChaveOP

	//Abre transa��o na chave da OP
	VarBeginT(oProcesso:cUIDGlobal, cChaveOP )

	//Recupera o valor atual da sequ�ncia desta OP
	lRet := VarGetXD(oProcesso:cUIDGlobal, cChaveOP, @cSequen)
	If !lRet
		//N�o encontrou a chave. Inicializa.
		cSequen := StrZero(1, _nTamSeqOP)
	Else
		//Encontrou a chave, incrementa a sequ�ncia
		cSequen := Soma1(cSequen)
	EndIf

	//Atualiza a global com a �ltima sequ�ncia
	lRet := VarSetXD(oProcesso:cUIDGlobal, cChaveOP, cSequen)

	//Fecha a transa��o da chave da OP
	VarEndT(oProcesso:cUIDGlobal, cChaveOP)

Return cSequen

/*/{Protheus.doc} prxItemOp
Retorna o pr�ximo item v�lido para utiliza��o na gera��o da tabela SC2

@type  Static Function
@author ricardo.prandi
@since 22/04/2020
@version P12.1.30
@param cNumOp   , Character, N�mero da OP (C2_NUM)
@param oProcesso, Object   , Refer�ncia da classe de controle do processamento
@return cItem   , Character, Pr�ximo item para utiliza��o na gera��o da tabela SC2
/*/
Static Function prxItemOp(cNumOp, oProcesso)
	Local aOpItem   := {}
	Local cItem     := ""
	Local lRet      := .T.
	Local cItemMax  := PadR("", _nTamItmOP,"9")

	If oProcesso:cIncOP == "2"
		cItem := PadR("01", _nTamItmOP)
	Else
		cChaveOP := "ITEMOP"

		//Abre transa��o na chave da OP
		VarBeginT(oProcesso:cUIDGlobal, cChaveOP )

		//Recupera o valor atual da sequ�ncia desta OP
		lRet := VarGetAD(oProcesso:cUIDGlobal, cChaveOP, @aOpItem)
		If !lRet
			//N�o encontrou a chave. Inicializa.
			cItem := StrZero(1, _nTamItmOP)
		Else
			//Encontrou a chave, incrementa a sequ�ncia
			cNumOp := aOpItem[DOC_NUM]
			cItem  := Soma1(aOpItem[DOC_ITEM])
			
			If cItem > cItemMax
				cNumOp := oProcesso:incDocUni("C2_NUM")
				cItem := StrZero(1, _nTamItmOP)
			EndIf
		EndIf

		aOpItem := {cNumOp,cItem}

		//Atualiza a global com a �ltima sequ�ncia
		lRet := VarSetAD(oProcesso:cUIDGlobal, cChaveOP, aOpItem)

		//Fecha a transa��o da chave da OP
		VarEndT(oProcesso:cUIDGlobal, cChaveOP)
	EndIf

Return cItem