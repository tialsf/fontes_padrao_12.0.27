#INCLUDE "TOTVS.CH"

//DEFINES do subarray de SALDOS do produto, com as posi��es de cada tipo de quantidade.
#DEFINE SALDOS_QTD_POS_ENTRADA_PREV  1
#DEFINE SALDOS_QTD_POS_SAIDA_PREV    2
#DEFINE SALDOS_QTD_TAMANHO           2

Static oExcPrevistos := Nil
Static _lNewMRP      := Nil
Static snTamCod      := 90

/*/{Protheus.doc} PCPA146
Exclus�o dos Documentos Previstos

@type  Function
@author renan.roeder
@since 29/11/2019
@version P12.1.27
@param 01 - cTicket   , character, Ticket do processamento
@param 02 - lHoriFirme, l�gico   , indica se considera o horizonte firme do produto
@param 03 - dDataIni  , data     , data de processamento do MRP utilizada como refer�ncia (database)
@param 04 - cErrorUID , character, codigo identificador do controle de erros multi-thread
@return Nil
/*/
Function PCPA146(cTicket, lHoriFirme, dDataIni, cErrorUID)

	Local oStatus      := MrpDados_Status():New(cTicket)

	Default lHoriFirme := .F.
	Default nTentativa := 1

	oStatus:setStatus("tempo_exlusao_previstos_ini", MicroSeconds())

	oExcPrevistos := ExclusaoPrevistos():New(cTicket, .T., lHoriFirme, dDataIni, cErrorUID)
	oExcPrevistos:AtualizaStatus()
	oExcPrevistos:Processar()
	oExcPrevistos:Destroy()

	FreeObj(oExcPrevistos)

Return Nil


/*/{Protheus.doc} ExclusaoPrevistos
Classe com as regras para exclus�o dos Documentos Previstos.

@author renan.roeder
@since 02/12/2019
@version P12.1.27
/*/
CLASS ExclusaoPrevistos FROM LongClassName

	DATA cErrorUID  AS CHARACTER
	DATA cTicket    AS CHARACTER
	DATA cUIDSld    AS CHARACTER
	DATA cUIDSldLte AS CHARACTER
	DATA cUIDSldLcz AS CHARACTER
	DATA cUIDStatus AS CHARACTER
	DATA cUIDOrdens AS CHARACTER
	DATA cUIDSB1Cnv AS CHARACTER
	DATA lHoriFirme AS LOGICAL
	DATA dDataIni   AS DATE
	DATA lExistDHN1 AS LOGICAL
	DATA lExistDHN3 AS LOGICAL
	DATA lExistAE   AS LOGICAL

	DATA cThrJobs   AS Character
	DATA nThrJobs   AS Numeric
	DATA nDecimal   AS Numeric

	METHOD New(cTicket,lCriaVar,lHoriFirme,dDataIni,cErrorUID) CONSTRUCTOR
	METHOD Destroy()

	METHOD Processar()
	METHOD criarSecaoGlobal()
	METHOD ExcluiOrdensProducao()
	METHOD ExcluiSolicitacoesCompra()
	METHOD ExcluiPedidosCompra()
	METHOD AtualizaContratoParceria(cNumSc,cItemSc,nQuant)
	METHOD SetSaldos(cProduto, cLocal, cLoteCtl, cNumLote, cLocaliz, cNumseri, nQtd, nTipo)
	METHOD GetSaldo(cProduto, cLocal, lRet)
	METHOD GetLoteSaldo(cProduto, cLocal, cLoteCtl, cNumlote, lRet)
	METHOD GetLocalizaSaldo(cProduto, cLocal, cLoteCtl, cNumLote, cLocaliz, cNumSeri, lRet)
	METHOD PersisteSaldos()
	METHOD AtualizaStatus()
	METHOD GetStatus()
	METHOD LimpaVarStatus()
	METHOD ExisteDHNValido(cTipo)
	METHOD CargaSB1Conversao()
	METHOD ConverteUM(cProduto, nQtd)
	METHOD AguardaInicioCalculoMRP(lCalTmpIni)
	METHOD ExisteAutorizacaoEntrega()

ENDCLASS

/*/{Protheus.doc} New
M�todo construtor da classe de exclus�o dos documentos previstos

@type  Method
@author renan.roeder
@since 29/11/2019
@version P12.1.27
@param 01 - cTicket   , Character, N�mero do ticket do processamento do MRP
@param 02 - lCriavar  , Logical  , Define se inicializar� as threads e vari�veis globais.
@param 03 - dDataIni  , data     , data de processamento do MRP utilizada como refer�ncia (database)
@param 04 - cErrorUID , character, codigo identificador do controle de erros multi-thread
@return Self
/*/
METHOD New(cTicket,lCriavar,lHoriFirme,dDataIni,cErrorUID) CLASS ExclusaoPrevistos

	Self:cTicket    := cTicket
	Self:cThrJobs   := Self:cTicket + "PROC"
	Self:cUIDSld    := "PREVSALDO_" + Self:cTicket
	Self:cUIDSldLte := "PREVLOTESALDO_" + Self:cTicket
	Self:cUIDSldLcz := "PREVLOCALIZSALDO_" + Self:cTicket
	Self:cUIDStatus := "PREVSTATUS_" + Self:cTicket
	Self:cUIDOrdens := "OPSINTEGRA_" + Self:cTicket
	Self:cUIDSB1Cnv := "SB1_CONV_" + Self:cTicket
	Self:nThrJobs   := 4
	Self:nDecimal   := GetSx3Cache("C7_QUANT","X3_DECIMAL")

	If cErrorUID == Nil
		cErrorUID := Self:cUIDStatus
	EndIf
	Self:cErrorUID := cErrorUID

	If lCriavar
		PCPIPCStart(Self:cThrJobs, Self:nThrJobs, 0, cEmpAnt, cFilAnt, cErrorUID) //Inicializa as Threads

		//Abre novas threads para executar os processos de forma paralela
		PCPIPCStart(Self:cThrJobs + "OP", Self:nThrJobs, 0, cEmpAnt, cFilAnt, cErrorUID) //Inicializa as Threads

		::criarSecaoGlobal()
		Self:lHoriFirme := lHoriFirme
		Self:dDataIni   := dDataIni

		VarSetXD(Self:cUIDStatus, "lHoriFirme", lHoriFirme)
		VarSetXD(Self:cUIDStatus, "dDataIni"  , dDataIni  )

		::cargaSB1Conversao()

	Else
		VarGetXD(Self:cUIDStatus, "lHoriFirme", @Self:lHoriFirme)
		VarGetXD(Self:cUIDStatus, "dDataIni"  , @Self:dDataIni)
	EndIf

	Self:lExistDHN1 := Self:ExisteDHNValido("1")
	Self:lExistDHN3 := Self:ExisteDHNValido("3")
	Self:lExistAE   := Self:ExisteAutorizacaoEntrega()

Return Self

/*/{Protheus.doc} Destroy
M�todo destrutor da classe de gera��o de exlus�o dos documentos previstos

@type  Method
@author renan.roeder
@since 29/11/2019
@version P12.1.27
@return Nil
/*/
METHOD Destroy() CLASS ExclusaoPrevistos

	PCPIPCFinish(Self:cThrJobs, 300, Self:nThrJobs)
	PCPIPCFinish(Self:cThrJobs + "OP", 300, Self:nThrJobs)

	//Limpa da mem�ria as vari�veis globais
	VarClean(Self:cUIDSld)
	VarClean(Self:cUIDSldLte)
	VarClean(Self:cUIDSldLcz)
	VarClean(Self:cUIDOrdens)
	VarClean(Self:cUIDSB1Cnv)

Return Nil

/*/{Protheus.doc} Processar
M�todo que gerencia a exlus�o dos documentos previstos.

@type  Method
@author renan.roeder
@since 29/11/2019
@version P12.1.27
@return Nil
/*/
METHOD Processar() CLASS ExclusaoPrevistos

	Local lOkGet    := .T.
	Local nSecAux   := 0
	Local nSecTotal
	Local oStatus   := MrpDados_Status():New(Self:cTicket)

	PCPIPCGO(Self:cThrJobs, .F., "A146ExcOPs", Self:cTicket)
	PCPIPCGO(Self:cThrJobs, .F., "A146ExcSCs", Self:cTicket)
	PCPIPCGO(Self:cThrJobs, .F., "A146ExcPCs", Self:cTicket)

	While IPCCount(Self:cThrJobs) < Self:nThrJobs
		Sleep(500)
	EndDo

	::PersisteSaldos()
	::AtualizaStatus()

	nSecAux   := oStatus:getStatus("tempo_exlusao_previstos_fim", @lOkGet)
	If lOkGet
		nSecTotal := MicroSeconds() - nSecAux
	Else
		nSecTotal := 0
	EndIf
	oStatus:setStatus("tempo_exlusao_previstos_fim", nSecTotal)

	lOkGet := .T.
	nSecAux := oStatus:getStatus("tempo_exlusao_previstos_ini", @lOkGet)
	If lOkGet
		oStatus:setStatus("tempo_exlusao_previstos", nSecTotal + nSecAux)
	Else
		oStatus:setStatus("tempo_exlusao_previstos", nSecTotal)
	EndIf

Return

/*/{Protheus.doc} criarSecaoGlobal
Cria a se��o de vari�veis globais que ser� utilizada no processamento

@type  Method
@author renan.roeder
@since 04/12/2019
@version P12.1.27
@return Nil
/*/
METHOD criarSecaoGlobal() CLASS ExclusaoPrevistos

	If !VarSetUID(Self:cUIDSld)
		LogMsg("PCPA146", 0, 0, 1, "", "", "Erro na cria��o da se��o de vari�veis globais de saldo.")
	EndIf
	If !VarSetUID(Self:cUIDSldLte)
		LogMsg("PCPA146", 0, 0, 1, "", "", "Erro na cria��o da se��o de vari�veis globais de saldo/lote.")
	EndIf
	If !VarSetUID(Self:cUIDSldLcz)
		LogMsg("PCPA146", 0, 0, 1, "", "", "Erro na cria��o da se��o de vari�veis globais de saldo/localiza��o.")
	EndIf
	If !VarSetUID(Self:cUIDStatus)
		LogMsg("PCPA146", 0, 0, 1, "", "", "Erro na cria��o da se��o de vari�veis globais de status.")
	EndIf
	If !VarSetUID(Self:cUIDOrdens)
		LogMsg("PCPA146", 0, 0, 1, "", "", "Erro na cria��o da se��o de vari�veis globais de Ordens.")
	EndIf
	If !VarSetUID(Self:cUIDSB1Cnv)
		LogMsg("PCPA146", 0, 0, 1, "", "", "Erro na cria��o da se��o de vari�veis globais de convers�o de UM.")
	EndIf

Return Nil

/*/{Protheus.doc} AguardaInicioCalculoMRP
Aguarda In�cio dos C�lculos do MRP

@type  Method
@author brunno.costa
@since 20/05/2020
@version P12.1.27
@param 01 - lCalTmpIni, l�gico, indica se deve realizar o calculo de tempo inicial
@return lReturn, l�gico, indica se iniciou o c�lculo do MRP e deve prosseguir na exclus�o dos documentos
/*/
METHOD AguardaInicioCalculoMRP(lCalTmpIni) CLASS ExclusaoPrevistos

	Local lReturn    := .T.
	Local oProcesso  := JsonObject():New()
	Local oStatus
	Local nSecTotal
	Local nLiberaSC2 := 0

	Default lCalTmpIni := .F.

	If lCalTmpIni
		oStatus := MrpDados_Status():New(Self:cTicket)
		VarGetXD(Self:cUIDStatus, "nLiberaSC2", @nLiberaSC2)

		While nLiberaSC2 < 3
			Sleep(200)
			VarGetXD(Self:cUIDStatus, "nLiberaSC2", @nLiberaSC2)
		EndDo

		nSecTotal := oStatus:getStatus("tempo_exlusao_previstos_ini")
		nSecTotal := MicroSeconds() - nSecTotal
		oStatus:setStatus("tempo_exlusao_previstos_ini", nSecTotal)
	EndIf

	//Aguarda In�cio do C�lculo do MRP
	aReturn := MrpGet(cFilAnt, Self:cTicket)
	oProcesso:fromJson(aReturn[2])
	While (oProcesso["mrpCalculationStatus"] == Nil .OR. !(oProcesso["mrpCalculationStatus"] $ "|2|3|4|9|"))
		Sleep(500)
		aReturn := MrpGet(cFilAnt, Self:cTicket)
		oProcesso:fromJson(aReturn[2])
	EndDo

	If lCalTmpIni
		oStatus:setStatus("tempo_exlusao_previstos_fim", MicroSeconds())
	EndIf

	//N�o executa em caso de Cancelamento ou Falha
	lReturn := !(oProcesso["mrpCalculationStatus"] $ "|4|9|")


Return lReturn

/*/{Protheus.doc} ExcluiOrdensProducao
Exclui os registros da tabela SC2.

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
/*/
METHOD ExcluiOrdensProducao() CLASS ExclusaoPrevistos
	Local cThrID     := Self:cThrJobs + "OP"
	Local cUpdDel    := ""
	Local cWhereIN   := ""
	Local nTotalThr  := 4
	Local nQtd       := 0

	VarSetXD(Self:cUIDStatus, "nLiberaSC2", 0)

	PCPIPCGO(cThrID, .F., "A146SldSC2", Self:cTicket, Self:lHoriFirme, Self:dDataIni)
	PCPIPCGO(cThrID, .F., "A146SldSD4", Self:cTicket, Self:lHoriFirme, Self:dDataIni)
	PCPIPCGO(cThrID, .F., "A146SldSDC", Self:cTicket, Self:lHoriFirme, Self:dDataIni)

	If Self:AguardaInicioCalculoMRP(.T.)

		PCPIPCGO(cThrID, .F., "A146AtuPMP", Self:cTicket, Self:lHoriFirme, Self:dDataIni)

		//Aguarda o t�rmino das consultas no banco
		VarGetXD(Self:cUIDStatus, "nLiberaSC2", @nQtd)
		While nQtd < 4
			VarGetXD(Self:cUIDStatus, "nLiberaSC2", @nQtd)
			Sleep(50)
		EndDo

		//Apaga as ordens de produ��o
		cUpdDel := " UPDATE " + RetSqlName("SC2")
		cUpdDel +=    " SET D_E_L_E_T_   = '*' "
		If !Empty(FWX2Unico( 'SC2'))
			cUpdDel +=  " , R_E_C_D_E_L_ = R_E_C_N_O_ "
		EndIf
		cUpdDel +=  " WHERE " + getWhereC2(.T., " ")

		If Self:lHoriFirme
			MrpDominio_HorizonteFirme():criaScriptIN(Self:dDataIni, "SVK", "SC2", "C2_PRODUTO", "C2_DATPRI", getWhereC2(.T., " "), @cWhereIN)
			cUpdDel   += " AND (R_E_C_N_O_ NOT IN ("
			cUpdDel   += cWhereIN
			cUpdDel   += "))"
		EndIf

		If TcSqlExec(cUpdDel) < 0
			Final("Erro ao excluir os registros de Ordens de Produ��o.", TcSqlError())
		EndIf

		//Elimina os registros das demais tabelas.
		PCPIPCGO(cThrID, .F., "A146DelSD4", Self:lHoriFirme, Self:dDataIni)
		PCPIPCGO(cThrID, .F., "A146DelSDC", Self:lHoriFirme, Self:dDataIni)
		PCPIPCGO(cThrID, .F., "A146DelOS" , Self:lHoriFirme, Self:dDataIni)
		PCPIPCGO(cThrID, .F., "A146DelSHY", Self:lHoriFirme, Self:dDataIni)

		//Aguarda o t�rmino - libera��o das Threads
		While IPCCount(cThrID) < nTotalThr
			Sleep(50)
		EndDo

	EndIf

Return

/*/{Protheus.doc} A146SldSD4
Percorre os empenhos alimentando as vari�veis globais de saldo.

@type  Function
@author lucas.franca
@since 20/02/2020
@version P12.1.30
@param 01 - cTicket   , Character, N�mero do ticket do MRP em processamento
@param 02 - lHoriFirme, l�gico   , indica se considera o horizonte firme do produto
@param 03 - dDataIni  , data     , data de processamento do MRP utilizada como refer�ncia (database)
@return Nil
/*/
Function A146SldSD4(cTicket, lHoriFirme, dDataIni)
	Local cAliasSD4 := GetNextAlias()
	Local cWhereIN  := ""
	Local cQuerySD4 := ""
	Local nInd      := 1
	Local nSizeQtd  := GetSx3Cache("D4_QUANT","X3_TAMANHO")
	Local nDecQtd   := GetSx3Cache("D4_QUANT","X3_DECIMAL")

	Default lHoriFirme := .F.

	If oExcPrevistos == Nil
		oExcPrevistos := ExclusaoPrevistos():New(cTicket,.F.)
	EndIf

	cQuerySD4 := "SELECT SD4.D4_FILIAL, "
	cQuerySD4 +=       " SD4.D4_COD, "
	cQuerySD4 +=       " SD4.D4_LOCAL, "
	cQuerySD4 +=       " SD4.D4_QUANT, "
	cQuerySD4 +=       " SD4.D4_LOTECTL, "
	cQuerySD4 +=       " SD4.D4_NUMLOTE "
	cQuerySD4 +=  " FROM " + RetSqlName("SD4") + " SD4 "
	cQuerySD4 += " INNER JOIN " + RetSqlName("SC2") + " SC2 ON " + getWhereC2(.F., " ")
	cQuerySD4 +=   " AND SD4.D4_OP = " + getC2OP()
	cQuerySD4 += " WHERE SD4.D4_FILIAL  = '" + xFilial("SD4") + "' "
	cQuerySD4 +=   " AND SD4.D_E_L_E_T_ = ' '"

	If lHoriFirme
		MrpDominio_HorizonteFirme():criaScriptIN(dDataIni, "SVK", "SC2", "C2_PRODUTO", "C2_DATPRI", getWhereC2(.T., " "), @cWhereIN)
		cQuerySD4   += " AND (SC2.R_E_C_N_O_ NOT IN ("
		cQuerySD4   += cWhereIN
		cQuerySD4   += "))"
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuerySD4),cAliasSD4,.F.,.F.)

	VarSetX(oExcPrevistos:cUIDStatus, "nLiberaSC2", @nInd, 1, 1)

	TcSetField(cAliasSD4, 'D4_QUANT', 'N', nSizeQtd, nDecQtd)

	While !(cAliasSD4)->(Eof())
		oExcPrevistos:SetSaldos((cAliasSD4)->D4_COD,(cAliasSD4)->D4_LOCAL, (cAliasSD4)->D4_LOTECTL, (cAliasSD4)->D4_NUMLOTE,,, (cAliasSD4)->D4_QUANT, 2)
		(cAliasSD4)->(dbSkip())
	EndDo
	(cAliasSD4)->(dbCloseArea())

Return Nil

/*/{Protheus.doc} A146SldSC2
Percorre as ordens de produ��o alimentando as vari�veis globais de saldo.

@type  Function
@author lucas.franca
@since 20/02/2020
@version P12.1.30
@param 01 - cTicket   , Character, N�mero do ticket do MRP em processamento
@param 02 - lHoriFirme, l�gico   , indica se considera o horizonte firme do produto
@param 03 - dDataIni  , data     , data de processamento do MRP utilizada como refer�ncia (database)
@return Nil
/*/
Function A146SldSC2(cTicket, lHoriFirme, dDataIni)
	Local aDadosDel  := {}
	Local cAliasSC2  := GetNextAlias()
	Local cWhere     := ""
	Local cWhereIN   := ""
	Local lIntNewMRP := Ma650MrpOn(@_lNewMRP, "")
	Local nInd       := 1
	Local nSizeQtd   := GetSx3Cache("C2_QUANT", "X3_TAMANHO")
	Local nDecQtd    := GetSx3Cache("C2_QUANT", "X3_DECIMAL")

	If oExcPrevistos == Nil
		oExcPrevistos := ExclusaoPrevistos():New(cTicket,.F.)
	EndIf

	cWhere := "%" + getWhereC2(.F., " ")
	If lHoriFirme
		MrpDominio_HorizonteFirme():criaScriptIN(dDataIni, "SVK", "SC2", "C2_PRODUTO", "C2_DATPRI", getWhereC2(.T., " "), @cWhereIN)
		cWhere   += " AND (SC2.R_E_C_N_O_ NOT IN ("
		cWhere   += cWhereIN
		cWhere   += "))"
	EndIf
	cWhere += "%"

	BeginSql Alias cAliasSC2
		COLUMN C2_DATPRI AS DATE
		COLUMN C2_DATPRF AS DATE
		COLUMN C2_DATRF  AS DATE
		COLUMN C2_QUANT  AS Numeric(nSizeQtd, nDecQtd)
		COLUMN C2_QUJE   AS Numeric(nSizeQtd, nDecQtd)
		SELECT SC2.C2_FILIAL,
		       SC2.C2_NUM,
		       SC2.C2_ITEM,
		       SC2.C2_SEQUEN,
		       SC2.C2_ITEMGRD,
		       SC2.C2_SEQPAI,
		       SC2.C2_PRODUTO,
		       SC2.C2_LOCAL,
		       SC2.C2_QUANT,
		       SC2.C2_QUJE,
		       SC2.C2_PERDA,
		       SC2.C2_DATPRI,
		       SC2.C2_DATPRF,
		       SC2.C2_DATRF,
		       SC2.C2_TPOP,
		       SC2.C2_STATUS,
		       SC2.R_E_C_N_O_ AS RECNO,
		       SC2.C2_OPC,
		       SC2.C2_MOPC
		  FROM %Table:SC2% SC2
		 WHERE %Exp:cWhere%
	EndSql

	VarSetX(oExcPrevistos:cUIDStatus, "nLiberaSC2", @nInd, 1, 1)

	While !(cAliasSC2)->(Eof())
		oExcPrevistos:SetSaldos((cAliasSC2)->C2_PRODUTO, (cAliasSC2)->C2_LOCAL, , , , , (cAliasSC2)->C2_QUANT, 1)

		If lIntNewMRP
			If (cAliasSC2)->(FieldPos("C2_MOPC")) == 0
				SC2->(DbGoTo((cAliasSC2)->RECNO))
				A650AddInt(@aDadosDel, 0, "DELETE", "SC2")
			Else
				A650AddInt(@aDadosDel, 0, "DELETE", cAliasSC2)
			EndIf
		EndIf

		(cAliasSC2)->(dbSkip())
	EndDo
	(cAliasSC2)->(dbCloseArea())

	If lIntNewMRP .And. Len(aDadosDel) > 0
		//A integra��o das ordens � iniciada no pool de threads principal
		//para que n�o seja necess�rio aguardar o fim neste processo
		//da exclus�o das ordens.
		PCPIPCGO(oExcPrevistos:cThrJobs, .F., "A146IntOp" , oExcPrevistos:cTicket, aDadosDel)

		aSize(aDadosDel, 0)

	EndIf

Return Nil

/*/{Protheus.doc} A146IntOp
Executa a integra��o das ordens de produ��o com o MRP.

@type  Function
@author lucas.franca
@since 20/02/2020
@version P12.1.30
@param cTicket  , Character, N�mero do ticket do MRP em processamento
@param aDadosDel, Array    , array de dados formatados para MATA650INT
@return Nil
/*/
Function A146IntOp(cTicket, aDadosDel)
	Local lIntNewMRP := .F.
	Local oTemp      := Nil

	lIntNewMRP := Ma650MrpOn(@_lNewMRP, @oTemp)

	If oExcPrevistos == Nil
		oExcPrevistos := ExclusaoPrevistos():New(cTicket,.F.)
	EndIf

	If lIntNewMRP .AND. oExcPrevistos:AguardaInicioCalculoMRP()
		MATA650INT("DELETE", @aDadosDel, oTemp)
	EndIf

	aSize(aDadosDel, 0)
	oTemp:Delete()
	oTemp := Nil

Return Nil

/*/{Protheus.doc} A146SldSDC
Percorre as composi��es dos empenhos alimentando as vari�veis globais de saldo.

@type  Function
@author lucas.franca
@since 20/02/2020
@version P12.1.30
@param 01 - cTicket   , Character, N�mero do ticket do MRP em processamento
@param 02 - lHoriFirme, l�gico   , indica se considera o horizonte firme do produto
@param 03 - dDataIni  , data     , data de processamento do MRP utilizada como refer�ncia (database)
@return Nil
/*/
Function A146SldSDC(cTicket, lHoriFirme, dDataIni)
	Local cAliasSDC := GetNextAlias()
	Local cWhereIN  := ""
	Local cQuerySDC := ""
	Local nInd      := 1
	Local nSizeQtd  := GetSx3Cache("DC_QUANT","X3_TAMANHO")
	Local nDecQtd   := GetSx3Cache("DC_QUANT","X3_DECIMAL")

	If oExcPrevistos == Nil
		oExcPrevistos := ExclusaoPrevistos():New(cTicket,.F.)
	EndIf

	cQuerySDC := " SELECT SDC.DC_FILIAL,"
	cQuerySDC +=        " SDC.DC_PRODUTO,"
	cQuerySDC +=        " SDC.DC_LOCAL,"
	cQuerySDC +=        " SDC.DC_LOCALIZ,"
	cQuerySDC +=        " SDC.DC_NUMSERI,"
	cQuerySDC +=        " SDC.DC_LOTECTL,"
	cQuerySDC +=        " SDC.DC_NUMLOTE,"
	cQuerySDC +=        " SDC.DC_QUANT,"
	cQuerySDC +=        " SDC.R_E_C_N_O_"
	cQuerySDC +=   " FROM " + RetSqlName("SDC") + " SDC "
	cQuerySDC += " INNER JOIN " + RetSqlName("SC2") + " SC2 ON " + getWhereC2(.F., " ")
	cQuerySDC +=   " AND SDC.DC_OP = " + getC2OP()
	cQuerySDC +=  " WHERE SDC.DC_FILIAL  = '" + xFilial("SDC") + "'"
	cQuerySDC +=    " AND SDC.D_E_L_E_T_ = ' '"

	If lHoriFirme
		MrpDominio_HorizonteFirme():criaScriptIN(dDataIni, "SVK", "SC2", "C2_PRODUTO", "C2_DATPRI", getWhereC2(.T., " "), @cWhereIN)
		cQuerySDC   += " AND (SC2.R_E_C_N_O_ NOT IN ("
		cQuerySDC   += cWhereIN
		cQuerySDC   += "))"
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuerySDC),cAliasSDC,.F.,.F.)

	VarSetX(oExcPrevistos:cUIDStatus, "nLiberaSC2", @nInd, 1, 1)

	TcSetField(cAliasSDC, 'DC_QUANT', 'N', nSizeQtd, nDecQtd)

	Do While !(cAliasSDC)->(Eof())

		oExcPrevistos:SetSaldos( (cAliasSDC)->DC_PRODUTO,;
		                         (cAliasSDC)->DC_LOCAL,;
		                         (cAliasSDC)->DC_LOTECTL,;
		                         (cAliasSDC)->DC_NUMLOTE,;
		                         (cAliasSDC)->DC_LOCALIZ,;
		                         (cAliasSDC)->DC_NUMSERI,;
		                         (cAliasSDC)->DC_QUANT,;
		                         2)

		(cAliasSDC)->(dbSkip())
	EndDo
	(cAliasSDC)->(dbCloseArea())
Return Nil

/*/{Protheus.doc} A146AtuPMP
Atualiza os campos HC_OP e HC_STATUS das ordens que ser�o exclu�das.

@type  Function
@author lucas.franca
@since 20/02/2020
@version P12.1.30
@param 01 - cTicket   , Character, N�mero do ticket do MRP em processamento
@param 02 - lHoriFirme, l�gico   , indica se considera o horizonte firme do produto
@param 03 - dDataIni  , data     , data de processamento do MRP utilizada como refer�ncia (database)
@return Nil
/*/
Function A146AtuPMP(cTicket, lHoriFirme, dDataIni)
	Local cUpdate  := ""
	Local cWhereIN := ""
	Local nInd      := 1

	If oExcPrevistos == Nil
		oExcPrevistos := ExclusaoPrevistos():New(cTicket,.F.)
	EndIf

	cUpdate := " UPDATE " + RetSqlName("SHC")
	cUpdate +=    " SET HC_OP      = '" + Criavar("HC_OP",.F.) + "', "
	cUpdate +=        " HC_STATUS  = '" + Criavar("HC_STATUS",.F.) + "' "
	cUpdate +=  " WHERE HC_FILIAL  = '" + xFilial("SHC") + "'"
	cUpdate +=    " AND HC_OP IN (SELECT " + getC2OP()
	cUpdate +=                    " FROM " + RetSqlName("SC2") + " SC2 "
	cUpdate +=                   " WHERE " + getWhereC2(.F.," ")

	If lHoriFirme
		MrpDominio_HorizonteFirme():criaScriptIN(dDataIni, "SVK", "SC2", "C2_PRODUTO", "C2_DATPRI", getWhereC2(.T., " "), @cWhereIN)
		cUpdate   += " AND (R_E_C_N_O_ NOT IN ("
		cUpdate   += cWhereIN
		cUpdate   += "))"
	EndIf

	cUpdate +=                    ")"
	cUpdate +=    " AND D_E_L_E_T_ = ' '"

	If TcSqlExec(cUpdate) < 0
		Final("Erro ao atualizar os registros de Plano Mestre de Produ��o.", TcSqlError())
	EndIf

	VarSetX(oExcPrevistos:cUIDStatus, "nLiberaSC2", @nInd, 1, 1)

Return Nil

/*/{Protheus.doc} ExistC2Del
Monta Trecho de Query Exists Referente Registro Deletado na SC2 (QUE N�O EXISTE COMO N�O DELETADO)
@type  Function
@author brunno.costa
@since 22/07/2020
@version P12.1.27
@return cQuery, caracter, query referente trecho Exists C2 deletado
/*/
Static Function ExistC2Del(cCampo)
	Local cQuery := ""
	cQuery += " EXISTS ( SELECT 1 "
	cQuery +=          " FROM "
	cQuery +=               " (SELECT EXCLUIDO.OP "
	cQuery +=                " FROM (SELECT " + getC2OP() + " OP "
	cQuery +=                      " FROM " + RetSqlName("SC2") + " SC2 "
	cQuery +=                      " WHERE (C2_FILIAL = '" + xFilial("SC2") + "') "
	cQuery +=                             " AND (D_E_L_E_T_ = ' ')) EXISTENTE "
	cQuery +=                " RIGHT OUTER JOIN "
	cQuery +=                          "(SELECT " + getC2OP() + " OP "
	cQuery +=                           " FROM " + RetSqlName("SC2") + " SC2 "
	cQuery +=                           " WHERE (C2_FILIAL = '" + xFilial("SC2") + "') "
	cQuery +=                             " AND (D_E_L_E_T_ = '*')) EXCLUIDO "
	cQuery +=                " ON EXISTENTE.OP = EXCLUIDO.OP "
	cQuery +=                " WHERE (EXISTENTE.OP IS NULL)) SC2 "
	cQuery +=           " WHERE " + cCampo + " = SC2.OP) "
Return cQuery

/*/{Protheus.doc} A146DelSD4
Apaga os registros de Empenhos Previstos

@type  Function
@author lucas.franca
@since 20/02/2020
@version P12.1.30
@return Nil
/*/
Function A146DelSD4()
	Local cUpdDel  := ""

	cUpdDel := " UPDATE " + RetSqlName("SD4")
	cUpdDel +=    " SET D_E_L_E_T_   = '*' "
	If !Empty(FWX2Unico( 'SD4'))
		cUpdDel +=  " , R_E_C_D_E_L_ = R_E_C_N_O_ "
	EndIf
	cUpdDel +=  " WHERE D4_FILIAL  = '" + xFilial("SD4") + "' "
	cUpdDel +=    " AND D_E_L_E_T_ = ' ' "
	cUpdDel +=    " AND " + ExistC2Del("D4_OP")

	If TcSqlExec(cUpdDel) < 0
		Final("Erro ao excluir os registros de Empenhos.", TcSqlError())
	EndIf

Return

/*/{Protheus.doc} A146DelSDC
Apaga os registros de Composi��es de Empenhos Previstos

@type  Function
@author lucas.franca
@since 20/02/2020
@version P12.1.30
@return Nil
/*/
Function A146DelSDC()
	Local cUpdDel  := ""

	cUpdDel := " UPDATE " + RetSqlName("SDC")
	cUpdDel +=    " SET D_E_L_E_T_   = '*' "
	If !Empty(FWX2Unico( 'SDC'))
		cUpdDel +=  " , R_E_C_D_E_L_ = R_E_C_N_O_ "
	EndIf
	cUpdDel +=  " WHERE DC_FILIAL  = '" + xFilial("SDC") + "' "
	cUpdDel +=    " AND D_E_L_E_T_ = ' ' "
	cUpdDel +=    " AND " + ExistC2Del("DC_OP")

	If TcSqlExec(cUpdDel) < 0
		Final("Erro ao excluir os registros de Composi��o de Empenhos.", TcSqlError())
	EndIf
Return

/*/{Protheus.doc} A146DelOS
Apaga os registros de Ordens de Substitui��o

@type  Function
@author lucas.franca
@since 20/02/2020
@version P12.1.30
@return Nil
/*/
Function A146DelOS()
	Local cUpdDel  := ""

	If SuperGetMV("MV_PCPOS",.F.,.F.)
		cUpdDel := " UPDATE " + RetSqlName("SVJ")
		cUpdDel +=    " SET D_E_L_E_T_   = '*' "
		If !Empty(FWX2Unico( 'SVJ'))
			cUpdDel +=  " , R_E_C_D_E_L_ = R_E_C_N_O_ "
		EndIf
		cUpdDel +=  " WHERE VJ_FILIAL  = '" + xFilial("SVJ") + "' "
		cUpdDel +=    " AND D_E_L_E_T_ = ' ' "
		cUpdDel +=    " AND VJ_NUM     IN (SELECT SVF.VF_NUM "
		cUpdDel +=                         " FROM " + RetSqlName("SVF") + " SVF "
		cUpdDel +=                        " WHERE SVF.VF_FILIAL  = '" + xFilial("SVF") + "' "
		cUpdDel +=                          " AND SVF.D_E_L_E_T_ = ' ' "
		cUpdDel +=                          " AND " + ExistC2Del("SVF.VF_OP") + " ) "

		If TcSqlExec(cUpdDel) < 0
			Final("Erro ao excluir os registros de Ordens de substitui��o (Detalhe).", TcSqlError())
		EndIf

		cUpdDel := " UPDATE " + RetSqlName("T4I")
		cUpdDel +=    " SET D_E_L_E_T_   = '*' "
		If !Empty(FWX2Unico( 'T4I'))
			cUpdDel +=  " , R_E_C_D_E_L_ = R_E_C_N_O_ "
		EndIf
		cUpdDel +=  " WHERE T4I_FILIAL  = '" + xFilial("T4I") + "' "
		cUpdDel +=    " AND D_E_L_E_T_  = ' ' "
		cUpdDel +=    " AND T4I_NUM     IN (SELECT SVF.VF_NUM "
		cUpdDel +=                         " FROM " + RetSqlName("SVF") + " SVF "
		cUpdDel +=                        " WHERE SVF.VF_FILIAL  = '" + xFilial("SVF") + "' "
		cUpdDel +=                          " AND SVF.D_E_L_E_T_ = ' ' "
		cUpdDel +=                          " AND " + ExistC2Del("SVF.VF_OP") + " ) "

		If TcSqlExec(cUpdDel) < 0
			Final("Erro ao excluir os registros de Ordens de substitui��o (Endere�os).", TcSqlError())
		EndIf

		cUpdDel := " UPDATE " + RetSqlName("SVF")
		cUpdDel +=    " SET D_E_L_E_T_   = '*' "
		If !Empty(FWX2Unico( 'SVF'))
			cUpdDel +=  " , R_E_C_D_E_L_ = R_E_C_N_O_ "
		EndIf
		cUpdDel +=  " WHERE VF_FILIAL  = '" + xFilial("SVF") + "' "
		cUpdDel +=    " AND D_E_L_E_T_ = ' ' "
		cUpdDel +=    " AND " + ExistC2Del("VF_OP")

		If TcSqlExec(cUpdDel) < 0
			Final("Erro ao excluir os registros de Ordens de substitui��o (Mestre).", TcSqlError())
		EndIf
	EndIf
Return Nil

/*/{Protheus.doc} A146DelSHY
Apaga os registros de Opera��es da Ordem

@type  Function
@author lucas.franca
@since 20/02/2020
@version P12.1.30
@return Nil
/*/
Function A146DelSHY()
	Local cUpdDel := ""

	cUpdDel := " UPDATE " + RetSqlName("SHY")
	cUpdDel +=    " SET D_E_L_E_T_   = '*' "
	If !Empty(FWX2Unico( 'SHY'))
		cUpdDel +=  " , R_E_C_D_E_L_ = R_E_C_N_O_ "
	EndIf
	cUpdDel +=  " WHERE HY_FILIAL  = '" + xFilial("SHY") + "' "
	cUpdDel +=    " AND D_E_L_E_T_ = ' ' "
	cUpdDel +=    " AND " + ExistC2Del("HY_OP")

	If TcSqlExec(cUpdDel) < 0
		Final("Erro ao excluir os registros de Opera��es da Ordem de Produ��o.", TcSqlError())
	EndIf
Return Nil

/*/{Protheus.doc} ExcluiSolicitacoesCompra
Exclui os registros da tabela SC1

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
/*/
METHOD ExcluiSolicitacoesCompra() CLASS ExclusaoPrevistos
	Local cAliasSC1 := GetNextAlias()
	Local cUpdDel   := ""
	Local cFilSC1   := xFilial("SC1")
	Local cQuerySC1 := ""
	Local lAprovEt  := SuperGetMV("MV_APRSCEC",.F.,.T.) // Alcada por Entidade Contabil
	Local lMrpDel   := SuperGetMV("MV_MRPDEL",.F.,.F.)
	Local nSizeQtd  := GetSx3Cache("C1_QUANT","X3_TAMANHO")
	Local nDecQtd   := GetSx3Cache("C1_QUANT","X3_DECIMAL")
	Local cWhereIN  := ""
	Local cWhere    := ""

	Local oJsonAlc  := JsonObject():New()
	Local oJsonDhn  := JsonObject():New()

	cQuerySC1 := " SELECT SC1.C1_PRODUTO,"                    + ;
						" SC1.C1_LOCAL,"                      + ;
	                    " SC1.C1_NUM,"                        + ;
						" SC1.C1_QUANT,"                      + ;
						" SC1.R_E_C_N_O_ AS RECNO,"           + ;
						" SC1.C1_OP"                          + ;
	               " FROM " + RetSqlName("SC1") + " SC1"      + ;
	              " WHERE "

	cWhere    := " SC1.D_E_L_E_T_ = ' ' AND " + ;
	             " SC1.C1_FILIAL  = '" + cFilSC1 + "'" + ;
	             " AND SC1.C1_TPOP    = 'P'"

	If lMrpDel
		cWhere += " AND SC1.C1_SEQMRP <> '" + CriaVar("C1_SEQMRP") + "'"
	EndIf

	cQuerySC1 += cWhere

	If Self:lHoriFirme
		MrpDominio_HorizonteFirme():criaScriptIN(Self:dDataIni, "SVK", "SC1", "C1_PRODUTO", "C1_DATPRF", cWhere, @cWhereIN)
		cQuerySC1   += " AND (R_E_C_N_O_ NOT IN ("
		cQuerySC1   += cWhereIN
		cQuerySC1   += "))"
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuerySC1),cAliasSC1,.T.,.T.)

	TcSetField(cAliasSC1, 'C1_QUANT', 'N', nSizeQtd, nDecQtd)

	Do While !(cAliasSC1)->(Eof())
		::SetSaldos((cAliasSC1)->C1_PRODUTO, (cAliasSC1)->C1_LOCAL,,,,, (cAliasSC1)->C1_QUANT, 1)
		(cAliasSC1)->(dbSkip())
	EndDo

	If Self:AguardaInicioCalculoMRP()

		If lAprovEt .OR. Self:lExistDHN1
			(cAliasSC1)->(DbGoTop())
			Do While !(cAliasSC1)->(Eof())

				If lAprovEt .And. !Empty((cAliasSC1)->(C1_OP))
					If oJsonAlc[(cAliasSC1)->(C1_NUM)] == Nil
						oJsonAlc[(cAliasSC1)->(C1_NUM)] := .T.
						A650ALCCTB((cAliasSC1)->(C1_OP), (cAliasSC1)->(RECNO))
					EndIf
				EndIf

				If Self:lExistDHN1
					If oJsonDhn[(cAliasSC1)->(C1_NUM)] == Nil
						oJsonDhn[(cAliasSC1)->(C1_NUM)] := .T.
						COMEXCDHN("1", cFilSC1, (cAliasSC1)->(C1_NUM))
					EndIf
				EndIf

				(cAliasSC1)->(dbSkip())
			EndDo
		EndIf

		cUpdDel := " UPDATE " + RetSqlName("SC1")
		cUpdDel +=    " SET D_E_L_E_T_   = '*' "
		If !Empty(FWX2Unico( 'SC1'))
			cUpdDel +=  " , R_E_C_D_E_L_ = R_E_C_N_O_ "
		EndIf
		cUpdDel +=  " WHERE D_E_L_E_T_ = ' ' AND "
		cUpdDel +=   " C1_FILIAL  = '" + xFilial("SC1") + "' "
		cUpdDel +=   " AND C1_TPOP    = 'P' "

		If lMrpDel
			cUpdDel += " AND C1_SEQMRP <> '" + CriaVar("C1_SEQMRP") + "'"
		EndIf

		If Self:lHoriFirme
			cUpdDel   += " AND (R_E_C_N_O_ NOT IN ("
			cUpdDel   += cWhereIN
			cUpdDel   += "))"
		EndIf

		If TcSqlExec(cUpdDel) < 0
			Final("Erro ao excluir os registros de Solicita��o de compras.", TcSqlError())
		EndIf

	EndIf
	(cAliasSC1)->(dbCloseArea())

Return

/*/{Protheus.doc} ExisteDHNValido
Verifica se existem registros na DHN para exclus�o

@type  Method
@author brunno.costa
@since 18/05/2020
@version P12.1.27
@param 01 - cTipo  , caracter, tipo do documento
@lReturn, l�gico, indica se existem registros da DHN para exclus�o
/*/

METHOD ExisteDHNValido(cTipo) CLASS ExclusaoPrevistos

	Local cAliasDHN := GetNextAlias()
	Local cQueryDHN
	Local lReturn   := .F.
	Local lRetAux   := VarGetXD(Self:cUIDSB1Cnv, "ExisteDHNValido" + cTipo, @lReturn)

	If !lRetAux
		If cTipo == "1"
			cQueryDHN :=  " SELECT COUNT(*) QTD "  + ;
						" FROM " + RetSqlName("DHN") + " DHN JOIN " + ;
								RetSqlName("SC1") + " SC1 " + ;
								" ON DHN.DHN_DOCDES = SC1.C1_NUM " + ;
						" WHERE   (DHN.D_E_L_E_T_ = ' ') " + ;
							" AND (SC1.D_E_L_E_T_ = ' ') " + ;
							" AND (SC1.C1_TPOP = 'P') " + ;
							" AND (SC1.C1_RESIDUO = ' ') " + ;
							" AND ((SC1.C1_QUANT - SC1.C1_QUJE) > 0) " + ;
							" AND (DHN_FILDES = '" + xFilial("DHN") + "' OR DHN_FILDES = ' ') " + ;
							" AND (DHN_FILIAL = '" + xFilial("DHN") + "') " + ;
							" AND (DHN_TIPO = '" + cTipo + "') "
		ElseIf cTipo == "3"
			cQueryDHN :=  " SELECT COUNT(*) QTD "  + ;
						" FROM " + RetSqlName("DHN") + " DHN JOIN " + ;
								RetSqlName("SC7") + " SC7 " + ;
								" ON DHN.DHN_DOCDES = SC7.C7_NUM " +;
						" WHERE   (DHN.D_E_L_E_T_ = ' ') " + ;
							" AND (SC7.D_E_L_E_T_ = ' ') " + ;
							" AND (SC7.C7_TPOP = 'P') " + ;
							" AND (SC7.C7_RESIDUO = ' ') " + ;
							" AND ((SC7.C7_QUANT - SC7.C7_QUJE) > 0) " + ;
							" AND (DHN_FILDES = '" + xFilial("DHN") + "' OR DHN_FILDES = ' ') " + ;
							" AND (DHN_FILIAL = '" + xFilial("DHN") + "') " + ;
							" AND (DHN_TIPO = '" + cTipo + "') "
		EndIf


		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQueryDHN),cAliasDHN,.T.,.T.)
		If !(cAliasDHN)->(Eof())
			lReturn := ((cAliasDHN)->QTD > 0)
		EndIf
		(cAliasDHN)->(dbCloseArea())
		VarSetXD(Self:cUIDSB1Cnv, "ExisteDHNValido" + cTipo, lReturn)
	EndIf

Return lReturn

/*/{Protheus.doc} ExisteAutorizacaoEntrega
Verifica se existem registros de autoriza��o de entrega

@type  Method
@author brunno.costa
@since 18/05/2020
@version P12.1.27
@lReturn, l�gico, indica se existem registros da SC3 para exclus�o
/*/

METHOD ExisteAutorizacaoEntrega() CLASS ExclusaoPrevistos

	Local cAliasSC3 := GetNextAlias()
	Local cQuerySC3
	Local lReturn   := .F.
	Local lRetAux   := VarGetXD(Self:cUIDSB1Cnv, "ExisteAutorizacaoEntrega", @lReturn)

	If !lRetAux
		cQuerySC3 :=  " SELECT COUNT(*) QTD "  + ;
						" FROM " + RetSqlName("SC3") + " SC3 JOIN " + ;
							RetSqlName("SC1") + " SC1 " + ;
							" ON SC3.C3_NUM = SC1.C1_NUM " +;
							" AND SC3.C3_ITEM = SC1.C1_ITEM " +;
						" WHERE    (SC3.D_E_L_E_T_ = ' ') " + ;
							" AND (SC3.C3_FILIAL = '" + xFilial("SC3") + "') " + ;
							" AND (SC1.D_E_L_E_T_ = ' ') " + ;
							" AND (SC1.C1_TPOP = 'P') " + ;
							" AND (SC1.C1_RESIDUO = ' ') " + ;
							" AND (SC1.C1_FILIAL = '" + xFilial("SC1") + "') " + ;
							" AND ((SC1.C1_QUANT - SC1.C1_QUJE) > 0) " +;
							" AND ((SC3.C3_QUANT - SC3.C3_QUJE) > 0) "

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuerySC3),cAliasSC3,.T.,.T.)
		If !(cAliasSC3)->(Eof())
			lReturn := ((cAliasSC3)->QTD > 0)
		EndIf
		(cAliasSC3)->(dbCloseArea())
		VarSetXD(Self:cUIDSB1Cnv, "ExisteAutorizacaoEntrega", lReturn)
	EndIf

Return lReturn

/*/{Protheus.doc} cargaSB1Conversao
Carrega Fator Multiplicativo de Convers�o da Quantidade

@type  Function
@author brunno.costa
@since 19/05/2020
@version P12.1.30
@return Nil
/*/
METHOD cargaSB1Conversao() CLASS ExclusaoPrevistos
	Local cAliasSB1 := GetNextAlias()
	Local cProduto
	Local cQuerySB1 := ""

	cQuerySB1 := " SELECT        B1_COD, (CASE B1_TIPCONV WHEN 'D' THEN ROUND(1 / B1_CONV, 8) ELSE B1_CONV END) AS B1_CONV "
	cQuerySB1 += " FROM " + RetSqlName("SB1")
	cQuerySB1 += " WHERE        (D_E_L_E_T_ = ' ') AND (B1_CONV <> 0) AND B1_FILIAL  = '" + xFilial("SB1") + "'"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuerySB1),cAliasSB1,.F.,.F.)

	Do While !(cAliasSB1)->(Eof())
		cProduto := PadR((cAliasSB1)->B1_COD, snTamCod)
		VarSetXD(Self:cUIDSB1Cnv, cProduto, (cAliasSB1)->B1_CONV)
		(cAliasSB1)->(dbSkip())
	EndDo
	(cAliasSB1)->(dbCloseArea())

Return Nil

/*/{Protheus.doc} ConverteUM
Converte Na Segunda Unidade de Medida

@type  Function
@author brunno.costa
@since 19/05/2020
@version P12.1.30
@param 01 - cProduto, caracter, c�digo do  produto
@param 02 - nQtd    , n�mero  , quantidade na primeira unidade de medida
@return nQtd2aUM, n�mero, quantidade na segunda unidade de medida
/*/
METHOD converteUM(cProduto, nQtd) CLASS ExclusaoPrevistos
	Local lRet      := .F.
	Local nQtd2aUM  := 0
	Local nFator    := 0

	lRet := VarGetXD(Self:cUIDSB1Cnv, cProduto, @nFator)
	If lRet
		nQtd2aUM := Round(nQtd * nFator, Seld:nDecimal)
	EndIf

Return nQtd2aUM

/*/{Protheus.doc} ExcluiPedidosCompra
Exclui os registros da tabela SC7

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
/*/
METHOD ExcluiPedidosCompra() CLASS ExclusaoPrevistos
	Local cAliasSC7 := GetNextAlias()
	Local cFilSC7   := xFilial("SC7")
	Local cQuerySC7 := ""
	Local cUpdDel   := ""
	Local lMrpDel   := SuperGetMV("MV_MRPDEL",.F.,.F.)
	Local nSizeQtd  := GetSx3Cache("C7_QUANT","X3_TAMANHO")
	Local nDecQtd   := GetSx3Cache("C7_QUANT","X3_DECIMAL")
	Local cWhereIN  := ""
	Local cWhere    := ""

	cQuerySC7 := " SELECT SC7.C7_FILIAL,"          + ;
	                    " SC7.C7_PRODUTO,"         + ;
						" SC7.C7_LOCAL,"           + ;
	                    " SC7.C7_NUM,"             + ;
						" SC7.C7_NUMSC,"           + ;
						" SC7.C7_ITEMSC,"          + ;
	                    " SC7.C7_QUANT,"           + ;
						" SC7.R_E_C_N_O_ AS RECNO"            + ;
	               " FROM " + RetSqlName("SC7") + " SC7"      + ;
	              " WHERE SC7.C7_FILIAL  = '" + cFilSC7 + "'" + ;
	                " AND SC7.C7_TPOP    = 'P'"               + ;
	                " AND SC7.C7_TIPO    = 2"                 + ;
	                " AND SC7.D_E_L_E_T_ = ' '"

	If lMrpDel
		cQuerySC7 += " AND SC7.C7_SEQMRP <> '" + CriaVar("C7_SEQMRP") + "'"
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuerySC7),cAliasSC7,.T.,.T.)

	TcSetField(cAliasSC7, 'C7_QUANT', 'N', nSizeQtd, nDecQtd)

	While !(cAliasSC7)->(Eof())
		::SetSaldos((cAliasSC7)->C7_PRODUTO, (cAliasSC7)->C7_LOCAL,,,,, (cAliasSC7)->C7_QUANT, 1)
		(cAliasSC7)->(dbSkip())
	EndDo

	If Self:AguardaInicioCalculoMRP()

		If Self:lExistAE .OR. Self:lExistDHN3
			(cAliasSC7)->(DbGoTop())
			While !(cAliasSC7)->(Eof())

				If Self:lExistAE
					::AtualizaContratoParceria((cAliasSC7)->C7_NUMSC, (cAliasSC7)->C7_ITEMSC, (cAliasSC7)->C7_QUANT	)
				EndIf

				If Self:lExistDHN3
					COMEXCDHN("3", cFilSC7, (cAliasSC7)->C7_NUM)
				EndIf

				(cAliasSC7)->(dbSkip())
			EndDo
		EndIf

		cUpdDel := " UPDATE " + RetSqlName("SC7")
		cUpdDel +=    " SET D_E_L_E_T_   = '*' "
		If !Empty(FWX2Unico( 'SC7'))
			cUpdDel +=  " , R_E_C_D_E_L_ = R_E_C_N_O_ "
		EndIf
		cUpdDel +=  " WHERE D_E_L_E_T_ = ' ' AND "

		cWhere +=    " C7_FILIAL  = '" + cFilSC7 + "'"
		cWhere +=    " AND C7_TPOP    = 'P'"
		cWhere +=    " AND C7_TIPO    = 2 "

		If lMrpDel
			cWhere += " AND C7_SEQMRP <> '" + CriaVar("C7_SEQMRP") + "'"
		EndIf

		cUpdDel += cWhere

		If Self:lHoriFirme
			MrpDominio_HorizonteFirme():criaScriptIN(Self:dDataIni, "SVK", "SC7", "C7_PRODUTO", "C7_DATPRF", cWhere, @cWhereIN)
			cUpdDel   += " AND (R_E_C_N_O_ NOT IN ("
			cUpdDel   += cWhereIN
			cUpdDel   += "))"
		EndIf

		If TcSqlExec(cUpdDel) < 0
			Final("Erro ao excluir os registros de Pedidos de compras.", TcSqlError())
		EndIf
	EndIf

	(cAliasSC7)->(dbCloseArea())

Return

/*/{Protheus.doc} AtualizaContratoParceria
Subtrai a quantidade da SC da quantidade produzida do contrato de parceria.

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
@param 01 cNumSc , Character, N�mero da solicita��o de compra.
@param 02 cItemSc, Character, Item da solicita��o de compra.
@param 03 nQuant , Character, Quantidade. da solicita��o de compra.
/*/
METHOD AtualizaContratoParceria(cNumSc, cItemSc, nQuant) CLASS ExclusaoPrevistos

	dbSelectArea("SC3")
	SC3->(dbSetOrder(1))
	If SC3->(dbSeek(xFilial("SC3")+cNumSc+cItemSc))
		RecLock("SC3",.F.)
		Replace C3_QUJE	With SC3->C3_QUJE - nQuant
		SC3->(MsUnlock())
	EndIf
Return

/*/{Protheus.doc} SetSaldos
Atualiza a tabela de mem�ria para controle dos saldos, adicionando a quantidade recebida em nQtd.
Ser� atualizada a quantidade da tabela de mem�ria de acordo com o par�metro nTipo.

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
@param cProduto, Character, C�digo do produto que ser� atualizado.
@param cLocal  , Character, C�digo do local de estoque.
@param cLoteCtl, Character, C�digo do lote.
@param cNumLote, Character, C�digo do sublote.
@param cLocaliz, Character, C�digo do endere�o.
@param cNumseri, Character, C�digo do n�mero de s�rie.
@param nQtd    , Numeric  , Quantidade.
@param nTipo   , Numeric  , Identifica qual � a quantidade que ser� atualizada.
                            1 - Entrada (B2_SALPPRE);
                            2 - Sa�da   (B2_QEMPPRE)
/*/
METHOD SetSaldos(cProduto, cLocal, cLoteCtl, cNumLote, cLocaliz, cNumseri, nQtd, nTipo) CLASS ExclusaoPrevistos
	Local aDados    := {}
	Local cChaveSld := ""
	Local cChaveLte := ""
	Local cChaveLcz := ""
	Local lRet      := .T.

	Default cLoteCtl := ""
	Default cNumLote := ""
	Default cLocaliz := ""
	Default cNumSeri := ""

    cChaveSld := cProduto + cLocal + CHR(13) + "SLD"
    cChaveLte := cProduto + cLocal + cLoteCtl + cNumLote + CHR(13) + "LTE"
    cChaveLcz := cProduto + cLocal + cLoteCtl + cNumLote + cLocaliz + cNumseri + CHR(13) + "LCZ"

	If Empty(cLocaliz)
		//Inicia transa��o saldo simples
		VarBeginT(Self:cUIDSld, cChaveSld)

		//Recupera os estoques deste produto
		aDados := Self:GetSaldo(cProduto, cLocal, @lRet)

		If !lRet
			aDados := Array(SALDOS_QTD_TAMANHO)
			aDados[SALDOS_QTD_POS_ENTRADA_PREV ] := 0
			aDados[SALDOS_QTD_POS_SAIDA_PREV   ] := 0
		EndIf

		If nTipo == 1
			aDados[SALDOS_QTD_POS_ENTRADA_PREV ] += nQtd
		ElseIf nTipo == 2
			aDados[SALDOS_QTD_POS_SAIDA_PREV   ] += nQtd
		EndIf

		//Seta os dados na tabela de mem�ria
		lRet := VarSetAD(Self:cUIDSld, cChaveSld, aDados )
		If !lRet
			LogMsg("PCPA146", 0, 0, 1, "", "", "Erro ao atualizar a tabela de saldos do produto. Produto:" + AllTrim(cProduto) + ". Local:" + AllTrim(cLocal) + ".")
		EndIf

		//Finaliza a transa��o saldo simples
		VarEndT(Self:cUIDSld, cChaveSld)

		If !Empty(cLoteCtl)

			aSize(aDados,0)

			//Inicia transa��o saldo com lote/sublote
			VarBeginT(Self:cUIDSldLte, cChaveLte)

			//Recupera os estoques deste produto
			aDados := Self:GetLoteSaldo(cProduto, cLocal, cLoteCtl, cNumLote, @lRet)

			If !lRet
				aDados := Array(SALDOS_QTD_TAMANHO)
				aDados[SALDOS_QTD_POS_ENTRADA_PREV ] := 0
				aDados[SALDOS_QTD_POS_SAIDA_PREV   ] := 0
			EndIf

			If nTipo == 1
				aDados[SALDOS_QTD_POS_ENTRADA_PREV ] += nQtd
			ElseIf nTipo == 2
				aDados[SALDOS_QTD_POS_SAIDA_PREV   ] += nQtd
			EndIf

			//Seta os dados na tabela de mem�ria
			lRet := VarSetAD(Self:cUIDSldLte, cChaveLte, aDados )
			If !lRet
				LogMsg("PCPA146", 0, 0, 1, "", "", "Erro ao atualizar a tabela de saldos/lote do produto. Produto:" + AllTrim(cProduto) + ". Local:" + AllTrim(cLocal) + ".")
			EndIf

			//Finaliza a transa��o saldo com lote/sublote
			VarEndT(Self:cUIDSldLte, cChaveLte)
		EndIf

	Else
		//Inicia transa��o saldo com endere�o
		VarBeginT(Self:cUIDSldLcz, cChaveLcz)

		//Recupera os estoques deste produto
		aDados := Self:GetLocalizaSaldo(cProduto, cLocal, cLoteCtl, cNumLote, cLocaliz, cNumseri, @lRet)

		If !lRet
			aDados := Array(SALDOS_QTD_TAMANHO)
			aDados[SALDOS_QTD_POS_ENTRADA_PREV ] := 0
			aDados[SALDOS_QTD_POS_SAIDA_PREV   ] := 0
		EndIf

		If nTipo == 1
			aDados[SALDOS_QTD_POS_ENTRADA_PREV ] += nQtd
		ElseIf nTipo == 2
			aDados[SALDOS_QTD_POS_SAIDA_PREV   ] += nQtd
		EndIf

		//Seta os dados na tabela de mem�ria
		lRet := VarSetAD(Self:cUIDSldLcz, cChaveLcz, aDados )
		If !lRet
			LogMsg("PCPA146", 0, 0, 1, "", "", "Erro ao atualizar a tabela de saldos/localiza��o do produto. Produto:" + AllTrim(cProduto) + ". Local:" + AllTrim(cLocal) + ".")
		EndIf

		//Finaliza a transa��o saldo com endere�o
		VarEndT(Self:cUIDSldLcz, cChaveLcz)
	EndIf

Return

/*/{Protheus.doc} GetSaldo
Recupera os saldos de determinado produto e local

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
@param cProduto, Character, C�digo do produto.
@param cLocal  , Character, C�digo do local.
@param lRet    , Logico   , Retorna por refer�ncia se houve erro ao recuperar os dados.
@return aSaldos, Array  , Array com os saldos dos produtos - [1] Saldo Previsto [2] Empenho Previsto.
/*/
METHOD GetSaldo(cProduto, cLocal, lRet) CLASS ExclusaoPrevistos
	Local aSaldos   := {}
	Local cChaveSld := cProduto + cLocal + CHR(13) + "SLD"

	//Recupera a tabela de saldos do produto
	lRet := VarGetAD(Self:cUIDSld, cChaveSld, @aSaldos )
Return aSaldos

/*/{Protheus.doc} GetLoteSaldo
Recupera os saldos de determinado produto,local,lote e sublote.

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
@param cProduto, Character, C�digo do produto.
@param cLocal  , Character, C�digo do local.
@param cLoteCtl, Character, C�digo do lote.
@param cNumlote, Character, C�digo do sublote.
@param lRet    , Logico   , Retorna por refer�ncia se houve erro ao recuperar os dados.
@return aSaldos, Array  , Array com os saldos dos produtos - [1] Saldo Previsto [2] Empenho Previsto.
/*/
METHOD GetLoteSaldo(cProduto, cLocal, cLoteCtl, cNumLote, lRet) CLASS ExclusaoPrevistos
	Local aSaldos   := {}
	Local cChaveLte := cProduto + cLocal + cLoteCtl + cNumLote + CHR(13) + "LTE"

	//Recupera a tabela de saldos do produto
	lRet := VarGetAD(Self:cUIDSldLte, cChaveLte, @aSaldos )
Return aSaldos

/*/{Protheus.doc} GetLocalizaSaldo
Recupera os saldos de determinado produto,local,lote,sublote e localiza��o.

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
@param cProduto, Character, C�digo do produto.
@param cLocal  , Character, C�digo do local.
@param cLoteCtl, Character, C�digo do lote.
@param cNumlote, Character, C�digo do sublote.
@param cLocaliz, Character, C�digo do endere�o.
@param cNumSeri, Character, C�digo do n�mero de s�rie.
@param lRet    , Logico   , Retorna por refer�ncia se houve erro ao recuperar os dados.
@return aSaldos, Array  , Array com os saldos dos produtos - [1] Saldo Previsto [2] Empenho Previsto.
/*/
METHOD GetLocalizaSaldo(cProduto, cLocal, cLoteCtl, cNumLote, cLocaliz, cNumSeri, lRet) CLASS ExclusaoPrevistos
	Local aSaldos   := {}
	Local cChaveLcz := cProduto + cLocal + cLoteCtl + cNumLote + cLocaliz + cNumSeri + CHR(13) + "LCZ"

	//Recupera a tabela de saldos do produto
	lRet := VarGetAD(Self:cUIDSldLcz, cChaveLcz, @aSaldos )
Return aSaldos

/*/{Protheus.doc} PersisteSaldos
Grava os dados em mem�ria nas tabelas SB2,SB8,SBF.

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
/*/
METHOD PersisteSaldos() CLASS ExclusaoPrevistos
	Local lError    := .F.
	Local aDados    := {}
	Local nX        := 0
	Local cFilSB2   := xFilial("SB2")
	Local cProduto  := ""
	Local cLocal    := ""
	Local cLoteCtl  := ""
	Local cNumLote  := ""
	Local cLocaliz  := ""
	Local cNumSeri  := ""
	Local nTamPrd   := GetSx3Cache("B1_COD","X3_TAMANHO")
	Local nTamLoc   := GetSx3Cache("B1_LOCPAD","X3_TAMANHO")
	Local nTamLot   := GetSx3Cache("D4_LOTECTL","X3_TAMANHO")
	Local nTamNLt   := GetSx3Cache("D4_NUMLOTE","X3_TAMANHO")
	Local nTamLcz   := GetSx3Cache("DC_LOCALIZ","X3_TAMANHO")
	Local nTamNSr   := GetSx3Cache("DC_NUMSERI","X3_TAMANHO")
	Local nQtEmp    := 0
	Local nQtSal    := 0
	Local nQtdSegUM := 0
	Local nTotDados := 0

	lError  := !VarGetAA( Self:cUIDSld, @aDados)
	If !lError
		nTotDados := Len(aDados)
	Else
		nTotDados := 0
		LogMsg("PCPA146", 0, 0, 1, "", "", "Erro ao persistir saldo na tabela SB2")
	EndIf

	For nX := 1 To nTotDados

		cProduto := SubStr(aDados[nX][1],1,nTamPrd)
		cLocal   := SubStr(aDados[nX][1],nTamPrd+1,nTamLoc)
		nQtSal   := aDados[nX][2][1]
		nQtEmp   := aDados[nX][2][2]

		dbSelectArea("SB2")
		dbSetOrder(1)
		If !dbSeek(cFilSB2+cProduto+cLocal)
			CriaSB2(cProduto,cLocal)
			SB2->(MsUnlock())
		EndIf

		If nQtSal > 0
			nQtdSegUM := Self:ConverteUM(cProduto, nQtSal)
			GravaB2Pre("-", nQtSal, "P", nQtdSegUM, .T.)
		EndIf

		If nQtEmp > 0
			nQtdSegUM := Self:ConverteUM(cProduto, nQtEmp)
			GravaB2Emp("-", nQtEmp, "P", .F., nQtdSegUM)
		EndIf

	Next nX

	aSize(aDados,0)

	lError  := !VarGetAA( Self:cUIDSldLte, @aDados)
	If !lError
		nTotDados := Len(aDados)
	Else
		nTotDados := 0
		LogMsg("PCPA146", 0, 0, 1, "", "", "Erro ao persistir saldo na tabela SB8")
	EndIf

	For nX := 1 To nTotDados

		cProduto := SubStr(aDados[nX][1],1,nTamPrd)
		cLocal   := SubStr(aDados[nX][1],nTamPrd+1,nTamLoc)
		cLoteCtl := SubStr(aDados[nX][1],nTamPrd+nTamLoc+1,nTamLot)
		cNumLote := SubStr(aDados[nX][1],nTamPrd+nTamLoc+nTamLot+1,nTamNLt)
		nQtEmp   := aDados[nX][2][2]

		dbSelectArea("SB8")
		dbSetOrder(2)
		If dbSeek(xFilial("SB8")+cNumLote+cLoteCtl+cProduto+cLocal)
			nQtdSegUM := Self:ConverteUM(cProduto, nQtEmp)
			GravaB8Emp("-",nQtEmp,"P",.F.,nQtdSegUM)
		EndIf

	Next nX

	aSize(aDados,0)

	lError  := !VarGetAA( Self:cUIDSldLcz, @aDados)
	If !lError
		nTotDados := Len(aDados)
	Else
		nTotDados := 0
		LogMsg("PCPA146", 0, 0, 1, "", "", "Erro ao persistir saldo na tabela SBF")
	EndIf

	For nX := 1 To Len(aDados)

		cProduto := SubStr(aDados[nX][1],1,nTamPrd)
		cLocal   := SubStr(aDados[nX][1],nTamPrd+1,nTamLoc)
		cLoteCtl := SubStr(aDados[nX][1],nTamPrd+nTamLoc+1,nTamLot)
		cNumLote := SubStr(aDados[nX][1],nTamPrd+nTamLoc+nTamLot+1,nTamNLt)
		cLocaliz := SubStr(aDados[nX][1],nTamPrd+nTamLoc+nTamLot+nTamNLt+1,nTamLcz)
		cNumSeri := SubStr(aDados[nX][1],nTamPrd+nTamLoc+nTamLot+nTamNLt+nTamLcz+1,nTamNSr)
		nQtEmp   := aDados[nX][2][2]

		dbSelectArea("SBF")
		dbSetOrder(1)
		If dbSeek(xFilial()+cLocal+cLocaliz+cProduto+cNumSeri+cLoteCtl+cNumLote)
			nQtdSegUM := Self:ConverteUM(cProduto, nQtEmp)
			GravaBFEmp("-",nQtEmp,"P",.F.,nQtdSegUM)
		EndIf

	Next nX

Return

/*/{Protheus.doc} AtualizaStatus
Atualiza a vari�vel de mem�ria com o progresso do processamento.

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
/*/
METHOD AtualizaStatus() CLASS ExclusaoPrevistos
	Local cStatus   := "PREVSTATUS"
	Local nProgress := 0
	Local lRet      := .T.
	VarBeginT(Self:cUIDStatus,cStatus)
	lRet := VarGetXD(Self:cUIDStatus,cStatus,@nProgress)
	If lRet
		nProgress += 20
	Else
		nProgress := 20
	EndIf
	lRet := VarSetXD(Self:cUIDStatus,cStatus,nProgress)
	IF !lRet
		LogMsg("PCPA146", 0, 0, 1, "", "", "Erro ao atualizar a vari�vel global de status da exclus�o de documentos previstos.")
	EndIf
	VarEndT(Self:cUIDStatus,cStatus)
Return

/*/{Protheus.doc} GetStatus
Busca o progresso do processamento.

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
/*/
METHOD GetStatus() CLASS ExclusaoPrevistos
	Local cStatus   := "PREVSTATUS"
	Local lRet      := .T.
	Local nProgress := 0

	lRet := VarGetXD(Self:cUIDStatus,cStatus,@nProgress)
	If !lRet
		nProgress := 0
	EndIf

Return nProgress

/*/{Protheus.doc} LimpaVarStatus
Limpa vari�vel de mem�ria que guarda o progresso do processamento.

@type  Method
@author renan.roeder
@since 02/12/2019
@version P12.1.27
/*/
METHOD LimpaVarStatus() CLASS ExclusaoPrevistos
	VarClean(Self:cUIDStatus)
Return

/*/{Protheus.doc} A146ExcOPs
Fun��o para chamada do m�todo de exclus�o de OPs
@author renan.roeder
@since 02/12/2019
@version P12
@param 01 - cTicket, character, Ticket do processamento
/*/
Function A146ExcOPs(cTicket)
	If oExcPrevistos == Nil
		oExcPrevistos := ExclusaoPrevistos():New(cTicket,.F.)
	EndIf
	oExcPrevistos:ExcluiOrdensProducao()
	oExcPrevistos:AtualizaStatus()
Return

/*/{Protheus.doc} A146ExcSCs
Fun��o para chamada do m�todo de exclus�o de SCs
@author renan.roeder
@since 02/12/2019
@version P12
@param 01 - cTicket, character, Ticket do processamento
/*/
Function A146ExcSCs(cTicket)
	If oExcPrevistos == Nil
		oExcPrevistos := ExclusaoPrevistos():New(cTicket,.F.)
	EndIf
	oExcPrevistos:ExcluiSolicitacoesCompra()
	oExcPrevistos:AtualizaStatus()
Return

/*/{Protheus.doc} A146ExcPCs
Fun��o para chamada do m�todo de exclus�o de PCs
@author renan.roeder
@since 02/12/2019
@version P12
@param 01 - cTicket, character, Ticket do processamento
/*/
Function A146ExcPCs(cTicket)
	If oExcPrevistos == Nil
		oExcPrevistos := ExclusaoPrevistos():New(cTicket,.F.)
	EndIf
	oExcPrevistos:ExcluiPedidosCompra()
	oExcPrevistos:AtualizaStatus()
Return

/*/{Protheus.doc} getWhereC2
Monta a condi��o WHERE padr�o da tabela SC2

@type  Static Function
@author lucas.franca
@since 18/02/2020
@version P12.1.30
@param lNoAlias, Logic    , Identifica que n�o deve adicionar o alias "SC2" antes das colunas
@param cDeleted, Character, Filtro de registros deletados.
@return cWhere , Character, Retorna o where padr�o da SC2
/*/
Static Function getWhereC2(lNoAlias, cDeleted)
	Local cWhere := ""

	cWhere :=     " SC2.C2_FILIAL = '" + xFilial("SC2") + "'"
	cWhere += " AND SC2.C2_TPOP   = 'P' "
	cWhere += " AND SC2.D_E_L_E_T_ = '" + cDeleted + "' "

	If SuperGetMV("MV_MRPDEL",.F.,.F.)
		cWhere += " AND SC2.C2_SEQMRP <> '" + CriaVar("C2_SEQMRP") + "'"
	EndIf

	If lNoAlias
		cWhere := StrTran(cWhere, "SC2.", " ")
	EndIf

Return cWhere

/*/{Protheus.doc} getC2OP
Monta o SQL retornando o c�digo da OP, podendo ser
C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD ou o C2_OP.

@type  Static Function
@author lucas.franca
@since 20/02/2020
@version P12.1.30
@return cQryC2OP, Character, Query montando o retorno do campo C2_OP
/*/
Static Function getC2OP()
	Local cQryC2OP := ""

	cQryC2OP := " CASE SC2.C2_OP "
	cQryC2OP +=      " WHEN ' ' THEN "
	If "MSSQL" $ TCGetDB()
		cQryC2OP +=       " SC2.C2_NUM+SC2.C2_ITEM+SC2.C2_SEQUEN+SC2.C2_ITEMGRD "
	Else
		cQryC2OP +=       " SC2.C2_NUM||SC2.C2_ITEM||SC2.C2_SEQUEN||SC2.C2_ITEMGRD "
	EndIf
	cQryC2OP +=      " ELSE "
	cQryC2OP +=           " SC2.C2_OP "
	cQryC2OP +=      " END "

Return cQryC2OP
