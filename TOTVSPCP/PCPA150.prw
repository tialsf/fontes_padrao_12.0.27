#INCLUDE "TOTVS.CH"
#INCLUDE "PCPA150.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} PCPA150
Tela para impress�o de Etiquetas RFID
@author Marcelo Neumann
@since 27/05/2020
@version P12
/*/
Function PCPA150()

	Local aButtons := {{.F., Nil},{.F., Nil},{.F., Nil},{.F., Nil},{.F., Nil},{.F., Nil},{.T., STR0002},; //"Imprimir"
	                   {.T., STR0004},{.F., Nil},{.F., Nil},{.F., Nil},{.F., Nil},{.F., Nil},{.F., Nil}}  //"Fechar"

	FWExecView(STR0002                    ,; //"Imprimir
	           'PCPA150'                  ,;
			   MODEL_OPERATION_UPDATE     ,;
			   /*oDlg*/                   ,;
			   /*bCloseOnOK*/             ,;
			   {|oView| ImprimeEti(oView)},;
			   /*nPercReducao*/           ,;
			   aButtons                   ,;
			   {|oView| FechaTela(oView)}  )

Return

/*/{Protheus.doc} ModelDef
Defini��o do modelo da tela
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return oModel, objeto, modelo de dados definido
/*/
Static Function ModelDef()

	Local oModel    := MPFormModel():New('PCPA150')
	Local oStruCab  := FWFormModelStruct():New()
	Local oStruGrid := FWFormStruct(1, "HWN", {|cCampo| "|" + AllTrim(cCampo) + "|" $ "|HWN_PROD|HWN_LOTE|HWN_SUBLOT|HWN_NUMSER|"})

	//Altera as estruturas dos modelos
 	AltStruMod(@oStruCab, @oStruGrid)

	//FLD_INVISIVEL - Modelo "invis�vel"
	oModel:addFields('FLD_INVISIVEL', /*cOwner*/, oStruCab, , , {|| LoadMdlFld()})
	oModel:GetModel("FLD_INVISIVEL"):SetDescription(STR0001) //"Gera��o de Etiquetas RFID"
	oModel:GetModel("FLD_INVISIVEL"):SetOnlyQuery(.T.)

	//GRD_ETIQUETAS - Grid para informar as etiquetas a serem impressas
	oModel:AddGrid("GRD_ETIQUETAS", "FLD_INVISIVEL", oStruGrid, , {|oModel| PostLine(oModel)})
	oModel:GetModel("GRD_ETIQUETAS"):SetDescription(STR0001) //"Gera��o de Etiquetas RFID"
	oModel:GetModel("GRD_ETIQUETAS"):SetOnlyQuery(.T.)

	oModel:SetDescription(STR0001) //"Gera��o de Etiquetas RFID"
	oModel:SetPrimaryKey({})

Return oModel

/*/{Protheus.doc} ViewDef
Defini��o da View
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return oView, objeto, view definida
/*/
Static Function ViewDef()

	Local oStruGrid := FWFormStruct(2, "HWN", {|cCampo| "|" + AllTrim(cCampo) + "|" $ "|HWN_PROD|HWN_LOTE|HWN_SUBLOT|HWN_NUMSER|"})
	Local oView     := FWFormView():New()

	//Altera os campos da estrutura para a view
	AltStrView(@oStruGrid)

	//Defini��es da View
	oView:SetModel(FWLoadModel("PCPA150"))

	//V_GRD_ETIQUETAS - View da Grid das etiquetas
	oView:AddGrid("V_GRD_ETIQUETAS", oStruGrid, "GRD_ETIQUETAS")

	//Relaciona a SubView com o Box
	oView:CreateHorizontalBox("BOX_GRID", 100)
	oView:SetOwnerView("V_GRD_ETIQUETAS", 'BOX_GRID')

	oView:AddUserButton(STR0003, "", {|oView| LimpaTela(oView)}, , , {MODEL_OPERATION_UPDATE}, .T.) //"Limpar"

	//Seta para n�o exibir a mensagem de Modifica��o
	oView:showUpdateMsg(.F.)

Return oView

/*/{Protheus.doc} AltStruMod
Edita os campos da estrutura do Model
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param 01 oStruCab , objeto, estrutura do modelo FLD_INVISIVEL
@param 02 oStruGrid, objeto, estrutura do modelo GRD_ETIQUETAS
@return Nil
/*/
Static Function AltStruMod(oStruCab, oStruGrid)

	//Adiciona campos
	oStruCab:AddField(STR0005                                      ,; // [01]  C   Titulo do campo  - "Invis�vel"
	                  STR0005                                      ,; // [02]  C   ToolTip do campo - "Invis�vel"
	                  "INVISIVEL"                                  ,; // [03]  C   Id do Field
	                  "C"                                          ,; // [04]  C   Tipo do campo
	                  1                                            ,; // [05]  N   Tamanho do campo
	                  0                                            ,; // [06]  N   Decimal do campo
	                  NIL, NIL, NIL, .F., NIL, NIL, .T., .T.)

	oStruGrid:AddField(STR0036                                     ,; // [01]  C   Titulo do campo  //"Descri��o"
	                   STR0036                                     ,; // [02]  C   ToolTip do campo //"Descri��o"
	                   "CDESCPROD"                                 ,; // [03]  C   Id do Field
	                   "C"                                         ,; // [04]  C   Tipo do campo
	                   GetSx3Cache("B1_DESC","X3_TAMANHO")         ,; // [05]  N   Tamanho do campo
	                   0, NIL, NIL, NIL, .F.                       ,; // [10]  L   Indica se o campo tem preenchimento obrigat�rio
	                   NIL, NIL, .T., .T.)

	oStruGrid:AddField(STR0006                                     ,; // [01]  C   Titulo do campo  - "Qtd. de Etiquetas"
	                   STR0006                                     ,; // [02]  C   ToolTip do campo - "Qtd. de Etiquetas"
	                   "NQTDETIQUETAS"                             ,; // [03]  C   Id do Field
	                   "N"                                         ,; // [04]  C   Tipo do campo
	                   5                                           ,; // [05]  N   Tamanho do campo
	                   0                                           ,; // [06]  N   Decimal do campo
	                   NIL, NIL, NIL, .T.                          ,; // [10]  C   Obrigat�rio?
	                   {|| 1}                                      ,; // [11]  B   Inicializador padr�o
	                   NIL, .T., .T.)

	oStruGrid:AddField(STR0007                                     ,; // [01]  C   Titulo do campo  - "Qtd. Por Etiqueta"
	                   STR0007                                     ,; // [02]  C   ToolTip do campo - "Qtd. Por Etiqueta"
	                   "NFATOR"                                    ,; // [03]  C   Id do Field
	                   "N"                                         ,; // [04]  C   Tipo do campo
	                   GetSX3Cache("VK_RFIDFAT", "X3_TAMANHO")     ,; // [05]  N   Tamanho do campo
	                   GetSX3Cache("VK_RFIDFAT", "X3_DECIMAL")     ,; // [06]  N   Decimal do campo
	                   NIL, NIL, NIL, .T.                          ,; // [10]  C   Obrigat�rio?
	                   {|| 1}                                      ,; // [11]  B   Inicializador padr�o
	                   NIL, .T., .T.)

	oStruGrid:AddField(STR0039                                     ,; // [01]  C   Titulo do campo  - "Total"
	                   STR0039                                     ,; // [02]  C   ToolTip do campo - "Total"
	                   "NTOTAL"                                    ,; // [03]  C   Id do Field
	                   "N"                                         ,; // [04]  C   Tipo do campo
	                   GetSX3Cache("VK_RFIDFAT", "X3_TAMANHO")     ,; // [05]  N   Tamanho do campo
	                   GetSX3Cache("VK_RFIDFAT", "X3_DECIMAL")     ,; // [06]  N   Decimal do campo
	                   NIL, NIL, NIL, .T.                          ,; // [10]  C   Obrigat�rio?
	                   {|| 1}                                      ,; // [11]  B   Inicializador padr�o
	                   NIL, .T., .T.)

	oStruGrid:AddField(STR0008                                     ,; // [01]  C   Titulo do campo  - "Rastro"
	                   STR0008                                     ,; // [02]  C   ToolTip do campo - "Rastro"
	                   "CRASTRO"                                   ,; // [03]  C   Id do Field
	                   "C"                                         ,; // [04]  C   Tipo do campo
	                   1, 0, NIL, NIL, NIL, .F., NIL, NIL, .T., .T.)

	oStruGrid:AddField(STR0009                                     ,; // [01]  C   Titulo do campo  - "Endere�o"
	                   STR0009                                     ,; // [02]  C   ToolTip do campo - "Endere�o"
	                   "CLOCALIZ"                                  ,; // [03]  C   Id do Field
	                   "C"                                         ,; // [04]  C   Tipo do campo
	                   1, 0, NIL, NIL, NIL, .F., NIL, NIL, .T., .T.)

	//Condi��o para altera��o
	oStruGrid:SetProperty("HWN_LOTE"     , MODEL_FIELD_WHEN , FWBuildFeature(STRUCT_FEATURE_WHEN  , "P150WhLote()"))
	oStruGrid:SetProperty("HWN_SUBLOT"   , MODEL_FIELD_WHEN , FWBuildFeature(STRUCT_FEATURE_WHEN  , "P150WhSubL()"))
	oStruGrid:SetProperty("HWN_NUMSER"   , MODEL_FIELD_WHEN , FWBuildFeature(STRUCT_FEATURE_WHEN  , "P150WhNumS()"))

	//Valida��es
	oStruGrid:SetProperty("HWN_PROD"     , MODEL_FIELD_VALID, FWBuildFeature(STRUCT_FEATURE_VALID , "P150ValPrd()"))
	oStruGrid:SetProperty("HWN_LOTE"     , MODEL_FIELD_VALID, FWBuildFeature(STRUCT_FEATURE_VALID , "P150ValLot()"))
	oStruGrid:SetProperty("HWN_SUBLOT"   , MODEL_FIELD_VALID, FWBuildFeature(STRUCT_FEATURE_VALID , "P150ValSub()"))
	oStruGrid:SetProperty("HWN_NUMSER"   , MODEL_FIELD_VALID, FWBuildFeature(STRUCT_FEATURE_VALID , "P150ValSer()"))
	oStruGrid:SetProperty("NQTDETIQUETAS", MODEL_FIELD_VALID, FWBuildFeature(STRUCT_FEATURE_VALID , "P150ValQtd()"))
	oStruGrid:SetProperty("NFATOR"       , MODEL_FIELD_VALID, FWBuildFeature(STRUCT_FEATURE_VALID , "P150ValFat()"))

	//Gatilhos
	oStruGrid:AddTrigger("NFATOR"       , "NTOTAL", , {|| TrigTotal()})
	oStruGrid:AddTrigger("NQTDETIQUETAS", "NTOTAL", , {|| TrigTotal()})
	oStruGrid:AddTrigger("HWN_PROD"     , "NFATOR", , {|| TrigFator()})


Return

/*/{Protheus.doc} AltStrView
Edita os campos da estrutura da View
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param oStruGrid, objeto, estrutura da View V_GRD_ETIQUETAS
@return Nil
/*/
Static Function AltStrView(oStruGrid)

	oStruGrid:SetProperty("HWN_PROD", MVC_VIEW_ORDEM, "01")
	oStruGrid:AddField("CDESCPROD"                 ,; // [01]  C   Nome do Campo
	                   "02"                        ,; // [02]  C   Ordem
	                   STR0036                     ,; // [03]  C   Titulo do campo    //"Descri��o"
	                   STR0036                     ,; // [04]  C   Descricao do campo //"Descri��o"
	                   NIL                         ,; // [05]  A   Array com Help
	                   "C"                         ,; // [06]  C   Tipo do campo
	                   "@S15"                      ,; // [07]  C   Picture
	                   NIL, NIL, .F.               ,; // [10]  L   Indica se o campo � alteravel
	                   NIL, NIL, NIL, NIL, NIL, .T., NIL, NIL)

	oStruGrid:AddField("NQTDETIQUETAS"             ,; // [01]  C   Nome do Campo
	                   "10"                        ,; // [02]  C   Ordem
	                   STR0006                     ,; // [03]  C   Titulo do campo    - "Qtd. de Etiquetas"
	                   STR0006                     ,; // [04]  C   Descricao do campo - "Qtd. de Etiquetas"
	                   {STR0030}                   ,; // [05]  A   Array com Help     - "Indica quantas c�pias da etiqueta ser�o impressas para este produto. Este campo � obrigat�rio e n�o permite o valor zero."
	                   "N"                         ,; // [06]  C   Tipo do campo
	                   "99,999"                    ,; // [07]  C   Picture
	                   NIL, NIL, .T.               ,; // [10]  L   Indica se o campo � alteravel
	                   NIL, NIL, NIL, NIL, NIL, .T., NIL, NIL)

	oStruGrid:AddField("NFATOR"                    ,; // [01]  C   Nome do Campo
	                   "11"                        ,; // [02]  C   Ordem
	                   STR0007                     ,; // [03]  C   Titulo do campo    - "Qtd. por Etiqueta"
	                   STR0007                     ,; // [04]  C   Descricao do campo - "Qtd. por Etiqueta"
	                   {STR0031}                   ,; // [05]  A   Array com Help     - "Indica a quantidade do produto que cada etiqueta representa."
	                   "N"                         ,; // [06]  C   Tipo do campo
	                   GetSX3Cache("VK_RFIDFAT", "X3_PICTURE"),; // [07]  C   Picture
	                   NIL, NIL, .T.               ,; // [10]  L   Indica se o campo � alteravel
	                   NIL, NIL, NIL, NIL, NIL, .T., NIL, NIL)

	oStruGrid:AddField("NTOTAL"                    ,; // [01]  C   Nome do Campo
	                   "12"                        ,; // [02]  C   Ordem
	                   STR0039                     ,; // [03]  C   Titulo do campo    - "Total"
	                   STR0039                     ,; // [04]  C   Descricao do campo - "Total"
	                   {STR0040}                   ,; // [05]  A   Array com Help     - "Quantidade total do produto que ser� impresso"
	                   "N"                         ,; // [06]  C   Tipo do campo
	                   GetSX3Cache("VK_RFIDFAT", "X3_PICTURE"),; // [07]  C   Picture
	                   NIL, NIL, .F.               ,; // [10]  L   Indica se o campo � alteravel
	                   NIL, NIL, NIL, NIL, NIL, .T., NIL, NIL)

	//Consulta padr�o
	oStruGrid:SetProperty("HWN_PROD"  , MVC_VIEW_LOOKUP, "SVKRFI")
	oStruGrid:SetProperty("HWN_LOTE"  , MVC_VIEW_LOOKUP, "SB8RFI")
	oStruGrid:SetProperty("HWN_SUBLOT", MVC_VIEW_LOOKUP, "SB8RFS")
	oStruGrid:SetProperty("HWN_NUMSER", MVC_VIEW_LOOKUP, "SBFRFI")

Return

/*/{Protheus.doc} LoadMdlFld
Carga do modelo mestre (invis�vel)
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return aLoad, array, array de load do modelo preenchido
/*/
Static Function LoadMdlFld()

	Local aLoad := {}

	aAdd(aLoad, {"A"}) //Dados
	aAdd(aLoad, 1    ) //Recno

Return aLoad

/*/{Protheus.doc} LimpaTela
Limpa a grid (fun��o do bot�o "Limpar")
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param oView, objeto, objeto da view
@return Nil
/*/
Static Function LimpaTela(oView)

	Local oModel := FwModelActive()

	oModel:GetModel("GRD_ETIQUETAS"):ClearData(.T., .T.)

Return

/*/{Protheus.doc} FechaTela
Fecha a tela (fun��o do bot�o "Fechar")
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param oView, objeto, objeto da view
@return l�gico, indica se a tela pode ser fechada
/*/
Static Function FechaTela(oView)

	Local oModel := FwModelActive()

	//Necess�rio para n�o exibir a mensagem de formul�rio n�o alterado
	oModel:lModify := .F.
	oView:lModify  := .F.

Return .T.

/*/{Protheus.doc} P150WhLote
Condi��o para altera��o do campo Lote
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return l�gico, indica se o campo pode ser editado (.T.) ou n�o (.F.)
/*/
Function P150WhLote()

	Local oModel  := FwModelActive()
	Local cRastro := oModel:GetModel("GRD_ETIQUETAS"):GetValue("CRASTRO")

	If cRastro == "L" .Or. cRastro == "S"
		Return .T.
	EndIf

Return .F.

/*/{Protheus.doc} P150WhSubL
Condi��o para altera��o do campo Sub-Lote
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return l�gico, indica se o campo pode ser editado (.T.) ou n�o (.F.)
/*/
Function P150WhSubL()

	Local oModel := FwModelActive()

	If oModel:GetModel("GRD_ETIQUETAS"):GetValue("CRASTRO") == "S"
		Return .T.
	EndIf

Return .F.

/*/{Protheus.doc} P150WhNumS
Condi��o para altera��o do campo N�mero de S�rie
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return l�gico, indica se o campo pode ser editado (.T.) ou n�o (.F.)
/*/
Function P150WhNumS()

	Local oModel := FwModelActive()

	If oModel:GetModel("GRD_ETIQUETAS"):GetValue("CLOCALIZ") == "S"
		Return .T.
	EndIf

Return .F.

/*/{Protheus.doc} P150ValPrd
Valida��o do campo Produto
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return l�gico, indica se o valor digitado � v�lido (.T.) ou n�o (.F.)
/*/
Function P150ValPrd()

	Local oModel    := FwModelActive()
	Local cProduto  := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_PROD")

	//Campo de preenchimento obrigat�rio
	If Empty(cProduto)
		Help( ,  , "Help", ,  STR0010,; //"Produto n�o informado."
			 1, 0, , , , , , {STR0011}) //"Informe um produto para imprimir a etiqueta."
		Return .F.
	EndIf

	//Deve existir na SB1
	SB1->(dbSetOrder(1))
	If !SB1->(dbSeek(xFilial("SB1") + cProduto))
		Help( ,  , "Help", ,  STR0012,; //"Produto n�o cadastrado."
			 1, 0, , , , , , {STR0013}) //"Informe um produto v�lido para imprimir a etiqueta."
		Return .F.
	EndIf
	oModel:GetModel("GRD_ETIQUETAS"):LoadValue("CDESCPROD", SB1->B1_DESC)

	//Deve possuir controle do RFID ativo (VK_RFID)
	If !UsaRFID(cProduto)
		Help( ,  , "Help", ,  STR0014,; //"Produto n�o controla RFID."
			 1, 0, , , , , , {STR0015}) //"Informe um produto que possua controle de RFID."
		Return .F.
	EndIf

	oModel:GetModel("GRD_ETIQUETAS"):LoadValue("HWN_SUBLOT", "")
	oModel:GetModel("GRD_ETIQUETAS"):LoadValue("HWN_LOTE"  , "")
	oModel:GetModel("GRD_ETIQUETAS"):LoadValue("HWN_NUMSER", "")
	oModel:GetModel("GRD_ETIQUETAS"):LoadValue("CRASTRO"  , SB1->B1_RASTRO )
	oModel:GetModel("GRD_ETIQUETAS"):LoadValue("CLOCALIZ" , SB1->B1_LOCALIZ)

Return .T.

/*/{Protheus.doc} P150ValLot
Valida��o do campo Lote
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param lObriga, l�gico, indica se o prenchimento � obrigat�rio
@return lOk, l�gico, indica se o valor digitado � v�lido (.T.) ou n�o (.F.)
/*/
Function P150ValLot(lObriga)

	Local aAreaSB8 := SB8->(GetArea())
	Local lOk      := .T.
	Local oModel   := FwModelActive()
	Local cProduto := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_PROD")
	Local cLote    := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_LOTE")
	Default lObriga := .F.

	If !Empty(cLote) .Or. lObriga
		//Deve existir na tabela SB8
		SB8->(dbSetOrder(5))
		If !SB8->(dbSeek(xFilial("SB8") + cProduto + cLote))
			Help( ,  , "Help", ,  STR0018,; //"Lote n�o cadastrado."
				 1, 0, , , , , , {STR0019}) //"Informe um lote v�lido para o produto."
			lOk := .F.
		EndIf
	EndIf

	RestArea(aAreaSB8)

Return lOk

/*/{Protheus.doc} P150ValSub
Valida��o do campo Sub-Lote
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param lObriga, l�gico, indica se o prenchimento � obrigat�rio
@return lOk, l�gico, indica se o valor digitado � v�lido (.T.) ou n�o (.F.)
/*/
Function P150ValSub(lObriga)

	Local aAreaSB8 := SB8->(GetArea())
	Local lOk      := .T.
	Local oModel   := FwModelActive()
	Local cProduto := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_PROD")
	Local cLote    := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_LOTE")
	Local cSubLote := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_SUBLOT")
	Default lObriga := .F.

	If !Empty(cSubLote) .Or. lObriga
		//Deve existir na tabela SB8
		SB8->(dbSetOrder(5))
		If !SB8->(dbSeek(xFilial("SB8") + cProduto + cLote + cSubLote))
			Help( ,  , "Help", ,  STR0020,; //"Sub-Lote n�o cadastrado."
				 1, 0, , , , , , {STR0021}) //"Informe um sub-lote v�lido para o produto."
			lOk := .F.
		EndIf
	EndIf

	RestArea(aAreaSB8)

Return lOk

/*/{Protheus.doc} P150ValSer
Valida��o do campo N�mero de S�rie
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return lOk, l�gico, indica se o valor digitado � v�lido (.T.) ou n�o (.F.)
/*/
Function P150ValSer()

	Local cAliasQry := ""
	Local cQryWhere := ""
	Local lOk       := .T.
	Local oModel    := FwModelActive()
	Local cLote     := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_LOTE")
	Local cNumSerie := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_NUMSER")
	Local cProduto  := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_PROD")
	Local cRastro   := oModel:GetModel("GRD_ETIQUETAS"):GetValue("CRASTRO")
	Local cSubLote  := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_SUBLOT")

	If !Empty(cNumSerie)
		cAliasQry := GetNextAlias()
		cQryWhere := " BF_NUMSERI = '" + cNumSerie + "'"

		If cRastro == "L"
			cQryWhere += " AND BF_LOTECTL = '" + cLote + "'"

		ElseIf cRastro == "S"
			cQryWhere += " AND BF_LOTECTL = '" + cLote    + "'" + ;
			             " AND BF_NUMLOTE = '" + cSubLote + "'"
		EndIf
		cQryWhere := "%" + cQryWhere + "%"

		BeginSql Alias cAliasQry
		  SELECT 1
			FROM %Table:SBF%
		   WHERE BF_FILIAL  = %xFilial:SBF%
		     AND BF_PRODUTO = %Exp:cProduto%
		     AND %Exp:cQryWhere%
			 AND %NotDel%
		EndSql

		//Deve existir na tabela SBF
		If (cAliasQry)->(EOF())
			Help( ,  , "Help", ,  STR0022,; //"N�mero de S�rie n�o cadastrado."
				 1, 0, , , , , , {STR0023}) //"Informe um n�mero de s�rie v�lido para o produto."
			lOk := .F.
		EndIf

		(cAliasQry)->(dbCloseArea())
	EndIf

Return lOk

/*/{Protheus.doc} P150ValQtd
Valida��o do campo Quantidade de Etiquetas
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return l�gico, indica se o valor digitado � v�lido (.T.) ou n�o (.F.)
/*/
Function P150ValQtd()

	Local oModel := FwModelActive()
	Local nQuant := oModel:GetModel("GRD_ETIQUETAS"):GetValue("NQTDETIQUETAS")

	//Preenchimento obrigat�rio
	If Empty(nQuant)
		Help( ,  , "Help", ,  STR0024,; //"Quantidade de etiquetas n�o informada."
			 1, 0, , , , , , {STR0025}) //"Informe a quantidade de etiquetas a serem impressas."
		Return .F.

	//Valor m�nimo � 1;
	ElseIf nQuant < 1
		Help( ,  , "Help", ,  STR0026,; //"Quantidade de etiquetas n�o pode ser menor que 1."
			 1, 0, , , , , , {STR0026}) //"Informe uma quantidade maior ou igual a 1."
		Return .F.
	EndIf

Return .T.

/*/{Protheus.doc} P150ValFat
Valida��o do campo Qtd. por Etiqueta

@author lucas.franca
@since 30/07/2020
@version P12
@return l�gico, indica se o valor digitado � v�lido (.T.) ou n�o (.F.)
/*/
Function P150ValFat()
	Local oModel := FwModelActive()
	Local nFator := oModel:GetModel("GRD_ETIQUETAS"):GetValue("NFATOR")

	//Preenchimento obrigat�rio
	If Empty(nFator)
		Help( ,  , "Help", ,  STR0041,; //"Qtd. Por Etiqueta n�o informada."
			 1, 0, , , , , , {STR0042}) //"Informe um valor maior que 0 para o campo Quantidade por Etiqueta."
		Return .F.

	//Valor deve ser positivo;
	ElseIf nFator < 0
		Help( ,  , "Help", ,  STR0043,; //"Quantidade por etiqueta n�o pode conter valores negativos."
			 1, 0, , , , , , {STR0042}) //"Informe um valor maior que 0 para o campo Quantidade por Etiqueta."
		Return .F.
	EndIf

Return .T.

/*/{Protheus.doc} PostLine
Valida a linha digitada (LinhaOk)
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param oSubModel, objeto, submodelo correspondente ao grid
@return l�gico, indica se as etiquetas foram impressas com sucesso
/*/
Static Function PostLine(oSubModel)

	Local cRastro := oSubModel:GetValue("CRASTRO")

	If Empty(oSubModel:GetValue("HWN_PROD"))
		Help( ,  , "Help", ,  STR0010,; //"Produto n�o informado."
			 1, 0, , , , , , {STR0011}) //"Informe um produto para imprimir a etiqueta."
		Return .F.
	EndIf

	If cRastro == "L"
		If !P150ValLot(.T.)
			Return .F.
		EndIf
	ElseIf cRastro == "S"
		If !P150ValSub(.T.)
			Return .F.
		EndIf
	EndIf

	If oSubModel:GetValue("CLOCALIZ") == "S"
		If !P150ValSer()
			Return .F.
		EndIf
	EndIf

	If Empty(oSubModel:GetValue("NQTDETIQUETAS"))
		Help( ,  , "Help", ,  STR0024,; //"Quantidade de etiquetas n�o informada."
			 1, 0, , , , , , {STR0025}) //"Informe a quantidade de etiquetas a serem impressas."
		Return .F.
	EndIf

Return .T.

/*/{Protheus.doc} ImprimeEti
Imprime as etiquetas digitadas
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param oView, objeto, objeto da view
@return l�gico, indica se as etiquetas foram impressas com sucesso
/*/
Static Function ImprimeEti(oView)

	Local oModel := FwModelActive()

	If PostLine(oModel:GetModel("GRD_ETIQUETAS"))
		FWMsgRun( , {|| ProcImpres(oView)}, STR0037, STR0035) //"Imprimindo", "Imprimindo etiquetas..."
	EndIf

Return .F.

/*/{Protheus.doc} ProcImpres
Processa a impress�o as etiquetas digitadas
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param oView, objeto, objeto da view
@return Nil
/*/
Static Function ProcImpres(oView)

	Local aParam   := {}
	Local cCodImpr := ""
	Local nInd     := 0
	Local nQtdRegs := 0
	Local oModel   := FwModelActive()
	Local oMdlGrid := oModel:GetModel("GRD_ETIQUETAS")
	Local nLen     := 0

	If oMdlGrid:IsEmpty() .Or. oMdlGrid:Length(.T.) == 0
		Help( ,  , "Help", ,  STR0010,; //"Produto n�o informado."
			 1, 0, , , , , , {STR0011}) //"Informe um produto para imprimir a etiqueta."
		Return
	EndIf

	//Necess�rio para n�o exibir a mensagem de formul�rio n�o alterado
	oModel:lModify := .T.
	oView:lModify  := .T.

	cCodImpr := RFIDSelImp()

	If !Empty(cCodImpr)
		aAdd(aParam, cCodImpr)
		aAdd(aParam, {})
		nLen := oMdlGrid:Length(.F.)
		For nInd := 1 To nLen
			If !oMdlGrid:IsDeleted(nInd) .And. !Empty(oMdlGrid:GetValue("HWN_PROD", nInd))
				nQtdRegs++
				aAdd(aParam[2], Array(RFIDGetCon("ARR_APARAM_SIZE")))

				aParam[2][nQtdRegs][RFIDGetCon("ARR_APARAM_PRODUTO" )] := oMdlGrid:GetValue("HWN_PROD"     , nInd)
				aParam[2][nQtdRegs][RFIDGetCon("ARR_APARAM_LOTE"    )] := oMdlGrid:GetValue("HWN_LOTE"     , nInd)
				aParam[2][nQtdRegs][RFIDGetCon("ARR_APARAM_SUBLOTE" )] := oMdlGrid:GetValue("HWN_SUBLOT"   , nInd)
				aParam[2][nQtdRegs][RFIDGetCon("ARR_APARAM_NUMSERIE")] := oMdlGrid:GetValue("HWN_NUMSER"   , nInd)
				aParam[2][nQtdRegs][RFIDGetCon("ARR_APARAM_QUANT"   )] := oMdlGrid:GetValue("NFATOR"       , nInd)
				aParam[2][nQtdRegs][RFIDGetCon("ARR_APARAM_COPIAS"  )] := oMdlGrid:GetValue("NQTDETIQUETAS", nInd)
				aParam[2][nQtdRegs][RFIDGetCon("ARR_APARAM_ALIAS"   )] := " "
				aParam[2][nQtdRegs][RFIDGetCon("ARR_APARAM_CHAVE"   )] := " "
			EndIf
		Next nInd

		If nQtdRegs == 0
			Help( ,  , "Help", ,  STR0033,; //"N�o existem dados v�lidos para serem impressos."
				 1, 0, , , , , , {STR0034}) //"Verifique os dados informados."
		Else
			If RFIDPrint("PCPA150", aParam)
				MsgInfo(STR0032) //"Processamento finalizado."
			EndIf
			aSize(aParam ,0)
		EndIf
	EndIf

Return

/*/{Protheus.doc} P150CoB8Fi
Consulta padr�o do campo Lote e Sub-Lote - Filtro
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param nInd, numeric, indica se � o campo Lote (1) ou o Sub-Lote (2)
@return lOk, l�gico, indica se o registro deve ser exibido na Consulta Padr�o
/*/
Function P150CoB8Fi(nInd)

	Local lOk    := .T.
	Local oModel := FwModelActive()

	If oModel <> Nil
		If SB8->B8_PRODUTO <> oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_PROD")
			lOk := .F.
		EndIf
	EndIF

Return lOk

/*/{Protheus.doc} P150CoB8Re
Consulta padr�o do campo Lote e Sub-Lote - Retorno
@author Marcelo Neumann
@since 27/05/2020
@version P12
@param nInd, numeric, indica se � o campo Lote (1) ou o Sub-Lote (2)
@return cValue, caracter, lote ou sub-lote selecionado
/*/
Function P150CoB8Re(nInd)

	Local cValue    := ""
	Local oModel    := FwModelActive()
	Local oView     := FwViewActive()
	Local oGridView := Nil

	If oModel <> Nil
		//Consulta acionada pelo campo LOTE
		If nInd == 1
			If oModel:GetModel("GRD_ETIQUETAS"):CanSetValue("HWN_SUBLOT")
				oModel:GetModel("GRD_ETIQUETAS"):LoadValue("HWN_LOTE", SB8->B8_LOTECTL)
				oModel:GetModel("GRD_ETIQUETAS"):SetValue("HWN_SUBLOT", SB8->B8_NUMLOTE)
			EndIf
			cValue := SB8->B8_LOTECTL

		//Consulta acionada pelo campo SUB-LOTE
		Else
			If oModel:GetModel("GRD_ETIQUETAS"):CanSetValue("HWN_LOTE")
				oModel:GetModel("GRD_ETIQUETAS"):SetValue("HWN_LOTE", SB8->B8_LOTECTL)
			EndIf
			cValue := SB8->B8_NUMLOTE
		EndIf

		If oView <> Nil
			oGridView := oView:GetSubView("V_GRD_ETIQUETAS")
			oGridView:DeActivate(.T.)
			oGridView:Activate()
		EndIf
	EndIF

Return cValue

/*/{Protheus.doc} P150CoBFFi
Consulta padr�o do campo N�mero de S�rie - Filtro
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return lOk, l�gico, indica se o registro deve ser exibido na Consulta Padr�o
/*/
Function P150CoBFFi()

	Local cLote    := ""
	Local cSubLote := ""
	Local lOk      := .T.
	Local oModel   := FwModelActive()

	If oModel <> Nil
		cLote    := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_LOTE")
		cSubLote := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_SUBLOT")

		If SBF->BF_PRODUTO <> oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_PROD")
			lOk := .F.
		EndIf

		If !Empty(cLote)
			If SBF->BF_LOTECTL <> cLote
				lOk := .F.
			EndIf
		EndIf

		If !Empty(cSubLote)
			If SBF->BF_NUMLOTE <> cSubLote
				lOk := .F.
			EndIf
		EndIf
	EndIF

Return lOk

/*/{Protheus.doc} P150CoBFRe
Consulta padr�o do campo N�mero de S�rie - Retorno
@author Marcelo Neumann
@since 27/05/2020
@version P12
@return caracter, n�mero de s�rie selecionado
/*/
Function P150CoBFRe()

	Local cLote     := ""
	Local cSubLote  := ""
	Local lSetOk    := .F.
	Local oGridView := Nil
	Local oModel    := FwModelActive()
	Local oView     := FwViewActive()

	If oModel <> Nil .And. !Empty(SBF->BF_NUMSERI)
		cLote    := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_LOTE")
		cSubLote := oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_SUBLOT")

		If oModel:GetModel("GRD_ETIQUETAS"):CanSetValue("HWN_LOTE")
			lSetOk := oModel:GetModel("GRD_ETIQUETAS"):SetValue("HWN_LOTE", SBF->BF_LOTECTL)
		EndIf

		If lSetOk .And. oModel:GetModel("GRD_ETIQUETAS"):CanSetValue("HWN_SUBLOT")
			lSetOk := oModel:GetModel("GRD_ETIQUETAS"):SetValue("HWN_SUBLOT", SBF->BF_NUMLOTE)
		EndIf

		If lSetOk
			If oView <> Nil
				oGridView := oView:GetSubView("V_GRD_ETIQUETAS")
				oGridView:DeActivate(.T.)
				oGridView:Activate()
			EndIf
		Else
			oModel:GetModel("GRD_ETIQUETAS"):LoadValue("HWN_LOTE"  , cLote)
			oModel:GetModel("GRD_ETIQUETAS"):LoadValue("HWN_SUBLOT", cSubLote)
		EndIf
	EndIF

Return SBF->BF_NUMSERI

/*/{Protheus.doc} TrigTotal
Trigger para calcular a qtd. total do produto de acordo com o fator.

@type  Static Function
@author lucas.franca
@since 29/07/2020
@version P12.1.31
@return nTotal, Numeric, Quantidade total do produto
/*/
Static Function TrigTotal()
	Local nTotal  := 0
	Local nQtdEtq := 0
	Local nFator  := 0
	Local oModel := FwModelActive()

	nQtdEtq := oModel:GetModel("GRD_ETIQUETAS"):GetValue("NQTDETIQUETAS")
	nFator  := oModel:GetModel("GRD_ETIQUETAS"):GetValue("NFATOR")

	nTotal := nQtdEtq * nFator
Return nTotal

/*/{Protheus.doc} TrigFator
Trigger para preencher o campo Fator Quantidade de acordo com o produto

@type  Static Function
@author lucas.franca
@since 29/07/2020
@version P12.1.31
@return nFator, Numeric, Fator de quantidade do produto
/*/
Static Function TrigFator()
	Local lFound := .F.
	Local nFator := 0
	Local oModel := FwModelActive()

	If !Empty(oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_PROD"))
		If SVK->VK_COD != oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_PROD") .Or. SVK->VK_FILIAL != xFilial("SVK")
			SVK->(dbSetOrder(1))
			lFound := SVK->(dbSeek(xFilial("SVK")+oModel:GetModel("GRD_ETIQUETAS"):GetValue("HWN_PROD")))
		Else
			lFound := .T.
		EndIf
		If lFound
			nFator := SVK->VK_RFIDFAT
		EndIf
	EndIf

	If nFator <= 0
		nFator := 1
	EndIf

Return nFator
