#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"

//Define constantes para utilizar nos arrays.
//Em outros fontes, utilizar a fun��o A200APICnt
//para recuperar o valor das constantes.
//Ao criar novas constantes, adicionar na fun��o A200APICnt
//Campos do CABE�ALHO
#DEFINE ARRAY_ESTRU_CAB_POS_FILIAL      1
#DEFINE ARRAY_ESTRU_CAB_POS_PAI         2
#DEFINE ARRAY_ESTRU_CAB_POS_QBASE       3
#DEFINE ARRAY_ESTRU_CAB_POS_COMPON      4
#DEFINE ARRAY_ESTRU_CAB_SIZE            4
//Campos do COMPONENTE
#DEFINE ARRAY_ESTRU_CMP_POS_COMP        1
#DEFINE ARRAY_ESTRU_CMP_POS_SEQ         2
#DEFINE ARRAY_ESTRU_CMP_POS_REVINI      3
#DEFINE ARRAY_ESTRU_CMP_POS_REVFIM      4
#DEFINE ARRAY_ESTRU_CMP_POS_QTDNEC      5
#DEFINE ARRAY_ESTRU_CMP_POS_VLDINI      6
#DEFINE ARRAY_ESTRU_CMP_POS_VLDFIM      7
#DEFINE ARRAY_ESTRU_CMP_POS_PERDA       8
#DEFINE ARRAY_ESTRU_CMP_POS_QTDFIXA     9
#DEFINE ARRAY_ESTRU_CMP_POS_GRPOPC      10
#DEFINE ARRAY_ESTRU_CMP_POS_ITEMOPC     11
#DEFINE ARRAY_ESTRU_CMP_POS_POTENC      12
#DEFINE ARRAY_ESTRU_CMP_POS_FANTASM     13
#DEFINE ARRAY_ESTRU_CMP_POS_ALTERNATIVO 14
#DEFINE ARRAY_ESTRU_CMP_POS_RECNO       15
#DEFINE ARRAY_ESTRU_CMP_POS_LOCAL       16
#DEFINE ARRAY_ESTRU_CMP_SIZE            16
//Campos do ALTERNATIVO
#DEFINE ARRAY_ESTRU_ALT_POS_ALTERN      1
#DEFINE ARRAY_ESTRU_ALT_POS_TIPOCONV    2
#DEFINE ARRAY_ESTRU_ALT_POS_FATORCONV   3
#DEFINE ARRAY_ESTRU_ALT_POS_VIGENCIA    4
#DEFINE ARRAY_ESTRU_ALT_POS_ESTOQUE     5
#DEFINE ARRAY_ESTRU_ALT_POS_SEQUENCIA   6
#DEFINE ARRAY_ESTRU_ALT_SIZE            6
/*/{Protheus.doc} A200APICnt
Recupera o valor das constantes utilizadas para
auxiliar na montagem do array das estruturas para integra��o.

@type  Function
@author lucas.franca
@since 11/07/2019
@version P12.1.27
@param cInfo, Caracter, Define qual constante se deseja recuperar o valor.
@return nValue, Numeric, Valor da constante
/*/
Function A200APICnt(cInfo)
	Local nValue := ARRAY_ESTRU_CAB_SIZE
	Do Case
		Case cInfo == "ARRAY_ESTRU_CAB_POS_FILIAL"
			nValue := ARRAY_ESTRU_CAB_POS_FILIAL
		Case cInfo == "ARRAY_ESTRU_CAB_POS_PAI"
			nValue := ARRAY_ESTRU_CAB_POS_PAI
		Case cInfo == "ARRAY_ESTRU_CAB_POS_QBASE"
			nValue := ARRAY_ESTRU_CAB_POS_QBASE
		Case cInfo == "ARRAY_ESTRU_CAB_POS_COMPON"
			nValue := ARRAY_ESTRU_CAB_POS_COMPON
		Case cInfo == "ARRAY_ESTRU_CAB_SIZE"
			nValue := ARRAY_ESTRU_CAB_SIZE
		Case cInfo == "ARRAY_ESTRU_CMP_POS_COMP"
			nValue := ARRAY_ESTRU_CMP_POS_COMP
		Case cInfo == "ARRAY_ESTRU_CMP_POS_SEQ"
			nValue := ARRAY_ESTRU_CMP_POS_SEQ
		Case cInfo == "ARRAY_ESTRU_CMP_POS_REVINI"
			nValue := ARRAY_ESTRU_CMP_POS_REVINI
		Case cInfo == "ARRAY_ESTRU_CMP_POS_REVFIM"
			nValue := ARRAY_ESTRU_CMP_POS_REVFIM
		Case cInfo == "ARRAY_ESTRU_CMP_POS_QTDNEC"
			nValue := ARRAY_ESTRU_CMP_POS_QTDNEC
		Case cInfo == "ARRAY_ESTRU_CMP_POS_VLDINI"
			nValue := ARRAY_ESTRU_CMP_POS_VLDINI
		Case cInfo == "ARRAY_ESTRU_CMP_POS_VLDFIM"
			nValue := ARRAY_ESTRU_CMP_POS_VLDFIM
		Case cInfo == "ARRAY_ESTRU_CMP_POS_PERDA"
			nValue := ARRAY_ESTRU_CMP_POS_PERDA
		Case cInfo == "ARRAY_ESTRU_CMP_POS_QTDFIXA"
			nValue := ARRAY_ESTRU_CMP_POS_QTDFIXA
		Case cInfo == "ARRAY_ESTRU_CMP_POS_GRPOPC"
			nValue := ARRAY_ESTRU_CMP_POS_GRPOPC
		Case cInfo == "ARRAY_ESTRU_CMP_POS_ITEMOPC"
			nValue := ARRAY_ESTRU_CMP_POS_ITEMOPC
		Case cInfo == "ARRAY_ESTRU_CMP_POS_POTENC"
			nValue := ARRAY_ESTRU_CMP_POS_POTENC
		Case cInfo == "ARRAY_ESTRU_CMP_POS_FANTASM"
			nValue := ARRAY_ESTRU_CMP_POS_FANTASM
		Case cInfo == "ARRAY_ESTRU_CMP_POS_ALTERNATIVO"
			nValue := ARRAY_ESTRU_CMP_POS_ALTERNATIVO
		Case cInfo == "ARRAY_ESTRU_CMP_POS_RECNO"
			nValue := ARRAY_ESTRU_CMP_POS_RECNO
		Case cInfo == "ARRAY_ESTRU_CMP_POS_LOCAL"
			nValue := ARRAY_ESTRU_CMP_POS_LOCAL
		Case cInfo == "ARRAY_ESTRU_CMP_SIZE"
			nValue := ARRAY_ESTRU_CMP_SIZE
		Case cInfo == "ARRAY_ESTRU_ALT_POS_ALTERN"
			nValue := ARRAY_ESTRU_ALT_POS_ALTERN
		Case cInfo == "ARRAY_ESTRU_ALT_POS_TIPOCONV"
			nValue := ARRAY_ESTRU_ALT_POS_TIPOCONV
		Case cInfo == "ARRAY_ESTRU_ALT_POS_FATORCONV"
			nValue := ARRAY_ESTRU_ALT_POS_FATORCONV
		Case cInfo == "ARRAY_ESTRU_ALT_POS_VIGENCIA"
			nValue := ARRAY_ESTRU_ALT_POS_VIGENCIA
		Case cInfo == "ARRAY_ESTRU_ALT_POS_ESTOQUE"
			nValue := ARRAY_ESTRU_ALT_POS_ESTOQUE
		Case cInfo == "ARRAY_ESTRU_ALT_POS_SEQUENCIA"
			nValue := ARRAY_ESTRU_ALT_POS_SEQUENCIA
		Case cInfo == "ARRAY_ESTRU_ALT_SIZE"
			nValue := ARRAY_ESTRU_ALT_SIZE
		Otherwise
			nValue := ARRAY_ESTRU_CAB_SIZE
	EndCase
Return nValue

/*/{Protheus.doc} PCPA200MRP
Identifica os dados que sofreram altera��o e executa a integra��o
com o novo MRP.

@type  Function
@author lucas.franca
@since 11/07/2019
@version P12.1.27
@param oEvent    , Object   , Objeto do evento default do programa PCPA200
@param oModel    , Object   , Objeto do modelo de dados da tela.
@param oTTPend   , Object   , Objeto da tabela tempor�ria utilizada no processamento das pend�ncias. Tabela criada pela fun��o TTPendMRP
@param oIntegra  , Object   , Objeto com c�digos de produtos pais para executar a integra��o de inclus�o/atualiza��o. (opcional)
@param cOperation, Character, Opera��o executada ("INSERT"/"SYNC"/"DELETE")
@param aSuccess  , Array    , Carrega os registros que foram integrados com sucesso
@param aError    , Array    , Carrega os registros que n�o foram integrados por erro
@param lOnlyDel  , Logic    , Indica que est� sendo executada uma opera��o de Sincroniza��o apenas excluindo os dados existentes (envia somente filial).
@return Nil
/*/
Function PCPA200MRP(oEvent, oModel, oTTPend, oIntegra, cOperation, aSuccess, aError, lOnlyDel)
	Local aChaves    := {}
	Local aDadosCab  := {}
	Local aDadosCmp  := {}
	Local aDadosInc  := {}
	Local aDadosDel  := {}
	Local cChave     := ""
	Local cFil       := ""
	Local cPai       := ""
	Local lDeleta    := .F.
	Local nOpcao     := 0
	Local nIndex     := 0
	Local nTotal     := 0
	Local oLinDel    := Nil
	Local oLines     := Nil
	Local oFields    := Nil
	Local oPaiInt    := JsonObject():New()

	Default cOperation := "INSERT"
	Default aSuccess   := {}
	Default aError     := {}
	Default lOnlyDel   := .F.

	If oEvent == Nil .And. oIntegra == Nil
		Return
	EndIf

	If oEvent != Nil
		oLinDel := oEvent:oDadosCommit["oLinDel"]
		oLines  := oEvent:oDadosCommit["oLines"]
		oFields := oEvent:oDadosCommit["oFields"]
	EndIf

	If oModel != Nil
		If oModel:GetOperation() == MODEL_OPERATION_DELETE
			lDeleta := .T.
		EndIf
	EndIf

	//Busca os dados de Atualiza��o/Inclus�o/Exclus�o
	If lDeleta
		//Monta array com os dados.
		aDadosCab := Array(ARRAY_ESTRU_CAB_SIZE)
		//Dados do cabe�alho
		aDadosCab[ARRAY_ESTRU_CAB_POS_FILIAL] := oModel:GetModel("SG1_DETAIL"):GetValue("G1_FILIAL")
		aDadosCab[ARRAY_ESTRU_CAB_POS_PAI   ] := oModel:GetModel("SG1_MASTER"):GetValue("G1_COD")

		aAdd(aDadosDel, aDadosCab)
	Else
		If oIntegra == Nil
			oIntegra := JsonObject():New()
			aChaves  := oLines:GetNames()
			nTotal   := Len(aChaves)
			For nIndex := 1 To nTotal
				cChave := aChaves[nIndex]

				//Prote��o para n�o gravar um registro que tenha ficado inv�lido
				If oLines[cChave] == Nil;
				   .Or. Empty(oLines[cChave][oFields["G1_COD"]]);
				   .Or. Empty(oLines[cChave][oFields["G1_COMP"]])
					Loop
				EndIf

				cFil := oLines[cChave][oFields["G1_FILIAL"]]
				cPai := oLines[cChave][oFields["G1_COD"]]
				If Empty(cFil)
					cFil := xFilial("SG1")
				EndIf

				If oIntegra[cFil+cPai] == Nil
					oIntegra[cFil+cPai] := 0
				EndIf
				oIntegra[cFil+cPai]++
			Next nIndex
		EndIf

		aDadosInc := cargaSG1(oIntegra, oModel, lOnlyDel)

		//Busca os dados de Exclus�o.
		//A exclus�o � feita somente quando revis�o manual.
		If oEvent != Nil .And. !oEvent:mvlRevisaoAutomatica
			oPaiInt := JsonObject():New()
			aChaves := oLinDel:GetNames()
			nTotal  := Len(aChaves)
			For nIndex := 1 To nTotal
				cChave := aChaves[nIndex]
				If oLinDel[cChave] == Nil .Or. !oLinDel[cChave] .Or. oLines[cChave] == Nil .Or. oLines[cChave][oFields["NREG"]] <= 0
					Loop
				EndIf
				If oPaiInt[oLines[cChave][oFields["G1_COD"]]] == Nil
					//Monta array com os dados.
					aDadosCab := Array(ARRAY_ESTRU_CAB_SIZE)
					//Dados do cabe�alho
					aDadosCab[ARRAY_ESTRU_CAB_POS_FILIAL] := oLines[cChave][oFields["G1_FILIAL"]]
					aDadosCab[ARRAY_ESTRU_CAB_POS_PAI   ] := oLines[cChave][oFields["G1_COD"]]
					aDadosCab[ARRAY_ESTRU_CAB_POS_COMPON] := {}
					oPaiInt[oLines[cChave][oFields["G1_COD"]]] := aClone(aDadosCab)
				EndIf

				aDadosCmp := Array(ARRAY_ESTRU_CMP_SIZE)
				aDadosCmp[ARRAY_ESTRU_CMP_POS_RECNO] := oLines[cChave][oFields["NREG"]]

				aAdd(oPaiInt[oLines[cChave][oFields["G1_COD"]]][ARRAY_ESTRU_CAB_POS_COMPON], aClone(aDadosCmp))

			Next nIndex

			aChaves := oPaiInt:GetNames()
			nTotal  := Len(aChaves)
			For nIndex := 1 To nTotal
				aAdd(aDadosDel, aClone(oPaiInt[aChaves[nIndex]]))
			Next nIndex
		EndIf
	EndIf

	If Len(aDadosDel) > 0
		If oModel:GetOperation() == MODEL_OPERATION_DELETE
			nOpcao := 1
		Else
			nOpcao := 2
		EndIf
		PCPA200INT("DELETE", aDadosDel, oTTPend, nOpcao, @aSuccess, @aError, lOnlyDel)
	EndIf

	If Len(aDadosInc) > 0 .OR. cOperation == "SYNC"
		PCPA200INT(cOperation, aDadosInc, oTTPend, Nil, @aSuccess, @aError, lOnlyDel)
	EndIf

	aSize(aChaves  , 0)
	aSize(aDadosCab, 0)
	aSize(aDadosCmp, 0)
	aSize(aDadosInc, 0)
	aSize(aDadosDel, 0)

	If oPaiInt != Nil
		FreeObj(oPaiInt)
	EndIf
	If oIntegra != Nil
		FreeObj(oIntegra)
	EndIf
Return Nil

/*/{Protheus.doc} getQtdBase
Retorna a quantidade base de um produto.

@type  Static Function
@author lucas.franca
@since 11/07/2019
@version P12.1.27
@param oModel   , Object   , Modelo de dados da tela do PCPA200
@param cProduct , Character, C�digo do produto.
@return nQtdBase, Numeric  , Quantidade base do produto
/*/
Static Function getQtdBase(oModel, cProduct)
	Local nQtdBase := 1

	If oModel != Nil .And. cProduct == oModel:GetModel("SG1_MASTER"):GetValue("G1_COD")
		nQtdBase := oModel:GetModel("SG1_MASTER"):GetValue("NQTBASE")
	Else
		SB1->(dbSetOrder(1))
		If SB1->(dbSeek(xFilial("SB1")+cProduct))
			nQtdBase := RetFldProd(cProduct,"B1_QB")
		EndIf
	EndIf
Return nQtdBase

/*/{Protheus.doc} A200APIAlt
Faz a carga dos produtos alternativos de um componente da estrutura.

@type  Static Function
@author lucas.franca
@since 12/07/2019
@version P12.1.27
@param cPai , Character, C�digo do produto PAI
@param cComp, Character, C�digo do produto componente
@return aAltern, Array, Array com os dados dos produtos alternativos.
/*/
Function A200APIAlt(cPai, cComp)
	Local aAltern := {}
	Local cChave  := ""
	Local nPos    := 0

	SGI->(dbSetOrder(1))
	cChave := xFilial("SGI") + cComp
	If SGI->(dbSeek(cChave))
		While SGI->(!Eof()) .And. SGI->GI_FILIAL+SGI->GI_PRODORI == cChave
			aAdd(aAltern, Array(ARRAY_ESTRU_ALT_SIZE))
			nPos++

			aAltern[nPos][ARRAY_ESTRU_ALT_POS_ALTERN   ] := SGI->GI_PRODALT
			aAltern[nPos][ARRAY_ESTRU_ALT_POS_SEQUENCIA] := SGI->GI_ORDEM
			aAltern[nPos][ARRAY_ESTRU_ALT_POS_TIPOCONV ] := SGI->GI_TIPOCON
			aAltern[nPos][ARRAY_ESTRU_ALT_POS_FATORCONV] := SGI->GI_FATOR
			aAltern[nPos][ARRAY_ESTRU_ALT_POS_VIGENCIA ] := SGI->GI_DATA
			aAltern[nPos][ARRAY_ESTRU_ALT_POS_ESTOQUE  ] := SGI->GI_ESTOQUE
			SGI->(dbSkip())
		End
	EndIf
Return aAltern

/*/{Protheus.doc} PCPA200INT
Fun��o que executa a integra��o das estruturas com o MRP.

@type  Function
@author lucas.franca
@since 16/07/2019
@version P12.1.27
@param cOperacao, Character, Opera��o que ser� executada ('DELETE'/'INSERT'/'SYNC')
@param aDados   , Array    , Array com os dados que devem ser integrados com o MRP.
@param oTTPend  , Object   , Objeto da tabela tempor�ria utilizada no processamento das pend�ncias. Tabela criada pela fun��o TTPendMRP
@param nOpcao   , Numeric  , Op��o para exclus�o de estruturas. (1=Estrutura inteira; 2=Componente da estrutura; 3=Alternativo do componente)
@param aSuccess , Array    , Carrega os registros que foram integrados com sucesso
@param aError   , Array    , Carrega os registros que n�o foram integrados por erro
@param lOnlyDel , Logic    , Indica que est� sendo executada uma opera��o de Sincroniza��o apenas excluindo os dados existentes (envia somente filial).
@return Nil
/*/
Function PCPA200INT(cOperation, aDados, oTTPend, nOpcao, aSuccess, aError, lOnlyDel)
	Local aReturn   := {}
	Local aAltern   := {}
	Local aCompon   := {}
	Local cApi      := "MRPBILLOFMATERIAL"
	Local lAllError := .F.
	Local nIndex    := 0
	Local nIndCmp   := 0
	Local nIndAlt   := 0
	Local nTotal    := 0
	Local nTotCmp   := 0
	Local nTotAlt   := 0
	Local oJsonData := Nil
	Local oJsonCmp  := Nil
	Local oJsonAlt  := Nil

	Default nOpcao   := 2
	Default aSuccess := {}
	Default aError   := {}
	Default lOnlyDel := .F.

	nTotal := Len(aDados)
	oJsonData := JsonObject():New()

	oJsonData["items"] := Array(nTotal)

	For nIndex := 1 To nTotal
		//Monta o cabe�alho
		oJsonData["items"][nIndex] := JsonObject():New()
		oJsonData["items"][nIndex]["branchId"] := aDados[nIndex][ARRAY_ESTRU_CAB_POS_FILIAL]
		
		If !(cOperation == "SYNC" .And. lOnlyDel)
			oJsonData["items"][nIndex]["product" ] := aDados[nIndex][ARRAY_ESTRU_CAB_POS_PAI   ]
			If cOperation $ "|INSERT|SYNC|"
				oJsonData["items"][nIndex]["itemAmount"] := aDados[nIndex][ARRAY_ESTRU_CAB_POS_QBASE]
			EndIf

			//Adiciona os componentes
			If aDados[nIndex][ARRAY_ESTRU_CAB_POS_COMPON] != Nil .And. Len(aDados[nIndex][ARRAY_ESTRU_CAB_POS_COMPON]) > 0
				nTotCmp := Len(aDados[nIndex][ARRAY_ESTRU_CAB_POS_COMPON])

				oJsonData["items"][nIndex]["listOfMRPComponents"] := Array(nTotCmp)
				For nIndCmp := 1 To nTotCmp
					aCompon := aDados[nIndex][ARRAY_ESTRU_CAB_POS_COMPON][nIndCmp]

					oJsonData["items"][nIndex]["listOfMRPComponents"][nIndCmp] := JsonObject():New()
					oJsonCmp := oJsonData["items"][nIndex]["listOfMRPComponents"][nIndCmp]

					oJsonCmp["component"            ] := aCompon[ARRAY_ESTRU_CMP_POS_COMP]
					oJsonCmp["sequence"             ] := aCompon[ARRAY_ESTRU_CMP_POS_SEQ]
					oJsonCmp["startRevison"         ] := aCompon[ARRAY_ESTRU_CMP_POS_REVINI]
					oJsonCmp["endRevison"           ] := aCompon[ARRAY_ESTRU_CMP_POS_REVFIM]
					oJsonCmp["quantity"             ] := aCompon[ARRAY_ESTRU_CMP_POS_QTDNEC]
					oJsonCmp["startDate"            ] := convDate(aCompon[ARRAY_ESTRU_CMP_POS_VLDINI])
					oJsonCmp["endDate"              ] := convDate(aCompon[ARRAY_ESTRU_CMP_POS_VLDFIM])
					oJsonCmp["percentageScrap"      ] := aCompon[ARRAY_ESTRU_CMP_POS_PERDA]
					oJsonCmp["fixedQuantity"        ] := getQtdFixa(aCompon[ARRAY_ESTRU_CMP_POS_QTDFIXA])
					oJsonCmp["optionalGroup"        ] := aCompon[ARRAY_ESTRU_CMP_POS_GRPOPC]
					oJsonCmp["optionalItem"         ] := aCompon[ARRAY_ESTRU_CMP_POS_ITEMOPC]
					oJsonCmp["potency"              ] := aCompon[ARRAY_ESTRU_CMP_POS_POTENC]
					oJsonCmp["warehouse"            ] := aCompon[ARRAY_ESTRU_CMP_POS_LOCAL]
					oJsonCmp["isGhostMaterial"      ] := aCompon[ARRAY_ESTRU_CMP_POS_FANTASM]
					oJsonCmp["code"                 ] := aDados[nIndex][ARRAY_ESTRU_CAB_POS_FILIAL] + cValToChar(aCompon[ARRAY_ESTRU_CMP_POS_RECNO])
					//Adiciona os alternativos se existir.
					If aCompon[ARRAY_ESTRU_CMP_POS_ALTERNATIVO] != Nil .And. Len(aCompon[ARRAY_ESTRU_CMP_POS_ALTERNATIVO]) > 0
						nTotAlt := Len(aCompon[ARRAY_ESTRU_CMP_POS_ALTERNATIVO])
						oJsonCmp["listOfMRPAlternatives"] := Array(nTotAlt)
						For nIndAlt := 1 To nTotAlt
							aAltern := aCompon[ARRAY_ESTRU_CMP_POS_ALTERNATIVO][nIndAlt]
							oJsonCmp["listOfMRPAlternatives"][nIndAlt] := JsonObject():New()
							oJsonAlt := oJsonCmp["listOfMRPAlternatives"][nIndAlt]

							oJsonAlt["alternative"     ] := aAltern[ARRAY_ESTRU_ALT_POS_ALTERN]
							oJsonAlt["conversionType"  ] := getConvType(aAltern[ARRAY_ESTRU_ALT_POS_TIPOCONV])
							oJsonAlt["conversionFactor"] := aAltern[ARRAY_ESTRU_ALT_POS_FATORCONV]
							oJsonAlt["vigency"         ] := convDate(aAltern[ARRAY_ESTRU_ALT_POS_VIGENCIA])
							oJsonAlt["inventory"       ] := aAltern[ARRAY_ESTRU_ALT_POS_ESTOQUE]
							oJsonAlt["sequence"        ] := aAltern[ARRAY_ESTRU_ALT_POS_SEQUENCIA]
						Next nIndAlt
					EndIf
				Next nIndCmp
			EndIf
		EndIf
	Next nIndex

	If cOperation $ "|INSERT|SYNC|"
		If cOperation == "INSERT"
			aReturn := MrpBOMPost(oJsonData)
		Else
			aReturn := MrpBOMSync(oJsonData)
		EndIf
		PrcPendMRP(aReturn, cApi, oJsonData, oTTPend, .F., @aSuccess, @aError, @lAllError, '1')
	Else
		aReturn := MrpEstrDel(oJsonData, nOpcao)
		PrcPendMRP(aReturn, cApi, oJsonData, oTTPend, .F., @aSuccess, @aError, @lAllError, '2')
	EndIf

	FreeObj(oJsonData)
	oJsonData := Nil

	aSize(aReturn , 0)
	aSize(aAltern , 0)
	aSize(aCompon , 0)

	If oJsonData != Nil
		FreeObj(oJsonData)
		oJsonData := Nil
	EndIf
	If oJsonCmp != Nil
		FreeObj(oJsonCmp)
		oJsonCmp := Nil
	EndIf
	If oJsonAlt != Nil
		FreeObj(oJsonAlt)
		oJsonAlt := Nil
	EndIf

Return Nil

/*/{Protheus.doc} getQtdFixa
Transforma o valor da quantidade fixa da SG1 para o valor correspondente
que deve ser enviado para a API

@type  Static Function
@author lucas.franca
@since 16/07/2019
@version P12.1.27
@param cG1FixVar, Character, Valor do campo G1_FIXVAR para realizar convers�o ao valor recebido na API
@return cFixVar, Character, Valor convertido para o esperado na API
/*/
Static Function getQtdFixa(cG1FixVar)
	Local cFixVar := "2"

	If cG1FixVar == "F"
		cFixVar := "1"
	EndIf
Return cFixVar

/*/{Protheus.doc} getConvType
Transforma o valor do fator de convers�o da SGI para o valor correspondente
que deve ser enviado para a API

@type  Static Function
@author lucas.franca
@since 16/07/2019
@version P12.1.27
@param cGIConvType, Character, Valor do campo GI_TIPOCON para realizar convers�o ao valor recebido na API
@return cConvType , Character, Valor convertido para o esperado na API
/*/
Static Function getConvType(cGIConvType)
	Local cConvType := "1"

	If cGIConvType == "D"
		cConvType := "2"
	EndIf
Return cConvType

/*/{Protheus.doc} convDate
Converte uma data do tipo DATE para o formato string AAAA-MM-DD

@type  Static Function
@author lucas.franca
@since 13/05/2019
@version P12.1.25
@param dData, Date, Data que ser� convertida
@return cData, Caracter, Data convertida para o formato utilizado na integra��o.
/*/
Static Function convDate(dData)
	Local cData := ""
	If !Empty(dData)
		cData := StrZero(Year(dData),4) + "-" + StrZero(Month(dData),2) + "-" + StrZero(Day(dData),2)
	EndIf
Return cData

/*/{Protheus.doc} cargaSG1
Faz a carga dos dados da SG1, buscando direto na tabela.
Ir� fazer a busca de todos os c�digos pais recebidos no objeto
json oIntegra.

@type  Static Function
@author lucas.franca
@since 16/07/2019
@version P12.1.27
@param oIntegra, Object, Objeto JSON com os c�digos PAIS que devem ser carregados.
@param oModel  , Object, Objeto do modelo de dados do PCPA200
@param lOnlyDel, Logic , Indica que est� sendo executada uma opera��o de Sincroniza��o apenas excluindo os dados existentes (envia somente filial).
@return aDadosInc, Array, Array com os dados da estrutura.
/*/
Static Function cargaSG1(oIntegra, oModel, lOnlyDel)
	Local aChaves    := {}
	Local aDadosInc  := {}
	Local aDadosCab  := {}
	Local aDadosCmp  := {}
	Local cChave     := ""
	Local cFil       := ""
	Local cPai       := ""
	Local cQuery     := ""
	Local cAliasQry  := ""
	Local nIndex     := 0
	Local nTotal     := 0
	Local nTamFilial := FwSizeFilial()
	Local nTamProd   := GetSx3Cache("G1_COD"  , "X3_TAMANHO")
	Local nTamQtd    := GetSx3Cache("G1_QUANT", "X3_TAMANHO")
	Local nDecQtd    := GetSx3Cache("G1_QUANT", "X3_DECIMAL")
	Local nTamPerda  := GetSx3Cache("G1_PERDA", "X3_TAMANHO")
	Local nDecPerda  := GetSx3Cache("G1_PERDA", "X3_DECIMAL")
	Local oQuery     := Nil
	Local cPrdTable  := SuperGetMv("MV_ARQPROD",.F.,"SB1")

	Default lOnlyDel := .F.

	aChaves := oIntegra:GetNames()
	nTotal  := Len(aChaves)

	For nIndex := 1 To nTotal
		cChave := aChaves[nIndex]
		If oIntegra[cChave] == Nil .Or. oIntegra[cChave] <= 0
			Loop
		EndIf

		cFil := Left(cChave, nTamFilial)
		cPai := Right(cChave, nTamProd)

		//Monta array com os dados.
		aDadosCab := Array(ARRAY_ESTRU_CAB_SIZE)
		//Dados do cabe�alho
		aDadosCab[ARRAY_ESTRU_CAB_POS_FILIAL] := cFil
		If ! (lOnlyDel .And. Empty(cPai))
			aDadosCab[ARRAY_ESTRU_CAB_POS_PAI   ] := cPai
			aDadosCab[ARRAY_ESTRU_CAB_POS_QBASE ] := getQtdBase(oModel, cPai)
			aDadosCab[ARRAY_ESTRU_CAB_POS_COMPON] := {}

			If Empty(aDadosCab[ARRAY_ESTRU_CAB_POS_FILIAL])
				aDadosCab[ARRAY_ESTRU_CAB_POS_FILIAL] := xFilial("SG1")
			EndIf

			If oQuery == Nil
				If cPrdTable == "SBZ"

					cQuery := "SELECT SG1.G1_COMP,"
					cQuery += "	   SG1.G1_TRT,"
					cQuery += "	   SG1.G1_QUANT,"
					cQuery += "	   SG1.G1_INI,"
					cQuery += "	   SG1.G1_FIM,"
					cQuery += "	   SG1.G1_REVINI,"
					cQuery += "	   SG1.G1_REVFIM,"
					cQuery += "	   SG1.G1_PERDA,"
					cQuery += "	   SG1.G1_FIXVAR,"
					cQuery += "	   SG1.G1_POTENCI,"
					cQuery += "	   SG1.G1_GROPC,"
					cQuery += "	   SG1.G1_OPC,"
					cQuery += "	   SG1.G1_LOCCONS,"

					cQuery += "	   CASE "
					cQuery += "		  WHEN SG1.G1_FANTASM = ' ' "
					cQuery += "		  THEN"
					cQuery += "			  CASE "
					cQuery += "				  WHEN SBZ.BZ_FANTASM = 'S' "
					cQuery += "				  THEN 'T' "
					cQuery += "				  WHEN SBZ.BZ_FANTASM = 'N' "
					cQuery += "				  THEN 'F' "
					cQuery += "				  ELSE CASE"
					cQuery += "						  WHEN SB1.B1_FANTASM = 'S' "
					cQuery += "						  THEN 'T'"
					cQuery += "						  WHEN SB1.B1_FANTASM = 'N' "
					cQuery += "						  THEN 'F'"
					cQuery += "						  ELSE 'F'"
					cQuery += "					    END"
					cQuery += "			  END "
					cQuery += "		WHEN SG1.G1_FANTASM = '1'"
					cQuery += "			THEN 'T'"
					cQuery += "			ELSE 'F'"
					cQuery += "	END AS FANTASMA,"

					cQuery += "	SG1.R_E_C_N_O_ AS RECSG1,"
					cQuery += "	(SELECT COUNT(SGI.GI_FILIAL) FROM "+RetSqlName("SGI")+" SGI  WHERE  SGI.GI_FILIAL = '"+xFilial("SGI")+"' AND SGI.GI_PRODORI = SG1.G1_COMP AND SGI.D_E_L_E_T_ = ' ' )  AS ALTERNATIVOS"
					cQuery += "	FROM "+RetSqlName("SG1")+" SG1 INNER JOIN "+RetSqlName("SB1")+" SB1"
					cQuery += "	ON B1_FILIAL = '"+xFilial("SB1")+"' AND SB1.B1_COD = SG1.G1_COMP AND SB1.D_E_L_E_T_ = ' '"

					cQuery += "	LEFT OUTER JOIN "+RetSqlName("SBZ")+" SBZ"
					cQuery += "	   ON SBZ.BZ_FILIAL  = '"+xFilial("SBZ")+"' AND SB1.B1_COD =  SBZ.BZ_COD AND SBZ.D_E_L_E_T_ = ' '"

					cQuery += "	WHERE  SG1.G1_FILIAL = ? "
					cQuery += "	  AND SG1.D_E_L_E_T_ = ' '"
					cQuery += "	  AND SG1.G1_COD     = ? "

				Else
					cQuery := " SELECT SG1.G1_COMP, "
					cQuery +=        " SG1.G1_TRT, "
					cQuery +=        " SG1.G1_QUANT, "
					cQuery +=        " SG1.G1_INI, "
					cQuery +=        " SG1.G1_FIM, "
					cQuery +=        " SG1.G1_REVINI, "
					cQuery +=        " SG1.G1_REVFIM, "
					cQuery +=        " SG1.G1_PERDA, "
					cQuery +=        " SG1.G1_FIXVAR, "
					cQuery +=        " SG1.G1_POTENCI, "
					cQuery +=        " SG1.G1_GROPC, "
					cQuery +=        " SG1.G1_OPC, "
					cQuery +=        " SG1.G1_LOCCONS, "
					cQuery +=        " CASE WHEN SG1.G1_FANTASM = ' ' THEN "
					cQuery +=                  " CASE WHEN (SELECT SB1.B1_FANTASM "
					cQuery +=                               " FROM " + RetSqlName("SB1") + " SB1 "
					cQuery +=                              " WHERE SB1.B1_FILIAL  = '" + xFilial("SB1") + "' "
					cQuery +=                                " AND SB1.D_E_L_E_T_ = ' ' "
					cQuery +=                                " AND SB1.B1_COD     = SG1.G1_COMP ) = 'S' THEN 'T' "
					cQuery +=                  " ELSE 'F' "
					cQuery +=                  " END "
					cQuery +=             " WHEN SG1.G1_FANTASM = '1' THEN 'T' "
					cQuery +=             " ELSE 'F' "
					cQuery +=        " END AS FANTASMA, "
					cQuery +=        " SG1.R_E_C_N_O_ AS RECSG1, "
					cQuery +=        " (SELECT COUNT(SGI.GI_FILIAL) "
					cQuery +=           " FROM " + RetSqlName("SGI") + " SGI "
					cQuery +=          " WHERE SGI.GI_FILIAL  = '" + xFilial("SGI") + "' "
					cQuery +=            " AND SGI.GI_PRODORI = SG1.G1_COMP "
					cQuery +=            " AND SGI.D_E_L_E_T_ = ' ' ) AS ALTERNATIVOS "
					cQuery +=   " FROM " + RetSqlName("SG1") + " SG1 "
					cQuery +=  " WHERE SG1.G1_FILIAL  = ? "
					cQuery +=    " AND SG1.D_E_L_E_T_ = ' ' "
					cQuery +=    " AND SG1.G1_COD     = ? "
				EndIf

				cQuery := ChangeQuery(cQuery)
				oQuery := FWPreparedStatement():New(cQuery)
			EndIf

			oQuery:SetString(1,cFil)
			oQuery:SetString(2,cPai)

			cQuery := oQuery:GetFixQuery()

			cAliasQry := GetNextAlias()
			dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cQuery),cAliasQry,.T.,.T.)
			TcSetField(cAliasQry, "G1_INI"  , "D", 8        , 0)
			TcSetField(cAliasQry, "G1_FIM"  , "D", 8        , 0)
			TcSetField(cAliasQry, "G1_QUANT", "N", nTamQtd  , nDecQtd)
			TcSetField(cAliasQry, "G1_PERDA", "N", nTamPerda, nDecPerda)

			While (cAliasQry)->(!Eof())

				aDadosCmp := Array(ARRAY_ESTRU_CMP_SIZE)
				aDadosCmp[ARRAY_ESTRU_CMP_POS_COMP   ] := (cAliasQry)->G1_COMP
				aDadosCmp[ARRAY_ESTRU_CMP_POS_SEQ    ] := (cAliasQry)->G1_TRT
				aDadosCmp[ARRAY_ESTRU_CMP_POS_QTDNEC ] := (cAliasQry)->G1_QUANT
				aDadosCmp[ARRAY_ESTRU_CMP_POS_VLDINI ] := (cAliasQry)->G1_INI
				aDadosCmp[ARRAY_ESTRU_CMP_POS_VLDFIM ] := (cAliasQry)->G1_FIM
				aDadosCmp[ARRAY_ESTRU_CMP_POS_REVINI ] := (cAliasQry)->G1_REVINI
				aDadosCmp[ARRAY_ESTRU_CMP_POS_REVFIM ] := (cAliasQry)->G1_REVFIM
				aDadosCmp[ARRAY_ESTRU_CMP_POS_PERDA  ] := (cAliasQry)->G1_PERDA
				aDadosCmp[ARRAY_ESTRU_CMP_POS_QTDFIXA] := (cAliasQry)->G1_FIXVAR
				aDadosCmp[ARRAY_ESTRU_CMP_POS_POTENC ] := (cAliasQry)->G1_POTENCI
				aDadosCmp[ARRAY_ESTRU_CMP_POS_GRPOPC ] := (cAliasQry)->G1_GROPC
				aDadosCmp[ARRAY_ESTRU_CMP_POS_ITEMOPC] := (cAliasQry)->G1_OPC
				aDadosCmp[ARRAY_ESTRU_CMP_POS_LOCAL  ] := (cAliasQry)->G1_LOCCONS
				aDadosCmp[ARRAY_ESTRU_CMP_POS_FANTASM] := Iif(AllTrim((cAliasQry)->FANTASMA) == "T", .T., .F.)
				aDadosCmp[ARRAY_ESTRU_CMP_POS_RECNO  ] := (cAliasQry)->RECSG1
				If (cAliasQry)->ALTERNATIVOS > 0
					aDadosCmp[ARRAY_ESTRU_CMP_POS_ALTERNATIVO] := A200APIAlt(cPai, (cAliasQry)->G1_COMP)
				Else
					aDadosCmp[ARRAY_ESTRU_CMP_POS_ALTERNATIVO] := {}
				EndIf
				aAdd(aDadosCab[ARRAY_ESTRU_CAB_POS_COMPON], aClone(aDadosCmp))

				(cAliasQry)->(dbSkip())
			End
			(cAliasQry)->(dbCloseArea())
		EndIf
		aAdd(aDadosInc, aClone(aDadosCab))
	Next nIndex

	aSize(aDadosCab, 0)
	aSize(aDadosCmp, 0)
	aSize(aChaves  , 0)
	If oQuery != Nil
		oQuery:Destroy()
		oQuery := Nil
	EndIf
Return aDadosInc
