#INCLUDE "TOTVS.CH"

/*/{Protheus.doc} PCPA200Rec
Fun��o para realiza��o de valida��es de recursividade (loop) nas estruturas (PCPA200)

@author brunno.costa
@since 26/05/2020
@version P12

@param 01 - cCodPesq    , caracter, c�digo do item para pesquisa
@param 02 - cCodValid   , caracter, c�digo do componente a ser comparado
@param 03 - cMsgEstr    , caracter, (RETORNO POR REFERENCIA) caminho da estrutura que ficar� inconsistente
@param 04 - oDadosCommit, objeto  , Json com os dados para commit
@param 05 - cFilSG1		, caracter, filial utilizada no seek da tabela SG1.
@param 06 - lPai        , l�gico  , identifica se a valida��o � para o produto pai. Nesse caso, faz a busca no sentido PAI->COMPON
                                    verificando se o G1_COMP � igual ao cCodValid.
                                    Se n�o for o produto pai, faz a busca no sentido COMPON->PAI, verificando se o G1_COD � igual ao cCodValid.
@return lExiste		- Indica se o iem � usado na estrutura
/*/

Function PCPA200Rec(cCodPesq, cCodValid, cMsgEstr, oDadosCommit, cFilSG1, lPai)

	Local cAliasTop    := GetNextAlias()
	Local cBanco       := TCGetDB()
	Local cQuery       := ""
	Local cQryUniAll   := ""
	Local lExiste      := .F.
	Local lQueryOk     := .F.
	Local oTempTable   := criaTmpTRB()
	Local cDirecao     := " > "
	Local cCplWhere    := ""
	Local cCplOnJoin   := ""
	Local cCpoCompar   := ""
	Local oOldError

	//Prote��o contra SQL Injection
	cCodValid  := StrTran(cCodValid,  "'", "")

	If lPai
		cCplWhere    := " (SG1_Base.G1_COD  = '" + cCodPesq + "')"
		cCplOnJoin   := " ON Qry_Recurs.G1_COMP = SG1_Rec.G1_COD  "
		cCpoCompar   := "G1_COMP"
	Else
		cCplWhere    := " (SG1_Base.G1_COMP = '" + cCodPesq + "')"
		cCplOnJoin   := " ON Qry_Recurs.G1_COD  = SG1_Rec.G1_COMP "
		cCpoCompar   := "G1_COD"
	EndIf

	cQuery := " WITH EstruturaRecursiva(G1_COD, G1_COMP, PathCod)"
	cQuery += " AS ("
	cQuery +=      " SELECT SG1_Base.G1_COD,"
	cQuery +=             " SG1_Base.G1_COMP,"
	cQuery +=             " Cast( Trim(SG1_Base.G1_COD) || '" + cDirecao + "' || Trim(SG1_Base.G1_COMP) AS VarChar(8000) ) AS PathCod"
	cQuery +=      " FROM ( cQryMemFro ) SG1_Base"
	cQuery +=      " WHERE "
	cQuery += cCplWhere

	cQryUniAll +=       " UNION ALL"
	cQryUniAll +=       " SELECT SG1_Rec.G1_COD,"
	cQryUniAll +=              " SG1_Rec.G1_COMP,"
	cQryUniAll +=             " Cast( (Qry_Recurs.PathCod || '" + cDirecao + "' || Trim(SG1_Rec.G1_COMP) ) AS VarChar(8000) ) PathCod"
	cQryUniAll +=        " FROM " + RetSqlName( "SG1" ) + " SG1_Rec "
	cQryUniAll +=             " INNER JOIN EstruturaRecursiva Qry_Recurs"
	cQryUniAll += cCplOnJoin
	cQryUniAll +=       " WHERE SG1_Rec.D_E_L_E_T_ = ' '"
	cQryUniAll +=         " AND SG1_Rec.G1_FILIAL  = '" + cFilSG1 + "' "

	//Tratamento dados em memoria
	PesMemoria(@cQryUniAll, @cQuery, oTempTable, oDadosCommit, cFilSG1)

	cQuery +=   " )"
	cQuery += " SELECT DISTINCT Resultado.PathCod "
	cQuery +=   " FROM EstruturaRecursiva Resultado"
	cQuery +=  " WHERE Resultado." + cCpoCompar + " = '" + cCodValid + "'"

	//Realiza ajustes da Query para cada banco
	If cBanco == "POSTGRES"

		//Altera sintaxe da clausula WITH
		cQuery := StrTran(cQuery, 'WITH ', 'WITH recursive ')

		//Corrige Falhas internas de Bin�rio - POSTGRES
		cQuery := StrTran(cQuery, CHR(13), " ")
		cQuery := StrTran(cQuery, CHR(10), " ")
		cQuery := StrTran(cQuery, CHR(09), " ")

	ElseIf cBanco == "MSSQL"
		//Substitui a fun��o Trim
		cQuery := StrTran(cQuery, "Trim(", "RTrim(")
		//Substitui concatena��o || por +
		cQuery := StrTran(cQuery, '||', '+')

	ElseIf cBanco != "ORACLE"
		//Substitui concatena��o || por +
		cQuery := StrTran(cQuery, '||', '+')

	EndIf

	If cBanco == "ORACLE"
		cQuery := StrTran(cQuery,"VarChar(8000)","VarChar(4000)")
	EndIf

	oOldError    := ErrorBlock( {|| .T. } )

	BEGIN SEQUENCE
		dbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAliasTop, .T., .T.)
		lQueryOk := .T.
		If Select(cAliasTop) == 0
			lQueryOk := .F.
		ElseIf !(cAliasTop)->(Eof())
			lExiste  := .T.
			cMsgEstr := AllTrim((cAliasTop)->PathCod)
		EndIf
		(cAliasTop)->(dbCloseArea())

	RECOVER
		lQueryOk := .F.

	END SEQUENCE

	ErrorBlock( oOldError )

	If !lQueryOk
		lExiste  := .T.
		If lPai
			cMsgEstr := AllTrim(cCodPesq)  + cDirecao + "..." + cDirecao + AllTrim(cCodPesq)
		Else
			cMsgEstr := AllTrim(cCodValid) + cDirecao + "..." + cDirecao + AllTrim(cCodValid)
		EndIf
	EndIf

	oTempTable:Delete()
	oTempTable := Nil

Return lExiste

/*/{Protheus.doc} PesMemoria
Realiza ajustes na query de pesquisa para considerar os dados em memoria

@author brunno.costa
@since 26/05/2020
@version P12.1.30

@param 01 - cQryUniAll  , caracter, trecho da query UNION ALL utilizada para copia em MSSQL
@param 02 - cQuery      , caracter, query recursiva de pesquisa original - retornada por referencia
@param 03 - oTempTable  , objeto  , objeto com a tabela temporaria
@param 04 - oDadosCommit, objeto  , Json com os dados para commit
@param 05 - cFilSG1		, caracter, filial utilizada no seek da tabela SG1.
@return oTempTable	- Objeto da tabela tempor�ria.
/*/
Static Function PesMemoria(cQryUniAll, cQuery, oTempTable, oDadosCommit, cFilSG1)

	Local cBanco       := TCGetDB()
	Local nIndAux      := 0
	Local cQryMemFro   := ""
	Local cQryMemCpl   := ""
	Local cRecnos      := ""
	Local oModel       := FWModelActive()
	Local oModelGrAt   := oModel:GetModel("SG1_DETAIL")
	Local cAliasTRB    := oTempTable:GetAlias()
	Local oLines       := oDadosCommit["oLines"]
	Local oFields      := oDadosCommit["oFields"]
	Local oLinDel      := oDadosCommit["oLinDel"]
	Local aLinhasMem   := oLines:GetNames()

	cQryMemFro := " SELECT G1_FILIAL, "
	cQryMemFro +=        " G1_COD, "
	cQryMemFro +=        " G1_REVINI, "
	cQryMemFro +=        " G1_REVFIM, "
	cQryMemFro +=        " G1_COMP, "
	cQryMemFro +=        " G1_TRT, "
	cQryMemFro +=        " G1_INI, "
	cQryMemFro +=        " G1_FIM "
	cQryMemFro +=   " FROM " + RetSqlName("SG1") + " "
	cQryMemFro +=  " WHERE D_E_L_E_T_ = ' ' "
	cQryMemFro +=    " AND G1_FILIAL  = '" + cFilSG1 + "' "
	cQryMemFro += " X_UpdRecnos_X "
	cQryMemFro += " UNION ALL "
	cQryMemFro += " SELECT G1_FILIAL, "
	cQryMemFro +=        " G1_COD, "
	cQryMemFro +=        " '   ' G1_REVINI, "
	cQryMemFro +=        " 'ZZZ' G1_REVFIM, "
	cQryMemFro +=        " G1_COMP, "
	cQryMemFro +=        " G1_TRT, "
	cQryMemFro +=        " G1_INI, "
	cQryMemFro +=        " G1_FIM "
	cQryMemFro +=   " FROM " + oTempTable:GetRealName()

	//Analise dos dados no JSON com os dados para commit - Dados alterados ainda nao gravados no banco
	For nIndAux := 1 to Len(aLinhasMem)
		If oLines[aLinhasMem[nIndAux]] != Nil
			//Tratamento de exclusoes
			If oLinDel[aLinhasMem[nIndAux]]
				If Empty(cRecnos)
					cRecnos := cValToChar(oLines[aLinhasMem[nIndAux]][oFields["NREG"]])
				Else
					cRecnos += ", " + cValToChar(oLines[aLinhasMem[nIndAux]][oFields["NREG"]])
				EndIf
			Else
				//Tratamento de inclusoes
				RecLock(cAliasTRB, .T.)
				(cAliasTRB)->G1_FILIAL := cFilSG1
				(cAliasTRB)->G1_COD    := oLines[aLinhasMem[nIndAux]][oFields["G1_COD"]]
				(cAliasTRB)->G1_REVINI := oLines[aLinhasMem[nIndAux]][oFields["G1_REVINI"]]
				(cAliasTRB)->G1_REVFIM := oLines[aLinhasMem[nIndAux]][oFields["G1_REVFIM"]]
				(cAliasTRB)->G1_INI    := oLines[aLinhasMem[nIndAux]][oFields["G1_INI"]]
				(cAliasTRB)->G1_FIM    := oLines[aLinhasMem[nIndAux]][oFields["G1_FIM"]]
				(cAliasTRB)->G1_COMP   := oLines[aLinhasMem[nIndAux]][oFields["G1_COMP"]]
				(cAliasTRB)->G1_TRT    := oLines[aLinhasMem[nIndAux]][oFields["G1_TRT"]]
				(cAliasTRB)->(MsUnLock())
			EndIf
		EndIf
	Next

	//Analise dos dados na SG1_DETAIL - Dados alterados(ou nao) exibidos na tela
	For nIndAux := 1 to oModelGrAt:Length(.F.)
		If !Empty(oModelGrAt:GetValue("G1_COD", nIndAux)) .AND. (oModelGrAt:IsUpdated(nIndAux) .OR. oModelGrAt:IsDeleted(nIndAux))
			//Tratamento de exclusoes
			If oModelGrAt:IsDeleted(nIndAux)
				If Empty(cRecnos)
					cRecnos := cValToChar(oModelGrAt:GetValue("NREG", nIndAux))
				Else
					cRecnos += ", " + cValToChar(oModelGrAt:GetValue("NREG", nIndAux))
				EndIf
			Else
				//Tratamento de inclusoes
				RecLock(cAliasTRB, .T.)
				(cAliasTRB)->G1_FILIAL := cFilSG1
				(cAliasTRB)->G1_COD    := oModelGrAt:GetValue("G1_COD"   , nIndAux)
				(cAliasTRB)->G1_REVINI := oModelGrAt:GetValue("G1_REVINI", nIndAux)
				(cAliasTRB)->G1_REVFIM := oModelGrAt:GetValue("G1_REVFIM", nIndAux)
				(cAliasTRB)->G1_INI    := oModelGrAt:GetValue("G1_INI"   , nIndAux)
				(cAliasTRB)->G1_FIM    := oModelGrAt:GetValue("G1_FIM"   , nIndAux)
				(cAliasTRB)->G1_COMP   := oModelGrAt:GetValue("G1_COMP"  , nIndAux)
				(cAliasTRB)->G1_TRT    := oModelGrAt:GetValue("G1_TRT"   , nIndAux)
				(cAliasTRB)->(MsUnLock())
			EndIf
		EndIf
	Next

	//SQL SERVER
	If cBanco == "MSSQL"
		cQryMemCpl := StrTran(cQryUniAll, "FROM " + RetSqlName( "SG1" ) + " SG1_Rec",;
																			"FROM (SELECT G1_FILIAL, G1_COD, '   ' G1_REVINI, 'ZZZ' G1_REVFIM, G1_COMP, G1_TRT, G1_INI, G1_FIM " +;
																			      "FROM " + oTempTable:GetRealName() + ") SG1_Rec")

		cQryMemCpl := StrTran(cQryMemCpl, "WHERE SG1_Rec.D_E_L_E_T_ = ' '", "")
		cQryUniAll +=         " SG1_Rec.X_UpdRecnos_X "
		cQryUniAll +=         " AND SG1_Rec.D_E_L_E_T_ = ' ' "

	//ORACLE, POSTGRES
	Else
		cQryUniAll := StrTran(cQryUniAll, "SG1_Rec.D_E_L_E_T_ = ' '", "1=1")
		cQryUniAll := StrTran(cQryUniAll, "FROM " + RetSqlName( "SG1" ) + " SG1_Rec",;
																			"FROM (SELECT G1_FILIAL, G1_COD, G1_REVINI, G1_REVFIM, G1_COMP, G1_TRT, G1_INI, G1_FIM "+;
																			      " FROM " + RetSqlName( "SG1" ) +;
																						" WHERE D_E_L_E_T_ = ' ' " +;
																						" AND G1_FILIAL = '" + cFilSG1 + "' " +;
																						" X_UpdRecnos_X " +;
																						" UNION " +;
																						" SELECT G1_FILIAL, G1_COD, '   ' G1_REVINI, 'ZZZ' G1_REVFIM, G1_COMP, G1_TRT, G1_INI, G1_FIM " +;
																			      " FROM " + oTempTable:GetRealName() + ") SG1_Rec")
	EndIf

	cQuery     += cQryUniAll + cQryMemCpl
	cQuery     := StrTran(cQuery, "cQryMemFro", cQryMemFro)

	If !Empty(cRecnos)
		cQuery := StrTran(cQuery, "SG1_Rec.X_UpdRecnos_X", " AND SG1_Rec.R_E_C_N_O_ NOT IN (" + cRecnos + ") ")
		cQuery := StrTran(cQuery, "X_UpdRecnos_X", " AND R_E_C_N_O_ NOT IN (" + cRecnos + ") ")
	Else
		cQuery := StrTran(cQuery, "SG1_Rec.X_UpdRecnos_X", "")
		cQuery := StrTran(cQuery, "X_UpdRecnos_X", "")
	EndIf
Return

/*/{Protheus.doc} criaTmpTRB
Cria a tabela tempor�ria conforme a estrutura da tabela SG1

@author brunno.costa
@since 26/05/2020
@version P12.1.30

@return oTempTable	- Objeto da tabela tempor�ria.
/*/
Static Function criaTmpTRB()
	Local oTempTable := FwTemporaryTable():New()

	oTempTable:SetFields(SG1->(dbStruct()))
	oTempTable:AddIndex("01",{"G1_FILIAL","G1_COD","G1_COMP","G1_TRT", "G1_REVINI", "G1_REVFIM"})
	oTempTable:Create()

Return oTempTable