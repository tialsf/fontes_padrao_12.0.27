#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "ProductionAppointment.ch"

WSRESTFUL ProductionAppointment DESCRIPTION "Servi�o REST para manipula��o do apontamento de produ��o"

WSDATA ProductionOrderNumber  AS String   
WSDATA appointmentType        AS String
WSDATA ActivityID             AS String
WSDATA ActivityCode           AS String
WSDATA MachineCode            AS String 
WSDATA Split                  AS String

WSMETHOD GET ProductionOrder	DESCRIPTION "Valida a ordem de produ��o e Recupera os dados da ordem de produ��o"	                     WSSYNTAX "/v1/ProductionOrder/{ProductionOrderNumber}/{appointmentType}/{ActivityCode}/{MachineCode}/{Split}/{ActivityID}" PATH "/v1/ProductionOrder/"
WSMETHOD GET SplitOrder         DESCRIPTION "Recupera os dados dos splits da ordem de produ��o/m�quina"	                                 WSSYNTAX "/v1/SplitOrder/{ProductionOrderNumber}/{MachineCode}" PATH "/v1/SplitOrder/"
WSMETHOD GET MachineValidation  DESCRIPTION "Valida se � poss�vel utilizar a m�quina para iniciar um apontamento"	                     WSSYNTAX "/v1/MachineValidation/{MachineCode}" PATH "/v1/MachineValidation/"
WSMETHOD GET SplitValidation    DESCRIPTION "Valida se � poss�vel utilizar o split para iniciar um apontamento"	                         WSSYNTAX "/v1/SplitValidation/{MachineCode}/{ProductionOrderNumber}/{ActivityID}/{Split}" PATH "/v1/SplitValidation/"
WSMETHOD GET OperationTime      DESCRIPTION "Recupera os tempos da opera��o"	                                                         WSSYNTAX "/v1/OperationTime/{ProductionOrderNumber}/{ActivityID}/{Split}" PATH "/v1/OperationTime/"
WSMETHOD POST mata250      		DESCRIPTION "Inclui novo apontamento na ordem de produ��o atrav�s do apontamento simplificado (mata250)" WSSYNTAX "/v1/mata250/" PATH "/v1/mata250/"
WSMETHOD POST mata681      		DESCRIPTION "Inclui novo apontamento na ordem de produ��o atrav�s do apontamento por opera��o (mata681)" WSSYNTAX "/v1/mata681/" PATH "/v1/mata681/"
WSMETHOD POST sfca314      		DESCRIPTION "Inclui novo apontamento na ordem de produ��o atrav�s do apontamento SFC (sfca314)"          WSSYNTAX "/v1/sfca314/" PATH "/v1/sfca314/"

END WSRESTFUL

WSMETHOD GET ProductionOrder WSRECEIVE ProductionOrderNumber, ActivityCode, appointmentType, MachineCode, Split, ActivityID WSSERVICE ProductionAppointment
Local aSOX  := {}
Local lGet  := .T.
Local nI := 0
Local oJson

Private l250Auto 	:= .F.
Private l680Auto 	:= .F.
Private l681Auto 	:= .F.
Private l240:=.F.,l250 :=.F.,l241:=.F.,l242:=.F.,l261:=.F.,l185:=.F.,l650:=.F.,l680:=.F.,l681:=.F.,l682:=.F.
Private nOpcAuto    := 3
Private lMSErroAuto := .F.
Private lAutoErrNoFile := .T.
Private aTELA[0][0],aGETS[0]
Private aRotAuto    := Nil
Private lPerdInf    := SuperGetMV("MV_PERDINF",.F.,.F.)
Private nFCICalc    := SuperGetMV("MV_FCICALC",.F.,0)
Private lUsaSegUm

//Parado no teste da OP localhost:8040/rest/productionappointment/v1/ProductionOrder?ProductionOrderNumber=00436901001&appointmentType=1 e est� ocorrendo erro de variavel private

// define o tipo de retorno do m�todo
::SetContentType("application/json")

// define o tipo de retorno do m�todo
oJson := JsonObject():New()

// verifica se recebeu parametro pela URL
// exemplo: http://localhost:8080/sample/1
If !Empty(::ProductionOrderNumber)
	
	if !Empty (::appointmentType)

		if ::appointmentType == '1' // Mata250
			l250 := .T.
			l250Auto := .T.

			RegToMemory("SD3",.T.) // Inicializar as vari�veis de mem�ria do mata250

			M->D3_OP := ::ProductionOrderNumber
			
			SC2->(dbSetOrder(1))
			if SC2->(dbSeek(xFilial("SC2")+Padr(M->D3_OP,TamSX3("D3_OP")[1])))

				M->D3_COD := SC2->C2_PRODUTO
				M->D3_EMISSAO := DTOC(DATE())

				lret := A250IniOP()
 
				if lret = .T.

					oJson['ProductionOrderNumber'] 		:= trim(M->D3_OP)
					oJson['ItemCode'] 					:= trim(M->D3_COD)
					oJson['ApprovedQuantity'] 			:= M->D3_QUANT 
					oJson['WarehouseCode'] 				:= trim(M->D3_LOCAL)
					oJson['UnitOfMeasureCode'] 			:= trim(M->D3_UM)
					oJson['DocumentCode'] 				:= trim(M->D3_DOC)
					oJson['StartReportDateTime'] 		:= M->D3_EMISSAO 
					oJson['Part_Total'] 				:= trim(M->D3_PARCTOT) 
					oJson['LotPotency'] 				:= M->D3_POTENCI
					oJson['CostCenter'] 				:= trim(M->D3_CC)
					oJson['LedgerAcct'] 				:= trim(M->D3_CONTA)
					oJson['UnitOfMeasureCode2'] 		:= trim(M->D3_SEGUM)
					oJson['UnitOfMeasureCode2Quantity'] := M->D3_QTSEGUM
					oJson['LotCode'] 					:= trim(M->D3_LOTECTL)
					oJson['LotDueDate'] 				:= DTOC(M->D3_DTVALID)
					oJson['Service'] 					:= trim(M->D3_SERVIC)
					oJson['MovimentType']				:= trim(M->D3_TM)
		
					::SetResponse(oJson:toJson())

				Else
					If lMSErroAuto
						lGet := .F.
						SetRestFault(400, EncodeUTF8(FormataErro())) 
					EndIf		
				EndIf
			Else
				SetRestFault(400, EncodeUTF8(STR0001)) //"Ordem de Produ��o n�o cadastrada"	
				lGet := .F.
			EndIf		

			SC2->(dbclosearea())	
		else
			if ::appointmentType == '3' // Mata681
				If !Empty(::ActivityCode)
					l681 := .T.
					l681Auto := .T.

					RegToMemory("SH6",.T.) // Inicializar as vari�veis de mem�ria do mata681

					M->H6_OP := ::ProductionOrderNumber
					M->H6_OP := Padr(M->H6_OP,TamSX3("H6_OP")[1])

					SC2->(dbSetOrder(1))
					if SC2->(dbSeek(xFilial("SC2")+M->H6_OP))

						M->H6_PRODUTO := SC2->C2_PRODUTO
						M->H6_DATAINI := DTOC(DATE())
						M->H6_DATAFIN := DTOC(DATE())
						M->H6_DTAPONT := DTOC(DATE())
						M->H6_OPERAC  := ::ActivityCode
						M->H6_OPERAC  := Padr(M->H6_OPERAC,TamSX3("H6_OPERAC")[1])

						pergunte("MTA680",.F.)
						lret := A680IniOP()

						if lret = .T.

							lret := A680InOper()

							if lret = .T.

                               lret := A680Oper(.F.)

                               if lret = .T.
                                  oJson['ProductionOrderNumber'] 		:= trim(M->H6_OP)
                                  oJson['ItemCode'] 					:= trim(M->H6_PRODUTO)
                                  oJson['WarehouseCode'] 				:= trim(M->H6_LOCAL)
                                  oJson['ActivityCode'] 				:= trim(M->H6_OPERAC)
                                  oJson['MachineCode'] 				:= trim(M->H6_RECURSO)
                                  oJson['StartReportDateTime'] 		:= M->H6_DATAINI
                                  oJson['EndReportDateTime'] 		    := M->H6_DATAFIN
                                  oJson['ReportDateTime'] 		    := M->H6_DTAPONT
                                  oJson['LotCode'] 					:= trim(M->H6_LOTECTL)
                                  oJson['LotDueDate'] 				:= DTOC(M->H6_DTVALID)
                                  oJson['LotPotency'] 				:= M->H6_POTENCI
                                  oJson['ApprovedQuantity'] 			:= M->H6_QTDPROD 
                                  oJson['UnitOfMeasureCode2Quantity'] := M->H6_QTDPRO2
                                  oJson['Part_Total'] 				:= trim(M->H6_PT) 
                                  oJson['Apportionment']              := M->H6_RATEIO
		
                                  ::SetResponse(oJson:toJson())
                               Else
                                  If lMSErroAuto
                                     lGet := .F.
                                     SetRestFault(400, EncodeUTF8(FormataErro())) 
                                  EndIf
                               EndIf
							Else
								If lMSErroAuto
									lGet := .F.
									SetRestFault(400, EncodeUTF8(FormataErro())) 
								EndIf		
							EndIf
						Else
							If lMSErroAuto
								lGet := .F.
								SetRestFault(400, EncodeUTF8(FormataErro())) 
							EndIf		
						EndIf
					Else
						SetRestFault(400, EncodeUTF8(STR0001)) //"Ordem de Produ��o n�o cadastrada"	
						lGet := .F.
					EndIf
				Else
					SetRestFault(400, EncodeUTF8(STR0010)) //"Opera��o n�o informada."
					lGet := .F.
				EndIf
			Else
				If ::appointmentType == '4' // SFCA314
					If Empty(::MachineCode)
						SetRestFault(400, EncodeUTF8(STR0014)) //"N�o foi informada a m�quina."
						lGet := .F.
					EndIf
					If lGet
						If Empty(::ProductionOrderNumber)
							SetRestFault(400, EncodeUTF8(STR0003)) //"Ordem de Produ��o n�o informada"
							lGet := .F.
						EndIf
					EndIf
					If lGet
						If Empty(::Split)
							SetRestFault(400, EncodeUTF8(STR0027)) //"N�o foi informado o split."
							lGet := .F.
						EndIf
					EndIf
					If lGet
						If Empty(::ActivityID)
							SetRestFault(400, EncodeUTF8(STR0024)) //"N�o foi informado o ID da opera��o" 
							lGet := .F.
						EndIf
					EndIf

					If lGet
						cOp 	   := PadR(::ProductionOrderNumber,TamSX3("CYV_NRORPO")[1])
						cIdOper    := PadR(::ActivityID,TamSX3("CYV_CDAT")[1])
						cSplit	   := PadR(::Split,TamSX3("CYV_IDATQO")[1])
						cMaq       := PadR(::MachineCode,TamSX3("CYV_CDMQ")[1])

						DbSelectArea("CYQ")
						CYQ->(DbSetOrder(1))
						If CYQ->(!DbSeek(xFilial("CYQ")+cOp))
							SetRestFault(400, EncodeUTF8(STR0001)) //"Ordem de Produ��o n�o cadastrada" 
							lGet := .F.
						EndIf
					EndIf

					If lGet
						DbSelectArea("CYY")
						CYY->(DbSetOrder(1))
						If CYY->(!DbSeek(xFilial("CYY")+cOp+cIdOper+cSplit))
							SetRestFault(400, EncodeUTF8(STR0029)) //"Split n�o cadastrado."         
							lGet := .F.
						EndIf
					EndIf

					If lGet
						If CYY->CYY_CDMQ != cMaq
							SetRestFault(400, EncodeUTF8(STR0031)) //"M�quina n�o pertence ao Id da Opera��o."         
							lGet := .F.
						EndIf
					EndIf

					If lGet
						DbSelectArea("CY9")
						CY9->(DbSetOrder(1))
						If CY9->(!DbSeek(xFilial("CY9")+cOp+cIdOper))
							SetRestFault(400, EncodeUTF8(STR0030)) //"Opera��es da Ordem n�o cadastrada."         
							lGet := .F.
						EndIf
					EndIf
						
					If lGet
						//Quantidade disponivel do Split -- sem considerar as opera��es anteriores
						cQtdDisp := (CYY->CYY_QTAT - CYY->CYY_QTATAP - CYY->CYY_QTATRF)

						oJson['appointmentType']       := '4'              //Tipo do apontamento
						oJson['MachineCode']           := cMaq             //Maquina
						oJson['ProductionOrderNumber'] := cOp              //Ordem Produ��o
						oJson['Split']                 := cSplit           //Split
						oJson['ActivityID']            := cIdOper          //ID Opera��o
						oJson['ActivityCode']          := CY9->CY9_CDAT    //Opera��o
						oJson['ItemCode']              := CYY->CYY_CDAC    //Item
						oJson['StartSetupDateTime']    := ' '              //Data Inicio Prepara��o
						oJson['StartSetupTime']        := ' '              //Hora In�cio Prepara��o
						oJson['EndSetupDateTime']      := ' '              //Data Fim Prepara��o
						oJson['EndSetupTime']          := ' '              //Hora Fim Prepara��o
						oJson['SetupCode']             := ' '              //C�digo Prepara��o
						oJson['ToolCode']              := ' '              //C�digo da Ferramenta
						oJson['ApprovedQuantity']      := cQtdDisp         //Quantidade aprovada
						oJson['ScrapQuantity']         := 0                //Quantidade refugada
						oJson['StartReportDateTime']   := DTOC(DATE())     //Data In�cio
						oJson['StartReportTime']       := Time()           //Hora In�cio
						oJson['EndReportDateTime']     := DTOC(DATE())     //Data Fim
						oJson['EndReportTime']         := ' '              //Hora Fim
						oJson['ProductionShiftCode']   := ' '              //Modelo Turno
						oJson['DocumentCode']          := cOp              //Documento
						oJson['DocumentSeries']        := ' '              //S�rie Documento
						oJson['WarehouseCode']         := CYQ->CYQ_CDDP    //Dep�sito
						oJson['LotCode']               := ' '              //Lote/Serie
						oJson['LotDueDate']            := ' '              //Data Validade Lote
						oJson['OperatorName']          := ' '              //Operador
						oJson['ProductionTeamCode']    := ' '              //Equipe
											
						::SetResponse(oJson:toJson())
					EndIf
				EndIf
			EndIf
		EndIf
	else
		SetRestFault(400, EncodeUTF8(STR0002)) 	//"Tipo de apontamento n�o informado"
		lGet := .F.
	EndIf
Else
	SetRestFault(400, EncodeUTF8(STR0003)) 	//"Ordem de Produ��o n�o informada"
	lGet := .F.
EndIf

Return lGet

WSMETHOD GET SplitOrder  WSRECEIVE ProductionOrderNumber, MachineCode  WSSERVICE ProductionAppointment
Local aCab  := {}
Local aCYY  := {}
Local lGet  := .T.
Local nI    := 0
Local oJson

// define o tipo de retorno do m�todo
::SetContentType("application/json")

If Empty(::ProductionOrderNumber)
	SetRestFault(400, EncodeUTF8(STR0003)) 	//"Ordem de Produ��o n�o informada"
	lGet := .F.
EndIf

If lGet
	If Empty(::MachineCode)
		SetRestFault(400, EncodeUTF8(STR0014)) //"N�o foi informada a m�quina."
		lGet := .F.
	EndIf
EndIf

If lGet
	aCab := ConCab(::ProductionOrderNumber, ::MachineCode)
	aCYY := ConSplit(::ProductionOrderNumber, ::MachineCode)

	If Len(aCYY) < 1 .Or. Len(aCab) < 1
		lGet := .F.
		SetRestFault(400, EncodeUTF8(STR0029)) //"Split n�o cadastrado."    
	EndIf
EndIf

If lGet
	oJson := JsonObject():New() 
	oJson["Splits"] := {}
	Aadd(oJson["Splits"], JsonObject():New())

	::SetResponse('[')
	oJson['ProductionOrderNumber'] := aCab[1,1]
	oJson['ItemCode']              := aCab[1,2]
	oJson['ItemDescription']       := aCab[1,3]
	oJson['UnitOfMeasureCode']     := aCab[1,4]

	oJson["Splits"] := {}
		For nI := 1 To len(aCYY)
			Aadd(oJson["Splits"], JsonObject():New())			
			oJson["Splits"][nI]['split']                 := aCYY[nI,1]
			oJson["Splits"][nI]['ActivityID']            := aCYY[nI,2]
			oJson["Splits"][nI]['ActivityCode']          := aCYY[nI,3]
			oJson["Splits"][nI]['OperationDescription']  := aCYY[nI,4]
		Next nI

	::SetResponse(oJson:toJson())
	::SetResponse(']') 
EndIf

Return lGet

WSMETHOD GET MachineValidation WSRECEIVE MachineCode  WSSERVICE ProductionAppointment
Local lGet  := .T.
Local oJson
Local cMsgErro := ''

// define o tipo de retorno do m�todo
::SetContentType("application/json")

If Empty(::MachineCode)
	SetRestFault(400, EncodeUTF8(STR0014)) //"N�o foi informada a m�quina."
	lGet := .F.
EndIf

If lGet
	//Verifica se existe apontamento de parada em aberto para a m�quina
	If !SFCParAber(::MachineCode,, 1,@cMsgErro)
		lGet := .F.
		SetRestFault(400, EncodeUTF8(cMsgErro))	
	EndIf
EndIf

If lGet
	//Verifica se existe apontamento de produ��o em aberto para a m�quina
	If !SFCApAbeMq(::MachineCode,,1,@cMsgErro)
		lGet := .F.
		SetRestFault(400, EncodeUTF8(cMsgErro))
	EndIf
EndIf

If lGet
	//Verifica se existe apontamento de parada de prepara��o iniciado para a m�quina
	If SFCSetupIn(::MachineCode,1,@cMsgErro)
		lGet := .F.
		SetRestFault(400, EncodeUTF8(cMsgErro))
	EndIf
EndIf

If lGet
	//M�quina n�o pode iniciar apontamento
	If !SFCPerIni(::MachineCode,1,@cMsgErro)
		lGet := .F.
		SetRestFault(400, EncodeUTF8(cMsgErro)) 				
	EndIf
EndIf

If lGet
	oJson := JsonObject():New() 
	
	oJson['MachineCode']           := ::MachineCode
	oJson['Status']                := .T.
	
	::SetResponse(oJson:toJson())
EndIf

Return lGet

WSMETHOD GET SplitValidation WSRECEIVE MachineCode,ProductionOrderNumber,ActivityID,Split  WSSERVICE ProductionAppointment

Local lGet      := .T.
Local oJson
Local cMsgErro  := ''
Local lStatus   := .T.
Local lExistIni := .F.

// define o tipo de retorno do m�todo
::SetContentType("application/json")

If Empty(::MachineCode)
	SetRestFault(400, EncodeUTF8(STR0014)) //"N�o foi informada a m�quina."
	lGet := .F.
EndIf

If lGet
	If Empty(::ProductionOrderNumber)
		SetRestFault(400, EncodeUTF8(STR0003)) 	//"Ordem de Produ��o n�o informada"
		lGet := .F.
	EndIf
EndIf

If lGet
	If Empty(::ActivityID)
		SetRestFault(400, EncodeUTF8(STR0024)) //"N�o foi informado o ID da opera��o."
		lGet := .F.	
	EndIf
EndIf

If lGet
	If Empty(::Split)
		SetRestFault(400, EncodeUTF8(STR0027)) //"N�o foi informado o split."
		lGet := .F.	
	EndIf
EndIf

If lGet
	cOp 	   := PadR(::ProductionOrderNumber,TamSX3("CYV_NRORPO")[1])
	cIdOper    := PadR(::ActivityID,TamSX3("CYV_CDAT")[1])
	cSplit	   := PadR(::Split,TamSX3("CYV_IDATQO")[1])
	cMaq       := PadR(::MachineCode,TamSX3("CYV_CDMQ")[1])

	DbSelectArea("CYQ")
	CYQ->(DbSetOrder(1))
	If CYQ->(!DbSeek(xFilial("CYQ")+cOp))
		SetRestFault(400, EncodeUTF8(STR0001)) //"Ordem de Produ��o n�o cadastrada" 
		lGet := .F.
	EndIf
EndIf

If lGet
	DbSelectArea("CYY")
	CYY->(DbSetOrder(1))
	If CYY->(!DbSeek(xFilial("CYY")+cOp+cIdOper+cSplit))
		SetRestFault(400, EncodeUTF8(STR0029)) //"Split n�o cadastrado."         
		lGet := .F.
	EndIf
EndIf

If lGet
	If CYY->CYY_CDMQ != cMaq
		SetRestFault(400, EncodeUTF8(STR0031)) //"M�quina n�o pertence ao Id da Opera��o."         
		lGet := .F.
	EndIf
EndIf

If lGet
	DbSelectArea("CY9")
	CY9->(DbSetOrder(1))
	If CY9->(!DbSeek(xFilial("CY9")+cOp+cIdOper))
		SetRestFault(400, EncodeUTF8(STR0030)) //"Opera��es da Ordem n�o cadastrada."         
		lGet := .F.
	EndIf
EndIf

If lGet
	//Verifica se existe apontamento de produ��o em aberto do split
	If !SFCApAbeSp(cOp, cIdOper, cSplit, ,1,@cMsgErro)
		cNRSQRP := ' '
		lGet := ConSeqApo(cOp, cIdOper, cSplit, @cNRSQRP)

		If !lGet 
			SetRestFault(400, EncodeUTF8(STR0034)) //"N�o foi poss�vel localizar a sequ�ncia do apontamento de inicio para realizar a finaliza��o."
		Else
			lExistIni := .T.
			lStatus   := .F.
		EndIf
	EndIf
EndIf
		
If lGet .And. lExistIni
	DbSelectArea("CYV")
	CYV->(DbSetOrder(1))
	If CYV->(!DbSeek(xFilial("CYV")+cNRSQRP))					
		SetRestFault(400, EncodeUTF8(STR0035)) //"Serial do apontamento de inicio n�o cadastrado."
		lGet := .F.
	EndIf

	If lGet
		//Se existir apontamento em aberto para o split
		//Verificar se est� aberto para a m�quina
		If CYV->CYV_CDMQ != cMaq
			SetRestFault(400, EncodeUTF8(STR0044)) //"M�quina que iniciou o apontamento difere da m�quina corrente. Para retomar o apontamento deve ser selecionada a m�quina que iniciou o apontamento."
			lGet := .F.
		EndIf
	EndIf

	If lGet
		//Se for a mesma m�quina retornar a hora de in�cio e fim
		cDataIniRe := CYV->CYV_DTRPBG
		cTimeIniRe := CYV->CYV_HRRPBG	
	EndIf
Else
	cDataIniRe := ' '
	cTimeIniRe := ' '
EndIf

If lGet
	oJson := JsonObject():New() 
	
	oJson['MachineCode']           := ::MachineCode
	oJson['ProductionOrderNumber'] := ::ProductionOrderNumber
	oJson['ActivityID']            := ::ActivityID
	oJson['Split']                 := ::Split
	oJson['StartReportDateTime']   := cDataIniRe    //Data In�cio
	oJson['StartReportTime']       := cTimeIniRe    //Hora In�cio
	oJson['Status']                := lStatus
	
	::SetResponse(oJson:toJson())
EndIf

Return lGet

WSMETHOD GET OperationTime WSRECEIVE ProductionOrderNumber,ActivityID,Split  WSSERVICE ProductionAppointment

Local lGet     := .T.
Local oJson
Local nTmpPad  := 0
Local nTmpApon := 0
Local nTmpTot  := 0

// define o tipo de retorno do m�todo
::SetContentType("application/json")


If Empty(::ProductionOrderNumber)
	SetRestFault(400, EncodeUTF8(STR0003)) 	//"Ordem de Produ��o n�o informada"
	lGet := .F.
EndIf

If lGet
	If Empty(::ActivityID)
		SetRestFault(400, EncodeUTF8(STR0024)) //"N�o foi informado o ID da opera��o."
		lGet := .F.	
	EndIf
EndIf

If lGet
	If Empty(::Split)
		SetRestFault(400, EncodeUTF8(STR0027)) //"N�o foi informado o split."
		lGet := .F.	
	EndIf
EndIf

If lGet
	cOp 	   := PadR(::ProductionOrderNumber,TamSX3("CYV_NRORPO")[1])
	cIdOper    := PadR(::ActivityID,TamSX3("CYV_CDAT")[1])
	cSplit	   := PadR(::Split,TamSX3("CYV_IDATQO")[1])

	DbSelectArea("CYY")
	CYY->(DbSetOrder(1))
	If CYY->(DbSeek(xFilial("CYY")+cOp+cIdOper+cSplit))
		//CYY->CYY_QTPAAT - Tempo padr�o total para produzir o Split
		//CYY->CYY_QTAT   - Quantidade Prevista no Split

		//Tempo padr�o - Tempo que levar� para produzir uma unidade do Split.
		nTmpPad  := (CYY->CYY_QTPAAT / CYY->CYY_QTAT)
	
		//Tempo apontado - Tempo j� apontado no Split
		nTmpApon := CYY->CYY_QTTEAT

		//Tempo total - Tempo padr�o total para produzir o Split
		nTmpTot  := CYY->CYY_QTPAAT

	Else		
		SetRestFault(400, EncodeUTF8(STR0029)) //"Split n�o cadastrado."
		lGet := .F.	
	EndIf
EndIf

If lGet
	oJson := JsonObject():New() 
		
	oJson['ProductionOrderNumber'] := ::ProductionOrderNumber
	oJson['ActivityID']            := ::ActivityID
	oJson['Split']                 := ::Split
	oJson['OperationUnitTime ']    := nTmpPad
	oJson['TimeAppointed ']        := nTmpApon
	oJson['OpTimeInt ']            := nTmpTot
	
	::SetResponse(oJson:toJson())
EndIf

Return lGet


WSMETHOD POST mata250 WSSERVICE ProductionAppointment
	Local lPost := .T.
	Local cBody := " "
	Local aSOX := {}
	Local cCodForm   := " "
	Local cPrgApon   := " "
	Local cImagem    := " "
	Local cDescricao := " "
	Local nI := 0
	local aMata250    := {}
	Local oJson
	local dDatalote
	local dDatemis
	Local lGanhoPr    := SuperGetMV("MV_GANHOPR",.F.,.T.)
	Local lPercPrm    := SuperGetMV("MV_PERCPRM",.F., 0)
	Local nDiferenca  := 0
	Local nQtdApon := 0
	Local nPerda := 0
	Local nNumero := 0
	Local lApoPend  := SuperGetMV("MV_APPENMO",.F.,1)
	Local lMsgPend  := " "

	Private l250Auto 	:= .T.
	Private l240:=.F.,l250 :=.T.,l241:=.F.,l242:=.F.,l261:=.F.,l185:=.F.,l650:=.F.,l680:=.F.,l681:=.F.
	Private lMSErroAuto := .F.
	Private lAutoErrNoFile := .T.
	Private lPerdInf    := SuperGetMV("MV_PERDINF",.F.,.F.)

	oJson := JsonObject():New()

	cBody := ::GetContent()
	
	conout(cBody)


	If oJson:fromJson( cBody ) <> nil
		SetRestFault(400, EncodeUTF8(STR0006)) //"Par�metros do apontamento n�o enviados ou inv�lidos."                                                                                                                                                                                                                                                                                                                                                                                                                                                              
		lPost := .F.
	Else
		If Empty(oJson['ProductionOrderNumber'])
			SetRestFault(400, EncodeUTF8(STR0003)) //"Ordem de Produ��o n�o informada."
			lPost := .F.
		elseif Empty(oJson['MovimentType']) 
			SetRestFault(400, EncodeUTF8(STR0007)) //"Tipo de movimento n�o informada."
			lPost := .F.
		elseif oJson['appointmentType'] <> "1"
			SetRestFault(400, EncodeUTF8(STR0005)) //"O tipo de apontamento do formul�rio deve ser do tipo Apontamento simplificado (MATA250)"
			lPost := .F.
		elseif empty(oJson['Part_Total'])
			SetRestFault(400, EncodeUTF8(STR0008)) //"N�o foi informado se o apontamento � parcial/total"
			lPost := .F.
		elseif empty(oJson['ApprovedQuantity']) .And. empty(oJson['ScrapQuantity'])
			SetRestFault(400, EncodeUTF8(STR0009)) //"N�o foi informado a quantidade a ser apontada"
			lPost := .F.
		EndIf
	EndIf

	If lPost
		 	
		AADD (aMata250, {"D3_TM", oJson['MovimentType'], nil})
		AADD (aMata250, {"D3_OP", oJson['ProductionOrderNumber'], nil})
		

		//Quando a quantidade vier em caracter, ser� efetuada a convers�o .
		If !empty(oJson['ApprovedQuantity']) 
			If ValType(oJson['ApprovedQuantity']) == "C"
				If AT("," , oJson['ApprovedQuantity']) > 0
					nQtdApon := Val(StrTran(oJson['ApprovedQuantity'],",",".")) 
				else
					nQtdApon := Val(oJson['ApprovedQuantity'])
				EndIf
			else
				nQtdApon := oJson['ApprovedQuantity']
			endif
		
			AADD (aMata250, {"D3_QUANT", nQtdApon, nil})
		Endif
		
		if !empty(oJson['ItemCode'])
			AADD (aMata250, {"D3_COD", oJson['ItemCode'], nil})
		EndIf

		if !empty(oJson['WarehouseCode'])
			AADD (aMata250, {"D3_LOCAL", oJson['WarehouseCode'], nil})
		endif

		if !empty(oJson['UnitOfMeasureCode'])
			AADD (aMata250, {"D3_UM", oJson['UnitOfMeasureCode'], nil})
		EndIf

		if !empty(oJson['DocumentCode'])
			AADD (aMata250, {"D3_DOC", oJson['DocumentCode'], nil})
		endif

		if !empty(oJson['UnitOfMeasureCode'])
			AADD (aMata250, {"D3_UM", oJson['UnitOfMeasureCode'], nil})
		EndIf
		
		if !empty(oJson['StartReportDateTime'])
			dDatemis := CTOD(oJson['StartReportDateTime'])
			AADD (aMata250, {"D3_EMISSAO", dDatemis, nil})
		endif 

		
		if !empty(oJson['LotPotency'])
			If ValType(oJson['LotPotency']) == "C"

				If AT("," , oJson['LotPotency']) > 0
					nNumero := Val(StrTran(oJson['LotPotency'],",",".")) 
				else
					nNumero := Val(oJson['LotPotency'])
				EndIf
			else
				nNumero := oJson['LotPotency']
			EndIf
			AADD (aMata250, {"D3_POTENCI",nNumero, nil})
		endif
		
		if !empty(oJson['CostCenter'])
			AADD (aMata250, {"D3_CC", oJson['CostCenter'], nil})
		endif
		
		if !empty(oJson['LedgerAcct'])
			AADD (aMata250, {"D3_CONTA", oJson['LedgerAcct'], nil})
		endif 
		
		if !empty(oJson['UnitOfMeasureCode2'])
			AADD (aMata250, {"D3_SEGUM",oJson['UnitOfMeasureCode2'], nil})
		endif 
		
		if !empty(oJson['LotCode'])
			AADD (aMata250, {"D3_LOTECTL",oJson['LotCode'], nil})
		endif 
		
		if !empty(oJson['LotDueDate'])
			dDatalote := CTOD(oJson['LotDueDate'])
			AADD (aMata250, {"D3_DTVALID",dDatalote, nil})
		endif 

		//Quando a quantidade vier em caracter, ser� efetuada a convers�o .
		If !empty(oJson['UnitOfMeasureCode2Quantity'])
			If ValType(oJson['UnitOfMeasureCode2Quantity']) == "C"

				If AT("," , oJson['UnitOfMeasureCode2Quantity']) > 0
					nNumero := Val(StrTran(oJson['UnitOfMeasureCode2Quantity'],",",".")) 
				else
					nNumero := Val(oJson['UnitOfMeasureCode2Quantity'])
				EndIf
			else
				nNumero := oJson['UnitOfMeasureCode2Quantity']
			EndIf
			AADD (aMata250, {"D3_QTSEGUM",nNumero, nil})
		EndIf

		If !empty(oJson['ScrapQuantity'])
			If ValType(oJson['ScrapQuantity']) == "C"
				If AT("," , oJson['ScrapQuantity']) > 0
					nPerda := Val(StrTran(oJson['ScrapQuantity'],",",".")) 
				Else
					nPerda := Val(oJson['ScrapQuantity'])
				EndIf
			Else
				nPerda := oJson['ScrapQuantity']
			Endif
		
			AADD (aMata250, {"D3_PERDA",nPerda, nil})
		Endif

		If !empty(oJson['Service'])
			AADD (aMata250, {"D3_SERVIC",oJson['Service'], nil})
		Endif
		
		AADD (aMata250, {"D3_PARCTOT",oJson['Part_Total'], nil})

		//Para gerar a produ��o a maior quando o percentual est� maior que zero
		If lGanhoPr = .F. .And. lPercPrm > 0 
			SC2->(dbSetOrder(1))
			SC2->(dbSeek(xFilial("SC2")+ oJson['ProductionOrderNumber']))

			If (SC2->C2_QUANT - SC2->C2_QUJE - If(lPerdInf,0,SC2->C2_PERDA)) < (nQtdApon + If(lPerdInf,0,nPerda) )
				nDiferenca := nQtdApon + If(lPerdInf,0,nPerda) - (SC2->C2_QUANT - SC2->C2_QUJE - If(lPerdInf,0,SC2->C2_PERDA)) 
				AADD (aMata250, {"D3_QTMAIOR",nDiferenca, nil})  
			EndIf
		Endif 

		AADD (aMata250, {"PENDENTE",lApoPend, nil})  
		
		//Chamar EXECAUTO
		msExecAuto({|x,y| MATA250(x,y)},aMata250,3)

		If lMSErroAuto
			If lApoPend == "3"
				oJson['Status'] := (STR0040) //'Apontamento enviado para pendente com sucesso'
				oJson['idAppointment'] := xFilial("T4K")+T4K->(T4K_COD+T4K_LOCAL+T4K_SEQ)
				oJson['OpClose'] := .F.
				
				//saldo da ordem ap�s o apontamento
				SC2->(dbSetOrder(1))
				SC2->(dbSeek(xFilial("SC2")+ oJson['ProductionOrderNumber']))

				oJson['ReportQuantity'] := SC2->C2_QUANT - SC2->C2_QUJE - If(lPerdInf,0,SC2->C2_PERDA) 
				::SetResponse(oJson:toJson())		
			Else
				lPost := .F.
				If lApoPend == "2"
					lMsgPend := STR0012
				Endif
				
				SetRestFault(400, EncodeUTF8(lMsgPend+FormataErro()))
			Endif
		Else
			oJson['Status'] := (STR0041) //'Apontamento efetuado com sucesso'
			oJson['idAppointment'] := xFilial("SD3")+SD3->(D3_COD+D3_LOCAL+D3_NUMSEQ)
			
			
			//saldo da ordem ap�s o apontamento
			SC2->(dbSetOrder(1))
			SC2->(dbSeek(xFilial("SC2")+ oJson['ProductionOrderNumber']))
			
			If !Empty(SC2->C2_DATRF) 			
				oJson['OpClose'] := .T.
			Else
				oJson['OpClose'] := .F.
			EndIf
			 
		    oJson['ReportQuantity'] := SC2->C2_QUANT - SC2->C2_QUJE - If(lPerdInf,0,SC2->C2_PERDA) 
			::SetResponse(oJson:toJson())		
		EndIf		
	EndIf

	FreeObj(oJson)

Return lPost


WSMETHOD POST mata681 WSSERVICE ProductionAppointment
	Local lPost := .T.
	Local cBody := " "
	local aMata681    := {}
	Local oJson
	local dDataApont
	Local lGanhoPr    := SuperGetMV("MV_GANHOPR",.F.,.T.)
	Local lPercPrm    := SuperGetMV("MV_PERCPRM",.F., 0)
	Local nDiferenca  := 0
	Local nQtdApon := 0
	Local nPerda := 0
	Local nNumero := 0
	Local lApoPend  := SuperGetMV("MV_APPENMO",.F.,1)
	Local lMsgPend := " " 

	Private l681Auto 	:= .T.
	Private l240:=.F.,l250 :=.F.,l241:=.F.,l242:=.F.,l261:=.F.,l185:=.F.,l650:=.F.,l680:=.F.,l681:=.T.
	Private lMSErroAuto := .F.
	Private lAutoErrNoFile := .T.
	Private lPerdInf    := SuperGetMV("MV_PERDINF",.F.,.F.)

	oJson := JsonObject():New()

	cBody := ::GetContent()
	
	conout(cBody)

	If oJson:fromJson( cBody ) <> nil
		SetRestFault(400, EncodeUTF8(STR0006)) //"Par�metros do apontamento n�o enviados ou inv�lidos."                                                                                                                                                                                                                                                                                                                                                                                                                                                              
		lPost := .F.
	Else
		If Empty(oJson['ProductionOrderNumber'])
			SetRestFault(400, EncodeUTF8(STR0003)) //"Ordem de Produ��o n�o informada."
			lPost := .F.
		ElseIf Empty(oJson['ActivityCode']) 
			SetRestFault(400, EncodeUTF8(STR0010)) //"Opera��o n�o informada."
			lPost := .F.
		ElseIf oJson['appointmentType'] <> "3"
			SetRestFault(400, EncodeUTF8(STR0011)) //"O tipo de apontamento do formul�rio deve ser do tipo Apontamento por Opera��o (MATA681)"
			lPost := .F.
		ElseIf empty(oJson['Part_Total'])
			SetRestFault(400, EncodeUTF8(STR0008)) //"N�o foi informado se o apontamento � parcial/total"
			lPost := .F.
		ElseIf empty(oJson['ApprovedQuantity']) .And. empty(oJson['ScrapQuantity'])
			SetRestFault(400, EncodeUTF8(STR0009)) //"N�o foi informado a quantidade a ser apontada"
			lPost := .F.
		EndIf
	EndIf

	If lPost
		AADD (aMata681, {"H6_OP", oJson['ProductionOrderNumber'], nil})
		
		If !empty(oJson['ItemCode'])
			AADD (aMata681, {"H6_PRODUTO", oJson['ItemCode'], nil})
		EndIf
		
		If !empty(oJson['ActivityCode'])
			AADD (aMata681, {"H6_OPERAC", oJson['ActivityCode'], nil})
		EndIf
		
		If !empty(oJson['MachineCode'])
			AADD (aMata681, {"H6_RECURSO", oJson['MachineCode'], nil})
		EndIf
		
		If !empty(oJson['ToolCode'])
			AADD (aMata681, {"H6_FERRAM", oJson['ToolCode'], nil})
		EndIf
		
		If !empty(oJson['StartReportDateTime'])
			dDataApont := CTOD(oJson['StartReportDateTime'])
			AADD (aMata681, {"H6_DATAINI", dDataApont, nil})
		EndIf
		
		If !empty(oJson['StartReportTime'])
			AADD (aMata681, {"H6_HORAINI", oJson['StartReportTime'], nil})
		EndIf 
		
		If !empty(oJson['EndReportDateTime'])
			dDataApont := CTOD(oJson['EndReportDateTime'])
			AADD (aMata681, {"H6_DATAFIN", dDataApont, nil})
		EndIf 
		
		If !empty(oJson['EndReportTime'])
			AADD (aMata681, {"H6_HORAFIN", oJson['EndReportTime'], nil})
		EndIf 

		//Quando a quantidade vier em caracter, ser� efetuada a convers�o .
		If !empty(oJson['ApprovedQuantity']) 
			If ValType(oJson['ApprovedQuantity']) == "C"
				If AT("," , oJson['ApprovedQuantity']) > 0
					nQtdApon := Val(StrTran(oJson['ApprovedQuantity'],",",".")) 
				Else
					nQtdApon := Val(oJson['ApprovedQuantity'])
				EndIf
			Else
				nQtdApon := oJson['ApprovedQuantity']
			EndIf
		
			AADD (aMata681, {"H6_QTDPROD", nQtdApon, nil})
		EndIf

		If !empty(oJson['ScrapQuantity'])
			If ValType(oJson['ScrapQuantity']) == "C"
				If AT("," , oJson['ScrapQuantity']) > 0
					nPerda := Val(StrTran(oJson['ScrapQuantity'],",",".")) 
				Else
					nPerda := Val(oJson['ScrapQuantity'])
				EndIf
			Else
				nPerda := oJson['ScrapQuantity']
			EndIf
		
			AADD (aMata681, {"H6_QTDPERD",nPerda, nil})
		EndIf
		
		AADD (aMata681, {"H6_PT",oJson['Part_Total'], nil})
		
		If !empty(oJson['ReportDateTime'])
			dDataApont := CTOD(oJson['ReportDateTime'])
			AADD (aMata681, {"H6_DTAPONT", dDataApont, nil})
		EndIf 

		If !empty(oJson['Split'])
			AADD (aMata681, {"H6_DESDOBR", oJson['Split'], nil})
		EndIf

		If !empty(oJson['RealTime'])
			AADD (aMata681, {"H6_TEMPO", oJson['RealTime'], nil})
		EndIf
		
		If !empty(oJson['LotCode'])
			AADD (aMata681, {"H6_LOTECTL",oJson['LotCode'], nil})
		EndIf 
		
		If !empty(oJson['SubLotCode'])
			AADD (aMata681, {"H6_NUMLOTE",oJson['SubLotCode'], nil})
		EndIf 
		
		If !empty(oJson['LotDueDate'])
			dDataApont := CTOD(oJson['LotDueDate'])
			AADD (aMata681, {"H6_DTVALID",dDataApont, nil})
		EndIf 

		If !empty(oJson['Comments'])
			AADD (aMata681, {"H6_OBSERVA", oJson['Comments'], nil})
		EndIf

		If !empty(oJson['OperatorCode'])
			AADD (aMata681, {"H6_OPERADO", oJson['OperatorCode'], nil})
		EndIf

		If !empty(oJson['AlternativeSequence'])
			AADD (aMata681, {"H6_SEQ", oJson['AlternativeSequence'], nil})
		EndIf

		//Quando a quantidade vier em caracter, ser� efetuada a convers�o .
		If !empty(oJson['UnitOfMeasureCode2Quantity'])
			If ValType(oJson['UnitOfMeasureCode2Quantity']) == "C"

				If AT("," , oJson['UnitOfMeasureCode2Quantity']) > 0
					nNumero := Val(StrTran(oJson['UnitOfMeasureCode2Quantity'],",",".")) 
				Else
					nNumero := Val(oJson['UnitOfMeasureCode2Quantity'])
				EndIf
			Else
				nNumero := oJson['UnitOfMeasureCode2Quantity']
			EndIf
			AADD (aMata681, {"H6_QTDPRO2",nNumero, nil})
		EndIf

		If !empty(oJson['LotPotency'])
			If ValType(oJson['LotPotency']) == "C"

				If AT("," , oJson['LotPotency']) > 0
					nNumero := Val(StrTran(oJson['LotPotency'],",",".")) 
				Else
					nNumero := Val(oJson['LotPotency'])
				EndIf
			Else
				nNumero := oJson['LotPotency']
			EndIf
			AADD (aMata681, {"H6_POTENCI",nNumero, nil})
		EndIf

		If !empty(oJson['Apportionment'])
			If ValType(oJson['Apportionment']) == "C"

				If AT("," , oJson['Apportionment']) > 0
					nNumero := Val(StrTran(oJson['Apportionment'],",",".")) 
				Else
					nNumero := Val(oJson['Apportionment'])
				EndIf
			Else
				nNumero := oJson['Apportionment']
			EndIf
			AADD (aMata681, {"H6_RATEIO",nNumero, nil})
		EndIf

		If !empty(oJson['WarehouseCode'])
			AADD (aMata681, {"H6_LOCAL", oJson['WarehouseCode'], nil})
		EndIf

		//Para gerar a produ��o a maior quando o percentual est� maior que zero
		If lGanhoPr = .F. .And. lPercPrm > 0 
			SC2->(dbSetOrder(1))
			SC2->(dbSeek(xFilial("SC2")+ oJson['ProductionOrderNumber']))

			If (SC2->C2_QUANT - SC2->C2_QUJE - If(lPerdInf,0,SC2->C2_PERDA)) < (nQtdApon + If(lPerdInf,0,nPerda) )
				nDiferenca := nQtdApon + If(lPerdInf,0,nPerda) - (SC2->C2_QUANT - SC2->C2_QUJE - If(lPerdInf,0,SC2->C2_PERDA)) 
				AADD (aMata681, {"H6_QTMAIOR",nDiferenca, nil})  
			EndIf
		EndIf   

		AADD (aMata681, {"PENDENTE",lApoPend, nil})

		//Chamar EXECAUTO
		msExecAuto({|x,y| MATA681(x,y)},aMata681,3)

		If lMSErroAuto
			If lApoPend == "3"
				oJson['Status'] := (STR0040) //'Apontamento enviado para pendente com sucesso'
				oJson['idAppointment'] := xFilial("T4K")+T4K->(T4K_COD+T4K_LOCAL+T4K_SEQ)
				oJson['OpClose'] := .F.
				
				//saldo da ordem ap�s o apontamento
				SC2->(dbSetOrder(1))
				SC2->(dbSeek(xFilial("SC2")+ oJson['ProductionOrderNumber']))

				oJson['ReportQuantity'] := SC2->C2_QUANT - SC2->C2_QUJE - If(lPerdInf,0,SC2->C2_PERDA) 
				::SetResponse(oJson:toJson())		
			Else
				lPost := .F.
				If lApoPend == "2"
					lMsgPend := STR0012
				Endif
				
				SetRestFault(400, EncodeUTF8(lMsgPend+FormataErro()))
			Endif
		Else
			oJson['Status'] := (STR0041) //'Apontamento efetuado com sucesso'
			oJson['idAppointment'] := xFilial("SH6")+SH6->(H6_PRODUTO+H6_LOCAL+H6_IDENT)
			
			
			//saldo da ordem ap�s o apontamento
			SC2->(dbSetOrder(1))
			SC2->(dbSeek(xFilial("SC2")+ oJson['ProductionOrderNumber']))
			
			If !Empty(SC2->C2_DATRF) 			
				oJson['OpClose'] := .T.
			Else
				oJson['OpClose'] := .F.
			EndIf
			 
		    oJson['ReportQuantity'] := SC2->C2_QUANT - SC2->C2_QUJE - If(lPerdInf,0,SC2->C2_PERDA) 
			::SetResponse(oJson:toJson())		
		EndIf		
	EndIf

	FreeObj(oJson)

Return lPost

WSMETHOD POST sfca314 WSSERVICE ProductionAppointment

	Local lPost := .T.
	Local cBody := " "
	Local oJson
	Local oModel
	Local oModelCY0
	Local oModelCYW
	Local oModelCZ0
	Local aError := {}
	
    Private lPerdInf    := SuperGetMV("MV_PERDINF",.F.,.F.)
	Private lMSErroAuto := .F.
	Private lAutoErrNoFile := .T.	
	Private _IsSFCA318 := .F.
	Private _IsPost314 := .T.

	Private cPROG := 'APIREST'
	Private cTipApon := ' '

	oJson := JsonObject():New()

	cBody := ::GetContent()
	conout(cBody)

	If oJson:fromJson( cBody ) <> nil
		SetRestFault(400, EncodeUTF8(STR0006)) //"Par�metros do apontamento n�o enviados ou inv�lidos."                                                                                                                                                                                                                                                                                                                                                                                                                                                              
		lPost := .F.
	EndIf

	If lPost
		If oJson['appointmentType'] <> "4"
			SetRestFault(400, EncodeUTF8(STR0013)) //"O tipo de apontamento do formul�rio deve ser Produ��o Ch�o de F�brica."
			lPost := .F.
		EndIf
	EndIf

	If lPost
		cIniFim := oJson['StartEndReport']
		If !Empty(cIniFim)
			If cIniFim != 'I' .And. cIniFim != 'F' .And. cIniFim != 'A'
				SetRestFault(400, EncodeUTF8(STR0033)) //"O indicador de apontamento In�cio e Fim deve ser I(in�cio), F(fim) ou A(abandonar)."
				lPost := .F.
			Else
				cTipApon := cIniFim
			EndIf
		EndIf
	EndIf

	If lPost
		If Empty(oJson['ProductionOrderNumber'])
			SetRestFault(400, EncodeUTF8(STR0003)) //"Ordem de Produ��o n�o informada."
			lPost := .F.		
		ElseIf Empty(oJson['MachineCode']) 
			SetRestFault(400, EncodeUTF8(STR0014)) //"N�o foi informada a m�quina."
			lPost := .F.
		ElseIf Empty(oJson['Split']) 
			SetRestFault(400, EncodeUTF8(STR0027)) //"N�o foi informado o split."
			lPost := .F.		
		ElseIf Empty(oJson['ActivityID']) 
			SetRestFault(400, EncodeUTF8(STR0024)) //"N�o foi informado o ID da opera��o."
			lPost := .F.			
		ElseIf Empty(oJson['StartReportDateTime']) .And. cIniFim != 'F' .And. cIniFim != 'A' 
			SetRestFault(400, EncodeUTF8(STR0015)) //"N�o foi informada a data de in�cio da produ��o."
			lPost := .F.
		ElseIf Empty(oJson['StartReportTime']) .And. cIniFim != 'F' .And. cIniFim != 'A'
			SetRestFault(400, EncodeUTF8(STR0016)) //"N�o foi informada a hora de in�cio da produ��o."
			lPost := .F.
		ElseIf Empty(oJson['EndReportDateTime']) .And. cIniFim != 'I' .And. cIniFim != 'A'
			SetRestFault(400, EncodeUTF8(STR0017)) //"N�o foi informada a data fim da produ��o."
			lPost := .F.
		ElseIf Empty(oJson['EndReportTime']) .And. cIniFim != 'I' .And. cIniFim != 'A'
			SetRestFault(400, EncodeUTF8(STR0018)) //"N�o foi informada a hora fim da produ��o."
			lPost := .F.
		EndIf
	EndIf

	If lPost
		cOp 	   := PadR(oJson['ProductionOrderNumber'],TamSX3("CYV_NRORPO")[1])
		cOper 	   := PadR(oJson['ActivityCode'],TamSX3("CYV_CDAT")[1])
		cIdOper    := PadR(oJson['ActivityID'],TamSX3("CYV_IDAT")[1])
		cSplit	   := PadR(oJson['Split'],TamSX3("CYV_IDATQO")[1])
		cMaq       := PadR(oJson['MachineCode'],TamSX3("CYV_CDMQ")[1])
		cItem      := PadR(oJson['ItemCode'],TamSX3("CYV_CDACRP")[1])
		cTurno     := PadR(oJson['ProductionShiftCode'],TamSX3("CYV_CDTN")[1])
		cDoc       := PadR(oJson['DocumentCode'],TamSX3("CYV_NRDO")[1])
		cSerieDoc  := PadR(oJson['DocumentSeries'],TamSX3("CYV_NRSR")[1])
		cDeposito  := PadR(oJson['WarehouseCode'],TamSX3("CYV_CDDP")[1])
		cLoteSerie := PadR(oJson['LotCode'],TamSX3("CYV_CDLOSR")[1])
		cValLote   := PadR(oJson['LotDueDate'],TamSX3("CYV_DTVDLO")[1])
		cFerram    := PadR(oJson['ToolCode'],TamSX3("CYV_CDFEPO")[1])
		cOperador  := PadR(oJson['OperatorName'],TamSX3("CYW_CDOE")[1])
		cEquipe    := PadR(oJson['ProductionTeamCode'],TamSX3("CYW_CDOE")[1])
		
		//Quando a quantidade vier em caracter, ser� efetuada a convers�o .
		If !Empty(oJson['ApprovedQuantity'])
			If ValType(oJson['ApprovedQuantity']) == "C"
				If AT("," , oJson['ApprovedQuantity']) > 0
					nQuant := Val(StrTran(oJson['ApprovedQuantity'],",",".")) 
				Else
					nQuant := Val(oJson['ApprovedQuantity'])
				EndIf
			Else
				nQuant := oJson['ApprovedQuantity']
			EndIf
		Else
			nQuant := 0
		EndIf
        
		//DATAS DE PRODU��O
		If !Empty(oJson['StartReportDateTime'])
			cDataIniRe := CTOD(oJson['StartReportDateTime'])
		EndIf
		If !Empty(oJson['EndReportDateTime'])
			cDataFimRe := CTOD(oJson['EndReportDateTime'])
		EndIf
		cTimeIniRe := oJson['StartReportTime']
		cTimeFimRe := oJson['EndReportTime']
		
		//DATAS DE SETUP
		If !Empty(oJson['StartSetupDateTime'])
			cDataIniSe := CTOD(oJson['StartSetupDateTime'])
		Else
			cDataIniSe := ' '
		EndIf
		If !Empty(oJson['EndSetupDateTime'])
			cDataFimSe := CTOD(oJson['EndSetupDateTime'])
		Else
			cDataFimSe := ' '
		EndIf
		cTimeIniSe := oJson['StartSetupTime']
		cTimeFimSe := oJson['EndSetupTime']
		cCodSetup  := PadR(oJson['SetupCode'],TamSX3("CYV_CDSU")[1])

		If !Empty(cDataIniSe) .Or. !Empty(cDataFimSe) .Or. !Empty(cTimeIniSe) .Or. !Empty(cTimeFimSe) .Or. !Empty(cCodSetup)
			If Empty(cDataIniSe) 
				SetRestFault(400, EncodeUTF8(STR0019)) //"N�o foi informada a data de in�cio do setup."
				lPost := .F.			
			ElseIf Empty(cDataFimSe) 
				SetRestFault(400, EncodeUTF8(STR0020)) //"N�o foi informada a data final do setup."
				lPost := .F.		
			ElseIf Empty(cTimeIniSe)
				SetRestFault(400, EncodeUTF8(STR0021)) //"N�o foi informada a hora de in�cio do setup."
				lPost := .F.		
			ElseIf Empty(cTimeFimSe) 
				SetRestFault(400, EncodeUTF8(STR0022)) //"N�o foi informada a hora final do setup."
				lPost := .F.					
			ElseIf Empty(cCodSetup)
				SetRestFault(400, EncodeUTF8(STR0023)) //"N�o foi informado o c�digo do setup."
				lPost := .F.					
			EndIf
		EndIf
	EndIf

	If lPost
		If (cIniFim != 'I' .And. cIniFim != 'A') .Or. Empty(cIniFim) //Apontamento normal
			
			//Posicionar no split para ativar o model
			DbSelectArea("CYY")
			CYY->(DbSetOrder(1))
			If CYY->(!DbSeek(xFilial("CYY")+cOp+cIdOper+cSplit))
				SetRestFault(400, EncodeUTF8(STR0029)) //"Split n�o cadastrado."
   				lPost := .F.
			Endif		

			If lPost
				If CYY->CYY_CDMQ != cMaq
					SetRestFault(400, EncodeUTF8(STR0031)) //"M�quina n�o pertence ao Id da Opera��o."         
					lPost := .F.
				EndIf
			EndIf

			DbSelectArea("CY9")
			CY9->(DbSetOrder(1))
			If CY9->(!DbSeek(xFilial("CY9")+cOp+cIdOper))
				SetRestFault(400, EncodeUTF8(STR0043)) //"Opera��o n�o cadastrada."
   				lPost := .F.
			EndIf

			If lPost .And. Empty(cOper)				
				cOper := CY9->CY9_CDAT
			EndIf

			If lPost .And. !Empty(cOper)				
				If CY9->CY9_CDAT != cOper
					SetRestFault(400, EncodeUTF8(STR0032)) //"Opera��o n�o pertence ao Id da Opera��o."
					lPost := .F.
				EndIf
			EndIf

			If lPost .And. cIniFim == 'F'
				//Buscar serial
				cNRSQRP := ' '
				lPost := ConSeqApo(cOp, cIdOper, cSplit, @cNRSQRP)

				If !lPost 
					SetRestFault(400, EncodeUTF8(STR0034)) //"N�o foi poss�vel localizar a sequ�ncia do apontamento de inicio para realizar a finaliza��o."
				EndIf
			EndIf

			If lPost .And. cIniFim == 'F'
				DbSelectArea("CYV")
				CYV->(DbSetOrder(1))
				If CYV->(!DbSeek(xFilial("CYV")+cNRSQRP))					
					SetRestFault(400, EncodeUTF8(STR0035)) //"Serial do apontamento de inicio n�o cadastrado."
					lPost := .F.
				Else
					cDataIniRe := CYV->CYV_DTRPBG
					cTimeIniRe := CYV->CYV_HRRPBG					
				EndIf
			EndIf

			If lPost				
				// Instancia o modelo
				oModel := FWLoadModel( "SFCA314" )

				//Limpa vari�vel de erro
				aErro := oModel:GetErrorMessage(.T.)
				aErro := {}

				If cIniFim == 'F'
					oModel:SetOperation( 4 ) //Finalizar apontamento
				Else
					oModel:SetOperation( 3 ) //Incluir apontamento
				EndIf

				If !oModel:Activate()   				
   					lPost := .F.
					aErro := oModel:GetErrorMessage()
     				If !Empty(aErro[6])
        				cMsgErro := oModel:GetErrorMessage()[6]
     				Else
						cMsgErro := (STR0026) //"Ocorreram erros ao realizar o apontamento."
     				EndIf     								
					SetRestFault(400, EncodeUTF8(cMsgErro))   
				EndIf
			EndIf

			If lPost
				//MODELOS
				oModelCY0 := oModel:GetModel( "CY0DETAIL" )               //REFUGO
				oModelCYW := oModel:GetModel( "CYWDETAIL" )               //MAO DE OBRA
				oModelCZ0 := oModel:GetModel( "CZ0DETAIL" )               //FERRAMENTA
			
				oModel:SetValue("CYVMASTER","CYV_NRORPO",cOp)             //OP
         		oModel:SetValue("CYVMASTER","CYV_IDAT"  ,cIdOper) 	      //ID OPERA��O
				oModel:SetValue("CYVMASTER","CYV_CDAT"  ,cOper)           //OPERA��O
         		oModel:SetValue("CYVMASTER","CYV_IDATQO",cSplit)  	      //SPLIT
         		oModel:SetValue("CYVMASTER","CYV_CDMQ"  ,cMaq)            //MAQUINA	
				oModel:SetValue("CYVMASTER","CYV_DTRPBG",cDataIniRe)      //DATA INICIO REPORTE         	
				oModel:SetValue("CYVMASTER","CYV_HRRPBG",cTimeIniRe)      //HORA INICIO REPORTE
         	
				//INFORMA��ES SETUP
				If !Empty(cCodSetup)
					oModel:SetValue("CYVMASTER","CYV_DTBGSU",cDataIniSe)  //DATA INICIO SETUP
         			oModel:SetValue("CYVMASTER","CYV_DTEDSU",cDataFimSe)  //DATA FIM SETUP
					oModel:SetValue("CYVMASTER","CYV_HRBGSU",cTimeIniSe)  //HORA INICIO SETUP
         			oModel:SetValue("CYVMASTER","CYV_HREDSU",cTimeFimSe)  //HORA FIM SETUP
					oModel:SetValue("CYVMASTER","CYV_CDSU",cCodSetup)     //CODIGO SETUP
				EndIf

				//QUANTIDADES
				lRet := oModel:SetValue("CYVMASTER","CYV_QTATRP",nQuant)  //QUANTIDADE REPORTADA			
				lRet := oModel:SetValue("CYVMASTER","CYV_QTATAP",nQuant)  //QUANTIDADE APROVADA
				oModel:SetValue("CYVMASTER","CYV_QTATRF",0)               //QUANTIDADE REFUGADA 
				oModel:SetValue("CYVMASTER","CYV_QTATRT",0)               //QUANTIDADE RETRABALHADA
				//aError := oModel:GetModel():GetErrorMessage()
						
				If !Empty(cItem)
					oModel:SetValue("CYVMASTER","CYV_CDACRP",cItem)             //ITEM
				EndIf
				If !Empty(cTurno)
					oModel:SetValue("CYVMASTER","CYV_CDTN",cTurno)             //TURNO
				EndIf
				If !Empty(cDoc)
					oModel:SetValue("CYVMASTER","CYV_NRDO",cDoc)               //Documento
				EndIf
				If !Empty(cSerieDoc)			
					oModel:SetValue("CYVMASTER","CYV_NRSR",cSerieDoc)          //S�rie Documento
				EndIf
				If !Empty(cDeposito)			
					oModel:SetValue("CYVMASTER","CYV_CDDP",cDeposito)          //Dep�sito
				EndIf
				If !Empty(cLoteSerie)			
					oModel:SetValue("CYVMASTER","CYV_CDLOSR",cLoteSerie)       //Lote/Serie
				EndIf
				If !Empty(cValLote)			
					oModel:SetValue("CYVMASTER","CYV_DTVDLO",cValLote)         //Data Validade Lote
				EndIf

				oModel:SetValue("CYVMASTER","CYV_DTRPED",cDataFimRe)      //DATA FIM REPORTE
				oModel:SetValue("CYVMASTER","CYV_HRRPED",cTimeFimRe)      //HORA FIM REPORTE

				//FERRAMENTA
				If !Empty(cFerram)
					oModelCZ0:SetValue("CZ0_CDFE",cFerram)
				EndIf

				//MAO DE OBRA
				If !Empty(cOperador)
      				oModelCYW:SetValue("CYW_CDOE",cOperador)
				EndIf
				If !Empty(cEquipe)   
      				oModelCYW:SetValue("CYW_CDGROE",cEquipe)
				EndIf
				If !Empty(cOperador) .Or. !Empty(cEquipe)    
   					oModelCYW:SetValue("CYW_DTBGRP",cDataIniRe)
   					oModelCYW:SetValue("CYW_HRBGRP",cTimeIniRe)
   					oModelCYW:SetValue("CYW_DTEDRP",cDataFimRe)
   					oModelCYW:SetValue("CYW_HREDRP",cTimeFimRe)
				EndIf

				// Valida o modelo
				If oModel:VldData()   				
   					If !oModel:CommitData()
     					lPost := .F.					
						aErro := oModel:GetErrorMessage()
     					If !Empty(aErro[6])
        					cMsgErro := oModel:GetErrorMessage()[6]
     					Else
							cMsgErro := (STR0026) //"Ocorreram erros ao realizar o apontamento."
     					EndIf     								
						SetRestFault(400, EncodeUTF8(cMsgErro))
   					EndIf
				Else
   					lPost := .F.					
					aErro := oModel:GetErrorMessage()
     				If !Empty(aErro[6])
        				cMsgErro := oModel:GetErrorMessage()[6]
     				Else
						cMsgErro := (STR0026) //"Ocorreram erros ao realizar o apontamento."
     				EndIf     								
					SetRestFault(400, EncodeUTF8(cMsgErro))
				EndIf
			EndIf

			If lPost
				oJson['Status'] := (STR0041)//'Apontamento efetuado com sucesso'
						
				//saldo da ordem ap�s o apontamento
				DbSelectArea("SC2")
				SC2->(dbSetOrder(1))
				SC2->(dbSeek(xFilial("SC2")+cOp))			
				If !Empty(SC2->C2_DATRF) 			
					oJson['OpClose'] := .T.
				Else
					oJson['OpClose'] := .F.
				EndIf
			 
		    	oJson['ApprovedQuantity'] := SC2->C2_QUANT - SC2->C2_QUJE - If(lPerdInf,0,SC2->C2_PERDA) 
				::SetResponse(oJson:toJson())		
			EndIf
		Else
			If cIniFim == 'I'	

				//Posicionar no split para ativar o model
				DbSelectArea("CYY")
				CYY->(DbSetOrder(1))
				If CYY->(!DbSeek(xFilial("CYY")+cOp+cIdOper+cSplit))
					SetRestFault(400, EncodeUTF8(STR0029)) //"Split n�o cadastrado."
   					lPost := .F.
				Endif		

				If lPost
					// Instancia o modelo
					oModel := FWLoadModel( "SFCA319" )

					//Limpa vari�vel de erro
					aErro := oModel:GetErrorMessage(.T.)
					aErro := {}

					oModel:SetOperation( 3 ) //iniciar apontamento
					If !oModel:Activate()   				
   						lPost := .F.
						aErro := oModel:GetErrorMessage()
     					If !Empty(aErro[6])
        					cMsgErro := oModel:GetErrorMessage()[6]
     					Else
							cMsgErro := (STR0026) //"Ocorreram erros ao realizar o apontamento."
     					EndIf     								
						SetRestFault(400, EncodeUTF8(cMsgErro))   
					EndIf
				EndIf

				If lPost
					oModel:SetValue("CYVMASTER","CYV_NRORPO",cOp)             //OP
         			oModel:SetValue("CYVMASTER","CYV_IDAT"  ,cIdOper) 	      //ID OPERA��O
         			oModel:SetValue("CYVMASTER","CYV_IDATQO",cSplit)  	      //SPLIT
         			oModel:SetValue("CYVMASTER","CYV_CDMQ"  ,cMaq)            //MAQUINA	
					oModel:SetValue("CYVMASTER","CYV_DTRPBG",cDataIniRe)      //DATA INICIO REPORTE         	
					oModel:SetValue("CYVMASTER","CYV_HRRPBG",cTimeIniRe)      //HORA INICIO REPORTE

					// Valida o modelo
					If oModel:VldData()   				
   						If !oModel:CommitData()
     						lPost := .F.					
							aErro := oModel:GetErrorMessage()
     						If !Empty(aErro[6])
        						cMsgErro := oModel:GetErrorMessage()[6]
     						Else
								cMsgErro := (STR0026) //"Ocorreram erros ao realizar o apontamento."
     						EndIf     								
							SetRestFault(400, EncodeUTF8(cMsgErro))
   						EndIf
					Else
   						lPost := .F.					
						aErro := oModel:GetErrorMessage()
     					If !Empty(aErro[6])
        					cMsgErro := oModel:GetErrorMessage()[6]
     					Else
							cMsgErro := (STR0026) //"Ocorreram erros ao realizar o apontamento."
     					EndIf     								
						SetRestFault(400, EncodeUTF8(cMsgErro))
					EndIf
				EndIf
	
				If lPost
					oJson['Status'] := (STR0042) //'Inicio do Apontamento efetuado com sucesso'
					::SetResponse(oJson:toJson())		
				EndIf
			Else
				If cIniFim == 'A'
					//Posicionar no split para ativar o model
					DbSelectArea("CYY")
					CYY->(DbSetOrder(1))
					If CYY->(!DbSeek(xFilial("CYY")+cOp+cIdOper+cSplit))
						SetRestFault(400, EncodeUTF8(STR0029)) //"Split n�o cadastrado."
   						lPost := .F.
					Endif		

					If lPost
						If CYY->CYY_CDMQ != cMaq
							SetRestFault(400, EncodeUTF8(STR0031)) //"M�quina n�o pertence ao Id da Opera��o."         
							lPost := .F.
						EndIf
					EndIf
		
					If lPost
						//Buscar serial
						cNRSQRP := ' '
						lPost := ConSeqApo(cOp, cIdOper, cSplit, @cNRSQRP)

						If !lPost 
							SetRestFault(400, EncodeUTF8(STR0036)) //"N�o foi poss�vel localizar a sequ�ncia do apontamento de inicio para abandonar o apontamento."
						EndIf
					EndIf

					If lPost
						DbSelectArea("CYV")
						CYV->(DbSetOrder(1))
						If CYV->(!DbSeek(xFilial("CYV")+cNRSQRP))					
							SetRestFault(400, EncodeUTF8(STR0035)) //"Serial do apontamento de inicio n�o cadastrado."
							lPost := .F.
						EndIf
					EndIf

					If lPost
						// Verificar se apontamento selecionado est� iniciado
						If !(!CYV->CYV_LGRPEO .AND. CYV->CYV_TPSTRP == '1')
							SetRestFault(400, EncodeUTF8(STR0037)) //"Somente apontamentos iniciados podem ser abandonados."
							lPost := .F.
						EndIf
					EndIf					
					
					If lPost
						// Instancia o modelo
						oModel := FWLoadModel( "SFCA319" )

						//Limpa vari�vel de erro
						aErro := oModel:GetErrorMessage(.T.)
						aErro := {}

						oModel:SetOperation( 4 ) //abandonar apontamento
						If !oModel:Activate()   				
   							lPost := .F.
							aErro := oModel:GetErrorMessage()
     						If !Empty(aErro[6])
        						cMsgErro := oModel:GetErrorMessage()[6]
     						Else
								cMsgErro := (STR0026) //"Ocorreram erros ao realizar o apontamento."
     						EndIf     								
							SetRestFault(400, EncodeUTF8(cMsgErro))   
						EndIf
					EndIf

					If lPost
						oModel:SetValue('CYVMASTER','CYV_CDUSOE', CYV->CYV_CDUSRP)
						oModel:SetValue('CYVMASTER','CYV_DTEO'  , DATE())
						oModel:SetValue('CYVMASTER','CYV_LGRPEO', .T.)

						// Valida o modelo
						If oModel:VldData()   				
   							If !oModel:CommitData()
     							lPost := .F.					
								aErro := oModel:GetErrorMessage()
     							If !Empty(aErro[6])
        							cMsgErro := oModel:GetErrorMessage()[6]
     							Else
									cMsgErro := (STR0026) //"Ocorreram erros ao realizar o apontamento."
     							EndIf     								
								SetRestFault(400, EncodeUTF8(cMsgErro))
   							EndIf
						Else
   							lPost := .F.					
							aErro := oModel:GetErrorMessage()
     						If !Empty(aErro[6])
        						cMsgErro := oModel:GetErrorMessage()[6]
     						Else
								cMsgErro := (STR0026) //"Ocorreram erros ao realizar o apontamento."
     						EndIf     								
							SetRestFault(400, EncodeUTF8(cMsgErro))
						EndIf
					EndIf					
	
					If lPost
						oJson['Status'] = (STR0039) //"Apontamento abandonado com sucesso."
						::SetResponse(oJson:toJson())		
					EndIf
				EndIf	
			EndIf
		EndIf
	EndIf

	FreeObj(oJson)

Return lPost

/*/{Protheus.doc} FormataErro()
Fun��o para reunir e formatar as mensagens de erro para exibir no APP
@author Parffit Jim Balsanelli
@since 18/09/2018
@version 1.0
@return
/*/
Static Function FormataErro()

	Local nCount    := 0
	Local aErroAuto := {}
	Local cLogErro  := ""

	aErroAuto := GetAutoGRLog()
	For nCount := 1 To Len(aErroAuto)
		//Retorna somente a mensagem de erro (Help) e o valor que est� inv�lido, sem quebras de linha e sem tags '<>'
		cLogErro += StrTran( StrTran( StrTran( StrTran( StrTran( aErroAuto[nCount], "/", "" ), "<", "" ), ">", "" ), CHR(10), " "), CHR(13), "") + ("|")
	Next nCount

Return cLogErro

/*/{Protheus.doc} ConSplit()
Fun��o para consultar as informa��es do Split
@author Michele Lais Girardi
@since 28/05/2020
@version 1.0
@return
/*/
Static Function ConSplit(cOp, cMaq)
Local cQuery      := ""
Local cAlias      := ""
Local aCYY        := {}

    cAlias  := GetNextAlias()

	cQuery := " SELECT CYY.CYY_NRORPO NUM_OP, "
	cQuery += "        CYY.CYY_IDATQO SPLIT,  "
	cQuery += "        CYY.CYY_IDAT ID_OPER,  "
	cQuery += "        CY9.CY9_CDAT COD_OPER, "
	cQuery += "        CY9.CY9_DSAT DESC_OPER "
	cQuery += "   FROM " + RetSqlName("CYY") + " CYY " + "," + RetSqlName("CY9") + " CY9 "
	cQuery += "  WHERE CY9.CY9_FILIAL  = '" + xFilial( "CY9" ) + "'"
	cQuery += "    AND CYY.CYY_FILIAL  = '" + xFilial( "CYY" ) + "'"
	cQuery += "    AND CYY.CYY_NRORPO  = '" +cOp+ "'"
	cQuery += "    AND CYY.CYY_CDMQ    = '" +cMaq+ "'"
	cQuery += "    AND CYY.CYY_NRORPO  = CY9.CY9_NRORPO "
	cQuery += "    AND CYY.CYY_IDAT    = CY9.CY9_IDAT "
	cQuery += "    AND (CYY.CYY_QTAT - CYY.CYY_QTATAP - CYY.CYY_QTATRF) > 0
	cQuery += "    AND CYY.D_E_L_E_T_  = ' ' "
	cQuery += "    AND CY9.D_E_L_E_T_  = ' ' "
	cQuery += "  ORDER BY CYY.CYY_NRORPO, CYY.CYY_IDAT, CYY.CYY_IDATQO "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.T.,.T.)
	While (cAlias)->(!Eof())
		aAdd(aCYY,{(cAlias)->SPLIT, (cAlias)->ID_OPER, (cAlias)->COD_OPER, (cAlias)->DESC_OPER})
		(cAlias)->(dbSkip())    
	End

	(cAlias)->(DBCloseArea())

Return aCYY 

/*/{Protheus.doc} ConCab()
Fun��o para consultar as informa��es do cabe�alho do spli
@author Michele Lais Girardi
@since 28/05/2020
@version 1.0
@return
/*/
Static Function ConCab(cOp, cMaq)
Local cQuery      := ""
Local cAlias      := ""
Local aCab        := {}

    cAlias  := GetNextAlias()

	cQuery := " SELECT DISTINCT CZ3_CDAC  COD_ITEM,  "
	cQuery += "        CZ3_DSAC           DESC_ITEM,  "
	cQuery += "        CZ3_CDUN           UNID_MED  "
	cQuery += "   FROM " + RetSqlName("CYY") + " CYY " + "," + RetSqlName("CZ3") + " CZ3 "
	cQuery += "  WHERE CZ3.CZ3_FILIAL  = '" + xFilial( "CZ3" ) + "'"
	cQuery += "    AND CYY.CYY_FILIAL  = '" + xFilial( "CYY" ) + "'"
	cQuery += "    AND CYY.CYY_NRORPO  = '" +cOp+ "'"
	cQuery += "    AND CYY.CYY_CDMQ    = '" +cMaq+ "'"
	cQuery += "    AND CYY.CYY_CDAC    = CZ3.CZ3_CDAC "
	cQuery += "    AND CYY.D_E_L_E_T_  = ' ' "
	cQuery += "    AND CZ3.D_E_L_E_T_  = ' ' "


	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.T.,.T.)
	While (cAlias)->(!Eof())
		aAdd(aCab,{cOp, (cAlias)->COD_ITEM, (cAlias)->DESC_ITEM, (cAlias)->UNID_MED})
		Exit
	End

	(cAlias)->(DBCloseArea())

Return aCab 

//-------------------------------------------------------------------
/*/{Protheus.doc} ConSeqApo
Consulta a sequencia do apontamento que est� em aberto para a m�quina

@param  cNRORPO    Ordem de Produ��o
@param  cIDAT      Identificador Opera��o
@param  cIDATQO    Identificador Split

@return lRet        Se encontrou para retorna false

@author Michele Girardi
@since 27/07/2020
@version 12
/*/
//----------------------------------------------------------------
Function ConSeqApo(cNRORPO, cIDAT, cIDATQO, cNRSQRP)
Local lRet      := .F.
Local cQuery    := ""
Local cAliasCYV := GetNextAlias()

	cQuery := "  SELECT CYV_NRSQRP SEQAPON "
	cQuery += "    FROM " + RetSqlName("CYV") + " CYV " 
	cQuery += "   WHERE CYV.CYV_FILIAL  = '" + xFilial( "CYV" ) + "'"
	cQuery += "     AND CYV.CYV_NRORPO  = '" +cNRORPO+ "'"
	cQuery += "     AND CYV.CYV_IDAT    = '" +cIDAT+ "'"
	cQuery += "     AND CYV.CYV_IDATQO  = '" +cIDATQO+ "'"
	cQuery += "     AND CYV.CYV_TPSTRP  = '1' "
	cQuery += "     AND CYV.CYV_LGRPEO  = 'F' "
	cQuery += "     AND CYV.D_E_L_E_T_  = ' ' "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasCYV,.T.,.T.)
	While (cAliasCYV)->(!Eof())
		cNRSQRP := (cAliasCYV)->SEQAPON
		lRet := .T.
		Exit
	End

	(cAliasCYV)->(DBCloseArea())

Return lRet
