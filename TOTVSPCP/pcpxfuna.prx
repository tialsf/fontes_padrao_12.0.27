#INCLUDE "PROTHEUS.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} ClcPrzPcp()
DEVOLVE O PRAZO DE ENTREGA DE UM PRODUTO

@param cProduto	- Codigo do produto cujo prazo sera calculado.
@param nQuant	- Quantidade a comprar/produzir.
@param cFornece	- Codigo do fornecedor de compra.
@param cLoja    - Loja do fornecedor de compra.
@param lIniFim  - Indica se calcula pela data inicio.	
@param dData    - Data de referencia para calculo do prazo.
@param cRotOP   - Roteiro informado na OP

@author Michele Lais Girardi
@since 07/02/2019
@version P12
@return nPrazo
/*/
//--------------------------------------------------------------------
Function ClcPrzPcp(cProduto,nQuant,cFornece,cLoja,lIniFim,dData,cRotOP)
Local aArea    := GetArea()
Local aCalend  := {}
Local nPrazo   := 0
Local nHrsOper := 0
Local nHrsCale := 0
Local nHrsTot  := 0
Local nQtdeAux := 0
Local nX       := 0
Local cRoteiro := ""
Local dDataFim := dDataBase

PRIVATE Quantidade := 0
PRIVATE cTipoTemp  := SuperGetMV("MV_TPHR",.F.,"C")
PRIVATE nPrecisao  := SuperGetMV("MV_PRECISA",.F.,4)

Default nQuant 	 := 0
Default cFornece := Space(30)
Default cLoja 	 := Space(2)
Default lIniFim  := .T.

nQuant := IIF(nQuant == Nil .or. empty(nQuant),0,nQuant)

DbSelectArea("SB1")
If B1_COD != cProduto .or. xFilial("SB1") != B1_FILIAL
	dbSeek(xFilial("SB1")+cProduto)
Endif

nQuant 		:= IIF(nQuant==0,RetFldProd(SB1->B1_COD,"B1_LE"),nQuant)
nQuant 		:= IIF(nQuant==0,1,nQuant)
Quantidade	:=	nQuant          // Para Usar em formulas

//-- Se produto fabricado e MV_PEOPERA, calcula pelo G2
If !Empty(cRotOP)
	cValRot := cRotOP
Else
	If !Empty(SB1->B1_OPERPAD)
		cValRot := SB1->B1_OPERPAD
	Else
		cValRot := '01'
	EndIf
EndIf

cRoteiro  := A650VldRot(SB1->B1_COD, cValRot)

SG2->(dbSetOrder(1))
If SuperGetMV("MV_PEOPERA",.F.,.F.) .And. SG2->(dbSeek(xFilial("SG2")+SB1->B1_COD+cRoteiro)) .And. !Empty(dData)
	dDataFim := dData
	
	SH7->(dbSetOrder(1))
	SH1->(dbSetOrder(1))
	While !SG2->(EOF()) .And. SG2->(G2_FILIAL+G2_PRODUTO+G2_CODIGO) == xFilial("SG2")+SB1->B1_COD+cRoteiro
		//-- Posiciona recurso
		SH1->(dbSeek(xFilial("SH1")+SG2->G2_RECURSO))

		// Calcula tempo de duracao baseado no Tipo de Operacao
		If SG2->G2_TPOPER $ " 1"
			nHrsOper := Round(nQuant*(If(Empty(A690HoraCt(SG2->G2_TEMPAD)),1,A690HoraCt(SG2->G2_TEMPAD))/If(Empty(SG2->G2_LOTEPAD),1,SG2->G2_LOTEPAD)),5)
			If SH1->H1_MAOOBRA # 0
				nHrsOper := Round(nHrsOper/SH1->H1_MAOOBRA,5)
			EndIf
		ElseIf SG2->G2_TPOPER == "4"
			nQtdeAux := nQuant % If(Empty(SG2->G2_LOTEPAD),1,SG2->G2_LOTEPAD)
			nQtdeAux := Int(nQuant) + If(nQtdeAux > 0,If(Empty(SG2->G2_LOTEPAD),1,SG2->G2_LOTEPAD) - nQtdeAux,0)
			nHrsOper := Round(nQtdeAux * (If(Empty(A690HoraCt(SG2->G2_TEMPAD)),1,A690HoraCt(SG2->G2_TEMPAD)) / If(Empty(SG2->G2_LOTEPAD),1,SG2->G2_LOTEPAD)),5)
			If SH1->H1_MAOOBRA # 0
				nHrsOper := Round(nHrsOper/SH1->H1_MAOOBRA,5)
			EndIf
		ElseIf SG2->G2_TPOPER == "2" .Or. SG2->G2_TPOPER == "3"
			nHrsOper := If(Empty(A690HoraCt(SG2->G2_TEMPAD)),1,A690HoraCt(SG2->G2_TEMPAD))
		EndIf

		nHrsOper += A690HoraCt(If(Empty(SG2->G2_FORMSTP),SG2->G2_SETUP,Formula(SG2->G2_FORMSTP)))	//-- Soma setup
		nHrsOper += A690HoraCt(SG2->G2_TEMPEND)				//-- Soma tempo fim da operacao

		//-- Obtem horas trabalhadas na semana pelo calendario
		SH7->(dbSeek(xFilial("SH7")+SH1->H1_CALEND))
		aCalend := A780Arrays(4)
		
		//-- Aloca horas da operacao de acordo com horas do calendario
		While nHrsOper > 0 .And. !Empty(aCalend[1]+aCalend[2]+aCalend[3]+aCalend[4]+aCalend[5]+aCalend[6]+aCalend[7])
			//nX := If(Empty(Dow(dDataFim)-1),7,Dow(dDataFim)-1)
			nXDow := Dow(dDataFim)
			Do Case
				Case nXDow == 0
					nX := 7
				Case nXDow == 1
					nX := 7
				Case nXDow > 1				
					nX := nXDow - 1				 			
			EndCase
			
			If Empty(aCalend[nX])	//-- Dia nao util (pula o dia)
				nHrsTot += 24

				If lIniFim
					dDataFim++
				Else
					dDataFim--
				EndIf
			Else					//-- Dia util (considera horas do dia)
				nHrsCale := (Len(StrTran(aCalend[nX]," ","")) * (60 / nPrecisao)) / 60
				nHrsTot += Min(nHrsCale,nHrsOper)
				If nHrsCale <= nHrsOper
					If lIniFim
						dDataFim++
					Else
						dDataFim--
					EndIf
				EndIf
				nHrsOper -= Min(nHrsCale,nHrsOper)
			EndIf
		End

		SG2->(dbSkip())
	End

	nPrazo := Abs(dDataFim - dData)
Else
	If !Empty(nPrazo := Posicione("SA5",1,xFilial("SA5")+cFornece+cLoja+cProduto,"A5_PE"))
		If SA5->A5_TIPE == "4" 		// Mes
			nPrazo *= 30
		ElseIf SA5->A5_TIPE == "3"	// Semana
			nPrazo *= 7
		ElseIf SA5->A5_TIPE == "5"	// Ano
			nPrazo *= 365
		ElseIf SA5->A5_TIPE == "1"	// Hora
			nPrazo := Int(nPrazo/24)
		EndIf
	ElseIf Empty(RetFldProd(SB1->B1_COD,"B1_FORPRZ"))
		nPrazo := RetFldProd(SB1->B1_COD,"B1_PE")
		If RetFldProd(SB1->B1_COD,"B1_TIPE") == "M"		 // Mes
			nPrazo *= 30
		ElseIf RetFldProd(SB1->B1_COD,"B1_TIPE") == "S"	// Semana
			nPrazo *= 7
		ElseIf RetFldProd(SB1->B1_COD,"B1_TIPE") == "A"	// Ano
			nPrazo *= 365
		ElseIf RetFldProd(SB1->B1_COD,"B1_TIPE") == "H"	// Hora
			nPrazo := Int(nPrazo/24)
		EndIf
	Else
		nPrazo := Formula(RetFldProd(SB1->B1_COD,"B1_FORPRZ"))
		If ValType(nPrazo) # "N"
			Help(" ",1,"FORMUL")
			nPrazo:=0
		EndIf
		nPrazo := IIF(nPrazo == Nil,0,nPrazo)  // Nao existe a formula
	EndIf
End

RestArea(aArea)
Return nPrazo

/*
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o	 矨PrModRec     � Autor 砇odrigo de A Sartorio� Data � 21/03/06 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Retorna codigo do produto a ser pesquisado no cadastro de    潮�
北�          � produtos                                                     潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� cRecurso  = Recurso posicionado a ser pesquisado             潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so		 � Mata680,Mata681                                              潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ModRecPCP(cRecurso) 
LOCAL cRet      := ""
LOCAL cCusto    := ""
LOCAL cGrupo    := ""
LOCAL lTemIndice:= .F.
LOCAL aArea     := GetArea()
SH1->(dbSetOrder(1))

If ExistBlock("MTAREMOD")
	cRet := ExecBlock("MTAREMOD",.F.,.F.,{cRecurso})
EndIf

// Certifica-se de que o recurso esta posicionado
If SH1->(MsSeek(xFilial("SH1")+cRecurso)) .And. Empty(cRet)
	// Obtem os dados do recurso
	cCusto:=SH1->H1_CCUSTO
	cGrupo:=SH1->H1_GCCUSTO
	cRet  :="MOD"+Alltrim(cCusto)
	// Checa codigo de produto
	SB1->(dbSetOrder(1))
	If SB1->(MsSeek(xFilial("SB1")+cRet))
		If SB1->B1_MSBLQL == '1'
			lExistMod := .F.
		Else
			lExistMod := .T.
		EndIf
	Else
		lExistMod := .F.
	EndIf

	If !lExistMod
		dbSelectArea("SIX")
		dbSeek("SB1")
		While ("SB1" == INDICE) .and. !Eof()
			cSixChave := AllTrim(CHAVE)
			If SIX->ORDEM == "8" .And. cSixChave == "B1_FILIAL+B1_CCCUSTO+B1_GCCUSTO"
				lTemIndice:=.T.
				Exit
			EndIf
			dbSkip()
		End
		If lTemIndice
			dbSelectArea("SB1")
			SB1->(dbSetOrder(8))
			SB1->(dbGotop())
			dbSeek(xFilial("SB1")+cCusto+cGrupo)
			While !Eof() .And. (SB1->B1_FILIAL+SB1->B1_CCCUSTO+SB1->B1_GCCUSTO == xFilial("SB1")+cCusto+cGrupo)
				cRet := SB1->B1_COD
				If SB1->B1_MSBLQL == '1' //Bloqueado
					SB1->(dbSkip())
				Else
					Exit
				EndIf				
			End
		EndIf
	EndIf
EndIf
RestArea(aArea)
Return cRet



/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    砅CPCodUsr � Autor � Edson Maricate        � Data �27.01.1999潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Retorna o codigo do Usuario Corrente.                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� Void PCPCodUsr()                                           潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砇etorno   � RetC1  - Codigo do usuario corrente.                       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Generico.                                                  潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
/*/
Function PCPCodUsr(cUser)

Local cCodUser	:= "" 
Local aUsers   	:= {}
Local nPos     	:= 0
Local nLenUSR  	:= 0
//Default somente atribui se par鈓etro recebido for nulo, se for vazio n鉶 atribui
Default cUser := cUserName

If GetRemoteType() == -1 // Verifica se o acesso � por Job/WebService 
	aUsers   	:= FwSFAllUsers()
	nLenUSR  	:= Len(aUsers[1,3])
	nPos := aScan( aUsers, { |x| x[3] == Padr(cUser, nLenUSR) } )

	If nPos > 0 
		cCodUser := aUsers[nPos][2]
	ElseIf !Empty(cUserName)
		cCodUser := PCPCodUsr(cUserName)
	EndIf
	
	If Empty( cCodUser )
		cCodUser := __cUserId
	EndIf
	
Else
	cCodUser := __cUserId
Endif

Return cCodUser