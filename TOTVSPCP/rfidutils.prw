#INCLUDE "TOTVS.CH"
#INCLUDE "RFIDUTILS.CH"

Static __ExIMG11d := Nil
Static __ExIMG11i := Nil
Static __ExIMG11z := Nil
Static __BaseOk   := Nil

#DEFINE ARR_APARAM_PRODUTO   1
#DEFINE ARR_APARAM_LOTE      2
#DEFINE ARR_APARAM_SUBLOTE   3
#DEFINE ARR_APARAM_NUMSERIE  4
#DEFINE ARR_APARAM_QUANT     5
#DEFINE ARR_APARAM_COPIAS    6
#DEFINE ARR_APARAM_ALIAS     7
#DEFINE ARR_APARAM_CHAVE     8
#DEFINE ARR_APARAM_SIZE      8

#DEFINE ARR_DATAEPC_EXISTE   1
#DEFINE ARR_DATAEPC_QUANT    2
#DEFINE ARR_DATAEPC_ATIVO    3
#DEFINE ARR_DATAEPC_PRODUTO  4
#DEFINE ARR_DATAEPC_LOTE     5
#DEFINE ARR_DATAEPC_SUBLOTE  6
#DEFINE ARR_DATAEPC_NUMSERIE 7
#DEFINE ARR_DATAEPC_SIZE     7

/*/{Protheus.doc} RFIDNewEPC
Fun��o de cria��o de c�digos EPC e grava��o da HWN
@author marcelo.neumann
@since 22/05/2020
@version P12
@param 01 cProduto  , Caracter, c�digo do produto a ser gerado o EPC
@param 02 cLote     , Caracter, lote do produto
@param 03 cSubLote  , Caracter, sublote do produto
@param 04 cNumSerie , Caracter, n�ymero de s�rie do produto
@param 05 nQuant    , Num�rico, quantidade da EPC
@param 07 cAlias    , Caracter, alias do registro que deu origem � EPC
@param 08 cChaveOrig, Caracter, chave do registro que deu origem � EPC
@return   cCodEPC   , Caracter, c�digo EPC gerado
/*/
Function RFIDNewEPC(cProduto, cLote, cSubLote, cNumSerie, nQuant, cAlias, cChaveOrig)

	Local aAreaHWN  := HWN->(GetArea())
	Local aAreaSVK  := SVK->(GetArea())
	Local cChave    := xFilial("HWN") + Trim(cProduto)
	Local cCodEPC   := Nil
	Local cKeyHexa  := ""
	Local cSeqHexa  := ""
	Local nNextSeq  := 0
	Local nRFIDKey  := 0

	Default cProduto   := ""
	Default cLote      := ""
	Default cSubLote   := ""
	Default cNumSeri   := ""
	Default nQuant     := 0

	//Par�metro cProduto n�o pode ser branco
	If Empty(cProduto)
		Return Nil
	EndIf

	//Verifica se o produto usa RFID
	If !UsaRFID(cProduto)
		RestArea(aAreaSVK)
		Return Nil
	EndIf

	nRFIDKey := SVK->VK_RFIDKEY
	cKeyHexa := PadL(Upper(NToC(nRFIDKey,16)), 11, "0")

	If SemafProd(.T., cChave)
		nNextSeq := NextSeqHWN(cProduto)
		cSeqHexa := PadL(Upper(NToC(nNextSeq,16)), 13, "0")
		cCodEPC  := cKeyHexa + cSeqHexa

		RecLock("HWN",.T.)
			HWN->HWN_FILIAL := xFilial("HWN")
			HWN->HWN_EPC    := cCodEPC
			HWN->HWN_STATUS := "1"
			HWN->HWN_PROD   := cProduto
			HWN->HWN_LOTE   := cLote
			HWN->HWN_SUBLOT := cSubLote
			HWN->HWN_NUMSER := cNumSerie
			HWN->HWN_QUANT  := nQuant
			HWN->HWN_SEQUEN := nNextSeq
			HWN->HWN_ALIAS  := cAlias
			HWN->HWN_CHAVE  := cChaveOrig
		HWN->(MsUnLock())

		SemafProd(.F., cChave)

		RFIDGrvRas({{cCodEPC, nQuant}}, "1")
	EndIf

	RestArea(aAreaSVK)
	RestArea(aAreaHWN)

Return cCodEPC

/*/{Protheus.doc} BaseOk
Fun��o para verificar se a base est� ok para a utiliza��o do RFID
@type Static Function
@author marcelo.neumann
@since 22/05/2020
@version P12
@return __BaseOk, logical, indica se a base est� configurada adequadamente para o RFID
/*/
Static Function BaseOk()

	If __BaseOk == Nil
		__BaseOk := .F.

		//Tabela HWN n�o existe no dicion�rio de dados;
		If FWAliasInDic("HWN", .F.)
			__BaseOk := .T.
		EndIf
	EndIf

Return __BaseOk

/*/{Protheus.doc} UsaRFID
Verifica se a base e/ou o produto usa RFID.
Para validar somente a base, n�o passar o par�metro cProduto
@type Function
@author marcelo.neumann
@since 24/06/2020
@version P12
@param cProduto, Caracter, C�digo do produto caso deseje verificar se o produto controla RFID
@return L�gico, Indica se a base e/ou o produto usam RFID
/*/
Function UsaRFID(cProduto)

	Default cProduto := ""

	//Verifica se a base est� ok
	If !BaseOk()
		Return .F.
	EndIf

	If !Empty(cProduto)
		If SVK->VK_COD <> cProduto .Or. SVK->VK_FILIAL <> xFilial("SVK")
			//Produto recebido por par�metro n�o possui cadastro na tabela SVK
			SVK->(dbSetOrder(1))
			If !SVK->(dbSeek(xFilial("SVK") + cProduto))
				Return .F.
			EndIf
		EndIf

		//Produto recebido por par�metro n�o controla RFID (VK_RFID<>1)
		//Produto recebido por par�metro n�o possui VK_RFIDKEY informado
		If SVK->VK_RFID <> "1" .Or. Empty(SVK->VK_RFIDKEY)
			Return .F.
		EndIf
	EndIf

Return .T.

/*/{Protheus.doc} NextSeqHWN
Fun��o para buscar o pr�ximo sequencial do produto na tabela
@type Static Function
@author marcelo.neumann
@since 22/05/2020
@version P12
@param cProduto, caracter, c�digo do produto para buscar a �ltima sequ�ncia
@return nNextSeq, numeric, pr�xima sequ�ncia para o produto
/*/
Static Function NextSeqHWN(cProduto)

	Local cAliasQry := GetNextAlias()
	Local nNextSeq  := 0

    BeginSql Alias cAliasQry
		SELECT MAX(HWN.HWN_SEQUEN) MAXSEQ
		  FROM %Table:HWN% HWN
		 WHERE HWN.HWN_FILIAL = %xfilial:HWN%
		   AND HWN.HWN_PROD   = %Exp:cProduto%
		   AND HWN.%NotDel%
    EndSql

	nNextSeq := (cAliasQry)->MAXSEQ + 1

	(cAliasQry)->(dbCloseArea())

Return nNextSeq

/*/{Protheus.doc} SemafProd
Cria sem�foro para n�o repetir a �ltima sequ�ncia (HWN_SEQUEN) do produto
@type Static Function
@author marcelo.neumann
@since 22/05/2020
@version P12
@param 01 lLock   , logical  , indica se ser� feito o LOCK (.T.) ou UNLOCK (.F.)
@param 02 cProduto, character, produto a ser utilizado no sem�foro
@return logical, indica que conseguiu realizar o Lock
/*/
Static Function SemafProd(lLock, cChave)

	Local nTry := 0

	If lLock
		While !LockByName("RFID_SEQ_" + cChave, .T., .T.)
			If nTry > 1000
				Return .F.
			EndIf
			nTry++
			Sleep(100)
		End
	Else
		UnLockByName("RFID_SEQ_" + cChave, .T., .T.)
	EndIf

Return .T.

/*/{Protheus.doc} RFIDGetEpc
Recupera as informa��es de uma EPC

@type  Function
@author lucas.franca
@since 01/06/2020
@version P12.1.30
@param cEpc, Character, C�digo da EPC para busca
@return aDataEPC, Array, Array contendo as informa��es da EPC.
        aDataEPC[ARR_DATAEPC_EXISTE  ] - L�gico    - Identifica se a EPC existe.
        aDataEPC[ARR_DATAEPC_QUANT   ] - Number    - Quantidade da EPC
        aDataEPC[ARR_DATAEPC_ATIVO   ] - L�gico    - Identifica se a EPC est� ativa ou inativa.
        aDataEPC[ARR_DATAEPC_PRODUTO ] - Character - C�digo do produto
        aDataEPC[ARR_DATAEPC_LOTE    ] - Character - Lote do produto
        aDataEPC[ARR_DATAEPC_SUBLOTE ] - Character - Sub-lote do produto
        aDataEPC[ARR_DATAEPC_NUMSERIE] - Character - N�mero de s�rie do produto
/*/
Function RFIDGetEpc(cEpc)
	Local aDataEPC := Array(ARR_DATAEPC_SIZE)
	Local lExiste  := .T.

	cEpc := PadR(cEpc, GetSX3Cache("HWN_EPC","X3_TAMANHO"))

	If HWN->HWN_EPC != cEpc .Or. HWN->HWN_FILIAL != xFilial("HWN")
		HWN->(dbSetOrder(1))
		If !HWN->(dbSeek(xFilial("HWN")+cEpc))
			lExiste := .F.
		EndIf
	EndIf

	If lExiste
		aDataEPC[ARR_DATAEPC_EXISTE  ] := .T.
		aDataEPC[ARR_DATAEPC_PRODUTO ] := HWN->HWN_PROD
		aDataEPC[ARR_DATAEPC_LOTE    ] := HWN->HWN_LOTE
		aDataEPC[ARR_DATAEPC_SUBLOTE ] := HWN->HWN_SUBLOT
		aDataEPC[ARR_DATAEPC_NUMSERIE] := HWN->HWN_NUMSER

		//Se a etiqueta n�o est� ativa, deve retornar a quantidade 0
		If HWN->HWN_STATUS == "1"
			aDataEPC[ARR_DATAEPC_QUANT] := HWN->HWN_QUANT
			aDataEPC[ARR_DATAEPC_ATIVO] := .T.
		Else
			aDataEPC[2] := 0
			aDataEPC[ARR_DATAEPC_QUANT] := 0
			aDataEPC[ARR_DATAEPC_ATIVO] := .F.
		EndIf
	Else
		aDataEPC[ARR_DATAEPC_EXISTE  ] := .F.
		aDataEPC[ARR_DATAEPC_QUANT   ] := 0
		aDataEPC[ARR_DATAEPC_ATIVO   ] := .F.
		aDataEPC[ARR_DATAEPC_PRODUTO ] := ""
		aDataEPC[ARR_DATAEPC_LOTE    ] := ""
		aDataEPC[ARR_DATAEPC_SUBLOTE ] := ""
		aDataEPC[ARR_DATAEPC_NUMSERIE] := ""
	EndIf

Return aDataEPC

/*/{Protheus.doc} RFIDSelImp
Fun��o para selecionar a impressora que gravar� as etiquetas RFID
@type Function
@author marcelo.neumann
@since 09/06/2020
@version P12
@return cCodImpr, Caracter, C�digo da impressora selecionada (CB5_CODIGO)
/*/
Function RFIDSelImp()

	Local cCodImpr := ""
	Local nCount   := 0

	dbSelectArea("CB5")
	CB5->(dbSetOrder(1))
	If CB5->(dbSeek(xFilial("CB5")))
		nCount := 1

		CB5->(dbSkip())
		If !CB5->(EOF()) .And. CB5->CB5_FILIAL == xFilial("CB5")
			nCount := 2
		EndIf

		CB5->(dbSkip(-1))
	EndIf

	//Se n�o existir nenhuma impressora cadastrada, emitir mensagem de erro
	If nCount == 0
		Help( ,  , "Help", ,  STR0001,; //"N�o existe impressora cadastrada."
 			 1, 0, , , , , , {STR0002}) //"Cadastre o local de impress�o (ACDA060)."

	//Se existir somente uma impressora cadastrada, utilizar essa impressora
	ElseIf nCount == 1
		cCodImpr := CB5->CB5_CODIGO

	//Se existir mais de uma impressora cadastrada, exibir uma tela para que o usu�rio escolha qual modelo de impressora � o desejado
	ElseIf nCount > 1
		If ConPad1(,,,"CB5PCP",,, .F.)
			cCodImpr := CB5->CB5_CODIGO
		EndIf
	EndIf

Return cCodImpr

/*/{Protheus.doc} ExisRdmake
Fun��o para verificar se o RDMAKE de impress�o RFID est� compilado no RPO
@type Static Function
@author marcelo.neumann
@since 04/06/2020
@version P12
@param  cRDMAKE , Caracter, C�digo do RDMAKE a verificar a exist�ncia
@return lExIMG11, L�gico  , Indica se o RDMAKE est� compilado no RPO
/*/
Static Function ExisRdmake(cRDMAKE)

	Local lExIMG11 := .F.

	//Fabricante DATAMAX
	If cRDMAKE == "IMG11D"
		If __ExIMG11d == Nil
			__ExIMG11d := .F.

			If ExistBlock(cRDMAKE)
				__ExIMG11d := .T.
			EndIf
		EndIf
		lExIMG11 := __ExIMG11d

	//Fabricante INTERMEC
	ElseIf cRDMAKE == "IMG11I"
		If __ExIMG11i == Nil
			__ExIMG11i := .F.

			If ExistBlock(cRDMAKE)
				__ExIMG11i := .T.
			EndIf
		EndIf
		lExIMG11 := __ExIMG11i

	//Fabricante ZEBRA
	ElseIf cRDMAKE == "IMG11Z"
		If __ExIMG11z == Nil
			__ExIMG11z := .F.

			If ExistBlock(cRDMAKE)
				__ExIMG11z := .T.
			EndIf
		EndIf
		lExIMG11 := __ExIMG11z
	EndIf

Return lExIMG11

/*/{Protheus.doc} RFIDPrint
Prepara impress�o da etiqueta RFID
@type Function
@author marcelo.neumann
@since 10/07/2020
@version P12
@param 01 cRotina, Caracter, Rotina que est� chamando a impress�o da etiqueta
@param 02 aParam , Array   , Array com os registros a serem impressos:
                             aParam[1], Caracter, C�digo da Impressora
							 aParam[2], Array   , Lista de registros a imprimir:
							                      aParam[2][n][1], Caracter, C�digo do produto
												  aParam[2][n][2], Caracter, Lote do produto
												  aParam[2][n][3], Caracter, SubLote do produto
												  aParam[2][n][4], Caracter, Serie do produto
												  aParam[2][n][5], Num�rico, Quantidade do produto
												  aParam[2][n][6], Caracter, Quantidade de produto na etiqueta
												  aParam[2][n][7], Caracter, Alias da origem do registro
												  aParam[2][n][8], Caracter, Chave da origem do registro
@return lOk, L�gico  , Indica se a etiqueta foi impressa
/*/
Function RFIDPrint(cRotina, aParam)

	Local aAreaSB1  := SB1->(GetArea())
	Local aAreaSVK  := SVK->(GetArea())
	Local aProdImp  := {}
	Local cCodImpr  := ""
	Local cHelpErro := ""
	Local cHelpSolu := ""
	Local cRDMAKE   := "IMG11"
	Local lLimpaPar := .F.
	Local lOk       := .T.
	Local nIndex    := 1
	Local nQtdRegs  := 0
	Default aParam  := {}

	//Verifica se a base est� ok
	If !UsaRFID()
		Return .F.
	EndIf

	Do Case
		//Tratamento exclusivo da Rotina MATA103 (Documento de Entrada)
		Case cRotina == "MATA103"
			If (Inclui .Or. Altera) .And. ;
			   ((SA2->A2_IMPIP == "4") .Or. (SA2->A2_IMPIP $ "03 " .And. SuperGetMv("MV_IMPIP",.F.,"3") == "4" ))

				//Busca impressora cadastrada no par�metro 'MV_IACD02'
				cCodImpr := SuperGetMV("MV_IACD02",.F.)
				If Empty(cCodImpr)
					cHelpErro := STR0003                   //"Impressora n�o informada."
					cHelpSolu := STR0021 + " 'MV_IACD02'." //"Cadastre a impressora RFID no par�metro"
				EndIf

				lLimpaPar := .T.
				aSize(aParam, 0)
				aAdd(aParam, cCodImpr)
				aAdd(aParam, {})
				nQtdRegs := GetRegsSD1(@aParam[2])
			Else
				lOk := .F.
			EndIf

		//Tratamento exclusivo da Rotina MATA140 (Pr�-nota de Entrada)
		Case cRotina == "MATA140"
			If SF1->F1_STATCON <> "1" .And. ;
			   (!l140Auto .Or. GetAutoPar("AUTIMPIP", aAutoCab, 0) == 1) .And. ;
			   ((SA2->A2_IMPIP == "2") .Or. (SA2->A2_IMPIP $ "03 " .And. SuperGetMv("MV_IMPIP", .F., "3") == "2"))

				//Busca impressora cadastrada no par�metro 'MV_IACD02'
				cCodImpr := SuperGetMV("MV_IACD02",.F.)
				If Empty(cCodImpr)
					cHelpErro := STR0003                   //"Impressora n�o informada."
					cHelpSolu := STR0021 + " 'MV_IACD02'." //"Cadastre a impressora RFID no par�metro"
				EndIf

				lLimpaPar := .T.
				aSize(aParam, 0)
				aAdd(aParam, cCodImpr)
				aAdd(aParam, {})
				nQtdRegs := GetRegsSD1(@aParam[2])
			Else
				lOk := .F.
			Endif

		//Tratamento exclusivo da Rotina MATA250 (Apontamento)
		Case cRotina == "MATA250"
			//Busca impressora cadastrada no par�metro 'MV_IACD04'
			cCodImpr := SuperGetMV("MV_IACD04", .F.)
			If Empty(cCodImpr)
				cHelpErro := STR0003                   //"Impressora n�o informada."
				cHelpSolu := STR0021 + " 'MV_IACD04'." //"Cadastre a impressora RFID no par�metro"
			EndIf

			//Verifica se o produto usa RFID
			If UsaRFID(SD3->D3_COD)
				nQtdRegs  := 1
				lLimpaPar := .T.
				aSize(aParam, 0)
				aAdd(aParam, cCodImpr)
				aAdd(aParam, {})
				aAdd(aParam[2], Array(ARR_APARAM_SIZE))
				aParam[2][1][ARR_APARAM_PRODUTO ] := SD3->D3_COD
				aParam[2][1][ARR_APARAM_LOTE    ] := SD3->D3_LOTECTL
				aParam[2][1][ARR_APARAM_SUBLOTE ] := SD3->D3_NUMLOTE
				aParam[2][1][ARR_APARAM_NUMSERIE] := SD3->D3_NUMSERI
				aParam[2][1][ARR_APARAM_QUANT   ] := SD3->D3_QUANT
				aParam[2][1][ARR_APARAM_COPIAS  ] := NIL
				aParam[2][1][ARR_APARAM_ALIAS   ] := "SD3"
				aParam[2][1][ARR_APARAM_CHAVE   ] := SD3->D3_FILIAL + SD3->D3_COD + SD3->D3_NUMSEQ
			EndIf

		//Tratamento exclusivo da Rotina MATA265 (Endere�amento)
		Case cRotina == "MATA265"
			If Type("l265AUTO") <> 'L' .Or. !l265AUTO
				//Busca impressora cadastrada no par�metro 'MV_IACD03'
				cCodImpr := SuperGetMV("MV_IACD03",.F.)
				If Empty(cCodImpr)
					cHelpErro := STR0003                   //"Impressora n�o informada."
					cHelpSolu := STR0021 + " 'MV_IACD03'." //"Cadastre a impressora RFID no par�metro"
				EndIf

				lLimpaPar := .T.
				aSize(aParam, 0)
				aAdd(aParam, cCodImpr)
				aAdd(aParam, {})
				nQtdRegs := GetRegsSDB(@aParam[2])
			Else
				lOk := .F.
			Endif

		Otherwise
			//Avalia o array de par�metros para n�o estourar error.log
			If ValType(aParam)       != 'A' .Or. Empty(aParam)       .Or. Len(aParam)       < 2 .Or. ;
			   ValType(aParam[2])    != 'A' .Or. Empty(aParam[2])    .Or. Len(aParam[2])    < 1 .Or. ;
			   ValType(aParam[2][1]) != 'A' .Or. Empty(aParam[2][1]) .Or. Len(aParam[2][1]) < ARR_APARAM_SIZE

				lOk := .F.
				Help( ,  , "Help", ,  STR0020, ; //"Par�metros para impress�o da etiqueta RFID inv�lidos."
				     1, 0, , , , , , {STR0022})  //"Verifique os par�metros enviados."
			Else
				If Empty(aParam[1])
					cHelpErro := STR0003                         //"Impressora n�o informada."
					cHelpSolu := STR0004 + " '" + cRotina + "'." //"Verifique a impressora enviada pela rotina"
				EndIf
				cCodImpr := aParam[1]
				nQtdRegs := Len(aParam[2])
			EndIf
	EndCase

	If lOk
		//Se n�o tem mensagem de erro, manda para a impress�o
		If Empty(cHelpErro)
			//Valida��o da impressora
			lOk := VldImpress(cCodImpr, @cRDMAKE, @cHelpErro, @cHelpSolu)
		Else
			lOk := .F.
		EndIf

		//Valida quais produtos devem ter etiqueta impressa
		For nIndex := 1 To nQtdRegs
			If VldProduto(aParam[2][nIndex], cRotina)
				aAdd(aProdImp, aClone(aParam[2][nIndex]))

				//Se existe produto para imprimir, verifica se j� n�o houve erro de impressora
				If !lOk
					Exit
				EndIf
			EndIf
		Next nIndex

		//Limpa o array da mem�ria
		If lLimpaPar
			aSize(aParam, 0)
		EndIf

		//Se existe produto para imprimir, manda imprimir ou gera o erro (caso tenha)
		If Len(aProdImp) > 0
			//Se n�o tem mensagem de erro, manda para a impress�o
			If Empty(cHelpErro)
				lOk := ImprimeEti(cCodImpr, aProdImp, cRDMAKE)
			Else
				lOk := .F.
				Help( ,  , "Help", ,  cHelpErro, 1, 0, , , , , , {cHelpSolu})
			EndIf
			aSize(aProdImp, 0)
		EndIf
	EndIf

	RestArea(aAreaSB1)
	RestArea(aAreaSVK)

Return lOk

/*/{Protheus.doc} GetRegsSD1
Carrega os produtos da SD1 - Nota de Entrada
@type Static Function
@author marcelo.neumann
@since 10/07/2020
@version P12
@param 01 aProds, Caracter, Array para ser carregado com os produtos da nota (refer�ncia)
@return nQtdRegs, Num�rico, Quantidade de produtos da SD1
/*/
Static Function GetRegsSD1(aProds)

	Local cAliasQry := GetNextAlias()
	Local nQtdRegs  := 0

	BeginSql Alias cAliasQry
	  SELECT SD1.D1_COD,
	         SD1.D1_LOTECTL,
			 SD1.D1_NUMLOTE,
			 SD1.D1_QUANT,
			 SD1.D1_ITEM
	    FROM %Table:SD1% SD1,
             %Table:SVK% SVK,
             %Table:SB1% SB1
	   WHERE SD1.D1_FILIAL   = %xfilial:SD1%
		 AND SD1.D1_DOC      = %Exp:SF1->F1_DOC%
		 AND SD1.D1_SERIE    = %Exp:SF1->F1_SERIE%
		 AND SD1.D1_FORNECE  = %Exp:SF1->F1_FORNECE%
		 AND SD1.D1_LOJA     = %Exp:SF1->F1_LOJA%
		 AND SVK.VK_FILIAL   = %xfilial:SVK%
		 AND SVK.VK_COD      = SD1.D1_COD
		 AND SVK.VK_RFID     = '1'
		 AND SVK.VK_RFIDKEY <> 0
		 AND SB1.B1_FILIAL   = %xfilial:SB1%
		 AND SB1.B1_COD      = SVK.VK_COD
		 AND SB1.B1_LOCALIZ <> 'S'
		 AND SD1.%NotDel%
		 AND SVK.%NotDel%
		 AND SB1.%NotDel%
	EndSql

	While (cAliasQry)->(!Eof())
		nQtdRegs++
		aAdd(aProds, Array(ARR_APARAM_SIZE))

		aProds[nQtdRegs][ARR_APARAM_PRODUTO ] := (cAliasQry)->D1_COD
		aProds[nQtdRegs][ARR_APARAM_LOTE    ] := (cAliasQry)->D1_LOTECTL
		aProds[nQtdRegs][ARR_APARAM_SUBLOTE ] := (cAliasQry)->D1_NUMLOTE
		aProds[nQtdRegs][ARR_APARAM_NUMSERIE] := ""
		aProds[nQtdRegs][ARR_APARAM_QUANT   ] := (cAliasQry)->D1_QUANT
		aProds[nQtdRegs][ARR_APARAM_COPIAS  ] := NIL
		aProds[nQtdRegs][ARR_APARAM_ALIAS   ] := "SD1"
		aProds[nQtdRegs][ARR_APARAM_CHAVE   ] := xFilial("SD1") + SF1->F1_DOC + SF1->F1_SERIE + SF1->F1_FORNECE + SF1->F1_LOJA + ;
		                                         (cAliasQry)->D1_ITEM + (cAliasQry)->D1_COD + (cAliasQry)->D1_LOTECTL + (cAliasQry)->D1_NUMLOTE
		(cAliasQry)->(dbSkip())
	End

	(cAliasQry)->(dbCloseArea())

Return nQtdRegs

/*/{Protheus.doc} GetRegsSDB
Carrega os produtos da SDB - Endere�amento
@type Static Function
@author marcelo.neumann
@since 10/07/2020
@version P12
@param 01 aProds, Caracter, Array para ser carregado com os produtos (refer�ncia)
@return nQtdRegs, Num�rico, Quantidade de produtos da SDB
/*/
Static Function GetRegsSDB(aProds)

	Local cAliasQry := GetNextAlias()
	Local nQtdRegs  := 0

	BeginSql Alias cAliasQry
	  SELECT SDB.DB_ITEM,
	  		 SDB.DB_QUANT,
	  		 SDB.DB_NUMSERI
	    FROM %Table:SDB% SDB,
             %Table:SVK% SVK,
             %Table:SB1% SB1
	   WHERE SDB.DB_FILIAL   = %xfilial:SDB%
		 AND SDB.DB_PRODUTO  = %Exp:SDA->DA_PRODUTO%
		 AND SDB.DB_LOCAL    = %Exp:SDA->DA_LOCAL%
		 AND SDB.DB_NUMSEQ   = %Exp:SDA->DA_NUMSEQ%
		 AND SDB.DB_DOC      = %Exp:SDA->DA_DOC%
		 AND SDB.DB_SERIE    = %Exp:SDA->DA_SERIE%
		 AND SDB.DB_CLIFOR   = %Exp:SDA->DA_CLIFOR%
		 AND SDB.DB_LOJA     = %Exp:SDA->DA_LOJA%
		 AND SDB.DB_ESTORNO <> "S"
		 AND SVK.VK_FILIAL   = %xfilial:SVK%
		 AND SVK.VK_COD      = SDB.DB_PRODUTO
		 AND SVK.VK_RFID     = '1'
		 AND SVK.VK_RFIDKEY <> 0
		 AND SB1.B1_FILIAL   = %xfilial:SB1%
		 AND SB1.B1_COD      = SVK.VK_COD
		 AND SDB.%NotDel%
		 AND SVK.%NotDel%
		 AND SB1.%NotDel%
	EndSql

	While (cAliasQry)->(!Eof())
		nQtdRegs++
		aAdd(aProds, Array(ARR_APARAM_SIZE))

		aProds[nQtdRegs][ARR_APARAM_PRODUTO ] := SDA->DA_PRODUTO
		aProds[nQtdRegs][ARR_APARAM_LOTE    ] := SDA->DA_LOTECTL
		aProds[nQtdRegs][ARR_APARAM_SUBLOTE ] := SDA->DA_NUMLOTE
		aProds[nQtdRegs][ARR_APARAM_NUMSERIE] := (cAliasQry)->DB_NUMSERI
		aProds[nQtdRegs][ARR_APARAM_QUANT   ] := (cAliasQry)->DB_QUANT
		aProds[nQtdRegs][ARR_APARAM_COPIAS  ] := NIL
		aProds[nQtdRegs][ARR_APARAM_ALIAS   ] := "SDB"
		aProds[nQtdRegs][ARR_APARAM_CHAVE   ] := xFilial("SDB") + SDA->DA_PRODUTO + SDA->DA_LOCAL + SDA->DA_NUMSEQ + ;
		                                         SDA->DA_DOC + SDA->DA_SERIE + SDA->DA_CLIFOR + SDA->DA_LOJA + (cAliasQry)->DB_ITEM
		(cAliasQry)->(dbSkip())
	End

	(cAliasQry)->(dbCloseArea())

Return nQtdRegs

/*/{Protheus.doc} VldProduto
Verifica se deve ser impressa a etiqueta para o produto
@type Static Function
@author marcelo.neumann
@since 10/07/2020
@version P12
@param 01 aProduto, Array   , Array com as informa��es do produto a ser impresso
@param 02 cRotina , Caracter, Rotina que est� chamando a impress�o da etiqueta
@return L�gico, Indica se deve imprimir a etiqueta para o produto
/*/
Static Function VldProduto(aProduto, cRotina)

	//Se vier da rotina de impress�o, n�o faz valida��o, somente imprime
	If cRotina == "PCPA150"
		Return .T.
	Endif

	//Se o produto tiver controle de endere�o, s� imprime se a rotina for MATA265 (MATA140 e MATA103 j� est� excluindo do aParam)
	If cRotina <> "MATA140" .And. cRotina <> "MATA103" .And. cRotina <> "MATA265"
		If Localiza(aProduto[ARR_APARAM_PRODUTO])
			Return .F.
		EndIf
	EndIf

	//Verifica se j� foi impressa etiqueta para o produto (para o MATA250 n�o � necess�ria essa verifica��o)
	If cRotina <> "MATA250"
		If !ValidReimp(aProduto[ARR_APARAM_ALIAS], aProduto[ARR_APARAM_CHAVE], @aProduto[ARR_APARAM_QUANT])
			Return .F.
		EndIf
	EndIf

Return .T.

/*/{Protheus.doc} ValidReimp
Verifica se j� foi impressa alguma etiqueta para o produto (chave) e questiona a reimpress�o
@type Static Function
@author marcelo.neumann
@since 10/07/2020
@version P12
@param 01 cAlias , Caracter, Alias do registro que deu origem � EPC (HWN_ALIAS)
@param 02 cChave , Caracter, Chave do registro que deu origem � EPC (HWN_CHAVE)
@param 03 nQuant , Num�rico, Quantidade do produto (passado por refer�ncia)
@return lImprimir, L�gico  , Indica se deve chamar a fun��o de impress�o da etiqueta
/*/
Static Function ValidReimp(cAlias, cChave, nQuant)

	Local aOpcoes   := {}
	Local cAliasQry := ""
	Local cMensagem := ""
	Local lImprimir := .T.
	Local nQtdHWN   := 0

	//Se for job, s� imprime se nunca foi immpressa uma etiqueta para a chave
	If IsBlind()
		HWN->(dbSetOrder(3))
		lImprimir := !(HWN->(dbSeek(xFilial("HWN") + cAlias + cChave)))
	Else
		//Busca a quantidade de etiquetas j� impressas
		cAliasQry := GetNextAlias()

		BeginSql Alias cAliasQry
		  SELECT SUM(HWN.HWN_QUANT) QUANT
			FROM %Table:HWN% HWN
		   WHERE HWN.HWN_FILIAL = %xfilial:HWN%
			 AND HWN.HWN_ALIAS  = %Exp:cAlias%
			 AND HWN.HWN_CHAVE  = %Exp:cChave%
			 AND HWN.%NotDel%
		EndSql

		nQtdHWN := (cAliasQry)->QUANT

		(cAliasQry)->(dbCloseArea())

		//Verifica se j� foi impressa alguma etiqueta para esse produto
		If nQtdHWN > 0
			If nQuant == nQtdHWN
				lImprimir := .F.
			Else
				HWN->(dbSetOrder(3))
				HWN->(dbSeek(xFilial("HWN") + cAlias + cChave))

				cMensagem := STR0025 //"J� foi impressa a etiqueta RFID para esse produto:"
				cMensagem += CHR(13) + CHR(10)
				cMensagem += STR0026 + ": " + AllTrim(HWN->HWN_PROD) //"Produto:

				If !Empty(HWN->HWN_LOTE)
					cMensagem += CHR(13) + CHR(10) + STR0027 + ": " + AllTrim(HWN->HWN_LOTE) //"Lote"

					If !Empty(HWN->HWN_SUBLOT)
						cMensagem += CHR(13) + CHR(10) + STR0028 + ": " + AllTrim(HWN->HWN_LOTE) //"Sublote"
					EndIf
				EndIf

				If !Empty(HWN->HWN_NUMSER)
					cMensagem += CHR(13) + CHR(10) + STR0029 + ": " + AllTrim(HWN->HWN_NUMSER) //"S�rie"
				EndIf

				cMensagem += CHR(13) + CHR(10)
				cMensagem += CHR(13) + CHR(10)
				cMensagem += STR0030 + ": " + cValToChar(nQtdHWN) //"Quantidade Impressa"
				cMensagem += CHR(13) + CHR(10)
				cMensagem += STR0031 + ": " + cValToChar(nQuant) //"Quantidade Atual"
				cMensagem += CHR(13) + CHR(10)
				cMensagem += CHR(13) + CHR(10)
				cMensagem += STR0032 //"Deseja reimprimir?"

				If nQtdHWN < nQuant
					aOpcoes := {STR0033 + " (" + cValToChar(nQuant - nQtdHWN) + ")", STR0034, STR0035} //"Somente Diferen�a, "N�o, "Reimprimir"
				Else
					aOpcoes := {STR0034, STR0035}
				EndIf

				nOpcao := Aviso(STR0036, cMensagem, aOpcoes, 3) //"RFID - Reimpress�o"

				If nOpcao == Len(aOpcoes) - 1
					lImprimir := .F.
				ElseIf nOpcao < Len(aOpcoes)
					nQuant -= nQtdHWN
				Endif
			EndIf
		EndIf
	EndIf

Return lImprimir

/*/{Protheus.doc} ImprimeEti
Prepara a impressora e imprime as etiquetas dos produtos
@type Static Function
@author marcelo.neumann
@since 10/07/2020
@version P12
@param 01 cCodImpr, Caracter, C�digo da impressora que ser� usada
@param 02 aProdImp, Array   , Array com as informa��es dos produtos
@param 03 cRDMAKE , Caracter, Nome do fonte RDMAKE que faz a impress�o da etiqueta
@return   lOk     , L�gico  , Indica se todas as etiquetas foram impressas
/*/
Static Function ImprimeEti(cCodImpr, aProdImp, cRDMAKE)
	Local aImpResto := {}
	Local lOk       := .T.
	Local nIndex    := 1
	Local nQtdRegs  := Len(aProdImp)
	Local nCopias   := 0
	Local nQtdOrig  := 0

	//Prepara a impressora e instancia a classe para utiliza��o da impressora
	If CB5SetImp(cCodImpr)
		//Percorre e imprime os registros
		For nIndex := 1 To nQtdRegs
			//Verifica posicionamento da SB1
			If SB1->B1_COD <> aProdImp[nIndex][ARR_APARAM_PRODUTO] .Or. SB1->B1_FILIAL <> xFilial("SB1")
				SB1->(dbSetOrder(1))
				SB1->(dbSeek(xFilial("SB1") + aProdImp[nIndex][ARR_APARAM_PRODUTO]))
			EndIf

			//Se a quantidade de produtos na etiqueta n�o foi enviada
			If Empty(aProdImp[nIndex][ARR_APARAM_COPIAS])
				//Verifica posicionamento da SVK
				If SVK->VK_COD <> aProdImp[nIndex][ARR_APARAM_PRODUTO] .Or. SVK->VK_FILIAL <> xFilial("SVK")
					SVK->(dbSetOrder(1))
					SVK->(dbSeek(xFilial("SVK") + aProdImp[nIndex][ARR_APARAM_PRODUTO]))
				EndIf

				nCopias  := aProdImp[nIndex][ARR_APARAM_QUANT] / SVK->VK_RFIDFAT
				nQtdOrig := aProdImp[nIndex][ARR_APARAM_QUANT]

				If nCopias >= 1
					//Se a quantidade a ser impressa � > ou = ao fator do produto, ir� imprimir a quantidade de etiquetas
					//de acordo com o fator de quantidade.
					aProdImp[nIndex][ARR_APARAM_COPIAS] := Int(nCopias)
					aProdImp[nIndex][ARR_APARAM_QUANT ] := SVK->VK_RFIDFAT
				Else
					//Se a quantidade a ser impressa � < que o fator do produto, ir� imprimir 1 etiqueta com a quantidade total.
					aProdImp[nIndex][ARR_APARAM_COPIAS] := 1
					nCopias := 0
				EndIf

				//Verifica se � necess�rio imprimir uma etiqueta extra com a quantidade que restou da divis�o com o fator de quantidade.
				If Int(nCopias) < nCopias
					aImpResto := aClone(aProdImp[nIndex])
					aImpResto[ARR_APARAM_COPIAS] := 1
					aImpResto[ARR_APARAM_QUANT ] := nQtdOrig % SVK->VK_RFIDFAT
				EndIf
			EndIf

			//Chama o RDMAKE que faz a impress�o da etiqueta
			ExecBlock(cRDMAKE, , , aProdImp[nIndex])

			//Executa o RDMAKE novamente, se necess�rio, para imprimir as quantidades restantes da etiqueta.
			If Len(aImpResto) > 0
				ExecBlock(cRDMAKE, , , aImpResto)
				aSize(aImpResto, 0)
			EndIf
		Next nIndex

		//Finaliza a impress�o das etiquetas
		MSCBCLOSEPRINTER()
	Else
		Help( ,  , "Help", ,  STR0023                                         ,; //"N�o foi poss�vel preparar a impressora."
			 1, 0, , , , , , {STR0024 + " '" + Trim(cCodImpr) + "' " + STR0015}) //"Verifique o cadastro da impressora '" XXX "' no programa Local de Impress�o (ACDA060)."
		lOk := .F.
	EndIf

Return lOk

/*/{Protheus.doc} VldImpress
Verifica se a impressora est� configurada e se est� tudo Ok para a impress�o
@type Static Function
@author marcelo.neumann
@since 10/07/2020
@version P12
@param 01 cCodImpr , Caracter, C�digo da impressora que ser� usada
@param 02 cRDMAKE  , Caracter, C�digo do fonte RDMAKE que far� a impress�o da etiqueta (passado por refer�ncia)
@param 03 cHelpErro, Caracter, Mensagem com o Help (Problema) ocorrido (passado por refer�ncia)
@param 04 cHelpSolu, Caracter, Mensagem com o Help (Solu��o) ocorrido (passado por refer�ncia)
@return   L�gico   , Indica se est� tudo certo para imprimir
/*/
Static Function VldImpress(cCodImpr, cRDMAKE, cHelpErro, cHelpSolu)

	Local cIndFab := ""

	//Verifica se a impressora est� cadastrada no ACDA060
	If cCodImpr <> CB5->CB5_CODIGO .Or. xFilial("CB5") <> CB5->CB5_FILIAL
		CB5->(dbSetOrder(1))
		If !CB5->(dbSeek(xFilial("CB5") + cCodImpr))
			cHelpErro := STR0005 + Trim(cCodImpr) + STR0006 //"Impressora '" xxx "' n�o cadastrada."
			cHelpSolu := STR0007                            //"Cadastre a impressora no programa Local de Impress�o (ACDA060)."
			Return .F.
		EndIf
	EndIf

	//Verifica qual a fabricante para direcionar para o RDMAKE correto
	If MSCbModelo("ZPL", CB5->CB5_MODELO)
		cIndFab := "Z" //Zebra

	ElseIf MSCbModelo("DPL", CB5->CB5_MODELO)
		cIndFab := "D" //Datamax

	ElseIf MSCbModelo("IPL", CB5->CB5_MODELO)
		cIndFab := "I" //Intermec

	ElseIf MSCbModelo("EPL", CB5->CB5_MODELO)
		//Fabricante Elton n�o suporta impress�o de etiquetas RFID
		cHelpErro := STR0008 + Trim(cCodImpr) + STR0009 + Trim(CB5->CB5_MODELO) + STR0010 //"A impressora '"XXX "' (Modelo '" YYY "') n�o suporta impress�o de etiquetas RFID."
		cHelpSolu := STR0011                                                              //"Informe uma impressora v�lida."
		Return .F.
	EndIf

	//Verifica se o modelo est� cadastrado
	If Empty(cIndFab)
		cHelpErro := STR0012 + Trim(CB5->CB5_MODELO) + STR0013 //"Modelo '" XXXX "' n�o encontrado."
		cHelpSolu := STR0014 + Trim(cCodImpr) + STR0015        //"Verifique o modelo cadastrado para a impressora '" XXX "' no programa Local de Impress�o (ACDA060)."
		Return .F.
	EndIf

	cRDMAKE += cIndFab

	//Caso o RDMAKE 'IMG11x' n�o esteja compilado no reposit�rio, dever� ser exibida mensagem de erro
	If !ExisRdmake(cRDMAKE)
		cHelpErro := STR0016 + cRDMAKE + STR0017        //"Fun��o de impress�o (" XXX ") n�o compilada no RPO."
		cHelpSolu := STR0018 + Lower(cIndFab) + STR0019 //"Compilar o fonte 'acdimg11" x ".prx' (RDMAKE) com as configura��es da impress�o."
		Return .F.
	EndIf

Return .T.

/*/{Protheus.doc} RFIDGetCon
Recupera o valor das constantes utilizadas para auxiliar na montagem do array de impress�o
@type Function
@author marcelo.neumann
@since 10/07/2020
@version P12.1.27
@param cInfo  , Caracter, Define qual constante se deseja recuperar o valor.
@return nValue, Num�rico, Valor da constante
/*/
Function RFIDGetCon(cInfo)
	Local nValue := ARR_APARAM_SIZE

	Do Case
		Case cInfo == "ARR_APARAM_PRODUTO"
			nValue := ARR_APARAM_PRODUTO
		Case cInfo == "ARR_APARAM_LOTE"
			nValue := ARR_APARAM_LOTE
		Case cInfo == "ARR_APARAM_SUBLOTE"
			nValue := ARR_APARAM_SUBLOTE
		Case cInfo == "ARR_APARAM_NUMSERIE"
			nValue := ARR_APARAM_NUMSERIE
		Case cInfo == "ARR_APARAM_QUANT"
			nValue := ARR_APARAM_QUANT
		Case cInfo == "ARR_APARAM_COPIAS"
			nValue := ARR_APARAM_COPIAS
		Case cInfo == "ARR_APARAM_ALIAS"
			nValue := ARR_APARAM_ALIAS
		Case cInfo == "ARR_APARAM_CHAVE"
			nValue := ARR_APARAM_CHAVE
		Case cInfo == "ARR_APARAM_SIZE"
			nValue := ARR_APARAM_SIZE
		Otherwise
			nValue := ARR_APARAM_SIZE
	EndCase

Return nValue

/*/{Protheus.doc} RFIDGetCnE
Recupera o valor das constantes utilizadas pela fun��o RFIDGetEpc
@type Function
@author marcelo.neumann
@since 05/08/2020
@version P12.1.27
@param cInfo  , Caracter, Define qual constante se deseja recuperar o valor.
@return nValue, Num�rico, Valor da constante
/*/
Function RFIDGetCnE(cInfo)
	Local nValue := ARR_DATAEPC_EXISTE

	Do Case
		Case cInfo == "ARR_DATAEPC_EXISTE"
			nValue := ARR_DATAEPC_EXISTE
		Case cInfo == "ARR_DATAEPC_QUANT"
			nValue := ARR_DATAEPC_QUANT
		Case cInfo == "ARR_DATAEPC_ATIVO"
			nValue := ARR_DATAEPC_ATIVO
		Case cInfo == "ARR_DATAEPC_PRODUTO"
			nValue := ARR_DATAEPC_PRODUTO
		Case cInfo == "ARR_DATAEPC_LOTE"
			nValue := ARR_DATAEPC_LOTE
		Case cInfo == "ARR_DATAEPC_SUBLOTE"
			nValue := ARR_DATAEPC_SUBLOTE
		Case cInfo == "ARR_DATAEPC_NUMSERIE"
			nValue := ARR_DATAEPC_NUMSERIE
		Case cInfo == "ARR_DATAEPC_SIZE"
			nValue := ARR_DATAEPC_SIZE
		Otherwise
			nValue := ARR_DATAEPC_SIZE
	EndCase

Return nValue

/*/{Protheus.doc} RFIDGrvRas
Grava a rastreabilidade da EPC
@type Function
@author marcelo.neumann
@since 16/07/2020
@version P12.1.27
@param 01 aEPC   , Array   , EPCs a gravar a rastreabilidade:
                             cEpc   , Caracter, C�digo EPC a ser gravado na tabela de rastreabilidade
                             nQuant , Numeric , Quantidade da EPC
@param 02 cAcao  , Caracter, A��o que foi realizada na EPC
@param 03 cRotina, Caracter, Rotina que fez a a��o na EPC, caso n�o informado, usa FunName()
@return lOk      , Logical , Indica se houve erro ao gravar os registros
/*/
Function RFIDGrvRas(aEPC, cAcao, cRotina)

	Local aInsert := {}
	Local cCols   := "HWW_FILIAL,HWW_EPC,HWW_DATA,HWW_HORA,HWW_USER,HWW_QTD,HWW_ROTINA,HWW_ACAO"
	Local cFilHWW := xFilial("HWW")
	Local cTime   := Time()
	Local dData   := Date()
	Local lOk     := .T.
	Local nIndex  := 1
	Local nTotal  := Len(aEPC)
	Default cRotina := FunName()

	If nTotal == 1
		RecLock("HWW", .T.)
			HWW->HWW_FILIAL := cFilHWW
			HWW->HWW_EPC    := aEPC[1][1]
			HWW->HWW_DATA   := dData
			HWW->HWW_HORA   := cTime
			HWW->HWW_USER   := cUserName
			HWW->HWW_QTD    := aEPC[1][2]
			HWW->HWW_ROTINA := cRotina
			HWW->HWW_ACAO   := cAcao
		HWW->(MsUnLock())

	ElseIf nTotal > 0
		For nIndex := 1 To nTotal
			aAdd(aInsert, {cFilHWW        ,;
						   aEPC[nIndex][1],;
						   dData          ,;
						   cTime          ,;
						   cUserName      ,;
						   aEPC[nIndex][2],;
						   cRotina        ,;
						   cAcao          })
		Next nIndex

		If TCDBInsert(RetSqlName("HWW"), cCols, aInsert) < 0
			lOk := .F.
		EndIf
	EndIf

Return lOk
