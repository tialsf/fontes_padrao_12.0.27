#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#include "tbiconn.ch"
#Define ENTER chr(13) + chr(10)

//--------------------------------
//Esqueminha para remover warning
Function __DCLA001()
Scheddef()
Return
//--------------------------------

//-------------------------------------------------------------------
/*/{Protheus.doc} DCLA001
Funco principal para JOB ANP45

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function DCLA001(aParam)
Local cTime	:= Time()

conout("Iniciado Job - ANP45" )
T_DCLA001(.T.)
conout("Finalizado Job - ANP45 - Tempo Total "+ElapTime(cTime, Time()) )

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} Scheddef
Funco Scheddef para configurar Job

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Static Function SchedDef()
Local aParam  := {}

aParam := { "P",;			//Tipo R para relatorio P para processo
            ,;	//Pergunte do relatorio, caso nao use passar ParamDef
            ,;			//Alias
            ,;			//Array de ordens
            }				//Titulo

Return aParam
//-------------------------------------------------------------------
/*/{Protheus.doc} CheckAnp45
Valida campos novos DCL

@author Alexandre Gimenez
@since 26/7/2017
@version P12
/*/
//-------------------------------------------------------------------
Function CheckAnp45(lJob)
Local cMens	:= ""
Local cTitle:= ""
Local cIndex:= ""
Local lRet := .F.

If (LBC->(FieldPos("LBC_DATAOR")) == 0) .OR. (LBC->(FieldPos("LBC_VOLTER")) == 0) .Or. (SD1->(FieldPos("D1_T_ANP45")) == 0)
	lRet := .T.
EndIf
If !lRet
	DbselectArea("LBC")
	DbSetOrder(1)
	cIndex:= IndexKey()
	If !("LBC_DATAOR" == SUBSTR(cIndex,(Len(cIndex)-10),10))
		lRet := .T.
	EndIf
EndIF
If !lRet
	dbSelectArea( "SIX" )
	SIX->( dbSetOrder( 1 ) )
	lRet := !(SIX->(dbSeek("LBB2")))//Verifica Indice 2 - LBB_FILIAL+LBB_CODREG+LBB_LOCMNT+LBB_CODPRO+LBB_MES+LBB_ANO
EndIf
If lRet
	cTitle := "Alteracao na Estrutura do DCL"
	cMens := "Atencao." + ENTER
	cMens += ""+ENTER
	cMens += "A estrtura de dados do Relatorio ANP45 sofreu alteracoes para atender a Legislacao." + ENTER
	cMens += "" + ENTER
	cMens += "A versao da rotina est� mais atualizada que a versao do Dicionario de dados, para se adequar eh necessario que verifique a documentacao disponivel da Issue DMANMAT01-786 no TDN.
	If lJob
		conout(cTitle)
		conout(cMens)
	Else
		MsgInfo(cMens,cTitle)
	EndIf
EndIf	

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DCLA001
Funco de menu para reprocessar ANP45

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Template Function DCLA001(lJob)
local dData	:= dDataBase	// Data de Referencia
Local lFilial	:= .F.			// .T. Processa somente filial Logada / .F. Todas as filiais
Local lSemana	:= .T.			// .T. Processa semana toda / .F. Somente data de referencia
Local cProd	:= ""			// Range produtos
Local aProd	:= {}
Local aSemana	:= {}
Local lProc	:= .T.
Local cMsg		:= ""
Local cWhereProd	:= ""
Local cAliasProd	:= GetNextAlias()
Local oProcess
Local dDataAux := dDatabase
Local nDiaSem  := 0

Default lJob		:= .F.

Private cTime := Time()

IF CheckAnp45(lJob) // verifica campos novos DCL
	Return
EndIf

If lJob	
	lProc := .T.
Else
	lProc :=  Pergunte("ANP45REP")
EndIf

If lProc	
	//---------------------------
	// Variaveis  Pergunte
	//---------------------------
	MakeSqlExp("ANP45REP")
	dData	:= IIF(lJob,dDataBase, MV_PAR01)
	cProd	:= IIF(lJob,""		, MV_PAR02)
	lSemana:= IIF(lJob,.F.		, MV_PAR03 == 1 )
	lFilial:= IIF(lJob,.F.		, MV_PAR04 == 2 )
	If !lJob
		//---------------------------
		// Valida��o de Datas
		//---------------------------
		nDiaSem := DOW (dData)
		If nDiaSem = 1
			nDiaSem := 8
		EndIf
		If nDiaSem > 4   
			dDataAux := dData - (nDiaSem-5)  
		else
			dDataAux := dData + (5-nDiaSem)
		EndIf
		
		aSemana	:= ANP45Data(dDataAUX) 
		If lSemana 
			If aSemana[3] >= dDataBase
				lProc := .F.
			EndIf
		Else
			If dData >= dDataBase
				lProc := .F.
			Else
				dDataAUX := dData
			EndIf
		EndIf
	EndIf
	If !lProc
		cMsg := "A data de processmento eh superior a data atual, nao eh possivel reprocessar"
	Else
		//---------------------------
		// Monta Array de Produtos
		//---------------------------
		cWhereProd := " SB1.B1_ANP45 = 'T' "
		If !Empty(cProd)
			cWhereProd += " AND " + cProd
		EndIf
		cWhereProd := "%"+cWhereProd+"%"
		
		BeginSql Alias cAliasProd
			SELECT B1_COD
				FROM	%Table:SB1% SB1 
					WHERE SB1.%NotDel% 
					AND SB1.B1_FILIAL = %xFilial:SB1%
					AND %Exp:cWhereProd%
		EndSql	
		
		While !(cAliasProd)->(EOF())	
			aAdd(aProd,(cAliasProd)->B1_COD)
			(cAliasProd)->(DbSkip())
		EndDo
		(cAliasProd)->(DbCloseArea())
		If Len(aProd) == 0
			cMsg := "Nao existe produtos configurados a ser processado. Verifique o cadastro de produto o campo ANP45"
			lProc := .F.
		Endif
	EndIf
	
	If lProc
		If lJob 
			DCLJob(aProd,dDataAux,lFilial)
		Else
			//---------------------------
			// Reprocessamento (Menu) 
			//---------------------------
			oProcess := MSNewProcess():New( { | lEnd | DCLReproc( @oProcess,@lEnd, aProd,dDataAux,lFilial,lSemana  ) }, "Reprocessando Relat�rio ANP45", "Reprocessando", .F. )
			oProcess:Activate()
		EndIf
	Else
		If lJob
			conout("DCLJob - "+ cMsg)
		Else
			Help(" ",1,"DCL001PROC",,cMsg,1,0)
		EndIf
	EndIf
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} DCLJob
Funco para Processar ANP45 por job

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function DCLJob(aProd,dData,lFilial)
Local lGrava	:= .T. // Job Sempre Grava
Local lReproc	:= .F. // Job
Local nProd	:= 0

For nProd := 1 to Len(aProd) 
	Conout("Job ANP45 - Iniciou produto: "+Alltrim(aProd[nProd]))
	//------------------------
	//Processa Armazenamento
	//------------------------
	DclGrvLbc(aProd[nProd],dData,lFilial,lGrava,lReproc)
	//------------------------
	// Processa Est. Transito
	//------------------------
	DclGrvLbc(aProd[nProd],dData,lFilial,lGrava,lReproc,"2")
	Conout("Job ANP45 - Finalizou produto: "+Alltrim(aProd[nProd]))
Next nProd

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} DCLReproc
Funco para reprocessar ANP45 (Separa produtos)

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function DCLReproc( oProcess,lEnd, aProd,dData,lFilial,lSemana )
Local lGrava	:= .T. // Reprocessamento Sempre Grava
Local lReproc	:= .T. // Reprocessamento
Local nProd	:= 0
Local nData	:= 0
Local nDiasSem:= IIF(lSemana,7,1)
Local aSemana	:= ANP45Data(dData)
Local dDataPro

Static lTransito := .F.

oProcess:SetRegua1(Len(aProd))

For nProd := 1 to Len(aProd)
	oProcess:IncRegua1("Processando produto: "+Alltrim(aProd[nProd]))
	oProcess:SetRegua2(nDiasSem*2)
	lTransito := .F. //Roda transito so 1 vez na semana(Performance)
	For nData := 1 to nDiasSem
		If lSemana
			dDataPro := aSemana[2]+nData-1
		Else
			dDataPro := dData
		EndIf
		//------------------------
		//Processa Armazenamento
		//------------------------
		oProcess:IncRegua2("Armazenamento Dia: "+DTOC(dDataPro)+" Semana: "+Alltrim(Str(aSemana[1])))
		DclGrvLbc(aProd[nProd],dDataPro,lFilial,lGrava,lReproc)
		//------------------------
		// Processa Est. Transito
		//------------------------
		oProcess:IncRegua2("Est. Transito Dia: "+DTOC(dDataPro)+" Semana: "+Alltrim(Str(aSemana[1])))
		DclGrvLbc(aProd[nProd],dDataPro,lFilial,lGrava,lReproc,"2")
	Next nData
Next nProd

Return



//-------------------------------------------------------------------
/*/{Protheus.doc} DclGrvLbc
Funco para gravar LBC, Detalhamento ANP45

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function DclGrvLbc(cProd,dDataReg,lFilial,lGrava,lReproc,cNature)
Local cCodReg 	:= StrZero(Val(SuperGetMV("MV_T_CSIMP",.F.,"")), 10) // pegar do simp
Local cInst1		:= StrZero(Val(SuperGetMV("MV_T_INST",.F.,"",cFilAnt)), 10) // Pegar do Simp Filial origem
Local cInst2		:= Replicate("0",10)
Local cInst2Branco	:= Replicate("0",10)
Local aInst2		:= {}
Local cFilOri		:= cFilAnt
Local cFilProc	:= cFilAnt
Local cWhereArm	:= ""
Local cWhereMnt	:= ""
Local cAliasArm	:= GetNextAlias()
Local cAliasSld	:= GetNextAlias()
Local cAliasLBC	:= GetNextAlias()	
Local cNNRMode	:= FWModeAccess("NNR")
Local aLocManut	:= {}
Local aArms		:= {}
Local aFilMnt		:= {}
Local nSaldo		:= 0
Local nX			:= 0
Local nY			:= 0 
Local nZ			:= 0
Local nI			:= 0	
Local nLastro		:= 0
Local nPosTran		:= 0
Local nPosTer		:= 0
Local cSeek		:= ""
Local lSeek		:= .F.
Local lDeleted	:= .F.
Local aSemana	:= ANP45Data(dDataReg)
Local nSaldoTer	:= 0
Local lSaldoTer	:= SuperGetMV("MV_ANP45TE",.F.,.F.)
Local lUsaPETN3	:= SuperGetMV("MV_SDTESN3",.F.,0) == 0
Local cArmTerDe	:= Replicate(" ",TamSx3("NNR_CODIGO")[2])
Local cArmTerAte	:= Replicate("Z",TamSx3("NNR_CODIGO")[2])
Local cDataIni		:= ""
Local cDataFin		:= ""
Local cSeekLBC		:= ""

Default dDataReg	:= dDataBase
Default lFilial	:= .T.
Default lGrava	:= .T.
Default lReproc	:= .F.
Default cNature	:= "1" 	// 1-Armazenamento - 2 Transito

//-----------------------------------------------
//Identifica Locais de manutencao
//-----------------------------------------------
If lFilial //Somente filial corrente
	aLocManut := {GetLocMnt(cFilOri)[1,2]}
Else
	aLocManut:= GetAllMnt()
EndIf
//--------------------------------------------------

For nX := 1 to Len(aLocManut)

	If lGrava  

		//-------------------
		//Zera acumuladores
		//-------------------
		//aInst2 	:= {{cInst2,VolTra,dDataReg,VolTer,DataOrigem}}
		aInst2 	:= {{cInst2,0,DtoS(dDataReg),0,""}}
		aFilMnt	:= GetFilMnt(aLocManut[nX])
		//-------------------
	
		//-----------------------------
		//Processa Filiais
		//-----------------------------
		For nZ := 1 to Len(aFilMnt)
			cFilProc := aFilMnt[nZ,1]
			MTChangeF(cFilProc)// Troca a filial do sistema
			
			//-------------------
			//Zera acumuladores
			//-------------------
			nSaldo 	:= 0
			nSaldoTer	:= 0
			nLastro	:= 0
			cWhereArm	:= "" 
			cInst1		:= StrZero(Val(SuperGetMV("MV_T_INST",.F.,"",cFilProc)), 10) // Pegar do Simp Filial origem
			//-------------------
			
			If cNature == "1"
				If lReproc 
					//------------
					//Monta Arms
					//-------------
					BeginSql Alias cAliasArm
					SELECT  NNR_FILIAL,NNR_CODIGO
						FROM	%Table:NNR% NNR
						WHERE NNR.NNR_FILIAL = %xFilial:NNR%
						AND NNR.%NotDel%
						AND NNR.NNR_ANP45 = 'T' 
					EndSql
					
					While !(cAliasArm)->(EOF()) 
						nSaldo += CalcEst(cProd,(cAliasArm)->NNR_CODIGO,dDataReg+1)[1]
						(cAliasArm)->(DbSkip())
					EndDo
					(cAliasArm)->(DBCloseArea())
				Else
					nSaldo := DclSaldoANP(cProd,cFilProc)
				EndIf

				//--------------------
				//Atualiza Lastro
				//--------------------	
				nLastro	:= GetLastro(cProd,aFilMnt,cFilProc)
				
				//---------------------------
				//Calcula poduto em terceiros
				//---------------------------
				If lSaldoTer
					aInst2 := MtANPTer(cProd,dDataReg)
				EndIf
	
			EndIf
	
			If cNature == "2" /*.And. !lTransito*/
				aInst2 := GetTransito(cProd,dDataReg,aFilMnt)
			EndIf

			//---------------------------------------
			// Grava por local de instala��o destino
			//---------------------------------------
			For nI := 1 To Len(aInst2)
				If cNature == "2" //tratamento para em transito sempre inst 1 = origem, inst 2 = destino
					cInst1 		:= aInst2 [nI,1]
					aInst2[nI,1]:= aInst2 [nI,6]
				EndIf		
				DbSelectArea("LBC") 
				DbsetOrder(1) //LBC_FILIAL+LBC_FILORI+LBC_NATURE+LBC_CODREG+LBC_INST1+LBC_INST2+LBC_LOCMNT+LBC_CODPRO+LBC_DATA+LBC_DATAOR
				If lSaldoTer .And. aInst2[nI,4] != 0
					//Quando for estoque em terceiros, o codigo do terceiro tem que ficar no inst1 e o inst2 com zero
					cSeek := xFilial("LBC")+cFilProc+cNature+cCodReg+aInst2[nI,1]+cInst2Branco+aLocManut[nX]+cProd+aInst2[nI,3]+aInst2[nI,5]
				Else
					cSeek := xFilial("LBC")+cFilProc+cNature+cCodReg+cInst1+aInst2[nI,1]+aLocManut[nX]+cProd+aInst2[nI,3]+aInst2[nI,5]
				EndIf 
				lSeek := LBC->(MsSeek(cSeek))	
			
				If lSeek
					Reclock("LBC",.F.)
						LBC_LASTRO	:= nLastro
						LBC_VOLTRA	:= aInst2[nI,2]
						If lSaldoTer 
							If aInst2[nI,4] == 0 // Tem saldo em terceiro
								LBC_VOLLOC	:= nSaldo	
							Else
								LBC_VOLLOC	:= 0
							EndIf	
							LBC_VOLTER	:= aInst2[nI,4]
						Else
							LBC_VOLLOC	:= nSaldo
						EndIf					
					MsUnlock()		
				Else
					RecLock("LBC",.T.)
						LBC_FILIAL	:= xFilial("LBC") //compartilhado
						LBC_FILORI	:=	cFilProc
						LBC_NATURE	:= cNature
						LBC_CODREG	:= cCodReg
						If lSaldoTer .And. aInst2[nI,4] != 0 
							//Quando for estoque em terceiros, o codigo do terceiro tem que ficar no inst1 e o inst2 com zero
							LBC_INST1	:= aInst2[nI,1]
							LBC_INST2	:= cInst2Branco						
						Else
							LBC_INST1	:= cInst1
							LBC_INST2	:= aInst2[nI,1]
						EndIf
						LBC_LOCMNT	:= aLocManut[nX]
						LBC_CODPRO	:= cProd
						LBC_SEMANA	:= ANP45Data(dDataReg)[1]
						LBC_ANO	:= ANP45MesAno(dDataReg,"A")
						LBC_DATA	:= StoD(aInst2[nI,3])
						LBC_LASTRO	:= nLastro
						LBC_VOLTRA	:= aInst2[nI,2]
						LBC_DATAOR	:= StoD(aInst2[nI,5])
						If lSaldoTer 
							If aInst2[nI,4] == 0 // Tem saldo em terceiro
								LBC_VOLLOC	:= nSaldo	
							Else
								LBC_VOLLOC	:= 0
							EndIf	
							LBC_VOLTER	:= aInst2[nI,4]
						Else
							LBC_VOLLOC	:= nSaldo
						EndIf		
					LBC->(MsUnlock())
				EndIf
				//----------------
				// Atu Sintetico
				//----------------
				DclGrvLbb(dDataReg)
			Next nI
			//-----------------------
			//Valida terceiro que 
			//deixou de ser.
			//-----------------------
			cDataIni :=  DtoS(dDataReg)
			If lSaldoTer
				BeginSql Alias cAliasLBC
					Select LBC_INST1,R_E_C_N_O_ RecLBC
						from %Table:LBC% LBC
					WHERE LBC.LBC_FILIAL = %xFilial:LBC%
						AND LBC.LBC_FILORI = %Exp:cFilProc%
						AND LBC.LBC_NATURE = %Exp:cNature%
						AND LBC.LBC_CODREG = %Exp:cCodReg%
						AND LBC.LBC_LOCMNT = %Exp:aLocManut[nX]%
						AND LBC.LBC_CODPRO = %Exp:cProd%
						AND LBC.LBC_VOLTER > 0
						AND LBC.LBC_DATA = %Exp:cDataIni%
						AND LBC.%NotDel%
				EndSql
				
				While !((cAliasLBC)->(Eof()))
					nPosTer :=  aScan(aInst2, {|X| X[1] == (cAliasLBC)->LBC_INST1}) //Valida se item da query existe no Array
					If nPoster == 0 // N�o tem no array, ent�o n�o � mais terceiro.
						LBC->(DbGoto((cAliasLBC)->RecLBC))
						cSeekLBC	:= LBC->(LBC_NATURE+LBC_CODREG+LBC_INST1+LBC_INST2+LBC_LOCMNT+LBC_CODPRO)
						Reclock("LBC",.F.)
						dbDelete()
						MsUnlock()
						// Atu Sintetico
						DclGrvLbb(dDataReg,cSeekLBC)
					EndIf				
					(cAliasLBC)->(DbSkip())	
				EndDo
				(cAliasLBC)->(DbCloseArea())
			EndIf
		
			//----------------------------
			// Busca Transito gravado, 
			// que n�o  � mais transito 
			//-----------------------------
			If cNature == "2" 
				
				cDataIni := DtoS(aSemana[2]) // Primeiro dia
				cDataFin := DtoS(dDataReg) // dia do processamento
				
				BeginSql Alias cAliasLBC
					SELECT  R_E_C_N_O_ as RecLBC
							FROM 	%Table:LBC% LBC
								WHERE LBC.LBC_FILIAL = %xFilial:LBC%
									AND LBC.LBC_FILORI = %Exp:cFilProc%
									AND LBC.LBC_NATURE = %Exp:cNature%
									AND LBC.LBC_CODREG = %Exp:cCodReg%
									AND LBC.LBC_INST1  = %Exp:cInst1%
									AND LBC.LBC_LOCMNT = %Exp:aLocManut[nX]%
									AND LBC.LBC_CODPRO = %Exp:cProd%
									AND LBC.%NotDel%
									//AND LBC.LBC_DATA >= %Exp:cDataIni%
									AND LBC.LBC_DATA = %Exp:cDataFin%
								ORDER BY LBC.LBC_DATAOR,LBC.LBC_DATA
				EndSql
				While !((cAliasLBC)->(Eof()))
					LBC->(DbGoto((cAliasLBC)->RecLBC))
					nPosTran :=  aScan(aInst2, {|X| X[1] == LBC->LBC_INST2 .And.  X[5] == Dtos(LBC->LBC_DATAOR)  })
					If nPosTran == 0  //Registro n�o encontrado, N�o tem transito	
						cSeekLBC	:= LBC->(LBC_NATURE+LBC_CODREG+LBC_INST1+LBC_INST2+LBC_LOCMNT+LBC_CODPRO)
						Reclock("LBC",.F.)
						dbDelete()
						MsUnlock()						
						// Atu Sintetico
						DclGrvLbb(dDataReg,cSeekLBC)
					Else
						If LBC->LBC_VOLTRA != aInst2[nPosTran,2]
							Reclock("LBC",.F.)
							LBC_VOLTRA	:= aInst2[nPosTran,2]
							MsUnlock()						
							// Atu Sintetico
							DclGrvLbb(dDataReg)							
						EndIf
					EndIf
					(cAliasLBC)->(DbSkip())					
				EndDo
				
				(cAliasLBC)->(DbCloseArea())
			EndIf
			MTChangeF(cFilOri) // Volta Filial do Sistema	
		Next Nz		
	EndIf
Next nX


Return nSaldo

//-------------------------------------------------------------------
/*/{Protheus.doc} DclGrvLbb
Funco para gravar/atualizar LBB, Aglutinado ANP45

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Static Function DclGrvLbb(dDataReg,cSeekLBC)
Local lSeek		:= .F.
Local nESMD		:= 0
Local aDados		:= Array(7)
Local lSaldoTer	:= SuperGetMV("MV_ANP45TE",.F.,.F.)
Local nEsmObj	:= 0
Local cMes		:= ANP45MesAno(dDataReg,"M") 
Local cAno		:= ANP45MesAno(dDataReg,"A")
Local cWhere	:= ""
Local cQuery	:= ""
Local cAliasLBB	:= GetNextAlias()
Local cSeekLBC2	:= ""

Default cSeekLBC	:= LBC->(LBC_NATURE+LBC_CODREG+LBC_INST1+LBC_INST2+LBC_LOCMNT+LBC_CODPRO)

cSeekLBC2 := STR(LBC->LBC_SEMANA,TamSx3("LBC_SEMANA")[1])

cWhere	:= " LBB_NATURE||LBB_CODREG||LBB_INST1||LBB_INST2||LBB_LOCMNT||LBB_CODPRO||LBB_MES||LBB_ANO = "
cWhere += "'" + cSeekLbc + cMes + cAno + " '"

cWhere += " AND LBB_SEMANA = " + cSeekLbc2

cQuery := "SELECT R_E_C_N_O_ RECNO FROM " + RetSqlName("LBB") + " WHERE " + cWhere + " AND D_E_L_E_T_ = ' ' "

cQuery := ChangeQuery(cQuery)
DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),cAliasLBB,.T.,.T.)

lSeek := !(Empty((cAliasLBB)->RECNO))
			
If !lSeek
	LBB->(DbSetOrder(2))//LBB_FILIAL+LBB_CODREG+LBB_LOCMNT+LBB_CODPRO+LBB_MES+LBB_ANO
	If LBB->(DbSeek(xFilial("LBB")+LBC->(LBC_CODREG+LBC_LOCMNT+LBC_CODPRO)+cMes+cAno))
		nEsmObj := LBB->LBB_ESDMOB
	Else
		nEsmObj := GetESDOB(LBC->LBC_CODPRO,LBC->LBC_LOCMNT,dDataReg)
	EndIf

	//----------------------
	// Cadastra novo item
	//----------------------
	RecLock("LBB",.T.)
		LBB->LBB_FILIAL	:= xFilial("LBB") //compartilhado
		LBB->LBB_NATURE	:= LBC->LBC_NATURE
		LBB->LBB_CODREG	:= LBC->LBC_CODREG
		LBB->LBB_INST1	:= LBC->LBC_INST1
		LBB->LBB_INST2	:= LBC->LBC_INST2
		LBB->LBB_LOCMNT	:= LBC->LBC_LOCMNT
		LBB->LBB_CODPRO	:= LBC->LBC_CODPRO
		LBB->LBB_SEMANA	:= LBC->LBC_SEMANA
		LBB->LBB_MES	:= cMes
		LBB->LBB_ANO	:= cAno
		If lSaldoTer
			LBB->LBB_ESMD	:= (LBC->LBC_VOLLOC +LBC->LBC_VOLTER+ LBC->LBC_VOLTRA) / 7
		Else
			LBB->LBB_ESMD	:= (LBC->LBC_VOLLOC + LBC->LBC_VOLTRA) / 7	
		EndIf	
		LBB->LBB_ESDMOB	:= nEsmObj
		LBB->LBB_OBS	:= GetObs(GetFilMnt(LBC->LBC_LOCMNT))
	LBB->(MsUnlock())
Else

	LBB->(DbGoTo((cAliasLBB)->RECNO))

	//----------------------
	// Atualiza ESMD
	//----------------------
	aDados[1] := LBC->LBC_NATURE
	aDados[2] := LBC->LBC_CODREG
	aDados[3] := LBC->LBC_INST1
	aDados[4] := LBC->LBC_INST2
	aDados[5] := LBC->LBC_LOCMNT
	aDados[6] := LBC->LBC_CODPRO
	aDados[7] := LBC->LBC_SEMANA
	nESMD := GetESMD(aDados)

	RecLock("LBB",.F.)
		LBB_ESMD := nESMD
	LBB->(MsUnlock())
	
	
EndIf
(cAliasLBB)->(dbCloseArea())
Return


//-------------------------------------------------------------------
/*/{Protheus.doc} DclSaldoANP(aFil)
Funco para recuperar o saldo atual SB2 
somando as filiais 

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function DclSaldoANP(cProd,cFil)
Local nRet			:= 0
Local nZ			:= 0
local cLocMnt		:= GetLocMnt(cFilAnt)[1,2]
Local aFilMnt 	:= GetFilMnt(cLocMnt)
Local cWhereArm	:= ""
Local cAliasSld	:= GetNextAlias()

Default cFil := ''

If Empty(cFil)
	//----------------------
	//Where Filiais LocMnt
	//----------------------		
	For nZ := 1 to Len(aFilMnt)
		If nZ != 1
			cWhereArm += " OR "
		EndIf
			cWhereArm += " SB2.B2_FILIAL = '"+ aFilMnt[nZ,1] +"'"
	Next nz
Else
	cWhereArm := " SB2.B2_FILIAL = '" + cFil +"'"
EndIf 
cWhereArm := '% ('+ cWhereArm + ' )%'

//----------------------
//Busca Saldo Atual 
//----------------------		
BeginSql Alias cAliasSld
SELECT SUM(B2_QATU) SOMASALDO
	FROM %Table:SB2% SB2
	JOIN %Table:NNR% NNR ON NNR.%NotDel% 
		AND NNR.NNR_FILIAL = SUBSTRING(SB2.B2_FILIAL,1,LEN(NNR.NNR_FILIAL)) 
		AND NNR.NNR_CODIGO = SB2.B2_LOCAL 
		AND NNR.NNR_ANP45 = 'T'
WHERE SB2.%NotDel%  
	AND SB2.B2_COD = %Exp:cProd%
	AND %Exp:cWhereArm%
EndSql	 
//----------------------
//Atualiza Saldo Atual
//----------------------			
nRet := (cAliasSld)->SOMASALDO
(cAliasSld)->(dbCloseArea())	

Return nRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GetLastro
Funco para recuperar o lastro dos tanques de um produto

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Static Function GetLastro(cProd,aFiliais,cFilProc)
Local nRet :=0
Local cAliasLastro := GetNextAlias()
Local nx := 0
Local cWhere := ""

Default cFilProc	:= ""

If Empty(cFilProc) 	
	For nX := 1 To Len(aFiliais)
		If Empty(cWhere)
			cWhere := " ( LB4_FILIAL = '" +  aFiliais[nX,1] + "'"
		Else
			cWhere += " Or LB4_FILIAL = '" +  aFiliais[nX,1] + "'"
		EndIf
	Next nX
		cWhere := "%" + cWhere + ")%"
Else
	cWhere := "%"+" LB4_FILIAL = '"+cFilProc+"'"+"%"
EndIf
//Obs tabela LB4 sempre exclusiva

	BeginSql Alias cAliasLastro
		SELECT Distinct  LB4_FILIAL,LB4_TANQUE,LB4_COD,LB4_LASTRO
		FROM	%Table:LB4% LB4
		JOIN %Table:SB1% SB1 
			ON SB1.B1_COD = LB4.LB4_COD 
			AND SB1.B1_FILIAL = LB4.LB4_FILIAL 
			AND SB1.B1_ANP45 = 'T'
			AND SB1.%NotDel%
		WHERE LB4.%NotDel% 
		AND %Exp:cWhere%
	EndSql
	
	While !(cAliasLastro)->(EOF())
		nRet += (cAliasLastro)->LB4_LASTRO
		(cAliasLastro)->(dbSkip())
	EndDo
(cAliasLastro)->(dbCloseArea())
Return nRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GetObs
Funco para criar obs com filiais do sistema no local de manutencao

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Static Function GetObs(aFiliais)
Local nX	:= 0
Local cRet	:= " Filiais "
Local cFil	:= ""

For nX := 1 To Len(aFiliais)
	If cFil != aFiliais[nX,1]
		cRet += " - " + aFiliais[nX,1]
	EndIf	
Next nX


Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GetESMD
Calcula a media de estoque mensal conforme outros registros

aDados[1] - LBC->LBC_NATURE
aDados[2] - LBC->LBC_CODREG
aDados[3] - LBC->LBC_INST1
aDados[4] - LBC->LBC_INST2
aDados[5] - LBC->LBC_LOCMNT
aDados[6] - LBC->LBC_CODPRO
aDados[7] - LBC->LBC_SEMANA

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function GetESMD(aDados)
Local cAliasMed := GetNextAlias()
Local nRet			:= 0
Local nSoma		:= 0
Local nCount		:= 0
Local cCampos		:= ""
Local lSaldoTer	:= SuperGetMV("MV_ANP45TE",.F.,.F.)

If lSaldoTer
	cCampos := "%LBC_VOLLOC + LBC_VOLTRA + LBC_VOLTER%"
Else
	cCampos := "%LBC_VOLLOC + LBC_VOLTRA%"
EndIf

//----------------------
// Atualiza saldo
//----------------------
BeginSql Alias cAliasMed
SELECT  SUM(%Exp:cCampos%) ESMD_MEDIA, LBC_DATA
	FROM	%Table:LBC% LBC
WHERE LBC.%NotDel%
	AND LBC_NATURE	= %Exp:aDados[1]%
	AND LBC_CODREG	= %Exp:aDados[2]%
	AND LBC_INST1		= %Exp:aDados[3]%
	AND LBC_INST2		= %Exp:aDados[4]%
	AND LBC_LOCMNT	= %Exp:aDados[5]%
	AND LBC_CODPRO	= %Exp:aDados[6]%
	AND LBC_SEMANA	= %Exp:aDados[7]%
	AND LBC_ANO 	= %Exp:Alltrim(str(year(MV_PAR01)))%
GROUP BY LBC_DATA
EndSql


While !(cAliasMed)->(EOF())
	nSoma += (cAliasMed)->ESMD_MEDIA
	nCount++
	(cAliasMed)->(DbSkip())
EndDo
(cAliasMed)->(dbCloseArea())


nRet := nSoma/ 7 //Sempre dividir por 7 //nCount

Return nRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DclGetWhere()
Calcula a media de estoque mensal conforme outros registros


@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function DclGetWhere()
Local cRet			:= ""
Local dDataRef	:= IIF(Empty(MV_PAR01),dDataBase,MV_PAR01)
Local aCodPro   := {}
Local nI        := 0

//MV_PAR01 - DATA DE REFERENCIA
//MV_PAR03 - PERIODO
If MV_PAR03 == 1 //Semana
	cRet := "LBB_SEMANA = "+ AllTrim(Str(ANP45Data(dDataRef)[1]))
ElseIf MV_PAR03 == 2 //Mes 
	cRet := "LBB_MES = '"+ ANP45MesAno(dDataRef,"M") +"'"  
EndIf

// Sempre filtra pelo ano.
If !Empty(cRet)
	cRet += " AND "
EndIf
cRet += " LBB_ANO = '" + ANP45MesAno(dDataRef,"A") +"' "

If !Empty(MV_PAR02)
    aCodPro := StrTokArr2(AllTrim(MV_PAR02), ";")
    cRet += "AND LBB_CODPRO IN (" 
    For nI := 1 To Len(aCodPro)
        If Left(AllTrim(aCodPro[nI]), 1) <> "'"
            cRet += "'" + aCodPro[nI] + "',"
        Else
            cRet += aCodPro[nI] + ","
        EndIf
    Next 
    cRet := Left(cRet, Len(cRet)-1)
    cRet += ") "
EndIf

If !Empty(MV_PAR04)
    cRet += "AND LBB_LOCMNT IN ('" + MV_PAR04 + "') "
EndIf

cRet := "%"+ cRet + "%"
Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} ANP45Sema
Funco para retornar array com semana ANP45 Com base na data

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function ANP45Sema(nSemana,dData)
Local aRet := Array(3)
Local dDataIni

Default dData := dDatabase()

dDataIni := ANP45IniAno(dData)

aRet[1]	:= nSemana // Semana
aRet[2]	:= dDataIni
aRet[3] := aRet[2] + 6 // Data Final da Semana

Return aRet


//-------------------------------------------------------------------
/*/{Protheus.doc} ANP45Data
Funco para retornar array com semana ANP45 Com base na data

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function ANP45Data(dData)
Local dDataIni := ANP45IniAno(dData)
Local nSemana := VAL(RetSem(dData))
Local aRet := ANP45Sema(nSemana,dData)

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} ANP45MesAno
Funco para retornar o mes com base na semana ANP45 Com base na data

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function ANP45MesAno(dData,cTipo)
Local cRet		:= ""
Local aSemana	:= {}
Local nDia		:= 0
Local dDataVld
Local nDiaSem
Local dDataAux

Default dData := dDataBase
Default cTipo	:= "M"

aSemana := ANP45Data(dData)
nDia := Day(aSemana[3])
If nDia > 4
	dDataVld := aSemana[3]// Considera como data o Ultimo dia por ter ao menos 4 dias na semana
Else
	dDataVld := aSemana[2]// Considera como data o primeiro dia por ter mais de 4 dias
EndIf

If cTipo $ "Mm" // M�s
	cRet := SUBSTR(CMONTH(dDataVld), 1, 3) 	
ElseIf cTipo $ "Aa" //Ano
	nDiaSem := DOW (dData)
	If nDiaSem = 1
		nDiaSem := 8
	EndIf
	If nDiaSem > 4   
		dDataAux := dData - (nDiaSem-5)  
	else
		dDataAux := dData + (5-nDiaSem)
	EndIf
	cRet := Str(Year(dDataAux),4)	
ElseIf cTipo == "MES" //Ano
	cRet := AllTrim(Str(MONTH(dDataVld)))	
EndIf

Return cRet



//-------------------------------------------------------------------
/*/{Protheus.doc} ANP45IniAno
Funco para o primeiro dia do ano. 

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function ANP45IniAno(dData)
Local nYear 	:= Year(dData)
Local dData1	:= CTOD("01/01/"+Str(nYear))
Local nDiaSem	:= DOW(dData1)
Local lDomingo:= .F. // - apurado de 2�-feira a domingo de cada semana do m�s corrente do ano atual
Local lAfter	:= .F. // -M�s corrente da semana: m�s que abrange, no m�nimo, 4 (quatro) dias da semana.
Local nFator	:= 0
Local dRet		:= dData1
Local dDataAux  := dData

nDiaSem := DOW (dData) 
If nDiaSem = 1 
	nDiaSem := 8
EndIF
If nDiaSem > 4  
	dDataAux := dData - (nDiaSem-5)  
else
	dDataAux := dData + (5-nDiaSem)
EndIf
nYear 	:= Year(dDataAux)
dData1	:= CTOD("01/01/"+Str(nYear))		

If lDomingo
	nFator := (nDiaSem - 1)
Else
	nFator := (nDiaSem - 2)
	If nFator < 0
		nFator := 6 //para quando Semana Comeca segunda e dia 1 � Domingo
	EndIf
EndIf
dRet := dData - nFator //Primeiro dia da semana.

//4 sexta,5 sabado ,6 domingo - Menos de 3 dias na semana, vale a proxima. 
If nFator > 3 
	lAfter := .T.
EndIf

If lAfter
	dRet := dRet + 7 // uma semana depois
EndIf

Return dRet	


//-------------------------------------------------------------------
/*/{Protheus.doc} GetTransito(cProd,dDataRef,aFilMnt)
Funcao para recuperar o estoque em transito das filiais

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function GetTransito(cProd,dDataRef,aFilMnt)
//Local cInst2 		:= StrZero(Val(SuperGetMV("MV_T_INST",.F.,"",cFilAnt)), 10) 
Local cAliasTran	:= GetNextAlias()
Local nX			:= 0
Local cWhere		:= ""
Local cWhereEst		:= ""
Local cWhereEstA1	:= ""
Local aRet			:= {}
Local aDatas		:= ANP45Data(dDataRef)
Local cDataRef		:= DtoS(dDataRef)
Local cFilCGC		:= MTDataFil(cFilAnt,"M0_CGC")
Local aEstados		:= StrToArray(GetEstados(cFilAnt),"|")

aDatas[2]:= DtoS(aDatas[2])
aDatas[3]:= DtoS(aDatas[3])

For nX := 1 To Len(aFilMnt)
	If Empty(cWhere)
		cWhere := " ( SD2.D2_FILIAL = '" +  aFilMnt[nX,1] + "'"
	Else
		cWhere += " OR SD2.D2_FILIAL = '" +  aFilMnt[nX,1] + "'"
	EndIf
Next nX
cWhere := "%" + cWhere + ")%"

For nX := 1 To Len(aEstados)
	If Empty(cWhereEst)
		cWhereEst := " ( SA2.A2_EST = '" +  aEstados[nX] + "'"
		cWhereEstA1:= " ( SA1.A1_EST = '" +  aEstados[nX] + "'"
	Else
		cWhereEst += " OR SA2.A2_EST = '" +  aEstados[nX] + "'"
		cWhereEstA1+= " OR SA1.A1_EST = '" +  aEstados[nX] + "'"
	EndIf
Next nX
cWhereEst := "%" + cWhereEst + ")%"
cWhereEstA1 := "%" + cWhereEstA1 + ")%"

BeginSql Alias cAliasTran

Select TAB.FILIAL, TAB.DATAORI, TAB.CODIGO,TAB.LOJA,TAB.INST,SUM(TAB.TRANSITO) as TRANSITO FROM (

	//-------------------------
	//Atualmente em transito 
	//Nota de fornecedor 
	//-------------------------
	SELECT SD1.D1_FILIAL FILIAL,
			SD1.D1_EMISSAO DATAORI,
			SA2.A2_COD CODIGO,
       	SA2.A2_LOJA LOJA,
       	A2_T_INST INST,
       	Sum(D1_QUANT) TRANSITO
	FROM %Table:SD1% SD1
		JOIN %Table:SA2% SA2 ON SA2.A2_COD = SD1.D1_FORNECE
			AND SA2.A2_LOJA = SD1.D1_LOJA
			AND SA2.%NotDel%
			AND SA2.A2_FILIAL = %xFilial:SA2%
			AND %Exp:cWhereEst% //trata mesmo local de manut
	WHERE SD1.D1_COD = %Exp:cProd%
  		AND SD1.D1_TRANSIT = 'S'
  		//AND SD1.D1_EMISSAO = %Exp:cDataRef%
  		//AND SD1.D1_EMISSAO >= %Exp:aDatas[2]%
  		AND SD1.D1_EMISSAO <= %Exp:cDataRef% //%Exp:aDatas[3]%
  		AND SD1.D1_ORIGLAN <> 'LF'
  		AND SD1.%NotDel%
  		AND SD1.D1_FILIAL = %xFilial:SD1%
	GROUP BY SD1.D1_FILIAL,
				SD1.D1_EMISSAO,
				SA2.A2_COD,
    	    	SA2.A2_LOJA,
         		A2_T_INST
         		
UNION
	//----------------------------
	//Esteve em transito 
	//Nota de fornecedor
	//----------------------------
	SELECT SD1.D1_FILIAL FILIAL,
			SD1.D1_EMISSAO DATAORI,
		 	SA2.A2_COD CODIGO,
       	SA2.A2_LOJA LOJA,
       	A2_T_INST INST,
       	SUM(D1_QUANT) TRANSITO
	FROM %Table:SD1% SD1
		JOIN %Table:SF4% SF4 ON SF4.F4_CODIGO = SD1.D1_TES
			AND SF4.F4_TRANSIT = 'S'
			AND SF4.%NotDel%
			AND SF4.F4_FILIAL = %xFilial:SF4%
		JOIN %Table:SA2% SA2 ON SA2.A2_COD = SD1.D1_FORNECE
			AND SA2.A2_LOJA = SD1.D1_LOJA
			AND SA2.%NotDel%
			AND SA2.A2_FILIAL = %xFilial:SA2%
			AND %Exp:cWhereEst% //trata mesmo local de manut
		JOIN %Table:SD3% SD3 ON SD3.D3_DOC = SD1.D1_DOC
			AND SD3.D3_COD = SD1.D1_COD
			AND SD3.D3_QUANT = SD1.D1_QUANT
			AND SD3.D3_LOCAL = SD1.D1_LOCAL
			AND SD3.D3_CF = 'DE6'
			AND SD3.D3_EMISSAO > %Exp:cDataRef% //Considera se devolveu at� data atual
			AND SD3.%NotDel%
			AND SD3.D3_FILIAL = %xFilial:SD3%
	WHERE SD1.D1_COD = %Exp:cProd%
		//AND SD1.D1_EMISSAO = %Exp:cDataRef%
  		//AND SD1.D1_EMISSAO >= %Exp:aDatas[2]%
  		AND SD1.D1_EMISSAO <= %Exp:cDataRef% //%Exp:aDatas[3]%
  		AND SD1.D1_ORIGLAN <> 'LF'
  		AND SD1.%NotDel%
  		AND SD1.D1_FILIAL = %xFilial:SD1%
	GROUP BY SD1.D1_FILIAL,
			SD1.D1_EMISSAO,
	 		SA2.A2_COD,
         	SA2.A2_LOJA,
         	A2_T_INST

UNION
	//------------------------- 
	//Nota de Transferencia 
	//Sem PreNota, com PreNota e Classificado apos o periodo
	//-------------------------
	SELECT SD2.D2_FILIAL FILIAL,
			SD2.D2_EMISSAO DATAORI,
			"" CODIGO,
       	"" LOJA,
       	"" INST,
       	SUM(D2_QUANT) TRANSITO
		FROM %Table:SD2% SD2
			JOIN %Table:SA1% SA1 ON SA1.A1_COD = SD2.D2_CLIENTE
				AND SA1.A1_LOJA = SD2.D2_LOJA
				AND SA1.A1_FILIAL = SUBSTRING(SD2.D2_FILIAL,1,LEN(SA1.A1_FILIAL)) 
				AND SA1.%NotDel%
				AND (SA1.A1_FILTRF = %Exp:cFilAnt%
				     OR SA1.A1_CGC = %Exp:cFilCGC%)
				AND %Exp:cWhereEstA1% //trata mesmo local de manut
			JOIN %Table:SF4% SF4 ON SF4.F4_CODIGO = SD2.D2_TES
				AND SF4.F4_TRANFIL = '1'
				AND SF4.F4_FILIAL = SUBSTRING(SD2.D2_FILIAL,1,LEN(SF4.F4_FILIAL))
				AND SF4.%NotDel%		
			LEFT JOIN %Table:SD1% SD1 ON  SD1.D1_DOC = SD2.D2_DOC 
				AND SD1.D1_SERIE = SD2.D2_SERIE
				AND SD1.D1_EMISSAO = SD2.D2_EMISSAO 
				AND SD1.D1_FILIAL = %Exp:cFilAnt%
				AND SD1.%NotDel%	
		WHERE SD2.%NotDel% 
				//AND SD2.D2_EMISSAO >= %Exp:aDatas[2]%
				AND SD2.D2_EMISSAO <= %Exp:cDataRef% // %Exp:aDatas[3]%
      			//Trata Transito
      			AND (SD1.D1_TES IS NULL 
      					OR SD1.D1_TES = ' ' 
      					OR SD1.D1_DTDIGIT > %Exp:cDataRef%)	
      			//Trata Filais
      			AND %Exp:cWhere%	
		GROUP BY SD2.D2_FILIAL,
				SD2.D2_EMISSAO,
				SA1.A1_COD,
         		SA1.A1_LOJA,
         		A1_T_INST
UNION
	//-------------------------
	//Atualmente em transito 
	//Pr� nota de fornecedor 
	//-------------------------
	SELECT SD1.D1_FILIAL FILIAL,
			SD1.D1_EMISSAO DATAORI,
			SA2.A2_COD CODIGO,
       	SA2.A2_LOJA LOJA,
       	A2_T_INST INST,
       	Sum(D1_QUANT) TRANSITO
	FROM %Table:SD1% SD1
		JOIN %Table:SA2% SA2 ON SA2.A2_COD = SD1.D1_FORNECE
			AND SA2.A2_LOJA = SD1.D1_LOJA
			AND SA2.%NotDel%
			AND SA2.A2_FILIAL = %xFilial:SA2%
			AND %Exp:cWhereEst% //trata mesmo local de manut
	WHERE SD1.D1_COD = %Exp:cProd%
  		AND SD1.D1_T_ANP45 = '1'
  		AND SD1.D1_TES = ' '
  		//AND SD1.D1_EMISSAO = %Exp:cDataRef%
  		//AND SD1.D1_EMISSAO >= %Exp:aDatas[2]%
  		AND SD1.D1_EMISSAO <= %Exp:cDataRef% //%Exp:aDatas[3]%
  		AND SD1.D1_ORIGLAN <> 'LF'
  		AND SD1.%NotDel%
  		AND SD1.D1_FILIAL = %xFilial:SD1%
	GROUP BY SD1.D1_FILIAL,
				SD1.D1_EMISSAO,
				SA2.A2_COD,
    	    	SA2.A2_LOJA,
         		A2_T_INST
         		
UNION         		
	//-------------------------
	//Atualmente em transito 
	//Pr� nota de fornecedor 
	//-------------------------
	SELECT SD1.D1_FILIAL FILIAL,
			SD1.D1_EMISSAO DATAORI,
			SA2.A2_COD CODIGO,
       	SA2.A2_LOJA LOJA,
       	A2_T_INST INST,
       	Sum(D1_QUANT) TRANSITO
	FROM %Table:SD1% SD1
		JOIN %Table:SA2% SA2 ON SA2.A2_COD = SD1.D1_FORNECE
			AND SA2.A2_LOJA = SD1.D1_LOJA
			AND SA2.%NotDel%
			AND SA2.A2_FILIAL = %xFilial:SA2%
			AND %Exp:cWhereEst% //trata mesmo local de manut
	WHERE SD1.D1_COD = %Exp:cProd%
  		AND SD1.D1_T_ANP45 = '1'
  		AND SD1.D1_TES <> ' ' //Tes Preenchida
  		//AND SD1.D1_EMISSAO >= %Exp:aDatas[2]% // Emitida na semana
  		AND SD1.D1_EMISSAO <= %Exp:cDataRef% // Emitida na semana 
  		AND SD1.D1_DTDIGIT > %Exp:cDataRef% // Recebida na semana seguinte
  		AND SD1.D1_ORIGLAN <> 'LF'
  		AND SD1.%NotDel%
  		AND SD1.D1_FILIAL = %xFilial:SD1%
	GROUP BY SD1.D1_FILIAL,
				SD1.D1_EMISSAO,
				SA2.A2_COD,
    	    	SA2.A2_LOJA,
         		A2_T_INST         		

)As TAB GROUP BY TAB.FILIAL, TAB.DATAORI, TAB.CODIGO,TAB.LOJA,TAB.INST
        		
EndSql

While !(cAliasTran)->(EOF()) 
	If Empty((cAliasTran)->(CODIGO+LOJA+INST))
		aAdd(aRet,{StrZero(Val(SuperGetMV("MV_T_INST",.F.,"",(cAliasTran)->FILIAL)), 10) ,;
						(cAliasTran)->TRANSITO,;
						 cDataRef,;
						 0,(cAliasTran)->DATAORI,;
						 PadR((cAliasTran)->INST,10)})
	Else
		aAdd(aRet,{PadR((cAliasTran)->INST,10),;
						(cAliasTran)->TRANSITO,;
						 cDataRef,;
						 0,(cAliasTran)->DATAORI,;
						 StrZero(Val(SuperGetMV("MV_T_INST",.F.,"",(cAliasTran)->FILIAL)), 10)})
	EndIf
	(cAliasTran)->(DbSkip())
EndDo
(cAliasTran)->(DbCloseArea())

Return aRet	
//-------------------------------------------------------------------
/*/{Protheus.doc} GetESDOB(LBB_CODPRO,LBB_LOCMNT,LBB_MES,LBB_ANO)
Funcao montar query que retorna o estoque objetivo do local de manutencao no periodo

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function GetESDOB(cCodPro,cLocMnt,dDataReg)
Local nRet 		:= 0
Local nX			:= 0
Local aFiliais	:= GetFilMnt(cLocMnt)
Local nDias		:= GetLocMnt(aFiliais[1,1])[1,3]
Local cWhereFil	:= ""
Local cAliasESD	:= GetNextAlias()
Local dRef			:= ANPIniMes(dDataReg)
Local dDataIni	:= ANPIniMes(CTOD("15/"+ANP45MesAno(dRef,"MES")+"/"+Str(Val(ANP45MesAno(dRef,"A"))-1,4)))
Local dDataFim
Local cDataIni
Local cDataFim
Local aArea		:= GetArea()

If Month(dRef) == 12
	dDataFim := ANPIniMes(CTOD("15/01/"+ANP45MesAno(dRef,"A")))
Else
	dDataFim := ANPIniMes(CTOD("15/"+Str(Val(ANP45MesAno(dRef,"MES"))+1)+"/"+Str(Val(ANP45MesAno(dRef,"A"))-1,4))) 
EndIf

cDataIni := DtoS(dDataIni)
cDataFim := DtoS(dDataFim)

For nX := 1 to Len(aFiliais)
	If nX != 1
		cWhereFil += ' OR '
	EndIf
	cWhereFil += " D2_FILIAL =  '"+aFiliais[nX,1]+"'"	 
Next nX
cWhereFil := "%("+ cWhereFil +" )%"

BeginSql Alias cAliasESD
	SELECT SUM(ISNULL(SG1.G1_QUANT * SD2.D2_QUANT, SD2.D2_QUANT)) QUANT
		FROM %Table:SD2% SD2
			JOIN %Table:SF4% SF4 ON SF4.F4_CODIGO = SD2.D2_TES
				AND SF4.F4_ESTOQUE = 'S'
				AND SF4.F4_FILIAL = %xFilial:SF4%
				AND SF4.%NotDel%
			JOIN %Table:SB1% SB1 ON SB1.B1_COD = SD2.D2_COD
				AND SB1.B1_FILIAL = %xFilial:SB1%
				AND SB1.%NotDel%
			JOIN %Table:SA1% SA1 ON SA1.A1_COD = SD2.D2_CLIENTE
				AND SA1.A1_LOJA = SD2.D2_LOJA 
				AND SA1.A1_FILIAL = %xFilial:SA1%
				AND SA1.%NotDel%
			LEFT JOIN %Table:SG1% SG1 ON SG1.G1_COD = SD2.D2_COD
				AND SG1.G1_COMP IN (SELECT SB1X.B1_COD 
									FROM %Table:SB1% SB1X 
										WHERE SB1X.B1_ANP45 = 'T'
											AND SB1X.B1_FILIAL = %xFilial:SB1% 
											AND SB1X.%NotDel%)
				AND SG1.G1_INI <= %Exp:cDataIni%
				AND SG1.G1_FIM > %Exp:cDataFim%
				AND SG1.G1_FILIAL = %xFilial:SG1%
				AND SG1.%NotDel%
		WHERE  NOT (SB1.B1_ANP45 = 'F' AND SG1.G1_COMP IS NULL)
  				AND SD2.D2_EMISSAO >= %Exp:cDataIni% 
				AND SD2.D2_EMISSAO < %Exp:cDataFim% 
  				AND SD2.D2_ORIGLAN <> 'LF'
  				AND IsNull(SG1.G1_COMP,SD2.D2_COD) = %Exp:cCodPro%
  				AND %Exp:cWhereFil%
  				AND SD2.%NotDel%
EndSQL	

nRet := ( (cAliasESD)->QUANT / 30 ) * nDias
	
(cAliasESD)->(DbCloseArea())

RestArea(aArea)
  				
Return nRet	 
//-------------------------------------------------------------------
/*/{Protheus.doc} GetLocMnt(cFil)
Funcao para recuperar Locais de manuten��o

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function GetLocMnt(cFil,cEst,lIgual)
Local aRet		:= {}
Local aLocais	:= {}
Local aFiliais	:= {}
Local nX		:=	0
Local nLocal	:=	0
Local cTipo		:= SuperGetMv("MV_T_ANP",.F.,'D')
Local aAreaSM0	:= SM0->(GetArea())

Default cFil	:= ""
Default cEst 	:= ""
Default lIgual	:= .F.

//------------------
//Tabela ANP
//------------------
If cTipo == "P"
	//--------------------------------------------
	// para produtores de derivados de petrolio
	//--------------------------------------------
	aAdd(aLocais,{"AC|AM|RO|RR|PA|AP",5})//1 Unidades Federadas da Regi�o Norte, exceto TO
	aAdd(aLocais,{"BA|SE|AL|PE|PB|RN|CE|PI|MA|TO",5})//2 TO e Unidades Federadas da Regi�o Nordeste
	aAdd(aLocais,{"ES|MG|MS|MT|RJ|SP|DF|GO",3})//3 Unidades Federadas da Regi�o Centro-Oeste e Sudeste
	aAdd(aLocais,{"PR|SC|RS",3})//4 Unidades Federadas da Regi�o Sul
Else
	//--------------------------------------------
	//para distribuidores de combustiveis
	//-------------------------------------------- 
	aAdd(aLocais,{"AC|AM|AP|PA|RO|RR",5})//1 Unidades Federadas da Regi�o Norte, exceto TO
	aAdd(aLocais,{"BA|SE",3})//2 BA e SE
	aAdd(aLocais,{"AL|CE|MA|PB|PE|PI|RN|TO",5})//3 TO e Unidades Federadas da Regi�o Nordeste, com exce��o de BA e SE
	aAdd(aLocais,{"DF|ES|GO|MG|MS|MT|RJ|SP",3})//4 Unidades Federadas da Regi�o Centro-Oeste e Sudeste
	aAdd(aLocais,{"PR|SC|RS",3})//5 Unidades Federadas da Regi�o Sul
EndIf

SM0->(dbSetOrder(1))

IF Empty(cFil)
	aFiliais := FwLoadSM0()
	For nX := 1 To len(aFiliais)
		If aFiliais[nX,1] == cEmpAnt
			SM0->(MsSeek(cEmpAnt+aFiliais[nX,2]))
			nLocal := aScan(aLocais, {|x| SM0->M0_ESTENT $ x[1]})
			If nLocal >0
				aAdd(aRet,{aFiliais[nX,2],Str(nLocal,1),aLocais[nLocal,2]})
			EndIf
		EndIf	
	Next nX 
Else
	SM0->(MsSeek(cEmpAnt+cFil))
	nLocal := aScan(aLocais, {|x| SM0->M0_ESTENT $ x[1]})
	If nLocal >0
		aAdd(aRet,{cFil,Str(nLocal,1),aLocais[nLocal,2]})
		If !Empty(cEst)
			lIgual := cEst $ aLocais[nLocal,1]  
		EndIf
	EndIf 	
EndIf

RestArea(aAreaSM0)
aAreaSM0 := aSize(aAreaSM0,0)
aFiliais := aSize(aFiliais,0)
aLocais := aSize(aLocais,0)
Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GetFilMnt(cLocal)
Funcao para as Filiais de um mesmo Local de manuten��o

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function GetFilMnt(cLocal)
Local aAllFil	:= GetLocMnt()
Local nX		:= 0
Local aRet		:= {}

For	nX := 1 To Len(aAllFil)
	If aAllFil[nX,2] == cLocal
		aAdd(aRet,aAllFil[nX])	
	EndIf
Next nX

Return aRet


//-------------------------------------------------------------------
/*/{Protheus.doc} GetAllMnt()
Funcao para as Filiais de um mesmo Local de manuten��o

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function GetAllMnt()
Local aAllFil	:= GetLocMnt()
Local nX		:= 0
Local cLocal	:= ""
Local aRet		:= {}

For	nX := 1 To Len(aAllFil)
	If ( aScan(aRet, {|x| x == aAllFil[nX,2] }) == 0 )
		aAdd(aRet,aAllFil[nX,2])	
	EndIf
Next nX

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} ANPIniMes()
Funcao para identificar inicio e fim do mes segundo ANP

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function ANPIniMes(dDataRef)
Local dRet	
Local cMes 	:= ANP45MesAno(dDataRef,"MES")
Local cAno		:= ANP45MesAno(dDataRef,"A")
Local nFator  := 0

If Val(cMes) ==  Val(ANP45MesAno(CTOD("01/"+cMes+"/"+cAno),"MES"))
	nFator := ( DOW(CTOD(("01/"+cMes+"/"+cAno))) - 2)
	If nFator < 0 //Nunca vai entrar
		nFator := 6 //para quando Semana Comeca segunda e dia 1 � Domingo
	EndIf
	dRet := CTOD("01/"+cMes+"/"+cAno) - nFator
Else
	nFator := ( DOW(CTOD(("01/"+cMes+"/"+cAno))))
	If nFator == 2// se for segunda
		nFator := 0
	ElseIf nFator != 1
		nFator := 9 - nFator
	EndIf
	dRet := CTOD("01/"+cMes+"/"+cAno) + nFator
EndIf

Return dRet



//-------------------------------------------------------------------
/*/{Protheus.doc} MTChangeF()
Funcao para trocar de filial

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function MTChangeF(cFil)
Local aAreaSM0 := SM0->(GetArea())

SM0->(dbSetOrder(1))
If SM0->(MsSeek(cEmpAnt+cFil))
	cFilAnt := FWCodFil()
EndIf

RestArea(aAreaSM0)
Return



//-------------------------------------------------------------------
/*/{Protheus.doc} MTChangeF()
Funcao para retornar dado da SM0

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Function MTDataFil(cFil,cData)
Local aAreaSM0 := SM0->(GetArea())
Local cRet := ''

SM0->(dbSetOrder(1))
If SM0->(MsSeek(cEmpAnt+cFil))
	cRet := SM0->&(cData)
EndIf

RestArea(aAreaSM0)
Return cRet


//-------------------------------------------------------------------
/*/{Protheus.doc} MtANPTer()
Calcular Poder de terceiro 

@author Alexandre Gimenez
@Return aRet[1] Inst2
		 aRet[2] Saldo em Transito
		 aRet[3] Data do Registro
		 aRet[4] Saldo em Terceiro
		 
@since 10/04/2017
@version P12
/*/
//-------------------------------------------------------------------
Function MtANPTer(cProd,dDataReg)
Local aRet		:= {}
Local aSaldo	:= {}
Local lUsaPETN3	:= SuperGetMV("MV_SDTESN3",.F.,0) == 0
Local cArmTerDe	:= Replicate(" ",TamSx3("NNR_CODIGO")[1])
Local cArmTerAte:= Replicate("Z",TamSx3("NNR_CODIGO")[1])
Local nX		:= 0
Local aArea		:= GetArea()
Local aAreaSA2	:= {}
Local lIgual	:= .F.
Local aLocal	:= {}

//Adiciona valor vazio para ser considerado local 
aADD(aRet,{Replicate("0",10),0,DtoS(dDataReg),0,""})
	
aSaldo := SaldoTerc(cProd,cArmTerDe,"E",dDataReg,cArmTerAte,.T. /*lCliFor*/,,lUsaPETN3,,,,,.F. /*lIdent*/)
DbSelectArea("SA2")
aAreaSA2 := GetArea()
DbSetOrder(1)
For nX := 1 To Len(aSaldo)
	If !Empty(aSaldo[nX,1]) 
		If SubStr(aSaldo[nX,1],1,1) == "F" .And. aSaldo[nX,2] > 0  // Cliente+Loja
			If SA2->(DbSeek(xFilial("SA2")+SubStr(aSaldo[nX,1],2))) 
				aLocal := GetLocMnt(cFilAnt,SA2->A2_EST,@lIgual)	
				If lIgual
					If !Empty(SA2->A2_T_INST)
						aADD(aRet,{PadR(SA2->A2_T_INST,10),0,DtoS(dDataReg),aSaldo[nX,2],""})
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf
Next nX

RestArea(aAreaSA2)
RestArea(aArea)

aSaldo := aSize(aSaldo,0)
aLocal := aSize(aLocal,0) 
aArea := aSize(aArea,0)
aAreaSA2 := aSize(aAreaSA2,0)

Return aRet


//-------------------------------------------------------------------
/*/{Protheus.doc} GetEstados(cFil)
Funcao para recuperar Locais de manuten��o

@author Alexandre Gimenez
@since 05/5/2016
@version P12
/*/
//-------------------------------------------------------------------
Static Function GetEstados(cFil)
Local aRet 		:= {}
Local aLocais	:= {}
Local cTipo		:= SuperGetMv("MV_T_ANP",.F.,'D')
Local cEst		:= MTDataFil(cFil,"M0_ESTENT")
Local nLocal	:= 0
//------------------
//Tabela ANP
//------------------
If cTipo == "P"
	//--------------------------------------------
	// para produtores de derivados de petrolio
	//--------------------------------------------
	aAdd(aLocais,{"AC|AM|RO|RR|PA|AP",5})//1 Unidades Federadas da Regi�o Norte, exceto TO
	aAdd(aLocais,{"BA|SE|AL|PE|PB|RN|CE|PI|MA|TO",5})//2 TO e Unidades Federadas da Regi�o Nordeste
	aAdd(aLocais,{"ES|MG|MS|MT|RJ|SP|DF|GO",3})//3 Unidades Federadas da Regi�o Centro-Oeste e Sudeste
	aAdd(aLocais,{"PR|SC|RS",3})//4 Unidades Federadas da Regi�o Sul
Else
	//--------------------------------------------
	//para distribuidores de combustiveis
	//-------------------------------------------- 
	aAdd(aLocais,{"AC|AM|AP|PA|RO|RR",5})//1 Unidades Federadas da Regi�o Norte, exceto TO
	aAdd(aLocais,{"BA|SE",3})//2 BA e SE
	aAdd(aLocais,{"AL|CE|MA|PB|PE|PI|RN|TO",5})//3 TO e Unidades Federadas da Regi�o Nordeste, com exce��o de BA e SE
	aAdd(aLocais,{"DF|ES|GO|MG|MS|MT|RJ|SP",3})//4 Unidades Federadas da Regi�o Centro-Oeste e Sudeste
	aAdd(aLocais,{"PR|SC|RS",3})//5 Unidades Federadas da Regi�o Sul
EndIf

nLocal := aScan(aLocais, {|x| cEst $ x[1]})
If nLocal > 0
	aRet := aLocais[nLocal,1]
EndIf

Return aRet



