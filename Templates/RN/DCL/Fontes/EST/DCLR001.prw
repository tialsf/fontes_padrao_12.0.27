#include "protheus.ch"
#include "report.ch"


//-------------------------------------------------------------------
/*/{Protheus.doc} DCLR001
Relat�rio resumido para atender a resolu��o ANP 45/2013

@author Alexandre Gimenez
@since 05/5/2016
@version P11/P12
/*/
//-------------------------------------------------------------------
Template Function DCLR001()
Local oReport
Local oLBB
Local lRet := AliasInDic("LBB")


Pergunte("ANP45REL",.F.)

DEFINE REPORT oReport NAME "DCLR001" TITLE "ANP 45"  ACTION {|oReport| PrintReport(oReport)} PARAMETER "ANP45REL"

	DEFINE SECTION oLBB OF oReport TITLE "Relatorio ANP45" TABLES "LBB","SB1" 

		DEFINE CELL NAME "LBB_NATURE" OF oLBB ALIAS "LBB"
		DEFINE CELL NAME "LBB_CODREG" OF oLBB ALIAS "LBB"
		DEFINE CELL NAME "LBB_INST1" OF oLBB ALIAS "LBB"
		DEFINE CELL NAME "LBB_INST2" OF oLBB ALIAS "LBB"
		DEFINE CELL NAME "LBB_LOCMNT" OF oLBB ALIAS "LBB"
		DEFINE CELL NAME "LBB_CODPRO" OF oLBB ALIAS "LBB"
		DEFINE CELL NAME "B1_DESC" OF oLBB ALIAS "SB1"
		DEFINE CELL NAME "LBB_SEMANA" OF oLBB ALIAS "LBB"
		DEFINE CELL NAME "LBB_MES" OF oLBB ALIAS "LBB"
		DEFINE CELL NAME "LBB_ANO" OF oLBB ALIAS "LBB"
		DEFINE CELL NAME "LBB_ESMD" OF oLBB ALIAS "LBB"
		

oReport:PrintDialog()

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} PrintReport
Impress�o de Relat�rio

@author Alexandre Gimenez 
@since 05/5/2016
@version P11/P12
/*/
//-------------------------------------------------------------------
Static Function PrintReport(oReport)
Local cAlias := GetNextAlias()
Local cWhere := DclGetWhere()
Local nMult	 := SuperGetMv("MV_ANP45UM",.F.,1)
Local cMult	 := ''
 

If nMult = 2
	cMult := "Case when B1_TIPCONV = 'M' THEN ( Case WHEN B1_CONV > 0 then(LBB_ESMD * B1_CONV ) Else (LBB_ESMD) END ) 	When B1_TIPCONV = 'D' THEN ( Case WHEN B1_CONV > 0 then(LBB_ESMD / B1_CONV ) Else (LBB_ESMD)  END ) ELSE (LBB_ESMD) End"
ElseIF nMult = 3
	cMult := "Case when B5_TCONDCL = 'M' THEN ( Case WHEN B5_CONVDCL > 0 then(LBB_ESMD * B5_CONVDCL ) Else (LBB_ESMD) END ) When B5_TCONDCL = 'D' THEN ( Case WHEN B5_CONVDCL > 0 then(LBB_ESMD / B5_CONVDCL ) Else (LBB_ESMD)  END ) ELSE (LBB_ESMD) End"
Else
	cMult := "LBB_ESMD"
EndIf

cMult := "%" + cMult + "%"

MakeSqlExp("ANP45REL")
	
BEGIN REPORT QUERY oReport:Section(1)
	
	BeginSql alias cAlias
		SELECT LBB_NATURE,LBB_CODREG,LBB_INST1,LBB_INST2,LBB_LOCMNT,LBB_CODPRO,
			   LBB_SEMANA,LBB_MES,LBB_ANO,B1_DESC,
			   (%Exp:cMult%) /1000 as LBB_ESMD
		
		FROM %table:LBB% LBB,%table:SB1% SB1 
		LEFT JOIN %table:SB5% SB5 
			ON B5_FILIAL = %xfilial:SB5% 
				AND SB1.B1_COD = SB5.B5_COD 
				AND SB5.%NotDel%
		WHERE LBB.%NotDel% 
		AND B1_FILIAL = %xfilial:SB1% AND B1_COD = LBB_CODPRO
		AND LBB_ESMD <> 0
		AND SB1.%NotDel%
		AND %Exp:cWhere%
	EndSql
	
END REPORT QUERY oReport:Section(1)

	oReport:Section(1):Print()
Return