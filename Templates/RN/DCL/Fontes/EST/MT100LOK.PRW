/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �MT100LOK  � Autor � ANTONIO CORDEIRO      � Data � JULHO/2000��
�������������������������������������������������������������������������Ĵ��
���Descri��o � Ponto de Entrada para confirmar o item da NF. Entrada      ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Especifico                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

#include "rwmake.ch"

Template Function Mt100lok()
Local _aArea        := GetArea()
Local _aAreaSF5     := SF5->(GetArea())
Local _aAreaSB1     := SB1->(GetArea())
Local _aAreaLB2     := LB2->(GetArea())
Local _aAreaSD3     := SD3->(GetArea())
Local _lRet			:= .T.
Local _nPosX_ATUTQ  := aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_X_ATUTQ"})
Local _nPosLOCAL 	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_LOCAL"})
Local _nPosORP  	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_T_DPROV"})
Local _nPosCod   	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_COD"})
Local _nPosQtd   	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_QUANT"})
Local _nPosX_TQ  	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_X_TQ"})
Local _nPosX_VIA 	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_X_VIA"})
Local _nPosX_QTD1	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_X_QTD1"})
Local _nPosX_ENT	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_X_ENT"})
local _nPosX_Apur   := aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_X_APUR"})
Local _nPosTes   	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_TES"})
Local _nPosCF   	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "D1_CF"})

Local _nPosDel      := Len(aHeader) + 1
Local _Mens         := ""
Local _Mens1        := ""
Local I				:= 0
Local cMensDados    :=""
//CHKTEMPLATE("PETROEST")

If HasTemplate("DCLEST")

	SB1->(msSeek(xFilial("SB1")+Acols[N,_nPosCod]))
	IF Acols[N,_nPosX_ATUTQ]=="S" .AND. !aCols[n,_nPosDel]
		IF EMPTY(Acols[N,_nPosX_TQ])
			_Mens:=_Mens+"-> Tanque Invalido"+CHR(10)
			_lRet:=.F.
		ENDIF
		IF CTIPO<>"D" .AND. CTIPO<>"N"
			_Mens:=_Mens+"-> Somente Tipo de notas Normal e Devolucao deve atualizar tanque"+CHR(10)
			_lRet:=.F.
		ENDIF
		
		IF Acols[N,_nPosX_ENT]<= GETMV("MV_ULMES")
			_Mens:=_Mens+"-> Data de Entrega Anterior a Fechamento de Estoque"+CHR(10)
			_lRet:=.F.
		ENDIF
	

		
		IF ! (LB2->(msSeek(xFilial("LB2")+Acols[N,_nPosX_TQ]+Acols[N,_nPosCod])))
			_Mens:=_Mens+"-> Tanque Nao Cadastrado para este Produto"+CHR(10)
			_lRet:=.F.
		ENDIF
		
		IF Acols[N,_nPosX_VIA]<>"01" .AND. Acols[N,_nPosX_VIA]<>"02"
			_Mens:= "-> Tipo descarga deve ser 01-Nota Acompanha Produto/02-Nota nao acompanha produto"+CHR(10)
			_lRet:=.F.
		ELSE
			SF5->(dbOrderNickName("DCLSF5_01"))
			IF Acols[N,_nPosX_VIA]=="01"
				IF ! (SF5->(msSeek(xFilial("SF5")+"DE"+"R"))) .OR. ! (SF5->(msSeek(xFilial("SF5")+"DE"+"D")))
					_Mens:=_Mens+"-> Movimentos para Ajustes de Descarga Nao Cadastrados "+CHR(10)
					_lRet:=.F.
				ENDIF
				IF Acols[N,_nPosX_QTD1]<=0
					_Mens:=_Mens+"Quantidade apurada 20 graus Nao informada"+CHR(10)
					_lRet:=.F.
				ENDIF
				
				IF ALLTRIM(ALLTRIM(Acols[N,_nPosCF])) $ SuperGetMV('MV_XCFORET',.F.,"")
					
					LB6->(DBSETORDER(2))
					IF ! LB6->(DBSEEK(XFILIAL('LB6')+ca100For+cLoja))
						_lRet:=.F.
						_Mens+='Fornecedor nao encontrado no cadastro de cessionarias (LB6) '+CHR(10)
					ELSE
						IF Acols[N,_nPosLOCAL]<>LB6->LB6_LOCAL
							_lRet:=.F.
							_Mens+='Armazem Invalido!!!  Armazem do Cessionario : '+LB6->LB6_LOCAL+ ' Armazem digitado: '+Acols[N,_nPosLOCAL]+CHR(10)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				
				IF Acols[N,_nPosX_QTD1]<=0
					_Mens:=_Mens+"Quantidade apurada 20 graus Nao informada"+CHR(10)
					_lRet:=.F.
				ENDIF
				
				IF ! (SF5->(msSeek(xFilial("SF5")+"PR"+"R"))) .OR. ! (SF5->(msSeek(xFilial("SF5")+"PR"+"D")))
					_Mens:=_Mens+"-> Movimentes para Estorno de Bombeio Nao Cadastrados "+CHR(10)
					_lRet:=.F.
				ENDIF
				
				IF ! (SF5->(msSeek(xFilial("SF5")+"BO"+"R"))) .OR. ! (SF5->(msSeek(xFilial("SF5")+"BO"+"D")))
					_Mens:=_Mens+"-> Movimentes para Ajustes de Bombeio Nao Cadastrados "+CHR(10)
					_lRet:=.F.
				ENDIF
				DBSELECTAREA("SD3")
				dbOrderNickName("DCLSD3_01")
				IF  ! SD3->(msSeek(xFilial("SD3")+(Acols[N,_nPosORP])+"PR"+" "))
					_Mens1:=_Mens1+"Movimento provisorio com esta ORP nao encontrado "+CHR(10)
					_Mens1:=_Mens1+"Confirma geracao de perda de bombeio no total do item da nota fiscal"+CHR(10)
					IF MsgYesNo(_Mens1)
					ELSE
						_lRet:=.F.
						_MENS:=_MENS+"Nota nao confirmada"
					ENDIF
				ELSE
					
					IF Acols[N,_nPosX_Apur]<>SD3->D3_QUANT
						_Mens+='Quantidade da Nota nao pode ser diferente da Quantidade do movimento provisorio (SD3)  '+CHR(10)
						_lRet:=.F.
					ENDIF
					
					
					IF ALLTRIM(Acols[N,_nPosCF]) $ SuperGetMV('MV_XCFORET',.F.,"")
						
						IF ! EMPTY(Acols[N,_nPosORP])
							LB6->(DBSETORDER(2))
							IF ! LB6->(DBSEEK(XFILIAL('LB6')+ca100For+cLoja))
								_lRet:=.F.
								_Mens+='Fornecedor nao encontrado no cadastro de cessionarias (LB6) '+CHR(10)
							ELSE
								IF Substr(Acols[N,_nPosORP],5,2)<>LB6->LB6_LOCAL
									_lRet:=.F.
									_Mens+='Armazem do cessionario: '+LB6->LB6_LOCAL+ ' Diferente da ORP: '+Substr(Acols[N,_nPosORP],5,2)+CHR(10)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF SD3->D3_COD <> Acols[N,_nPosCod]
					_Mens:=_Mens+"-> Produto invalido para esta ORP "+CHR(10)
					_lRet:=.F.
				ENDIF
	
				IF SD3->D3_LOCAL <> Acols[N,_nPosLocal]
					_Mens:=_Mens+"-> Local invalido para esta ORP!! o armazem deve ser igual ao final do nr. da Orp. "+CHR(10)
					_lRet:=.F.
				ENDIF
	
				FOR I:=1 TO LEN(ACOLS)-1
					IF !aCols[I,_nPosDel]
						IF Acols[I,_nPosORP] == Acols[I+1,_nPosORP]
							_Mens:=_Mens+"-> Nao pode existir nr. de ORP repetido"+CHR(10)
							_lRet:=.F.
						ENDIF
					ENDIF
				NEXT
				
			ENDIF
			SF5->(dbSetOrder(1))
		ENDIF
		IF ! _lRet
			MsgBox(_Mens,"Dados Incorretos","INFO")
		ENDIF
		
		IF _lRet
			_lRet:= T_TDCEA10MARG()
		Endif
		
	ENDIF
	
	
	If _lRet
		/*
		������������������������������������������������������������������
		������������������������������������������������������������������
		��������������������������������������������������������������Ŀ��
		���Nao Permite Volume de Entrada + Volume em estoque ser maior ���
		���que o declarado no Campo: A5XVOLMAX - Tabela SA5.		   ���
		���������������������������������������������������������������ٱ�
		������������������������������������������������������������������
		������������������������������������������������������������������*/
		_lRet:= T_TDCEA10VMAX (GDFieldGet("D1_COD"),ca100For,cLoja,GDFieldGet("D1_LOCAL"),"1",Acols[N,_nPosQtd])
	ENDIF
	
	
/*	IF ALLTRIM(aCols[n,_nPosCod]) $ SUPERGETMV('MV_ESTZERO',.F.,'  ')  // Tratamento para produtos sem estoque.
		SF4->(DBSETORDER(1))
		IF SF4->(DBSEEK(XFILIAL('SF4')+aCols[n,_nPosTes])) .AND. SF4->F4_ESTOQUE =='S'
			cMensDados:=T_TDCEC001(aCols[n,_nPosCod],aCols[n,_nPosQtd])
			IF ! Empty(cMensDados)
				MsgBox(cMensDados,'Item nao pode ser gravado','info')
				_lRet:=.F.
			ENDIF
		ENDIF
	ENDIF */
	
	
	LB2->(RestArea(_aAreaLB2))
	SB1->(RestArea(_aAreaSB1))
	SF5->(RestArea(_aAreaSF5))
	SD3->(RestArea(_aAreaSD3))
	RestArea(_aArea)
EndIf
RETURN(_lRet)

