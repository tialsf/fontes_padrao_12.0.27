#Include 'Protheus.ch'

/*
����������������������������������������������������������������������
����������������������������������������������������������������������
������������������������������������������������������������������Ŀ��
���Fun�ao    � MT250TOK  � Autor � Robson Sales  � Data �18/03/2014���
������������������������������������������������������������������Ĵ��
���Descri�ao � Ponto de entrada MT250TOK, para validar se prossegue���
���          � ou nao com o apontamento de producao.               ���
�������������������������������������������������������������������ٱ�
����������������������������������������������������������������������
����������������������������������������������������������������������
*/
Template Function MT250TOK()

Local lRet := .T.
Local aAreaSD4 := SD4->(GetArea())
Local cProg := AllTrim(FunName())

dbselectarea("SD4")
dbsetorder(2)

If (cProg <> "MATA460A") .And. (cProg <> "MATA460B")
	If MsSeek(xFilial("SD4")+M->D3_OP)
		While Alltrim(SD4->D4_OP) == Alltrim(M->D3_OP)
			lRet := T_ValEstDcl(SD4->D4_COD,SD4->D4_LOCAL,SD4->D4_QUANT,M->D3_EMISSAO,3)
			If !lRet
				Exit
			EndIf
			dbselectarea("SD4")
			dbskip()
		EndDo
	EndIf
EndIf

RestArea(aAreaSD4)

Return lRet