#include "rwmake.ch"
Template Function MT410CPY()
Local cCmpUsr   := SuperGetMV("MV_CMPUSR",,"C5_MENNOTA")

If HasTemplate("DCLEST")
	//-- Validar conteudo parametro MV_CMPUSR
	If (Empty(cCmpUsr) .Or. SC5->(FieldPos(cCmpUsr)) == 0)
		cCmpUsr := "C5_MENNOTA"
	EndIf
	//-- Limpar campos copiados
	If SC5->(FieldPos("C5_T_ENVT")) > 0
		M->C5_T_ENVT := ""
	EndIf
	&(cCmpUsr) := ""
EndIf

Return Nil