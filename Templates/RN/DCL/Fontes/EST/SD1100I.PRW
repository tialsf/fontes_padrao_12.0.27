#Include "SD1100I.ch"
#Include "RWMAKE.CH"
#Include "TOPCONN.CH"

/*����������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Funcao    � SD1100I  � Autor � Materiais              � Data � MAI/2003 ���
��������������������������������������������������������������������������Ĵ��
���Descricao � Ponto de entrada da nota fiscal de entrada que gera movimen-���
���          � tacao de estoque para equalizacao de estoque.               ���
��������������������������������������������������������������������������Ĵ��
���Uso       � Template DCL-EST                                            ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Template Function SD1100I()

Local _aArea 	  	:= GetArea()
Local _aAreaSF5 	:= SF5->(GetArea())
Local _aAreaSB1 	:= SB1->(GetArea())
Local lRateio		:= SuperGetMV("MV_XPSBOMB",.F., .F.)
Local cTipo			:= "BO"
Private _aOpcoes	:= {OemToAnsi(STR0001),; //"NFEB-NF de Bombeio atualiza o almoxarifado base (MV_X_LBASE)"
					   OemToAnsi(STR0002)} //"NFED-NF de Descarga atualiza o almoxarifado corrente da nota fical"
Private _nOpcao		:= 1
Private lMsErroAuto	:= .F.

SetPrvt("CMSDALIAS,NMSDINDEX,NMSDRECNO,_LENTROU,_LFEZ,_QUANT")
SetPrvt("_TM,_EMISSAO,_COD,_UM,_TANQUE,_CF")
SetPrvt("_CLIENTE,_LOJA,_LOCAL,_NREDUZ,_DOC,_GRUPO")
SetPrvt("_CUSTO1,_NUMSEQ,_SEGUM,_QTSEGUM,_TIPO,_ESTORNO")
SetPrvt("_CHAVE,_DIF,_MENS,_DIFER,I,CTES")
SetPrvt("NQUANT,_CNUMFA,")

If HasTemplate("DCLEST") .And. SD1->(FieldPos('D1_X_ATUTQ')) > 0
	//���������������������������������������������������������������������Ŀ
	//�Executa o Ponto de Entrada se "Atualiza tanque?" for igual a "SIM"   �
	//�����������������������������������������������������������������������
	If SD1->D1_X_ATUTQ == "S"
		dbSelectArea("SD1")
		dbSelectArea("SF1")
		
		Reclock("SF1",.F.)
		If !EMPTY(SD1->D1_T_DPROV)
	   		SF1->F1_BOMDES := "NFEB" 
	   		LBZ->(dbSetOrder(2)) // LBZ_FILIAL + LBZ_CODIGO
	   		If LBZ->(dbSeek(SD1->D1_FILIAL+SUBSTR(SD1->D1_T_DPROV,1,4)))
				cTipo := LBZ->LBZ_TIPO
	   		EndIf
		Else
			SF1->F1_BOMDES := "NFED"
		EndIf	
		MSUnlock()
		
		//��������������������������������������������������������������������Ŀ
		//� NOTAS FISCAIS ENTRADA DE BOMBEIO - SO PROCESSA SE ATUALIZA ESTOQUE �
		//����������������������������������������������������������������������
		SB1->(MsSeek(xFilial("SB1")+SD1->D1_COD))
		If	(SF1->F1_TIPO == "N" .OR. SF1->F1_TIPO == "D") .AND.;   //SO NOTAS DEVOLUCAO E NORMAL
			! Empty(SD1->D1_X_ENT) .AND. ;                          //SO DATA ENTREGA PREENCHIDA
			SB1->B1_X_REQUI <> "S" .AND.;                           //SO PRODUTOS APROPRIACAO DIRETA PRODUTO ACABADO
			! Empty(SD1->D1_X_TQ)                                   //SO TANQUE PREENCHIDO
			If  SD1->D1_X_VIA == "02" 								//RECEBIMENTO
				//��������������������������������������������������������������Ŀ
				//� VERIFICA SE PEDIDO EXISTE NO CADASTRO SD3 MOV.INTERNO        �
				//����������������������������������������������������������������
				dbSelectArea("SD3")
				dbOrderNickName("DCLSD3_01")
				_PROV := 0
				If dbSeek(xFilial()+SD1->D1_T_DPROV+"PR"+" ")
					_PROV := SD3->D3_QUANT
					
					aVetor := {	{"D3_NUMSEQ"	,SD3->D3_NUMSEQ	,NIL},;
								{"D3_CHAVE"		,SD3->D3_CHAVE	,NIL},;
								{"D3_COD"		,SD3->D3_COD	,NIL},;
					            {"INDEX"		,4				,NIL}}

					MSExecAuto({|x,y| MATA240(x,y)},aVetor,5) // Estorno
					If lMsErroAuto
						MostraErro()
						Aviso(OemToAnsi(STR0006),OemToAnsi(STR0007),{"Ok"}) // "A inclus�o do Documento de Entrada n�o ser� concluida devido aos erros apresentados."
						lContDCL := .F.
						Return()
					EndIf
				EndIf
				
				_DIFER := _PROV - SD1->D1_QUANT
				If _DIFER <> 0
					If SF1->F1_BOMDES == "NFEB"
						//������������������������������������������������������Ŀ
						//� GERA MOVIMENTACAO INTERNA DA DIFERENCA DO BOMBEIO    �
						//��������������������������������������������������������
						SF5->(dbOrderNickName("DCLSF5_01"))
						SF5->(MsSeek(xFilial("SF5")+cTipo+"R"))
						_TMR := SF5->F5_CODIGO
						SF5->(MsSeek(xFilial("SF5")+cTipo+"D"))
						_TMD := SF5->F5_CODIGO
						SF5->(dbSetOrder(1))
						lMsErroAuto := .F.
						dbSelectArea("SD3")
						
						aVetor := {	{"D3_TM"     ,IIf(_Difer>0,_TMD,_TMR)							,NIL},;
									{"D3_COD"    ,SD1->D1_COD										,NIL},;
									{"D3_QUANT"  ,IIf(_Difer<0,(_Difer*-1),_Difer)					,NIL},;
									{"D3_LOCAL"  ,IIf(!lRateio,GETMV("MV_X_LBASE"),SD1->D1_LOCAL)	,NIL},;
									{"D3_DOC"    ,SD1->D1_DOC										,NIL},;
									{"D3_EMISSAO",SD1->D1_DTDIGIT									,NIL} }
									
						If SD3->(FieldPos("D3_X_CLI")) > 0
							Aadd(aVetor,{"D3_X_CLI"  ,SD1->D1_FORNECE,NIL})
							Aadd(aVetor,{"D3_X_LOJA" ,SD1->D1_LOJA,NIL})
						EndIf
						If SD3->(FieldPos("D3_X_TQ")) > 0
							Aadd(aVetor,{"D3_X_TQ"   ,SD1->D1_X_TQ,NIL})
						EndIf
						If SD3->(FieldPos("D3_X_NF")) > 0
							Aadd(aVetor,{"D3_X_NF"   ,SD1->D1_T_DPROV,NIL})
						EndIf
						If SD3->(FieldPos("D3_X_TMT")) > 0
							Aadd(aVetor,{"D3_X_TMT"  ,cTipo,NIL})
						EndIf

						MSExecAuto({|x,y| MATA240(x,y)},aVetor,3) //Inclusao
						If lMsErroAuto
							MostraErro()
							Aviso(OemToAnsi(STR0006),OemToAnsi(STR0007),{"Ok"}) // "A inclus�o do Documento de Entrada n�o ser� concluida devido aos erros apresentados."
							lContDCL := .F.
							Return()
						EndIf
					ElseIf  Trim(SF1->F1_BOMDES) == "NFED"
						//������������������������������������������������������Ŀ
						//� GERA MOVIMENTACAO INTERNA DA DIFERENCA DA DESCARGA   �
						//��������������������������������������������������������
						SF5->(dbOrderNickName("DCLSF5_01"))
						SF5->(msSeek(xFilial("SF5")+"PR"+"R"))
						_TMR := SF5->F5_CODIGO
						SF5->(msSeek(xFilial("SF5")+"PR"+"D"))
						_TMD := SF5->F5_CODIGO
						SF5->(dbSetOrder(1))
						lMsErroAuto := .F.
						dbSelectArea("SD3")
						
						aVetor := {	{"D3_TM"     ,IIf(_Difer>0,_TMD,_TMR)			,NIL},;
									{"D3_COD"    ,SD1->D1_COD						,NIL},;
									{"D3_QUANT"  ,IIf(_Difer<0,(_Difer*-1),_Difer)	,NIL},;
									{"D3_LOCAL"  ,SD1->D1_LOCAL						,NIL},;
									{"D3_DOC"  	 ,SD1->D1_DOC						,NIL},;
									{"D3_EMISSAO",SD1->D1_DTDIGIT					,NIL}}
						
						If SD3->(FieldPos("D3_X_CLI")) > 0
							Aadd(aVetor,{"D3_X_CLI"  ,SD1->D1_FORNECE,NIL})
							Aadd(aVetor,{"D3_X_LOJA" ,SD1->D1_LOJA,NIL})
						EndIf
						If SD3->(FieldPos("D3_X_TQ")) > 0
							Aadd(aVetor,{"D3_X_TQ"   ,SD1->D1_X_TQ,NIL})
						EndIf
						If SD3->(FieldPos("D3_X_NF")) > 0
							Aadd(aVetor,{"D3_X_NF"   ,SD1->D1_T_DPROV,NIL})
						EndIf
						If SD3->(FieldPos("D3_X_TMT")) > 0
							Aadd(aVetor,{"D3_X_TMT"  ,"DE",NIL})
						EndIf

						MSExecAuto({|x,y| MATA240(x,y)},aVetor,3) //Inclusao
						If lMsErroAuto
							MostraErro()
							Aviso(OemToAnsi(STR0006),OemToAnsi(STR0007),{"Ok"}) // "A inclus�o do Documento de Entrada n�o ser� concluida devido aos erros apresentados."
							lContDCL := .F.
							Return()
						EndIf
					EndIf
				EndIf
			ElseIf  SD1->D1_X_VIA == "01"
				_DIFER := SD1->D1_X_QTD1 - SD1->D1_QUANT
				If _DIFER <> 0
					SF5->(dbOrderNickname("DCLSF5_01"))
					SF5->(msSeek(xFilial("SF5")+"DE"+"R"))
					_TMR := SF5->F5_CODIGO
					SF5->(msSeek(xFilial("SF5")+"DE"+"D"))
					_TMD := SF5->F5_CODIGO
					SF5->(dbSetOrder(1))
					lMsErroAuto := .F.
					dbSelectArea("SD3")
					
					aVetor := {	{"D3_TM"     ,IIf(_Difer>0,_TMD,_TMR)			,NIL},;
								{"D3_COD"    ,SD1->D1_COD						,NIL},;
								{"D3_QUANT"  ,IIf(_Difer<0,(_Difer*-1),_Difer)	,NIL},;
								{"D3_LOCAL"  ,SD1->D1_LOCAL						,NIL},;
								{"D3_DOC"    ,SD1->D1_DOC						,NIL},;
								{"D3_EMISSAO",SD1->D1_DTDIGIT					,NIL}}
					
					If SD3->(FieldPos("D3_X_TQ")) > 0
						Aadd(aVetor,{"D3_X_TQ"   ,SD1->D1_X_TQ,NIL})
					EndIf
					If SD3->(FieldPos("D3_X_NF")) > 0
						Aadd(aVetor,{"D3_X_NF"   ,SD1->D1_DOC,NIL})
					EndIf
					If SD3->(FieldPos("D3_X_TMT")) > 0
						Aadd(aVetor,{"D3_X_TMT"  ,"DE",NIL})
					EndIf

					MSExecAuto({|x,y| MATA240(x,y)},aVetor,3) //Inclusao
					If lMsErroAuto
						MostraErro()
						Aviso(OemToAnsi(STR0006),OemToAnsi(STR0007),{"Ok"}) // "A inclus�o do Documento de Entrada n�o ser� concluida devido aos erros apresentados."
						lContDCL := .F.
						Return()
					EndIf

					If SD3->(FieldPos("D3_X_NF")) > 0 .And. SD3->(FieldPos("D3_X_CLI")) > 0
						Reclock("SD3")
						SD3->D3_X_NF   := SD1->D1_DOC
						SD3->D3_X_CLI  := SD1->D1_FORNECE
						SD3->D3_X_LOJA := SD1->D1_LOJA
						MSUnlock()
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf
EndIf

SB1->(RestArea(_aAreaSB1))
SF5->(RestArea(_aAreaSF5))
RestArea(_aArea)

Return()