#include "rwmake.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �TDCEA002  � Autor � Antonio Cordeiro   � Data � JUNHO/2002  ���
�������������������������������������������������������������������������͹��
���Descri��o �Browse para informacao de Hora de Saida da Nota Fiscal      ���
���          � 		                                                      ���
�������������������������������������������������������������������������͹��
���Sintaxe   �TDCEA002()	                                              ���
�������������������������������������������������������������������������͹��
���Parametros�Nenhum			                                          ���
�������������������������������������������������������������������������͹��
���Retorno   �Nenhum			                                          ���
�������������������������������������������������������������������������͹��
���Uso       �Template DCL/Estoque                                        ���
�������������������������������������������������������������������������͹��
���Arquivos  � SF2 - Notas Fiscais de Saida                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Descri��o � PLANO DE MELHORIA CONTINUA        �Programa   TDCEA002.PRW ���
�������������������������������������������������������������������������Ĵ��
���ITEM PMC  � Responsavel              � Data       	|BOPS             ���
�������������������������������������������������������������������������Ĵ��
���      01  �                          �           	|                 ���
���      02  � Ricardo Berti            � 24/03/2006	| 00000095146     ���
���      03  �                          �           	|                 ���
���      04  �                          �           	|                 ���
���      05  �                          �           	|                 ���
���      06  �                          �           	|                 ���
���      07  �                          �           	|                 ���
���      08  �                          �           	|                 ���
���      09  �                          �           	|                 ���
���      10  � Ricardo Berti            � 24/03/2006	| 00000095146     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Template Function TDCEA002()
Local I:=0, J:=0
Local nTamSX1   := Len(SX1->X1_GRUPO)
Local nTamNF	:= TamSX3('D1_DOC')[1]
//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que ra somente as variaveis definidas pelo usuario,  		�
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������
Private _cPerg      :="CEA002"
Private aRegs      :={}

CHKTEMPLATE("DCLEST")
lAS400 := .F.

#IFNDEF WINDOWS
	ScreenDraw("SMT050",3,0,0,0)
	@ 03,03 Say "Notas Fiscais Saida" Color "R/W"
	cCor := SetColor()
#ELSE
	cCadastro := OemtoAnsi("Notas Fiscais Saida")
#ENDIF

aRotina :=	{{ "Pesquisar" ,   "AxPesqui"                  , 0, 1},;
			 { "Informar Horas ","T_TDCEA01A", 0, 6}}

DBSELECTAREA("SX1")
aAdd(aRegs,{_cPerg,"01","Serie............. ?","","","mv_ch1","C",03,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{_cPerg,"02","Nota Fiscal de.... ?","","","mv_ch2","C",06,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{_cPerg,"03","Nota Fiscal Ate... ?","","","mv_ch3","C",06,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{_cPerg,"04","Data Emissao...... ?","","","mv_ch4","D",08,0,0,"G","","mv_par04","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{_cPerg,"05","Data da Saida..... ?","","","mv_ch5","D",08,0,0,"G","","mv_par05","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{_cPerg,"06","Hora Saida HH/MM.. ?","","","mv_ch6","C",05,0,0,"G","","mv_par06","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{_cPerg,"07","Sobrepor Dados.... ?","","","mv_ch7","N",01,0,0,"C","","mv_par07","Nao","","","","","Sim","","","","","","","","","","","","","","","","","","","","",""})

For i:=1 to Len(aRegs)
	If dbSeek(PADR(_cPerg,nTamSX1)+aRegs[i,2]) .And. SX1->X1_PERGUNT <> aRegs[i,3]
		RecLock("SX1",.F.)
		For j:=1 to FCount()
			If j <= Len(aRegs[i])
				FieldPut(j,aRegs[i,j])
			Endif
		Next
		MsUnlock()
	ElseIf !dbSeek(PADR(_cPerg,nTamSX1)+aRegs[i,2])
		RecLock("SX1",.T.)
		For j:=1 to FCount()
			If j <= Len(aRegs[i])
				FieldPut(j,aRegs[i,j])
			Endif
		Next
		MsUnlock()
	Endif
Next

If TamSX3('D2_DOC')[1] <> 6
	dbSelectArea("SX1")
	dbSetOrder(1)
	If dbSeek(PADR(_cPerg,nTamSX1)+"02") .And. X1_TAMANHO <> nTamNF
		RecLock("SX1",.F.)
		SX1->X1_TAMANHO := nTamNF
		MsUnlock()
    Endif
	If dbSeek(PADR(_cPerg,nTamSX1)+"03") .And. X1_TAMANHO <> nTamNF
		RecLock("SX1",.F.)
		SX1->X1_TAMANHO := nTamNF
		MsUnlock()
    Endif
EndIf

dbSelectArea("SF2")
dbSetOrder(1)
DBGOBOTTOM()
mBrowse( 6,1,22,75,"SF2")

dbSelectArea("SF2")
dbSetOrder(1)
Return Nil


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � TDCEA01A � Autor �Assist.de Conversao do AP5 IDE �21/01/01 ���
�������������������������������������������������������������������������͹��
���Descri��o � Digitacao e gravacao de Hora de Saida da Nota Fiscal       ���
���          � 		                                                      ���
�������������������������������������������������������������������������͹��
���Sintaxe   � TDCEA01A()	                                              ���
�������������������������������������������������������������������������͹��
���Parametros� Nenhum			                                          ���
�������������������������������������������������������������������������͹��
���Retorno   � Nenhum			                                          ���
�������������������������������������������������������������������������͹��
���Uso       � TDCEA002 - Template DCL/Estoque                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Template Function TDCEA01A()        // incluido pelo assistente de conversao do AP5 IDE em 21/01/01
Local nTamSX1   := Len(SX1->X1_GRUPO)
//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_VALIDA,_TAN,_PRODUTO,")

_Valida:= .T.
dbselectarea("SX1")
dbgotop()
IF DBSEEK(PADR(_cPerg,nTamSX1)+"02")
	RECLOCK("SX1",.F.)
	SX1->X1_CNT01:=SF2->F2_DOC
	DBUNLOCK()
	
	DBSKIP()
	RECLOCK("SX1",.F.)
	SX1->X1_CNT01:=SF2->F2_DOC
	DBUNLOCK()
	DBSKIP()
	
	RECLOCK("SX1",.F.)
	SX1->X1_CNT01:=DTOC(DDATABASE)
	DBUNLOCK()
	DBSKIP()
	
	RECLOCK("SX1",.F.)
	SX1->X1_CNT01:=TIME()
	DBUNLOCK()
ENDIF

IF ! PERGUNTE(_CPERG,.T.)
ELSE
	IF MV_PAR05 <= GETMV("MV_ULMES") .AND. ! EMPTY(MV_PAR05)
		MsgBox("Data Informada Anterior ao Ultimo Fechamento","Data Invalida","Info")
		RETURN
	ENDIF
	
	IF SUBSTR(MV_PAR06,3,1)==":" .AND. VAL(SUBSTR(MV_PAR06,1,2)) <=24 .AND. VAL(SUBSTR(MV_PAR06,1,2))>0 ;
		.AND. VAL(SUBSTR(MV_PAR06,4,2)) <=59 .AND. VAL(SUBSTR(MV_PAR06,4,2))>=0 ;
		.AND. ! EMPTY(SUBSTR(MV_PAR06,1,1)) ;
		.AND. ! EMPTY(SUBSTR(MV_PAR06,2,1)) ;
		.AND. ! EMPTY(SUBSTR(MV_PAR06,4,1)) ;
		.AND. ! EMPTY(SUBSTR(MV_PAR06,5,1))
		DBSELECTAREA("SF2")
		DBSETORDER(1)
		IF DBSEEK(XFILIAL("SF2")+MV_PAR02+MV_PAR01)
			WHILE SF2->F2_FILIAL == xFilial("SF2") .And. ;
			  SF2->F2_DOC <= MV_PAR03 .AND. SF2->F2_SERIE == MV_PAR01 .AND. !EOF()
				RECLOCK("SF2",.F.)
				IF SF2->F2_EMISSAO <= GETMV("MV_ULMES")
					MsgBox("NF: "+SF2->F2_DOC+"/"+SF2->F2_SERIE+" Anterior ao Fechamento: ","Data Invalida","Info")
					MSUNLOCK()
					DBSKIP()
					LOOP
                ELSEIF SF2->F2_EMISSAO <> MV_PAR04
                    DBSKIP()
                    LOOP
				ELSE
					IF MV_PAR07==1
						SF2->F2_X_HORA:=IIF(EMPTY(SF2->F2_X_HORA),MV_PAR06,SF2->F2_X_HORA)
					ELSE
						SF2->F2_X_HORA:=MV_PAR06
					ENDIF
				ENDIF
				
				IF MV_PAR04 < SF2->F2_EMISSAO
					MsgBox(" Data de Emissao da Nota: "+SF2->F2_DOC+" Maior que a Data Informada!!! VERIFICAR ","Atencao","Info")
					//SF2->F2_X_HORA:=SPACE(5)
					_Valida:= .F.
				ELSE
					SF2->F2_X_DATA:=SF2->F2_EMISSAO
					IF MV_PAR07==1
						SF2->F2_X_DATA:=IIF(EMPTY(SF2->F2_X_DATA),MV_PAR04,SF2->F2_X_DATA)
					ELSE
						SF2->F2_X_DATA:=MV_PAR04
					ENDIF
				ENDIF
				MSUNLOCK()
				DBSKIP()
			ENDDO
		ELSE
			MsgBox('Nota Nao Localizada ',"Atencao","Info")
		ENDIF
	ELSE
		DBSELECTAREA("SF2")
		DBSETORDER(1)
		IF EMPTY(MV_PAR06) .AND. DBSEEK(XFILIAL("SF2")+MV_PAR02+MV_PAR01)
			IF SF2->F2_EMISSAO <= GETMV("MV_ULMES")
				MsgBox("NF: "+SF2->F2_DOC+"/"+SF2->F2_SERIE+" Anterior ao Fechamento: ","Data Invalida","Info")
			ELSE
				MsgBox(" Baixa Estornada -> Nota Fiscal: "+SF2->F2_DOC+"/"+SF2->F2_SERIE,"Atencao","Info")
				RECLOCK("SF2",.F.)
				SF2->F2_X_DATA:=CTOD("  /  /  ")
				SF2->F2_X_HORA:=SPACE(5)
				MSUNLOCK()
			ENDIF
		ELSE
			MsgBox(" Hora Informada INVALIDA !!! -> Informar  HH:MM ","Atencao","Info")
		ENDIF
	ENDIF
ENDIF
DBSEEK(XFILIAL("SF2")+MV_PAR02+MV_PAR01)
Return(NIL)

