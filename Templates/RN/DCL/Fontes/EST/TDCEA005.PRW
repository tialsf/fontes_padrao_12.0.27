#Include "TOPCONN.CH"
#Include "rwmake.ch"
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �TDCEA005  � Autor � Antonio Cordeiro   � Data � JUNHO/2002  ���
�������������������������������������������������������������������������͹��
���Descri��o �Cadastro de Tanques                                         ���
�������������������������������������������������������������������������͹��
���Uso       �Template/Estoque                                            ���
�������������������������������������������������������������������������͹��
���Arquivos  � LB2 - CADASTRO DE TANQUES                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Template Function TDCEA005()

Local _cAlias := Alias()
Local _cIndex := IndexOrd()
Local _nRecno := Recno()
Local nReceb  := 0
Local nSaida  := 0
Local nAbertu := 0
Local cFilOri := cFilAnt


CHKTEMPLATE("DCLEST")

DBSELECTAREA("LB2")
DBGOTOP()
WHILE ! EOF()
	
	cFilAnt := LB2->LB2_FILIAL
	DBSELECTAREA("SD3")
	DBSETORDER(6)
	DBSEEK(xFilial("SD3")+DTOS(DDATABASE),.T.)
	WHILE SD3->D3_EMISSAO == DDATABASE .AND. ! EOF()
		IF SD3->D3_ESTORNO=="S"
			DbSkip()
			Loop
		Endif
		SB1->(DBSEEK(xFilial("SB1")+SD3->D3_COD))
      IF ! EMPTY(SD3->D3_X_TMT) .AND. SD3->D3_X_TQ == LB2->LB2_TANQUE
			
			IF VAL(SD3->D3_TM) <=500
				
				IF ALLTRIM(SD3->D3_X_TMT)=="TR"
					nReceb:=nReceb + SD3->D3_QUANT
				ELSEIF SD3->D3_X_TMT $ "PR"
					nReceb:=nReceb + SD3->D3_QUANT
				ELSE
					DBSELECTAREA("SD3")
					DbSkip()
					Loop
				ENDIF
			ELSE
				IF ALLTRIM(SD3->D3_X_TMT)=="TR"
					nSaida:=nSaida + SD3->D3_QUANT
				ELSEIF SD3->D3_X_TMT $ "PR"
					nSaida:=nSaida + SD3->D3_QUANT
				ELSE
					DBSELECTAREA("SD3")
					DbSkip()
					Loop
				ENDIF
			ENDIF
		ELSE
			DBSELECTAREA("SD3")
			DbSkip()
			Loop
		ENDIF
		
		DBSELECTAREA("SD3")
		DBSKIP()
	ENDDO
	
	
	DBSELECTAREA("SD1")
	DBORDERNICKNAME("DCLSD1_01")
	DBSEEK(xFilial("SD1")+DTOS(DDATABASE),.T.)
	WHILE SD1->D1_X_ENT  == DDATABASE .AND. ! EOF() .AND. SD1->D1_FILIAL == xFilial("SD1")
		IF SD1->D1_X_TQ == LB2->LB2_TANQUE
			nReceb  :=nReceb + SD1->D1_X_QTD1
		ENDIF
		DBSELECTAREA("SD1")
		DBSKIP()
	ENDDO
	//��������������������������������������������������������������Ŀ
	//� Fim da apuracao das notas fiscais de Entrada                 �
	//����������������������������������������������������������������
	
	
	
	//��������������������������������������������������������������Ŀ
	//� Inicio da Apuracao das notas fiscais de saidas               �
	//����������������������������������������������������������������
	
	IF SUBSTR(RDDNAME(),1,6) <> 'DBFCDX'
		
		If Select("TMP3") > 0     // Verificando se o alias esta em uso
			dbSelectArea("TMP3")
			dbCloseArea()
		EndIf
		
		cQuery := "SELECT SF2.F2_X_DATA DATA1,SD2.D2_X_TQ1 TQ1,SD2.D2_X_TQ2 TQ2,SD2.D2_X_TQ3 TQ3,SD2.D2_COD COD, SUM(SD2.D2_X_QTD1) QTD1, SUM(SD2.D2_X_QTD2) QTD2, SUM(SD2.D2_X_QTD3) QTD3"
		cQuery := cQuery + " FROM "+RetSqlName("SF2")+" SF2, "+RetSqlName("SD2")+" SD2 "
		cQuery := cQuery + " WHERE SF2.F2_X_DATA >= '"+DTOS(DDATABASE)+"' AND "
		cQuery := cQuery + "       SF2.F2_X_DATA <= '"+DTOS(DDATABASE)+"' AND "
		cQuery := cQuery + "       SF2.F2_FILIAL  = '"+xFilial("SF2")+"' AND "
		cQuery := cQuery + "       SF2.F2_DOC      = SD2.D2_DOC AND "
		cQuery := cQuery + "       SF2.F2_SERIE    = SD2.D2_SERIE AND "
		cQuery := cQuery + "       SF2.F2_FILIAL   = SD2.D2_FILIAL AND "
		cQuery := cQuery + "       SF2.D_E_L_E_T_  <> '*' AND "
		cQuery := cQuery + "       SD2.D_E_L_E_T_  <> '*' "
		cQuery := cQuery + " GROUP BY SF2.F2_X_DATA,SD2.D2_X_TQ1,SD2.D2_X_TQ2,SD2.D2_X_TQ3,SD2.D2_COD"
		MEMOWRIT("TMP3.SQL",cQuery)
		TCQUERY cQuery NEW ALIAS "TMP3"
		
		DBGOTOP()
		WHILE ! EOF()
			SB1->(DBSEEK(xFilial("SB1")+TMP3->COD))
			IF SB1->B1_X_REQUI <> "S"
				IF TMP3->TQ1 == LB2->LB2_TANQUE
					nSaida:=nSaida+TMP3->QTD1
				ENDIF
			ELSE
				DBSELECTAREA("SG1")
				DBORDERNICKNAME("DCLSG1_01")
				DBSEEK(xFilial("SG1")+TMP3->COD+TMP3->DATA1,.T.)
				dFim:=SG1->G1_FIM
				WHILE TMP3->COD == SG1->G1_COD .AND. ! EOF() .AND. dFim == SG1->G1_FIM
					_SEQ:=SUBSTR(SG1->G1_TRT,3,1)
					_Tanque:="TMP3->TQ"+_SEQ
					_QTD:="TMP3->QTD"+_SEQ
					IF &(_Tanque) == LB2->LB2_TANQUE
						nSaida:=nSaida+&(_QTD)
					ENDIF
					DBSELECTAREA("SG1")
					DBSKIP()
				ENDDO
			ENDIF
			DBSELECTAREA("TMP3")
			DBSKIP()
		ENDDO
		//��������������������������������������������������������������Ŀ
		//� Fim da Apuracao das notas fiscais de saidas                  �
		//����������������������������������������������������������������
		
		dbSelectArea("TMP3")
		dbCloseArea()
		
	ELSE
		
		//��������������������������������������������������������������Ŀ
		//� Inicio da apuracao das Notas Fiscais de Saida                �
		//����������������������������������������������������������������
		DBSELECTAREA("SF2")
		DBORDERNICKNAME("DCLSF2_01")
		DBSEEK(xFilial("SF2")+DTOS(DDATABASE),.T.)
		WHILE SF2->F2_X_DATA <= DDATABASE .AND. ! EOF() .AND. SF2->F2_FILIAL == xFilial("SF2")
			DBSELECTAREA("SD2")
			DBSETORDER(3)
			IF DBSEEK(SF2->F2_FILIAL+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA)
				WHILE SF2->F2_FILIAL == SD2->D2_FILIAL .AND.;
					SF2->F2_DOC    == SD2->D2_DOC    .AND.;
					SF2->F2_SERIE  == SD2->D2_SERIE  .AND.;
					SF2->F2_CLIENTE== SD2->D2_CLIENTE.AND.;
					SF2->F2_LOJA   == SD2->D2_LOJA
					SB1->(DBSEEK(xFilial("SB1")+SD2->D2_COD))

					IF SB1->B1_X_REQUI <> "S"
						If SD2->D2_X_TQ1 == LB2->LB2_TANQUE
							nSaida:=nSaida + SD2->D2_X_QTD1
						ENDIF
					ELSE
						DBSELECTAREA("SG1")
						DBORDERNICKNAME("DCLSG1_01")
						DBSEEK(xFilial("SG1")+SD2->D2_COD+DTOS(SF2->F2_X_DATA),.T.)
						dFim:=SG1->G1_FIM
						WHILE SD2->D2_COD == SG1->G1_COD .AND. ! EOF() .AND. dFim == SG1->G1_FIM
							_SEQ:=SUBSTR(SG1->G1_TRT,3,1)
							_Tanque:="SD2->D2_X_TQ"+_SEQ
							_QTD:="SD2->D2_X_QTD"+_SEQ
							IF LB2->LB2_TANQUE == &(_Tanque)
								nSaida := nSaida + &(_QTD)
							ENDIF
							DBSELECTAREA("SG1")
							DBSKIP()
						ENDDO
					ENDIF
					DBSELECTAREA("SD2")
					DBSKIP()
				ENDDO
			ENDIF
			DBSELECTAREA("SF2")
			DBSKIP()
		ENDDO
		
	ENDIF
	DBSELECTAREA("LB3")
	DBSETORDER(1)
	IF DBSEEK(xFilial("LB3")+DTOS(DDATABASE)+LB2->LB2_COD+LB2->LB2_TANQUE)
		nAbertu:=LB3->LB3_VOL20
	ELSE
		nAbertu:=0
	ENDIF
	DBSELECTAREA("LB2")
	RECLOCK("LB2",.F.)
	LB2->LB2_ABERTU:=nAbertu
	LB2->LB2_RECEB :=nReceb
	LB2->LB2_SAIDA :=nSaida
	LB2->LB2_DISPO :=LB2->LB2_CAPTOT - nAbertu - nReceb + nSaida
	MSUNLOCK()
	nSaida:=0
	nReceb:=0
	nAbertu:=0
	DBSKIP()
ENDDO

cFilAnt := cFilOri
dbSelectArea(_cAlias)
dbSetOrder(_cIndex)
dbGoTo(_nRecno)
AxCadastro("LB2","CADASTRO DE TANQUE",".T.",".T.")

Return(nil)

