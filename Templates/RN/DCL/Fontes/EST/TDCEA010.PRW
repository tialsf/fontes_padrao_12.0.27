#Include "rwmake.ch"

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TDCEA010  � Autor � Materiais             � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Manutencao do Cadastro de Bombeio                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Template Function TDCEA010()

Local   cPoLocal	:= ""
Local   cPoQtd		:= ""
Local _aAreaSX3		:= SX3->(GetArea())

Private nOpca := 0

CHKTEMPLATE("DCLEST")

Private lRateio		:= SuperGetMV("MV_XPSBOMB",.F., .F.)
Private nTotCampos	:= 1
Private cCadastro 	:= "Manutencao no Cadastro de Recebimento Antecipado"
Private aRotina   	:= {{ "Pesquisar"   ,"AxPesqui",0,1},;
						{ "Visualizar"  ,"T_DCEA10_VISUAL()",0,2},;
						{ "Incluir"     ,"T_DCEA10_INCLUI()",0,3},;
						{ "Alterar"     ,"T_DCEA10_ALTERA()",0,4},;
						{ "Excluir"     ,"T_DCEA10_DELETA()",0,5},;
						{ "Estornar"	,"T_DCEA10_ESTORNA()",0,6},;
						{ "Legenda"		,"T_DCEA10_Legenda()",0,8}}

Private aCor		:= {{"LBZ->LBZ_VO20GF==0 .AND. LBZ->LBZ_DATA  > DDATABASE","BR_VERDE"},;
						{"LBZ->LBZ_VO20GF==0 .AND. LBZ->LBZ_DATA <= DDATABASE","BR_AMARELO"},;
						{"LBZ->LBZ_VO20GF<>0 "                                ,"BR_VERMELHO"}}

//����������������������������������Ŀ
//� Ajustes de Dicionario            �
//������������������������������������
AjustaSX3()
AjustaSX6()
AjusteSX7()
AjusteSXB()

SX3->(dbSetOrder(2))

cPoLocal	:= "LBZ_LOCA01"
cPoQTd 		:= "LBZ_QTLO01"

While SX3->(dbSeek(cPoLocal)) .And. SX3->(dbSeek(cPoQtd))
	nTotCampos	:= nTotCampos + 1
	cPoLocal	:= "LBZ_LOCA" + STRZERO(nTotCampos,2)
	cPoQTd  	:= "LBZ_QTLO" + STRZERO(nTotCampos,2)
EndDo

SX3->(RestArea(_aAreaSX3))
nTotCampos := nTotCampos-1

dbselectarea("LBZ")
mBrowse(,,,,"LBZ",,,,,,aCor,,)

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �DCEA10_VISUAL � Autor � Materiais         � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Visualizacao da Digitacao de Bombeio                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Template Function DCEA10_VISUAL()

Local aButtons		:= {}

AAdd(aButtons,{'Distribuir Qtd.',{|| T_CEA010_M2() },'Distribuir Qtd.'}) //"Opcionais Default"
nOpca := AxVisual("LBZ",LBZ->(RECNO()),2,,,,,aButtons )

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �DCEA10_Legenda� Autor � Materiais         � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Legendas da Digitacao de Bombeio                           ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Template Function DCEA10_Legenda()

Local _aLegenda		:= {}

aAdd(_aLegenda,{ 'BR_VERDE'   ,"Programado" })
aAdd(_aLegenda,{ 'BR_AMARELO' ,"Pendente" })
aAdd(_aLegenda,{ 'BR_VERMELHO',"Encerrado" })
BrwLegenda(cCadastro,'Legenda',_aLegenda)

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �DCEA10_INCLUI� Autor � Materiais          � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Inclusao da Digitacao de Bombeio                           ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Template Function DCEA10_INCLUI()

Local aButtons		:= {}
Local I1			:= 0

AAdd(aButtons,{'Distribuir Qtd.',{|| T_CEA010_M2() },'Distribuir Qtd.'})

IF DDATABASE <= GETMV("MV_ULMES")
	MsgBox("Fechamento de estoque ja Promovido para esta Data","Atencao","Info")
ELSE
	SF5->(dbOrderNickName("DCLSF5_01"))
	IF SF5->(msSeek(xFilial("SF5")+"PR"+"D"))
		//��������������������������������������Ŀ
		//� Inclusao da Digitacao de Bombeio     �
		//����������������������������������������
		Begin Transaction
			nOpca := AxInclui("LBZ",LBZ->(RECNO()),3,,,,"T_DCEA10TudOk()",,,aButtons)
			If nOpca == 1
				T_DCEA10_Prov()
			EndIf
		End Transaction
	Else
		MsgBox("Tipo de Movimento Provisorio (PR) N�o Cadastrado.","Atenc�o","Info")
	EndIF
EndIf

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �DCEA10_ALTERA� Autor � Materiais          � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Alteracao da Digitacao de Bombeio                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Template Function DCEA10_ALTERA()

Local aButtons		:= {}
Local I1			:= 0

AAdd(aButtons,{'Distribuir Qtd.',{|| T_CEA010_M2() },'Distribuir Qtd.'})

If LBZ->LBZ_DATA <= GETMV("MV_ULMES")
	MsgBox("Fechamento de estoque ja Promovido para esta Data","Atencao","Info")
Else
	If LBZ->LBZ_VO20GF <> 0
		MsgBox("Rec.Antecipado ja Encerrado nao Pode ser Alterado","Atencao","Info")
	Else
		SF5->(dbOrderNickName("DCLSF5_01"))
		If SF5->(msSeek(xFilial("SF5")+"PR"+"D"))
			//��������������������������������������Ŀ
			//� Alteracao da Digitacao de Bombeio    �
			//����������������������������������������
			Begin Transaction
				nOpca := AxAltera("LBZ",LBZ->(RECNO()),4,,,,,"T_DCEA10TudOk()",,,aButtons )
				If nOpca == 1
					T_DCEA10_Prov()
				EndIf
			End Transaction
		Else
			MsgBox("Tipo de Movimento Provisorio (PR) N�o Cadastrado.","Aten��o","Info")
		EndIf
	EndIf
EndIf

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � DCEA10_Prov � Autor � Materiais          � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Geracao dos Movimentos Provisorios                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Template Function DCEA10_Prov()

Local I1			:= 0
Local nDifer		:= 0
Local nMediaPond	:= 0
Local nQTLOC		:= 0
Local cTMD			:= ""
Local cTMR			:= ""
Local cTMT			:= ""

Private lMsErroAuto	:= .F.

//��������������������������������������Ŀ
//� Seleciona o TM para a geracao do PR  �
//����������������������������������������
IF SF5->(msSeek(xFilial("SF5")+"PR"+"D"))
	cTMD := SF5->F5_CODIGO
	cTMT := SF5->F5_X_TMT
	SF5->(dbSetOrder(1))
EndIf

If LBZ->LBZ_VO20GF <> 0
	//��������������������������������������������������Ŀ
	//� Gera os Movimentos Provisorios de Entrada - PR   �
	//����������������������������������������������������
	For I1 := 1 To nTotCampos
		cLocal := "LBZ->LBZ_LOCA"+STRZERO(I1,2)
		nQTLOC := "LBZ->LBZ_QTLO"+STRZERO(I1,2)
		If &(nQTLOC)<> 0 .AND. ! EMPTY(&(cLocal))
			MsgBox("O Recebimento Antecipado gerar� os Movimentos Internos Provis�rios de Estoque.","Aten��o","Info")
			
			aVetor:= {	{"D3_TM"		,cTMD										,NIL},;
						{"D3_COD"		,LBZ->LBZ_COD								,NIL},;
						{"D3_QUANT"		,&(nQTLOC)									,NIL},;
						{"D3_LOCAL"		,&(cLocal)									,NIL},;
						{"D3_DOC"  		,LBZ->LBZ_CODIGO							,NIL},;
						{"D3_EMISSAO"	,LBZ->LBZ_DATA								,NIL},;
						{"D3_X_TQ"		,LBZ->LBZ_TANQUE							,NIL},;
						{"D3_X_NF"		,LBZ->LBZ_CODIGO+&(cLocal)					,NIL},;
						{"D3_X_CLI"		,LBZ->LBZ_FORNEC							,NIL},;
						{"D3_X_LOJA"	,LBZ->LBZ_LOJA								,NIL},;
						{"D3_X_TMT"		,cTMT										,NIL},;
						{"D3_OBS"		,LBZ->LBZ_OBS								,NIL}}
			
			MSExecAuto({|x,y| MATA240(x,y)},aVetor,3) //Inclusao
			
			If lMsErroAuto
				DisarmTransaction()
				MostraErro()
				Return()
			Else
				Reclock("SD3",.F.)
				SD3->D3_T_TEMPT	:= LBZ->LBZ_TEMPTF
				SD3->D3_T_TEMPA	:= LBZ->LBZ_TEMPF
				SD3->D3_T_DENSA	:= LBZ->LBZ_DENSF
				SD3->D3_T_FATCO	:= Iif(LBZ->LBZ_FATCOF == 0,1,LBZ->LBZ_FATCOF)
				MSUnlock()
			EndIf
		EndIf
	Next
	
	//�������������������������������������������������������������������Ŀ
	//� Verifica se existe diferenca no Volume 20G para gerar Perda/Ganho �
	//���������������������������������������������������������������������
	nDifer := (LBZ->LBZ_VO20GF - LBZ->LBZ_VO20GI) - LBZ->LBZ_QTDSOL
	
	If nDifer <> 0
		//�������������������������������������������������������������Ŀ
		//� Seleciona os TM's para geracao da Perda e Ganho - BO ou DE  �
		//���������������������������������������������������������������
		SF5->(dbOrderNickName("DCLSF5_01"))
		//����������������������������������������������������������������Ŀ
		//� Cancela transacao se nao encontrar os TM's para Perda e Ganho  �
		//������������������������������������������������������������������
		If SF5->(msSeek(xFilial("SF5")+LBZ->LBZ_TIPO+"R"))
			cTMR := SF5->F5_CODIGO
		Else
			MsgBox("Tipo de Movimento Provis�rio ("+ Alltrim(LBZ->LBZ_TIPO) +") N�o Cadastrado.","Aten��o","Info")
			DisarmTransaction()
			Return()
		EndIf
		If SF5->(msSeek(xFilial("SF5")+LBZ->LBZ_TIPO+"D"))
			cTMD := SF5->F5_CODIGO
		Else
			MsgBox("Tipo de Movimento Provis�rio ("+ Alltrim(LBZ->LBZ_TIPO) +") N�o Cadastrado.","Aten��o","Info")
			DisarmTransaction()
			Return()
		EndIf
		SF5->(dbSetOrder(1))
		lMsErroAuto := .F.
		dbSelectArea("SD3")
		
		//�����������������������������������������������������������Ŀ
		//� Gera os Movimentos de Perda/Ganho com Rateio - BO ou DE   �
		//�������������������������������������������������������������
		If lRateio
			For I1 := 1 To nTotCampos
				cLocal := "LBZ->LBZ_LOCA"+STRZERO(I1,2)
				nQTLOC := "LBZ->LBZ_QTLO"+STRZERO(I1,2)
				If &(nQTLOC)<> 0 .AND. ! Empty(&(cLocal))
					
					nPercen := &(nQTLOC) / LBZ->LBZ_QTDSOL
					nMediaPond := ABS(nDifer * nPercen)
					
					aVetor := {	{"D3_TM"		,Iif(nDifer > 0,cTMD,cTMR)					,NIL},;
								{"D3_COD"		,LBZ->LBZ_COD								,NIL},;
								{"D3_QUANT"		,nMediaPond									,Nil},;
								{"D3_LOCAL"		,&(cLocal)									,Nil},;
								{"D3_X_CLI"		,LBZ->LBZ_FORNEC							,NIL},;
								{"D3_X_LOJA"	,LBZ->LBZ_LOJA								,NIL},;
								{"D3_X_NF"		,LBZ->LBZ_CODIGO+"99"						,NIL},;
								{"D3_X_TQ"		,LBZ->LBZ_TANQUE							,NIL},;
								{"D3_X_TMT"		,LBZ->LBZ_TIPO								,NIL},;
								{"D3_DOC"		,LBZ->LBZ_CODIGO							,NIL},;
								{"D3_OBS"		,LBZ->LBZ_OBS								,NIL},;
								{"D3_EMISSAO"	,LBZ->LBZ_DATA								,NIL}}
					
					MSExecAuto({|x,y| MATA240(x,y)},aVetor,3) //Inclusao
					
					IF lMsErroAuto
						DisarmTransaction()
						MostraErro()
						Return()
					Else
						Reclock("SD3",.F.)
						SD3->D3_T_FATCO	:= Iif(LBZ->LBZ_FATCOF == 0,1,LBZ->LBZ_FATCOF)
						MSUnlock()
					Endif
				Endif
			Next
		//�����������������������������������������������������������Ŀ
		//� Gera os Movimentos de Perda/Ganho sem Rateio - BO ou DE   �
		//�������������������������������������������������������������
		Else
			aVetor := {	{"D3_TM"		,IIF(nDifer > 0,cTMD,cTMR)					,NIL},;
						{"D3_COD"		,LBZ->LBZ_COD								,NIL},;
						{"D3_QUANT"		,Iif(nDifer < 0,(nDifer * -1),nDifer)		,NIL},;
						{"D3_LOCAL"		,GETMV("MV_X_LBASE")						,NIL},;
						{"D3_X_CLI"		,LBZ->LBZ_FORNEC							,NIL},;
						{"D3_X_LOJA"	,LBZ->LBZ_LOJA								,NIL},;
						{"D3_X_NF"		,LBZ->LBZ_CODIGO+"99"						,NIL},;
						{"D3_X_TQ"		,LBZ->LBZ_TANQUE							,NIL},;
						{"D3_DOC"		,LBZ->LBZ_CODIGO							,NIL},;
						{"D3_X_TMT" 	,LBZ->LBZ_TIPO								,NIL},;
						{"D3_OBS"		,LBZ->LBZ_OBS								,NIL},;
						{"D3_EMISSAO"	,LBZ->LBZ_DATA								,NIL}}
			
			MSExecAuto({|x,y| MATA240(x,y)},aVetor,3) //Inclusao
		EndIf
		
		If lMsErroAuto
			DisarmTransaction()
			MostraErro()
			Return()
		Else
			Reclock("SD3",.F.)
			SD3->D3_T_FATCO	:= Iif(LBZ->LBZ_FATCOF == 0,1,LBZ->LBZ_FATCOF)
			MSUnlock()
		EndIf
	EndIf
EndIf

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �DCEA10_DELETA� Autor � Materiais          � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Geracao dos Movimentos Provisorios                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Template Function DCEA10_DELETA()

Local nI			:= 0
Local nTamNF		:= TamSX3('D3_X_NF')[1]
Local _cLocais		:= ""

Begin Transaction
	If LBZ->LBZ_DATA <= GETMV("MV_ULMES")
		MsgBox("Fechamento de estoque ja Promovido para esta Data","Atencao","Info")
	Else
		If LBZ->LBZ_VO20GF <> 0
			//����������������������������������������������������������Ŀ
			//� Valida se os Movimentos Provisorios podem ser Estornados �
			//������������������������������������������������������������
			lEstorno := .F.
			For nI := 1 TO nTotCampos
				cLocal := "LBZ->LBZ_LOCA"+STRZERO(nI,2)
				nQTLOC := "LBZ->LBZ_QTLO"+STRZERO(nI,2)
				SD3->(dbOrderNickName("DCLSD3_01"))
				If &(nQTLOC)<> 0 .AND. ! EMPTY(&(cLocal))
					If ! SD3->(msSeek(xFilial("SD3")+PadR(LBZ->LBZ_CODIGO+&(cLocal),nTamNF)+"PR"+" "))
						_cLocais += &(cLocal)+'/'
						lEstorno := .T.
					EndIf
				EndIf
			Next nI
			//���������������������������������������������������������Ŀ
			//� Inicia o processo de Estorno dos Movimentos Provisorios �
			//�����������������������������������������������������������
			If !lEstorno
				nOpca := AxDeleta("LBZ",LBZ->(RECNO()),5)
				If nOpca == 2
					//�����������������������������������������������������Ŀ
					//� Estorna os Movimentos Provisorios de Entrada - PR   �
					//�������������������������������������������������������
					FOR nI := 1 TO nTotCampos
						cLocal:="LBZ->LBZ_LOCA"+STRZERO(nI,2)
						nQTLOC:="LBZ->LBZ_QTLO"+STRZERO(nI,2)
						If &(nQTLOC)<> 0 .AND. !Empty(&(cLocal))
							SD3->(dbOrderNickName("DCLSD3_01"))
							If SD3->(msSeek(xFilial("SD3")+PadR(LBZ->LBZ_CODIGO+&(cLocal),nTamNF)+"PR"+" "))
								lMsErroAuto := .F.
								
								aVetor := {	{"D3_NUMSEQ"	,SD3->D3_NUMSEQ		,NIL},;
											{"D3_CHAVE"		,SD3->D3_CHAVE		,NIL},;
											{"D3_COD"		,SD3->D3_COD		,NIL},;
											{"INDEX"		,4					,NIL}}
											
								MSExecAuto({|x,y| MATA240(x,y)},aVetor,5) // Estorno
								If lMsErroAuto
									MostraErro()
									DisarmTransaction()
									Return()
								EndIf
						    EndIf
						EndIf
					Next nI
					//��������������������������������������������������������������Ŀ
					//� Estorna os Movimentos de Perda/Ganho sem Rateio - BO ou DE   �
					//����������������������������������������������������������������
					SD3->(dbOrderNickName("DCLSD3_01"))
					While SD3->(msSeek(xFilial("SD3")+PadR(LBZ->LBZ_CODIGO+"99",nTamNF)+LBZ->LBZ_TIPO+" "))
						lMsErroAuto := .F.
						
						aVetor := {	{"D3_NUMSEQ"	,SD3->D3_NUMSEQ		,NIL},;
									{"D3_CHAVE"		,SD3->D3_CHAVE		,NIL},;
									{"D3_COD"		,SD3->D3_COD		,NIL},;
									{"INDEX"		,4					,NIL}}
									
						MSExecAuto({|x,y| MATA240(x,y)},aVetor,5) // Estorno
						If lMsErroAuto
							MostraErro()
							DisarmTransaction()
							Return()
						EndIf						
						SD3->(DBSKIP())
					EndDo
				EndIf
			Else
				MsgBox("Nota fiscal j� informada para os locais: "+_cLocais+;
				". O registro s� poder� ser exclu�do se n�o houver nenhuma nota fiscal associada.","Aten��o","Info")
			EndIf
		Else
			AxDeleta("LBZ",LBZ->(RECNO()),5)
		EndIf
	EndIf
End Transaction

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �DCEA10_ESTORNA� Autor � Materiais         � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Estorna os Mov. Provisorios da Digitacao de Bombeio        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Template Function DCEA10_ESTORNA()

Local nI			:= 0
Local nTamNF		:= TamSX3('D3_X_NF')[1]
Local cLocal		:= " " 
Local _cLocais		:= "" 
Local lEstorno		:= .F.
Local aAreaLBZ		:= LBZ->(GetArea())

Begin Transaction
	If LBZ->LBZ_DATA <= GETMV("MV_ULMES")
		MsgBox("O Fechamento de Estoque j� foi realizado para esta Data.","Aten��o","Info")
	Else
		If LBZ->LBZ_VO20GF <> 0
			//����������������������������������������������������������Ŀ
			//� Valida se os Movimentos Provisorios podem ser Estornados �
			//������������������������������������������������������������
			For nI := 1 To nTotCampos
				cLocal:="LBZ->LBZ_LOCA"+STRZERO(nI,2)
				nQTLOC:="LBZ->LBZ_QTLO"+STRZERO(nI,2)
				SD3->(dbOrderNickName("DCLSD3_01"))
				If &(nQTLOC)<> 0 .And. ! EMPTY(&(cLocal))
					If ! SD3->(msSeek(xFilial("SD3")+PadR(LBZ->LBZ_CODIGO+&(cLocal),nTamNF)+"PR"+" "))
						lEstorno := .T.
						_cLocais += &(cLocal)+'/'
						Exit
					EndIf
				EndIf
			Next nI
			//���������������������������������������������������������Ŀ
			//� Inicia o processo de Estorno dos Movimentos Provisorios �
			//�����������������������������������������������������������
			If !lEstorno
				If MsgYesNo("Deseja estornar a apura��o do Recebimento Antecipado?")
					//�����������������������������������������������������Ŀ
					//� Estorna os Movimentos Provisorios de Entrada - PR   �
					//�������������������������������������������������������
					For nI := 1 To nTotCampos
						cLocal := "LBZ->LBZ_LOCA"+STRZERO(nI,2)
						nQTLOC := "LBZ->LBZ_QTLO"+STRZERO(nI,2)
						If &(nQTLOC)<> 0 .And. ! EMPTY(&(cLocal))
							SD3->(dbOrderNickName("DCLSD3_01"))
							If SD3->(msSeek(xFilial("SD3")+PadR(LBZ->LBZ_CODIGO+&(cLocal),nTamNF)+"PR"+" "))
								lMsErroAuto := .F.
								
								aVetor := {	{"D3_NUMSEQ"	,SD3->D3_NUMSEQ		,NIL},;
											{"D3_CHAVE"		,SD3->D3_CHAVE		,NIL},;
											{"D3_COD"		,SD3->D3_COD		,NIL},;
							           	 	{"INDEX"		,4					,NIL}}
							           	 	
								MSExecAuto({|x,y| MATA240(x,y)},aVetor,5) // Estorno
								If lMsErroAuto
									MostraErro()
									DisarmTransaction()
									Return()
								EndIf	
							EndIf
						EndIf
					Next nI
					//��������������������������������������������������������������Ŀ
					//� Estorna os Movimentos de Perda/Ganho sem Rateio - BO ou DE   �
					//����������������������������������������������������������������
					While SD3->(msSeek(xFilial("SD3")+PadR(LBZ->LBZ_CODIGO+"99",nTamNF)+LBZ->LBZ_TIPO+" "))
						lMsErroAuto := .F.
						
						aVetor := {	{"D3_NUMSEQ"	,SD3->D3_NUMSEQ		,NIL},;
									{"D3_CHAVE"		,SD3->D3_CHAVE		,NIL},;
									{"D3_COD"		,SD3->D3_COD		,NIL},;
						          	{"INDEX"		,4					,NIL}}
						          	
						MSExecAuto({|x,y| MATA240(x,y)},aVetor,5) // Estorno
						If lMsErroAuto
							MostraErro()
							DisarmTransaction()
							Return()
						EndIf
						SD3->(DbSkip())
					EndDo
					//���������������������������Ŀ
					//� Ajusta Volume Final 20G   �
					//�����������������������������
					DbSelectArea("LBZ")
					Reclock("LBZ",.F.)
					LBZ->LBZ_VO20GF := 0
					MsUnlock()
				EndIf
			Else
				MsgBox("Nota Fiscal j� informada para os locais: "+_cLocais+". O registro n�o pode ser estornado.","Aten��o","Info")
			EndIf
		Else
			MsgBox("S� � permitido estorno de Recebimentos Antecipados j� encerrados.","Aten��o","Info")
		EndIf
	EndIf
End Transaction

RestArea(aAreaLBZ)

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � DCEA10TUDOk  � Autor � Materiais         � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Verifica se os campos obrigatorios estao preenchidos       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Template Function DCEA10TUDOk()

Local lDuplico		:= .F.
Local lRet			:= .T.
Local nTotal		:= 0
Local aLocais		:= {}
Local aArea			:= GetArea()
Local I				:= 0

For I := 1 TO nTotCampos
	cLocal := "M->LBZ_LOCA"+STRZERO(I,2)
	nQTLOC := "M->LBZ_QTLO"+STRZERO(I,2)
	IF !EMPTY(&(cLocal))
		nTotal	:= nTotal+&(nQTLOC)
		npos	:= ascan(aLocais,&(cLocal))
		IF NPOS == 0
			AADD(aLocais,&(cLocal))
		ELSE
			lDuplico := .T.
		ENDIF
	ENDIF
Next

IF M->LBZ_QTDSOL <> nTotal
	MsgBox("A Soma das quantidade dos Locais est� Diferente da Qtd. Solicitada","Aten��o","Info")
	lRet := .F.
ENDIF

IF lDuplico .AND. lRet
	MsgBox("N�o podem existir dois Locais Iguais.","Aten��o","Info")
	lRet := .F.
ENDIF

IF lRet
	For I := 1 TO nTotCampos
		cLocal:="M->LBZ_LOCA"+STRZERO(I,2)
		nQTLOC:="M->LBZ_QTLO"+STRZERO(I,2)
		
		IF ! EMPTY(&(cLocal))
			LB6->(dbSetOrder(1))
			If !LB6->(dbSeek(xFilial('LB6')+&(cLocal)))
				MsgAlert("Cessionaria n�o localizada.","Aviso")
				_lRet := .F.
			Else
				lRet:= T_TDCEA10VMAX (M->LBZ_COD,LB6->LB6_COD,LB6->LB6_LOJA,&(cLocal),"3",&(nQTLOC))
				IF !lRet
					nOpca := 0
					Return(lRet)
				Endif
			Endif
		ENDIF
	Next
Endif

RestArea(aArea)

Return lRet

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AjustaSX3  � Autor � Materiais           � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Acerta campos do DCL-EST                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Static Function AjustaSX3()

Local aArea		:= GetArea()
Local aAreaSX3	:= SX3->(GetArea())

dbSelectArea("SX3")
dbsetOrder(2)
If dbSeek("LBZ_TEMPTF") .And. "@E 99" $ SX3->X3_PICTVAR
	Reclock("SX3",.F.)
	Replace X3_PICTVAR With " "
	MsUnlock()
EndIf   

If dbSeek("D1_T_DPROV") .And. X3_VLDUSER <> 'T_M103VlBom(M->D1_T_DPROV)'
	Reclock("SX3",.F.)
	Replace X3_VLDUSER With 'T_M103VlBom(M->D1_T_DPROV)'
	MsUnlock()
EndIf
RestArea(aAreaSX3)
RestArea(aArea)

Return Nil


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TDCEA10VMAX � Autor �Luiz Enrique			� Data �JULHO/2013���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Valida quantidade de Volume Recebido.						  ���
���			 �Nao Permite Volume de Entrada + Volume em estoque ser maior ���
���			 �que o declarado no Campo: A5_XVOLMAX - Tabela SA5.		  ���
���			 �Chamado Por: P.E.: MT100LOK, P.E.: MT240TOK ou TDCEA010.	  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Template Combustiveis/Lubrificantes - Estoque              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
TEMPLATE FUNCTION TDCEA10VMAX(cProduto,Cfornec,cLoja,cLocal,cOrig,nQuant)

Local cmsg := ''
Local lret := .t.

SA5->(dbSetOrder(1)) //A5_FILIAL+A5_FORNECE+A5_LOJA+A5_PRODUTO
If !SA5->(dbSeek(xFilial("SA5")+Cfornec+cLoja+cProduto)) .Or. Empty(SA5->A5_XVOLMAX)
	Return .t.
Endif

SB2->(dbSetOrder(1))


If !SB2->(DBSEEK(XFILIAL()+cProduto+cLocal))
	MsgBox(" Inconsistencia no Saldo do Produto ","Atencao","Info")
	Return .f.
Endif

If SB2->B2_QATU + nQuant > SA5->A5_XVOLMAX
	cmsg := "Volume informado para armazen : (" +cLocal+')  '+Alltrim(Transform(nQuant,"@E 999,999,999")) + chr(13)+chr(10)
	cmsg += "Acrescentado ao Volume existente: " + Alltrim(Transform(SB2->B2_QATU,"@E 999,999,999")) + chr(13)+chr(10)
	cmsg += "� maior que o Volume M�ximo Permitido: " + Alltrim(Transform(SA5->A5_XVOLMAX,"@E 999,999,999"))
	MsgBox(cmsg,"Atencao","Info")
	Return .f.
Endif

Return lret

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TDCEA10MARG � Autor �Luiz Enrique			� Data �JULHO/2013���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Verifica a Margem aceitavel a Menor ou a Maior entre as Qtds���
���Descri��o �Fiscal e Apuradas. Em (%).								  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Template Combustiveis/Lubrificantes - Estoque              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
TEMPLATE FUNCTION TDCEA10MARG()

Local nTamNF		:= TamSX3('D3_X_NF')[1]
Local nPerc			:= 0
Local cmsg			:= ''
Local cSeek			:= ''
Local cProdu		:= GDFieldGet("D1_COD")
Local cLocal		:= GDFieldGet("D1_LOCAL")
Local cXVia			:= GDFieldGet("D1_X_VIA")
Local nQtd			:= GDFieldGet("D1_QUANT")
Local nQtd1			:= GDFieldGet("D1_X_QTD1")
Local cDprov		:= GDFieldGet("D1_T_DPROV")

//CNFISCAL+CSERIE+CA100FOR+CLOJA
SA5->(dbSetOrder(1)) //A5_FILIAL+A5_FORNECE+A5_LOJA+A5_PRODUTO
If !SA5->(dbSeek(xFilial("SA5")+CA100FOR+CLOJA+cProdu)) .Or. Empty(SA5->A5_TOLEDIF)
	Return .t.
Endif

cSeek:= Padr(cDprov + cLocal,nTamNF)+"PR"+" "

If cXVia == "02" // TIPO BOMBEIO
	SD3->(dbOrderNickName("DCLSD3_01"))
	If SD3->(msSeek(xFilial("SD3")+cSeek))
		nPerc:= XPercent (SD3->D3_QUANT,nQtd )
	Else
		ApMsgAlert("Movimento nao localizado: " + Alltrim(cSeek),"Aviso")
		Return .f.
	Endif
Else	//TIPO DESCARGA
	nPerc:= XPercent (nQtd1,nQtd)
Endif

If nPerc < SA5->A5_TOLEDIF
	Return .t.
Endif

cmsg := "Inconsistencia de Dados." + chr(13)+chr(10)
cmsg += "A Margem aceitavel de " + Transform(SA5->A5_TOLEDIF,"@E 9999.99") + "% " + chr(13)+chr(10)
cmsg += "Entre a diferen�a da Quantidade Fiscal e a Apurada, foi ultrapassada."+ chr(13)+chr(10)
cmsg += "A diferen�a esta em " + Transform(nPerc,"@E 9999.99") + "%."

ApMsgAlert(cmsg,"Aviso")

Return .f.


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TDCEA10LIM  � Autor � Antonio Cordeiro      Data �OUT/2013  ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Limpa valores digitados nos armazens e aloca somente no 1�.s���
���Descri��o �campo a partir da quantidade digitada                       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Template Combustiveis/Lubrificantes - Estoque              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
TEMPLATE FUNCTION TDCEA10LIM()

Local cLoca		:= ""
Local cCong		:= ""
Local cQtLo		:= 0
Local cLocal01	:= GETMV("MV_X_LBASE")
Local nCont		:= 0

FOR nCont:=1 TO nTotCampos
	cLoca:="M->LBZ_LOCA"+STRZERO(nCont,2)
	cCong:="M->LBZ_CONG"+STRZERO(nCont,2)
	cQTLO:="M->LBZ_QTLO"+STRZERO(nCont,2)
	IF nCont==1
		cLoca01:=&(cLoca)
	ELSE
		&(cQTLO) := 0
		&(cLoca) := "  "
		&(cCong) := "                               "
	ENDIF
Next

Return(cLocal01)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �XPercent  � Autor �Luiz Enrique			� Data �JULHO/2013���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Calculo da Porcentagem.									  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Static Function XPercent (nQ1,nQ2)

Local nPercent:= 0

nPercent := If (nQ1 >= nq2,nQ1/nQ2,nQ2/nQ1)
nPercent := (nPercent - 1) *100

Return nPercent

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AjustaSX6  � Autor � Materiais           � Data �12/01/2015���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Acerta so parametros MV_DCLEST*                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Static Function AjustaSX6()

Local aArea    := GetArea()
Local aAreaSX6 := SX6->(GetArea())
Local cTexto1  := "Se .T. mostra aviso de estoque negativo com op��o"
Local cTexto2  := "para aceitar ou rejeitar a operacao, se .F. emite"

SX6->(dbSetOrder(1))

// Ajusta MV_DCLEST1
If SX6->(MsSeek(xFilial("SX6")+"MV_DCLEST1")) .And. !(SubStr(cTexto1,1,13) $ SX6->X6_DESCRIC)
	RecLock("SX6",.F.)
	SX6->X6_DESCRIC := cTexto1
	SX6->X6_DESC1   := cTexto2
	SX6->X6_DESC2   := "aviso e n�o permite a operacao. Para Doc. Entrada"
	SX6->(MsUnLock())
EndIf
// Ajusta MV_DCLEST2
If SX6->(MsSeek(xFilial("SX6")+"MV_DCLEST2")) .And. !(SubStr(cTexto1,1,13) $ SX6->X6_DESCRIC)
	RecLock("SX6",.F.)
	SX6->X6_DESCRIC := cTexto1
	SX6->X6_DESC1   := cTexto2
	SX6->X6_DESC2   := "aviso e n�o permite a operacao. Para Nota de Saida"
	SX6->(MsUnLock())
EndIf
// Ajusta MV_DCLEST3
If SX6->(MsSeek(xFilial("SX6")+"MV_DCLEST3")) .And. !(SubStr(cTexto1,1,13) $ SX6->X6_DESCRIC)
	RecLock("SX6",.F.)
	SX6->X6_DESCRIC := cTexto1
	SX6->X6_DESC1   := cTexto2
	SX6->X6_DESC2   := "aviso e n�o permite a operacao. Para Mov. Interno"
	SX6->(MsUnLock())
EndIf

RestArea(aAreaSX6)
RestArea(aArea)

Return Nil

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AjusteSXB  � Autor � Materiais           � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Acerto de Consulta Padrao DCL-EST                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Static Function AjusteSXB()

Local aArea		:= GetArea()
Local cContem	:= "SD3->D3_ESTORNO<>'S' .AND. ! EMPTY(SD3->D3_X_NF) .AND. SD3->D3_X_TMT == 'PR'"

dbSelectArea("SXB")
SXB->(dbSetOrder(1))

If SXB->(MsSeek(PadR("LB3",Len(SXB->XB_ALIAS))+"6"))
	If Alltrim(SXB->XB_CONTEM) <> cContem
		RecLock("SXB",.F.)
		SXB->XB_CONTEM		:= cContem
		SXB->(MsUnLock())
	EndIf
Else
	RecLock("SXB",.T.)
	SXB->XB_ALIAS		:= "LB3"
	SXB->XB_TIPO		:= "6"
	SXB->XB_SEQ			:= "01"
	SXB->XB_COLUNA		:= "" 
	SXB->XB_DESCRI		:= ""
	SXB->XB_DESCSPA		:= ""
	SXB->XB_DESCENG		:= ""
	SXB->XB_CONTEM		:= cContem
	SXB->XB_WCONTEM		:= ""
	SXB->(MsUnLock())
EndIf

RestArea(aArea)

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AjusteSX7  � Autor � Materiais           � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Acerto de gatilho do DCL-EST                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Static Function AjusteSX7()

Local aArea		:= GetArea()

dbSelectArea("SX7")
SX7->(dbSetOrder(1))

If SX7->(MsSeek(PadR("D3_TM",Len(SX7->X7_CAMPO))+PadR("002",Len(SX7->X7_SEQUENC))))
	If !("T_TDC10TM(M->D3_TM)" $ SX7->X7_REGRA)
		RecLock("SX7",.F.)
		SX7->X7_REGRA		:= "T_TDC10TM(M->D3_TM)"
		SX7->X7_SEEK		:= "N"
		SX7->X7_ALIAS		:= ""
		SX7->X7_ORDEM		:= 0
		SX7->X7_CHAVE		:= ""
		SX7->X7_CONDIC		:= "FindFunction('T_TDC10TM')"
		SX7->(MsUnLock())
	EndIf
Else
	RecLock("SX7",.T.)
	SX7->X7_CAMPO		:= "D3_TM"
	SX7->X7_SEQUENC		:= "002"
	SX7->X7_REGRA		:= "T_TDC10TM(M->D3_TM)"
	SX7->X7_CDOMIN		:= "D3_X_TMT"
	SX7->X7_TIPO		:= "P"
	SX7->X7_SEEK		:= "N"
	SX7->X7_ALIAS		:= ""
	SX7->X7_ORDEM		:= 0
	SX7->X7_CHAVE		:= ""
	SX7->X7_CONDIC		:= "FindFunction('T_TDC10TM')"
	SX7->X7_PROPRI		:= "T"
	SX7->(MsUnLock())
EndIf

RestArea(aArea)

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � TDC10TM    � Autor � Materiais           � Data �18/10/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho utilizado no D3_TM para gravacao do D3_X_TM        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Template Function TDC10TM(cTM)

Local cTMT		:= ""
Local aArea		:= GetArea()
Local aAreaSF5	:= SF5->(GetArea())

dbSelectArea("SF5")
SF5->(dbSetOrder(1))

If SF5->(MsSeek(xFilial("SF5")+cTM))
	cTMT := SF5->F5_X_TMT
EndIf

RestArea(aAreaSF5)
RestArea(aArea)

Return cTMT