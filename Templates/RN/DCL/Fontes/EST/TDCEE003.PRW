/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � TDCEE003 � Autor � Antonio Cordeiro      � Data �julho/2002���
�������������������������������������������������������������������������Ĵ��
���Descricao � Calculo da Temperatura Media                               ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Template Estoque Combustiveis                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

#include "rwmake.ch"    

Template Function TDCEE003()

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("CAREA,NREC,CIND,NMEDTP,NMEDIA,")
CHKTEMPLATE("DCLEST")

//��������������������������������������������������������������Ŀ
//�                                                              �
//����������������������������������������������������������������

cArea := Alias()
nRec := Recno()
cInd := IndexOrd()
nMedTp:= M->LB3_TEMP
nMedia:=0
If M->LB3_TEMP1 > 0
	nMedia:=nMedia+1
EndIf
If M->LB3_TEMP2 > 0
	nMedia:=nMedia+1
EndIf
If M->LB3_TEMP3 > 0
	nMedia:=nMedia+1
Endif
nMedTp:= (M->LB3_TEMP1 + M->LB3_TEMP2 + M->LB3_TEMP3 )/nMedia

DbSelectArea(cArea)
DbSetOrder(cInd)
Dbgoto(nRec)
Return(nMedTp)
