/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � TDCEE004 � Autor � Antonio Cordeiro      � Data �julho/2002���
�������������������������������������������������������������������������Ĵ��
���Descricao � Calculo Volume de Produto no Tanque com Base em Fatores    ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Template Estoque Combustiveis                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

#include "rwmake.ch"

Template Function TDCEE004()

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������


Local _cAlias := Alias()
Local _cIndex := IndexOrd()
Local _nRecno := Recno()
Local nAltura :=0
Local nFator  :=0
Local nVolume :=0
Local nSelo   :=0
Local nConta  :=0
Local cFaixa  :=ALLTRIM(SUPERGETMV('MV_FXARQ',.F.,'I'))

CHKTEMPLATE("DCLEST")

nSELO:= GETMV("MV_X_SELO")


IF M->LBZ_TIPO=='BO'  .AND. M->LBZ_ALTI <> 0
	
	dbSelectArea("LB4")
	dbSetOrder(1)
	dbSeek(xFilial()+M->LBZ_TANQUE,.F.)
	IF READVAR() = "M->LBZ_ALTI" .OR. READVAR() = "M->LBZ_FATCOI"
		nAltura:=M->LBZ_ALTI
		nFator :=M->LBZ_FATCOI
	ELSE
		nAltura:=M->LBZ_ALTF
		nFator :=M->LBZ_FATCOF
	ENDIF
	
	While !eof() .and. LB4->LB4_FILIAL==xFilial() .and. LB4->LB4_TANQUE==M->LBZ_TANQUE
		
		IF (nALTURA) >= LB4->LB4_CMINI .and. (nALTURA) <= LB4->LB4_CMFIM
			
			IF cFaixa=='F'
			   nconta:= LB4->LB4_CMFIM - nALTURA
			Else
			   nconta:= nALTURA - LB4->LB4_CMINI
			Endif   
			
			If nconta > 0
				If nALTURA >= nSELO
					IF cFaixa=='F'
					   nvolume:= Int((LB4->LB4_ACUM - NoRound(LB4->LB4_FATOR * nconta)-LB4->LB4_LASTRO))
					ELSE
					   nvolume:= Int((LB4->LB4_ACUM + NoRound(LB4->LB4_FATOR * nconta)-LB4->LB4_LASTRO))
					ENDIF   
				Else
					IF cFaixa=='F'
					   nvolume:= Int(LB4->LB4_ACUM - NoRound(LB4->LB4_FATOR * nconta))
					ELSE
					   nvolume:= Int(LB4->LB4_ACUM + NoRound(LB4->LB4_FATOR * nconta))
					ENDIF   
				Endif
			else
				If nALTURA >= nSELO
					nVolume:= Int(LB4->LB4_ACUM-LB4->LB4_LASTRO)
				Else
					nVolume:= Int(LB4->LB4_ACUM)
				Endif
			endif
			Exit
		Endif
		DbSelectArea("LB4")
		DbSkip()
	Enddo
	NVOLUME:=NVOLUME * NFATOR
ELSE
	NVOLUME:=M->LBZ_QTDSOL * M->LBZ_FATCOF
ENDIF


dbSelectArea(_cAlias)
dbSetOrder(_cIndex)
dbGoTo(_nRecno)
Return(nVolume)



