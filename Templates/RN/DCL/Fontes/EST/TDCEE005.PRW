/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � TDCEE005 � Autor � Antonio Cordeiro      � Data �julho/2002���
�������������������������������������������������������������������������Ĵ��
���Descricao � Calculo Volume de Produto no Tanque com Base em Fatores    ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Template Estoque Combustiveis                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

#include "rwmake.ch"

Template Function TDCEE005()

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

Local _cAlias := Alias()
Local _cIndex := IndexOrd()
Local _nRecno := Recno()
Local nVolume := 0
Local nRet    := 0
Local _aAreaSX3		:= SX3->(GetArea())
Local cPoLocal:=""
Local cPoQtd  :=""
LocaL I:=0 
Local J:=0 
Local nQtd:=0
Local nVar1:=""

cPoLocal:="LBZ_LOCA01"
cPoQTd  :="LBZ_QTLO01"
SX3->(dbSetOrder(2))
WHILE SX3->(dbSeek(cPoLocal)) .and. SX3->(dbSeek(cPoQtd))
	nTotCampos:=nTotCampos+1
	cPoLocal:="LBZ_LOCA"+STRZERO(nTotCampos,2)
	cPoQTd  :="LBZ_QTLO"+STRZERO(nTotCampos,2)
ENDDO
SX3->(RestArea(_aAreaSX3))
nTotCampos:=nTotCampos-1

FOR I:=1 TO nTotCampos
	IF READVAR() =="M->LBZ_QTLO"+STRZERO(I,2)
       nQtd:=0
       FOR J:=1 TO nTotCampos
           IF I=nTotCampos
              IF J<>1
                 nQtd +=&("M->LBZ_QTLO"+STRZERO(J,2))
              ENDIF   
           ELSE
              IF I+1<>J
                 nQtd +=&("M->LBZ_QTLO"+STRZERO(J,2))
              ENDIF
           ENDIF   
       NEXT       
       nVolume := M->LBZ_QTDSOL-nQtd       
       nRet:=&("M->LBZ_QTLO"+STRZERO(I,2))
       IF I<>nTotCampos
          nVar1:="M->LBZ_QTLO"+STRZERO(I+1,2)
       ELSE
          nVar1:="M->LBZ_QTLO"+STRZERO(1,2)
       ENDIF   
       IF nVolume > 0
          &(nVar1):=nVolume
       ELSEIF nVolume < 0
          nQtd:=0
          MsgAlert("Volume negativo Invalido")
          FOR J:=1 TO nTotCampos
            IF J<>I
               nQtd +=&("M->LBZ_QTLO"+STRZERO(J,2))
            ENDIF
          NEXT       
          nRet:=M->LBZ_QTDSOL - nQtd
       ENDIF
       EXIT
    ENDIF
NEXT


dbSelectArea(_cAlias)
dbSetOrder(_cIndex)
dbGoTo(_nRecno)
Return(nRet)



