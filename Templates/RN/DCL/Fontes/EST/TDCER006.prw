#Include 'Protheus.ch'
#include "REPORT.CH"
#INCLUDE "TOPCONN.CH"

Template Function TDCER006()

//
// TDCER006 - Relat�rio de vendas entre congeneres - Distribuidor
// 
Local oReport
CHKTEMPLATE("DCLEST")

// Interface de impressao  
oReport := ReportDef()
oReport:PrintDialog()

Return

//
// ReportDef 
//

Static Function ReportDef()

Local oReport 
Local oSection 
Local oCell         
Local cPerg   := "CER006"
Local lQuery  := .T.
Local aRegs	:= {}  
Local cTamQtd	:=	TamSX3('D1_QUANT')[1]
Local i,j
Local nTamSX1   := Len(SX1->X1_GRUPO)


CHKTEMPLATE("DCLEST")

If SX1->(dbSeek(PADR(cPerg,nTamSX1)+'01'))
   While SX1->X1_GRUPO = PADR(cPerg,nTamSX1)
	  	If SX1->X1_ORDEM == '01' .And. AllTRIM(SX1->X1_F3) == 'SM0'
	   		exit
	   	EndIF
   		RecLock("SX1",.F.)
	  	SX1->(dbDelete())
	  	MsUnlock()
	  	SX1->(DbSkip())
	End do
Endif


Dbselectarea('SX1')
aAdd(aRegs,{cPerg,"01","Filial De ........ ?","","","mv_ch1","C",TamSX3('D1_FILIAL')[1],0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","SM0","",""})
aAdd(aRegs,{cPerg,"02","Filial Ate ....... ?","","","mv_ch2","C",TamSX3('D1_FILIAL')[1],0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","SM0","",""})
aAdd(aRegs,{cPerg,"03","Data Inicio....... ?","","","mv_ch3","D",						 08,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"04","Data Final ....... ?","","","mv_ch4","D",						 08,0,0,"G","","mv_par04","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"05","Produtos  ........ ?","","","mv_ch5","C",						 30,0,0,"R","","mv_par05","","","","D1_COD","","","","","","","","","","","","","","","","","","","","","SB1","",""})

For i:=1 to Len(aRegs)
    If ! SX1->(dbSeek(PADR(cPerg,nTamSX1)+aRegs[i,2]))
        RecLock("SX1",.T.)
        For j:=1 to FCount()
            If j <= Len(aRegs[i])
                FieldPut(j,aRegs[i,j])
            Endif
        Next
        MsUnlock()
    Endif
Next

//������������������������������������������������������������������������Ŀ
//�Criacao do componente de impressao                                      �
//�                                                                        �
//�TReport():New                                                           �
//�ExpC1 : Nome do relatorio                                               �
//�ExpC2 : Titulo                                                          �
//�ExpC3 : Pergunte                                                        �
//�ExpB4 : Bloco de codigo que sera executado na confirmacao da impressao  �
//�ExpC5 : Descricao                                                       �
//�                                                                        �
//��������������������������������������������������������������������������
oReport := TReport():New("TDCER006",'Rel. Vendas entre congeneres - Distribuidor',cPerg, {|oReport| ReportPrint(oReport,cPerg)},'Este relat�rio lista as notas de entradas para os produtos selecionados suprimindo os fornecedores informados') 
oReport:SetLandscape()    

Pergunte(cPerg,.F.)

oSection := TRSection():New(oReport,'Vendas entre congeneres - Distribuidor',{"SD1"}) 
oSection :SetHeaderPage()
oSection:SetNoFilter("SD1")

TRCell():New(oSection,"D1_FORNECE"	,"SD1","RAZ�O SOCIAL DA DISTRIBUIDORA VENDEDORA"	,/*Picture*/						,/*Tamanho*/,/*lPixel*/,{|| (cAliasSD1)->NOMFOR })
TRCell():New(oSection,"D1_EMISSAO"	,"SD1","DATA DE EMISS�O DA NOTA FISCAL"			,/*Picture*/						,/*Tamanho*/,/*lPixel*/,{|| (cAliasSD1)->D1_EMISSAO })
TRCell():New(oSection,"D1_COD"		,"SD1","PRODUTO (�LEO DIESEL A OU B100)"			,/*Picture*/						,/*Tamanho*/,/*lPixel*/,{|| (cAliasSD1)->B1_DESC })
TRCell():New(oSection,"D1_QUANT"	,"SD1","VOLUME VENDIDO (m�)"						,PesqPict("SD1","D1_QUANT",14)	,cTamQtd    ,/*lPixel*/,{|| (cAliasSD1)->QTDE })
TRCell():New(oSection,"NOMEMP"	,"SD1","RAZ�O SOCIAL DA DISTRIBUIDORA COMPRADORA",/*Picture*/						,/*Tamanho*/,/*lPixel*/,{|| (cAliasSD1)->NOMEMP })
TRCell():New(oSection,"F1_CHVNFE"	,"SF1","CHAVE DE ACESSO DA Nfe"						,/*Picture*/						,/*Tamanho*/,/*lPixel*/,{|| (cAliasSD1)->F1_CHVNFE })

Return(oReport)


// 
// ReportPrint
//

Static Function ReportPrint(oReport,cPerg)

Local oSection  := oReport:Section(1)
Local aStrucSD1 := SD1->(dbStruct())
Local cAliasSD1 := GetNextAlias()
Local cNomEmp	  := ' '
Local cEmpAnt, cFilAnt
Local cWhere
Local cCampo	  := GetNewPar("MV_SCANC04","")	
Local cSkipTES  := GetNewPar("MV_DCLSKPT","")
Local cQuery    := ""
Local aFilaux	  := {}

If empty(cSkipTES)
	cSkipTES:= ''
EndIf
If Empty(cCampo)
	Alert ('MV_SCANC04 que indica o campo na tabela SA2 que classifica a categoria do fornecedor n�o configurado, impossivel seguir.')
	return
EndiF

Dbselectarea('SM0')
SM0->(dbSetOrder(1))
SM0->(dbSeek(FwCodEmp()))
While SM0->(!EOF()) .And. SM0->M0_CODIGO == FwCodEmp()
	aAdd(aFilaux,{SM0->M0_CODFIL,SM0->M0_FILIAL})
	SM0->(DbSkip())
End

oReport:NoUserFilter()  // Desabilita a aplicacao do filtro do usuario no filtro/query das secoes

	oReport:EndPage() //Reinicia Paginas

	oReport:SetTitle('Vendas entre congeneres - Distribuidor')

	//�������������������������������������������������������������������Ŀ
	//�Esta rotina foi escrita para adicionar no select os campos         �
	//�usados no filtro do usuario, quando houver.                        �
	//���������������������������������������������������������������������
	dbSelectArea("SD1")
	
	dbSelectArea("SB1")
	dbSetOrder(1)
	
	MakeSqlExpr(cPerg)
	
	cWhere := cCampo+"='DIS' AND "+MV_PAR05

	cAliasSD1:= "CER006"
	cQuery := "SELECT SA2.A2_NOME +' ('+SA2.A2_CGC+')' NOMFOR, SD1.D1_EMISSAO, SB1.B1_DESC, SD1.D1_QUANT / 1000 QTDE, SD1.D1_FILIAL NOMEMP,SF1.F1_CHVNFE, SD1.D1_DOC, SD1.D1_SERIE" 
	cQuery += "FROM  "+RetSqlName("SD1")+"  SD1 "
	cQuery += "LEFT JOIN  "+RetSqlName("SA2")+" SA2 ON SA2.A2_FILIAL = '"+xFilial("SA2")+"' AND SA2.A2_COD = SD1.D1_FORNECE AND SA2.A2_LOJA = SD1.D1_LOJA AND SA2.D_E_L_E_T_= ' ' " 
	cQuery += "LEFT JOIN  "+RetSqlName("SF1")+" SF1 ON SF1.F1_FILIAL = SD1.D1_FILIAL AND SF1.F1_DOC = SD1.D1_DOC AND SF1.F1_SERIE = SD1.D1_SERIE AND SF1.F1_FORNECE = SD1.D1_FORNECE AND SF1.F1_LOJA = SD1.D1_LOJA AND SF1.D_E_L_E_T_= ' '"
	cQuery += "LEFT JOIN  "+RetSqlName("SB1")+" SB1 ON SB1.B1_FILIAL = '"+xFilial("SB1")+"' AND SB1.B1_COD = SD1.D1_COD AND SB1.D_E_L_E_T_= ' ' "
	cQuery += "WHERE SD1.D1_FILIAL >= '"+MV_PAR01+"' AND SD1.D1_FILIAL <= '"+MV_PAR02+"' AND SD1.D1_DTDIGIT >= '"+DtoS(MV_PAR03)+"' AND SD1.D1_DTDIGIT <= '"+DtoS(MV_PAR04)+"' AND SD1.D1_UM = 'L ' AND " 
	cQuery += "SD1.D1_TES <> ' ' AND "
    If !Empty(cSkipTES)
        cQuery += "SD1.D1_TES NOT IN ("+cSkipTES+") AND "
    EndIf
    cQuery += cCampo+"='DIS' AND "+MV_PAR05+" AND SD1.D_E_L_E_T_= ' ' AND SF1.F1_CHVNFE <> ' '"
	cQuery += "ORDER BY SD1.D1_EMISSAO, SA2.A2_NOME"
	
	cQuery := ChangeQuery(cQuery)

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSD1,.T.,.T.)

	//������������������������������������������������������������������������Ŀ
	//�Inicio da impressao do fluxo do relat�rio                               �
	//��������������������������������������������������������������������������
	dbSelectArea(cAliasSD1)
	oReport:SetMeter((cAliasSD1)->(LastRec()))
	oSection:Init()
	
	While !oReport:Cancel() .And. !(cAliasSD1)->(Eof())
	    
		If oReport:Cancel()
			Exit
		EndIf
		
		oReport:IncMeter()

		oSection:Cell("D1_FORNECE")	:SetValue((cAliasSD1)->NOMFOR)
		oSection:Cell("D1_EMISSAO")	:SetValue(stod((cAliasSD1)->D1_EMISSAO))
		oSection:Cell("D1_COD")		:SetValue((cAliasSD1)->B1_DESC)
		oSection:Cell("D1_QUANT")	:SetValue((cAliasSD1)->QTDE)
		oSection:Cell("NOMEMP")		:SetValue(Iif(!Empty((cAliasSD1)->NOMEMP),aFilaux[aScan(aFilaux,{|x| alltrim(x[1])==alltrim((cAliasSD1)->NOMEMP)}),2],(cAliasSD1)->NOMEMP))
		oSection:Cell("F1_CHVNFE")	:SetValue((cAliasSD1)->F1_CHVNFE)
		oSection:PrintLine()
		(cAliasSD1)->(Dbskip())

	EndDo

	oSection:Finish()			


Return NIL
