#Include 'Protheus.ch'
#Define ENTER chr(13) + chr(10)
/*
����������������������������������������������������������������������
����������������������������������������������������������������������
������������������������������������������������������������������Ŀ��
���Fun�ao    � ValEstDcl � Autor � Robson Sales  � Data �17/03/2014���
������������������������������������������������������������������Ĵ��
���Descri�ao � Valida se a movimentacao deixara o saldo em estoque ���
���          � negativa, e atraves dos parametros MV_DCLEST1/2/3   ���
���          � permite ou nao prosseguir com a movimentacao.       ���
������������������������������������������������������������������Ĵ��
���Parametros� cCod      = Codigo do produto                       ���
���          � cLocal    = Armazem                                 ���
���          � nQuant    = Quantidade da movimentacao              ���
���          � dData     = Data de emissao do movimento            ���
���          � nOperacao = 1(SD1), 2(SD2) e 3(SD3)                 ���
�������������������������������������������������������������������ٱ�
����������������������������������������������������������������������
����������������������������������������������������������������������
*/
Template Function ValEstDcl(cCod,cLocal,nQuant,dData,nOperacao)

Local nSaldo 	:= 0
Local lRet   	:= .T.
Local cMens  	:= ""
Local cTitle 	:= "DCL-EST: Movimenta��o deixar� Saldo Negativo"
Local lSegue 	:= SuperGetMv("MV_DCLEST"+Alltrim(Str(nOperacao)),.F.,.F.)
Local lAlcada	:= SUPERGETMV('MV_ANPALC',.F.,.F.)
Local lEstZero	:= Alltrim(cCod) $ SUPERGETMV('MV_ESTZERO',.F.,'  ')
Local aAreaSB2	:= SB2->(GetArea())
Local aAreaSB1	:= SB1->(GetArea())
Local aAreaNNR	:= NNR->(GetArea())

// Para o caso EstZero n�o valida saldo, por que sempre ser� Zero.
If !lEstZero
	
	nSaldo := CalcEst(cCod,cLocal,dData+1)[1]
	
	// Subtrai empenho, reserva e previsto do saldo Fisico
	dbSelectArea("SB2")
	dbSetOrder(1)
	If MsSeek(xFilial("SB2")+cCod+cLocal)
		nSaldo -= SB2->B2_RESERVA        // Abaixo Caso n�o venha da produ��o automatica retira o apontamento do empenho para n�o ter empenho em duplicidade. 
		nSaldo -= SB2->B2_QEMP - SB2->B2_SALPEDI - Iif (!IsInCallStack("T_M460CUST") .and. SB2->B2_QEMP >= nQuant,nQuant,0)
	EndIf
	
	If (nSaldo - nQuant) < 0 
		cMens := "ATEN��O" + ENTER
		cMens += ""+ENTER
		cMens += "A movimenta��o deixar� Saldo Negativo em Estoque." + ENTER
		cMens += ""+ENTER
		cMens += "Produto..........:   " + Alltrim(cCod) + ENTER
		cMens += "Armaz�m.......:   " + Alltrim(cLocal) + ENTER
		cMens += "Em estoque....:   " + Alltrim(Transform(nSaldo,"@E 99,999,999.99")) + ENTER 
		cMens += "Necessidade..:   "  + Alltrim(Transform(nQuant,"@E 99,999,999.99")) + ENTER
		cMens += "Diferen�a.......:   " + Alltrim(Transform((nQuant-nSaldo),"@E 99,999,999.99")) + ENTER
		If lSegue
			cMens += ""+ENTER
			cMens += "Prosseguir mesmo assim?"
			If ! MsgYesNo(cMens,cTitle)
				lRet:=.F.
				Help(,,"DCLEST_Help",,"O movimento n�o foi processado.",1,0) 
			EndIf
		Else
			MsgInfo(cMens,cTitle)
			lRet:=.F.
			Help(,,"DCLEST_Help",,"O movimento n�o foi processado.",1,0)
		EndIf
	EndIf
	
	///- Verifica saldo para ANP45	
	If lRet .And. lAlcada .And. (SB1->(FieldPos("B1_ANP45")) > 0) .And. AliasInDic("LBB")
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSelectArea("NNR")
		DbSetOrder(1)
		If SB1->(MsSeek(xFilial("SB1")+cCod)) .And. NNR->(MsSeek(xFilial("NNR")+cLocal))
			If SB1->B1_ANP45 .And. NNR->NNR_ANP45
				lRet:= DclAlcAnp(cCod,nQuant)
			EndIf
		EndIf
		RestArea(aAreaNNR)
		RestArea(aAreaSB1)	
	EndIf		
	RestArea(aAreaSB2)
EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DclAlcAnp
Funcao de valida��o de saldo do produto perante ao objetivo de estoque
estabelicido na resolucao ANP45

@author alexandre.gimenez	

@param cCod,nQuant - Codigo do produto, Quantidade desejada
@return lRet
@since 30/06/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Function DclAlcAnp(cCod,nQuant)
Local lRet 		:= .T.
Local dDataRef	:= dDataBase
Local nEsmObj		:= 0
Local aLocMnt		:= GetLocMnt(cFilAnt)
Local aSemana		:= ANP45Data(dDataRef)
Local cMes			:= ANP45MesAno(dDataRef,"M")
Local cAno			:= ANP45MesAno(dDataRef,"A")
Local cCodReg		:= StrZero(Val(SuperGetMV("MV_T_CSIMP",.F.,"")), 10) // pegar do simp
Local nSaldo		:= DclSaldoANP(cCod)
Local nX			:=	0
Local nSaldoDisp	:=	0
Local nTransito	:=	0
Local nSaldoTer	:= 0
Local aTransito	:= {}
Local aTerceiro := {}
Local cMens		:= ""
Local cTitle 		:= "ANP45: Movimenta��o deixar� saldo em estoque abaixo do estoque minimo semanal esperado"

Local lTransit	:= SUPERGETMV('MV_ANPTRAN',.F.,.F.)
Local lSaldoTer	:= SuperGetMV("MV_ANP45TE",.F.,.F.)
Local lSegue	:= RetCodUsr() $ SUPERGETMV('MV_ANPUSR',.F.,'000000') // Por padrao Admin

//Busca Meta
DbSelectArea("LBB")
DbSetOrder(2)//LBB_FILIAL+LBB_CODREG+LBB_LOCMNT+LBB_CODPRO+LBB_MES+LBB_ANO
If LBB->(DbSeek(xFilial("LBB")+cCodReg+aLocMnt[1,2]+cCod+cMes+cAno))
	nEsmObj := LBB->LBB_ESDMOB
Else
	nEsmObj := GetESDOB(cCod,aLocMnt[1,2],dDataRef)
EndIf

//Avalia saldo em Terceiros
If lSaldoTer 
	aTerceiro := MtANPTer(cCod,dDataRef)
	aEval(aTerceiro,{|aTerceiro| nSaldoTer += aTerceiro[4]})
EndIf

//Avalia saldo com ou sem transito
If lTransit
	aTransito	:= GetTransito(cCod,dDataRef,GetFilMnt(aLocMnt[2]))
	For nX := 1 To Len(aTransito)
		nTransito += aTransito[nX,2]
	Next nX
EndIf

nSaldoDisp := (nSaldo + nTransito + nSaldoTer) 
	
// Valida se tem saldo e exibe msg
If (nSaldoDisp - nEsmObj - nQuant) < 0 
		cMens := "ATEN��O" + ENTER
		cMens += ""+ENTER
		cMens += "ANP45: Movimenta��o deixar� saldo em estoque abaixo do estoque minimo semanal esperado" + ENTER
		cMens += ""+ENTER
		cMens += "Produto..........:   " + Alltrim(cCod) + ENTER
		cMens += "Estoque Objetivo.:   " + Alltrim(Transform(nEsmObj 	,"@E 99,999,999.99")) + ENTER
		cMens += "Em estoque.......:   " + Alltrim(Transform(nSaldoDisp,"@E 99,999,999.99")) + ENTER 
		cMens += "Necessidade......:   " + Alltrim(Transform(nQuant		,"@E 99,999,999.99")) + ENTER
		cMens += "Diferen�a........:   " + Alltrim(Transform(((nQuant+nEsmObj)-nSaldoDisp),"@E 99,999,999.99")) + ENTER
		If lSegue
			cMens += ""+ENTER
			cMens += "Prosseguir mesmo assim?"
			If ! MsgYesNo(cMens,cTitle)
				lRet:=.F.
				Help(,,"ANP45_Help",,"O movimento n�o foi processado.",1,0) 
			EndIf
		Else
			MsgInfo(cMens,cTitle)
			lRet:=.F.
			Help(,,"ANP45_Help",,"O movimento n�o foi processado.",1,0)
		EndIf
	EndIf
Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} DCLVldBloq
Funcao responsavel por validar se um produto esta bloqueado, 
Caso retornar verdadeiro o produto esta bloqueado

@author alexandre.gimenez	

@param cCod,cLocal - Codigo e Armazem do produto
@return lRet
@since 23/07/2014
@version 1.0
/*/
//-------------------------------------------------------------------
Template Function DCLVldBloq(cCod,cLocal)
Local lRet := .F.
Local aArea := GetArea()

DbSelectArea("SB1")
DbSetOrder(1)
If SB1->(DbSeek(xFilial("SB1")+cCod+cLocal))
	lRet := SB1->B1_MSBLQL == "1" //--bloqueado
	If lRet
		Help(,,"DCLEST_MSBLQL",,"O Produto "+AllTrim(cCod)+" esta bloqueado. O movimento n�o foi processado.",1,0)
	EndIf
EndIf
RestArea(aArea)
Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} EstornaDCL
Fun��o para movimentos provis�rios da PetroBahia

@author Douglas.Nunes
@since 29/07/2014
@version 1.0          

@param cBusca caractere, C�digo do Documento a ser Estornado 
@param nTipo, num�rico,	1 - Exclus�o do documento de entrada
							2 - Exclus�o do documento de sa�da antes do 
								recebimento do documento de entrada

/*/
//-------------------------------------------------------------------
Function EstornaDCL(cBusca, nTipo)

Local cTipMov 		:= SuperGetMv('MV_TMPRV',,'')
Local aArea   		:= GetArea()
Local aRec			:= {}
Local aMov    		:= {}
Local aEstorna 		:= {}
Local lPos    		:= .F.
Local nX      		:= 0
Local xValor  		:= Nil   
Local nDif			:= 0
Local lRet 			:= .T.
Local lRec			:= .T. // Recria movimento

PRIVATE lMsErroAuto	:= .F.
	
	If nTipo == 1
		SD3->(DBORDERNICKNAME('DCL_D3F1'))//D3_FILIAL+D3_CHAVEF1
	Else
		SD3->(DBORDERNICKNAME('DCL_D3F2'))//D3_FILIAL+D3_T_SF2
	EndIf
	
	// Posiciona no primeiro Registro da chave de busca
	If SD3->(DbSeek(xFilial('SD3')+cBusca))
		If nTipo == 1
			lPos := .T.
		Else
			While Alltrim(xFilial('SD3')+cBusca) == Alltrim(If(nTipo == 1,SD3->(D3_FILIAL+D3_CHAVEF1),SD3->(D3_FILIAL+D3_T_SF2)))
				If Empty(SD3->D3_ESTORNO)
					lPos := .T.
					Exit
				EndIf
				SD3->(dbSkip())
			EndDo
		EndIf
	EndIf

	If lPos
		If nTipo == 1 // Devolu��o de NFS
			While AllTrim(SD3->D3_CHAVEF1) == cBusca 
				//guarda recno para mudar a chave depois
				aAdd(aRec,SD3->(Recno()))
				//N�o deve processar Movimentos 999
				If SD3->D3_TM != cTipMov 					
					SD3->(DBSkip())
					Loop
				EndIf
				
				//Reincluir todos os encontrados.
				Aadd(aMov,{})
				aADD(aMov[Len(aMov)],{"D3_FILIAL" ,xFilial("SD3")	,NIL})
				aADD(aMov[Len(aMov)],{"D3_TM"	  ,cTipMov  		,NIL})
				aADD(aMov[Len(aMov)],{"D3_UM"	  ,SD3->D3_UM		,NIL})
				aADD(aMov[Len(aMov)],{"D3_COD"	  ,SD3->D3_COD		,NIL})
				aADD(aMov[Len(aMov)],{"D3_DOC"	  ,SD3->D3_DOC		,NIL})
				aADD(aMov[Len(aMov)],{"D3_QUANT"  ,SD3->D3_QUANT	,NIL})
				aADD(aMov[Len(aMov)],{"D3_LOCAL"  ,SD3->D3_LOCAL	,NIL})
				aADD(aMov[Len(aMov)],{"D3_EMISSAO",SD3->D3_EMISSAO	,NIL})
				aADD(aMov[Len(aMov)],{"D3_X_TMT"  ,SD3->D3_X_TMT  	,NIL})
				aADD(aMov[Len(aMov)],{"D3_T_SF2"  ,SD3->D3_T_SF2	,NIL})
				
				SD3->(DBSkip())	
			EndDo
			
			//Inclui os novos movimentos
			For nX := 1 to Len(aMov)
				MsExecAuto({|a,b| Mata240(a,b)},aMov[nX],3)
				If lMsErroAuto
					Exit
				EndIf
			Next nX
			
			//Marca os movimentos recriados, para n�o recriara novamente
			IF !lMsErroAuto
				For nX := 1 To Len(aRec)
					SD3->(DbGoto(aRec[nX]))
					RecLock('SD3',.F.)
					SD3->D3_CHAVEF1 := 'EX'+cBusca
					SD3->(MSUnlock())
				Next Nx
			EndIf	

		ElseIf nTipo == 2 .AND. Empty(SD3->D3_CHAVEF1) // Excluir NFS
			If SD3->D3_TM == cTipMov .AND. AllTrim(SD3->D3_ESTORNO) <> 'S'
				Aadd(aEstorna,{"D3_FILIAL"	,xFilial("SD3")	,NIL})
				Aadd(aEstorna,{"D3_NUMSEQ"	,SD3->D3_NUMSEQ	,NIL})
				Aadd(aEstorna,{"D3_CHAVE"	,SD3->D3_CHAVE	,NIL})
				Aadd(aEstorna,{"D3_COD"		,SD3->D3_COD	,NIL})
				Aadd(aEstorna,{"INDEX"		,4		 		,Nil})
					
				MsExecAuto({|a,b| Mata240(a,b)},aEstorna,5)//Realiza o Estorno do Documento
			EndIf
			
		ElseIf nTipo == 2 .AND. !Empty(SD3->D3_CHAVEF1) 
			Help(,,"EstornaDCL",,'N�o � poss�vel excluir o Documento de Sa�da pois h� o Documento de Entrada associado: ' + Alltrim(SD3->D3_CHAVEF1),1,0)
			lRet := .F.
		EndIf
	Else
		If !MsgYesNo("N�o foi poss�vel localizar o movimento solicitado para efetuar o estorno. Deseja prosseguir e n�o fazer o estorno do movimento provis�rio?","EstornaDCL")
			lRet := .F.
		Endif
	EndIf
	
	If lMsErroAuto
		Mostraerro()
		lRet := .F.
	EndIf
	
	RestArea(aArea)	
	
Return lRet        

//-------------------------------------------------------------------
/*/{Protheus.doc} M330ApEst 
Fun��o para controlar o apagamento dos pares de estornos somente de datas iguais nos
movimentos internos na execu��o do recalculo (mata330) PetroBahia

@author Nilton

@param cBusca caractere, C�digo do Documento a ser Estornado 
@param nTipo, num�rico,	1 - Exclus�o do documento de entrada
							2 - Exclus�o do documento de sa�da antes do 
								recebimento do documento de entrada

/*/
//-------------------------------------------------------------------
Template Function M330ApEst(cCod,cLocal,nNumseq,dDtemi,nD3Recno)
Local lRet := .T.

dbSelectArea("SD3")
dbSetOrder(3)
cSeek:=xFilial("SD3")+cCod+cLocal+nNumseq
cCompara:="D3_FILIAL+D3_COD+D3_LOCAL+D3_NUMSEQ"
dbSeek(cSeek)
While !Eof() .And. cSeek == &(cCompara)
	If D3_ESTORNO == "S" .and. D3_EMISSAO <> dDtemi .and. nD3Recno <> recno()
	   lRet:= .F.
	Endif   
	dbSkip()
EndDo

Return lRet      

//-------------------------------------------------------------------
/*/{Protheus.doc} M330ApEst 
Fun��o para substituir o valid exist('SD3',M->D1_T_DPROV,14) com indice posicional 
para indice com nickname
@author Nilton
/*/
//-------------------------------------------------------------------
Template Function M103VlBom (cTDprov)
Local lRet := .F.

dbSelectArea("SD3")
DbOrderNickName("DCLSD3_01")
cSeek:=xFilial("SD3")+cTDprov
cCompara:="D3_FILIAL+D3_X_NF"
dbSeek(cSeek)
While !Eof() .And. cSeek == &(cCompara)
   lRet:= .T.
   Exit
   dbSkip()
EndDo

Return lRet        
