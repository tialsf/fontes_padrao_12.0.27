/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RETFUN    �Autor  �Andre Melo          � Data �  13/05/03   ���
�������������������������������������������������������������������������͹��
���Desc.     � Funcao de preenchimento do campo ao precionarmos F3        ���
�������������������������������������������������������������������������͹��
���Uso       � Template - DCL - Classif. Fiscal Automatica                ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

//�������������������������������������������������������������������������ͻ
//� Funcao de preenchimento do campo Class. Tribut. do Produto (LBV_CLASPR) � 
//�������������������������������������������������������������������������ͼ
Template Function FREF31()

Local	cRet	:= ""
Local	lRet	:= .F.
Local	cSeek	:= '"' + xFilial("LX5") + '17"'
Local	cWhile	:= "!EOF() .And. LX5_FILIAL+LX5_TABELA==" + cSeek
Local 	cTabela := "17"
Local 	cTabSX5 := "I7"

Local	nPosTip	:= 0	,;
		nPosOper:= 0	,;
		cAtual	:= ""	,;
		cOpera	:= ""

Private	oDlg, oLbx, oChk
Private	oOk     := LoadBitMap(GetResources(), "LBTIK")        	// Bitmap utilizado no Lisbox  (Marcado)
Private oNo     := LoadBitMap(GetResources(), "LBNO")			// Bitmap utilizado no Lisbox  (Desmarcado)
Private oNever  := LoadBitMap(GetResources(), "BR_VERMELHO")	// Bitmap utilizado no Lisbox  (Desabilitado)
Private	lMark	:= .F.

cAtual	:= _CCLASPRO   

//+�����������������������������������������������������������������������������+
//| Verificando se existe a tabela no LX5, sen�o, fazer a c�pia do SX5 para LX5 |                                            
//+�����������������������������������������������������������������������������+

	If ! LX5->(DbSeek(xFilial() + cTabela))
		If SX5->(DbSeek(xFilial() + cTabSX5))
			Do While xFilial("LX5") + cTabSX5 == SX5->X5_FILIAL + SX5->X5_TABELA
			dbSelectArea("LX5")
			dbSetOrder(1)
				RecLock("LX5",.T.)
				LX5->LX5_FILIAL := SX5->X5_FILIAL
				LX5->LX5_TABELA := cTabela
				LX5->LX5_CHAVE  := SX5->X5_CHAVE
				LX5->LX5_DESCRI := SX5->X5_DESCRI
				LX5->LX5_DESCSP := SX5->X5_DESCSPA
				LX5->LX5_DESCEN := SX5->X5_DESCENG
				MsUnLock()			
				SX5->(DbSkip())                                              	
			EndDo
		Endif
	Endif


DEFINE MSDIALOG oDlg TITLE "Clas.Trib.do Produto" FROM 5,30 TO 25,80

aRet	:= LocxGrid("LX5",cWhile,,.T.,".F.",cSeek,1,,{"LX5_TABELA"})
aCab	:= AClone(aRet[3])
aLin	:= AClone(aRet[5])
aTam	:= AClone(aRet[4])
aCpo	:= AClone(aRet[1])

aEval( aLin , { |x,y| aLin[y,1] := Iif( AllTrim(aLin[y,2]) $ cAtual , 1 , -1 ) })

oLbx := TwBrowse():New(022,000,000,000,,aCab,aTam,oDlg,,,,,,,,,,,,.F.,,.T.,,.F.,,,)
oLbx:nHeight	:= (__DlgHeight(oDlg)*2) - 8
oLbx:nWidth		:= __DlgWidth(oDlg)*2
oLbx:lColDrag	:= .T.
oLbx:nFreeze	:= 1
oLbx:SetArray(aLin)
oLbx:bLine		:= LocxBLin('oLbx',aCpo,.T.)
oLbx:bLDblClick	:={ || ChgMarkLb(oLbx,aLin,{|| .T. },.T.) }

ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,{|| lRet := .T.,oDlg:End()},{|| lRet := .F.,oDlg:End()},,)

If lRet
	aEval( aLin, { |x,y| cRet += Iif( aLin[y,1] == 1, Alltrim(aLin[y,2]), "" ) } )
Else
	cRet := cAtual
EndIf

Return cRet    
            
Template Function FREF32()

Local	cRet	:= ""
Local	lRet	:= .F.
Local	cSeek	:= '"' + xFilial("LX5") + '18"'
Local	cWhile	:= "!EOF() .And. LX5_FILIAL+LX5_TABELA==" + cSeek
Local 	cTabela := "18"
Local 	cTabSX5 := "I8"

Local	nPosTip	:= 0	,;
		nPosOper:= 0	,;
		cAtual	:= ""	,;
		cOpera	:= ""

Private	oDlg, oLbx, oChk
Private	oOk     := LoadBitMap(GetResources(), "LBTIK")        	// Bitmap utilizado no Lisbox  (Marcado)
Private oNo     := LoadBitMap(GetResources(), "LBNO")			// Bitmap utilizado no Lisbox  (Desmarcado)
Private oNever  := LoadBitMap(GetResources(), "BR_VERMELHO")	// Bitmap utilizado no Lisbox  (Desabilitado)
Private	lMark	:= .F.

cAtual	:= _CCLASPRO

//+�����������������������������������������������������������������������������+
//| Verificando se existe a tabela no LX5, sen�o, fazer a c�pia do SX5 para LX5 |                                            
//+�����������������������������������������������������������������������������+

	If ! LX5->(DbSeek(xFilial() + cTabela))
		If SX5->(DbSeek(xFilial() + cTabSX5))
			Do While xFilial("LX5") + cTabSX5 == SX5->X5_FILIAL + SX5->X5_TABELA
			dbSelectArea("LX5")
			dbSetOrder(1)
				RecLock("LX5",.T.)
				LX5->LX5_FILIAL := SX5->X5_FILIAL
				LX5->LX5_TABELA := cTabela
				LX5->LX5_CHAVE  := SX5->X5_CHAVE
				LX5->LX5_DESCRI := SX5->X5_DESCRI
				LX5->LX5_DESCSP := SX5->X5_DESCSPA
				LX5->LX5_DESCEN := SX5->X5_DESCENG
				MsUnLock()			
				SX5->(DbSkip())                                              	
			EndDo
		Endif
	Endif


DEFINE MSDIALOG oDlg TITLE "Clas.Trib.do Cliente" FROM 5,30 TO 25,80

aRet	:= LocxGrid("LX5",cWhile,,.T.,".F.",cSeek,1,,{"LX5_TABELA"})
aCab	:= AClone(aRet[3])
aLin	:= AClone(aRet[5])
aTam	:= AClone(aRet[4])
aCpo	:= AClone(aRet[1])

aEval( aLin , { |x,y| aLin[y,1] := Iif( AllTrim(aLin[y,2]) $ cAtual , 1 , -1 ) })

oLbx := TwBrowse():New(022,000,000,000,,aCab,aTam,oDlg,,,,,,,,,,,,.F.,,.T.,,.F.,,,)
oLbx:nHeight	:= (__DlgHeight(oDlg)*2) - 8
oLbx:nWidth		:= __DlgWidth(oDlg)*2
oLbx:lColDrag	:= .T.
oLbx:nFreeze	:= 1
oLbx:SetArray(aLin)
oLbx:bLine		:= LocxBLin('oLbx',aCpo,.T.)
oLbx:bLDblClick	:={ || ChgMarkLb(oLbx,aLin,{|| .T. },.T.) }

ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,{|| lRet := .T.,oDlg:End()},{|| lRet := .F.,oDlg:End()},,)

If lRet
	aEval( aLin, { |x,y| cRet += Iif( aLin[y,1] == 1, Alltrim(aLin[y,2]), "" ) } )
Else
	cRet := cAtual
EndIf

Return cRet    


//������������������������������������������������������������ͻ
//� Funcao de preenchimento do campo Mensagens  (LBV_MENS)     � 
//������������������������������������������������������������ͼ
Template Function FREF33()

Local	cRet	:= ""
Local	lRet	:= .F.
Local	cSeek	:= '"' + xFilial("SM4") + '"'
Local	cWhile	:= "!EOF() .And. M4_FILIAL==" + cSeek

Local	nPosTip	:= 0	,;
		nPosOper:= 0	,;
		cAtual	:= ""	,;
		cOpera	:= ""

Private	oDlg, oLbx, oChk
Private	oOk     := LoadBitMap(GetResources(), "LBTIK")        	// Bitmap utilizado no Lisbox  (Marcado)
Private oNo     := LoadBitMap(GetResources(), "LBNO")			// Bitmap utilizado no Lisbox  (Desmarcado)
Private oNever  := LoadBitMap(GetResources(), "BR_VERMELHO")	// Bitmap utilizado no Lisbox  (Desabilitado)
Private	lMark	:= .F.

cAtual	:= M->LBV_MENS

DEFINE MSDIALOG oDlg TITLE "Mensagens" FROM 5,30 TO 35,80

aRet	:= LocxGrid("SM4",cWhile,,.T.,".F.",cSeek,1)
aCab	:= AClone(aRet[3])
aLin	:= AClone(aRet[5])
aTam	:= AClone(aRet[4])
aCpo	:= AClone(aRet[1])

aEval( aLin , { |x,y| aLin[y,1] := Iif( aLin[y,2] $ cAtual , 1 , -1 ) })

oLbx := TwBrowse():New(022,000,000,000,,aCab,aTam,oDlg,,,,,,,,,,,,.F.,,.T.,,.F.,,,)
oLbx:nHeight	:= (__DlgHeight(oDlg)*2) - 20
oLbx:nWidth		:= __DlgWidth(oDlg)*2
oLbx:lColDrag	:= .T.
oLbx:nFreeze	:= 1
oLbx:SetArray(aLin)
oLbx:bLine		:= LocxBLin('oLbx',aCpo,.T.)
oLbx:bLDblClick	:={ || ChgMarkLb(oLbx,aLin,{|| .T. },.T.) }

ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,{|| lRet := .T.,oDlg:End()},{|| lRet := .F.,oDlg:End()},,)

If lRet
	aEval( aLin, { |x,y| cRet += Iif( aLin[y,1] == 1, Alltrim(aLin[y,2]), "" ) } )
Else
	cRet := cAtual
EndIf

Return cRet    


