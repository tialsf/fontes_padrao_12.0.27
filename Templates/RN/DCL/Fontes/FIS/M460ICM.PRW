/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Funcao    � M460ICM  � Autor � Irina Sanches Pires    � Data � 17.12.03 ���
��������������������������������������������������������������������������Ĵ��
���Descricao � Ponto de entrada da preparacao da nota fiscal para calculo  ���
���          � do ICMS PP cfe tabela de Aliquotas especifica               ���
���          � Variaveis de retorno : _ALIQICM                             ���
���          �                        _BASEICM                             ���
���          �                        _VALICM                              ���
���          � Tabelas SF4, SC5 e SC6 ja estao posicionadas                ���
��������������������������������������������������������������������������Ĵ��
��������������������������������������������������������������������������Ĵ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/

#include "rwmake.ch"

Template Function M460ICM()

Local _nValFecp	 	:= 0
Local _nAlqFecp		:= 0
//Devera usar a funcao HasTemplate ao inves de se usar o ChkTemplate. 
//Como esse programa eh um ponto de entrada do pedido de venda padrao, 
//ele sera executado mesmo se o cliente nao possui licen�a de uso.
//Ex.: Cliente possui 2 empresas, porem compra apenas 1 licenca de template.
//Nesse caso, a empresa que nao possui licenca nao conseguirar executar um pedido
//de venda caso utilize o mesmo RPO da empresa que possui a licenca.
If HasTemplate("DCLFIS") 
	SetPrvt("_area,_aAreaSA1, _AareaSB1, _aAreaSF4")
	SetPrvt("_lPedDCL")
	
	_aArea    := GetArea()
	_aAreaSA1 := SA1->(GetArea())
	_aAreaSB1 := SB1->(GetArea())
	_aAreaSF4 := SF4->(GetArea())
	
	_aAliasSC6	:= PARAMIXB[1]

	If _lPedDCL == Nil
		_lPedDCL := .F.
	EndIf

	If !(_lPedDCL) //tem SC5, gravar o pedido. 
		_cProduto 	:= SC6->C6_PRODUTO
		_cXTpop	:= SC6->C6_X_TPOP
		_cCliente	:= SC5->C5_CLIENTE                 	
		_cLoja 	:= SC5->C5_LOJACLI
		_cTipo 	:= SC5->C5_TIPO
		_cXCTcli 	:= SC5->C5_X_CTCLI     	
	else //ainda n�o tem SC5 para filtrar, ent�o trabalho com a mem�ria / apresentar a planilha financeira.
		
		aHeader 	:= PARAMIXB[2]
		_nPosProd 	:= aScan(aHeader,{|x| AllTrim(x[2])=="C6_PRODUTO"})
		_nPosTpOp	:= aScan(aHeader,{|x| AllTrim(x[2])=="C6_X_TPOP"})
		
		_cProduto 	:= _aAliasSC6[_nPosProd]
		_cXTpop	:= _aAliasSC6[_nPosTpOp]
		_cCliente	:= M->C5_CLIENTE
		_cLoja 	:= M->C5_LOJACLI
		_cTipo 	:= M->C5_TIPO
		_cXCTcli 	:= M->C5_X_CTCLI		
	endif

	/*
	//�������������������������������������������������������������������������Ŀ
	//�Calculando o valor do ICMS Proprio com Base na Tabela de Aliquotas - LBX �
	//�para empresas que comercializam combustivel.                             �
	//���������������������������������������������������������������������������
	*/
	
	//VERIFICA SE POSSUI INCIDENCIA DE ICMS PROPRIO PARA PRODUTOS REFINADOS
		
		SA1->(dbSetOrder(1))
		SA1->(msSeek(xFilial("SA1")+_cCliente+_cLoja))
		SB1->(dbSetOrder(1))
		SB1->(msSeek(xFilial("SB1")+_cProduto))
	
		//busca a aliquota e calcula o valor do icms proprio
		Aliquota(@_nValFecp,@_nAlqFecp)
		
	SA1->(RestArea(_aAreaSA1))
	SB1->(RestArea(_aAreaSB1))
	SF4->(RestArea(_aAreaSF4))
	RestArea(_aArea)
EndIf	
Return ({_nValFecp,_nAlqFecp})

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ĵ��
���Fun�ao    � Irina S. Pires - Busca Aliquota do ICMS PP na Tabela LBX   o���
���21/08/01  � fun��o ALIQUOTA                                             ���
��������������������������������������������������������������������������Ĵ��
���Uso       � RDMAKE                                                      ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/

Static Function Aliquota(_nValFecp,_nAlqFecp)

Local nMV_ICMPAD 	:=GetMv('MV_ICMPAD')
Local _cMvEstado  	:= Alltrim(GetNewPar("MV_ESTADO","")) 
Local _lFecp 	 	:= .T. 
Local _lInscrito 	:= .T.
Local _cA1Tipo 		:= SA1->A1_TIPO 

   

If _cTipo $ "D/B" //Devolucao de Compra de Fornecedor
	_cEst := SA2->A2_EST
Else
	_cEst := SA1->A1_EST
EnDif  

//������������������������������������
// Verifica se o cliente � inscrito  �						
//������������������������������������
If	SA1->A1_CONTRIB == "1" .And. SA1->A1_TPJ == "3" .And. Empty(SA1->A1_INSCR)    
	_lInscrito 	:= 	.F.
Endif

//�����������������������������������������������������������������������������������������������������Ŀ
//� Validacoes para ICMS Proprio:				 														�
//�																										�
//�-> Nao majorar em Operacoes Internas quando o campo CFC_FCPINT = Nao									�
//�-> Nao majorar em Devolucoes, pois a MATXFIS busca a aliquota do documento original					�
//�-> Nao majorar em Operacoes Interestaduais de Entrada, porque n�o tem cadastro de DCL para entrada	�
//�-> Nao majorar em Operacoes Interestaduais de Saida, exceto destinadas a Consumidor Final. 			�
//�������������������������������������������������������������������������������������������������������
If _lFecp .And.		(;
						( _cEst == _cMvEstado .And. SF4->F4_ISEFECP == "1" .And. CFC->CFC_FCPINT $ "2") .Or.;
						( _cTipo $ "D" ) .Or.;
						( _cEst <> _cMvEstado  .And. _cA1Tipo <> "F" );
					)
	
	_lFecp		:=	.F.
Endif


//����������������������������
// BUSCA ALIQUOTA DO ICMS PP �
//����������������������������
dbSelectArea("LBX")
dbSetOrder(2)
dbSeek(xFilial("LBX")+_cXTpop)
If !Eof()
   Do While !eof() .and. LBX_FILIAL = xFilial("LBX") .and. LBX_TPOP == _cXTpop
     	//verificando os registros validos (data) e qual o tipo de operacao valido.
	  If  dtos(dDataBase) >= dtos(LBX->LBX_DATADE) .and.;
		  dtos(dDataBase) <= dtos(LBX->LBX_DATAAT) .and.;
		  SB1->B1_X_CT $ LBX->LBX_PROD .and.;
		  _cEst $ LBX->LBX_UF .and.;
		  _cXCTcli $ LBX->LBX_CTCLI                  
 
       	  _ALIQICM := 0 
		  _ALIQICM   := LBX->LBX_ICMSPP 
		  
		  exit
	  Endif
	  dbSkip()
   EnDdo
Else
   _ALIQICM   := If ( SB1->B1_PICM == 0 , nMV_ICMPAD ,  SB1->B1_PICM )
Endif 

//���������������������������������������������������������������������������������������������������������������������������������Ŀ
//� 														ICMS Proprio			 												�
//�Nas operacoes interestaduais destinadas a nao contribuintes, devo utilizar a majoracao com aliquota do estado de origem.			�  
//����������������������������������������������������������������������������������������������������������������������������������� 

If _lFecp 
	If 	CFC->(dbSeek(xFilial("CFC")+_cMvEstado+_cEst+_cProduto))
   		If _cEst <> _cMvEstado .And. !_lInscrito // Fecp na Origem
	   		_nAlqFecp	:=   CFC->CFC_ALFCPO 
	   		_ALIQICM 	:=  _ALIQICM + _nAlqFecp
	 	Else
	   		_nAlqFecp  	:=   CFC->CFC_ALQFCP  // Fecp no Destino
			_ALIQICM 	:=  _ALIQICM + _nAlqFecp
		Endif	
	Endif
Endif		

//�������������������������������������������������Ŀ
//       Calcula a base do ICMS pr�prio 		    �
//�������������������������������������������������Ŀ
If _BASEICM <> 0	
	_VALICM    := _BASEICM * (_ALIQICM/100)
Endif

If _lFecp
 	_nValFecp 	:= _BASEICM * ( _nAlqFecp / 100 )
Endif


Return