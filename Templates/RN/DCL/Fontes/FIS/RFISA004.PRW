/*/
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � RFISA004 � Autor � Irina Sanches Pires� Data �  13/08/2003 ���
�������������������������������������������������������������������������͹��
���Descri��o � Utilizado para calcular o Valor de ICMS a complementar do   ��
���          � Diesel a ser destacado na mensagem de Nota Fiscal          ���
���          � Efetua o Calculo do ICMS Repassado                         ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico Total Distribuidora                             ���
�������������������������������������������������������������������������͹��
���Arquivos  � SM4 - Cadastro de Formulas                                 ���
���          � LBV - Cadastro de Tes Inteligente                          ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
#include "rwmake.ch"
User Function RFISA004()

SetPrvt("_CALIAS,_NINDEX,_NRECNO,_cMens,_nCompl")

_cAlias := Alias()
_nIndex := IndexOrd()
_nRecno := Recno()

_cMens    := _cClas := ""
_nCompl   := 0 

//********************************************************************************* //
//acumula os valores de ICMS ST de Origem e Destino para serem utilizados nos       //
//calculos de do ICMS complementar e ICMS a ser repassado                           //
//********************************************************************************* //
Sb1->(dbSeek(xFilial("SB1")+SD2->D2_COD))
_cClas    := SB1->B1_X_CT

dbSelectArea("SD2")
_cDoc      := SD2->D2_DOC
_cSerie    := SD2->D2_SERIE
_nIcmsretO := _nIcmsretD :=  0 
//_cCod      := SD2->D2_COD

//grava ambiente 
_cAlias1  := Alias()
_nIndex1  := IndexOrd()
_nRecno1  := Recno()

dbSetOrder(3)
do While !eof() .and. SD2->D2_FILIAL == SM0->M0_CODFIL .AND. SD2->D2_DOC == _cDoc .AND. SD2->D2_SERIE == _cSerie //;
	//.and. SD2->D2_COD == _cCod
	if SB1->B1_X_CT  == _cClas   //pertence ao mesmo grupo de produtos
		_nIcmsretO += SD2->D2_ICMRETO
		_nIcmsretD += SD2->D2_ICMRETD
    endif 
	dbSelectArea("SD2")
	dbSkip()
	Sb1->(dbSeek(xFilial("SB1")+SD2->D2_COD))
	Loop
enDdo

//retorna ambiente 
DbSelectArea(_cAlias1)
DbSetOrder(_nIndex1)
DbGoTo(_nRecno1)
//

_nCompl  := round((_nIcmsretD - _nIcmsretO),2)
if _nCompl <= 0
   _cMens := " ICMS REPASSADO R$ " + str(_nIcmsretD,10,2)
else
   _cMens := " VLR.COMPLEMENTAR OLEO DIESEL R$" + str(_nCompl,8,2) + " ICMS REPASSADO R$ " + str(_nIcmsretD,10,2)
enDif    

DbSelectArea(_cAlias)
DbSetOrder(_nIndex)
DbGoTo(_nRecno)

Return(_cMens)