/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � RFISA008 � Autor � Totvs                      �  18/02/2008���
�������������������������������������������������������������������������͹��
���Descri��o � Utilizado para calcular a Base e o Valor do ICMS ST da QAV- ��
���          � JET - FULL                                                 ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico Total Distribuidora                             ���
�������������������������������������������������������������������������͹��
���Arquivos  � SM4 - Cadastro de Formulas                                 ���
���          � LBV - Cadastro de Tes Inteligente                          ���
�������������������������������������������������������������������������ͼ��
���Update    �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
#include "rwmake.ch"

User Function RFISA008()

SetPrvt("_cAlias,_nIndex,_nRecno,cMens")

_cAlias := Alias()
_nIndex := IndexOrd()
_nRecno := Recno()

lNewTab := .T.

//variaveis Genericas
cMens     := _cClastb := _cTpop    := _cClas    := _cTpCli := ""
_nQuant   := _nValor  := _nBaseDes := _nBaseOri := 0
_nComp    := _nCompD  := 1

//variaveis de Origem
_nPrOri   := _nBaseO  := _nIcmO  := _nAliqSTO :=  _nMVAO  := _nPMPFO := 0

//variaveis de Destino
_nPrDes   := _nBaseD  := _nIcmD  := _nAliqSTD :=  _nMVAD  := _nPMPFD := 0

cOriDest := ""

cAliasSD2 := Iif (type("PARAMIXB")=="U","SD2",PARAMIXB[1])

If Empty(cAliasSD2)
   Return(cMens)
Endif

SD2->(dbSetOrder(3)) //D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM
If !SD2->(MsSeek(xFilial("SD2")+(cAliasSD2)->(D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM)))
	Return(cMens)
Endif

nQuantQav := GetNewPar("MV_QUANTQA",1) // Propor��o do querosene de avia��o

dbSelectArea("SB1")
dbSetOrder(1)
If dbSeek(xFilial("SB1")+SD2->D2_COD)
	_cClastb := SB1->B1_X_CT //classificacao tributaria do cliente
	_cTpCli	 := SB1->B1_TIPO
Else
	MsgAlert(" Produto nao Cadastrado! ")
Endif

//��������������������������������������������������������������Ŀ
//� busca tipo de opera��o no Pedido de Vendas para busca das    �
//� aliquotas de ICMS PP e ST                                    �
//����������������������������������������������������������������
dbSelectArea("SC6")
dbSetOrder(1)
If dbSeek(xFilial("SC6")+SD2->D2_PEDIDO+SD2->D2_ITEMPV+SD2->D2_COD)
	_cTpop := SC6->C6_X_TPOP
Endif

SX3->(dbSetOrder(2))
If !SX3->(dbSeek("LLA_PROD"))
	If (GetVersao(.F.)=="11" .Or. GetRpoRelease()<"12.1.016")
		lNewTab := .F.
	Else
		Final("Ambiente desatualizado. Entre em contato com o suporte.")
	Endif
Endif

//�����������������������������������������������������������������������Ŀ
//� busca das aliquotas de ICMS PP e ST de destino - Venda Interestadual  �
//�������������������������������������������������������������������������
dbSelectArea("LBX")
dbSetOrder(2) //LBX_FILIAL+LBX_TPOP
If dbSeek(xFilial("LBX")+_cTpop)
	While !Eof() .And. LBX->LBX_FILIAL == xFilial("LBX") .And. LBX->LBX_TPOP == _cTpop
		//verificando os registros validos (data) e qual o tipo de operacao valido.
		If SD2->D2_EMISSAO >= LBX->LBX_DATADE .And. SD2->D2_EMISSAO <= LBX->LBX_DATAAT .And.;
			_cClastb       $ LBX->LBX_PROD .And.;
			SA1->A1_EST    $ LBX->LBX_UF .And.;
			SA1->A1_X_CT   $ LBX->LBX_CTCLI
			//Busca as aliquotas de ICMS proprio e substituto
			_nAliqSTD := LBX->LBX_ICMSST
			Exit
		endif
		dbSkip()
	EndDo
Else
	MsgAlert ("Produto / UF / Tipo de Operacao nao possuem Aliquotas Cadastradas! ")
Endif

If lNewTab
	//�����������������������������������������������������������Ŀ
	//� busca da MVA e PMPF de destino - Venda Interestadual      �
	//�������������������������������������������������������������
	dbSelectArea("LLB")
	dbSetOrder(1) //LLB_FILIAL+LLB_UF+LLB_PROD+LLB_TPOP
	If dbSeek(xFilial("LLB")+SA1->A1_EST+_cClastb+_cTpop)
		While !Eof() .And. LLB->LLB_FILIAL == xFilial("LLB") .And. LLB->LLB_PROD == _cClastb .And.;
			SA1->A1_EST == LLB->LLB_UF .And. LLB->LLB_TPOP == _cTpop
			//verificando os registros validos (data) e qual o tipo de operacao valido.
			If SD2->D2_EMISSAO >= LLB->LLB_DATADE .And. SD2->D2_EMISSAO <= LLB->LLB_DATAAT
				If LLB->LLB_PMPF > 0
					_nPMPFD := LLB->LLB_PMPF
				Else
					If SM0->M0_ESTENT == SA1->A1_EST //Venda Interna
						_nMVAD := LLB->LLB_MVAI/100 // MVA Interna
					Else
						_nMVAD := LLB->LLB_MVAE/100 // MVA Interestadual
					Endif
				Endif
				Exit
			Endif
			dbSkip()
		EndDo
	Else
		MsgAlert("Produto / UF nao cadastrados!")
	Endif
	//�����������������������������������������������������������������Ŀ
	//� busca preco de refinaria no UF de Destino - Venda Interestadual �
	//�������������������������������������������������������������������
	If Findfunction("T_cFA009PBASE")
		_nBaseDes:=T_CFA009PBASE(xFilial("LLA"),SA1->A1_EST,_cClastb,SD2->D2_COD,SD2->D2_EMISSAO)
		If _nBaseDes<=0
			If _nMVAD > 0
				MsgAlert("Preco Base -> UF Destino / Produto nao cadastrados! ")
			Endif
		Endif
	Else
		dbSelectArea("LLA")
		dbSetOrder(1)
		If dbSeek(xFilial("LLA")+SA1->A1_EST+_cClastb)
			While !Eof() .And. LLA->LLA_FILIAL == xFilial("LLA") .And. LLA->LLA_PROD == _cClastb .And.;
				SA1->A1_EST == LLA->LLA_UF
				//verificando os registros validos (data) e qual o tipo de operacao valido.
				If SD2->D2_EMISSAO >= LLA->LLA_DATADE .And. SD2->D2_EMISSAO <= LLA->LLA_DATAAT
					_nBaseDes := LLA->LLA_PRBASE //preco base
					Exit
				Endif
				dbSkip()
			EndDo
		Elseif _nMVAD > 0
			MsgAlert("Preco Base -> UF Destino / Produto nao cadastrados! ")
		Endif
	Endif
Else
	//�����������������������������������������������������������Ŀ
	//� busca da MVA e PMPF de destino - Venda Interestadual      �
	//�������������������������������������������������������������
	dbSelectArea("LEZ")
	dbSetOrder(1) //LEZ_FILIAL+LEZ_UF+LEZ_PROD+LEZ_TPOP
	If dbSeek(xFilial("LEZ")+SA1->A1_EST+_cClastb+_cTpop)
		While !Eof() .And. LEZ->LEZ_FILIAL == xFilial("LEZ") .And. LEZ->LEZ_PROD == _cClastb .And.;
			SA1->A1_EST == LEZ->LEZ_UF .And. LEZ->LEZ_TPOP == _cTpop
			//verificando os registros validos (data) e qual o tipo de operacao valido.
			If SD2->D2_EMISSAO >= LEZ->LEZ_DATADE .And. SD2->D2_EMISSAO <= LEZ->LEZ_DATAAT
				If LEZ->LEZ_PMPF > 0
					_nPMPFD := LEZ->LEZ_PMPF
				Else
					If SM0->M0_ESTENT == SA1->A1_EST //Venda Interna
						_nMVAD := LEZ->LEZ_MVAI/100 // MVA Interna
					Else
						_nMVAD := LEZ->LEZ_MVAE/100 // MVA Interestadual
					Endif
				Endif
				Exit
			Endif
			dbSkip()
		EndDo
	Else
		MsgAlert("Produto / UF nao cadastrados!")
	Endif
	//�����������������������������������������������������������������Ŀ
	//� busca preco de refinaria no UF de Destino - Venda Interestadual �
	//�������������������������������������������������������������������
	If Findfunction("T_cFA009PBASE")
		_nBaseDes:=T_CFA009PBASE(xFilial("LEY"),SA1->A1_EST,_cClastb,SD2->D2_COD,SD2->D2_EMISSAO)
		If _nBaseDes<=0
			If _nMVAD > 0
				MsgAlert("Preco Base -> UF Destino / Produto nao cadastrados! ")
			Endif
		Endif
	Else
		dbSelectArea("LEY")
		dbSetOrder(1)
		If dbSeek(xFilial("LEY")+SA1->A1_EST+_cClastb)
			While !Eof() .And. LEY->LEY_FILIAL == xFilial("LEY") .And. LEY->LEY_PROD == _cClastb .And.;
				SA1->A1_EST == LEY->LEY_UF
				//verificando os registros validos (data) e qual o tipo de operacao valido.
				If SD2->D2_EMISSAO >= LEY->LEY_DATADE .And. SD2->D2_EMISSAO <= LEY->LEY_DATAAT
					_nBaseDes := LEY->LEY_PRBASE //preco base
					Exit
				Endif
				dbSkip()
			EndDo
		Elseif _nMVAD > 0
			MsgAlert("Preco Base -> UF Destino / Produto nao cadastrados! ")
		Endif
	Endif
Endif

//Venda Interestadual
If SM0->M0_ESTENT <> SA1->A1_EST
	//���������������������������������������������������������������������Ŀ
	//� busca das aliquotas de ICMS PP e ST de origem - venda interestadual �
	//�����������������������������������������������������������������������
	dbSelectArea("LBX")
	dbSetOrder(2) //LBX_FILIAL+LBX_TPOP
	If dbSeek(xFilial("LBX")+_cTpop)
		While !Eof() .And. LBX->LBX_FILIAL = xFilial("LBX") .And. LBX->LBX_TPOP == _cTpop
			//verificando os registros validos (data) e qual o tipo de operacao valido.
			If SD2->D2_EMISSAO >= LBX->LBX_DATADE .And. SD2->D2_EMISSAO <= LBX->LBX_DATAAT .And.;
				_cClastb        $ LBX->LBX_PROD .And.;
				SA1->A1_X_CT    $ LBX->LBX_CTCLI
				//SM0->M0_ESTENT  $ LBX->LBX_UF .And.
				
				//Busca as aliquotas de ICMS proprio e substituto
				_nAliqSTO := LBX->LBX_ICMSST
				exit
			Endif
			dbSkip()
		EndDo
	Else
		MsgAlert ("Produto / UF / Tipo de Operacao nao possuem Aliquotas Cadastradas! ")
	Endif

	If lNewTab
		//���������������������������������������������������������Ŀ
		//� busca da MVA e PMPF de origem - venda interestadual     �
		//�����������������������������������������������������������
		dbSelectArea("LLB")
		dbSetOrder(1) //LLB_FILIAL+LLB_UF+LLB_PROD+LLB_TPOP
		If dbSeek(xFilial("LLB")+SA1->A1_EST+_cClastb+_cTpop)
			While !Eof() .And. LLB->LLB_FILIAL == xFilial("LLB") .And. LLB->LLB_PROD == _cClastb .And.;
				/*SM0->M0_ESTENT == LLB->LLB_UF .And.*/ LLB->LLB_TPOP == _cTpop
				//verificando os registros validos (data) e qual o tipo de operacao valido.
				If SD2->D2_EMISSAO >= LLB->LLB_DATADE .And. SD2->D2_EMISSAO <= LLB->LLB_DATAAT
					If LLB->LLB_PMPF > 0
						_nPMPFO := LLB->LLB_PMPF
					Else
						_nMVAO  := LLB->LLB_MVAI/100 //MVA Interna
					Endif
					Exit
				Endif
				dbSkip()
			EndDo
		Else
			MsgAlert("Produto / UF nao cadastrados! ")
		Endif
		//��������������������������������������������������������������������Ŀ
		//� busca preco de refinaria no UF de Origem - venda interestadual     �
		//����������������������������������������������������������������������
		If Findfunction("T_cFA009PBASE")
			_nBaseOri:=T_CFA009PBASE(xFilial("LLA"),SA1->A1_EST,_cClastb,SD2->D2_COD,SD2->D2_EMISSAO)
			If _nBaseOri<=0
				If _nMVAO > 0
					MsgAlert("Preco Base -> UF Origem / Produto nao cadastrados! ")
				Endif
			EnDif
		Else
			dbSelectArea("LLA")
			dbSetOrder(1)
			If dbSeek(xFilial("LLA")+SM0->M0_ESTENT+_cClastb)
				While !Eof() .And. LLA->LLA_FILIAL == xFilial("LLA") .And. LLA->LLA_PROD == _cClastb .And.;
					SM0->M0_ESTENT == LLA->LLA_UF
					//verificando os registros validos (data) e qual o tipo de operacao valido.
					If SD2->D2_EMISSAO >= LLA->LLA_DATADE .And. SD2->D2_EMISSAO <= LLA->LLA_DATAAT
						_nBaseOri := LLA->LLA_PRBASE //preco base
						Exit
					Endif
					dbSkip()
				EndDo
			Elseif _nMVAO > 0
				MsgAlert("Preco Base -> UF Destino / Produto nao cadastrados! ")
			Endif
		Endif
	Else
		//���������������������������������������������������������Ŀ
		//� busca da MVA e PMPF de origem - venda interestadual     �
		//�����������������������������������������������������������
		dbSelectArea("LEZ")
		dbSetOrder(1) //LEZ_FILIAL+LEZ_UF+LEZ_PROD+LEZ_TPOP
		If dbSeek(xFilial("LEZ")+SA1->A1_EST+_cClastb+_cTpop)
			While !Eof() .And. LEZ->LEZ_FILIAL == xFilial("LEZ") .And. LEZ->LEZ_PROD == _cClastb .And.;
				/*SM0->M0_ESTENT == LEZ->LEZ_UF .And.*/ LEZ->LEZ_TPOP == _cTpop
				//verificando os registros validos (data) e qual o tipo de operacao valido.
				If SD2->D2_EMISSAO >= LEZ->LEZ_DATADE .And. SD2->D2_EMISSAO <= LEZ->LEZ_DATAAT
					If LEZ->LEZ_PMPF > 0
						_nPMPFO := LEZ->LEZ_PMPF
					Else
						_nMVAO  := LEZ->LEZ_MVAI/100 //MVA Interna
					Endif
					Exit
				Endif
				dbSkip()
			EndDo
		Else
			MsgAlert("Produto / UF nao cadastrados! ")
		Endif
		//��������������������������������������������������������������������Ŀ
		//� busca preco de refinaria no UF de Origem - venda interestadual     �
		//����������������������������������������������������������������������
		If Findfunction("T_cFA009PBASE")
			_nBaseOri:=T_CFA009PBASE(xFilial("LEY"),SA1->A1_EST,_cClastb,SD2->D2_COD,SD2->D2_EMISSAO)
			If _nBaseOri<=0
				If _nMVAO > 0
					MsgAlert("Preco Base -> UF Origem / Produto nao cadastrados! ")
				Endif
			EnDif
		Else
			dbSelectArea("LEY")
			dbSetOrder(1)
			If dbSeek(xFilial("LEY")+SM0->M0_ESTENT+_cClastb)
				While !Eof() .And. LEY->LEY_FILIAL == xFilial("LEY") .And. LEY->LEY_PROD == _cClastb .And.;
					SM0->M0_ESTENT == LEY->LEY_UF
					//verificando os registros validos (data) e qual o tipo de operacao valido.
					If SD2->D2_EMISSAO >= LEY->LEY_DATADE .And. SD2->D2_EMISSAO <= LEY->LEY_DATAAT
						_nBaseOri := LEY->LEY_PRBASE //preco base
						Exit
					Endif
					dbSkip()
				EndDo
			Elseif _nMVAO > 0
				MsgAlert("Preco Base -> UF Destino / Produto nao cadastrados! ")
			Endif
		Endif
	Endif
Endif

/*
��� Memoria de calculo Opera��o Interna / Interestadual - PMPF E MVA
���
��� MVA: Pauta = (Pre�o Base / 100-Al�quota Interna)*MVA
��� PMPF: considera o PMPF informado no cadastro de MVA / PMPF
���
��� Detalhe da mensagem impressa no DANFE em informa��es complementares
���
��� BC ST = pauta * Quantidade
��� ICMS  = BC ST * Al�quota Interna
���
*/

if _nPMPFD > 0
	If _cTpCli == "MP"
		_nPrDes := (_nPMPFD / nQuantQav)
	Else
		_nPrDes := _nPMPFD
	Endif
Else
	_nPrDes := (_nBaseDes/((100-_nAliqSTD)/100)) * (1+_nMVAD)
Endif

if SM0->M0_ESTENT <> SA1->A1_EST
	if _nPMPFO > 0
		If _cTpCli == "MP"
			_nPrOri := (_nPMPFO  / nQuantQav)
		Else
			_nPrOri := _nPMPFO
		Endif
	Else
		_nPrOri := (_nBaseOri/((100-_nAliqSTO)/100)) * (1+_nMVAO)
	Endif
Endif

//********************************************************************************* //
//acumula as quantidades do memso produto, caso a nota possua mais de um item com o //
//mesmo codigo de produto (casos de condi��o de pagto diferenciadas). Os calculos   //
//dos impostos deve, ser efetuados com Base na qunatidade total do mesmo produto    //

//********************************************************************************* //
Sb1->(dbSeek(xFilial("SB1")+SD2->D2_COD))
_cClas := SB1->B1_X_CT

if SB1->B1_X_CT == _cClas //pertence ao mesmo grupo de produtos
	_nQuant += SD2->D2_QUANT
	_nValor += SD2->D2_TOTAL+SD2->D2_ICMSRET+SD2->D2_VALIPI

		Reclock("SD2",.F.)
		If SA1->A1_X_CT $ alltrim(GetMV("MV_X_CTCLI")) //cliente consumidor
			SD2->D2_BRICMSD := Round((SD2->D2_TOTAL+SD2->D2_ICMSRET+SD2->D2_VALIPI)* _nComp,2)
			SD2->D2_ICMRETD := Round(((SD2->D2_TOTAL+SD2->D2_ICMSRET+SD2->D2_VALIPI)* _nComp) * (_nAliqSTD/100),2)
		else  //cliente revendedor
			SD2->D2_BRICMSD := Round(_nPrDes*(SD2->D2_QUANT * _nComp),2)
			SD2->D2_ICMRETD := Round((_nPrDes*(SD2->D2_QUANT * _nComp))* (_nAliqSTD/100),2)
		endif

		if !Empty(_nPrDes)
			If _cTpCli == "MP" .Or. _nPMPFD > 0
				_nBaseD := round(_nPrDes*(_nQuant),2)
			Else
				_nBaseD := round(_nPrDes*(_nQuant * nQuantQav),2)
			endif
			_nIcmD := round(_nBaseD * (_nAliqSTD/100),2)

			SD2->D2_BRICMSD := _nBaseD
			SD2->D2_ICMRETD := _nIcmD
			SD2->D2_ALIQSOL := nAliqSTD
		endif

		If SM0->M0_ESTENT <> SA1->A1_EST
			If _cTpCli == "MP" .Or. _nPMPFO > 0
				_nBaseO := round(_nPrOri*(_nQuant),2)
			Else
				_nBaseO := round(_nPrOri*(_nQuant * nQuantQav),2)
			Endif
			_nIcmO := round(_nBaseO * (_nAliqSTO/100),2)

			SD2->D2_BRICMSO := _nBaseO
			SD2->D2_ICMRETO := _nIcmO
			SD2->D2_ALIQSOL := _nAliqSTO
		Else
			if !Empty(_nPrDes)
				If _cTpCli == "MP" .Or. _nPMPFD > 0
					_nBaseD := round(_nPrDes*(_nQuant),2)
				Else
					_nBaseD := round(_nPrDes*(_nQuant * nQuantQav),2)
				endif
			endif
			_nIcmD := round(_nBaseD * (_nAliqSTD/100),2)
			SD2->D2_BRICMSO := nBaseD
			SD2->D2_ICMRETO := nIcmD
			SD2->D2_ALIQSOL := nAliqSTD
		Endif
		MsUnLock()

enDif

/*
��� -- IMPRESSAO DE MENSAGEM NO DANFE
��� Memoria de Calculo para demonstrar a mensagem
��� Preco ou Pauta = (Preco Base / 100-Aliquota Interna)*MVA*Quantidade*Aliquota Interna
��� BC ST = pauta * Quantidade
��� ICMS  = BC ST * Aliquota Interna
*/

//Mensagem de Origem
If SM0->M0_ESTENT <> SA1->A1_EST
	If _cTpCli == "MP" .Or. _nPMPFO > 0
		_nBaseO := round(_nPrOri*(_nQuant),2)
	Else
		_nBaseO := round(_nPrOri*(_nQuant * nQuantQav),2)
	Endif
	cMens  := cMens + " QAV-BC ST ORIG =R$ " + Alltrim(Transform(SD2->D2_BRICMSO,"@E 999,999,999.99"))
	_nIcmO := round(_nBaseO * (_nAliqSTO/100),2)
	cMens  := cMens + " ICMS ST ORIG =R$ " + Alltrim(Transform(SD2->D2_ICMRETO,"@E 999,999,999.99"))
Endif

	//Mensagem de Destino
	if !Empty(_nPrDes)
		If _cTpCli == "MP" .Or. _nPMPFD > 0
			_nBaseD := round(_nPrDes*(_nQuant),2)
		Else
			_nBaseD := round(_nPrDes*(_nQuant * nQuantQav),2)
		endif
	endif

	If SM0->M0_ESTENT <> SA1->A1_EST
		cOriDest := "DEST"
	Else
		cOriDest := "ORIG"
	EndIf

	If !Empty(cMens)
		cMens += " / "
	EndIf

	If SM0->M0_ESTENT <> SA1->A1_EST
		cMens  := cMens + " QAV-BC ST " +cOriDest+" =R$ " + Alltrim(Transform(_nBaseD,"@E 999,999,999.99"))
		_nIcmD := round(_nBaseD * (_nAliqSTD/100),2)
		cMens  := cMens + " ICMS ST " +cOriDest+" =R$ " + Alltrim(Transform(_nIcmD,"@E 999,999,999.99"))
	Else
		cMens  := cMens + " QAV-BC ST " +cOriDest+" =R$ " + Alltrim(Transform(SD2->D2_BRICMSD,"@E 999,999,999.99"))
		cMens  := cMens + " ICMS ST " +cOriDest+" =R$ " + Alltrim(Transform(SD2->D2_ICMRETD,"@E 999,999,999.99"))
	Endif

DbSelectArea(_cAlias)
DbSetOrder(_nIndex)
DbGoTo(_nRecno)

Return(cMens)
