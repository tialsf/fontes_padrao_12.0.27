/*/                 
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���ExecBlock �TDCFG004  � Por:Antonio Cordeiro de Moura � Data �JULHO/2002���
�������������������������������������������������������������������������Ĵ��
���Descricao � Limpa dados dos itens dos pedidos de venda apos redigitacao���
���          � do codigo do cliente                                       ���      
�������������������������������������������������������������������������Ĵ��
���Uso       � Especifico Template Combustiveis/Fiscal                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
#include "rwmake.ch"

Template Function TDCFG004()
Local _nPosTES   	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "C6_TES"})	//preco venda
Local _nPosX_TPOP	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "C6_X_TPOP"})
Local _nPosX_REGRA	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "C6_X_REGRA"})
Local _nPosPRODUTO	:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "C6_PRODUTO"})
Local _nPosCFOP		:= aScan(aHeader,{|x| Upper(alltrim(x[2])) == "C6_CF"})
Local I := 0
Local nAcolsOri := n
Local cNumItem		:= ''
 M->C5_X_CTCLI := SA1->A1_X_CT
CHKTEMPLATE("DCLFIS")
FOR I:=1 TO LEN(aCols)
    n := I
    
   aCols[I,_nPosTES]:=SPACE(3)
   If !Empty(aCols[I,_nPosX_TPOP]) .And. !Empty(aCols[I,_nPosPRODUTO]) 
   		aCols[I,_nPosTES] := T_TDCFG001(.T., @cNumItem)
   EndIf
NEXT   
If  oGetDad <> nil
	oGetDad:ForceRefresh()
EndIf
n := nAcolsOri
Return(SA1->A1_X_CT)
