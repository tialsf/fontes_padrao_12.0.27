/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �          �   Adalberto Moreno Batista    � Data �06.05.2003���
�������������������������������������������������������������������������Ĵ��
���Descricao � Emissao do ANEXO I conforme convenio 54/02                 ���
�������������������������������������������������������������������������Ĵ��
���Arquivos  � LDI - Informacoes para impressao dos Anexos Fiscais        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
#include "rwmake.ch"
#include "TOPCONN.CH"

Template Function TDCFR001()
Local i:=0, J:=0
Local cDesc1	:= "Este programa tem como objetivo imprimir o ANEXO I confome legislacao para empresas que comercializam produtos derivados de petroleo"
Local cPerg   	:= PadR("ANXI",Len(SX1->X1_GRUPO))

Private titulo	:= "Relatorios Convenio 54/02"
imprime			:= .T.
aRegs			:= {}
Private lEnd       := .F.
Private lAbortPrint:= .F.
Private limite     := 132
Private tamanho    := "M"
Private nomeprog   := "TDCFR001" // Coloque aqui o nome do programa para impressao no cabecalho
Private nTipo      := 15
Private aReturn    := { "Zebrado", 1, "Administracao", 1, 2, 1, "", 1}
Private nLastKey   := 0    
Private cbtxt      := Space(10)
Private cbcont     := 00
Private CONTFL     := 01
Private nPag       := 1
Private nLin       := 80
Private wnrel      := "ANXI" // Coloque aqui o nome do arquivo usado para impressao em disco
Private cString    := "LDI"
Private mv_tpemp, mv_nome, mv_rg, mv_ufrg, mv_cargo, mv_fone

CHKTEMPLATE("DCLFIS")


//---- DELETE PERGUNTA CPR001 ----      
DbSelectArea("SX1")
SX1->(DbSetOrder(1))   

If SX1->(DbSeek("CPR001"))
	Do while Alltrim(SX1->X1_GRUPO) == "CPR001"
		RecLock("SX1",.F.)
		SX1->(dbDelete())
		MsUnLock()
		SX1->(DBSKIP())
	Enddo
EndIf       

//---- DELETE PERGUNTA ANEXI----      
DbSelectArea("SX1")
SX1->(DbSetOrder(1))   

If SX1->(DbSeek("ANEXI"))
	Do while Alltrim(SX1->X1_GRUPO) == "ANEXI"
		RecLock("SX1",.F.)
		SX1->(dbDelete())
		MsUnLock()
		SX1->(DBSKIP())
	Enddo
EndIf

//����������������������������������������������������������ٱ�
//						"ANXI"								ٱ�
//����������������������������������������������������������ٱ�

/*-----------------------"mv_par01"--------------------------*/

aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}

Aadd( aHelpPor, "Informar o M�s para gerar")
Aadd( aHelpPor, " o relat�rio Anexo I ")
HelpEng	:=	aHelpSpa	:=	aHelpPor
      
PutSx1(cPerg, "01","M�s:","M�s:","M�s:",;
	"mv_ch1","C",2,0,0,"G","Pertence('01/02/03/04/05/06/07/08/09/10/11/12')","","","","mv_par01","","","","",;
	"","","","","","","","","","",""," ",aHelpPor,aHelpEng,aHelpSpa)

/*-----------------------"mv_par02"--------------------------*/

aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}

Aadd( aHelpPor, "Informar o Ano para gerar")
Aadd( aHelpPor, " o relat�rio Anexo I ")
HelpEng	:=	aHelpSpa	:=	aHelpPor
      
PutSx1(cPerg, "02","Ano:","Ano:","Ano:",;
	"mv_ch2","C",4,0,0,"G",,"","","","mv_par02","","","","",;
	"","","","","","","","","","",""," ",aHelpPor,aHelpEng,aHelpSpa)

/*-----------------------"mv_par03"--------------------------*/

aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}

Aadd( aHelpPor, "Informar o Grupo do Anexo(Chave)")       
Aadd( aHelpPor, " pertinente a Gasolina ")
Aadd( aHelpPor, " para gera��o do Anexo I")
HelpEng	:=	aHelpSpa	:=	aHelpPor
      
PutSx1(cPerg, "03","Grupo Gasolina:","Grupo Gasolina:","Grupo Gasolina:",;
	"mv_ch3","C",5,0,0,"G","ValidSX1mv(mv_par03)","","","","mv_par03","","","","",;
	"","","","","","","","","","",""," ",aHelpPor,aHelpEng,aHelpSpa)	
	

/*-----------------------"mv_par04"--------------------------*/
aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}

Aadd( aHelpPor, "Informar o Grupo do Anexo(Chave)")       
Aadd( aHelpPor, " pertinente ao �leo")
Aadd( aHelpPor, " para gera��o do Anexo I")
HelpEng	:=	aHelpSpa	:=	aHelpPor
      
PutSx1(cPerg, "04","Grupo �leo:","Grupo �leo:","Grupo �leo:",;
	"mv_ch4","C",5,0,0,"G","ValidSX1mv(mv_par04)","","","","mv_par04","","","","",;
	"","","","","","","","","","",""," ",aHelpPor,aHelpEng,aHelpSpa)	
		


//Parametros de impressao do rodap�
Pergunte("CFAIMP",.F.)
mv_tpemp	:= mv_par01
mv_nome		:= mv_par02
mv_cpf		:= mv_par03
mv_rg		:= mv_par04
mv_ufrg		:= mv_par05
mv_cargo	:= mv_par06
mv_fone		:= mv_par07


If Pergunte(cPerg,.T.,"Anexo I")
	
	If Empty(MV_PAR03) .And. Empty(MV_PAR04)
   		Alert("Informe uma chave, para impress�o do relat�rio!")
		Pergunte(cPerg,.T.)
	Endif 
		
	wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,"","",.F.,,.F.,Tamanho,,.F.)
	
	If nLastKey == 27
		Return
	Endif
	
	SetDefault(aReturn,cString)
	
	If nLastKey == 27
		Return
	Endif
	
	nTipo := If(aReturn[4]==1,15,18)
	
	//���������������������������������������������������������������������Ŀ
	//� Processamento. RPTSTATUS monta janela com a regua de processamento. �
	//�����������������������������������������������������������������������
	RptStatus({|| RunReport() },Titulo)
endif
Return()

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao� RunReport    �                               �      �          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RunReport()
//Definicao das variaveis
Local _aLCab := T_LOAnexo("C",1)
Local _aLQd1 := {}
Local _aLQd2 := {}
Local _aLQd3 := {}
Local _aLQd4 := {}
Local _aLRod := T_LOAnexo("R")
Local _aDQd1 := {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}}
Local _aDQd2 := {}
Local _aDQd3 := {}
Local _aDQd4 := {{0,0},{0,0},{0,0},{0,0},{},{0,0}}
Local _cEstado := Alltrim(GetNewPar("MV_ESTADO",""))    
Local _cPerAnt  := iif(mv_par01<>"01",mv_par02+StrZero(Val(mv_par01)-1,2),StrZero(Val(mv_par02)-1,4)+"12")
Local _cPeriodo := mv_par02+mv_par01
Local I:=0, aCad, cOrdem, nPos, _cCNPJ, _nFator, _aSldInic, _aProduto
Local _aSTot2   := {0,0,0}	//Total Geral para o quadro 2
Local _aSTot3   := {0,0,0,0}	//Subtotal para o quadro 3
Local _aGTot3   := {0,0,0,0}	//Total geral para o quadro 3
Local _aGTot4   := {0,0}		//Total geral para o quadro 4
Local K			:=0
Local _cQuery 		:= ""   
Local nA 			:= ""
Local cAliasLDZ 	:= "LDZ"
Local cAliasLDI 	:= "LDI"
Local cAliasGA		:= "LDI"
Local nSoma 		:= 0  
Local cCfopTrans 	:= Alltrim(GetNewPar("MV_CFOTRAN",""))   // CFOP para identificar opera��o de transfer�ncia
Local cMvConge      := Alltrim(GetNewPar("MV_CONGEN",""))     // Informar o c�digo congenere
Local _aPxChvMV3 	:= IIf (!Empty(mv_par03),ListaIN(mv_par03),{}) // Filtra da fun��o ListaIN - colhendo em um Array os Subprodutos de tal chave Gasolina.
Local _aPxChvMV4 	:= IIf (!Empty(mv_par04),ListaIN(mv_par04),{}) // Filtra da fun��o ListaIN - colhendo em um Array os Subprodutos de tal chave Oleo.
Local _cListaMV3	:= ""                                                                                                                                  
Local _cListaMV4 	:= "" 
Local _cLdzMV3 		:= "" 
Local _cLdzMV4 		:= ""
Local _nA 			:= 0 
Local nUF 	  		:= 0  
Local nUfs1    		:= 0
Local nUfs2    		:= 0
Local SubMV3		:= Alltrim(Substr(mv_par03,1,3))


LDZ->(dbSetOrder(1))
LDI->(dbSetOrder(1))

//                    1         2         3         4         5         6         7         8         9         0         1         2         3
//           123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
aAdd(_aLQd1,'                                 QUADRO 1 - APURACAO DA MEDIA PONDERADA DO VALOR DA BASE DE CALCULO')
aAdd(_aLQd1,'+-------------------------------------------------+------------------+-------------------+------------------+----------------------------+')
aAdd(_aLQd1,'| HISTORICO                                       |   QTDE.DE        |   QTDE DE GAS.A   |    VL.UNIT.      |   BASE DE CALCULO          |')
aAdd(_aLQd1,'|                                                 |   COMBUSTIVEL    |    OU DIESEL A    |    MEDIO         |   DA ST                    |')
aAdd(_aLQd1,'| ESTOQUE INICIAL                                 |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| (+)RECEBIMENTOS (ENTRADAS)                      |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| (=)TOTAL DISPONIVEL NO PERIODO                  |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| MEDIA PONDERADA UNITARIA DA BC-ST               |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| (+)RECEBIMENTOS (DEVOLUCOES)                    |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| (=)TOTAL DE ENTRADAS                            |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| (-)REMESSAS (SAIDAS)                            |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| (-)REMESSAS (DEVOLUCOES)                        |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| (=)TOTAL DE SAIDAS                              |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| (-)PERDAS                                       |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| (+)GANHOS                                       |   ###########    |    ###########    |    ##########    |   ######################   |')
aAdd(_aLQd1,'| (=)ESTOQUE FINAL                                |   ###########    |    ###########    |    ##########    |   ######################   |')

aAdd(_aLQd2,'                                 QUADRO 2 -APURACAO DA PROPORCIONALIDADE POR FORNECEDOR')
aAdd(_aLQd2,'+-------------------------------------------+-----------------+-----------------+------------------+------------+------------------------+')
aAdd(_aLQd2,'| CNPJ                                      | ESTOQUE INICIAL |    RECEBIMENTOS | TOTAL DISPONIVEL | PROPORCAO  |       ESTOQUE FINAL    |')
aAdd(_aLQd2,'| ####################################      |     ########### |     ########### |      ########### |   ##### %  | #####################  |')

aAdd(_aLQd3,'                                 QUADRO 3 - RELACAO DOS RECEBIMENTOS NO PERIODO (ENTRADAS)')
aAdd(_aLQd3,'+------+--------------------+     +--------------------+---------------------+     +-----------------------+---------------------+')
aAdd(_aLQd3,'| CNPJ | ################## |     | INSCRICAO ESTADUAL | ################### |     | INSCRICAO ESTADUAL ST | ################### |')
aAdd(_aLQd3,'+------+-------+------------+-----+--------------------+---------------------+-----+-----------------------+---------------------+')
aAdd(_aLQd3,'| RAZAO SOCIAL | ############################################################################################################### |')
aAdd(_aLQd3,'+----------+---+-------------------------------------------------------------------------------------------------------+----+----+')
aAdd(_aLQd3,'| ENDERECO | ######################################################################################################### | UF | ## |')
aAdd(_aLQd3,'+----------+--------+--------+---------------------+---------------------+----------------------+------------+---------+----+----+')
aAdd(_aLQd3,'|    NOTA FISCAL    | CFOP   |       QUANTIDADE DE |    QUANTIDADE DE    |      BASE DE CALCULO |  ALIQUOTA  |              ICMS |')
aAdd(_aLQd3,'| NUMERO | DATA     |        |         COMBUSTIVEL | GAS. A OU DIESEL A  |      DA ST           |            |                   |')
aAdd(_aLQd3,'+--------+----------+--------+---------------------+---------------------+----------------------+------------+-------------------+')
aAdd(_aLQd3,'| ###### | ######## | #####  |         ########### |         ########### |       ############## |   ##### %  |    ############## |')
aAdd(_aLQd3,'+--------+----------+--------+---------------------+---------------------+----------------------+------------+-------------------+')
aAdd(_aLQd3,'| #########################  |         ########### |         ########### |       ############## |            |    ############## |') 
aAdd(_aLQd3,'+----------------------------+---------------------+---------------------+----------------------+------------+-------------------+')

aAdd(_aLQd4,'     QUADRO 4 - RELACAO DAS REMESSAS REALIZADAS NO PERIODO (SAIDAS)')
aAdd(_aLQd4,'+----------------------------------+----------------------+---------------------------+')
aAdd(_aLQd4,'| OPERACOES DESTINADAS             | QTDE. DE COMBUSTIVEL | QTDE DE GAS A ou DIESEL A |')
aAdd(_aLQd4,'| TRANSFERENCIAS                   |          ########### |        #################  |')
aAdd(_aLQd4,'| AO EXTERIOR                      |          ########### |        #################  |')
aAdd(_aLQd4,'| SAIDAS PARA CONGENERES           |          ########### |        #################  |')
aAdd(_aLQd4,'| AO PROPRIO ESTADO                |          ########### |        #################  |')
aAdd(_aLQd4,'| A UNIDADE FEDERADA               |                      |                           |')
aAdd(_aLQd4,'| ##                               |          ########### |        #################  |')
aAdd(_aLQd4,'| TOTAL                            |          ########### |        #################  |')



// Ordena em uma lista para tratamento na Query
If !Empty(_aPxChvMV3)
	For _nA := 1 To Len(_aPxChvMV3)  
   		_cLdzMV3 += "'"    
		_cLdzMV3 += _aPxChvMV3[_nA]
   		_cLdzMV3 += "'"    			
		IIf(_nA <> Len(_aPxChvMV3),_cLdzMV3 += ",",)                           
	Next 
Else
   _cLdzMV3 := "''"
Endif                

	
// Ordena em uma lista para tratamento na Query
If !Empty(_aPxChvMV4)
	For _nA := 1 To Len(_aPxChvMV4)  
   		_cLdzMV4 += "'"    
		_cLdzMV4 += _aPxChvMV4[_nA]
   		_cLdzMV4 += "'"    			
		IIf(_nA <> Len(_aPxChvMV4),_cLdzMV4 += ",",)                           
	Next 
Else
   _cLdzMV4 := "''"
Endif    

//Buscando o saldo final do mes anterior - iniciando montagem do quadro 1 e quadro 2
_cQuery := "SELECT LDZ_FILIAL, LDZ_ANOMES, LDZ_COD, LDZ_CNPJ, LDZ_QTDEFI, LDZ_BSTFIM, LDZ_MEDIA, LDZ_CODSEF, LDZ_UF, LDZ_IEF, B1_COD, B1_TIPO, B1_X_CT"
_cQuery += " FROM "+RetSQLName("LDZ")+" LDZ "       
_cQuery += " INNER JOIN "+RetSQLName("SB1")+" SB1 "
_cQuery += " ON (LDZ.LDZ_FILIAL = SB1.B1_FILIAL "
_cQuery += " AND LDZ.LDZ_COD = SB1.B1_COD " 
_cQuery += " AND SB1.D_E_L_E_T_ <> '*') " 
_cQuery += " WHERE LDZ.LDZ_ANOMES = '"+_cPeriodo+"'"
_cQuery += " AND (LDZ.LDZ_COD IN ("+_cLdzMV3+") " 
_cQuery += " OR LDZ.LDZ_COD IN ("+_cLdzMV4+")) "
_cQuery += " AND LDZ.D_E_L_E_T_ <> '*'"
_cQuery += "ORDER BY "+SqlOrder(LDZ->(IndexKey()))

cAliasLDZ := GetNextAlias()
// Criando a tabela temporaria
_cQuery := ChangeQuery(_cQuery)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,_cQuery),cAliasLDZ, .F., .T.)

DbSelectArea("SA2")
SA2->(DbSetOrder(1))  
DbSelectArea("SA1")
SA1->(DbSetOrder(1)) 
		
Do while !(cAliasLDZ)->(Eof())
	
	if (cAliasLDZ)->LDZ_CNPJ <> 'MEDIA BASE-ST'
		
		//Quadro 1
		if (cAliasLDZ)->B1_TIPO == "MP" //produto materia-prima
			_aDQd1[1,2] += (cAliasLDZ)->LDZ_QTDEFI
			_aDQd1[3,2] += (cAliasLDZ)->LDZ_QTDEFI
			_aDQd1[6,2] += (cAliasLDZ)->LDZ_QTDEFI

			_aDQd1[1,4] += (cAliasLDZ)->LDZ_BSTFIM
			_aDQd1[3,4] += (cAliasLDZ)->LDZ_BSTFIM

		else
			_aDQd1[1,4] += (cAliasLDZ)->LDZ_BSTFIM
			_aDQd1[3,4] += (cAliasLDZ)->LDZ_BSTFIM

			
		endif       


	
		// Total do Quadro 2
		_aSTot2[2] += (cAliasLDZ)->LDZ_QTDEFI  //total de estoque disponivel
		_aSTot2[3] += (cAliasLDZ)->LDZ_QTDEFI  //total de estoque disponivel	
	endif                                                                   
	
		//Buscando a media do mes
	If (cAliasLDZ)->LDZ_CNPJ == 'MEDIA BASE-ST '
		//Quadro 1
		_aDQd1[4,3] += (cAliasLDZ)->LDZ_MEDIA
	Endif     
                                                     
	(cAliasLDZ)->(dbSkip())
enddo

_aDQd1[1,3] += NoRound(_aDQd1[1,4] / _aDQd1[1,2],2)   

// Ordena em uma lista para tratamento na Query  
If !Empty(MV_PAR03)
	If !Empty(_aPxChvMV3)
		For _nA := 1 To Len(_aPxChvMV3)  
	   		_cListaMV3 += "'"    
			_cListaMV3 += _aPxChvMV3[_nA]
	   		_cListaMV3 += "'"    			
			IIf(_nA <> Len(_aPxChvMV3),_cListaMV3 += ",",)                           
		Next 
	Else
	   _cListaMV3 := "''"
	Endif                
Else 
   _cListaMV3 := "''"
Endif
	
// Ordena em uma lista para tratamento na Query
If !Empty(MV_PAR04)
	If !Empty(_aPxChvMV4)
		For _nA := 1 To Len(_aPxChvMV4)  
	   		_cListaMV4 += "'"    
			_cListaMV4 += _aPxChvMV4[_nA]
	   		_cListaMV4 += "'"    			
			IIf(_nA <> Len(_aPxChvMV4),_cListaMV4 += ",",)                           
		Next 
	Else
	   _cListaMV4 := "''"
	Endif    
Else 
	_cListaMV4 := "''"
Endif


_cQuery := "SELECT LDI_FILIAL, LDI_ANOMES, LDI_COD, LDI_DOC, LDI_CLIFOR, LDI_LOJA, LDI_EMISSA, LDI_TPMOV, LDI_CNPJ, LDI_CTORIG, "
_cQuery += " LDI_QTDE, LDI_BASEST, LDI_ALIQD, LDI_VLUNIT, LDI_VICMPP, "
_cQuery += " LDI_VICMST, LDI_TPDORI, LDI_QTDEOR, LDI_EST, LDI_CFOP, LDI_TPDEST, LDI_QTEDEV, LDI_ALIQO, LDI_CODORI, B1_COD, B1_TIPO, B1_X_CT"
_cQuery += " FROM "+RetSQLName("LDI")+" LDI " 
_cQuery += " INNER JOIN "+RetSQLName("SB1")+" SB1 "
_cQuery += " ON (LDI.LDI_FILIAL = SB1.B1_FILIAL "
_cQuery += " AND LDI.LDI_COD = SB1.B1_COD " 
_cQuery += " AND SB1.D_E_L_E_T_ <> '*') " 
_cQuery += " WHERE LDI.LDI_ANOMES = '"+_cPeriodo+"'"
_cQuery += " AND (LDI.LDI_COD IN ("+_cListaMV3+") " 
_cQuery += " OR LDI.LDI_COD IN ("+_cListaMV4+")) "
_cQuery += " AND LDI.D_E_L_E_T_ <> '*'"
_cQuery += "ORDER BY "+SqlOrder(LDI->(IndexKey()))

cAliasLDI := GetNextAlias()
// Criando a tabela temporaria
_cQuery := ChangeQuery(_cQuery)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,_cQuery),cAliasLDI, .F., .T.) 

//Concluindo a montagem do quadro 1, montando o quadro 3 e 4 com base na movimentacao do mes corrente.
Do while !(cAliasLDI)->(Eof())

	if (cAliasLDI)->LDI_TPMOV = '1'	//entradas
		//Quadro 1
		if (cAliasLDI)->B1_TIPO == "MP" //produto materia-prima
			
			if (cAliasLDI)->LDI_TPDORI == "F"  //nota de compra     
				_aDQd1[2,2] += (cAliasLDI)->LDI_QTDE
				_aDQd1[3,2] += (cAliasLDI)->LDI_QTDE
				_aDQd1[2,4] += (cAliasLDI)->LDI_BASEST				
				_aDQd1[3,4] += (cAliasLDI)->LDI_BASEST
			else  //nota de devolu��o
				If (cAliasLDI)->B1_TIPO == "MP" 
					_aDQd1[5,2] += (cAliasLDI)->LDI_QTDE
				else
					_aDQd1[5,1] += (cAliasLDI)->LDI_QTDE
				endif
				//_aDQd1[5,4] += (cAliasLDI)->LDI_BASEST  18/08/04
			endif

			_aDQd1[6,2] += (cAliasLDI)->LDI_QTDE
			//_aDQd1[6,4] += (cAliasLDI)->LDI_BASEST
			
		else
			
			if (cAliasLDI)->LDI_TPDORI == "F"  //nota de compra

				_aDQd1[2,4] += (cAliasLDI)->LDI_BASEST
				

				_aDQd1[3,4] += (cAliasLDI)->LDI_BASEST

			endif
			
			//_aDQd1[6,4] += (cAliasLDI)->LDI_BASEST
			
		endif  
		/*if !(cAliasLDI)->LDI_TPDORI == "F"  //nota de compra
			_aDQd1[5,1] += (cAliasLDI)->LDI_QTDE
		endif*/

		_aDQd1[2,1] += (cAliasLDI)->LDI_QTDE  
		//_aDQd1[3,1] += (cAliasLDI)->LDI_QTDE
		_aDQd1[6,1] += (cAliasLDI)->LDI_QTDE
		
		//Devolu��es n�o dever�o ser consideradas nos Quadros 2 e 3
		if (cAliasLDI)->LDI_TPDORI == "F"  // Nota De Compra
			
			//Quadro 2
			nPos := aScan(_aDQd2, {|x| x[1]=(cAliasLDI)->LDI_CNPJ})
			if nPos = 0
				(cAliasLDI)->(aAdd(_aDQd2,{LDI_CNPJ,_aDQd1[1,2],_aDQd1[2,2],_aDQd1[3,2],0,0}))
			else 
				_aDQd2[nPos,3] := _aDQd1[2,2]
				_aDQd2[nPos,4] += (cAliasLDI)->LDI_QTDE
			endif           
			
			// Total do Quadro 2
			_aSTot2[1] += (cAliasLDI)->LDI_QTDE
			_aSTot2[2] += (cAliasLDI)->LDI_QTDE
			_aSTot2[3] += (cAliasLDI)->LDI_QTDE  
	
			//Quadro 3 
			//If SA2->(dbSeek(xFilial("SA2")+Alltrim((cAliasLDI)->LDI_CLIFOR)+(cAliasLDI)->LDI_LOJA))
				aCad := GetAdvFVal("SA2",{"A2_INSCR","A2_NOME","A2_END","A2_X_CT"},xFilial("SA2")+(cAliasLDI)->LDI_CNPJ,3)
				(cAliasLDI)->(aAdd(_aDQd3,{	LDI_CNPJ,;
				aCad[1],;
				aCad[2],;
				aCad[3],;
				aCad[4],;
				LDI_EST,;
				LDI_DOC,;
				LDI_EMISSA,;
				LDI_CFOP,;
				LDI_QTDE,;
				IIF((cAliasLDI)->B1_TIPO == "MP",LDI_QTDE,0),;
				LDI_BASEST,;
				LDI_ALIQD,;
				IIF (LDI_BASEST<>0,Round((LDI_BASEST * LDI_ALIQD)/100,2),0)}))    
				 NoRound(_aDQd1[12,4] / _aDQd1[12,2],2)
		   //	Endif
		endif 
		 
	elseif (cAliasLDI)->LDI_TPMOV = '2'	//saidas
		
		if (cAliasLDI)->LDI_TPDORI == "F" //Nota de Venda
			_aDQd1[7,2] += iif((cAliasLDI)->B1_TIPO=="MP",(cAliasLDI)->LDI_QTDE,0)
		else // Nota de devolu��o
 			_aDQd1[8,2] += iif((cAliasLDI)->B1_TIPO=="MP",(cAliasLDI)->LDI_QTDE,0)
			_aDQd1[8,1] += (cAliasLDI)->LDI_QTDE 
		endif
		
		_aDQd1[7,1] += (cAliasLDI)->LDI_QTDEOR 
		_aDQd1[9,1] += (cAliasLDI)->LDI_QTDEOR - _aDQd1[8,1]     
		
		_aDQd1[9,2] += iif((cAliasLDI)->B1_TIPO=="MP",(cAliasLDI)->LDI_QTDE,0)
		        

		If (cAliasLDI)->LDI_TPDEST = 'F'  //pode indicar (F)ornecedor ou (C)liente
			If SA2->(dbSeek(xFilial("SA2")+(cAliasLDI)->LDI_CLIFOR+(cAliasLDI)->LDI_LOJA))
				aCad := GetAdvFVal("SA2",{"A2_INSCR","A2_NOME","A2_END","A2_X_CT"},xFilial("SA2")+(cAliasLDI)->LDI_CNPJ,3)
			Endif
		elseif (cAliasLDI)->LDI_TPDEST = 'C'  
			If SA1->(dbSeek(xFilial("SA1")+(cAliasLDI)->LDI_CLIFOR+(cAliasLDI)->LDI_LOJA))
				aCad := GetAdvFVal("SA1",{"A1_INSCR","A1_NOME","A1_END","A1_X_CT"},xFilial("SA1")+(cAliasLDI)->LDI_CNPJ,3)
			End
		endif
		
		//Quadro 4
		Do Case
			Case  Alltrim((cAliasLDI)->LDI_CFOP)$cCfopTrans
				If (cAliasLDI)->B1_TIPO == "MP"	//Transferencia entre filiais
					_aDQd4[1,2] += (cAliasLDI)->LDI_QTDE
				else
					_aDQd4[1,1] += (cAliasLDI)->LDI_QTDE	
				endif
			Case (cAliasLDI)->LDI_EST = 'EX' // Quinzenal
				If (cAliasLDI)->B1_TIPO == "MP" 	//Exterior  
					_aDQd4[2,2] += (cAliasLDI)->LDI_QTDE
				else
					_aDQd4[2,1] += (cAliasLDI)->LDI_QTDE			
				endif      
			Case aCad[4]$cMvConge // Quinzenal	
				if (cAliasLDI)->B1_TIPO == "MP"	//ao proprio estado/consumidor
					_aDQd4[3,2] += (cAliasLDI)->LDI_QTDE
				else
					_aDQd4[3,1] += (cAliasLDI)->LDI_QTDE								
				endif 
			Case (cAliasLDI)->LDI_EST = _cEstado  
				If (cAliasLDI)->LDI_COD = (cAliasLDI)->LDI_CODORI
					if (cAliasLDI)->B1_TIPO == "MP"
						_aDQd4[4,1] += (cAliasLDI)->LDI_QTDEOR
						_aDQd4[4,2] += (cAliasLDI)->LDI_QTDEOR	
					Endif
				Else
					if (cAliasLDI)->B1_TIPO == "MP"
						_aDQd4[4,2] += (cAliasLDI)->LDI_QTDE
						_aDQd4[4,1] += (cAliasLDI)->LDI_QTDEOR
					Else
						_aDQd4[4,1] += (cAliasLDI)->LDI_QTDEOR
					Endif
				Endif                                          
			Otherwise // Mensal
				If (nPos:= Ascan(_aDQd4[5],{|x| x[1]==(cAliasLDI)->LDI_EST })) == 0
					Aadd(_aDQd4[5],{(cAliasLDI)->LDI_EST,0,0})			   					
					nPos := Len(_aDQd4[5])
				EndIf  
			
				If (cAliasLDI)->LDI_COD = (cAliasLDI)->LDI_CODORI
					If (cAliasLDI)->B1_TIPO=="MP"
						_aDQd4[5,nPos,3]  += (cAliasLDI)->LDI_QTDEOR
						_aDQd4[5,nPos,2] += (cAliasLDI)->LDI_QTDEOR
					Endif
				Else 
					If (cAliasLDI)->B1_TIPO=="MP"
						_aDQd4[5,nPos,3] += (cAliasLDI)->LDI_QTDE
						_aDQd4[5,nPos,2] += (cAliasLDI)->LDI_QTDEOR
					Else
						_aDQd4[5,nPos,2] += (cAliasLDI)->LDI_QTDEOR
					Endif
				Endif
		EndCase         

	elseif (cAliasLDI)->LDI_TPMOV = '3'	//ganhos
		
		if (cAliasLDI)->B1_TIPO == "MP"
			_aDQd1[11,2] += (cAliasLDI)->LDI_QTDE
		endif
			//_aDQd1[11,1] += (cAliasLDI)->LDI_QTDE
	elseif (cAliasLDI)->LDI_TPMOV = '4'	//perdas
		
		if (cAliasLDI)->B1_TIPO == "MP"
			_aDQd1[10,2] += (cAliasLDI)->LDI_QTDE
		endif
			//_aDQd1[10,1] += (cAliasLDI)->LDI_QTDE
	endif

	//Quadro 2
	_aStot2[3]  += iif((cAliasLDI)->B1_TIPO=="MP",_aDQd1[12,2],0)
		
	(cAliasLDI)->(dbSkip()) 
enddo 

//_aDQd1[12,1] := _aDQd1[6,1]-_aDQd1[9,1]
_aDQd1[12,2] := _aDQd1[6,2]-_aDQd1[9,2]
_aDQd1[12,3] := _aDQd1[4,3]
_aDQd1[12,4] := _aDQd1[12,2]*_aDQd1[12,3]
    
For nUF:= 1 To Len(_aDQd4[5])
	nUfs1  += _aDQd4[5,nUF,2]  
	nUfs2  += _aDQd4[5,nUF,3]
Next
_aDQd4[6,1] += _aDQd4[1,1] + _aDQd4[2,1] + _aDQd4[3,1] + _aDQd4[4,1] + nUfs1
_aDQd4[6,2] += _aDQd4[1,2] + _aDQd4[2,2] + _aDQd4[3,2] + _aDQd4[4,2] + nUfs2

//Quadro 2 - Proporcoes
for i:=1 to len(_aDQd2)
	_nFator	:= Round(_aDQd2[i,4]/_aStot2[2],4)
	_aDQd2[i,5] := _nFator
	_aDQd2[i,6] := _aDQd1[6,2] - _aDQd1[9,2]
next

//Calculando a proporcionalidade e identificando o CNPJ com maior participacao
nMaiorProp := 0
cCNPJmaior := ""
cCNPJmenor := ""
for k:= 1 to len(_aDQd2)
	if _aDQd2[k,5] > nMaiorProp
		nMaiorProp := _aDQd2[k,5]
		cCNPJmaior := _aDQd2[k,1]
	endif
next  

//Transferindo as movimentacoes dos fornecedores com menos de 1% de participacao
for k:= 1 to len(_aDQd2)
	if _aDQd2[k,5] < 0.01
		cCNPJmenor := _aDQd2[k,1]
		//Atualizar a qtde do fornec de maior proporc. com o conteudo do de menor proporc.
		nPos := aScan(_aDQd2, {|x| x[1]=cCNPJmaior})
		_aDQd2[nPos,2] += _aDQd2[k,2]		//inicial
		_aDQd2[nPos,3] += _aDQd2[k,3]       //recebimento
		_aDQd2[nPos,4] := _aDQd2[nPos,2]+_aDQd2[nPos,3]        //disponivel
		_aDQd2[nPos,5] := Round(_aDQd2[nPos,4] / _aStot2[2],4) //proporcao
		_aDQd2[nPos,6] := _aDQd1[6,2] - _aDQd1[9,2]
		
		//Zerando o CNPJ com participacao menor que 1% para nao ser impresso
		_aDQd2[k,1] := space(14)
	endif
next      

//Colocando na ordem correta
_aDQd2 := aSort(_aDQd2,,,{|x,y| x[1] < y[1] })
_aDQd3 := aSort(_aDQd3,,,{|x,y| x[1]+x[7] < y[1]+y[7] })

//SetRegua(RecCount())
//imprimindo o cabecalho
ImpCab(_aLCab)
//imprimindo o quadro 1
FmtLin({},													_aLQd1[01],,,@nLin)
FmtLin({},													_aLQd1[02],,,@nLin)
FmtLin({},													_aLQd1[03],,,@nLin)
FmtLin({},													_aLQd1[04],,,@nLin)
FmtLin({},													_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[1,1],"@E 999,999,999"),;
Transf(_aDQd1[1,2],"@E 999,999,999"),;
Transf(_aDQd1[1,3],"@E 999999999.99"),;
Transf(_aDQd1[1,4],"@E 999,999,999")},	_aLQd1[05],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[2,1],"@E 999,999,999"),;
Transf(_aDQd1[2,2],"@E 999,999,999"),;
Transf(_aDQd1[2,3],"@E 999,999,999"),;
Transf(_aDQd1[2,4],"@E 999,999,999")},	_aLQd1[06],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[3,1],"@E 999,999,999"),;
Transf(_aDQd1[3,2],"@E 999,999,999"),;
Transf(_aDQd1[3,3],"@E 999,999,999"),;
Transf(_aDQd1[3,4],"@E 999,999,999")},	_aLQd1[07],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[4,1],"@E 999,999,999"),;
Transf(_aDQd1[4,2],"@E 999,999,999"),;
Transf(_aDQd1[4,3],"@E 999999999.99"),;
Transf(_aDQd1[4,4],"@E 999,999,999")},	_aLQd1[08],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[5,1],"@E 999,999,999"),;
Transf(_aDQd1[5,2],"@E 999,999,999"),;
Transf(_aDQd1[5,3],"@E 999,999,999"),;
Transf(_aDQd1[5,4],"@E 999,999,999")},	_aLQd1[09],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[6,1],"@E 999,999,999"),;
Transf(_aDQd1[6,2],"@E 999,999,999"),;
Transf(_aDQd1[6,3],"@E 999,999,999"),;
Transf(_aDQd1[6,4],"@E 999,999,999")},	_aLQd1[10],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[7,1],"@E 999,999,999"),;
Transf(_aDQd1[7,2],"@E 999,999,999"),;
Transf(_aDQd1[7,3],"@E 999,999,999"),;
Transf(_aDQd1[7,4],"@E 999,999,999")},	_aLQd1[11],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[8,1],"@E 999,999,999"),;
Transf(_aDQd1[8,2],"@E 999,999,999"),;
Transf(_aDQd1[8,3],"@E 999,999,999"),;
Transf(_aDQd1[8,4],"@E 999,999,999")},	_aLQd1[12],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[9,1],"@E 999,999,999"),;
Transf(_aDQd1[9,2],"@E 999,999,999"),;
Transf(_aDQd1[9,3],"@E 999,999,999"),;
Transf(_aDQd1[9,4],"@E 999,999,999")},	_aLQd1[13],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[10,1],"@E 999,999,999"),;
Transf(_aDQd1[10,2],"@E 999,999,999"),;
Transf(_aDQd1[10,3],"@E 999,999,999"),;
Transf(_aDQd1[10,4],"@E 999,999,999")},	_aLQd1[14],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[11,1],"@E 999,999,999"),;
Transf(_aDQd1[11,2],"@E 999,999,999"),;
Transf(_aDQd1[11,3],"@E 999,999,999"),;
Transf(_aDQd1[11,4],"@E 999,999,999")},	_aLQd1[15],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)
FmtLin({Transf(_aDQd1[12,1],"@E 999,999,999"),;
Transf(_aDQd1[12,2],"@E 999,999,999"),;
Transf(_aDQd1[12,3],"@E 999999999.99"),;
Transf(_aDQd1[12,4],"@E 999,999,999,999.99")},	_aLQd1[16],,,@nLin)
FmtLin({},											_aLQd1[02],,,@nLin)

//Imprimindo o quadro 2
FmtLin({},	_aLQd2[01],,,@nLin)
if Len(_aDQd2) >= 1	//teste se ha movimento
	_cCNPJ := "@@@@"
	for i:=1 to len(_aDQd2)
		if _aDQd2[i,1] <> " "
			if _cCNPJ = "@@@@"		//_aDQd2[i,1] <> _cCNPJ
				FmtLin({},	_aLQd2[02],,,@nLin)
				FmtLin({},	_aLQd2[03],,,@nLin)
				FmtLin({},	_aLQd2[02],,,@nLin)
				_cCNPJ := _aDQd2[i,1]
			endif
			FmtLin({Transf(_aDQd2[i,1],"@R 99.999.999/9999-99"),;
			Iif(_aDQd2[i,2] < 0,"0,000",Transf(_aDQd2[i,2],"@E 999,999,999")),;
			Transf(_aDQd2[i,3],"@E 999,999,999"),;
			Transf(_aDQd2[i,4],"@E 999,999,999"),;
			Transf((_aDQd2[i,5]*100),"@E 999.99"),;
			Transf(_aDQd2[i,6],"@E 999,999,999,999,999.99")},	_aLQd2[04],,,@nLin)
			FmtLin({},	_aLQd2[02],,,@nLin)
			_aStot2[3] := _aDQd2[i,6]
		endif			
	next 
	i:=0
	for i:=1 to len(_aDQd2)  
   		nSoma += Iif(_aDQd2[i,2] < 0,0,_aDQd2[i,2])
	next  
	If nSoma >= 0
			FmtLin({"SOMA",;
			Iif(nSoma==0,"0,000",Transf(nSoma,"@E 999,999,999")),;
			Transf(_aStot2[1],"@E 999,999,999"),;
			Transf(_aStot2[2],"@E 999,999,999"),;
			"100,00",;
			Transf(_aStot2[3],"@E 999,999,999,999,999.99")},	_aLQd2[04],,,@nLin)
			FmtLin({},	_aLQd2[02],,,@nLin)
	Else
	  		nSoma := nSoma
	Endif
else
	T_ISemMovAnexo()
endif
T_IRodAnexo(_aLRod)

ImpCab(_aLCab)

//Imprimindo o Quadro 3
FmtLin({},	_aLQd3[01],,,@nLin)
if len(_aDQd3) >= 1
	_cCNPJ := "@@@@"
	for i:=1 to len(_aDQd3)
		if _aDQd3[i,1] <> _cCNPJ
			if _cCNPJ<>"@@@@"
				FmtLin({"TOTAL DO REMETENTE",;
				Transf(_aSTot3[1],"@E 999,999,999"),;
				Transf(_aSTot3[2],"@E 999,999,999"),;
				Transf(_aSTot3[3],"@E 999,999,999.99"),;
				Transf(_aSTot3[4],"@E 999,999,999.99")},	_aLQd3[14],,,@nLin)
				FmtLin({},											_aLQd3[15],,,@nLin)
			endif
			FmtLin({},				_aLQd3[02],,,@nLin)
			FmtLin({Transf(_aDQd3[i,1],"@R 99.999.999/9999-99"),;
			_aDQd3[i,2],;
			""},			_aLQd3[03],,,@nLin)	//
			FmtLin({},				_aLQd3[04],,,@nLin)
			FmtLin({_aDQd3[i,3]},	_aLQd3[05],,,@nLin)	//
			FmtLin({},				_aLQd3[06],,,@nLin)
			FmtLin({_aDQd3[i,4],;
			_aDQd3[i,6]},	_aLQd3[07],,,@nLin)	//
			FmtLin({},				_aLQd3[08],,,@nLin)
			FmtLin({},				_aLQd3[09],,,@nLin)
			FmtLin({},				_aLQd3[10],,,@nLin)
			FmtLin({},				_aLQd3[11],,,@nLin)
			_cCNPJ := _aDQd3[i,1]
			_aSTot3 := {0,0,0,0}
		endif
		FmtLin({_aDQd3[i,7],;
		_aDQd3[i,8],;
		_aDqd3[i,9],;
		Transf(_aDQd3[i,10],"@E 999,999,999"),;
		Transf(_aDQd3[i,11],"@E 999,999,999"),;
		Transf(_aDQd3[i,12],"@E 999,999,999.99"),;
		Transf(_aDQd3[i,13],"@E 999"),;
		Transf(_aDQd3[i,14],"@E 999,999.99")},	_aLQd3[12],,,@nLin)
		FmtLin({},											_aLQd3[13],,,@nLin)
		//incremento dos totais e subtotais
		_aSTot3[1] += _aDQd3[i,10]
		_aSTot3[2] += _aDQd3[i,11]
		_aSTot3[3] += _aDQd3[i,12]
		_aSTot3[4] += _aDQd3[i,14]
		
		_aGTot3[1] += _aDQd3[i,10]
		_aGTot3[2] += _aDQd3[i,11]
		_aGTot3[3] += _aDQd3[i,12]
		_aGTot3[4] += _aDQd3[i,14]
		
		if nLin+13 > 70
			
			ImpCab(_aLCab)
			FmtLin({},				_aLQd3[01],,,@nLin)
			FmtLin({},				_aLQd3[02],,,@nLin)
			FmtLin({Transf(_aDQd3[i,1],"@R 99.999.999/9999-99"),;
			_aDQd3[i,2],;
			""},			_aLQd3[03],,,@nLin)	//
			FmtLin({},				_aLQd3[04],,,@nLin)
			FmtLin({_aDQd3[i,3]},	_aLQd3[05],,,@nLin)	//
			FmtLin({},				_aLQd3[06],,,@nLin)
			FmtLin({_aDQd3[i,4],;
			_aDQd3[i,6]},	_aLQd3[07],,,@nLin)	//
			FmtLin({},				_aLQd3[08],,,@nLin)
			FmtLin({},				_aLQd3[09],,,@nLin)
			FmtLin({},				_aLQd3[10],,,@nLin)
			FmtLin({},				_aLQd3[11],,,@nLin)
		endif
		
	next
	FmtLin({"TOTAL DO REMETENTE",;
	Transf(_aSTot3[1],"@E 999,999,999"),;
	Transf(_aSTot3[2],"@E 999,999,999"),;
	Transf(_aSTot3[3],"@E 999,999,999.99"),;
	Transf(_aSTot3[4],"@E 999,999,999.99")},	_aLQd3[14],,,@nLin)
	FmtLin({},											_aLQd3[15],,,@nLin)
	
	FmtLin({"TOTAL DO PERIODO",;
	Transf(_aGTot3[1],"@E 999,999,999"),;
	Transf(_aGTot3[2],"@E 999,999,999"),;
	Transf(_aGTot3[3],"@E 999,999,999.99"),;
	Transf(_aGTot3[4],"@E 999,999,999.99")},	_aLQd3[14],,,@nLin)
	FmtLin({},		   									_aLQd3[15],,,@nLin)
else
	T_ISemMovAnexo()
endif 
T_IRodAnexo(_aLRod)

//Imprimindo o Quadro 4   


ImpCab(_aLCab)
FmtLin({},	_aLQd4[01],,,@nLin)  
FmtLin({},	_aLQd4[02],,,@nLin)  
FmtLin({},	_aLQd4[03],,,@nLin)  
if len(_aDQd4) >=1
	FmtLin({Transf(_aDQd4[1,1],"@E 999,999,999"),;
	Transf(_aDQd4[1,2],"@E 999,999,999")},	_aLQd4[04],,,@nLin)
	
	FmtLin({Transf(_aDQd4[2,1],"@E 999,999,999"),;
	Transf(_aDQd4[2,2],"@E 999,999,999")},	_aLQd4[05],,,@nLin)
	
	FmtLin({Transf(_aDQd4[3,1],"@E 999,999,999"),;
	Transf(_aDQd4[3,2],"@E 999,999,999")},	_aLQd4[06],,,@nLin)
	
	FmtLin({Transf(_aDQd4[4,1],"@E 999,999,999"),;
	Transf(_aDQd4[4,2],"@E 999,999,999")},	_aLQd4[07],,,@nLin)
	FmtLin({},	_aLQd4[08],,,@nLin)  
	For nUF:= 1 To Len(_aDQd4[5])
		FmtLin({Transf(_aDQd4[5,nUF,1],"@!"),;
		Transf(_aDQd4[5,nUF,2],"@E 999,999,999"),;
		Transf(_aDQd4[5,nUF,3],"@E 999,999,999")},	_aLQd4[09],,,@nLin)
	Next
	
	FmtLin({Transf(_aDQd4[6,1],"@E 999,999,999"),;
	Transf(_aDQd4[6,2],"@E 999,999,999")},	_aLQd4[10],,,@nLin)
	FmtLin({},	_aLQd4[02],,,@nLin)  

else
	T_ISemMovAnexo()
endif
T_IRodAnexo(_aLRod)

//���������������������������������������������������������������������Ŀ
//� Finaliza a execucao do relatorio...                                 �
//�����������������������������������������������������������������������
Set Device to Screen

//���������������������������������������������������������������������Ŀ
//� Se impressao em disco, chama o gerenciador de impressao...          �
//�����������������������������������������������������������������������

If aReturn[5]==1
	dbCommitAll()
	SET PRINTER TO
	OurSpool(wnrel)
Endif

MS_FLUSH()

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao� ImpCab       �                               �      �          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpCab(_aLCab)
Local aTpEmp := {"","","",""}
if mv_tpemp == 1	//TRR
	aTpEmp[1] := "X"
elseif mv_tpemp == 2	//Distribuidora
	aTpEmp[2] := "X"
elseif mv_tpemp == 3	//Importador
	aTpEmp[3] := "X"
else		//Outros
	aTpEmp[4] := "X"
endif
nLin := 0

FmtLin({},										_aLCab[01],,,@nLin)
FmtLin({},															_aLCab[02],,,@nLin)
FmtLin({},															_aLCab[03],,,@nLin)
FmtLin({T_NomeMes(Val(mv_par01)),;
mv_par02,;
iif(!Empty(mv_par03),"GASOLINA","DIESEL"),;
Str(nPag++,2)},														_aLCab[04],,,@nLin)
FmtLin({},															_aLCab[05],,,@nLin)
FmtLin({},															_aLCab[06],,,@nLin)
FmtLin({},															_aLCab[07],,,@nLin)
FmtLin({aTpEmp[1],aTpEmp[2],aTpEmp[3],aTpEmp[4]},					_aLCab[08],,,@nLin)
FmtLin({},															_aLCab[09],,,@nLin)
FmtLin({Transf(SM0->M0_CGC,"@R 99.999.999/9999-99"),;
SM0->M0_INSC},												_aLCab[10],,,@nLin)
FmtLin({},															_aLCab[11],,,@nLin)
FmtLin({SM0->M0_NOMECOM},											_aLCab[12],,,@nLin)
FmtLin({},															_aLCab[13],,,@nLin)
FmtLin({AllTrim(SM0->M0_ENDENT)+" - "+AllTrim(SM0->M0_COMPENT)+" - "+AllTrim(SM0->M0_CIDENT),;
SM0->M0_ESTENT},											_aLCab[14],,,@nLin)
FmtLin({},															_aLCab[15],,,@nLin)
Return()                                          




