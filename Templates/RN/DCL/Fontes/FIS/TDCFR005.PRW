/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �          �   Adalberto Moreno Batista    � Data �20.05.2003���
�������������������������������������������������������������������������Ĵ��
���Descricao � Emissao do ANEXO V conforme convenio 54/02                 ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Especifico PDV Brasil                                      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
#include "rwmake.ch"
#include "TOPCONN.CH"

Template Function TDCFR005()

Local cDesc1	:= "Este programa tem como objetivo imprimir o ANEXO V conforme legislacao para empresas que comercializam produtos derivados de petroleo"
Local titulo	:= "Anexo V do Convenio 54/02"
Local imprime	:= .T.
Local aRegs		:= {}
Local I:=0, J:=0
Private lEnd       := .F.
Private lAbortPrint:= .F.
Private limite     := 132
Private tamanho    := "M"
Private nomeprog   := "TDCFR005" // Coloque aqui o nome do programa para impressao no cabecalho
Private nTipo      := 15
Private aReturn    := { "Zebrado", 1, "Administracao", 1, 2, 1, "", 1}
Private nLastKey   := 0
Private cPerg      := PadR("ANXV",Len(SX1->X1_GRUPO)) 
Private cbtxt      := Space(10)
Private cbcont     := 00
Private CONTFL     := 01
Private m_pag      := 01
Private wnrel      := "ANXV" // Coloque aqui o nome do arquivo usado para impressao em disco
Private cString    := "LDI"
Private nLin
Private nPag
Private mv_tpemp, mv_nome, mv_cpf, mv_rg, mv_ufrg, mv_cargo, mv_fone

CHKTEMPLATE("DCLFIS") 

//---- DELETE PERGUNTA CPR005 ----      
DbSelectArea("SX1")
SX1->(DbSetOrder(1))   

If SX1->(DbSeek("CFR005"))
	Do while Alltrim(SX1->X1_GRUPO) == "CFR005"
		RecLock("SX1",.F.)
		SX1->(dbDelete())
		MsUnLock()
		SX1->(DBSKIP())
	Enddo
EndIf  

//---- DELETE PERGUNTA ANEXV ----      
DbSelectArea("SX1")
SX1->(DbSetOrder(1))   

If SX1->(DbSeek("ANEXV"))
	Do while Alltrim(SX1->X1_GRUPO) == "ANEXV"
		RecLock("SX1",.F.)
		SX1->(dbDelete())
		MsUnLock()
		SX1->(DBSKIP())
	Enddo
EndIf

//����������������������������������������������������������ٱ�
//						"ANXV"	     						ٱ�
//����������������������������������������������������������ٱ�

/*-----------------------"mv_par01"--------------------------*/

aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}

Aadd( aHelpPor, "Informar o M�s para gerar")
Aadd( aHelpPor, " o relat�rio Anexo V ")
HelpEng	:=	aHelpSpa	:=	aHelpPor
      
PutSx1(cPerg, "01","M�s:","M�s:","M�s:",;
	"mv_ch1","C",2,0,0,"G","Pertence('01/02/03/04/05/06/07/08/09/10/11/12')","","","","mv_par01","","","","",;
	"","","","","","","","","","",""," ",aHelpPor,aHelpEng,aHelpSpa)

/*-----------------------"mv_par02"--------------------------*/

aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}

Aadd( aHelpPor, "Informar o Ano para gerar")
Aadd( aHelpPor, " o relat�rio Anexo V ")
HelpEng	:=	aHelpSpa	:=	aHelpPor
      
PutSx1(cPerg, "02","Ano:","Ano:","Ano:",;
	"mv_ch2","C",4,0,0,"G",,"","","","mv_par02","","","","",;
	"","","","","","","","","","",""," ",aHelpPor,aHelpEng,aHelpSpa)    
	
	
	
/*-----------------------"mv_par03"--------------------------*/

aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}

Aadd( aHelpPor, "Informar o Grupo do Anexo(Chave)")       
Aadd( aHelpPor, " para gera��o do resumo Anexo V - ")
Aadd( aHelpPor, " �LCOOL ET�LICO ANIDRO")
HelpEng	:=	aHelpSpa	:=	aHelpPor
      
PutSx1(cPerg, "03","Grupo AEAC:","Grupo AEAC:","Grupo AEAC:",;
	"mv_ch3","C",5,0,0,"G","ValidSX1mv(mv_par03)","","","","mv_par03","","","","",;
	"","","","","","","","","","",""," ",aHelpPor,aHelpEng,aHelpSpa)
	

/*-----------------------"mv_par04"--------------------------*/

aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}

Aadd( aHelpPor, "Informar o Grupo do Anexo(Chave)")       
Aadd( aHelpPor, " para gera��o do resumo Anexo V - ")
Aadd( aHelpPor, " BIODIESEL-B100 COMBUST�VEL")
HelpEng	:=	aHelpSpa	:=	aHelpPor
      
PutSx1(cPerg, "04","Grupo Biodiesel:","Grupo Biodiesel:","Grupo Biodiesel:",;
	"mv_ch4","C",5,0,0,"G","ValidSX1mv(mv_par04)","","","","mv_par04","","","","",;
	"","","","","","","","","","",""," ",aHelpPor,aHelpEng,aHelpSpa)
		
		
/*-----------------------"mv_par05"--------------------------*/

aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}

Aadd( aHelpPor, "Cod.Fornecedor de Destino")       
HelpEng	:=	aHelpSpa	:=	aHelpPor
      
PutSx1(cPerg, "05","Cod.Fornec.Dest.:","Cod.Fornec.Dest.:","Cod.Fornec.Dest.:",;
	"mv_ch5","C",6,0,0,"G",,"SA2","","","mv_par05","","","","",;
	"","","","","","","","","","",""," ",aHelpPor,aHelpEng,aHelpSpa)  			

/*-----------------------"mv_par06"--------------------------*/

aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}

Aadd( aHelpPor, "Cod. Loja de Destino")       
HelpEng	:=	aHelpSpa	:=	aHelpPor
      
PutSx1(cPerg, "06","Lj.Fornece.Dest.:","Lj.Fornece.Dest.:","Lj.Fornece.Dest.:",;
	"mv_ch6","C",2,0,0,"G",,"","","","mv_par06","","","","",;
	"","","","","","","","","","",""," ",aHelpPor,aHelpEng,aHelpSpa)  		

Pergunte("CFAIMP",.F.)
mv_tpemp	:= mv_par01
mv_nome		:= mv_par02
mv_cpf		:= mv_par03
mv_rg		:= mv_par04
mv_ufrg		:= mv_par05
mv_cargo	:= mv_par06
mv_fone		:= mv_par07


if Pergunte(cPerg,.T.)

	
	If !Empty(MV_PAR03) .and. !Empty(MV_PAR04)
		MsgAlert("Para gera��o deste relat�rio preencher pelo a chave!")
		Pergunte(cPerg,.T.)
	endif
	
	wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,"","",.F.,,.F.,Tamanho,,.F.)
	
	If nLastKey == 27
		Return
	Endif
	
	SetDefault(aReturn,cString)
	
	If nLastKey == 27
		Return
	Endif
	
	nTipo := If(aReturn[4]==1,15,18)
	
	//���������������������������������������������������������������������Ŀ
	//� Processamento. RPTSTATUS monta janela com a regua de processamento. �
	//�����������������������������������������������������������������������
	RptStatus({|| RunReport() },Titulo)
endif
Return()

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao� RunReport    �                               �      �          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RunReport()
Local _cQuery, i, k, w, aCad, nPos, _aSTot, _aGTot, nBase, lCab
Local _aLCab 	:= T_LOAnexo("C",5)
Local _aLQd2	:= {}
Local _aLQd4	:= {}
Local _aLQd5	:= {}
Local _aDQd41	:= {}
Local _aDQd42	:= {}
Local _aEst		:= {}
Local _nDQd51 	:= 0
Local _nDQd52 	:= 0
Local _aLRod 	:= T_LOAnexo("R")
Local _cListaMV3 := "" 
Local _cListaMV4 := ""
Local _aPxChvMV3 := IIf (!Empty(mv_par03),ListaIN(mv_par03),{})   // Filtra da fun��o ListaIN - colhendo em um Array os Subprodutos de tal chave.
Local _aPxChvMV4 := IIf (!Empty(mv_par04),ListaIN(mv_par04),{})   // Filtra da fun��o ListaIN - colhendo em um Array os Subprodutos de tal chave.
Local _cPerAnt  := iif(mv_par01<>"01",mv_par02+StrZero(Val(mv_par01)-1,2),StrZero(Val(mv_par02)-1,4)+"12")
Local _aPropor 	:= {}
Local _nTProp	:= 0 
Local nA		:= 0  
Local nB 		:= 0
Local nC		:= 0
Local _cPeriodo := mv_par02+mv_par01       
Local cRefina      := Alltrim(GetNewPar("MV_REFINA",""))     // Informar se o fornecedor ou cliente � refinaria 

Private cEst

//Definindo as variaveis
nPag := 1
nLin := 100

//                    1         2         3         4         5         6         7         8         9         0         1         2         3
//           123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
aAdd(_aLQd2,' 2-DADOS DO DESTINATARIO DO RELATORIO')																								//1
aAdd(_aLQd2,' 3-DADOS DO SUJEITO PASSIVO POR SUBSTITUICAO QUE TIVER ORIGINALMENTE RETIDO O IMPOSTO DA GASOLINA "A"')								//2
aAdd(_aLQd2,'+------+--------------------+                                                         +--------------------+---------------------+')	//3
aAdd(_aLQd2,'| CNPJ | ################## |                                                         | INSCRICAO ESTADUAL | ################### |')	//4
aAdd(_aLQd2,'+------+-------+------------+---------------------------------------------------------+--------------------+---------------------+')	//5
aAdd(_aLQd2,'| RAZAO SOCIAL | ############################################################################################################### |')	//6
aAdd(_aLQd2,'+----------+---+-------------------------------------------------------------------------------------------------------+----+----+')	//7
aAdd(_aLQd2,'| ENDERECO | ######################################################################################################### | UF | ## |')	//8
aAdd(_aLQd2,'+----------+-----------------------------------------------------------------------------------------------------------+----+----+')	//9

aAdd(_aLQd4,' 4-APURACAO DO IMPOSTO DEVIDO A UF DE ORIGEM DO AEAC NO PERIODO')												//1
aAdd(_aLQd4,' 4.1-AQUISICOES EFETUADAS PELO EMITENTE DO RELATORIO')                                                         //2
aAdd(_aLQd4,' 4.2-AQUISICOES EFETUADAS POR CLIENTES DO EMITENTE DO RELATORIO')                                              //3
aAdd(_aLQd4,'+--------------------+---------+----------------------------+-------------------------------------------+')    //4
aAdd(_aLQd4,'| CNPJ DO REMETENTE  |PROPORCAO|      QUANTIDADES DE AEAC   |        ICMS DEVIDO A UF DE ORIGEM         |')    //5
aAdd(_aLQd4,'| DO AEAC            |         |    TOTAL    | PROPORCIONAL |   BASE CALCULO |ALIQUOTA |           ICMS |')    //6
aAdd(_aLQd4,'+--------------------+---------+-------------+--------------+----------------+---------+----------------+')    //7
aAdd(_aLQd4,'| ################## | ####### | ########### |  ########### | ############## |   ##    | ############## |')    //8
aAdd(_aLQd4,'| SOMA......................................................| ############## |         | ############## |')    //9
aAdd(_aLQd4,'| TOTAL DO PERIODO..........................................| ############## |         | ############## |')    //10
aAdd(_aLQd4,'+-----------------------------------------------------------+----------------+---------+----------------+')    //11

aAdd(_aLQd5,' 5-RESULTADO DA APURACAO')															//1
aAdd(_aLQd5,'+-----------------------------------------------------------+----------------+')   //2
aAdd(_aLQd5,'| 5.1-IMPOSTO A SER REPASSADO A UF DE ORIGEM                | ############## |')   //3
aAdd(_aLQd5,'| 5.2-IMPOSTO A SER PROVISIONADO PELA REFINARIA             | ############## |')   //4


// Ordena em uma lista para tratamento na Query - AEAC
If !Empty(_aPxChvMV3)
	For nB := 1 To Len(_aPxChvMV3)  
   		_cListaMV3 += "'"    
		_cListaMV3 += _aPxChvMV3[nB]
   		_cListaMV3 += "'"    			
		IIf(nB <> Len(_aPxChvMV3),_cListaMV3 += ",",)                           
	Next 
Else
   _cListaMV3 := "''"
Endif                

// Ordena em uma lista para tratamento na Query  -B100

If !Empty(_aPxChvMV4)
	For nC := 1 To Len(_aPxChvMV4)  
	   		_cListaMV4 += "'"    
	    	_cListaMV4 += _aPxChvMV4[nC]
	   		_cListaMV4 += "'"    			
		IIf(nC <> Len(_aPxChvMV4),_cListaMV4 += ",",)                           
	Next 
Else
   _cListaMV4 := "''"
Endif

	
             
#IFDEF TOP
	//Selecionando as entradas de Alcool Anidro
	_cQuery := "SELECT LDI_EST, LDI_CNPJ, LDI_ALIQO, LDI_VICMST, SUM(LDI_QTDE) LDI_QTDE, SUM(LDI_BASEST) LDI_BASEST,  " 
	_cQuery += "SUM(LDI_TOTAL) LDI_TOTAL " 
	_cQuery += "FROM "+RetSQLName("LDI")+" LDI "
	_cQuery += "WHERE LDI.LDI_FILIAL = '"+xFilial("LDI")+"'"
	_cQuery +=       " AND LDI.LDI_ANOMES = '"+mv_par02+mv_par01+"'"
	_cQuery +=       " AND (LDI.LDI_COD IN ("+_cListaMV3+") " 
	_cQuery +=       " OR LDI.LDI_COD IN ("+_cListaMV4+")) "
	//_cQuery +=       " AND LDI.LDI_CLIFOR = '"+MV_PAR06+"'"
	//_cQuery +=       " AND LDI.LDI_LOJA = '"+MV_PAR07+"'"
	_cQuery +=       " AND LDI.LDI_EST <> '"+SM0->M0_ESTENT+"'"
	_cQuery +=       " AND LDI.LDI_TPMOV = '1'"  //Somente registros de entrada
	_cQuery +=       " AND LDI.LDI_TPDORI <> 'C'"  //nao considera Devolucao de venda 18/08/04
	_cQuery +=       " AND LDI.D_E_L_E_T_ <> '*' "
	_cQuery += "GROUP BY LDI.LDI_EST, LDI.LDI_CNPJ, LDI_ALIQO, LDI_VICMST "
	_cQuery += "ORDER BY LDI.LDI_EST, LDI.LDI_CNPJ, LDI_ALIQO, LDI_VICMST "
	
	// Verificando se a existencia da area TRB
	if Select("TRB") > 0
		dbSelectArea("TRB")
		dbCloseArea()
	endif
	// Criando a tabela temporaria
	_cQuery := ChangeQuery(_cQuery)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,_cQuery),"TRB", .F., .T.)
	
#ELSE

	//��������������������������������������������������������������Ŀ
	//� Cria arquivo Temporario                                      �
	//����������������������������������������������������������������
	_aTRB := {}
	AADD(_aTRB,{"LDI_CNPJ"   ,"C",14,0})
	AADD(_aTRB,{"LDI_QTDE"   ,"N",09,0}) 
	AADD(_aTRB,{"LDI_EST"    ,"C",02,0})
	AADD(_aTRB,{"LDI_AQLIQD" ,"N",06,2})
	AADD(_aTRB,{"LDI_BASEST" ,"N",12,2})
	AADD(_aTRB,{"LDI_TOTAL"  ,"N",12,2})
	
	// Verificando se a existencia da area TRB
	if Select("TRB") > 0
		dbSelectArea("TRB")
		dbCloseArea()
	endif
	
	c_ArqTRB := CriaTrab(_aTRB,.t.)
	dbUseArea(.t.,,c_ArqTRB,"TRB")
	dbSelectArea("TRB")
	IndRegua("TRB",c_ArqTRB,"LDI_EST+LDI_CNPJ",,,"Criando Indice...")
	
	dbSelectArea("LDI")
	dbSetOrder(1)
	If dbSeek(xFilial("LDI")+MV_PAR02+MV_PAR01)
		Do While !eof() .and. LDI->LDI_FILIAL == SM0->M0_CODFIL .AND. ;
			LDI->LDI_ANOMES == MV_PAR02+MV_PAR01
			
			dbSelectArea("TRB")
			If dbSeek(LDI->LDI_CNPJ,.T.)
				RecLock("TRB",.F.)
				TRB->LDI_QTDE	+= LDI->LDI_QTDE
				TRB->LDI_BASEST	+= LDI->LDI_BASEST
				TRB->LDI_TOTAL	+= LDI->LDI_TOTAL
				TRB->LDI_ALIQO	+= LDI->LDI_ALIQO
				TRB->LDI_VICMST	+= LDI->LDI_VIMCST
				msUnlock()
			Else
				RecLock("TRB",.T.)
				TRB->LDI_EST 	:= LDI->LDI_EST
				TRB->LDI_CNPJ	:= LDI->LDI_CNPJ
				TRB->LDI_QTDE	:= LDI->LDI_QTDE
				TRB->LDI_BASEST	:= LDI->LDI_BASEST
				TRB->LDI_TOTAL	:= LDI->LDI_TOTAL
				TRB->LDI_ALIQO	:= LDI->LDI_ALIQO
				TRB->LDI_VICMST	:= LDI->LDI_VIMCST
				msUnlock()
			EnDif
			dbSeLectArea("LDI")
			dbSkip()
			Loop
		Enddo
	EnDif	
#ENDIF

dbSelectArea("TRB")
dbGoTop()
Do while !("TRB")->(Eof())
	
	cEst 		:= TRB->LDI_EST
	dbSelectArea("TRB")
	//Alimentado o quadro 4.1

	nPos		:= aScan(_aDQd41,{|x| x[1]=TRB->LDI_CNPJ})
	nBase       := TRB->LDI_BASEST
	if nPos=0
		TRB->(aAdd(_aDQd41,{TRB->LDI_CNPJ,TRB->LDI_QTDE,nBase,TRB->LDI_ALIQO,TRB->LDI_VICMST,TRB->LDI_EST}))
	else
		_aDQd41[nPos,2] += TRB->LDI_QTDE
		_aDQd41[nPos,3] += nBase
		_aDQd41[nPos,5] += TRB->LDI_VICMST
	endif
	
	//Alimentado o quadro 4.2
	
	//Alimentando array de estados remetentes de AEAC
	if aScan(_aEst,TRB->LDI_EST)=0
		aAdd(_aEst,TRB->LDI_EST)
	endif
	
	("TRB")->(dbSkip())
enddo

_aGTot := {0,0}
//Imprimindo o anexo
if Len(_aDQd41) >= 1 .or. Len(_aDQd42) >= 1
	
	//primeira quebra por estado remetente do AEAC
	for w:=1 to len(_aEst)
		nPag := 1
		cEst := _aEst[w]
		
		//segunda quebra por proporcionalidade de fornecedores de Gasolina A
		for i:=1 to len(_aDQd41)
		    _aGTot := {0,0}
			nLin   := 0
			
			ImpCab(_aLCab)
			

			aCad := GetAdvFVal("SA2",{"A2_INSCR","A2_NOME","A2_END","A2_MUN","A2_EST","A2_CGC","A2_X_CT"},xFilial("SA2")+MV_PAR05+MV_PAR06,1)			

			//Quadro 2
			FmtLin({},						_aLQd2[01],,,@nLin)
			FmtLin({},						_aLQd2[03],,,@nLin)
			FmtLin({Transf(aCad[6],"@R 99.999.999/9999-99"),;
			aCad[1]},				_aLQd2[04],,,@nLin)
			FmtLin({},						_aLQd2[05],,,@nLin)
			FmtLin({aCad[2]},				_aLQd2[06],,,@nLin)
			FmtLin({},						_aLQd2[07],,,@nLin)
			FmtLin({AllTrim(aCad[3])+" - "+AllTrim(aCad[4]),;
			aCad[5]},				_aLQd2[08],,,@nLin)
			FmtLin({},						_aLQd2[09],,,@nLin)

			If aCad[7]$cRefina
				aCad := GetAdvFVal("SA2",{"A2_INSCR","A2_NOME","A2_END","A2_MUN","A2_EST","A2_CGC","A2_X_CT"},xFilial("SA2")+MV_PAR05+MV_PAR06,1)			
					
				//Quadro 3
				FmtLin({},						_aLQd2[02],,,@nLin)
				FmtLin({},						_aLQd2[03],,,@nLin)
				FmtLin({Transf(aCad[6],"@R 99.999.999/9999-99"),;
				aCad[1]},				_aLQd2[04],,,@nLin)
				FmtLin({},						_aLQd2[05],,,@nLin)
				FmtLin({aCad[2]},				_aLQd2[06],,,@nLin)
				FmtLin({},						_aLQd2[07],,,@nLin)
				FmtLin({AllTrim(aCad[3])+" - "+AllTrim(aCad[4]),;
				aCad[5]},				_aLQd2[08],,,@nLin)
				FmtLin({},						_aLQd2[09],,,@nLin) 
			Endif   
			
			//Quadro 4
			FmtLin({},	_aLQd4[01],,,@nLin)
			//Quandro 4.1
			_aSTot := {0,0}         
			lCab   := .T. 
			if len(_aDQd41)>=1
				for k:=1 to len(_aDQd41)
					if lCab 
						FmtLin({},	_aLQd4[02],,,@nLin)
						FmtLin({},	_aLQd4[04],,,@nLin)
						FmtLin({},	_aLQd4[05],,,@nLin)
						
						FmtLin({},	_aLQd4[06],,,@nLin)
						FmtLin({},	_aLQd4[07],,,@nLin)
						lCab := .F. 
					endif
					FmtLin({Transf(_aDQd41[k,1],"@R 99.999.999/9999-99"),; //cnpj
					Transf("100,00","@E 999.99"),;        //Propor��o
					Transf(_aDQd41[k,2],"@E 999,999,999"),;  //Total
					Transf(_aDQd41[k,2],"@E 999,999,999"),;     //Proporcional
					Transf(_aDQd41[k,3],"@E 999,999,999.99"),;   //BASE CALCULO ST
					Transf(_aDQd41[k,4],"@E 999"),;           //ALIQUOTA
					Transf(_aDQd41[k,5],"@E 999,999,999.99")},		_aLQd4[08],,,@nLin)    //ICMS
					FmtLin({},													    _aLQd4[07],,,@nLin)
					_aSTot[1] += _aDQd41[k,3]
					_aSTot[2] += _aDQd41[k,5]
					_aGTot[1] += _aDQd41[k,3]
					_aGTot[2] += _aDQd41[k,5]
				next
				FmtLin({Transf(_aSTot[1],"@E 999,999,999.99"),;
				Transf(_aSTot[2],"@E 999,999,999.99")},	_aLQd4[09],,,@nLin)
				FmtLin({},								_aLQd4[11],,,@nLin)
				
				_nDQd51 := _aSTot[1]
			endif
			
			//Quandro 4.2
			_aSTot := {0,0}        
			lCab   := .T. 
			if len(_aDQd42)>=1
				for k:=1 to len(_aDQd42)
					if lCab 
						FmtLin({},	_aLQd4[03],,,@nLin)
						FmtLin({},	_aLQd4[04],,,@nLin)
						FmtLin({},	_aLQd4[05],,,@nLin)
						FmtLin({},	_aLQd4[06],,,@nLin)
						FmtLin({},	_aLQd4[07],,,@nLin)
						lCab := .F. 
					endif
					FmtLin({Transf(_aDQd42[k,1],"@R 99.999.999/9999-99"),;
					Transf("100,00","@E 999.99"),;
					Transf(_aDQd42[k,2],"@E 999,999,999"),;
					Transf(_aDQd42[k,2],"@E 999,999,999"),;
					Transf(_aDQd42[k,3],"@E 999,999,999.99"),;
					Transf(_aDQd42[k,4],"@E 999"),;
					Transf(_aDQd42[k,5],"@E 999,999,999.99")},    _aLQd4[08],,,@nLin)
					FmtLin({},													_aLQd4[07],,,@nLin)
					_aSTot[1] += _aDQd42[k,3]
					_aSTot[2] += _aDQd42[k,5]
					_aGTot[1] += _aDQd42[k,3]
					_aGTot[2] += _aDQd42[k,5]
				next
				FmtLin({Transf(_aSTot[1],"@E 999,999,999.99"),;
				Transf(_aSTot[2],"@E 999,999,999.99")},	_aLQd4[09],,,@nLin)
				FmtLin({},							    _aLQd4[11],,,@nLin)
				
				_nDQd52 := _aSTot[2]
			endif
			
			FmtLin({Transf(_aGTot[1],"@E 999,999,999.99"),;
			Transf(_aGTot[2],"@E 999,999,999.99")},	_aLQd4[10],,,@nLin)
			FmtLin({},										_aLQd4[11],,,@nLin)
			
			//Quadro 5
			FmtLin({},										_aLQd5[01],,,@nLin)
			FmtLin({},		 								_aLQd5[02],,,@nLin)
			FmtLin({Transf(_nDQd51,"@E 999,999,999.99")},	_aLQd5[03],,,@nLin)
			FmtLin({},										_aLQd5[02],,,@nLin)
			FmtLin({Transf(_nDQd52,"@E 999,999,999.99")},	_aLQd5[04],,,@nLin)
			FmtLin({},										_aLQd5[02],,,@nLin)    
			T_IRodAnexo(_aLRod)	
		next
	next   	
else
	nLin := 0
	ImpCab(_aLCab,1)
	nLin++
	T_ISemMovAnexo()
	T_IRodAnexo(_aLRod)
endif   
 

// Verificando se a existencia da area TRB
if Select("TRB") > 0
	dbSelectArea("TRB")
	dbCloseArea()
endif

//���������������������������������������������������������������������Ŀ
//� Finaliza a execucao do relatorio...                                 �
//�����������������������������������������������������������������������
Set Device to Screen

//���������������������������������������������������������������������Ŀ
//� Se impressao em disco, chama o gerenciador de impressao...          �
//�����������������������������������������������������������������������

If aReturn[5]==1
	dbCommitAll()
	SET PRINTER TO
	OurSpool(wnrel)
Endif

MS_FLUSH()

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao� ImpCab       �                               �      �          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpCab(_aLCab,nTotPag)
FmtLin({},															_aLCab[01],,,@nLin)
FmtLin({},															_aLCab[02],,,@nLin)
FmtLin({},															_aLCab[03],,,@nLin)
FmtLin({T_NomeMes(Val(mv_par01)),;
mv_par02,;
cEst,;
Str(nPag++,2)},										_aLCab[04],,,@nLin)
FmtLin({},															_aLCab[05],,,@nLin)
FmtLin({},															_aLCab[06],,,@nLin)
FmtLin({},															_aLCab[07],,,@nLin)
FmtLin({Transf(SM0->M0_CGC,"@R 99.999.999/9999-99"),;
SM0->M0_INSC},												_aLCab[08],,,@nLin)
FmtLin({},															_aLCab[09],,,@nLin)
FmtLin({SM0->M0_NOMECOM},											_aLCab[10],,,@nLin)
FmtLin({},															_aLCab[11],,,@nLin)
FmtLin({AllTrim(SM0->M0_ENDENT)+" - "+AllTrim(SM0->M0_COMPENT)+" - "+AllTrim(SM0->M0_CIDENT),;
SM0->M0_ESTENT},											_aLCab[12],,,@nLin)
FmtLin({},															_aLCab[13],,,@nLin)
Return()
