/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � TSMPA008 � Autor � Itamar Oliveira       � Data � 30/11/04 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Edicao da tabela LX5 - Tabela 07-> Ativ. Economica ANP	  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Template DCL (Distribuicao de Combustiveis e Lubrificantes)���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

#Include "PROTHEUS.CH"

Template Function TSMPA008()
Local nQ:=0, nFiliais:=0, nCon:=0

SetPrvt("NOPCX,CTABELA,AFILIAIS,CEMPRESA,NREGSM0,AHEADER")
SetPrvt("NUSADO,LX5_FILIAL,LX5_TABELA,LX5_CHAVE,LX5_DESCRI,CCHAVE")
SetPrvt("CDESCRI,ACOLS,NQ,NLINGETD,CTITULO,AC")
SetPrvt("AR,ACGD,CLINHAOK,CTUDOOK,N,NFILIAIS")

#IFDEF PROTHEUS
	SetPrvt("NOPCX,NUSADO,AHEADER,ACOLS,CCHAVE,CDESCRI")
	SetPrvt("NLINGETD,CTITULO,AC,AR,ACGD,CLINHAOK,CTUDOOK,AFILIAIS,CEMPRESA,NREGSM0")
#ENDIF

//+--------------------------------------------------------------+
//� Opcao de acesso para o Modelo 2                              �
//+--------------------------------------------------------------+
// 3,4 Permitem alterar getdados e incluir linhas
// 6 So permite alterar getdados e nao incluir linhas
// Qualquer outro numero so visualiza

CHKTEMPLATE("DCLDCP")

nOpcx   := 3      //Adcionado o valor "3" na Variavel para 
                  //permitir a edicao do conteudo
cTabela := "07"  // Tabela 07 Atividades Economicas ANP

//+--------------------------------------------------------------+
//� Montando aHeader                                             �
//+--------------------------------------------------------------+

DbSelectArea("SM0")
DbSetOrder(1)

If Sx2->(DbSeek("LX5")) .And. SX2->X2_MODO == "E"
	aFiliais := { }
	cEmpresa := SM0->M0_CODIGO
	nRegSM0  := Recno()
	DbGoTop()
	While ! Eof()
		If cEmpresa == SM0->M0_CODIGO
			AADD(aFiliais, SM0->M0_CODFIL)
		Endif
		DbSkip()
	EndDo
	DbGoTo(nRegSM0)
Else
	aFiliais := { xFilial() }
Endif

dbSelectArea("SX3")
dbSetOrder(1)
dbSeek("LX5")

aHeader := {}
nUsado  := 0

While !Eof() .And. (X3_ARQUIVO == "LX5")
	If X3Uso(X3_USADO) .AND. cNivel >= X3_NIVEL
		If AllTrim(X3_CAMPO) $ "LX5_DESCRI*LX5_CHAVE"
			nUsado := nUsado + 1
			AADD(aHeader,{ TRIM(X3_TITULO), X3_CAMPO, X3_PICTURE, X3_TAMANHO, X3_DECIMAL,;
			"!Empty(M->" + AllTrim(X3_CAMPO) + ")", X3_USADO  , X3_TIPO,;
			X3_ARQUIVO, X3_CONTEXTO })
		EndIf
	Endif
	dbSkip()
End

dbSelectArea("LX5")
dbSetOrder(1)

If ! dbSeek(xFilial() + "00" + cTabela)
	RecLock("LX5", .T.)
	LX5_FILIAL := xFilial()
	LX5_TABELA := "00"
	LX5_CHAVE  := cTabela
	LX5_DESCRI := "Atividades Economicas ANP"
	MsUnLock()
EndIf

//+--------------------------------------------------------------+
//� Variaveis do Cabecalho do Modelo 2                           �
//+--------------------------------------------------------------+
cChave  := AllTrim(LX5->LX5_CHAVE)
cDescri := Substr(LX5->LX5_DESCRI, 1, 35)

//��������������������������������������������������������������Ŀ
//� Posiciona os itens da tabela conforme a filial corrente      �
//����������������������������������������������������������������
dbSeek(xFilial() + cTabela)

//+--------------------------------------------------------------+
//� Montando aCols                                               �
//+--------------------------------------------------------------+
aCols    := {}

While !Eof() .And. LX5->LX5_FILIAL == xFilial() .And. LX5->LX5_TABELA == cTabela
	Aadd(aCols, Array(nUsado+1))
	For nQ :=1 To nUsado
		aCols[Len(aCols),nQ] := FieldGet(FieldPos(aHeader[nQ,2]))
	Next
	aCols[Len(aCols),nUsado + 1] := .F.
	dbSkip()
EndDo

If Len(aCols) == 0
	AADD(aCols,Array(nUsado+1))
	For nQ := 1 To nUsado
		aCols[Len(aCols),nQ] := CriaVar(FieldName(FieldPos(aHeader[nQ,2])))
	Next
	aCols[Len(aCols),nUsado+1] := .F.
EndIf

//+--------------------------------------------------------------+
//� Variaveis do Rodape do Modelo 2                              �
//+--------------------------------------------------------------+
nLinGetD :=0

//+--------------------------------------------------------------+
//� Titulo da Janela                                             �
//+--------------------------------------------------------------+
cTitulo := cDescri

//+--------------------------------------------------------------+
//� Array com descricao dos campos do Cabecalho do Modelo 2      �
//+--------------------------------------------------------------+
// aC[n,1] = Nome da Variavel Ex.:"cCliente"
// aC[n,2] = Array com coordenadas do Get [x,y], em Windows estao em PIXEL
// aC[n,3] = Titulo do Campo
// aC[n,4] = Picture
// aC[n,5] = Validacao
// aC[n,6] = F3
// aC[n,7] = Se campo e' editavel .t. se nao .f.

aC := {}

#IFDEF WINDOWS
	AADD(aC, {"cChave" , {20,05}, "Tabela ", "@!", " ", "", .F.})
	AADD(aC, {"cDescri", {20,50}, " "      , "@!", " ", "", .F.})
#ELSE
	AADD(aC, {"cChave" , {5,15} , "Tabela ", "@!", " ", "", .F.})
	AADD(aC, {"cDescri", {5,50} , " "      , "@!", " ", "", .F.})
#ENDIF


aR := {}

//+--------------------------------------------------------------+
//� Array com coordenadas da GetDados no modelo2                 �
//+--------------------------------------------------------------+
#IFDEF WINDOWS
	aCGD := {44,5,118,315}
#ELSE
	aCGD := {10,04,15,73}
#ENDIF


//+--------------------------------------------------------------+
//� Validacoes na GetDados da Modelo 2                           �
//+--------------------------------------------------------------+
cLinhaOk := "(!Empty(aCols[n,2]) .Or. aCols[n,3])"
cTudoOk  := "AllwaysTrue()"

//+--------------------------------------------------------------+
//� Chamada da Modelo2                                           �
//+--------------------------------------------------------------+
// Se Modelo2() retornou .t., confirmou, caso contrario cancelou
// No Windows existe a funcao de apoio CallMOd2Obj() que retorna o
// objeto Getdados Corrente

N:=1
If Modelo2(cTitulo, aC, aR, aCGD, nOpcx, cLinhaOk, cTudoOk,,,,9999)
	For nCon := 1 To Len(aCols)
		If aCols[nCon, Len(aHeader) + 1] // Ultima posicao de cada elemento aCols
			// Determina se o registro foi excluido ou nao
			For nFiliais := 1 TO Len(aFiliais) // Quantas filiais existirem
				If dbSeek(aFiliais[nFiliais] + cTabela + aCols[nCon, 1])
					aArea:=GetArea()
					DbSelectArea("SA1")
					DbOrderNickName("DCL_SIMP19")
					If DbSeek(xFilial() + AllTrim(aCols[nCon,1]))
						cMens:="A atividade economica " + AllTrim(aCols[nCon,1]) + " nao pode ser excluido por estar "
						cMens:=cMens+"relacionada com o cadastro de clientes."
						Alert(cMens)
						RestArea(aArea)
					Else
						DbSelectArea("SA2")
						DbOrderNickName("DCL_SIMP20")
						If DbSeek(xFilial() + AllTrim(aCols[nCon,1]))
							cMens:="A atividade economica " + AllTrim(aCols[nCon,1]) + " nao pode ser excluido por estar "
							cMens:=cMens+"relacionada com o cadastro de fornecedores."
							Alert(cMens)
							RestArea(aArea)
						Else					
							RestArea(aArea)
							RecLock("LX5",.F.,.T.)
							dbDelete()
						EndIf
					EndIf
				EndIf
			Next
		Else
			If ! Empty(aCols[nCon, 1]) // Caso a chave esteja em branco nao GRAVO
				For nFiliais := 1 TO Len(aFiliais) // Quantas filiais existirem
					If dbSeek(aFiliais[nFiliais] + cTabela + aCols[nCon,1])
						If aCols[nCon, 2] != LX5->LX5_DESCRI
							RecLock("LX5",.F.)
							Replace LX5_DESCRI with aCols[nCon,2]
						EndIf
					Else
						RecLock("LX5",.T.)
						Replace LX5_FILIAL with aFiliais[nFiliais]
						Replace LX5_TABELA with cTabela
						Replace LX5_CHAVE  with aCols[nCon,1]
						Replace LX5_DESCRI with aCols[nCon,2]
					EndIf
					MsUnlock()
				Next
			Endif
		Endif
	Next
	DbCommitAll()
Endif

Return