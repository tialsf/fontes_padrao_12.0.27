#Include "PROTHEUS.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �TSMPA009  � Autor �Itamar Oliveira 	 � Data �  22/12/04   ���
�������������������������������������������������������������������������͹��
���Descricao �Cadastro de Produtos i-SIMP ANP							  ���
�������������������������������������������������������������������������͹��
���Uso       �Template DCL (Distribuicao de Combustiveis) SIMP			  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Template Function TSMPA009()
//���������������������������������������������������������������������Ŀ
//� Declaracao de Variaveis                                             �
//�����������������������������������������������������������������������

Private cString := "LHY"

CHKTEMPLATE("DCLDCP")

dbSelectArea("LHY")
dbSetOrder(1)

AxCadastro(cString,"Cadastro de Produtos i-SIMP", "T_ValExcProd()")
Return
         
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ValExc   � Autor �Itamar Oliveira 	 � Data �  16/02/05   ���
�������������������������������������������������������������������������͹��
���Descricao �Valida a exclusao dos produtos							  ���
�������������������������������������������������������������������������͹��
���Uso       �Template DCL (Distribuicao de Combustiveis) SIMP			  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Template Function ValExcProd()
Local lValida:=.T., Area:=GetArea(), cMens:=""

	// Verifica se existe no relacao no cadastro de produtos padrao.
	// Se existir a relacao nao deixa apagar.
	DbSelectArea("SB1")
	DbOrderNickName("DCL_SIMP1") // Codigo i-SIMP do Produto
	If DBSEEK(xFilial()+LHY->LHY_CODIGO)     
		cMens:= "Esse registro nao pode ser excluido por estar relacionado com o produto "
		cMens:= cMens + AllTrim(SB1->B1_COD) + "-" + AllTrim(SB1->B1_DESC) + " no cadastro de produtos padrao. "
		Alert(cMens)
		lValida:=.F.
	EndIf

RestArea(Area)
Return(lValida)