#Include "PROTHEUS.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �TSMPA010  � Autor �Itamar Oliveira 	 � Data �  22/12/04   ���
�������������������������������������������������������������������������͹��
���Descricao �Cadastro das Caract. Fis-Quimi do Produto					  ���
�������������������������������������������������������������������������͹��
���Uso       �Template DCL (Distribuicao de Combustiveis) SIMP			  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Template Function TSMPA010()
//���������������������������������������������������������������������Ŀ
//� Declaracao de Variaveis                                             �
//�����������������������������������������������������������������������
Private cString := "LI1"

CHKTEMPLATE("DCLDCP")

dbSelectArea("LI1")
dbSetOrder(1)

AxCadastro(cString,"Cadastro de Caract. Fis-Quim.","T_ValExcCFQ()")
Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ValExcCFQ � Autor �Itamar Oliveira 	 � Data �  17/02/05   ���
�������������������������������������������������������������������������͹��
���Descricao �Valida a exclusao das caracteristicas fis-quim. do produto  ���
�������������������������������������������������������������������������͹��
���Uso       �Template DCL (Distribuicao de Combustiveis) SIMP			  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Template Function ValExcCFQ()
Local lValida:=.T., Area:=GetArea(), cMens:=""

	// Verifica se existe relacao no cadastro de clientes e fornecedores
	// Se existir a relacao nao deixa apagar.
	DbSelectArea("LHW")
	DbOrderNickName("DCL_SIMP11") // Codigo do Laudo de Qualidade
	If DBSEEK(xFilial()+LI1->LI1_CODIGO)
		cMens:= "Esse registro nao pode ser excluido por estar relacionado com laudos de qualidade."
		Alert(cMens)
		lValida:=.F.
	EndIf

RestArea(Area)
Return(lValida)