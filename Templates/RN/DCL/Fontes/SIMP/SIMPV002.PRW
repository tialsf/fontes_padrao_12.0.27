/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � TSMPV002 � Autor � Itamar Oliveira       � Data � 17/01/05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Validacao para liberacao dos campos D1_T_EMBAR			  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Template DCL (Distribuicao de Combustiveis e Lubrificantes)���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

#Include "PROTHEUS.CH"

Template Function TSMPV002()
Local lRet		:= .T. // vari�vel de retorno
Local nPosCod 	:= aScan(aHeader,{|x| Upper(Alltrim(x[2]))== "D1_T_MODAL" }) //pega a posi��o do campo D1_T_MODAL
Local cCod		:= aCols[n,nPosCod] //pega o valor que esta no aCols da linha corrente e coluna especificada pela posi��o do campo D1_T_TTDCP

CHKTEMPLATE("DCLDCP")

If Val(cCod) != 4
	lRet := .F.
EndIf
 
Return lRet

