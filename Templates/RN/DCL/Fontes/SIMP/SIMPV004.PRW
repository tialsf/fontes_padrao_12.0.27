/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � TSMPV004 � Autor � Itamar Oliveira       � Data � 17/01/05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Validacao para liberacao de alteracao do campo LJ1_COD_SI  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Template DCL (Distribuicao de Combustiveis e Lubrificantes)���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

#Include "PROTHEUS.CH"

Template Function TSMPV004()
Local lValida:=.T., Area:=GetArea()

	If ALTERA
		DbSelectArea("SF4") // TES
		DbOrderNickName("DCL_SIMP2") // Codigo i-SIMP da Operacao
		If DBSEEK(xFilial()+M->LJ1_COD_SI)
			lValida:=.F.
		Else
			DbSelectArea("SF5") // TM
			DbOrderNickName("DCL_SIMP3") // Codigo i-SIMP da Operacao
			If DBSEEK(xFilial()+M->LJ1_COD_SI)
				lValida:=.F.
			EndIf
		EndIf
	EndIf

RestArea(Area)
Return(lValida)