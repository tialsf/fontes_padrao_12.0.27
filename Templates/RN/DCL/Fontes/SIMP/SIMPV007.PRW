/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � TSMPV007 � Autor � Itamar Oliveira       � Data � 18/01/05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Validacao para liberacao de alteracao do campo LDY_CODIGO  ���
���Tabela de Municipios													  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Template DCL (Distribuicao de Combustiveis e Lubrificantes)���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

#Include "PROTHEUS.CH"

Template Function TSMPV007()
Local lValida:=.T., Area:=GetArea()

	If ALTERA
		DbSelectArea("SA1") // Clientes
		DbOrderNickName("DCL_SIMP9") // Codigo i-SIMP do Municipio
		If DBSEEK(xFilial()+M->LDY_CODIGO)
			lValida:=.F.
		Else
			DbSelectArea("SA2") // Fornecedores
			DbOrderNickName("DCL_SIMP10") // Codigo i-SIMP do Municipio
			If DBSEEK(xFilial()+M->LDY_CODIGO)
				lValida:=.F.
			EndIf
		EndIf
	EndIf

RestArea(Area)
Return(lValida)