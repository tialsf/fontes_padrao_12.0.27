#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH"

/*
�����������������������������������������������������������������������������          
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ�� 
���Fun�ao    �TPLLX5    � Autor � 	Templates           � Data �Junho/2004���
�������������������������������������������������������������������������Ĵ��
���Descricao �Carga de Dados dos arquivos de transacoes (LDE e LDF) para: ���
���          �V609:LDE ---> OZ5  //  LDF ---> OZ6                         ���
���          �V710:LDE ---> OZ5(padrao na 710)//LDF --->OZ6(padrao na 710)���
�������������������������������������������������������������������������Ĵ��
���Obs:      �Os arqs OZ5 e OZ6 sao do padrao da V710. Como o aplicador do���
���          �template nao trata arqs q nao comecem com L (padrao de tpls)���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Template Function TPLEXEC(aTplEmp,aTplFiles)

Local ni
Local nj
Local cField
Local lNew

Local cTplEmp
Local cTplFil
Local cAllEmp := ""
Local nPos

Local lChkLX5 := .T.
Local lChkLXB := .T.
Local lChkLDY := .T.
Local lChkLHY := .T.
Local lChkLHZ := .T.
Local lChkLI1 := .T.
Local lChkLJ1 := .T.
Local lChkLJ7 := .T.
Local lChkLJN := .T.

Local nInc    := 0
Local nAlt    := 0

ConOut("Iniciando fun��o TPLLX5")

__VSave(aTplEmp,"Var1.kk")
__VSave(aTplFiles,"Var2.kk")

If ChkTplFile("LX5",aTplFiles,".DTP")
	ConOut("Ok. - LX5.DTP encontrado!")
Else
	ConOut("ERRO - LX5.DTP n�o encontrado")
	lChkLX5 := .F.
EndIf

If ChkTplFile("LXB",aTplFiles,".DTP")
	ConOut("Ok. - LXB.DTP encontrado!")
Else
	ConOut("ERRO - LXB.DTP n�o encontrado")
	lChkLXB := .F.
EndIf
     
If ChkTplFile("LDY",aTplFiles,".DTP")
	ConOut("Ok. - LDY.DTP encontrado!")
Else
	ConOut("ERRO - LDY.DTP n�o encontrado")
	lChkLDY := .F.
EndIf
     
If ChkTplFile("LHY",aTplFiles,".DTP")
	ConOut("Ok. - LHY.DTP encontrado!")
Else
	ConOut("ERRO - LHY.DTP n�o encontrado")
	lChkLHY := .F.
EndIf

If ChkTplFile("LHZ",aTplFiles,".DTP")
	ConOut("Ok. - LHZ.DTP encontrado!")
Else
	ConOut("ERRO - LHZ.DTP n�o encontrado")
	lChkLHZ := .F.
EndIf

If ChkTplFile("LI1",aTplFiles,".DTP")
	ConOut("Ok. - LI1.DTP encontrado!")
Else
	ConOut("ERRO - LI1.DTP n�o encontrado")
	lChkLI1 := .F.
EndIf

If ChkTplFile("LJ1",aTplFiles,".DTP")
	ConOut("Ok. - LJ1.DTP encontrado!")
Else
	ConOut("ERRO - LJ1.DTP n�o encontrado")
	lChkLJ1 := .F.
EndIf

If ChkTplFile("LJ7",aTplFiles,".DTP")
	ConOut("Ok. - LJ7.DTP encontrado!")
Else
	ConOut("ERRO - LJ7.DTP n�o encontrado")
	lChkLJ7 := .F.
EndIf

If ChkTplFile("LJN",aTplFiles,".DTP")
	ConOut("Ok. - LJN.DTP encontrado!")
Else
	ConOut("ERRO - LJN.DTP n�o encontrado")
	lChkLJN := .F.
EndIf

For ni := 1 To Len(aTplEmp)
	//  Faz a Checagem dos arquivos

	cTplEmp := Subs(aTplEmp[ni],1,2)
	cTplFil := Subs(aTplEmp[ni],3,2)

	//Fecha todos os arquivos abertos

	//prepara ambiente da empresa
	ConOut("Abrindo ambiente para empresa " + cTplEmp + " filial " + cTplFil + " ...")
	RpcSetEnv(cTplEmp,cTplFil)
	ConOut("Ambiente aberto!")

	ConOut("Verificando e abrindo arquivos")

	If !(cTplEmp $ cAllEmp)
		cAllEmp += cTplEmp + "#"
	
		If lChkLX5
			ChkFile("LX5")
			ConOut("Inicio do tratamento do arquivo LX5")
	
			DbSelectArea("LX5TPL")
			DbGoTop()
			While !Eof()
				DbSelectArea("LX5")
				lNew := !DbSeek(xFilial("LX5")+LX5TPL->LX5_TABELA+LX5TPL->LX5_CHAVE)
				If lNew
					nInc++
				Else
					nAlt++
				EndIf
				RecLock("LX5",lNew)
				For nj := 1 To FCount()
					uVar := NIL
					cField := Field(nj)
					If cField == "LX5_FILIAL"
						uVar := xFilial("LX5")
					Else
						nPos := LX5TPL->(FieldPos(cField))
						If nPos > 0
							uVar := LX5TPL->(FieldGet(nPos))
						EndIf
					EndIf
					If uVar <> NIL
						FieldPut(nj,uVar)
					EndIf
				Next nj
				MsUnlock()
				DbSelectArea("LX5TPL")
				DbSkip()
			EndDo
			
			ConOut(Str(nInc,6) + " Registros incluidos")
			ConOut(Str(nAlt,6) + " Registros alterados")
			nInc    := 0
			nAlt    := 0
		End If
			
		If lChkLXB
	
			ChkFile("LXB")
			ConOut("Inicio do tratamento do arquivo LXB")			
			
			DbSelectArea("LXB")
			DbSetOrder(1)
			DbGoTop()
		
			DbSelectArea("LXBTPL")
			DbGoTop()
			While !Eof()
				DbSelectArea("LXB")
				lNew := !DbSeek(xFilial("LXB")+LXBTPL->LXB_CAMPO+LXBTPL->LXB_TABELA)
				If lNew
					nInc++
				Else
					nAlt++
				EndIf
				RecLock("LXB",lNew)
				For nj := 1 To FCount()
					uVar := NIL
					cField := Field(nj)
					If cField == "LXB_FILIAL"
						uVar := xFilial("LXB")
					Else
						nPos := LXBTPL->(FieldPos(cField))
						If nPos > 0
							uVar := LXBTPL->(FieldGet(nPos))
						EndIf
					EndIf
					
					If uVar <> NIL
						FieldPut(nj,uVar)
					EndIf
				Next nj
				MsUnlock()
				DbSelectArea("LXBTPL")
				DbSkip()
			EndDo

			ConOut(Str(nInc,6) + " Registros incluidos")
			ConOut(Str(nAlt,6) + " Registros alterados")
			nInc    := 0
			nAlt    := 0
		EndIf
				
		If lChkLDY
		
			ChkFile("LDY")
			ConOut("Inicio do tratamento do arquivo LDY")

			DbSelectArea("LDYTPL")
			DbGoTop()
	
			While !Eof()
				DbSelectArea("LDY")
				lNew := !DbSeek(xFilial("LDY")+LDYTPL->LDY_CODIGO)

				If lNew
					nInc++
				Else
					nAlt++
				EndIf
	
				RecLock("LDY",lNew)
				For nj := 1 To FCount()
					uVar := NIL
					cField := Field(nj)
					If cField == "LDY_FILIAL"
						uVar := xFilial("LDY")
					Else
						nPos := LDYTPL->(FieldPos(cField))
						If nPos > 0
							uVar := LDYTPL->(FieldGet(nPos))
						EndIf
					EndIf
					If uVar <> NIL
						FieldPut(nj,uVar)
					EndIf
				Next nj
				MsUnlock()
				DbSelectArea("LDYTPL")
				DbSkip()
			EndDo
			
			ConOut(Str(nInc,6) + " Registros incluidos")
			ConOut(Str(nAlt,6) + " Registros alterados")
			nInc    := 0
			nAlt    := 0
		EndIf	
		
		If lChkLHY
		
			ChkFile("LHY")
			ConOut("Inicio do tratamento do arquivo LHY")

			DbSelectArea("LHYTPL")
			DbGoTop()
	
			While !Eof()
				DbSelectArea("LHY")
				lNew := !DbSeek(xFilial("LHY")+LHYTPL->LHY_CODIGO)

				If lNew
					nInc++
				Else
					nAlt++
				EndIf
	
				RecLock("LHY",lNew)
				For nj := 1 To FCount()
					uVar := NIL
					cField := Field(nj)
					If cField == "LHY_FILIAL"
						uVar := xFilial("LHY")
					Else
						nPos := LHYTPL->(FieldPos(cField))
						If nPos > 0
							uVar := LHYTPL->(FieldGet(nPos))
						EndIf
					EndIf
					If uVar <> NIL
						FieldPut(nj,uVar)
					EndIf
				Next nj
				MsUnlock()
				DbSelectArea("LHYTPL")
				DbSkip()
			EndDo
			
			ConOut(Str(nInc,6) + " Registros incluidos")
			ConOut(Str(nAlt,6) + " Registros alterados")
			nInc    := 0
			nAlt    := 0
		EndIf	

		If lChkLI1
		
			ChkFile("LI1")
			ConOut("Inicio do tratamento do arquivo LI1")

			DbSelectArea("LI1TPL")
			DbGoTop()
	
			While !Eof()
				DbSelectArea("LI1")
				lNew := !DbSeek(xFilial("LI1")+LI1TPL->LI1_CODIGO)

				If lNew
					nInc++
				Else
					nAlt++
				EndIf
	
				RecLock("LI1",lNew)
				For nj := 1 To FCount()
					uVar := NIL
					cField := Field(nj)
					If cField == "LI1_FILIAL"
						uVar := xFilial("LI1")
					Else
						nPos := LI1TPL->(FieldPos(cField))
						If nPos > 0
							uVar := LI1TPL->(FieldGet(nPos))
						EndIf
					EndIf
					If uVar <> NIL
						FieldPut(nj,uVar)
					EndIf
				Next nj
				MsUnlock()
				DbSelectArea("LI1TPL")
				DbSkip()
			EndDo
			
			ConOut(Str(nInc,6) + " Registros incluidos")
			ConOut(Str(nAlt,6) + " Registros alterados")
			nInc    := 0
			nAlt    := 0
		EndIf	

		If lChkLJ1
		
			ChkFile("LJ1")
			ConOut("Inicio do tratamento do arquivo LJ1")

			DbSelectArea("LJ1TPL")
			DbGoTop()
	
			While !Eof()
				DbSelectArea("LJ1")
				lNew := !DbSeek(xFilial("LJ1")+LJ1TPL->LJ1_COD_SI)

				If lNew
					nInc++
				Else
					nAlt++
				EndIf
	
				RecLock("LJ1",lNew)
				For nj := 1 To FCount()
					uVar := NIL
					cField := Field(nj)
					If cField == "LJ1_FILIAL"
						uVar := xFilial("LJ1")
					Else
						nPos := LJ1TPL->(FieldPos(cField))
						If nPos > 0
							uVar := LJ1TPL->(FieldGet(nPos))
						EndIf
					EndIf
					If uVar <> NIL
						FieldPut(nj,uVar)
					EndIf
				Next nj
				MsUnlock()
				DbSelectArea("LJ1TPL")
				DbSkip()
			EndDo
			
			ConOut(Str(nInc,6) + " Registros incluidos")
			ConOut(Str(nAlt,6) + " Registros alterados")
			nInc    := 0
			nAlt    := 0
		EndIf	
		
		If lChkLHZ
		
			ChkFile("LHZ")
			ConOut("Inicio do tratamento do arquivo LHZ")

			DbSelectArea("LHZTPL")
			DbGoTop()
	
			While !Eof()
				DbSelectArea("LHZ")
				lNew := !DbSeek(xFilial("LHZ")+LHZTPL->LHZ_CODIGO)

				If lNew
					nInc++
				Else
					nAlt++
				EndIf
	
				RecLock("LHZ",lNew)
				For nj := 1 To FCount()
					uVar := NIL
					cField := Field(nj)
					If cField == "LHZ_FILIAL"
						uVar := xFilial("LHZ")
					Else
						nPos := LHZTPL->(FieldPos(cField))
						If nPos > 0
							uVar := LHZTPL->(FieldGet(nPos))
						EndIf
					EndIf
					If uVar <> NIL
						FieldPut(nj,uVar)
					EndIf
				Next nj
				MsUnlock()
				DbSelectArea("LHZTPL")
				DbSkip()
			EndDo
			
			ConOut(Str(nInc,6) + " Registros incluidos")
			ConOut(Str(nAlt,6) + " Registros alterados")
		EndIf	
		
		If lChkLJ7
		
			ChkFile("LJ7")
			ConOut("Inicio do tratamento do arquivo LJ7")

			DbSelectArea("LJ7TPL")
			DbGoTop()
	
			While !Eof()
				DbSelectArea("LJ7")
				lNew := !DbSeek(xFilial("LJ7")+LJ7TPL->LJ7_CDSIMP)

				If lNew
					nInc++
				Else
					nAlt++
				EndIf
	
				RecLock("LJ7",lNew)
				For nj := 1 To FCount()
					uVar := NIL
					cField := Field(nj)
					If cField == "LJ7_FILIAL"
						uVar := xFilial("LJ7")
					Else
						nPos := LJ7TPL->(FieldPos(cField))
						If nPos > 0
							uVar := LJ7TPL->(FieldGet(nPos))
						EndIf
					EndIf
					If uVar <> NIL
						FieldPut(nj,uVar)
					EndIf
				Next nj
				MsUnlock()
				DbSelectArea("LJ7TPL")
				DbSkip()
			EndDo
			
			ConOut(Str(nInc,6) + " Registros incluidos")
			ConOut(Str(nAlt,6) + " Registros alterados")
			nInc    := 0
			nAlt    := 0
		EndIf	
		
		If lChkLJN
		
			ChkFile("LJN")
			ConOut("Inicio do tratamento do arquivo LJN")

			DbSelectArea("LJNTPL")
			DbGoTop()
	
			While !Eof()
				DbSelectArea("LJN")
				lNew := !DbSeek(xFilial("LJN")+LJNTPL->LJN_CDINST)

				If lNew
					nInc++
				Else
					nAlt++
				EndIf
	
				RecLock("LJN",lNew)
				For nj := 1 To FCount()
					uVar := NIL
					cField := Field(nj)
					If cField == "LJN_FILIAL"
						uVar := xFilial("LJN")
					Else
						nPos := LJNTPL->(FieldPos(cField))
						If nPos > 0
							uVar := LJNTPL->(FieldGet(nPos))
						EndIf
					EndIf
					If uVar <> NIL
						FieldPut(nj,uVar)
					EndIf
				Next nj
				MsUnlock()
				DbSelectArea("LJNTPL")
				DbSkip()
			EndDo
			
			ConOut(Str(nInc,6) + " Registros incluidos")
			ConOut(Str(nAlt,6) + " Registros alterados")
			nInc    := 0
			nAlt    := 0
		EndIf	

	EndIf
 	
 	ConOut("Finalizando Ambiente!")
	//fecha TODOS os alias abertos
	RpcClearEnv()
	ConOut("Ambiente Finalizado!")
Next nI
RESET ENVIRONMENT 
Conout("Operacao Finalizada com sucesso!!!!")
Return .T.
