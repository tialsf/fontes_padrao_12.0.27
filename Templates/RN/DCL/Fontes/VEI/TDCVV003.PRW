/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TDCVV003  � Autor �ANTONIO CORDEIRO       � Data �JULHO/2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Valida digitacao do codigo do Motorista                    ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Template Combustiveis Veiculos                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

#include "rwmake.ch"

Template Function TDCVV003()
Local _aArea  := GetArea()
Local cMens   := ""
Local lRet    := .T.
CHKTEMPLATE("DCLVEI")
IF GETMV("MV_T_MOPE")=="S" .AND. LBY->LBY_VENCTO < DDATABASE 
   cMens:=" Curso Mope do Motorista Vencido"+CHR(13)
   lRet:=.F.
ENDIF
IF GETMV("MV_T_EXMED")=="S" .AND. LBY->LBY_DTVALE < DDATABASE 
   cMens:=" Exame medico Motorista vencido"+CHR(13)
   lRet:=.F.
ENDIF
IF GETMV("MV_T_VISTO")=="S"
   IF LBY->LBY_DTVIST < DDATABASE
      cMens:=" Motorista com Data de Vistoria Vencida"+CHR(13)
      lRet:=.F.
   ENDIF
   IF ! EMPTY(LBY->LBY_ITEMDE)
      cMens:=" Motorista com Itens em Desacordo"+CHR(13)
      lRet:=.F.
   ENDIF
ENDIF
IF ! lRet
	MsgBox(cMens,"informacao","INFO")
ENDIF
RestArea(_aArea)
Return(lRet)


