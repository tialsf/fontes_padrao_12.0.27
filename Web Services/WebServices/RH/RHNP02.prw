#INCLUDE "TOTVS.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "RHNP02.CH"

Function RHNP02()
Return .T.

Private cMRrhKeyTree := ""


//*********************************************************
// Servi�o de dados referentes ao colaborador
//*********************************************************
WSRESTFUL Data	DESCRIPTION STR0001 FORMAT APPLICATION_JSON 
WSDATA employeeId   AS String
WSDATA Token  		As String 

	WSMETHOD GET getImage ;
 	DESCRIPTION STR0008 ; //"Retorna a foto do colaborador" 
 	PATH		"/profile/image/{employeeId}"    ;
 	WSSYNTAX	"data/profile/image/{employeeId}";
 	PRODUCES	"application/json;charset=utf-8" 

	WSMETHOD GET getIsCoord ;
 	DESCRIPTION STR0010 ; //"Retorna se o usu�rio � coordenador"
 	PATH		"/profile/isCoordinator/{employeeId}"    ;
 	WSSYNTAX	"data/profile/isCoordinator/{employeeId}";
 	PRODUCES	"application/json;charset=utf-8" 

	WSMETHOD GET getProfile;
 	DESCRIPTION STR0009 ; //"Retorna os dados cadastrais do colaborador"
 	WSSYNTAX	"/data/profile/{employeeId} || data/profile/summary/{employeeId}";

END WSRESTFUL


// -------------------------------------------------------------------
// - GET RESPONS�VEL EM RETORNAR A FOTO DO COLABORADOR.
// -------------------------------------------------------------------
WSMETHOD GET getImage	 	 ; 
		HEADERPARAM Token 	 ;
		PATHPARAM employeeId ;
		WSREST Data

Local cJsonObj		:= "JsonObject():New()"
Local oItem			:= &cJsonObj
Local oMessages		:= &cJsonObj
Local nLenParms		:= Len(::aURLParms)
Local aMessages		:= {}
Local aDataLogin  	:= {}
Local cJson			:= ""
Local cToken		:= ""
Local cRD0Login		:= ""
Local cBranchVld	:= ""
Local cRD0Cod		:= ""
Local cMatSRA		:= ""
	
cToken  := Self:GetHeader('Authorization')
::SetHeader('Access-Control-Allow-Credentials' , "true")

aDataLogin := GetDataLogin(cToken)
If Len( aDataLogin ) > 0
	cMatSRA 	:= aDataLogin[1]
	cRD0Login 	:= aDataLogin[2]
	cRD0Cod 	:= aDataLogin[3]
	cBranchVld 	:= aDataLogin[5]
EndIf

If Empty(cRD0Login) .Or. Empty(cBranchVld)
	oMessages["type"]   := "error"
	oMessages["code"]   := "401"
	oMessages["detail"] := EncodeUTF8(STR0002) //"Dados inv�lidos."
	Aadd(aMessages,oMessages)
ElseIf nLenParms == 3 .And. !Empty(::aURLParms[3]) .And. ::aURLParms[2] == "image"
	DataImage(oItem, aMessages, cRD0Login, cBranchVld, STRTOKARR( ::aURLParms[3], "|" ))
Else
	Aadd(aMessages,EncodeUTF8(STR0003)) //"O par�metro employeeId � necess�rio para o servi�o de dados do colaborador."
EndIf

If len(aMessages) > 0
	oItem["data"] 		:= {}
	oItem["length"] 	:= ( Len(oItem["data"]) )
	oItem["messages"] 	:= aMessages
EndIf

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cjson)

Return(.T.)


// -------------------------------------------------------------------
// - GET RESPONS�VEL EM RETORNAR SE O USU�RIO � COORDENADOR.
// -------------------------------------------------------------------
WSMETHOD GET getIsCoord 	 ; 
		HEADERPARAM Token 	 ;
		PATHPARAM employeeId ;
		WSREST Data

Local cJsonObj		:= "JsonObject():New()"
Local oItem			:= &cJsonObj
Local oMessages		:= &cJsonObj
Local nLenParms		:= Len(::aURLParms)
Local aMessages		:= {}
Local aDataLogin 	:= {}
Local cJson			:= ""
Local cToken		:= ""
Local cRD0Login		:= ""
Local cBranchVld	:= ""
Local cRD0Cod		:= ""
Local cMatSRA		:= ""
	
cToken  := Self:GetHeader('Authorization')
::SetHeader('Access-Control-Allow-Credentials' , "true")

aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
	cMatSRA    := aDataLogin[1]
	cRD0Login  := aDataLogin[2]
	cRD0Cod    := aDataLogin[3]
	cBranchVld := aDataLogin[5]
EndIf

If Empty(cRD0Login) .Or. Empty(cBranchVld)
	oMessages["type"]   := "error"
	oMessages["code"]   := "401"
	oMessages["detail"] := EncodeUTF8(STR0002) //"Dados inv�lidos."
	Aadd(aMessages,oMessages)
ElseIf nLenParms == 3 .And. !Empty(::aURLParms[3]) .And. ::aURLParms[2] == "isCoordinator"
	oItem["isCoordinator"] := fGetTeamManager(cBranchVld, cMatSRA) 	//Verifica se a matricula logada e de um gestor
Else
	Aadd(aMessages,EncodeUTF8(STR0003)) //"O par�metro employeeId � necess�rio para o servi�o de dados do colaborador."
EndIf

If len(aMessages) > 0
	oItem["data"] 		:= {}
	oItem["length"] 	:= ( Len(oItem["data"]) )
	oItem["messages"] 	:= aMessages
EndIf

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cjson)

Return(.T.)


// -------------------------------------------------------------------
// - GET RESPONS�VEL EM RETORNAR OS DADOS CADASTRAIS DO COLABORADOR.
// -------------------------------------------------------------------
WSMETHOD GET getProfile    	 ; 
		HEADERPARAM Token 	 ;
		PATHPARAM employeeId ;
		WSREST Data

Local cJsonObj		:= "JsonObject():New()"
Local oItem			:= &cJsonObj
Local oMessages		:= &cJsonObj
Local nLenParms		:= Len(::aURLParms)
Local aMessages		:= {}
Local aIdFunc       := {}
Local aDataLogin	:= {}
Local cJson			:= ""
Local cToken		:= ""
Local cRD0Login		:= ""
Local cBranchVld	:= ""
Local cRD0Cod		:= ""
Local cMatSRA		:= ""
Local lRet			:= .T.
	
cToken  := Self:GetHeader('Authorization')
::SetHeader('Access-Control-Allow-Credentials' , "true")

aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
	cMatSRA    := aDataLogin[1]
	cRD0Login  := aDataLogin[2]
	cRD0Cod    := aDataLogin[3]
	cBranchVld := aDataLogin[5]
EndIf

If nLenParms > 0
	If  nLenParms == 3 .And. ::aURLParms[2] == "summary" .And. !Empty(::aURLParms[3]) .And. (::aUrlParms[3] != "%7Bcurrent%7D") .and. (::aUrlParms[3] != "{current}")
		aIdFunc := STRTOKARR( ::aUrlParms[3], "|" )
	ElseIf nLenParms == 2 .And. ::aURLParms[1] == "profile" .And. !Empty(::aURLParms[2]) .And. (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
		aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
	EndIf 

	If Len(aIdFunc) > 1 
		//valida se o solicitante da requisi��o pode ter acesso as informa��es
		If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
			cBranchVld	:= aIdFunc[1]
			cMatSRA		:= aIdFunc[2]
		Else
			cBranchVld	:= ""
			cMatSRA		:= ""
		EndIf	
	EndIf
EndIF

If Empty(cRD0Login) .Or. Empty(cBranchVld)
	oMessages["type"]   := "error"
	oMessages["code"]   := "401"
	oMessages["detail"] := EncodeUTF8(STR0002) //"Dados inv�lidos."
	Aadd(aMessages,oMessages)
ElseIf !Empty(cBranchVld) .And. !Empty(cMatSRA)
	DataProfile(oItem, aMessages, cRD0Login, IF((::aURLParms[2] == "summary"), .T., .F.), cBranchVld, cRD0Cod, cMatSRA)
Else
	Aadd(aMessages,EncodeUTF8(STR0003)) //"O par�metro employeeId � necess�rio para o servi�o de dados do colaborador."
EndIf

If len(aMessages) > 0
	oItem["data"] 		:= {}
	oItem["length"] 	:= ( Len(oItem["data"]) )
	oItem["messages"] 	:= aMessages
	lRet := .F.
EndIf

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cjson)

Return(lRet)


// -------------------------------------------------------------------
// -------------------------------------------------------------------
// - FUN��ES GENERICAS DO SERVI�O DATA/PROFILE.
// -------------------------------------------------------------------
Function DataProfile(oItem,aMessages,cRD0Login,lSummary,cBranchVld,cRD0Cod,cMatSRA)
Local cJsonObj   	:= "JsonObject():New()"
Local oSummary 		:= &cJsonObj
Local oCoordinator	:= &cJsonObj
Local oItemData	 	:= &cJsonObj
Local oTeams		:= &cJsonObj
Local oAddresses	:= &cJsonObj
Local oAddreType	:= &cJsonObj
Local oNumberPhone	:= &cJsonObj
Local oNumberCel	:= &cJsonObj
Local oEmailWork	:= &cJsonObj
Local oEmailPersonal:= &cJsonObj
Local oState		:= &cJsonObj
Local oCountry		:= &cJsonObj
Local oPersonalData	:= &cJsonObj
Local oBornCity		:= &cJsonObj
Local oContacts		:= &cJsonObj
Local aDateGMT		:= {}
Local aDocs			:= {}
Local aEmails		:= {}
Local aPhones		:= {}
Local aAddressBody	:= {}
Local aTeams		:= {}
Local aData			:= {}
Local aAreaSRA		:= {}
Local aCoordinator  := {}
Local aStructREST	:= {}
Local cRoutine		:= "W_PWSA260.APW" // Dados Cadastrais - Utilizada para buscar a VIS�O a partir da rotina; (AI8_VISAPV) na fun��o GetVisionAI8().
Local aVision		:= {}
Local cVision		:= ""
Local oFlProperties := &cJsonObj
Local aFlProperties	:= {}

Default oItem		:= &cJsonObj
Default aMessages	:= {}
Default cRD0Login	:= ''
Default lSummary    := .F.
Default cBranchVld  := ""
Default cRD0Cod		:= ""
Default cMatSRA		:= ""

// - Efetua o posicionamento no funcion�rio (SRA)
dbSelectArea("SRA")
SRA->( dbSetOrder(1) )
If SRA->( dbSeek( cBranchVld + cMatSRA ))

	If !lSummary
		// ----------------------------------------------
		// - A Fun��o GetVisionAI8() devolve por padr�o
		// - Um Array com a seguinte estrutura:
		// - aVision[1][1] := "" - AI8_VISAPV
		// - aVision[1][2] := 0  - AI8_INIAPV
		// - aVision[1][3] := 0  - AI8_APRVLV
		// - Por isso as posi��es podem ser acessadas 
		// - Sem problemas, ex: cVision := aVision[1][1] 
		// ----------------------------------------------
		aVision := GetVisionAI8(cRoutine, cBranchVld)
		cVision := aVision[1][1] 
		
		// --------------------------------------
		// - aStructREST - Estrutura Hierarquica
		// - Do Funcion�rio Logado.
		// --------------------------------------
		aAreaSRA    := SRA->( GetArea() )

		cMRrhKeyTree := fMHRKeyTree(cBranchVld, SRA->RA_MAT)
		aStructREST  := APIGetStructure(cRD0Cod, "", cVision, cBranchVld, SRA->RA_MAT, , , , , cBranchVld, SRA->RA_MAT, , , , , .T., {cEmpAnt})

		RestArea(aAreaSRA)
		aCoordinator := CoordInfos(aClone(aStructREST),SRA->RA_MAT,cBranchVld)

		// - Summary
		oSummary["id"] 						:= cBranchVld +"|" +cMatSRA 
		oSummary["name"]					:= Alltrim( EncodeUTF8(SRA->RA_NOME) ) 
		oSummary["roleDescription"] 		:= Alltrim( EncodeUTF8( FDesc("SRJ", SRA->RA_CODFUNC, "RJ_DESC",,cBranchVld ) ) ) 
		
		oItemData["summary"] 				:= oSummary
		oItemData["positionLevel"]			:= Alltrim( EncodeUTF8(fDesc("SQ3",SRA->RA_CARGO,"SQ3->Q3_DESCSUM",,cBranchVld )) )
		oItemData["registry"]				:= SRA->RA_MAT
		
		aDateGMT							:= Iif(!Empty(SRA->RA_ADMISSA),LocalToUTC( DTOS(SRA->RA_ADMISSA), "12:00:00" ),{})
		oItemData["admissionDate"]			:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")		
		
       If !empty(SRA->RA_DEPTO) 
          oItemData["department"]           := Alltrim( EncodeUTF8( fDesc("SQB",SRA->RA_DEPTO,"QB_DESCRIC",, xFilial("SQB" , cBranchVld) ) ) )
   		   oTeams["name"]					:= Alltrim( EncodeUTF8( fDesc("SQB", GetDepSup(SRA->RA_DEPTO,cBranchVld),"QB_DESCRIC") ) )//Alltrim( EncodeUTF8( fDesc("SQB", SRA->RA_DEPTO,"QB_DESCRIC") ) ) // - Protheus n�o possui o conceito de times, portanto segue o valor do department, para adequar a API.
		   oTeams["default"]   				:= .T.
       Else
          oItemData["department"]           := ""
          oTeams["name"]                    := ""
          oTeams["default"]                 := .F.
       EndIf
		
		Aadd(aTeams, oTeams)
		
		oItemData["teams"] 					:= aTeams
		
		// - Coordinator
		oCoordinator["id"] 					:= aCoordinator[3] +"|" +aCoordinator[4]
		oCoordinator["name"]				:= EncodeUTF8(aCoordinator[1])
		oCoordinator["roleDescription"]		:= Iif(Len(aCoordinator) >= 2, EncodeUTF8(aCoordinator[2]), "")
		
		oItemData["coordinator"] 			:= oCoordinator
		oItemData["gender"]					:= Iif( SRA->RA_SEXO == "M", "Masculino", "Feminino") 
		oItemData["nickname"]				:= Alltrim( EncodeUTF8(SRA->RA_APELIDO) )
		
		// - Data de Nascimento no format UTC
		aDateGMT							:= {}
		aDateGMT 							:= Iif(!Empty(SRA->RA_NASC),LocalToUTC( DTOS(SRA->RA_NASC), "12:00:00" ),{})
		oItemData["bornDate"]				:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")		
		IF cPaisLoc == "BRA"
			oItemData["bornCity"]				:= Alltrim( EncodeUTF8(SRA->RA_MUNNASC) )
		ENDIF
		oItemData["nacionality"]			:= Alltrim( EncodeUTF8( Posicione("SX5",1,XFILIAL("SX5") + '34' + PADR( SRA->RA_NACIONA,TamSx3("X5_CHAVE")[1]," " ), "X5_DESCRI" ) ) )
		
		oPersonalData["nickname"]			:= Alltrim( EncodeUTF8(SRA->RA_APELIDO) )
		oPersonalData["gender"]				:= Iif( SRA->RA_SEXO == "M", "Masculino", "Feminino")
		
		// - Data de Nascimento no format UTC
		aDateGMT							:= {}
		aDateGMT 							:= Iif(!Empty(SRA->RA_NASC),LocalToUTC( DTOS(SRA->RA_NASC), "12:00:00" ),{}) 
		oPersonalData["bornDate"]			:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")		
				
		oBornCity["id"]						:= "0"
		IF cPaisLoc == "BRA"
			oBornCity["name"]					:= Alltrim( EncodeUTF8(SRA->RA_MUNNASC) )
		ENDIF
		oBornCity["abbr"]					:= Nil
		oBornCity["country"]				:= Nil
		
		oCountry["id"]						:= ""
		IF cPaisLoc == "BRA"
			oCountry["name"]					:= Alltrim( EncodeUTF8( fDesc("CCH",SRA->RA_CPAISOR,"CCH_PAIS",,SRA->RA_FILIAL) ) )
		ENDIF
		
		oState["id"]						:= ""
		oState["abbr"]						:= Alltrim( EncodeUTF8(SRA->RA_ESTADO) )
		oState["name"]						:= Alltrim( EncodeUTF8(SRA->RA_ESTADO) )
		oState["country"]					:= oCountry
		oBornCity["state"]					:= oState
		
		oPersonalData["bornCity"]			:= oBornCity	
		
		oItemData["personalData"]			:= oPersonalData 
		
		// - Adresses
		IF cPaisLoc == "BRA"
			oAddresses["id"]					:= "pessoal"
			oAddresses["abbr"]					:= Alltrim( EncodeUTF8( SRA->RA_LOGRTP ) ) 
			oAddresses["name"]					:= Alltrim( EncodeUTF8( fDescRCC("S054", SRA->RA_LOGRTP, 1, 4, 5, 20) ) )
		ENDIF

		oAddreType["addressType"]			:= oAddresses
		oAddreType["name"]					:= Alltrim( EncodeUTF8(SRA->RA_ENDEREC) )
		oAddreType["default"]				:= .T.
		oAddreType["type"]					:= "home"
		oAddreType["id"]					:= "pessoal"
		oAddreType["zipcode"]				:= Alltrim( EncodeUTF8(SRA->RA_CEP) )
		IF cPaisLoc == "BRA" 
			oAddreType["number"]				:= Alltrim( EncodeUTF8(SRA->RA_NUMENDE) )
		ENDIF

		oAddreType["complement"]			:= Alltrim( EncodeUTF8(SRA->RA_COMPLEM) )
		oAddreType["neighborhood"]			:= Alltrim( EncodeUTF8(SRA->RA_BAIRRO) )
		
		oAddresses							:= Nil
		oAddresses							:= &cJsonObj
		
		oAddresses["id"]					:= ""
		oAddresses["name"]					:= Alltrim( EncodeUTF8(SRA->RA_MUNICIP) )
		oAddresses["abbr"]					:= Alltrim( EncodeUTF8(SRA->RA_MUNICIP) )
		oAddresses["country"]				:= Nil

		oCountry 							:= Nil
		oCountry							:= &cJsonObj
		
		oCountry["id"]						:= ""
		oCountry["abbr"]					:= ""
		IF cPaisLoc == "BRA"
			oCountry["name"]					:= Alltrim( EncodeUTF8( fDesc("CCH",SRA->RA_CPAISOR,"CCH_PAIS",,SRA->RA_FILIAL) ) )
		ENDIF
		
		oState 								:= Nil
		oState								:= &cJsonObj
		
		oState["id"]						:= ""
		oState["abbr"]						:= Alltrim( EncodeUTF8(SRA->RA_ESTADO) )
		oState["name"]						:= Alltrim( EncodeUTF8(SRA->RA_ESTADO) )
		oState["country"]					:= oCountry
		oAddresses["state"]					:= oState
		
		oAddreType["city"]					:= oAddresses
				
		Aadd(aAddressBody, oAddreType)
		
		oItemData["addresses"] 				:= aAddressBody
		
		// - Phone
		IF cPaisLoc == "BRA"
			oNumberPhone["id"]					:= Iif( !Empty(SRA->RA_DDDFONE) .And. !Empty(SRA->RA_TELEFON), "casa", "" )
			oNumberPhone["ddd"]					:= Val(Alltrim( SRA->RA_DDDFONE ))
		ENDIF
		oNumberPhone["region"]				:= Nil
		oNumberPhone["number"]				:= Alltrim( SRA->RA_TELEFON )
		oNumberPhone["default"]				:= .T.
		oNumberPhone["type"]				:= "home"
				
		Aadd(aPhones, oNumberPhone )
		
		// - Phone
		IF cPaisLoc == "BRA"
			oNumberCel["id"]					:= Iif( !Empty(SRA->RA_DDDCELU) .And. !Empty(SRA->RA_NUMCELU), "celular", "" )
			oNumberCel["region"]				:= Nil
			oNumberCel["ddd"]					:= Val(Alltrim( SRA->RA_DDDCELU ))
			oNumberCel["number"]				:= Alltrim( SRA->RA_NUMCELU )		
			oNumberCel["default"]				:= .F.
			oNumberCel["type"]					:= "mobile"
		
			Aadd(aPhones, oNumberCel )
		ENDIF

				
		// - E-mails

		oFlProperties := &cJsonObj
		oFlProperties["field"] 			:= "emails.profissional"
		oFlProperties["visible"]		:= .T.
		oFlProperties["editable"]		:= .F.
		oFlProperties["required"]		:= .F.

		oEmailWork["id"] 				:= Iif( !Empty(SRA->RA_EMAIL), "profissional", "" )
		oEmailWork["email"]				:= Alltrim( EncodeUTF8( SRA->RA_EMAIL ) ) 
		oEmailWork["default"]			:= .T.
		oEmailWork["type"]				:= EncodeUTF8("work")
		oEmailWork["props"]				:= oFlProperties
	
		Aadd(aEmails, oEmailWork)
				
		// - E-mails
		IF cPaisLoc == "BRA"
			oFlProperties := &cJsonObj
			oFlProperties["field"] 			:= "emails.pessoal"
			oFlProperties["visible"]		:= .T.
			oFlProperties["editable"]		:= .F.
			oFlProperties["required"]		:= .F.

			oEmailPersonal["id"]			:= Iif( !Empty(SRA->RA_EMAIL2), "pessoal", "" )
			oEmailPersonal["email"]			:= Alltrim( EncodeUTF8( SRA->RA_EMAIL2 ) ) 
			oEmailPersonal["default"]		:= .F.
			oEmailPersonal["type"]			:= EncodeUTF8("home")
			oEmailPersonal["props"]			:= oFlProperties

			Aadd(aEmails, oEmailPersonal)
		ENDIF
		
		
			
		oContacts["phones"]					:= aPhones
		oContacts["emails"]					:= aEmails
		oItemData["contacts"]				:= oContacts
		
		/*
		 * - No protheus n�o h� webReferences
		 * - Portanto ser� enviado para a API 
		 * - Uma lista vazia.
		oWebReferences["url"] 				:= EncodeUTF8( "http://fluig.totvs.com/andre.pontes" ) 
		oWebReferences["default"]			:= .T.
		oWebReferences["type"]				:= "fluig" 
		Aadd(aWebReferences, oWebReferences)
		*/
		
		oItemData["webReferences"]			:= {}
		
		// - Obt�m os Documentos necess�rios		
		GetDocuments(@aDocs)
		oItemData["documents"]				:= aDocs
		
		//-----------------------------------		
		//Adiciona tratamento para visualizacao e edicao de campos - FieldProperties
		//-----------------------------------
		
		//Nao exibir o campo Equipe porque nao e usado no Protheus
		oFlProperties := &cJsonObj
		oFlProperties["field"] 				:= "teams.name"
		oFlProperties["visible"]			:= .F.
		oFlProperties["editable"]			:= .F.
		oFlProperties["required"]			:= .F.
		
		Aadd(aFlProperties, oFlProperties)
		
		//Exibe o Apelido apenas de tiver sido informado no cadastro
		If Empty(SRA->RA_APELIDO)
			oFlProperties := &cJsonObj
			oFlProperties["field"] 			:= "personalData.nickname"
			oFlProperties["visible"]		:= .F.
			oFlProperties["editable"]		:= .F.
			oFlProperties["required"]		:= .F.	
		
			Aadd(aFlProperties, oFlProperties)
		EndIf
		
		oItemData["props"]		:= aFlProperties
					
	Else
		// - Summary
		oItemData["id"] 				:= SRA->RA_FILIAL +"|" +SRA->RA_MAT
		oItemData["name"]				:= Alltrim( EncodeUTF8(SRA->RA_NOME) ) 
		oItemData["roleDescription"] 	:= Alltrim( EncodeUTF8( FDesc("SRJ", SRA->RA_CODFUNC, "RJ_DESC",,cBranchVld ) ) )

		aDateGMT						:= Iif(!Empty(SRA->RA_ADMISSA),LocalToUTC( DTOS(SRA->RA_ADMISSA), "12:00:00" ),{})
		oItemData["admissionDate"]		:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")		

		// - Data de Nascimento no format UTC
		aDateGMT						:= {}
		aDateGMT						:= Iif(!Empty(SRA->RA_NASC),LocalToUTC( DTOS(SRA->RA_NASC), "12:00:00" ),{}) 
		oItemData["bornDate"]		    := Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")		
	EndIf
		
	Aadd(aData, oItemData)	
	oItem["data"] 		:= oItemData
	oItem["length"] 		:= Len(aData)
	oItem["messages"] 	:= Iif( Empty(aData), EncodeUTF8(STR0004) + cRD0Login , aMessages ) //"N�o foi poss�vel carregar os dados para colaborador: "
Else
	aMessages := { EncodeUTF8(STR0004) + cMatSRA } //"N�o foi poss�vel carregar os dados para colaborador: "
EndIf

Return( oItem )


Function DataImage(oItem, aMessages, cRD0Login, cBranchVld, aFunc)
Local cQryRD0	 	:= GetNextAlias()
Local cJsonObj		:= "JsonObject():New()"
Local oItemRet		:= &cJsonObj
Local cRD0Branch	:= "" 
Local cBkpMod		:= cModulo //Vari�vel publica com o modulo que est� startado. Por padr�o em ambientes REST vem como FAT.
Local aRet			:= {}
Local nPos		   	:= 0
Local cFuncFoto		:= ""
Local cMatRD0		:= ""
Local cBranchRD0	:= ""
Local cQrySRA		:= ""
Local __cDelete 	:= "% SRA.D_E_L_E_T_ = ' ' %"
Local cEmpSRA		:= cEmpAnt

Default oItem    	:= &cJsonObj
Default aMessages	:= {}
Default cRd0Login	:= {}
Default cBranchVld	:= FwCodFil()

oItemRet["content"]	:= ""
oItemRet["type"]	:= "" 

cModulo := "GPE" // Atribui o m�dulo GPE para consultar corretamente as fotos no banco de dados.
If !empty(aFunc) .and. (Alltrim(aFunc[1]) == "{current}" .Or. Alltrim(aFunc[1]) == "%7Bcurrent%7D")
	//Busca foto do colaborador logado
  	cRD0Branch := xFilial("RD0", cBranchVld)
  	BEGINSQL ALIAS cQryRD0
    	SELECT RD0.RD0_NOME, RD0.RD0_CODIGO, RD0.RD0_CIC, RD0.RD0_LOGIN, RD0.RD0_BITMAP
    	FROM %table:RD0% RD0
    	WHERE RD0_FILIAL =  %Exp:cRD0Branch% AND
        	RD0.RD0_LOGIN = %exp:Upper(cRD0Login)%  OR
        	RD0.RD0_CIC   = %exp:cRD0Login%  AND
        	RD0.%notDel%
	ENDSQL

  	If !(cQryRD0)->(Eof())
    	cFuncFoto := (cQryRD0)->RD0_BITMAP

    	//busca matriculas associada a pessoa, para capturar a foto do SRA
    	If GetAccessEmployee(cRD0Login, @aRet, .T., .T.)
        	If Len(aRet) >= 1
            	nPos := Ascan(aRet,{|x| !(x[10] $ "30/31")}) 
    	        If nPos > 0
        	        cMatRD0     := aRet[nPos][1]
            	    cBranchRD0  := aRet[nPos][3]
	            Else
    	            cMatRD0     := aRet[1][1]
        	        cBranchRD0  := aRet[1][3]
            	EndIf 
	        EndIf

    	    dbSelectArea("SRA")
        	SRA->( dbSetOrder(1) )
	        If (SRA->( dbSeek( xFilial("SRA" , cBranchRD0) + cMatRD0) ))
    	       If !empty(SRA->RA_BITMAP)
        	       cFuncFoto := Alltrim(SRA->RA_BITMAP)
            	 EndIf    
	        EndIf

    	    oItemRet["content"] := RetFoto_( SRA->RA_FILIAL, SRA->RA_MAT, cFuncFoto )
        	oItemRet["type"]    := "jpg" 
	    EndIf   
	EndIf
  	(cQryRD0)->( DbCloseArea() )

Else
	If Len(aFunc) > 2
		cEmpSRA := aFunc[3]
	EndIf

	cQrySRA 	:= GetNextAlias()
	__cSRAtab 	:= "%" + RetFullName("SRA", cEmpSRA) + "%"

	BeginSql ALIAS cQrySRA
		SELECT RA_FILIAL, RA_MAT, RA_BITMAP 
		FROM %exp:__cSRAtab% SRA
		WHERE 
			SRA.RA_FILIAL = %Exp:aFunc[1]% AND
			SRA.RA_MAT = %Exp:aFunc[2]% AND
			%exp:__cDelete%
	EndSql

	If (cQrySRA)->(!Eof())
		cFuncFoto := Alltrim( (cQrySRA)->RA_BITMAP )
	EndIf

	If !empty(cFuncFoto)
		oItemRet["content"] := RetFoto_( (cQrySRA)->RA_FILIAL, (cQrySRA)->RA_MAT, cFuncFoto )
		oItemRet["type"]    := "jpg" 
	EndIf

	(cQrySRA)->( DBCloseArea() )  

EndIf

cModulo := cBkpMod //Volta ao modulo original
oItem["data"] 		:= oItemRet
oItem["length"]		:= 1

oItemRet			:= Nil
oItemRet			:= &cJsonObj
oItem["messages"] 	:= {}
	
Return( oItem )


Function RetFoto_(Filial,Matricula,cFoto)
Local cBmpPict
Local nHandle
Local nBytesRead
Local cErro			:= ""
Local cLine			:= ""
Local cRet			:= ""
Local cPathPict		:= GetSrvProfString("Startpath","")
Local cExtensao		:= ".JPG"
Local lProcImg		:= .T.
Local lMRHLoadImg	:= ExistBlock("MRHLoadImg")


If !Empty( cBmpPict := Upper( AllTrim( cFoto ) ) )
	If RepExtract(cBmpPict,cPathPict+cBmpPict)
		If !File(cPathPict+cBmpPict+cExtensao)
			cExtensao := ".BMP"
		EndIf
		
		If File(cPathPict+cBmpPict+cExtensao)
			nHandle := fOpen(cPathPict+cBmpPict+cExtensao,0)
			// Se houver erro de abertura abandona processamento
			If nHandle = -1
				cErro := STR0005 //"problemas na abertura da foto"                                                                                                                                                                                                                                                                                                                                                                                                                                                           
				Return ""
			EndIf
			
			nBytesRead := fSeek(nHandle,0,2)

			If ExistBlock("MRHLoadImg")
		    	If ( ValType( nTamMax := ExecBlock("MRHLoadImg", .F. , .F. ,{Filial,Matricula,cFoto}) ) == "N" )
					If nBytesRead > nTamMax
						lProcImg := .F.
					EndIf
				EndIf
         	EndIf         

			If lProcImg
				fSeek(nHandle,0,0)	//Posiciona no inicio do arquivo
				cLine := Space(nBytesRead)
				fRead(nHandle,@cLine,nBytesRead) // Leitura da primeira linha do arquivo texto
			EndIf

			// Fecha o Arquivo
			fClose(nHandle)
			fErase(cPathPict+cBmpPict+cExtensao)
		EndIf
		
	Else
		cErro := STR0006 //"Falha ao extrair foto"
	EndIf	
	
EndIf

If !Empty(cLine)
	cRet := Encode64(cLine)
EndIf
Return cRet


Function GetDocuments(aDocs)
Local cJsonObj		:= "JsonObject():New()"
Local oFieldsDoc	:= &cJsonObj
Local oDocuments	:= &cJsonObj
Local aFieldsDoc	:= {}
Local aDateGMT		:= {}
Local aBoxCNH       := Iif(cPaisLoc == "BRA", RetSx3Box( Posicione("SX3", 2, "RA_CATCNH", "X3CBox()" ),,, 1 ), {})
    
Default aDocs		:= {}

	// -------------------------------------------------------
	// - Documents
	oDocuments["type"] 					:= "brid"
	oDocuments["label"]					:= Nil

	// - BRID - RG
	oFieldsDoc["type"] 					:= "number"
	oFieldsDoc["value"]					:= Alltrim( SRA->RA_RG )
	oFieldsDoc["label"]					:= Nil
	oFieldsDoc["mask"]					:= "99999999-99"
	Aadd(aFieldsDoc, oFieldsDoc)
	oDocuments["fields"]				:= aFieldsDoc

	// - ESTADO EMISSOR 
	oFieldsDoc 							:= &cJsonObj
	oFieldsDoc["type"] 					:= "senderState"
	oFieldsDoc["value"]					:= SRA->RA_RGUF
	oFieldsDoc["label"]					:= Nil
	oFieldsDoc["mask"]					:= Nil
	Aadd(aFieldsDoc, oFieldsDoc)
	oDocuments["fields"]				:= aFieldsDoc

	// - ESTADO EMISSOR
	oFieldsDoc 							:= &cJsonObj
	oFieldsDoc["type"] 					:= "sender"
	oFieldsDoc["value"]					:= SRA->RA_RGORG
	oFieldsDoc["label"]					:= Nil
	oFieldsDoc["mask"]					:= Nil
	Aadd(aFieldsDoc, oFieldsDoc)
	oDocuments["fields"]				:= aFieldsDoc

    // - DATA DE EMISS�O
	IF cPaisLoc == "BRA"
		oFieldsDoc 							:= &cJsonObj
		oFieldsDoc["type"] 					:= "senderDate"

		aDateGMT							:= Iif(!Empty(SRA->RA_DTRGEXP),LocalToUTC( DTOS(SRA->RA_DTRGEXP), "12:00:00" ),{}) 
		oFieldsDoc["value"]					:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")			
		oFieldsDoc["label"]					:= Nil
		oFieldsDoc["mask"]					:= "99/99/9999"
		Aadd(aFieldsDoc, oFieldsDoc)
	ENDIF
    
    oDocuments["fields"]				:= aFieldsDoc
    Aadd(aDocs, oDocuments)

    // ------------------------------------------------
    // - DOCUMENTS - CPF
    // - N�dulo respons�vel para o WIDGET DE CPF no UX.
    oFieldsDoc := Nil
    oDocuments := Nil
    aFieldsDoc := {}
    oFieldsDoc := &cJsonObj
    oDocuments := &cJsonObj

    // - CPF
    oDocuments["type"] 					:= "cpf"
    oDocuments["label"]					:= Nil

    oFieldsDoc["type"] 					:= "number"
    oFieldsDoc["value"]					:= Alltrim( SRA->RA_CIC )
    oFieldsDoc["label"]					:= Nil
    oFieldsDoc["mask"]					:= "999.999.999-99"
    Aadd(aFieldsDoc, oFieldsDoc)
    oDocuments["fields"]				:= aFieldsDoc

    Aadd(aDocs, oDocuments)

    // ----------------------------------------------------------------
    // - DOCUMENTS - CARTEIRA DE TRABALHO - WORKCARD
    // - N�dulo respons�vel para o WIDGET DE CARTEIRA DE TRABALHO no UX.
    oFieldsDoc := Nil
    oDocuments := Nil
    aFieldsDoc := {}
    oFieldsDoc := &cJsonObj
    oDocuments := &cJsonObj

    // - CARTEIRA DE TRABALHO
    oDocuments["type"] 					:= "workCard"
    oDocuments["label"]					:= Nil

    // N�MERO
    oFieldsDoc["type"] 					:= "number"
    oFieldsDoc["value"]					:= Alltrim( SRA->RA_NUMCP )
    oFieldsDoc["label"]					:= Nil
    oFieldsDoc["mask"]					:= ""
    Aadd(aFieldsDoc, oFieldsDoc)

    // S�RIE
    oFieldsDoc 							:= &cJsonObj
    oFieldsDoc["type"] 					:= "series"
    oFieldsDoc["value"]					:= Alltrim( SRA->RA_SERCP )
    oFieldsDoc["label"]					:= Nil
    oFieldsDoc["mask"]					:= ""
    Aadd(aFieldsDoc, oFieldsDoc)

    // ESTADO EMISSOR
    oFieldsDoc 							:= &cJsonObj
    oFieldsDoc["type"] 					:= "senderState"
    oFieldsDoc["value"]					:= Alltrim( SRA->RA_UFCP )
    oFieldsDoc["label"]					:= Nil
    oFieldsDoc["mask"]					:= ""
    Aadd(aFieldsDoc, oFieldsDoc)

    // DATA DE EMISS�O
	IF cPaisLoc == "BRA"
		oFieldsDoc 							:= &cJsonObj
		oFieldsDoc["type"] 					:= "senderDate"
		aDateGMT							:= Iif(!Empty(SRA->RA_DTCPEXP),LocalToUTC( DTOS(SRA->RA_DTCPEXP), "00:00:00" ),{})
		oFieldsDoc["value"]					:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "00:00:00")				 
		oFieldsDoc["label"]					:= Nil
		oFieldsDoc["mask"]					:= ""
		Aadd(aFieldsDoc, oFieldsDoc)
		oDocuments["fields"]				:= aFieldsDoc
		Aadd(aDocs, oDocuments)
	ENDIF

    // ------------------------------------------------
    // - DOCUMENTS - PIS - WORKCARD
    // - N�dulo respons�vel para o WIDGET DE PIS no UX.
    oFieldsDoc							:= Nil
    oDocuments							:= Nil
    aFieldsDoc							:= {}
    oFieldsDoc							:= &cJsonObj
    oDocuments							:= &cJsonObj
    oDocuments["type"] 		  			:= "pis"
    oDocuments["label"]					:= Nil

    oFieldsDoc["type"] 					:= "number"
    oFieldsDoc["value"]					:= Alltrim( SRA->RA_PIS)
    oFieldsDoc["label"]					:= Nil
    oFieldsDoc["mask"]					:= "999.99999.99-9"
    Aadd(aFieldsDoc, oFieldsDoc)
    oDocuments["fields"]				:= aFieldsDoc
    Aadd(aDocs, oDocuments)

    // --------------------------------------------------------------
    // - DOCUMENTS - TITULO DE ELEITOR 
    // - N�dulo respons�vel para o WIDGET DE TITULO DE ELEITOR no UX.
    oFieldsDoc						 	:= Nil
    oDocuments							:= Nil
    aFieldsDoc							:= {}
    oFieldsDoc							:= &cJsonObj
    oDocuments							:= &cJsonObj
    oDocuments["type"] 					:= "elector"
    oDocuments["label"]					:= Nil

    oFieldsDoc["type"] 					:= "number"
    oFieldsDoc["value"]					:= Alltrim( SRA->RA_TITULOE)
    oFieldsDoc["label"]					:= Nil
    oFieldsDoc["mask"]					:= "9999999999-99"
    Aadd(aFieldsDoc, oFieldsDoc)

    oFieldsDoc 							:= &cJsonObj
    oFieldsDoc["type"] 					:= "zone"
    oFieldsDoc["value"]					:= Alltrim( SRA->RA_ZONASEC)
    oFieldsDoc["label"]					:= Nil
    oFieldsDoc["mask"]					:= ""
    Aadd(aFieldsDoc, oFieldsDoc)

	IF cPaisLoc == "BRA"
		oFieldsDoc 							:= &cJsonObj
		oFieldsDoc["type"] 					:= "section"
		oFieldsDoc["value"]					:= Alltrim( SRA->RA_SECAO)
		oFieldsDoc["label"]					:= Nil
		oFieldsDoc["mask"]					:= ""
		Aadd(aFieldsDoc, oFieldsDoc)
		oDocuments["fields"]				:= aFieldsDoc
		Aadd(aDocs, oDocuments)
	ENDIF

    // -------------------------------------------------------
    // - DOCUMENTS - RESERVISTA 
    // - N�dulo respons�vel para o WIDGET DE RESERVISTA no UX.
    oFieldsDoc 							:= Nil
    oDocuments 							:= Nil
    aFieldsDoc 							:= {}
    oFieldsDoc 							:= &cJsonObj
    oDocuments 							:= &cJsonObj
    oDocuments["type"] 					:= "war"
    oDocuments["label"]					:= Nil

    oFieldsDoc["type"] 					:= "number"
    oFieldsDoc["value"]					:= Alltrim( SRA->RA_RESERVI )
    oFieldsDoc["label"]					:= Nil
    oFieldsDoc["mask"]					:= ""
    Aadd(aFieldsDoc, oFieldsDoc)
    oDocuments["fields"]				:= aFieldsDoc
    Aadd(aDocs, oDocuments)

    // ------------------------------------------------
    // - DOCUMENTS - CNH 
    // - N�dulo respons�vel para o WIDGET DE CNH no UX.
    oFieldsDoc							:= Nil
    oDocuments							:= Nil
    aFieldsDoc							:= {}
    aDateGMT							:= {}
    oFieldsDoc							:= &cJsonObj
    oDocuments							:= &cJsonObj

    oDocuments["type"] 					:= "driverLicense"
    oDocuments["label"]					:= "Carteira de motorista"

    oFieldsDoc["type"] 					:= "number"
    oFieldsDoc["value"]					:= Alltrim( SRA->RA_HABILIT )
    oFieldsDoc["label"]					:= Nil
    oFieldsDoc["mask"]					:= ""
    Aadd(aFieldsDoc, oFieldsDoc)

    oFieldsDoc 							:= &cJsonObj
	IF cPaisLoc == "BRA"
		If !Empty( SRA->RA_CATCNH )
		//Exibe o c�digo da categoria da CNH
			oFieldsDoc["type"]				:= "other"
			oFieldsDoc["value"]				:= Alltrim(aBoxCNH[Ascan( aBoxCNH, { |aBox| aBox[2] = SRA->RA_CATCNH } )][3])
			oFieldsDoc["label"]				:= EncodeUTF8(STR0007) //"Categoria"
		Else
			oFieldsDoc["type"] 				:= ""
			oFieldsDoc["value"]				:= ""
			oFieldsDoc["label"]				:= ""
		EndIf
		oFieldsDoc["mask"]					:= ""
		Aadd(aFieldsDoc, oFieldsDoc)

		oFieldsDoc 							:= &cJsonObj
		oFieldsDoc["type"] 					:= "sender"
		oFieldsDoc["value"]					:= Alltrim( EncodeUTF8(SRA->RA_CNHORG))
		oFieldsDoc["label"]					:= Nil
		oFieldsDoc["mask"]					:= ""
		Aadd(aFieldsDoc, oFieldsDoc)

		oFieldsDoc 							:= &cJsonObj
		oFieldsDoc["type"] 					:= "senderDate"
		aDateGMT							:= Iif(!Empty(SRA->RA_DTEMCNH),LocalToUTC( DTOS(SRA->RA_DTEMCNH), "12:00:00" ),{})  
		oFieldsDoc["value"]					:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")				 
		oFieldsDoc["label"]					:= Nil
		oFieldsDoc["mask"]					:= ""
		Aadd(aFieldsDoc, oFieldsDoc)

		oFieldsDoc 							:= &cJsonObj
		oFieldsDoc["type"] 					:= "dueDate"

		aDateGMT						    := {}
		aDateGMT							:= Iif(!Empty(SRA->RA_DTVCCNH),LocalToUTC( DTOS(SRA->RA_DTVCCNH), "12:00:00" ),{})  
		oFieldsDoc["value"]					:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")			
		oFieldsDoc["label"]					:= Nil
		oFieldsDoc["mask"]					:= ""
		Aadd(aFieldsDoc, oFieldsDoc)
		oDocuments["fields"]				:= aFieldsDoc
		Aadd(aDocs, oDocuments)

		// --------------------------------------------------------
		// - DOCUMENTS - PASSAPORTE 
		// - N�dulo respons�vel ppara o WIDGET DE PASSAPORTE no UX.
		oFieldsDoc 							:= Nil
		oDocuments 							:= Nil
		aFieldsDoc 							:= {}
		aDateGMT   							:= {}
		oFieldsDoc 							:= &cJsonObj
		oDocuments 							:= &cJsonObj

		oDocuments["type"] 					:= "passport"
		oDocuments["label"]					:= Nil
		oFieldsDoc["type"] 					:= "number"
		oFieldsDoc["value"]					:= Alltrim( SRA->RA_NUMEPAS )
		oFieldsDoc["label"]					:= Nil
		oFieldsDoc["mask"]					:= ""
		Aadd(aFieldsDoc, oFieldsDoc)

		oFieldsDoc 							:= &cJsonObj
		oFieldsDoc["type"] 					:= "originCountry"
		oFieldsDoc["value"]					:= Alltrim( EncodeUTF8(FDESC("CCH",SRA->RA_CODPAIS,"CCH_PAIS",,SRA->RA_FILIAL) ) )
		oFieldsDoc["label"]					:= Nil
		oFieldsDoc["mask"]					:= ""
		Aadd(aFieldsDoc, oFieldsDoc)

		oFieldsDoc 							:= &cJsonObj
		oFieldsDoc["type"] 					:= "senderDate"
		aDateGMT							:= Iif(!Empty(SRA->RA_DEMIPAS),LocalToUTC( DTOS(SRA->RA_DEMIPAS), "12:00:00" ),{}) 
		oFieldsDoc["value"]					:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")			
		oFieldsDoc["label"]					:= Nil
		oFieldsDoc["mask"]					:= ""
		Aadd(aFieldsDoc, oFieldsDoc)

		oFieldsDoc 							:= &cJsonObj
		oFieldsDoc["type"] 					:= "dueDate"
		aDateGMT						    := {}
		aDateGMT							:= Iif(!Empty(SRA->RA_DVALPAS),LocalToUTC( DTOS(SRA->RA_DVALPAS), "12:00:00" ),{}) 
		oFieldsDoc["value"]					:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")		
		oFieldsDoc["label"]					:= Nil
		oFieldsDoc["mask"]					:= ""
		Aadd(aFieldsDoc, oFieldsDoc)
		
		oDocuments["fields"]				:= aFieldsDoc
    	Aadd(aDocs, oDocuments)
	ENDIF

Return(Nil)


Function CoordInfos(aStructREST,cMat,cBranchVld)
Local aRet			:= {}
Local nX			:= 0
Local nI			:= 0

Default aStructREST := {}
Default cMat		:= ""
Default cBranchVld  := FwCodFil()

For nX := 1 To Len(aStructREST)
	For nI := 1 To Len(aStructRest[nX]:ListOfEmployee)
		If aStructRest[nX]:ListOfEmployee[nI]:Registration == cMat

			Aadd(aRet,Alltrim(aStructRest[nX]:ListOfEmployee[nI]:NameSup))
			Aadd(aRet, fSupGetPosition( aStructRest[nX]:ListOfEmployee[nI]:SupFilial, aStructRest[nX]:ListOfEmployee[nI]:SupRegistration, aStructRest[nX]:ListOfEmployee[nI]:SupEmpresa ) )
			Aadd(aRet,Alltrim(aStructRest[nX]:ListOfEmployee[nI]:SupFilial))
			Aadd(aRet,Alltrim(aStructRest[nX]:ListOfEmployee[nI]:SupRegistration))
			Exit

		EndIf
	Next nI
Next nX

Return( aRet )
