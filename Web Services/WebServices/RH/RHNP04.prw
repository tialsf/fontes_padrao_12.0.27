#INCLUDE "TOTVS.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "RHNP04.CH"

Function RHNP04()
Return .T.

Private cMRrhKeyTree := ""


WSRESTFUL Request DESCRIPTION STR0001 //"Request Notifica��es"

	WSDATA WsNull		As String  Optional
	WSDATA employeeId	As String  Optional
	WSDATA page			As String  Optional
	WSDATA pageSize		AS String  Optional
	WSDATA tab  		As String  Optional
	WSDATA type  		As String  Optional
	WSDATA types  		As String  Optional
	WSDATA date			As String  Optional
	WSDATA name			As String  Optional
	WSDATA employeeName	As String  Optional
	WSDATA pending 		As boolean Optional
	WSDATA requestId    As String  Optional
	WSDATA status		As String  Optional
	WSDATA startDate	As String  Optional

	//****************************** GETs ***********************************

	WSMETHOD GET getCount ;
	DESCRIPTION STR0003 ; //"Retorna a quantidade de pend�ncias."
	WSSYNTAX "/notifications/count/{employeeId}" ;
	PATH "/notifications/count/{employeeId}" ;
	PRODUCES 'application/json;charset=utf-8'

	WSMETHOD GET getRequestCount ;
	DESCRIPTION STR0010 ; //"Retorna as quantidades de solicita��es do usu�rio."
	WSSYNTAX "/count/{employeeId}" ;
	PATH "/count/{employeeId}" ;
	PRODUCES 'application/json;charset=utf-8'

	WSMETHOD GET getNotifications ;
	DESCRIPTION STR0004 ; //"Retorna as pend�ncias do usu�rio."
	WSSYNTAX "/notifications/{employeeId}" ;
	PATH "/notifications/{employeeId}" ;
	PRODUCES 'application/json;charset=utf-8'

	WSMETHOD GET Requisitions ;
	DESCRIPTION STR0011 ; //"Retorna as requisi��es do usu�rio respons�vel."
	WSSYNTAX "/requisitions/{employeeId}" ;
	PATH "/requisitions/{employeeId}" ;
	PRODUCES 'application/json;charset=utf-8'

	//****************************** PUTs ***********************************

	WSMETHOD PUT Requisitions ;
	DESCRIPTION STR0019 ; //"Aprova ou Reprova requisi��es do usu�rio respons�vel."
	WSSYNTAX "/requisitions/{employeeId}" ;
	PATH "/requisitions/{employeeId}" ;
	PRODUCES 'application/json;charset=utf-8'

	WSMETHOD PUT editNotifications ;
	DESCRIPTION STR0005 ; //"Aprova ou Reprova notifica��es."
	WSSYNTAX "/notifications/{employeeId}" ;
	PATH "/notifications/{employeeId}" ;
	PRODUCES 'application/json;charset=utf-8'

END WSRESTFUL

//******************** M�todos GETs ********************
//******************************************************

WSMETHOD GET getCount PATHPARAM employeeId WSREST Request

Local cJsonObj 	 	:= "JsonObject():New()"
Local oItemData	 	:= &cJsonObj
Local oItem		 	:= &cJsonObj
Local oMessages	  	:= &cJsonObj
Local nLenParms	 	:= Len(::aURLParms)
Local cMatSRA		:= ""
Local cBranchVld	:= ""
Local cToken	  	:= ""
Local cSubMat		:= ""
Local cSubBranch	:= ""
Local cSubDepts		:= ""
Local nX			:= 0
Local aSubstitute	:= {}
Local aMessages		:= {}
Local aDataLogin	:= {}
Local lAuth    		:= .T.
Local lSubstitute	:= .F.

DEFAULT Self:types 	:= ""

::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken     := Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cBranchVld := aDataLogin[5]
EndIf

If Empty(cMatSRA) .Or. Empty(cBranchVld)
   ::SetHeader('Status', "401")

	oMessages["type"]   := "error"
	oMessages["code"]   := "401"
	oMessages["detail"] := EncodeUTF8(STR0006) //"Dados inv�lidos."

	Aadd(aMessages,oMessages)
	lAuth := .F.
EndIf

If lAuth .And. nLenParms == 3 .And. !Empty(::aURLParms[3])
	//Verifica se o funcionario esta substituindo o seu superior
	aSubstitute := fGetSupNotify( cBranchVld, cMatSRA, .T. )

	If Len(aSubstitute) > 0
		For nX := 1 To Len(aSubstitute)
			cSubBranch += "'" + aSubstitute[nX, 1] + "',"
			cSubMat	+= "'" + aSubstitute[nX, 2] + "',"
			cSubDepts += aSubstitute[nX, 3]
		Next nX
		cSubMat		:= SubStr( cSubMat, 1, Len(cSubMat)-1 )
		cSubBranch	:= SubStr( cSubBranch, 1, Len(cSubBranch)-1 )
		cSubDepts	:= SubStr( cSubDepts, 1, Len(cSubDepts)-1 )
		lSubstitute	:= .T.
	Else
		cSubMat	:= "'" + cMatSRA + "'"
		cSubBranch := "'" + cBranchVld + "'"
	EndIf

	countNotifications(cSubMat,cJsonObj,@oItemData,cSubBranch,cSubDepts,Self:types,"notify")
EndIf

// - Por por padr�o todo objeto tem
// - data: contendo a estrutura do JSON
// - messages: para determinados avisos
// - length: informativo sobre o tamanho.
oItem["data"] 		  := oItemData
oItem["messages"] 	  := aMessages
oItem["length"]   	  := 1 

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)


WSMETHOD GET getRequestCount PATHPARAM employeeId WSREST Request

Local cJsonObj 	 	:= "JsonObject():New()"
Local oItemData	 	:= &cJsonObj
Local oItem		 	:= &cJsonObj
Local oMessages	  	:= &cJsonObj
Local nLenParms	 	:= Len(::aURLParms)
Local nTotReq		:= 0
Local nTotPend		:= 0
Local cMatSRA		:= ""
Local cBranchVld	:= ""
Local cToken	  	:= ""
Local aMessages		:= {}
Local aDataLogin	:= {}
Local lAuth    		:= .T.

DEFAULT Self:type 	:= ""
DEFAULT Self:types 	:= ""

::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken     := Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cBranchVld := aDataLogin[5]
EndIf

If Empty(cMatSRA) .Or. Empty(cBranchVld)
   ::SetHeader('Status', "401")

	oMessages["type"]   := "error"
	oMessages["code"]   := "401"
	oMessages["detail"] := EncodeUTF8(STR0006) //"Dados inv�lidos."
	Aadd(aMessages,oMessages)
	lAuth := .F.
EndIf

If nLenParms > 0
	//varinfo("::aUrlParms -> ",::aUrlParms) 
	If (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
	   If !Empty(::aUrlParms[2])
 
		  aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
		  If Len(aIdFunc) > 1 
		  
			  //valida se o solicitante da requisi��o pode ter acesso as informa��es
				 If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
					cBranchVld	:= aIdFunc[1]
					cMatSRA		:= aIdFunc[2]
				 Else
					cBranchVld	:= ""
					cMatSRA		:= ""
			  EndIf	
		  EndIf
 
	   EndIf
	EndIf
	
    If !Empty(cBranchVld) .And. !Empty(cMatSRA)
		countNotifications(cMatSRA,cJsonObj,@oItemData,cBranchVld,"",Self:types,"count",@nTotReq,@nTotPend)
	ENDIF
EndIF

oItem["totalPending"] := nTotPend
oItem["totalRequest"] := nTotReq
oItem["subTotals"] 	  := oItemData

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)


WSMETHOD GET getNotifications PATHPARAM employeeId WSREST Request
Local cJsonObj 	 	:= "JsonObject():New()"
Local oItemData	 	:= &cJsonObj
Local oItem		 	:= &cJsonObj
Local oMessages		:= &cJsonObj
Local nX			:= 0
Local nCount		:= 0
Local nIniCount		:= 0
Local nFimCount 	:= 0
Local aQPs          := {}
Local aMessages		:= {}
Local aItems		:= {}
Local aDataLogin	:= {}
Local cMatSRA		:= ""
Local cMatLogin		:= ""
Local cBranchVld	:= ""
Local cSubMat		:= ""
Local cSubBranch	:= ""
Local cSubDepts		:= ""
Local cTypeFilter	:= ""
Local lAuth			:= .T.

// - Par�metros enviados pela URL - QueryString
DEFAULT self:tab			:= ""
DEFAULT Self:type 			:= ""
DEFAULT self:date 			:= ""
DEFAULT self:page			:= "*"
DEFAULT self:pageSize  		:= "6"
DEFAULT self:employeeName	:= ""

Aadd(aQPs,self:tab)
Aadd(aQPs,self:type)
Aadd(aQPs,self:date)
Aadd(aQPs,self:page)
Aadd(aQPs,self:pageSize)
Aadd(aQPs,self:employeeName)

::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken      := Self:GetHeader('Authorization')
aDataLogin 	:= GetDataLogin(cToken)
cTypeFilter := Self:type

If Len(aDataLogin) > 0
	cMatSRA     := aDataLogin[1]
	cMatLogin   := aDataLogin[2]
	cBranchVld  := aDataLogin[5]
EndIf

If Empty(cMatSRA) .Or. Empty(cBranchVld)
	oMessages["type"]   := "info"
	oMessages["code"]   := "401"
	oMessages["detail"] := EncodeUTF8(STR0007) //"As notifica��es n�o foram encontradas."

	Aadd(aMessages,oMessages)
	lAuth := .F.
EndIf

If lAuth
	//Verifica se o funcionario esta substituindo o seu superior
	aSubstitute := fGetSupNotify( cBranchVld, cMatSRA, .T. )

	If Len(aSubstitute) > 0
		For nX := 1 To Len(aSubstitute)
			cSubMat	+= "'" + aSubstitute[nX, 2] + "',"
			cSubBranch += "'" + aSubstitute[nX, 1] + "',"
			cSubDepts += aSubstitute[nX, 3]
		Next nX
		cSubMat		:= SubStr( cSubMat, 1, Len(cSubMat)-1 )
		cSubBranch	:= SubStr( cSubBranch, 1, Len(cSubBranch)-1 )
		cSubDepts	:= SubStr( cSubDepts, 1, Len(cSubDepts)-1 )
		cMatLogin	:= "" //Quando substitui seu gestor o funcionario pode ver suas proprias solicitacoes
	Else
		cSubMat	:= "'" + cMatSRA + "'"
		cSubBranch := "'" + cBranchVld + "'"
	EndIf

	//Faz o controle de paginacao
	If Self:page == "*" .Or. Empty(Self:page)
		// o objetivo do caracter '*' � de manter a compatibilidade com vers�es anteriores
		// que n�o realizam pagina��o, nesse caso o QP Page n�o foi informado na URL, onde 
		// a quantidade de registro devolvido n�o deve ser limitado pela pagina��o 
		nIniCount := 1 
		nFimCount := 999999
	Else
		nIniCount := ( Val(Self:pageSize) * ( Val(Self:page) - 1 ) ) + 1
		nFimCount := ( nIniCount + Val(Self:pageSize) ) - 1
	EndIf

	getNotifications(cSubMat,cJsonObj,@oItemData,cSubBranch,@aItems,aQPs,cMatSRA,cSubDepts,@nCount,nIniCount,nFimCount)
EndIf

oItem["hasNext"]	:= (nCount > nFimCount)
oItem["items"]		:= aItems

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)


WSMETHOD GET Requisitions PATHPARAM employeeId WSREST Request
Local cJsonObj 	 	:= "JsonObject():New()"
Local oItems		:= &cJsonObj
Local cMatSRA		:= ""
Local cBranchVld	:= ""
Local cToken		:= ""
Local cType         := ""
Local cWhere		:= "%"
LocaL aData 		:= {}
Local aType			:= {}
Local aIdFunc       := {}
Local aDataLogin	:= {}
Local nCount		:= 0
Local nIniCount		:= 0
Local nFimCount 	:= 0
Local nX			:= 0

DEFAULT self:startDate := ""
DEFAULT self:status    := ""
DEFAULT self:name      := ""
DEFAULT self:requestId := ""
DEFAULT self:types     := ""
DEFAULT self:pending   := .F.
DEFAULT self:page      := "1"
DEFAULT self:pageSize  := "6"

Self:SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cBranchVld := aDataLogin[5]
EndIf

If Len(::aUrlParms) > 0
	If (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
		If !Empty(::aUrlParms[2])

			aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
			If Len(aIdFunc) > 1 
			
			    //valida se o solicitante da requisi��o pode ter acesso as informa��es
                If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                   cBranchVld	:= aIdFunc[1]
                   cMatSRA		:= aIdFunc[2]
                Else
                   cBranchVld	:= ""
                   cMatSRA		:= ""
	    		EndIf	
			EndIf

		EndIf
	EndIf
EndIF

If !Empty(cBranchVld) .And. !Empty(cMatSRA)

	If Self:pending
		cWhere += " RH3.RH3_FILAPR = '" +cBranchVld +"' AND "
		cWhere += " RH3.RH3_MATAPR = '" +cMatSRA    +"' "
	Else
		cWhere += " RH3.RH3_FILINI = '" +cBranchVld +"' AND "
		cWhere += " RH3.RH3_MATINI = '" +cMatSRA    +"' "
	ENDIF

	If !empty(Self:name)
		cWhere += "AND SRA.RA_NOME LIKE '" + UPPER(Self:name) + "%' "
    ENDIF

	If !empty(Self:requestId)
		cWhere += "AND RH3.RH3_CODIGO = '" +Self:requestId + "' "
    ENDIF

	If !empty(Self:startDate)
		cWhere += "AND RH3.RH3_DTSOLI = '" +  dTos( cTod( Format8601( .T., Self:startDate ) ) ) + "' "
    ENDIF

	If !empty(Self:status)
		cWhere += "AND "

		If UPPER(Self:status) == "APPROVED"
			cWhere += "RH3.RH3_STATUS = '2' "
		ElseIf  UPPER(Self:status) == "APPROVING"
			cWhere += "RH3.RH3_STATUS IN ('1','4') "
		ElseIf UPPER(Self:status) == "REJECTED"
			cWhere += "RH3.RH3_STATUS = '3' "
		ElseIf UPPER(Self:status) == "CLOSED"
			cWhere += "RH3.RH3_STATUS IN ('2','3') "
		Else
			cWhere += "RH3.RH3_STATUS IN ('1','2','3','4') "
		ENDIF
    ENDIF

	If !empty(Self:types)
		aType := Strtokarr(self:types , "|")
		If Len(aType) > 0
	
			cType  := ""
			For nX := 1 to Len(aType)
				If nX > 1 
					cType += ","
				EndIf
	
				If UPPER(aType[nX]) == "DEMISSION"
					cType += "'6'"
				ElseIf UPPER(aType[nX]) == "VACATION"
					cType += "'B'"
				ElseIf UPPER(aType[nX]) == "ALLOWANCE"
					cType += "'8'"
				ElseIf UPPER(aType[nX]) == "CLOCKING"
					cType += "'Z'"
				EndIf
			Next nX
	
			If !empty(cType)
				cWhere += "AND RH3.RH3_TIPO IN (" +cType +") "
			Endif
		EndIf
	EndIf

	cWhere += "%"

	//Faz o controle de paginacao
	If Self:page == "1" .Or. Empty(Self:page)
		nIniCount := 1 
		nFimCount := If( Empty(Self:pageSize), 6, Val(Self:pageSize) )
	Else
		nIniCount := ( Val(Self:pageSize) * ( Val(Self:page) - 1 ) ) + 1
		nFimCount := ( nIniCount + Val(Self:pageSize) ) - 1
	EndIf

	//Requisi��es de demiss�o.
	aData := fRequests(cBranchVld, cMatSRA, cWhere, @nCount, nIniCount, nFimCount)
	
	oItems["hasNext"] := (nCount > nFimCount)
	oItems["items"]   := aData
Else
	oItems["type"]   := "error"
	oItems["code"]   := "401"
	oItems["detail"] := EncodeUTF8(STR0006) //"Dados inv�lidos."
EndIf

cJson := FWJsonSerialize(oItems, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)


//******************** M�todos PUTs ********************
//******************************************************

// - PUT RESPONS�VEL POR APROVAR OU REPROVAR REQUISICOES PENDENTES.
WSMETHOD PUT Requisitions PATHPARAM employeeId WSREST Request
Local cBody         := Self:GetContent()
Local cMsg          := ""
Local cJson         := ""
Local cToken        := ""
Local lOk           := .T.

Self:SetHeader('Access-Control-Allow-Credentials' , "true")
cToken := Self:GetHeader('Authorization')

	cMsg := fAvalAprRepr(cToken, cBody)

	If empty(cMsg)
		cJson := FWJsonSerialize(cBody, .F., .F., .T.)
		Self:SetResponse(cJson)
	Else
		lOk  := .F.
		SetRestFault(400, cMsg, .T.)
	EndIf

Return (lOk)


// PUT RESPONS�VEL POR APROVAR OU REPROVAR AS NOTIFICA��ES PENDENTES.
WSMETHOD PUT editNotifications PATHPARAM employeeId WSREST Request
Local cJsonObj 	 	:= "JsonObject():New()"
Local oBody			:= &cJsonObj
Local oItem			:= &cJsonObj
Local oItemDetail	:= &cJsonObj
Local cBody			:= ::GetContent()
Local aUrlParam		:= ::aUrlParms
Local cJson			:= ""
Local cToken		:= ""
Local lOK			:= .T.
Local aRequests		:= {}
Local aFields		:= {}

::SetHeader('Access-Control-Allow-Credentials' , "true")
cToken  := Self:GetHeader('Authorization')

oBody:FromJson(cBody)

aRequests	:= If(oBody:hasProperty("requisitions"), oBody["requisitions"], "")		//nova e contrato aprova��o notifica��es
aFields		:= If(oBody:hasProperty("fields"), oBody["fields"], "")					//antigo modelo aprova��o notifica��es

If empty(aRequests) .and. !empty(aFields)
	//legado de request para clientes sem atualiza��o de front-end
	EditRH3(aUrlParam,cBody,cJsonObj,@oItem,@oItemDetail,cToken,.T.)

	cJson := FWJsonSerialize(oItem, .F., .F., .T.)
	Self:SetResponse(cJson)
else
	cMsg := fAvalAprRepr(cToken, cBody)

	If empty(cMsg)
		cJson := FWJsonSerialize(oBody, .F., .F., .T.)
		Self:SetResponse(cJson)
	Else
		lOk  := .F.
		SetRestFault(400, cMsg, .T.)
	EndIf
EndIf

Return (lOk)
