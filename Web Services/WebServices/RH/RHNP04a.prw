#INCLUDE "TOTVS.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "RESTFUL.CH"

#INCLUDE "RHNP04.CH"

Function RHNP04a()
Return .T.


Function getNotifications(cMatSRA,cJsonObj,oItemData,cBranchVld,aItems,aQPs,cMatAprov,cSubDepts,nCount,nIniCount,nFimCount)
Local nI			:= 0
Local cQuery		:= GetNextAlias()
Local cQuerySRF		:= GetNextAlias()
Local aArea			:= GetArea()
Local cBranch		:= xFilial("RH3")
Local cType 		:= "'B','8','Z'"
Local cDescType		:= ""
Local oFields		:= Nil
Local oEmployee		:= Nil
Local oProps		:= Nil
Local aFields		:= {}
Local aExtFields	:= {}
Local aEmployee		:= {}
Local aSeqClock		:= {}
Local cWhere		:= ""
Local cAux			:= ""
Local cAuxCode		:= ""
Local cJustify		:= ""
Local __cSRFtab		:= ""
Local __cSRFDel 	:= "% SRF.D_E_L_E_T_ = ' ' %"
Local __cRH3Del 	:= "% RH3.D_E_L_E_T_ = ' ' %"
Local lJustify		:= .F.

Default cJsonObj	:= "JsonObject():New()"
Default oItemData	:= &cJsonObj
Default cMatSRA		:= ""
Default cMatAprov	:= ""
Default cBranchVld	:= FwCodFil()
Default aItems		:= {}
Default aQPs		:= {}
Default cSubDepts	:= ""
DEFAULT nCount    	:= 0
DEFAULT nIniCount 	:= 1
DEFAULT nFimCount 	:= 6


oFields   := JsonObject():New()
oEmployee := JsonObject():New()

//Retorna uma matriz com entrada/saida de cada dia para classificar as marcacoes do espelho
aSeqClock := fGetSeqClock(cBranch, cMatSRA, cMatAprov)

//************************************************************
//Prepara��o do cWhere
//************************************************************
cWhere := "%"
cWhere += " RH3.RH3_FILAPR IN (" + cBranchVld + ") AND "
cWhere += " RH3.RH3_MATAPR IN (" + cMatSRA + ") AND "

If !empty(aQPs[6]) //nome do colaborador
	cWhere += " SRA.RA_NOME LIKE '%" + UPPER( aQPs[6] ) + "%' AND "
EndIf

If !Empty(aQPs[3]) //data da solicita��o
	cWhere += " RH3.RH3_DTSOLI = '" + Substr(aQPs[3],1,4) + Substr(aQPs[3],6,2) + Substr(aQPs[3],9,2) + "' AND "
EndIf

If empty(aQPs[1]) .and. !empty(aQPs[2]) //Queryparam (Tab .and. Type)
	If Lower(aQPs[2])     == "allowance"
		cType := "'8'"
	ElseIf Lower(aQPs[2]) == "clocking"
		cType := "'Z'"
	Elseif Lower(aQPs[2]) == "vacation"
		cType := "'B'"
	Elseif Lower(aQPs[2]) == "demission"
		cType := "'6'"
	EndIf

	cWhere += " RH3.RH3_TIPO IN (" + cType + ") "
EndIf

If !empty(aQPs[1]) .and. aQPs[1] == "timesheet" //Queryparam (Tab)
	cWhere += " RH3.RH3_TIPO IN ('8','Z') " //"allowance" e "clocking"
EndIf

If !empty(aQPs[1]) .and. aQPs[1] == "vacation" //Queryparam (Tab)
	cWhere += " RH3.RH3_TIPO = 'B' "
EndIf

If empty(aQPs[1]) .and. empty(aQPs[2])
	cWhere += " RH3.RH3_TIPO IN ('8','Z','B') " //"allowance", "clocking" e "vacation"
EndIf

If !Empty( cSubDepts )
	cWhere += " AND SRA.RA_DEPTO IN (" + cSubDepts + ") "
EndIF

cWhere += "%"


//************************************************************
//Execu��o da Query
//************************************************************
If !Empty( cSubDepts ) .or. !empty(aQPs[6])
	BEGINSQL ALIAS cQuery
	    SELECT RH3.RH3_FILIAL, RH3.RH3_CODIGO, RH3.RH3_MAT, RH3.RH3_DTSOLI, RH3.RH3_VISAO, RH3.RH3_FILINI, RH3.RH3_MATINI, RH3.RH3_FILAPR, RH3.RH3_MATAPR, RH3.RH3_STATUS, RH3.RH3_TIPO, RH3.RH3_EMP
			FROM %table:RH3% RH3
	    INNER JOIN %table:SRA% SRA
			ON RH3.RH3_FILIAL = SRA.RA_FILIAL AND RH3.RH3_MAT = SRA.RA_MAT
	    WHERE RH3.RH3_MAT <> %Exp:cMatAprov% AND
			RH3.RH3_STATUS = '1' AND
			%Exp:cWhere% AND
			%exp:__cRH3Del%
		Order by RH3.RH3_DTSOLI
	ENDSQL
Else
	BEGINSQL ALIAS cQuery
	    SELECT RH3.RH3_FILIAL, RH3.RH3_CODIGO, RH3.RH3_MAT, RH3.RH3_DTSOLI, RH3.RH3_VISAO, RH3.RH3_FILINI, RH3.RH3_MATINI, RH3.RH3_FILAPR, RH3.RH3_MATAPR, RH3.RH3_STATUS, RH3.RH3_TIPO, RH3.RH3_EMP
			FROM %table:RH3% RH3
	    WHERE RH3.RH3_MAT <> %Exp:cMatAprov% AND
			RH3.RH3_STATUS = '1' AND
			%Exp:cWhere% AND
			%exp:__cRH3Del%
		Order by RH3.RH3_DTSOLI
	ENDSQL
EndIf

While (cQuery)->(!Eof())

	nCount++
	If ( nCount >= nIniCount .and. nCount <= nFimCount )
		oItemData					 := JsonObject():New()

		oItemData["id"] 			 := (cQuery)->RH3_FILIAL + (cQuery)->RH3_MAT + (cQuery)->RH3_CODIGO + (cQuery)->RH3_MATAPR
		oItemData["type"]			 := GetENUMDecode((cQuery)->RH3_TIPO)
		oItemData["canApprove"] 	 := Iif((cQuery)->RH3_MATAPR $ cMatSRA, .T., .F.)

		oEmployee 					 := JsonObject():New()
		aEmployee 					 := getSummary((cQuery)->RH3_MAT, (cQuery)->RH3_FILIAL, (cQuery)->RH3_EMP)
		oEmployee["id"] 			 := aEmployee[4] +"|" +aEmployee[1] //Filial+Mat
		oEmployee["name"]			 := aEmployee[2]
		oEmployee["roleDescription"] := aEmployee[3]

		oItemData["employeeSummary"] := oEmployee

		If (cQuery)->RH3_TIPO == "B"
			__cSRFtab := "%" + RetFullName("SRF", (cQuery)->RH3_EMP) + "%"
			__cSRFDel := "% SRF.D_E_L_E_T_ = ' ' %"
		EndIf

		If RH4->(dbSeek(xFilial("RH4", (cQuery)->RH3_FILIAL) + (cQuery)->RH3_CODIGO ))
			lJustify := .F.

			While RH4->(!Eof())

				oFields := JsonObject():New()
				If RH4->RH4_CODIGO == (cQuery)->RH3_CODIGO
					getFields(@oFields, (cQuery)->RH3_TIPO, (cQuery)->RH3_CODIGO, aSeqClock)

					If !empty(oFields['type'])
						Aadd(aFields,oFields)

						If (cQuery)->RH3_TIPO == 'Z' .and. oFields['type'] == 'justify' //inclus�o manual
							lJustify := .T.
						EndIf
					EndIf
				Else
					EXIT
				EndIf

				cAux     := (cQuery)->RH3_MAT
				cAuxCode := (cQuery)->RH3_CODIGO
				RH4->(DbSkip())
			EndDo

			// add o 'justify' vazio, mesmo quando n�o exista observa��o na solic. abono
			// com objetivo de n�o bloquear o processo de aprova��o/reprova��o nas notifia��es
			If !lJustify .And. (cQuery)->RH3_TIPO == 'Z'
				cDescType := "justify"

				oFields := JsonObject():New()
				oFields["type"]  := EncodeUTF8(cDescType)
				oFields["value"] := ""

				oProps := JsonObject():New()
				oProps["field"]     := cDescType
				oProps["visible"]   := .T.
				oProps["editable"]  := .F.
				oProps["required"]  := .F.

				oFields["props"]    := oProps
				Aadd(aFields,oFields)
			EndIf
		EndIf


		If (cQuery)->RH3_TIPO == "B"

			//Busca per�odo aquisitivo atual em aberto
			BEGINSQL ALIAS cQuerySRF
				SELECT SRF.RF_FILIAL, SRF.RF_MAT, SRF.RF_DATABAS, SRF.RF_DATAFIM
				FROM %exp:__cSRFtab% SRF
				WHERE
					SRF.RF_FILIAL = %Exp:(cQuery)->RH3_FILIAL% AND
					SRF.RF_MAT = %Exp:(cQuery)->RH3_MAT% AND
					SRF.RF_STATUS = '1' AND
					%exp:__cSRFDel%
			ENDSQL

			IF (cQuerySRF)->(!Eof())
				oFields := JsonObject():New()
				oFields["type"]  := "initVacationLimit"
				oFields["value"] := formatGMT( (cQuerySRF)->RF_DATABAS, .T. )
				Aadd(aFields, oFields)

				oFields := JsonObject():New()
				oFields["type"]  := "endVacationLimit"
				oFields["value"] := formatGMT( (cQuerySRF)->RF_DATAFIM, .T. )
				Aadd(aFields, oFields)
			EndIf

			(cQuerySRF)->( DBCloseArea() )

		EndIf

		//Busca justificativas (nao considera marcacoes que ja possui tratamento especifico)
		If !(cQuery)->RH3_TIPO == "Z"
			cJustify := getRGKJustify((cQuery)->RH3_FILIAL,(cQuery)->RH3_CODIGO)
			If !empty(cJustify)
				oFields := JsonObject():New()
				oFields["type"]  := "justify"
				oFields["value"] := cJustify

				//Libera campo de justificativa
				oProps := JsonObject():New()
				oProps["field"]     := "justify"
				oProps["visible"]   := .T.
				oProps["editable"]  := .F.
				oProps["required"]  := .F.

				oFields["props"]    := oProps
				Aadd(aFields, oFields)
			Else
				oFields := JsonObject():New()
				oFields["type"]  := "justify"
				oFields["value"] := ""

				//Libera campo de justificativa
				oProps := JsonObject():New()
				oProps["field"]     := "justify"
				oProps["visible"]   := .T.
				oProps["editable"]  := .F.
				oProps["required"]  := .F.

				oFields["props"]    := oProps
				Aadd(aFields, oFields)
			EndIf
		EndIf

		//Inicializa campos contrato
		aExtFields := getInitFields((cQuery)->RH3_TIPO)
		For nI := 1 To Len(aExtFields)
			oFields := JsonObject():New()
			oFields["type"]  := aExtFields[nI][1]
			oFields["value"] := aExtFields[nI][2]

	        //Inclui properties de cada campo
			oProps := JsonObject():New()
			oProps["field"]     := aExtFields[nI][1]
			oProps["visible"]   := aExtFields[nI][4]
			oProps["editable"]  := aExtFields[nI][5]
			oProps["required"]  := aExtFields[nI][6]
			oFields["props"]    := oProps

			Aadd(aFields, oFields)
		Next nI

		//Carrega Fields
		oItemData["fields"] := aFields
		aFields             := {}
		Aadd(aItems,oItemData)
	EndIf

	(cQuery)->(DbSkip())
EndDo

(cQuery)->( DBCloseArea() )

RestArea(aArea)

Return(Nil)


/*/{Protheus.doc}countNotifications()
- Efetua a contagem das notifica��es do usu�rio logado.
@author:	Matheus Bizutti
/*/
Function countNotifications(cMatSRA,cJsonObj,oItemData,cBranchVld,cSubDepts,cTypes,cAction,nTotReq,nTotPend)

Local nX            := 0
Local nQtdTotal     := 0
Local aData         := {}
Local aTypesFilter  := {}
Local oSubTotals    := Nil
Local cWhere		:= ""
Local cTypeQRY      := ""
Local cQuery        := GetNextAlias()
Local cQryCount 	:= GetNextAlias()

Default cJsonObj    := "JsonObject():New()"
Default oItemData   := &cJsonObj
Default cMatSRA     := ""
Default cBranchVld  := FwCodFil()
Default cSubDepts	:= ""
Default cTypes   	:= ""
Default cAction   	:= ""
Default nTotReq		:= 0
Default nTotPend	:= 0

oSubTotals := &cJsonObj

aTypesFilter := STRTOKARR(cTypes, "|")
IF len(aTypesFilter) > 0
	For nX := 1 To Len(aTypesFilter)
		If nX > 1
			cTypeQRY	+= ","	
		ENDIF

		IF aTypesFilter[nX] == "allowance"
			cTypeQRY	+= "'8'"
		elseif aTypesFilter[nX] == "vacation"
			cTypeQRY	+= "'B'"
		elseif aTypesFilter[nX] == "clocking"
			cTypeQRY	+= "'Z'"
		elseif cAction == "count" .and. aTypesFilter[nX] == "demission"
			cTypeQRY	+= "'6'"
        else
            //tratamento caso seja informado algum par�metro n�o reconhecido
			cTypeQRY    += "''"
		ENDIF	
	Next nX
ENDIF

//Caso nenhum filtro seja informado, todos os tipos ser�o retornados
IF empty(cTypeQRY) .and. cAction == "notify"
   cTypeQRY := "'B','8','Z'"
ElseIf empty(cTypeQRY) .and. cAction == "count"
   cTypeQRY := "'B','8','Z','6'"
EndIF

cWhere := ""
If cAction == "count"
	cWhere := "%"
	cWhere += "(( RH3.RH3_FILINI = '" + cBranchVld + "' AND "
	cWhere += " RH3.RH3_MATINI = '" + cMatSRA    + "') OR "
	cWhere += "( RH3.RH3_FILAPR = '" + cBranchVld + "' AND 
	cWhere += " RH3.RH3_MATAPR = '" +  cMatSRA    + "')) AND"
	cWhere += " RH3.RH3_TIPO   IN (" + cTypeQRY  + ") "
Elseif cAction == "notify"
    cWhere := "%"
    cWhere += " RH3.RH3_FILAPR IN (" + cBranchVld + ") AND "
    cWhere += " RH3.RH3_MATAPR IN (" + cMatSRA    + ") AND "
    cWhere += " RH3.RH3_TIPO   IN (" + cTypeQRY   + ") "
ENDIF

//Quando o funcionario esta substituindo seu gestor
If !Empty( cWhere )
	If !Empty( cSubDepts )

		cWhere += " AND SRA.RA_DEPTO IN (" + cSubDepts + ") "
		cWhere += "%"

		BEGINSQL ALIAS cQuery

			SELECT RH3.RH3_TIPO, COUNT(*) QTD
					FROM %table:RH3% RH3
			INNER JOIN %table:SRA% SRA
					ON RH3_FILIAL = RA_FILIAL AND RH3_MAT = RA_MAT
			WHERE  RH3.RH3_STATUS = '1' AND
					%Exp:cWhere% AND
					RH3.%NotDel% AND 
					SRA.%NotDel%
			GROUP BY RH3.RH3_TIPO

		ENDSQL
	Else

		cWhere += "%"

		BEGINSQL ALIAS cQuery

			SELECT RH3.RH3_TIPO, COUNT(*) QTD
					FROM %table:RH3% RH3
			WHERE  RH3.RH3_STATUS IN ('1','4') AND
					%Exp:cWhere% 			   AND
					RH3.%NotDel%
			GROUP  BY RH3.RH3_TIPO

		ENDSQL

	EndIf

	While (cQuery)->(!Eof())
		nQtdTotal   += (cQuery)->QTD

		oSubTotals := &cJsonObj
		oSubTotals["type"]  := GetENUMDecode( (cQuery)->RH3_TIPO )
		oSubTotals["total"] := (cQuery)->QTD
		Aadd(aData,oSubTotals)

		(cQuery)->(DbSkip())
	EndDo
	(cQuery)->( DBCloseArea() )

	If cAction == "count"
		//Tratativa para acumular os totais.
		If !Empty( cSubDepts )

			BEGINSQL ALIAS cQryCount

				SELECT RH3.RH3_FILINI, RH3.RH3_MATINI, RH3.RH3_FILAPR, RH3.RH3_MATAPR
						FROM %table:RH3% RH3
				INNER JOIN %table:SRA% SRA
						ON RH3_FILIAL = RA_FILIAL AND RH3_MAT = RA_MAT
				WHERE  RH3.RH3_STATUS = '1' AND
						%Exp:cWhere% AND
						RH3.%NotDel% AND 
						SRA.%NotDel%
			ENDSQL

		Else

			BEGINSQL ALIAS cQryCount

				SELECT RH3.RH3_FILINI, RH3.RH3_MATINI, RH3.RH3_FILAPR, RH3.RH3_MATAPR
						FROM %table:RH3% RH3
				WHERE  RH3.RH3_STATUS IN ('1','4') AND
						%Exp:cWhere% 			   AND
						RH3.%NotDel%
			ENDSQL

		EndIf

		While (cQryCount)->(!Eof())

			//requisi��es em aberto que foram iniciadas pelo usu�rio logado
			If (cQryCount)->RH3_FILINI == cBranchVld .And. (cQryCount)->RH3_MATINI == cMatSRA
				nTotReq += 1
			EndIf

			//requisi��es em aberto que est�o no passo do usu�rio logado para que seja tomado uma a��o
			If (cQryCount)->RH3_FILAPR == cBranchVld .And. (cQryCount)->RH3_MATAPR == cMatSRA
				nTotPend += 1
			EndIf

			(cQryCount)->(DbSkip())
		EndDo
		(cQryCount)->( DBCloseArea() )
	ENDIF

EndIf

If cAction == "notify"
	If nQtdTotal > 0
		oItemData["subtotals"] 	:= aData
		oItemData["total"] 		:= nQtdTotal
	EndIf
else
	oItemData := aData
EndIf
Return(Nil)


Static Function getFields(oFields,cTypeRequest,cCodRH4,aSeqClock)

//Local cJsonObj 	 	:= "JsonObject():New()"
Local oProps		:= JsonObject():New() 
Local nPos			:= 0
Local cCpoRH4		:= AllTrim(RH4->RH4_CAMPO)

Default cTypeRequest := ""
Default cCodRH4 := ""
Default aSeqClock := {}

DO CASE
	CASE cTypeRequest == "B" // VACATION
		If cCpoRH4 == "R8_DATAINI"
			oFields["type"]        := EncodeUTF8("initDate")
			oFields["value"]       := formatGMT(Alltrim(RH4->RH4_VALNOV))
		ElseIf cCpoRH4 == "R8_DATAFIM"
		  	oFields["type"]        := EncodeUTF8("endDate")
		  	oFields["value"]       := formatGMT(Alltrim(RH4->RH4_VALNOV))
		ElseIf cCpoRH4 == "R8_DURACAO"
			oFields["type"]        := EncodeUTF8("totalDays")
          oFields["value"]        := Alltrim(RH4->RH4_VALNOV)
		Elseif cCpoRH4 == "TMP_DABONO"
			oFields["type"]        := EncodeUTF8("vacationBonus")
          oFields["value"]        := Alltrim(RH4->RH4_VALNOV)
       Else
          oFields["type"]         := ""
          oFields["value"]        := ""
       EndIf

	CASE cTypeRequest == "Z" // CLOCKING
		If cCpoRH4 == "P8_DATA"
			oFields["type"] := EncodeUTF8("initDate")
			oFields["value"] := formatGMT(Alltrim(RH4->RH4_VALNOV))
		ElseIf cCpoRH4 == "P8_HORA"
			oFields["type"] := EncodeUTF8("initHour")
			oFields["value"] := HourToMs(strZero( TimeToFloat(Alltrim(RH4->RH4_VALNOV)), 5, 2)) //TimeToFloat fun��o do RHLIBHRS.
		//O campo TMP_TEXT s� existe no MeuRH ent�o considera a �ltima atribui��o caso exista os dois.
		//Dessa forma manter� a compatibilidade com as solicita��es originadas no Portal GCH
		ElseIf cCpoRH4 $ "P8_MOTIVRG|TMP_TEXT"
		  	oFields["type"] := EncodeUTF8("justify")
			oFields["value"] := EncodeUTF8(Alltrim(RH4->RH4_VALNOV))

			//Adiciona o properties
			oProps["field"]     := "justify"
			oProps["visible"]   := .T.
			oProps["editable"]  := .F.
			oProps["required"]  := .F.

			oFields["props"]    := oProps

		ElseIf cCpoRH4 == "TMP_NOME"
			//Define o sentido - Entrada/Saida
			nPos := aScan(aSeqClock, {|x| x[4] == cCodRH4} )
			If nPos > 0
				oFields["type"] := EncodeUTF8("direction")
				oFields["value"] := aSeqClock[nPos,3]
			EndIf
		EndIf

	CASE cTypeRequest == "8"
		If cCpoRH4 == "RF0_DTPREI"
			oFields["type"] := EncodeUTF8("initDate")
			oFields["value"] := formatGMT(Alltrim(RH4->RH4_VALNOV))

		ElseIf cCpoRH4 == "RF0_DTPREF"
		  	oFields["type"] := EncodeUTF8("endDate")
			oFields["value"] := formatGMT(Alltrim(RH4->RH4_VALNOV))
		ElseIf cCpoRH4 == "RF0_HORINI"
			oFields["type"] := EncodeUTF8("initHour")
			oFields["value"] := HourToMs(Alltrim(RH4->RH4_VALNOV))

			//Adiciona o properties
			oProps["field"]     := "initHour"
			oProps["visible"]   := .T.
			oProps["editable"]  := .F.
			oProps["required"]  := .F.

			oFields["props"]    := oProps
			
		ElseIf cCpoRH4 == "RF0_HORFIM"
			oFields["type"] := EncodeUTF8("endHour")
			oFields["value"] := HourToMs(Alltrim(RH4->RH4_VALNOV))

			//Adiciona o properties
			oProps["field"]     := "endHour"
			oProps["visible"]   := .T.
			oProps["editable"]  := .F.
			oProps["required"]  := .F.

			oFields["props"]    := oProps
		ElseIf cCpoRH4 == "TMP_ABOND"
			oFields["type"] := EncodeUTF8("reason")
			oFields["value"] := EncodeUTF8(Alltrim(RH4->RH4_VALNOV))
		EndIf

ENDCASE

Return(Nil)


Static Function getInitFields(cTypeRequest)
Local aInitFields := {}

Default cTypeRequest := ""

//Inicializa o campo e tambem o properties a partir do terceiro elemento:
//Aadd( aInitFields, { /*Type*/, /*Value*/, /*lProperties*/, /*lvisible*/, /*leditable*/, /*lrequired*/ } )

If cTypeRequest $ "B/8" // VACATION/allowance
	Aadd( aInitFields, {"totalHours", "", .F., .F., .F., .F. } )
    Aadd( aInitFields, {"status"    , "", .F., .F., .F., .F. } )
    Aadd( aInitFields, {"direction" , "", .F., .F., .F., .F. } )
    Aadd( aInitFields, {"other"     , "", .F., .F., .F., .F. } )

    If cTypeRequest == "B"
    	Aadd( aInitFields, {"reason", "", .F., .F., .F., .F. } )
    EndIf
EndIf

If cTypeRequest == "Z"
	Aadd( aInitFields, {"reason"	, "", .T., .F., .F., .F. } )
EndIf

Return(aInitFields)

/*/{Protheus.doc}GetNotifys()
- Efetua a contagem das notifica��es do usu�rio logado.

@author:	Matheus Bizutti
/*/
Function GetNotifys(cMatSRA,cJsonObj,oItemData,cBranchVld)

Local nI			:= 0
Local nQtd			:= 0
Local cType 		:= "'B','8'" //"'8','B'"
Local aData		:= {}
Local aEnum		:= {}
Local aRequests	:= {}
Local oSubTotals	:= Nil

Default cJsonObj	:= "JsonObject():New()"
Default oItemData	:= &cJsonObj
Default cMatSRA 	:= ""
Default cBranchVld	:= FwCodFil()

oSubTotals := &cJsonObj
Aadd(aEnum,"vacation")
Aadd(aEnum,"allowance")
Aadd(aEnum,"clocking")

aRequests := fGetAllReq(cBranchVld, cMatSRA, cType, 0, "", "", .F., "", .T., .F., .F., .T.)

For nI := 1 To Len(aRequests)

	If Alltrim(aRequests[nI]:Registration) == Alltrim(cMatSRA) .Or. Alltrim(cMatSRA) != Alltrim(aRequests[nI]:ApproverRegistration)
		Loop
	EndIf

	If aRequests[nI]:Status:Code == "1"
		nQtd += 1
	EndIf

Next nI

oSubTotals["type"]  := aEnum[1] // @FIXME
oSubTotals["total"] := nQtd

Aadd(aData,oSubTotals)

oItemData["subtotals"] 	:= aData
oItemData["total"] 		:= nQtd

Return(Nil)


/*/{Protheus.doc} fGetSeqclock
Retorna uma matriz com sentido (entrada/saida) das marcacoes incluidas pelo espelho de acordo com a data/hora
@author:	Marcelo Silveira
@since:		16/04/2019
@param:		cFilSRA - Filial do aprovador;
			cMatSRA - Matricula do aprovador;
@return:	aSentido - Array com as datas/horas classificadas como entrada/saida
/*/
Function fGetSeqclock(cFilSRA, cMatSRA, cMatAprov)

Local cQuery	:= GetNextAlias()
Local nX  		:= 0
Local nPos  	:= 0
Local nCount	:= 0
Local cCod		:= ""
Local cLastDt	:= ""
Local cWhere	:= ""
Local cExit		:= EncodeUTF8(STR0008) //"Sa�da"
Local cEntry	:= EncodeUTF8(STR0009) //"Entrada"
Local aSentido 	:= {}

cWhere := "%"
cWhere += " RH3.RH3_FILAPR IN ('" + cFilSRA + "') AND "
cWhere += " RH3.RH3_MATAPR IN (" + cMatSRA + ") "
cWhere += "%"

BEGINSQL ALIAS cQuery
	SELECT RH3_CODIGO, RH4.RH4_CAMPO, RH4_VALNOV
	FROM %table:RH3% RH3
	INNER JOIN %table:RH4% RH4 ON
		RH4_FILIAL = RH3_FILIAL AND
		RH4_CODIGO = RH3_CODIGO
	WHERE
		RH3.RH3_MAT <> %Exp:cMatAprov%  AND
		RH3.RH3_STATUS ='1' AND RH3.RH3_TIPO ='Z' AND
		RH4_CAMPO IN ('P8_DATA','P8_HORA','TMP_DIRECT') AND
		%Exp:cWhere% AND
        RH3.%notDel% AND RH4.%notDel%
    ORDER BY RH3_CODIGO, RH3_FILIAL, RH3_MAT
ENDSQL

//Inclui na matriz os marcacoes incluidas para aprovacao
While (cQuery)->(!Eof())

	If( AllTrim((cQuery)->RH4_CAMPO) == 'P8_DATA')
		nPos := 1
	Elseif ( AllTrim((cQuery)->RH4_CAMPO) == 'P8_HORA')
		nPos := 2
	Elseif ( AllTrim((cQuery)->RH4_CAMPO) == 'TMP_DIRECT')
		nPos := 5
	Endif						

	If !(cCod == (cQuery)->RH3_CODIGO)
		aAdd( aSentido, {Ctod("//"), "", "", (cQuery)->RH3_CODIGO, ""} )
		cCod := (cQuery)->RH3_CODIGO
	EndIf

	aSentido[Len(aSentido),nPos] := AllTrim(RH4_VALNOV)

	(cQuery)->(DbSkip())

Enddo

If Len(aSentido) > 0
	//Ordena as marcacoes incluidas por data e hora
	aSort(aSentido,,,{|x,y| DtoS(cTod(x[1]))+StrTran(StrZero( Val(x[2]),5,2),".", ":") < DtoS(cToD(y[1]))+StrTran(StrZero( Val(y[2]),5,2),".", ":") })

	//Se a dire��o n�o existir na solicita��o classifica cada registro como entrada/saida de acordo com a ordem dos horarios
	For nX := 1 to Len(aSentido)
		If !Empty(aSentido[nX, 05])
			aSentido[nX,3] := EncodeUTF8(aSentido[nX, 05])
		Else
			nCount		:= If( cLastDt == aSentido[nX, 01], nCount, 0 )
			nCount++

			aSentido[nX,3] 	:= Iif(nCount % 2 == 0 , cExit, cEntry)
			cLastDt 	:= aSentido[nX, 01]
		EndIf
	Next nX
EndIf

(cQuery)->(DbCloseArea())

Return(aSentido)


/*/{Protheus.doc} fRequests
Retorna um array com as requisi��es realizadas
@author:	Henrique Ferreira
@since:		09/12/2019
@param:		cFilSRA  - Filial do aprovador;
			cMatSRA	 - Matricula do aprovador;
			cWhere	 - Array com as datas/horas classificadas como entrada/saida
			cTipoReq - Tipo de requisi��o ser filtrada na query
			cStatus  - Status a ser filtrado na query
@return: Array com as requisi��es filtradas de acordo com os par�metros.
/*/
Function fRequests(cFilSRA, cMatSRA, cWhere, nCount, nIniCount, nFimCount)
Local cJsonObj 	:= "JsonObject():New()"
Local oFields   := &cJsonObj
Local cQryRH3   := GetNextAlias()
Local aData     := {}
Local aReturn	:= {}
Local nX		:= 0

DEFAULT nCount    := 0
DEFAULT nIniCount := 1
DEFAULT nFimCount := 6

BeginSql alias cQryRH3
	SELECT SRA.RA_NOMECMP, SRA.RA_NOME, RH3.RH3_CODIGO, RH3.RH3_FILIAL, RH3.RH3_MAT, RH3.RH3_TIPO, RH3.RH3_DTSOLI, RH3.RH3_FILAPR, RH3.RH3_MATAPR, RH3.RH3_STATUS
	FROM  %table:RH3% RH3 
	INNER JOIN 
		  %table:SRA% SRA
		  ON RH3.RH3_FILIAL = SRA.RA_FILIAL AND 
		     RH3.RH3_MAT    = SRA.RA_MAT
	WHERE  RH3.%notDel% AND 
		   SRA.%notDel% AND
		   %Exp:cWhere% 
EndSql

While (cQryRH3)->(!Eof())
	aAdd(aData, { ;
					(cQryRH3)->RH3_MAT, 									  										;		// MATR�CULA DO FUNCION�RIO QUE SOFREU A A��O
					AllTrim( If ( !Empty((cQryRH3)->RA_NOMECMP), (cQryRH3)->RA_NOMECMP, (cQryRH3)->RA_NOME) ), 	  	; 		// NOME DO FUNCION�RIO QUE SOFREU A A��O
					(cQryRH3)->RH3_FILIAL + "|" + (cQryRH3)->RH3_CODIGO, 								  			;		// ID DA SOLICITA��O (FILIAL + CODIGO DA SOLICITA��O)
					(cQryRH3)->RH3_CODIGO,  												 			  			;		// CODIGO DA SOLICITA��O.
					GetENUMDecode( (cQryRH3)->RH3_TIPO ),	 														;		// TIPO DE SOLICITA��O.
					(cQryRH3)->RH3_DTSOLI,   															  			;		// DATA DA SOLICIA��O.
					AllTrim(Posicione( "SRA",1,(cQryRH3)->RH3_FILAPR+(cQryRH3)->RH3_MATAPR, "RA_NOME" ) ),			;	    // NOME DO RESPONS�VEL APROVADOR NA ETAPA.
					getStatusWKF( (cQryRH3)->RH3_STATUS ),   														;		// STATUS DA SOLICITA��O.
					fStatusLabel( (cQryRH3)->RH3_STATUS ),	 												  		;		// DESCRI��O DO STATUS DA SOLICITA��O.
					(cQryRH3)->RH3_FILAPR + "|" + (cQryRH3)->RH3_MATAPR									  			;		// ID DO RESPONS�VEL DO APROVADOR.
		})
	(cQryRH3)->(DbSkip())
EndDo
(cQryRH3)->( DBCloseArea() )

If Len(aData) > 0
	For nX := 1 to Len(aData)
		nCount++
		If ( nCount >= nIniCount .And. nCount <= nFimCount )
			oFields   := &cJsonObj
			oFields["employeeId"] 		:= aData[nX,1]
			oFields["employeeName"] 	:= aData[nX,2]
			oFields["id"] 				:= aData[nX,3]
			oFields["requestId"] 		:= aData[nX,4]
			oFields["requestType"] 		:= aData[nX,5]
			oFields["startDate"] 		:= SubStr(aData[nX,6],1,4) + "-" + SubStr(aData[nX,6],5,2) + "-" + SubStr(aData[nX,6],7,2) + "T" + "12:00:00" + "Z"
			oFields["responsableName"] 	:= aData[nX,7]
			oFields["status"] 			:= aData[nX,8]
			oFields["statusLabel"] 		:= aData[nX,9]
			oFields["responsableId"]  	:= aData[nX,10]

			aAdd(aReturn, oFields)
		EndIf
	Next nX
EndIf

Return(aReturn)

/*/{Protheus.doc} fAvalAprRepr
Retorna um array com as requisi��es realizadas
@author:	Marcelo Faria
@since:		08/06/2019
@param:		cBody  - dados da requisi��o
			cToken - dados do usu�rio de login
@return:	string com ocorr�ncias do processo de avalia��o da solicita��o
/*/
Function fAvalAprRepr(cToken,cBody)
Local cJsonObj 	 	:= "JsonObject():New()"
Local oItemDetail   := &cJsonObj
Local oRequest		:= WSClassNew("TRequest")
Local cLenFilial    := FWSizeFilial()
Local cLenMat       := TamSX3("RA_MAT")[1]
Local cLenRH3Cod    := TamSX3("RH3_CODIGO")[1]
Local cOrgCFG		:= SuperGetMv("MV_ORGCFG", NIL, "0")
Local aRequests		:= {}
Local aGetStruct    := {}
Local aDataLogin	:= {}
Local aSubstitute	:= {}
Local cSubMat		:= ""
Local cSubBranch 	:= ""
Local cFilToken		:= ""
Local cMatToken		:= ""
Local cRH3Fil       := ""
Local cRH3Mat       := ""
Local cRH3Cod       := ""
Local cApprover     := ""
Local cEmpApr       := ""
Local cUsrCurrent   := ""
Local cFilApr       := ""
Local cJustify		:= ""
Local cMotive		:= ""
Local cMsg			:= ""
Local nX            := 0
Local nSupLevel     := 0
Local lAprove		:= .T.
Local lSubstitute   := .F.

default cToken		:= ""
default cBody		:= ""

	aDataLogin  := GetDataLogin(cToken)
	If Len(aDataLogin) > 0
		cMatToken	:= aDataLogin[1]
		cUsrCurrent	:= aDataLogin[1]
		cFilToken	:= aDataLogin[5]
	EndIf

	oItemDetail:FromJson(cBody)
	lAprove     := If(oItemDetail:hasProperty("approved"), oItemDetail["approved"], .F.)
	cJustify    := If(oItemDetail:hasProperty("justify"), AllTrim(oItemDetail["justify"]), "")
	aRequests   := If(oItemDetail:hasProperty("requisitions"), oItemDetail["requisitions"], "")

	If valtype(lAprove) == "L" .And. !Empty(aRequests) .and. !Empty(cToken)
		oRequest:Status	:= WSClassNew("TRequestStatus")

		//Verifica se o funcionario esta substituindo o seu superior
		aSubstitute := fGetSupNotify( cFilToken, cMatToken, .F. )
		If Len(aSubstitute) > 0
			For nX := 1 To Len(aSubstitute)
				cSubMat	+= aSubstitute[nX, 2]
				cSubBranch += aSubstitute[nX, 1]
				lSubstitute := .T.
			Next nX
		Else
			cSubMat	:= cMatToken
			cSubBranch := cFilToken
		EndIf

		//aprova��o e reprova��o em lote
		Begin Transaction
		
			For nX := 1 To Len( aRequests )
				cRH3Fil 	:= Substr(oItemDetail["requisitions"][nX]["id"],1,cLenFilial)
				cRH3Mat		:= Substr(oItemDetail["requisitions"][nX]["id"],cLenFilial+1,cLenMat)
				cRH3Cod 	:= Substr(oItemDetail["requisitions"][nX]["id"],cLenfilial+cLenMat+1,cLenRH3Cod)

				DbSelectArea("RH3")
				RH3->( dbSetOrder(1) )
			
				If RH3->( dbSeek(xFilial("RH3", cRH3Fil) + cRH3Cod ) )
					
					//Dados da estrutura hierarquica
					cMRrhKeyTree := fMHRKeyTree(cSubBranch, cSubMat)
					aGetStruct   := APIGetStructure("", cOrgCFG, RH3->RH3_VISAO, cSubBranch, cSubMat, , , , RH3->RH3_TIPO, cSubBranch, cSubMat, , , , , .T., {cEmpAnt})
		
					If len(aGetStruct) > 0 .and. (valtype(aGetStruct[1]:ListOfEmployee[1]:LevelSup) == "N") .and. (aGetStruct[1]:ListOfEmployee[1]:LevelSup != 99)
						cEmpApr   := aGetStruct[1]:ListOfEmployee[1]:SupEmpresa
						cFilApr   := aGetStruct[1]:ListOfEmployee[1]:SupFilial
						nSupLevel := aGetStruct[1]:ListOfEmployee[1]:LevelSup
						cApprover := aGetStruct[1]:ListOfEmployee[1]:SupRegistration
					Else
						nSupLevel := aGetStruct[1]:ListOfEmployee[1]:LevelSup
					EndIf

					//Inclui justificativa padrao caso nao tenha sido informada
					If Empty( cJustify )
						cMotive := If( lAprove, STR0016, STR0017 ) //"Aprovado via App MeuRH em:"#"Reprovado via App MeuRH em:"
						cMotive := Alltrim( EncodeUTF8( cMotive ) + Space(1) + dToC(date()) + Space(1) + Time() )
					Else
						cMotive := EncodeUTF8(cJustify)
					EndIf
							
					//Verifica se o aprovador eh o usuario corrente. Se positivo o APRROVER deve ficar "''" 
					cApprover := If( cApprover == cUsrCurrent, "", cApprover )				
			
					oRequest:Branch 				:= cRH3Fil
					oRequest:Registration			:= cRH3Mat
					oRequest:Code 					:= cRH3Cod
					oRequest:Observation 			:= cMotive
			
					oRequest:ApproverBranch			:= cFilApr
					oRequest:ApproverRegistration 	:= cApprover
					oRequest:EmpresaAPR				:= cEmpApr
					oRequest:ApproverLevel			:= nSupLevel
			
					//Guarda os dados da aprovacao feita pelo substituto para geracao do historico
					If lSubstitute
						oRequest:ApproverSubBranch		:= cFilToken
						oRequest:ApproverSubRegistration:= cMatToken
					EndIf
			
					If lAprove
						ApproveRequest(oRequest)
					Else
						ReproveRequest(oRequest)
					EndIf
			
				Else
					If Empty(cMsg)
						cMsg := EncodeUTF8(STR0020) + cRH3Cod //"N�o foi poss�vel realizar opera��o para essas solicita��es: " 
					else
						cMsg += "," +cRH3Cod 
					EndIf		
				EndIf
			
			Next nX
		
		End Transaction

	Else
		cMsg := EncodeUTF8(STR0018)  //"N�o foi poss�vel realizar essa opera��o. Verifique os dados e o status das requisi��es selecionadas." 
	EndIF

Return cMsg
