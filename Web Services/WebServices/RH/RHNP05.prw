#INCLUDE "TOTVS.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "FILEIO.ch"
#INCLUDE "AP5MAIL.ch"

#INCLUDE "RHNP.CH"

STATIC   cMeurhLog := GetConfig("RESTCONFIG","meurhLog", "")

Function RHNP05()
Return .T.


//**************************************************
//"Autentica��es"
//**************************************************
WSRESTFUL Auth DESCRIPTION STR0041

WSDATA userId	As String Optional
WSDATA password	As String Optional

WSMETHOD POST DESCRIPTION "POST"  WSSYNTAX  "/auth/login | /auth/logout'

WSMETHOD GET getLogged ;
 DESCRIPTION STR0049 ; //"Valida token JWT do login" 
 WSSYNTAX "/auth/isLogged" ;
 PATH     "/auth/isLogged" ;
 PRODUCES "application/json;charset=utf-8"

WSMETHOD POST getResetLink ;
 DESCRIPTION STR0050 ; //"Envia link por email para reset senha" 
 WSSYNTAX "/renewPassword" ;
 PATH     "/renewPassword" ;
 PRODUCES "application/json;charset=utf-8"

WSMETHOD PUT editPassword ;
 DESCRIPTION STR0051 ; //"Atualiza a senha do usuario." 
 WSSYNTAX "/resetPassword" ;
 PATH     "/resetPassword" ;
 PRODUCES "application/json;charset=utf-8"

 WSMETHOD GET isFirstLogin ;
 DESCRIPTION STR0079; // Trocar a senha no primeiro login
 WSSYNTAX   "auth/isFirstLogin/{employeeId} || auth/isFirstLogin" ;
 
 WSMETHOD POST ChangePassWithLastPass ;
 DESCRIPTION STR0092; // Realiza troca de senha do usu�rio.
 WSSYNTAX   "auth/ChangePassWithLastPass"   ;
 PATH       "ChangePassWithLastPass"   ;
 PRODUCES "application/json;charset=utf-8" ;
 TTALK "v2"

END WSRESTFUL


//************************************************************
// "Servi�os de Contexto"
//************************************************************
WSRESTFUL Setting DESCRIPTION STR0080
WSDATA employeeId As String Optional
WSDATA WsNull     As String Optional
WSDATA type       As String Optional

WSMETHOD GET getAllContexts ;
 DESCRIPTION STR0081 ; //"Retorna os contextos do usu�rio para sele��o."
 WSSYNTAX "/setting/contexts" ;
 PATH "/contexts" ;
 PRODUCES 'application/json;charset=utf-8'

WSMETHOD GET getContext ;
 DESCRIPTION STR0082 ; //"Retorna o contexto corrente do usu�rio."
 WSSYNTAX "/setting/context" ;
 PATH "/context" ;
 PRODUCES 'application/json;charset=utf-8'

WSMETHOD PUT setContext ;
 DESCRIPTION STR0083 ; //"Atualiza o contexto selecionado."
 WSSYNTAX "/setting/context" ;
 PATH "/context" ;
 PRODUCES 'application/json;charset=utf-8'

WSMETHOD GET getPermissions ;
 DESCRIPTION STR0084 ; //"Verifica libera��o de acesso as funcionalidades."
 WSSYNTAX "/setting/permissions/{employeeId}" ;
 PATH "/permissions/{employeeId}" ;
 PRODUCES 'application/json;charset=utf-8'

WSMETHOD GET getFieldProperties ;
 DESCRIPTION STR0085 ; //"Verifica os elementos que podem ser visualizados e/ou editados na p�gina."
 WSSYNTAX "/setting/fieldProperties/{pageFilter}" ;
 PATH "fieldProperties/{pageFilter}" ;
 PRODUCES 'application/json;charset=utf-8'

END WSRESTFUL


//***************************************
//M�todos REST AUTH
//***************************************

WSMETHOD POST WSSERVICE Auth

Local cJsonObj 	 	:= "JsonObject():New()"
Local oItemData	 	:= &cJsonObj
Local oItem		 	:= &cJsonObj
Local aMessages		:= {}
Local cBody			:= ""
Local cJson			:= ""
Local lRet			:= .T.
Local oMsgReturn	:= &cJsonObj
Local lSetAccess	:= .F.
Local cRedirect		:= ""
Local aValueCookie  := {}
Local cUser			:= ""
Local cPassword		:= ""
Local nLenParms	 	:= Len(::aURLParms)
Local aRet			:= {}
Local aDataEnv      := {}
Local aDtHr         := {}
Local cMatValid		:= ""
Local cBranchVld	:= ""
Local cToken		:= ""
Local cKey			:= ""
Local cRestPort		:= ""
Local cUserID       := ""
Local cRestFault	:= ""
Local lIsWeb		:= .F.
Local nPos		    := 0
Local cString		:= ""
Local cPPAccess     := GetMv("MV_ACESSPP",,"")

DEFAULT ::userId 		:= ""
DEFAULT ::password	:= ""

// - Por por padr�o todo objeto tem
// - data: contendo a estrutura do JSON
// - messages: para determinados avisos
// - length: informativo sobre o tamanho.

::SetHeader('Access-Control-Allow-Credentials' , "true")


If nLenParms >= 1 .And. Lower(::aURLParms[1]) == "login"

   If cMeurhLog != "0"
      aDtHr := FwTimeUF("SP",,.T.)

      conout("")
      conout(EncodeUTF8(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"    ))
      conout(EncodeUTF8(">>> " +GetVersao()                                    ))
      conout(EncodeUTF8(">>> " +GetBuild()                                     ))
      conout(EncodeUTF8(">>> build dba: " +TCGetBuild() +" - " +TCSrvType()    ))
      
      aDataEnv := GetAPOInfo("aplib050.prw")
      conout(EncodeUTF8(">>> build lib: " +DTos(aDataEnv[4])                   ))

      aDataEnv := GetAPOInfo("rhnp05.prw")
      conout(EncodeUTF8(">>> build mrh: " +DTos(aDataEnv[4])                   ))

      conout(EncodeUTF8(">>> ENV/RPO: " +GetEnvServer() +"/" +GetRPORelease()  ))
      
      conout(EncodeUTF8(">>> "                                                 ))
      conout(EncodeUTF8(">>> MeuRH Autentication"                              ))
   EndIf   

	cBody := Encode64(::GetContent())

	If !Empty(cBody)
	
		// ----------------------------------------------------------------------------------------------
		// - O Front-End envia o corpo da requisi��o em
		// - FORMDATA, EX: "user=user&password=password&redirectUrl=http://localhost:8084/T1/rest"
		// - E assim decodificamos essa URL para devolver os valores separados;
		// - USER=USER; PASSWORD=PASSWORD; REDIRECTURL=http://localhost:8084/T1/?restPort=9103
		// ----------------------------------------------------------------------------------------------
		aValueCookie := DecodeURL(Decode64(cBody))

		If Len(aValueCookie) >= 3
			cUser 	  := aValueCookie[1]
			cPassword := aValueCookie[2]
			cRedirect := aValueCookie[3]

           // -----------------------------------------------------
           // - Persiste o acesso do usu�rio - PRTLOGIN WSPORTAL01|
           // -----------------------------------------------------
           UnifiedLoginRH(@lRet,cUser,cPassword,"2",cPPAccess,.T.,,,@cRestFault)
        Else
           lRet       := .F.
           cRestFault := EncodeUTF8(STR0042) //"user/password/redirect n�o localizados na requisi��o" 
		EndIf

		
		If lRet
			// ---------------------------------------
			// - Ap�s validar o usu�rio e senha;
			// - Posiciona na RD0, captura as 
			// - Matr�culas que o usu�rio tem acesso;
			// - E provis�riamente loga com a Primeira
			// ---------------------------------------
			lSetAccess := GetAccessEmployee(cUser, @aRet, lRet, .T.)
			
			If lSetAccess
				If Len(aRet) >= 1
					nPos := Ascan(aRet,{|x| !(x[10] $ "30/31")}) 
					If nPos > 0
						cMatValid  := aRet[nPos][1]
						cBranchVld := aRet[nPos][3]
					Else
						cMatValid  := aRet[1][1]
						cBranchVld := aRet[1][3]
					EndIf 
				EndIf

				cKey   := cMatValid+"|"+cUser+"|"+RD0->RD0_CODIGO+"|"+DtoS(dDataBase)+"|"+cBranchVld
				cUserID := GetConfig("RESTCONFIG","userId", "")
				
				If Empty(cUserID) //nova pesquisa em virtude da atualiza��o do appWebWizard
					cUserID := GetConfig("HTTPREST","userId", "")
				EndIf

				//Busca usuario do portal
				If Empty(cUserID)  
					dbSelectArea("AI3")
					AI3->(dbSetOrder(1))
					If AI3->(dbSeek(xFilial("AI3")+RD0->RD0_PORTAL))
						cUserID := UsrRetName(AI3->AI3_USRSIS)
					EndIf
				EndIf

				//Gera token
				If !Empty(cUserID)
					//cToken := FwJWT2Bear(cUserID,{"payments/","payment/","data/","team/", "request/", "timesheet/", "/team/", "setting/"},Date(),Seconds() + Val(GetConfig("RESTCONFIG","RefreshTokenTimeout", 600)),Nil,Nil,{ {"key",cKey} })

					//Retirada a passagem de data e segundos para a funcao FwJWT2Bear por orientacao do Framework - 01/2019
					cToken := FwJWT2Bear(cUserID,{"payments/","payment/","data/","team/", "request/", "timesheet/", "/team/", "setting/"},Nil,Nil,Nil,Nil,{ {"key",cKey} })
				Else
					If cMeurhLog != "0"
						conout(EncodeUTF8(STR0046 + cUser +' - ' + STR0068 )) //">>> usuario nao autenticado: "##"Este usuario nao esta vinculado a um usuario interno do Protheus."
					EndIF

					cRedirect += "auth-error.html"
					oMsgReturn["type"]   := "error"
					oMsgReturn["code"]   := "400"
					oMsgReturn["detail"] := EncodeUTF8(STR0046 + cUser + ' - ' + STR0068) //"Nenhuma matricula localizada para o usuario: "##"Este usuario nao esta vinculado a um usuario interno do Protheus."
				EndIf
				
				cRestPort := GetConfig("RESTCONFIG","restPort", "")
              
				If Empty(cRestPort) //nova pesquisa em virtude da atualiza��o do appWebWizard
					cRestPort := GetConfig("HTTPREST","Port", "")
				EndIf  
				
				If 'restPort' $ cRedirect 
					cRedirect := StrTran( cRedirect, "?restPort", "" )
				Else
					lIsWeb	:= .T.
				EndIf
								
				::SetHeader('Set-Authorization', 'Bearer ' + cToken)
				::SetHeader('Access-Control-Expose-Headers', 'Set-Authorization')
				
				If !Empty(cToken)

					oMsgReturn["type"]   := "success"
					oMsgReturn["code"]   := "200"
					oMsgReturn["detail"] := EncodeUTF8(STR0001) //"Usu�rio autenticado"

					If cMeurhLog != "0"
						conout(EncodeUTF8(STR0043 +cUser +' - ' +cBranchVld +cMatValid)) //">>> usuario autenticado: "
					EndIf
				Else
					cRedirect += "auth-error.html"
					oMsgReturn["detail"] := EncodeUTF8(STR0047) //"Usu�rio autenticado, mas token n�o gerado"
					
					If cMeurhLog != "0"
						conout(EncodeUTF8(STR0048 +cUser +' - ' +cBranchVld +cMatValid)) //">>> token nao gerado: "
					EndIF
				EndIf				

			Else
				cRestFault := EncodeUTF8(STR0044) //"login realizado, mas matricula n�o localizada!"

				If cMeurhLog != "0"
					conout(EncodeUTF8(STR0044 +cUser))
				EndIF

				cRedirect += "auth-error.html"
				oMsgReturn["type"]   := "error"
				oMsgReturn["code"]   := "400"
				oMsgReturn["detail"] := EncodeUTF8(STR0045 +cUser) //"Nenhuma matricula localizada para o usuario: "
			EndIf
		Else
            If cMeurhLog != "0"
			    conout(EncodeUTF8(STR0046 +cUser +' - ' +cRestFault)) //">>> usuario nao autenticado: "
			EndIF   
	       		
			cRedirect += "auth-error.html"
			oMsgReturn["type"]   := "error"
			oMsgReturn["code"]   := "400"
			oMsgReturn["detail"] := EncodeUTF8(cRestFault)
		EndIf
				
		Aadd(aMessages, oMsgReturn)

		oItem["data"] 		  := oItemData
		oItem["messages"] 	  := aMessages
		oItem["length"]   	  := 1 
	
		If !lIsWeb
			cString   := Right(cRedirect, 1)
			cRedirect := Iif(cString != "/", cRedirect + "/",cRedirect)
		EndIf

   		If lIsWeb
           If "auth-error.html" $ cRedirect
              cJson := '<html><head><title>Moved</title></head><body><script type="text/javascript">window.location="' +cRedirect +'";</script></body></html>'
           Else
              cJson := '<html><head><title>Moved</title></head><body><script type="text/javascript">window.location="'+cRedirect+'?token=' + cToken + '&restPort=' + Alltrim(cRestPort) + '";</script></body></html>'
           EndIf   
	    Else
           If "auth-error.html" $ cRedirect
              cJson := cRedirect
           Else
     			cJson := cRedirect+'?token=' + cToken + '&restPort=' + Alltrim(cRestPort)
     		EndIf	
       EndIf

		::SetResponse(cJson)

       If cMeurhLog != "0"
          	conout(EncodeUTF8(">>> Data/Hora: " +dToC(date()) +Space(1) +Time() ))
          	conout(EncodeUTF8(">>> Dt/Hr tmz: " +aDtHr[1] +"/" +aDtHr[2] ))
          	conout(EncodeUTF8(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"  ))
          	conout("")
		EndIF   	
	EndIf
ElseIf nLenParms >= 1 .And. ::aURLParms[1] == "logout"

	// ---------------------------------
	// - Elimina o Bearer authorization
	// ---------------------------------
	oItemData["redirect"] := "logout.html"
	::SetHeader('Set-Authorization', '=')
	
	oMsgReturn["type"]   := "success"
	oMsgReturn["code"]   := "200"
	oMsgReturn["detail"] := EncodeUTF8(STR0002) //"Usu�rio deslogado"
	oItem["data"] 		:= oItemData
	Aadd(aMessages, oMsgReturn)
	
	oItem["messages"] 	:= aMessages
	oItem["length"]   	:= 1
	
	cJson :=  FWJsonSerialize(oItem, .F., .F., .T.)
	::SetResponse(cJson)
EndIf

Return(.T.) 


// -------------------------------------------------------------------
// - GET RESPONS�VEL POR VERIFICAR SE O TOKEN EST� VALIDO.
// -------------------------------------------------------------------
WSMETHOD GET getLogged WSREST Auth

Local cJson      := ""
Local cToken     := ""
Local cMatSRA    := ""
Local cBranchVld := ""
Local cLogin     := ""
Local aDataLogin := {}

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken     := Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
   cMatSRA      := aDataLogin[1]
   cBranchVld   := aDataLogin[5]
   cLogin       := aDataLogin[2]
EndIf

If Empty(cBranchVld) .Or. Empty(cMatSRA) .Or. Empty(cLogin)
   cJson   := '{ '               ;
            + '"isLogged":false' ;
            + ' }'
Else
   cJson   := '{ '               ;
            + '"isLogged":true'  ;
            + ' }'
EndIf

::SetResponse(cJson)

Return(.T.)


// -------------------------------------------------------------------
// - POST RESPONS�VEL POR ENVIAR EMAIL PARA O RESET DA SENHA.
// -------------------------------------------------------------------
WSMETHOD POST getResetLink WSREST Auth

Local cQryRD0       := GetNextAlias()
Local cJsonObj      := "JsonObject():New()"
Local oItemDetail   := &cJsonObj
Local oItemData     := &cJsonObj
Local oItem         := &cJsonObj
Local oMsgReturn    := &cJsonObj
Local cBody         := ::GetContent()
Local cRestFault    := ""
Local cJson         := ""
Local aValueBody    := {}
Local aMessages     := {}
Local cUrl          := ""
Local cUrlImg       := ""
Local cTitEmail     := ""
Local cMsgEmail     := ""
Local lRet          := .T.
local cRetCrypto    := ""
local cKey          := ""

Local nHandle
Local nX            := 0
Local nErro         := 0
Local cRoot         := ""
Local cPath         := ""
Local cHtmlTemplate := ""
Local cHtmlMsg      := ""
Local aHtmlMsg      := {}
Local cArquivo      := "rstpwd_ptbr.html"
Local cLines        := ""
Local nLines        := ""
Local lRobot        := .F.

    #ifdef SPANISH
       cArquivo     := "rstpwd_es.html"
    #else
	    #ifdef ENGLISH
           cArquivo := "rstpwd_en.html"
        #else
           cArquivo := "rstpwd_ptbr.html"
    	#endif
    #endif

    ::SetContentType("application/json")
    ::SetHeader('Access-Control-Allow-Credentials' , "true")

    If Empty(cBody)
       lRet       := .F.
       cRestFault := EncodeUTF8(STR0054) //"body n�o localizado na requisi��o" 
    EndIf
     
    aValueBody := DecodeURL(cBody)
    //varinfo("aValueBody: ",aValueBody)
    //varinfo("URL base recebido: ",aValueBody[2])
    //varinfo("User recebido    : ",aValueBody[1])
    //varinfo("Email recebido   : ",aValueBody[3])
    //varinfo("Execu��o do Robo : ",aValueBody[4])

   lRobot := ( aValueBody[4] == "1" )
    If lRet .and. ( Len(aValueBody) != 4 .or. empty(aValueBody[3]) ) 
       lRet       := .F.
       cRestFault := EncodeUTF8(STR0053) //"user/email n�o localizados na requisi��o" 
    EndIf

    If lRet .and. !RD0->(ColumnPos("RD0_RSTPWD")) > 0
       lRet       := .F.
       cRestFault := EncodeUTF8(STR0063) //"Campo RSTPWD n�o localizado na tabela RD0" 

       If cMeurhLog != "0"
          conout(EncodeUTF8(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"     ))
          conout(EncodeUTF8(">>> MeuRH Reset Password"                                 ))
          conout(EncodeUTF8(">>> " +FwCutOff(STR0065,.T.) +": " +FwCutOff(STR0063,.T.) )) //"aviso"
          conout(EncodeUTF8(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"     ))
       EndIf
    EndIf

    // Somente busca configura��es de arquivo se n�o for execu��o do rob�.
    // Para execu��o do rob�, n�o importa o arquivo, mas sim se os campos est�o devidamente preenchidos.
    If !lRobot .And. lRet
       //Busca PATH de template e-mail HTML utilizando HTTP
       cHTTP     := alltrim(StrTran( aValueBody[2] , "http://", ""))  
	    cPath     := GetConfig(cHTTP,"path", "")
       If empty(cPath) 
          If  RIGHT(cHTTP, 1) = "/"
              //retira a �ltima barra e procura novamente no INI
              cHTTP := SUBSTR( cHTTP, 1, len(cHTTP)-1 )  
              cPath := GetConfig(cHTTP,"path", "")
          EndIf

          If  empty(cPath) 
              //Busca PATH de template e-mail HTML utilizando HTTPs
              cHTTP     := alltrim(StrTran( aValueBody[2] , "https://", ""))  
              cPath     := GetConfig(cHTTP,"path", "")
              If empty(cPath) 
                 If  RIGHT(cHTTP, 1) = "/"
                    //retira a �ltima barra e procura novamente no INI
                    cHTTP := SUBSTR( cHTTP, 1, len(cHTTP)-1 )  
                    cPath := GetConfig(cHTTP,"path", "")
                 EndIf
              EndIf    
          EndIf  
       EndIf    

       If empty(cPath) 
          lRet := .F.
          conout(EncodeUTF8(">>> MeuRH Reset Password - Path n�o localizado: ") +aValueBody[2] )
       EndIf
    EndIf

    // Somente busca configura��es de arquivo se n�o for execu��o do rob�.
    // Para execu��o do rob�, n�o importa o arquivo, mas sim se os campos est�o devidamente preenchidos.
    If !lRobot .And. lRet
       If  RIGHT(cPath, 1) != "\"
           cPath := cPath + "\"
       EndIF

       cPathHTML     := UpStrTran(cPath, GetSrvProfString("ROOTPATH",""), "") 
       cHtmlTemplate := cPathHTML + cArquivo
       //varinfo("cHtmlTemplate: ",cHtmlTemplate)

       If !File(cHtmltemplate)
          lRet := .F.
          conout(EncodeUTF8(">>> MeuRH Reset Password - Template HTML e-mail n�o localizado: " +cHtmlTemplate ))
       EndIf
    EndIf


    If lRet
       cLines := MemoRead( cHtmlTemplate )
       nLines := MLCount( cLines )
       //varinfo("cLines: ", cLines)
       //varinfo("nLines: ", nLines)

       aadd(aHtmlMsg , cLines)
    EndIf


    If lRet
       //valida RD0
       BEGINSQL ALIAS cQryRD0
          SELECT RD0.RD0_FILIAL, RD0.RD0_CODIGO, RD0.RD0_CIC, RD0.RD0_LOGIN, RD0.RD0_EMAIL, RD0.RD0_NOME 
          FROM %table:RD0% RD0
          WHERE ( RD0.RD0_LOGIN  = %Exp:aValueBody[1]%            OR
                  RD0.RD0_LOGIN  = %Exp:Upper(aValueBody[1])%     OR 
                  RD0.RD0_CIC    = %Exp:aValueBody[1]%            OR 
                  RD0.RD0_EMAIL  = %Exp:aValueBody[1]%            OR
                  RD0.RD0_EMAIL  = %Exp:Upper(aValueBody[1])%     ) AND
                ( RD0.RD0_EMAIL  = %exp:aValueBody[3]%            OR
                  RD0.RD0_EMAIL  = %exp:Upper(aValueBody[3])%     ) AND 
                  RD0.%notDel%
       ENDSQL

       If !(cQryRD0)->(Eof())

            dbSelectArea("RD0")
            dbSetOrder(1)
            If dbSeek(xFilial("RD0")+(cQryRD0)->RD0_CODIGO)

               cRetCrypto = rc4crypt( (cQryRD0)->RD0_CODIGO +";" +aValueBody[1] +";" +aValueBody[3] +";" +dToC(date()) +";" +Time() +";" +alltrim(str(Randomize(100000,999000))) +";" ,"MeuRH#P12%Dutchman", .T.) 
               //processo crypto - retorno ser� em c�digo ASCII hexadecimal
               //varinfo("Hash RC4 gerada: ",cRetCrypto)

               //atualiza hash RD0
               Reclock("RD0",.F.)
               RD0->RD0_RSTPWD := cRetCrypto
               RD0->( MsUnlock() )

               cUrl := aValueBody[2] +"#/resetPassword?hash=" + cRetCrypto //"http://spon4718.sp01.local:8081/T1/#/resetPassword?hash=" 
               //varinfo("cUrl: ",cUrl)

               aNome := strtokarr((cQryRD0)->RD0_NOME, " ") 
               If Len(aNome) > 0
                  aHtmlMsg[1] := StrTran( aHtmlMsg[1], "%first_name%", aNome[1] )
               Else
                  aHtmlMsg[1] := StrTran( aHtmlMsg[1], "%first_name%", " " )
               EndIf
               aHtmlMsg[1]    := StrTran( aHtmlMsg[1], "%link_pwd%", cUrl )
               aHtmlMsg[1]    := StrTran( aHtmlMsg[1], "%date_time%", cValToChar(day( Date() )) +" de " +mesextenso( Date() ) +" de " +cValToChar(year( Date() )) +" " +SUBSTR(TIME(), 1, 5) )

               If "@totvs" $ LOWER( Alltrim((cQryRD0)->RD0_EMAIL) )  
                  //em virtude dos programas clientes de email n�o realizarem busca no ambiente local
                  //para valida��o das imagens no corpo do email para recuperar senha, foi subentendido
                  //que: caso o rementente seja @totvs, a funcionalidade est� em processo de valida��o
                  //sendo assim a URL da imagem montada est� sendo redirecionada para um servidor externo
                  //publico do time de ofertas para valida��o 

                  aHtmlMsg[1] := StrTran( aHtmlMsg[1], "%img_header%", "http://187.94.56.71:8087/pp/images/meurh/header_email.png" )
                  aHtmlMsg[1] := StrTran( aHtmlMsg[1], "%img_bottom%", "http://187.94.56.71:8087/pp/images/meurh/totvs.png" )
               Else
                  cUrlImg     := aValueBody[2] + "assets/img/header_email.png"  
                  aHtmlMsg[1] := StrTran( aHtmlMsg[1], "%img_header%", cUrlImg )
                  cUrlImg     := aValueBody[2] + "totvs.png" 
                  aHtmlMsg[1] := StrTran( aHtmlMsg[1], "%img_bottom%", cUrlImg )
               EndIf
               
               cTitEmail      := EncodeUTF8(STR0078)  //"MeuRH Esqueci minha senha"

               //Vers�o inicial do html 
               //cMsgEmail := '<html>'
               //cMsgEmail += '<head>'
               //cMsgEmail += '<title>'+cTitEmail+'</title>'
               //cMsgEmail += '</head>'
               //cMsgEmail += '<body>'
               //cMsgEmail += '<table borderColor="#0099cc" height="29" cellSpacing="1" width="750" borderColorLight="#0099cc" border=1>'
               //cMsgEmail += '<tr>'
               //cMsgEmail += '<td borderColor="#0099cc" borderColorLight="#0099cc" align="left" width="606" borderColorDark=v bgColor="#0099cc" height="1">'
               //cMsgEmail += '<p align="center"><font face="Arial" color="#ffffff" size="3"><b>' +"Link para gerar nova senha" +'</b></font></p></td>'
               //cMsgEmail += '</tr>'
               //cMsgEmail += '<tr>'
               //cMsgEmail += '<td align="left" width="606" height="32">'
               //cMsgEmail += '<br>'
               //cMsgEmail += '<p align="left">'
               //cMsgEmail += '<font face="Arial" color="#0099cc" size="2"><b>' +"Clique no link:" +' </b></font>' //Clique no link:
               //cMsgEmail += '<a href="' +cUrl +'"> ' +cUrl +'</a>'
               //cMsgEmail += '<br>'
               //cMsgEmail += '<br>'
               //cMsgEmail += '</p>'
               //cMsgEmail += '</td>'
               //cMsgEmail += '</tr>'
               //cMsgEmail += '</body>'
               //cMsgEmail += '</html>'
 
               //Arquivo final de verificacao criado apenas para valida��o do email final
               //nHandle := FCreate("\web\PortalMeuRH\rstpwd_ptbr_final.html")
               //FWrite(nHandle, aHtmlMsg[1])
               //FClose(nHandle)

               //Dispara e-mail
               nErro := RH_Email( Lower(Alltrim((cQryRD0)->RD0_EMAIL)) ,'' ,cTitEmail ,aHtmlMsg[1] ,'' ,'') 
               If nErro != 0
                  lRet       := .F.
                  Do Case
                        Case nErro == 1
                           cRestFault := EncodeUTF8(STR0096) //"Servidor SMTP n�o encontrado (MV_RELSERV)"
                        Case nErro == 2
                           cRestFault := EncodeUTF8(STR0097) //"Conta de email n�o encontrado (MV_EMCONTA)"
                        Case nErro == 3
                           cRestFault := EncodeUTF8(STR0098) //"Senha de email n�o encontrado (MV_EMSENHA)"
                        Case nErro == 4
                           cRestFault := EncodeUTF8(STR0099) //"Email do Destinat�rio n�o informado"
                        Case nErro == 5
                           cRestFault := EncodeUTF8(STR0100) //"Erro na conex�o com o servidor SMTP"
                        OTHERWISE
                           cRestFault := EncodeUTF8(STR0101) //"Erro desconhecido no envio de email" 
                  EndCase
               Else
                  lRet       := .T.
               EndIf
            Else
               lRet       := .F.
               cRestFault := EncodeUTF8(STR0062) //"Participante n�o localizado na RD0" 
            EndIf

       else   
          lRet       := .F.
          cRestFault := EncodeUTF8(STR0052) //"usario/email n�o localizado na base de dados" 
       EndIf   

       (cQryRD0)->( DbCloseArea() )
    EndIf


    If lRet
       oItemData["user"]        := aValueBody[1]
       oItemData["redirectUrl"] := aValueBody[2]
       oItemData["email"]       := aValueBody[3]

       oMsgReturn["type"]       := "success"
       oMsgReturn["code"]       := "200"
       oMsgReturn["detail"]     := EncodeUTF8(STR0055) //"link para reset da senha enviado por e-mail"
       Aadd(aMessages, oMsgReturn)
    Else
       oMsgReturn["type"]       := "error"
       oMsgReturn["code"]       := "400"
       oMsgReturn["detail"]     := cRestFault
       Aadd(aMessages, oMsgReturn)
    EndIf

    oItem["data"]     := oItemData
    oItem["messages"] := aMessages
    oItem["length"]   := 1

    cJson :=  FWJsonSerialize(oItem, .F., .F., .T.)
    ::SetResponse(cJson)

Return(.T.)



// -------------------------------------------------------------------
// - PUT RESPONS�VEL POR ATUALIZAR A SENHA DO USU�RIO.
// -------------------------------------------------------------------
WSMETHOD PUT editPassword WSREST Auth

Local cJsonObj      := "JsonObject():New()"
Local oItemDetail   := &cJsonObj
Local oItemData     := &cJsonObj
Local oItem         := &cJsonObj
Local oMsgReturn    := &cJsonObj
Local cBody         := ::GetContent()
Local nLenParms     := Len(::aURLParms)
Local nHashVld      := (SuperGetMv("MV_HASHVLD",,60) == 0, 60)
Local cQuery        := GetNextAlias()
Local lRet          := .T.
Local lRC4          := .T.
Local aValueBody    := {}
Local aMessages     := {}
local aKey          := {}
Local cWhere        := ""
Local cRestFault    := ""
Local cJson         := ""
local cNewPwd       := ""
local cKey          := ""
local cValidTime    := ""

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

	If !Empty(cBody) 
		oItemDetail:FromJson(cBody)
	EndIf
    
    If nLenParms >= 1 .And. ::aURLParms[1] == "resetPassword"
       //a hash foi retirada da URL e passada para o body
       //varinfo("hash recebida     : ", ::aURLParms[2] )

       If !Empty(cBody) .and. lRet
       	  
          If !Empty(oItemDetail["password"]) .And. !Empty(oItemDetail["hash"]) .And. len(alltrim(oItemDetail["password"])) <= 6 
             //varinfo("nova pwd: ", aValueBody[1] )
             //varinfo("hash    : ", aValueBody[2] )
             
             // processo descriptografia
             BEGIN SEQUENCE             
                cKey := rc4crypt( oItemDetail["hash"], "MeuRH#P12%Dutchman", .F., .T. )

                // verifica validade do hash
                aKey := STRTOKARR(cKey, ";")
                //varinfo("aKey: ", aKey)

                //Valida data e hora recebida no hash
                aDiffDatas := DateDiffYMD( aKey[4] , date() )
                //varinfo("Data Max: ", aKey[4] )
                //varinfo("Data Atu: ", Date() )
             RECOVER
                lRC4 := .F.
             END SEQUENCE

             If aDiffDatas[3] > 0 .or. !lRC4 
                //hash: data fora da validade! solicite novamente. 
                lRet       := .F.
                cRestFault := EncodeUTF8(STR0067) //"Link incorreto ou expirado. Solicite novamente outro link!"
                
                //limpa hash na RD0
                dbSelectArea("RD0")
                dbSetOrder(1)
                If dbSeek(xFilial("RD0") + alltrim(aKey[1]))
                   If alltrim(RD0->RD0_RSTPWD) == alltrim(oItemDetail["hash"])
                      Reclock("RD0",.F.)
                      RD0->RD0_RSTPWD := ""
                      RD0->( MsUnlock() )
                   EndIF
                EndIf 

             Else
                //nSegundos := ELAPTIME( "10:00:00", TIME() )
                //varinfo("Dif segundos : ", nSegundos )
                //IncTime([<cTime>],<nIncHours>,<nIncMinuts>,<nIncSeconds> ) -> Somar 
                //DecTime([<cTime>],<nDecHours>,<nDecMinuts>,<nDecSeconds> ) -> Subtrair
                
                cValidTime := IncTime( aKey[5] , 0 , nHashVld , 0 )
                //varinfo("Hora Max: ", cValidTime )
                //varinfo("Hora Atu: ", Time() )

               If Time() > cValidTime
                  //"hash: hora fora da validade! solicite novamente."
                  lRet       := .F.
                  cRestFault := EncodeUTF8(STR0067) //"Link incorreto ou expirado. Solicite novamente outro link!"
                   
                  //limpa hash na RD0
                  dbSelectArea("RD0")
                  dbSetOrder(1)
                  If dbSeek(xFilial("RD0") + alltrim(aKey[1]))
                     If alltrim(RD0->RD0_RSTPWD) == alltrim(oItemDetail["hash"])
                        Reclock("RD0",.F.)
                        RD0->RD0_RSTPWD := ""
                        RD0->( MsUnlock() )
                     EndIF
                  EndIf 
               Else
                  //pesquisa hash na tabela RD0 e atualiza nova senha
                  If RD0->(ColumnPos("RD0_RSTPWD")) > 0
                     dbSelectArea("RD0")
                     dbSetOrder(1)
                     If dbSeek(xFilial("RD0")+ aKey[1])

                        If RD0->RD0_RSTPWD != oItemDetail["hash"]
                           //HASH recebida invalida/n�o confere 
                           lRet       := .F.
                           cRestFault := EncodeUTF8(STR0067) //"Link incorreto ou expirado. Solicite novamente outro link!" 
                        Else
                           //Atualiza password
                           cNewPwd := Upper(AllTrim(oItemDetail["password"]))
                           cNewPwd := Padr(cNewPwd,6)
                           cNewPwd := Embaralha(cNewPwd, 0)
                            
                           Begin Transaction
                              //atualizando senha RD0
                              Reclock("RD0",.F.)
                              RD0->RD0_SENHA  := cNewPwd
                              RD0->RD0_RSTPWD := ""
                              RD0->( MsUnlock() )

                              //atualizando senha SRA
                              cWhere := "%AND RDZ.RDZ_ENTIDA='SRA' AND RDZ.RDZ_CODRD0='" +RD0->(RD0_CODIGO) +"'%"

                              BEGINSQL ALIAS cQuery
                                 SELECT RDZ.RDZ_CODRD0, RDZ.RDZ_CODENT
                                 FROM %table:RDZ% RDZ
                                 WHERE RDZ.%notDel%
                                       %exp:cWhere%
                              ENDSQL

                              While !(cQuery)->(Eof()) .And. ((cQuery)->RDZ_CODRD0==RD0->(RD0_CODIGO))

                                 DbSelectArea("SRA")
                                 DbSetOrder(1)
                                 If SRA->( DbSeek( AllTrim((cQuery)->RDZ_CODENT) ) )
                                    SRA->( Reclock("SRA",.F.) )
                                    SRA->RA_SENHA := cNewPwd
                                    SRA->( Msunlock() )
                                 EndIf

                                  (cQuery)->(dbSkip())
                              EndDo
                              (cQuery)->(dbCloseArea())
                            
                              lRet       := .T.
                           End Transaction
                               
                        EndIf   
                     Else
                        lRet       := .F.
                        cRestFault := EncodeUTF8(STR0062) //"Participante n�o localizado na RD0" 
                     EndIf
                  Else
                     lRet       := .F.
                     cRestFault := EncodeUTF8(STR0063) //"Campo RSTPWD n�o localizado na tabela RD0" 

                     If cMeurhLog != "0"
                        conout(EncodeUTF8(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"     ))
                        conout(EncodeUTF8(">>> MeuRH Reset Password"                                 ))
                        conout(EncodeUTF8(">>> " +FwCutOff(STR0065,.T.) +": " +FwCutOff(STR0063,.T.) )) //"aviso"
                        conout(EncodeUTF8(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"     ))
                     EndIf

                  EndIf

               EndIf
            EndIf

         Else
            lRet       := .F.
            cRestFault := EncodeUTF8(STR0091) //"A senha deve ter no m�ximo 6 caracteres."
         EndIf
      Else
         lRet       := .F.
         cRestFault := EncodeUTF8(STR0059) //"body n�o localizado na requisi��o" 
      EndIf

   Else
      lRet       := .F.
      cRestFault := EncodeUTF8(STR0060) //"servico pwd invalido" 
   EndIf
    

   If lRet
      oItemData["password"] := oItemDetail["password"]
      oItemData["hash"]     := oItemDetail["hash"]

      oMsgReturn["type"]    := "success"
      oMsgReturn["code"]    := "200"
      oMsgReturn["detail"]  := EncodeUTF8(STR0061) //"senha atualizada com sucesso"
      Aadd(aMessages, oMsgReturn)

	   oItem["data"]          := oItemData
	   oItem["messages"]      := aMessages
	   oItem["length"]        := 1
	   cJson                  :=  FWJsonSerialize(oItem, .F., .F., .T.)
	   ::SetResponse(cJson)
   Else
		SetRestFault(400, cRestFault, .T.)
   EndIf

Return(lRet)


// -------------------------------------------------------------------
// - GET RESPONS�VEL POR VERIFICAR SE � O PRIMEIRO LOGIN DO USU�RIO.
// -------------------------------------------------------------------
WSMETHOD GET isFirstLogin WSREST Auth

Local cJsonObj       := "JsonObject():New()"
Local oItem          := &cJsonObj
Local cToken         := ""
Local cLogin         := ""
Local cSenha         := ""
Local cBranchVld     := ""
Local cCodVld        := ""
Local cHash          := ""
Local lFirst         := .F.
Local lExitCpo       := RD0->(ColumnPos("RD0_RSTPWD")) > 0
Local aDataLogin     := {}

cToken     := Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
   cLogin       := aDataLogin[2]
   cBranchVld   := aDataLogin[5]
   cCodVld      := aDataLogin[3]
EndIf

If !Empty(cLogin) .And. lExitCpo

   dbSelectArea("RD0")
   dbSetOrder(1)
   If dbSeek(xFilial("RD0", cBranchVld) + cCodVld)

      //Senha padr�o do ERP.
      // Dois �ltimos digitos do ano de nascimento + dois d�gitos do dia da admiss�o + dois �ltimos d�gitos do CPF
      cSenha := SubStr(dToS(RD0->RD0_DTNASC),3,2) + SubStr(dToC(RD0->RD0_DTADMI),1,2) + SubStr(RD0->RD0_CIC,10,2)
      
      //Se a senha cadastrada para o usu�rio for igual da senha padr�o do ERP, Ent�o solicita troca de senha.
      If Upper(AllTrim(Embaralha(RD0->RD0_SENHA,1))) == cSenha
         lFirst := .T.
         cHash  := rc4crypt( RD0->RD0_CODIGO +";" +cLogin +";" + RD0->RD0_EMAIL +";" +dToC(date()) +";" +Time() +";" +alltrim(str(Randomize(100000,999000))) +";" ,"MeuRH#P12%Dutchman", .T.)

         //ATUALIZA O CAMPO RD0_RSTPWD COM O NOVO HASH
         RecLock("RD0", .F.)
            RD0->RD0_RSTPWD := cHash
         MsUnLock()
      EndIf
   EndIf
   
EndIf

oItem["isFirstLogin"]  := lFirst

cJson :=  FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)

// -------------------------------------------------------------------
// - REALIZA A TROCA DE SENHA DO USUARIO
// -------------------------------------------------------------------
WSMETHOD POST ChangePassWithLastPass WSREST Auth

Local cJsonObj		:= "JsonObject():New()"
Local cJson			:= &cJsonObj
Local oItemDetail	:= &cJsonObj
Local cBody			:= ::GetContent()
Local lRet			:= .T.
Local lRobot      := .F.
Local cToken		:= ""
Local cBranchVld	:= ""
Local cMatSRA		:= ""
Local cCodVld		:= ""
Local cLogin		:= ""
Local cLastPwd		:= ""
Local cFormLogin	:= ""
Local cFormLastPwd   := ""
Local cFormNewPwd	:= ""
Local cFormPwdOk	:= ""
Local cMsgErro		:= EncodeUTF8(STR0090) //"Usu�rio ou senha inv�lidos."
Local cPPAccess   := GetMv("MV_ACESSPP",,"")
Local aQryParam   := Self:aQueryString
Local lExitCpo		:= RD0->(ColumnPos("RD0_RSTPWD")) > 0
Local lEncrypted  := .F.
Local lUpdSRA     := .F.
Local nX          := 0
Local aDataLogin  := {}

cToken      := Self:GetHeader('Authorization')

aDataLogin  := GetDataLogin(cToken)
If Len(aDataLogin) > 0
   cMatSRA      := aDataLogin[1]
   cBranchVld   := aDataLogin[5]
   cCodVld      := aDataLogin[3]
EndIf

If !lExitCpo .Or. Empty(cBranchVld) .Or. Empty(cMatSRA) .Or. Empty(cCodVld) .Or. Empty(cBody)
   lRet := .F.
Else

   For nX := 1 To Len(aQryParam)
      If UPPER(aQryParam[nX,1]) == "ISENCRYPTED"
         lEncrypted := aQryParam[nX,2] == "true"
      EndIf
   Next nX
   oItemDetail:FromJson(cBody)

   cFormLogin		:= If(oItemDetail:hasProperty("user"), AllTrim(oItemDetail["user"]), "")
   cFormLastPwd 	:= If(oItemDetail:hasProperty("lastPassword"), oItemDetail["lastPassword"], "")
   cFormNewPwd 	:= If(oItemDetail:hasProperty("password"), oItemDetail["password"], "")
   cFormPwdOk  	:= If(oItemDetail:hasProperty("confirmPassword"), oItemDetail["confirmPassword"], "")
   lRobot         := If(oItemDetail:hasProperty("execRobo"), oItemDetail["execRobo"], "0") == "1"

   If lEncrypted
      //Remove o encode64
      cFormLogin     := DECODE64( cFormLogin )
      cFormLastPwd   := DECODE64( cFormLastPwd)
      cFormNewPwd    := DECODE64( cFormNewPwd )
      cFormPwdOk     := DECODE64( cFormPwdOk )

      // Remove as strings especiais definidas
      cFormLogin     := fRemoveStr( cFormLogin )
      cFormLastPwd   := fRemoveStr( cFormLastPwd )
      cFormNewPwd    := fRemoveStr( cFormNewPwd )
      cFormPwdOk     := fRemoveStr( cFormPwdOk )
         
      //Desembaralha as strings.
      cFormLogin     := fInvString(cFormLogin)
      cFormLastPwd   := fInvString(cFormLastPwd)
      cFormNewPwd    := fInvString(cFormNewPwd)
      cFormPwdOk     := fInvString(cFormPwdOk)
   EndIf

   //Transforma em mai�sculas.
   cFormLogin     := UPPER(cFormLogin)
   cFormLastPwd   := UPPER(cFormLastPwd)
   cFormNewPwd    := UPPER(cFormNewPwd)
   cFormPwdOk     := UPPER(cFormPwdOk)

   If Empty(cFormLogin) .Or. Empty(cFormLastPwd) .Or. Empty(cFormNewPwd) .Or. !( cFormNewPwd == cFormPwdOk )
         lRet := .F.
   Else

      If Len( AllTrim( cFormNewPwd ) ) > 6 .Or. Len( AllTrim( cFormPwdOk ) ) > 6 
         cMsgErro := EncodeUTF8(STR0091) //"A senha deve ter no m�ximo 6 caracteres."
         lRet := .F.
      ElseIf cFormLastPwd == cFormNewPwd
         cMsgErro := EncodeUTF8(STR0093) //"A nova senha n�o pode ser igual a �ltima cadastrada."
         lRet := .F.
      Else
         dbSelectArea("RD0")
         dbSetOrder(1)
			
         If dbSeek( xFilial("RD0", cBranchVld) + cCodVld )
			
            //Obtem os dados do login existente antes da atualizacao
            If !Empty( AllTrim( RD0->RD0_LOGIN ) )
               //Considera o campo login de forma prioritaria
               cLogin	:= UPPER( AllTrim( RD0->RD0_LOGIN ) )
            Else
               //Caso contrario considera os campos CPF ou Email conforme o parametro MV_ACESSPP
               cLogin := If( cPPAccess == "1", UPPER(AllTrim(RD0->RD0_EMAIL)), UPPER(AllTrim(RD0->RD0_CIC)) )
            EndIf

            cLastPwd	:= UPPER( AllTrim( Embaralha( RD0->RD0_SENHA,1 ) ) )
	
            //Verifica se os dados recebidos na requisicao estao corretos
            If !(cLogin == cFormLogin)
               lRet := .F.
               cMsgErro := EncodeUTF8( STR0094 )
            ElseIf !(cLastPwd == cFormLastPwd)
               lRet := .F.
               cMsgErro := EncodeUTF8( STR0095 )
            Else
            //Atualiza a senha
               RD0->( RecLock("RD0",.F.) )
               RD0->RD0_SENHA := Embaralha(PADR(cFormNewPwd,6),0)
               RD0->RD0_RSTPWD := ""
               RD0->( MsUnLock() )
               lUpdSRA := .T.
            EndIf

            //Atualiza a senha do cadastro do funcionario
            If lUpdSRA
               dbSelectArea("SRA")
               dbSetOrder(1)
               If SRA->( dbSeek( cBranchVld + cMatSRA ) )
                  SRA->( RecLock("SRA",.F.) )
                  SRA->RA_SENHA := Embaralha(PADR(cFormNewPwd,6),0)
                  SRA->( MsUnLock() )
               EndIf
            EndIf

			Else
				lRet := .F.
			EndIf
		EndIf
	   
	 EndIf
	      
EndIf

If lRet
	cJson :=  FWJsonSerialize(oItemDetail, .F., .F., .T.)
	::SetResponse(cJson)
Else
   If lRobot
      lRet := .T.
      oItemDetail        			 := &cJsonObj
      oItemDetail["errorCode"] 	 := "400"
      oItemDetail["errorMessage"] := cMsgErro
      cJson := FWJsonSerialize(oItemDetail, .F., .F., .T.)
      ::SetResponse(cJson)
   Else
      SetRestFault(400, cMsgErro)
   EndIf
EndIf

Return( lRet )



//***************************************
// M�todos REST CONTEXT
//***************************************

// ---------------------------------------------------------------
// - GET RESPONS�VEL POR RETORNAR OS CONTEXTOS DO USU�RIO LOGADO.
// ---------------------------------------------------------------
WSMETHOD GET getAllContexts  WSREST Setting

Local cJsonObj    := "JsonObject():New()"
Local oItem       := &cJsonObj
Local oMessages   := &cJsonObj
Local oValida     := &cJsonObj
Local cBranchVld  := ""
Local cMatSRA     := ""
Local cLogin      := ""
Local cToken      := ""
Local aItemCtx    := {}
Local aDadosCtx   := {}
Local aMessages   := {}
Local aDataLogin  := {}


::SetHeader('Access-Control-Allow-Credentials' , "true")
cToken  := Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
   cMatSRA      := aDataLogin[1]
   cBranchVld   := aDataLogin[5]
   cLogin       := aDataLogin[2]
EndIf

If Empty(cBranchVld) .Or. Empty(cMatSRA) .Or. Empty(cLogin)
	oMessages["type"]   := "error"
	oMessages["code"]   := "401"
	oMessages["detail"] := EncodeUTF8(STR0086) //"Dados inv�lidos para valida��o do contexto."

	Aadd(aMessages,oMessages)
Else
	//Busca multiplus v�nculos
	getMultV(cBranchVld,cMatSRA,cLogin,cJsonObj,@aDadosCtx,@aItemCtx,.T.)
EndIf


// - Por por padr�o todo objeto tem
// - data: contendo a estrutura do JSON
// - messages: para determinados avisos
// - length: informativo sobre o tamanho.
oItem["data"]   := Iif(Empty(aDadosCtx),oValida,aDadosCtx)
oItem["length"] := Iif(Empty(aDadosCtx),1,Len(aDadosCtx))
If Len(aDadosCtx) < 1
    oMessages["type"]   := "info"
    oMessages["code"]   := ""
    oMessages["detail"] := EncodeUTF8(STR0087) //"Nenhuma matr�cula localizada no servi�o de contexto."

    Aadd(aMessages, oMessages)
EndIf
oItem["messages"] := aMessages

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cjson)


Return(.T.)

// -------------------------------------------------------------------
// - GET RESPONS�VEL POR RETORNAR O CONTEXTO ATUAL DO USU�RIO LOGADO.
// -------------------------------------------------------------------
WSMETHOD GET getContext  WSREST Setting

Local cJsonObj      := "JsonObject():New()"
Local oItem         := &cJsonObj
Local oMessages     := &cJsonObj
Local oValida       := &cJsonObj
Local cBranchVld    := ""
Local cMatSRA       := ""
Local cLogin        := ""
Local cToken        := ""
Local aItemCtx      := {}
Local aDadosCtx     := {}
Local aMessages     := {}
Local aDataLogin    := {}


::SetHeader('Access-Control-Allow-Credentials' , "true")
cToken  := Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
   cMatSRA      := aDataLogin[1]
   cBranchVld   := aDataLogin[5]
   cLogin       := aDataLogin[2]
EndIf

If Empty(cBranchVld) .Or. Empty(cMatSRA) .Or. Empty(cLogin)
    cJson := resultSetContext(,.F.)
Else
    //Carrega contexto atual
    getMultV(cBranchVld,cMatSRA,cLogin,cJsonObj,@aDadosCtx,@aItemCtx,.F.)

    //prepara json retorno
    cJson := resultSetContext(aItemCtx,.F.)
EndIf

::SetResponse(cjson)

Return(.T.)

// --------------------------------------------------------------
// - PUT RESPONS�VEL POR ATUALIZAR O CONTEXTO DO USU�RIO LOGADO.
// --------------------------------------------------------------
WSMETHOD PUT setContext WSREST Setting
Local cJsonObj      := "JsonObject():New()"
Local oItemDetail   := &cJsonObj
Local oMessages     := &cJsonObj
Local oValida       := &cJsonObj
Local cBody         := ::GetContent()
Local aItemCtx      := {}
Local aEmployeeID   := {}
Local aDadosCtx     := {}
Local aMessages     := {}
Local aDataLogin    := {}
Local cToken        := ""
Local cKey          := ""
Local cBranchVld    := ""
Local cMatSRA       := ""
Local cLogin        := ""
Local cUser         := ""
Local cUserId       := ""
Local cRetorno      := ""

cToken      := Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
   cMatSRA      := aDataLogin[1]
   cBranchVld   := aDataLogin[5]
   cLogin       := aDataLogin[2]
EndIf

If !Empty(cBody) 
    //******parse employeeID, exemplo:
    // "{"current":false,"branchName":"COORDENA��O RH MOBILE         ","status":"active","companyName":"Filial BELO HOR/Grupo TOTVS 1","employeeID":"D MG 01 |00502 |000207","employeeType":"internal"}"

	oItemDetail:FromJson(cBody)
EndIf


If Empty(cBranchVld) .Or. Empty(cMatSRA) .Or. Empty(cLogin) .Or. Empty(cBody)
    cJson := resultSetContext(,.T.)
Else
    aEmployeeID := StrTokArr(oItemDetail["employeeID"], "|")
    //varinfo("aEmployeeID: ",aEmployeeID)       
	
	dbSelectArea("RD0")
	RD0->(dbSetOrder(1))
    If RD0->(dbSeek( xFilial("RD0") + aEmployeeID[3] ))
       //Busca UserID do Portal para gera��o do token
       cUserID := GetConfig("RESTCONFIG","userId", "")

       If Empty(cUserID) //nova pesquisa em virtude da atualiza��o do appWebWizard
		  cUserID := GetConfig("HTTPREST","userId", "")
       EndIf

	   //Busca usuario do portal baseado no usu�rio do RD0
       If Empty(cUserID)  
		  dbSelectArea("AI3")
		  AI3->(dbSetOrder(1))
		  If AI3->(dbSeek( xFilial("AI3")+RD0->RD0_PORTAL ))
			 cUserID := UsrRetName(AI3->AI3_USRSIS)
		  EndIf
	   EndIf

       //carrega matricula e filial para o novo contexto solicitado  
       cKey := aEmployeeID[2] +"|" +cLogin +"|" +RD0->RD0_CODIGO +"|" +DtoS(dDataBase) +"|" +aEmployeeID[1]

       //Gera token
       If !Empty(cUserID)
          //Retirada a passagem de data e segundos para a funcao FwJWT2Bear por orientacao do Framework - 01/2019
          cToken := FwJWT2Bear(cUserID,{"payments/","payment/","data/","team/", "request/", "timesheet/", "/team/", "setting/"},Nil,Nil,Nil,Nil,{ {"key",cKey} })
       EndIf

       //configura o header
       ::SetHeader('Access-Control-Allow-Credentials' , "true")
       ::SetHeader('Access-Control-Expose-Headers', 'Set-Authorization')
       ::SetHeader('Set-Authorization', 'Bearer ' + cToken)

       //prepara informa��es
       getMultV( aEmployeeID[1],aEmployeeID[2],cLogin,cJsonObj,@aDadosCtx,@aItemCtx,.F.)
    EndIf

    //prepara json retorno
    cJson := resultSetContext(aItemCtx,.T.)
EndIf

::SetResponse(cjson)

Return (.T.)


// --------------------------------------------------------
// - GET RESPONS�VEL PARA AVALIAR AS PERMISS�ES DE ACESSO.
// --------------------------------------------------------
WSMETHOD GET getPermissions  WSREST Setting

Local cJsonObj       := "JsonObject():New()"
Local cJson          := &cJsonObj
Local cToken         := ""
Local cMatSRA        := ""
Local cLogin         := ""
Local cRD0Cod        := ""
Local cBranchVld     := ""
Local cServManager   := ""
Local cServDemit     := ""
Local oPermissions   := &cJsonObj
Local lDemit         := .F.
Local lEmployManager := .F.
Local aServices      := {}
Local aDataLogin     := {}
Local nX             := 0

//Servicos gerenciais que sao disponiveis apenas para quem possui equipe
cServManager += "clockingGeoView||clockingGeoDisconsider||substituteRequest||absenceManager||teamManagement||requisitions||demission||demissionRequest"		

//Servicos disponiveis para funcionarios demitidos
cServDemit += "payment||annualReceipt"

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken         := Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA      := aDataLogin[1]
   cBranchVld   := aDataLogin[5]
   cLogin       := aDataLogin[2]
   cRD0Cod      := aDataLogin[3]
   lDemit       := aDataLogin[6]
EndIf

//varinfo("URLParams     : ", ::aURLParms)

If !Empty(::aURLParms[1]) .And. ::aURLParms[1] == "permissions"

   //Carrega lista de permissionamentos do usu�rio de portal logado
   aServices := fPermission(cBranchVld , cLogin, cRD0Cod)

   If !lDemit
      //Indica se o funcionario e responsavel por algum departamento
      lEmployManager := fGetTeamManager(cBranchVld, cMatSRA) 
   EndIf

	For nX := 1 To Len( aServices )
      If lDemit
         If aServices[nX,1] $ cServDemit
            oPermissions[ aServices[nX,1] ] := aServices[nX,2] == "1"
         EndIf
      Else
         If aServices[nX,1] $ cServManager
            oPermissions[ aServices[nX,1] ] := aServices[nX,2] == "1" .And. lEmployManager
         Else
            oPermissions[ aServices[nX,1] ] := aServices[nX,2] == "1"
         EndIf
      EndIf
	Next nX

	cJson := FWJsonSerialize(oPermissions, .F., .F., .T.)
EndIf

//varinfo("cJson: ",cJson)
::SetResponse(cJson)

Return(.T.)

// --------------------------------------------------------------------
// - GET RESPONS�VEL PARA AVALIAR AS PERMISS�ES ATUALIZA��O DE CAMPOS.
// --------------------------------------------------------------------
WSMETHOD GET getFieldProperties  WSREST Setting

Local cJsonObj   := "JsonObject():New()"
Local oItem      := &cJsonObj
Local oProps     := &cJsonObj
Local aData      := {}
Local aDataLogin := {}

Local cJson      := ""
Local cToken     := ""
Local cMatSRA    := ""
Local cBranchVld := ""
Local lHabil     := .F.
Local lDemit     := .F.

Local nLenParms  := Len(::aURLParms)

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken       := Self:GetHeader('Authorization')

aDataLogin   := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA      := aDataLogin[1]
   cBranchVld   := aDataLogin[5]
   cRD0Cod      := aDataLogin[3]
   cLogin       := aDataLogin[2]
   lDemit       := aDataLogin[6]
EndIf

//pageFilter
// payment, vacation, clocking, allowance, profile, notification, occurrence, clockingGeoUpdate, clockingRegister, clockingUpdate
// varinfo("URLParams: ", ::aURLParms)
 
If nLenParms > 1 .And. Lower(::aURLParms[2]) == "demission"
   oProps              := &cJsonObj
   oProps["field"]     := "demissionType" //Tipo do desligamento
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "demissionJustify" //Justificativa do desligamento
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "newHire" //Gera nova contrata��o
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .F.
   Aadd(aData,oProps)
      
   oProps              := &cJsonObj
   oProps["field"]     := "demissionDate" //Data do desligamento
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)
   
   oProps              := &cJsonObj
   oProps["field"]     := "demissionIniciative" //Iniciativa do desligamento
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)
   
   oProps              := &cJsonObj
   oProps["field"]     := "demissionNoticeType" //Tipo de Aviso Pr�vio
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)
   
   oProps              := &cJsonObj
   oProps["field"]     := "demissionReason" //Motivo do desligamento	
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)
   
   oProps              := &cJsonObj
   oProps["field"]     := "employeeDemit" //Considera ex-funcion�rios (demitidos na compet�ncia atual ou posterior)
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)	
   
   oProps              := &cJsonObj
   oProps["field"]     := "noticeDays" //N�mero de dias do aviso pr�vio
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)	
EndIf 

If nLenParms > 1 .And. Lower(::aURLParms[2]) == "medicalcertificate"
   oProps              := &cJsonObj
   oProps["field"]     := "type"
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "reason"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps) 

   oProps              := &cJsonObj
   oProps["field"]     := "doctorName"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "medicalRegionalCouncil"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)
EndIf

If nLenParms > 1 .And. Lower(::aURLParms[2]) == "clockingupdate"
   oProps              := &cJsonObj
   oProps["field"]     := "hour"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "justify"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)
   
   oProps              := &cJsonObj
   oProps["field"]     := "reason"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)
   
   oProps              := &cJsonObj
   oProps["field"]     := "direction"
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)
EndIf

If nLenParms > 1 .And. Lower(::aURLParms[2]) == "clocking"
   fPermission(cBranchVld , cLogin, cRD0Cod, "timesheet", @lHabil)
   oProps              := &cJsonObj
   oProps["field"]     := "balanceSummary"
   oProps["visible"]   := lHabil .And. !lDemit
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)
EndIf

If nLenParms > 1 .And. Lower(::aURLParms[2]) == "payment"
   oProps              := &cJsonObj
   oProps["field"]     := "download"
   oProps["visible"]   := .T.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)
EndIf

If nLenParms > 1 .And. Lower(::aURLParms[2]) == "clockingregister"
   oProps              := &cJsonObj
   oProps["field"]     := "hour"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "justify"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "reason"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "direction"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)
EndIf

If nLenParms > 1 .And. Lower(::aURLParms[2]) == "substituterequest"
   oProps              := &cJsonObj
   oProps["field"]     := "canViewSalary"
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "canManageTeam"
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "divisions"
   oProps["visible"]   := .T.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)
EndIf

If nLenParms > 1 .And. Lower(::aURLParms[2]) == "clockinggeoupdate"
   oProps              := &cJsonObj
   oProps["field"]     := "justify"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "reason"
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "filterButton"
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)   
EndIf

If !Empty(::aURLParms[1]) .And. ::aURLParms[2] == "vacation"
   oProps              := &cJsonObj
   oProps["field"]     := "justify"
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "indirectSubordinatesLevel"
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "hierarchicalLevel"
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)
EndIf

If !Empty(::aURLParms[1]) .And. ::aURLParms[2] == "profile"
   oProps              := &cJsonObj
   oProps["field"]     := "positionLevel"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "registry"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "department"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   oProps              := &cJsonObj
   oProps["field"]     := "admissionDate"
   oProps["visible"]   := .T.
   oProps["editable"]  := .T.
   oProps["required"]  := .T.
   Aadd(aData,oProps)

   //******* Summary
   oProps              := &cJsonObj
   oProps["field"]     := "summary. "
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   //******* teams
   oProps              := &cJsonObj
   oProps["field"]     := "teams. "
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   //******* coordinator
   oProps              := &cJsonObj
   oProps["field"]     := "coordinator. "
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   //******* personalData
   oProps              := &cJsonObj
   oProps["field"]     := "personalData. "
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   //******* addresses
   oProps              := &cJsonObj
   oProps["field"]     := "addresses. "
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   //******* contacts
   oProps              := &cJsonObj
   oProps["field"]     := "contacts. "
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   //******* webReferences
   oProps              := &cJsonObj
   oProps["field"]     := "webReferences. "
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

   //******* documents
   oProps              := &cJsonObj
   oProps["field"]     := "documents. "
   oProps["visible"]   := .F.
   oProps["editable"]  := .F.
   oProps["required"]  := .F.
   Aadd(aData,oProps)

EndIf

oItem["hasNext"]  := .F.
oItem["items"]    := aData

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)
