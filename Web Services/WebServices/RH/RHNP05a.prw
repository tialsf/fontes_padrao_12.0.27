#INCLUDE "TOTVS.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "RESTFUL.CH"

#INCLUDE "RHNP.CH"

Function RHNP05a()
Return .T.


/*/{Protheus.doc} fPermission
Retorna o permissionamento do MeuRH
@author:    Marcelo Faria
@since:     12/05/2020
@param:		cBranchVld - Filial do Token;
@param:		cLogin - Login do Token;
@param:		cRD0Cod - Login do participante (RD0);
@param:		cService - Nome do servi�o que est� sendo avaliado;
@param:		lHabil - Indica se o servico est� ou nao habilitado;
@return:    Array de Servi�os;
/*/
Function fPermission(cBranchVld, cLogin, cRD0Cod, cService, lHabil)
Local aArea         := {}
Local aServices     := {}
Local cFilAI3       := ""
Local cPortal       := ""

Default cBranchVld	:= ""
Default cLogin      := ""
Default cRD0Cod     := ""
Default cService    := ""
Default lHabil      := .F.

    If !Empty(cBranchVld) 
        aArea := GetArea()
        
        dbSelectArea("RD0")
        RD0->(dbSetOrder(10))
        If RD0->(dbSeek( FWxFilial("RD0", cBranchVld)+ Padr(UPPER(AllTrim(cLogin)),TamSx3("RD0_LOGIN")[1])))
            cPortal := RD0->RD0_PORTAL
        Else
            RD0->( dbSetOrder(1) )
            If RD0->(dbSeek( FWxFilial("RD0", cBranchVld) + UPPER(AllTrim(cRD0Cod))) )
                cPortal := RD0->RD0_PORTAL
            EndIf
        EndIf

        If AliasInDic("RJD") .And. !Empty( cPortal )
            cFilAI3 := Iif( FWModeAccess("AI3") == "C", FwxFilial("AI3", cBranchVld), cBranchVld )
            RJD->( DbGoTop() )
            RJD->(dbSetOrder(1))
            If RJD->(dbSeek(FWxFilial("RJD", cBranchVld) + cPortal))
                While RJD->( !Eof() ) 

                    If RJD->RJD_FILIAL == cFilAI3 .And. RJD->RJD_CODUSU == cPortal
                       If Empty(cService) 
                            Aadd(aServices,{ AllTrim(RJD->RJD_WS), RJD->RJD_HABIL })
                       ElseIf AllTrim(RJD->RJD_WS) == AllTrim(cService) 
                            Aadd(aServices,{ AllTrim(RJD->RJD_WS), RJD->RJD_HABIL })
                            lHabil := RJD->RJD_HABIL == "1"
                            exit
                       EndIf         
                    EndIf

                    RJD->(DbSkip())
                EndDo
            EndIf
        EndIf

        If Empty( aServices ) .and. Empty( cService )	
            aServices := TCFA006SRV( .T. )
        EndIf

        RestArea(aArea)
    EndIf

Return aServices


// ---------------------------------------------------
// - FUNCIONALIDADES DIVERSAS DO SERVI�O de CONTEXTO.
// ---------------------------------------------------
/*/{Protheus.doc}getMultV()
- Prepara os dados para o caregamento de m�ltiplos v�nculos do usu�rio.
@author:	Marcelo Faria
/*/
Function getMultV(cBranchVld,cMatSRA,cLogin,cJsonObj,aDadosCtx,aItemCtx,lContext)

Local nI            := 0
Local aInfo         := {}
Local aRet          := {}
Local aDadosFunc    := {}
Local oItemData     := Nil

Default cBranchVld  := FwCodFil()
Default cMatSRA     := ""
Default cLogin      := ""
Default cJsonObj    := "JsonObject():New()"
DEFAULT aDadosCtx   := {}
DEFAULT aItemCtx    := {}


//Busca todas as matr�culas do usu�rio logado
//MatParticiant() com parametro MeuRH, para validar login tamb�m pelo RD0_CIC
If lContext .and. MatParticipant(cLogin, @aRet, .T., .T.)

    For nI := 1 to len(aRet)

        //busca dados passando filial e matr�cula. N�o adiciona matr�culas transferidas.
        If !( aRet[nI,10] $ "30/31" ) .And. fGetFunc(aRet[nI,3], aRet[nI,1], @aDadosFunc)

            oItemData                 :=  &cJsonObj
            oItemData["employeeType"] := "internal"

            //descri��o do departamento
            oItemData["branchName"]   := alltrim(EncodeUTF8(fDesc('SQB',aDadosFunc[1][2],'SQB->QB_DESCRIC',,aRet[nI,3],1)))

            //dados do funcion�rio para autentica��o (Filial+Mat+Codigo)
            oItemData["employeeID"]   := aRet[nI,3]+"|"+aRet[nI,1]+"|"+RD0->RD0_CODIGO

            //verifica situa��o atual
            If SRA->RA_SITFOLH == "D"
               oItemData["status"]    := "inactive"
            Else
               oItemData["status"]    := "active"
            EndIf

            //identifica qual a matr�cula corrente do funcion�rio
            //If nI == 1
            If alltrim(aRet[nI,1]) == cMatSRA
                oItemData["current"]  := .T.
            Else
                oItemData["current"]  := .F.
            EndIf

            If !fInfo(@aInfo, aRet[nI,3])
                oItemData["companyName"] := ""
            Else
                //descri��o da filial/empresa
                oItemData["companyName"] := alltrim(aInfo[1]) +'/' +alltrim(aInfo[2])
            EndIf

            //carrega matr�cula localizada
            Aadd(aDadosCtx,oItemData)
            oItemData := Nil
        Endif

    Next

Else

    //Carrega apenas o contexto da matr�cula atual
    If fGetFunc(cBranchVld, cMatSRA, @aDadosFunc)

        oItemData                 :=  &cJsonObj
        oItemData["employeeType"] := "internal"

        //descri��o do departamento
        oItemData["branchName"]   := EncodeUTF8(fDesc('SQB',aDadosFunc[1][2],'SQB->QB_DESCRIC',,,1))

        //dados do funcion�rio para autentica��o (Filial+Mat+Codigo)
        oItemData["employeeID"]   := cBranchVld+"|"+cMatSRA+"|"+RD0->RD0_CODIGO

        //verifica situa��o atual
        If SRA->RA_SITFOLH == "D" .And. SRA->RA_RESCRAI $ '30/31'
           oItemData["status"]    := "inactive"
        Else
           oItemData["status"]    := "active"
        EndIf

        //identifica qual a matr�cula corrente do funcion�rio
        oItemData["current"]  := .T.

        If !fInfo(@aInfo,cBranchVld)
            oItemData["companyName"] := ""
        Else
           //descri��o da filial/empresa
           oItemData["companyName"] := alltrim(aInfo[1]) +'/' +alltrim(aInfo[2])
        EndIf

        //carrega matr�cula localizada
        Aadd(aItemCtx,oItemData["employeeID"])
        Aadd(aItemCtx,oItemData["branchName"])
        Aadd(aItemCtx,oItemData["companyName"])

        //carrega matr�cula localizada
        Aadd(aDadosCtx,oItemData)
        oItemData := Nil
    Endif

EndIf

Return(Nil)


/*/{Protheus.doc}resultSetContext()
- Prepara o json para retorno de atualiza��o do contextocontext
@author:    Marcelo Faria
/*/

Function resultSetContext(aItemCtx,lSet)
Local cRet := ""
Local cMsg := ""

DEFAULT aItemCtx := {}
DEFAULT lSet     := .F.

/*
cRetorno :=   '{  "data": {  "data": {"branchName": "COORDENACAO RH MOBILE",';
            + '"companyName":"Filial BELO HOR/Grupo TOTVS 1",'  ;
            + '"current":true,'                                 ;
            + '"employeeID":"D MG 01 |00502 |000206",'          ;
            + '"employeeType":"internal",'                      ;
            + '"status":"active" }, '                           ;
            + '"length": 1 ,'                                   ;
            + '"messages": {"code": null, "type": "success", "detail": "result ok"} } }'
*/

If len(aItemCtx) > 0

   cRet  :=   '{  "data": { '                                               ;
            + '"employeeType":"internal",'                                  ;
            + '"status":"active",'                                          ;
            + '"employeeID":"' +aItemCtx[1]                           +'",' ; //"D MG 01 |00502 |000206
            + '"branchName":"' +EncodeUTF8(aItemCtx[2])               +'",' ; //COORDENACAO RH MOBILE
            + '"companyName":"'+EncodeUTF8(aItemCtx[3])               +'",' ; //Filial BELO HOR/Grupo TOTVS 1
            + '"current":true'                                              ;
            + '},'                                                          ;
            + '"messages": [{'                                              ;
            + '"code": null,'                                               ;
            + '"type": "success",'                                          ;
            + '"detail": "'   +'Contexto alterado com sucesso!'       +'"'  ; //Contexto alterado com sucesso!
            + '}],'                                                         ;
            + '"length": 1 ,'                                               ;
            + '"HttpStatusCode": 200'                                       ;
            + '}'

Else

   If lSet
      //PUT Context
      cMsg := EncodeUTF8(STR0088) //"N�o foi poss�vel atualizar o contexto!"
   Else
      //Get Context
      cMsg := EncodeUTF8(STR0089) //"N�o foi poss�vel buscar o contexto!"
   Endif

   cRet  :=   '{  "data": { '                                   ;
            + '"employeeType":"",'                              ;
            + '"status":"",'                                    ;
            + '"employeeID":"",'                                ;
            + '"branchName": "",'                               ;
            + '"companyName":"",'                               ;
            + '"current":false'                                 ;
            + '},'                                              ;
            + '"messages": [{'                                  ;
            + '"code": 204,'                                    ;
            + '"type": "error",'                                ;
            + '"detail": ' +'"' +cMsg +'"'                      ;
            + '}],'                                             ;
            + '"length": 1 ,'                                   ;
            + '"HttpStatusCode": 204'                           ;
            + '}'
Endif

Return(cRet)

/*/{Protheus.doc} fGetTeamManager
- Respons�vel por indicar se uma determinada matricula � respons�vel por algum departamento

@author:	Marcelo Silveira
@since:		28/02/2018
@param:		cFilTeam = Filial que sera pesquisada a estrutura hierarquica;
			cMatTeam = Matricula que sera pesquisada a estrutura hierarquica;
			aEmpFunc = Retorna as empresas abrangidas pelo funcionario conforme sua estrutura hierarquica
			cRoutine = Rotina do WS do Portal para localizar a vis�o que sera avaliada
			cOrgCFG = Valor do parametro MV_ORGCFG 
			lEmp = Se verdadeiro ira retornar as empresas no array passado por referencia em aEmpFunc 
			cEmpFunc = Empresa que sera pesquisada na estrutura hierarquica
@Return:	lLeadTeam = Retorna verdadeiro/falso caso o funcionario seja respons�vel por algum departamento
/*/
Function fGetTeamManager(cFilTeam, cMatTeam, aEmpFunc, cRoutine, cOrgCFG, lEmp, cEmpFunc)

Local cQueryA 	:= ""
Local cQueryB 	:= ""
Local cVision	:= ""
Local cTypeOrg  := ""
Local nX        := 0
Local nY        := 0
Local aEmp      := {}
Local aDepAux	:= {}
Local aVision	:= {}
Local aVisao	:= {}
Local aChaves	:= {}
Local aEmpAux	:= {}
Local lLeadTeam	:= .F.
Local lChkRD4	:= .F.

DEFAULT cFilTeam := ""
DEFAULT cMatTeam := ""
DEFAULT aEmpFunc := {}
DEFAULT cRoutine := "W_PWSA100A.APW" //Considera o WS de Ferias por default
DEFAULT cOrgCfg  := SuperGetMv("MV_ORGCFG",NIL,"0")
DEFAULT lEmp     := .F. //Retorna as empresas da estrutura validas para o funcionario
DEFAULT cEmpFunc := cEmpAnt

If !( cOrgCfg == "0" )
	
	//Verifica se existe visao, e se positivo se � estrutura por departamento
	aVision := GetVisionAI8(cRoutine, cFilTeam, cEmpFunc) 
	cVision := aVision[1][1]
	
	If !Empty( cVision )

		TipoOrg(@cTypeOrg, cVision)

		//Considera outras empresas apenas na hierarquia de vis�o por Departamentos
		If cTypeOrg == "2"
			cRD4Alias := GetNextAlias()
			BeginSQL ALIAS cRD4Alias
				SELECT DISTINCT RD4_EMPIDE
				FROM %table:RD4% RD4 
				WHERE RD4.RD4_CODIGO = %exp:cVision% 
				AND	RD4.RD4_FILIAL = %xfilial:RD4% 
				AND	RD4.%notDel%                   
			EndSQL	
			
			While !(cRD4Alias)->(Eof())
				lChkRD4 := .T.
				aAdd( aEmp, (cRD4Alias)->RD4_EMPIDE )
				(cRD4Alias)->(dbSkip())
			EndDo 
			(cRD4Alias)->(dbCloseArea())
		EndIf
		
	EndIf
	
EndIf

//Quando nao encontra nenhuma empresa a partir da visao considera a propria empresa
If Empty(aEmp)
	aAdd( aEmp, cEmpFunc )
EndIf

//Valida as empresas que serao consideradas
For nX := 1 To Len( aEmp )

    //-----------------------------------
    //VERIFICA SE O FUNCIONARIO E LIDER
    //-----------------------------------
    If !lLeadTeam
    
        __cSQBtab := "%" + RetFullName("SQB", aEmp[nX]) + "%"
        cQueryA  := GetNextAlias()
    
        BEGINSQL ALIAS cQueryA
        
            SELECT COUNT(*) NQTD
                FROM %exp:__cSQBtab% SQB
            WHERE QB_FILRESP = %Exp:cFilTeam% AND
                QB_MATRESP = %Exp:cMatTeam% AND
                SQB.%NotDel%
        ENDSQL
        
        lLeadTeam := (cQueryA)->NQTD > 0
        
        //Quando a chamada da rotina � apenas para verificar se o funcionario � responsavel,
        //quando for atendida a condicao sai do la�o sem precisar verificar as outras empresas
        If lLeadTeam 
            If !lEmp
                (cQueryA)->( dbCloseArea() )
                Exit				
            Else
                (cQueryA)->( dbCloseArea() )
            EndIf
        EndIf
    
    EndIf
    
    //--------------------------------------------------------
    //CARREGA OS DEPARTAMENTOS QUE O FUNCIONARIO � RESPONS�VEL
    //--------------------------------------------------------
    If lEmp
    
        __cSQBtab := "%" + RetFullName("SQB", aEmp[nX]) + "%"
        cQueryB  := GetNextAlias()
    
        BEGINSQL ALIAS cQueryB
            SELECT QB_FILIAL, QB_DEPTO
                FROM %exp:__cSQBtab% SQB
            WHERE QB_FILRESP = %Exp:cFilTeam% AND
                QB_MATRESP = %Exp:cMatTeam% AND
                SQB.%NotDel%			   	  
        ENDSQL

        While !(cQueryB)->(Eof())
            aAdd( aDepAux, { aEmp[nX], (cQueryB)->QB_FILIAL, (cQueryB)->QB_DEPTO } )
            (cQueryB)->(dbSkip())
        EndDo 

        (cQueryB)->( dbCloseArea() )
                
    EndIf

Next nX

//--------------------------------------------------------------------
//VALIDA AS EMPRESAS CONFORME A HIERARQUIA DE VISAO POR DEPARTAMENTOS
    //----------------------------------------------------------------
If lEmp .And. lChkRD4
    
    aVisao  := {}
    aChaves := {}
    aEmpAux := {}
    
    cRD4Alias := GetNextAlias()
    BeginSQL ALIAS cRD4Alias
        SELECT RD4_EMPIDE, RD4_FILIDE, RD4_CODIDE, RD4_CHAVE
        FROM %table:RD4% RD4 
        WHERE RD4.RD4_CODIGO = %exp:cVision% 
        AND	RD4.RD4_FILIAL = %xfilial:RD4% 
        AND	RD4.%notDel%  
    EndSQL	
    
    //Obtem a relacao de Departamentos/Filiais da visao que esta sendo avaliada
    While !(cRD4Alias)->(Eof())
        lChkRD4 := .T.
        aAdd( aVisao, { (cRD4Alias)->RD4_EMPIDE, (cRD4Alias)->RD4_FILIDE, (cRD4Alias)->RD4_CODIDE, (cRD4Alias)->RD4_CHAVE } )
        (cRD4Alias)->(dbSkip())
    EndDo 			
    (cRD4Alias)->(dbCloseArea())

    //Obtem a chave dos departamentos que serao considerados na visao
    For nX := 1 To Len( aDepAux )
        nPos := aScan( aVisao, { |x| x[1]+x[2]+x[3] == aDepAux[nX,1] + aDepAux[nX,2] + aDepAux[nX,3] } )
        If nPos >0
            aAdd( aChaves, { aVisao[nPos,4] } )
        EndIf 
    Next nX

    //Identifica as empresas dentro da hierarquia conforme a chave dos departamentos considerados
    If Len(aChaves) > 0 
        For nX := 1 To Len( aChaves ) 
            cCodChave := AllTrim( aChaves[nX,1] )
            cTamChave := Len( cCodChave )
            
            For nY := 1 To Len(aVisao)
                If cCodChave == SubStr( aVisao[nY,4], 1, cTamChave )
                    If aScan( aEmpAux, { |x| x == aVisao[nY,1] } ) == 0
                        aAdd( aEmpAux, aVisao[nY,1] )
                    EndIf
                EndIf				
            Next nY
            
        Next nX
    EndIf

EndIf

//Retorna a relacao de empresas conforme a estrutura dos departamentos ou da visao
If lEmp
    aEmpFunc := If( Len(aEmpAux) > 0, aClone(aEmpAux), aClone(aEmp) )
EndIf

Return( lLeadTeam )