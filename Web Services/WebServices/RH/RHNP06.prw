#INCLUDE "TOTVS.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "RHNP06.CH"

Function RHNP06()
Return .T.

Private cMRrhKeyTree := ""


WSRESTFUL Timesheet DESCRIPTION EncodeUTF8(STR0001) //"Servi�o de Ponto e Abonos"

WSDATA employeeId		As String Optional
WSDATA WsNull 			As String Optional
WSDATA initPeriod		As String Optional
WSDATA endPeriod		As String Optional
WSDATA referenceDate	As String Optional
WSDATA latitude     	As String Optional
WSDATA longitude    	As String Optional


//****************************** GETs ***********************************

//"Retorna os tipos de abonos cadastrados e dispon�veis no ERP para uso no APP - MEU RH."
WSMETHOD GET GetAllowances DESCRIPTION EncodeUTF8(STR0002) ;
PATH "/allowancesTypes" PRODUCES 'application/json;charset=utf-8'

//"Retorna o espelho de ponto do colaborador."
WSMETHOD GET clockings DESCRIPTION EncodeUTF8(STR0004) ;
PATH "/clockings/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//Retorna os per�odos de ponto dispon�veis para o usu�rio.
WSMETHOD GET GetPeriods DESCRIPTION EncodeUTF8(STR0008) ;
PATH "/periods/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//Saldos - Retorna os saldos de horas do per�odo do colaborador.
WSMETHOD GET GetBalanceSummary DESCRIPTION EncodeUTF8(STR0009) ;
PATH "/balanceSummary/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//Resumo do Periodo - Retorna o resumo do total das ocorr�ncias do per�odo do colaborador.
WSMETHOD GET GetTotSummPeriod DESCRIPTION EncodeUTF8(STR0010) ;
PATH "/occurrencesTotalSummaryPeriod/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//Resumo diario - Retorna as ocorr�ncias de ponto do colaborador.
WSMETHOD GET GetOccurrences DESCRIPTION EncodeUTF8(STR0011) ;
PATH "/occurrences/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//Retorna os motivos da batida.
WSMETHOD GET GetClockTypes DESCRIPTION EncodeUTF8(STR0011) ;
PATH "/clockingsReasonTypes" PRODUCES 'application/json;charset=utf-8'

//Retorna arquivo PDF do espelho do ponto
WSMETHOD GET gFileClocking DESCRIPTION EncodeUTF8(STR0011) ;
PATH "/clockings/report/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//"Retorna as batidas de ponto por Geolocaliza��o funcion�rio"
WSMETHOD GET geolocation DESCRIPTION EncodeUTF8(STR0017) ;
PATH "/clockingsGeolocation/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//"Retorna as batidas de ponto do dia atual"
WSMETHOD GET todayClockings DESCRIPTION EncodeUTF8(STR0019) ;
PATH "/todayClockings/{employeeId}/{latitude}/{longitude}" PRODUCES 'application/json;charset=utf-8'

//"Retorna a data e hora atuais do servidor com base no fuso hor�rio do usu�rio"
WSMETHOD GET currentTime DESCRIPTION EncodeUTF8(STR0020) ;
PATH "/clockingsGeolocation/currentTime/{latitude}/{longitude}" PRODUCES 'application/json;charset=utf-8'

//"Retorna os motivos para desconsiderar batidas"
WSMETHOD GET disconsider DESCRIPTION EncodeUTF8(STR0022) ;
PATH "/disconsiderReasons/" PRODUCES 'application/json;charset=utf-8'

//M�todo que retorna a lista das solicita��es de Abono.
WSMETHOD GET GetListAllowance DESCRIPTION EncodeUTF8(STR0051) ;
PATH "/allowances/{employeeId}" PRODUCES 'application/json;charset=utf-8'


//****************************** POSTs ***********************************

//"M�todo que inclui uma solicita��o de Abono."
WSMETHOD POST SetAllowanceRequest DESCRIPTION EncodeUTF8(STR0003) ;
PATH "/allowances/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//"Inclus�o de batidas do espelho de ponto."
WSMETHOD POST SetClocking DESCRIPTION EncodeUTF8(STR0004) ;
PATH "/clocking/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//"Inclus�o de batida 373 no ponto"
WSMETHOD POST geolocation DESCRIPTION EncodeUTF8(STR0017) ;
PATH "/clockingsGeolocation/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//"Inclus�o de batidas do espelho de ponto"
WSMETHOD POST SetVariousClockings DESCRIPTION EncodeUTF8(STR0016) ;
PATH "/clockings/{employeeId}" PRODUCES 'application/json;charset=utf-8'


//****************************** PUTs ***********************************

//"Atualiza as batidas de ponto por Geolocaliza��o funcion�rio"
WSMETHOD PUT geolocation DESCRIPTION EncodeUTF8("") ;
PATH "/clockingsGeolocation/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//"Atualiza varias batidas do espelho do ponto"
WSMETHOD PUT editVariousClockings DESCRIPTION EncodeUTF8(STR0023) ;
PATH "/clockings/{employeeId}" PRODUCES 'application/json;charset=utf-8'

//"Atualiza uma batida do espelho do ponto"
WSMETHOD PUT editOneClockings DESCRIPTION EncodeUTF8(STR0028) ;
PATH "/clocking/{employeeId}/{id}" PRODUCES 'application/json;charset=utf-8'

//"Atualiza uma solicita��o de abono"
WSMETHOD PUT editAllowance DESCRIPTION EncodeUTF8(STR0060) ;
PATH "/allowances/{employeeId}/{id}" PRODUCES 'application/json;charset=utf-8'


//****************************** DELETEs ***********************************

//"Exclus�o da batida do espelho do ponto"
WSMETHOD DELETE dClocking DESCRIPTION EncodeUTF8(STR0057) ;  
WSSYNTAX "/timesheet/clocking/{employeeId}/{id}" ;
PATH "/clocking/{employeeId}/{id}" PRODUCES 'application/json;charset=utf-8'

//"Exclus�o da solicita��o de abono"
WSMETHOD DELETE dAllowance DESCRIPTION EncodeUTF8(STR0058) ;
WSSYNTAX "/timesheet/allowances/{employeeId}/{id}" ;
PATH "/allowances/{employeeId}/{id}" PRODUCES 'application/json;charset=utf-8'

END WSRESTFUL


WSMETHOD GET GetClockTypes PATHPARAM employeeId WSREST Timesheet

Local cJsonObj 		:= "JsonObject():New()"
Local oItem			:= &cJsonObj
Local aData			:= {}
Local cToken	 	:= ""
Local cMatSRA		:= ""
Local cBranchVld	:= ""

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken  := Self:GetHeader('Authorization')

cMatSRA	 	 := GetRegisterHR(cToken)
cBranchVld 	 := GetBranch(cToken)

If Empty(cMatSRA) .Or. Empty(cBranchVld)

Else
	aData := fGetClockType(cBranchVld)
EndIf

oItem["items"] 	  := aData
oItem["hasNext"]  := .F.

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)

// -------------------------------------------------------------------
// Retorna o arquivo PDF do espelho do ponto
// -------------------------------------------------------------------
WSMETHOD GET gFileClocking PATHPARAM employeeId WSREST Timesheet

Local oFile 			:= Nil
Local aQryParam 	:= Self:aQueryString
Local aLog 				:= {}
Local aIdFunc			:= {}
Local aPeriods		:= {}
Local aDataLogin	:= {}
Local cToken			:= ""
Local cBranchVld	:= ""
Local cMatSRA			:= ""
Local cLogin			:= ""
Local cRD0Cod			:= ""
Local cPer				:= ""
Local cArqLocal		:= ""
Local cExtFile		:= ""
Local cFileName		:= ""
Local cFile				:= ""
Local cPDF				:= ".PDF"
Local nX					:= 0
Local nCont				:= 0
Local lContinua		:= .T.
Local lRet				:= .T.
Local lHabil			:= .T.
Local lDemit			:= .F.

DEFAULT Self:initPeriod	:= ""
DEFAULT Self:endPeriod	:= ""

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA      := aDataLogin[1]
   cBranchVld   := aDataLogin[5]
   cLogin       := aDataLogin[2]
   cRD0Cod      := aDataLogin[3]
   lDemit       := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "timesheet", @lHabil)
If !lHabil .Or. lDemit
	SetRestFault(400, EncodeUTF8( STR0068 )) //"Permiss�o negada aos servi�os do espelho de ponto!"
	Return (.F.)  
EndIf

If !( "current" $ Self:employeeId )
	aIdFunc := STRTOKARR( Self:employeeId, "|" )
	If Len(aIdFunc) > 0
		cBranchVld	:= aIdFunc[1]
		cMatSRA		:= aIdFunc[2]
	EndIf
EndIf

If !Empty(cMatSRA) .And. !Empty(cBranchVld)
	//Posiciona a tabela SRA na matricula que esta sendo impressa
	dbSelectArea("SRA")
	dbSetOrder(1)
	If dbSeek( cBranchVld + cMatSRA )

		cExtFile	:= DTOS( DATE() ) + SubStr( TIME(), 1, 2) //Ano + Mes + Dia + Hora	
	    cFileName 	:= AllTrim(cBranchVld) + "_" + AllTrim(cMatSRA) + "_CLOCK_"
	    cArqLocal 	:= GetSrvProfString ("STARTPATH","")
	
	    //Obtem o periodo da queryparam que veio na requisicao
		For nX := 1 To Len( aQryParam )
			If UPPER(aQryParam[nX,1]) == "INITPERIOD"
				cPer := dToS( CtoD( Format8601( .T., aQryParam[nX,2]) ) )
			ElseIf UPPER(aQryParam[nX,1]) == "ENDPERIOD"
				cPer += dToS( CtoD( Format8601( .T., aQryParam[nX,2]) ) )
			EndIf
		Next

		//Se nao vier o periodo na requisicao considera o que estiver aberto no ponto
		If Empty(cPer) 
			aPeriods := GetPerAponta( 1, cBranchVld , cMatSRA, .F.)
			If Len(aPeriods) > 0
				cPer	:= dToS( aPeriods[1,1] ) + dToS( aPeriods[1,2] ) //Ex.: 2019070120190731 
			EndIf
		EndIf
	
		//Valida se a admissao eh inferior ao final do periodo do ponto que esta aberto
		If !Empty(cPer) .And. Len(cPer) >= 16
			lContinua := dToS( SRA->RA_ADMISSA ) < SubStr( cPer, 9, 8 )
		EndIf
		
		If lContinua
			
			//------------------------------------------------------------------------------
			//Existe um problema ainda nao solucionado que o APP envia mais de uma requisicao via mobile
			//Quando isso ocorre o sistema nao gera o arquivo e envia uma resposta sem conteudo. 
			//Solucao paliativa:
			//Caso alguma requisicao falhe tentaremos gerar o arquivo novamente por 5 vezes no maximo
			//Cada nova requisicao ira gerar o arquivo com um nome diferente (Filial + Matricula + nX) 
			//------------------------------------------------------------------------------
			For nX := 1 To 3

				//Se existir o arquivo REL/PDF nao executamos a PONR010 porque indica uma requisicao em andamento
				If !File( cArqLocal + cFileName + cExtFile + '*' )
					//Faz a geracao do arquivo PDF do espelho do ponto
					PONR010( .T., cBranchVld, cMatSRA, cPer, , {}, .T., cFileName + cExtFile + cValToChar(nX))
				EndIf
			
			    //Avalia o arquivo gerado no servidor
			    While lContinua
				    
				    If File( cArqLocal + cFileName + cExtFile + cValToChar(nX) + cPDF )
			    		oFile := FwFileReader():New( cArqLocal + cFileName + cExtFile + cValToChar(nX) + cPDF )
			    		
			    		If (oFile:Open())
					    	cFile := oFile:FullRead()
					        oFile:Close()
			    		EndIf
				    EndIf
			
				    //Em determinados ambientes pode ocorrer demora na geracao do arquivo, entao tentar localizar por 5 segundos no maximo.
				    If ( lContinua := Empty(cFile) .And. nCont <= 4 )
				    	nCont++
				    	Sleep(1000)
				    EndIf
			    End
		    
			    If !Empty(cFile)
			    	Exit
			    Else
			    	lContinua := .T.
			    	Conout( EncodeUTF8(">>>"+ STR0042 +"("+ cValToChar(nX) +")") ) //"Aguardando a geracao do espelho do ponto..."
			    EndIf

			Next nX
		EndIf    

    EndIf

    //Se algum erro impediu a geracao do arquivo, faz a geracao de um arquivo PDF com a mensagem do erro.
	If Empty( cFile )

    	aAdd( aLog, STR0046 ) //"Durante o processamento ocorreram erros que impediram a grava��o dos dados. Contate o administrador do sistema."
    	aAdd( aLog, "" )
    	aAdd( aLog, STR0047 ) //"Poss�veis causas do problema:"
    	aAdd( aLog, "- " + STR0048 ) //"Inexist�ncia de marca��es no per�odo solicitado."
    	aAdd( aLog, "- " + STR0049 ) //"Dado incorreto no per�odo de apontamento solicitado."
	
		fPDFMakeFileMessage( aLog, cFileName, @cFile ) 
	EndIf  

    //Exclui arquivos temporarios que nao foram eliminados no termino do processamento (REL/PDF) 
    fExcFileMRH( cArqLocal + cFileName + '*' )

	::SetHeader("Content-Disposition", "attachment; filename=" + cFileName + cPDF)
	::SetResponse(cFile)
	
EndIf

Return( lRet )


WSMETHOD POST SetClocking PATHPARAM employeeId WSREST Timesheet
	
	Local aUrlParam		:= ::aUrlParms
	Local cBody				:= ::GetContent()
	Local cJsonObj		:= "JsonObject():New()"
	Local oItem				:= &cJsonObj
	Local oItemDetail	:= &cJsonObj
	Local cToken			:= ""
	Local cBranchVld	:= ""
	Local cMatSRA			:= ""
	Local cLogin			:= ""
	Local cRD0Cod			:= ""
	Local cStatus			:= ""
	Local cMsgLog			:= ""
	Local cJson				:= ""
	Local lRet				:= .T.
	Local lHabil			:= .T.
	Local lDemit			:= .F.
	Local lRobot			:= .F.
	Local aIdFunc			:= {}
	Local aDataLogin	:= {}
	
  ::SetHeader('Access-Control-Allow-Credentials' , "true")
	cToken		:= Self:GetHeader('Authorization')

	aDataLogin := GetDataLogin(cToken, .T.)
	If Len(aDataLogin) > 0
		cMatSRA      := aDataLogin[1]
		cBranchVld   := aDataLogin[5]
		cLogin       := aDataLogin[2]
		cRD0Cod      := aDataLogin[3]
		lDemit       := aDataLogin[6]
	EndIf	

	//Valida Permissionamento
	fPermission(cBranchVld, cLogin, cRD0Cod, "clockingRegister", @lHabil)
	If !lHabil .Or. lDemit
		SetRestFault(400, EncodeUTF8( STR0066 )) //"Permiss�o negada aos servi�os de marca��o de ponto!"
		Return (.F.)  
	EndIf

    //Verifica se eh o gestor que esta fazendo a solicitacao
	If !( "current" $ Self:employeeId )
		aIdFunc := STRTOKARR( Self:employeeId, "|" )
		If Empty(aIdFunc)
			aIdFunc := {}			
		EndIf
	EndIf

    lRet := fSetClocking(aUrlParam, DecodeUTF8(cBody), @cJsonObj, @oItem, @oItemDetail, cToken, @cStatus, @cMsgLog, aIdFunc, @lRobot)

    If lRet
        If !Empty(cStatus)
            ::SetHeader('Status', cStatus)
        EndIf
        cJson := FWJsonSerialize(oItemDetail, .F., .F., .T.)
        ::SetResponse(cJson)
    Else
        If lRobot
			lRet := .T.
			oItem        			:= &cJsonObj
			oItem["errorCode"] 		:= "400"
			oItem["errorMessage"] 	:= cMsgLog
			cJson := FWJsonSerialize(oItem, .F., .F., .T.)
			Self:SetResponse(cJson)
		Else
        	SetRestFault(400, EncodeUTF8(cMsgLog))
		EndIf
    EndIf
Return lRet



WSMETHOD GET GetOccurrences PATHPARAM employeeId WSREST Timesheet

Local cJsonObj 	 	:= "JsonObject():New()"
Local oItem		 	:= &cJsonObj
Local oItemDetail	:= &cJsonObj
Local aData			:= {}
Local aEventos		:= {}
Local aIdFunc		:= {}
Local aDataLogin	:= {}
Local cJson			:= ""
Local cToken        := ""
Local cBranchVld	:= ""
Local cMatSRA		:= ""
Local cLogin		:= ""
Local cRD0Cod		:= ""
Local nX			:= 0
Local nSaldo		:= 0
Local lSexagenal	:= .T. //SuperGetMv("MV_HORASDE",, "N") == "S" //Desabilitado porque o App existe somente sexagenal
Local lDemit      := .F.
Local lHabil      := .T.

DEFAULT Self:referenceDate	:= Ctod("//")

Self:SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "timesheet", @lHabil)
If !lHabil .Or. lDemit
	SetRestFault(400, EncodeUTF8( STR0068 )) //"Permiss�o negada aos servi�os do espelho de ponto!"
	Return (.F.)  
EndIf

If !Empty(Self:referenceDate)
	
	If !( "current" $ Self:employeeId )
		aIdFunc := STRTOKARR( Self:employeeId, "|" )
		If Len(aIdFunc) > 0
			cBranchVld	:= aIdFunc[1]
			cMatSRA		:= aIdFunc[2]
		EndIf
	EndIf
	
	aEventos := fSumOccurPer( cBranchVld, cMatSRA, Self:referenceDate, Self:referenceDate, lSexagenal )
	
	If !Empty(aEventos)
		
		For nX := 1 To Len(aEventos)
			nSaldo := aEventos[nX,3]
			nSaldo := If( lSexagenal, nSaldo, fConvHr( nSaldo, "D", .T., 2 ) )
			oItemDetail	:= &cJsonObj
			oItemDetail["id"] 				:= cValToChar(nX)
			oItemDetail["date"] 			:= Self:referenceDate
			oItemDetail["referenceDate"]	:= Self:referenceDate
			oItemDetail["total"]			:= HourToMs( StrZero(nSaldo,5,2) ) 
			oItemDetail["description"] 		:= EncodeUTF8(aEventos[nX,2])
			aAdd( aData, oItemDetail )				
		Next nX

	EndIf
	
EndIf

oItem["hasNext"] 	:= CtoD( Format8601(.T.,Self:initPeriod) )
oItem["items"]		:= aData

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)


WSMETHOD GET GetTotSummPeriod PATHPARAM employeeId WSREST Timesheet

Local cJsonObj 	 	:= "JsonObject():New()"
Local oItem		 	:= &cJsonObj
Local oItemDetail	:= &cJsonObj
Local aData			:= {}
Local aEventos		:= {}
Local cJson			:= ""
Local cToken		:= ""
Local cBranchVld	:= ""
Local cMatSRA		:= ""
Local nX			:= 0
Local nSaldo		:= 0
Local lSexagenal	:= .T. //SuperGetMv("MV_HORASDE",, "N") == "S" //Desabilitado porque o App existe somente sexagenal

DEFAULT Self:initPeriod	:= ""
DEFAULT Self:endPeriod	:= ""

Self:SetHeader('Access-Control-Allow-Credentials' , "true")

If !Empty(Self:initPeriod) .And. !Empty(Self:endPeriod)
	
	If ( "current" $ Self:employeeId )
		cToken		:= Self:GetHeader('Authorization')
		cBranchVld	:= GetBranch(cToken)
		cMatSRA		:= GetRegisterHR(cToken)
	Else
		aIdFunc := STRTOKARR(Self:employeeId, "|")
		If Len(aIdFunc) > 0
			cBranchVld := aIdFunc[1]
			cMatSRA	   := aIdFunc[2]
		EndIf
	EndIf
	
	aEventos := fSumOccurPer( cBranchVld, cMatSRA, Self:initPeriod, Self:endPeriod, lSexagenal )
	
	If !Empty(aEventos)
		
		For nX := 1 To Len(aEventos)
			nSaldo := aEventos[nX,3]
			nSaldo := If( lSexagenal, nSaldo, fConvHr( nSaldo, "D", .T., 2 ) )
			oItemDetail	:= &cJsonObj
			oItemDetail["id"] 			:= cValToChar(nX)
			oItemDetail["description"] 	:= EncodeUTF8(aEventos[nX,2])
			oItemDetail["value"]		:= If(nSaldo < 100.00, StrTran( StrZero(nSaldo,5,2), ".", ":" ), StrTran( StrZero(nSaldo,6,2), ".", ":" )) 
			aAdd( aData, oItemDetail )				
		Next nX

	EndIf
	
EndIf

oItem["initPeriod"] 				:= CtoD( Format8601(.T.,Self:initPeriod) )
oItem["endPeriod"]  				:= CtoD( Format8601(.T.,Self:endPeriod) )
oItem["occurrencesTotalSummary"]	:= aData

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
Self:SetResponse(cJson)

Return(.T.)


WSMETHOD GET GetBalanceSummary PATHPARAM employeeId WSREST Timesheet

Local cJsonObj 	 	:= "JsonObject():New()"
Local oItem		 	:= &cJsonObj
Local aEventos		:= {}
Local aPeriods		:= {}
Local aIdFunc		:= {}
Local cJson			:= ""
Local cToken		:= ""
Local cBranchVld	:= ""
Local cMatSRA		:= ""
Local cIniPer		:= ""
Local cFimPer		:= ""

::SetHeader('Access-Control-Allow-Credentials' , "true")

DEFAULT Self:initPeriod	:= ""
DEFAULT Self:endPeriod	:= ""

If ( "current" $ Self:employeeId )
	cToken		:= Self:GetHeader('Authorization')
	cBranchVld	:= GetBranch(cToken)
	cMatSRA		:= GetRegisterHR(cToken)
Else
	aIdFunc := STRTOKARR( Self:employeeId, "|" )
	If Len(aIdFunc) > 0
		cBranchVld	:= aIdFunc[1]
		cMatSRA		:= aIdFunc[2]
	EndIf
EndIf

If !Empty(cBranchVld) .And. !Empty(cMatSRA)

	//Se nao vier o periodo na requisicao considera o que estiver aberto no ponto
	If Empty(Self:initPeriod) .Or. Empty(Self:endPeriod) 
		aPeriods := GetPerAponta( 1, cBranchVld , cMatSRA, .F.)
		If Len(aPeriods) > 0
			cIniPer := dToS( aPeriods[1,1] )
			cFimPer := dToS( aPeriods[1,2] )
		EndIf
	Else
		cIniPer := Self:initPeriod
		cFimPer := Self:endPeriod		
	EndIf
	
	aEventos := fBalanceSumPer( cBranchVld, cMatSRA, cIniPer, cFimPer, .T. )
	
	If !Empty(aEventos)
		oItem["previous"] := HourToMs( cValToChar( Abs(aEventos[1]) ) ) * If( aEventos[1] > 0, 1, -1 )
		oItem["current"]  := HourToMs( cValToChar( Abs(aEventos[3]) ) ) * If( aEventos[3] > 0, 1, -1 )
		oItem["next"] 	  := HourToMs( cValToChar( Abs(aEventos[2]) ) ) * If( aEventos[2] > 0, 1, -1 )
	EndIf
	
EndIf

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)


WSMETHOD GET GetPeriods PATHPARAM employeeId WSREST Timesheet

Local cJsonObj 	 	:= "JsonObject():New()"
Local oItem		 	:= &cJsonObj
Local oItemDetail	:= &cJsonObj
Local oMessages	  	:= &cJsonObj
Local cMatSRA		:= ""
Local cBranchVld	:= ""
Local cToken	  	:= ""
Local aDateGMT		:= ""
Local lAuth    		:= .T.
Local aData			:= {}
Local aPeriods		:= {}
Local aMessages		:= {}
Local aIdFunc		:= {}
Local initPeriod    := Ctod("//")
Local endPeriod     := Ctod("//")
Local nPer			:= 0
Local nX			:= 0

::SetHeader('Access-Control-Allow-Credentials' , "true")

If ( "current" $ Self:employeeId )
	cToken  := Self:GetHeader('Authorization')
	cMatSRA    := GetRegisterHR(cToken)
	cBranchVld := GetBranch(cToken)
Else
	aIdFunc := STRTOKARR( Self:employeeId, "|" )
	If Len(aIdFunc) > 0
		cBranchVld := aIdFunc[1]
		cMatSRA	   := aIdFunc[2]
	EndIf
EndIf

If Empty(cMatSRA) .Or. Empty(cBranchVld)

	oMessages["type"]   := "error"
	oMessages["code"]   := "401"
	oMessages["detail"] := EncodeUTF8(STR0006) //"Dados inv�lidos."

	Aadd(aMessages,oMessages)
	lAuth := .F.

EndIf

aPeriods	:= GetPeriodApont(cBranchVld,,6)//GetPerAponta( nNumPerAnt , cBranchVld , cMatSRA, lReturn)
nPer		:= Len(aPeriods)

For nX := 1 To nPer

	oItemDetail					:= &cJsonObj
	initPeriod 					:= aPeriods[nX,1]
	endPeriod   				:= aPeriods[nX,2]

	aDateGMT 			  		:= LocalToUTC( DTOS(initPeriod), "12:00:00" )
	oItemDetail["initDate"]		:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")
	aDateGMT 					:= {}

	aDateGMT 		  			:= LocalToUTC( DTOS(endPeriod), "12:00:00" )
	oItemDetail["endDate"]		:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")
	oItemDetail["actualPeriod"]	:= nX == nPer

	aAdd( aData, oItemDetail )

Next nX

oItem["items"] 	  := aData
oItem["hasNext"]  := .F.

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)


WSMETHOD GET GetAllowances WSREST Timesheet

Local cJsonObj		:= "JsonObject():New()"
Local oItemData		:= &cJsonObj
Local oItem			:= &cJsonObj
Local oAllowType	:= &cJsonObj
Local aData			:= {}
Local aDataLogin	:= {}
Local cToken		:= ""
Local cMatSRA		:= ""
Local cBranchVld	:= ""
Local cLogin		:= ""
Local cRD0Cod		:= ""
Local lDemit    := .F.
Local lHabil    := .T.

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "allowance", @lHabil)
If !lHabil .Or. lDemit
	SetRestFault(400, EncodeUTF8( STR0065 )) //"Permiss�o negada aos servi�os de abono!"
	Return (.F.)  
EndIf

If !Empty(Self:aURLParms[1]) .And. Self:aURLParms[1] == "allowancesTypes"
	GetAllowances(cJsonObj,oItemData,oAllowType,@aData,cBranchVld,cMatSRA)
EndIf

oItem["items"] 	  := aData
oItem["hasNext"]  := .T.

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)


WSMETHOD GET GetListAllowance PATHPARAM employeeId WSREST Timesheet
Local cJsonObj		:= "JsonObject():New()"
Local oItem			:= &cJsonObj
Local aData			:= {}
Local aDataLogin	:= {}
Local cJson			:= ""
Local cPage			:= ""
Local cPageSize		:= ""
Local cStatus		:= ""
Local cToken        := ""
Local cLogin		:= ""
Local cBranchVld	:= ""
Local cMatSRA		:= ""
Local cRD0Cod		:= ""
Local nX			:= 0
Local nTipo			:= 3
Local lNext        	:= .F.
Local lDemit        := .F.
Local lHabil        := .T.
Local aIdFunc       := {}
Local aQryParam		:= Self:aQueryString

Self:SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "allowance", @lHabil)
If !lHabil .Or. lDemit
	SetRestFault(400, EncodeUTF8( STR0065 )) //"Permiss�o negada aos servi�os de abono!"
	Return (.F.)  
EndIf

If Len(::aUrlParms) > 0
	If (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
		If !Empty(::aUrlParms[2])

			aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
			If Len(aIdFunc) > 1 
			
			    //valida se o solicitante da requisi��o pode ter acesso as informa��es
                If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                   cBranchVld	:= aIdFunc[1]
                   cMatSRA		:= aIdFunc[2]
                Else
                   cBranchVld	:= ""
                   cMatSRA		:= ""
	    		EndIf	
			EndIf

		EndIf
	EndIf
EndIF

If !Empty(cBranchVld) .And. !Empty(cMatSRA)
	For nX := 1 To Len( aQryParam )
		Do Case
			Case UPPER(aQryParam[nX,1]) == "PAGE"
				cPage		:= aQryParam[nX,2] 
			Case UPPER(aQryParam[nX,1]) == "PAGESIZE"
				cPageSize	:= aQryParam[nX,2]
			Case UPPER(aQryParam[nX,1]) == "STATUS"
				cStatus		:= aQryParam[nX,2]
		End Case
	Next

	//1 = Pendentes de aprovacao (status pending)
	//2 = Aprovados ou Reprovados (status notpending)
	//3 = Todos (status vazio)
	nTipo := If( Empty(cStatus), 3, If( cStatus == "notpending", 2, 1 ) )
	
	aData := fGetListAllowance( nTipo, cBranchVld, cMatSRA, Nil, cPage, cPageSize, @lNext, aDataLogin ) 
EndIf

oItem["hasNext"]  := lNext
oItem["items"]    := aData

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
Self:SetResponse(cJson)

Return (.T.)


WSMETHOD POST SetAllowanceRequest PATHPARAM employeeId WSREST Timesheet

Local cBody				:= ::GetContent()
Local aUrlParam		:= ::aUrlParms
Local cJsonObj		:= "JsonObject():New()"
Local oItem				:= &cJsonObj
Local oItemDetail	:= &cJsonObj
Local cJson				:= ""
Local cStatus			:= ""
Local cToken			:= ""
Local cBranchVld	:= ""
Local cMatSRA			:= ""
Local cLogin			:= ""
Local cRD0Cod			:= ""
Local lHabil			:= .T.
Local lDemit			:= .F.
Local aDataLogin	:= {}
Local aIdFunc			:= {}

::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA      := aDataLogin[1]
   cBranchVld   := aDataLogin[5]
   cLogin       := aDataLogin[2]
   cRD0Cod      := aDataLogin[3]
   lDemit       := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "allowance", @lHabil)
If !lHabil .Or. lDemit
	SetRestFault(400, EncodeUTF8( STR0065 )) //"Permiss�o negada aos servi�os de abono!"
	Return (.F.)  
EndIf

//Verifica se eh o gestor que esta fazendo a solicitacao
If !( "current" $ Self:employeeId )
	aIdFunc := STRTOKARR( Self:employeeId, "|" )
	If Empty(aIdFunc)
		aIdFunc := {}			
	EndIf
EndIf

AllowanceRequest(aUrlParam,DecodeUTF8(cBody),@cJsonObj,@oItem,@oItemDetail,cToken,@cStatus,aIdFunc)

If !Empty(cStatus)
	::SetHeader('Status', cStatus)
EndIf

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return (.T.)


WSMETHOD GET clockings PATHPARAM employeeId WSREST Timesheet

Local cJsonObj 	 	:= "JsonObject():New()"
Local oItem		 	:= &cJsonObj
Local oMessages	  	:= &cJsonObj
Local lAuth    		:= .T.
Local lDemit      := .F.
Local lHabil      := .T.
Local cToken        := ""
Local cBranchVld	:= ""
Local cMatSRA		:= ""
Local cLogin		:= ""
Local cRD0Cod		:= ""
Local aDataLogin	:= {}
Local aMessages		:= {}
Local aIdFunc		:= {}
Local aData			:= {}
Local aPeriods		:= {}
Local initPeriod    := CtoD( Format8601(.T.,Self:initPeriod) )
Local endPeriod     := CtoD( Format8601(.T.,Self:endPeriod) )
Local aDateGMT		:= ""

::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "timesheet", @lHabil)
If !lHabil .Or. lDemit
	SetRestFault(400, EncodeUTF8( STR0068 )) //"Permiss�o negada aos servi�os do espelho de ponto!"
	Return (.F.)  
EndIf

If !( "current" $ Self:employeeId )
	aIdFunc := STRTOKARR( Self:employeeId, "|" )
	If Len(aIdFunc) > 0
		cBranchVld	:= aIdFunc[1]
		cMatSRA		:= aIdFunc[2]
	EndIf
EndIf

If Empty(cMatSRA) .Or. Empty(cBranchVld)

	oMessages["type"]   := "error"
	oMessages["code"]   := "401"
	oMessages["detail"] := EncodeUTF8(STR0006) //"Dados inv�lidos."

	Aadd(aMessages,oMessages)
	lAuth := .F.

EndIf

If( Empty(initPeriod) .And. Empty(endPeriod) )
	aPeriods	:= GetPeriodApont(cBranchVld,cMatSRA)
	If Len(aPeriods) > 0
		initPeriod 	:= aPeriods[1,1]
		endPeriod   := aPeriods[1,2]
	EndIf
EndIf

If lAuth
	getClockings(cJsonObj,@aData,cBranchVld,cMatSRA,initPeriod,endPeriod)
EndIf

// - Por por padr�o todo objeto tem
// - data: contendo a estrutura do JSON
// - messages: para determinados avisos
// - length: informativo sobre o tamanho.
aDateGMT 			  	:= LocalToUTC( DTOS(initPeriod), "12:00:00" )
oItem["initPeriod"]	:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")
aDateGMT := {}

aDateGMT 			  	:= LocalToUTC( DTOS(endPeriod), "12:00:00" )
oItem["endPeriod"]   := Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")
oItem["clockings"]   := aData

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return(.T.)


WSMETHOD POST geolocation PATHPARAM employeeId WSREST Timesheet

    Local oClocking		:= &("JsonObject():New()")
    Local cBody			:= self:GetContent()
	Local cToken        := ""
	Local cBranchVld	:= ""
	Local cMatSRA		:= ""
	Local cLogin		:= ""
	Local cRD0Cod		:= ""
    Local cMsg			:= ""
    Local lRet			:= .F.
	Local lDemit      := .F.
	Local lHabil      := .T.
	Local aDataLogin	:= {}
    Local oItem
    Local oItemDetail

    ::SetHeader('Access-Control-Allow-Credentials' , "true")

	cToken		:= Self:GetHeader('Authorization')
	
	aDataLogin := GetDataLogin(cToken, .T.)
	If Len(aDataLogin) > 0
		cMatSRA    := aDataLogin[1]
		cLogin     := aDataLogin[2]
		cRD0Cod    := aDataLogin[3]
		cBranchVld := aDataLogin[5]
		lDemit     := aDataLogin[6]
	EndIf

	//Valida Permissionamento
	fPermission(cBranchVld, cLogin, cRD0Cod, "clockingGeoRegister", @lHabil)
	If !lHabil .Or. lDemit
		SetRestFault(400, EncodeUTF8( STR0067 )) //"Permiss�o negada aos servi�os de marca��o por geolocaliza��o!"
		Return (.F.)  
	EndIf

    lRet := fSetGeoClocking( DecodeUTF8(cBody), @oItem, @oItemDetail, cToken, @cMsg )

    If lRet
        cJson := FWJsonSerialize(oItemDetail, .F., .F., .T.)
        ::SetResponse(cJson)
	Else
        SetRestFault(400, cMsg)
	EndIf

Return( lRet )


//"Retorna as batidas de ponto do dia atual"
WSMETHOD GET todayClockings PATHPARAM employeeId, latitude, longitude WSREST Timesheet
    Local oItem		 	:= &("JsonObject():New()")
    Local aData		 	:= {}
	Local aDataLogin	:= {}
	Local cToken        := ""
	Local cBranchVld	:= ""
	Local cMatSRA		:= ""
	Local cLogin		:= ""
	Local cRD0Cod		:= ""
	Local lDemit    := .F.
	Local lHabil    := .T.

    ::SetHeader('Access-Control-Allow-Credentials' , "true")

	cToken		:= Self:GetHeader('Authorization')

	aDataLogin := GetDataLogin(cToken, .T.)
	If Len(aDataLogin) > 0
		cMatSRA    := aDataLogin[1]
		cLogin     := aDataLogin[2]
		cRD0Cod    := aDataLogin[3]
		cBranchVld := aDataLogin[5]
		lDemit     := aDataLogin[6]
	EndIf	

	//Valida Permissionamento
	fPermission(cBranchVld, cLogin, cRD0Cod, "clockingGeoRegister", @lHabil)
	If !lHabil .Or. lDemit
		SetRestFault(400, EncodeUTF8( STR0067 )) //"Permiss�o negada aos servi�os de marca��o por geolocaliza��o!"
		Return (.F.)  
	EndIf

    If !Empty(cMatSRA) .And. !Empty(cBranchVld)
        aData := GetDayClocks(cBranchVld, cMatSRA)
    EndIf

    oItem["hasNext"] 	:= .F.
    oItem["items"]		:= aData

    cJson := FWJsonSerialize(oItem, .F., .F., .T.)
    ::SetResponse(cJson)

Return .T.

// Retorna a data e hora atuais do servidor com base no fuso hor�rio do usu�rio
WSMETHOD GET currentTime PATHPARAM latitude, longitude WSREST Timesheet
    Local oItem 		:= &("JsonObject():New()")
	Local aHorTmz 		:= {}
	Local aDataLogin	:= {}
	Local cToken        := ""
	Local cBranchVld	:= ""
	Local cMatSRA		:= ""
	Local cLogin		:= ""
	Local cRD0Cod		:= ""
	Local cTimezone		:= ""
	Local cMsg			:= ""
	Local lRet 			:= .T.
	Local lHabil    := .T.
	Local lDemit    := .F.
	Local nX 			:= 0

    ::SetHeader('Access-Control-Allow-Credentials' , "true")

	cToken		:= Self:GetHeader('Authorization')

	aDataLogin := GetDataLogin(cToken, .T.)
	If Len(aDataLogin) > 0
		cMatSRA    := aDataLogin[1]
		cLogin     := aDataLogin[2]
		cRD0Cod    := aDataLogin[3]
		cBranchVld := aDataLogin[5]
		lDemit     := aDataLogin[6]
	EndIf

	//Valida Permissionamento
	fPermission(cBranchVld, cLogin, cRD0Cod, "clockingGeoRegister", @lHabil)
	If !lHabil .Or. lDemit
		SetRestFault(400, EncodeUTF8( STR0067 )) //"Permiss�o negada aos servi�os de marca��o por geolocaliza��o!"
		Return (.F.)  
	EndIf

	For nX := 1 To Len( Self:aQueryString )
		If UPPER(Self:aQueryString[nX,1]) == "TIMEZONE"
			cTimezone := Self:aQueryString[nX,2]
		EndIf
	Next

	If !empty(cTimezone)
		aHorTmz := fGetTimezone( val(cTimezone) )
		If empty(aHorTmz)
			lRet := .F.
			cMsg :=  EncodeUTF8(STR0064) //"Ocorreu um problema na verifica��o do timezone para marca��o"
		Else
		    oItem["actualDate"] := aHorTmz[1]
    		oItem["actualTime"] := aHorTmz[2]
		EndIf
	Else
	    oItem["actualDate"] := FwTimeStamp(6, dDataBase, "12:00:00" )
    	oItem["actualTime"] := Seconds()*1000 // O formato esperado � miliseconds.
	EndIf

	If  lRet
		cJson := FWJsonSerialize(oItem, .F., .F., .T.)
		::SetResponse(cJson)
	Else
		SetRestFault(400, cMsg)
	EndIf

Return lRet

WSMETHOD GET disconsider WSREST Timesheet

    Local oItem		 	:= &("JsonObject():New()")
    Local cToken        := ""
    Local cMatSRA       := ""
    Local cBranchVld    := ""
    Local cType         := "2" //-- Motivos de Rejei��o
    Local aData         := {}

    ::SetContentType("application/json")
    ::SetHeader('Access-Control-Allow-Credentials' , "true")

    cToken  := Self:GetHeader('Authorization')

    cMatSRA	   := GetRegisterHR(cToken)
    cBranchVld := GetBranch(cToken)

    If !Empty(cMatSRA) .And. !Empty(cBranchVld)
        aData := fGetClockType(cBranchVld, cType)
    EndIf

    oItem["items"] 	 := aData
    oItem["hasNext"] := .F.

    cJson := FWJsonSerialize(oItem, .F., .F., .T.)

    ::SetResponse(cJson)

Return .T.


WSMETHOD GET geolocation PATHPARAM employeeId WSREST Timesheet

Local oItem			:= &("JsonObject():New()")
Local cMatSRA		:= ""
Local cBranchVld	:= ""
Local cToken		:= ""
Local cLogin		:= ""
Local cRD0Cod		:= ""
Local cMatTeam		:= ""
Local cFilTeam		:= ""
Local cEmpTeam		:= ""
Local lDemit		:= .F.
Local lHabil		:= .T.
Local aDataLogin	:= {}
Local aDataFunc		:= {}
Local aPeriods		:= {}
Local aData			:= {}
Local initPeriod	:= CtoD( Format8601(.T.,Self:initPeriod) )
Local endPeriod		:= CtoD( Format8601(.T.,Self:endPeriod) )

::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')

aDataLogin  := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
	cBranchVld := aDataLogin[5]
	cLogin     := aDataLogin[2]
	cRD0Cod    := aDataLogin[3]	
	lDemit     := aDataLogin[6]
EndIf   

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "clockingGeoRegister", @lHabil)
If !lHabil .Or. lDemit
	SetRestFault(400, EncodeUTF8( STR0067 )) //"Permiss�o negada aos servi�os de marca��o por geolocaliza��o!"
	Return (.F.)  
EndIf

If "|" $ Self:employeeId
  aDataFunc := STRTOKARR( Self:employeeId, "|" )
  If Len(aDataFunc) > 0
	  cFilTeam := aDataFunc[1]
	  cMatTeam := aDataFunc[2]	    
	  cEmpTeam := If( Len(aDataFunc) > 2, aDataFunc[3], cEmpAnt )	    
	EndIf
Else
	cFilTeam := cBranchVld
	cMatTeam := Self:employeeId
	cEmpTeam := cEmpAnt
EndIf

If !Empty(cMatTeam) .And. !Empty(cFilTeam)

    If( Empty(initPeriod) .And. Empty(endPeriod) )
        
		aPeriods := GetDataForJob( "1", {cFilTeam, cMatTeam, Nil}, cEmpTeam )
		
        If Len(aPeriods) > 0
            initPeriod 	:= aPeriods[1,1]
            endPeriod   := aPeriods[1,2]
        EndIf
    EndIf

	aData := getGeoClockings(cFilTeam,cMatTeam,initPeriod,endPeriod,cEmpTeam)
EndIf

oItem["hasNext"] 	:= .F.
oItem["items"]		:= aData

cJson := FWJsonSerialize(oItem, .F., .F., .T.)
::SetResponse(cJson)

Return .T.

WSMETHOD PUT geolocation PATHPARAM employeeId WSREST Timesheet

Local oItem			:= &("JsonObject():New()")
Local cMatSRA		:= ""
Local cBranchVld	:= ""
Local cToken	  	:= ""
Local cLogin		:= ""
Local cRD0Cod		:= ""
Local cMsg			:= ""
Local cFilTeam		:= ""
Local cMatTeam		:= ""
Local cEmpTeam		:= cEmpAnt
Local cBody			:= self:GetContent()
Local lHabil		:= .T.
Local lDemit		:= .F.
Local lRet			:= .F.
Local aDataLogin	:= {}
Local aIdFunc		:= {}

::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')

If "|" $ Self:employeeId
	aIdFunc := STRTOKARR( Self:employeeId, "|" )
	nDataId := Len( aIdFunc )
	If nDataId > 1
		cFilTeam := aIdFunc[1]
		cMatTeam := aIdFunc[2]		
		cEmpTeam := If( nDataId > 2, aIdFunc[3], cEmpTeam )
	EndIf	
Else
	cMatTeam := Self:employeeId
	cFilTeam := cBranchVld
EndIf

aDataLogin  := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
	cBranchVld	:= aDataLogin[5]
	cLogin		:= aDataLogin[2]
	cRD0Cod		:= aDataLogin[3]
	lDemit		:= aDataLogin[6]
EndIf   

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "clockingGeoRegister", @lHabil)
If !lHabil .Or. lDemit
	SetRestFault(400, EncodeUTF8( STR0067 )) //"Permiss�o negada aos servi�os de marca��o por geolocaliza��o!"
	Return (.F.)  
EndIf

If !Empty(cMatTeam) .And. !Empty(cFilTeam)
	If( lRet := UpdGeoClock(cFilTeam, cMatTeam, cBody, @oItem, @cMsg, cEmpTeam ) )
		cJson := FWJsonSerialize(oItem, .F., .F., .T.)
		::SetResponse(cJson)
	Else
		SetRestFault(400, cMsg )
	EndIf
Else
	cMsg := EncodeUTF8(STR0036) //"Essa batida n�o foi desconsiderada porque n�o foram localizados os dados da requisi��o original."
	SetRestFault(400, cMsg )
EndIf

Return( lRet )


WSMETHOD PUT EditVariousClockings PATHPARAM employeeId WSREST Timesheet
	Local aUrlParam		:= ::aUrlParms
	Local cBody				:= ::GetContent()
	Local cJsonObj		:= "JsonObject():New()"
	Local oItem				:= &cJsonObj
	Local oItemDetail	:= &cJsonObj
	Local oResponse		:= &cJsonObj
	Local cToken			:= ""
	Local cBranchVld	:= ""
	Local cMatSRA			:= ""
	Local cLogin			:= ""
	Local cRD0Cod			:= ""
	Local cMsgLog			:= ""
	Local cJson				:= ""
	Local lRet				:= .T.
	Local lHabil			:= .T.
	Local lDemit			:= .F.
	Local aIdFunc			:= {}
	Local aDataLogin	:= {}

	::SetHeader('Access-Control-Allow-Credentials' , "true")

	cToken		:= Self:GetHeader('Authorization')

	aDataLogin := GetDataLogin(cToken, .T.)
	If Len(aDataLogin) > 0
		cMatSRA      := aDataLogin[1]
		cBranchVld   := aDataLogin[5]
		cLogin       := aDataLogin[2]
		cRD0Cod      := aDataLogin[3]
		lDemit       := aDataLogin[6]
	EndIf

	//Valida Permissionamento
	fPermission(cBranchVld, cLogin, cRD0Cod, "clockingUpdate", @lHabil)
	If !lHabil .Or. lDemit
		SetRestFault(400, EncodeUTF8( STR0066 )) //"Permiss�o negada aos servi�os de marca��o de ponto!"
		Return (.F.)  
	EndIf

	If !( "current" $ Self:employeeId )
		aIdFunc := STRTOKARR( Self:employeeId, "|" )
		If Empty(aIdFunc)
			aIdFunc := {}			
		EndIf
	EndIf

    lRet := fEditClocking(aUrlParam, DecodeUTF8(cBody), @cJsonObj, @oItem, @oItemDetail, cToken, @cMsgLog, aIdFunc)
    If lRet
        oResponse["date"]               := oItemDetail["date"]
        oResponse["clockings"]          := aClone(oItemDetail["clockings"])
        
        cJson := FWJsonSerialize(oResponse, .F., .F., .T.)
        ::SetResponse(cJson)
    Else
        SetRestFault(400, EncodeUTF8(cMsgLog))
    EndIf

Return lRet


WSMETHOD POST SetVariousClockings PATHPARAM employeeId WSREST Timesheet
	Local aUrlParam			:= ::aUrlParms
	Local cBody					:= ::GetContent()
	Local cJsonObj			:= "JsonObject():New()"
	Local oItem					:= &cJsonObj
	Local oItemDetail		:= &cJsonObj
	Local cToken				:= ""
	Local cBranchVld		:= ""
	Local cMatSRA				:= ""
	Local cLogin				:= ""
	Local cRD0Cod				:= ""
	Local cStatus				:= ""
	Local cMsgLog				:= ""
	Local cJson         := ""
	Local lHabil				:= .T.
	Local lDemit				:= .F.
	Local lRet					:= .T.
	Local lRobot				:= .F.
	Local aIdFunc       := {}
	Local aDataLogin		:= {}

	::SetHeader('Access-Control-Allow-Credentials' , "true")
	cToken		:= Self:GetHeader('Authorization')

	aDataLogin := GetDataLogin(cToken, .T.)
	If Len(aDataLogin) > 0
		cMatSRA      := aDataLogin[1]
		cBranchVld   := aDataLogin[5]
		cLogin       := aDataLogin[2]
		cRD0Cod      := aDataLogin[3]
		lDemit       := aDataLogin[6]
	EndIf	

	//Valida Permissionamento
	fPermission(cBranchVld, cLogin, cRD0Cod, "clockingRegister", @lHabil)
	If !lHabil .Or. lDemit
		SetRestFault(400, EncodeUTF8( STR0066 )) //"Permiss�o negada aos servi�os de marca��o de ponto!"
		Return (.F.)  
	EndIf

    //Verifica se eh o gestor que esta fazendo a solicitacao
	If !( "current" $ Self:employeeId )
		aIdFunc := STRTOKARR( Self:employeeId, "|" )
		If Empty(aIdFunc)
			aIdFunc := {}			
		EndIf
	EndIf

    lRet := fSetClocking(aUrlParam, DecodeUTF8(cBody), @cJsonObj, @oItem, @oItemDetail, cToken, @cStatus, @cMsgLog, aIdFunc, @lRobot)
    If lRet
        If !Empty(cStatus)
            ::SetHeader('Status', cStatus)
        EndIf
        cJson := FWJsonSerialize(oItem, .F., .F., .T.)
        ::SetResponse(cJson)
    Else
        If lRobot
			lRet := .T.
			oItem        			:= &cJsonObj
			oItem["errorCode"] 		:= "400"
			oItem["errorMessage"] 	:= cMsgLog
			cJson := FWJsonSerialize(oItem, .F., .F., .T.)
			Self:SetResponse(cJson)
		Else
        	SetRestFault(400, EncodeUTF8(cMsgLog))
		EndIf
    EndIf
Return lRet

WSMETHOD PUT EditOneClockings PATHPARAM employeeId WSREST Timesheet
	Local aUrlParam			:= ::aUrlParms
	Local cBody					:= ::GetContent()
	Local cJsonObj			:= "JsonObject():New()"
	Local oItem					:= &cJsonObj
	Local oItemDetail		:= &cJsonObj
	Local cToken				:= ""
	Local cBranchVld		:= ""
	Local cMatSRA				:= ""
	Local cLogin				:= ""
	Local cRD0Cod				:= ""
	Local cMsgLog				:= ""
	Local cJson					:= ""
	Local lRet					:= .T.
	Local lHabil				:= .T.
	Local lDemit				:= .F.
	Local aIdFunc				:= {}
	Local aDataLogin		:= {}

	::SetHeader('Access-Control-Allow-Credentials' , "true")

	cToken		:= Self:GetHeader('Authorization')

	aDataLogin := GetDataLogin(cToken, .T.)
	If Len(aDataLogin) > 0
		cMatSRA      := aDataLogin[1]
		cBranchVld   := aDataLogin[5]
		cLogin       := aDataLogin[2]
		cRD0Cod      := aDataLogin[3]
		lDemit       := aDataLogin[6]
	EndIf	

	//Valida Permissionamento
	fPermission(cBranchVld, cLogin, cRD0Cod, "clockingUpdate", @lHabil)
	If !lHabil .Or. lDemit
		SetRestFault(400, EncodeUTF8( STR0066 )) //"Permiss�o negada aos servi�os de marca��o de ponto!"
		Return (.F.)  
	EndIf

	If !( "current" $ Self:employeeId )
		aIdFunc := STRTOKARR( Self:employeeId, "|" )
		If Empty(aIdFunc)
			aIdFunc := {}			
		EndIf
	EndIf

    lRet := fEditClocking(aUrlParam, DecodeUTF8(cBody), @cJsonObj, @oItem, @oItemDetail, cToken, @cMsgLog, aIdFunc)
    If lRet
        cJson := FWJsonSerialize(oItemDetail, .F., .F., .T.)
        ::SetResponse(cJson)
    Else
        SetRestFault(400, EncodeUTF8(cMsgLog))
    EndIf

Return lRet


WSMETHOD DELETE dClocking WSREST Timesheet

	Local lRet				:= .T.
	Local lHabil			:= .T.
	Local lDemit			:= .F.
	Local aUrlParam		:= Self:aUrlParms
	Local cCodigo			:= ""
	Local cToken			:= ""
	Local cBranchVld	:= ""
	Local cMatSRA			:= ""
	Local cLogin			:= ""
	Local cRD0Cod			:= ""
	Local aDataLogin	:= {}
	Local aRet				:= { .F., ""}
		
	::SetContentType("application/json")
	::SetHeader('Access-Control-Allow-Credentials' , "true")

	cToken		:= Self:GetHeader('Authorization')

	aDataLogin := GetDataLogin(cToken, .T.)
	If Len(aDataLogin) > 0
		cMatSRA      := aDataLogin[1]
		cBranchVld   := aDataLogin[5]
		cLogin       := aDataLogin[2]
		cRD0Cod      := aDataLogin[3]
		lDemit       := aDataLogin[6]
	EndIf	
	
	//Valida Permissionamento
	fPermission(cBranchVld, cLogin, cRD0Cod, "clockingRegister", @lHabil)
	If !lHabil .Or. lDemit
		SetRestFault(400, EncodeUTF8( STR0066 )) //"Permiss�o negada aos servi�os de marca��o de ponto!"
		Return (.F.)  
	EndIf

	If AT("|", aUrlParam[3]) > 0
		cCodigo := STRTOKARR( aUrlParam[3] , "|" )[2]
		aRet := DelBatida(cBranchVld, cMatSRA, cCodigo)
	Else
		aRet[2] := EncodeUTF8(STR0033) //"Essa batida n�o pode ser exclu�da. O seu Tipo ou Status atual n�o permite a exclus�o."
	EndIf
	
	If aRet[1]
	   	HttpSetStatus(204)
   	Else	
	   	lRet := .F.
		SetRestFault(400, aRet[2])
	EndIf  
Return lRet


WSMETHOD DELETE dAllowance WSREST Timesheet

Local aUrlParam		:= Self:aUrlParms
Local aKeyRH3			:= {}
Local aDataLogin	:= {}
Local lRet				:= .T.
Local lHabil			:= .T.
Local lDemit			:= .F.
Local cErro				:= ""
Local cToken			:= ""
Local cBranchVld	:= ""
Local cMatSRA			:= ""
Local cLogin			:= ""
Local cRD0Cod			:= ""

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
	cMatSRA      := aDataLogin[1]
	cBranchVld   := aDataLogin[5]
	cLogin       := aDataLogin[2]
	cRD0Cod      := aDataLogin[3]
	lDemit       := aDataLogin[6]
EndIf	

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "allowance", @lHabil)
If !lHabil .Or. lDemit
	SetRestFault(400, EncodeUTF8( STR0065 )) //"Permiss�o negada aos servi�os de abono!"
	Return (.F.)  
EndIf

	If Len(::aUrlParms) >= 3

        aKeyRH3 := STRTOKARR( aUrlParam[3] , "|" )
		If Len(aKeyRH3) != 2
			lRet     := .F.
			cErro    := EncodeUTF8(STR0059) //"chave invalida de pesquisa da requisi��o!"    
        EndIf

		If lRet .and. (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
			If !Empty(::aUrlParms[2])

				aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
				If Len(aIdFunc) > 1 
				
					//valida se o solicitante da requisi��o pode ter acesso as informa��es
					If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
						cFilFunc := aIdFunc[1]
						cMatFunc := aIdFunc[2]
					Else
						lRet     := .F.
						cErro    := EncodeUTF8(STR0052) //"usu�rio n�o autorizado para exclus�o da solcita��o!"
					EndIf	
				EndIf

			EndIf
		EndIf
	Else
		lRet     := .F.
		cErro    := EncodeUTF8(STR0056) //"par�metros invalidos na requisi��o!"
	EndIF

	If lRet
		Begin Transaction

			DbSelectArea("RH3")
	        DBSetOrder(1)
			If RH3->( dbSeek( aKeyRH3[1] + aKeyRH3[2] ) ) //Filial+Cod RH3

                //confirma se o registro ponterado � da solicita��o desejada
                If !(RH3->RH3_CODIGO == aKeyRH3[2])
					lRet  := .F.
					cErro := EncodeUTF8(STR0055) //"solicita��o de abono n�o localizada!"
                Endif

				If lRet .and. (RH3->RH3_STATUS == '2' .or. RH3->RH3_STATUS == '3' .or. RH3->RH3_TIPO != '8')
					lRet  := .F.
					cErro := EncodeUTF8(STR0053) //"n�o ser� poss�vel excluir a solicita��o!"
				Else
                    If  !empty(RH3->RH3_FILINI+RH3->RH3_MATINI)
						 If	(RH3->RH3_FILINI + RH3->RH3_MATINI) != (cBranchVld + cMatSRA)
							lRet  := .F.
							cErro := EncodeUTF8(STR0054) //"usu�rio sem permiss�o para excluir a solicita��o!"
		 				 EndIf
                    ElseIf RH3->RH3_MAT != cMatSRA
							lRet  := .F.
							cErro := EncodeUTF8(STR0054) //"usu�rio sem permiss�o para excluir a solicita��o!"
                    EndIf

	                //Valida movimenta��o do workflow
                    If lRet 
					   cErro := EncodeUTF8( fVldWkf(RH3->RH3_FILIAL+RH3->RH3_CODIGO, RH3->RH3_CODIGO, "D") )

                       If !empty(cErro)
	                       lRet := .F.
                       EndIf
                    EndIf

                    If lRet
						//elimina registro inicial(seq 001) do hist�rico RGK
						DbSelectArea("RGK")
				        DBSetOrder(1)
                        If RGK->( dbSeek( aKeyRH3[1] + RH3->RH3_MAT + aKeyRH3[2] ) )
                            RecLock( "RGK", .F. )
                            RGK->( dbDelete() )
                            RGK->( MsUnlock() )
                        EndIf

                        //Processa elimina��o da solicita��o
						DbSelectArea("RH3")
						RecLock( "RH3", .F. )
						RH3->( dbDelete() )
						RH3->( MsUnlock() )

						DbSelectArea("RH4")
				        DBSetOrder(1)
						If RH4->( dbSeek( aKeyRH3[1] + aKeyRH3[2] ) )
							While RH4->(!Eof())
								If RH4->RH4_CODIGO == aKeyRH3[2]
									RecLock( "RH4", .F. )
									RH4->( dbDelete() )
									RH4->( MsUnlock() )	             
								EndIf
								RH4->( dbSkip() )
							EndDo
						EndIf

					EndIF   
				EndIf
			Else
				lRet  := .F.
				cErro := EncodeUTF8(STR0055) //"solicita��o de abono n�o localizada!"
			EndIf	

		End Transaction
    EndIf
	
	If lRet
	   	HttpSetStatus(204)
   	Else	
	   	lRet := .F.
		SetRestFault(400, cErro)
	EndIf  
Return lRet


WSMETHOD PUT editAllowance WSREST Timesheet

Local aUrlParam		:= Self:aUrlParms
Local aKeyRH3		:= {}
Local aDataLogin	:= {}
Local lRet			:= .T.
Local lDemit		:= .F.
Local lHabil		:= .T.
Local cErro			:= ""
Local cToken		:= ""
Local cBranchVld	:= ""
Local cFilToReq		:= ""
Local cMatToReq		:= ""
Local cLogin		:= ""
Local cRD0Cod		:= ""

Local cJustify		:= ""
Local cReason		:= ""
Local cReasonDesc	:= ""
Local cInitDate		:= ""
Local cEndDate		:= ""
Local aMsToHour		:= {}

Local cBody			:= ::GetContent()
Local cJsonObj		:= "JsonObject():New()"
Local oItemDetail	:= &cJsonObj

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')

aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cLogin       := aDataLogin[2]
   cRD0Cod      := aDataLogin[3]
   lDemit       := aDataLogin[6]
   cMatSRA      := cMatToReq := aDataLogin[1]
   cBranchVld   := cFilToReq := aDataLogin[5]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "allowance", @lHabil)
If !lHabil .Or. lDemit
	SetRestFault(400, EncodeUTF8( STR0065 )) //"Permiss�o negada aos servi�os de abono!"
	Return (.F.)  
EndIf

If Len(::aUrlParms) >= 3

	aKeyRH3 := STRTOKARR( aUrlParam[3] , "|" )
	If Len(aKeyRH3) != 2
		lRet     := .F.
		cErro    := EncodeUTF8(STR0059) //"chave invalida de pesquisa da requisi��o!"    
	EndIf

	If lRet .and. (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
		If !Empty(::aUrlParms[2])

			aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
			If Len(aIdFunc) > 1 
			
				//valida se o solicitante da requisi��o pode ter acesso as informa��es
				If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
					cFilToReq := aIdFunc[1]
					cMatToReq := aIdFunc[2]
				Else
					lRet     := .F.
					cErro    := EncodeUTF8(STR0052) //"usu�rio n�o autorizado para exclus�o da solcita��o!"
				EndIf	
			EndIf

		EndIf
	EndIf
Else
	lRet     := .F.
	cErro    := EncodeUTF8(STR0056) //"par�metros invalidos na requisi��o!"
EndIF

If Empty(cBody)
	lRet  := .F.
	cErro := EncodeUTF8(STR0061) //"Informa��es da requisi��o n�o recebida"
EndIf

If lRet
	DbSelectArea("RH3")
	DBSetOrder(1)
	If RH3->( dbSeek( aKeyRH3[1] + aKeyRH3[2] ) ) //Filial+Cod RH3

		//confirma se o registro ponterado � da solicita��o desejada
		If !(RH3->RH3_CODIGO == aKeyRH3[2])
			lRet  := .F.
			cErro := EncodeUTF8(STR0055) //"solicita��o de abono n�o localizada!"
		Endif

		If lRet .and. (RH3->RH3_STATUS == '2' .or. RH3->RH3_STATUS == '3' .or. RH3->RH3_TIPO != '8')
			lRet  := .F.
			cErro := EncodeUTF8(STR0062) //"n�o ser� poss�vel atualizar a solicita��o!"
		Else
			If  !empty(RH3->RH3_FILINI+RH3->RH3_MATINI)
				If	(RH3->RH3_FILINI + RH3->RH3_MATINI) != (cBranchVld + cMatSRA)
					lRet  := .F.
					cErro := EncodeUTF8(STR0063) //"usu�rio sem permiss�o para alterar a solicita��o!"
				EndIf
			ElseIf RH3->RH3_MAT != cMatSRA
				lRet  := .F.
				cErro := EncodeUTF8(STR0063) //"usu�rio sem permiss�o para alterar a solicita��o!"
			EndIf

			//Valida movimenta��o do workflow
			If lRet 
				cErro := EncodeUTF8( fVldWkf(RH3->RH3_FILIAL+RH3->RH3_CODIGO, RH3->RH3_CODIGO, "U") )

				If !empty(cErro)
					lRet := .F.
				EndIf
			EndIf
		EndIf
	Else
		lRet  := .F.
		cErro := EncodeUTF8(STR0055) //"solicita��o de abono n�o localizada!"
	EndIf	

	If lRet  
		oItemDetail:FromJson(cBody)
		cInitDate   := Iif(oItemDetail:hasProperty("initDate"),Format8601(.T.,oItemDetail["initDate"]),"")
		cEndDate    := Iif(oItemDetail:hasProperty("endDate"),Format8601(.T.,oItemDetail["endDate"]),"")
		aMsToHour   := milisSecondsToHour(oItemDetail["initHour"],oItemDetail["endHour"])
		cReason     := Iif(oItemDetail:hasProperty("allowanceType"),oItemDetail["allowanceType"]["id"]," ")
		cReasonDesc := Iif(oItemDetail:hasProperty("allowanceType"),oItemDetail["allowanceType"]["description"]," ")
		cJustify    := oItemDetail["justify"]

		//Verifica se n�o existe abono cadastrado para a mesma data e hora
		If GetJustification( cFilToReq, cMatToReq, CToD(cInitDate), CToD(cEndDate), aMsToHour[1], aMsToHour[2], aKeyRH3[2] )

			//efetua atualiza��o RH4
			Begin Transaction
				DBSelectArea("RH4")
				DBSetOrder(1)
				RH4->(DbSeek(RH3->(RH3_FILIAL + RH3_CODIGO)))

				While RH4->(RH4_FILIAL + RH4_CODIGO) == RH3->(RH3_FILIAL + RH3_CODIGO) .And. !RH4->(Eof())

					RecLock("RH4", .F.)
						If AllTrim(RH4->RH4_CAMPO) == "RF0_DTPREI"
							RH4->RH4_VALANT := RH4->RH4_VALNOV
							RH4->RH4_VALNOV := cInitDate
						ElseIf AllTrim(RH4->RH4_CAMPO) == "RF0_DTPREF"
							RH4->RH4_VALANT := RH4->RH4_VALNOV
							RH4->RH4_VALNOV := cEndDate
						ElseIf AllTrim(RH4->RH4_CAMPO) == "RF0_HORINI"
							RH4->RH4_VALANT := RH4->RH4_VALNOV
							RH4->RH4_VALNOV := alltrim(str( noround(aMsToHour[1], 2) ))
						ElseIf AllTrim(RH4->RH4_CAMPO) == "RF0_HORFIM"
							RH4->RH4_VALANT := RH4->RH4_VALNOV
							RH4->RH4_VALNOV := alltrim(str( noround(aMsToHour[2], 2) ))
						ElseIf AllTrim(RH4->RH4_CAMPO) == "RF0_CODABO"
							RH4->RH4_VALANT := RH4->RH4_VALNOV
							RH4->RH4_VALNOV := cReason
						ElseIf AllTrim(RH4->RH4_CAMPO) == "TMP_ABOND"
							RH4->RH4_VALANT := RH4->RH4_VALNOV
							RH4->RH4_VALNOV := DecodeUTF8( cReasonDesc )
						ElseIf AllTrim(RH4->RH4_CAMPO) == "RF0_HORTAB"
							RH4->RH4_VALANT := RH4->RH4_VALNOV
															If empty(aMsToHour[1]) 
								RH4->RH4_VALNOV := "S"
							Else
								RH4->RH4_VALNOV := "N"
							EndIf	
						EndIf
					MsUnLock()

					RH4->(DbSkip())
				EndDo

				//Atualiza RGK                    
				If !empty(cJustify)
					DBSelectArea("RGK")
					DBSetOrder(1) //RGK_FILIAL +RGK_MAT +RGK_CODIGO +RGK_SEQUEN +RGK_DATA
					If RGK->(DbSeek( xFilial("RGK",RH3->RH3_FILIAL) + RH3->RH3_MAT + RH3->RH3_CODIGO , .T. ))

						DBSelectArea("RDY")
						DBSetOrder(2) //RDY_FILTAB +RDY_CHAVE +RDY_SEQ
						//If RDY->(DbSeek( (cQueryRGK)->RGK_FILIAL + (cQueryRGK)->RGK_CODCON +'001' ))
						If RDY->(DbSeek( RGK->RGK_FILIAL + RGK->RGK_CODCON +'001' ))
							RecLock("RDY", .F.)
							RDY->RDY_TEXTO := alltrim(cJustify)
							MsUnLock()
						EndIf
					EndIf	
				EndIf

			End Transaction
		Else
			lRet  := .F.
			cErro := EncodeUTF8(STR0007) //"J� existe abono cadastrado para essa data/hora"
		EndIf
	EndIf
EndIf
	
If lRet
	HttpSetStatus(204)
Else	
	lRet := .F.
	SetRestFault(400, cErro)
EndIf  

Return lRet