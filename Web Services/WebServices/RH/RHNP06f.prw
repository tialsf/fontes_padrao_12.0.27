#INCLUDE "TOTVS.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "RESTFUL.CH"

#INCLUDE "RHNP06.CH"
#INCLUDE "PONCALEN.CH"

Static lIsBlind	:= IsBlind()


Function fSetClocking(aUrlParam, cBody, cJsonObj, oItem, oItemDetail, cToken, cStatus, cMsgLog, aIdFunc, lRobot)
    Local cApprover				:= ""
    Local cVision	 			:= ""
    Local cEmpApr				:= ""
    Local cFilApr				:= ""
    Local nSupLevel				:= 0
    Local aVision				:= {}
    Local aGetStruct			:= {}
    Local aMSToHour 			:= Array(02)
	Local aDataLogin			:= {}
    Local cTypeReq				:= "Z"
    Local cRoutine				:= "W_PWSA400.APW" //Marca��o de Ponto: Utilizada para buscar a VIS�O a partir da rotina; (AI8_VISAPV) na fun��o GetVisionAI8().
    Local cMsgReturn 			:= EncodeUTF8(STR0029) //"Dados atualizados com sucesso."
    Local cBranchVld			:= FwCodFil()
    Local oRequest				:= Nil
    Local oAttendControlRequest	:= Nil
    Local lRet                  := .T.
    Local cAllJustify           := ""
    Local cAllReason            := ""
    Local cEntry                := STR0069 //"Entrada"
    Local cExit                 := STR0070 //"Sa�da"
    Local aTrab                 := {}
	Local aPeriods				:= {}
    Local nA                    := 0
    Local lGestor               := .F.
	Local dPerIni 	 			:= cToD("//")
	Local dPerFim 	 			:= cToD("//")

    Default aUrlParam			:= {}
    Default cBody				:= ""
    Default cJsonObj			:= "JsonObject():New()"
    Default oItem 	 			:= &cJsonObj
    Default oItemDetail			:= &cJsonObj
    Default cToken				:= ""
    Default cStatus				:= ""
    Default cMsgLog             := ""
    Default aIdFunc             := {}
	Default lRobot				:= .F.

    oRequest					:= WSClassNew("TRequest")
    oRequest:RequestType		:= WSClassNew("TRequestType")
    oRequest:Status				:= WSClassNew("TRequestStatus")
    oAttendControlRequest		:= WSClassNew("TAttendControl")

    If !Empty( aIdFunc )
	    cBranchVld 	:= aIdFunc[1]
	    cMatSRA		:= aIdFunc[2]
	    lGestor		:= .T.
    Else
		aDataLogin  := GetDataLogin(cToken)
		If Len(aDataLogin) > 0
			cMatSRA		:= aDataLogin[1]
			cRD0Cod    	:= aDataLogin[3]
			cBranchVld 	:= aDataLogin[5]
		EndIf   	
    EndIf

    If !Empty(cBody)
        oItemDetail:FromJson(cBody)
		
		//Busca per�odo ativo no ponto conforme MV_PONMES.
		aPeriods := GetPerAponta( 1 , cBranchVld , cMatSRA, .F.)
		If Len(aPeriods) > 0
			dPerIni := aPeriods[1,1]
			dPerFim := aPeriods[1,2]
		EndIf
        
        //Em a justificativa tiver sido informada para todas as batidas 
        cAllJustify := If(oItemDetail:hasProperty("justify"),oItemDetail["justify"],"")
        cAllReason  := If(oItemDetail:hasProperty("reason"),oItemDetail["reason"],"")
		lRobot		:= If(oItemDetail:hasProperty("execRobo"), oItemDetail["execRobo"], "0") == "1"
        
        If oItemDetail:hasProperty("clockings") .And. ValType(oItemDetail["clockings"]) == "A"
            For nA := 1 To len(oItemDetail["clockings"])

                //Verifica se a justificativa para multiplas marca��es foi preenchida.
                If Empty(cAllJustify) .And. If( oItemDetail["clockings"][nA]:hasProperty("justify"), Empty(oItemDetail["clockings"][nA]["justify"]), .T. )
                  lRet := .F.
                  Exit
                EndIf
            
                AAdd(aTrab,{;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("hour"),oItemDetail["clockings"][nA]["hour"]," "),;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("date"),oItemDetail["clockings"][nA]["date"]," "),;
                    Iif(!Empty(cAllJustify), cAllJustify, oItemDetail["clockings"][nA]["justify"] ),;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("referenceDate"),oItemDetail["clockings"][nA]["referenceDate"]," "),;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("direction"),oItemDetail["clockings"][nA]["direction"]," "),;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("origin"),oItemDetail["clockings"][nA]["origin"]," "),;
                    Iif(!Empty(cAllReason), cAllReason, oItemDetail["reason"]);                    
                })
            Next nA
        Else

	       	//Verifica se o motivo e a justificativa da marca��o individual foram preenchidos.
	       	lRet := If( oItemDetail:hasProperty("justify"), !Empty(oItemDetail["justify"]), .F.)
        
            AAdd(aTrab,{;
                Iif(oItemDetail:hasProperty("hour"),oItemDetail["hour"]," "),;
                Iif(oItemDetail:hasProperty("date"),oItemDetail["date"]," "),;
                oItemDetail["justify"],;
                Iif(oItemDetail:hasProperty("referenceDate"),oItemDetail["referenceDate"]," "),;
                Iif(oItemDetail:hasProperty("direction"),oItemDetail["direction"]," "),;
                Iif(oItemDetail:hasProperty("origin"),oItemDetail["origin"]," "),;
                Iif(oItemDetail:hasProperty("reason"),oItemDetail["reason"]["id"]," ");
            })
        EndIf
        
        //-- Verifica se o campo motivo foi informado
        If lRet .And. !Empty(aTrab[1][7])
            // ----------------------------------------------
            // - A Fun��o GetVisionAI8() devolve por padr�o
            // - Um Array com a seguinte estrutura:
            // - aVision[1][1] := "" - AI8_VISAPV
            // - aVision[1][2] := 0  - AI8_INIAPV
            // - aVision[1][3] := 0  - AI8_APRVLV
            // - Por isso as posi��o podem ser acessadas
            // - Sem problemas, ex: cVision := aVision[1][1]
            // ----------------------------------------------
            aVision := GetVisionAI8(cRoutine, cBranchVld)
            cVision := aVision[1][1]

            // -------------------------------------------------------------------------------------------
            // - Efetua a busca dos dados referentes a Estrutura Oreganizacional dos dados da solicita��o.
            // - Obs.: Nao e necessario quando o gestor solicita ao subordinado, porque ja entra aprovado.
            //- -------------------------------------------------------------------------------------------
            If !lGestor
				cMRrhKeyTree := fMHRKeyTree(cBranchVld, cMatSRA)
	            aGetStruct := APIGetStructure(cRD0Cod, SUPERGETMV("MV_ORGCFG"), cVision, cBranchVld, cMatSRA, , , ,cTypeReq , cBranchVld, cMatSRA, , , , , .T., {cEmpAnt})

				If valtype(aGetStruct[1]) == "L" .and. !aGetStruct[1]
					cMsgLog := AllTrim( EncodeUTF8(aGetStruct[2]) +" - " +EncodeUTF8(aGetStruct[3]) )
					lRet 	:= .F.
				Else
					If Len(aGetStruct) >= 1 .And. !(Len(aGetStruct) == 3 .And. !aGetStruct[1])
						cEmpApr   := aGetStruct[1]:ListOfEmployee[1]:SupEmpresa
						cFilApr   := aGetStruct[1]:ListOfEmployee[1]:SupFilial
						nSupLevel := aGetStruct[1]:ListOfEmployee[1]:LevelSup
						cApprover := aGetStruct[1]:ListOfEmployee[1]:SupRegistration
					EndIf
				EndIf
            EndIf

            If Len(aTrab) > 0 .And. lRet
				//Valida se as marca��es n�o est�o com data inferior ao per�odo em aberto no MV_PONMES.
				lRet := fVldMarc(aTrab, dPerIni, dPerFim, @cMsgLog)
                If lRet 
					For nA := 1 To Len(aTrab)
						aMsToHour	:= milisSecondsToHour(aTrab[nA][1],aTrab[nA][1])
						//-- Verifica se data da batida � menor/igual data atual
						If cTod(Format8601(.T.,aTrab[nA][2])) <= dDataBase
							oRequest:Branch						:= cBranchVld
							oRequest:StarterBranch				:= cBranchVld
							oRequest:StarterRegistration		:= cMatSRA
							oRequest:Registration				:= cMatSRA
							oRequest:ApproverBranch				:= cFilApr
							oRequest:ApproverRegistration		:= cApprover
							oRequest:EmpresaAPR					:= cEmpApr
							oRequest:Empresa					:= cEmpAnt
							oRequest:ApproverLevel				:= nSupLevel
							oRequest:Vision						:= cVision

							oAttendControlRequest:Branch  		:= cBranchVld
							oAttendControlRequest:Registration	:= cMatSRA
							oAttendControlRequest:Name			:= Alltrim(Posicione('SRA',1,cBranchVld+cMatSRA,'SRA->RA_NOME'))
							oAttendControlRequest:EntryExit     := Iif(Alltrim(aTrab[nA][5])=="entry", cEntry, cExit)
							oAttendControlRequest:Observation  	:= GetMotDesc(aTrab[nA][7])
							oAttendControlRequest:Motive        := aTrab[nA][3]
							oAttendControlRequest:Date			:= Format8601(.T.,aTrab[nA][2])
							oAttendControlRequest:Hour			:= cValToChar(aMsToHour[1])

							AddAttendControlRequest(oRequest, oAttendControlRequest, .T., @cMsgReturn, STR0038 ) //"MEURH"
						Else
							cMsgLog := STR0024
							lRet    := .F.
							Exit
						EndIf
					Next nA
				EndIf
            EndIf
        Else
            cMsgLog := STR0071 //"Os campos 'Motivo' e 'Justificativa' devem ser informados!"
            lRet    := .F.
        EndIf
    EndIf

Return lRet


/*/{Protheus.doc}GetAllowances
@author:	Matheus Bizutti
@since:		18/08/2017
/*/
Function GetAllowances(cJsonObj,oItemData,oAllowType,aData,cBranchVld,cMatSRA)

Local cQuery   := GetNextAlias()
Local cBrchSP6 := ""
Local aProps   := fGetPropAllowance()
Local oProps   := Nil

Default cJsonObj	:= "JsonObject():New()"
Default oItemData	:= &cJsonObj
Default oAllowType	:= &cJsonObj
Default aData		:= {}
Default cBranchVld	:= FwCodFil()
Default cMatSRA		:= ""

cBrchSP6 := xFilial("SP6", cBranchVld)
oProps   := &cJsonObj

BEGINSQL ALIAS cQuery

	SELECT *
	FROM
		%Table:SP6% SP6
	WHERE
		SP6.P6_FILIAL = %Exp:cBrchSP6% AND
		SP6.%NotDel%
ENDSQL

If !Empty(cQuery)
	While !(cQuery)->(Eof())

	If (cQuery)->P6_PREABO == "S"
		oAllowType := &cJsonObj

		oAllowType["id"]  		  := EncodeUTF8((cQuery)->P6_CODIGO)
		oAllowType["description"] := EncodeUTF8( AllTrim((cQuery)->P6_DESC) )
		oAllowType["type"]        := "hour"
		oAllowType["props"]		  := aProps

		aAdd(aData, oAllowType)
		oAllowType := Nil

	EndIf
	(cQuery)->(dbSkip())

	EndDo
EndIf

(cQuery)->(dbCloseArea())

Return(Nil)



Function AllowanceRequest(aUrlParam,cBody,cJsonObj,oItem,oItemDetail,cToken,cStatus,aIdFunc)

Local oMessages		:= Nil
Local oRequest		:= Nil
Local cApprover		:= ""
Local cVision	 	:= ""
Local cEmpApr		:= ""
Local cFilApr		:= ""
Local nSupLevel		:= 0
Local aGetStruct	:= {}
Local aDataLogin	:= {}
Local cTypeReq		:= "8"
Local aVision		:= {}
Local cRoutine		:= "W_PWSA160.APW" // Justifica de Abono: Utilizada para buscar a VIS�O a partir da rotina; (AI8_VISAPV) na fun��o GetVisionAI8().
Local cReason		:= ""
Local cBranchVld	:= FwCodFil()
Local cMatSRA		:= ""
Local cFilToReq		:= ""
Local cMatToReq		:= ""
Local cInitDate		:= ""
Local cEndDate		:= ""
Local cMsgReturn 	:= EncodeUTF8(STR0029) //"Dados atualizados com sucesso."
Local aMSToHour 	:= Array(02)
Local cRD0Cod	 	:= ""
Local lIncAbono		:= .F.
Local lRet			:= .T.
Local oScheduleJustificationRequest := Nil

Default aUrlParam		:= {}
Default cBody			:= ""
Default cJsonObj		:= "JsonObject():New()"
Default oItem 	 		:= &cJsonObj
Default oItemDetail		:= &cJsonObj
Default cToken			:= ""
Default aIdFunc			:= {}

oMessages 						:= &cJsonObj
oRequest						:= WSClassNew("TRequest")
oRequest:RequestType			:= WSClassNew("TRequestType")
oRequest:Status					:= WSClassNew("TRequestStatus")
oScheduleJustificationRequest	:= WSClassNew("TScheduleJustification")

aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
	cRD0Cod		:= aDataLogin[3]
	cMatSRA		:= cMatToReq := aDataLogin[1]
	cBranchVld	:= cFilToReq := aDataLogin[5]	
EndIf

If !Empty(cBody)

	oItemDetail:FromJson(cBody)

	// ----------------------------------------------
	// - A Fun��o GetVisionAI8() devolve por padr�o
	// - Um Array com a seguinte estrutura:
	// - aVision[1][1] := "" - AI8_VISAPV
	// - aVision[1][2] := 0  - AI8_INIAPV
	// - aVision[1][3] := 0  - AI8_APRVLV
	// - Por isso as posi��o podem ser acessadas
	// - Sem problemas, ex: cVision := aVision[1][1]
	// ----------------------------------------------
	aVision := GetVisionAI8(cRoutine, cBranchVld)
	cVision := aVision[1][1]

	// -------------------------------------------------------------------------------------------
	// - Efetua a busca dos dados referentes a Estrutura Oreganizacional dos dados da solicita��o.
	//- -------------------------------------------------------------------------------------------
	 cMRrhKeyTree := fMHRKeyTree(cBranchVld, cMatSRA)
	 aGetStruct := APIGetStructure(cRD0Cod, SUPERGETMV("MV_ORGCFG"), cVision, cBranchVld, cMatSRA, , , ,cTypeReq , cBranchVld, cMatSRA, , , , , .T., {cEmpAnt})

	If Valtype(aGetStruct[1]) == "L" .And. !aGetStruct[1]
		cMsgReturn := AllTrim( EncodeUTF8(aGetStruct[2]) +" - " +EncodeUTF8(aGetStruct[3]) )
		lRet := .F.
	Else
		If Len(aGetStruct) >= 1 .And. !(Len(aGetStruct) == 3 .And. !aGetStruct[1])
			cEmpApr   := aGetStruct[1]:ListOfEmployee[1]:SupEmpresa
			cFilApr   := aGetStruct[1]:ListOfEmployee[1]:SupFilial
			nSupLevel := aGetStruct[1]:ListOfEmployee[1]:LevelSup
			cApprover := aGetStruct[1]:ListOfEmployee[1]:SupRegistration
		EndIf
	EndIf
	
	If lRet
		// -------------------------------------------------------------------------------------------
		// Ajusta o solicitante caso a solicitacao esteja sendo feita de um gestor para o subordinado 
		//- -------------------------------------------------------------------------------------------
		If Len(aIdFunc) > 0
			cFilToReq	:= aIdFunc[1]
			cMatToReq	:= aIdFunc[2]
		EndIf

		oRequest:Branch					:= cFilToReq
		oRequest:StarterBranch			:= cBranchVld
		oRequest:Registration           := cMatToReq
		oRequest:StarterRegistration	:= cMatSRA
		oRequest:Observation            := Alltrim(oItemDetail["justify"])
		oRequest:ApproverBranch			:= cFilApr
		oRequest:ApproverRegistration   := cApprover
		oRequest:EmpresaAPR				:= cEmpApr
		oRequest:Empresa                := cEmpAnt
		oRequest:ApproverLevel			:= nSupLevel
		oRequest:Vision                 := cVision

		// --------------------------------
		// - CONVERTE O VALOR QUE VEM EM
		// - DATE TIME ISO 8601 DO CLIENT.
		// --------------------------------
		cInitDate	:= Iif(oItemDetail:hasProperty("initDate"),Format8601(.T.,oItemDetail["initDate"]),"")
		cEndDate	:= Iif(oItemDetail:hasProperty("endDate"),Format8601(.T.,oItemDetail["endDate"]),"")
		cReason 	:= Iif(oItemDetail:hasProperty("allowanceType"),oItemDetail["allowanceType"]["id"]," ")

		aMsToHour := milisSecondsToHour(oItemDetail["initHour"],oItemDetail["endHour"])

		oScheduleJustificationRequest:Reason  		:= cReason
		oScheduleJustificationRequest:InitialDate	:= CToD(cInitDate)
		oScheduleJustificationRequest:FinalDate		:= CToD(cEndDate)
		oScheduleJustificationRequest:InitialTime	:= aMsToHour[1]
		oScheduleJustificationRequest:FinalTime		:= aMsToHour[2]

		//Verifica se n�o existe abono cadastrado para a mesma data e hora
		lIncAbono := GetJustification( cFilToReq, cMatToReq, CToD(cInitDate), CToD(cEndDate), aMsToHour[1], aMsToHour[2] )

		//Funcao que efetua a grava��o da requisi��o no Protheus.
		If lIncAbono
			AddScheduleJustificationRequest(oRequest, oScheduleJustificationRequest, .T., @cMsgReturn, STR0038 ) //"MEURH"
		Else
			cStatus := "400"
			oItem["code"] 		:= cStatus
			oItem["message"]	:= EncodeUTF8(STR0007) //"J� existe abono cadastrado para essa data/hora"
		EndIf
	Else
		cStatus := "400"
		oItem["code"] 		:= cStatus
		oItem["message"]	:= cMsgReturn
	EndIf
EndIf

Return(Nil)



Function getClockings(cJsonObj,aData,cBranchVld,cMatSRA,dPerIni,dPerFim)

Local oClockings	:= Nil

Local cAliasMarc		:= "SP8"
Local lGetMarcAuto		:= ( SuperGetMv( "MV_GETMAUT" , NIL , "S" , cFilAnt ) == "S" )
Local nI				:= 0
Local nY				:= 0
Local nA                := 0
Local nCount			:= 0
Local nTamM				:= 0
Local nPos              := 0
Local cJson				:= "JsonObject():New()"
Local cID 				:= ""
Local oStatus 			:= ""
Local cRetStatus		:= ""
Local cLabStatus		:= ""
Local aDateGMT			:= {}
Local aMarcAux			:= {}
Local aSaveMarc			:= {}
Local lRegRS3			:= .F.
Local lPeriodo 			:= dPerIni != dPerFim
Local dLastDt			:= Ctod("//")
Local cQueryAlias 		:= Nil
Local aSequen           := {}
Local dAuxIniPer   		:= Ctod("//")
Local dAuxFimPer   		:= Ctod("//")
Local aPeriods			:= {}
Local aMarcGet			:= {}

Private aMarcacoes		:= {}
Private aTabCalend  	:= {}
Private aTabPadrao		:= {}
Private aRecsMarcAutDele:= {}

Default cJsonObj		:= JsonObject():New()
Default aData			:= {}
Default cBranchVld		:= FwCodFil()
Default cMatSRA			:= ""
Default dPerIni     	:= Ctod("//")
Default dPerFim     	:= Ctod("//")

dbSelectArea("SRA")
SRA->(dbSetOrder(1))
If SRA->(dbSeek(cBranchVld+cMatSRA))

	//Verifica se esta sendo solicitado o periodo ou um data especifica
	If lPeriodo
		dAuxIniPer	:= dPerIni
		dAuxFimPer	:= dPerFim
	Else
		aPeriods := GetPerAponta( 1, cBranchVld , cMatSRA, .F.)
		If Len(aPeriods) > 0
			dAuxIniPer := aPeriods[1,1]
			dAuxFimPer := aPeriods[1,2]
		EndIf
	EndIf

	/*/
	�������������������������������������������������������������Ŀ
	� Carrega o Calendario de Marcacoes do Funcionario            �
	���������������������������������������������������������������/*/
	GetMarcacoes(	@aMarcGet			,;	//01 -> Marcacoes do Funcionario
					@aTabCalend			,;	//02 -> Calendario de Marcacoes
					@aTabPadrao			,;	//03 -> Tabela Padrao
					NIL     			,;	//04 -> Turnos de Trabalho
					dAuxIniPer			,;	//05 -> Periodo Inicial
					dAuxFimPer			,;	//06 -> Periodo Final
					SRA->RA_FILIAL		,;	//07 -> Filial
					SRA->RA_MAT			,;	//08 -> Matricula
					SRA->RA_TNOTRAB		,;	//09 -> Turno
					SRA->RA_SEQTURN		,;	//10 -> Sequencia de Turno
					SRA->RA_CC			,;	//11 -> Centro de Custo
					cAliasMarc			,;	//12 -> Alias para Carga das Marcacoes
					.T.					,;	//13 -> Se carrega Recno em aMarcacoes
					.T.		 			,;	//14 -> Se considera Apenas Ordenadas
					NIL					,;  //15 -> Verifica as Folgas Automaticas
					NIL  				,;  //16 -> Se Grava Evento de Folga Mes Anterior
					lGetMarcAuto		,;	//17 -> Se Carrega as Marcacoes Automaticas
					@aRecsMarcAutDele	,;	//18 -> Registros de Marcacoes Automaticas que deverao ser Deletados
					NIL					,;	//19
					NIL					,;	//20
					NIL					,;	//21
					NIL					,;	//22
					.T.					,;	//23 -> Se carrega as marcacoes das duas tabelas SP8 e SPG
					)

	//Quando for uma data especifica considera somente as marcacoes dessa data
	If lPeriodo
		aMarcacoes := aClone( aMarcGet )
	ElseIf !Empty(aMarcGet)
		For nA := 1 To Len(aMarcGet)
			If aMarcGet[nA][1] == dPerIni
				Aadd(aMarcacoes, aMarcGet[nA])
			EndIf
		Next nA
	EndIf
	
	cQueryAlias := GetNextAlias()

	BEGINSQL ALIAS cQueryAlias
		SELECT RS3_DATA, RS3_HORA, RS3_STATUS, RS3_JUSTIF, RS3_CODIGO, RH3_STATUS, RS3_FILIAL, P8_TPMCREP, P8_MOTIVRG
		FROM %table:RS3% RS3
		INNER JOIN %table:RH3% RH3 ON
			RS3_FILIAL = RH3_FILIAL AND
			RS3_CODIGO = RH3_CODIGO
		LEFT JOIN %table:SP8% SP8 ON
			RS3_FILIAL = P8_FILIAL AND
			RS3_MAT = P8_MAT AND
			RS3_DATA = P8_DATA AND
			RS3_HORA = P8_HORA
		WHERE RS3_FILIAL = %exp:SRA->RA_FILIAL% AND
		RS3_MAT = %exp:SRA->RA_MAT% AND
		RS3_DATA >= %exp:DtoS(dPerIni)% AND
		RS3_DATA <= %exp:DtoS(dPerFim)% AND
		RS3.%notDel% AND RH3.%notDel% AND
		( SP8.%notDel% OR SP8.D_E_L_E_T_ IS NULL )
		ORDER BY RS3_FILIAL, RS3_MAT, RS3_DATA, RS3_HORA
	ENDSQL

	While (cQueryAlias)->(!Eof())
		//Se a marca��o n�o estiver sido desconsiderada, ou estiver sido desconsiderada pelo gestor, adiciona no array.
		//As marca��es que foram desconsideradas diretamente dentro do Protheus n�o ser�o apresentadas no espelho de ponto.
		//As marca��es desconsideradas dentro do Protheus ficam: P8_TPMCREP = 'D', RS3_STATUS = 1, RH3_STATUS = '2'
		If ( ( (cQueryAlias)->P8_TPMCREP <> 'D' ) .Or. ;
			(  (cQueryAlias)->P8_TPMCREP == 'D' .And.  (cQueryAlias)->RS3_STATUS == "2" .And. (cQueryAlias)->RH3_STATUS == "3") )
			(cQueryAlias)->( aAdd( aMarcAux,{ ;
											StoD(RS3_DATA),;
											RS3_HORA,;
											If(RS3_STATUS=="0","P",If(RS3_STATUS=="2","R","A")),;
											RS3_JUSTIF,;
											RS3_CODIGO,;
											RH3_STATUS,;
											If(RH3_STATUS $ "2|1|4","01","99") }))
		EndIf
		(cQueryAlias)->(DbSkip())
	EndDo

	(cQueryAlias)->(DbCloseArea())

	nTamM	:= Len(aMarcacoes)

	For nY := 1 To nTamM
		//Ignora marcacoes desconsideradas e incluidas pelo portal, esta �ltima ja foi carregada da tabela RS3
		If aMarcacoes[ nY, 27 ] <> "D" .and. !(aMarcacoes[ nY , 04] == "P")
			//Utiliza a posicao 7 (Funcao do Relogio) para controle de ordenacao porque esse dado nao eh usado no MeuRH
			aMarcacoes[ nY, 7 ] := "01"
			aAdd(aMarcAux, aMarcacoes[nY] )
		EndIf
	Next nY

	aSort(aMarcAux,,,{|x,y| DtoS(x[1])+ x[7]+ StrTran(StrZero(x[2],5,2),".", ":") < DtoS(y[1])+ y[7]+ StrTran(StrZero(y[2],5,2),".", ":") })
	aSaveMarc := aClone(aMarcacoes)
	aMarcacoes := aClone(aMarcAux)

EndIf

For nI := 1 To Len(aMarcacoes)

	lRegRS3 := aMarcacoes[nI, 03] $ "*P*A*R*"
	cID     := cValToChar(nI) + Iif(lRegRS3,"|"+aMarcacoes[nI, 05],"")

	oClockings := &cJson

	oClockings["id"]			:= cID

	aDateGMT 					:= LocalToUTC( DTOS(aMarcacoes[nI][1]), "12:00:00" )
	oClockings["date"]			:= Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")
	aDateGMT := {}

	oClockings["origin"] 		:= fRetTpFlag( aMarcacoes[nI][4], lRegRS3 )

	aDateGMT 					:= LocalToUTC( Iif(!lRegRS3 .And. !Empty(aMarcacoes[nI][25]),DTOS(aMarcacoes[nI][25]), DTOS(aMarcacoes[nI][1])), "12:00:00" )
	oClockings["referenceDate"] := Iif(Empty(aDateGMT),"",Substr(aDateGMT[1],1,4) + "-" + Substr(aDateGMT[1],5,2) + "-" + Substr(aDateGMT[1],7,2) + "T" + "12:00:00" + "Z")
	aDateGMT := {}

	oClockings["hour"]			:= HourToMs(strZero(aMarcacoes[nI][2], 5, 2))

	//Controle de direcao e sequencia da batida (sequencia por pares)
	//1E e 1S ==> sequencia 1,1 respectivamente (sequencia por pares)
	//2E e 2S ==> sequencia 2,2 respectivamente
	If !dLastDt == aMarcacoes[nI, 01]
		nCount  := 1
		aSequen := {}
		aAdd( aSequen, { "1", "" } )
	Else
		nCount ++
		nPos   := Len( aSequen )
		
	 	If Empty( aSequen[nPos,2] )
	 		aSequen[nPos,2] := "1"
	 	Else
	 		aAdd( aSequen, { "1", "" } )
	 	EndIf
	EndIf
	
	oClockings["direction"]	:= If( nCount % 2 == 0, "exit", "entry" )
	oClockings["sequence"]  := Len( aSequen )
	
    If lRegRS3
		//Solicitacao em processo de aprovacao
		If !(aMarcacoes[nI, 06] == "3")
			Do Case
				Case aMarcacoes[nI, 03] == "P"
					cRetStatus	:= "approving"
					cLabStatus	:= STR0013 //"Aguardando aprova��o"
				Case aMarcacoes[nI, 03] == "A"
					cRetStatus	:= "approved"
					cLabStatus	:= STR0014 //"Aprovada"
			End Case
		Else
			//Somente as solicitacoes reprovadas pelo gestor ficam demonstradas no App
			cRetStatus	:= "reproved"
			cLabStatus	:= STR0015 //"Reprovada"
		EndIf
	Else
		//Incluido status para todas as demais marcacoes para desabilitar as opcoes de edicao
		cRetStatus	:= "approved"
		cLabStatus	:= STR0014 //"Aprovada"
	EndIf
	
	oStatus := &cJson
	oStatus["id"]        := cID
	oStatus["status"]    := EncodeUTF8(cRetStatus)
	oStatus["label"]     := EncodeUTF8(cLabStatus)
	oClockings["status"] := oStatus	

	dLastDt	:= aMarcacoes[nI, 01]

	Aadd(aData,oClockings)

Next nI

Return(Nil)

/*/{Protheus.doc} fRetTpFlag
Retorna a descri��o do tipo da marca��o conforme o Flag
@author:	Marcelo Silveira
@since:		04/04/2019
@param:		cFlag - Flag da marcacao;
			lRS3 - Verdadeiro se a marcacao foi incluida pelo App;
@return:	cRet - descricao do tipo da marcacao conforme o flag.
/*/
Function fRetTpFlag( cFlag, lRS3 )

Local cRet := "empty"

DEFAULT cFlag := ""
DEFAULT lRS3  := .F.

//Tipo da marca��o conforme o Flag
If !Empty(cFlag)

	//Quando inclu�da via App o tipo sempre ser� manual
	If lRS3 .And. Len(cFlag) > 1
		cRet := "manual"
	Else
		Do Case
			Case cFlag $ "E"
				 cRet := "clock" 	//Lidas e gravadas atrav�s do rel�gio.
			Case cFlag $ "I"
				cRet := "manual" 	//Informadas (manual)
			Case cFlag $ "A|G"
				cRet := "automatic" //Autom�tica ou Gerada.
			OtherWise
				cRet := "empty"
		End Case
	EndIf
EndIf

Return(cRet)

/*/{Protheus.doc} GetPeriodApont
Retorna os periodos de apontamento
@author:	Matheus Bizutti
@since:		12/04/2017
@param:		cBranchVld - Filial;
			cMatSRA - Matricula;
			nNumPer - Numero de per�odos que serao retornados;
@return:	Array - periodos de apontamento.
/*/
Function GetPeriodApont(cBranchVld, cMatSRA, nNumPer, cEmpJob, lJob, cUID)

	Local aData 		:= {}
	Local aPerSPO 		:= {}

	Default cBranchVld	:= FwCodFil()
	Default cMatSRA		:= Nil
	Default nNumPer		:= 1
	Default cEmpJob		:= cEmpAnt
	Default lJob		:= .F.

	If lJob
		//Instancia o ambiente para a empresa onde a funcao sera executada
		RPCSetType( 3 )
		RPCSetEnv( cEmpJob, cBranchVld )
	EndIf

	//Ao retornar o historico de periodos considera dados da SPO
	If nNumPer > 1
		aPerSPO	:= fPerSPO(cEmpJob, cBranchVld)
	EndIf
	aData := GetPerAponta( nNumPer, cBranchVld, cMatSRA, .F., , , aPerSPO )

	If lJob
		//Atualiza a variavel de controle que indica a finalizacao do JOB
		PutGlbValue(cUID, "1")
	EndIf

Return( aData )


/*/{Protheus.doc}GetJustification
Verifica se ja existe abono cadastrado para o dia o funcionario no dia/hora informado
@author:	Marcelo Silveira
@since:		18/02/2019
@param:		cFilSra - Filial;
			cMatSra - Matr�cula;
			cInitDate - Data in�cial do Abono;
			cEndDate - Data final do Abono;
			cInitHour - Hora inicial do Abono
			cEndHour - Hora final do Abono;
			cKeyUpd - Codigo do registro que esta sendo alterado (nao considera esse registro);
@return:	lRet - Se n�o tiver abono cadastrado ser� verdadeiro.
/*/
Function GetJustification( cFilSra, cMatSra, cInitDate, cEndDate, cInitHour, cEndHour, cKeyUpd )

Local cAliasQry  := GetNextAlias()
Local cAliasAux1 := GetNextAlias()
Local lRet 	   	 := .T.
Local nHorIni	 := ''
Local nHorFim    := ''
Local dDataIni	 := ''
Local dDataFim	 := ''
Local cFiltro	 := '%%'

DEFAULT cKeyUpd	 := ''

If !Empty( cKeyUpd )
	cFiltro := "% AND RH3_CODIGO <> '" + cKeyUpd +"' %"
EndIf

BeginSql alias cAliasQry
	SELECT RH3.RH3_FILIAL, RH3.RH3_CODIGO, RH3.RH3_STATUS
	FROM  %table:RH3% RH3
	WHERE
		RH3.RH3_FILIAL = %exp:cFilSra% AND
		RH3.RH3_MAT = %exp:cMatSra% AND
    	RH3.RH3_TIPO = '8' AND
		RH3.%notDel% AND
		RH3.RH3_STATUS != '3'
		%exp:cFiltro%
EndSql

While !(cAliasQry)->(Eof())

	BeginSql alias cAliasAux1
		SELECT *
		FROM  %table:RH4% RH4
		WHERE
			RH4.RH4_CODIGO = %exp:(cAliasQry)->RH3_CODIGO% AND
			(RH4.RH4_CAMPO = "RF0_DTPREI" OR
			 RH4.RH4_CAMPO = "RF0_DTPREF" OR
			 RH4.RH4_CAMPO = "RF0_HORINI" OR
			 RH4.RH4_CAMPO = "RF0_HORFIM") AND
			RH4.%notDel%
	EndSql

	dDataIni	:= ''
	dDataFim	:= ''
	nHorIni	    := ''
	nHorFim	    := ''
	While !(cAliasAux1)->(Eof())
		If (cAliasAux1)->RH4_CAMPO = "RF0_DTPREI"
			dDataIni := CTOD((cAliasAux1)->(RH4_VALNOV))
		EndIf
		If (cAliasAux1)->RH4_CAMPO = "RF0_DTPREF"
			dDataFim := CTOD((cAliasAux1)->(RH4_VALNOV))
		EndIf
		If (cAliasAux1)->RH4_CAMPO = "RF0_HORINI"
			nHorIni := Val((cAliasAux1)->(RH4_VALNOV))
		EndIf

		If (cAliasAux1)->RH4_CAMPO = "RF0_HORFIM"
			nHorFim := Val((cAliasAux1)->(RH4_VALNOV))
		EndIf

		(cAliasAux1)->(DBSkip())

	Enddo

	If (cInitDate >= dDataIni .AND. cEndDate <= dDataFim) .OR. ;
	   (cEndDate >= dDataIni .AND. cEndDate <= dDataFim)
		If (cInitHour >= nHorIni .AND. cInitHour <= nHorFim) .OR. ;
		   (cEndHour > nHorIni .AND. cEndHour <= nHorFim)
			lRet := .F.
			(cAliasAux1)->(DBCloseArea())
			Exit
		EndIf
	EndIf
	(cAliasAux1)->(DBCloseArea())

	(cAliasQry)->(DBSkip())
Enddo
(cAliasQry)->(DBCloseArea())

Return(lRet)


/*/{Protheus.doc} fGetClockType
Carrega os motivos para inclusao de marcacao manual
@author:	Marcelo Silveira
@since:		18/02/2019
@param:		cBranchVld - Filial;
			aData - Matriz de referencia para retorno dos dados;
@return:	Nulo
/*/
Function fGetClockType(cBranchVld, cType)

Local cAlias		:= GetNextAlias()
Local cJsonObj		:= "JsonObject():New()"
Local cBrchRFD		:= ""
Local oClockType	:= Nil
Local aData         := {}

Default cBranchVld	:= FwCodFil()
Default cType       := "1"

cBrchRFD := xFilial("RFD", cBranchVld)

//Somente marcacoes e para inclusao - Aplicacao/Tipo = 1
BEGINSQL ALIAS cAlias
	SELECT RFD_CODIGO, RFD_DESC
	    FROM %Table:RFD% RFD
	   WHERE RFD.RFD_FILIAL = %Exp:cBrchRFD%
         AND RFD.RFD_APLIC  = '1'
         AND RFD.RFD_TIPO   = %Exp:cType%
         AND RFD.%NotDel%
ENDSQL

If !Empty(cAlias)
	While !(cAlias)->(Eof())

        oClockType 					:= &cJsonObj
        oClockType["id"]			:= EncodeUTF8((cAlias)->RFD_CODIGO)
        oClockType["description"]	:= EncodeUTF8( AllTrim((cAlias)->RFD_DESC) )

        aAdd(aData, oClockType)
        oClockType := Nil

	(cAlias)->(dbSkip())

	EndDo
EndIf

(cAlias)->(dbCloseArea())

Return aData


Function fSetGeoClocking(cBody,oItem,oItemDetail,cToken,cMsg)

Local cApprover				:= ""
Local cVision	 			:= ""
Local cEmpApr				:= ""
Local cFilApr				:= ""
Local cTitTurno				:= ""
Local cTitSeq				:= ""
Local cTitRegra				:= ""
Local nSupLevel				:= 0
Local nRet					:= 0
Local aVision				:= {}
Local aGetStruct			:= {}
Local aHorTmz				:= {}
Local aMSToHour 			:= Array(02)
Local aMSToHourRefer		:= Array(02)
Local aDataLogin			:= {}
Local cTypeReq				:= "Z"
Local cRoutine				:= "W_PWSA400.APW" //Marca��o de Ponto: Utilizada para buscar a VIS�O a partir da rotina; (AI8_VISAPV) na fun��o GetVisionAI8().
Local cMsgReturn 			:= EncodeUTF8(STR0029) //"Dados atualizados com sucesso."
Local cBranchVld			:= FwCodFil()
Local oRequest				:= Nil
Local oAttendControlRequest	:= Nil
Local nHour                 := Seconds()*1000
Local cJsonObj	    		:= "JsonObject():New()"
Local lRet                  := .T.
Local lUpdRH3				:= .T.
Local lAprov				:= FindFunction("FAPROVPON") //Funcao do TCFA040 - Essa verificacao deve ser retirada apos o release 12.1.26
Local lCamposTZ				:= RS3->(ColumnPos("RS3_TMZ")) .And. RS3->(ColumnPos("RS3_DTREF")) .And. RS3->(ColumnPos("RS3_HRREF")) // Novos campos timezone

Default cBody				:= ""
Default oItem 	 			:= &cJsonObj
Default oItemDetail			:= &cJsonObj
Default cToken				:= ""
Default cMsg				:= ""

oRequest					:= WSClassNew("TRequest")
oRequest:RequestType		:= WSClassNew("TRequestType")
oRequest:Status				:= WSClassNew("TRequestStatus")
oAttendControlRequest		:= WSClassNew("TAttendControl")

aDataLogin := GetDataLogin(cToken)
If Len(aDataLogin) > 0
	cMatSRA	   := aDataLogin[1]
	cRD0Cod    := aDataLogin[3]
	cBranchVld := aDataLogin[5]	
EndIf

If !Empty(cBody) .And. lAprov

	//Verifica se existem inconsistencias no cadastro do funcionario
	DbSelectArea("SRA")
	If ( lRet := SRA->(dbSeek(cBranchVld + cMatSRA)) )
		cNome := AllTrim(SRA->RA_NOME)
		If Empty(SRA->RA_TNOTRAB) .Or. Empty(SRA->RA_SEQTURN) .Or. Empty(SRA->RA_REGRA)
			lRet		:= .F.
			cTitTurno	:= GetSx3Cache("RA_TNOTRAB", "X3_TITULO")
			cTitSeq		:= GetSx3Cache("RA_SEQTURN", "X3_TITULO")
			cTitRegra	:= GetSx3Cache("RA_REGRA", "X3_TITULO")
			cMsg 		:= EncodeUTF8(STR0041) +" ("+ cTitTurno +"), ("+ cTitSeq +"), ("+ cTitRegra + ")" //"O cadastro do funcionario est� incompleto. Verifique os campos:"
		EndIf
	EndIf

	If lRet
		oItemDetail:FromJson(cBody)
	
	    // ----------------------------------------------
		// - A Fun��o GetVisionAI8() devolve por padr�o
		// - Um Array com a seguinte estrutura:
		// - aVision[1][1] := "" - AI8_VISAPV
		// - aVision[1][2] := 0  - AI8_INIAPV
		// - aVision[1][3] := 0  - AI8_APRVLV
		// - Por isso as posi��o podem ser acessadas
		// - Sem problemas, ex: cVision := aVision[1][1]
		// ----------------------------------------------
		aVision := GetVisionAI8(cRoutine, cBranchVld)
		cVision := aVision[1][1]
	
		// -------------------------------------------------------------------------------------------
		// - Efetua a busca dos dados referentes a Estrutura Oreganizacional dos dados da solicita��o.
		//- -------------------------------------------------------------------------------------------
		 cMRrhKeyTree := fMHRKeyTree(cBranchVld, cMatSRA)
		 aGetStruct := APIGetStructure(cRD0Cod, SUPERGETMV("MV_ORGCFG"), cVision, cBranchVld, cMatSRA, , , ,cTypeReq , cBranchVld, cMatSRA, , , , , .T., {cEmpAnt})
	
		 If Len(aGetStruct) >= 1 .And. !(Len(aGetStruct) == 3 .And. !aGetStruct[1])
		 	cEmpApr   := aGetStruct[1]:ListOfEmployee[1]:SupEmpresa
		 	cFilApr   := aGetStruct[1]:ListOfEmployee[1]:SupFilial
		 	nSupLevel := aGetStruct[1]:ListOfEmployee[1]:LevelSup
		 	cApprover := aGetStruct[1]:ListOfEmployee[1]:SupRegistration
		 EndIf
	
		cDate		:= FwTimeStamp(6, dDataBase, "12:00:00" )	//Data da inclus�o
		cJustify	:= STR0035                     				//"Batida por GeoLocaliza��o"
		aMsToHour	:= milisSecondsToHour(nHour,nHour)
	
		oRequest:Branch						:= cBranchVld
		oRequest:StarterBranch				:= cBranchVld
		oRequest:StarterRegistration		:= cMatSRA
		oRequest:Registration				:= cMatSRA
		oRequest:Empresa					:= cEmpAnt
		oRequest:Vision						:= cVision
	
		oAttendControlRequest:Branch  		:= cBranchVld
		oAttendControlRequest:Registration	:= cMatSRA
		oAttendControlRequest:Name			:= cNome
		oAttendControlRequest:Observation  	:= cJustify
		oAttendControlRequest:Latitude		:= oItemDetail["latitude"]
		oAttendControlRequest:Longitude		:= oItemDetail["longitude"]

		If !empty(oItemDetail["timezone"])
			If lCamposTZ 
				aMsToHourRefer := milisSecondsToHour(oItemDetail["hour"],oItemDetail["hour"])

				oAttendControlRequest:Timezone		:= oItemDetail["timezone"]
				oAttendControlRequest:DateRefer		:= Format8601(.T.,oItemDetail["date"])
				oAttendControlRequest:HourRefer		:= cValToChar(NoRound(aMsToHourRefer[1],2))
			EndIf

			aHorTmz := fGetTimezone(oItemDetail["timezone"])
			If empty(aHorTmz)
				lRet := .F.
				cMsg := EncodeUTF8(STR0064) //"Ocorreu um problema na verifica��o do timezone para marca��o"
			Else
				aMsToHour := milisSecondsToHour(aHorTmz[2],aHorTmz[2])
				oAttendControlRequest:Date	:= Format8601(.T.,aHorTmz[1])  
				oAttendControlRequest:Hour	:= cValToChar(NoRound(aMsToHour[1],2))
			EndIf
		Else
			oAttendControlRequest:Date		:= Format8601(.T.,cDate)
			oAttendControlRequest:Hour		:= cValToChar(NoRound(aMsToHour[1],2))
		EndIf

		If lRet
			BEGIN TRANSACTION
				lRet := AddAttendControlRequest(oRequest, oAttendControlRequest, .T., @cMsgReturn, STR0038, .T. ) //"MEURH"
				
				If lRet
					nRet := fAprovPon( cBranchVld, cMatSRA, oRequest:Code, @cMsg, , lUpdRH3, lIsBlind )
					If nRet # 0
						cMsg := If( !Empty(cMsg), EncodeUTF8(cMsg), EncodeUTF8(STR0040) ) //"A batida est� fora do per�odo permitido para inclus�o!" 
						lRet := .F.
						DisarmTransaction()
						Break
					EndIf
				Else
					cMsg := cMsgReturn
					DisarmTransaction()
					Break
				EndIf
				
			END TRANSACTION
		EndIf	

	EndIf
Else
	lRet := .F.
	cMsg := EncodeUTF8(STR0034) //"Falta recursos para se processar essa requisi��o. � necess�rio atualizar o sistema para a expedi��o mais recente."
EndIf

Return lRet


Function GetDayClocks(cBranchVld, cMatSRA )

    Local cAlias      := GetNextAlias()
    local aClockings  := {}
    Local oClocking   := Nil
    Local cDirection  := ""

    Default cBranchVld		:= FwCodFil()
    Default cMatSRA			:= ""
    Default dPerIni     	:= Ctod("//")
    Default dPerFim     	:= Ctod("//")

    dbSelectArea("SRA")
    SRA->(dbSetOrder(1))
    If SRA->( dbSeek( cBranchVld + cMatSRA ) )

        BEGINSQL ALIAS cAlias
            SELECT RH3_FILIAL,
                    RS3_DATA,
                    RS3_HORA,
                    RS3_STATUS,
                    RS3_JUSTIF,
                    RS3_CODIGO,
                    RS3_LATITU,
                    RS3_LONGIT,
					P8_TPMCREP,
					P8_MOTIVRG
                  FROM %table:RS3% RS3
            INNER JOIN %table:RH3% RH3
                 ON RS3_FILIAL = RH3_FILIAL
                AND RS3_CODIGO = RH3_CODIGO
                AND RH3.%notDel%
			INNER JOIN %table:SP8% SP8
				ON RS3_FILIAL = P8_FILIAL
				AND RS3_MAT = P8_MAT
				AND RS3_DATA = P8_DATA
				AND RS3_HORA = P8_HORA
				AND SP8.%notDel%
              WHERE RS3_FILIAL = %exp:SRA->RA_FILIAL%
                AND RS3_MAT    = %exp:SRA->RA_MAT%
                AND RS3_DATA   = %exp:DtoS(dDataBase)%
                AND RS3_STATUS <> '2'
                AND RS3_LATITU <> ' '
                AND RS3_LONGIT <> ' '
				AND P8_TPMCREP <> 'D'
                AND RS3.%notDel%
            ORDER BY RS3_FILIAL, RS3_MAT, RS3_DATA, RS3_HORA
        ENDSQL

        While (cAlias)->(!Eof())

            oClocking := &("JsonObject():New()")

            If cDirection != "entry"
                cDirection := "entry"
            Else
                cDirection := "exit"
            EndIf

            oClocking["date"]	   := FwTimeStamp(6, SToD((cAlias)->RS3_DATA), "12:00:00" )
            oClocking["hour"]	   := HourToMs(StrZero((cAlias)->RS3_HORA, 5, 2))
            oClocking["latitude"]  := (cAlias)->RS3_LATITU
            oClocking["longitude"] := (cAlias)->RS3_LONGIT
            oClocking["direction"] := cDirection

            Aadd( aClockings, oClocking )
            (cAlias)->(DbSkip())
        EndDo

        (cAlias)->(DbCloseArea())

    EndIf

Return aClockings



Function getGeoClockings(cBranchVld,cMatSRA,dPerIni,dPerFim,cEmpTeam)

    Local cAlias      	:= GetNextAlias()
    local aClockings  	:= {}
    Local oClocking   	:= Nil
	Local __cTabRS3   	:= ""
	Local __cTabRH3   	:= ""
	Local __cTabSP8   	:= ""

    Default cBranchVld	:= FwCodFil()
    Default cMatSRA		:= ""
    Default dPerIni     := Ctod("//")
    Default dPerFim     := Ctod("//")
    Default cEmpTeam    := cEmpAnt

	__cTabRS3   := "%" + RetFullName("RS3", cEmpTeam) + "%"
	__cTabRH3   := "%" + RetFullName("RH3", cEmpTeam) + "%"
	__cTabSP8   := "%" + RetFullName("SP8", cEmpTeam) + "%"

	BEGINSQL ALIAS cAlias
		SELECT RH3_FILIAL,
				RS3_DATA,
				RS3_HORA,
				RS3_STATUS,
				RS3_JUSTIF,
				RS3_CODIGO,
				RH3_STATUS,
				RS3_LATITU,
				RS3_LONGIT,
				P8_TPMCREP,
				P8_MOTIVRG
		FROM %exp:__cTabRS3% RS3
		INNER JOIN %exp:__cTabRH3% RH3			
				ON RS3_FILIAL = RH3_FILIAL
			AND RS3_CODIGO = RH3_CODIGO
			AND RH3.%notDel%
		INNER JOIN %exp:__cTabSP8% SP8			
			ON RS3_FILIAL = P8_FILIAL
			AND RS3_MAT = P8_MAT
			AND RS3_DATA = P8_DATA
			AND RS3_HORA = P8_HORA
			AND SP8.%notDel%
			WHERE RS3_FILIAL  = %exp:cBranchVld%
			AND RS3_MAT     = %exp:cMatSRA%
			AND RS3_DATA   >= %exp:DtoS(dPerIni)%
			AND RS3_DATA   <= %exp:DtoS(dPerFim)%
			AND RS3_LATITU <> ' '
			AND RS3_LONGIT <> ' '
			AND RS3.%notDel%
		ORDER BY RS3_FILIAL, RS3_MAT, RS3_DATA, RS3_HORA
	ENDSQL

	While (cAlias)->(!Eof())

		oClocking := &("JsonObject():New()")

		oClocking["id"]	          := (cAlias)->RS3_CODIGO
		oClocking["disconsider"]  := ( (cAlias)->RS3_STATUS == "2" .Or. (cAlias)->P8_TPMCREP == 'D' )
		oClocking["date"]	      := FwTimeStamp(6, SToD((cAlias)->RS3_DATA), "12:00:00" )
		oClocking["justify"]	  := EncodeUTF8((cAlias)->RS3_JUSTIF)
		oClocking["latitude"]	  := (cAlias)->RS3_LATITU
		oClocking["longitude"]	  := (cAlias)->RS3_LONGIT
		oClocking["hour"]	      := HourToMs(StrZero((cAlias)->RS3_HORA, 5, 2))

		If oClocking["disconsider"]
			oClocking["reason"]	      := &("JsonObject():New()")
			oClocking["reason"]["id"] := (cAlias)->RS3_CODIGO
			oClocking["reason"]["description"] := (cAlias)->(getRGKJustify(xFilial("RGK", RH3_FILIAL), RS3_CODIGO))
			If Empty(oClocking["reason"]["description"])
				oClocking["reason"]["description"] := ALLTRIM( (cAlias)->P8_MOTIVRG )
			EndIf
		EndIf

		Aadd( aClockings, oClocking )
		(cAlias)->(DbSkip())
	EndDo

    (cAlias)->(DbCloseArea())

Return aClockings


Function UpdGeoClock(cBranchVld, cMatSRA, cBody, oItem, cMsg, cEmpSra )
    Local oClocking := &("JsonObject():New()")
    Local cMotivo   := ""
    Local cCodRH3   := ""
    Local lRet      := .F.
	
	DEFAULT cEmpSra	:= cEmpAnt

    oClocking:FromJson( cBody )

    cMotivo := If( oClocking:hasProperty("justify"), oClocking["justify"], "" )
    cCodRH3 := If( oClocking:hasProperty("id"), oClocking["id"], "" )    

    If !Empty( cCodRH3 )
     	If !Empty( cMotivo )
	        If TCFA40Rej( cMotivo, cBranchVld, cCodRH3, cMatSRA, cEmpSra )
	            oItem := oClocking
	            lRet  := .T.
	        EndIf
	    Else
	    	cMsg := EncodeUTF8(STR0039) //"O campo justificativa deve ser informado!"
	    EndIf
    Else
    	cMsg := EncodeUTF8(STR0036) //"Essa batida n�o foi desconsiderada porque n�o foram localizados os dados da requisi��o original."	
    EndIf

Return lRet

/*/{Protheus.doc} fSumOccurPer
Retorna o resumo das ocorr�ncias do per�odo/diario do colaborador.
@author:	Marcelo Silveira
@since:		10/06/2019
@param:		cBranchVld - Filial;
			cMatSRA - Matricula;
			cPerIni - Data inicio do periodo a ser pesquisado;
			cPerFim - Data Fim do periodo a ser pesquisado;
			lSexagenal - Se calcula em formato sexagesimal ou centensimal;
@return:	Array - eventos do banco de horas do periodo
/*/
Function fSumOccurPer( cBranchVld, cMatSRA, cPerIni, cPerFim, lSexagenal )

Local aArea			:= {}
Local aEventos		:= {}
Local cDtIni		:= ""
Local cDtFim		:= ""
Local cCod			:= ""
Local cAliasQry		:= ""
Local cWhere 		:= ""
Local cJoinFil 		:= ""
Local cAliasAux 	:= ""
Local cPrefixo		:= ""
Local cDescEve		:= ""
Local dIniPonMes	:= cToD("//")
Local dFimPonMes	:= cToD("//")
Local nSaldo		:= 0
Local nSaldoAux		:= 0
Local lImpAcum		:= .T.

DEFAULT lSexagenal	:= .T.

cDtIni 		:= StrTran( SubStr(cPerIni, 1, 10), "-", "" )
cDtFim 		:= StrTran( SubStr(cPerFim, 1, 10), "-", "" )

If !Empty(cDtIni) .And. !Empty(cDtFim)

	aArea		:= GetArea()
	cAliasQry	:= GetNextAlias()

	GetPonMesDat( @dIniPonMes , @dFimPonMes , cBranchVld )
	lImpAcum 	:= ( sTod(cDtFim) < dIniPonMes )
	cAliasAux 	:= If( lImpAcum, "SPH", "SPC")
	cPrefixo	:= If( lImpAcum, "PH_", "PC_")		

	cWhere += "%"
	cWhere += cPrefixo + "FILIAL = '" + cBranchVld + "' AND "
	cWhere += cPrefixo + "MAT = '" + cMatSRA + "' AND "
	cWhere += cPrefixo + "DATA >= '" + cDtIni + "' AND "
	cWhere += cPrefixo + "DATA <= '" + cDtFim + "' "
	cWhere += "%"

	If lImpAcum
	
		cJoinFil:= "%" + FWJoinFilial("SPH", "SP9") + "%"
	
		BeginSql Alias cAliasQry
		
		 	SELECT             
				SPH.PH_DATA, 
				SPH.PH_PD, 
				SPH.PH_PDI , 
				SPH.PH_QUANTC, 
				SPH.PH_QUANTI,
				SPH.PH_ABONO, 
				SPH.PH_QTABONO, 
				SP9.P9_CODIGO, 
				SP9.P9_DESC
			FROM 
				%Table:SPH% SPH
			INNER JOIN %Table:SP9% SP9
			ON %exp:cJoinFil% AND SP9.%NotDel% AND SPH.PH_PD = SP9.P9_CODIGO		
			WHERE
				%Exp:cWhere% AND SPH.%NotDel%
			ORDER BY SPH.PH_DATA, SPH.PH_PD
		
		EndSql 	
	Else
		cJoinFil:= "%" + FWJoinFilial("SPC", "SP9") + "%"
		
		BeginSql Alias cAliasQry
		
		 	SELECT             
				SPC.PC_DATA, 
				SPC.PC_PD, 
				SPC.PC_PDI,
				SPC.PC_QUANTC, 
				SPC.PC_QUANTI,
				SPC.PC_ABONO, 
				SPC.PC_QTABONO, 
				SP9.P9_CODIGO, 
				SP9.P9_DESC
			FROM 
				%Table:SPC% SPC
			INNER JOIN %Table:SP9% SP9
			ON %exp:cJoinFil% AND SP9.%NotDel% AND SPC.PC_PD = SP9.P9_CODIGO			
			WHERE
				%Exp:cWhere%  AND SPC.%NotDel%
			ORDER BY SPC.PC_DATA, SPC.PC_PD	
		EndSql 	
	EndIf
	
	//Considera todos os eventos: Autorizados/Nao Autorizados
	While !(cAliasQry)->(Eof())

		//Prioriza o Cod Informando, depois o c�digo de abono, depois o c�digo calculado.
		If !Empty((cAliasQry)->&(cPrefixo+"PDI") )
			cCod := (cAliasQry)->&(cPrefixo+"PDI")
		ElseIf !Empty((cAliasQry)->&(cPrefixo+"ABONO") )
			cCod := (cAliasQry)->&(cPrefixo+"ABONO")
			//Se a quantidade calculada for maior que a quantidade abonada, ent�o tem abono parcial.
			If (cAliasQry)->&(cPrefixo+"QUANTC") > (cAliasQry)->&(cPrefixo+"QTABONO")
				nSaldoAux := __TimeSub( (cAliasQry)->&(cPrefixo+"QUANTC") , (cAliasQry)->&(cPrefixo+"QTABONO") )
				//Nesse caso, adiciona o evento com a diferen�a calculada - qtde abonada.
				If ( nPos := aScan( aEventos, {|x| x[1] == (cAliasQry)->P9_CODIGO .And. x[2] == (cAliasQry)->P9_DESC } ) ) == 0
					aAdd( aEventos, { (cAliasQry)->P9_CODIGO, (cAliasQry)->P9_DESC, nSaldoAux } )
				Else
					If lSexagenal
						aEventos[nPos,3] := __TimeSum( aEventos[nPos,3], nSaldoAux )
					Else
						aEventos[nPos,3] := aEventos[nPos,3] + fConvhR(nSaldoAux,"D",,5)				
					EndIf 		
				EndIf
			EndIf
		else
			cCod := (cAliasQry)->P9_CODIGO
		EndIf
		
		//Prioriza o Cod Informando, depois o c�digo de abono, depois o c�digo calculado.
		If !Empty((cAliasQry)->&(cPrefixo+"PDI"))
			cDescEve :=	DescPdPon( (cAliasQry)->&(cPrefixo+"PDI") ) //Fun��o do PONXFUN
		ElseIf !Empty((cAliasQry)->&(cPrefixo+"ABONO"))
			cDescEve := DescAbono((cAliasQry)->&(cPrefixo+"ABONO"),"C") //Fun��o do PONXFUN
		else
			cDescEve := (cAliasQry)->P9_DESC
		EndIf

		//Prioriza a qtde informada, depois a qtde de abono, depois qtde calculado.
		If (cAliasQry)->&(cPrefixo+"QUANTI") > 0
			nSaldo := (cAliasQry)->&(cPrefixo+"QUANTI")
		ElseIf (cAliasQry)->&(cPrefixo+"QTABONO") > 0
			nSaldo := (cAliasQry)->&(cPrefixo+"QTABONO")			
		else
			nSaldo := (cAliasQry)->&(cPrefixo+"QUANTC")
		EndIf
	
		If ( nPos := aScan( aEventos, {|x| x[1] == cCod .And. x[2] == cDescEve } ) ) == 0
			aAdd( aEventos, { cCod, cDescEve, nSaldo } )
		Else
			If lSexagenal
				aEventos[nPos,3] := __TimeSum( aEventos[nPos,3], nSaldo )
			Else
				aEventos[nPos,3] := aEventos[nPos,3] + fConvhR(nSaldo,"D",,5)				
			EndIf 		
		EndIf
	
		(cAliasQry)->(DbSkip())
	EndDo
	
	(cAliasQry)->(DbCloseArea())

	RestArea( aArea )

EndIf

Return( aEventos )

/*/{Protheus.doc} fBalanceSumPer
Retorna o banco de horas do funcionario conforme o periodo que esta sendo pesquisado.
@author:	Marcelo Silveira
@since:		10/06/2019
@param:		cBranchVld - Filial;
			cMatSRA - Matricula;
			cPerIni - Data inicio do periodo a ser pesquisado;
			cPerFim - Data Fim do periodo a ser pesquisado;
			lSexagenal - Se calcula em formato sexagesimal ou centensimal;
			lTeam - Se verdadeiro ira pesquisar dos funcionarios do time conforme a matricula;
			lDetail - Indica se o saldo do time sera detalhado por funcionario
			cFilter - Expressao de filtro para ser adicionada na query
@return:	Array - banco de horas do periodo
/*/
Function fBalanceSumPer( cBranchVld, cMatSRA, cPerIni, cPerFim, lSexagenal, lTeam, lDetail, cFilter )

Local aSaldoDet   := {}
Local aRetSaldo   := {}
Local aArea       := {}
Local cDtIni      := ""
Local cDtFim      := ""
Local cTpCod      := ""
Local cName       := ""
Local cLastFil    := ""
Local cFilProc    := ""
Local cMatProc    := ""
Local cFilSPI     := ""
Local cMatSPI     := ""
Local cAliasSRA   := ""
Local cAliasSPI   := ""
Local cRoutine    := "W_PWSA400.APW" 	//Marca��o de Ponto
Local lProc       := .F.
Local nPos        := 0
Local nX          := 0
Local nValor      := 0
Local nCurrent    := 0
Local nCredito    := 0
Local nDebito     := 0
Local nSaldoAtu   := 0
Local nSaldoAnt   := 0
Local lValoriza   := SuperGetMV("MV_TCFBHVL",.F.,.F.) //Utiliza saldo de BH valorizado .T. ou real .F. (padrao))  

DEFAULT lSexagenal := .T.
DEFAULT lTeam      := .F.
DEFAULT lDetail    := .F.
DEFAULT cFilter    := "%%"

cDtIni 	:= StrTran( SubStr(cPerIni, 1, 10), "-", "" )
cDtFim 	:= StrTran( SubStr(cPerFim, 1, 10), "-", "" )

//Busca os dados da equipe caso a requisicao seja do saldo do time
If lTeam
  aRetSaldo  := {0,0}
  aVision    := GetVisionAI8(cRoutine, cBranchVld)
  cVision    := aVision[1][1]

  cMRrhKeyTree := fMHRKeyTree(cBranchVld, cMatSRA)
  aCoordTeam := APIGetStructure("", "", cVision, cBranchVld, cMatSRA, , , , , cBranchVld, cMatSRA, , , , , .T., {cEmpAnt})

  For nX := 1 To Len( aCoordTeam[1]:ListOfEmployee )
    If !aCoordTeam[1]:ListOfEmployee[nX]:Registration == cMatSRA //Nao considera a matricula do lider/gestor
      cMatSPI += "'" + aCoordTeam[1]:ListOfEmployee[nX]:Registration + "',"

      If !(cLastFil == aCoordTeam[1]:ListOfEmployee[nX]:EmployeeFilial)
        cFilSPI  += "'" + aCoordTeam[1]:ListOfEmployee[nX]:EmployeeFilial + "',"
        cLastFil := aCoordTeam[1]:ListOfEmployee[nX]:EmployeeFilial
      EndIf
    EndIf
  Next
    
  cMatSPI  := SubStr(cMatSPI, 2, Len(cMatSPI)-3 )
  cFilSPI  := SubStr(cFilSPI, 2, Len(cFilSPI)-3 )
Else
  aRetSaldo := {0,0,0}
  cMatSPI   := cMatSRA
  cFilSPI   := cBranchVld
EndIf

If !Empty(cFilSPI) .And. !Empty(cMatSPI) .And. !Empty(cDtIni) .And. !Empty(cDtFim)
	
  aArea     := GetArea()
  cAliasSPI := GetNextAlias()
	
  If lDetail
    cSQ3tab := "%" + RetSqlName("SQ3") + "%"
    cJoin   := "% SRA.RA_CARGO = SQ3.Q3_CARGO AND " + fMHRTableJoin("SRA", "SQ3") + "%"

    cAliasSRA := GetNextAlias()

    BeginSql alias cAliasSRA
      SELECT RA_FILIAL, RA_MAT, RA_NOME, RA_NOMECMP, RA_CARGO, Q3_DESCSUM 
      FROM %table:SRA% SRA
      LEFT JOIN %exp:cSQ3tab% SQ3
        ON %exp:cJoin%
      WHERE 
        %Exp:cFilter%
				SRA.RA_FILIAL IN ( %exp:cFilSPI% ) AND
        SRA.RA_MAT IN ( %exp:cMatSPI% ) AND 
        SRA.%notDel%		
      ORDER BY 
        SRA.RA_FILIAL, SRA.RA_NOME
    EndSql

    //Adiciona no detalhe todos os subordinados do gestor	
    While (cAliasSRA)->( !Eof() )
      cName    := If( !Empty((cAliasSRA)->RA_NOMECMP), AllTrim((cAliasSRA)->RA_NOMECMP), AllTrim((cAliasSRA)->RA_NOME ) ) //Nome (prioriza nome completo)
      cCargo   := If( !Empty((cAliasSRA)->Q3_DESCSUM), AllTrim((cAliasSRA)->Q3_DESCSUM), EncodeUTF8(STR0072) ) //N�o cadastrado # //Descricao do cargo
      aAdd( aSaldoDet, { 0, 0, 0, (cAliasSRA)->RA_FILIAL, (cAliasSRA)->RA_MAT, cName, cCargo } )
      (cAliasSRA)->(DbSkip())
    End

    (cAliasSRA)->( DBCloseArea() )
  EndIf

  BeginSql alias cAliasSPI
    SELECT PI_FILIAL, PI_MAT, PI_PD, PI_QUANT, PI_QUANTV, PI_CC, PI_DATA, PI_STATUS, PI_DTBAIX
    FROM %table:SPI% SPI
    WHERE 	
      SPI.PI_FILIAL IN ( %exp:cFilSPI% ) AND 
      SPI.PI_MAT IN ( %exp:cMatSPI% ) AND
      SPI.%notDel%
    ORDER BY 
      SPI.PI_FILIAL, SPI.PI_MAT, SPI.PI_DATA	
  EndSql

  While (cAliasSPI)->( !Eof() )

    PosSP9((cAliasSPI)->PI_PD, cBranchVld)
    cTpCod := SP9->P9_TIPOCOD 

    //Cada funcionario processado � atualizado no array com seu respectivo valor
		//O somatorio ocorrer� quando houver a mudan�a de Filial + Matricula da tabela SPI
    If lDetail .And. lProc .And. !( cFilProc + cMatProc == (cAliasSPI)->(PI_FILIAL + PI_MAT) )

			nPos := aScan( aSaldoDet, { |x| x[4] + x[5] == cFilProc + cMatProc } )

			If nPos > 0
				If lSexagenal
					nSaldoAtu := __TimeSum(nSaldoAtu, __TimeSub( __TimeSum( nSaldoAnt , nCredito ) , nDebito ))
					nCurrent  := __TimeSub( nCredito, nDebito )
				Else
					nSaldoAtu := ( nSaldoAtu + nSaldoAnt + nCredito ) - nDebito
					nCurrent  := nCredito - nDebito
				EndIf

				aSaldoDet[nPos, 1] := nSaldoAnt
				aSaldoDet[nPos, 2] := nSaldoAtu
				aSaldoDet[nPos, 3] := nCurrent
			EndIf

      nSaldoAnt := 0 
      nSaldoAtu := 0
      nCurrent  := 0
      nCredito  := 0
      nDebito   := 0
    EndIf

    // Totaliza Saldo Anterior
    If (!lTeam .Or. lDetail) .And. (cAliasSPI)->PI_DATA < cDtIni
      If !((cAliasSPI)->PI_STATUS == 'B' .AND. (cAliasSPI)->PI_DTBAIX < cDtIni)
        If ((cAliasSPI)->PI_STATUS == 'B' .AND. (cAliasSPI)->PI_DTBAIX <= cDtFim)

          If cTpCod $ "1*3"

            nValor := If( lValoriza, (cAliasSPI)->PI_QUANTV, (cAliasSPI)->PI_QUANT )
            If lSexagenal
              nSaldoAnt := __TimeSum(nSaldoAnt,nValor)  
              nSaldoAtu := __TimeSub(nSaldoAtu,nValor)
            Else
              nSaldoAnt := nSaldoAnt + fConvhR(nValor,"D",,5) 
              nSaldoAtu := nSaldoAtu - fConvhR(nValor,"D",,5) 
            EndIf 

          Else
						
            nValor := If( lValoriza, (cAliasSPI)->PI_QUANTV, (cAliasSPI)->PI_QUANT )
            If lSexagenal
              nSaldoAnt := __TimeSub(nSaldoAnt,nValor)
              nSaldoAtu := __TimeSum(nSaldoAtu,nValor)    
            Else
              nSaldoAnt := nSaldoAnt - fConvhR(nValor,"D",,5)
              nSaldoAtu := nSaldoAtu + fConvhR(nValor,"D",,5)  
            EndIf

          EndIf
        Else
          If cTpCod $ "1*3"
						
            nValor := If( lValoriza, (cAliasSPI)->PI_QUANTV, (cAliasSPI)->PI_QUANT )
            If lSexagenal
              nSaldoAnt := __TimeSum(nSaldoAnt,nValor)  
            Else
              nSaldoAnt := nSaldoAnt + fConvhR(nValor,"D",,5) 
            EndIf 
					
          Else

            nValor := If( lValoriza, (cAliasSPI)->PI_QUANTV, (cAliasSPI)->PI_QUANT )
            If lSexagenal
              nSaldoAnt := __TimeSub(nSaldoAnt,nValor)  
            Else
              nSaldoAnt := nSaldoAnt - fConvhR(nValor,"D",,5) 
            EndIf

          EndIf

        EndIf
      EndIf
    ElseIf (cAliasSPI)->PI_DATA <= cDtFim
      If !((cAliasSPI)->PI_STATUS == 'B' .AND. (cAliasSPI)->PI_DTBAIX <= cDtFim)
        If cTpCod $ "1*3"
					
          nValor := If( lValoriza, (cAliasSPI)->PI_QUANTV, (cAliasSPI)->PI_QUANT )
          If lSexagenal
            nCredito := __TimeSum(nCredito,nValor)  
          Else
            nCredito := nCredito + fConvhR(nValor,"D",,5) 
          EndIf 

        Else

          nValor := If( lValoriza, (cAliasSPI)->PI_QUANTV, (cAliasSPI)->PI_QUANT )
          If lSexagenal
            nDebito := __TimeSum(nDebito,nValor)  
          Else
            nDebito := nDebito + fConvhR(nValor,"D",,5) 
          EndIf

        EndIf
      EndIf	
    Endif

    //Guarda as informacoes do funcionario que est� sendo processado para totalizar quando mudar de matricula
    If lDetail .And. !( cFilProc+cMatProc == (cAliasSPI)->(PI_FILIAL+PI_MAT) )
      lProc    := .T. //Indica que j� vez a leitura de algum funcionario
      cFilProc := (cAliasSPI)->PI_FILIAL //Filial
      cMatProc := (cAliasSPI)->PI_MAT //Matricula
    EndIf

    (cAliasSPI)->(DbSkip())
  End

  (cAliasSPI)->( DBCloseArea() )

  //No detalhamento, o �ltimo funcionario da query nunca � adicionado porque nao tem dados para comparacao.
  //Ent�o nesse ponto adicionamos no array aSaldoDet os dados do �ltimo funcionario.
  If lDetail .And. lProc
			nPos := aScan( aSaldoDet, { |x| x[4] + x[5] == cFilProc + cMatProc } )

			If nPos > 0
				If lSexagenal
					nSaldoAtu := __TimeSum(nSaldoAtu, __TimeSub( __TimeSum( nSaldoAnt , nCredito ) , nDebito ))
					nCurrent  := __TimeSub( nCredito, nDebito )
				Else
					nSaldoAtu := ( nSaldoAtu + nSaldoAnt + nCredito ) - nDebito
					nCurrent  := nCredito - nDebito
				EndIf

				aSaldoDet[nPos, 1] := nSaldoAnt
				aSaldoDet[nPos, 2] := nSaldoAtu
				aSaldoDet[nPos, 3] := nCurrent
			EndIf
  EndIf
	
  If nSaldoAnt <> 0 .or. nCredito > 0 .or. nDebito > 0 .Or. Len(aSaldoDet) > 0
    
    If lTeam
      
      If lDetail //Saldo total do time detalhado por funcionario
        aRetSaldo := aClone(aSaldoDet)
      Else
        aRetSaldo := { nCredito, nDebito } //Saldo total do time
      EndIf

    Else 
      
      If lSexagenal
        nSaldoAtu := __TimeSum(nSaldoAtu, __TimeSub( __TimeSum( nSaldoAnt , nCredito ) , nDebito ))
        nCurrent  := __TimeSub( nCredito, nDebito )
      Else
        nSaldoAtu := ( nSaldoAtu + nSaldoAnt + nCredito ) - nDebito
        nCurrent  := nCredito - nDebito
      EndIf
      aRetSaldo := { nSaldoAnt, nSaldoAtu, nCurrent }

    EndIf
  
  EndIf
	
  RestArea( aArea )

EndIf

Return( aRetSaldo )


Function fEditClocking(aUrlParam, cBody, cJsonObj, oItem, oItemDetail, cToken, cMsgLog, aIdFunc)
    Local aMSToHour             := Array(02)
	Local aDataLogin			:= {}
    Local cMsgReturn            := ""
    Local cBranchVld            := FwCodFil()
    Local cMatSRA               := ""
    Local cMatManager           := ""
    Local cFilManager           := ""
    Local oRequest              := Nil
    Local oAttendControlRequest := Nil
    Local nA                    := 0
    Local cCodeReq              := ""
    Local cAllJustify           := ""
    Local lRet                  := .T.
    Local aStatus               := {"","",""}
    Local aTrab                 := {}
    Local lEmployManager 		:= .F.

    Default aUrlParam           := {}
    Default cBody               := ""
    Default cJsonObj            := "JsonObject():New()"
    Default oItem               := &cJsonObj
    Default oItemDetail         := &cJsonObj
    Default cToken              := ""
    Default cMsgLog             := ""
    Default aIdFunc             := {}

    oRequest                    := WSClassNew("TRequest")
    oRequest:RequestType        := WSClassNew("TRequestType")
    oRequest:Status             := WSClassNew("TRequestStatus")
    oAttendControlRequest       := WSClassNew("TAttendControl")

	aDataLogin := GetDataLogin(cToken)
	If Len(aDataLogin) > 0
		cMatManager := cMatSRA      := aDataLogin[1]
		cFilManager := cBranchVld   := aDataLogin[5]
	EndIf

	If Len(aIdFunc) > 0
		cBranchVld	:= aIdFunc[1]
		cMatSRA		:= aIdFunc[2]
	EndIf

    If !Empty(cBody)
        oItemDetail:FromJson(cBody)

        //Em a justificativa tiver sido informada para todas as batidas 
        cAllJustify := If(oItemDetail:hasProperty("justify"),oItemDetail["justify"],"")

        If oItemDetail:hasProperty("clockings") .And. ValType(oItemDetail["clockings"]) == "A"
            For nA := 1 To len(oItemDetail["clockings"])
                aStatus := {"","",""}
                If oItemDetail["clockings"][nA]:hasProperty("status")
                    aStatus[1] := oItemDetail["clockings"][nA]["status"]["status"]
                    aStatus[2] := oItemDetail["clockings"][nA]["status"]["id"]
                    aStatus[3] := oItemDetail["clockings"][nA]["status"]["label"]
                EndIf
                AADD(aTrab,{;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("hour"),oItemDetail["clockings"][nA]["hour"]," "),;
                    aStatus,;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("date"),oItemDetail["clockings"][nA]["date"]," "),;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("sequence"),oItemDetail["clockings"][nA]["sequence"]," "),;
                    Iif(!Empty(cAllJustify), cAllJustify, Iif(oItemDetail["clockings"][nA]:hasProperty("justify"),oItemDetail["clockings"][nA]["justify"]," ")),;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("id"),oItemDetail["clockings"][nA]["id"]," "),;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("referenceDate"),oItemDetail["clockings"][nA]["referenceDate"]," "),;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("direction"),oItemDetail["clockings"][nA]["direction"]," "),;
                    Iif(oItemDetail["clockings"][nA]:hasProperty("origin"),oItemDetail["clockings"][nA]["origin"]," "),;
                    Iif(oItemDetail:hasProperty("reason"),oItemDetail["reason"]," ");
                })
            Next nA
        Else
            aStatus := {"","",""}
            If oItemDetail:hasProperty("status")
                aStatus[1] := oItemDetail["status"]["status"]
                aStatus[2] := oItemDetail["status"]["id"]
                aStatus[3] := oItemDetail["status"]["label"]
            EndIf
            AADD(aTrab,{;
                Iif(oItemDetail:hasProperty("hour"),oItemDetail["hour"]," "),;
                aStatus,;
                Iif(oItemDetail:hasProperty("date"),oItemDetail["date"]," "),;
                Iif(oItemDetail:hasProperty("sequence"),oItemDetail["sequence"]," "),;
                Iif(!Empty(cAllJustify), cAllJustify, Iif(oItemDetail:hasProperty("justify"),oItemDetail["justify"]," ")),;
                Iif(oItemDetail:hasProperty("id"),oItemDetail["id"]," "),;
                Iif(oItemDetail:hasProperty("referenceDate"),oItemDetail["referenceDate"]," "),;
                Iif(oItemDetail:hasProperty("direction"),oItemDetail["direction"]," "),;
                Iif(oItemDetail:hasProperty("origin"),oItemDetail["origin"]," "),;
                ""; //-- No request do metodo "Atualiza UMA batida do espelho do ponto" n�o � enviado o parametro "reason" pois isso � inicializado com ""
            })
        EndIf

        If Len(aTrab) > 0
			lEmployManager := fGetTeamManager(cFilManager, cMatManager)
			
			DbSelectArea("RH3")
			For nA := 1 To len(aTrab)
				cCodeReq    := aTrab[nA][6]
				aMsToHour   := milisSecondsToHour(aTrab[nA][1],aTrab[nA][1])
				If "|" $ cCodeReq
					cCodeReq := STRTOKARR( cCodeReq , "|" )[2]

					RH3->(DbSetOrder(1))
					If RH3->(DbSeek( cBranchVld + cCodeReq ))
						//Altera somente status 1 (Em processo de aprova��o) ou 4 (Aguardando aprovacao RH) para quem tem equipe
						If RH3->RH3_STATUS == "1" .Or. (lEmployManager .And. RH3->RH3_STATUS == "4")
						oRequest:Branch                     := cBranchVld
						oAttendControlRequest:Branch        := cBranchVld
						oAttendControlRequest:Registration  := cMatSRA
						oAttendControlRequest:Name          := Alltrim(Posicione('SRA',1,cBranchVld+cMatSRA,'SRA->RA_NOME'))
						oAttendControlRequest:EntryExit     := Iif(Alltrim(aTrab[nA][8])=="entry","Entrada","Sa�da")
						oAttendControlRequest:Date          := Format8601(.T.,aTrab[nA][3])
						oAttendControlRequest:Hour          := cValToChar(aMsToHour[1])
						oAttendControlRequest:Observation   := GetMotDesc( aTrab[nA][10], cCodeReq )
						oAttendControlRequest:Motive        := aTrab[nA][5]
						oAttendControlRequest:codeRequest   := cCodeReq

							AddAttendControlRequest(oRequest, oAttendControlRequest, .T., @cMsgReturn, STR0038 ) //"MEURH")
						Else //-- Qualquer outro status nao permite alteracao
							cMsgLog := STR0037 //"Essa batida n�o pode ser alterada. O seu Tipo ou Status atual n�o permite altera��o."
							lRet := .F.
							Exit
						EndIf
					EndIf
				Else
					cMsgLog := STR0026 //"S� � poss�vel editar batidas que foram inclu�das manualmente!"
					lRet := .F.
					Exit
				EndIf
			Next nA
        EndIf
    EndIf

Return lRet


Function GetMotDesc(cCodigo, cCodRH3)
    Local cMotDesc := ""
    Local aArea	:= GetArea()

    If !Empty(cCodigo)
	    dbSelectArea("RFD")
	    RFD->(dbSetOrder(1))
	    If RFD->(dbSeek(FWxFilial("RFD") + cCodigo))	
	        cMotDesc := RFD->RFD_DESC
	    EndIf
	Else
	    dbSelectArea("RH4")
	    RH4->(dbSetOrder(1))
	    If RH4->(dbSeek(FWxFilial("RH4") + cCodRH3))	
			While !RH4->(Eof())
			    If Alltrim(RH4->RH4_CAMPO) == "P8_MOTIVRG"
					cMotDesc  := AllTrim( RH4->RH4_VALNOV )
					Exit
				EndIf
				RH4->(dbskip())
			Enddo
	    EndIf
    EndIf
	
	RestArea(aArea)

Return cMotDesc


Function DelBatida(cFilialPar, cMatSRA, cCodigo)
	Local lRet		:= .T.
	Local cFilRH3	:= Iif( FWModeAccess("RH3") == "C", FwxFilial("RH3"), cFilialPar )
	Local cFilRH4	:= Iif( FWModeAccess("RH4") == "C", FwxFilial("RH4"), cFilialPar )
	Local cFilRS3	:= Iif( FWModeAccess("RS3") == "C", FwxFilial("RS3"), cFilialPar )
	Local cMessage	:= ""
	Local lEmployManager := .F.
	
	dbSelectArea("RH3")
	RH3->(dbSetOrder(1))
	If RH3->(dbSeek(cFilRH3 + cCodigo))
	
		lEmployManager := fGetTeamManager(cFilialPar, cMatSRA)
		
		//Exclui somente status 1 (Em processo de aprova��o) ou 4 (Aguardando aprovacao RH) para quem tem equipe
		If RH3->RH3_STATUS == '1' .Or. (lEmployManager .And. RH3->RH3_STATUS == "4")
			Begin Transaction
				RecLock("RH3",.F.)
				RH3->(dbDelete())
				RH3->(MsUnlock())

				DbSelectArea("RH4")
				RH4->(DbSetOrder(1))
				If RH4->(DbSeek(cFilRH4 + cCodigo))
					While RH4->(!Eof() .and. RH4_FILIAL + RH4_CODIGO == cFilRH4 + cCodigo)
						RecLock("RH4",.F.)
						RH4->(DbDelete())
						RH4->(MsUnLock())
						RH4->(DbSkip())
					EndDo
				EndIf

				DbSelectArea("RS3")
				RS3->(DbSetOrder(1))
				If RS3->(DbSeek(cFilRS3 + cCodigo))
					RecLock("RS3",.F.)
					RS3->(DbDelete())
					RS3->(MsUnLock())
					RS3->(DbSkip())
				EndIf
			End Transaction
		Else
			cMessage := EncodeUTF8(STR0032) //"Esta batida n�o poder� ser exclu�da porque o processo de aprova��o j� foi iniciado."
			lRet := .F.
		EndIf
	EndIf	

Return {lRet, cMessage}


/*/{Protheus.doc} fGetListAllowance
Carrega as solicitacoes de abono do funcionario conforme os parametros
@author:	Marcelo Faria
@since:		23/12/2019
@param:		nType - 1=Solicitacoes pendentes, 2=diferentes de pendentes, 3=Todos;
			cFilSra - Filial;
			cMatSra - Matr�cula;
			cCodReq - Codigo da requisicao;
			cPage - Numero da pagina;
			cPageSize - Numero de registros;
			lNextPage - Indica se existe ou nao mais registros;
@return:	aFields - array com o json das solicitacoes			
/*/
Function fGetListAllowance( nType, cBranchVld, cMatSRA, cCodReq, cPage, cPageSize, lNextPage, aDataLogin )

Local cJsonObj     	:= "JsonObject():New()"
Local oFields      	:= &cJsonObj
Local oType      	:= &cJsonObj
Local aFields      	:= {}
Local aOrdFields   	:= {}
Local nX     		:= 0
Local nNumRegs 		:= 0
Local nRegCount     := 0
Local nRegIniCount  := 0 
Local nRegFimCount  := 0

Local cCodAbn		:= ""
Local cDscAbn		:= ""
Local cIniReq		:= ""
Local cMatLogin		:= ""
Local cFilLogin		:= ""

Local cQryRH3		:= ""
Local cQryRH4		:= ""
Local cDataIni		:= ""

Local cWhere 		:= "%"
Local lCount		:= .F.
Local lIniReq		:= .F.
Local aProps		:= fGetPropAllowance()

DEFAULT nType       := 3
DEFAULT	cCodReq		:= ""
DEFAULT cPage		:= ""
DEFAULT cPageSize	:= ""
DEFAULT lNextPage	:= .F.
DEFAULT aDataLogin	:= {}

If Len(aDataLogin) > 0
	cMatLogin	:= aDataLogin[1]
	cFilLogin	:= aDataLogin[5]
Else
	cMatLogin	:= cMatSRA
	cFilLogin	:= cBranchVld
EndIf

//controle de paginacao
If !Empty(cPage) .And. !Empty(cPageSize)
	If cPage == "1" .Or. cPage == ""
		nRegIniCount := 1 
		nRegFimCount := If( Empty( Val(cPageSize) ), 20, Val(cPageSize) )
	Else
		nRegIniCount := ( Val(cPageSize) * ( Val(cPage) - 1 ) ) + 1
		nRegFimCount := ( nRegIniCount + Val(cPageSize) ) - 1
	EndIf
	lCount := .T.
EndIf

If !empty(cCodReq)
	cWhere += " RH3.RH3_CODIGO = '" + cCodReq + "' AND " 
Else
	If nType == 1 //pending
		cWhere += " RH3.RH3_STATUS IN ('1','4') AND "	
	ElseIf nType == 2 //notpending
		cWhere += " RH3.RH3_STATUS IN ('2','3') AND "
	EndIf
EndIf

If RH3->(ColumnPos("RH3_EMP")) > 0 
	cWhere += " RH3.RH3_EMP = '" + cEmpAnt + "' AND "
EndIf

cWhere += "%"

cQryRH3 := GetNextAlias()
BEGINSQL ALIAS cQryRH3
	SELECT * 
	FROM %Table:RH3% RH3
	WHERE
		RH3.RH3_TIPO   =  '8'              AND
		RH3.RH3_FILIAL =  %Exp:cBranchVld% AND
		RH3.RH3_MAT    =  %Exp:cMatSRA%    AND
		%Exp:cWhere% 
		RH3.%NotDel%
ENDSQL

While !(cQryRH3)->(Eof()) 
	oFields 			   := &cJsonObj 
	oFields["id"]		   := (cQryRH3)->RH3_FILIAL +"|" +(cQryRH3)->RH3_CODIGO
	oFields["status"]	   := If( (cQryRH3)->RH3_STATUS=='2', "approved", If((cQryRH3)->RH3_STATUS=='3', 'rejected', 'pending') )
	oFields["statusLabel"] := fStatusLabel( (cQryRH3)->RH3_STATUS )
	
	cQryRH4 := GetNextAlias()
	BEGINSQL ALIAS cQryRH4
		SELECT RH4_FILIAL, RH4_CODIGO, RH4_CAMPO, RH4_VALNOV
		FROM   %Table:RH4% RH4
		WHERE  RH4.RH4_FILIAL = %Exp:(cQryRH3)->RH3_FILIAL% AND 
				RH4.RH4_CODIGO = %Exp:(cQryRH3)->RH3_CODIGO% AND 
				RH4.%NotDel%
	ENDSQL

	While (cQryRH4)->(!Eof()) 
		cCpoRH4 := AllTrim((cQryRH4)->RH4_CAMPO)		
		
		DO CASE
			CASE cCpoRH4 == "RF0_DTPREI"
				oFields["initDate"]         := cDataIni := formatGMT(Alltrim((cQryRH4)->RH4_VALNOV))
			CASE cCpoRH4 == "RF0_HORINI"
				oFields["initHour"]         := HourToMs(strZero( Val(Alltrim((cQryRH4)->RH4_VALNOV)), 5, 2))
			CASE cCpoRH4 == "RF0_DTPREF"
				If Alltrim((cQryRH4)->RH4_VALNOV) == "/  /"
					oFields["endDate"]      := cTod("//")
				Else
					oFields["endDate"]      := formatGMT(Alltrim((cQryRH4)->RH4_VALNOV))
				EndIf
			CASE cCpoRH4 == "RF0_HORFIM"
				oFields["endHour"]          := HourToMs(strZero( Val(Alltrim((cQryRH4)->RH4_VALNOV)), 5, 2))
			CASE cCpoRH4 == "RF0_CODABO"
				cCodAbn                     := Alltrim((cQryRH4)->RH4_VALNOV)
			CASE cCpoRH4 == "TMP_ABOND"					
				cDscAbn	                    := Alltrim((cQryRH4)->RH4_VALNOV)
		ENDCASE			
		
		(cQryRH4)->(DbSkip())
	EndDo
	(cQryRH4)->( DBCloseArea() )

	oFields["justify"]	:= Alltrim( getRGKJustify(xFilial("RGK", (cQryRH3)->RH3_FILIAL), (cQryRH3)->RH3_CODIGO, "'000001'") )

	oType				     := &cJsonObj 
	oType["id"] 		     := cCodAbn 
	oType["description"]     := EncodeUTF8(cDscAbn)	
	oType["props"] 		     := aProps			
	oFields["allowanceType"] := oType
	
	If (cQryRH3)->RH3_NVLAPR != (cQryRH3)->RH3_NVLINI   
		cIniReq := (cQryRH3)->RH3_FILINI + (cQryRH3)->RH3_MATINI
		lIniReq := !Empty(cIniReq) .And. cIniReq == (cFilLogin + cMatLogin)

		If Val( GetRGKSeq( (cQryRH3)->RH3_CODIGO , .T.) ) == 2 .And. lIniReq
			oFields["canAlter"]	:= .T.
		Else
			oFields["canAlter"]	:= .F.    
		EndIf
	EndIf

	Aadd( aFields, {oFields, cDataIni} )

	(cQryRH3)->(dbSkip()) 
EndDo

(cQryRH3)->( DBCloseArea() )


If ( nNumRegs := Len(aFields) ) > 0

	//Reordena os registros conforme a data do abono (mais recente primeiro)
	aSort(aFields,,,{ | x,y | x[2] > y[2] } )

	For nX := 1 To nNumRegs

		nRegCount ++		
		
		If !lCount .Or. ( nRegCount >= nRegIniCount .And. nRegCount <= nRegFimCount )
			Aadd( aOrdFields, aFields[nX,1] )
		Else
			If nRegCount >= nRegFimCount
				lNextPage := .T.
				Exit
			EndIf				
		EndIf

	Next nX
EndIf

Return( aOrdFields )


/*/{Protheus.doc} fGetTimezone
Carrega as solicitacoes de abono do funcionario conforme os parametros
@author:	Marcelo Faria, Jose Marcelo, Henrique Ferreira
@since:		15/04/2020
@param:		cTimezone - timezone da geolocaliza��o;
@return:	aResultTmz - array com data e hora local do timezone			
/*/
Function fGetTimezone( TmzBatida )
Local aResultTmz	:= {}
Local aMSToHour		:= Array(02)
Local nHour			:= Seconds()*1000
Local nMinGMTServer	:= 0
Local nDifMinTmz	:= 0
Local nDifHrTmz		:= 0
Local nDia			:= 0

DEFAULT	TmzBatida := 0

	If !empty(TmzBatida) .and. valtype(TmzBatida) == "N"
		// Apura diferen�a em minutos do hor�rio do servidor para o GMT+0 
		nMinGMTServer :=  Hrs2Min( SUBSTR(  FWTimeStamp(5) , 20 ) )
		aMsToHour := milisSecondsToHour(nHour,nHour)

		TmzBatida := TmzBatida * -1 

		// avaliar a diferen�a em minutos do timezone do servidor e o timezone da marca��o
		If TmzBatida == nMinGMTServer
			// os timezones s�o iguais e retorna o valor padr�o do server
	        aAdd(aResultTmz, FwTimeStamp(6, dDataBase, "12:00:00" )) 
	        aAdd(aResultTmz, Seconds()*1000) 

		ElseIf nMinGMTServer < TmzBatida
			//servidor est� a leste(direita) da marca��o 
			nDifMinTmz := TmzBatida - nMinGMTServer

			// Avalia��o do Min2Hrs() com o resultado em minutos sempre positivo
			nDifHrTmz := Min2Hrs( nDifMinTmz )
			nHrTmz    := somaHoras( NoRound(aMsToHour[1],2)  , nDifHrTmz)

			If nHrTmz > 23.59
				nNewHor := somaHoras( 0.00 , SomaHoras( SubHoras( nHrTmz , 23.59 ) , 0.01 ) )
				nHrTmz  := nNewHor
				nDia    := 1
			EndIf

	        aAdd(aResultTmz, FwTimeStamp(6, dDataBase + nDia, "12:00:00" )) 
	        aAdd(aResultTmz, (Hrs2Min(nHrTmz) * 60) * 1000) 

		Elseif nMinGMTServer > TmzBatida
			//servidor est� a oeste(esquerda) da marca��o 
			nDifMinTmz := nMinGMTServer - TmzBatida

			// Avalia��o do Min2Hrs() com o resultado em minutos sempre positivo
			nDifHrTmz := Min2Hrs( nDifMinTmz )
			nHrTmz 	  := SubHoras( NoRound(aMsToHour[1],2)  , nDifHrTmz)

			// batida            server
			// 17/04	      	 18/04
			// 11:00 p.m.   ---  01:00 a.m.

			If negativo(nHrTmz)
				nNewHor := SubHoras( 23.59 , SubHoras(nHrTmz * (-1) , 0.01) )
				nHrTmz  := nNewHor
				nDia    := 1
			EndIf

	        aAdd(aResultTmz, FwTimeStamp(6, dDataBase - nDia, "12:00:00" )) 
	        aAdd(aResultTmz, (Hrs2Min(nHrTmz) * 60) * 1000) 
		EndIf

	EndIf	

Return aResultTmz


/*/{Protheus.doc} fGetPropAllowance
Carrega as propriedades dos tipos de abono (fieldProperties)
@author:	Marcelo Silveira
@since:		08/06/2020
@return:	aProps - array com os objetos fieldproperties do allowance			
/*/
Function fGetPropAllowance()

Local cJsonObj := "JsonObject():New()"
Local oProps   := &cJsonObj
Local aProps   := {}

	oProps := &cJsonObj
	oProps["field"]     := "totalHour"
	oProps["visible"]   := .F.
	oProps["editable"]  := .F.
	oProps["required"]  := .F.
	Aadd(aProps, oProps)

	oProps := &cJsonObj
	oProps["field"]     := "initHour"
	oProps["visible"]   := .T.
	oProps["editable"]  := .T.
	oProps["required"]  := .T.
	Aadd(aProps, oProps)

	oProps := &cJsonObj
	oProps["field"]     := "endHour"
	oProps["visible"]   := .T.
	oProps["editable"]  := .T.
	oProps["required"]  := .T.
	Aadd(aProps, oProps)

	oProps := &cJsonObj
	oProps["field"]     := "justify"
	oProps["visible"]   := .T.
	oProps["editable"]  := .T.
	oProps["required"]  := .T.
	Aadd(aProps, oProps)

Return( aProps )

/*/{Protheus.doc} fVldMarc
Valida se as batidas est�o dentro do per�odo de apontamento.
@author:	Henrique Ferreira
@since:		22/06/2020
@return:	lRet - Retorna se a batida est� dentro do per�odo e pode ser gravada.
/*/
Function fVldMarc(aBatidas, dPerIni, dPerFim, cMsg)

Local nX		:= 0
Local nDAberto	:= 0
Local lRet		:= .T.

DEFAULT aBatidas := {}
DEFAULT dPerIni  := cTod("//")
DEFAULT dPerFim  := cTod("//")

If Empty(dPerIni) .Or. Empty(dPerFim)
	cMsg := STR0073 //N�o foi poss�vel carregar o conte�do o MV_PONMES. Verifique se o param�tro est� preenchido corretamente.
	lRet := .F.
Else
	nDAberto := SuperGetMv("MV_DABERTO" , NIL , 0)

	If dDataBase > (dPerFim + nDAberto)
		cMsg := STR0075 //O per�odo de apontamento atual encontra-se fechado para a manuten��o de batidas.
		lRet := .F.
	Else
		For nX := 1 To Len(aBatidas)
			If cTod(Format8601(.T.,aBatidas[nX][2])) < dPerIni
				cMsg := STR0074 // A data da batida n�o pode ser inferior � data de in�cio do per�odo de apontamento.
				lRet := .F.
				Exit
			EndIf
		Next nX
	EndIf

EndIf

Return lRet

/*/{Protheus.doc} fUpdGeoClock
Realiza a reprovacao de uma batida que foi incluida por geolocalizao.
@author:	Marcelo Silveira
@since:		25/06/2019
@param:		cBranchVld - Filial;
			cMatSRA - Matricula;
			cCodRH3 - Codigo da solicitacao;
			cObserva - Motivo da rejeicao para ficar registrado na marcacao
			cEmpRest - Empresa
			lJob - A rotina esta sendo executada via job
			cUID - Identificacao do Job
@return:	lRet - Retorna verdadeiro (.T.) quando a operacao foi realizada com sucesso
/*/			
Function fUpdGeoClock( cBranchVld, cMatSRA, cCodRH3, cObserva, cEmpRest, lJob, cUID )

Local cAliasQry 	:= ""
Local cAliasMarc	:= "SP8"
Local nPos			:= 0
Local lRet			:= .F.

Private aMarcacoes	:= {}
Private aTabCalend  := {}
Private aTabPadrao	:= {}

DEFAULT lJob		:= .F.
DEFAULT cEmpRest	:= ""
DEFAULT cUID		:= ""

If lJob
	RPCSetType( 3 )
	RPCSetEnv( cEmpRest, cBranchVld )
EndIf

cAliasQry := GetNextAlias()

BeginSql alias cAliasQry
	SELECT * FROM %table:RH4% RH4 WHERE RH4.RH4_CODIGO = %exp:cCodRH3% AND RH4.%notDel%
EndSql

While !(cAliasQry)->(Eof())
	Do Case
		Case Alltrim((cAliasQry)->RH4_CAMPO) == "P8_FILIAL"
			cFilMat  := SubStr((cAliasQry)->RH4_VALNOV,1,TamSx3('P8_FILIAL')[1])
		Case Alltrim((cAliasQry)->RH4_CAMPO) == "P8_MAT"
			cMat     := SubStr((cAliasQry)->RH4_VALNOV,1,TamSx3('P8_MAT')[1])
		Case Alltrim((cAliasQry)->RH4_CAMPO) == "P8_DATA"
			dData    := CtoD((cAliasQry)->RH4_VALNOV)
		Case Alltrim((cAliasQry)->RH4_CAMPO) == "P8_HORA"
			nHora    := Val(StrTran((cAliasQry)->RH4_VALNOV,":","."))
		Case Alltrim((cAliasQry)->RH4_CAMPO) == "P8_LATITU"
			cLatitu  := AllTrim((cAliasQry)->RH4_VALNOV)						
		Case Alltrim((cAliasQry)->RH4_CAMPO) == "P8_LONGIT"
			cLongit  := AllTrim((cAliasQry)->RH4_VALNOV)
	EndCase
	(cAliasQry)->(dbskip())
Enddo

(cAliasQry)->( DBCloseArea() )

aPeriods := GetPerAponta( 1, cBranchVld , cMatSRA, .F.)
If Len(aPeriods) > 0
	dPerIni := aPeriods[1,1]
	dPerFim := aPeriods[1,2]
EndIf

If !Empty(dPerIni) .And. !Empty(dPerFim) 

	dbSelectArea("SRA")
	SRA->(dbSetOrder(1))
	If SRA->(dbSeek(cBranchVld+cMatSRA))
	
		GetMarcacoes(	@aMarcacoes			,;	//01 -> Marcacoes do Funcionario
						@aTabCalend			,;	//02 -> Calendario de Marcacoes
						@aTabPadrao			,;	//03 -> Tabela Padrao
						NIL     			,;	//04 -> Turnos de Trabalho
						dPerIni				,;	//05 -> Periodo Inicial
						dPerFim				,;	//06 -> Periodo Final
						SRA->RA_FILIAL		,;	//07 -> Filial
						SRA->RA_MAT			,;	//08 -> Matricula
						SRA->RA_TNOTRAB		,;	//09 -> Turno
						SRA->RA_SEQTURN		,;	//10 -> Sequencia de Turno
						SRA->RA_CC			,;	//11 -> Centro de Custo
						cAliasMarc			,;	//12 -> Alias para Carga das Marcacoes
						.T.					,;	//13 -> Se carrega Recno em aMarcacoes
						.T.		 			,;	//14 -> Se considera Apenas Ordenadas
						NIL					,;  //15 -> Verifica as Folgas Automaticas
						NIL  				,;  //16 -> Se Grava Evento de Folga Mes Anterior
						NIL					,;	//17 -> Se Carrega as Marcacoes Automaticas
						NIL					,;	//18 -> Registros de Marcacoes Automaticas que deverao ser Deletados
						NIL					,;	//19
						NIL					,;	//20
						NIL					,;	//21
						NIL					,;	//22
						.F.					,;	//23 -> Se carrega as marcacoes das duas tabelas SP8 e SPG
						)
	
		nPos := aScan( aMarcacoes , { |x| x[AMARC_DATA] == dData .And. x[AMARC_HORA] == nHora .And. AllTrim(x[AMARC_LATITU]) == cLatitu .And. AllTrim(x[AMARC_LONGIT]) == cLongit } ) 
	
		If nPos > 0
			lRet := .T.
			aMarcacoes[ nPos, AMARC_TPMCREP ] := "D"
			aMarcacoes[ nPos, AMARC_MOTIVRG ] := DecodeUTF8( cObserva )
			
			//Atualiza a marcacao que foi desconsiderada pelo gestor
			PutMarcacoes( { aMarcacoes[ nPos ] }, cBranchVld, cMatSRA, "SP8", .F., Nil, Nil, Nil, Nil )
		EndIf

		//Atualiza a tabela RS3
		DbSelectArea("RS3")
		DbSetOrder(1)
		If DbSeek( RH3->(cBranchVld + cCodRH3) )
			RecLock("RS3",.F.)
			RS3->RS3_STATUS := "2" //Reprovado
			RS3->( MsUnLock() )
		EndIf

		//Atualiza a tabela RH3
		DbSelectArea("RH3")
		DbSetOrder(1)
		If RH3->( DbSeek(cBranchVld + cCodRH3) )
			RecLock("RH3",.F.)		
			RH3->RH3_STATUS := "3" //Reprovado
			RH3->RH3_DTATEN := dDataBase
			RH3->( MsUnLock() )
		EndIf
	
	EndIf
EndIf

If lJob
	//Atualiza a variavel de controle que indica a finalizacao do JOB
	PutGlbValue(cUID, "1")
EndIf

Return( lRet )


/*/{Protheus.doc} fPerSPO
Retorna os Periodos de Apontamento da tabela SPO 
@author:	Marcelo Silveira
@since:		12/08/2020
@param:		cEmpSRA - Empresa;
			cFilSRA - Filial;
@return:	aPerSPO - Array com os ultimos 6 periodos de apontamento da SPO
/*/	
Function fPerSPO( cEmpSRA, cFilSRA )

Local nCount	:= 0
Local aPerSPO	:= {}
Local cFilSPO 	:= ""
Local cQrySPO 	:= ""
Local cSPOtab	:= ""
Local cDelete 	:= "% SPO.D_E_L_E_T_ = ' ' %"

If !Empty(cEmpSRA)

	cQrySPO := GetNextAlias()
	cSPOtab := "%" + RetFullName("SPO", cEmpSRA) + "%"
	cFilSPO := xFilial("SPO", cFilSRA)

	BeginSql ALIAS cQrySPO

        COLUMN PO_DATAINI AS DATE
        COLUMN PO_DATAFIM AS DATE

		SELECT PO_DATAINI, PO_DATAFIM 
		FROM %exp:cSPOtab% SPO
		WHERE 
			SPO.PO_FILIAL = %Exp:cFilSPO% AND
			%exp:cDelete%
		ORDER BY 2 DESC
	EndSql

	//Retorna somente os ultimos 6 periodos
	While (cQrySPO)->(!Eof()) .And. (nCount < 7)
		nCount ++
		aAdd( aPerSPO, { (cQrySPO)->PO_DATAINI, (cQrySPO)->PO_DATAFIM } ) 

		(cQrySPO)->( dbSkip() )
	End

	(cQrySPO)->( dbCloseArea() )

EndIf

Return( aPerSPO )
