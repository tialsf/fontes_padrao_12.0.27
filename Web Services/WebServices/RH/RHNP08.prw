#INCLUDE "TOTVS.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "RHNP08.CH"

#DEFINE  PAGE_LENGTH 10

#DEFINE OPERATION_INSERT  1
#DEFINE OPERATION_UPDATE  2
#DEFINE OPERATION_APPROVE 3
#DEFINE OPERATION_REPROVE 4
#DEFINE OPERATION_DELETE  5

Function RHNP08()
Return .T.

Private cMRrhKeyTree := ""


WSRESTFUL Vacation DESCRIPTION STR0001 //"Servi�os de Vacation"
WSDATA type         As String Optional
WSDATA page         As String Optional
WSDATA pageSize     As String Optional
WSDATA employeeId   As String Optional
WSDATA vacationId   As String Optional
WSDATA WsNull       As String Optional

WSMETHOD GET getInfoVacation ;
 DESCRIPTION STR0002 ; //"Servi�o GET que retorna os dados das solicita��es de f�rias."
 WSSYNTAX "/vacation/info/{employeeId}" ;
 PATH "/info/{employeeId}" ;
 PRODUCES 'application/json;charset=utf-8'

WSMETHOD GET getHistoryVacation ;
 DESCRIPTION STR0003 ; //"Servi�o GET que retorna o hist�rico de movimenta��es de f�rias."
 WSSYNTAX "/vacation/history/{employeeId}" ;
 PATH "/history/{employeeId}" ;
 PRODUCES 'application/json;charset=utf-8'

WSMETHOD GET NextDaysVacation ;
 DESCRIPTION STR0029; //Servi�o GET que retorna dados de f�rias programadas do funcion�rio ;
 WSSYNTAX "/vacation/myVacation/{employeeId}" ;
 PATH "/myVacation/{employeeId}" ;
 PRODUCES 'application/json;charset=utf-8'

WSMETHOD POST postRequestVacation ;
  DESCRIPTION STR0010 ; //"Servi�o POST respons�vel pela inclus�o da solicita��o de f�rias."
  WSSYNTAX "/vacation/request/{employeeId}" ;
  PATH "/vacation/request/{employeeId}" ;
  PRODUCES 'application/json;charset=utf-8'

WSMETHOD PUT putRequestVacation ;
  DESCRIPTION STR0011 ; //"Servi�o PUT respons�vel pela edi��o da solicita��o de f�rias."
  WSSYNTAX "/vacation/request/{employeeId}" ;
  PATH "/request/{employeeId}" ;
  PRODUCES 'application/json;charset=utf-8'

WSMETHOD DELETE delRequestVacation ;
  DESCRIPTION STR0012 ; //"Servi�o DEL respons�vel pela exclus�o da solicita��o de f�rias."
  WSSYNTAX "/vacation/request/{employeedId}/{vacationId}" ;
  PATH "/request/{employeedId}/{vacationId}" ;
  PRODUCES 'application/json;charset=utf-8'

WSMETHOD GET NoticeVacation ;
  DESCRIPTION STR0045 ; //"Retorna arquivo PDF do aviso de f�rias"
  WSSYNTAX "/vacation/notice/report/{employeeId}/{vacationId}" ;
  PATH "/notice/report/{employeeId}/{vacationId}" ;
  PRODUCES 'application/json;charset=utf-8'

WSMETHOD GET ReportVacation ;
  DESCRIPTION STR0046 ; //"Retorna arquivo PDF do recibo de f�rias"
  WSSYNTAX "/vacation/detail/report/{employeeId}/{vacationId}" ;
  PATH "/detail/report/{employeeId}/{vacationId}" ;
  PRODUCES 'application/json;charset=utf-8'

WSMETHOD GET getDetailVacation ;
  DESCRIPTION STR0038 ; //Retorna o detalhe do recibo de f�rias
  WSSYNTAX "/vacation/detail/{employeeId}/{vacationId}" ;
  PATH "/detail/{employeeId}/{vacationId}" ;
  PRODUCES 'application/json;charset=utf-8'

END WSRESTFUL


// -------------------------------------------------------------------
// GET - Retorna os dados de f�rias do per�odo aquisitivo aberto
// SRF (F�rias confirmadas do per�odo em aberto)
// RH3 (Solicita��o de F�rias apenas em processo de aprova��o)
//
// retorna estrutura "vacationInfoResponse"
// -- hasNext
// -- Array of vacationInfo
// -------------------------------------------------------------------
WSMETHOD GET getInfoVacation WSREST Vacation

Local nPorDFer     := val( SuperGetMv("MV_PORDFER",,30) )
Local cJsonObj     := "JsonObject():New()"
Local oItem        := &cJsonObj
Local oVac         := &cJsonObj
Local cQuery       := GetNextAlias()
Local aData        := {}
Local aPeriod      := {}
Local aDadosSRH    := {}
Local dDataIniPer  := cToD(" / / ")
Local dDataFimPer  := cToD(" / / ")
Local aDataLogin   := {}
Local lAvalSolic   := .T.
Local lHabSolic    := .T.
Local lLibAlt      := .F.
Local lProgFer     := .F.
Local lExistSRH    := .F.
Local lDemit       := .F.
Local lHabil       := .T.
Local nDiasSRH     := 0
Local nAboSRH      := 0
Local nDiasSolic   := 0
Local nAboProg     := 0
Local nQtdRFxRH    := 0
Local nI           := 0
Local nX           := 0
Local cJson        := ""
Local cToken       := ""
Local cMatSRA      := ""
Local cBranchVld   := ""
Local cLogin       := ""
Local cRD0Cod      := ""
Local cDtaProg     := ""
Local cStatusFer   := ""
Local cStatusLab   := ""
Local nLenParms    := Len(::aURLParms)
Local aOcurances   := {}
Local aDateGMT     := {}
Local cDtConv      := ""

::SetHeader('Access-Control-Allow-Credentials' , "true")
cToken     := Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "vacation", @lHabil)
If !lHabil .Or. lDemit
   SetRestFault(400, EncodeUTF8( STR0055 )) //"Permiss�o negada aos servi�os de f�rias!"
   Return (.F.)  
EndIf

//avalia solicitante e destino da requisi��o 
If nLenParms > 0
   //varinfo("::aUrlParms -> ",::aUrlParms) 
   If (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
      If !Empty(::aUrlParms[2])

         aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
         If Len(aIdFunc) > 1 
         
             //valida se o solicitante da requisi��o pode ter acesso as informa��es
                If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                   cBranchVld	:= aIdFunc[1]
                   cMatSRA		:= aIdFunc[2]
                Else
                   cBranchVld	:= ""
                   cMatSRA		:= ""
             EndIf	
         EndIf

      EndIf
   EndIf
EndIF


If !Empty(cBranchVld) .And. !Empty(cMatSRA)

	//Busca f�rias programadas e confirmadas
	//Status do Periodo de Ferias (1=Ativo / 2=Prescrito / 3-Pago)
	BEGINSQL ALIAS cQuery
        COLUMN RF_DATABAS AS DATE
        COLUMN RF_DATAINI AS DATE
        COLUMN RF_DATINI2 AS DATE
        COLUMN RF_DATINI3 AS DATE
        COLUMN RF_DATAFIM AS DATE
        	      
		SELECT 
			SRF.RF_DATABAS,
			SRF.RF_DATAFIM,
			SRF.RF_DATAINI,
			SRF.RF_DATINI2,
			SRF.RF_DATINI3,
			SRF.RF_DFEPRO1,
			SRF.RF_DFEPRO2,
			SRF.RF_DFEPRO3,
			SRF.RF_DABPRO1,
			SRF.RF_DABPRO2,
			SRF.RF_DABPRO3,
			SRF.RF_PERC13S, 
			SRF.RF_DFERVAT, 
			SRF.RF_DFERAAT,
			SRF.RF_DFERANT, 
			SRF.R_E_C_N_O_
		FROM 
			%Table:SRF% SRF
		WHERE 
			SRF.RF_FILIAL = %Exp:cBranchVld% AND 
			SRF.RF_MAT    = %Exp:cMatSRA%    AND
			SRF.RF_STATUS = '1'              AND
			SRF.%NotDel%
        ORDER BY SRF.RF_DATABAS 			
	ENDSQL
	
   While (cQuery)->(!Eof())

        aPeriod := PeriodConcessive( dtos((cQuery)->RF_DATABAS) , dtos((cQuery)->RF_DATAFIM) )
        //varinfo("aPeriod: ",aPeriod)

        dDataIniPer := aPeriod[1]
        dDataFimPer := aPeriod[2] - nPorDFer
        
        //============================================= 
    	  //Busca solicita��es f�rias em andamento na RH3 do usu�rio recebido diretamente na rota da url	
        //1=Em processo de aprova��o;2=Atendida;3=Reprovada;4=Aguardando Efetiva��o do RH;5=Aguardando Aprova��o do RH
        aOcurances := {}
        If lAvalSolic
           GetVacationWKF(@aOcurances, cMatSRA, cBranchVld, cMatSRA, cBranchVld, cEmpAnt, "'1','4'")
           //varinfo("getInfo aOcurances: ",aOcurances)

           lAvalSolic := .F.
        EndIf   

        nDiasSolic := 0
        nAboProg   := 0
        nQtdRFxRH  := 0

        If Len(aOcurances) > 0 
           //J� existe solicita��o em andamento necessita
           //finalizar para poder realizar um novo processo
           lHabSolic := .F.        

           //inclui registro no array principal
           For nI := 1  To Len(aOcurances)
               oVac                    := &cJsonObj 
               oVac["days"]            := aOcurances[nI][12]                           //Dias de f�rias
               oVac["status"]          := "approving"                                  //"approved" "approving" "reject" "empty" "closed"

               oVac["initVacation"]    := Substr(aOcurances[nI][5],7,4) + "-" + ;
                                          Substr(aOcurances[nI][5],4,2) + "-" + ;
                                          Substr(aOcurances[nI][5],1,2) + "T00:00:00Z" //Data de in�cio das f�rias
               oVac["endVacation"]     := Substr(aOcurances[nI][6],7,4) + "-" + ;
                                          Substr(aOcurances[nI][6],4,2) + "-" + ;
                                          Substr(aOcurances[nI][6],1,2) + "T00:00:00Z"  //Data final das f�rias

               If !empty(aOcurances[nI][21]) .and. !empty(aOcurances[nI][22])
                  oVac["initPeriod"]   := Substr(aOcurances[nI][21],7,4) + "-" + ;
                                          Substr(aOcurances[nI][21],4,2) + "-" + ;
                                          Substr(aOcurances[nI][21],1,2) + "T00:00:00Z" //Data de in�cio das f�rias
                  oVac["endPeriod"]    := Substr(aOcurances[nI][22],7,4) + "-" + ;
                                          Substr(aOcurances[nI][22],4,2) + "-" + ;
                                          Substr(aOcurances[nI][22],1,2) + "T00:00:00Z"  //Data final das f�rias
               EndIf

               If alltrim(aOcurances[nI][17]) == "4"
                  oVac["statusLabel"]  := EncodeUTF8(STR0005)                           //"Aguardando aprova��o do RH"
               Else
                  oVac["statusLabel"]  := EncodeUTF8(STR0004)                           //"Em processo de aprova��o"
               EndIf
               oVac["id"]              := "RH3"              +"|" +;
                                          cBranchVld         +"|" +;
                                          cMatSRA            +"|" +;              
                                          aOcurances[nI][15] +"|" +;              
                                          alltrim( str(aOcurances[nI][16]) )            //Identificador de solicita��es
               oVac["vacationBonus"]   := 0                                             //Dias de abono 
               oVac["advance"]         := 0                                             //Adiantamento do 13
               oVac["hasAdvance"]      := aOcurances[nI][14]                            //Se foi solicitado Adiantamento do 13

               //Avalia possibilidade de altera��o ou exclus�o das solicita��es
               oVac["canAlter"]        := .F.
               oVac["limitDate"]       := ""                                            //Data limite para solicita��o de f�rias
               oVac["balance"]         := 0

               lLibAlt                 := .F.
               
               // Se for a filial+matricula logada for a mesma filial+matricula que iniciou a solicita��o de f�rias
               // E possuir somente 2 hist�ricos, ou seja, nenhuma aprova��o, ent�o permite alterar..
               If aDataLogin[5] == aOcurances[nI][23] .And. aDataLogin[1] == aOcurances[nI][24] .And. Val( GetRGKSeq( aOcurances[nI][15] , .T.) ) == 2

                  oVac["canAlter"]     := .T.
                  oVac["balance"]      := aOcurances[nI][12]                            //Dias de f�rias da solicita��o original

                  aDateGMT             := {}
                  aDateGMT             := LocalToUTC( dtos(dDataFimPer), "12:00:00"  )
                  cDtConv              := DTOS( dDataFimPer )
                  oVac["limitDate"]    := Substr(cDtConv,1,4) + "-" + ;
                                          Substr(cDtConv,5,2) + "-" + ;
                                          Substr(cDtConv,7,2) + "T" + ;
                                          aDateGMT[2] + "Z"                            //Data limite para solicita��o de f�rias
               EndIf

               Aadd(aData,oVac)

               nDiasSolic += val(aOcurances[nI][12])
            Next nI
        EndIf

        //Se tiver ferias calculadas obtem os dados do cabecalho de Ferias
        aDadosSRH  := fGetSRH( cBranchVld, cMatSRA, (cQuery)->RF_DATABAS, (cQuery)->RF_DATAFIM )
        lExistSRH  := Len(aDadosSRH) > 0
        
        //Status padrao das programacoes � aprovado. Altera para calculado (calculated) somente quando existe calculo
        cStatusFer := "approved"
        cStatusLab := EncodeUTF8(STR0009) //"Confirmada"

        //============================================= 
        //********************* Avalia programa��es SRF
        lProgFer := .F.

        //Carrega o primeiro per�odo de f�rias confirmados
		If !Empty((cQuery)->RF_DATAINI) .and. (cQuery)->RF_DATAINI > dDataBase
		
            If lExistSRH
               If Ascan( aDadosSRH, {|x| ( x[1] == DTOS((cQuery)->RF_DATAINI) .And. x[2] == (cQuery)->RF_DFEPRO1 ) } ) > 0 
                  cStatusFer 	:= "calculated"
                  cStatusLab	:= EncodeUTF8(STR0047) //"Calculada"
                  nQtdRFxRH		+= (cQuery)->RF_DFEPRO1
               EndIf
           EndIf		
		
           oVac                    := &cJsonObj
           oVac["balance"]         := 0
           oVac["days"]            := (cQuery)->RF_DFEPRO1                               //Dias de f�rias
           oVac["status"]          := cStatusFer                                         //"approved" "approving" "reject" "empty" "closed"

           cDtConv                 := dTos( (cQuery)->RF_DATAINI ) 
           oVac["initVacation"]    := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Data de in�cio das f�rias

           cDtConv                 := dTos( (cQuery)->RF_DATAINI + ((cQuery)->RF_DFEPRO1 - 1) ) 
           oVac["endVacation"]     := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Data final das f�rias

           cDtConv                 := dTos( (cQuery)->RF_DATABAS ) 
           oVac["initPeriod"]      := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Inicio do per�odo aquisitivo  "2018-02-01T00:00:00Z"

           cDtConv                 := dTos( (cQuery)->RF_DATAFIM ) 
           oVac["endPeriod"]       := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Final do per�odo aquisitivo   "2019-01-31T00:00:00Z"

           oVac["statusLabel"]     := cStatusLab		                                //"Confirmada" "Calculada
           oVac["id"]              := "SRF"                         +"|" +;
                                      cBranchVld                    +"|" +;
                                      cMatSRA                       +"|" +;              
                                      dtos((cQuery)->RF_DATABAS)    +"|" +;              
                                      dtos((cQuery)->RF_DATAINI)    +"|" +;              
                                      alltrim(str((cQuery)->R_E_C_N_O_))                 //Identificador de f�rias
           oVac["vacationBonus"]   := (cQuery)->RF_DABPRO1                               //Dias de abono
           oVac["advance"]         := (cQuery)->RF_PERC13S                               //optional - Adiantamento do 13
           If (cQuery)->RF_PERC13S > 0           
               oVac["hasAdvance"]  := .T.                                                //Se foi solicitado Adiantamento do 13
           Else
               oVac["hasAdvance"]  := .F.                                                //Se foi solicitado Adiantamento do 13
           EndIf    
           oVac["limitDate"]       := ""                                                 //Data limite para solicita��o de f�rias
           oVac["canAlter"]        := .F.                                                //Verifica se a solicita��o de f�rias pode ser editada e exclu�da de acordo com o status
           Aadd(aData,oVac)

           lProgFer := .T.
		EndIf
        nDiasSolic += (cQuery)->RF_DFEPRO1 + (cQuery)->RF_DABPRO1
        nAboProg   := (cQuery)->RF_DABPRO1
		
        //Carrega o segundo per�odo de f�rias confirmado
		If !Empty((cQuery)->RF_DATINI2) .and. (cQuery)->RF_DATINI2 > dDataBase

            If lExistSRH
               If Ascan( aDadosSRH, {|x| ( x[1] == DTOS((cQuery)->RF_DATINI2) .And. x[2] == (cQuery)->RF_DFEPRO2 ) } ) > 0 
                  cStatusFer 	:= "calculated"
                  cStatusLab	:= EncodeUTF8(STR0047) //"Calculada"
                  nQtdRFxRH		+= (cQuery)->RF_DFEPRO2
               EndIf
           EndIf

           oVac                    := &cJsonObj
           oVac["balance"]         := 0
           oVac["days"]            := (cQuery)->RF_DFEPRO2                               //Dias de f�rias
           oVac["status"]          := cStatusFer                                         //"approved" "approving" "reject" "empty" "closed"

           cDtConv                 := dTos( (cQuery)->RF_DATINI2 ) 
           oVac["initVacation"]    := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Data de in�cio das f�rias

           cDtConv                 := dTos( (cQuery)->RF_DATINI2 + ((cQuery)->RF_DFEPRO2 - 1) ) 
           oVac["endVacation"]     := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Data final das f�rias

           cDtConv                 := dTos( (cQuery)->RF_DATABAS ) 
           oVac["initPeriod"]      := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Inicio do per�odo aquisitivo  "2018-02-01T00:00:00Z"

           cDtConv                 := dTos( (cQuery)->RF_DATAFIM ) 
           oVac["endPeriod"]       := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Final do per�odo aquisitivo   "2019-01-31T00:00:00Z"

           oVac["statusLabel"]     := cStatusLab		                                //"Confirmada" "Calculada
           oVac["id"]              := "SRF"                         +"|" +;
                                      cBranchVld                    +"|" +;
                                      cMatSRA                       +"|" +;              
                                      dtos((cQuery)->RF_DATABAS)    +"|" +;              
                                      dtos((cQuery)->RF_DATINI2)    +"|" +;              
                                      alltrim(str((cQuery)->R_E_C_N_O_))                 //Identificador de f�rias
           oVac["vacationBonus"]   := (cQuery)->RF_DABPRO2                               //Dias de abono
           oVac["advance"]         := (cQuery)->RF_PERC13S                               //optional - Adiantamento do 13
           If (cQuery)->RF_PERC13S > 0           
               oVac["hasAdvance"]  := .T.                                                //Se foi solicitado Adiantamento do 13
           Else
               oVac["hasAdvance"]  := .F.                                                //Se foi solicitado Adiantamento do 13
           EndIf    
           oVac["limitDate"]       := ""                                                 //Data limite para solicita��o de f�rias
           oVac["canAlter"]        := .F.                                                //Verifica se a solicita��o de f�rias pode ser editada e exclu�da de acordo com o status
           Aadd(aData,oVac)

           lProgFer := .T.
		EndIf
        nDiasSolic += (cQuery)->RF_DFEPRO2 + (cQuery)->RF_DABPRO2
        nAboProg   := (cQuery)->RF_DABPRO2
		
        //Carrega o terceiro per�odo de f�rias confirmado
		If !Empty((cQuery)->RF_DATINI3)  .and. (cQuery)->RF_DATINI3 > dDataBase

            If lExistSRH
               If Ascan( aDadosSRH, {|x| ( x[1] == DTOS((cQuery)->RF_DATINI3) .And. x[2] == (cQuery)->RF_DFEPRO3 ) } ) > 0 
                  cStatusFer 	:= "calculated"
                  cStatusLab	:= EncodeUTF8(STR0047) //"Calculada"
                  nQtdRFxRH		+= (cQuery)->RF_DFEPRO3
               EndIf
           EndIf

           oVac                    := &cJsonObj
           oVac["balance"]         := 0
           oVac["days"]            := (cQuery)->RF_DFEPRO3                               //Dias de f�rias
           oVac["status"]          := cStatusFer                                         //"approved" "approving" "reject" "empty" "closed"

           cDtConv                 := dTos( (cQuery)->RF_DATINI3 ) 
           oVac["initVacation"]    := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Data de in�cio das f�rias

           cDtConv                 := dTos( (cQuery)->RF_DATINI3 + ((cQuery)->RF_DFEPRO3 - 1) ) 
           oVac["endVacation"]     := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Data final das f�rias

           cDtConv                 := dTos( (cQuery)->RF_DATABAS ) 
           oVac["initPeriod"]      := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Inicio do per�odo aquisitivo  "2018-02-01T00:00:00Z"

           cDtConv                 := dTos( (cQuery)->RF_DATAFIM ) 
           oVac["endPeriod"]       := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Final do per�odo aquisitivo   "2019-01-31T00:00:00Z"

           oVac["statusLabel"]     := cStatusLab		                                //"Confirmada" "Calculada
           oVac["id"]              := "SRF"                         +"|" +;
                                      cBranchVld                    +"|" +;
                                      cMatSRA                       +"|" +;              
                                      dtos((cQuery)->RF_DATABAS)    +"|" +;              
                                      dtos((cQuery)->RF_DATINI3)    +"|" +;              
                                      alltrim(str((cQuery)->R_E_C_N_O_))                 //Identificador de f�rias
           oVac["vacationBonus"]   := (cQuery)->RF_DABPRO3                               //Dias de abono
           oVac["advance"]         := (cQuery)->RF_PERC13S                               //optional - Adiantamento do 13
           If (cQuery)->RF_PERC13S > 0           
               oVac["hasAdvance"]  := .T.                                                //Se foi solicitado Adiantamento do 13
           Else
               oVac["hasAdvance"]  := .F.                                                //Se foi solicitado Adiantamento do 13
           EndIf    
           oVac["limitDate"]       := ""                                                 //Data limite para solicita��o de f�rias
           oVac["canAlter"]        := .F.                                                //Verifica se a solicita��o de f�rias pode ser editada e exclu�da de acordo com o status
           Aadd(aData,oVac)

           lProgFer := .T.
		EndIf
        nDiasSolic += (cQuery)->RF_DFEPRO3 + (cQuery)->RF_DABPRO3
        nAboProg   := (cQuery)->RF_DABPRO3


        /*  avaliar registros na SRH calculados com data de inicio posterior a data do sistema, 
            para que possa ser criado um card, caso n�o existam programa��es na SRF...
            al�m disso ser utilizado para abater do saldo dispon�vel de f�rias, prevendo que
            o fechamento ainda possa n�o ter sido realizado, atualizando os saldos na SRF  */
        nDiasSRH := 0
        nAboSRH  := 0
        cDtaProg := If( Empty((cQuery)->RF_DATAINI), "", dtos((cQuery)->RF_DATAINI) ) + "|" 
        cDtaProg += If( Empty((cQuery)->RF_DATINI2), "", dtos((cQuery)->RF_DATINI2) ) + "|"
        cDtaProg += If( Empty((cQuery)->RF_DATINI3), "", dtos((cQuery)->RF_DATINI3) ) + "|"
        
        For nX := 1 To Len( aDadosSRH )

          //Considera as Ferias se nao houver programacao, ou se houver ferias mas com data de inicio diferente da programacao 
           If ( STOD(aDadosSRH[nX,1]) >= dDataBase .And. (!lProgFer .Or. !aDadosSRH[nX,1] $ cDtaProg ) ) 
              oVac                    := &cJsonObj 
              oVac["balance"]         := 0                      //Saldo dispon�vel
              oVac["days"]            := aDadosSRH[nX,2]        //Dias de f�rias
              oVac["status"]          := "calculated"           //"approved" "approving" "reject" "empty" "closed" "calculated"
              
              oVac["initVacation"]    := Substr(aDadosSRH[nX,1],1,4) + "-" + ;
                                         Substr(aDadosSRH[nX,1],5,2) + "-" + ;
                                         Substr(aDadosSRH[nX,1],7,2) + "T00:00:00Z" //Data de in�cio das f�rias
              cDtConv                 := dTos( aDadosSRH[nX,3] ) 
              oVac["endVacation"]     := Substr(cDtConv,1,4) + "-" + ;
                                         Substr(cDtConv,5,2) + "-" + ;
                                         Substr(cDtConv,7,2) + "T00:00:00Z"  //Data final das f�rias

              oVac["initPeriod"]      := Substr(aDadosSRH[nX,4],1,4) + "-" + ;
                                         Substr(aDadosSRH[nX,4],5,2) + "-" + ;
                                         Substr(aDadosSRH[nX,4],7,2) + "T00:00:00Z"  //Inicio do per�odo aquisitivo  "2018-02-01T00:00:00Z"
              cDtConv                 := dTos( aDadosSRH[nX,5] ) 
              oVac["endPeriod"]       := Substr(cDtConv,1,4) + "-" + ;
                                         Substr(cDtConv,5,2) + "-" + ;
                                         Substr(cDtConv,7,2) + "T00:00:00Z"  //Final do per�odo aquisitivo   "2019-01-31T00:00:00Z"
              
              oVac["statusLabel"]     := EncodeUTF8(STR0027)    //"Em processo de c�lculo"
              oVac["id"]              := "SRH"              +"|" +;
                                         cBranchVld         +"|" +;
                                         cMatSRA            +"|" +;              
                                         aDadosSRH[nX,4]    +"|" +;              
                                         aDadosSRH[nX,1]    +"|" +;              
                                         aDadosSRH[nX,8]        //Identificador de f�rias
              oVac["vacationBonus"]   := aDadosSRH[nX,6]        //Dias de abono
              oVac["advance"]         := aDadosSRH[nX,7]        //optional - Adiantamento do 13
              If aDadosSRH[nX,7]  > 0           
                 oVac["hasAdvance"]  := .T.                    //Se foi solicitado Adiantamento do 13
              Else
                 oVac["hasAdvance"]  := .F.                    //Se foi solicitado Adiantamento do 13
              EndIf    
              oVac["limitDate"]       := ""                    //Data limite para solicita��o de f�rias
              oVac["canAlter"]        := .F.                   //Verifica se a solicita��o de f�rias pode ser editada e exclu�da de acordo com o status
              Aadd(aData,oVac)
           EndIf

           nDiasSRH := nDiasSRH + aDadosSRH[nX,2] //Dias de ferias 
           nAboSRH  := nAboSRH  + aDadosSRH[nX,6] //Dias de abono pecuniario

        Next nX
        
        /* Carrega o card para "solicitar f�rias" em virtude de saldo pendente
           apenas para o primeiro per�odo aquisitivo em aberto, para que as 
           solicita��es dos saldos sejam feitos na sequencia
           
           exemplo:
           DATABAS   DATAFIM   DIASDIR(dias vencidos)  DFERVAT(dias vencidos)  DFERAAT(dias proporcionais)
           20180210  20190209     30                     30                       0
           20190210  20200209     30                     0                       10
        */
        //Considera o abono do calculo somente quando ele nao consta na programacao de ferias
        If nAboProg == 0 .And. nAboSRH > 0
            nDiasSRH += nAboSRH
        EndIf

        If ( ( (cQuery)->RF_DFERVAT - (nDiasSolic + nDiasSRH - nQtdRFxRH) ) > 0    .or.  ;
             ( (cQuery)->RF_DFERAAT - (nDiasSolic + nDiasSRH - nQtdRFxRH) ) > 0 )  .and. ;
             lHabSolic

           lHabSolic := .F.        
           oVac                    := &cJsonObj

           //Avalia se as ferias calculadas ja constavam na programacao (nQtdRFxRH). 
           //Neste caso, nao podemos considerar os dois valores senao os dias de ferias pendentes ficarao incorretos.           
           If (cQuery)->RF_DFERVAT > 0
              oVac["balance"]      := (cQuery)->RF_DFERVAT - (nDiasSolic + nDiasSRH - nQtdRFxRH)
           Else
              oVac["balance"]      := (cQuery)->RF_DFERAAT - (nDiasSolic + nDiasSRH - nQtdRFxRH)
           EndIf
           
           oVac["status"]          := "empty"                                            //"approved" "approving" "reject" "empty" "closed"
           oVac["initVacation"]    := ""
           oVac["endVacation"]     := ""

           cDtConv                 := dTos( (cQuery)->RF_DATABAS ) 
           oVac["initPeriod"]      := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Inicio do per�odo aquisitivo  "2018-02-01T00:00:00Z"

           cDtConv                 := dTos( (cQuery)->RF_DATAFIM ) 
           oVac["endPeriod"]       := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T00:00:00Z"                 //Final do per�odo aquisitivo   "2019-01-31T00:00:00Z"

           oVac["id"]              := "SRF"                         +"|" +;
                                      cBranchVld                    +"|" +;
                                      cMatSRA                       +"|" +;              
                                      dtos((cQuery)->RF_DATABAS)    +"|" +;              
                                      dtos((cQuery)->RF_DATAFIM)    +"|" +;              
                                      alltrim(str((cQuery)->R_E_C_N_O_))                 //Identificador de f�rias
           oVac["hasAdvance"]      := .F.                                                //Se foi solicitado Adiantamento do 13

           aDateGMT				   := {}
           aDateGMT  			   := LocalToUTC( dtos(dDataFimPer), "12:00:00"  )
           cDtConv                 := DTOS( dDataFimPer )
           oVac["limitDate"]       := Substr(cDtConv,1,4) + "-" + ;
                                      Substr(cDtConv,5,2) + "-" + ;
                                      Substr(cDtConv,7,2) + "T" + ;
                                      aDateGMT[2] + "Z"                                  //Data limite para solicita��o de f�rias

           oVac["canAlter"]        := .F.                                               //Verifica se a solicita��o de f�rias pode ser editada e exclu�da de acordo com o status

           If oVac["balance"] > 0
              Aadd(aData,oVac)
           EndIf
        EndIf 


        (cQuery)->( DbSkip() )
    EndDo
	
	(cQuery)->(DBCloseArea())		

ENDIF

    oItem["hasNext"] := .F.
    oItem["items"]   := aData

    cJson := FWJsonSerialize(oItem, .F., .F., .T.)
    ::SetResponse(cJson)

Return(.T.)


// ---------------------------------------------------------
// GET - Retorna o hist�rico de solicita��es de f�rias 
// SRH (F�rias processadas)
// RH3 (Solicita��es de F�rias reprovadas)
//
// retorna estrutura "vacationInfoResponse"
// -- hasNext
// -- Array of vacationInfo
// ---------------------------------------------------------
WSMETHOD GET getHistoryVacation WSREST Vacation

Local cJsonObj      := "JsonObject():New()"
Local oItem         := &cJsonObj
Local oVac          := &cJsonObj
Local aData         := {}
Local aDataResult   := {}
Local aDataLogin    := {}
Local cQuery        := GetNextAlias()
Local aPeriod       := {}
Local dDataIniPer   := cToD(" / / ")
Local dDataFimPer   := cToD(" / / ")
Local cJson         := ""
Local cToken        := ""
Local cMatSRA       := ""
Local cBranchVld    := ""
Local cLogin        := ""
Local cRD0Cod       := ""
Local aServices     := {}
Local aOcurances    := {}
Local aPerFerias    := {}
Local lMaisPaginas  := .F.
Local lHabil        := .T.
Local lDemit        := .F.
Local nI, nX        := 0
Local nRegCount     := 0
Local nRegCountIni  := 0 
Local nRegCountFim  := 0
Local nLenParms     := Len(::aURLParms)

DEFAULT Self:page     := "1"
DEFAULT Self:pageSize := "10"

::SetHeader('Access-Control-Allow-Credentials' , "true")
cToken     := Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
	lDemit     := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "vacation", @lHabil)
If !lHabil .Or. lDemit
   SetRestFault(400, EncodeUTF8( STR0055 )) //"Permiss�o negada aos servi�os de f�rias!"
   Return (.F.)  
EndIf

//Prepara o controle de paginacao
If Self:page == "1" .Or. Self:page == ""
  nRegCountIni := 1 
  nRegCountFim := val(Self:pageSize)
Else
  nRegCountIni := ( val(Self:pageSize) * (val(Self:Page) - 1)  ) + 1
  nRegCountFim := ( nRegCountIni + val(Self:pageSize) ) - 1
EndIf   

//avalia solicitante e destino da requisi��o 
If nLenParms > 0
   //varinfo("::aUrlParms -> ",::aUrlParms) 
   If (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
      If !Empty(::aUrlParms[2])

         aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
         If Len(aIdFunc) > 1 
         
             //valida se o solicitante da requisi��o pode ter acesso as informa��es
                If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                   cBranchVld	:= aIdFunc[1]
                   cMatSRA		:= aIdFunc[2]
                Else
                   cBranchVld	:= ""
                   cMatSRA		:= ""
             EndIf	
         EndIf

      EndIf
   EndIf
EndIF


If !Empty(cBranchVld) .And. !Empty(cMatSRA)

   //================================== 
   //Avalia as f�rias calculadas na SRH
	BEGINSQL ALIAS cQuery
        COLUMN RH_DATABAS AS DATE
        COLUMN RH_DBASEAT AS DATE
        COLUMN RH_DATAINI AS DATE
        COLUMN RH_DATAFIM AS DATE
	
		SELECT 
			SRH.RH_DATABAS,
			SRH.RH_DBASEAT,
			SRH.RH_DATAINI,
			SRH.RH_DATAFIM,
			SRH.RH_DABONPE,
			SRH.RH_ACEITE,
			SRH.RH_DFERIAS,
			SRH.RH_DFERVEN,
			SRH.RH_PERC13S,
			SRH.R_E_C_N_O_,
			SRH.RH_FILIAL,
			SRH.RH_MAT
		FROM 
			%Table:SRH% SRH
		WHERE 
			SRH.RH_FILIAL   = %Exp:cBranchVld% AND 
			SRH.RH_MAT      = %Exp:cMatSRA %   AND
			SRH.%NotDel%
		ORDER BY
			SRH.RH_DATABAS DESC		 
	ENDSQL

	While (cQuery)->(!Eof())
	
          //F�rias calculadas mais que a data de inicio ainda n�o ocorreu, ser�o 
          //disponibilizadas no card do servi�o "/vacation/info" como confirmadas 
          If (cQuery)->RH_DATAINI < dDataBase           
	
             oVac                    := &cJsonObj 
             oVac["balance"]         := ((cQuery)->RH_DFERVEN - (cQuery)->RH_DFERIAS) //Saldo dispon�vel
             oVac["days"]            := (cQuery)->RH_DFERIAS                          //Dias de f�rias
             oVac["status"]          := "closed"                                      //"approved" "approving" "reject" "empty" "closed"
             oVac["initVacation"]    := (cQuery)->RH_DATAINI                          //Data inicial das f�rias
             oVac["endVacation"]     := (cQuery)->RH_DATAFIM                          //Data final das f�rias
             oVac["initPeriod"]      := (cQuery)->RH_DATABAS                          //Inicio do per�odo aquisitivo  "2018-02-01T00:00:00Z"
             oVac["endPeriod"]       := (cQuery)->RH_DBASEAT                          //Final do per�odo aquisitivo   "2019-01-31T00:00:00Z"

             If (cQuery)->RH_DATAFIM < dDataBase           
                oVac["statusLabel"]  := EncodeUTF8(STR0006)                           //"Finalizadas"
             Else  
                oVac["statusLabel"]  := EncodeUTF8(STR0007)                           //"Em Andamento"
             EndIf

             oVac["id"]              := "SRH"                         +"|" +;
                                        (cQuery)->RH_FILIAL           +"|" +;
                                        (cQuery)->RH_MAT              +"|" +;              
                                        dtos((cQuery)->RH_DATABAS)    +"|" +;              
                                        dtos((cQuery)->RH_DATAINI)    +"|" +;              
                                        alltrim(str((cQuery)->R_E_C_N_O_))            //Identificador de f�rias

             oVac["vacationBonus"]   := (cQuery)->RH_DABONPE                          //Dias de abono
             oVac["advance"]         := (cQuery)->RH_PERC13S                          //optional - Adiantamento do 13
             If (cQuery)->RH_PERC13S > 0           
                 oVac["hasAdvance"]  := .T.                                           //Se foi solicitado Adiantamento do 13
             Else
                 oVac["hasAdvance"]  := .F.                                           //Se foi solicitado Adiantamento do 13
             EndIf    
             oVac["limitDate"]       := ""                                            //Data limite para solicita��o de f�rias
             oVac["canAlter"]        := .F.                                           //Verifica se a solicita��o de f�rias pode ser editada e exclu�da de acordo com o status
             Aadd(aData,oVac)

          EndIf
          
         (cQuery)->(DBSkip())
	EndDo
	(cQuery)->(DBCloseArea())


    //==================================== 
    //Busca solicita��es rejeitadas na RH3 do usu�rio recebido diretamente na rota da url
    //1=Em processo de aprova��o;2=Atendida;3=Reprovada;4=Aguardando Efetiva��o do RH;5=Aguardando Aprova��o do RH
    aOcurances := {}
    GetVacationWKF(@aOcurances, cMatSRA, cBranchVld, cMatSRA, cBranchVld, cEmpAnt, "'3'")
    //varinfo("getHistory aOcurances: ",aOcurances)

    If Len(aOcurances) > 0 
        //inclui registro no array principal
        For nI := 1  To Len(aOcurances)
            oVac                    := &cJsonObj 
            oVac["balance"]         := 0
            oVac["days"]            := aOcurances[nI][12]                           //Dias de f�rias
            oVac["status"]          := "reject"                                     //"approved" "approving" "reject" "empty" "closed"

            oVac["initVacation"]    := cTod(aOcurances[nI][5])                      //Data de in�cio das f�rias
            oVac["endVacation"]     := cTod(aOcurances[nI][6])                      //Data final das f�rias
            If !empty(aOcurances[nI][21]) .and. !empty(aOcurances[nI][22])
               oVac["initPeriod"]      := cTod(aOcurances[nI][21])
               oVac["endPeriod"]       := cTod(aOcurances[nI][22])
            EndIf

            oVac["statusLabel"]     := EncodeUTF8(STR0008)                          //"Rejeitada"
            oVac["id"]              := "RH3"              +"|" +;
                                       cBranchVld         +"|" +;
                                       cMatSRA            +"|" +;              
                                       aOcurances[nI][15] +"|" +;              
                                       alltrim( str(aOcurances[nI][16]) )           //Identificador de solicita��es
            oVac["vacationBonus"]   := aOcurances[nI][7]                            //Dias de abono 
            oVac["advance"]         := 0                                            //Adiantamento do 13
            oVac["hasAdvance"]      := aOcurances[nI][14]                           //Se foi solicitado Adiantamento do 13
            oVac["limitDate"]       := ""                                           //Data limite para solicita��o de f�rias
            oVac["canAlter"]        := .F.
            Aadd(aData,oVac)
        Next nI
    EndIf


    //Ordenando resultado pela data de inicio das f�ras
    ASORT(aData, , , { | x,y | x["initVacation"] > y["initVacation"] } )

    //Avalia a pagina��o ap�s a ordena��o
    For nI := 1 To Len(aData)

		nRegCount ++
		If ( nRegCount >= nRegCountIni .And. nRegCount <= nRegCountFim )
            Aadd(aDataResult , aData[nI])
		Else
			If nRegCount > nRegCountFim
				lMaisPaginas := .T.
			EndIf   
		EndIf   

    Next nI

ENDIF

    oItem["hasNext"]  := lMaisPaginas
    oItem["items"]    := aDataResult
    oItem["length"]   := Len(aData)

    cJson := FWJsonSerialize(oItem, .F., .F., .T.)
    ::SetResponse(cJson)

Return(.T.)

WSMETHOD GET NextDaysVacation WSREST Vacation

Local cJsonObj      := "JsonObject():New()"
Local oItem         := &cJsonObj
Local oVac          := &cJsonObj
Local cQuery        := GetNextAlias()
Local aData         := {}
Local aPeriod       := {}
Local aDateGMT      := {}
Local aIDFunc       := {}
Local aServices     := {}
Local aDataLogin    := {}
Local cRestFault    := ""
Local cJson         := ""
Local cToken        := ""
Local cMatSRA       := ""
Local cID           := ""
Local cBranchVld    := ""
Local cLogin        := ""
Local cRD0Cod       := ""
Local cStatus       := ""
Local cDtConv       := ""
local cDtBsIni      := ""
Local cDtBsFim      := ""
Local cDtFerIni     := ""
Local dDataIniPer   := cTod("")
Local dDataFimPer   := cTod("")
Local nDiasFer      := 0
Local nPorDFer      := Val(SuperGetMv("MV_PORDFER",,"30"))
Local lRet          := .T.
Local lTem13        := .F.
Local lDemit        := .F.
Local lHabil        := .T.

::SetHeader('Access-Control-Allow-Credentials' , "true")
cToken      := Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "vacation", @lHabil)
If !lHabil .Or. lDemit
   SetRestFault(400, EncodeUTF8( STR0055 )) //"Permiss�o negada aos servi�os de f�rias!"
   Return (.F.)  
EndIf

//avalia solicitante e destino da requisi��o 
If len(::aUrlParms) > 0

   //varinfo("::aUrlParms -> ",::aUrlParms) 
   If (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
      If !Empty(::aUrlParms[2])

         aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
         If Len(aIdFunc) > 1 
         
             //valida se o solicitante da requisi��o pode ter acesso as informa��es
                If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                   cBranchVld	:= aIdFunc[1]
                   cMatSRA		:= aIdFunc[2]
                Else
                   cRestFault := EncodeUTF8(STR0043) +cBranchVld +"/" +cMatSRA //"usu�rio sem permiss�o para execu��o da requisi��o: "
                   cBranchVld	:= ""
                   cMatSRA		:= ""
             EndIf	
         EndIf

      EndIf
   EndIf
EndIF

If !Empty(cBranchVld) .And. !Empty(cMatSRA)

  BEGINSQL ALIAS cQuery
      COLUMN RF_DATABAS AS DATE
      COLUMN RF_DATAINI AS DATE
      COLUMN RF_DATINI2 AS DATE
      COLUMN RF_DATINI3 AS DATE
      COLUMN RF_DATAFIM AS DATE
            
   SELECT 
      SRF.RF_DATABAS,
      SRF.RF_DATAFIM,
      SRF.RF_DATAINI,
      SRF.RF_DATINI2,
      SRF.RF_DATINI3,
      SRF.RF_DFEPRO1,
      SRF.RF_DFEPRO2,
      SRF.RF_DFEPRO3,
      SRF.RF_DABPRO1,
      SRF.RF_DABPRO2,
      SRF.RF_DABPRO3,
      SRF.RF_PERC13S, 
      SRF.RF_DFERVAT, 
      SRF.RF_DFERAAT,
      SRF.RF_DFERANT, 
      SRF.R_E_C_N_O_
   FROM 
      %Table:SRF% SRF
   WHERE 
      SRF.RF_FILIAL = %Exp:cBranchVld% AND 
      SRF.RF_MAT    = %Exp:cMatSRA%    AND
      SRF.RF_STATUS = '1'              AND
      SRF.%NotDel%
      ORDER BY SRF.RF_DATABAS		
   ENDSQL

  While (cQuery)->(!Eof())


   //Busca informa��es do per�odo concessivo para saber a data limite de cada per�odo.
   //Somente busca se existir programa��o
   If !Empty((cQuery)->RF_DATAINI) .Or. !Empty((cQuery)->RF_DATINI2) .Or. !Empty((cQuery)->RF_DATINI3)
      aPeriod     := PeriodConcessive( dtos((cQuery)->RF_DATABAS) , dtos((cQuery)->RF_DATAFIM) )
      dDataIniPer := aPeriod[1]
      dDataFimPer := aPeriod[2] - nPorDFer
   EndIf

   lTem13 := .F.
   //Verifica se existe programa��o e a data de inicio � menor que a database
   If !Empty((cQuery)->RF_DATAINI) .And. (cQuery)->RF_DATAINI > dDataBase
      nDiasFer  := (cQuery)->RF_DFEPRO1               // Qtde dias de f�rias
      cStatus   := "approved"                         // Status

      cDtFerIni := dToS((cQuery)->RF_DATAINI)         // Data in�cio f�rias.
      cDtFerIni := Substr(cDtFerIni,1,4) + "-" + ;
                   SubStr(cDtFerIni,5,2) + "-" + ;
                   SubStr(cDtFerIni,7,2) + "-" + ;
                   "T00:00:00Z"

      cDtBsIni  := dToS((cQuery)->RF_DATABAS)         // Database in�cio
      cDtBsIni  := Substr(cDtBsIni,1,4) + "-" + ;
                   SubStr(cDtBsIni,5,2) + "-" + ;
                   SubStr(cDtBsIni,7,2) + "-" + ;
                   "T00:00:00Z"

      cDtBsFim  := dToS((cQuery)->RF_DATAFIM)         // Database fim
      cDtBsFim  := Substr(cDtBsFim,1,4) + "-" + ;
                   SubStr(cDtBsFim,5,2) + "-" + ;
                   SubStr(cDtBsFim,7,2) + "-" + ;
                   "T00:00:00Z"                 

      cID       := cBranchVld + "|" + cMatSRA + "|" + dToS((cQuery)->RF_DATABAS) + "|" + dToS((cQuery)->RF_DATAINI)                   
      lTem13    := (cQuery)->RF_PERC13S > 0

      cDtConv   := dToS(dDataFimPer)                  //Data limite para gozo
      cDtConv   := Substr(cDtConv,1,4) + "-" + ;
                   SubStr(cDtConv,5,2) + "-" + ;
                   SubStr(cDtConv,7,2) + "-" + ;
                   "T00:00:00Z"
      
      //Se encontrar, sai do loop
      Exit

   ElseIf !Empty((cQuery)->RF_DATINI2) .And. (cQuery)->RF_DATINI2 > dDataBase
      nDiasFer  := (cQuery)->RF_DFEPRO2               // Qtde dias de f�rias
      cStatus   := "approved"                         // Status

      cDtFerIni := dToS((cQuery)->RF_DATINI2)         // Data in�cio f�rias.   
      cDtFerIni := Substr(cDtFerIni,1,4) + "-" + ;
                   SubStr(cDtFerIni,5,2) + "-" + ;
                   SubStr(cDtFerIni,7,2) + "-" + ;
                   "T00:00:00Z"
 

      cDtBsIni  := dToS((cQuery)->RF_DATABAS)          // Database in�cio
      cDtBsIni  := Substr(cDtBsIni,1,4) + "-" + ;
                   SubStr(cDtBsIni,5,2) + "-" + ;
                   SubStr(cDtBsIni,7,2) + "-" + ;
                   "T00:00:00Z"

      cDtBsFim  := dToS((cQuery)->RF_DATAFIM)           // Database fim
      cDtBsFim  := Substr(cDtBsFim,1,4) + "-" + ;
                   SubStr(cDtBsFim,5,2) + "-" + ;
                   SubStr(cDtBsFim,7,2) + "-" + ;
                   "T00:00:00Z" 
                   
      cID       := cBranchVld + "|" + cMatSRA + "|" + dToS((cQuery)->RF_DATABAS) + "|" + dToS((cQuery)->RF_DATINI2)
      lTem13    := (cQuery)->RF_PERC13S > 0

      cDtConv   := dToS(dDataFimPer)                    //Data limite para gozo
      cDtConv   := Substr(cDtConv,1,4) + "-" + ;
                   SubStr(cDtConv,5,2) + "-" + ;
                   SubStr(cDtConv,7,2) + "-" + ;
                   "T00:00:00Z"
      //Se encontrar, sai do loop
      Exit
      
   ElseIf !Empty((cQuery)->RF_DATINI3) .And. (cQuery)->RF_DATINI3 > dDataBase
      nDiasFer  := (cQuery)->RF_DFEPRO3                  // Qtde dias de f�rias
      cStatus   := "approved"                            // Status

      cDtFerIni := dToS((cQuery)->RF_DATINI3)            // Data in�cio f�rias. 
      cDtFerIni := Substr(cDtFerIni,1,4) + "-" + ;
                   SubStr(cDtFerIni,5,2) + "-" + ;
                   SubStr(cDtFerIni,7,2) + "-" + ;
                   "T00:00:00Z"

      cDtBsIni  := dToS((cQuery)->RF_DATABAS)             // Database in�cio
      cDtBsIni  := Substr(cDtBsIni,1,4) + "-" + ;
                   SubStr(cDtBsIni,5,2) + "-" + ;
                   SubStr(cDtBsIni,7,2) + "-" + ;
                   "T00:00:00Z"

      cDtBsFim  := dToS((cQuery)->RF_DATAFIM)            // Database fim
      cDtBsFim  := Substr(cDtBsFim,1,4) + "-" + ;
                   SubStr(cDtBsFim,5,2) + "-" + ;
                   SubStr(cDtBsFim,7,2) + "-" + ;
                   "T00:00:00Z" 

      cID       := cBranchVld + "|" + cMatSRA + "|" + dToS((cQuery)->RF_DATABAS) + "|" + dToS((cQuery)->RF_DATINI3)
      lTem13    := (cQuery)->RF_PERC13S > 0

      cDtConv   := dToS(dDataFimPer)                     //Data limite para gozo
      cDtConv   := Substr(cDtConv,1,4) + "-" + ;   
                   SubStr(cDtConv,5,2) + "-" + ;
                   SubStr(cDtConv,7,2) + "-" + ;
                   "T00:00:00Z" 
      
      //Se encontrar, sai do loop
      Exit

   Else
      //Se n�o existir programa��o, verifica se h� f�rias calculada para o per�odo aquisitivo em quest�o.
      SRH->(dbSetOrder(1))
      If SRH->(dbSeek(cBranchVld + cMatSRA + dToS((cQuery)->RF_DATABAS))) 

         //Busca o per�odo concessivo de acordo com os dados da SRH.
         aPeriod     := PeriodConcessive( dtos(SRH->RH_DATABAS) , dtos(SRH->RH_DBASEAT) )
         dDataIniPer := aPeriod[1]
         dDataFimPer := aPeriod[2] - nPorDFer

         nDiasFer    := SRH->RH_DFERIAS                  //quantidade de dias de f�rias
         cStatus     := "approved"                       //status

         cDtFerIni := dToS(SRH->RH_DATAINI)              //Data in�cio das f�rias.
         cDtFerIni := Substr(cDtFerIni,1,4) + "-" + ;
                      SubStr(cDtFerIni,5,2) + "-" + ;
                      SubStr(cDtFerIni,7,2) + "-" + ;
                      "T00:00:00Z"

         cDtBsIni    := dToS(SRH->RH_DATABAS)            
         cDtBsIni    := Substr(cDtBsIni,1,4) + "-" + ;   //database in�cio
                        SubStr(cDtBsIni,5,2) + "-" + ;
                        SubStr(cDtBsIni,7,2) + "-" + ;
                        "T00:00:00Z"

         cDtBsFim    := dToS(SRH->RH_DBASEAT)  
         cDtBsFim    := Substr(cDtBsFim,1,4) + "-" + ;   //database fim
                        SubStr(cDtBsFim,5,2) + "-" + ;
                        SubStr(cDtBsFim,7,2) + "-" + ;
                        "T00:00:00Z" 

         cID         := cBranchVld + "|" + cMatSRA + "|" + dToS(SRH->RH_DATABAS) + "|" + dToS(SRH->RH_DATAINI)
         lTem13      := SRH->RH_PERC13S > 0              //solicitado 13?

         cDtConv     := dToS(dDataFimPer)
         cDtConv     := Substr(cDtConv,1,4) + "-" + ;    //data limite para gozo das f�rias;
                        SubStr(cDtConv,5,2) + "-" + ;
                        SubStr(cDtConv,7,2) + "-" + ;
                        "T00:00:00Z" 
         
         //Se encontrar, sai do loop
         Exit
      EndIf
   EndIf

   (cQuery)->(dbSkip())
  EndDo

  (cQuery)->(dbCloseArea())
ENDIF

If empty(cRestFault) .And. lRet
   oVac                    := &cJsonObj
   oVac["days"]            := nDiasFer
   oVac["status"]          := cStatus
   oVac["initVacation"]    := cDtFerIni
   oVac["initPeriod"]      := cDtBsIni
   oVac["endPeriod"]       := cDtBsFim
   oVac["id"]              := cID
   oVac["hasAdvance"]      := lTem13
   oVac["limitDate"]       := cDtConv
   
   lRet := .T.
   cJson := FWJsonSerialize(oVac, .F., .F., .T.)
   ::SetResponse(cJson)  
Else
   lRet := .F.
   SetRestFault(400, EncodeUTF8(cRestFault), .T.)
EndIf

Return (lRet)


// -------------------------------------------------------------------
// - Atualiza��o da inclus�o de solicita��o de f�rias.
// -------------------------------------------------------------------
WSMETHOD POST postRequestVacation WSREST Vacation
Local cJsonObj          := "JsonObject():New()"
Local oItemDetail       := &cJsonObj
Local oItemData         := &cJsonObj
Local oItem             := &cJsonObj
Local oMsgReturn        := &cJsonObj

Local lRet              := .T.
Local cRestFault        := ""
Local nReturnCode       := 0 
Local cRoutine          := "W_PWSA100A.APW" 
Local cBody             := ::GetContent()
Local cAliasRH4         := GetNextAlias()

Local cApprover         := ""
Local cEmpApr           := ""
Local cFilApr           := ""
Local cVision           := ""
Local aVision           := {}
Local aGetStruct        := {}
Local aEmployee         := {}
Local aMessages         := {}
Local aServices         := {}
Local aDataLogin        := {}
Local nSupLevel         := 0
Local nDias             := 0
Local nDiasAbn          := 0
Local lSolic13          := .F.
Local lDemit            := .F.
Local lHabil            := .T.
Local cIniVac           := ""
Local cEndVac           := ""
Local cIniPer           := ""
Local cEndPer           := ""

Local cBranchVld        := FwCodFil()
Local cMatSRA           := ""
Local cLogin            := ""
Local cRD0Cod           := ""
Local cBranchSolic      := ""
Local cMatSolic         := ""

Local oRequest          := Nil
Local oVacationRequest  := Nil

::SetHeader('Access-Control-Allow-Credentials' , "true")
cToken     := Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

    //Valida Permissionamento
	 fPermission(cBranchVld, cLogin, cRD0Cod, "vacation", @lHabil)
	 If !lHabil .Or. lDemit
         SetRestFault(400, EncodeUTF8( STR0055 )) //"Permiss�o negada aos servi�os de f�rias!"
         Return (.F.)  
	 EndIf

    If !Empty(cBody)

	   oItemDetail:FromJson(cBody)
       nDias   := Iif(oItemDetail:hasProperty("days"),oItemDetail["days"], 0)
       nDiasAbn:= Iif(oItemDetail:hasProperty("vacationBonus"),oItemDetail["vacationBonus"], 0)
       cIniVac := Iif(oItemDetail:hasProperty("initVacation"), Format8601(.T.,oItemDetail["initVacation"]), "")
       cEndVac := Iif(oItemDetail:hasProperty("endVacation"), Format8601(.T.,oItemDetail["endVacation"]), "")
       lSolic13:= Iif(oItemDetail:hasProperty("hasAdvance"),oItemDetail["hasAdvance"], .F.)       
       cIniPer := Iif(oItemDetail:hasProperty("initPeriod"), Format8601(.T.,oItemDetail["initPeriod"]), "")
       cEndPer := Iif(oItemDetail:hasProperty("endPeriod"), Format8601(.T.,oItemDetail["endPeriod"]), "")

       //avalia solicitante e destino da requisi��o 
       If len(::aUrlParms) > 0
          //varinfo("::aUrlParms -> ",::aUrlParms) 

          If (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
             If !Empty(::aUrlParms[2])
       
                aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
                If Len(aIdFunc) > 1 
                
                    //valida se o solicitante da requisi��o pode ter acesso as informa��es
                    If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                       cMatSolic    := cMatSRA
                       cBranchSolic := cBranchVld

                       cBranchVld	:= aIdFunc[1]
                       cMatSRA		:= aIdFunc[2]
                    Else
                       cRestFault   := EncodeUTF8(STR0043) +cBranchVld +"/" +cMatSRA //"usu�rio sem permiss�o para execu��o da requisi��o: "
                       cBranchVld	:= ""
                       cMatSRA		:= ""
                    EndIf	
                EndIf
       
             EndIf
          EndIf
       ELSE
         cRestFault := EncodeUTF8(STR0044) //"erro na requisi��o do servi�o de f�rias"
       EndIF

       If empty(cRestFault) .and. (cPaisLoc $ "BRA|CHI|PAR|URU")
          //valida��es diversas para f�rias
          cRestFault := fVldSolicFer(cBranchVld,cMatSRA,cIniVac,nDias,nDiasAbn,lSolic13)
       ENDIF

       If empty(cRestFault)
          //busca dados do solicitante
          aEmployee := getSummary( cMatSRA, cBranchVld)

          //busca vis�o para a solicita��o de f�rias
	      aVision := GetVisionAI8(cRoutine, cBranchVld)
	      cVision := aVision[1][1]

          //busca estrutura organizacional do workflow
      	 cMRrhKeyTree := fMHRKeyTree(cBranchSolic, cMatSolic)
          aGetStruct   := APIGetStructure(cRD0Cod, SUPERGETMV("MV_ORGCFG"), cVision, cBranchVld, cMatSRA, , , , "B", cBranchSolic, cMatSolic, , , , , .T.,, {cEmpAnt})
          //varinfo("aGetStruct: ",aGetStruct)

          If valtype(aGetStruct[1]) == "L" .and. !aGetStruct[1] 
             cRestFault := alltrim( EncodeUTF8(aGetStruct[2]) +" - " +EncodeUTF8(aGetStruct[3]) ) 
          Else
             If Len(aGetStruct) >= 1 .And. !(Len(aGetStruct) == 3 .And. !aGetStruct[1])
                cEmpApr   := aGetStruct[1]:ListOfEmployee[1]:SupEmpresa
                cFilApr   := aGetStruct[1]:ListOfEmployee[1]:SupFilial
                nSupLevel := aGetStruct[1]:ListOfEmployee[1]:LevelSup
                cApprover := aGetStruct[1]:ListOfEmployee[1]:SupRegistration
             EndIf
          EndIf   
       EndIf


       If empty(cRestFault)

         //Prepara objeto RH3 Request
         oRequest              := WSClassNew("TRequest")
         oRequest:RequestType  := WSClassNew("TRequestType")
         oRequest:Status       := WSClassNew("TRequestStatus")

         oRequest:Code                               := GetSX8Num("RH3", "RH3_CODIGO",RetSqlName("RH3"))
         oRequest:RequestType:Code                   := "B"	                //Ferias
         oRequest:Status:Code                        := "1"
         oRequest:Origem                             := EncodeUTF8(STR0013) //MEURH
         oRequest:RequestDate                        := dDataBase
         oRequest:ResponseDate                       := CTod("")
         oRequest:Branch				                 := cBranchVld
         oRequest:Registration		                 := cMatSRA
         oRequest:StarterKey                         := fBuscaChaveFuncionario(cBranchVld, cMatSRA, cVision)
         oRequest:StarterBranch		                 := If( !Empty(cBranchSolic), cBranchSolic, cBranchVld )           //pedido feito pelo pr�prio usu�rio do app
         oRequest:StarterRegistration	              := If( !Empty(cMatSolic), cMatSolic, cMatSRA )                   //pedido feito pelo pr�prio usu�rio do app
         oRequest:Vision				                 := cVision
         oRequest:Observation                        := Alltrim(STR0014 +" - " +aEmployee[2] +" - " +dToC(date()) +Space(1) +Time())
         oRequest:ApproverBranch                     := cFilApr
         oRequest:ApproverRegistration               := cApprover
         oRequest:ApproverLevel                      := nSupLevel

         //Utilizada tratativa do portal. Caso n�o exista aprovador, solicita��o entrar� como aprovada automaticamente.
         If Empty(cApprover)
            oRequest:ApproverLevel                   := 0
            oRequest:Status:Code                     := "4"
            oRequest:ResponseDate                    := dDataBase
         EndIf
         
         oRequest:EmpresaAPR                         := cEmpApr
         oRequest:Empresa                            := cEmpAnt

         //Prepara objeto RH4 Vacation
         oVacationRequest                            := WSClassNew("TVacation")
         oVacationRequest:Branch                     := cBranchVld
         oVacationRequest:Registration               := cMatSRA
         oVacationRequest:Name                       := aEmployee[2]
         oVacationRequest:InitialDate                := cIniVac
         oVacationRequest:FinalDate                  := cEndVac
         oVacationRequest:Days                       := nDias
         oVacationRequest:PecuniaryDays              := nDiasAbn
         oVacationRequest:PecuniaryAllowance         := Iif( nDiasAbn>0, ".T.", ".F.") 
         oVacationRequest:ThirteenthSalary1stInstall := Iif( lSolic13  , ".T.", ".F.")  


         Begin Transaction

             //Atualiza requisi��o diretamente em virtude da origem 
             DBSelectArea("RH3")
	          DBSetOrder(1)	//RH3_FILIAL+RH3_CODIGO

             Reclock("RH3", .T.)
             RH3->RH3_CODIGO	:= oRequest:Code
             RH3->RH3_FILIAL	:= oRequest:Branch
             RH3->RH3_MAT		:= oRequest:Registration
             RH3->RH3_TIPO		:= oRequest:RequestType:Code
             RH3->RH3_ORIGEM	:= "MEURH"
             RH3->RH3_DTSOLI	:= oRequest:RequestDate
             RH3->RH3_NVLINI	:= oRequest:StarterLevel
             RH3->RH3_FILINI	:= oRequest:StarterBranch
             RH3->RH3_MATINI	:= oRequest:StarterRegistration
             RH3->RH3_KEYINI	:= oRequest:StarterKey
             RH3->RH3_VISAO   := oRequest:Vision
             RH3->RH3_STATUS  := oRequest:Status:Code
             RH3->RH3_NVLAPR  := oRequest:ApproverLevel
             RH3->RH3_FILAPR  := oRequest:ApproverBranch
             RH3->RH3_MATAPR  := oRequest:ApproverRegistration

             If RH3->(ColumnPos("RH3_EMP")) > 0 .AND. RH3->(ColumnPos("RH3_EMPINI")) > 0 .AND. RH3->(ColumnPos("RH3_EMPAPR")) > 0
               If Empty(oRequest:Empresa)
                  oRequest:Empresa := cEmpAnt
               EndIf
            
               If Empty(oRequest:EmpresaAPR)
                  oRequest:EmpresaAPR := cEmpAnt
               EndIf
            
               RH3->RH3_EMP	   := oRequest:Empresa
               RH3->RH3_EMPINI	:= cEmpAnt
               RH3->RH3_EMPAPR	:= oRequest:EmpresaAPR
             ElseIf RH3->(ColumnPos("RH3_EMPAPR")) > 0
               RH3->RH3_EMPAPR	:= oRequest:EmpresaAPR
             EndIf

             RH3->(MsUnlock())


             //Atualiza detalhes e hist�rico
             nReturnCode:= fAddVacationRequest(oVacationRequest, oRequest:Code, OPERATION_INSERT)
             If nReturnCode > 0
                cRestFault := EncodeUTF8(STR0028) //"Erro na inclus�o da solicita��o de f�rias"
                Break
             Else
                //complementa RH4 com os dados do per�odo aquisito relacionado 
                BeginSQL ALIAS cAliasRH4
                   SELECT COUNT(*) QTD  FROM %table:RH4% RH4
                		  WHERE RH4.RH4_CODIGO = %exp:oRequest:Code%
                            AND RH4.RH4_FILIAL = %exp:oVacationRequest:Branch%
                            AND RH4.%NotDel%
                EndSQL

                If (cAliasRH4)->(!Eof()) .and. (cAliasRH4)->QTD > 0   
                   DBSelectArea("RH4")

                   Reclock("RH4", .T.)
                   RH4->RH4_FILIAL	:= oVacationRequest:Branch 
                   RH4->RH4_CODIGO	:= oRequest:Code
                   RH4->RH4_ITEM	:= (cAliasRH4)->QTD + 1
                   RH4->RH4_CAMPO	:= "RF_DATABAS"
                   RH4->RH4_VALNOV	:= cIniPer
                   RH4->(MsUnlock())

                   Reclock("RH4", .T.)
                   RH4->RH4_FILIAL	:= oVacationRequest:Branch 
                   RH4->RH4_CODIGO	:= oRequest:Code
                   RH4->RH4_ITEM	:= (cAliasRH4)->QTD + 2
                   RH4->RH4_CAMPO	:= "RF_DATAFIM"
                   RH4->RH4_VALNOV	:= cEndPer
                   RH4->(MsUnlock())
                EndIf
               (cAliasRH4)->( dbCloseArea() )

               
                //Atualiza hist�rico
                nReturnCode:= fPutHistory(oRequest, OPERATION_INSERT) //RGK - RDY
                If nReturnCode > 0
                   cRestFault := EncodeUTF8(STR0028) //"Erro na inclus�o da solicita��o de f�rias"
                   Break
                EndIf
             EndIf

         End Transaction

       EndIf
    EndIf 


    If empty(cRestFault) .And. lRet
       oMsgReturn["type"]         := "success"
       oMsgReturn["code"]         := "200"
       oMsgReturn["detail"]       := EncodeUTF8(STR0015) //"Atualiza��o realizada com sucesso"
       Aadd(aMessages, oMsgReturn)

       //atualiza dados para o retorno
       oItemData["days"]          := nDias
       oItemData["vacationBonus"] := nDiasAbn
       oItemData["initVacation"]  := cIniVac
       oItemData["endVacation"]   := cEndVac
       oItemData["hasAdvance"]    := Iif(lSolic13, "true", "false")
       oItemData["initPeriod"]    := cIniPer
       oItemData["endPeriod"]     := cEndPer

       oItem["data"]              := oItemData
       oItem["messages"]          := aMessages
       oItem["length"]            := 1

       cJson :=  FWJsonSerialize(oItem, .F., .F., .T.)
       ::SetResponse(cJson)
    Else
       lRet := .F.
       SetRestFault(400, EncodeUTF8(cRestFault), .T.)
    EndIf

Return(lRet)


// -------------------------------------------------------------------
// - Atualiza��o da edi��o dos itens da solicita��o de f�rias.
// -------------------------------------------------------------------
WSMETHOD PUT putRequestVacation WSREST Vacation
Local cJsonObj       := "JsonObject():New()"
Local oItem          := &cJsonObj
Local oItemData      := &cJsonObj
Local oItemDetail    := &cJsonObj
Local oMsgReturn     := &cJsonObj
Local cBody          := ::GetContent()
Local lRet           := .T.
Local aMessages      := {}
Local aParam         := {}
Local aServices      := {}
Local aDataLogin     := {}

Local cRestFault     := ""
Local cBranchVld     := ""
Local cMatSRA        := ""
Local cLogin         := ""
Local cRD0Cod        := ""
Local cToken         := ""
Local cJson          := ""
Local cKey           := ""

Local nDias          := 0
Local nDiasAbn       := 0
Local lSolic13       := .F.
Local lDemit			:= .F.
Local lHabil			:= .T.
Local cIniVac        := ""
Local cEndVac        := ""
Local cIniPer        := ""
Local cEndPer        := ""
Local cID            := ""

::SetHeader('Access-Control-Allow-Credentials' , "true")
cToken     := Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

   //Valida Permissionamento
   fPermission(cBranchVld, cLogin, cRD0Cod, "vacation", @lHabil)
   If !lHabil .Or. lDemit
      SetRestFault(400, EncodeUTF8( STR0055 )) //"Permiss�o negada aos servi�os de f�rias!"
      Return (.F.)  
   EndIf

    If !Empty(cBody)
	    oItemDetail:FromJson(cBody)
       cID     := Iif(oItemDetail:hasProperty("id"),oItemDetail["id"], "")

       nDias   := Iif(oItemDetail:hasProperty("days"),oItemDetail["days"], 0)
       nDiasAbn:= Iif(oItemDetail:hasProperty("vacationBonus"),oItemDetail["vacationBonus"], 0)
       cIniVac := Iif(oItemDetail:hasProperty("initVacation"), Format8601(.T.,oItemDetail["initVacation"]), "")
       cEndVac := Iif(oItemDetail:hasProperty("endVacation"), Format8601(.T.,oItemDetail["endVacation"]), "")
       cIniPer := Iif(oItemDetail:hasProperty("initPeriod"), Format8601(.T.,oItemDetail["initPeriod"]), "")
       cEndPer := Iif(oItemDetail:hasProperty("endPeriod"), Format8601(.T.,oItemDetail["endPeriod"]), "")

       If oItemDetail:hasProperty("hasAdvance") 
          //chegando com tipo caracter no PUT
          If (Valtype(oItemDetail["hasAdvance"]) == "L" .And. oItemDetail["hasAdvance"]) .Or. ;
             (Valtype(oItemDetail["hasAdvance"]) == "C" .And. oItemDetail["hasAdvance"] == ".T.")
             lSolic13 := .T.
          Else  
             lSolic13 := .F.
          EndIf       
       Else
          lSolic13 := .F.
       EndIf

       //avalia solicitante e destino da requisi��o 
       If len(::aUrlParms) > 0
          //varinfo("::aUrlParms -> ",::aUrlParms) 
          If (::aUrlParms[2] != "%7Bcurrent%7D") .and. (::aUrlParms[2] != "{current}")
             If !Empty(::aUrlParms[2])
       
                aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
                If Len(aIdFunc) > 1 
                
                    //valida se o solicitante da requisi��o pode ter acesso as informa��es
                    If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                       cBranchVld	:= aIdFunc[1]
                       cMatSRA		:= aIdFunc[2]
                    Else
                       cRestFault   := EncodeUTF8(STR0043) +cBranchVld +"/" +cMatSRA //"usu�rio sem permiss�o para execu��o da requisi��o: "
                       cBranchVld	:= ""
                       cMatSRA		:= ""
                    EndIf	
                EndIf
       
             EndIf
          EndIf
       ELSE
         cRestFault := EncodeUTF8(STR0044) //"erro na requisi��o do servi�o de f�rias"
       EndIF
       
       If empty(cRestFault)
          aParam := StrTokArr(cID , "|")
          cKey   := aParam[2] + aParam[4]

          //Valida movimenta��o do workflow
          cRestFault := fVldWkf(cKey, aParam[4], "U")
       ENDIF

       If empty(cRestFault) .and. (cPaisLoc $ "BRA|CHI|PAR|URU")
          //valida��es diversas para f�rias
          cRestFault := fVldSolicFer(cBranchVld,cMatSRA,cIniVac,nDias,nDiasAbn,lSolic13)
       EndIf
    Else
       cRestFault := STR0026 //"Informa��es da requisi��o n�o recebida"
    EndIf
    
    If empty(cRestFault)
       //atualiza RH4
       Begin Transaction
           DBSelectArea("RH4")
           DBSetOrder(1)
           RH4->(DbSeek(RH3->(RH3_FILIAL + RH3_CODIGO)))

           While RH4->(RH4_FILIAL + RH4_CODIGO) == RH3->(RH3_FILIAL + RH3_CODIGO) .And. !RH4->(Eof())

              RecLock("RH4", .F.)
                 If AllTrim(RH4->RH4_CAMPO) == "R8_DATAINI"
                    RH4->RH4_VALANT := RH4->RH4_VALNOV
                    RH4->RH4_VALNOV := cIniVac
                 ElseIf AllTrim(RH4->RH4_CAMPO) == "R8_DATAFIM"
                    RH4->RH4_VALANT := RH4->RH4_VALNOV
                    RH4->RH4_VALNOV := cEndVac
                 ElseIf AllTrim(RH4->RH4_CAMPO) == "R8_DURACAO"
                    RH4->RH4_VALANT := RH4->RH4_VALNOV
                    RH4->RH4_VALNOV := AllTrim(Str(nDias))   
                 ElseIf AllTrim(RH4->RH4_CAMPO) == "TMP_ABONO"
                    RH4->RH4_VALANT := RH4->RH4_VALNOV
                    If nDiasAbn > 0  
                       RH4->RH4_VALNOV := ".T."
                    Else
                       RH4->RH4_VALNOV := ".F."
                    EndIf
                 ElseIf AllTrim(RH4->RH4_CAMPO) == "TMP_DABONO"
                    RH4->RH4_VALANT := RH4->RH4_VALNOV
                    RH4->RH4_VALNOV := AllTrim(Str(nDiasAbn))       
                 ElseIf AllTrim(RH4->RH4_CAMPO) == "TMP_1P13SL"
                    RH4->RH4_VALANT := RH4->RH4_VALNOV
                    If lSolic13  
                       RH4->RH4_VALNOV := ".T."
                    Else
                       RH4->RH4_VALNOV := ".F."
                    EndIf
                 EndIf
              MsUnLock()

              RH4->(DbSkip())
           EndDo
       End Transaction
    EndIf

    If empty(cRestFault)
       oMsgReturn["type"]      := "success"
       oMsgReturn["code"]      := "200"
       oMsgReturn["detail"]    := EncodeUTF8(STR0022) //"Altera��o realizada com sucesso"

       Aadd(aMessages, oMsgReturn)

       If !Empty(cBody)
          //atualiza dados para o retorno
          oItemData["days"]          := nDias
          oItemData["vacationBonus"] := nDiasAbn
          oItemData["initVacation"]  := cIniVac
          oItemData["endVacation"]   := cEndVac
          oItemData["hasAdvance"]    := Iif(lSolic13, .T. , .F. )
          oItemData["initPeriod"]    := cIniPer
          oItemData["endPeriod"]     := cEndPer
       EndIf

       oItem["data"]                 := oItemData
       oItem["messages"]             := aMessages
       oItem["length"]               := 1

       cJson :=  FWJsonSerialize(oItem, .F., .F., .T.)
       ::SetResponse(cJson)
    Else
       lRet := .F.
       SetRestFault(400, EncodeUTF8(cRestFault), .T.)
    EndIf

Return(lRet)


// -------------------------------------------------------------------
// - Atualiza��o da dele��o de solicita��o de f�rias.
// -------------------------------------------------------------------
WSMETHOD DELETE delRequestVacation WSREST Vacation
Local lRet           := .T.
Local lDemit			:= .F.
Local lHabil			:= .T.
Local cJsonObj       := "JsonObject():New()"
Local cBody          := ::GetContent()
Local aUrlParam      := ::aUrlParms
Local oItem          := &cJsonObj
Local oItemData      := &cJsonObj
Local oMsgReturn     := &cJsonObj
Local aMessages      := {}
Local aParam         := {}
Local aIDFunc        := {}
Local aDataLogin     := {}
Local aServices      := {}

Local cRestFault     := ""
Local cBranchVld     := ""
Local cMatSRA        := ""
Local cToken         := ""
Local cLogin         := ""
Local cRD0Cod        := ""
Local cJson          := ""
Local cKey           := ""
Local cKeyRGK        := ""

::SetHeader('Access-Control-Allow-Credentials' , "true")
cToken     := Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA     := aDataLogin[1]
   cLogin      := aDataLogin[2]
   cRD0Cod     := aDataLogin[3]
   cBranchVld  := aDataLogin[5]
   lDemit      := aDataLogin[6]
EndIf

	//Valida Permissionamento
	fPermission(cBranchVld, cLogin, cRD0Cod, "vacation", @lHabil)
	If !lHabil .Or. lDemit
		SetRestFault(400, EncodeUTF8( STR0055 )) //"Permiss�o negada aos servi�os de f�rias!"
		Return (.F.)  
	EndIf

   If len(aUrlParam) > 0 .And. !Empty(aUrlParam[1]) .And. aUrlParam[1] == "request"

      //origem requisi��o DELETE
      If Len(aUrlParam) == 3 .And. aUrlParam[3] != "undefined"
         aParam := StrTokArr(aUrlParam[3], "|")
      EndIf

      //"RH3" , Filial , Matricula , RH3_CODIGO , R_E_C_N_O_
      If len(aParam) != 5
         cRestFault := STR0023 //"Erro na requisi��o, par�metros invalidos"
      EndIf
       
      //avalia solicitante e destino da requisi��o 
      If empty(cRestFault)
        If (aUrlParam[2] != "%7Bcurrent%7D") .and. (aUrlParam[2] != "{current}")
           If !Empty(aUrlParam[2])
               aIdFunc := STRTOKARR( aUrlParam[2], "|" )
               If Len(aIdFunc) > 1 
               
                  //valida se o solicitante da requisi��o pode ter acesso as informa��es
                  If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                     cBranchVld	:= aIdFunc[1]
                     cMatSRA		:= aIdFunc[2]
                  Else
                     cRestFault   := EncodeUTF8(STR0043) +cBranchVld +"/" +cMatSRA //"usu�rio sem permiss�o para execu��o da requisi��o: "
                     cBranchVld	:= ""
                     cMatSRA		:= ""
                  EndIf	
               EndIf
           EndIf
        EndIf
      EndIF
      
   EndIf

   If empty(cRestFault)
      cKey    := aParam[2] + aParam[4]
      cKeyRGK := aParam[2] + aParam[3] + aParam[4] //Filial + Matricula + C�digo

      //Valida movimenta��o do workflow
      cRestFault := fVldWkf(cKey, aParam[4], "D")
   ENDIF

   If empty(cRestFault)

      Begin Transaction
         RecLock("RH3",.F.)
         RH3->(dbDelete())
         RH3->(MsUnlock())

         DbSelectArea("RH4")
         RH4->( dbSetOrder(1) )
         RH4->( dbSeek(cKey) )
         While !Eof() .And. RH4->(RH4_FILIAL+RH4_CODIGO) == cKey;

            RecLock("RH4",.F.)
            RH4->(dbDelete())
            RH4->(MsUnlock())
            RH4->(dBSkip())

         EndDo

         DbSelectArea("RGK")
         RGK->( dbSetOrder(1) )
         RGK->( dbSeek(cKeyRGK) )
         While !Eof() .And. RGK->(RGK_FILIAL+RGK_MAT+RGK_CODIGO) == cKeyRGK;

            RecLock("RGK",.F.)
            RGK->(dbDelete())
            RGK->(MsUnlock())
            RGK->(dBSkip())

         EndDo
      End Transaction

   EndIf

    If empty(cRestFault)
       HttpSetStatus(204)

       oMsgReturn["type"]      := "success"
       oMsgReturn["code"]      := "204"
       oMsgReturn["detail"]    := EncodeUTF8(STR0022) //"Exclus�o realizada com sucesso"
       Aadd(aMessages, oMsgReturn)

       oItem["data"]           := oItemData
       oItem["messages"]       := aMessages
       oItem["length"]         := 1

       cJson :=  FWJsonSerialize(oItem, .F., .F., .T.)
       ::SetResponse(cJson)
    Else
       lRet := .F.
       SetRestFault(400, EncodeUTF8(cRestFault), .T.)
    EndIf

Return(lRet)

// -------------------------------------------------------------------
// Retorna o arquivo PDF do AVISO de f�rias
// -------------------------------------------------------------------
WSMETHOD GET NoticeVacation WSREST Vacation
Local cJsonObj    := "JsonObject():New()"
Local oItem       := &cJsonObj
Local aLog        := {}
Local aIdFunc     := {}
Local aDados      := {}
Local aInfo       := {}
Local aDataLogin  := {}
Local cToken      := ""
Local cMatSRA     := ""
Local cBranchVld  := ""
Local cLogin      := ""
Local cRD0Cod     := ""
Local cDescFunc   := ""
Local cPer        := ""
Local cExtFile    := ""
Local nX          := 0
Local nCont       := 0
Local lContinua   := .T.
Local lRet        := .T.
Local lDemit		:= .F.
Local lHabil		:= .T.
Local dPgDataBas  := CtoD('//')
Local dPgDataIni  := CtoD('//')

Local oFile       := Nil
Local cArqLocal   := ""
Local cFileName   := ""
Local cFile       := ""
Local cPDF        := ".PDF"

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken      := Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "vacationNotice", @lHabil)
If !lHabil .Or. lDemit
   SetRestFault(400, EncodeUTF8( STR0056 )) //"Permiss�o negada aos servi�os para aviso de f�rias!"
   Return (.F.)  
EndIf

If  Len(::aUrlParms) == 4

	If !( "current" $ ::aUrlParms[3] )
		If !Empty(::aUrlParms[3])

			aIdFunc := STRTOKARR( ::aUrlParms[3], "|" )
			If Len(aIdFunc) > 1 
			
			    //valida se o solicitante da requisi��o pode ter acesso as informa��es
                If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                   cBranchVld	:= aIdFunc[1]
                   cMatSRA		:= aIdFunc[2]
                Else
                   cBranchVld	:= ""
                   cMatSRA		:= ""
	    		EndIf	
			EndIf

		EndIf
	EndIf

	If !Empty(::aUrlParms[4]) 
		aIdVac := STRTOKARR( ::aUrlParms[4], "|" )

        //aIdVac[1] - tipo do registro de f�rias SRF ou SRH
        //aIdVac[2] - Filial
        //aIdVac[3] - Matr�cula
        //aIdVac[4] - Data Base
        //aIdVac[5] - Data Inicial
        //aIdVac[6] - RECNO

        If !len(aIdVac) > 0 .or. (aIdVac[1] != "SRF" .and. aIdVac[1] != "SRH")
           lContinua := .F.
        Else
           //Busca dados da empresa/filial (RHLIBFIL)
           lContinua := fInfo(@aInfo, cBranchVld)
        EndIf
    EndIf
Else
    lContinua := .F.
EndIF


If !Empty(cBranchVld) .And. !Empty(cMatSRA) .And. lContinua
    
	//Posiciona a tabela SRA na matricula que esta sendo impressa
	dbSelectArea("SRA")
	dbSetOrder(1)
	If dbSeek( cBranchVld + cMatSRA )
	
	   cExtFile		:= DTOS( DATE() ) + SubStr( TIME(), 1, 2) //Ano + Mes + Dia + Hora
	   cFileName 	:= Alltrim(cBranchVld) + "_" + Alltrim(cMatSRA) + "_notice_"
	   cArqLocal 	:= GetSrvProfString ("STARTPATH","")
       cDescFunc    := Alltrim(Posicione("SRJ", 1, xFilial("SRJ", SRA->RA_FILIAL) + SRA->RA_CODFUNC, "SRJ->RJ_DESC"))

       aDados := {}
       aAdd( aDados, ""                          ) //  1) inicializa tipo do aviso
       aAdd( aDados, AllTrim(aInfo[5])           ) //  2) inicializa estado da empresa
       aAdd( aDados, cToD(" / / ")               ) //  3) inicializa data do aviso
	   If ! Empty(SRA->RA_NOMECMP)
		  aAdd( aDados, AllTrim(SRA->RA_NOMECMP) ) //  4) inicializa nome do funcion�rio
       Else 
          aAdd( aDados, AllTrim(SRA->RA_NOME)    ) //  4) inicializa nome do funcion�rio
       EndIf
       aAdd( aDados, SRA->RA_NUMCP               ) //  5) inicializa carteira de trabalho
       aAdd( aDados, SRA->RA_SERCP               ) //  6) inicializa s�rie da carteira de trabalho
       aAdd( aDados, cDescFunc                   ) //  7) inicializa descri��o da fun��o
       aAdd( aDados, 0                           ) //  8) inicializa dias de licen�a remunerada m�s seguinte
       aAdd( aDados, 0                           ) //  9) inicializa dias de licen�a remunerada
       aAdd( aDados, cToD(" / / ")               ) // 10) inicializa inicio do per�odo aquisitivo
       aAdd( aDados, cToD(" / / ")               ) // 11) inicializa t�rmino do per�odo aquisitivo
       aAdd( aDados, cToD(" / / ")               ) // 12) inicializa inicio do gozo de f�rias
       aAdd( aDados, cToD(" / / ")               ) // 13) inicializa t�rmino do gozo de f�rias
       aAdd( aDados, AllTrim(aInfo[3])           ) // 14) inicializa nome da empresa
       aAdd( aDados, aInfo[8]                    ) // 15) inicializa CNPJ da empresa
       aAdd( aDados, 0                           ) // 16) inicializa dias de abono pecuni�rio
       aAdd( aDados, ""                          ) // 17) inicializa informa��es do aceite
       aAdd( aDados, SRA->RA_FILIAL              ) // 18) inicializa filial do funcion�rio
       aAdd( aDados, SRA->RA_MAT                 ) // 19) inicializa matr�cula do funcion�rio
       aAdd( aDados, SRA->RA_ADMISSA             ) // 20) inicializa admiss�o do funcion�rio
       aAdd( aDados, cToD(" / / ")               ) // 21) inicializa data do recibo
       
       If aIdVac[1] == "SRH" //F�rias Calculadas

          dbSelectArea("SRH")
	      dbSetOrder(1) //Filial + Mat + DataBas + DataIni
          If SRH->(DBSeek(SRA->RA_FILIAL + SRA->RA_MAT + aIdVac[4] + aIdVac[5] ))
             aDados[1]  := "F"
             aDados[3]  := SRH->RH_DTAVISO
             aDados[8]  := SRH->RH_DIALRE1
             aDados[9]  := SRH->RH_DIALREM
             aDados[10] := SRH->RH_DATABAS
             aDados[11] := SRH->RH_DBASEAT 
             aDados[12] := SRH->RH_DATAINI
             aDados[13] := SRH->RH_DATAFIM
             aDados[16] := SRH->RH_DABONPE
             aDados[21] := SRH->RH_DTRECIB
          EndIf

       ElseIf aIdVac[1] == "SRF" //F�rias Programadas

      dPgDataBas := Ctod(Substr(aIdVac[4],7,2) + "/" + Substr(aIdVac[4],5,2) + "/" + Substr(aIdVac[4],1,4))
      dPgDataIni := Ctod(Substr(aIdVac[5],7,2) + "/" + Substr(aIdVac[5],5,2) + "/" + Substr(aIdVac[5],1,4))
          
      dbSelectArea("SRF")
      SRF->(dbSetOrder(1))  //Filial + Mat + DataBas + PD
      If SRF->(DbSeek(SRA->RA_FILIAL+SRA->RA_MAT))
	   
         While SRF->(!EoF()) .and. SRA->RA_FILIAL+SRA->RA_MAT == SRF->RF_FILIAL+SRF->RF_MAT 

               If SRF->RF_DATABAS == dPgDataBas
                  aDados[1]  := "P"
                  aDados[10] := SRF->RF_DATABAS
                  aDados[11] := SRF->RF_DATAFIM
                  
                  If SRF->RF_DATAINI == dPgDataIni
                     aDados[3]  := SRF->RF_DATAINI - 30
                     aDados[21] := DataValida(DataValida(SRF->RF_DATAINI-1,.F.)-1,.F.)
                     aDados[12] := SRF->RF_DATAINI
                     aDados[13] := SRF->RF_DATAINI + (SRF->RF_DFEPRO1 - 1) 
                     aDados[16] := SRF->RF_DABPRO1
                  ElseIf SRF->RF_DATINI2 == dPgDataIni
                     aDados[3]  := SRF->RF_DATINI2 - 30
                     aDados[21] := DataValida(DataValida(SRF->RF_DATINI2-1,.F.)-1,.F.)
                     aDados[12] := SRF->RF_DATINI2
                     aDados[13] := SRF->RF_DATINI2 + (SRF->RF_DFEPRO2 - 1) 
                     aDados[16] := SRF->RF_DABPRO2
                  ElseIf SRF->RF_DATINI3 == dPgDataIni
                     aDados[3]  := SRF->RF_DATINI3 - 30
                     aDados[21] := DataValida(DataValida(SRF->RF_DATINI3-1,.F.)-1,.F.)
                     aDados[12] := SRF->RF_DATINI3
                     aDados[13] := SRF->RF_DATINI3 + (SRF->RF_DFEPRO3 - 1) 
                     aDados[16] := SRF->RF_DABPRO3
                  EndIf

               Exit
            EndIf

            SRF->(DbSkip())
         EndDo
	   
	      EndIf 
      EndIf
		
      If aDados[1] != ""
         
         //------------------------------------------------------------------------------
         //Existe um problema ainda nao solucionado que o APP envia mais de uma requisicao via mobile
         //Quando isso ocorre o sistema nao gera o arquivo e envia uma resposta sem conteudo. 
         //Solucao paliativa:
         //Caso alguma requisicao falhe tentaremos gerar o arquivo novamente por 3 vezes no maximo
         //Cada nova requisicao ira gerar o arquivo com um nome diferente (Filial + Matricula + nX) 
         //------------------------------------------------------------------------------
         For nX := 1 To 3

            //Se existir o arquivo REL/PDF nao executamos a geracao porque indica uma requisicao em andamento
            If !File( cArqLocal + cFileName + cExtFile + '*' )
               //Faz a geracao do HTML e cria��o do arquivo PDF para o recibo de f�rias
               lContinua := FindFunction("u_NoticeVacationReport")
               If lContinua
                  u_NoticeVacationReport(aDados, cArqLocal, cFileName + cExtFile + cValToChar(nX))
               EndIf
            EndIf
         
            //Avalia o arquivo gerado no servidor
            While lContinua
               
               If File( cArqLocal + cFileName + cExtFile + cValToChar(nX) + cPDF )
                  oFile := FwFileReader():New( cArqLocal + cFileName + cExtFile + cValToChar(nX) + cPDF )
                  
                  If (oFile:Open())
                     cFile := oFile:FullRead()
                        oFile:Close()
                  EndIf
               EndIf
      
               //Em determinados ambientes pode ocorrer demora na geracao do arquivo, entao tentar localizar por 5 segundos no maximo.
               If ( lContinua := Empty(cFile) .And. nCont <= 4 )
                  nCont++
                  Sleep(1000)
               EndIf
            End
         
            If !Empty(cFile)
               Exit
            Else
               lContinua := .T.
               Conout( EncodeUTF8(">>>"+ STR0030 +"("+ cValToChar(nX) +")") ) //"Aguardando a geracao do recibo de f�rias..."
            EndIf

         Next nX
      EndIf    

    EndIf


    //Se algum erro impediu a geracao do arquivo, faz a geracao de um arquivo PDF com a mensagem do erro.
	If Empty( cFile )
    	aAdd( aLog, STR0031 ) //"Durante o processamento ocorreram erros que impediram a grava��o dos dados. Contate o administrador do sistema."
    	aAdd( aLog, "" )
    	aAdd( aLog, STR0032 ) //"Poss�veis causas do problema:"
    	aAdd( aLog, "- " + STR0033 ) //"Erro na gera��o do arquivo pdf no servidor."
    	aAdd( aLog, "- " + STR0034 ) //"Erro na consulta do registro de programa��o de f�rias."
    	aAdd( aLog, "- " + STR0036 ) //"Funcionalidade RDMAKE RHNPMEURH n�o compilada no RPO."
	
		fPDFMakeFileMessage( aLog, cFileName, @cFile ) 
    Else 
        //Atualiza informa��o de download do recibo de f�rias calculadas
        If aDados[1] == "SRH"
           Begin Transaction
              SRH->( Reclock("SRH",.F.) )
                 RH_ACEITE := "MeuRH Download" +"|" +DtoS(Date()) +"|" +Time() +"|" +SRA->RA_CIC +"|" +SRA->RA_NOME 
              SRH->( MsUnlock() )
              SRH->( FkCommit() )
           End Transaction
        EndIf
	EndIf  

    //Exclui os arquivos temporarios gerados durante o processamento (REL/PDF/PD_)
    fExcFileMRH( cArqLocal + cFileName + '*' )

	::SetHeader("Content-Disposition", "attachment; filename=" + cFileName + cPDF)
	::SetResponse(cFile)
	
EndIf

Return( lRet )

// -------------------------------------------------------------------
// Retorna o arquivo PDF do recibo de f�rias
// -------------------------------------------------------------------
WSMETHOD GET ReportVacation WSREST Vacation

Local cToken      := ""
Local cMatSRA     := ""
Local cBranchVld  := ""
Local cLogin      := ""
Local cRD0Cod     := ""
Local cExtFile    := ""
Local cFile       := ""
Local cFileName   := ""
Local cArqLocal   := ""
Local cPDF        := ".PDF"
Local nX          := 0
Local nCont       := 0
Local aVerbas     := {}
Local aCabec      := {}
Local aInfo       := {}
Local aLog        := {}
Local aIdFunc     := {}
Local aDataLogin  := {}
Local lDemit		:= .F.
Local lHabil		:= .T.

Self:SetContentType("application/json")
Self:SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "vacationReceipt", @lHabil)
If !lHabil .Or. lDemit
   SetRestFault(400, EncodeUTF8( STR0057 )) //"Permiss�o negada aos servi�os para recibo de f�rias!"
   Return (.F.)  
EndIf

If Len(::aUrlParms) == 4

	If !( "current" $ ::aUrlParms[2] )
		If !Empty(::aUrlParms[2])

			aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
			If Len(aIdFunc) > 1 
			
			    //valida se o solicitante da requisi��o pode ter acesso as informa��es
                If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                   cBranchVld	:= aIdFunc[1]
                   cMatSRA		:= aIdFunc[2]
                Else
                   cBranchVld	:= ""
                   cMatSRA		:= ""
	    		EndIf	
			EndIf

		EndIf
	EndIf
	
	//Posiciona no funcionario que esta sendo impresso
    DbSelectArea("SRA")
    SRA->( dbSetOrder(1) )
    lContinua := SRA->( dbSeek( cBranchVld + cMatSRA ) )	

	If lContinua .And. !Empty(::aUrlParms[3])
	 
		aIdVac := STRTOKARR( ::aUrlParms[4], "|" )
		cRecno := If( Len(aIdVac) > 5, aIdVac[6], "" )

		If( lContinua := ExistBlock("VACATIONREPORT") ) .And. !Empty(cRecno)
		
			//Busca os dados das ferias
			fVacationDetail( cBranchVld, cMatSRA, cRecno, @aCabec, @aVerbas )

			If !Empty(aCabec) .And. !Empty(aVerbas)

				fInfo(@aInfo, cBranchVld)
			
				cExtFile	 := DTOS( DATE() ) + SubStr( TIME(), 1, 2) //Ano + Mes + Dia + Hora
				cFileName := Alltrim(cBranchVld) + "_" + Alltrim(cMatSRA) + "_vacation_"
				cArqLocal := GetSrvProfString ("STARTPATH","")
			
				For nX := 1 To 3
	
					//Se existir o arquivo REL/PDF nao executamos a geracao porque indica uma requisicao em andamento
					If !File( cArqLocal + cFileName + cExtFile + '*' )
						//Faz a geracao do HTML e cria��o do arquivo PDF para o recibo de f�rias
	                    
	                    If lContinua
	                       U_VACATIONREPORT( aCabec, aVerbas, aInfo, cArqLocal, cFileName + cExtFile + cValToChar(nX) )
	                    EndIf
					EndIf
				
				    //Avalia o arquivo gerado no servidor
				    While lContinua
					    
					    If File( cArqLocal + cFileName + cExtFile + cValToChar(nX) + cPDF )
				    		oFile := FwFileReader():New( cArqLocal + cFileName + cExtFile + cValToChar(nX) + cPDF )
				    		
				    		If (oFile:Open())
						    	cFile := oFile:FullRead()
						        oFile:Close()
				    		EndIf
					    EndIf
				
					    //Em determinados ambientes pode ocorrer demora na geracao do arquivo, entao tentar localizar por 5 segundos no maximo.
					    If ( lContinua := Empty(cFile) .And. nCont <= 4 )
					    	nCont++
					    	Sleep(1000)
					    EndIf
				    End
			    
				    If !Empty(cFile)
				    	Exit
				    Else
				    	lContinua := .T.
				    	Conout( EncodeUTF8(">>>"+ STR0030 +"("+ cValToChar(nX) +")") ) //"Aguardando a geracao do recibo de f�rias..."
				    EndIf
	
				Next nX
				
			EndIf

		EndIf
		
	EndIf

EndIf

If Empty( cFile )
	aAdd( aLog, STR0031 ) //"Durante o processamento ocorreram erros que impediram a grava��o dos dados. Contate o administrador do sistema."
	aAdd( aLog, "" )
	aAdd( aLog, STR0032 ) //"Poss�veis causas do problema:"
	aAdd( aLog, "- " + STR0033 ) //"Erro na gera��o do arquivo pdf no servidor."
	aAdd( aLog, "- " + STR0034 ) //"Erro na consulta do registro de programa��o de f�rias."
	aAdd( aLog, "- " + STR0036 ) //"Funcionalidade RDMAKE RHNPMEURH n�o compilada no RPO."

	fPDFMakeFileMessage( aLog, cFileName, @cFile ) 
EndIf  

//Exclui os arquivos temporarios gerados durante o processamento (REL/PDF/PD_)
fExcFileMRH( cArqLocal + cFileName + '*' )

Self:SetHeader("Content-Disposition", "attachment; filename=" + cFileName + cPDF)
Self:SetResponse(cFile)

Return( .T. )

// -------------------------------------------------------------------
// Retorna o detalhe do recibo de f�rias
// -------------------------------------------------------------------
WSMETHOD GET getDetailVacation WSREST Vacation

Local cJsonObj    := "JsonObject():New()"
Local oItem       := &cJsonObj
Local oItemDetail := &cJsonObj
Local oEvents     := &cJsonObj 
Local oSubtot     := &cJsonObj
Local nX          := 0
Local cMsg        := ""
Local cToken      := ""
Local cMatSRA     := ""
Local cBranchVld  := ""
Local cLogin      := ""
Local cRD0Cod     := ""
Local cDtAviso    := "" 
Local cDtRecibo   := ""
Local lRet        := .T.
Local lDemit		:= .F.
Local lHabil		:= .T.
Local aIdFunc     := {}
Local aTmpEvent   := {}
Local aEvents     := {}
Local aSubtot     := {}
Local aServices   := {}
Local aTmpSub     := {0,0,0}
Local aDataLogin  := {}
Local cPDLiq      := fGetCodFol("0102")
Local cQrySRH     := ""
Local cQrySRR     := ""

::SetContentType("application/json")
::SetHeader('Access-Control-Allow-Credentials' , "true")

cToken		:= Self:GetHeader('Authorization')
aDataLogin := GetDataLogin(cToken, .T.)
If Len(aDataLogin) > 0
   cMatSRA    := aDataLogin[1]
   cLogin     := aDataLogin[2]
   cRD0Cod    := aDataLogin[3]
   cBranchVld := aDataLogin[5]
   lDemit     := aDataLogin[6]
EndIf

//Valida Permissionamento
fPermission(cBranchVld, cLogin, cRD0Cod, "vacationReceipt", @lHabil)
If !lHabil .Or. lDemit
   SetRestFault(400, EncodeUTF8( STR0057 )) //"Permiss�o negada aos servi�os para recibo de f�rias!"
   Return (.F.)  
EndIf

If Len(::aUrlParms) == 3

	cToken     := Self:GetHeader('Authorization')
	cMatSRA    := GetRegisterHR(cToken)
	cBranchVld := GetBranch(cToken)

	If !( "current" $ ::aUrlParms[2] )
		If !Empty(::aUrlParms[2])

			aIdFunc := STRTOKARR( ::aUrlParms[2], "|" )
			If Len(aIdFunc) > 1 
			
			    //valida se o solicitante da requisi��o pode ter acesso as informa��es
                If getPermission(cBranchVld, cMatSRA, aIdFunc[1], aIdFunc[2])
                   cBranchVld	:= aIdFunc[1]
                   cMatSRA		:= aIdFunc[2]
                Else
                   cBranchVld	:= ""
                   cMatSRA		:= ""
	    		EndIf	
			EndIf

		EndIf
	EndIf

	If !Empty(::aUrlParms[3]) 
		aIdVac := STRTOKARR( ::aUrlParms[3], "|" )
		cRecno := aIdVac[6]
		
		If "SRF" $ aIdVac[1]
			dbSelectArea("SRH")
			If SRH->( dbSeek( cBranchVld + cMatSRA + aIdVac[4] + aIdVac[5]) )
				cRecno := Recno()
			EndIf
		EndIf

		cQrySRH	:= GetNextAlias()
		cQrySRR := GetNextAlias()		
		

        //aIdVac[1] - Tipo do registro de f�rias SRF ou SRH
        //aIdVac[2] - Filial
        //aIdVac[3] - Matr�cula
        //aIdVac[4] - Data Base
        //aIdVac[5] - Data Inicial
        //aIdVac[6] - RECNO

		//Executa a query para obter dados do cabecalho das ferias
		BEGINSQL ALIAS cQrySRH
		    COLUMN RH_DATAINI AS DATE
		    COLUMN RH_DTAVISO AS DATE
		    COLUMN RH_DTRECIB AS DATE
		
			SELECT 
				SRH.RH_FILIAL,
				SRH.RH_MAT,
				SRH.RH_DATAINI,
				SRH.RH_DTAVISO,
				SRH.RH_DTRECIB,
				SRH.R_E_C_N_O_
			FROM 
				%Table:SRH% SRH
			WHERE 
				SRH.RH_FILIAL   = %Exp:cBranchVld% AND 
				SRH.RH_MAT      = %Exp:cMatSRA %   AND
				SRH.R_E_C_N_O_  = %Exp:cRecno % AND
				SRH.%NotDel%
		ENDSQL    
		
		While (cQrySRH)->(!Eof())    

			cDtAviso  := (cQrySRH)->RH_DTAVISO 
			cDtRecibo := (cQrySRH)->RH_DTRECIB

			//Executa a query para obter dados dos itens das ferias
			BEGINSQL ALIAS cQrySRR
				SELECT 
					SRR.RR_PD,
					SRR.RR_HORAS,
					SRR.RR_VALOR
				FROM 
					%Table:SRR% SRR
				WHERE 
					SRR.RR_FILIAL = %Exp:(cQrySRH)->RH_FILIAL%  AND
					SRR.RR_MAT    = %Exp:(cQrySRH)->RH_MAT%     AND
					SRR.RR_TIPO3  = %Exp:'F'%                   AND
					SRR.RR_DATA   = %Exp:(cQrySRH)->RH_DATAINI% AND					
					SRR.%NotDel%
			ENDSQL
			
			While (cQrySRR)->( !Eof() )
				
				//Obtem informacoes da verba
				PosSrv( (cQrySRR)->RR_PD, (cQrySRH)->RH_FILIAL )
				
				//A verba Liquido de Ferias so pode ser demonstrada no subtotal
				If (cQrySRR)->RR_PD == cPDLiq 
					aTmpSub[3] += (cQrySRR)->RR_VALOR
				Else
					//As demais verbas de provento e desconto serao somadas para compor o subtotal 
					//e serao adicionadas no array aTmpEvent para compor os eventos das ferias
					DO CASE
						CASE SRV->RV_TIPOCOD == "1" //Proventos
							aTmpSub[1] += (cQrySRR)->RR_VALOR
							cType := "proceeds"
							
						CASE SRV->RV_TIPOCOD == "2" //Descontos
							aTmpSub[2] += (cQrySRR)->RR_VALOR
							cType := "deductions"
							
						OTHERWISE
							//Bases
							cType := "tax-base"
					END CASE
	
					aAdd( aTmpEvent, { ;
									cType,; 
									(cQrySRR)->RR_PD,;
									(cQrySRR)->RR_VALOR,; 
									(cQrySRR)->RR_HORAS,;
									AllTrim( SRV->RV_DESC ) } )
				EndIf
				
				(cQrySRR)->(DbSkip())
			Enddo

			(cQrySRH)->(DbSkip())
        Enddo
        
		//Gera os dados das verbas
		For nX := 1 To Len(aTmpEvent) 
			oEvents 				:= &cJsonObj 
			oEvents["type"]			:= aTmpEvent[nX,1]
			oEvents["id"]			:= aTmpEvent[nX,2]
			oEvents["value"]		:= aTmpEvent[nX,3]
			oEvents["quantity"]		:= aTmpEvent[nX,4] 
			oEvents["description"]	:= aTmpEvent[nX,5]
			aAdd( aEvents, oEvents )
		Next nX        

		//Gera os dados dos subtotais
		For nX := 1 To 3 
			oSubtot 				:= &cJsonObj 
			oSubtot["type"]			:= If( nX == 1, "proceeds", If( nX == 2, "deductions", "net-value"))
			oSubtot["value"]		:= aTmpSub[nX]
			oSubtot["description"]	:= If( nX == 1, EncodeUTF8(STR0039), If( nX == 2, EncodeUTF8(STR0040), EncodeUTF8(STR0041) ) ) //"Proventos"#"Descontos"#"L�quido"
			aAdd( aSubtot, oSubtot )
		Next nX

		//Gera os elementos do JSON
		oItemDetail["id"]         := ::aUrlParms[3]
		oItemDetail["payDate"]    := FwTimeStamp(6, cDtRecibo, "12:00:00" ) 
		oItemDetail["noticeDate"] := FwTimeStamp(6, cDtAviso,  "12:00:00" )
		oItemDetail["subtotals"]  := aSubtot        
		oItemDetail["events"]     := aEvents

		oItem["data"]		:= oItemDetail
		oItem["length"]		:= 1
		oItem["messages"]	:= {}
	Else
		cMsg := EncodeUTF8( STR0042 ) //"N�o foi poss�vel consultar os dados do recibo. Tente novamente e se o problema persistir contate o administrador do sistema"
		lRet := .F.
	EndIf

    (cQrySRH)->( DBCloseArea() )
    (cQrySRR)->( DBCloseArea() )

EndIf

If lRet
	cJson := FWJsonSerialize(oItem, .F., .F., .T.)
	::SetResponse(cjson)
Else
	SetRestFault(400, cMsg)
EndIf

Return( lRet )


/*/{Protheus.doc} fVldSolicFer
   Valida��es genericas relacionadas as f�rias  
/*/
Function fVldSolicFer(cFil, cMat, cDataIni, nFerDuracao, nDiasVendidos, lSolic13)
Local cMsgValid   := ""
Local aPerFerias  := {}
Local nPorDFer    := Val( SuperGetMv("MV_PORDFER",,30) )
Local dHj30       := Date() + nPorDFer
Local lRefTrab    := FindFunction("fRefTrab") .And. fRefTrab("F")
Local cFilFun     := cFil
Local cMatFun     := cMat
Local dDataIni    := CTOD(cDataIni)
Local dDSR        := CTOD("//")
Local nDiasValid  := 0

IF Select("SRA") > 0
	SRA->(dbCloseArea())
Endif
dbSelectArea("SRA")
If !SRA->( MsSeek( cFil + alltrim(cMat) ) )
   cMsgValid := OemToAnsi(STR0048) //"Cadastro do funcionario da solicitacao nao localizado!"
EndIf

If Empty(cMsgValid) .and. !lRefTrab
   nIdade := Int((dDataBase - SRA->RA_NASC) / 365)

   If !lRefTrab .And. nFerDuracao < 30 .AND. (nIdade < 18 .Or. nIdade > 50)
      If !lRefTrab .And. nIdade < 18 .AND. nFerDuracao < 30

         cMsgValid := OemToAnsi(STR0051) //"Menor de 18 anos, deve tirar f�rias em per�odo �nico!"
      ElseIf !lRefTrab .And. nIdade > 50 .AND. nFerDuracao < 30
         cMsgValid := OemToAnsi(STR0052) //"Maior de 50 anos, deve tirar f�rias em per�odo �nico!"
      EndIf
   EndIf   
EndIf               

If Empty(cMsgValid) .and. lRefTrab
   If fFeriado( cFilFun, dDataIni ) //se data inicial for Feriado
      //retorna que dia � feriado
      cMsgValid := OemToAnsi(STR0018) + " (" + cValToChar(Day(dDataIni))+"/"+ cValToChar(Month(dDataIni)) + "/" + cValToChar(Year(dDataIni)) + ")"  + OemToAnsi(STR0019) + "(" + cValToChar(Day(dDataIni))+"/"+ cValToChar(Month(dDataIni)) + "/" + cValToChar(Year(dDataIni)) +  ")" + "."

   ElseIf fFeriado( cFilFun, dDataIni + 1 ) //se dia seguinte � data inicial for feriado
      //retorna que antecede feriado
      cMsgValid := OemToAnsi(STR0018) + " (" + cValToChar(Day(dDataIni))+"/"+ cValToChar(Month(dDataIni)) + "/" + cValToChar(Year(dDataIni)) + ")"  + OemToAnsi(STR0019) + "(" + cValToChar(Day(dDataIni+1))+"/"+ cValToChar(Month(dDataIni+1)) + "/" + cValToChar(Year(dDataIni+1)) +  ")" + "."

   Else //verifica DSR
      dDSR := fVldDSR(cFilFun, cMatFun, dDataIni, 2, "D") //data do DSR
      If !Empty(dDSR) //retorna que antecede DSR
         cMsgValid := OemToAnsi(STR0018) + " (" + cValToChar(Day(dDataIni))+"/"+ cValToChar(Month(dDataIni)) + "/" + cValToChar(Year(dDataIni)) + ")"  + OemToAnsi(STR0020) + "(" + cValToChar(Day(dDSR))+"/"+ cValToChar(Month(dDSR)) + "/" + cValToChar(Year(dDSR)) +  ")" + "."
      EndIf
   EndIf
EndIf

If Empty(cMsgValid)
   //valida finais de semana
   nDOW := DOW(dDataIni)
   If nDow == 7
      cMsgValid := OemToAnsi(STR0016) //"O in�cio das f�rias n�o poder� ocorrer em um s�bado"
   ElseIf nDow == 1
      cMsgValid := OemToAnsi(STR0017) //"O in�cio das f�rias n�o poder� ocorrer em um domingo"
   EndIf
EndIf

If Empty(cMsgValid)
   If dDataIni < dHj30
      cMsgValid := OemToAnsi(STR0021) + cValToChar(nPorDFer) + OemToAnsi(STR0037) //"As f�rias devem ser solicitadas com pelo menos XX dias de anteced�ncia."
   EndIf
EndIf

If Empty(cMsgValid)
   dbSelectArea("SRF")
   DbSetOrder(1)
   If !SRF->( MsSeek( cFil + alltrim(cMat) ) )
      cMsgValid := OemToAnsi(STR0049) //"Nao existe periodo de ferias no cadastro de Programacao de Ferias!"
   Else
      fCarPerFer(cFil, Substr(cMat,1,TamSx3('RA_MAT')[1]), @aPerFerias)
      If len(aPerFerias) > 0
         SRF->(dbGoto(aPerFerias[1][30]))

         If (!Empty(SRF->RF_DATAINI) .and. !Empty(SRF->RF_DATINI2) .and. !Empty(SRF->RF_DATINI3))
            cMsgValid := OemToAnsi(STR0050) //"Todas as programacoes disponiveis ja estao ocupadas, verifique!"
         EndIf

         If (Empty(cMsgValid) .and. SRF->RF_PERC13S > 0 .and. lSolic13)
            cMsgValid := OemToAnsi(STR0058) //"Adiantamento de 13 Salario j� requisitado anteriormente!"
         EndIf

         If (Empty(cMsgValid) .and. nDiasVendidos > 0) 
            If (SRF->RF_DFEPRO1 + SRF->RF_DABPRO1 + SRF->RF_DFEPRO2 + SRF->RF_DABPRO2 + nFerDuracao + nDiasVendidos ) > (SRF->RF_DFERVAT + SRF->RF_DFERAAT + SRF->RF_DIASANT) 
               cMsgValid := OemToAnsi(STR0059) //"Quantidade de dias de abono supera o total permitido!"
            EndIf

            If (Empty(cMsgValid) .and. (SRF->RF_DABPRO1 + SRF->RF_DABPRO2 + nDiasVendidos) > 10)
               cMsgValid := OemToAnsi(STR0060) //"O total de abono n�o pode superar superar 10 dias no per�odo aquisitivo!"
            EndIf
         EndIf

         If (Empty(cMsgValid) .and. !Empty(SRF->RF_DATAINI))
            If (dDataIni >= SRF->RF_DATAINI .and. dDataIni <= DaySum( SRF->RF_DATAINI , SRF->RF_DFEPRO1 ))                                                  
                cMsgValid := OemToAnsi(STR0061) //"Periodo de f�rias em conflito com a primeira programa��o de f�rias!"
            EndIf
         EndIf
         If (Empty(cMsgValid) .and. !Empty(SRF->RF_DATINI2))
            If (dDataIni >= SRF->RF_DATINI2 .and. dDataIni <= DaySum( SRF->RF_DATINI2 , SRF->RF_DFEPRO2 ))                                                  
                cMsgValid := OemToAnsi(STR0062) //"Periodo de f�rias em conflito com a segunda programa��o de f�rias!"
            EndIf
         EndIf
      Else
         cMsgValid := OemToAnsi(STR0049) //"Nao existe periodo de ferias no cadastro de Programacao de Ferias!"
      EndIf
   EndIf
EndIf

If Empty(cMsgValid)
   /* - De acordo com a legisla��o, as f�rias podem ser divididas em 3 per�odos, 
      sendo uma quantidade m�nima de 05 dias para uma solicita��o.
      - N�o existe aqui a necessidade de validar outras tabelas como RH3 e SRH, 
      pois o saldo total j� � avaliado durante o GET, antes de iniciar o processo de solicita��o
   */ 

   // avalia��o do somatorio da qtde de dias programados
   If aPerFerias[1][33] > 0 .and. aPerFerias[1][34] == 0
      If (aPerFerias[1][3] - (nFerDuracao + aPerFerias[1][33]) < 5) .and. (aPerFerias[1][3] - (nFerDuracao + aPerFerias[1][33]) > 0)
         cMsgValid := OemToAnsi(STR0053) //O saldo final dos dias de f�rias n�o poder� ser menor que 5 dias!
      EndIf
   EndIf

   // avalia��o do somatorio da qtde de dias programados com o saldo dispon�vel no per�odo aquisitivo
   //RF_DIASDIR: Dias de Direito(referencia) 
   //RF_DFERVAT = aPerFerias[1][3] Dias Vencidos
   //RF_DFERAAT = aPerFerias[1][4] Dias Proporcionais
   //RF_DIASANT = aPerFerias[1][25] Dias Antecipados
   //RF_DFERANT = aPerFerias[1][14] Dias Pagos
   
   nDiasValid := nFerDuracao + aPerFerias[1][14] + aPerFerias[1][33] + aPerFerias[1][34] 
   
   If Empty(cMsgValid) .and. aPerFerias[1][3] > 0 
      If (aPerFerias[1][3] - nDiasValid) < 5 .and. (aPerFerias[1][3] - nDiasValid) > 0
         cMsgValid := OemToAnsi(STR0053) //O saldo final dos dias de f�rias n�o poder� ser menor que 5 dias!
      EndIf
   ElseIF Empty(cMsgValid) .and. aPerFerias[1][4] > 0 
      If (aPerFerias[1][4] - nDiasValid) < 5 .and. (aPerFerias[1][4] - nDiasValid) > 0
         cMsgValid := OemToAnsi(STR0053) //O saldo final dos dias de f�rias n�o poder� ser menor que 5 dias!
      EndIf
   EndIf

   // avalia o saldo de dias da �ltima programa��o de f�rias
   If Empty(cMsgValid) .and. aPerFerias[1][33] > 0 .and. aPerFerias[1][34] > 0
      If aPerFerias[1][3] - (nFerDuracao + aPerFerias[1][33] + aPerFerias[1][34]) > 0
         cMsgValid := OemToAnsi(STR0054) //Essa programa��o dever� possuir o saldo total de dias dispon�veis!
      EndIf
   EndIf
EndIf

Return( cMsgValid )


/*/{Protheus.doc} fVldWkf
   Valida��es para movimenta��o do workflow  
/*/
Function fVldWkf(cKey, cReq, cOper)
local cMsgFault := ""

default cKey  := ""
default cReq  := ""
default cOper := ""

    If cOper == "U" .or. cOper == "D"
    
       DBSelectArea("RH3")
       DBSetOrder(1)	//RH3_FILIAL+RH3_CODIGO
       If RH3->( dbSeek(cKey) )
          If RH3_NVLAPR != RH3_NVLINI   

             If Val( GetRGKSeq( cReq , .T.) ) == 2
                cMsgFault := ""
             Else
                cMsgFault := STR0024 //"N�o ser� poss�vel executar a requisi��o, pois o workflow foi movimentado"    
             EndIf
          EndIf
       Else   
          cMsgFault := STR0025 //"Identificador da solicita��o n�o localizado"
       EndIf

    ElseIf cOper == "I"
       
        If Val( GetRGKSeq( cReq , .T.) ) == 2
           cMsgFault := ""
        Else
           cMsgFault := STR0024 //"N�o ser� poss�vel executar a requisi��o, pois o workflow foi movimentado"    
        EndIf
    EndIf

Return( cMsgFault )


/*/{Protheus.doc} fGetVacationDetail
Efetua a inclusao ou a alteracao de uma solicitacao de atestado medico
@author:	Marcelo Silveira
@since:		27/05/2019
@param:		lNewReg - Indica se e inclusao (.T.) ou alteracao (.F.)
			cBranchVld - Filial;
			cMatSRA - Matr�cula;
			cBody - Json com o corpo da requisicao;
			oItemDetail - Objeto Json com o corpo da requisicao;
			cError - Erros durante a criacao do arquivo (referencia)
@return:	lRet - Verdadeiro se o registro foi incluido ou alterado com sucesso			
/*/
Function fVacationDetail(cBranchVld, cMatSRA, cRecno, aCabec, aVerbas )

Local cQrySRH     := GetNextAlias()
Local cQrySRR     := ""
Local lContinua   := .F.

DEFAULT cBranchVld   := ""
DEFAULT cMatSRA      := ""
DEFAULT cRecno       := ""
DEFAULT aCabec       := {}
DEFAULT aVerbas      := {}

If !Empty(cBranchVld) .And. !Empty(cMatSRA) .And. !Empty(cRecno)
	dbSelectArea( "SRA" )	
	SRR->(dbSetOrder(1))
	lContinua := dbSeek( cBranchVld + cMatSRA )	
EndIf

If lContinua

	//Executa a query para obter dados do cabecalho das ferias
	BEGINSQL ALIAS cQrySRH
	    COLUMN RH_DATAINI AS DATE
        COLUMN RH_DATAFIM AS DATE
	    COLUMN RH_DTAVISO AS DATE
	    COLUMN RH_DTRECIB AS DATE
        COLUMN RH_DATABAS AS DATE
        COLUMN RH_DBASEAT AS DATE
	
		SELECT 
			SRH.RH_FILIAL,
			SRH.RH_MAT,
			SRH.RH_DFERIAS,
			SRH.RH_DATAINI,
			SRH.RH_DATAFIM,
			SRH.RH_DTAVISO,
			SRH.RH_DTRECIB,
			SRH.RH_DATABAS,
			SRH.RH_DBASEAT,
			SRH.RH_DIALREM,
			SRH.RH_DIALRE1,
			SRH.RH_DABONPE,
			SRH.RH_ABOPEC,
			SRH.R_E_C_N_O_
		FROM 
			%Table:SRH% SRH
		WHERE 
			SRH.RH_FILIAL   = %Exp:cBranchVld% AND 
			SRH.RH_MAT      = %Exp:cMatSRA %   AND
			SRH.R_E_C_N_O_  = %Exp:cRecno %    AND
			SRH.%NotDel%
	ENDSQL    
	
	If (cQrySRH)->(!Eof())
	
		cQrySRR := GetNextAlias()
	
		While (cQrySRH)->(!Eof())
		
			//Gera dados do cabecalho
			aAdd( aCabec, { ;
								(cQrySRH)->RH_FILIAL,;
								(cQrySRH)->RH_MAT,; 
								(cQrySRH)->RH_DFERIAS,;							
								(cQrySRH)->RH_DATAINI,; 
								(cQrySRH)->RH_DATAFIM,; 
								(cQrySRH)->RH_DTAVISO,; 
								(cQrySRH)->RH_DTRECIB,; 
								(cQrySRH)->RH_DATABAS,; 
								(cQrySRH)->RH_DBASEAT,; 
								(cQrySRH)->RH_DIALREM,; 
								(cQrySRH)->RH_DIALRE1,;
								(cQrySRH)->RH_DABONPE,;
								(cQrySRH)->RH_ABOPEC } )							
	
			//Executa a query para obter dados dos itens das ferias
			BEGINSQL ALIAS cQrySRR
				SELECT 
					SRR.RR_PD,
					SRR.RR_HORAS,
					SRR.RR_VALOR
				FROM 
					%Table:SRR% SRR
				WHERE 
					SRR.RR_FILIAL = %Exp:(cQrySRH)->RH_FILIAL%  AND
					SRR.RR_MAT    = %Exp:(cQrySRH)->RH_MAT%     AND
					SRR.RR_TIPO3  = %Exp:'F'%                   AND
					SRR.RR_DATA   = %Exp:(cQrySRH)->RH_DATAINI% AND					
					SRR.%NotDel%
			ENDSQL
			
			While (cQrySRR)->( !Eof() )
				
				//Obtem informacoes da verba
				PosSrv( (cQrySRR)->RR_PD, (cQrySRH)->RH_FILIAL )
				
				aAdd( aVerbas, { SRV->RV_TIPOCOD, SRV->RV_CODFOL, (cQrySRR)->RR_PD, (cQrySRR)->RR_VALOR, (cQrySRR)->RR_HORAS, AllTrim( SRV->RV_DESC ) } )
				
				(cQrySRR)->(DbSkip())
			Enddo
	
			(cQrySRH)->(DbSkip())
	    Enddo
	    
	    (cQrySRR)->( DBCloseArea() )
	    
    EndIf

    (cQrySRH)->( DBCloseArea() )

EndIf

Return()

/*/{Protheus.doc} fGetSRH
Obtem os dados do cabe�alho de f�rias conforme os dados solicitados.
@author:	Marcelo Silveira
@since:		10/02/2020
@param:		cBranchVld - Filial;
			cMatSRA - Matr�cula;
			dDataBas - Data inicio das ferias;
			dDataFim - Data fim das ferias;
@return:	aDados - Array com o cabecalho das ferias			
/*/
Function fGetSRH(cBranchVld, cMatSRA, dDataBas, dDataFim )

Local cQuerySRH	:= ""
Local aDados 	:= {}

DEFAULT cBranchVld	:= ""
DEFAULT cMatSRA		:= ""
DEFAULT dDataBas	:= ""
DEFAULT dDataFim	:= ""
DEFAULT cID			:= ""

If !Empty(cBranchVld) .And. !Empty(cMatSRA) .And. !Empty(dDataBas) .And. !Empty(dDataFim)

	cQuerySRH	:= GetNextAlias()

    BEGINSQL ALIAS cQuerySRH
       COLUMN RH_DATABAS AS DATE
       COLUMN RH_DBASEAT AS DATE
       COLUMN RH_DATAINI AS DATE
       COLUMN RH_DATAFIM AS DATE

       SELECT 
		   SRH.RH_DATABAS,
		   SRH.RH_DBASEAT,
		   SRH.RH_DATAINI,
		   SRH.RH_DATAFIM,
		   SRH.RH_DABONPE,
		   SRH.RH_ACEITE,
		   SRH.RH_DFERIAS,
		   SRH.RH_DFERVEN,
		   SRH.RH_PERC13S,
		   SRH.R_E_C_N_O_,
		   SRH.RH_FILIAL,
		   SRH.RH_MAT
       FROM 
	       %Table:SRH% SRH
       WHERE 
		   SRH.RH_FILIAL   = %Exp:cBranchVld%   AND 
		   SRH.RH_MAT      = %Exp:cMatSRA %     AND
		   SRH.RH_DATABAS  = %Exp:dDataBas% 	AND
		   SRH.RH_DBASEAT  = %Exp:dDataFim%  	AND
		   SRH.%NotDel%
    ENDSQL
    
    While (cQuerySRH)->(!Eof())
       
		aAdd( aDados, { ;
							DTOS( (cQuerySRH)->RH_DATAINI ), ;
							(cQuerySRH)->RH_DFERIAS, ;
							(cQuerySRH)->RH_DATAFIM, ;
							DTOS( (cQuerySRH)->RH_DATABAS ), ;
							(cQuerySRH)->RH_DBASEAT, ;
							(cQuerySRH)->RH_DABONPE, ;
							(cQuerySRH)->RH_PERC13S, ;
							Alltrim( STR((cQuerySRH)->R_E_C_N_O_) );
						} )

		(cQuerySRH)->(DBSkip())
   EndDo 
   
   (cQuerySRH)->(DBCloseArea())       
        
EndIf        
        
Return( aDados )