#INCLUDE "TOTVS.CH"

/*/
{Protheus.doc} RHNPLIBJOB
- Rela�ao de funcoes do Meu RH que poder�o ser executadas via JOB quando � necess�rio obter dados de outro grupo;
/*/

/*/
{Protheus.doc} GetDataOrJob
- Executa a fun��o normal, ou via job quando a solicita��o � para outro grupo
@author:	Marcelo Silveira
@since:		03/08/2020
@param:		cRotina - C�digo que indica qual rotina ser� executada;
			aParams - Parametros que ser�o passados para a rotina que ser� executada;			
			cEmpToReq - Empresa para verificar se a execu��o ser� via Job;
@return:	uRet - Retorno conforme a fun��o que est� sendo executada

----------------------------------------------------------
Premissas para que uma fun��o possa ser executada via Job

Uma fun��o qualquer que est� sendo executada via Job:

	1. Dever� receber 3 par�metros adicionais: 
		- C�digo do Grupo onde a fun��o deve ser executada
		- Vari�vel l�gica para indicar a execu��o via JOB
		- Nome da vari�vel p�blica de controle 

	2. Na execu��o via Job dever� setar a conex�o e o ambiente do grupo (3 para n�o consumir licen�as)
		RPCSetType( 3 )
		RPCSetEnv( CEMPRESA, CFILIAL ) //Empresa e Filial para prepara��o do ambiente
	
	3. Antes do retorno da fun��o a vari�vel de controle dever� ser alimentada com valor "1" para indicar o t�rmino do Job
		PutGlbValue(cUID, "1")

Exemplo de uso: GetPeriodApont()
----------------------------------------------------------
/*/
Function GetDataForJob( cRotina, aParams, cEmpToReq )

Local uRet							//Retorno da fun��o

Local cUID			:= ""			//Nome da variavel de controle na execu��o via Job
Local nCount		:= 0			//Contador para controlar o limite para finaliza��o do Job
Local nTime			:= 1000			//Indica quantos segundos o sistema ir� aguardar a cada incremento do contador (default 1 segundo) 
Local lNumPar 		:= .F.			//Valida o n�mero de parametros que a fun��o executada deve ter
Local lExecJob		:= .F.			//Indica que a execu��o ser� feita via Job
Local cEmpAtual		:= cEmpAnt	//Grupo que est� logado para comparar se � o mesmo grupo da requisi��o 

Default cRotina 	:= ""
Default aParams		:= {}
Default cEmpToReq	:= cEmpAnt

	lExecJob := !(cEmpToReq == cEmpAtual) //Define a execu��o via Job quando a requisi��o � para outro grupo

	DO CASE 
		Case cRotina == "1" //Retorna os periodos de apontamento

			//Valida a quantidade de parametros esperados pela rotina => GetPeriodApont
			lNumPar := Len(aParams) == 3

			If lExecJob .And. lNumPar
				//Define o nome da variavel de controle
				cUID := "MRHG_PERAPO_" + AllTrim( Str(ThreadID()) )
				
				//Atribui a variavel de controle				
				PutGlbValue(cUID,"0")
				
				//Define a cria��o e o retorno do Job				
				uRet := StartJob( "GetPeriodApont", GetEnvServer(), .T., aParams[1], aParams[2], aParams[3], cEmpToReq, .T., cUID )
			Else
				uRet := GetPeriodApont(aParams[1], aParams[2], aParams[3])
			EndIf

		Case cRotina == "2" //Realiza a reprovacao de uma batida que foi incluida por geolocalizao

			//Valida a quantidade de parametros esperados pela rotina => fUpdGeoClock
			lNumPar := Len(aParams) == 4

			If lExecJob .And. lNumPar
				//Define o nome da variavel de controle
				cUID := "MRHU_GEOUPD_" + AllTrim( Str(ThreadID()) )

				//Atribui a variavel de controle
				PutGlbValue(cUID,"0")

				//Define a cria��o e o retorno do Job
				uRet := StartJob( "fUpdGeoClock", GetEnvServer(), .T., aParams[1], aParams[2], aParams[3], aParams[4], cEmpToReq, .T., cUID )
			Else
				uRet := fUpdGeoClock( aParams[1], aParams[2], aParams[3], aParams[4] )
			EndIf

	ENDCASE

	//Aguarda a finalizacao do JOB no m�ximo por 10 segundos antes de sair da rotina
	While lExecJob
		//Obtem o valor da variavel global
		If GetGlbValue(cUID) == "1"
			Exit
		Else 
			nCount ++
			Sleep(nTime)
		EndIf
		lExecJob := (nCount < 10)
	EndDo

	//Limpa a variavel global da memoria
	If !Empty(cUID)
		ClearGlbValue(cUID)
	EndIf

Return( uRet )

