#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "FWPRINTSETUP.CH" 
#INCLUDE "RPTDEF.CH"  

#INCLUDE "RHNPLIB.CH"

/*/{Protheus.doc}RHNPLIB

- Fonte agrupador de diversas Functions do Projeto MEU RH ( RH MOBILE );

@author:	Matheus Bizutti
@since:		24/08/2017

/*/

/*/{Protheus.doc} GetAccessEmployee
- Obt�m o acesso a RD0 e alimenta o array aRET com as matr�culas do usu�rio logado.

@author:	Matheus Bizutti
/*/
Function GetAccessEmployee(cRD0Login,aRet,lRetorno,lMeuRH)

Local lRet		:= .F.

Default cRD0Login := ""
Default aRet      := {}
Default lRetorno  := .F.
Default lMeuRH    := .F.

// - Verifica se existe o arquivo de relacionamento
// - Efetua o posicionamento no funcion�rio (SRA)
If lRetorno 

	If MatParticipant(cRD0Login, aRet, .T., lMeuRH)
		lRet := .T.
	EndIf
	
EndIf

Return(lRet)

/*/{Protheus.doc} GetRegisterHR
- L� a Matr�cula do USU�RIO GRAVADO NO TOKEN

@author:	Matheus Bizutti
/*/
Function GetRegisterHR(cToken)
	Local aHeader  := {}
	Default cToken := ''
	aHeader := GetClaims(cToken)
	
	If Len(aHeader) < 1
		Return ""
	EndIf
Return(aHeader[1])

/*/{Protheus.doc} GetLoginHR
- L� o LOGIN ( RD0_LOGIN ) do USU�RIO GRAVADO NO TOKEN

@author:	Matheus Bizutti
/*/
Function GetLoginHR(cToken)
	Local aHeader := {}
	Default cToken := ''
	aHeader := GetClaims(cToken)
	
	If Len(aHeader) < 2
		Return ""
	EndIf
Return(aHeader[2])

/*/{Protheus.doc} GetLoginHR
- L� o c�digo ( RD0_CODIGO ) do USU�RIO GRAVADO NO TOKEN

@author:	Matheus Bizutti
/*/
Function GetCODHR(cToken)
	Local aHeader  := {}
	Default cToken := ''
	aHeader := GetClaims(cToken)
	
	If Len(aHeader) < 3
		Return ""
	EndIf
Return(aHeader[3])

/*/{Protheus.doc} GetLoginHR
- L� a filial ( RDZ - RELACIONAMENTO ) do USU�RIO GRAVADO NO TOKEN

@author:	Matheus Bizutti
/*/
Function GetBranch(cToken)
	Local aHeader  := {}
	Default cToken := ''
	aHeader := GetClaims(cToken)
	
	If Len(aHeader) < 5
		Return ""
	EndIf
Return(aHeader[5])

/*/{Protheus.doc} GetDataLogin
- Retorna os dados de Login a partir do usuario gravado no Token
@author:	Marcelo Silveira
/*/
Function GetDataLogin(cToken, lChkFunc)
	Local aHeader  := {}
	Local lDemit   := .T.

	Default cToken   := ''
	Default lChkFunc := .F.

	aHeader := GetClaims(cToken)

	/*/
		aHeader[1] = Matr�cula (Tab. SRA)
		aHeader[2] = Login (Tab. RD0)
		aHeader[3] = Codigo (Tab. RD0)
		aHeader[4] = Database
		aHeader[5] = Filial (Tab. SRA)

		Opcional
		aHeader[6] = Situacao (.T. = demitido)
	/*/
	
	If Len(aHeader) < 5
		Return ""
	ElseIf lChkFunc //Verifica se o funcionario esta demitido
		DbSelectArea("SRA")
		SRA->(DbSetOrder(1))
		If SRA->( DbSeek( aHeader[5] + aHeader[1] ) ) 
			lDemit := SRA->RA_SITFOLH == "D"
			aAdd( aHeader, lDemit )
		EndIf	
	EndIf	

Return(aHeader)


/*/{Protheus.doc} DecodeURL
- DECODE do corpo de requisi��es que vem no FORMDATA FORMAT.

@author:	Matheus Bizutti
/*/
Function DecodeURL(cBody)

Local cURLDecode := ""
Local cUser      := ""
Local cPw        := ""
Local cEmail     := ""
Local cKey       := ""
Local cHash      := ""
Local nX         := 0
Local aPars      := {}
Local aKeyValue  := {}
Local aReturn    := {}
Local cExecRobo  := "0"

Default cBody    := ""

aPars := StrTokArr(cBody, "&")
//varinfo("aPars: ",aPars)

For nX := 1 To Len(aPars)
	aKeyValue := StrTokArr(aPars[nX], "=")
	If aKeyValue[1] == "user"
		cUser := Iif(Len(aKeyValue) >= 2,aKeyValue[2],"")
		cUser := StrTran(cUser, "+", "")
	ElseIf	aKeyValue[1] == "password"
	 	cPw := Iif(Len(aKeyValue) >= 2,aKeyValue[2],"")
	ElseIf	aKeyValue[1] == "redirectUrl"
		cURLDecode := StrTran( Iif(Len(aKeyValue) >= 2,aKeyValue[2],""), "%3A", ":" )
		cURLDecode := StrTran( cURLDecode, "%2F", "/" )
    ElseIf aKeyValue[1] == "email"
       cEmail := Iif(Len(aKeyValue) >= 2,aKeyValue[2],"")
       cEmail := StrTran( cEmail, "%40", "@" )
    ElseIf  aKeyValue[1] == "hash"
       cHash  := Iif(Len(aKeyValue) >= 2,aKeyValue[2],"")
	ElseIf aKeyValue[1] == "execRobo"
		cExecRobo := Iif(Len(aKeyValue) >= 2, aKeyValue[2], "0")
	EndIf
Next nX

cKey := cUser + "|" + cPw + "|" + cURLDecode + "|" + cEmail + "|" + cHash + "|" + cExecRobo
aReturn := StrTokArr(cKey, "|")

Return(aReturn)


/*/{Protheus.doc}fVldSolAut
Retorna se o usu�rio autenticado tem acesso as informa��es solicitadas.
@author: Gabriel A.	
@since: 24/07/2017
/*/
Function fVldSolAut(cFilAut, cMatAut, cFilSol, cMatSol, aMsg, lMsg, cRD0Login)
	Local lTemAcesso := .T.
	Local lUserCfg	 := .F.
	Local nPos		 := 0
	Local aRet		 := {}
	
	Default lMsg 	:= .T.
	Default aMsg 	:= {}
	Default cFilAut := ""
	Default cMatAut := ""
	Default cMatSol := ""
	Default cFilSol := ""
	Default cRD0Login := ""
	
	lUserCfg := GetAccessEmployee(cRD0Login, @aRet, .T.)
	
	If lUserCfg .And. Len(aRet) >= 1
	
		nPos := Ascan(aRet,{|x| Alltrim(x[3]) + Alltrim(x[1]) == Alltrim(cFilSol) + Alltrim(cMatSol)})
		
		If nPos == 0
			
			lTemAcesso := .F.
			If lMsg
				Aadd(aMsg, EncodeUTF8(STR0001)) //"O usu�rio autenticado n�o possui acesso aos dados."
			EndIf
		EndIf
		
	EndIf
	
Return lTemAcesso

/*/{Protheus.doc}GetConfig()
- Fun��o respons�vel por ler uma sess�o do appserver.ini e uma chave, e retornar o valor.
@author:	Matheus Bizutti
@since:		09/08/2017
/*/
Function GetConfig(cSession, cProperty,cDefault)

Local cValue := ""

Default cSession  := ""
Default cProperty := ""
Default cDefault  := ""

cValue := GetPvProfString(cSession, cProperty, "", GetAdv97()) 

Return(Iif(Empty(cValue),cDefault,cValue))


/*/{Protheus.doc}GetClaims()
- Fun��o respons�vel por obter o Token gerado no momento do Login, e retornar os dados de acesso do usu�rio logado.
@author:	Matheus Bizutti
@since:	09/08/2017
/*/
Function GetClaims(cToken)

Local nX        := 0
Local aClaims 	:= {}
Local aReturn	:= {}
Default cToken	:= ""

aClaims := JWTClaims(Substr(cToken,8,Len(cToken)))

For nX := 1 To Len(aClaims)
   If aClaims[nX][1] == "KEY"
      aReturn := StrTokArr(aClaims[nX][2], "|")
   EndIf
Next nX

Return(aReturn)


/*/{Protheus.doc}GetVisionAI8()
- Fun��o respons�vel por ler a VIS�O de determinada rotina na tabela AI8
@author:	Matheus Bizutti
@since:	21/08/2017
@param:	cRoutine - nome da rotina que a fun��o buscar� ex: W_PWSA01.APW
		cBranchVld - Filial utilizada no acesso ao APP.
		cCodEmp - Codiqo da empresa para pesquisa em tabela diferente da empresa logada
/*/
Function GetVisionAI8(cRoutine, cBranchVld, cCodEmp)

Local aVision	:= {}
Local cQuery    := GetNextAlias()
Local cBranchAI8:= ""
Local __cAI8tab := ""
Local cCodHR	:= "000006"

Default cRoutine   := ""
Default cBranchVld := FwCodFil()
Default cCodEmp    := cEmpAnt

cBranchAI8 := xFilial("AI8", cBranchVld)

If !Empty(cRoutine)

	__cAI8tab := "% " + RetFullName("AI8", cCodEmp) + " %"

	BEGINSQL ALIAS cQuery
	
		SELECT AI8.AI8_VISAPV, AI8.AI8_INIAPV, AI8.AI8_APRVLV
	   	FROM %exp:__cAI8tab% AI8
		WHERE AI8.AI8_FILIAL =  %Exp:cBranchAI8% AND
			  AI8.AI8_ROTINA =  %Exp:cRoutine%   AND
			  AI8.AI8_PORTAL =  %Exp:cCodHR%     AND
			  AI8.%notDel%
	ENDSQL

	While (cQuery)->(!Eof())
	
		Aadd(aVision, {(cQuery)->AI8_VISAPV,(cQuery)->AI8_INIAPV,(cQuery)->AI8_APRVLV})
	
		(cQuery)->(DbSkip())
	EndDo

(cQuery)->( DbCloseArea() )

EndIf

If Empty(aVision)
	// ------------------------------
	// INICIALIZANDO O ARRAY aVision |
	// ------------------------------
	aVision	:= Array(1,3)
	aVision[1][1] := ""
	aVision[1][2] := 0
	aVision[1][3] := 0	
EndIf

Return(aVision)

/*/{Protheus.doc}Format8601()
- Fun��o respons�vel por receber um DATETIME e devolver a data ou a hora.
@author:	Matheus Bizutti
@since:	21/08/2017
@param:	lDate - Quando informada .T., a fun��o devolve a data, caso contr�rio devolve a hora.
			cValue - Valor em DATETIME ISO 8601 que ser� utilizado para retorno de data ou hora.
/*/

Function Format8601(lDate,cValue,l2Dig)

Local cFormat	:= ""
Local cAuxFormat:= ""
Local nPosIni	:= 1
Local nTam		:= 4

Default lDate	:= .T.
Default cValue	:= ""
Default l2Dig	:= .F.

If !Empty(cValue)
	If lDate

		If l2Dig
			nPosIni := 3
			nTam := 2
		EndIf
		
		cAuxFormat := Substr(cValue,1,10)
		cFormat    := Substr(cAuxFormat,9,2) + "/" + Substr(cAuxFormat,6,2) + "/" + Substr(cAuxFormat,nPosIni,nTam) 
		
	Else
		cFormat    := Substr(cValue,12,5) 
		
	EndIf
EndIf

Return(cFormat)

/*/{Protheus.doc}SumDate
- Efetua SUM em datas;
@author: 	Matheus Bizutti	
@since:	12/04/2017

/*/
Function SumDate(dDate, nDays)

Default dDate := dDataBase
Default nDays := 0

Return DaySum( dDate , nDays )


/*/{Protheus.doc}PeriodConcessive
- Retorna o Periodo concessivo;
@author: 	Matheus Bizutti	
@since:	12/04/2017

/*/
Function PeriodConcessive(dInit, dEnd)

Local aDate		:= {}
Local dInitDate	:= CtoD(" / / ") 
Local dEndDate	:= CToD(" / / ")

Default dInit := CtoD(" / / ")
Default dEnd 	:= CtoD(" / / ")

dInitDate 	:= DaySum(STOD(dEnd),1)
dEndDate	:= YearSum(dInitDate,1)
dEndDate	:= DaySub(dEndDate,1)

Aadd(aDate,dInitDate)
Aadd(aDate,dEndDate)

Return(aDate)

/*/{Protheus.doc}StatusVacation
- Retorna o Status de F�rias;
@author: 	Matheus Bizutti	
@since:	12/04/2017

/*/
Function StatusVacation(cSitFolh)

Local cStatus	 := ""

Default cSitFolh := " "

If cSitFolh == "F"
	cStatus := "approving"
Else
	cStatus := "closed"
EndIf

Return (cStatus)


/*/{Protheus.doc}GetDepSup
- Efetua a busca do Depto Superior
@author: 	Matheus Bizutti
@since: 	13/07/2017
@param:	cDepto - SRA->RA_DEPTO | cBranchVld - Vari�vel da Filial da RDZ

/*/
Function GetDepSup(cDepto,cBranchVld)

Local cDepSup := ""
Local aArea   := GetArea()
Local aQBArea := SQB->(GetArea())

Default cDepto := ""

DbSelectArea("SQB")
If SQB->(DbSeek(xFilial("SQB", cBranchVld)+ cDepto))
	cDepSup := SQB->QB_DEPSUP
EndIf

RestArea(aArea)
RestArea(aQBArea)

Return(cDepSup)

Function GetENUMDecode(cCode)

Local cDesc := ""
Default cCode := ""

DO CASE
     CASE cCode == "B"
                cDesc := EncodeUTF8('vacation')
 
     CASE cCode == "8"
                cDesc := EncodeUTF8('allowance')
     
     CASE cCode == "Z"
                cDesc := EncodeUTF8('clocking') 

	 CASE cCode == "6"
                cDesc := EncodeUTF8('demission') 
				
     OTHERWISE
                cDesc := ''
     ENDCASE

Return (EncodeUTF8(cDesc))

/*/{Protheus.doc} getSummary
- Retorna dados do funcionario
@author: 	Matheus Bizutti, Marcelo Silveira (nova versao)
@since: 	13/07/2017
@param:	
	Matricula - Matricula do funcionario
	cBranchVld - Filial do funcionario
	cEmp - Empresa do funcionario
/*/
Function getSummary(cMat, cBranch, cEmp)

Local aReturn 	:= Array(04)
Local cAliasSRA := GetNextAlias()
Local cNome  	:= ""
Local cWhere	:= ""
Local __cDelete := "% SRA.D_E_L_E_T_ = ' ' AND SQ3.D_E_L_E_T_ = ' ' %"

Default cMat 	:= ""
Default cBranch := ""
Default cEmp    := cEmpAnt

	aReturn[1] := ""
	aReturn[2] := ""
	aReturn[3] := ""
	aReturn[4] := ""

	__cSRAtab 	:= "%" + RetFullName("SRA", cEmp) + "%"
	__cSQ3tab 	:= "%" + RetFullName("SQ3", cEmp) + "%"
	cJoinSQ3 	:= "% SRA.RA_CARGO = SQ3.Q3_CARGO AND " + fMHRTableJoin("SRA", "SQ3") + "%"
	cWhere 		:= "RA_FILIAL = '" + cBranch + "' AND RA_MAT = '" + cMat + "'"
	cWhere 		:= "% " + cWhere + " %" 

	BeginSql ALIAS cAliasSRA
		SELECT RA_FILIAL, RA_MAT, RA_CARGO, RA_NOME, RA_NOMECMP, Q3_CARGO, Q3_DESCSUM 
		FROM %exp:__cSRAtab% SRA
		LEFT JOIN %exp:__cSQ3tab% SQ3
			ON %exp:cJoinSQ3% 
		WHERE
			%Exp:cWhere% AND %exp:__cDelete%
	EndSql	

	While (cAliasSRA)->(!Eof())
		cNome := If( !Empty((cAliasSRA)->RA_NOMECMP), (cAliasSRA)->RA_NOMECMP, (cAliasSRA)->RA_NOME ) 

		aReturn[1] := (cAliasSRA)->RA_MAT
		aReturn[2] := EncodeUTF8( Alltrim( cNome ) )
		aReturn[3] := EncodeUTF8( Alltrim( (cAliasSRA)->Q3_DESCSUM ) ) 
		aReturn[4] := (cAliasSRA)->RA_FILIAL
		
		(cAliasSRA)->( DbSkip() )
	EndDo
	
	(cAliasSRA)->( DBCloseArea() )

Return(aReturn)


Function milisSecondsToHour(nMSInitParam,nMSEndParam)

Local aConvertHour	:= Array(2)

Local nMSInit	:= nMSInitParam
Local nMSEnd	:= nMSEndParam
Local nHourInit	:= 0 
Local nHourEnd	:= 0 

nMsInit := (nMsInit / (1000*60))
nMSEnd  := (nMsEnd  / (1000*60))

nHourInit := Min2Hrs(nMsInit)
nHourEnd  := Min2Hrs(nMSEnd)

aConvertHour[1] := nHourInit
aConvertHour[2] := nHourEnd

Return(aConvertHour)


Function HourToMs(cTime)
Local nMSTime	:= 0
Local aTime		:= {}

Default cTime	:= ""

	aTime := StrTokArr(cTime, ".") // = 9.05
	If Len(aTime) > 1
		If Len(aTime[2]) == 1
			aTime[2] := aTime[2] + "0"
		EndIf
		nMSTime := ((Val(aTime[1]) * 60) + Val(aTime[2])) * 60000
	Else
		nMSTime := ((Val(aTime[1]) * 60)) * 60000
	EndIf

Return(nMSTime)

/*/{Protheus.doc} formatGMT
Formata a hora para o formato json
@author:	Marcelo Silveira
@since:		23/05/2019
@param:		cValue  - Data em formato string ou caracter para conversao;
			lString - Verifica se a data veio no formato STRING(STOD) ao inv�s de CARACTER(CTOD) para realizar a convers�o.
@return:	cReturn - data no formato GMT			
/*/
Function formatGMT(cValue, lString)
Local cDateFormat	:= ""
Local cReturn		:= ""
Local aDateFormat	:= {}

Default cValue	:= ""
Default lString	:= .F.

If !lString
	cDateFormat := DTOS(CTOD(Alltrim(cValue)))
else
	cDateFormat := cValue
EndIf
aDateFormat := LocalToUTC( cDateFormat, "12:00:00" )

cReturn := Iif(Empty(aDateFormat),"",Substr(aDateFormat[1],1,4) + "-" + Substr(aDateFormat[1],5,2) + "-" + Substr(aDateFormat[1],7,2) + "T" + "12:00:00" + "Z")

Return( cReturn )


/*/{Protheus.doc} fPDFMakeFileMessage
//Gera um arquivo PDF para retorno de uma requisicao REST para exibir LOG de ocorrencias
@author:	Marcelo Silveira
@since:		20/08/2019
@return:	Nil	
/*/
Function fPDFMakeFileMessage( aMsg, cNameFile, cFile )

Local oFile
Local oPrint	
Local cArqLocal		:= ""
Local nX 			:= 0
Local nLin 			:= 0
Local nCont			:= 0
Local nTamMarg		:= 15
Local lContinua		:= .T.
Local cLocal		:= GetSrvProfString ("STARTPATH","")
Local oFont10n		:= TFont():New("Arial",10,10,,.T.,,,,.T.,.F.)	//Normal negrito
Local oFont10		:= TFont():New("Arial",10,10,,.F.,,,,.T.,.F.)	//Normal s/ negrito

DEFAULT aMsg		:= { OemToAnsi(STR0002) } //"Ocorreram erros durante o processamento"
DEFAULT cNameFile	:= "LOG_MESSAGE"
DEFAULT cFile		:= ""

	oPrint 		:= FWMSPrinter():New(cNameFile+".rel", IMP_PDF, .F., cLocal, .T., , , , .T., , .F., )
	
	oPrint:SetLandscape()
	oPrint:SetMargin(nTamMarg,nTamMarg,nTamMarg,nTamMarg)
	oPrint:StartPage()

	nSizePage	:= oPrint:nPageWidth / oPrint:nFactorHor

	nLin += 30
	oPrint:Say(nLin,  15, OemToAnsi(STR0003), oFont10n) //"LOG DE OCORR�NCIAS"
	nLin += 05		
	oPrint:Line(nLin, 15, nLin, nSizePage-(nTamMarg*3))
	
	nLin += 15
	For nX := 1 to Len( aMsg )
		oPrint:Say(nLin, 15, aMsg[nX], oFont10)
		nLin += 10
	Next nX
		
	oPrint:EndPage()
		
	cArqLocal		:= cLocal+cNameFile+".PDF"		
	oPrint:cPathPDF := cLocal 
	oPrint:lViewPDF := .F.
	oPrint:Print()
	
	While lContinua
	    If File( cArqLocal )
			oFile := FwFileReader():New( cArqLocal )
			
			If (oFile:Open())
		    	cFile := oFile:FullRead()
		        oFile:Close()
		        fErase(cArqLocal)		    		
			EndIf
	    EndIf
	    //Em determinados ambientes pode ocorrer demora na geracao do arquivo, entao tenta localizar por 5 segundos no maximo.
	    If ( lContinua := Empty(cFile) .And. nCont < 4 )
	    	nCont++
	    	Sleep(1000)
	    EndIf
    End	
		
Return()

/*/{Protheus.doc} fExcFileMRH
//Exclui arquivos da pasta system
//Usado para eliminar os temporarios da geracao PDF: Informe, Recibo e Espelho do Ponto
@author:	Marcelo Silveira
@since:		06/11/2019
@return:	Nil	
/*/
Function fExcFileMRH( cTpFile )
	
	Local cPath			:= GetSrvProfString("STARTPATH","")
	
	Default cTpFile 	:= ""
	
	//Verifica se foi passado o nome do arquivo que sera excluido
	//Nao sera considerado se vier somente o startPath na variavel 
	If Len(cTpFile) > Len(cPath)
		aEval( Directory(cTpFile), { |aArqToExc| fEraseArq( aArqToExc[1] ) })
	EndIf

Return

/*/{Protheus.doc} fEraseArq
//Exclui o arquivo fisico
@author:	Marcelo Silveira
@since:		18/03/2020
@return:	Nil	
/*/
STATIC Function fEraseArq( cArqToExc )

Local cExtensao := ".PDF|.PD_|.REL" //Extensoes de arquivos que podem ser excluidos

Default cArqToExc  := ""

If !Empty(cArqToExc)
	//Verifica pela extensao se o arquivo podera ser excluido 
	If SubStr( UPPER(cArqToExc), LEN(cArqToExc)-3, 4 ) $ cExtensao
		FERASE( cArqToExc )
	EndIf
EndIf	

Return

/*/{Protheus.doc} getPermission
//Veririca se o usuario pode acessar determinada rotina/recurso
@author:	
@since:		
@param:		cFilSolic = Filial do funcionario que esta solicitando a consulta
			cMatSolic = Matricula do funcionario que esta solicitando a consulta
			cFilValid = Filial do funcionario que esta sendo consultado
			cMatValid = Matricula do funcionario que esta sendo consultado			
			cEmpSolic = Empresa do funcionario que esta solicitando a consulta
			cEmpValid = Empresa do funcionario que esta sendo consultado
@return:	lRet - Verdadeiro/Falso caso o usucario tenha permissao para acessar a rotina/recurso	
/*/
Function getPermission(cFilSolic, cMatSolic, cFilValid, cMatValid, cEmpSolic, cEmpValid)
Local lRet    := .T.
//Local lChange := .F.

DEFAULT cEmpSolic := cEmpAnt
DEFAULT cEmpValid := cEmpAnt

If empty(cFilSolic) .or. empty(cMatSolic) .or. empty(cFilValid) .or. empty(cMatValid) 
   lRet := .F.
EndIf

//Nao verifica as permissoes quando o funcionario esta consultando seus proprios dados
If lRet .And. !( cFilSolic+cMatSolic+cEmpSolic == cFilValid+cMatValid+cEmpValid )
   
   If !(cEmpSolic == cEmpValid)
      __cSRAtab := "%" + RetFullName("SRA", cEmpValid) + "%"
      __cSQBtab := "%" + RetFullName("SQB", cEmpValid) + "%"
   Else
      __cSRAtab := "%" + RetSqlName("SRA") + "%"
      __cSQBtab := "%" + RetSqlName("SQB") + "%"
   EndIf

   cJoin := "% SRA.RA_DEPTO = SQB.QB_DEPTO AND " + fMHRTableJoin("SRA", "SQB") + "%"

   cQuery  := GetNextAlias()

	BEGINSQL ALIAS cQuery
		
		SELECT RA_FILIAL, RA_MAT, RA_DEPTO, QB_DEPTO, QB_FILRESP, QB_MATRESP
		FROM %exp:__cSRAtab% SRA
			INNER JOIN %exp:__cSQBtab% SQB
			ON %exp:cJoin%
		WHERE SRA.RA_FILIAL=%exp:cFilValid% AND SRA.RA_MAT=%exp:cMatValid%
		
	ENDSQL
	
	While (cQuery)->(!Eof())
		If !( (cQuery)->QB_FILRESP == cFilSolic .And. (cQuery)->QB_MATRESP == cMatSolic )
			lRet := .F.
		EndIf
		(cQuery)->(DbSkip())
	EndDo
	
	(cQuery)->( DbCloseArea() )

EndIf

Return(lRet)

/*/{Protheus.doc} fStatusLabel
Retorna o statuslabel de acordo com o status
@author:	Henrique Ferreira
@since:		10/12/2019
@param:		cStatus  - Status a ser tratado;

@return: Descri��o do status label.
/*/
Function fStatusLabel(cStatus)
Local cStatusLabel := ""

DEFAULT cStatus := ""

Do Case
	Case (cStatus) == "1"
		cStatusLabel := EncodeUTF8(STR0004) // Em processo de aprova��o
	Case (cStatus) == "2"
		cStatusLabel := EncodeUTF8(STR0005) // Atendida
	Case (cStatus) == "3"
		cStatusLabel := EncodeUTF8(STR0006) // Reprovada
	Otherwise
		cStatusLabel := EncodeUTF8(STR0007) // Aguardando efetiva��o do RH
End Case

Return cStatusLabel


Function getRGKJustify(cFil,cCode,cSeq)
Local cQueryRGK := GetNextAlias()
Local cQueryRDY := GetNextAlias()
Local cFilRDY   := ""
Local cJustify  := ""
Local cWhere    := "%"

DEFAULT cFil   := ""
DEFAULT cCode  := ""
DEFAULT cSeq   := ""

If !empty(cSeq)
   cWhere += " RGK.RGK_SEQUEN = " +cSeq +" AND "
EndIf
cWhere += "%"

If !empty(cCode)

	BEGINSQL ALIAS cQueryRGK
		SELECT RGK.RGK_CODCON,RGK.RGK_FILIAL
		FROM %table:RGK% RGK
			WHERE RGK.RGK_FILIAL  = %exp:cFil%  AND
			      RGK.RGK_CODIGO  = %exp:cCode% AND
                  RGK.RGK_CODCON != ' '         AND
				  %Exp:cWhere% 
                  RGK.%notDel%
	ENDSQL

	cFilRDY := "%'" + xFilial( 'RDY', (cQueryRGK)->RGK_FILIAL) + "'%"

	BEGINSQL ALIAS cQueryRDY
		SELECT RDY.RDY_TEXTO
		FROM %table:RDY% RDY
			WHERE RDY.RDY_FILIAL = %exp:cFilRDY% 	            AND
		         RDY.RDY_CHAVE  = %exp:(cQueryRGK)->RGK_CODCON% AND
		         RDY.%notDel%
	ENDSQL

	cJustify := (cQueryRDY)->RDY_TEXTO

	(cQueryRDY)->(DbCloseArea())
	(cQueryRGK)->(DbCloseArea())

EndIf

Return(EncodeUTF8(cJustify))


/*/{Protheus.doc} fMHRTableJoin
//Tratamento do campo filial para uso em queryes quando envolver tabelas com compartilhamento diferente
@author:	Marcelo Silveira
@since:		14/04/2020
@param:		ExpC1 - Vari�vel com Primeira tabela do "inner join"
			ExpC2 - Vari�vel com Segunda  tabela do "inner join"
			ExpC3 - Vari�vel indica se retorno dever� conter "%   %". Default = ""
@return:	cFilJoin - Campo filial das tabelas com Substring ou SubStr	
/*/
Function fMHRTableJoin( cTabela1, cTabela2, cEmbedded )

Local cFilJoin := ""

Default cEmbedded := ""

cFilJoin := cEmbedded + FWJoinFilial(cTabela1, cTabela2) + cEmbedded

If ( TCGETDB() $ 'DB2|ORACLE|POSTGRES|INFORMIX' )
	cFilJoin := STRTRAN(cFilJoin, "SUBSTRING", "SUBSTR")
EndIf

Return( cFilJoin )

/*/{Protheus.doc} fInvString
//Fun��o para inverter uma string
@author:	Henrique Ferreira
@since:		28/04/2020
@param:		cString - String a ser invertida

@return:	cRet - String invertida.	
/*/
Function fInvString(cString)

Local nX		:= 0
Local cRet		:= ""
Local nTamanho	:= Len(cString)

DEFAULT cString := ""

For nX := nTamanho To 1 Step -1
	cRet += SubStr(cString,nX,1)
Next nX

Return cRet

/*/{Protheus.doc} fRemoveStr
//Fun��o para remover uma substring de uma string
@author:	Henrique Ferreira
@since:		28/04/2020
@param:		cString - String a ser pesquisada.

@return:	cRet - String com os caracteres removidos.	
/*/
Function fRemoveStr(cString)

Local cStrOne	:= "<||$&%||>"
Local cStrTwo	:= ">[/;-="
Local cStrThree := "&%#@>.<"

If ( cStrOne $ cString )
	cString := StrTran(cString, cStrOne, "")
EndIf

If ( cStrTwo $ cString )
	cString := StrTran(cString, cStrTwo, "")
EndIf

If ( cStrThree $ cString)
	cString := StrTran(cString, cStrThree, "")
EndIf


Return cString




/*/{Protheus.doc} fMHRKeyTree
//Tratamento do campo filial para uso em queryes quando envolver tabelas com compartilhamento diferente
@author:	Marcelo Faria
@since:		28/04/2020
@param:		cFil - Filial do Funcion�rio
			cMat - Matr�cula do Funcion�rio
@return:	cKeyTree - Chave principal da arvore na estrutura	
/*/
Function fMHRKeyTree(cFil, cMat)
Local cKeyTree	:= ""
Local aArea		:= GetArea()
Local cOrgCFG	:= SUPERGETMV("MV_ORGCFG")

Default cFil	:= ""
Default cMat	:= ""

	If !empty(cFil) .and. !empty(cMat) .and. cOrgCFG == '0'

		DbSelectArea("SRA")
		If SRA->(DbSeek(cFil+cMat))

			DbSelectArea("SQB")
			If SQB->(DbSeek(xFilial("SQB", SRA->RA_FILIAL)+ SRA->RA_DEPTO))
				cKeyTree := Left(SQB->QB_KEYINI,3)
			EndIf

		EndIf	

	EndIf

RestArea(aArea)
Return( cKeyTree )
