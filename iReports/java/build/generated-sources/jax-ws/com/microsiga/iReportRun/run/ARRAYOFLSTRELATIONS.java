
package com.microsiga.iReportRun.run;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ARRAYOFLSTRELATIONS complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ARRAYOFLSTRELATIONS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LSTRELATIONS" type="{http://webservices.microsiga.com.br/}LSTRELATIONS" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ARRAYOFLSTRELATIONS", propOrder = {
    "lstrelations"
})
public class ARRAYOFLSTRELATIONS {

    @XmlElement(name = "LSTRELATIONS")
    protected List<LSTRELATIONS> lstrelations;

    /**
     * Gets the value of the lstrelations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lstrelations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLSTRELATIONS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LSTRELATIONS }
     * 
     * 
     */
    public List<LSTRELATIONS> getLSTRELATIONS() {
        if (lstrelations == null) {
            lstrelations = new ArrayList<LSTRELATIONS>();
        }
        return this.lstrelations;
    }

}
