
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GETPARAMETERSRESULT" type="{http://webservices.microsiga.com.br/}ARRAYOFLSTPARAMETERS"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getparametersresult"
})
@XmlRootElement(name = "GETPARAMETERSRESPONSE")
public class GETPARAMETERSRESPONSE {

    @XmlElement(name = "GETPARAMETERSRESULT", required = true)
    protected ARRAYOFLSTPARAMETERS getparametersresult;

    /**
     * Obt�m o valor da propriedade getparametersresult.
     * 
     * @return
     *     possible object is
     *     {@link ARRAYOFLSTPARAMETERS }
     *     
     */
    public ARRAYOFLSTPARAMETERS getGETPARAMETERSRESULT() {
        return getparametersresult;
    }

    /**
     * Define o valor da propriedade getparametersresult.
     * 
     * @param value
     *     allowed object is
     *     {@link ARRAYOFLSTPARAMETERS }
     *     
     */
    public void setGETPARAMETERSRESULT(ARRAYOFLSTPARAMETERS value) {
        this.getparametersresult = value;
    }

}
