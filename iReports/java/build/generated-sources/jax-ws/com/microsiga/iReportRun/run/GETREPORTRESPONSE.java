
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GETREPORTRESULT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getreportresult"
})
@XmlRootElement(name = "GETREPORTRESPONSE")
public class GETREPORTRESPONSE {

    @XmlElement(name = "GETREPORTRESULT", required = true)
    protected String getreportresult;

    /**
     * Obt�m o valor da propriedade getreportresult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGETREPORTRESULT() {
        return getreportresult;
    }

    /**
     * Define o valor da propriedade getreportresult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGETREPORTRESULT(String value) {
        this.getreportresult = value;
    }

}
