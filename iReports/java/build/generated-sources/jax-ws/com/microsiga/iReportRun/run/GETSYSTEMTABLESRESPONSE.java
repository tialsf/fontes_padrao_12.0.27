
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GETSYSTEMTABLESRESULT" type="{http://webservices.microsiga.com.br/}ARRAYOFSYSTABLESSTRU"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getsystemtablesresult"
})
@XmlRootElement(name = "GETSYSTEMTABLESRESPONSE")
public class GETSYSTEMTABLESRESPONSE {

    @XmlElement(name = "GETSYSTEMTABLESRESULT", required = true)
    protected ARRAYOFSYSTABLESSTRU getsystemtablesresult;

    /**
     * Obt�m o valor da propriedade getsystemtablesresult.
     * 
     * @return
     *     possible object is
     *     {@link ARRAYOFSYSTABLESSTRU }
     *     
     */
    public ARRAYOFSYSTABLESSTRU getGETSYSTEMTABLESRESULT() {
        return getsystemtablesresult;
    }

    /**
     * Define o valor da propriedade getsystemtablesresult.
     * 
     * @param value
     *     allowed object is
     *     {@link ARRAYOFSYSTABLESSTRU }
     *     
     */
    public void setGETSYSTEMTABLESRESULT(ARRAYOFSYSTABLESSTRU value) {
        this.getsystemtablesresult = value;
    }

}
