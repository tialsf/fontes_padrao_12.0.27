
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SESSIONID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EMPRESA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FILIAL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LISTTABLES" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sessionid",
    "empresa",
    "filial",
    "listtables"
})
@XmlRootElement(name = "GETTABLESX2NAME")
public class GETTABLESX2NAME {

    @XmlElement(name = "SESSIONID", required = true)
    protected String sessionid;
    @XmlElement(name = "EMPRESA", required = true)
    protected String empresa;
    @XmlElement(name = "FILIAL", required = true)
    protected String filial;
    @XmlElement(name = "LISTTABLES", required = true)
    protected String listtables;

    /**
     * Obt�m o valor da propriedade sessionid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSESSIONID() {
        return sessionid;
    }

    /**
     * Define o valor da propriedade sessionid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSESSIONID(String value) {
        this.sessionid = value;
    }

    /**
     * Obt�m o valor da propriedade empresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMPRESA() {
        return empresa;
    }

    /**
     * Define o valor da propriedade empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMPRESA(String value) {
        this.empresa = value;
    }

    /**
     * Obt�m o valor da propriedade filial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFILIAL() {
        return filial;
    }

    /**
     * Define o valor da propriedade filial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFILIAL(String value) {
        this.filial = value;
    }

    /**
     * Obt�m o valor da propriedade listtables.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLISTTABLES() {
        return listtables;
    }

    /**
     * Define o valor da propriedade listtables.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLISTTABLES(String value) {
        this.listtables = value;
    }

}
