
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de LSTCONNECTION complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LSTCONNECTION">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FIELDCDOM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FIELDDOM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TABLECDOM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LSTCONNECTION", propOrder = {
    "fieldcdom",
    "fielddom",
    "tablecdom"
})
public class LSTCONNECTION {

    @XmlElement(name = "FIELDCDOM", required = true)
    protected String fieldcdom;
    @XmlElement(name = "FIELDDOM", required = true)
    protected String fielddom;
    @XmlElement(name = "TABLECDOM", required = true)
    protected String tablecdom;

    /**
     * Obt�m o valor da propriedade fieldcdom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFIELDCDOM() {
        return fieldcdom;
    }

    /**
     * Define o valor da propriedade fieldcdom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFIELDCDOM(String value) {
        this.fieldcdom = value;
    }

    /**
     * Obt�m o valor da propriedade fielddom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFIELDDOM() {
        return fielddom;
    }

    /**
     * Define o valor da propriedade fielddom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFIELDDOM(String value) {
        this.fielddom = value;
    }

    /**
     * Obt�m o valor da propriedade tablecdom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTABLECDOM() {
        return tablecdom;
    }

    /**
     * Define o valor da propriedade tablecdom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTABLECDOM(String value) {
        this.tablecdom = value;
    }

}
