
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de LSTPARAMETERS complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LSTPARAMETERS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PARNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PARORDER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PARVALUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LSTPARAMETERS", propOrder = {
    "parname",
    "parorder",
    "parvalue"
})
public class LSTPARAMETERS {

    @XmlElement(name = "PARNAME", required = true)
    protected String parname;
    @XmlElement(name = "PARORDER", required = true)
    protected String parorder;
    @XmlElement(name = "PARVALUE", required = true)
    protected String parvalue;

    /**
     * Obt�m o valor da propriedade parname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARNAME() {
        return parname;
    }

    /**
     * Define o valor da propriedade parname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARNAME(String value) {
        this.parname = value;
    }

    /**
     * Obt�m o valor da propriedade parorder.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARORDER() {
        return parorder;
    }

    /**
     * Define o valor da propriedade parorder.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARORDER(String value) {
        this.parorder = value;
    }

    /**
     * Obt�m o valor da propriedade parvalue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARVALUE() {
        return parvalue;
    }

    /**
     * Define o valor da propriedade parvalue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARVALUE(String value) {
        this.parvalue = value;
    }

}
