
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de LSTRELATIONS complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LSTRELATIONS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LISTCONNECTION" type="{http://webservices.microsiga.com.br/}ARRAYOFLSTCONNECTION"/>
 *         &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LSTRELATIONS", propOrder = {
    "listconnection",
    "name"
})
public class LSTRELATIONS {

    @XmlElement(name = "LISTCONNECTION", required = true)
    protected ARRAYOFLSTCONNECTION listconnection;
    @XmlElement(name = "NAME", required = true)
    protected String name;

    /**
     * Obt�m o valor da propriedade listconnection.
     * 
     * @return
     *     possible object is
     *     {@link ARRAYOFLSTCONNECTION }
     *     
     */
    public ARRAYOFLSTCONNECTION getLISTCONNECTION() {
        return listconnection;
    }

    /**
     * Define o valor da propriedade listconnection.
     * 
     * @param value
     *     allowed object is
     *     {@link ARRAYOFLSTCONNECTION }
     *     
     */
    public void setLISTCONNECTION(ARRAYOFLSTCONNECTION value) {
        this.listconnection = value;
    }

    /**
     * Obt�m o valor da propriedade name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAME() {
        return name;
    }

    /**
     * Define o valor da propriedade name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAME(String value) {
        this.name = value;
    }

}
