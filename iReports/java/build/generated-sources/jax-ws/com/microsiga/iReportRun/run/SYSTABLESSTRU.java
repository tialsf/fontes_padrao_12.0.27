
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SYSTABLESSTRU complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SYSTABLESSTRU">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MODEEMP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MODEUN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TABELA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SYSTABLESSTRU", propOrder = {
    "description",
    "mode",
    "modeemp",
    "modeun",
    "tabela"
})
public class SYSTABLESSTRU {

    @XmlElement(name = "DESCRIPTION", required = true)
    protected String description;
    @XmlElement(name = "MODE", required = true)
    protected String mode;
    @XmlElement(name = "MODEEMP", required = true)
    protected String modeemp;
    @XmlElement(name = "MODEUN", required = true)
    protected String modeun;
    @XmlElement(name = "TABELA", required = true)
    protected String tabela;

    /**
     * Obt�m o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRIPTION() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRIPTION(String value) {
        this.description = value;
    }

    /**
     * Obt�m o valor da propriedade mode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODE() {
        return mode;
    }

    /**
     * Define o valor da propriedade mode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODE(String value) {
        this.mode = value;
    }

    /**
     * Obt�m o valor da propriedade modeemp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODEEMP() {
        return modeemp;
    }

    /**
     * Define o valor da propriedade modeemp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODEEMP(String value) {
        this.modeemp = value;
    }

    /**
     * Obt�m o valor da propriedade modeun.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODEUN() {
        return modeun;
    }

    /**
     * Define o valor da propriedade modeun.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODEUN(String value) {
        this.modeun = value;
    }

    /**
     * Obt�m o valor da propriedade tabela.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTABELA() {
        return tabela;
    }

    /**
     * Define o valor da propriedade tabela.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTABELA(String value) {
        this.tabela = value;
    }

}
