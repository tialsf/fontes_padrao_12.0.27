
package com.microsiga.iReportRun.run;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ARRAYOFLSTPARAMETERS complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ARRAYOFLSTPARAMETERS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LSTPARAMETERS" type="{http://webservices.microsiga.com.br/}LSTPARAMETERS" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ARRAYOFLSTPARAMETERS", propOrder = {
    "lstparameters"
})
public class ARRAYOFLSTPARAMETERS {

    @XmlElement(name = "LSTPARAMETERS")
    protected List<LSTPARAMETERS> lstparameters;

    /**
     * Gets the value of the lstparameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lstparameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLSTPARAMETERS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LSTPARAMETERS }
     * 
     * 
     */
    public List<LSTPARAMETERS> getLSTPARAMETERS() {
        if (lstparameters == null) {
            lstparameters = new ArrayList<LSTPARAMETERS>();
        }
        return this.lstparameters;
    }

}
