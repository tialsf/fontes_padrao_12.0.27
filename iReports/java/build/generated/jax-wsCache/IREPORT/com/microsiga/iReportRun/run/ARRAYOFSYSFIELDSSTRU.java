
package com.microsiga.iReportRun.run;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ARRAYOFSYSFIELDSSTRU complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ARRAYOFSYSFIELDSSTRU">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SYSFIELDSSTRU" type="{http://webservices.microsiga.com.br/}SYSFIELDSSTRU" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ARRAYOFSYSFIELDSSTRU", propOrder = {
    "sysfieldsstru"
})
public class ARRAYOFSYSFIELDSSTRU {

    @XmlElement(name = "SYSFIELDSSTRU")
    protected List<SYSFIELDSSTRU> sysfieldsstru;

    /**
     * Gets the value of the sysfieldsstru property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sysfieldsstru property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSYSFIELDSSTRU().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SYSFIELDSSTRU }
     * 
     * 
     */
    public List<SYSFIELDSSTRU> getSYSFIELDSSTRU() {
        if (sysfieldsstru == null) {
            sysfieldsstru = new ArrayList<SYSFIELDSSTRU>();
        }
        return this.sysfieldsstru;
    }

}
