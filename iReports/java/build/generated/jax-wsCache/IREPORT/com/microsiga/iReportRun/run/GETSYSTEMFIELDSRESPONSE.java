
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GETSYSTEMFIELDSRESULT" type="{http://webservices.microsiga.com.br/}ARRAYOFSYSFIELDSSTRU"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getsystemfieldsresult"
})
@XmlRootElement(name = "GETSYSTEMFIELDSRESPONSE")
public class GETSYSTEMFIELDSRESPONSE {

    @XmlElement(name = "GETSYSTEMFIELDSRESULT", required = true)
    protected ARRAYOFSYSFIELDSSTRU getsystemfieldsresult;

    /**
     * Obt�m o valor da propriedade getsystemfieldsresult.
     * 
     * @return
     *     possible object is
     *     {@link ARRAYOFSYSFIELDSSTRU }
     *     
     */
    public ARRAYOFSYSFIELDSSTRU getGETSYSTEMFIELDSRESULT() {
        return getsystemfieldsresult;
    }

    /**
     * Define o valor da propriedade getsystemfieldsresult.
     * 
     * @param value
     *     allowed object is
     *     {@link ARRAYOFSYSFIELDSSTRU }
     *     
     */
    public void setGETSYSTEMFIELDSRESULT(ARRAYOFSYSFIELDSSTRU value) {
        this.getsystemfieldsresult = value;
    }

}
