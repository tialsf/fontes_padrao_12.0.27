
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GETTABLESRELATIONRESULT" type="{http://webservices.microsiga.com.br/}ARRAYOFLSTRELATIONS"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "gettablesrelationresult"
})
@XmlRootElement(name = "GETTABLESRELATIONRESPONSE")
public class GETTABLESRELATIONRESPONSE {

    @XmlElement(name = "GETTABLESRELATIONRESULT", required = true)
    protected ARRAYOFLSTRELATIONS gettablesrelationresult;

    /**
     * Obt�m o valor da propriedade gettablesrelationresult.
     * 
     * @return
     *     possible object is
     *     {@link ARRAYOFLSTRELATIONS }
     *     
     */
    public ARRAYOFLSTRELATIONS getGETTABLESRELATIONRESULT() {
        return gettablesrelationresult;
    }

    /**
     * Define o valor da propriedade gettablesrelationresult.
     * 
     * @param value
     *     allowed object is
     *     {@link ARRAYOFLSTRELATIONS }
     *     
     */
    public void setGETTABLESRELATIONRESULT(ARRAYOFLSTRELATIONS value) {
        this.gettablesrelationresult = value;
    }

}
