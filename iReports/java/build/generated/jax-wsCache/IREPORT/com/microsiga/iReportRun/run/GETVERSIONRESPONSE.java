
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GETVERSIONRESULT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getversionresult"
})
@XmlRootElement(name = "GETVERSIONRESPONSE")
public class GETVERSIONRESPONSE {

    @XmlElement(name = "GETVERSIONRESULT", required = true)
    protected String getversionresult;

    /**
     * Obt�m o valor da propriedade getversionresult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGETVERSIONRESULT() {
        return getversionresult;
    }

    /**
     * Define o valor da propriedade getversionresult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGETVERSIONRESULT(String value) {
        this.getversionresult = value;
    }

}
