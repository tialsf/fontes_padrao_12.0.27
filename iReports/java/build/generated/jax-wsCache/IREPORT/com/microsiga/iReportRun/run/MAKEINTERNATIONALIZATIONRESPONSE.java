
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MAKEINTERNATIONALIZATIONRESULT" type="{http://webservices.microsiga.com.br/}INTERSTRU"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "makeinternationalizationresult"
})
@XmlRootElement(name = "MAKEINTERNATIONALIZATIONRESPONSE")
public class MAKEINTERNATIONALIZATIONRESPONSE {

    @XmlElement(name = "MAKEINTERNATIONALIZATIONRESULT", required = true)
    protected INTERSTRU makeinternationalizationresult;

    /**
     * Obt�m o valor da propriedade makeinternationalizationresult.
     * 
     * @return
     *     possible object is
     *     {@link INTERSTRU }
     *     
     */
    public INTERSTRU getMAKEINTERNATIONALIZATIONRESULT() {
        return makeinternationalizationresult;
    }

    /**
     * Define o valor da propriedade makeinternationalizationresult.
     * 
     * @param value
     *     allowed object is
     *     {@link INTERSTRU }
     *     
     */
    public void setMAKEINTERNATIONALIZATIONRESULT(INTERSTRU value) {
        this.makeinternationalizationresult = value;
    }

}
