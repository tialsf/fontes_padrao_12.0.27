
package com.microsiga.iReportRun.run;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.microsiga.iReportRun.run package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.microsiga.iReportRun.run
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GETPARAMETERS }
     * 
     */
    public GETPARAMETERS createGETPARAMETERS() {
        return new GETPARAMETERS();
    }

    /**
     * Create an instance of {@link GETTABLESRELATIONRESPONSE }
     * 
     */
    public GETTABLESRELATIONRESPONSE createGETTABLESRELATIONRESPONSE() {
        return new GETTABLESRELATIONRESPONSE();
    }

    /**
     * Create an instance of {@link ARRAYOFLSTRELATIONS }
     * 
     */
    public ARRAYOFLSTRELATIONS createARRAYOFLSTRELATIONS() {
        return new ARRAYOFLSTRELATIONS();
    }

    /**
     * Create an instance of {@link GETSYSTEMFIELDS }
     * 
     */
    public GETSYSTEMFIELDS createGETSYSTEMFIELDS() {
        return new GETSYSTEMFIELDS();
    }

    /**
     * Create an instance of {@link GETREPORTRESPONSE }
     * 
     */
    public GETREPORTRESPONSE createGETREPORTRESPONSE() {
        return new GETREPORTRESPONSE();
    }

    /**
     * Create an instance of {@link GETSYSTEMTABLESRESPONSE }
     * 
     */
    public GETSYSTEMTABLESRESPONSE createGETSYSTEMTABLESRESPONSE() {
        return new GETSYSTEMTABLESRESPONSE();
    }

    /**
     * Create an instance of {@link ARRAYOFSYSTABLESSTRU }
     * 
     */
    public ARRAYOFSYSTABLESSTRU createARRAYOFSYSTABLESSTRU() {
        return new ARRAYOFSYSTABLESSTRU();
    }

    /**
     * Create an instance of {@link GETTABLESRELATION }
     * 
     */
    public GETTABLESRELATION createGETTABLESRELATION() {
        return new GETTABLESRELATION();
    }

    /**
     * Create an instance of {@link GETPARAMETERSRESPONSE }
     * 
     */
    public GETPARAMETERSRESPONSE createGETPARAMETERSRESPONSE() {
        return new GETPARAMETERSRESPONSE();
    }

    /**
     * Create an instance of {@link ARRAYOFLSTPARAMETERS }
     * 
     */
    public ARRAYOFLSTPARAMETERS createARRAYOFLSTPARAMETERS() {
        return new ARRAYOFLSTPARAMETERS();
    }

    /**
     * Create an instance of {@link GETSYSTEMFIELDSRESPONSE }
     * 
     */
    public GETSYSTEMFIELDSRESPONSE createGETSYSTEMFIELDSRESPONSE() {
        return new GETSYSTEMFIELDSRESPONSE();
    }

    /**
     * Create an instance of {@link ARRAYOFSYSFIELDSSTRU }
     * 
     */
    public ARRAYOFSYSFIELDSSTRU createARRAYOFSYSFIELDSSTRU() {
        return new ARRAYOFSYSFIELDSSTRU();
    }

    /**
     * Create an instance of {@link GETTABLESX2NAMERESPONSE }
     * 
     */
    public GETTABLESX2NAMERESPONSE createGETTABLESX2NAMERESPONSE() {
        return new GETTABLESX2NAMERESPONSE();
    }

    /**
     * Create an instance of {@link GETSYSTEMTABLES }
     * 
     */
    public GETSYSTEMTABLES createGETSYSTEMTABLES() {
        return new GETSYSTEMTABLES();
    }

    /**
     * Create an instance of {@link MAKEINTERNATIONALIZATIONRESPONSE }
     * 
     */
    public MAKEINTERNATIONALIZATIONRESPONSE createMAKEINTERNATIONALIZATIONRESPONSE() {
        return new MAKEINTERNATIONALIZATIONRESPONSE();
    }

    /**
     * Create an instance of {@link INTERSTRU }
     * 
     */
    public INTERSTRU createINTERSTRU() {
        return new INTERSTRU();
    }

    /**
     * Create an instance of {@link GETREPORT }
     * 
     */
    public GETREPORT createGETREPORT() {
        return new GETREPORT();
    }

    /**
     * Create an instance of {@link GETVERSION }
     * 
     */
    public GETVERSION createGETVERSION() {
        return new GETVERSION();
    }

    /**
     * Create an instance of {@link GETVERSIONRESPONSE }
     * 
     */
    public GETVERSIONRESPONSE createGETVERSIONRESPONSE() {
        return new GETVERSIONRESPONSE();
    }

    /**
     * Create an instance of {@link MAKEINTERNATIONALIZATION }
     * 
     */
    public MAKEINTERNATIONALIZATION createMAKEINTERNATIONALIZATION() {
        return new MAKEINTERNATIONALIZATION();
    }

    /**
     * Create an instance of {@link GETTABLESX2NAME }
     * 
     */
    public GETTABLESX2NAME createGETTABLESX2NAME() {
        return new GETTABLESX2NAME();
    }

    /**
     * Create an instance of {@link SYSFIELDSSTRU }
     * 
     */
    public SYSFIELDSSTRU createSYSFIELDSSTRU() {
        return new SYSFIELDSSTRU();
    }

    /**
     * Create an instance of {@link LSTCONNECTION }
     * 
     */
    public LSTCONNECTION createLSTCONNECTION() {
        return new LSTCONNECTION();
    }

    /**
     * Create an instance of {@link SYSTABLESSTRU }
     * 
     */
    public SYSTABLESSTRU createSYSTABLESSTRU() {
        return new SYSTABLESSTRU();
    }

    /**
     * Create an instance of {@link LSTRELATIONS }
     * 
     */
    public LSTRELATIONS createLSTRELATIONS() {
        return new LSTRELATIONS();
    }

    /**
     * Create an instance of {@link LSTPARAMETERS }
     * 
     */
    public LSTPARAMETERS createLSTPARAMETERS() {
        return new LSTPARAMETERS();
    }

    /**
     * Create an instance of {@link ARRAYOFLSTCONNECTION }
     * 
     */
    public ARRAYOFLSTCONNECTION createARRAYOFLSTCONNECTION() {
        return new ARRAYOFLSTCONNECTION();
    }

}
