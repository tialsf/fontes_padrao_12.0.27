/*
 * IReportConnection.java
 *
 * Created on 16 de Janeiro de 2007, 14:48
 */
package com.microsiga.iReportRun.run;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;

/**
 * @author Alexandre Silva
 */
public class IReportConnection {

    public final static int CONN_ODBC = 0;
    public final static int CONN_MSSQL = 1;
    public final static int CONN_ORACLE = 2;
    public final static int CONN_DB2 = 3;
    public final static int DBF = 999;
    private String dataBaseURL = "";
    private String dataBasePort = "";
    private String dataBaseName = "";
    private String login = "";
    private String passwd = "";
    private String port = "";
    private int dbType = 0;
    private static final IReportLog log = IReportLog.getLog();

    public IReportConnection() {
    }

    /**
     * Retorna a conex�o estabelecida com o banco de dados. 
     * @return 
     */
    public Connection getConexao() throws ClassNotFoundException, SQLException, MalformedURLException, InstantiationException, IllegalAccessException {
        StringBuilder url = new StringBuilder("");
        Properties propriedades = new Properties();
        String classe = null;
        Connection conexao = null;
        boolean nativo = false;

        switch (dbType) {
            case CONN_MSSQL:
                //jdbc:sqlserver://<HOST>:<PORT>;databaseName=<DATABASE>
                classe = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

                url.append("jdbc:sqlserver://");
                url.append(dataBaseURL);

                //--------------------------------------------------------------
                // Se a porta n�o for informada n�o necessita colocar o dois pontos (:).
                //--------------------------------------------------------------
                if (!dataBasePort.equals("")) {
                    log.log().log(Level.INFO, " Use Port {0} in the Connection String ", dataBasePort);

                    url.append(":");
                    url.append(dataBasePort);
                }

                url.append(";databaseName=");
                url.append(dataBaseName);

                break;
            case CONN_ORACLE:
                //jdbc:oracle:thin:@<HOST>:<PORT>:<SID>
                classe = "oracle.jdbc.driver.OracleDriver";

                url.append("jdbc:oracle:thin:@");
                url.append(dataBaseURL);
                url.append(":");
                url.append(dataBasePort);
                url.append(":");
                url.append(dataBaseName);

                break;
            case CONN_DB2:
                //jdbc:db2://<HOST>:<PORTA>/<NOME>
                classe = "com.ibm.db2.jcc.DB2Driver";

                url.append("jdbc:db2://");
                url.append(dataBaseURL);
                url.append(":");
                url.append(dataBasePort);
                url.append("/");
                url.append(dataBaseName);

                break;
            case DBF:
                classe = "acs.jdbc.Driver";

                url.append("jdbc:atinav:");
                url.append(dataBaseURL);
                url.append(":");
                url.append(dataBasePort);

                break;
            case CONN_ODBC:
                classe = "sun.jdbc.odbc.JdbcOdbcDriver";

                url.append("jdbc:odbc:");
                url.append(dataBaseName);

                nativo = true;

                break;
        }

        if (nativo) {
            log.log().log(Level.INFO, "JDBC native driver: {0}", url.toString());

            Class.forName(classe);
            conexao = DriverManager.getConnection(url.toString(), login, passwd);
        } else {
            log.log().log(Level.INFO, "JDBC users driver: {0}", url.toString());

            Driver driver = (Driver) carregaJDBC().loadClass(classe).newInstance();
            propriedades.setProperty("user", login);
            propriedades.setProperty("password", passwd);
            conexao = driver.connect(url.toString(), propriedades);
        }

        log.log().log(Level.INFO, "Connection login: {0}", login);
        log.log().log(Level.INFO, "Connection password: {0}", '?');

        return conexao;
    }

    /**
     * Loader din�mico para arquivos .jar de drivers JDBC. 
     * @return
     * @throws MalformedURLException 
     */
    private ClassLoader carregaJDBC() throws MalformedURLException {
        String path = System.getProperty("user.dir").concat("\\").concat("ireport").concat("\\").concat("jdbc").concat("\\");
        File diretorio = new File(path);
        URL[] urls = null;

        log.log().log(Level.INFO, "Loading JDBC drivers from: {0}", diretorio);
        
        if (diretorio.isDirectory()) {
            File[] arquivos = diretorio.listFiles();

            if (arquivos.length > 0) {
                log.log().log(Level.INFO, "Files loaded: {0}", arquivos.length);
                urls = new URL[arquivos.length];

                for (int i = 0; i < arquivos.length; i++) {
                    File arquivo = (File) arquivos[i];
                    urls[i] = arquivo.toURI().toURL();
                }
            } else {
                log.log().log(Level.SEVERE, "JDBC drivers are not in: {0}", diretorio);
            }
        } else {
            log.log().log(Level.SEVERE, "JDBC drivers directory does not exist: {0}", diretorio);
        }

        return URLClassLoader.newInstance(urls, this.getClass().getClassLoader());
    }

    /**
     * 
     * @param dataBaseURL 
     */
    public void setURL(String dataBaseURL) {
        this.dataBaseURL = dataBaseURL;
    }

    /**
     * 
     * @param dataBasePort 
     */
    public void setPorta(String dataBasePort) {
        this.dataBasePort = !"!".equals(dataBasePort.trim()) ? dataBasePort.trim() : "";
    }

    /**
     * 
     * @param dataBaseName 
     */
    public void setDatabase(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }

    /**
     * 
     * @param login 
     */
    public void setUsuario(String login) {
        this.login = login;
    }

    /**
     * 
     * @param passwd 
     */
    public void setSenha(String passwd) {
        this.passwd = passwd;
    }

    /**
     * 
     * @param port 
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * 
     * @return 
     */
    public String getPort() {
        return port;
    }

    /**
     * 
     * @param dbType 
     */
    public void setTipo(int dbType) {
        this.dbType = dbType;
    }
}