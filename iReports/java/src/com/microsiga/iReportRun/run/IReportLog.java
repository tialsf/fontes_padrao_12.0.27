package com.microsiga.iReportRun.run;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author valdineyg
 */
public class IReportLog {

    private static IReportLog log;
    private static Logger logger;

    private IReportLog() {
        logger = Logger.getLogger("RunReport");

        try {
            FileHandler arquivo = new FileHandler("%t/RunReport.log");
            arquivo.setFormatter(new SimpleFormatter());
            logger.setLevel(Level.ALL);
            logger.addHandler(arquivo);
        } catch (Exception ex) {
            Logger.getLogger(RunReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Retorna inst�ncia �nica do gerenciador de log. 
     * @return 
     */
    public static synchronized IReportLog getLog() {
        if (log == null) {
            log = new IReportLog();
        }
        return log;
    }

    /**
     * Configura o gerencidor de log.
     * @return 
     */
    public Logger log() {
        return logger;
    }
}
