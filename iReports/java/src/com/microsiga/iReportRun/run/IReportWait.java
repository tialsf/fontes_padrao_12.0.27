/*
 * IReportWait.java
 */
package com.microsiga.iReportRun.run;

import java.awt.Dimension;
import javax.swing.ImageIcon;

/**
 * @author valdineyg
 */
public class IReportWait extends javax.swing.JFrame {

    static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new IReportWait().setVisible(true);
            }
        });
    }

    public IReportWait() {
        this.setPreferredSize(new Dimension(400, 85));
        initComponents();
    }

    /**
     * Define o progresso da barra de status. 
     * @param status 
     */
    public void setStatus(int progresso) {
        pbProgresso.setValue(progresso);
    }

    /**
     * Define a mensagem que ser� exibida. 
     * @param status 
     */
    public void showMensagem(String mensagem) {
        StringBuilder log = new StringBuilder("");
        log.append(mensagem);
        log.append("\n");
        log.append("\n");
        log.append("See complete log at: ");
        log.append(System.getProperty("java.io.tmpdir"));
        log.append("\\");
        log.append("RunReport.log");
        
        txtMensagen.setText(log.toString());
        
        this.setBounds(this.getX(), this.getY(), 400, 170);
        this.repaint();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pbProgresso = new javax.swing.JProgressBar();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtMensagen = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("iReport");
        setAlwaysOnTop(true);
        setIconImage(new ImageIcon(getClass().getResource("/com/microsiga/imagens/totvs.png")).getImage());
        setName("iReportWait"); // NOI18N
        setResizable(false);
        getContentPane().setLayout(null);

        pbProgresso.setBackground(new java.awt.Color(204, 204, 204));
        pbProgresso.setForeground(new java.awt.Color(153, 153, 0));
        pbProgresso.setBorderPainted(false);
        pbProgresso.setPreferredSize(new java.awt.Dimension(148, 17));
        pbProgresso.setStringPainted(true);
        getContentPane().add(pbProgresso);
        pbProgresso.setBounds(10, 20, 370, 20);

        txtMensagen.setColumns(1);
        txtMensagen.setEditable(false);
        txtMensagen.setFont(new java.awt.Font("Courier New", 0, 11)); // NOI18N
        txtMensagen.setLineWrap(true);
        txtMensagen.setRows(5);
        txtMensagen.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtMensagen.setMinimumSize(new java.awt.Dimension(164, 20));
        txtMensagen.setRequestFocusEnabled(false);
        jScrollPane1.setViewportView(txtMensagen);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(10, 60, 370, 74);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JProgressBar pbProgresso;
    private javax.swing.JTextArea txtMensagen;
    // End of variables declaration//GEN-END:variables
}
