/*
 * RunReport.java
 * Created on 16 de Janeiro de 2007, 11:01
 *
 * Instru��es:
 * A lib do projeto deve conter os arquivos do diret�rio \Jaspersoft\iReport-<vesao>\ireport\modules\ext
 * Ao cliar o client do webservice, informar o pacote com.microsiga.iReportRun.run para cria��o.
 * Par�metros para execu��o: |<relatorio>| |<tipo de driver jdbc>| |<database>| |<URL do BD>| |<porta do BD>| |<usu�rio do BD>| |<senha do BD>| |<URL do WS>| |<grupo de empresas>| |<empresa>| |unidade de negocio| |filial| |<destino>| |<copias>| |<imprime?>| |<codificacao>|
 */
package com.microsiga.iReportRun.run;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException; 
import java.net.URL;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;
import java.util.logging.Level;
import javax.swing.ImageIcon;
import javax.xml.namespace.QName;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.commons.codec.binary.Base64;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * Classe para execucao dos relatorios via Jasper.
 *
 * @author Alexandre Silva
 * @version 1.0
 */
public class RunReport {

    //Vers�o no formato protheus.data.ireport 
    public final static String VERSAO = "12.20140912.5.6.0";
    //Constantes. 
    public final static int PARAMETROS = 18;
    public final static int VIDEO = 0;
    public final static int IMPRESSORA = 1;
    public final static int EMPRESA = 0;
    public final static int UNIDADENEGOCIO = 1;
    public final static int FILIAL = 2;
    //Par�metros.
    private String nome = "";
    private String tipo_conexao = "";
    private String url_ws;
    private String grupo_empresa = "";
    private String empresa = "";
    private String unidade_negocio = "";
    private String filial = "";
    private int destino = VIDEO;
    private int copias = 1;
    private boolean imprimir = true;
    private String codificacao = "UTF-8";
    private String pais = "";
    private String idioma = "";
    //Utilit�rios.
    private IReportConnection conexao;
    private HashMap<String, ArrayList> tabelas = new HashMap<String, ArrayList>();
    private JasperReport relatorio;
    private Map parametros;
    //Log
    private static final IReportLog log = IReportLog.getLog();
    private static final IReportWait wait = new IReportWait();
    //Internacionaliza��o
    private static final ResourceBundle i18n = ResourceBundle.getBundle("international");

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length == PARAMETROS) {
            log.log().log(Level.INFO, "Parameters: {0} {1} {2} {3} {4} {5} |?| {7} {8} {9} {10} {11} {12} {13} {14} {15} {16} {17}", args);
            wait.setStatus(10);

            try {
                wait.setVisible(true);
                wait.setLocationRelativeTo(null);
                wait.setTitle(i18n.getString("Titulo").concat(VERSAO));
                RunReport relatorio = new RunReport(args);
                relatorio.exibeRelatorio();
            } catch (Exception ex) {
                log.log().log(Level.SEVERE, "Exception", ex);
                wait.showMensagem(ex.toString());
            }
        } else {
            log.log().log(Level.SEVERE, "Were informed only {0} parameter", args.length);
            wait.showMensagem("Insufficient number of parameters: " + args.length);
        }
    }

    /**
     * Coordena as atividades para a prepara��o do relat�rio para exibi��o.
     *
     * @param args
     * @throws FileNotFoundException
     * @throws Exception
     */
    public RunReport(String[] args) throws FileNotFoundException, Exception {
        this.configuraParametros(args);
        this.configuraConexao(args);
        this.verificaVersao();

        String relatorioString = carregaRelatorio();

        if (relatorioString.startsWith("<?xml")) {
            log.log().log(Level.INFO, "Running report in the format: '.jrxml'");
            wait.setStatus(20);

            Document relatorioXML = this.StringToXML(relatorioString);
            parametros = this.configuraRelatorio(relatorioXML);
            relatorio = this.copilaRelatorio(relatorioXML);
        } else {
            log.log().log(Level.INFO, "Running report in the format: '.jasper'");
            wait.setStatus(20);

            byte[] relatorioByte = Base64.decodeBase64(relatorioString.getBytes());
            relatorio = this.carregaRelatorio(new ByteArrayInputStream(relatorioByte));
            parametros = this.configuraRelatorio(JasperCompileManager.writeReportToXml(relatorio));
        }
    }

    /**
     * Define os atributos de execu��o com os valores dos par�metros recebidos.
     *
     * @param args
     * @throws NumberFormatException
     */
    private void configuraParametros(String[] args) throws NumberFormatException {
        //[0] Nome do relatorio. 
        this.setNome(args[0].replace("|", ""));
        //[1] Tipo da conex�o.
        this.setTipoConexao(Integer.parseInt(args[1].replace("|", "")));
        //[7] Localiza��o do webservice para obtencao do relat�rio. 
        this.setURL(args[7].replace("|", ""));
        //[8] Grupo de Empresas.
        this.setGrupoEmpresa(args[8].replace("|", ""));
        //[9] Empresas.
        this.setEmpresa(args[9].replace("|", "").replace("*", " "));
        //[10] Unidade de Neg�cio.
        this.setUnidadeNegocio(args[10].replace("|", "").replace("*", " "));
        //[11] Filial.
        this.setFilial(args[11].replace("|", "").replace("*", " "));
        //[12] Destino do relatorio.
        this.setDestino(Integer.parseInt(args[12].replace("|", "")));
        //[13] Numero de copias.
        this.setCopias(Integer.parseInt(args[13].replace("|", "")));
        //[14] Mostra a tela da sele��o da impressora.
        this.setImprime(args[14].replace("|", ""));
        //[15] Codifica��o do relat�rio       
        this.setCodificacao(args[15].replace("|", ""));
        //[16] Pa�s da instala��o
        this.setPais(args[16].replace("|", ""));
        //[17] Idioma da instala��o
        this.setIdioma(args[17].replace("|", ""));        
    }

    /**
     * Define os atributos de conex�o com os valores dos par�metros recebidos.
     *
     * @param args
     * @throws NumberFormatException
     */
    private void configuraConexao(String[] args) throws NumberFormatException {
        conexao = new IReportConnection();
        //[1] Tipo da conex�o.
        conexao.setTipo(Integer.parseInt(args[1].replace("|", "")));
        //[2] Nome do banco de dados ou conex�o ODBC.
        conexao.setDatabase(args[2].replace("|", ""));
        //[3] URL do banco de dados.
        conexao.setURL(args[3].replace("|", ""));
        //[4] Porta de conex�o do banco de dados. 
        conexao.setPorta(args[4].replace("|", ""));
        //[5] Usuario do banco de dados.
        conexao.setUsuario(args[5].replace("|", ""));
        //[6] Senha do Usuario do banco de dados.
        conexao.setSenha(args[6].replace("|", ""));
    }

    /**
     * Conecta ao webservice do Protheus.
     *
     * @return
     * @throws MalformedURLException
     */
    private IREPORTSOAP conectaWebService() throws MalformedURLException {
        IREPORT service = new IREPORT(new URL(getURL(true)), new QName("http://webservices.microsiga.com.br/", "IREPORT"));
        return service.getIREPORTSOAP();
    }

    /**
     * Verifica se o cliente e o servidor est�o na mesma vers�o.
     *
     * @return
     * @throws MalformedURLException
     */
    private void verificaVersao() throws MalformedURLException {
        try {
            String versaoServidor = conectaWebService().getversion("00");
            String[] versoes = {versaoServidor, VERSAO};

            if (versaoServidor == null ? VERSAO == null : versaoServidor.equalsIgnoreCase(VERSAO)) {
                log.log().log(Level.INFO, "Version: {0}", VERSAO);
            } else {
                log.log().log(Level.WARNING, "There are differences of versions between server {0} and client {1}", versoes);
            }
        } catch (Exception ex) {
            log.log().log(Level.WARNING, "There are differences of versions between server and client. Please, update the server.");
        }
    }

    /**
     * Recupera o relat�rio no formato de string atrav�s do webservice.
     *
     * @return
     * @throws MalformedURLException
     */
    private String carregaRelatorio() throws MalformedURLException {
        String cRet = conectaWebService().getreport("00", getNome());

        // Tratamento para caracteres com acento agudo.
        cRet = cRet.replaceAll("&Oacute;", "Ó");
        cRet = cRet.replaceAll("&Iacute;", "I");
        cRet = cRet.replaceAll("&Aacute;", "A");

        return cRet;
    }

    /**
     * Recupera o relat�rio no formato de string atrav�s do webservice.
     *
     * @param nome
     * @return
     * @throws MalformedURLException
     */
    private String carregaRelatorio(String nome) throws MalformedURLException {
        return conectaWebService().getreport("00", nome);
    }

    /**
     * Recupera o relat�rio no formato de string atrav�s do webservice.
     *
     * @param urlReport
     * @return
     * @throws JRException
     */
    private JasperReport carregaRelatorio(InputStream relatorioInputStream) throws JRException {
        return (JasperReport) JRLoader.loadObject(relatorioInputStream);
    }

    /**
     * Aplica os conceitos b�sicos do ERP nos subrelat�rios.
     *
     * @param xmlReport
     * @return
     * @throws DocumentException
     * @throws MalformedURLException
     * @throws JRException
     */
    private Map configuraRelatorio(Document relatorio) throws MalformedURLException, Exception {
        HashMap parametro = new HashMap();
        parametro.put(JRParameter.REPORT_LOCALE, new Locale(getIdioma(), getPais()));
        Element root = relatorio.getRootElement();
        String queryLanguage = root.element("queryString").attributeValue("language");

        if (queryLanguage == null || queryLanguage.equalsIgnoreCase("SQL")) {
            this.filtraTabela(root, parametro);
            this.filtraPergunta(root, parametro);
            this.filtraFilial(root);
            this.filtraDeletados(root);
            this.configuraSubRelatorio(root, parametro);
        } else {
            throw new Exception(MessageFormat.format("{0} Query Language is not supported in iReport Integration. You should use SQL Query Language.", queryLanguage));
        }

        return parametro;
    }

    /**
     * Aplica os conceitos b�sicos do ERP nos subrelat�rios.
     *
     * @param xmlReport
     * @return
     * @throws DocumentException
     * @throws MalformedURLException
     * @throws JRException
     */
    private Map configuraRelatorio(String xmlReport) throws DocumentException, MalformedURLException, JRException, Exception {
        SAXReader reader = new SAXReader();

        reader.setEntityResolver(new DTDJasperResolver());
        reader.setEncoding(getCodificacao());
        Document docRelatorio = reader.read(new InputStreamReader(new ByteArrayInputStream(xmlReport.getBytes())), getCodificacao());
        docRelatorio.setXMLEncoding(getCodificacao());

        return configuraRelatorio(docRelatorio);
    }

    /**
     * Aplica os conceitos b�sicos do ERP nos subrelat�rios.
     *
     * @param relatorio
     * @param parametros
     * @throws MalformedURLException
     * @throws JRException
     * @throws DocumentException
     */
    private void configuraSubRelatorio(Element relatorio, Map parametros) throws MalformedURLException, JRException, DocumentException {
        for (Iterator i = relatorio.elementIterator("parameter"); i.hasNext();) {
            Element parametro = (Element) i.next();
            String descricao = parametro.elementText("parameterDescription");
            String nome_relatorio = ((String) parametro.attribute("name").getData());

            if (descricao != null) {
                if (descricao.equalsIgnoreCase("SUBRELATORIO") || descricao.equalsIgnoreCase("SUBREPORT")) {
                    parametro.attribute("class").setData("java.lang.Object");

                    log.log().log(Level.INFO, "Loading subreport: {0}", nome_relatorio);
                    Document subRelatorioXML = this.StringToXML(carregaRelatorio(nome_relatorio));

                    filtraFilial(subRelatorioXML.getRootElement());
                    filtraDeletados(subRelatorioXML.getRootElement());

                    String subRelatorio = (String) parametro.attribute("name").getData();
                    parametros.put(subRelatorio, copilaRelatorio(subRelatorioXML));
                }
            }
        }
    }

    /**
     * Compila o relat�rio.
     *
     * @param docRelatorio
     * @return
     * @throws JRException
     */
    private JasperReport copilaRelatorio(Document docRelatorio) throws JRException {
        ByteArrayInputStream bytesRelatorio = new ByteArrayInputStream(docRelatorio.asXML().getBytes());
        return JasperCompileManager.compileReport(bytesRelatorio);
    }

    /**
     * Exibe o relat�rio.
     *
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InstantiationException
     * @throws MalformedURLException
     * @throws IllegalAccessException
     */
    protected void exibeRelatorio() throws ClassNotFoundException, SQLException, InstantiationException, MalformedURLException, IllegalAccessException, JRException {
        log.log().log(Level.INFO, "Filling report.");
        //Preenchimento do relat�rio.
        JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, getConexao().getConexao());

        //Defini��o do JasperViewer.
        JasperViewer visualizador = new JasperViewer(impressao, true);
        visualizador.setTitle(i18n.getString("Titulo").concat(VERSAO));
        visualizador.setIconImage(new ImageIcon(getClass().getResource("/com/microsiga/imagens/totvs.png")).getImage());

        //Defini��o do n�mero de c�pias. 
        int numeroPaginas = impressao.getPages().size();

        if (numeroPaginas > 0) {
            switch (this.getDestino()) {
                case VIDEO:
                    log.log().log(Level.INFO, "Displaying video report.");
                    wait.setStatus(90);
                    wait.dispose();

                    //Exibe o JasperViewer.
                    visualizador.setExtendedState(JasperViewer.MAXIMIZED_BOTH);
                    visualizador.setVisible(true);

                    break;
                case IMPRESSORA:
                    log.log().log(Level.INFO, "Send report to printer.");
                    wait.setStatus(90);
                    wait.dispose();

                    //Oculta o JasperViewer.
                    visualizador.dispose();

                    //Envia o relat�rio para impressora. 
                    if (isImprime()) {
                        JasperPrintManager.printPages(impressao, 0, numeroPaginas - 1, true);
                    } else {
                        for (int copia = 0; copia <= (getCopias() - 1); copia++) {
                            JasperPrintManager.printPages(impressao, 0, numeroPaginas - 1, false);
                        }
                    }
                    break;
            }
        } else {
            log.log().log(Level.INFO, "The document has no pages!");
            wait.setStatus(100);
            wait.dispose();
            visualizador.dispose();
        }

        System.gc();
    }

    /**
     * Parseia o valor da resposta de uma pergunta para o tipo de dado esperado
     * pelo par�metro no relat�rio.
     *
     * @param parametros
     * @param chave
     * @param conteudo
     * @return
     */
    private Object parseiaResposta(Iterator parametros, String chave, String conteudo) {
        boolean inclui = false;
        Object valor = null;

        while (parametros.hasNext()) {
            Element parametro = (Element) parametros.next();

            if (parametro.attributeValue("name").equalsIgnoreCase(chave)) {
                String paramType = parametro.attribute("class").getText();
                inclui = true;

                if (paramType.equalsIgnoreCase("java.lang.String")) {
                    valor = conteudo;
                } else if (paramType.equalsIgnoreCase("java.lang.Double")) {
                    valor = Double.parseDouble(conteudo);
                } else if (paramType.equalsIgnoreCase("java.lang.Float")) {
                    valor = Float.parseFloat(conteudo);
                } else if (paramType.equalsIgnoreCase("java.lang.Integer")) {
                    valor = Integer.parseInt(conteudo);
                } else if (paramType.equalsIgnoreCase("java.util.Date")) {
                    parametro.attribute("class").setValue("java.lang.String");
                    valor = conteudo;
                }
                
                break;
            }

            if (!parametros.hasNext() && !inclui) {
                valor = conteudo;
            }
        }
        return valor;
    }

    /**
     * Alimenta os par�metros do relat�rio com a resposta da pergunta com o qual
     * foi relacionado.
     *
     * @param root
     * @param parametros
     * @throws MalformedURLException
     */
    private void filtraPergunta(Element root, Map parametros) throws MalformedURLException {
        ARRAYOFLSTPARAMETERS result = conectaWebService().getparameters("", getGrupoEmpresa(), getEmpresa().concat(this.getUnidadeNegocio()).concat(getFilial()), getNome());

        log.log().log(Level.INFO, "Questions: {0}", result.getLSTPARAMETERS().size());
        wait.setStatus(60);

        for (Iterator i = result.getLSTPARAMETERS().iterator(); i.hasNext();) {
            LSTPARAMETERS parametro = (LSTPARAMETERS) i.next();
            StringBuilder chave = new StringBuilder();
            chave.append(getNome());
            chave.append("-");
            chave.append(parametro.getPARORDER().trim());
            Object valor = parseiaResposta(root.elementIterator("parameter"), chave.toString(), parametro.getPARVALUE());
            String caixa = decisaoCaixa(root.elementIterator("parameter"), chave.toString());
            parametros.put(caixa.toString(), valor);

            if (Integer.parseInt(parametro.getPARORDER().trim()) >= 100) {
                log.log().log(Level.INFO, "Entry Point Question: " + parametro.getPARNAME().trim() + "; Report Parameter: " + chave.toString() + "; Answer: {0}", valor);
            } else {
                log.log().log(Level.INFO, "Default Question: " + parametro.getPARNAME().trim() + "; Report Parameter: " + chave.toString() + "; Answer: {0}", valor);
            }
        }
    }
    
    /**
     * Decidi qual ser� a caixa correta a ser utilizada nos par�metros do Irepots.
     * @param parametros
     * @param chave
     * @return String com o conte�do do par�metro com a caixa correta.
     * @author Helio Leal
     */
    public String decisaoCaixa(Iterator parametros, String chave) {
        String chaveCaixaCorreta = "";

        while (parametros.hasNext()) {
            Element par = (Element) parametros.next();

            if (par.attributeValue("name").equalsIgnoreCase(chave.toString())) {
                chaveCaixaCorreta = par.attributeValue("name");
            }
        }
        return chaveCaixaCorreta;
    }

    /**
     * Retorna as tabelas que foram utilizadas na forma de par�metro no
     * relat�rio.
     *
     * @param root
     * @return
     */
    private StringBuilder getTabela(Element root) {
        StringBuilder lista_tabelas = new StringBuilder();

        for (java.util.Iterator i = root.elementIterator("parameter"); i.hasNext();) {
            Element parametro = (Element) i.next();

            if (parametro.elementText("parameterDescription") != null) {
                String descricao = parametro.elementText("parameterDescription");

                if (descricao.equalsIgnoreCase("TABLE") || descricao.equalsIgnoreCase("TABELA")) {
                    if (lista_tabelas.length() > 2) {
                        lista_tabelas.append("|");
                    }

                    lista_tabelas.append(parametro.attribute("name").getData());
                }
            }
        }
        return lista_tabelas;
    }

    /**
     * Recupera a regra de compartilhamento de cada tabela utlizada na forma de
     * par�metro no relat�rio.
     *
     * @param root
     * @param parametros
     * @throws MalformedURLException
     */
    private void filtraTabela(Element root, Map parametros) throws MalformedURLException {
        StringBuilder lista_tabelas = getTabela(root);

        log.log().log(Level.INFO, "Tables: {0}", lista_tabelas.toString());
        wait.setStatus(50);

        ARRAYOFSYSTABLESSTRU result = conectaWebService().gettablesx2NAME("", getGrupoEmpresa(), getEmpresa().concat(this.getUnidadeNegocio()).concat(getFilial()), lista_tabelas.toString());

        for (Iterator i = result.getSYSTABLESSTRU().iterator(); i.hasNext();) {
            ArrayList<String> compartilhamento = new ArrayList<String>();
            SYSTABLESSTRU tabela = (SYSTABLESSTRU) i.next();
            String Nome = tabela.getTABELA();
            String Alias = Nome.substring(0, 3);
            parametros.put(Alias, tabela.getTABELA());

            if (!this.tabelas.containsKey(tabela.getTABELA())) {
                compartilhamento.add(tabela.getMODEEMP());
                compartilhamento.add(tabela.getMODEUN());
                compartilhamento.add(tabela.getMODE());

                this.tabelas.put(tabela.getTABELA(), compartilhamento);
            }
        }
    }

    /**
     * Identifica se deve ser aplicado o conceito de gest�o de empresas no
     * relat�rio.
     *
     * @param root
     */
    private void filtraFilial(Element root) {
        StringBuilder query = new StringBuilder((String) root.element("queryString").getData());

        for (Iterator i = root.elementIterator("variable"); i.hasNext();) {
            Element variavel = (org.dom4j.Element) i.next();
            String nome_variavel = ((String) variavel.attribute("name").getData());

            if (nome_variavel.equalsIgnoreCase("FILIAL")) {
                gerenciaFilial(root, variavel, query);
                break;
            }
        }
    }

    /**
     * Identifica se deve ser aplicado o conceito de deletados no relat�rio.
     *
     * @param root
     */
    private void filtraDeletados(Element root) {
        StringBuilder query = new StringBuilder((String) root.element("queryString").getData());

        for (Iterator i = root.elementIterator("variable"); i.hasNext();) {
            Element variavel = (org.dom4j.Element) i.next();
            String nome_variavel = ((String) variavel.attribute("name").getData());

            if (nome_variavel.equalsIgnoreCase("DELETED") || nome_variavel.equalsIgnoreCase("D_E_L_E_T_")) {
                gerenciaDeletados(root, variavel, query);
                break;
            }
        }
    }

    /**
     * Aplica o conceito de deletados no relat�rio
     *
     * @param root
     * @param variavel
     * @param query
     */
    private void gerenciaDeletados(Element root, Element variavel, StringBuilder query) {
        StringBuilder selecao = new StringBuilder("");
        StringBuilder groupBy = new StringBuilder("");
        StringBuilder filtro = new StringBuilder("");

        log.log().log(Level.INFO, "Deleted Record Management: On");
        wait.setStatus(80);

        for (Iterator i = tabelas.keySet().iterator(); i.hasNext();) {
            String nome_tabela = (String) i.next();
            StringBuilder parametro = new StringBuilder("$P!{");
            parametro.append(nome_tabela.substring(0, 3));
            parametro.append("}");

            if (query.indexOf(parametro.toString()) > 0) {
                StringBuffer campo = new StringBuffer(nome_tabela);

                if (getTipoServidor().equalsIgnoreCase("AS400")) {
                    campo.append(".@DELETED@");
                } else {
                    campo.append(".D_E_L_E_T_");
                }

                selecao.append(campo);
                selecao.append(" AS ");
                selecao.append(nome_tabela.substring(0, 3));
                selecao.append("_IRDEL, ");

                if (!filtro.toString().isEmpty()) {
                    filtro.append(" && ");
                }

                filtro.append("$F{");
                filtro.append(nome_tabela.substring(0, 3));
                filtro.append("_IRDEL");
                filtro.append("}.contains(");
                filtro.append('"');
                filtro.append(' ');
                filtro.append('"');
                filtro.append(")");

                addField(root, "_IRDEL", nome_tabela);
                groupBy.append(campo);

                if (i.hasNext()) {
                    groupBy.append(", ");
                }
            }
        }

        log.log().log(Level.INFO, "Deleted Record Management Fields: {0}", selecao.toString());
        log.log().log(Level.INFO, "Deleted Record Management Filter: {0}", filtro.toString());

        setVariable(root, variavel, filtro.toString().trim());
        addQueryField(root, selecao.toString().trim());
        addQueryGroup(root, groupBy.toString().trim());
    }

    /**
     * Aplica o conceito de gest�o de empresas no relat�rio.
     *
     * @param root
     * @param variavel
     * @param query
     */
    private void gerenciaFilial(Element root, Element variavel, StringBuilder query) {
        StringBuilder selecao = new StringBuilder("");
        StringBuilder groupBy = new StringBuilder("");
        StringBuilder filtro = new StringBuilder("");

        log.log().log(Level.INFO, "Share Management: On");
        wait.setStatus(70);

        for (Iterator i = tabelas.keySet().iterator(); i.hasNext();) {
            String nome_tabela = (String) i.next();
            ArrayList<String> valor = tabelas.get(nome_tabela);
            StringBuilder parametro = new StringBuilder("$P!{");
            parametro.append(nome_tabela.substring(0, 3));
            parametro.append("}");

            if (query.indexOf(parametro.toString()) > 0) {
                StringBuilder campo = new StringBuilder(nome_tabela);
                campo.append(".");

                if (nome_tabela.startsWith("S")) {
                    campo.append(nome_tabela.substring(1, 3));
                } else {
                    campo.append(nome_tabela.substring(0, 3));
                }

                campo.append("_FILIAL");

                selecao.append(campo);
                selecao.append(" AS ");
                selecao.append(nome_tabela.substring(0, 3));
                selecao.append("_IRFIL, ");

                if (!filtro.toString().isEmpty()) {
                    filtro.append(" && ");
                }

                filtro.append("$F{");
                filtro.append(nome_tabela.substring(0, 3));
                filtro.append("_IRFIL");
                filtro.append("}.contains(");
                filtro.append('"');


                if (valor.get(EMPRESA).equalsIgnoreCase("E")) {
                    filtro.append(this.getEmpresa());
                } else {
                    for (int j = 0; j < this.getEmpresa().length(); j++) {
                        filtro.append(" ");
                    }
                }

                if (valor.get(UNIDADENEGOCIO).equalsIgnoreCase("E")) {
                    filtro.append(this.getUnidadeNegocio());
                } else {
                    for (int j = 0; j < this.getUnidadeNegocio().length(); j++) {
                        filtro.append(" ");
                    }
                }

                if (valor.get(FILIAL).equalsIgnoreCase("E")) {
                    filtro.append(this.getFilial());
                } else {
                    for (int j = 0; j < this.getFilial().length(); j++) {
                        filtro.append(" ");
                    }
                }

                filtro.append('"');
                filtro.append(")");

                addField(root, "_IRFIL", nome_tabela);
                groupBy.append(campo);

                if (i.hasNext()) {
                    groupBy.append(", ");
                }
            }
        }

        log.log().log(Level.INFO, "Share Management Fields: {0}", selecao.toString());
        log.log().log(Level.INFO, "Share Management Filter: {0}", filtro.toString());

        setVariable(root, variavel, filtro.toString());
        addQueryField(root, selecao.toString());
        addQueryGroup(root, groupBy.toString());
    }

    /**
     * Parseia a string recebida atrav�s do webservice com o conte�do do
     * relat�rio para o formato XML.
     *
     * @param relatorioString
     * @return
     * @throws DocumentException
     */
    private Document StringToXML(String relatorioString) throws DocumentException {
        SAXReader leitor = new SAXReader();
        leitor.setEntityResolver(new DTDJasperResolver());
        leitor.setEncoding(getCodificacao());
        Document relatorioXML = leitor.read(new InputStreamReader(new ByteArrayInputStream(relatorioString.getBytes())), getCodificacao());
        relatorioXML.setXMLEncoding(getCodificacao());
        return relatorioXML;
    }

    /**
     * Insere um novo 'field' no relat�rio.
     *
     * @param root
     * @param campo
     * @param tabela
     */
    private void addField(Element root, String campo, String tabela) {
        Element novoCampo = root.element("field").createCopy();
        StringBuilder nome_campo = new StringBuilder();
        nome_campo.append(tabela.substring(0, 3));
        nome_campo.append(campo);
        novoCampo.addAttribute("name", nome_campo.toString());
        novoCampo.addAttribute("class", "java.lang.String");
        List elements = root.elements();
        elements.add(elements.indexOf(root.element("field")), novoCampo);
    }

    /**
     * Define a express�o de uma vari�vel.
     *
     * @param variavel
     * @param filtro
     */
    private void setVariable(Element root, Element variavel, String filtro) {
        variavel.attribute("class").setData("java.lang.Boolean");

        if (!filtro.isEmpty()) {
            if (variavel.element("variableExpression") == null) {
                variavel.addElement("variableExpression").addCDATA(filtro);
            } else {
                variavel.element("variableExpression").setText(filtro);
            }

            addFilterExpression(root, variavel.attribute("name").getData().toString());
        }
    }

    /**
     * Insere um novo campo na query do relat�rio.
     *
     * @param root
     * @param campo
     */
    private void addQueryField(Element root, String campo) {
        StringBuilder queryString = new StringBuilder(root.element("queryString").getText());
        int atSelect = queryString.toString().toLowerCase().indexOf("select") + 7;
        queryString.insert(atSelect, campo);
        root.element("queryString").setText(queryString.toString());

        log.log().log(Level.INFO, "Query after add field: {0}", queryString.toString().trim());
    }

    /**
     * Insere um novo agrupador na query do relat�rio.
     *
     * @param root
     * @param campo
     */
    private void addQueryGroup(Element root, String campo) {
        StringBuilder queryString = new StringBuilder(root.element("queryString").getText());
        StringBuffer agrupador = new StringBuffer();
        String selecao = queryString.toString().toLowerCase();
        int at = -1;
        int atGroup = 0;

        //Localiza a express�o 'GROUP BY' na query do relat�rio para inser��o de novo agrupador
        do {
            int atBy = 0;
            atGroup = selecao.indexOf("group") + 5;
            atBy = selecao.indexOf("by", atGroup);
            if (atBy > atGroup && selecao.substring(atGroup, atBy).trim().length() == 0) {
                at = atBy + 3;
                break;
            }

        } while (selecao.indexOf("group", atGroup) != -1);

        //Insere o novo agrupador.    
        if (at != -1) {
            agrupador.append(campo);
            agrupador.append(", ");
            queryString.insert(at, agrupador);
            root.element("queryString").setText(queryString.toString());

            log.log().log(Level.INFO, "Query after add group by : {0}", queryString.toString().trim());
        }
    }

    /**
     * Insere uma nova express�o de filtro ao relat�rio.
     *
     * @param root
     * @param variavel
     */
    private void addFilterExpression(Element root, String variavel) {
        Element filtroAtual = root.element("filterExpression");
        String instancia = "";
        int atBoolean = 0;
        boolean hasBoolean = true;

        //new Boolean()
        if (filtroAtual != null) {
            instancia = (filtroAtual.getData().toString()).replace(';', ' ');
            atBoolean = instancia.indexOf("Boolean");
            atBoolean = (atBoolean > -1) ? (atBoolean += 8) : 0;

            if (atBoolean < 3) {
                hasBoolean = false;
                instancia = "new Boolean()";
                atBoolean = instancia.indexOf("Boolean") + 8;
            }
        } else {
            hasBoolean = false;
            instancia = "new Boolean()";
            atBoolean = instancia.indexOf("Boolean") + 8;
        }

        //$V{ <condicao> }.booleanValue()    
        StringBuilder condicao = new StringBuilder();
        condicao.append("$V{");
        condicao.append(variavel);
        condicao.append("}.booleanValue()");
        condicao.append((hasBoolean && filtroAtual != null) ? " && " : "");

        //new Boolean( $V{ <condicao> }.booleanValue() )
        StringBuilder filtro = new StringBuilder(instancia);
        filtro.insert(atBoolean, condicao.toString());

        if (filtroAtual == null) {
            List elementos = root.elements();
            Element novoFiltro = root.addElement("filterExpression");
            //O elemento 'filterExpression' precisa ser inserido antes da primeira destas �reas que estiver presente no relat�rio. 
            String[] areas = new String[]{"group", "background", "title", "pageHeader", "columnHeader", "detail", "columnFooter", "pageFooter", "lastPageFooter", "summary"};

            //Define a express�o de filtro.
            novoFiltro.setText(filtro.toString());

            //Insere o filtro no relat�rio de acordo com o dtd.
            for (int i = 0; i < 10; i++) {
                int at = elementos.indexOf(root.element(areas[i]));

                if (at > 0) {
                    root.remove(novoFiltro.detach());
                    elementos.add(at, novoFiltro);
                    break;
                }
            }
        } else {
            filtroAtual.setText(filtro.toString());
        }
    }

    /**
     * O retorno da fun��o deve ser: pt, en, es
     * OBS: sempre em min�sculo
     * @param language
     */
    public void setIdioma(String language) {
        log.log().log(Level.INFO, "Language sent: {0}", language);
        // Se o idioma vier vazio, pega as informa��es do S.O.
        if (language.isEmpty()) {
            idioma = System.getProperty("user.language");
        } else {
            idioma = language;
        }
    }

    /**
     * Retorna o idioma corrente
     *
     * @return c�digo do idioma
     */
    private String getIdioma() {
        log.log().log(Level.INFO, "Current language : {0}", idioma.toString().trim());
        return idioma;
    }

    /**
     *
     * @param country
     */
    public void setPais(String country) {
        log.log().log(Level.INFO, "Country sent: {0}", country);
        // Se o pa�s vier vazio, pega as informa��es do S.O.
        if (country.isEmpty()) {
            pais = System.getProperty("user.country");
        } else {
            pais = country;
        }
    }

    /**
     * Retorna o pais no qual o relat�rio est� sendo executado.
     *
     * @return c�digo do pais
     */
    private String getPais() {
        log.log().log(Level.INFO, "Current country : {0}", pais.toString().trim());
        return pais;
    }

    /**
     *
     * @return
     */
    public String getGrupoEmpresa() {
        return grupo_empresa;
    }

    /**
     *
     * @param empresa
     */
    public void setGrupoEmpresa(String empresa) {
        this.grupo_empresa = empresa;
    }

    /**
     *
     * @return
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     *
     * @param empresa
     */
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    /**
     *
     * @return
     */
    public String getUnidadeNegocio() {
        return unidade_negocio;
    }

    /**
     *
     * @param unidade_negocio
     */
    public void setUnidadeNegocio(String unidade_negocio) {
        this.unidade_negocio = unidade_negocio;
    }

    /**
     *
     * @return
     */
    public String getFilial() {
        return filial;
    }

    /**
     *
     * @param filial
     */
    public void setFilial(String filial) {
        this.filial = filial;
    }

    /**
     *
     * @return
     */
    private String getTipoServidor() {
        return tipo_conexao;
    }

    /**
     *
     * @param connType
     */
    private void setTipoConexao(int connType) {
        //Verifico se a conexao e AS400
        if (connType == 4) {
            setTipoServidor("AS400");
        } else {
            setTipoServidor("DEFAULT");
        }
    }

    /**
     *
     * @param srvType
     */
    private void setTipoServidor(String srvType) {
        this.tipo_conexao = srvType;
    }

    /**
     *
     * @return
     */
    public int getDestino() {
        return destino;
    }

    /**
     *
     * @param repDestino
     */
    public void setDestino(int repDestino) {
        this.destino = repDestino;
    }

    /**
     *
     * @return
     */
    public boolean isImprime() {
        return imprimir;
    }

    /**
     *
     * @param configura
     */
    public void setImprime(String configura) {
        this.imprimir = configura.equalsIgnoreCase("T");
    }

    /**
     *
     * @return
     */
    public int getCopias() {
        return copias;
    }

    /**
     *
     * @param numCopias
     */
    public void setCopias(int numCopias) {
        this.copias = numCopias;
    }

    /**
     *
     * @return
     */
    public IReportConnection getConexao() {
        return conexao;
    }

    /**
     *
     * @param conexao
     */
    public void setConexao(IReportConnection conexao) {
        this.conexao = conexao;
    }

    /**
     *
     * @return
     */
    public String getCodificacao() {
        return codificacao;
    }

    /**
     *
     * @param codificacao
     */
    public void setCodificacao(String codificacao) {
        this.codificacao = !codificacao.isEmpty() ? codificacao : "UTF-8";
    }

    /**
     *
     * @param wsdl
     */
    public void setURL(String wsdl) {
        this.url_ws = wsdl;
    }

    /**
     *
     * @param wsdl
     * @return
     */
    private String getURL(boolean wsdl) {
        String url = (!url_ws.startsWith("http://") ? "http://" : "") + this.url_ws;

        if (wsdl) {
            url = url.concat("/IREPORT.apw?WSDL");
        }

        return url;
    }

    /**
     *
     * @return
     */
    private String getNome() {
        return nome;
    }

    /**
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Arquivo de valida��o da estrutura do relat�rio.
     */
    class DTDJasperResolver implements org.xml.sax.EntityResolver {

        @Override
        public org.xml.sax.InputSource resolveEntity(String publicId, String systemId) {
            if (systemId.equalsIgnoreCase("http://jasperreports.sourceforge.net/dtds/jasperreport.dtd")) {
                java.io.InputStream in = getClass().getResourceAsStream("/com/microsiga/iReportRun/jasperreport.dtd");
                return new org.xml.sax.InputSource(in);
            } else {
                return null;
            }
        }
    }
}